/*
               File: PromptSolicitacoes
        Description: Selecione Solicitacoes
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 23:18:56.11
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class promptsolicitacoes : GXHttpHandler, System.Web.SessionState.IRequiresSessionState
   {
      public promptsolicitacoes( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public promptsolicitacoes( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_InOutSolicitacoes_Codigo ,
                           ref String aP1_InOutSolicitacoes_Novo_Projeto )
      {
         this.AV7InOutSolicitacoes_Codigo = aP0_InOutSolicitacoes_Codigo;
         this.AV8InOutSolicitacoes_Novo_Projeto = aP1_InOutSolicitacoes_Novo_Projeto;
         executePrivate();
         aP0_InOutSolicitacoes_Codigo=this.AV7InOutSolicitacoes_Codigo;
         aP1_InOutSolicitacoes_Novo_Projeto=this.AV8InOutSolicitacoes_Novo_Projeto;
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavOrderedby = new GXCombobox();
         cmbavDynamicfiltersselector1 = new GXCombobox();
         cmbavDynamicfiltersoperator1 = new GXCombobox();
         radavSolicitacoes_novo_projeto1 = new GXRadio();
         cmbavDynamicfiltersselector2 = new GXCombobox();
         cmbavDynamicfiltersoperator2 = new GXCombobox();
         radavSolicitacoes_novo_projeto2 = new GXRadio();
         cmbavDynamicfiltersselector3 = new GXCombobox();
         cmbavDynamicfiltersoperator3 = new GXCombobox();
         radavSolicitacoes_novo_projeto3 = new GXRadio();
         radSolicitacoes_Novo_Projeto = new GXRadio();
         cmbSolicitacoes_Status = new GXCombobox();
         chkavDynamicfiltersenabled2 = new GXCheckbox();
         chkavDynamicfiltersenabled3 = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
            {
               nRC_GXsfl_80 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_80_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_80_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
            {
               subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV13OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
               AV14OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
               AV15DynamicFiltersSelector1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
               AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
               AV17Solicitacoes_Novo_Projeto1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Solicitacoes_Novo_Projeto1", AV17Solicitacoes_Novo_Projeto1);
               AV19DynamicFiltersSelector2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
               AV20DynamicFiltersOperator2 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)));
               AV21Solicitacoes_Novo_Projeto2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Solicitacoes_Novo_Projeto2", AV21Solicitacoes_Novo_Projeto2);
               AV23DynamicFiltersSelector3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
               AV24DynamicFiltersOperator3 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)));
               AV25Solicitacoes_Novo_Projeto3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25Solicitacoes_Novo_Projeto3", AV25Solicitacoes_Novo_Projeto3);
               AV18DynamicFiltersEnabled2 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
               AV22DynamicFiltersEnabled3 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
               AV31TFSolicitacoes_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31TFSolicitacoes_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV31TFSolicitacoes_Codigo), 6, 0)));
               AV32TFSolicitacoes_Codigo_To = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32TFSolicitacoes_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV32TFSolicitacoes_Codigo_To), 6, 0)));
               AV35TFContratada_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TFContratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35TFContratada_Codigo), 6, 0)));
               AV36TFContratada_Codigo_To = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36TFContratada_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36TFContratada_Codigo_To), 6, 0)));
               AV39TFSistema_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFSistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV39TFSistema_Codigo), 6, 0)));
               AV40TFSistema_Codigo_To = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40TFSistema_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV40TFSistema_Codigo_To), 6, 0)));
               AV43TFServico_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43TFServico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV43TFServico_Codigo), 6, 0)));
               AV44TFServico_Codigo_To = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44TFServico_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV44TFServico_Codigo_To), 6, 0)));
               AV47TFSolicitacoes_Novo_Projeto_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47TFSolicitacoes_Novo_Projeto_Sel", AV47TFSolicitacoes_Novo_Projeto_Sel);
               AV50TFSolicitacoes_Objetivo = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50TFSolicitacoes_Objetivo", AV50TFSolicitacoes_Objetivo);
               AV51TFSolicitacoes_Objetivo_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51TFSolicitacoes_Objetivo_Sel", AV51TFSolicitacoes_Objetivo_Sel);
               AV54TFSolicitacoes_Usuario = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54TFSolicitacoes_Usuario", StringUtil.LTrim( StringUtil.Str( (decimal)(AV54TFSolicitacoes_Usuario), 6, 0)));
               AV55TFSolicitacoes_Usuario_To = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55TFSolicitacoes_Usuario_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV55TFSolicitacoes_Usuario_To), 6, 0)));
               AV58TFSolicitacoes_Data = context.localUtil.ParseDTimeParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58TFSolicitacoes_Data", context.localUtil.TToC( AV58TFSolicitacoes_Data, 8, 5, 0, 3, "/", ":", " "));
               AV59TFSolicitacoes_Data_To = context.localUtil.ParseDTimeParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59TFSolicitacoes_Data_To", context.localUtil.TToC( AV59TFSolicitacoes_Data_To, 8, 5, 0, 3, "/", ":", " "));
               AV64TFSolicitacoes_Usuario_Ult = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64TFSolicitacoes_Usuario_Ult", StringUtil.LTrim( StringUtil.Str( (decimal)(AV64TFSolicitacoes_Usuario_Ult), 6, 0)));
               AV65TFSolicitacoes_Usuario_Ult_To = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65TFSolicitacoes_Usuario_Ult_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV65TFSolicitacoes_Usuario_Ult_To), 6, 0)));
               AV68TFSolicitacoes_Data_Ult = context.localUtil.ParseDTimeParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV68TFSolicitacoes_Data_Ult", context.localUtil.TToC( AV68TFSolicitacoes_Data_Ult, 8, 5, 0, 3, "/", ":", " "));
               AV69TFSolicitacoes_Data_Ult_To = context.localUtil.ParseDTimeParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69TFSolicitacoes_Data_Ult_To", context.localUtil.TToC( AV69TFSolicitacoes_Data_Ult_To, 8, 5, 0, 3, "/", ":", " "));
               AV33ddo_Solicitacoes_CodigoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33ddo_Solicitacoes_CodigoTitleControlIdToReplace", AV33ddo_Solicitacoes_CodigoTitleControlIdToReplace);
               AV37ddo_Contratada_CodigoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37ddo_Contratada_CodigoTitleControlIdToReplace", AV37ddo_Contratada_CodigoTitleControlIdToReplace);
               AV41ddo_Sistema_CodigoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41ddo_Sistema_CodigoTitleControlIdToReplace", AV41ddo_Sistema_CodigoTitleControlIdToReplace);
               AV45ddo_Servico_CodigoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45ddo_Servico_CodigoTitleControlIdToReplace", AV45ddo_Servico_CodigoTitleControlIdToReplace);
               AV48ddo_Solicitacoes_Novo_ProjetoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48ddo_Solicitacoes_Novo_ProjetoTitleControlIdToReplace", AV48ddo_Solicitacoes_Novo_ProjetoTitleControlIdToReplace);
               AV52ddo_Solicitacoes_ObjetivoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52ddo_Solicitacoes_ObjetivoTitleControlIdToReplace", AV52ddo_Solicitacoes_ObjetivoTitleControlIdToReplace);
               AV56ddo_Solicitacoes_UsuarioTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56ddo_Solicitacoes_UsuarioTitleControlIdToReplace", AV56ddo_Solicitacoes_UsuarioTitleControlIdToReplace);
               AV62ddo_Solicitacoes_DataTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62ddo_Solicitacoes_DataTitleControlIdToReplace", AV62ddo_Solicitacoes_DataTitleControlIdToReplace);
               AV66ddo_Solicitacoes_Usuario_UltTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66ddo_Solicitacoes_Usuario_UltTitleControlIdToReplace", AV66ddo_Solicitacoes_Usuario_UltTitleControlIdToReplace);
               AV72ddo_Solicitacoes_Data_UltTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV72ddo_Solicitacoes_Data_UltTitleControlIdToReplace", AV72ddo_Solicitacoes_Data_UltTitleControlIdToReplace);
               AV76ddo_Solicitacoes_StatusTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV76ddo_Solicitacoes_StatusTitleControlIdToReplace", AV76ddo_Solicitacoes_StatusTitleControlIdToReplace);
               ajax_req_read_hidden_sdt(GetNextPar( ), AV75TFSolicitacoes_Status_Sels);
               AV84Pgmname = GetNextPar( );
               ajax_req_read_hidden_sdt(GetNextPar( ), AV10GridState);
               AV27DynamicFiltersIgnoreFirst = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersIgnoreFirst", AV27DynamicFiltersIgnoreFirst);
               AV26DynamicFiltersRemoving = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17Solicitacoes_Novo_Projeto1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21Solicitacoes_Novo_Projeto2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25Solicitacoes_Novo_Projeto3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV31TFSolicitacoes_Codigo, AV32TFSolicitacoes_Codigo_To, AV35TFContratada_Codigo, AV36TFContratada_Codigo_To, AV39TFSistema_Codigo, AV40TFSistema_Codigo_To, AV43TFServico_Codigo, AV44TFServico_Codigo_To, AV47TFSolicitacoes_Novo_Projeto_Sel, AV50TFSolicitacoes_Objetivo, AV51TFSolicitacoes_Objetivo_Sel, AV54TFSolicitacoes_Usuario, AV55TFSolicitacoes_Usuario_To, AV58TFSolicitacoes_Data, AV59TFSolicitacoes_Data_To, AV64TFSolicitacoes_Usuario_Ult, AV65TFSolicitacoes_Usuario_Ult_To, AV68TFSolicitacoes_Data_Ult, AV69TFSolicitacoes_Data_Ult_To, AV33ddo_Solicitacoes_CodigoTitleControlIdToReplace, AV37ddo_Contratada_CodigoTitleControlIdToReplace, AV41ddo_Sistema_CodigoTitleControlIdToReplace, AV45ddo_Servico_CodigoTitleControlIdToReplace, AV48ddo_Solicitacoes_Novo_ProjetoTitleControlIdToReplace, AV52ddo_Solicitacoes_ObjetivoTitleControlIdToReplace, AV56ddo_Solicitacoes_UsuarioTitleControlIdToReplace, AV62ddo_Solicitacoes_DataTitleControlIdToReplace, AV66ddo_Solicitacoes_Usuario_UltTitleControlIdToReplace, AV72ddo_Solicitacoes_Data_UltTitleControlIdToReplace, AV76ddo_Solicitacoes_StatusTitleControlIdToReplace, AV75TFSolicitacoes_Status_Sels, AV84Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               AV7InOutSolicitacoes_Codigo = (int)(NumberUtil.Val( gxfirstwebparm, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InOutSolicitacoes_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7InOutSolicitacoes_Codigo), 6, 0)));
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
               {
                  AV8InOutSolicitacoes_Novo_Projeto = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8InOutSolicitacoes_Novo_Projeto", AV8InOutSolicitacoes_Novo_Projeto);
               }
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            ValidateSpaRequest();
            PAAW2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV84Pgmname = "PromptSolicitacoes";
               context.Gx_err = 0;
               WSAW2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  WEAW2( ) ;
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?202042823185697");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("promptsolicitacoes.aspx") + "?" + UrlEncode("" +AV7InOutSolicitacoes_Codigo) + "," + UrlEncode(StringUtil.RTrim(AV8InOutSolicitacoes_Novo_Projeto))+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDDSC", StringUtil.BoolToStr( AV14OrderedDsc));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR1", AV15DynamicFiltersSelector1);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR1", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV16DynamicFiltersOperator1), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vSOLICITACOES_NOVO_PROJETO1", StringUtil.RTrim( AV17Solicitacoes_Novo_Projeto1));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR2", AV19DynamicFiltersSelector2);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR2", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV20DynamicFiltersOperator2), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vSOLICITACOES_NOVO_PROJETO2", StringUtil.RTrim( AV21Solicitacoes_Novo_Projeto2));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR3", AV23DynamicFiltersSelector3);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR3", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV24DynamicFiltersOperator3), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vSOLICITACOES_NOVO_PROJETO3", StringUtil.RTrim( AV25Solicitacoes_Novo_Projeto3));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED2", StringUtil.BoolToStr( AV18DynamicFiltersEnabled2));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED3", StringUtil.BoolToStr( AV22DynamicFiltersEnabled3));
         GxWebStd.gx_hidden_field( context, "GXH_vTFSOLICITACOES_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV31TFSolicitacoes_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFSOLICITACOES_CODIGO_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV32TFSolicitacoes_Codigo_To), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATADA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV35TFContratada_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFCONTRATADA_CODIGO_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV36TFContratada_Codigo_To), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFSISTEMA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV39TFSistema_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFSISTEMA_CODIGO_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV40TFSistema_Codigo_To), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFSERVICO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV43TFServico_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFSERVICO_CODIGO_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV44TFServico_Codigo_To), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFSOLICITACOES_NOVO_PROJETO_SEL", StringUtil.RTrim( AV47TFSolicitacoes_Novo_Projeto_Sel));
         GxWebStd.gx_hidden_field( context, "GXH_vTFSOLICITACOES_OBJETIVO", AV50TFSolicitacoes_Objetivo);
         GxWebStd.gx_hidden_field( context, "GXH_vTFSOLICITACOES_OBJETIVO_SEL", AV51TFSolicitacoes_Objetivo_Sel);
         GxWebStd.gx_hidden_field( context, "GXH_vTFSOLICITACOES_USUARIO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV54TFSolicitacoes_Usuario), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFSOLICITACOES_USUARIO_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV55TFSolicitacoes_Usuario_To), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFSOLICITACOES_DATA", context.localUtil.TToC( AV58TFSolicitacoes_Data, 10, 8, 0, 3, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "GXH_vTFSOLICITACOES_DATA_TO", context.localUtil.TToC( AV59TFSolicitacoes_Data_To, 10, 8, 0, 3, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "GXH_vTFSOLICITACOES_USUARIO_ULT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV64TFSolicitacoes_Usuario_Ult), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFSOLICITACOES_USUARIO_ULT_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV65TFSolicitacoes_Usuario_Ult_To), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFSOLICITACOES_DATA_ULT", context.localUtil.TToC( AV68TFSolicitacoes_Data_Ult, 10, 8, 0, 3, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "GXH_vTFSOLICITACOES_DATA_ULT_TO", context.localUtil.TToC( AV69TFSolicitacoes_Data_Ult_To, 10, 8, 0, 3, "/", ":", " "));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_80", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_80), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV79GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV80GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vDDO_TITLESETTINGSICONS", AV77DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vDDO_TITLESETTINGSICONS", AV77DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vSOLICITACOES_CODIGOTITLEFILTERDATA", AV30Solicitacoes_CodigoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vSOLICITACOES_CODIGOTITLEFILTERDATA", AV30Solicitacoes_CodigoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATADA_CODIGOTITLEFILTERDATA", AV34Contratada_CodigoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATADA_CODIGOTITLEFILTERDATA", AV34Contratada_CodigoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vSISTEMA_CODIGOTITLEFILTERDATA", AV38Sistema_CodigoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vSISTEMA_CODIGOTITLEFILTERDATA", AV38Sistema_CodigoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vSERVICO_CODIGOTITLEFILTERDATA", AV42Servico_CodigoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vSERVICO_CODIGOTITLEFILTERDATA", AV42Servico_CodigoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vSOLICITACOES_NOVO_PROJETOTITLEFILTERDATA", AV46Solicitacoes_Novo_ProjetoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vSOLICITACOES_NOVO_PROJETOTITLEFILTERDATA", AV46Solicitacoes_Novo_ProjetoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vSOLICITACOES_OBJETIVOTITLEFILTERDATA", AV49Solicitacoes_ObjetivoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vSOLICITACOES_OBJETIVOTITLEFILTERDATA", AV49Solicitacoes_ObjetivoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vSOLICITACOES_USUARIOTITLEFILTERDATA", AV53Solicitacoes_UsuarioTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vSOLICITACOES_USUARIOTITLEFILTERDATA", AV53Solicitacoes_UsuarioTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vSOLICITACOES_DATATITLEFILTERDATA", AV57Solicitacoes_DataTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vSOLICITACOES_DATATITLEFILTERDATA", AV57Solicitacoes_DataTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vSOLICITACOES_USUARIO_ULTTITLEFILTERDATA", AV63Solicitacoes_Usuario_UltTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vSOLICITACOES_USUARIO_ULTTITLEFILTERDATA", AV63Solicitacoes_Usuario_UltTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vSOLICITACOES_DATA_ULTTITLEFILTERDATA", AV67Solicitacoes_Data_UltTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vSOLICITACOES_DATA_ULTTITLEFILTERDATA", AV67Solicitacoes_Data_UltTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vSOLICITACOES_STATUSTITLEFILTERDATA", AV73Solicitacoes_StatusTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vSOLICITACOES_STATUSTITLEFILTERDATA", AV73Solicitacoes_StatusTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTFSOLICITACOES_STATUS_SELS", AV75TFSolicitacoes_Status_Sels);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTFSOLICITACOES_STATUS_SELS", AV75TFSolicitacoes_Status_Sels);
         }
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV84Pgmname));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vGRIDSTATE", AV10GridState);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vGRIDSTATE", AV10GridState);
         }
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSIGNOREFIRST", AV27DynamicFiltersIgnoreFirst);
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSREMOVING", AV26DynamicFiltersRemoving);
         GxWebStd.gx_hidden_field( context, "vINOUTSOLICITACOES_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7InOutSolicitacoes_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINOUTSOLICITACOES_NOVO_PROJETO", StringUtil.RTrim( AV8InOutSolicitacoes_Novo_Projeto));
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_CODIGO_Caption", StringUtil.RTrim( Ddo_solicitacoes_codigo_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_CODIGO_Tooltip", StringUtil.RTrim( Ddo_solicitacoes_codigo_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_CODIGO_Cls", StringUtil.RTrim( Ddo_solicitacoes_codigo_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_CODIGO_Filteredtext_set", StringUtil.RTrim( Ddo_solicitacoes_codigo_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_CODIGO_Filteredtextto_set", StringUtil.RTrim( Ddo_solicitacoes_codigo_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_CODIGO_Dropdownoptionstype", StringUtil.RTrim( Ddo_solicitacoes_codigo_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_CODIGO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_solicitacoes_codigo_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_CODIGO_Includesortasc", StringUtil.BoolToStr( Ddo_solicitacoes_codigo_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_CODIGO_Includesortdsc", StringUtil.BoolToStr( Ddo_solicitacoes_codigo_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_CODIGO_Sortedstatus", StringUtil.RTrim( Ddo_solicitacoes_codigo_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_CODIGO_Includefilter", StringUtil.BoolToStr( Ddo_solicitacoes_codigo_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_CODIGO_Filtertype", StringUtil.RTrim( Ddo_solicitacoes_codigo_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_CODIGO_Filterisrange", StringUtil.BoolToStr( Ddo_solicitacoes_codigo_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_CODIGO_Includedatalist", StringUtil.BoolToStr( Ddo_solicitacoes_codigo_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_CODIGO_Datalistfixedvalues", StringUtil.RTrim( Ddo_solicitacoes_codigo_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_CODIGO_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_solicitacoes_codigo_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_CODIGO_Sortasc", StringUtil.RTrim( Ddo_solicitacoes_codigo_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_CODIGO_Sortdsc", StringUtil.RTrim( Ddo_solicitacoes_codigo_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_CODIGO_Loadingdata", StringUtil.RTrim( Ddo_solicitacoes_codigo_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_CODIGO_Cleanfilter", StringUtil.RTrim( Ddo_solicitacoes_codigo_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_CODIGO_Rangefilterfrom", StringUtil.RTrim( Ddo_solicitacoes_codigo_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_CODIGO_Rangefilterto", StringUtil.RTrim( Ddo_solicitacoes_codigo_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_CODIGO_Noresultsfound", StringUtil.RTrim( Ddo_solicitacoes_codigo_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_CODIGO_Searchbuttontext", StringUtil.RTrim( Ddo_solicitacoes_codigo_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_CODIGO_Caption", StringUtil.RTrim( Ddo_contratada_codigo_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_CODIGO_Tooltip", StringUtil.RTrim( Ddo_contratada_codigo_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_CODIGO_Cls", StringUtil.RTrim( Ddo_contratada_codigo_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_CODIGO_Filteredtext_set", StringUtil.RTrim( Ddo_contratada_codigo_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_CODIGO_Filteredtextto_set", StringUtil.RTrim( Ddo_contratada_codigo_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_CODIGO_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratada_codigo_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_CODIGO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratada_codigo_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_CODIGO_Includesortasc", StringUtil.BoolToStr( Ddo_contratada_codigo_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_CODIGO_Includesortdsc", StringUtil.BoolToStr( Ddo_contratada_codigo_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_CODIGO_Sortedstatus", StringUtil.RTrim( Ddo_contratada_codigo_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_CODIGO_Includefilter", StringUtil.BoolToStr( Ddo_contratada_codigo_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_CODIGO_Filtertype", StringUtil.RTrim( Ddo_contratada_codigo_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_CODIGO_Filterisrange", StringUtil.BoolToStr( Ddo_contratada_codigo_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_CODIGO_Includedatalist", StringUtil.BoolToStr( Ddo_contratada_codigo_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_CODIGO_Datalistfixedvalues", StringUtil.RTrim( Ddo_contratada_codigo_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_CODIGO_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_contratada_codigo_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_CODIGO_Sortasc", StringUtil.RTrim( Ddo_contratada_codigo_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_CODIGO_Sortdsc", StringUtil.RTrim( Ddo_contratada_codigo_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_CODIGO_Loadingdata", StringUtil.RTrim( Ddo_contratada_codigo_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_CODIGO_Cleanfilter", StringUtil.RTrim( Ddo_contratada_codigo_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_CODIGO_Rangefilterfrom", StringUtil.RTrim( Ddo_contratada_codigo_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_CODIGO_Rangefilterto", StringUtil.RTrim( Ddo_contratada_codigo_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_CODIGO_Noresultsfound", StringUtil.RTrim( Ddo_contratada_codigo_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_CODIGO_Searchbuttontext", StringUtil.RTrim( Ddo_contratada_codigo_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_CODIGO_Caption", StringUtil.RTrim( Ddo_sistema_codigo_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_CODIGO_Tooltip", StringUtil.RTrim( Ddo_sistema_codigo_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_CODIGO_Cls", StringUtil.RTrim( Ddo_sistema_codigo_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_CODIGO_Filteredtext_set", StringUtil.RTrim( Ddo_sistema_codigo_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_CODIGO_Filteredtextto_set", StringUtil.RTrim( Ddo_sistema_codigo_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_CODIGO_Dropdownoptionstype", StringUtil.RTrim( Ddo_sistema_codigo_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_CODIGO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_sistema_codigo_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_CODIGO_Includesortasc", StringUtil.BoolToStr( Ddo_sistema_codigo_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_CODIGO_Includesortdsc", StringUtil.BoolToStr( Ddo_sistema_codigo_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_CODIGO_Sortedstatus", StringUtil.RTrim( Ddo_sistema_codigo_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_CODIGO_Includefilter", StringUtil.BoolToStr( Ddo_sistema_codigo_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_CODIGO_Filtertype", StringUtil.RTrim( Ddo_sistema_codigo_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_CODIGO_Filterisrange", StringUtil.BoolToStr( Ddo_sistema_codigo_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_CODIGO_Includedatalist", StringUtil.BoolToStr( Ddo_sistema_codigo_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_CODIGO_Datalistfixedvalues", StringUtil.RTrim( Ddo_sistema_codigo_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_CODIGO_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_sistema_codigo_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_CODIGO_Sortasc", StringUtil.RTrim( Ddo_sistema_codigo_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_CODIGO_Sortdsc", StringUtil.RTrim( Ddo_sistema_codigo_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_CODIGO_Loadingdata", StringUtil.RTrim( Ddo_sistema_codigo_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_CODIGO_Cleanfilter", StringUtil.RTrim( Ddo_sistema_codigo_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_CODIGO_Rangefilterfrom", StringUtil.RTrim( Ddo_sistema_codigo_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_CODIGO_Rangefilterto", StringUtil.RTrim( Ddo_sistema_codigo_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_CODIGO_Noresultsfound", StringUtil.RTrim( Ddo_sistema_codigo_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_CODIGO_Searchbuttontext", StringUtil.RTrim( Ddo_sistema_codigo_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_CODIGO_Caption", StringUtil.RTrim( Ddo_servico_codigo_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_CODIGO_Tooltip", StringUtil.RTrim( Ddo_servico_codigo_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_CODIGO_Cls", StringUtil.RTrim( Ddo_servico_codigo_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_CODIGO_Filteredtext_set", StringUtil.RTrim( Ddo_servico_codigo_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_CODIGO_Filteredtextto_set", StringUtil.RTrim( Ddo_servico_codigo_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_CODIGO_Dropdownoptionstype", StringUtil.RTrim( Ddo_servico_codigo_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_CODIGO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_servico_codigo_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_CODIGO_Includesortasc", StringUtil.BoolToStr( Ddo_servico_codigo_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_CODIGO_Includesortdsc", StringUtil.BoolToStr( Ddo_servico_codigo_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_CODIGO_Sortedstatus", StringUtil.RTrim( Ddo_servico_codigo_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_CODIGO_Includefilter", StringUtil.BoolToStr( Ddo_servico_codigo_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_CODIGO_Filtertype", StringUtil.RTrim( Ddo_servico_codigo_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_CODIGO_Filterisrange", StringUtil.BoolToStr( Ddo_servico_codigo_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_CODIGO_Includedatalist", StringUtil.BoolToStr( Ddo_servico_codigo_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_CODIGO_Datalistfixedvalues", StringUtil.RTrim( Ddo_servico_codigo_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_CODIGO_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_servico_codigo_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_CODIGO_Sortasc", StringUtil.RTrim( Ddo_servico_codigo_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_CODIGO_Sortdsc", StringUtil.RTrim( Ddo_servico_codigo_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_CODIGO_Loadingdata", StringUtil.RTrim( Ddo_servico_codigo_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_CODIGO_Cleanfilter", StringUtil.RTrim( Ddo_servico_codigo_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_CODIGO_Rangefilterfrom", StringUtil.RTrim( Ddo_servico_codigo_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_CODIGO_Rangefilterto", StringUtil.RTrim( Ddo_servico_codigo_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_CODIGO_Noresultsfound", StringUtil.RTrim( Ddo_servico_codigo_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_CODIGO_Searchbuttontext", StringUtil.RTrim( Ddo_servico_codigo_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_NOVO_PROJETO_Caption", StringUtil.RTrim( Ddo_solicitacoes_novo_projeto_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_NOVO_PROJETO_Tooltip", StringUtil.RTrim( Ddo_solicitacoes_novo_projeto_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_NOVO_PROJETO_Cls", StringUtil.RTrim( Ddo_solicitacoes_novo_projeto_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_NOVO_PROJETO_Selectedvalue_set", StringUtil.RTrim( Ddo_solicitacoes_novo_projeto_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_NOVO_PROJETO_Dropdownoptionstype", StringUtil.RTrim( Ddo_solicitacoes_novo_projeto_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_NOVO_PROJETO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_solicitacoes_novo_projeto_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_NOVO_PROJETO_Includesortasc", StringUtil.BoolToStr( Ddo_solicitacoes_novo_projeto_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_NOVO_PROJETO_Includesortdsc", StringUtil.BoolToStr( Ddo_solicitacoes_novo_projeto_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_NOVO_PROJETO_Sortedstatus", StringUtil.RTrim( Ddo_solicitacoes_novo_projeto_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_NOVO_PROJETO_Includefilter", StringUtil.BoolToStr( Ddo_solicitacoes_novo_projeto_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_NOVO_PROJETO_Filtertype", StringUtil.RTrim( Ddo_solicitacoes_novo_projeto_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_NOVO_PROJETO_Filterisrange", StringUtil.BoolToStr( Ddo_solicitacoes_novo_projeto_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_NOVO_PROJETO_Includedatalist", StringUtil.BoolToStr( Ddo_solicitacoes_novo_projeto_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_NOVO_PROJETO_Datalisttype", StringUtil.RTrim( Ddo_solicitacoes_novo_projeto_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_NOVO_PROJETO_Datalistfixedvalues", StringUtil.RTrim( Ddo_solicitacoes_novo_projeto_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_NOVO_PROJETO_Datalistproc", StringUtil.RTrim( Ddo_solicitacoes_novo_projeto_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_NOVO_PROJETO_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_solicitacoes_novo_projeto_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_NOVO_PROJETO_Sortasc", StringUtil.RTrim( Ddo_solicitacoes_novo_projeto_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_NOVO_PROJETO_Sortdsc", StringUtil.RTrim( Ddo_solicitacoes_novo_projeto_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_NOVO_PROJETO_Loadingdata", StringUtil.RTrim( Ddo_solicitacoes_novo_projeto_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_NOVO_PROJETO_Cleanfilter", StringUtil.RTrim( Ddo_solicitacoes_novo_projeto_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_NOVO_PROJETO_Rangefilterfrom", StringUtil.RTrim( Ddo_solicitacoes_novo_projeto_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_NOVO_PROJETO_Rangefilterto", StringUtil.RTrim( Ddo_solicitacoes_novo_projeto_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_NOVO_PROJETO_Noresultsfound", StringUtil.RTrim( Ddo_solicitacoes_novo_projeto_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_NOVO_PROJETO_Searchbuttontext", StringUtil.RTrim( Ddo_solicitacoes_novo_projeto_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_OBJETIVO_Caption", StringUtil.RTrim( Ddo_solicitacoes_objetivo_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_OBJETIVO_Tooltip", StringUtil.RTrim( Ddo_solicitacoes_objetivo_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_OBJETIVO_Cls", StringUtil.RTrim( Ddo_solicitacoes_objetivo_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_OBJETIVO_Filteredtext_set", StringUtil.RTrim( Ddo_solicitacoes_objetivo_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_OBJETIVO_Selectedvalue_set", StringUtil.RTrim( Ddo_solicitacoes_objetivo_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_OBJETIVO_Dropdownoptionstype", StringUtil.RTrim( Ddo_solicitacoes_objetivo_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_OBJETIVO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_solicitacoes_objetivo_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_OBJETIVO_Includesortasc", StringUtil.BoolToStr( Ddo_solicitacoes_objetivo_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_OBJETIVO_Includesortdsc", StringUtil.BoolToStr( Ddo_solicitacoes_objetivo_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_OBJETIVO_Sortedstatus", StringUtil.RTrim( Ddo_solicitacoes_objetivo_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_OBJETIVO_Includefilter", StringUtil.BoolToStr( Ddo_solicitacoes_objetivo_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_OBJETIVO_Filtertype", StringUtil.RTrim( Ddo_solicitacoes_objetivo_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_OBJETIVO_Filterisrange", StringUtil.BoolToStr( Ddo_solicitacoes_objetivo_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_OBJETIVO_Includedatalist", StringUtil.BoolToStr( Ddo_solicitacoes_objetivo_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_OBJETIVO_Datalisttype", StringUtil.RTrim( Ddo_solicitacoes_objetivo_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_OBJETIVO_Datalistfixedvalues", StringUtil.RTrim( Ddo_solicitacoes_objetivo_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_OBJETIVO_Datalistproc", StringUtil.RTrim( Ddo_solicitacoes_objetivo_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_OBJETIVO_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_solicitacoes_objetivo_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_OBJETIVO_Sortasc", StringUtil.RTrim( Ddo_solicitacoes_objetivo_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_OBJETIVO_Sortdsc", StringUtil.RTrim( Ddo_solicitacoes_objetivo_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_OBJETIVO_Loadingdata", StringUtil.RTrim( Ddo_solicitacoes_objetivo_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_OBJETIVO_Cleanfilter", StringUtil.RTrim( Ddo_solicitacoes_objetivo_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_OBJETIVO_Rangefilterfrom", StringUtil.RTrim( Ddo_solicitacoes_objetivo_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_OBJETIVO_Rangefilterto", StringUtil.RTrim( Ddo_solicitacoes_objetivo_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_OBJETIVO_Noresultsfound", StringUtil.RTrim( Ddo_solicitacoes_objetivo_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_OBJETIVO_Searchbuttontext", StringUtil.RTrim( Ddo_solicitacoes_objetivo_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_USUARIO_Caption", StringUtil.RTrim( Ddo_solicitacoes_usuario_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_USUARIO_Tooltip", StringUtil.RTrim( Ddo_solicitacoes_usuario_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_USUARIO_Cls", StringUtil.RTrim( Ddo_solicitacoes_usuario_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_USUARIO_Filteredtext_set", StringUtil.RTrim( Ddo_solicitacoes_usuario_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_USUARIO_Filteredtextto_set", StringUtil.RTrim( Ddo_solicitacoes_usuario_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_USUARIO_Dropdownoptionstype", StringUtil.RTrim( Ddo_solicitacoes_usuario_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_USUARIO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_solicitacoes_usuario_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_USUARIO_Includesortasc", StringUtil.BoolToStr( Ddo_solicitacoes_usuario_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_USUARIO_Includesortdsc", StringUtil.BoolToStr( Ddo_solicitacoes_usuario_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_USUARIO_Sortedstatus", StringUtil.RTrim( Ddo_solicitacoes_usuario_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_USUARIO_Includefilter", StringUtil.BoolToStr( Ddo_solicitacoes_usuario_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_USUARIO_Filtertype", StringUtil.RTrim( Ddo_solicitacoes_usuario_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_USUARIO_Filterisrange", StringUtil.BoolToStr( Ddo_solicitacoes_usuario_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_USUARIO_Includedatalist", StringUtil.BoolToStr( Ddo_solicitacoes_usuario_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_USUARIO_Datalistfixedvalues", StringUtil.RTrim( Ddo_solicitacoes_usuario_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_USUARIO_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_solicitacoes_usuario_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_USUARIO_Sortasc", StringUtil.RTrim( Ddo_solicitacoes_usuario_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_USUARIO_Sortdsc", StringUtil.RTrim( Ddo_solicitacoes_usuario_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_USUARIO_Loadingdata", StringUtil.RTrim( Ddo_solicitacoes_usuario_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_USUARIO_Cleanfilter", StringUtil.RTrim( Ddo_solicitacoes_usuario_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_USUARIO_Rangefilterfrom", StringUtil.RTrim( Ddo_solicitacoes_usuario_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_USUARIO_Rangefilterto", StringUtil.RTrim( Ddo_solicitacoes_usuario_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_USUARIO_Noresultsfound", StringUtil.RTrim( Ddo_solicitacoes_usuario_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_USUARIO_Searchbuttontext", StringUtil.RTrim( Ddo_solicitacoes_usuario_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_DATA_Caption", StringUtil.RTrim( Ddo_solicitacoes_data_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_DATA_Tooltip", StringUtil.RTrim( Ddo_solicitacoes_data_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_DATA_Cls", StringUtil.RTrim( Ddo_solicitacoes_data_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_DATA_Filteredtext_set", StringUtil.RTrim( Ddo_solicitacoes_data_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_DATA_Filteredtextto_set", StringUtil.RTrim( Ddo_solicitacoes_data_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_DATA_Dropdownoptionstype", StringUtil.RTrim( Ddo_solicitacoes_data_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_DATA_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_solicitacoes_data_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_DATA_Includesortasc", StringUtil.BoolToStr( Ddo_solicitacoes_data_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_DATA_Includesortdsc", StringUtil.BoolToStr( Ddo_solicitacoes_data_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_DATA_Sortedstatus", StringUtil.RTrim( Ddo_solicitacoes_data_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_DATA_Includefilter", StringUtil.BoolToStr( Ddo_solicitacoes_data_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_DATA_Filtertype", StringUtil.RTrim( Ddo_solicitacoes_data_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_DATA_Filterisrange", StringUtil.BoolToStr( Ddo_solicitacoes_data_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_DATA_Includedatalist", StringUtil.BoolToStr( Ddo_solicitacoes_data_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_DATA_Datalistfixedvalues", StringUtil.RTrim( Ddo_solicitacoes_data_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_DATA_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_solicitacoes_data_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_DATA_Sortasc", StringUtil.RTrim( Ddo_solicitacoes_data_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_DATA_Sortdsc", StringUtil.RTrim( Ddo_solicitacoes_data_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_DATA_Loadingdata", StringUtil.RTrim( Ddo_solicitacoes_data_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_DATA_Cleanfilter", StringUtil.RTrim( Ddo_solicitacoes_data_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_DATA_Rangefilterfrom", StringUtil.RTrim( Ddo_solicitacoes_data_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_DATA_Rangefilterto", StringUtil.RTrim( Ddo_solicitacoes_data_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_DATA_Noresultsfound", StringUtil.RTrim( Ddo_solicitacoes_data_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_DATA_Searchbuttontext", StringUtil.RTrim( Ddo_solicitacoes_data_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_USUARIO_ULT_Caption", StringUtil.RTrim( Ddo_solicitacoes_usuario_ult_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_USUARIO_ULT_Tooltip", StringUtil.RTrim( Ddo_solicitacoes_usuario_ult_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_USUARIO_ULT_Cls", StringUtil.RTrim( Ddo_solicitacoes_usuario_ult_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_USUARIO_ULT_Filteredtext_set", StringUtil.RTrim( Ddo_solicitacoes_usuario_ult_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_USUARIO_ULT_Filteredtextto_set", StringUtil.RTrim( Ddo_solicitacoes_usuario_ult_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_USUARIO_ULT_Dropdownoptionstype", StringUtil.RTrim( Ddo_solicitacoes_usuario_ult_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_USUARIO_ULT_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_solicitacoes_usuario_ult_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_USUARIO_ULT_Includesortasc", StringUtil.BoolToStr( Ddo_solicitacoes_usuario_ult_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_USUARIO_ULT_Includesortdsc", StringUtil.BoolToStr( Ddo_solicitacoes_usuario_ult_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_USUARIO_ULT_Sortedstatus", StringUtil.RTrim( Ddo_solicitacoes_usuario_ult_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_USUARIO_ULT_Includefilter", StringUtil.BoolToStr( Ddo_solicitacoes_usuario_ult_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_USUARIO_ULT_Filtertype", StringUtil.RTrim( Ddo_solicitacoes_usuario_ult_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_USUARIO_ULT_Filterisrange", StringUtil.BoolToStr( Ddo_solicitacoes_usuario_ult_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_USUARIO_ULT_Includedatalist", StringUtil.BoolToStr( Ddo_solicitacoes_usuario_ult_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_USUARIO_ULT_Datalistfixedvalues", StringUtil.RTrim( Ddo_solicitacoes_usuario_ult_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_USUARIO_ULT_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_solicitacoes_usuario_ult_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_USUARIO_ULT_Sortasc", StringUtil.RTrim( Ddo_solicitacoes_usuario_ult_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_USUARIO_ULT_Sortdsc", StringUtil.RTrim( Ddo_solicitacoes_usuario_ult_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_USUARIO_ULT_Loadingdata", StringUtil.RTrim( Ddo_solicitacoes_usuario_ult_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_USUARIO_ULT_Cleanfilter", StringUtil.RTrim( Ddo_solicitacoes_usuario_ult_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_USUARIO_ULT_Rangefilterfrom", StringUtil.RTrim( Ddo_solicitacoes_usuario_ult_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_USUARIO_ULT_Rangefilterto", StringUtil.RTrim( Ddo_solicitacoes_usuario_ult_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_USUARIO_ULT_Noresultsfound", StringUtil.RTrim( Ddo_solicitacoes_usuario_ult_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_USUARIO_ULT_Searchbuttontext", StringUtil.RTrim( Ddo_solicitacoes_usuario_ult_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_DATA_ULT_Caption", StringUtil.RTrim( Ddo_solicitacoes_data_ult_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_DATA_ULT_Tooltip", StringUtil.RTrim( Ddo_solicitacoes_data_ult_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_DATA_ULT_Cls", StringUtil.RTrim( Ddo_solicitacoes_data_ult_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_DATA_ULT_Filteredtext_set", StringUtil.RTrim( Ddo_solicitacoes_data_ult_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_DATA_ULT_Filteredtextto_set", StringUtil.RTrim( Ddo_solicitacoes_data_ult_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_DATA_ULT_Dropdownoptionstype", StringUtil.RTrim( Ddo_solicitacoes_data_ult_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_DATA_ULT_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_solicitacoes_data_ult_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_DATA_ULT_Includesortasc", StringUtil.BoolToStr( Ddo_solicitacoes_data_ult_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_DATA_ULT_Includesortdsc", StringUtil.BoolToStr( Ddo_solicitacoes_data_ult_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_DATA_ULT_Sortedstatus", StringUtil.RTrim( Ddo_solicitacoes_data_ult_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_DATA_ULT_Includefilter", StringUtil.BoolToStr( Ddo_solicitacoes_data_ult_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_DATA_ULT_Filtertype", StringUtil.RTrim( Ddo_solicitacoes_data_ult_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_DATA_ULT_Filterisrange", StringUtil.BoolToStr( Ddo_solicitacoes_data_ult_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_DATA_ULT_Includedatalist", StringUtil.BoolToStr( Ddo_solicitacoes_data_ult_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_DATA_ULT_Datalistfixedvalues", StringUtil.RTrim( Ddo_solicitacoes_data_ult_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_DATA_ULT_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_solicitacoes_data_ult_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_DATA_ULT_Sortasc", StringUtil.RTrim( Ddo_solicitacoes_data_ult_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_DATA_ULT_Sortdsc", StringUtil.RTrim( Ddo_solicitacoes_data_ult_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_DATA_ULT_Loadingdata", StringUtil.RTrim( Ddo_solicitacoes_data_ult_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_DATA_ULT_Cleanfilter", StringUtil.RTrim( Ddo_solicitacoes_data_ult_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_DATA_ULT_Rangefilterfrom", StringUtil.RTrim( Ddo_solicitacoes_data_ult_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_DATA_ULT_Rangefilterto", StringUtil.RTrim( Ddo_solicitacoes_data_ult_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_DATA_ULT_Noresultsfound", StringUtil.RTrim( Ddo_solicitacoes_data_ult_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_DATA_ULT_Searchbuttontext", StringUtil.RTrim( Ddo_solicitacoes_data_ult_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_STATUS_Caption", StringUtil.RTrim( Ddo_solicitacoes_status_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_STATUS_Tooltip", StringUtil.RTrim( Ddo_solicitacoes_status_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_STATUS_Cls", StringUtil.RTrim( Ddo_solicitacoes_status_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_STATUS_Selectedvalue_set", StringUtil.RTrim( Ddo_solicitacoes_status_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_STATUS_Dropdownoptionstype", StringUtil.RTrim( Ddo_solicitacoes_status_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_STATUS_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_solicitacoes_status_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_STATUS_Includesortasc", StringUtil.BoolToStr( Ddo_solicitacoes_status_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_STATUS_Includesortdsc", StringUtil.BoolToStr( Ddo_solicitacoes_status_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_STATUS_Sortedstatus", StringUtil.RTrim( Ddo_solicitacoes_status_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_STATUS_Includefilter", StringUtil.BoolToStr( Ddo_solicitacoes_status_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_STATUS_Filterisrange", StringUtil.BoolToStr( Ddo_solicitacoes_status_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_STATUS_Includedatalist", StringUtil.BoolToStr( Ddo_solicitacoes_status_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_STATUS_Datalisttype", StringUtil.RTrim( Ddo_solicitacoes_status_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_STATUS_Allowmultipleselection", StringUtil.BoolToStr( Ddo_solicitacoes_status_Allowmultipleselection));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_STATUS_Datalistfixedvalues", StringUtil.RTrim( Ddo_solicitacoes_status_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_STATUS_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_solicitacoes_status_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_STATUS_Sortasc", StringUtil.RTrim( Ddo_solicitacoes_status_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_STATUS_Sortdsc", StringUtil.RTrim( Ddo_solicitacoes_status_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_STATUS_Loadingdata", StringUtil.RTrim( Ddo_solicitacoes_status_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_STATUS_Cleanfilter", StringUtil.RTrim( Ddo_solicitacoes_status_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_STATUS_Rangefilterfrom", StringUtil.RTrim( Ddo_solicitacoes_status_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_STATUS_Rangefilterto", StringUtil.RTrim( Ddo_solicitacoes_status_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_STATUS_Noresultsfound", StringUtil.RTrim( Ddo_solicitacoes_status_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_STATUS_Searchbuttontext", StringUtil.RTrim( Ddo_solicitacoes_status_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_CODIGO_Activeeventkey", StringUtil.RTrim( Ddo_solicitacoes_codigo_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_CODIGO_Filteredtext_get", StringUtil.RTrim( Ddo_solicitacoes_codigo_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_CODIGO_Filteredtextto_get", StringUtil.RTrim( Ddo_solicitacoes_codigo_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_CODIGO_Activeeventkey", StringUtil.RTrim( Ddo_contratada_codigo_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_CODIGO_Filteredtext_get", StringUtil.RTrim( Ddo_contratada_codigo_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_CONTRATADA_CODIGO_Filteredtextto_get", StringUtil.RTrim( Ddo_contratada_codigo_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_CODIGO_Activeeventkey", StringUtil.RTrim( Ddo_sistema_codigo_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_CODIGO_Filteredtext_get", StringUtil.RTrim( Ddo_sistema_codigo_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_SISTEMA_CODIGO_Filteredtextto_get", StringUtil.RTrim( Ddo_sistema_codigo_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_CODIGO_Activeeventkey", StringUtil.RTrim( Ddo_servico_codigo_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_CODIGO_Filteredtext_get", StringUtil.RTrim( Ddo_servico_codigo_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_SERVICO_CODIGO_Filteredtextto_get", StringUtil.RTrim( Ddo_servico_codigo_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_NOVO_PROJETO_Activeeventkey", StringUtil.RTrim( Ddo_solicitacoes_novo_projeto_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_NOVO_PROJETO_Selectedvalue_get", StringUtil.RTrim( Ddo_solicitacoes_novo_projeto_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_OBJETIVO_Activeeventkey", StringUtil.RTrim( Ddo_solicitacoes_objetivo_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_OBJETIVO_Filteredtext_get", StringUtil.RTrim( Ddo_solicitacoes_objetivo_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_OBJETIVO_Selectedvalue_get", StringUtil.RTrim( Ddo_solicitacoes_objetivo_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_USUARIO_Activeeventkey", StringUtil.RTrim( Ddo_solicitacoes_usuario_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_USUARIO_Filteredtext_get", StringUtil.RTrim( Ddo_solicitacoes_usuario_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_USUARIO_Filteredtextto_get", StringUtil.RTrim( Ddo_solicitacoes_usuario_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_DATA_Activeeventkey", StringUtil.RTrim( Ddo_solicitacoes_data_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_DATA_Filteredtext_get", StringUtil.RTrim( Ddo_solicitacoes_data_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_DATA_Filteredtextto_get", StringUtil.RTrim( Ddo_solicitacoes_data_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_USUARIO_ULT_Activeeventkey", StringUtil.RTrim( Ddo_solicitacoes_usuario_ult_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_USUARIO_ULT_Filteredtext_get", StringUtil.RTrim( Ddo_solicitacoes_usuario_ult_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_USUARIO_ULT_Filteredtextto_get", StringUtil.RTrim( Ddo_solicitacoes_usuario_ult_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_DATA_ULT_Activeeventkey", StringUtil.RTrim( Ddo_solicitacoes_data_ult_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_DATA_ULT_Filteredtext_get", StringUtil.RTrim( Ddo_solicitacoes_data_ult_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_DATA_ULT_Filteredtextto_get", StringUtil.RTrim( Ddo_solicitacoes_data_ult_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_STATUS_Activeeventkey", StringUtil.RTrim( Ddo_solicitacoes_status_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_SOLICITACOES_STATUS_Selectedvalue_get", StringUtil.RTrim( Ddo_solicitacoes_status_Selectedvalue_get));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseFormAW2( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
         context.WriteHtmlTextNl( "</body>") ;
         context.WriteHtmlTextNl( "</html>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
      }

      public override String GetPgmname( )
      {
         return "PromptSolicitacoes" ;
      }

      public override String GetPgmdesc( )
      {
         return "Selecione Solicitacoes" ;
      }

      protected void WBAW0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            RenderHtmlHeaders( ) ;
            RenderHtmlOpenForm( ) ;
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, "", "", "", "false");
            wb_table1_2_AW2( true) ;
         }
         else
         {
            wb_table1_2_AW2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_AW2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 97,'',false,'" + sGXsfl_80_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled2_Internalname, StringUtil.BoolToStr( AV18DynamicFiltersEnabled2), "", "", chkavDynamicfiltersenabled2.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(97, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,97);\"");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 98,'',false,'" + sGXsfl_80_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled3_Internalname, StringUtil.BoolToStr( AV22DynamicFiltersEnabled3), "", "", chkavDynamicfiltersenabled3.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(98, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,98);\"");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 99,'',false,'" + sGXsfl_80_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfsolicitacoes_codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV31TFSolicitacoes_Codigo), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV31TFSolicitacoes_Codigo), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,99);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfsolicitacoes_codigo_Jsonclick, 0, "Attribute", "", "", "", edtavTfsolicitacoes_codigo_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptSolicitacoes.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 100,'',false,'" + sGXsfl_80_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfsolicitacoes_codigo_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV32TFSolicitacoes_Codigo_To), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV32TFSolicitacoes_Codigo_To), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,100);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfsolicitacoes_codigo_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfsolicitacoes_codigo_to_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptSolicitacoes.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 101,'',false,'" + sGXsfl_80_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratada_codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV35TFContratada_Codigo), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV35TFContratada_Codigo), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,101);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratada_codigo_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratada_codigo_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptSolicitacoes.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 102,'',false,'" + sGXsfl_80_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratada_codigo_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV36TFContratada_Codigo_To), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV36TFContratada_Codigo_To), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,102);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratada_codigo_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratada_codigo_to_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptSolicitacoes.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 103,'',false,'" + sGXsfl_80_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfsistema_codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV39TFSistema_Codigo), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV39TFSistema_Codigo), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,103);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfsistema_codigo_Jsonclick, 0, "Attribute", "", "", "", edtavTfsistema_codigo_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptSolicitacoes.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 104,'',false,'" + sGXsfl_80_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfsistema_codigo_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV40TFSistema_Codigo_To), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV40TFSistema_Codigo_To), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,104);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfsistema_codigo_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfsistema_codigo_to_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptSolicitacoes.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 105,'',false,'" + sGXsfl_80_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfservico_codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV43TFServico_Codigo), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV43TFServico_Codigo), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,105);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfservico_codigo_Jsonclick, 0, "Attribute", "", "", "", edtavTfservico_codigo_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptSolicitacoes.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 106,'',false,'" + sGXsfl_80_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfservico_codigo_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV44TFServico_Codigo_To), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV44TFServico_Codigo_To), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,106);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfservico_codigo_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfservico_codigo_to_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptSolicitacoes.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 107,'',false,'" + sGXsfl_80_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfsolicitacoes_novo_projeto_sel_Internalname, StringUtil.RTrim( AV47TFSolicitacoes_Novo_Projeto_Sel), StringUtil.RTrim( context.localUtil.Format( AV47TFSolicitacoes_Novo_Projeto_Sel, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,107);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfsolicitacoes_novo_projeto_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfsolicitacoes_novo_projeto_sel_Visible, 1, 0, "text", "", 1, "chr", 1, "row", 1, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptSolicitacoes.htm");
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 108,'',false,'" + sGXsfl_80_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavTfsolicitacoes_objetivo_Internalname, AV50TFSolicitacoes_Objetivo, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,108);\"", 0, edtavTfsolicitacoes_objetivo_Visible, 1, 0, 80, "chr", 3, "row", StyleString, ClassString, "", "200", -1, "", "", -1, true, "", "HLP_PromptSolicitacoes.htm");
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 109,'',false,'" + sGXsfl_80_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavTfsolicitacoes_objetivo_sel_Internalname, AV51TFSolicitacoes_Objetivo_Sel, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,109);\"", 0, edtavTfsolicitacoes_objetivo_sel_Visible, 1, 0, 80, "chr", 3, "row", StyleString, ClassString, "", "200", -1, "", "", -1, true, "", "HLP_PromptSolicitacoes.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 110,'',false,'" + sGXsfl_80_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfsolicitacoes_usuario_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV54TFSolicitacoes_Usuario), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV54TFSolicitacoes_Usuario), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,110);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfsolicitacoes_usuario_Jsonclick, 0, "Attribute", "", "", "", edtavTfsolicitacoes_usuario_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptSolicitacoes.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 111,'',false,'" + sGXsfl_80_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfsolicitacoes_usuario_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV55TFSolicitacoes_Usuario_To), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV55TFSolicitacoes_Usuario_To), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,111);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfsolicitacoes_usuario_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfsolicitacoes_usuario_to_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptSolicitacoes.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 112,'',false,'" + sGXsfl_80_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTfsolicitacoes_data_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTfsolicitacoes_data_Internalname, context.localUtil.TToC( AV58TFSolicitacoes_Data, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( AV58TFSolicitacoes_Data, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,112);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfsolicitacoes_data_Jsonclick, 0, "Attribute", "", "", "", edtavTfsolicitacoes_data_Visible, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptSolicitacoes.htm");
            GxWebStd.gx_bitmap( context, edtavTfsolicitacoes_data_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTfsolicitacoes_data_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptSolicitacoes.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 113,'',false,'" + sGXsfl_80_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTfsolicitacoes_data_to_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTfsolicitacoes_data_to_Internalname, context.localUtil.TToC( AV59TFSolicitacoes_Data_To, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( AV59TFSolicitacoes_Data_To, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,113);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfsolicitacoes_data_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfsolicitacoes_data_to_Visible, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptSolicitacoes.htm");
            GxWebStd.gx_bitmap( context, edtavTfsolicitacoes_data_to_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTfsolicitacoes_data_to_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptSolicitacoes.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divDdo_solicitacoes_dataauxdates_Internalname, 1, 0, "px", 0, "px", "Invisible", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 115,'',false,'" + sGXsfl_80_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_solicitacoes_dataauxdate_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_solicitacoes_dataauxdate_Internalname, context.localUtil.Format(AV60DDO_Solicitacoes_DataAuxDate, "99/99/99"), context.localUtil.Format( AV60DDO_Solicitacoes_DataAuxDate, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,115);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_solicitacoes_dataauxdate_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptSolicitacoes.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_solicitacoes_dataauxdate_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptSolicitacoes.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 116,'',false,'" + sGXsfl_80_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_solicitacoes_dataauxdateto_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_solicitacoes_dataauxdateto_Internalname, context.localUtil.Format(AV61DDO_Solicitacoes_DataAuxDateTo, "99/99/99"), context.localUtil.Format( AV61DDO_Solicitacoes_DataAuxDateTo, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,116);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_solicitacoes_dataauxdateto_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptSolicitacoes.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_solicitacoes_dataauxdateto_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptSolicitacoes.htm");
            context.WriteHtmlTextNl( "</div>") ;
            GxWebStd.gx_div_end( context, "left", "top");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 117,'',false,'" + sGXsfl_80_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfsolicitacoes_usuario_ult_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV64TFSolicitacoes_Usuario_Ult), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV64TFSolicitacoes_Usuario_Ult), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,117);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfsolicitacoes_usuario_ult_Jsonclick, 0, "Attribute", "", "", "", edtavTfsolicitacoes_usuario_ult_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptSolicitacoes.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 118,'',false,'" + sGXsfl_80_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfsolicitacoes_usuario_ult_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV65TFSolicitacoes_Usuario_Ult_To), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV65TFSolicitacoes_Usuario_Ult_To), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,118);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfsolicitacoes_usuario_ult_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfsolicitacoes_usuario_ult_to_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptSolicitacoes.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 119,'',false,'" + sGXsfl_80_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTfsolicitacoes_data_ult_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTfsolicitacoes_data_ult_Internalname, context.localUtil.TToC( AV68TFSolicitacoes_Data_Ult, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( AV68TFSolicitacoes_Data_Ult, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,119);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfsolicitacoes_data_ult_Jsonclick, 0, "Attribute", "", "", "", edtavTfsolicitacoes_data_ult_Visible, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptSolicitacoes.htm");
            GxWebStd.gx_bitmap( context, edtavTfsolicitacoes_data_ult_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTfsolicitacoes_data_ult_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptSolicitacoes.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 120,'',false,'" + sGXsfl_80_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTfsolicitacoes_data_ult_to_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTfsolicitacoes_data_ult_to_Internalname, context.localUtil.TToC( AV69TFSolicitacoes_Data_Ult_To, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( AV69TFSolicitacoes_Data_Ult_To, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,120);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfsolicitacoes_data_ult_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfsolicitacoes_data_ult_to_Visible, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptSolicitacoes.htm");
            GxWebStd.gx_bitmap( context, edtavTfsolicitacoes_data_ult_to_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTfsolicitacoes_data_ult_to_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptSolicitacoes.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divDdo_solicitacoes_data_ultauxdates_Internalname, 1, 0, "px", 0, "px", "Invisible", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 122,'',false,'" + sGXsfl_80_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_solicitacoes_data_ultauxdate_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_solicitacoes_data_ultauxdate_Internalname, context.localUtil.Format(AV70DDO_Solicitacoes_Data_UltAuxDate, "99/99/99"), context.localUtil.Format( AV70DDO_Solicitacoes_Data_UltAuxDate, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,122);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_solicitacoes_data_ultauxdate_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptSolicitacoes.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_solicitacoes_data_ultauxdate_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptSolicitacoes.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 123,'',false,'" + sGXsfl_80_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_solicitacoes_data_ultauxdateto_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_solicitacoes_data_ultauxdateto_Internalname, context.localUtil.Format(AV71DDO_Solicitacoes_Data_UltAuxDateTo, "99/99/99"), context.localUtil.Format( AV71DDO_Solicitacoes_Data_UltAuxDateTo, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,123);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_solicitacoes_data_ultauxdateto_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptSolicitacoes.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_solicitacoes_data_ultauxdateto_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptSolicitacoes.htm");
            context.WriteHtmlTextNl( "</div>") ;
            GxWebStd.gx_div_end( context, "left", "top");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_SOLICITACOES_CODIGOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 125,'',false,'" + sGXsfl_80_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_solicitacoes_codigotitlecontrolidtoreplace_Internalname, AV33ddo_Solicitacoes_CodigoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,125);\"", 0, edtavDdo_solicitacoes_codigotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptSolicitacoes.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_CONTRATADA_CODIGOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 127,'',false,'" + sGXsfl_80_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratada_codigotitlecontrolidtoreplace_Internalname, AV37ddo_Contratada_CodigoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,127);\"", 0, edtavDdo_contratada_codigotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptSolicitacoes.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_SISTEMA_CODIGOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 129,'',false,'" + sGXsfl_80_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_sistema_codigotitlecontrolidtoreplace_Internalname, AV41ddo_Sistema_CodigoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,129);\"", 0, edtavDdo_sistema_codigotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptSolicitacoes.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_SERVICO_CODIGOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 131,'',false,'" + sGXsfl_80_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_servico_codigotitlecontrolidtoreplace_Internalname, AV45ddo_Servico_CodigoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,131);\"", 0, edtavDdo_servico_codigotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptSolicitacoes.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_SOLICITACOES_NOVO_PROJETOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 133,'',false,'" + sGXsfl_80_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_solicitacoes_novo_projetotitlecontrolidtoreplace_Internalname, AV48ddo_Solicitacoes_Novo_ProjetoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,133);\"", 0, edtavDdo_solicitacoes_novo_projetotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptSolicitacoes.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_SOLICITACOES_OBJETIVOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 135,'',false,'" + sGXsfl_80_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_solicitacoes_objetivotitlecontrolidtoreplace_Internalname, AV52ddo_Solicitacoes_ObjetivoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,135);\"", 0, edtavDdo_solicitacoes_objetivotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptSolicitacoes.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_SOLICITACOES_USUARIOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 137,'',false,'" + sGXsfl_80_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_solicitacoes_usuariotitlecontrolidtoreplace_Internalname, AV56ddo_Solicitacoes_UsuarioTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,137);\"", 0, edtavDdo_solicitacoes_usuariotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptSolicitacoes.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_SOLICITACOES_DATAContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 139,'',false,'" + sGXsfl_80_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_solicitacoes_datatitlecontrolidtoreplace_Internalname, AV62ddo_Solicitacoes_DataTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,139);\"", 0, edtavDdo_solicitacoes_datatitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptSolicitacoes.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_SOLICITACOES_USUARIO_ULTContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 141,'',false,'" + sGXsfl_80_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_solicitacoes_usuario_ulttitlecontrolidtoreplace_Internalname, AV66ddo_Solicitacoes_Usuario_UltTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,141);\"", 0, edtavDdo_solicitacoes_usuario_ulttitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptSolicitacoes.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_SOLICITACOES_DATA_ULTContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 143,'',false,'" + sGXsfl_80_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_solicitacoes_data_ulttitlecontrolidtoreplace_Internalname, AV72ddo_Solicitacoes_Data_UltTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,143);\"", 0, edtavDdo_solicitacoes_data_ulttitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptSolicitacoes.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_SOLICITACOES_STATUSContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 145,'',false,'" + sGXsfl_80_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_solicitacoes_statustitlecontrolidtoreplace_Internalname, AV76ddo_Solicitacoes_StatusTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,145);\"", 0, edtavDdo_solicitacoes_statustitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptSolicitacoes.htm");
         }
         wbLoad = true;
      }

      protected void STARTAW2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Selecione Solicitacoes", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPAW0( ) ;
      }

      protected void WSAW2( )
      {
         STARTAW2( ) ;
         EVTAW2( ) ;
      }

      protected void EVTAW2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E11AW2 */
                           E11AW2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_SOLICITACOES_CODIGO.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E12AW2 */
                           E12AW2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATADA_CODIGO.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E13AW2 */
                           E13AW2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_SISTEMA_CODIGO.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E14AW2 */
                           E14AW2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_SERVICO_CODIGO.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E15AW2 */
                           E15AW2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_SOLICITACOES_NOVO_PROJETO.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E16AW2 */
                           E16AW2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_SOLICITACOES_OBJETIVO.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E17AW2 */
                           E17AW2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_SOLICITACOES_USUARIO.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E18AW2 */
                           E18AW2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_SOLICITACOES_DATA.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E19AW2 */
                           E19AW2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_SOLICITACOES_USUARIO_ULT.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E20AW2 */
                           E20AW2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_SOLICITACOES_DATA_ULT.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E21AW2 */
                           E21AW2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_SOLICITACOES_STATUS.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E22AW2 */
                           E22AW2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VORDEREDBY.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E23AW2 */
                           E23AW2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS1'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E24AW2 */
                           E24AW2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS2'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E25AW2 */
                           E25AW2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS3'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E26AW2 */
                           E26AW2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'DOCLEANFILTERS'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E27AW2 */
                           E27AW2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS1'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E28AW2 */
                           E28AW2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR1.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E29AW2 */
                           E29AW2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS2'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E30AW2 */
                           E30AW2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR2.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E31AW2 */
                           E31AW2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR3.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E32AW2 */
                           E32AW2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           dynload_actions( ) ;
                        }
                     }
                     else
                     {
                        sEvtType = StringUtil.Right( sEvt, 4);
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                        if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) )
                        {
                           nGXsfl_80_idx = (short)(NumberUtil.Val( sEvtType, "."));
                           sGXsfl_80_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_80_idx), 4, 0)), 4, "0");
                           SubsflControlProps_802( ) ;
                           AV28Select = cgiGet( edtavSelect_Internalname);
                           context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavSelect_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV28Select)) ? AV83Select_GXI : context.convertURL( context.PathToRelativeUrl( AV28Select))));
                           A439Solicitacoes_Codigo = (int)(context.localUtil.CToN( cgiGet( edtSolicitacoes_Codigo_Internalname), ",", "."));
                           A39Contratada_Codigo = (int)(context.localUtil.CToN( cgiGet( edtContratada_Codigo_Internalname), ",", "."));
                           A127Sistema_Codigo = (int)(context.localUtil.CToN( cgiGet( edtSistema_Codigo_Internalname), ",", "."));
                           A155Servico_Codigo = (int)(context.localUtil.CToN( cgiGet( edtServico_Codigo_Internalname), ",", "."));
                           A440Solicitacoes_Novo_Projeto = cgiGet( radSolicitacoes_Novo_Projeto_Internalname);
                           A441Solicitacoes_Objetivo = cgiGet( edtSolicitacoes_Objetivo_Internalname);
                           n441Solicitacoes_Objetivo = false;
                           A442Solicitacoes_Usuario = (int)(context.localUtil.CToN( cgiGet( edtSolicitacoes_Usuario_Internalname), ",", "."));
                           A444Solicitacoes_Data = context.localUtil.CToT( cgiGet( edtSolicitacoes_Data_Internalname), 0);
                           A443Solicitacoes_Usuario_Ult = (int)(context.localUtil.CToN( cgiGet( edtSolicitacoes_Usuario_Ult_Internalname), ",", "."));
                           A445Solicitacoes_Data_Ult = context.localUtil.CToT( cgiGet( edtSolicitacoes_Data_Ult_Internalname), 0);
                           cmbSolicitacoes_Status.Name = cmbSolicitacoes_Status_Internalname;
                           cmbSolicitacoes_Status.CurrentValue = cgiGet( cmbSolicitacoes_Status_Internalname);
                           A446Solicitacoes_Status = cgiGet( cmbSolicitacoes_Status_Internalname);
                           sEvtType = StringUtil.Right( sEvt, 1);
                           if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                           {
                              sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                              if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E33AW2 */
                                 E33AW2 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E34AW2 */
                                 E34AW2 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E35AW2 */
                                 E35AW2 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    Rfr0gs = false;
                                    /* Set Refresh If Orderedby Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Ordereddsc Changed */
                                    if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersselector1 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersoperator1 Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR1"), ",", ".") != Convert.ToDecimal( AV16DynamicFiltersOperator1 )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Solicitacoes_novo_projeto1 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vSOLICITACOES_NOVO_PROJETO1"), AV17Solicitacoes_Novo_Projeto1) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersselector2 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV19DynamicFiltersSelector2) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersoperator2 Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR2"), ",", ".") != Convert.ToDecimal( AV20DynamicFiltersOperator2 )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Solicitacoes_novo_projeto2 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vSOLICITACOES_NOVO_PROJETO2"), AV21Solicitacoes_Novo_Projeto2) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersselector3 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV23DynamicFiltersSelector3) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersoperator3 Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR3"), ",", ".") != Convert.ToDecimal( AV24DynamicFiltersOperator3 )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Solicitacoes_novo_projeto3 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vSOLICITACOES_NOVO_PROJETO3"), AV25Solicitacoes_Novo_Projeto3) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersenabled2 Changed */
                                    if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV18DynamicFiltersEnabled2 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersenabled3 Changed */
                                    if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV22DynamicFiltersEnabled3 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfsolicitacoes_codigo Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFSOLICITACOES_CODIGO"), ",", ".") != Convert.ToDecimal( AV31TFSolicitacoes_Codigo )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfsolicitacoes_codigo_to Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFSOLICITACOES_CODIGO_TO"), ",", ".") != Convert.ToDecimal( AV32TFSolicitacoes_Codigo_To )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontratada_codigo Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATADA_CODIGO"), ",", ".") != Convert.ToDecimal( AV35TFContratada_Codigo )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfcontratada_codigo_to Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATADA_CODIGO_TO"), ",", ".") != Convert.ToDecimal( AV36TFContratada_Codigo_To )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfsistema_codigo Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFSISTEMA_CODIGO"), ",", ".") != Convert.ToDecimal( AV39TFSistema_Codigo )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfsistema_codigo_to Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFSISTEMA_CODIGO_TO"), ",", ".") != Convert.ToDecimal( AV40TFSistema_Codigo_To )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfservico_codigo Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFSERVICO_CODIGO"), ",", ".") != Convert.ToDecimal( AV43TFServico_Codigo )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfservico_codigo_to Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFSERVICO_CODIGO_TO"), ",", ".") != Convert.ToDecimal( AV44TFServico_Codigo_To )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfsolicitacoes_novo_projeto_sel Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFSOLICITACOES_NOVO_PROJETO_SEL"), AV47TFSolicitacoes_Novo_Projeto_Sel) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfsolicitacoes_objetivo Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFSOLICITACOES_OBJETIVO"), AV50TFSolicitacoes_Objetivo) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfsolicitacoes_objetivo_sel Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFSOLICITACOES_OBJETIVO_SEL"), AV51TFSolicitacoes_Objetivo_Sel) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfsolicitacoes_usuario Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFSOLICITACOES_USUARIO"), ",", ".") != Convert.ToDecimal( AV54TFSolicitacoes_Usuario )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfsolicitacoes_usuario_to Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFSOLICITACOES_USUARIO_TO"), ",", ".") != Convert.ToDecimal( AV55TFSolicitacoes_Usuario_To )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfsolicitacoes_data Changed */
                                    if ( context.localUtil.CToT( cgiGet( "GXH_vTFSOLICITACOES_DATA"), 0) != AV58TFSolicitacoes_Data )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfsolicitacoes_data_to Changed */
                                    if ( context.localUtil.CToT( cgiGet( "GXH_vTFSOLICITACOES_DATA_TO"), 0) != AV59TFSolicitacoes_Data_To )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfsolicitacoes_usuario_ult Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFSOLICITACOES_USUARIO_ULT"), ",", ".") != Convert.ToDecimal( AV64TFSolicitacoes_Usuario_Ult )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfsolicitacoes_usuario_ult_to Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFSOLICITACOES_USUARIO_ULT_TO"), ",", ".") != Convert.ToDecimal( AV65TFSolicitacoes_Usuario_Ult_To )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfsolicitacoes_data_ult Changed */
                                    if ( context.localUtil.CToT( cgiGet( "GXH_vTFSOLICITACOES_DATA_ULT"), 0) != AV68TFSolicitacoes_Data_Ult )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfsolicitacoes_data_ult_to Changed */
                                    if ( context.localUtil.CToT( cgiGet( "GXH_vTFSOLICITACOES_DATA_ULT_TO"), 0) != AV69TFSolicitacoes_Data_Ult_To )
                                    {
                                       Rfr0gs = true;
                                    }
                                    if ( ! Rfr0gs )
                                    {
                                       /* Execute user event: E36AW2 */
                                       E36AW2 ();
                                    }
                                    dynload_actions( ) ;
                                 }
                              }
                              else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                              }
                           }
                           else
                           {
                           }
                        }
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void WEAW2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormAW2( ) ;
            }
         }
      }

      protected void PAAW2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbavOrderedby.Name = "vORDEREDBY";
            cmbavOrderedby.WebTags = "";
            if ( cmbavOrderedby.ItemCount > 0 )
            {
               AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            }
            cmbavDynamicfiltersselector1.Name = "vDYNAMICFILTERSSELECTOR1";
            cmbavDynamicfiltersselector1.WebTags = "";
            cmbavDynamicfiltersselector1.addItem("SOLICITACOES_NOVO_PROJETO", "Projeto", 0);
            if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
            {
               AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            }
            cmbavDynamicfiltersoperator1.Name = "vDYNAMICFILTERSOPERATOR1";
            cmbavDynamicfiltersoperator1.WebTags = "";
            cmbavDynamicfiltersoperator1.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
            {
               AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
            }
            radavSolicitacoes_novo_projeto1.Name = "vSOLICITACOES_NOVO_PROJETO1";
            radavSolicitacoes_novo_projeto1.WebTags = "";
            radavSolicitacoes_novo_projeto1.addItem("1", "Sim", 0);
            radavSolicitacoes_novo_projeto1.addItem("0", "N�o", 0);
            cmbavDynamicfiltersselector2.Name = "vDYNAMICFILTERSSELECTOR2";
            cmbavDynamicfiltersselector2.WebTags = "";
            cmbavDynamicfiltersselector2.addItem("SOLICITACOES_NOVO_PROJETO", "Projeto", 0);
            if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
            {
               AV19DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV19DynamicFiltersSelector2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
            }
            cmbavDynamicfiltersoperator2.Name = "vDYNAMICFILTERSOPERATOR2";
            cmbavDynamicfiltersoperator2.WebTags = "";
            cmbavDynamicfiltersoperator2.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator2.addItem("1", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator2.ItemCount > 0 )
            {
               AV20DynamicFiltersOperator2 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)));
            }
            radavSolicitacoes_novo_projeto2.Name = "vSOLICITACOES_NOVO_PROJETO2";
            radavSolicitacoes_novo_projeto2.WebTags = "";
            radavSolicitacoes_novo_projeto2.addItem("1", "Sim", 0);
            radavSolicitacoes_novo_projeto2.addItem("0", "N�o", 0);
            cmbavDynamicfiltersselector3.Name = "vDYNAMICFILTERSSELECTOR3";
            cmbavDynamicfiltersselector3.WebTags = "";
            cmbavDynamicfiltersselector3.addItem("SOLICITACOES_NOVO_PROJETO", "Projeto", 0);
            if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
            {
               AV23DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV23DynamicFiltersSelector3);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
            }
            cmbavDynamicfiltersoperator3.Name = "vDYNAMICFILTERSOPERATOR3";
            cmbavDynamicfiltersoperator3.WebTags = "";
            cmbavDynamicfiltersoperator3.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator3.addItem("1", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator3.ItemCount > 0 )
            {
               AV24DynamicFiltersOperator3 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)));
            }
            radavSolicitacoes_novo_projeto3.Name = "vSOLICITACOES_NOVO_PROJETO3";
            radavSolicitacoes_novo_projeto3.WebTags = "";
            radavSolicitacoes_novo_projeto3.addItem("1", "Sim", 0);
            radavSolicitacoes_novo_projeto3.addItem("0", "N�o", 0);
            GXCCtl = "SOLICITACOES_NOVO_PROJETO_" + sGXsfl_80_idx;
            radSolicitacoes_Novo_Projeto.Name = GXCCtl;
            radSolicitacoes_Novo_Projeto.WebTags = "";
            radSolicitacoes_Novo_Projeto.addItem("1", "Sim", 0);
            radSolicitacoes_Novo_Projeto.addItem("0", "N�o", 0);
            GXCCtl = "SOLICITACOES_STATUS_" + sGXsfl_80_idx;
            cmbSolicitacoes_Status.Name = GXCCtl;
            cmbSolicitacoes_Status.WebTags = "";
            cmbSolicitacoes_Status.addItem("A", "Ativo", 0);
            cmbSolicitacoes_Status.addItem("E", "Excluido", 0);
            cmbSolicitacoes_Status.addItem("R", "Rascunho", 0);
            if ( cmbSolicitacoes_Status.ItemCount > 0 )
            {
               A446Solicitacoes_Status = cmbSolicitacoes_Status.getValidValue(A446Solicitacoes_Status);
            }
            chkavDynamicfiltersenabled2.Name = "vDYNAMICFILTERSENABLED2";
            chkavDynamicfiltersenabled2.WebTags = "";
            chkavDynamicfiltersenabled2.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "TitleCaption", chkavDynamicfiltersenabled2.Caption);
            chkavDynamicfiltersenabled2.CheckedValue = "false";
            chkavDynamicfiltersenabled3.Name = "vDYNAMICFILTERSENABLED3";
            chkavDynamicfiltersenabled3.WebTags = "";
            chkavDynamicfiltersenabled3.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "TitleCaption", chkavDynamicfiltersenabled3.Caption);
            chkavDynamicfiltersenabled3.CheckedValue = "false";
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_802( ) ;
         while ( nGXsfl_80_idx <= nRC_GXsfl_80 )
         {
            sendrow_802( ) ;
            nGXsfl_80_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_80_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_80_idx+1));
            sGXsfl_80_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_80_idx), 4, 0)), 4, "0");
            SubsflControlProps_802( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV13OrderedBy ,
                                       bool AV14OrderedDsc ,
                                       String AV15DynamicFiltersSelector1 ,
                                       short AV16DynamicFiltersOperator1 ,
                                       String AV17Solicitacoes_Novo_Projeto1 ,
                                       String AV19DynamicFiltersSelector2 ,
                                       short AV20DynamicFiltersOperator2 ,
                                       String AV21Solicitacoes_Novo_Projeto2 ,
                                       String AV23DynamicFiltersSelector3 ,
                                       short AV24DynamicFiltersOperator3 ,
                                       String AV25Solicitacoes_Novo_Projeto3 ,
                                       bool AV18DynamicFiltersEnabled2 ,
                                       bool AV22DynamicFiltersEnabled3 ,
                                       int AV31TFSolicitacoes_Codigo ,
                                       int AV32TFSolicitacoes_Codigo_To ,
                                       int AV35TFContratada_Codigo ,
                                       int AV36TFContratada_Codigo_To ,
                                       int AV39TFSistema_Codigo ,
                                       int AV40TFSistema_Codigo_To ,
                                       int AV43TFServico_Codigo ,
                                       int AV44TFServico_Codigo_To ,
                                       String AV47TFSolicitacoes_Novo_Projeto_Sel ,
                                       String AV50TFSolicitacoes_Objetivo ,
                                       String AV51TFSolicitacoes_Objetivo_Sel ,
                                       int AV54TFSolicitacoes_Usuario ,
                                       int AV55TFSolicitacoes_Usuario_To ,
                                       DateTime AV58TFSolicitacoes_Data ,
                                       DateTime AV59TFSolicitacoes_Data_To ,
                                       int AV64TFSolicitacoes_Usuario_Ult ,
                                       int AV65TFSolicitacoes_Usuario_Ult_To ,
                                       DateTime AV68TFSolicitacoes_Data_Ult ,
                                       DateTime AV69TFSolicitacoes_Data_Ult_To ,
                                       String AV33ddo_Solicitacoes_CodigoTitleControlIdToReplace ,
                                       String AV37ddo_Contratada_CodigoTitleControlIdToReplace ,
                                       String AV41ddo_Sistema_CodigoTitleControlIdToReplace ,
                                       String AV45ddo_Servico_CodigoTitleControlIdToReplace ,
                                       String AV48ddo_Solicitacoes_Novo_ProjetoTitleControlIdToReplace ,
                                       String AV52ddo_Solicitacoes_ObjetivoTitleControlIdToReplace ,
                                       String AV56ddo_Solicitacoes_UsuarioTitleControlIdToReplace ,
                                       String AV62ddo_Solicitacoes_DataTitleControlIdToReplace ,
                                       String AV66ddo_Solicitacoes_Usuario_UltTitleControlIdToReplace ,
                                       String AV72ddo_Solicitacoes_Data_UltTitleControlIdToReplace ,
                                       String AV76ddo_Solicitacoes_StatusTitleControlIdToReplace ,
                                       IGxCollection AV75TFSolicitacoes_Status_Sels ,
                                       String AV84Pgmname ,
                                       wwpbaseobjects.SdtWWPGridState AV10GridState ,
                                       bool AV27DynamicFiltersIgnoreFirst ,
                                       bool AV26DynamicFiltersRemoving )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RFAW2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_SOLICITACOES_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A439Solicitacoes_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "SOLICITACOES_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A439Solicitacoes_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_CONTRATADA_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A39Contratada_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "CONTRATADA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A39Contratada_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_SISTEMA_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A127Sistema_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "SISTEMA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A127Sistema_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_SERVICO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A155Servico_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "SERVICO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A155Servico_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_SOLICITACOES_NOVO_PROJETO", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A440Solicitacoes_Novo_Projeto, ""))));
         GxWebStd.gx_hidden_field( context, "SOLICITACOES_NOVO_PROJETO", StringUtil.RTrim( A440Solicitacoes_Novo_Projeto));
         GxWebStd.gx_hidden_field( context, "gxhash_SOLICITACOES_OBJETIVO", GetSecureSignedToken( "", A441Solicitacoes_Objetivo));
         GxWebStd.gx_hidden_field( context, "SOLICITACOES_OBJETIVO", A441Solicitacoes_Objetivo);
         GxWebStd.gx_hidden_field( context, "gxhash_SOLICITACOES_USUARIO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A442Solicitacoes_Usuario), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "SOLICITACOES_USUARIO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A442Solicitacoes_Usuario), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_SOLICITACOES_DATA", GetSecureSignedToken( "", context.localUtil.Format( A444Solicitacoes_Data, "99/99/99 99:99")));
         GxWebStd.gx_hidden_field( context, "SOLICITACOES_DATA", context.localUtil.TToC( A444Solicitacoes_Data, 10, 8, 0, 3, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "gxhash_SOLICITACOES_USUARIO_ULT", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A443Solicitacoes_Usuario_Ult), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "SOLICITACOES_USUARIO_ULT", StringUtil.LTrim( StringUtil.NToC( (decimal)(A443Solicitacoes_Usuario_Ult), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_SOLICITACOES_DATA_ULT", GetSecureSignedToken( "", context.localUtil.Format( A445Solicitacoes_Data_Ult, "99/99/99 99:99")));
         GxWebStd.gx_hidden_field( context, "SOLICITACOES_DATA_ULT", context.localUtil.TToC( A445Solicitacoes_Data_Ult, 10, 8, 0, 3, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "gxhash_SOLICITACOES_STATUS", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A446Solicitacoes_Status, ""))));
         GxWebStd.gx_hidden_field( context, "SOLICITACOES_STATUS", StringUtil.RTrim( A446Solicitacoes_Status));
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavOrderedby.ItemCount > 0 )
         {
            AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
         {
            AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         }
         if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
         {
            AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
         {
            AV19DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV19DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         }
         if ( cmbavDynamicfiltersoperator2.ItemCount > 0 )
         {
            AV20DynamicFiltersOperator2 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
         {
            AV23DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV23DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
         }
         if ( cmbavDynamicfiltersoperator3.ItemCount > 0 )
         {
            AV24DynamicFiltersOperator3 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFAW2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV84Pgmname = "PromptSolicitacoes";
         context.Gx_err = 0;
      }

      protected void RFAW2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 80;
         /* Execute user event: E34AW2 */
         E34AW2 ();
         nGXsfl_80_idx = 1;
         sGXsfl_80_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_80_idx), 4, 0)), 4, "0");
         SubsflControlProps_802( ) ;
         nGXsfl_80_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", "");
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_802( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 A446Solicitacoes_Status ,
                                                 AV75TFSolicitacoes_Status_Sels ,
                                                 AV15DynamicFiltersSelector1 ,
                                                 AV16DynamicFiltersOperator1 ,
                                                 AV17Solicitacoes_Novo_Projeto1 ,
                                                 AV18DynamicFiltersEnabled2 ,
                                                 AV19DynamicFiltersSelector2 ,
                                                 AV20DynamicFiltersOperator2 ,
                                                 AV21Solicitacoes_Novo_Projeto2 ,
                                                 AV22DynamicFiltersEnabled3 ,
                                                 AV23DynamicFiltersSelector3 ,
                                                 AV24DynamicFiltersOperator3 ,
                                                 AV25Solicitacoes_Novo_Projeto3 ,
                                                 AV31TFSolicitacoes_Codigo ,
                                                 AV32TFSolicitacoes_Codigo_To ,
                                                 AV35TFContratada_Codigo ,
                                                 AV36TFContratada_Codigo_To ,
                                                 AV39TFSistema_Codigo ,
                                                 AV40TFSistema_Codigo_To ,
                                                 AV43TFServico_Codigo ,
                                                 AV44TFServico_Codigo_To ,
                                                 AV47TFSolicitacoes_Novo_Projeto_Sel ,
                                                 AV51TFSolicitacoes_Objetivo_Sel ,
                                                 AV50TFSolicitacoes_Objetivo ,
                                                 AV54TFSolicitacoes_Usuario ,
                                                 AV55TFSolicitacoes_Usuario_To ,
                                                 AV58TFSolicitacoes_Data ,
                                                 AV59TFSolicitacoes_Data_To ,
                                                 AV64TFSolicitacoes_Usuario_Ult ,
                                                 AV65TFSolicitacoes_Usuario_Ult_To ,
                                                 AV68TFSolicitacoes_Data_Ult ,
                                                 AV69TFSolicitacoes_Data_Ult_To ,
                                                 AV75TFSolicitacoes_Status_Sels.Count ,
                                                 A440Solicitacoes_Novo_Projeto ,
                                                 A439Solicitacoes_Codigo ,
                                                 A39Contratada_Codigo ,
                                                 A127Sistema_Codigo ,
                                                 A155Servico_Codigo ,
                                                 A441Solicitacoes_Objetivo ,
                                                 A442Solicitacoes_Usuario ,
                                                 A444Solicitacoes_Data ,
                                                 A443Solicitacoes_Usuario_Ult ,
                                                 A445Solicitacoes_Data_Ult ,
                                                 AV13OrderedBy ,
                                                 AV14OrderedDsc },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING,
                                                 TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT,
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.INT, TypeConstants.INT, TypeConstants.DATE,
                                                 TypeConstants.DATE, TypeConstants.INT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT,
                                                 TypeConstants.DATE, TypeConstants.INT, TypeConstants.DATE, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                                 }
            });
            lV17Solicitacoes_Novo_Projeto1 = StringUtil.PadR( StringUtil.RTrim( AV17Solicitacoes_Novo_Projeto1), 1, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Solicitacoes_Novo_Projeto1", AV17Solicitacoes_Novo_Projeto1);
            lV17Solicitacoes_Novo_Projeto1 = StringUtil.PadR( StringUtil.RTrim( AV17Solicitacoes_Novo_Projeto1), 1, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Solicitacoes_Novo_Projeto1", AV17Solicitacoes_Novo_Projeto1);
            lV21Solicitacoes_Novo_Projeto2 = StringUtil.PadR( StringUtil.RTrim( AV21Solicitacoes_Novo_Projeto2), 1, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Solicitacoes_Novo_Projeto2", AV21Solicitacoes_Novo_Projeto2);
            lV21Solicitacoes_Novo_Projeto2 = StringUtil.PadR( StringUtil.RTrim( AV21Solicitacoes_Novo_Projeto2), 1, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Solicitacoes_Novo_Projeto2", AV21Solicitacoes_Novo_Projeto2);
            lV25Solicitacoes_Novo_Projeto3 = StringUtil.PadR( StringUtil.RTrim( AV25Solicitacoes_Novo_Projeto3), 1, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25Solicitacoes_Novo_Projeto3", AV25Solicitacoes_Novo_Projeto3);
            lV25Solicitacoes_Novo_Projeto3 = StringUtil.PadR( StringUtil.RTrim( AV25Solicitacoes_Novo_Projeto3), 1, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25Solicitacoes_Novo_Projeto3", AV25Solicitacoes_Novo_Projeto3);
            lV50TFSolicitacoes_Objetivo = StringUtil.Concat( StringUtil.RTrim( AV50TFSolicitacoes_Objetivo), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50TFSolicitacoes_Objetivo", AV50TFSolicitacoes_Objetivo);
            /* Using cursor H00AW2 */
            pr_default.execute(0, new Object[] {lV17Solicitacoes_Novo_Projeto1, lV17Solicitacoes_Novo_Projeto1, lV21Solicitacoes_Novo_Projeto2, lV21Solicitacoes_Novo_Projeto2, lV25Solicitacoes_Novo_Projeto3, lV25Solicitacoes_Novo_Projeto3, AV31TFSolicitacoes_Codigo, AV32TFSolicitacoes_Codigo_To, AV35TFContratada_Codigo, AV36TFContratada_Codigo_To, AV39TFSistema_Codigo, AV40TFSistema_Codigo_To, AV43TFServico_Codigo, AV44TFServico_Codigo_To, AV47TFSolicitacoes_Novo_Projeto_Sel, lV50TFSolicitacoes_Objetivo, AV51TFSolicitacoes_Objetivo_Sel, AV54TFSolicitacoes_Usuario, AV55TFSolicitacoes_Usuario_To, AV58TFSolicitacoes_Data, AV59TFSolicitacoes_Data_To, AV64TFSolicitacoes_Usuario_Ult, AV65TFSolicitacoes_Usuario_Ult_To, AV68TFSolicitacoes_Data_Ult, AV69TFSolicitacoes_Data_Ult_To, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_80_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A446Solicitacoes_Status = H00AW2_A446Solicitacoes_Status[0];
               A445Solicitacoes_Data_Ult = H00AW2_A445Solicitacoes_Data_Ult[0];
               A443Solicitacoes_Usuario_Ult = H00AW2_A443Solicitacoes_Usuario_Ult[0];
               A444Solicitacoes_Data = H00AW2_A444Solicitacoes_Data[0];
               A442Solicitacoes_Usuario = H00AW2_A442Solicitacoes_Usuario[0];
               A441Solicitacoes_Objetivo = H00AW2_A441Solicitacoes_Objetivo[0];
               n441Solicitacoes_Objetivo = H00AW2_n441Solicitacoes_Objetivo[0];
               A440Solicitacoes_Novo_Projeto = H00AW2_A440Solicitacoes_Novo_Projeto[0];
               A155Servico_Codigo = H00AW2_A155Servico_Codigo[0];
               A127Sistema_Codigo = H00AW2_A127Sistema_Codigo[0];
               A39Contratada_Codigo = H00AW2_A39Contratada_Codigo[0];
               A439Solicitacoes_Codigo = H00AW2_A439Solicitacoes_Codigo[0];
               /* Execute user event: E35AW2 */
               E35AW2 ();
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 80;
            WBAW0( ) ;
         }
         nGXsfl_80_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              A446Solicitacoes_Status ,
                                              AV75TFSolicitacoes_Status_Sels ,
                                              AV15DynamicFiltersSelector1 ,
                                              AV16DynamicFiltersOperator1 ,
                                              AV17Solicitacoes_Novo_Projeto1 ,
                                              AV18DynamicFiltersEnabled2 ,
                                              AV19DynamicFiltersSelector2 ,
                                              AV20DynamicFiltersOperator2 ,
                                              AV21Solicitacoes_Novo_Projeto2 ,
                                              AV22DynamicFiltersEnabled3 ,
                                              AV23DynamicFiltersSelector3 ,
                                              AV24DynamicFiltersOperator3 ,
                                              AV25Solicitacoes_Novo_Projeto3 ,
                                              AV31TFSolicitacoes_Codigo ,
                                              AV32TFSolicitacoes_Codigo_To ,
                                              AV35TFContratada_Codigo ,
                                              AV36TFContratada_Codigo_To ,
                                              AV39TFSistema_Codigo ,
                                              AV40TFSistema_Codigo_To ,
                                              AV43TFServico_Codigo ,
                                              AV44TFServico_Codigo_To ,
                                              AV47TFSolicitacoes_Novo_Projeto_Sel ,
                                              AV51TFSolicitacoes_Objetivo_Sel ,
                                              AV50TFSolicitacoes_Objetivo ,
                                              AV54TFSolicitacoes_Usuario ,
                                              AV55TFSolicitacoes_Usuario_To ,
                                              AV58TFSolicitacoes_Data ,
                                              AV59TFSolicitacoes_Data_To ,
                                              AV64TFSolicitacoes_Usuario_Ult ,
                                              AV65TFSolicitacoes_Usuario_Ult_To ,
                                              AV68TFSolicitacoes_Data_Ult ,
                                              AV69TFSolicitacoes_Data_Ult_To ,
                                              AV75TFSolicitacoes_Status_Sels.Count ,
                                              A440Solicitacoes_Novo_Projeto ,
                                              A439Solicitacoes_Codigo ,
                                              A39Contratada_Codigo ,
                                              A127Sistema_Codigo ,
                                              A155Servico_Codigo ,
                                              A441Solicitacoes_Objetivo ,
                                              A442Solicitacoes_Usuario ,
                                              A444Solicitacoes_Data ,
                                              A443Solicitacoes_Usuario_Ult ,
                                              A445Solicitacoes_Data_Ult ,
                                              AV13OrderedBy ,
                                              AV14OrderedDsc },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING,
                                              TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.INT, TypeConstants.INT, TypeConstants.DATE,
                                              TypeConstants.DATE, TypeConstants.INT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT,
                                              TypeConstants.DATE, TypeConstants.INT, TypeConstants.DATE, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                              }
         });
         lV17Solicitacoes_Novo_Projeto1 = StringUtil.PadR( StringUtil.RTrim( AV17Solicitacoes_Novo_Projeto1), 1, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Solicitacoes_Novo_Projeto1", AV17Solicitacoes_Novo_Projeto1);
         lV17Solicitacoes_Novo_Projeto1 = StringUtil.PadR( StringUtil.RTrim( AV17Solicitacoes_Novo_Projeto1), 1, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Solicitacoes_Novo_Projeto1", AV17Solicitacoes_Novo_Projeto1);
         lV21Solicitacoes_Novo_Projeto2 = StringUtil.PadR( StringUtil.RTrim( AV21Solicitacoes_Novo_Projeto2), 1, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Solicitacoes_Novo_Projeto2", AV21Solicitacoes_Novo_Projeto2);
         lV21Solicitacoes_Novo_Projeto2 = StringUtil.PadR( StringUtil.RTrim( AV21Solicitacoes_Novo_Projeto2), 1, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Solicitacoes_Novo_Projeto2", AV21Solicitacoes_Novo_Projeto2);
         lV25Solicitacoes_Novo_Projeto3 = StringUtil.PadR( StringUtil.RTrim( AV25Solicitacoes_Novo_Projeto3), 1, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25Solicitacoes_Novo_Projeto3", AV25Solicitacoes_Novo_Projeto3);
         lV25Solicitacoes_Novo_Projeto3 = StringUtil.PadR( StringUtil.RTrim( AV25Solicitacoes_Novo_Projeto3), 1, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25Solicitacoes_Novo_Projeto3", AV25Solicitacoes_Novo_Projeto3);
         lV50TFSolicitacoes_Objetivo = StringUtil.Concat( StringUtil.RTrim( AV50TFSolicitacoes_Objetivo), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50TFSolicitacoes_Objetivo", AV50TFSolicitacoes_Objetivo);
         /* Using cursor H00AW3 */
         pr_default.execute(1, new Object[] {lV17Solicitacoes_Novo_Projeto1, lV17Solicitacoes_Novo_Projeto1, lV21Solicitacoes_Novo_Projeto2, lV21Solicitacoes_Novo_Projeto2, lV25Solicitacoes_Novo_Projeto3, lV25Solicitacoes_Novo_Projeto3, AV31TFSolicitacoes_Codigo, AV32TFSolicitacoes_Codigo_To, AV35TFContratada_Codigo, AV36TFContratada_Codigo_To, AV39TFSistema_Codigo, AV40TFSistema_Codigo_To, AV43TFServico_Codigo, AV44TFServico_Codigo_To, AV47TFSolicitacoes_Novo_Projeto_Sel, lV50TFSolicitacoes_Objetivo, AV51TFSolicitacoes_Objetivo_Sel, AV54TFSolicitacoes_Usuario, AV55TFSolicitacoes_Usuario_To, AV58TFSolicitacoes_Data, AV59TFSolicitacoes_Data_To, AV64TFSolicitacoes_Usuario_Ult, AV65TFSolicitacoes_Usuario_Ult_To, AV68TFSolicitacoes_Data_Ult, AV69TFSolicitacoes_Data_Ult_To});
         GRID_nRecordCount = H00AW3_AGRID_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17Solicitacoes_Novo_Projeto1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21Solicitacoes_Novo_Projeto2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25Solicitacoes_Novo_Projeto3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV31TFSolicitacoes_Codigo, AV32TFSolicitacoes_Codigo_To, AV35TFContratada_Codigo, AV36TFContratada_Codigo_To, AV39TFSistema_Codigo, AV40TFSistema_Codigo_To, AV43TFServico_Codigo, AV44TFServico_Codigo_To, AV47TFSolicitacoes_Novo_Projeto_Sel, AV50TFSolicitacoes_Objetivo, AV51TFSolicitacoes_Objetivo_Sel, AV54TFSolicitacoes_Usuario, AV55TFSolicitacoes_Usuario_To, AV58TFSolicitacoes_Data, AV59TFSolicitacoes_Data_To, AV64TFSolicitacoes_Usuario_Ult, AV65TFSolicitacoes_Usuario_Ult_To, AV68TFSolicitacoes_Data_Ult, AV69TFSolicitacoes_Data_Ult_To, AV33ddo_Solicitacoes_CodigoTitleControlIdToReplace, AV37ddo_Contratada_CodigoTitleControlIdToReplace, AV41ddo_Sistema_CodigoTitleControlIdToReplace, AV45ddo_Servico_CodigoTitleControlIdToReplace, AV48ddo_Solicitacoes_Novo_ProjetoTitleControlIdToReplace, AV52ddo_Solicitacoes_ObjetivoTitleControlIdToReplace, AV56ddo_Solicitacoes_UsuarioTitleControlIdToReplace, AV62ddo_Solicitacoes_DataTitleControlIdToReplace, AV66ddo_Solicitacoes_Usuario_UltTitleControlIdToReplace, AV72ddo_Solicitacoes_Data_UltTitleControlIdToReplace, AV76ddo_Solicitacoes_StatusTitleControlIdToReplace, AV75TFSolicitacoes_Status_Sels, AV84Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17Solicitacoes_Novo_Projeto1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21Solicitacoes_Novo_Projeto2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25Solicitacoes_Novo_Projeto3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV31TFSolicitacoes_Codigo, AV32TFSolicitacoes_Codigo_To, AV35TFContratada_Codigo, AV36TFContratada_Codigo_To, AV39TFSistema_Codigo, AV40TFSistema_Codigo_To, AV43TFServico_Codigo, AV44TFServico_Codigo_To, AV47TFSolicitacoes_Novo_Projeto_Sel, AV50TFSolicitacoes_Objetivo, AV51TFSolicitacoes_Objetivo_Sel, AV54TFSolicitacoes_Usuario, AV55TFSolicitacoes_Usuario_To, AV58TFSolicitacoes_Data, AV59TFSolicitacoes_Data_To, AV64TFSolicitacoes_Usuario_Ult, AV65TFSolicitacoes_Usuario_Ult_To, AV68TFSolicitacoes_Data_Ult, AV69TFSolicitacoes_Data_Ult_To, AV33ddo_Solicitacoes_CodigoTitleControlIdToReplace, AV37ddo_Contratada_CodigoTitleControlIdToReplace, AV41ddo_Sistema_CodigoTitleControlIdToReplace, AV45ddo_Servico_CodigoTitleControlIdToReplace, AV48ddo_Solicitacoes_Novo_ProjetoTitleControlIdToReplace, AV52ddo_Solicitacoes_ObjetivoTitleControlIdToReplace, AV56ddo_Solicitacoes_UsuarioTitleControlIdToReplace, AV62ddo_Solicitacoes_DataTitleControlIdToReplace, AV66ddo_Solicitacoes_Usuario_UltTitleControlIdToReplace, AV72ddo_Solicitacoes_Data_UltTitleControlIdToReplace, AV76ddo_Solicitacoes_StatusTitleControlIdToReplace, AV75TFSolicitacoes_Status_Sels, AV84Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17Solicitacoes_Novo_Projeto1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21Solicitacoes_Novo_Projeto2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25Solicitacoes_Novo_Projeto3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV31TFSolicitacoes_Codigo, AV32TFSolicitacoes_Codigo_To, AV35TFContratada_Codigo, AV36TFContratada_Codigo_To, AV39TFSistema_Codigo, AV40TFSistema_Codigo_To, AV43TFServico_Codigo, AV44TFServico_Codigo_To, AV47TFSolicitacoes_Novo_Projeto_Sel, AV50TFSolicitacoes_Objetivo, AV51TFSolicitacoes_Objetivo_Sel, AV54TFSolicitacoes_Usuario, AV55TFSolicitacoes_Usuario_To, AV58TFSolicitacoes_Data, AV59TFSolicitacoes_Data_To, AV64TFSolicitacoes_Usuario_Ult, AV65TFSolicitacoes_Usuario_Ult_To, AV68TFSolicitacoes_Data_Ult, AV69TFSolicitacoes_Data_Ult_To, AV33ddo_Solicitacoes_CodigoTitleControlIdToReplace, AV37ddo_Contratada_CodigoTitleControlIdToReplace, AV41ddo_Sistema_CodigoTitleControlIdToReplace, AV45ddo_Servico_CodigoTitleControlIdToReplace, AV48ddo_Solicitacoes_Novo_ProjetoTitleControlIdToReplace, AV52ddo_Solicitacoes_ObjetivoTitleControlIdToReplace, AV56ddo_Solicitacoes_UsuarioTitleControlIdToReplace, AV62ddo_Solicitacoes_DataTitleControlIdToReplace, AV66ddo_Solicitacoes_Usuario_UltTitleControlIdToReplace, AV72ddo_Solicitacoes_Data_UltTitleControlIdToReplace, AV76ddo_Solicitacoes_StatusTitleControlIdToReplace, AV75TFSolicitacoes_Status_Sels, AV84Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17Solicitacoes_Novo_Projeto1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21Solicitacoes_Novo_Projeto2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25Solicitacoes_Novo_Projeto3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV31TFSolicitacoes_Codigo, AV32TFSolicitacoes_Codigo_To, AV35TFContratada_Codigo, AV36TFContratada_Codigo_To, AV39TFSistema_Codigo, AV40TFSistema_Codigo_To, AV43TFServico_Codigo, AV44TFServico_Codigo_To, AV47TFSolicitacoes_Novo_Projeto_Sel, AV50TFSolicitacoes_Objetivo, AV51TFSolicitacoes_Objetivo_Sel, AV54TFSolicitacoes_Usuario, AV55TFSolicitacoes_Usuario_To, AV58TFSolicitacoes_Data, AV59TFSolicitacoes_Data_To, AV64TFSolicitacoes_Usuario_Ult, AV65TFSolicitacoes_Usuario_Ult_To, AV68TFSolicitacoes_Data_Ult, AV69TFSolicitacoes_Data_Ult_To, AV33ddo_Solicitacoes_CodigoTitleControlIdToReplace, AV37ddo_Contratada_CodigoTitleControlIdToReplace, AV41ddo_Sistema_CodigoTitleControlIdToReplace, AV45ddo_Servico_CodigoTitleControlIdToReplace, AV48ddo_Solicitacoes_Novo_ProjetoTitleControlIdToReplace, AV52ddo_Solicitacoes_ObjetivoTitleControlIdToReplace, AV56ddo_Solicitacoes_UsuarioTitleControlIdToReplace, AV62ddo_Solicitacoes_DataTitleControlIdToReplace, AV66ddo_Solicitacoes_Usuario_UltTitleControlIdToReplace, AV72ddo_Solicitacoes_Data_UltTitleControlIdToReplace, AV76ddo_Solicitacoes_StatusTitleControlIdToReplace, AV75TFSolicitacoes_Status_Sels, AV84Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17Solicitacoes_Novo_Projeto1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21Solicitacoes_Novo_Projeto2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25Solicitacoes_Novo_Projeto3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV31TFSolicitacoes_Codigo, AV32TFSolicitacoes_Codigo_To, AV35TFContratada_Codigo, AV36TFContratada_Codigo_To, AV39TFSistema_Codigo, AV40TFSistema_Codigo_To, AV43TFServico_Codigo, AV44TFServico_Codigo_To, AV47TFSolicitacoes_Novo_Projeto_Sel, AV50TFSolicitacoes_Objetivo, AV51TFSolicitacoes_Objetivo_Sel, AV54TFSolicitacoes_Usuario, AV55TFSolicitacoes_Usuario_To, AV58TFSolicitacoes_Data, AV59TFSolicitacoes_Data_To, AV64TFSolicitacoes_Usuario_Ult, AV65TFSolicitacoes_Usuario_Ult_To, AV68TFSolicitacoes_Data_Ult, AV69TFSolicitacoes_Data_Ult_To, AV33ddo_Solicitacoes_CodigoTitleControlIdToReplace, AV37ddo_Contratada_CodigoTitleControlIdToReplace, AV41ddo_Sistema_CodigoTitleControlIdToReplace, AV45ddo_Servico_CodigoTitleControlIdToReplace, AV48ddo_Solicitacoes_Novo_ProjetoTitleControlIdToReplace, AV52ddo_Solicitacoes_ObjetivoTitleControlIdToReplace, AV56ddo_Solicitacoes_UsuarioTitleControlIdToReplace, AV62ddo_Solicitacoes_DataTitleControlIdToReplace, AV66ddo_Solicitacoes_Usuario_UltTitleControlIdToReplace, AV72ddo_Solicitacoes_Data_UltTitleControlIdToReplace, AV76ddo_Solicitacoes_StatusTitleControlIdToReplace, AV75TFSolicitacoes_Status_Sels, AV84Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving) ;
         }
         return (int)(0) ;
      }

      protected void STRUPAW0( )
      {
         /* Before Start, stand alone formulas. */
         AV84Pgmname = "PromptSolicitacoes";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E33AW2 */
         E33AW2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "vDDO_TITLESETTINGSICONS"), AV77DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( "vSOLICITACOES_CODIGOTITLEFILTERDATA"), AV30Solicitacoes_CodigoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vCONTRATADA_CODIGOTITLEFILTERDATA"), AV34Contratada_CodigoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vSISTEMA_CODIGOTITLEFILTERDATA"), AV38Sistema_CodigoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vSERVICO_CODIGOTITLEFILTERDATA"), AV42Servico_CodigoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vSOLICITACOES_NOVO_PROJETOTITLEFILTERDATA"), AV46Solicitacoes_Novo_ProjetoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vSOLICITACOES_OBJETIVOTITLEFILTERDATA"), AV49Solicitacoes_ObjetivoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vSOLICITACOES_USUARIOTITLEFILTERDATA"), AV53Solicitacoes_UsuarioTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vSOLICITACOES_DATATITLEFILTERDATA"), AV57Solicitacoes_DataTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vSOLICITACOES_USUARIO_ULTTITLEFILTERDATA"), AV63Solicitacoes_Usuario_UltTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vSOLICITACOES_DATA_ULTTITLEFILTERDATA"), AV67Solicitacoes_Data_UltTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vSOLICITACOES_STATUSTITLEFILTERDATA"), AV73Solicitacoes_StatusTitleFilterData);
            /* Read variables values. */
            cmbavOrderedby.Name = cmbavOrderedby_Internalname;
            cmbavOrderedby.CurrentValue = cgiGet( cmbavOrderedby_Internalname);
            AV13OrderedBy = (short)(NumberUtil.Val( cgiGet( cmbavOrderedby_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            cmbavDynamicfiltersselector1.Name = cmbavDynamicfiltersselector1_Internalname;
            cmbavDynamicfiltersselector1.CurrentValue = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            AV15DynamicFiltersSelector1 = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            cmbavDynamicfiltersoperator1.Name = cmbavDynamicfiltersoperator1_Internalname;
            cmbavDynamicfiltersoperator1.CurrentValue = cgiGet( cmbavDynamicfiltersoperator1_Internalname);
            AV16DynamicFiltersOperator1 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator1_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
            AV17Solicitacoes_Novo_Projeto1 = cgiGet( radavSolicitacoes_novo_projeto1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Solicitacoes_Novo_Projeto1", AV17Solicitacoes_Novo_Projeto1);
            cmbavDynamicfiltersselector2.Name = cmbavDynamicfiltersselector2_Internalname;
            cmbavDynamicfiltersselector2.CurrentValue = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            AV19DynamicFiltersSelector2 = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
            cmbavDynamicfiltersoperator2.Name = cmbavDynamicfiltersoperator2_Internalname;
            cmbavDynamicfiltersoperator2.CurrentValue = cgiGet( cmbavDynamicfiltersoperator2_Internalname);
            AV20DynamicFiltersOperator2 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator2_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)));
            AV21Solicitacoes_Novo_Projeto2 = cgiGet( radavSolicitacoes_novo_projeto2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Solicitacoes_Novo_Projeto2", AV21Solicitacoes_Novo_Projeto2);
            cmbavDynamicfiltersselector3.Name = cmbavDynamicfiltersselector3_Internalname;
            cmbavDynamicfiltersselector3.CurrentValue = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            AV23DynamicFiltersSelector3 = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
            cmbavDynamicfiltersoperator3.Name = cmbavDynamicfiltersoperator3_Internalname;
            cmbavDynamicfiltersoperator3.CurrentValue = cgiGet( cmbavDynamicfiltersoperator3_Internalname);
            AV24DynamicFiltersOperator3 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator3_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)));
            AV25Solicitacoes_Novo_Projeto3 = cgiGet( radavSolicitacoes_novo_projeto3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25Solicitacoes_Novo_Projeto3", AV25Solicitacoes_Novo_Projeto3);
            AV18DynamicFiltersEnabled2 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
            AV22DynamicFiltersEnabled3 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfsolicitacoes_codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfsolicitacoes_codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFSOLICITACOES_CODIGO");
               GX_FocusControl = edtavTfsolicitacoes_codigo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV31TFSolicitacoes_Codigo = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31TFSolicitacoes_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV31TFSolicitacoes_Codigo), 6, 0)));
            }
            else
            {
               AV31TFSolicitacoes_Codigo = (int)(context.localUtil.CToN( cgiGet( edtavTfsolicitacoes_codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31TFSolicitacoes_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV31TFSolicitacoes_Codigo), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfsolicitacoes_codigo_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfsolicitacoes_codigo_to_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFSOLICITACOES_CODIGO_TO");
               GX_FocusControl = edtavTfsolicitacoes_codigo_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV32TFSolicitacoes_Codigo_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32TFSolicitacoes_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV32TFSolicitacoes_Codigo_To), 6, 0)));
            }
            else
            {
               AV32TFSolicitacoes_Codigo_To = (int)(context.localUtil.CToN( cgiGet( edtavTfsolicitacoes_codigo_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32TFSolicitacoes_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV32TFSolicitacoes_Codigo_To), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontratada_codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontratada_codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATADA_CODIGO");
               GX_FocusControl = edtavTfcontratada_codigo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV35TFContratada_Codigo = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TFContratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35TFContratada_Codigo), 6, 0)));
            }
            else
            {
               AV35TFContratada_Codigo = (int)(context.localUtil.CToN( cgiGet( edtavTfcontratada_codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TFContratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35TFContratada_Codigo), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontratada_codigo_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontratada_codigo_to_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATADA_CODIGO_TO");
               GX_FocusControl = edtavTfcontratada_codigo_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV36TFContratada_Codigo_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36TFContratada_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36TFContratada_Codigo_To), 6, 0)));
            }
            else
            {
               AV36TFContratada_Codigo_To = (int)(context.localUtil.CToN( cgiGet( edtavTfcontratada_codigo_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36TFContratada_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36TFContratada_Codigo_To), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfsistema_codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfsistema_codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFSISTEMA_CODIGO");
               GX_FocusControl = edtavTfsistema_codigo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV39TFSistema_Codigo = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFSistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV39TFSistema_Codigo), 6, 0)));
            }
            else
            {
               AV39TFSistema_Codigo = (int)(context.localUtil.CToN( cgiGet( edtavTfsistema_codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFSistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV39TFSistema_Codigo), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfsistema_codigo_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfsistema_codigo_to_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFSISTEMA_CODIGO_TO");
               GX_FocusControl = edtavTfsistema_codigo_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV40TFSistema_Codigo_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40TFSistema_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV40TFSistema_Codigo_To), 6, 0)));
            }
            else
            {
               AV40TFSistema_Codigo_To = (int)(context.localUtil.CToN( cgiGet( edtavTfsistema_codigo_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40TFSistema_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV40TFSistema_Codigo_To), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfservico_codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfservico_codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFSERVICO_CODIGO");
               GX_FocusControl = edtavTfservico_codigo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV43TFServico_Codigo = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43TFServico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV43TFServico_Codigo), 6, 0)));
            }
            else
            {
               AV43TFServico_Codigo = (int)(context.localUtil.CToN( cgiGet( edtavTfservico_codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43TFServico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV43TFServico_Codigo), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfservico_codigo_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfservico_codigo_to_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFSERVICO_CODIGO_TO");
               GX_FocusControl = edtavTfservico_codigo_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV44TFServico_Codigo_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44TFServico_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV44TFServico_Codigo_To), 6, 0)));
            }
            else
            {
               AV44TFServico_Codigo_To = (int)(context.localUtil.CToN( cgiGet( edtavTfservico_codigo_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44TFServico_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV44TFServico_Codigo_To), 6, 0)));
            }
            AV47TFSolicitacoes_Novo_Projeto_Sel = cgiGet( edtavTfsolicitacoes_novo_projeto_sel_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47TFSolicitacoes_Novo_Projeto_Sel", AV47TFSolicitacoes_Novo_Projeto_Sel);
            AV50TFSolicitacoes_Objetivo = cgiGet( edtavTfsolicitacoes_objetivo_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50TFSolicitacoes_Objetivo", AV50TFSolicitacoes_Objetivo);
            AV51TFSolicitacoes_Objetivo_Sel = cgiGet( edtavTfsolicitacoes_objetivo_sel_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51TFSolicitacoes_Objetivo_Sel", AV51TFSolicitacoes_Objetivo_Sel);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfsolicitacoes_usuario_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfsolicitacoes_usuario_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFSOLICITACOES_USUARIO");
               GX_FocusControl = edtavTfsolicitacoes_usuario_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV54TFSolicitacoes_Usuario = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54TFSolicitacoes_Usuario", StringUtil.LTrim( StringUtil.Str( (decimal)(AV54TFSolicitacoes_Usuario), 6, 0)));
            }
            else
            {
               AV54TFSolicitacoes_Usuario = (int)(context.localUtil.CToN( cgiGet( edtavTfsolicitacoes_usuario_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54TFSolicitacoes_Usuario", StringUtil.LTrim( StringUtil.Str( (decimal)(AV54TFSolicitacoes_Usuario), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfsolicitacoes_usuario_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfsolicitacoes_usuario_to_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFSOLICITACOES_USUARIO_TO");
               GX_FocusControl = edtavTfsolicitacoes_usuario_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV55TFSolicitacoes_Usuario_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55TFSolicitacoes_Usuario_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV55TFSolicitacoes_Usuario_To), 6, 0)));
            }
            else
            {
               AV55TFSolicitacoes_Usuario_To = (int)(context.localUtil.CToN( cgiGet( edtavTfsolicitacoes_usuario_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55TFSolicitacoes_Usuario_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV55TFSolicitacoes_Usuario_To), 6, 0)));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavTfsolicitacoes_data_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"TFSolicitacoes_Data"}), 1, "vTFSOLICITACOES_DATA");
               GX_FocusControl = edtavTfsolicitacoes_data_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV58TFSolicitacoes_Data = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58TFSolicitacoes_Data", context.localUtil.TToC( AV58TFSolicitacoes_Data, 8, 5, 0, 3, "/", ":", " "));
            }
            else
            {
               AV58TFSolicitacoes_Data = context.localUtil.CToT( cgiGet( edtavTfsolicitacoes_data_Internalname), 0);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58TFSolicitacoes_Data", context.localUtil.TToC( AV58TFSolicitacoes_Data, 8, 5, 0, 3, "/", ":", " "));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavTfsolicitacoes_data_to_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"TFSolicitacoes_Data_To"}), 1, "vTFSOLICITACOES_DATA_TO");
               GX_FocusControl = edtavTfsolicitacoes_data_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV59TFSolicitacoes_Data_To = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59TFSolicitacoes_Data_To", context.localUtil.TToC( AV59TFSolicitacoes_Data_To, 8, 5, 0, 3, "/", ":", " "));
            }
            else
            {
               AV59TFSolicitacoes_Data_To = context.localUtil.CToT( cgiGet( edtavTfsolicitacoes_data_to_Internalname), 0);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59TFSolicitacoes_Data_To", context.localUtil.TToC( AV59TFSolicitacoes_Data_To, 8, 5, 0, 3, "/", ":", " "));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_solicitacoes_dataauxdate_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Solicitacoes_Data Aux Date"}), 1, "vDDO_SOLICITACOES_DATAAUXDATE");
               GX_FocusControl = edtavDdo_solicitacoes_dataauxdate_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV60DDO_Solicitacoes_DataAuxDate = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60DDO_Solicitacoes_DataAuxDate", context.localUtil.Format(AV60DDO_Solicitacoes_DataAuxDate, "99/99/99"));
            }
            else
            {
               AV60DDO_Solicitacoes_DataAuxDate = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_solicitacoes_dataauxdate_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60DDO_Solicitacoes_DataAuxDate", context.localUtil.Format(AV60DDO_Solicitacoes_DataAuxDate, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_solicitacoes_dataauxdateto_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Solicitacoes_Data Aux Date To"}), 1, "vDDO_SOLICITACOES_DATAAUXDATETO");
               GX_FocusControl = edtavDdo_solicitacoes_dataauxdateto_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV61DDO_Solicitacoes_DataAuxDateTo = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61DDO_Solicitacoes_DataAuxDateTo", context.localUtil.Format(AV61DDO_Solicitacoes_DataAuxDateTo, "99/99/99"));
            }
            else
            {
               AV61DDO_Solicitacoes_DataAuxDateTo = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_solicitacoes_dataauxdateto_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61DDO_Solicitacoes_DataAuxDateTo", context.localUtil.Format(AV61DDO_Solicitacoes_DataAuxDateTo, "99/99/99"));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfsolicitacoes_usuario_ult_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfsolicitacoes_usuario_ult_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFSOLICITACOES_USUARIO_ULT");
               GX_FocusControl = edtavTfsolicitacoes_usuario_ult_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV64TFSolicitacoes_Usuario_Ult = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64TFSolicitacoes_Usuario_Ult", StringUtil.LTrim( StringUtil.Str( (decimal)(AV64TFSolicitacoes_Usuario_Ult), 6, 0)));
            }
            else
            {
               AV64TFSolicitacoes_Usuario_Ult = (int)(context.localUtil.CToN( cgiGet( edtavTfsolicitacoes_usuario_ult_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64TFSolicitacoes_Usuario_Ult", StringUtil.LTrim( StringUtil.Str( (decimal)(AV64TFSolicitacoes_Usuario_Ult), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfsolicitacoes_usuario_ult_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfsolicitacoes_usuario_ult_to_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFSOLICITACOES_USUARIO_ULT_TO");
               GX_FocusControl = edtavTfsolicitacoes_usuario_ult_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV65TFSolicitacoes_Usuario_Ult_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65TFSolicitacoes_Usuario_Ult_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV65TFSolicitacoes_Usuario_Ult_To), 6, 0)));
            }
            else
            {
               AV65TFSolicitacoes_Usuario_Ult_To = (int)(context.localUtil.CToN( cgiGet( edtavTfsolicitacoes_usuario_ult_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65TFSolicitacoes_Usuario_Ult_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV65TFSolicitacoes_Usuario_Ult_To), 6, 0)));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavTfsolicitacoes_data_ult_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"TFSolicitacoes_Data_Ult"}), 1, "vTFSOLICITACOES_DATA_ULT");
               GX_FocusControl = edtavTfsolicitacoes_data_ult_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV68TFSolicitacoes_Data_Ult = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV68TFSolicitacoes_Data_Ult", context.localUtil.TToC( AV68TFSolicitacoes_Data_Ult, 8, 5, 0, 3, "/", ":", " "));
            }
            else
            {
               AV68TFSolicitacoes_Data_Ult = context.localUtil.CToT( cgiGet( edtavTfsolicitacoes_data_ult_Internalname), 0);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV68TFSolicitacoes_Data_Ult", context.localUtil.TToC( AV68TFSolicitacoes_Data_Ult, 8, 5, 0, 3, "/", ":", " "));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavTfsolicitacoes_data_ult_to_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"TFSolicitacoes_Data_Ult_To"}), 1, "vTFSOLICITACOES_DATA_ULT_TO");
               GX_FocusControl = edtavTfsolicitacoes_data_ult_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV69TFSolicitacoes_Data_Ult_To = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69TFSolicitacoes_Data_Ult_To", context.localUtil.TToC( AV69TFSolicitacoes_Data_Ult_To, 8, 5, 0, 3, "/", ":", " "));
            }
            else
            {
               AV69TFSolicitacoes_Data_Ult_To = context.localUtil.CToT( cgiGet( edtavTfsolicitacoes_data_ult_to_Internalname), 0);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69TFSolicitacoes_Data_Ult_To", context.localUtil.TToC( AV69TFSolicitacoes_Data_Ult_To, 8, 5, 0, 3, "/", ":", " "));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_solicitacoes_data_ultauxdate_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Solicitacoes_Data_Ult Aux Date"}), 1, "vDDO_SOLICITACOES_DATA_ULTAUXDATE");
               GX_FocusControl = edtavDdo_solicitacoes_data_ultauxdate_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV70DDO_Solicitacoes_Data_UltAuxDate = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70DDO_Solicitacoes_Data_UltAuxDate", context.localUtil.Format(AV70DDO_Solicitacoes_Data_UltAuxDate, "99/99/99"));
            }
            else
            {
               AV70DDO_Solicitacoes_Data_UltAuxDate = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_solicitacoes_data_ultauxdate_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV70DDO_Solicitacoes_Data_UltAuxDate", context.localUtil.Format(AV70DDO_Solicitacoes_Data_UltAuxDate, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_solicitacoes_data_ultauxdateto_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Solicitacoes_Data_Ult Aux Date To"}), 1, "vDDO_SOLICITACOES_DATA_ULTAUXDATETO");
               GX_FocusControl = edtavDdo_solicitacoes_data_ultauxdateto_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV71DDO_Solicitacoes_Data_UltAuxDateTo = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71DDO_Solicitacoes_Data_UltAuxDateTo", context.localUtil.Format(AV71DDO_Solicitacoes_Data_UltAuxDateTo, "99/99/99"));
            }
            else
            {
               AV71DDO_Solicitacoes_Data_UltAuxDateTo = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_solicitacoes_data_ultauxdateto_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71DDO_Solicitacoes_Data_UltAuxDateTo", context.localUtil.Format(AV71DDO_Solicitacoes_Data_UltAuxDateTo, "99/99/99"));
            }
            AV33ddo_Solicitacoes_CodigoTitleControlIdToReplace = cgiGet( edtavDdo_solicitacoes_codigotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33ddo_Solicitacoes_CodigoTitleControlIdToReplace", AV33ddo_Solicitacoes_CodigoTitleControlIdToReplace);
            AV37ddo_Contratada_CodigoTitleControlIdToReplace = cgiGet( edtavDdo_contratada_codigotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37ddo_Contratada_CodigoTitleControlIdToReplace", AV37ddo_Contratada_CodigoTitleControlIdToReplace);
            AV41ddo_Sistema_CodigoTitleControlIdToReplace = cgiGet( edtavDdo_sistema_codigotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41ddo_Sistema_CodigoTitleControlIdToReplace", AV41ddo_Sistema_CodigoTitleControlIdToReplace);
            AV45ddo_Servico_CodigoTitleControlIdToReplace = cgiGet( edtavDdo_servico_codigotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45ddo_Servico_CodigoTitleControlIdToReplace", AV45ddo_Servico_CodigoTitleControlIdToReplace);
            AV48ddo_Solicitacoes_Novo_ProjetoTitleControlIdToReplace = cgiGet( edtavDdo_solicitacoes_novo_projetotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48ddo_Solicitacoes_Novo_ProjetoTitleControlIdToReplace", AV48ddo_Solicitacoes_Novo_ProjetoTitleControlIdToReplace);
            AV52ddo_Solicitacoes_ObjetivoTitleControlIdToReplace = cgiGet( edtavDdo_solicitacoes_objetivotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52ddo_Solicitacoes_ObjetivoTitleControlIdToReplace", AV52ddo_Solicitacoes_ObjetivoTitleControlIdToReplace);
            AV56ddo_Solicitacoes_UsuarioTitleControlIdToReplace = cgiGet( edtavDdo_solicitacoes_usuariotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56ddo_Solicitacoes_UsuarioTitleControlIdToReplace", AV56ddo_Solicitacoes_UsuarioTitleControlIdToReplace);
            AV62ddo_Solicitacoes_DataTitleControlIdToReplace = cgiGet( edtavDdo_solicitacoes_datatitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62ddo_Solicitacoes_DataTitleControlIdToReplace", AV62ddo_Solicitacoes_DataTitleControlIdToReplace);
            AV66ddo_Solicitacoes_Usuario_UltTitleControlIdToReplace = cgiGet( edtavDdo_solicitacoes_usuario_ulttitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66ddo_Solicitacoes_Usuario_UltTitleControlIdToReplace", AV66ddo_Solicitacoes_Usuario_UltTitleControlIdToReplace);
            AV72ddo_Solicitacoes_Data_UltTitleControlIdToReplace = cgiGet( edtavDdo_solicitacoes_data_ulttitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV72ddo_Solicitacoes_Data_UltTitleControlIdToReplace", AV72ddo_Solicitacoes_Data_UltTitleControlIdToReplace);
            AV76ddo_Solicitacoes_StatusTitleControlIdToReplace = cgiGet( edtavDdo_solicitacoes_statustitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV76ddo_Solicitacoes_StatusTitleControlIdToReplace", AV76ddo_Solicitacoes_StatusTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_80 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_80"), ",", "."));
            AV79GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( "vGRIDCURRENTPAGE"), ",", "."));
            AV80GridPageCount = (long)(context.localUtil.CToN( cgiGet( "vGRIDPAGECOUNT"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( "GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( "GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( "GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( "GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( "GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( "GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( "GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( "GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( "GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( "GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( "GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( "GRIDPAGINATIONBAR_Emptygridcaption");
            Ddo_solicitacoes_codigo_Caption = cgiGet( "DDO_SOLICITACOES_CODIGO_Caption");
            Ddo_solicitacoes_codigo_Tooltip = cgiGet( "DDO_SOLICITACOES_CODIGO_Tooltip");
            Ddo_solicitacoes_codigo_Cls = cgiGet( "DDO_SOLICITACOES_CODIGO_Cls");
            Ddo_solicitacoes_codigo_Filteredtext_set = cgiGet( "DDO_SOLICITACOES_CODIGO_Filteredtext_set");
            Ddo_solicitacoes_codigo_Filteredtextto_set = cgiGet( "DDO_SOLICITACOES_CODIGO_Filteredtextto_set");
            Ddo_solicitacoes_codigo_Dropdownoptionstype = cgiGet( "DDO_SOLICITACOES_CODIGO_Dropdownoptionstype");
            Ddo_solicitacoes_codigo_Titlecontrolidtoreplace = cgiGet( "DDO_SOLICITACOES_CODIGO_Titlecontrolidtoreplace");
            Ddo_solicitacoes_codigo_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_SOLICITACOES_CODIGO_Includesortasc"));
            Ddo_solicitacoes_codigo_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_SOLICITACOES_CODIGO_Includesortdsc"));
            Ddo_solicitacoes_codigo_Sortedstatus = cgiGet( "DDO_SOLICITACOES_CODIGO_Sortedstatus");
            Ddo_solicitacoes_codigo_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_SOLICITACOES_CODIGO_Includefilter"));
            Ddo_solicitacoes_codigo_Filtertype = cgiGet( "DDO_SOLICITACOES_CODIGO_Filtertype");
            Ddo_solicitacoes_codigo_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_SOLICITACOES_CODIGO_Filterisrange"));
            Ddo_solicitacoes_codigo_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_SOLICITACOES_CODIGO_Includedatalist"));
            Ddo_solicitacoes_codigo_Datalistfixedvalues = cgiGet( "DDO_SOLICITACOES_CODIGO_Datalistfixedvalues");
            Ddo_solicitacoes_codigo_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_SOLICITACOES_CODIGO_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_solicitacoes_codigo_Sortasc = cgiGet( "DDO_SOLICITACOES_CODIGO_Sortasc");
            Ddo_solicitacoes_codigo_Sortdsc = cgiGet( "DDO_SOLICITACOES_CODIGO_Sortdsc");
            Ddo_solicitacoes_codigo_Loadingdata = cgiGet( "DDO_SOLICITACOES_CODIGO_Loadingdata");
            Ddo_solicitacoes_codigo_Cleanfilter = cgiGet( "DDO_SOLICITACOES_CODIGO_Cleanfilter");
            Ddo_solicitacoes_codigo_Rangefilterfrom = cgiGet( "DDO_SOLICITACOES_CODIGO_Rangefilterfrom");
            Ddo_solicitacoes_codigo_Rangefilterto = cgiGet( "DDO_SOLICITACOES_CODIGO_Rangefilterto");
            Ddo_solicitacoes_codigo_Noresultsfound = cgiGet( "DDO_SOLICITACOES_CODIGO_Noresultsfound");
            Ddo_solicitacoes_codigo_Searchbuttontext = cgiGet( "DDO_SOLICITACOES_CODIGO_Searchbuttontext");
            Ddo_contratada_codigo_Caption = cgiGet( "DDO_CONTRATADA_CODIGO_Caption");
            Ddo_contratada_codigo_Tooltip = cgiGet( "DDO_CONTRATADA_CODIGO_Tooltip");
            Ddo_contratada_codigo_Cls = cgiGet( "DDO_CONTRATADA_CODIGO_Cls");
            Ddo_contratada_codigo_Filteredtext_set = cgiGet( "DDO_CONTRATADA_CODIGO_Filteredtext_set");
            Ddo_contratada_codigo_Filteredtextto_set = cgiGet( "DDO_CONTRATADA_CODIGO_Filteredtextto_set");
            Ddo_contratada_codigo_Dropdownoptionstype = cgiGet( "DDO_CONTRATADA_CODIGO_Dropdownoptionstype");
            Ddo_contratada_codigo_Titlecontrolidtoreplace = cgiGet( "DDO_CONTRATADA_CODIGO_Titlecontrolidtoreplace");
            Ddo_contratada_codigo_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATADA_CODIGO_Includesortasc"));
            Ddo_contratada_codigo_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_CONTRATADA_CODIGO_Includesortdsc"));
            Ddo_contratada_codigo_Sortedstatus = cgiGet( "DDO_CONTRATADA_CODIGO_Sortedstatus");
            Ddo_contratada_codigo_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_CONTRATADA_CODIGO_Includefilter"));
            Ddo_contratada_codigo_Filtertype = cgiGet( "DDO_CONTRATADA_CODIGO_Filtertype");
            Ddo_contratada_codigo_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_CONTRATADA_CODIGO_Filterisrange"));
            Ddo_contratada_codigo_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_CONTRATADA_CODIGO_Includedatalist"));
            Ddo_contratada_codigo_Datalistfixedvalues = cgiGet( "DDO_CONTRATADA_CODIGO_Datalistfixedvalues");
            Ddo_contratada_codigo_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_CONTRATADA_CODIGO_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_contratada_codigo_Sortasc = cgiGet( "DDO_CONTRATADA_CODIGO_Sortasc");
            Ddo_contratada_codigo_Sortdsc = cgiGet( "DDO_CONTRATADA_CODIGO_Sortdsc");
            Ddo_contratada_codigo_Loadingdata = cgiGet( "DDO_CONTRATADA_CODIGO_Loadingdata");
            Ddo_contratada_codigo_Cleanfilter = cgiGet( "DDO_CONTRATADA_CODIGO_Cleanfilter");
            Ddo_contratada_codigo_Rangefilterfrom = cgiGet( "DDO_CONTRATADA_CODIGO_Rangefilterfrom");
            Ddo_contratada_codigo_Rangefilterto = cgiGet( "DDO_CONTRATADA_CODIGO_Rangefilterto");
            Ddo_contratada_codigo_Noresultsfound = cgiGet( "DDO_CONTRATADA_CODIGO_Noresultsfound");
            Ddo_contratada_codigo_Searchbuttontext = cgiGet( "DDO_CONTRATADA_CODIGO_Searchbuttontext");
            Ddo_sistema_codigo_Caption = cgiGet( "DDO_SISTEMA_CODIGO_Caption");
            Ddo_sistema_codigo_Tooltip = cgiGet( "DDO_SISTEMA_CODIGO_Tooltip");
            Ddo_sistema_codigo_Cls = cgiGet( "DDO_SISTEMA_CODIGO_Cls");
            Ddo_sistema_codigo_Filteredtext_set = cgiGet( "DDO_SISTEMA_CODIGO_Filteredtext_set");
            Ddo_sistema_codigo_Filteredtextto_set = cgiGet( "DDO_SISTEMA_CODIGO_Filteredtextto_set");
            Ddo_sistema_codigo_Dropdownoptionstype = cgiGet( "DDO_SISTEMA_CODIGO_Dropdownoptionstype");
            Ddo_sistema_codigo_Titlecontrolidtoreplace = cgiGet( "DDO_SISTEMA_CODIGO_Titlecontrolidtoreplace");
            Ddo_sistema_codigo_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_SISTEMA_CODIGO_Includesortasc"));
            Ddo_sistema_codigo_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_SISTEMA_CODIGO_Includesortdsc"));
            Ddo_sistema_codigo_Sortedstatus = cgiGet( "DDO_SISTEMA_CODIGO_Sortedstatus");
            Ddo_sistema_codigo_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_SISTEMA_CODIGO_Includefilter"));
            Ddo_sistema_codigo_Filtertype = cgiGet( "DDO_SISTEMA_CODIGO_Filtertype");
            Ddo_sistema_codigo_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_SISTEMA_CODIGO_Filterisrange"));
            Ddo_sistema_codigo_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_SISTEMA_CODIGO_Includedatalist"));
            Ddo_sistema_codigo_Datalistfixedvalues = cgiGet( "DDO_SISTEMA_CODIGO_Datalistfixedvalues");
            Ddo_sistema_codigo_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_SISTEMA_CODIGO_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_sistema_codigo_Sortasc = cgiGet( "DDO_SISTEMA_CODIGO_Sortasc");
            Ddo_sistema_codigo_Sortdsc = cgiGet( "DDO_SISTEMA_CODIGO_Sortdsc");
            Ddo_sistema_codigo_Loadingdata = cgiGet( "DDO_SISTEMA_CODIGO_Loadingdata");
            Ddo_sistema_codigo_Cleanfilter = cgiGet( "DDO_SISTEMA_CODIGO_Cleanfilter");
            Ddo_sistema_codigo_Rangefilterfrom = cgiGet( "DDO_SISTEMA_CODIGO_Rangefilterfrom");
            Ddo_sistema_codigo_Rangefilterto = cgiGet( "DDO_SISTEMA_CODIGO_Rangefilterto");
            Ddo_sistema_codigo_Noresultsfound = cgiGet( "DDO_SISTEMA_CODIGO_Noresultsfound");
            Ddo_sistema_codigo_Searchbuttontext = cgiGet( "DDO_SISTEMA_CODIGO_Searchbuttontext");
            Ddo_servico_codigo_Caption = cgiGet( "DDO_SERVICO_CODIGO_Caption");
            Ddo_servico_codigo_Tooltip = cgiGet( "DDO_SERVICO_CODIGO_Tooltip");
            Ddo_servico_codigo_Cls = cgiGet( "DDO_SERVICO_CODIGO_Cls");
            Ddo_servico_codigo_Filteredtext_set = cgiGet( "DDO_SERVICO_CODIGO_Filteredtext_set");
            Ddo_servico_codigo_Filteredtextto_set = cgiGet( "DDO_SERVICO_CODIGO_Filteredtextto_set");
            Ddo_servico_codigo_Dropdownoptionstype = cgiGet( "DDO_SERVICO_CODIGO_Dropdownoptionstype");
            Ddo_servico_codigo_Titlecontrolidtoreplace = cgiGet( "DDO_SERVICO_CODIGO_Titlecontrolidtoreplace");
            Ddo_servico_codigo_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_SERVICO_CODIGO_Includesortasc"));
            Ddo_servico_codigo_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_SERVICO_CODIGO_Includesortdsc"));
            Ddo_servico_codigo_Sortedstatus = cgiGet( "DDO_SERVICO_CODIGO_Sortedstatus");
            Ddo_servico_codigo_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_SERVICO_CODIGO_Includefilter"));
            Ddo_servico_codigo_Filtertype = cgiGet( "DDO_SERVICO_CODIGO_Filtertype");
            Ddo_servico_codigo_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_SERVICO_CODIGO_Filterisrange"));
            Ddo_servico_codigo_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_SERVICO_CODIGO_Includedatalist"));
            Ddo_servico_codigo_Datalistfixedvalues = cgiGet( "DDO_SERVICO_CODIGO_Datalistfixedvalues");
            Ddo_servico_codigo_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_SERVICO_CODIGO_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_servico_codigo_Sortasc = cgiGet( "DDO_SERVICO_CODIGO_Sortasc");
            Ddo_servico_codigo_Sortdsc = cgiGet( "DDO_SERVICO_CODIGO_Sortdsc");
            Ddo_servico_codigo_Loadingdata = cgiGet( "DDO_SERVICO_CODIGO_Loadingdata");
            Ddo_servico_codigo_Cleanfilter = cgiGet( "DDO_SERVICO_CODIGO_Cleanfilter");
            Ddo_servico_codigo_Rangefilterfrom = cgiGet( "DDO_SERVICO_CODIGO_Rangefilterfrom");
            Ddo_servico_codigo_Rangefilterto = cgiGet( "DDO_SERVICO_CODIGO_Rangefilterto");
            Ddo_servico_codigo_Noresultsfound = cgiGet( "DDO_SERVICO_CODIGO_Noresultsfound");
            Ddo_servico_codigo_Searchbuttontext = cgiGet( "DDO_SERVICO_CODIGO_Searchbuttontext");
            Ddo_solicitacoes_novo_projeto_Caption = cgiGet( "DDO_SOLICITACOES_NOVO_PROJETO_Caption");
            Ddo_solicitacoes_novo_projeto_Tooltip = cgiGet( "DDO_SOLICITACOES_NOVO_PROJETO_Tooltip");
            Ddo_solicitacoes_novo_projeto_Cls = cgiGet( "DDO_SOLICITACOES_NOVO_PROJETO_Cls");
            Ddo_solicitacoes_novo_projeto_Selectedvalue_set = cgiGet( "DDO_SOLICITACOES_NOVO_PROJETO_Selectedvalue_set");
            Ddo_solicitacoes_novo_projeto_Dropdownoptionstype = cgiGet( "DDO_SOLICITACOES_NOVO_PROJETO_Dropdownoptionstype");
            Ddo_solicitacoes_novo_projeto_Titlecontrolidtoreplace = cgiGet( "DDO_SOLICITACOES_NOVO_PROJETO_Titlecontrolidtoreplace");
            Ddo_solicitacoes_novo_projeto_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_SOLICITACOES_NOVO_PROJETO_Includesortasc"));
            Ddo_solicitacoes_novo_projeto_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_SOLICITACOES_NOVO_PROJETO_Includesortdsc"));
            Ddo_solicitacoes_novo_projeto_Sortedstatus = cgiGet( "DDO_SOLICITACOES_NOVO_PROJETO_Sortedstatus");
            Ddo_solicitacoes_novo_projeto_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_SOLICITACOES_NOVO_PROJETO_Includefilter"));
            Ddo_solicitacoes_novo_projeto_Filtertype = cgiGet( "DDO_SOLICITACOES_NOVO_PROJETO_Filtertype");
            Ddo_solicitacoes_novo_projeto_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_SOLICITACOES_NOVO_PROJETO_Filterisrange"));
            Ddo_solicitacoes_novo_projeto_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_SOLICITACOES_NOVO_PROJETO_Includedatalist"));
            Ddo_solicitacoes_novo_projeto_Datalisttype = cgiGet( "DDO_SOLICITACOES_NOVO_PROJETO_Datalisttype");
            Ddo_solicitacoes_novo_projeto_Datalistfixedvalues = cgiGet( "DDO_SOLICITACOES_NOVO_PROJETO_Datalistfixedvalues");
            Ddo_solicitacoes_novo_projeto_Datalistproc = cgiGet( "DDO_SOLICITACOES_NOVO_PROJETO_Datalistproc");
            Ddo_solicitacoes_novo_projeto_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_SOLICITACOES_NOVO_PROJETO_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_solicitacoes_novo_projeto_Sortasc = cgiGet( "DDO_SOLICITACOES_NOVO_PROJETO_Sortasc");
            Ddo_solicitacoes_novo_projeto_Sortdsc = cgiGet( "DDO_SOLICITACOES_NOVO_PROJETO_Sortdsc");
            Ddo_solicitacoes_novo_projeto_Loadingdata = cgiGet( "DDO_SOLICITACOES_NOVO_PROJETO_Loadingdata");
            Ddo_solicitacoes_novo_projeto_Cleanfilter = cgiGet( "DDO_SOLICITACOES_NOVO_PROJETO_Cleanfilter");
            Ddo_solicitacoes_novo_projeto_Rangefilterfrom = cgiGet( "DDO_SOLICITACOES_NOVO_PROJETO_Rangefilterfrom");
            Ddo_solicitacoes_novo_projeto_Rangefilterto = cgiGet( "DDO_SOLICITACOES_NOVO_PROJETO_Rangefilterto");
            Ddo_solicitacoes_novo_projeto_Noresultsfound = cgiGet( "DDO_SOLICITACOES_NOVO_PROJETO_Noresultsfound");
            Ddo_solicitacoes_novo_projeto_Searchbuttontext = cgiGet( "DDO_SOLICITACOES_NOVO_PROJETO_Searchbuttontext");
            Ddo_solicitacoes_objetivo_Caption = cgiGet( "DDO_SOLICITACOES_OBJETIVO_Caption");
            Ddo_solicitacoes_objetivo_Tooltip = cgiGet( "DDO_SOLICITACOES_OBJETIVO_Tooltip");
            Ddo_solicitacoes_objetivo_Cls = cgiGet( "DDO_SOLICITACOES_OBJETIVO_Cls");
            Ddo_solicitacoes_objetivo_Filteredtext_set = cgiGet( "DDO_SOLICITACOES_OBJETIVO_Filteredtext_set");
            Ddo_solicitacoes_objetivo_Selectedvalue_set = cgiGet( "DDO_SOLICITACOES_OBJETIVO_Selectedvalue_set");
            Ddo_solicitacoes_objetivo_Dropdownoptionstype = cgiGet( "DDO_SOLICITACOES_OBJETIVO_Dropdownoptionstype");
            Ddo_solicitacoes_objetivo_Titlecontrolidtoreplace = cgiGet( "DDO_SOLICITACOES_OBJETIVO_Titlecontrolidtoreplace");
            Ddo_solicitacoes_objetivo_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_SOLICITACOES_OBJETIVO_Includesortasc"));
            Ddo_solicitacoes_objetivo_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_SOLICITACOES_OBJETIVO_Includesortdsc"));
            Ddo_solicitacoes_objetivo_Sortedstatus = cgiGet( "DDO_SOLICITACOES_OBJETIVO_Sortedstatus");
            Ddo_solicitacoes_objetivo_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_SOLICITACOES_OBJETIVO_Includefilter"));
            Ddo_solicitacoes_objetivo_Filtertype = cgiGet( "DDO_SOLICITACOES_OBJETIVO_Filtertype");
            Ddo_solicitacoes_objetivo_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_SOLICITACOES_OBJETIVO_Filterisrange"));
            Ddo_solicitacoes_objetivo_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_SOLICITACOES_OBJETIVO_Includedatalist"));
            Ddo_solicitacoes_objetivo_Datalisttype = cgiGet( "DDO_SOLICITACOES_OBJETIVO_Datalisttype");
            Ddo_solicitacoes_objetivo_Datalistfixedvalues = cgiGet( "DDO_SOLICITACOES_OBJETIVO_Datalistfixedvalues");
            Ddo_solicitacoes_objetivo_Datalistproc = cgiGet( "DDO_SOLICITACOES_OBJETIVO_Datalistproc");
            Ddo_solicitacoes_objetivo_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_SOLICITACOES_OBJETIVO_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_solicitacoes_objetivo_Sortasc = cgiGet( "DDO_SOLICITACOES_OBJETIVO_Sortasc");
            Ddo_solicitacoes_objetivo_Sortdsc = cgiGet( "DDO_SOLICITACOES_OBJETIVO_Sortdsc");
            Ddo_solicitacoes_objetivo_Loadingdata = cgiGet( "DDO_SOLICITACOES_OBJETIVO_Loadingdata");
            Ddo_solicitacoes_objetivo_Cleanfilter = cgiGet( "DDO_SOLICITACOES_OBJETIVO_Cleanfilter");
            Ddo_solicitacoes_objetivo_Rangefilterfrom = cgiGet( "DDO_SOLICITACOES_OBJETIVO_Rangefilterfrom");
            Ddo_solicitacoes_objetivo_Rangefilterto = cgiGet( "DDO_SOLICITACOES_OBJETIVO_Rangefilterto");
            Ddo_solicitacoes_objetivo_Noresultsfound = cgiGet( "DDO_SOLICITACOES_OBJETIVO_Noresultsfound");
            Ddo_solicitacoes_objetivo_Searchbuttontext = cgiGet( "DDO_SOLICITACOES_OBJETIVO_Searchbuttontext");
            Ddo_solicitacoes_usuario_Caption = cgiGet( "DDO_SOLICITACOES_USUARIO_Caption");
            Ddo_solicitacoes_usuario_Tooltip = cgiGet( "DDO_SOLICITACOES_USUARIO_Tooltip");
            Ddo_solicitacoes_usuario_Cls = cgiGet( "DDO_SOLICITACOES_USUARIO_Cls");
            Ddo_solicitacoes_usuario_Filteredtext_set = cgiGet( "DDO_SOLICITACOES_USUARIO_Filteredtext_set");
            Ddo_solicitacoes_usuario_Filteredtextto_set = cgiGet( "DDO_SOLICITACOES_USUARIO_Filteredtextto_set");
            Ddo_solicitacoes_usuario_Dropdownoptionstype = cgiGet( "DDO_SOLICITACOES_USUARIO_Dropdownoptionstype");
            Ddo_solicitacoes_usuario_Titlecontrolidtoreplace = cgiGet( "DDO_SOLICITACOES_USUARIO_Titlecontrolidtoreplace");
            Ddo_solicitacoes_usuario_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_SOLICITACOES_USUARIO_Includesortasc"));
            Ddo_solicitacoes_usuario_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_SOLICITACOES_USUARIO_Includesortdsc"));
            Ddo_solicitacoes_usuario_Sortedstatus = cgiGet( "DDO_SOLICITACOES_USUARIO_Sortedstatus");
            Ddo_solicitacoes_usuario_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_SOLICITACOES_USUARIO_Includefilter"));
            Ddo_solicitacoes_usuario_Filtertype = cgiGet( "DDO_SOLICITACOES_USUARIO_Filtertype");
            Ddo_solicitacoes_usuario_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_SOLICITACOES_USUARIO_Filterisrange"));
            Ddo_solicitacoes_usuario_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_SOLICITACOES_USUARIO_Includedatalist"));
            Ddo_solicitacoes_usuario_Datalistfixedvalues = cgiGet( "DDO_SOLICITACOES_USUARIO_Datalistfixedvalues");
            Ddo_solicitacoes_usuario_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_SOLICITACOES_USUARIO_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_solicitacoes_usuario_Sortasc = cgiGet( "DDO_SOLICITACOES_USUARIO_Sortasc");
            Ddo_solicitacoes_usuario_Sortdsc = cgiGet( "DDO_SOLICITACOES_USUARIO_Sortdsc");
            Ddo_solicitacoes_usuario_Loadingdata = cgiGet( "DDO_SOLICITACOES_USUARIO_Loadingdata");
            Ddo_solicitacoes_usuario_Cleanfilter = cgiGet( "DDO_SOLICITACOES_USUARIO_Cleanfilter");
            Ddo_solicitacoes_usuario_Rangefilterfrom = cgiGet( "DDO_SOLICITACOES_USUARIO_Rangefilterfrom");
            Ddo_solicitacoes_usuario_Rangefilterto = cgiGet( "DDO_SOLICITACOES_USUARIO_Rangefilterto");
            Ddo_solicitacoes_usuario_Noresultsfound = cgiGet( "DDO_SOLICITACOES_USUARIO_Noresultsfound");
            Ddo_solicitacoes_usuario_Searchbuttontext = cgiGet( "DDO_SOLICITACOES_USUARIO_Searchbuttontext");
            Ddo_solicitacoes_data_Caption = cgiGet( "DDO_SOLICITACOES_DATA_Caption");
            Ddo_solicitacoes_data_Tooltip = cgiGet( "DDO_SOLICITACOES_DATA_Tooltip");
            Ddo_solicitacoes_data_Cls = cgiGet( "DDO_SOLICITACOES_DATA_Cls");
            Ddo_solicitacoes_data_Filteredtext_set = cgiGet( "DDO_SOLICITACOES_DATA_Filteredtext_set");
            Ddo_solicitacoes_data_Filteredtextto_set = cgiGet( "DDO_SOLICITACOES_DATA_Filteredtextto_set");
            Ddo_solicitacoes_data_Dropdownoptionstype = cgiGet( "DDO_SOLICITACOES_DATA_Dropdownoptionstype");
            Ddo_solicitacoes_data_Titlecontrolidtoreplace = cgiGet( "DDO_SOLICITACOES_DATA_Titlecontrolidtoreplace");
            Ddo_solicitacoes_data_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_SOLICITACOES_DATA_Includesortasc"));
            Ddo_solicitacoes_data_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_SOLICITACOES_DATA_Includesortdsc"));
            Ddo_solicitacoes_data_Sortedstatus = cgiGet( "DDO_SOLICITACOES_DATA_Sortedstatus");
            Ddo_solicitacoes_data_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_SOLICITACOES_DATA_Includefilter"));
            Ddo_solicitacoes_data_Filtertype = cgiGet( "DDO_SOLICITACOES_DATA_Filtertype");
            Ddo_solicitacoes_data_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_SOLICITACOES_DATA_Filterisrange"));
            Ddo_solicitacoes_data_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_SOLICITACOES_DATA_Includedatalist"));
            Ddo_solicitacoes_data_Datalistfixedvalues = cgiGet( "DDO_SOLICITACOES_DATA_Datalistfixedvalues");
            Ddo_solicitacoes_data_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_SOLICITACOES_DATA_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_solicitacoes_data_Sortasc = cgiGet( "DDO_SOLICITACOES_DATA_Sortasc");
            Ddo_solicitacoes_data_Sortdsc = cgiGet( "DDO_SOLICITACOES_DATA_Sortdsc");
            Ddo_solicitacoes_data_Loadingdata = cgiGet( "DDO_SOLICITACOES_DATA_Loadingdata");
            Ddo_solicitacoes_data_Cleanfilter = cgiGet( "DDO_SOLICITACOES_DATA_Cleanfilter");
            Ddo_solicitacoes_data_Rangefilterfrom = cgiGet( "DDO_SOLICITACOES_DATA_Rangefilterfrom");
            Ddo_solicitacoes_data_Rangefilterto = cgiGet( "DDO_SOLICITACOES_DATA_Rangefilterto");
            Ddo_solicitacoes_data_Noresultsfound = cgiGet( "DDO_SOLICITACOES_DATA_Noresultsfound");
            Ddo_solicitacoes_data_Searchbuttontext = cgiGet( "DDO_SOLICITACOES_DATA_Searchbuttontext");
            Ddo_solicitacoes_usuario_ult_Caption = cgiGet( "DDO_SOLICITACOES_USUARIO_ULT_Caption");
            Ddo_solicitacoes_usuario_ult_Tooltip = cgiGet( "DDO_SOLICITACOES_USUARIO_ULT_Tooltip");
            Ddo_solicitacoes_usuario_ult_Cls = cgiGet( "DDO_SOLICITACOES_USUARIO_ULT_Cls");
            Ddo_solicitacoes_usuario_ult_Filteredtext_set = cgiGet( "DDO_SOLICITACOES_USUARIO_ULT_Filteredtext_set");
            Ddo_solicitacoes_usuario_ult_Filteredtextto_set = cgiGet( "DDO_SOLICITACOES_USUARIO_ULT_Filteredtextto_set");
            Ddo_solicitacoes_usuario_ult_Dropdownoptionstype = cgiGet( "DDO_SOLICITACOES_USUARIO_ULT_Dropdownoptionstype");
            Ddo_solicitacoes_usuario_ult_Titlecontrolidtoreplace = cgiGet( "DDO_SOLICITACOES_USUARIO_ULT_Titlecontrolidtoreplace");
            Ddo_solicitacoes_usuario_ult_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_SOLICITACOES_USUARIO_ULT_Includesortasc"));
            Ddo_solicitacoes_usuario_ult_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_SOLICITACOES_USUARIO_ULT_Includesortdsc"));
            Ddo_solicitacoes_usuario_ult_Sortedstatus = cgiGet( "DDO_SOLICITACOES_USUARIO_ULT_Sortedstatus");
            Ddo_solicitacoes_usuario_ult_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_SOLICITACOES_USUARIO_ULT_Includefilter"));
            Ddo_solicitacoes_usuario_ult_Filtertype = cgiGet( "DDO_SOLICITACOES_USUARIO_ULT_Filtertype");
            Ddo_solicitacoes_usuario_ult_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_SOLICITACOES_USUARIO_ULT_Filterisrange"));
            Ddo_solicitacoes_usuario_ult_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_SOLICITACOES_USUARIO_ULT_Includedatalist"));
            Ddo_solicitacoes_usuario_ult_Datalistfixedvalues = cgiGet( "DDO_SOLICITACOES_USUARIO_ULT_Datalistfixedvalues");
            Ddo_solicitacoes_usuario_ult_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_SOLICITACOES_USUARIO_ULT_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_solicitacoes_usuario_ult_Sortasc = cgiGet( "DDO_SOLICITACOES_USUARIO_ULT_Sortasc");
            Ddo_solicitacoes_usuario_ult_Sortdsc = cgiGet( "DDO_SOLICITACOES_USUARIO_ULT_Sortdsc");
            Ddo_solicitacoes_usuario_ult_Loadingdata = cgiGet( "DDO_SOLICITACOES_USUARIO_ULT_Loadingdata");
            Ddo_solicitacoes_usuario_ult_Cleanfilter = cgiGet( "DDO_SOLICITACOES_USUARIO_ULT_Cleanfilter");
            Ddo_solicitacoes_usuario_ult_Rangefilterfrom = cgiGet( "DDO_SOLICITACOES_USUARIO_ULT_Rangefilterfrom");
            Ddo_solicitacoes_usuario_ult_Rangefilterto = cgiGet( "DDO_SOLICITACOES_USUARIO_ULT_Rangefilterto");
            Ddo_solicitacoes_usuario_ult_Noresultsfound = cgiGet( "DDO_SOLICITACOES_USUARIO_ULT_Noresultsfound");
            Ddo_solicitacoes_usuario_ult_Searchbuttontext = cgiGet( "DDO_SOLICITACOES_USUARIO_ULT_Searchbuttontext");
            Ddo_solicitacoes_data_ult_Caption = cgiGet( "DDO_SOLICITACOES_DATA_ULT_Caption");
            Ddo_solicitacoes_data_ult_Tooltip = cgiGet( "DDO_SOLICITACOES_DATA_ULT_Tooltip");
            Ddo_solicitacoes_data_ult_Cls = cgiGet( "DDO_SOLICITACOES_DATA_ULT_Cls");
            Ddo_solicitacoes_data_ult_Filteredtext_set = cgiGet( "DDO_SOLICITACOES_DATA_ULT_Filteredtext_set");
            Ddo_solicitacoes_data_ult_Filteredtextto_set = cgiGet( "DDO_SOLICITACOES_DATA_ULT_Filteredtextto_set");
            Ddo_solicitacoes_data_ult_Dropdownoptionstype = cgiGet( "DDO_SOLICITACOES_DATA_ULT_Dropdownoptionstype");
            Ddo_solicitacoes_data_ult_Titlecontrolidtoreplace = cgiGet( "DDO_SOLICITACOES_DATA_ULT_Titlecontrolidtoreplace");
            Ddo_solicitacoes_data_ult_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_SOLICITACOES_DATA_ULT_Includesortasc"));
            Ddo_solicitacoes_data_ult_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_SOLICITACOES_DATA_ULT_Includesortdsc"));
            Ddo_solicitacoes_data_ult_Sortedstatus = cgiGet( "DDO_SOLICITACOES_DATA_ULT_Sortedstatus");
            Ddo_solicitacoes_data_ult_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_SOLICITACOES_DATA_ULT_Includefilter"));
            Ddo_solicitacoes_data_ult_Filtertype = cgiGet( "DDO_SOLICITACOES_DATA_ULT_Filtertype");
            Ddo_solicitacoes_data_ult_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_SOLICITACOES_DATA_ULT_Filterisrange"));
            Ddo_solicitacoes_data_ult_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_SOLICITACOES_DATA_ULT_Includedatalist"));
            Ddo_solicitacoes_data_ult_Datalistfixedvalues = cgiGet( "DDO_SOLICITACOES_DATA_ULT_Datalistfixedvalues");
            Ddo_solicitacoes_data_ult_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_SOLICITACOES_DATA_ULT_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_solicitacoes_data_ult_Sortasc = cgiGet( "DDO_SOLICITACOES_DATA_ULT_Sortasc");
            Ddo_solicitacoes_data_ult_Sortdsc = cgiGet( "DDO_SOLICITACOES_DATA_ULT_Sortdsc");
            Ddo_solicitacoes_data_ult_Loadingdata = cgiGet( "DDO_SOLICITACOES_DATA_ULT_Loadingdata");
            Ddo_solicitacoes_data_ult_Cleanfilter = cgiGet( "DDO_SOLICITACOES_DATA_ULT_Cleanfilter");
            Ddo_solicitacoes_data_ult_Rangefilterfrom = cgiGet( "DDO_SOLICITACOES_DATA_ULT_Rangefilterfrom");
            Ddo_solicitacoes_data_ult_Rangefilterto = cgiGet( "DDO_SOLICITACOES_DATA_ULT_Rangefilterto");
            Ddo_solicitacoes_data_ult_Noresultsfound = cgiGet( "DDO_SOLICITACOES_DATA_ULT_Noresultsfound");
            Ddo_solicitacoes_data_ult_Searchbuttontext = cgiGet( "DDO_SOLICITACOES_DATA_ULT_Searchbuttontext");
            Ddo_solicitacoes_status_Caption = cgiGet( "DDO_SOLICITACOES_STATUS_Caption");
            Ddo_solicitacoes_status_Tooltip = cgiGet( "DDO_SOLICITACOES_STATUS_Tooltip");
            Ddo_solicitacoes_status_Cls = cgiGet( "DDO_SOLICITACOES_STATUS_Cls");
            Ddo_solicitacoes_status_Selectedvalue_set = cgiGet( "DDO_SOLICITACOES_STATUS_Selectedvalue_set");
            Ddo_solicitacoes_status_Dropdownoptionstype = cgiGet( "DDO_SOLICITACOES_STATUS_Dropdownoptionstype");
            Ddo_solicitacoes_status_Titlecontrolidtoreplace = cgiGet( "DDO_SOLICITACOES_STATUS_Titlecontrolidtoreplace");
            Ddo_solicitacoes_status_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_SOLICITACOES_STATUS_Includesortasc"));
            Ddo_solicitacoes_status_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_SOLICITACOES_STATUS_Includesortdsc"));
            Ddo_solicitacoes_status_Sortedstatus = cgiGet( "DDO_SOLICITACOES_STATUS_Sortedstatus");
            Ddo_solicitacoes_status_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_SOLICITACOES_STATUS_Includefilter"));
            Ddo_solicitacoes_status_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_SOLICITACOES_STATUS_Filterisrange"));
            Ddo_solicitacoes_status_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_SOLICITACOES_STATUS_Includedatalist"));
            Ddo_solicitacoes_status_Datalisttype = cgiGet( "DDO_SOLICITACOES_STATUS_Datalisttype");
            Ddo_solicitacoes_status_Allowmultipleselection = StringUtil.StrToBool( cgiGet( "DDO_SOLICITACOES_STATUS_Allowmultipleselection"));
            Ddo_solicitacoes_status_Datalistfixedvalues = cgiGet( "DDO_SOLICITACOES_STATUS_Datalistfixedvalues");
            Ddo_solicitacoes_status_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_SOLICITACOES_STATUS_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_solicitacoes_status_Sortasc = cgiGet( "DDO_SOLICITACOES_STATUS_Sortasc");
            Ddo_solicitacoes_status_Sortdsc = cgiGet( "DDO_SOLICITACOES_STATUS_Sortdsc");
            Ddo_solicitacoes_status_Loadingdata = cgiGet( "DDO_SOLICITACOES_STATUS_Loadingdata");
            Ddo_solicitacoes_status_Cleanfilter = cgiGet( "DDO_SOLICITACOES_STATUS_Cleanfilter");
            Ddo_solicitacoes_status_Rangefilterfrom = cgiGet( "DDO_SOLICITACOES_STATUS_Rangefilterfrom");
            Ddo_solicitacoes_status_Rangefilterto = cgiGet( "DDO_SOLICITACOES_STATUS_Rangefilterto");
            Ddo_solicitacoes_status_Noresultsfound = cgiGet( "DDO_SOLICITACOES_STATUS_Noresultsfound");
            Ddo_solicitacoes_status_Searchbuttontext = cgiGet( "DDO_SOLICITACOES_STATUS_Searchbuttontext");
            Gridpaginationbar_Selectedpage = cgiGet( "GRIDPAGINATIONBAR_Selectedpage");
            Ddo_solicitacoes_codigo_Activeeventkey = cgiGet( "DDO_SOLICITACOES_CODIGO_Activeeventkey");
            Ddo_solicitacoes_codigo_Filteredtext_get = cgiGet( "DDO_SOLICITACOES_CODIGO_Filteredtext_get");
            Ddo_solicitacoes_codigo_Filteredtextto_get = cgiGet( "DDO_SOLICITACOES_CODIGO_Filteredtextto_get");
            Ddo_contratada_codigo_Activeeventkey = cgiGet( "DDO_CONTRATADA_CODIGO_Activeeventkey");
            Ddo_contratada_codigo_Filteredtext_get = cgiGet( "DDO_CONTRATADA_CODIGO_Filteredtext_get");
            Ddo_contratada_codigo_Filteredtextto_get = cgiGet( "DDO_CONTRATADA_CODIGO_Filteredtextto_get");
            Ddo_sistema_codigo_Activeeventkey = cgiGet( "DDO_SISTEMA_CODIGO_Activeeventkey");
            Ddo_sistema_codigo_Filteredtext_get = cgiGet( "DDO_SISTEMA_CODIGO_Filteredtext_get");
            Ddo_sistema_codigo_Filteredtextto_get = cgiGet( "DDO_SISTEMA_CODIGO_Filteredtextto_get");
            Ddo_servico_codigo_Activeeventkey = cgiGet( "DDO_SERVICO_CODIGO_Activeeventkey");
            Ddo_servico_codigo_Filteredtext_get = cgiGet( "DDO_SERVICO_CODIGO_Filteredtext_get");
            Ddo_servico_codigo_Filteredtextto_get = cgiGet( "DDO_SERVICO_CODIGO_Filteredtextto_get");
            Ddo_solicitacoes_novo_projeto_Activeeventkey = cgiGet( "DDO_SOLICITACOES_NOVO_PROJETO_Activeeventkey");
            Ddo_solicitacoes_novo_projeto_Selectedvalue_get = cgiGet( "DDO_SOLICITACOES_NOVO_PROJETO_Selectedvalue_get");
            Ddo_solicitacoes_objetivo_Activeeventkey = cgiGet( "DDO_SOLICITACOES_OBJETIVO_Activeeventkey");
            Ddo_solicitacoes_objetivo_Filteredtext_get = cgiGet( "DDO_SOLICITACOES_OBJETIVO_Filteredtext_get");
            Ddo_solicitacoes_objetivo_Selectedvalue_get = cgiGet( "DDO_SOLICITACOES_OBJETIVO_Selectedvalue_get");
            Ddo_solicitacoes_usuario_Activeeventkey = cgiGet( "DDO_SOLICITACOES_USUARIO_Activeeventkey");
            Ddo_solicitacoes_usuario_Filteredtext_get = cgiGet( "DDO_SOLICITACOES_USUARIO_Filteredtext_get");
            Ddo_solicitacoes_usuario_Filteredtextto_get = cgiGet( "DDO_SOLICITACOES_USUARIO_Filteredtextto_get");
            Ddo_solicitacoes_data_Activeeventkey = cgiGet( "DDO_SOLICITACOES_DATA_Activeeventkey");
            Ddo_solicitacoes_data_Filteredtext_get = cgiGet( "DDO_SOLICITACOES_DATA_Filteredtext_get");
            Ddo_solicitacoes_data_Filteredtextto_get = cgiGet( "DDO_SOLICITACOES_DATA_Filteredtextto_get");
            Ddo_solicitacoes_usuario_ult_Activeeventkey = cgiGet( "DDO_SOLICITACOES_USUARIO_ULT_Activeeventkey");
            Ddo_solicitacoes_usuario_ult_Filteredtext_get = cgiGet( "DDO_SOLICITACOES_USUARIO_ULT_Filteredtext_get");
            Ddo_solicitacoes_usuario_ult_Filteredtextto_get = cgiGet( "DDO_SOLICITACOES_USUARIO_ULT_Filteredtextto_get");
            Ddo_solicitacoes_data_ult_Activeeventkey = cgiGet( "DDO_SOLICITACOES_DATA_ULT_Activeeventkey");
            Ddo_solicitacoes_data_ult_Filteredtext_get = cgiGet( "DDO_SOLICITACOES_DATA_ULT_Filteredtext_get");
            Ddo_solicitacoes_data_ult_Filteredtextto_get = cgiGet( "DDO_SOLICITACOES_DATA_ULT_Filteredtextto_get");
            Ddo_solicitacoes_status_Activeeventkey = cgiGet( "DDO_SOLICITACOES_STATUS_Activeeventkey");
            Ddo_solicitacoes_status_Selectedvalue_get = cgiGet( "DDO_SOLICITACOES_STATUS_Selectedvalue_get");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR1"), ",", ".") != Convert.ToDecimal( AV16DynamicFiltersOperator1 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vSOLICITACOES_NOVO_PROJETO1"), AV17Solicitacoes_Novo_Projeto1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV19DynamicFiltersSelector2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR2"), ",", ".") != Convert.ToDecimal( AV20DynamicFiltersOperator2 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vSOLICITACOES_NOVO_PROJETO2"), AV21Solicitacoes_Novo_Projeto2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV23DynamicFiltersSelector3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR3"), ",", ".") != Convert.ToDecimal( AV24DynamicFiltersOperator3 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vSOLICITACOES_NOVO_PROJETO3"), AV25Solicitacoes_Novo_Projeto3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV18DynamicFiltersEnabled2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV22DynamicFiltersEnabled3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFSOLICITACOES_CODIGO"), ",", ".") != Convert.ToDecimal( AV31TFSolicitacoes_Codigo )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFSOLICITACOES_CODIGO_TO"), ",", ".") != Convert.ToDecimal( AV32TFSolicitacoes_Codigo_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATADA_CODIGO"), ",", ".") != Convert.ToDecimal( AV35TFContratada_Codigo )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFCONTRATADA_CODIGO_TO"), ",", ".") != Convert.ToDecimal( AV36TFContratada_Codigo_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFSISTEMA_CODIGO"), ",", ".") != Convert.ToDecimal( AV39TFSistema_Codigo )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFSISTEMA_CODIGO_TO"), ",", ".") != Convert.ToDecimal( AV40TFSistema_Codigo_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFSERVICO_CODIGO"), ",", ".") != Convert.ToDecimal( AV43TFServico_Codigo )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFSERVICO_CODIGO_TO"), ",", ".") != Convert.ToDecimal( AV44TFServico_Codigo_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFSOLICITACOES_NOVO_PROJETO_SEL"), AV47TFSolicitacoes_Novo_Projeto_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFSOLICITACOES_OBJETIVO"), AV50TFSolicitacoes_Objetivo) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFSOLICITACOES_OBJETIVO_SEL"), AV51TFSolicitacoes_Objetivo_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFSOLICITACOES_USUARIO"), ",", ".") != Convert.ToDecimal( AV54TFSolicitacoes_Usuario )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFSOLICITACOES_USUARIO_TO"), ",", ".") != Convert.ToDecimal( AV55TFSolicitacoes_Usuario_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vTFSOLICITACOES_DATA"), 0) != AV58TFSolicitacoes_Data )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vTFSOLICITACOES_DATA_TO"), 0) != AV59TFSolicitacoes_Data_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFSOLICITACOES_USUARIO_ULT"), ",", ".") != Convert.ToDecimal( AV64TFSolicitacoes_Usuario_Ult )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFSOLICITACOES_USUARIO_ULT_TO"), ",", ".") != Convert.ToDecimal( AV65TFSolicitacoes_Usuario_Ult_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vTFSOLICITACOES_DATA_ULT"), 0) != AV68TFSolicitacoes_Data_Ult )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vTFSOLICITACOES_DATA_ULT_TO"), 0) != AV69TFSolicitacoes_Data_Ult_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E33AW2 */
         E33AW2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E33AW2( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         lblJsdynamicfilters_Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         chkavDynamicfiltersenabled2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled2.Visible), 5, 0)));
         chkavDynamicfiltersenabled3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled3.Visible), 5, 0)));
         AV15DynamicFiltersSelector1 = "SOLICITACOES_NOVO_PROJETO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV19DynamicFiltersSelector2 = "SOLICITACOES_NOVO_PROJETO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV23DynamicFiltersSelector3 = "SOLICITACOES_NOVO_PROJETO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         imgAdddynamicfilters1_Jsonclick = "WWPDynFilterShow(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Jsonclick", imgAdddynamicfilters1_Jsonclick);
         imgRemovedynamicfilters1_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Jsonclick", imgRemovedynamicfilters1_Jsonclick);
         imgAdddynamicfilters2_Jsonclick = "WWPDynFilterShow(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Jsonclick", imgAdddynamicfilters2_Jsonclick);
         imgRemovedynamicfilters2_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Jsonclick", imgRemovedynamicfilters2_Jsonclick);
         imgRemovedynamicfilters3_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters3_Internalname, "Jsonclick", imgRemovedynamicfilters3_Jsonclick);
         edtavTfsolicitacoes_codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfsolicitacoes_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfsolicitacoes_codigo_Visible), 5, 0)));
         edtavTfsolicitacoes_codigo_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfsolicitacoes_codigo_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfsolicitacoes_codigo_to_Visible), 5, 0)));
         edtavTfcontratada_codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratada_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratada_codigo_Visible), 5, 0)));
         edtavTfcontratada_codigo_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfcontratada_codigo_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratada_codigo_to_Visible), 5, 0)));
         edtavTfsistema_codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfsistema_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfsistema_codigo_Visible), 5, 0)));
         edtavTfsistema_codigo_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfsistema_codigo_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfsistema_codigo_to_Visible), 5, 0)));
         edtavTfservico_codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfservico_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfservico_codigo_Visible), 5, 0)));
         edtavTfservico_codigo_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfservico_codigo_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfservico_codigo_to_Visible), 5, 0)));
         edtavTfsolicitacoes_novo_projeto_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfsolicitacoes_novo_projeto_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfsolicitacoes_novo_projeto_sel_Visible), 5, 0)));
         edtavTfsolicitacoes_objetivo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfsolicitacoes_objetivo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfsolicitacoes_objetivo_Visible), 5, 0)));
         edtavTfsolicitacoes_objetivo_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfsolicitacoes_objetivo_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfsolicitacoes_objetivo_sel_Visible), 5, 0)));
         edtavTfsolicitacoes_usuario_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfsolicitacoes_usuario_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfsolicitacoes_usuario_Visible), 5, 0)));
         edtavTfsolicitacoes_usuario_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfsolicitacoes_usuario_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfsolicitacoes_usuario_to_Visible), 5, 0)));
         edtavTfsolicitacoes_data_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfsolicitacoes_data_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfsolicitacoes_data_Visible), 5, 0)));
         edtavTfsolicitacoes_data_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfsolicitacoes_data_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfsolicitacoes_data_to_Visible), 5, 0)));
         edtavTfsolicitacoes_usuario_ult_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfsolicitacoes_usuario_ult_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfsolicitacoes_usuario_ult_Visible), 5, 0)));
         edtavTfsolicitacoes_usuario_ult_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfsolicitacoes_usuario_ult_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfsolicitacoes_usuario_ult_to_Visible), 5, 0)));
         edtavTfsolicitacoes_data_ult_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfsolicitacoes_data_ult_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfsolicitacoes_data_ult_Visible), 5, 0)));
         edtavTfsolicitacoes_data_ult_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfsolicitacoes_data_ult_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfsolicitacoes_data_ult_to_Visible), 5, 0)));
         Ddo_solicitacoes_codigo_Titlecontrolidtoreplace = subGrid_Internalname+"_Solicitacoes_Codigo";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_solicitacoes_codigo_Internalname, "TitleControlIdToReplace", Ddo_solicitacoes_codigo_Titlecontrolidtoreplace);
         AV33ddo_Solicitacoes_CodigoTitleControlIdToReplace = Ddo_solicitacoes_codigo_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33ddo_Solicitacoes_CodigoTitleControlIdToReplace", AV33ddo_Solicitacoes_CodigoTitleControlIdToReplace);
         edtavDdo_solicitacoes_codigotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_solicitacoes_codigotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_solicitacoes_codigotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratada_codigo_Titlecontrolidtoreplace = subGrid_Internalname+"_Contratada_Codigo";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_codigo_Internalname, "TitleControlIdToReplace", Ddo_contratada_codigo_Titlecontrolidtoreplace);
         AV37ddo_Contratada_CodigoTitleControlIdToReplace = Ddo_contratada_codigo_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37ddo_Contratada_CodigoTitleControlIdToReplace", AV37ddo_Contratada_CodigoTitleControlIdToReplace);
         edtavDdo_contratada_codigotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_contratada_codigotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratada_codigotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_sistema_codigo_Titlecontrolidtoreplace = subGrid_Internalname+"_Sistema_Codigo";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_sistema_codigo_Internalname, "TitleControlIdToReplace", Ddo_sistema_codigo_Titlecontrolidtoreplace);
         AV41ddo_Sistema_CodigoTitleControlIdToReplace = Ddo_sistema_codigo_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41ddo_Sistema_CodigoTitleControlIdToReplace", AV41ddo_Sistema_CodigoTitleControlIdToReplace);
         edtavDdo_sistema_codigotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_sistema_codigotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_sistema_codigotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_servico_codigo_Titlecontrolidtoreplace = subGrid_Internalname+"_Servico_Codigo";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servico_codigo_Internalname, "TitleControlIdToReplace", Ddo_servico_codigo_Titlecontrolidtoreplace);
         AV45ddo_Servico_CodigoTitleControlIdToReplace = Ddo_servico_codigo_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45ddo_Servico_CodigoTitleControlIdToReplace", AV45ddo_Servico_CodigoTitleControlIdToReplace);
         edtavDdo_servico_codigotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_servico_codigotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_servico_codigotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_solicitacoes_novo_projeto_Titlecontrolidtoreplace = subGrid_Internalname+"_Solicitacoes_Novo_Projeto";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_solicitacoes_novo_projeto_Internalname, "TitleControlIdToReplace", Ddo_solicitacoes_novo_projeto_Titlecontrolidtoreplace);
         AV48ddo_Solicitacoes_Novo_ProjetoTitleControlIdToReplace = Ddo_solicitacoes_novo_projeto_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48ddo_Solicitacoes_Novo_ProjetoTitleControlIdToReplace", AV48ddo_Solicitacoes_Novo_ProjetoTitleControlIdToReplace);
         edtavDdo_solicitacoes_novo_projetotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_solicitacoes_novo_projetotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_solicitacoes_novo_projetotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_solicitacoes_objetivo_Titlecontrolidtoreplace = subGrid_Internalname+"_Solicitacoes_Objetivo";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_solicitacoes_objetivo_Internalname, "TitleControlIdToReplace", Ddo_solicitacoes_objetivo_Titlecontrolidtoreplace);
         AV52ddo_Solicitacoes_ObjetivoTitleControlIdToReplace = Ddo_solicitacoes_objetivo_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52ddo_Solicitacoes_ObjetivoTitleControlIdToReplace", AV52ddo_Solicitacoes_ObjetivoTitleControlIdToReplace);
         edtavDdo_solicitacoes_objetivotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_solicitacoes_objetivotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_solicitacoes_objetivotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_solicitacoes_usuario_Titlecontrolidtoreplace = subGrid_Internalname+"_Solicitacoes_Usuario";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_solicitacoes_usuario_Internalname, "TitleControlIdToReplace", Ddo_solicitacoes_usuario_Titlecontrolidtoreplace);
         AV56ddo_Solicitacoes_UsuarioTitleControlIdToReplace = Ddo_solicitacoes_usuario_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56ddo_Solicitacoes_UsuarioTitleControlIdToReplace", AV56ddo_Solicitacoes_UsuarioTitleControlIdToReplace);
         edtavDdo_solicitacoes_usuariotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_solicitacoes_usuariotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_solicitacoes_usuariotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_solicitacoes_data_Titlecontrolidtoreplace = subGrid_Internalname+"_Solicitacoes_Data";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_solicitacoes_data_Internalname, "TitleControlIdToReplace", Ddo_solicitacoes_data_Titlecontrolidtoreplace);
         AV62ddo_Solicitacoes_DataTitleControlIdToReplace = Ddo_solicitacoes_data_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62ddo_Solicitacoes_DataTitleControlIdToReplace", AV62ddo_Solicitacoes_DataTitleControlIdToReplace);
         edtavDdo_solicitacoes_datatitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_solicitacoes_datatitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_solicitacoes_datatitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_solicitacoes_usuario_ult_Titlecontrolidtoreplace = subGrid_Internalname+"_Solicitacoes_Usuario_Ult";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_solicitacoes_usuario_ult_Internalname, "TitleControlIdToReplace", Ddo_solicitacoes_usuario_ult_Titlecontrolidtoreplace);
         AV66ddo_Solicitacoes_Usuario_UltTitleControlIdToReplace = Ddo_solicitacoes_usuario_ult_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV66ddo_Solicitacoes_Usuario_UltTitleControlIdToReplace", AV66ddo_Solicitacoes_Usuario_UltTitleControlIdToReplace);
         edtavDdo_solicitacoes_usuario_ulttitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_solicitacoes_usuario_ulttitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_solicitacoes_usuario_ulttitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_solicitacoes_data_ult_Titlecontrolidtoreplace = subGrid_Internalname+"_Solicitacoes_Data_Ult";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_solicitacoes_data_ult_Internalname, "TitleControlIdToReplace", Ddo_solicitacoes_data_ult_Titlecontrolidtoreplace);
         AV72ddo_Solicitacoes_Data_UltTitleControlIdToReplace = Ddo_solicitacoes_data_ult_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV72ddo_Solicitacoes_Data_UltTitleControlIdToReplace", AV72ddo_Solicitacoes_Data_UltTitleControlIdToReplace);
         edtavDdo_solicitacoes_data_ulttitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_solicitacoes_data_ulttitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_solicitacoes_data_ulttitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_solicitacoes_status_Titlecontrolidtoreplace = subGrid_Internalname+"_Solicitacoes_Status";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_solicitacoes_status_Internalname, "TitleControlIdToReplace", Ddo_solicitacoes_status_Titlecontrolidtoreplace);
         AV76ddo_Solicitacoes_StatusTitleControlIdToReplace = Ddo_solicitacoes_status_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV76ddo_Solicitacoes_StatusTitleControlIdToReplace", AV76ddo_Solicitacoes_StatusTitleControlIdToReplace);
         edtavDdo_solicitacoes_statustitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_solicitacoes_statustitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_solicitacoes_statustitlecontrolidtoreplace_Visible), 5, 0)));
         Form.Caption = "Selecione Solicitacoes";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavOrderedby.removeAllItems();
         cmbavOrderedby.addItem("1", "Projeto", 0);
         cmbavOrderedby.addItem("2", "Solicitacoes_Codigo", 0);
         cmbavOrderedby.addItem("3", "Contratada", 0);
         cmbavOrderedby.addItem("4", "Sistema", 0);
         cmbavOrderedby.addItem("5", "Servi�o", 0);
         cmbavOrderedby.addItem("6", "Objetivo", 0);
         cmbavOrderedby.addItem("7", "da Solicita��es", 0);
         cmbavOrderedby.addItem("8", "de inclus�o", 0);
         cmbavOrderedby.addItem("9", "Ultima Modifica��o", 0);
         cmbavOrderedby.addItem("10", "Ultima Modifica��o", 0);
         cmbavOrderedby.addItem("11", "Solicitacoes_Status", 0);
         if ( AV13OrderedBy < 1 )
         {
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         imgCleanfilters_Jsonclick = "WWPDynFilterHideAll(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgCleanfilters_Internalname, "Jsonclick", imgCleanfilters_Jsonclick);
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = AV77DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1) ;
         AV77DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1;
      }

      protected void E34AW2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV30Solicitacoes_CodigoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV34Contratada_CodigoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV38Sistema_CodigoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV42Servico_CodigoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV46Solicitacoes_Novo_ProjetoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV49Solicitacoes_ObjetivoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV53Solicitacoes_UsuarioTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV57Solicitacoes_DataTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV63Solicitacoes_Usuario_UltTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV67Solicitacoes_Data_UltTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV73Solicitacoes_StatusTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtSolicitacoes_Codigo_Titleformat = 2;
         edtSolicitacoes_Codigo_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Solicitacoes_Codigo", AV33ddo_Solicitacoes_CodigoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtSolicitacoes_Codigo_Internalname, "Title", edtSolicitacoes_Codigo_Title);
         edtContratada_Codigo_Titleformat = 2;
         edtContratada_Codigo_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Contratada", AV37ddo_Contratada_CodigoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratada_Codigo_Internalname, "Title", edtContratada_Codigo_Title);
         edtSistema_Codigo_Titleformat = 2;
         edtSistema_Codigo_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Sistema", AV41ddo_Sistema_CodigoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtSistema_Codigo_Internalname, "Title", edtSistema_Codigo_Title);
         edtServico_Codigo_Titleformat = 2;
         edtServico_Codigo_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Servi�o", AV45ddo_Servico_CodigoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtServico_Codigo_Internalname, "Title", edtServico_Codigo_Title);
         radSolicitacoes_Novo_Projeto_Titleformat = 2;
         radSolicitacoes_Novo_Projeto.Title.Text = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Projeto", AV48ddo_Solicitacoes_Novo_ProjetoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, radSolicitacoes_Novo_Projeto_Internalname, "Title", radSolicitacoes_Novo_Projeto.Title.Text);
         edtSolicitacoes_Objetivo_Titleformat = 2;
         edtSolicitacoes_Objetivo_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Objetivo", AV52ddo_Solicitacoes_ObjetivoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtSolicitacoes_Objetivo_Internalname, "Title", edtSolicitacoes_Objetivo_Title);
         edtSolicitacoes_Usuario_Titleformat = 2;
         edtSolicitacoes_Usuario_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "da Solicita��es", AV56ddo_Solicitacoes_UsuarioTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtSolicitacoes_Usuario_Internalname, "Title", edtSolicitacoes_Usuario_Title);
         edtSolicitacoes_Data_Titleformat = 2;
         edtSolicitacoes_Data_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "de inclus�o", AV62ddo_Solicitacoes_DataTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtSolicitacoes_Data_Internalname, "Title", edtSolicitacoes_Data_Title);
         edtSolicitacoes_Usuario_Ult_Titleformat = 2;
         edtSolicitacoes_Usuario_Ult_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Ultima Modifica��o", AV66ddo_Solicitacoes_Usuario_UltTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtSolicitacoes_Usuario_Ult_Internalname, "Title", edtSolicitacoes_Usuario_Ult_Title);
         edtSolicitacoes_Data_Ult_Titleformat = 2;
         edtSolicitacoes_Data_Ult_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Ultima Modifica��o", AV72ddo_Solicitacoes_Data_UltTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtSolicitacoes_Data_Ult_Internalname, "Title", edtSolicitacoes_Data_Ult_Title);
         cmbSolicitacoes_Status_Titleformat = 2;
         cmbSolicitacoes_Status.Title.Text = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Solicitacoes_Status", AV76ddo_Solicitacoes_StatusTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbSolicitacoes_Status_Internalname, "Title", cmbSolicitacoes_Status.Title.Text);
         AV79GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV79GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV79GridCurrentPage), 10, 0)));
         AV80GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV80GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV80GridPageCount), 10, 0)));
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV30Solicitacoes_CodigoTitleFilterData", AV30Solicitacoes_CodigoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV34Contratada_CodigoTitleFilterData", AV34Contratada_CodigoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV38Sistema_CodigoTitleFilterData", AV38Sistema_CodigoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV42Servico_CodigoTitleFilterData", AV42Servico_CodigoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV46Solicitacoes_Novo_ProjetoTitleFilterData", AV46Solicitacoes_Novo_ProjetoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV49Solicitacoes_ObjetivoTitleFilterData", AV49Solicitacoes_ObjetivoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV53Solicitacoes_UsuarioTitleFilterData", AV53Solicitacoes_UsuarioTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV57Solicitacoes_DataTitleFilterData", AV57Solicitacoes_DataTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV63Solicitacoes_Usuario_UltTitleFilterData", AV63Solicitacoes_Usuario_UltTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV67Solicitacoes_Data_UltTitleFilterData", AV67Solicitacoes_Data_UltTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV73Solicitacoes_StatusTitleFilterData", AV73Solicitacoes_StatusTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
      }

      protected void E11AW2( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV78PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV78PageToGo) ;
         }
      }

      protected void E12AW2( )
      {
         /* Ddo_solicitacoes_codigo_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_solicitacoes_codigo_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_solicitacoes_codigo_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_solicitacoes_codigo_Internalname, "SortedStatus", Ddo_solicitacoes_codigo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_solicitacoes_codigo_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_solicitacoes_codigo_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_solicitacoes_codigo_Internalname, "SortedStatus", Ddo_solicitacoes_codigo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_solicitacoes_codigo_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV31TFSolicitacoes_Codigo = (int)(NumberUtil.Val( Ddo_solicitacoes_codigo_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31TFSolicitacoes_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV31TFSolicitacoes_Codigo), 6, 0)));
            AV32TFSolicitacoes_Codigo_To = (int)(NumberUtil.Val( Ddo_solicitacoes_codigo_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32TFSolicitacoes_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV32TFSolicitacoes_Codigo_To), 6, 0)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E13AW2( )
      {
         /* Ddo_contratada_codigo_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratada_codigo_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratada_codigo_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_codigo_Internalname, "SortedStatus", Ddo_contratada_codigo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratada_codigo_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratada_codigo_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_codigo_Internalname, "SortedStatus", Ddo_contratada_codigo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_contratada_codigo_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV35TFContratada_Codigo = (int)(NumberUtil.Val( Ddo_contratada_codigo_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TFContratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35TFContratada_Codigo), 6, 0)));
            AV36TFContratada_Codigo_To = (int)(NumberUtil.Val( Ddo_contratada_codigo_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36TFContratada_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36TFContratada_Codigo_To), 6, 0)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E14AW2( )
      {
         /* Ddo_sistema_codigo_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_sistema_codigo_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_sistema_codigo_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_sistema_codigo_Internalname, "SortedStatus", Ddo_sistema_codigo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_sistema_codigo_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_sistema_codigo_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_sistema_codigo_Internalname, "SortedStatus", Ddo_sistema_codigo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_sistema_codigo_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV39TFSistema_Codigo = (int)(NumberUtil.Val( Ddo_sistema_codigo_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFSistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV39TFSistema_Codigo), 6, 0)));
            AV40TFSistema_Codigo_To = (int)(NumberUtil.Val( Ddo_sistema_codigo_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40TFSistema_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV40TFSistema_Codigo_To), 6, 0)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E15AW2( )
      {
         /* Ddo_servico_codigo_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_servico_codigo_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 5;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_servico_codigo_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servico_codigo_Internalname, "SortedStatus", Ddo_servico_codigo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_servico_codigo_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 5;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_servico_codigo_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servico_codigo_Internalname, "SortedStatus", Ddo_servico_codigo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_servico_codigo_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV43TFServico_Codigo = (int)(NumberUtil.Val( Ddo_servico_codigo_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43TFServico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV43TFServico_Codigo), 6, 0)));
            AV44TFServico_Codigo_To = (int)(NumberUtil.Val( Ddo_servico_codigo_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44TFServico_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV44TFServico_Codigo_To), 6, 0)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E16AW2( )
      {
         /* Ddo_solicitacoes_novo_projeto_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_solicitacoes_novo_projeto_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_solicitacoes_novo_projeto_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_solicitacoes_novo_projeto_Internalname, "SortedStatus", Ddo_solicitacoes_novo_projeto_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_solicitacoes_novo_projeto_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_solicitacoes_novo_projeto_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_solicitacoes_novo_projeto_Internalname, "SortedStatus", Ddo_solicitacoes_novo_projeto_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_solicitacoes_novo_projeto_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV47TFSolicitacoes_Novo_Projeto_Sel = Ddo_solicitacoes_novo_projeto_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47TFSolicitacoes_Novo_Projeto_Sel", AV47TFSolicitacoes_Novo_Projeto_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E17AW2( )
      {
         /* Ddo_solicitacoes_objetivo_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_solicitacoes_objetivo_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 6;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_solicitacoes_objetivo_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_solicitacoes_objetivo_Internalname, "SortedStatus", Ddo_solicitacoes_objetivo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_solicitacoes_objetivo_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 6;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_solicitacoes_objetivo_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_solicitacoes_objetivo_Internalname, "SortedStatus", Ddo_solicitacoes_objetivo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_solicitacoes_objetivo_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV50TFSolicitacoes_Objetivo = Ddo_solicitacoes_objetivo_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50TFSolicitacoes_Objetivo", AV50TFSolicitacoes_Objetivo);
            AV51TFSolicitacoes_Objetivo_Sel = Ddo_solicitacoes_objetivo_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51TFSolicitacoes_Objetivo_Sel", AV51TFSolicitacoes_Objetivo_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E18AW2( )
      {
         /* Ddo_solicitacoes_usuario_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_solicitacoes_usuario_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 7;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_solicitacoes_usuario_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_solicitacoes_usuario_Internalname, "SortedStatus", Ddo_solicitacoes_usuario_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_solicitacoes_usuario_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 7;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_solicitacoes_usuario_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_solicitacoes_usuario_Internalname, "SortedStatus", Ddo_solicitacoes_usuario_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_solicitacoes_usuario_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV54TFSolicitacoes_Usuario = (int)(NumberUtil.Val( Ddo_solicitacoes_usuario_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54TFSolicitacoes_Usuario", StringUtil.LTrim( StringUtil.Str( (decimal)(AV54TFSolicitacoes_Usuario), 6, 0)));
            AV55TFSolicitacoes_Usuario_To = (int)(NumberUtil.Val( Ddo_solicitacoes_usuario_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55TFSolicitacoes_Usuario_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV55TFSolicitacoes_Usuario_To), 6, 0)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E19AW2( )
      {
         /* Ddo_solicitacoes_data_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_solicitacoes_data_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 8;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_solicitacoes_data_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_solicitacoes_data_Internalname, "SortedStatus", Ddo_solicitacoes_data_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_solicitacoes_data_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 8;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_solicitacoes_data_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_solicitacoes_data_Internalname, "SortedStatus", Ddo_solicitacoes_data_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_solicitacoes_data_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV58TFSolicitacoes_Data = context.localUtil.CToT( Ddo_solicitacoes_data_Filteredtext_get, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58TFSolicitacoes_Data", context.localUtil.TToC( AV58TFSolicitacoes_Data, 8, 5, 0, 3, "/", ":", " "));
            AV59TFSolicitacoes_Data_To = context.localUtil.CToT( Ddo_solicitacoes_data_Filteredtextto_get, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59TFSolicitacoes_Data_To", context.localUtil.TToC( AV59TFSolicitacoes_Data_To, 8, 5, 0, 3, "/", ":", " "));
            if ( ! (DateTime.MinValue==AV59TFSolicitacoes_Data_To) )
            {
               AV59TFSolicitacoes_Data_To = context.localUtil.YMDHMSToT( (short)(DateTimeUtil.Year( AV59TFSolicitacoes_Data_To)), (short)(DateTimeUtil.Month( AV59TFSolicitacoes_Data_To)), (short)(DateTimeUtil.Day( AV59TFSolicitacoes_Data_To)), 23, 59, 59);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59TFSolicitacoes_Data_To", context.localUtil.TToC( AV59TFSolicitacoes_Data_To, 8, 5, 0, 3, "/", ":", " "));
            }
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E20AW2( )
      {
         /* Ddo_solicitacoes_usuario_ult_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_solicitacoes_usuario_ult_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 9;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_solicitacoes_usuario_ult_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_solicitacoes_usuario_ult_Internalname, "SortedStatus", Ddo_solicitacoes_usuario_ult_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_solicitacoes_usuario_ult_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 9;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_solicitacoes_usuario_ult_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_solicitacoes_usuario_ult_Internalname, "SortedStatus", Ddo_solicitacoes_usuario_ult_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_solicitacoes_usuario_ult_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV64TFSolicitacoes_Usuario_Ult = (int)(NumberUtil.Val( Ddo_solicitacoes_usuario_ult_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64TFSolicitacoes_Usuario_Ult", StringUtil.LTrim( StringUtil.Str( (decimal)(AV64TFSolicitacoes_Usuario_Ult), 6, 0)));
            AV65TFSolicitacoes_Usuario_Ult_To = (int)(NumberUtil.Val( Ddo_solicitacoes_usuario_ult_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65TFSolicitacoes_Usuario_Ult_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV65TFSolicitacoes_Usuario_Ult_To), 6, 0)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E21AW2( )
      {
         /* Ddo_solicitacoes_data_ult_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_solicitacoes_data_ult_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 10;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_solicitacoes_data_ult_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_solicitacoes_data_ult_Internalname, "SortedStatus", Ddo_solicitacoes_data_ult_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_solicitacoes_data_ult_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 10;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_solicitacoes_data_ult_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_solicitacoes_data_ult_Internalname, "SortedStatus", Ddo_solicitacoes_data_ult_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_solicitacoes_data_ult_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV68TFSolicitacoes_Data_Ult = context.localUtil.CToT( Ddo_solicitacoes_data_ult_Filteredtext_get, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV68TFSolicitacoes_Data_Ult", context.localUtil.TToC( AV68TFSolicitacoes_Data_Ult, 8, 5, 0, 3, "/", ":", " "));
            AV69TFSolicitacoes_Data_Ult_To = context.localUtil.CToT( Ddo_solicitacoes_data_ult_Filteredtextto_get, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69TFSolicitacoes_Data_Ult_To", context.localUtil.TToC( AV69TFSolicitacoes_Data_Ult_To, 8, 5, 0, 3, "/", ":", " "));
            if ( ! (DateTime.MinValue==AV69TFSolicitacoes_Data_Ult_To) )
            {
               AV69TFSolicitacoes_Data_Ult_To = context.localUtil.YMDHMSToT( (short)(DateTimeUtil.Year( AV69TFSolicitacoes_Data_Ult_To)), (short)(DateTimeUtil.Month( AV69TFSolicitacoes_Data_Ult_To)), (short)(DateTimeUtil.Day( AV69TFSolicitacoes_Data_Ult_To)), 23, 59, 59);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69TFSolicitacoes_Data_Ult_To", context.localUtil.TToC( AV69TFSolicitacoes_Data_Ult_To, 8, 5, 0, 3, "/", ":", " "));
            }
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E22AW2( )
      {
         /* Ddo_solicitacoes_status_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_solicitacoes_status_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 11;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_solicitacoes_status_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_solicitacoes_status_Internalname, "SortedStatus", Ddo_solicitacoes_status_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_solicitacoes_status_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 11;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_solicitacoes_status_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_solicitacoes_status_Internalname, "SortedStatus", Ddo_solicitacoes_status_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_solicitacoes_status_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV74TFSolicitacoes_Status_SelsJson = Ddo_solicitacoes_status_Selectedvalue_get;
            AV75TFSolicitacoes_Status_Sels.FromJSonString(AV74TFSolicitacoes_Status_SelsJson);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV75TFSolicitacoes_Status_Sels", AV75TFSolicitacoes_Status_Sels);
      }

      private void E35AW2( )
      {
         /* Grid_Load Routine */
         AV28Select = context.GetImagePath( "3914535b-0c03-44c5-9538-906a99cdd2bc", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavSelect_Internalname, AV28Select);
         AV83Select_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "3914535b-0c03-44c5-9538-906a99cdd2bc", "", context.GetTheme( )));
         edtavSelect_Tooltiptext = "Selecionar";
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 80;
         }
         sendrow_802( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_80_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(80, GridRow);
         }
      }

      public void GXEnter( )
      {
         /* Execute user event: E36AW2 */
         E36AW2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E36AW2( )
      {
         /* Enter Routine */
         AV7InOutSolicitacoes_Codigo = A439Solicitacoes_Codigo;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InOutSolicitacoes_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7InOutSolicitacoes_Codigo), 6, 0)));
         AV8InOutSolicitacoes_Novo_Projeto = A440Solicitacoes_Novo_Projeto;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8InOutSolicitacoes_Novo_Projeto", AV8InOutSolicitacoes_Novo_Projeto);
         context.setWebReturnParms(new Object[] {(int)AV7InOutSolicitacoes_Codigo,(String)AV8InOutSolicitacoes_Novo_Projeto});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void E23AW2( )
      {
         /* Orderedby_Click Routine */
         context.DoAjaxRefresh();
      }

      protected void E28AW2( )
      {
         /* 'AddDynamicFilters1' Routine */
         AV18DynamicFiltersEnabled2 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         imgAdddynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
      }

      protected void E24AW2( )
      {
         /* 'RemoveDynamicFilters1' Routine */
         AV26DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV27DynamicFiltersIgnoreFirst = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersIgnoreFirst", AV27DynamicFiltersIgnoreFirst);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV27DynamicFiltersIgnoreFirst = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersIgnoreFirst", AV27DynamicFiltersIgnoreFirst);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17Solicitacoes_Novo_Projeto1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21Solicitacoes_Novo_Projeto2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25Solicitacoes_Novo_Projeto3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV31TFSolicitacoes_Codigo, AV32TFSolicitacoes_Codigo_To, AV35TFContratada_Codigo, AV36TFContratada_Codigo_To, AV39TFSistema_Codigo, AV40TFSistema_Codigo_To, AV43TFServico_Codigo, AV44TFServico_Codigo_To, AV47TFSolicitacoes_Novo_Projeto_Sel, AV50TFSolicitacoes_Objetivo, AV51TFSolicitacoes_Objetivo_Sel, AV54TFSolicitacoes_Usuario, AV55TFSolicitacoes_Usuario_To, AV58TFSolicitacoes_Data, AV59TFSolicitacoes_Data_To, AV64TFSolicitacoes_Usuario_Ult, AV65TFSolicitacoes_Usuario_Ult_To, AV68TFSolicitacoes_Data_Ult, AV69TFSolicitacoes_Data_Ult_To, AV33ddo_Solicitacoes_CodigoTitleControlIdToReplace, AV37ddo_Contratada_CodigoTitleControlIdToReplace, AV41ddo_Sistema_CodigoTitleControlIdToReplace, AV45ddo_Servico_CodigoTitleControlIdToReplace, AV48ddo_Solicitacoes_Novo_ProjetoTitleControlIdToReplace, AV52ddo_Solicitacoes_ObjetivoTitleControlIdToReplace, AV56ddo_Solicitacoes_UsuarioTitleControlIdToReplace, AV62ddo_Solicitacoes_DataTitleControlIdToReplace, AV66ddo_Solicitacoes_Usuario_UltTitleControlIdToReplace, AV72ddo_Solicitacoes_Data_UltTitleControlIdToReplace, AV76ddo_Solicitacoes_StatusTitleControlIdToReplace, AV75TFSolicitacoes_Status_Sels, AV84Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         radavSolicitacoes_novo_projeto2.CurrentValue = StringUtil.RTrim( AV21Solicitacoes_Novo_Projeto2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, radavSolicitacoes_novo_projeto2_Internalname, "Values", radavSolicitacoes_novo_projeto2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         radavSolicitacoes_novo_projeto3.CurrentValue = StringUtil.RTrim( AV25Solicitacoes_Novo_Projeto3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, radavSolicitacoes_novo_projeto3_Internalname, "Values", radavSolicitacoes_novo_projeto3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         radavSolicitacoes_novo_projeto1.CurrentValue = StringUtil.RTrim( AV17Solicitacoes_Novo_Projeto1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, radavSolicitacoes_novo_projeto1_Internalname, "Values", radavSolicitacoes_novo_projeto1.ToJavascriptSource());
      }

      protected void E29AW2( )
      {
         /* Dynamicfiltersselector1_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E30AW2( )
      {
         /* 'AddDynamicFilters2' Routine */
         AV22DynamicFiltersEnabled3 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
         imgAdddynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
      }

      protected void E25AW2( )
      {
         /* 'RemoveDynamicFilters2' Routine */
         AV26DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV18DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17Solicitacoes_Novo_Projeto1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21Solicitacoes_Novo_Projeto2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25Solicitacoes_Novo_Projeto3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV31TFSolicitacoes_Codigo, AV32TFSolicitacoes_Codigo_To, AV35TFContratada_Codigo, AV36TFContratada_Codigo_To, AV39TFSistema_Codigo, AV40TFSistema_Codigo_To, AV43TFServico_Codigo, AV44TFServico_Codigo_To, AV47TFSolicitacoes_Novo_Projeto_Sel, AV50TFSolicitacoes_Objetivo, AV51TFSolicitacoes_Objetivo_Sel, AV54TFSolicitacoes_Usuario, AV55TFSolicitacoes_Usuario_To, AV58TFSolicitacoes_Data, AV59TFSolicitacoes_Data_To, AV64TFSolicitacoes_Usuario_Ult, AV65TFSolicitacoes_Usuario_Ult_To, AV68TFSolicitacoes_Data_Ult, AV69TFSolicitacoes_Data_Ult_To, AV33ddo_Solicitacoes_CodigoTitleControlIdToReplace, AV37ddo_Contratada_CodigoTitleControlIdToReplace, AV41ddo_Sistema_CodigoTitleControlIdToReplace, AV45ddo_Servico_CodigoTitleControlIdToReplace, AV48ddo_Solicitacoes_Novo_ProjetoTitleControlIdToReplace, AV52ddo_Solicitacoes_ObjetivoTitleControlIdToReplace, AV56ddo_Solicitacoes_UsuarioTitleControlIdToReplace, AV62ddo_Solicitacoes_DataTitleControlIdToReplace, AV66ddo_Solicitacoes_Usuario_UltTitleControlIdToReplace, AV72ddo_Solicitacoes_Data_UltTitleControlIdToReplace, AV76ddo_Solicitacoes_StatusTitleControlIdToReplace, AV75TFSolicitacoes_Status_Sels, AV84Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         radavSolicitacoes_novo_projeto2.CurrentValue = StringUtil.RTrim( AV21Solicitacoes_Novo_Projeto2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, radavSolicitacoes_novo_projeto2_Internalname, "Values", radavSolicitacoes_novo_projeto2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         radavSolicitacoes_novo_projeto3.CurrentValue = StringUtil.RTrim( AV25Solicitacoes_Novo_Projeto3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, radavSolicitacoes_novo_projeto3_Internalname, "Values", radavSolicitacoes_novo_projeto3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         radavSolicitacoes_novo_projeto1.CurrentValue = StringUtil.RTrim( AV17Solicitacoes_Novo_Projeto1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, radavSolicitacoes_novo_projeto1_Internalname, "Values", radavSolicitacoes_novo_projeto1.ToJavascriptSource());
      }

      protected void E31AW2( )
      {
         /* Dynamicfiltersselector2_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E26AW2( )
      {
         /* 'RemoveDynamicFilters3' Routine */
         AV26DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV22DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16DynamicFiltersOperator1, AV17Solicitacoes_Novo_Projeto1, AV19DynamicFiltersSelector2, AV20DynamicFiltersOperator2, AV21Solicitacoes_Novo_Projeto2, AV23DynamicFiltersSelector3, AV24DynamicFiltersOperator3, AV25Solicitacoes_Novo_Projeto3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV31TFSolicitacoes_Codigo, AV32TFSolicitacoes_Codigo_To, AV35TFContratada_Codigo, AV36TFContratada_Codigo_To, AV39TFSistema_Codigo, AV40TFSistema_Codigo_To, AV43TFServico_Codigo, AV44TFServico_Codigo_To, AV47TFSolicitacoes_Novo_Projeto_Sel, AV50TFSolicitacoes_Objetivo, AV51TFSolicitacoes_Objetivo_Sel, AV54TFSolicitacoes_Usuario, AV55TFSolicitacoes_Usuario_To, AV58TFSolicitacoes_Data, AV59TFSolicitacoes_Data_To, AV64TFSolicitacoes_Usuario_Ult, AV65TFSolicitacoes_Usuario_Ult_To, AV68TFSolicitacoes_Data_Ult, AV69TFSolicitacoes_Data_Ult_To, AV33ddo_Solicitacoes_CodigoTitleControlIdToReplace, AV37ddo_Contratada_CodigoTitleControlIdToReplace, AV41ddo_Sistema_CodigoTitleControlIdToReplace, AV45ddo_Servico_CodigoTitleControlIdToReplace, AV48ddo_Solicitacoes_Novo_ProjetoTitleControlIdToReplace, AV52ddo_Solicitacoes_ObjetivoTitleControlIdToReplace, AV56ddo_Solicitacoes_UsuarioTitleControlIdToReplace, AV62ddo_Solicitacoes_DataTitleControlIdToReplace, AV66ddo_Solicitacoes_Usuario_UltTitleControlIdToReplace, AV72ddo_Solicitacoes_Data_UltTitleControlIdToReplace, AV76ddo_Solicitacoes_StatusTitleControlIdToReplace, AV75TFSolicitacoes_Status_Sels, AV84Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         radavSolicitacoes_novo_projeto2.CurrentValue = StringUtil.RTrim( AV21Solicitacoes_Novo_Projeto2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, radavSolicitacoes_novo_projeto2_Internalname, "Values", radavSolicitacoes_novo_projeto2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         radavSolicitacoes_novo_projeto3.CurrentValue = StringUtil.RTrim( AV25Solicitacoes_Novo_Projeto3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, radavSolicitacoes_novo_projeto3_Internalname, "Values", radavSolicitacoes_novo_projeto3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         radavSolicitacoes_novo_projeto1.CurrentValue = StringUtil.RTrim( AV17Solicitacoes_Novo_Projeto1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, radavSolicitacoes_novo_projeto1_Internalname, "Values", radavSolicitacoes_novo_projeto1.ToJavascriptSource());
      }

      protected void E32AW2( )
      {
         /* Dynamicfiltersselector3_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E27AW2( )
      {
         /* 'DoCleanFilters' Routine */
         /* Execute user subroutine: 'CLEANFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_firstpage( ) ;
         context.DoAjaxRefresh();
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV75TFSolicitacoes_Status_Sels", AV75TFSolicitacoes_Status_Sels);
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         radavSolicitacoes_novo_projeto1.CurrentValue = StringUtil.RTrim( AV17Solicitacoes_Novo_Projeto1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, radavSolicitacoes_novo_projeto1_Internalname, "Values", radavSolicitacoes_novo_projeto1.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         radavSolicitacoes_novo_projeto2.CurrentValue = StringUtil.RTrim( AV21Solicitacoes_Novo_Projeto2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, radavSolicitacoes_novo_projeto2_Internalname, "Values", radavSolicitacoes_novo_projeto2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         radavSolicitacoes_novo_projeto3.CurrentValue = StringUtil.RTrim( AV25Solicitacoes_Novo_Projeto3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, radavSolicitacoes_novo_projeto3_Internalname, "Values", radavSolicitacoes_novo_projeto3.ToJavascriptSource());
      }

      protected void S172( )
      {
         /* 'RESETDDOSORTEDSTATUS' Routine */
         Ddo_solicitacoes_codigo_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_solicitacoes_codigo_Internalname, "SortedStatus", Ddo_solicitacoes_codigo_Sortedstatus);
         Ddo_contratada_codigo_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_codigo_Internalname, "SortedStatus", Ddo_contratada_codigo_Sortedstatus);
         Ddo_sistema_codigo_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_sistema_codigo_Internalname, "SortedStatus", Ddo_sistema_codigo_Sortedstatus);
         Ddo_servico_codigo_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servico_codigo_Internalname, "SortedStatus", Ddo_servico_codigo_Sortedstatus);
         Ddo_solicitacoes_novo_projeto_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_solicitacoes_novo_projeto_Internalname, "SortedStatus", Ddo_solicitacoes_novo_projeto_Sortedstatus);
         Ddo_solicitacoes_objetivo_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_solicitacoes_objetivo_Internalname, "SortedStatus", Ddo_solicitacoes_objetivo_Sortedstatus);
         Ddo_solicitacoes_usuario_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_solicitacoes_usuario_Internalname, "SortedStatus", Ddo_solicitacoes_usuario_Sortedstatus);
         Ddo_solicitacoes_data_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_solicitacoes_data_Internalname, "SortedStatus", Ddo_solicitacoes_data_Sortedstatus);
         Ddo_solicitacoes_usuario_ult_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_solicitacoes_usuario_ult_Internalname, "SortedStatus", Ddo_solicitacoes_usuario_ult_Sortedstatus);
         Ddo_solicitacoes_data_ult_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_solicitacoes_data_ult_Internalname, "SortedStatus", Ddo_solicitacoes_data_ult_Sortedstatus);
         Ddo_solicitacoes_status_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_solicitacoes_status_Internalname, "SortedStatus", Ddo_solicitacoes_status_Sortedstatus);
      }

      protected void S152( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV13OrderedBy == 2 )
         {
            Ddo_solicitacoes_codigo_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_solicitacoes_codigo_Internalname, "SortedStatus", Ddo_solicitacoes_codigo_Sortedstatus);
         }
         else if ( AV13OrderedBy == 3 )
         {
            Ddo_contratada_codigo_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_codigo_Internalname, "SortedStatus", Ddo_contratada_codigo_Sortedstatus);
         }
         else if ( AV13OrderedBy == 4 )
         {
            Ddo_sistema_codigo_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_sistema_codigo_Internalname, "SortedStatus", Ddo_sistema_codigo_Sortedstatus);
         }
         else if ( AV13OrderedBy == 5 )
         {
            Ddo_servico_codigo_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servico_codigo_Internalname, "SortedStatus", Ddo_servico_codigo_Sortedstatus);
         }
         else if ( AV13OrderedBy == 1 )
         {
            Ddo_solicitacoes_novo_projeto_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_solicitacoes_novo_projeto_Internalname, "SortedStatus", Ddo_solicitacoes_novo_projeto_Sortedstatus);
         }
         else if ( AV13OrderedBy == 6 )
         {
            Ddo_solicitacoes_objetivo_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_solicitacoes_objetivo_Internalname, "SortedStatus", Ddo_solicitacoes_objetivo_Sortedstatus);
         }
         else if ( AV13OrderedBy == 7 )
         {
            Ddo_solicitacoes_usuario_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_solicitacoes_usuario_Internalname, "SortedStatus", Ddo_solicitacoes_usuario_Sortedstatus);
         }
         else if ( AV13OrderedBy == 8 )
         {
            Ddo_solicitacoes_data_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_solicitacoes_data_Internalname, "SortedStatus", Ddo_solicitacoes_data_Sortedstatus);
         }
         else if ( AV13OrderedBy == 9 )
         {
            Ddo_solicitacoes_usuario_ult_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_solicitacoes_usuario_ult_Internalname, "SortedStatus", Ddo_solicitacoes_usuario_ult_Sortedstatus);
         }
         else if ( AV13OrderedBy == 10 )
         {
            Ddo_solicitacoes_data_ult_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_solicitacoes_data_ult_Internalname, "SortedStatus", Ddo_solicitacoes_data_ult_Sortedstatus);
         }
         else if ( AV13OrderedBy == 11 )
         {
            Ddo_solicitacoes_status_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_solicitacoes_status_Internalname, "SortedStatus", Ddo_solicitacoes_status_Sortedstatus);
         }
      }

      protected void S112( )
      {
         /* 'ENABLEDYNAMICFILTERS1' Routine */
         radavSolicitacoes_novo_projeto1.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, radavSolicitacoes_novo_projeto1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(radavSolicitacoes_novo_projeto1.Visible), 5, 0)));
         cmbavDynamicfiltersoperator1.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "SOLICITACOES_NOVO_PROJETO") == 0 )
         {
            radavSolicitacoes_novo_projeto1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, radavSolicitacoes_novo_projeto1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(radavSolicitacoes_novo_projeto1.Visible), 5, 0)));
            cmbavDynamicfiltersoperator1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         }
      }

      protected void S122( )
      {
         /* 'ENABLEDYNAMICFILTERS2' Routine */
         radavSolicitacoes_novo_projeto2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, radavSolicitacoes_novo_projeto2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(radavSolicitacoes_novo_projeto2.Visible), 5, 0)));
         cmbavDynamicfiltersoperator2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "SOLICITACOES_NOVO_PROJETO") == 0 )
         {
            radavSolicitacoes_novo_projeto2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, radavSolicitacoes_novo_projeto2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(radavSolicitacoes_novo_projeto2.Visible), 5, 0)));
            cmbavDynamicfiltersoperator2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         }
      }

      protected void S132( )
      {
         /* 'ENABLEDYNAMICFILTERS3' Routine */
         radavSolicitacoes_novo_projeto3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, radavSolicitacoes_novo_projeto3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(radavSolicitacoes_novo_projeto3.Visible), 5, 0)));
         cmbavDynamicfiltersoperator3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "SOLICITACOES_NOVO_PROJETO") == 0 )
         {
            radavSolicitacoes_novo_projeto3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, radavSolicitacoes_novo_projeto3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(radavSolicitacoes_novo_projeto3.Visible), 5, 0)));
            cmbavDynamicfiltersoperator3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         }
      }

      protected void S192( )
      {
         /* 'RESETDYNFILTERS' Routine */
         AV18DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         AV19DynamicFiltersSelector2 = "SOLICITACOES_NOVO_PROJETO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         AV20DynamicFiltersOperator2 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)));
         AV21Solicitacoes_Novo_Projeto2 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Solicitacoes_Novo_Projeto2", AV21Solicitacoes_Novo_Projeto2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV22DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
         AV23DynamicFiltersSelector3 = "SOLICITACOES_NOVO_PROJETO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
         AV24DynamicFiltersOperator3 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)));
         AV25Solicitacoes_Novo_Projeto3 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25Solicitacoes_Novo_Projeto3", AV25Solicitacoes_Novo_Projeto3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S202( )
      {
         /* 'CLEANFILTERS' Routine */
         AV31TFSolicitacoes_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31TFSolicitacoes_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV31TFSolicitacoes_Codigo), 6, 0)));
         Ddo_solicitacoes_codigo_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_solicitacoes_codigo_Internalname, "FilteredText_set", Ddo_solicitacoes_codigo_Filteredtext_set);
         AV32TFSolicitacoes_Codigo_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32TFSolicitacoes_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV32TFSolicitacoes_Codigo_To), 6, 0)));
         Ddo_solicitacoes_codigo_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_solicitacoes_codigo_Internalname, "FilteredTextTo_set", Ddo_solicitacoes_codigo_Filteredtextto_set);
         AV35TFContratada_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TFContratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35TFContratada_Codigo), 6, 0)));
         Ddo_contratada_codigo_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_codigo_Internalname, "FilteredText_set", Ddo_contratada_codigo_Filteredtext_set);
         AV36TFContratada_Codigo_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36TFContratada_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36TFContratada_Codigo_To), 6, 0)));
         Ddo_contratada_codigo_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_contratada_codigo_Internalname, "FilteredTextTo_set", Ddo_contratada_codigo_Filteredtextto_set);
         AV39TFSistema_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFSistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV39TFSistema_Codigo), 6, 0)));
         Ddo_sistema_codigo_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_sistema_codigo_Internalname, "FilteredText_set", Ddo_sistema_codigo_Filteredtext_set);
         AV40TFSistema_Codigo_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40TFSistema_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV40TFSistema_Codigo_To), 6, 0)));
         Ddo_sistema_codigo_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_sistema_codigo_Internalname, "FilteredTextTo_set", Ddo_sistema_codigo_Filteredtextto_set);
         AV43TFServico_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43TFServico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV43TFServico_Codigo), 6, 0)));
         Ddo_servico_codigo_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servico_codigo_Internalname, "FilteredText_set", Ddo_servico_codigo_Filteredtext_set);
         AV44TFServico_Codigo_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44TFServico_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV44TFServico_Codigo_To), 6, 0)));
         Ddo_servico_codigo_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_servico_codigo_Internalname, "FilteredTextTo_set", Ddo_servico_codigo_Filteredtextto_set);
         AV47TFSolicitacoes_Novo_Projeto_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47TFSolicitacoes_Novo_Projeto_Sel", AV47TFSolicitacoes_Novo_Projeto_Sel);
         Ddo_solicitacoes_novo_projeto_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_solicitacoes_novo_projeto_Internalname, "SelectedValue_set", Ddo_solicitacoes_novo_projeto_Selectedvalue_set);
         AV50TFSolicitacoes_Objetivo = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50TFSolicitacoes_Objetivo", AV50TFSolicitacoes_Objetivo);
         Ddo_solicitacoes_objetivo_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_solicitacoes_objetivo_Internalname, "FilteredText_set", Ddo_solicitacoes_objetivo_Filteredtext_set);
         AV51TFSolicitacoes_Objetivo_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51TFSolicitacoes_Objetivo_Sel", AV51TFSolicitacoes_Objetivo_Sel);
         Ddo_solicitacoes_objetivo_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_solicitacoes_objetivo_Internalname, "SelectedValue_set", Ddo_solicitacoes_objetivo_Selectedvalue_set);
         AV54TFSolicitacoes_Usuario = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54TFSolicitacoes_Usuario", StringUtil.LTrim( StringUtil.Str( (decimal)(AV54TFSolicitacoes_Usuario), 6, 0)));
         Ddo_solicitacoes_usuario_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_solicitacoes_usuario_Internalname, "FilteredText_set", Ddo_solicitacoes_usuario_Filteredtext_set);
         AV55TFSolicitacoes_Usuario_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55TFSolicitacoes_Usuario_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV55TFSolicitacoes_Usuario_To), 6, 0)));
         Ddo_solicitacoes_usuario_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_solicitacoes_usuario_Internalname, "FilteredTextTo_set", Ddo_solicitacoes_usuario_Filteredtextto_set);
         AV58TFSolicitacoes_Data = (DateTime)(DateTime.MinValue);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58TFSolicitacoes_Data", context.localUtil.TToC( AV58TFSolicitacoes_Data, 8, 5, 0, 3, "/", ":", " "));
         Ddo_solicitacoes_data_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_solicitacoes_data_Internalname, "FilteredText_set", Ddo_solicitacoes_data_Filteredtext_set);
         AV59TFSolicitacoes_Data_To = (DateTime)(DateTime.MinValue);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59TFSolicitacoes_Data_To", context.localUtil.TToC( AV59TFSolicitacoes_Data_To, 8, 5, 0, 3, "/", ":", " "));
         Ddo_solicitacoes_data_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_solicitacoes_data_Internalname, "FilteredTextTo_set", Ddo_solicitacoes_data_Filteredtextto_set);
         AV64TFSolicitacoes_Usuario_Ult = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64TFSolicitacoes_Usuario_Ult", StringUtil.LTrim( StringUtil.Str( (decimal)(AV64TFSolicitacoes_Usuario_Ult), 6, 0)));
         Ddo_solicitacoes_usuario_ult_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_solicitacoes_usuario_ult_Internalname, "FilteredText_set", Ddo_solicitacoes_usuario_ult_Filteredtext_set);
         AV65TFSolicitacoes_Usuario_Ult_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65TFSolicitacoes_Usuario_Ult_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV65TFSolicitacoes_Usuario_Ult_To), 6, 0)));
         Ddo_solicitacoes_usuario_ult_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_solicitacoes_usuario_ult_Internalname, "FilteredTextTo_set", Ddo_solicitacoes_usuario_ult_Filteredtextto_set);
         AV68TFSolicitacoes_Data_Ult = (DateTime)(DateTime.MinValue);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV68TFSolicitacoes_Data_Ult", context.localUtil.TToC( AV68TFSolicitacoes_Data_Ult, 8, 5, 0, 3, "/", ":", " "));
         Ddo_solicitacoes_data_ult_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_solicitacoes_data_ult_Internalname, "FilteredText_set", Ddo_solicitacoes_data_ult_Filteredtext_set);
         AV69TFSolicitacoes_Data_Ult_To = (DateTime)(DateTime.MinValue);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69TFSolicitacoes_Data_Ult_To", context.localUtil.TToC( AV69TFSolicitacoes_Data_Ult_To, 8, 5, 0, 3, "/", ":", " "));
         Ddo_solicitacoes_data_ult_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_solicitacoes_data_ult_Internalname, "FilteredTextTo_set", Ddo_solicitacoes_data_ult_Filteredtextto_set);
         AV75TFSolicitacoes_Status_Sels = (IGxCollection)(new GxSimpleCollection());
         Ddo_solicitacoes_status_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_solicitacoes_status_Internalname, "SelectedValue_set", Ddo_solicitacoes_status_Selectedvalue_set);
         AV15DynamicFiltersSelector1 = "SOLICITACOES_NOVO_PROJETO";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         AV16DynamicFiltersOperator1 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
         AV17Solicitacoes_Novo_Projeto1 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Solicitacoes_Novo_Projeto1", AV17Solicitacoes_Novo_Projeto1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S142( )
      {
         /* 'LOADDYNFILTERSSTATE' Routine */
         imgAdddynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         imgAdddynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
         if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(1));
            AV15DynamicFiltersSelector1 = AV12GridStateDynamicFilter.gxTpr_Selected;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "SOLICITACOES_NOVO_PROJETO") == 0 )
            {
               AV16DynamicFiltersOperator1 = AV12GridStateDynamicFilter.gxTpr_Operator;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)));
               AV17Solicitacoes_Novo_Projeto1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Solicitacoes_Novo_Projeto1", AV17Solicitacoes_Novo_Projeto1);
            }
            /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               lblJsdynamicfilters_Caption = "<script type=\"text/javascript\">";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(2);";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               imgAdddynamicfilters1_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
               imgRemovedynamicfilters1_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
               AV18DynamicFiltersEnabled2 = true;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
               AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(2));
               AV19DynamicFiltersSelector2 = AV12GridStateDynamicFilter.gxTpr_Selected;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
               if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "SOLICITACOES_NOVO_PROJETO") == 0 )
               {
                  AV20DynamicFiltersOperator2 = AV12GridStateDynamicFilter.gxTpr_Operator;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)));
                  AV21Solicitacoes_Novo_Projeto2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Solicitacoes_Novo_Projeto2", AV21Solicitacoes_Novo_Projeto2);
               }
               /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
               S122 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(3);";
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
                  imgAdddynamicfilters2_Visible = 0;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
                  imgRemovedynamicfilters2_Visible = 1;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
                  AV22DynamicFiltersEnabled3 = true;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
                  AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(3));
                  AV23DynamicFiltersSelector3 = AV12GridStateDynamicFilter.gxTpr_Selected;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
                  if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "SOLICITACOES_NOVO_PROJETO") == 0 )
                  {
                     AV24DynamicFiltersOperator3 = AV12GridStateDynamicFilter.gxTpr_Operator;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)));
                     AV25Solicitacoes_Novo_Projeto3 = AV12GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25Solicitacoes_Novo_Projeto3", AV25Solicitacoes_Novo_Projeto3);
                  }
                  /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
                  S132 ();
                  if ( returnInSub )
                  {
                     returnInSub = true;
                     if (true) return;
                  }
               }
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"</script>";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
            }
         }
         if ( AV26DynamicFiltersRemoving )
         {
            lblJsdynamicfilters_Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         }
      }

      protected void S162( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV10GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV10GridState.gxTpr_Orderedby = AV13OrderedBy;
         AV10GridState.gxTpr_Ordereddsc = AV14OrderedDsc;
         AV10GridState.gxTpr_Filtervalues.Clear();
         if ( ! ( (0==AV31TFSolicitacoes_Codigo) && (0==AV32TFSolicitacoes_Codigo_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFSOLICITACOES_CODIGO";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV31TFSolicitacoes_Codigo), 6, 0);
            AV11GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( (decimal)(AV32TFSolicitacoes_Codigo_To), 6, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (0==AV35TFContratada_Codigo) && (0==AV36TFContratada_Codigo_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFCONTRATADA_CODIGO";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV35TFContratada_Codigo), 6, 0);
            AV11GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( (decimal)(AV36TFContratada_Codigo_To), 6, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (0==AV39TFSistema_Codigo) && (0==AV40TFSistema_Codigo_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFSISTEMA_CODIGO";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV39TFSistema_Codigo), 6, 0);
            AV11GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( (decimal)(AV40TFSistema_Codigo_To), 6, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (0==AV43TFServico_Codigo) && (0==AV44TFServico_Codigo_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFSERVICO_CODIGO";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV43TFServico_Codigo), 6, 0);
            AV11GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( (decimal)(AV44TFServico_Codigo_To), 6, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV47TFSolicitacoes_Novo_Projeto_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFSOLICITACOES_NOVO_PROJETO_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV47TFSolicitacoes_Novo_Projeto_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV50TFSolicitacoes_Objetivo)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFSOLICITACOES_OBJETIVO";
            AV11GridStateFilterValue.gxTpr_Value = AV50TFSolicitacoes_Objetivo;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV51TFSolicitacoes_Objetivo_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFSOLICITACOES_OBJETIVO_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV51TFSolicitacoes_Objetivo_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (0==AV54TFSolicitacoes_Usuario) && (0==AV55TFSolicitacoes_Usuario_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFSOLICITACOES_USUARIO";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV54TFSolicitacoes_Usuario), 6, 0);
            AV11GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( (decimal)(AV55TFSolicitacoes_Usuario_To), 6, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (DateTime.MinValue==AV58TFSolicitacoes_Data) && (DateTime.MinValue==AV59TFSolicitacoes_Data_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFSOLICITACOES_DATA";
            AV11GridStateFilterValue.gxTpr_Value = context.localUtil.TToC( AV58TFSolicitacoes_Data, 8, 5, 0, 3, "/", ":", " ");
            AV11GridStateFilterValue.gxTpr_Valueto = context.localUtil.TToC( AV59TFSolicitacoes_Data_To, 8, 5, 0, 3, "/", ":", " ");
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (0==AV64TFSolicitacoes_Usuario_Ult) && (0==AV65TFSolicitacoes_Usuario_Ult_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFSOLICITACOES_USUARIO_ULT";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV64TFSolicitacoes_Usuario_Ult), 6, 0);
            AV11GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( (decimal)(AV65TFSolicitacoes_Usuario_Ult_To), 6, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (DateTime.MinValue==AV68TFSolicitacoes_Data_Ult) && (DateTime.MinValue==AV69TFSolicitacoes_Data_Ult_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFSOLICITACOES_DATA_ULT";
            AV11GridStateFilterValue.gxTpr_Value = context.localUtil.TToC( AV68TFSolicitacoes_Data_Ult, 8, 5, 0, 3, "/", ":", " ");
            AV11GridStateFilterValue.gxTpr_Valueto = context.localUtil.TToC( AV69TFSolicitacoes_Data_Ult_To, 8, 5, 0, 3, "/", ":", " ");
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( AV75TFSolicitacoes_Status_Sels.Count == 0 ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFSOLICITACOES_STATUS_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV75TFSolicitacoes_Status_Sels.ToJSonString(false);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         new wwpbaseobjects.savegridstate(context ).execute(  AV84Pgmname+"GridState",  AV10GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_MeetrikaVs3")) ;
      }

      protected void S182( )
      {
         /* 'SAVEDYNFILTERSSTATE' Routine */
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         if ( ! AV27DynamicFiltersIgnoreFirst )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV15DynamicFiltersSelector1;
            if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "SOLICITACOES_NOVO_PROJETO") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV17Solicitacoes_Novo_Projeto1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV17Solicitacoes_Novo_Projeto1;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV16DynamicFiltersOperator1;
            }
            if ( AV26DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV18DynamicFiltersEnabled2 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV19DynamicFiltersSelector2;
            if ( ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "SOLICITACOES_NOVO_PROJETO") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV21Solicitacoes_Novo_Projeto2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV21Solicitacoes_Novo_Projeto2;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV20DynamicFiltersOperator2;
            }
            if ( AV26DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV22DynamicFiltersEnabled3 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV23DynamicFiltersSelector3;
            if ( ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "SOLICITACOES_NOVO_PROJETO") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV25Solicitacoes_Novo_Projeto3)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV25Solicitacoes_Novo_Projeto3;
               AV12GridStateDynamicFilter.gxTpr_Operator = AV24DynamicFiltersOperator3;
            }
            if ( AV26DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
      }

      protected void wb_table1_2_AW2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableSearchCell'>") ;
            wb_table2_5_AW2( true) ;
         }
         else
         {
            wb_table2_5_AW2( false) ;
         }
         return  ;
      }

      protected void wb_table2_5_AW2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_74_AW2( true) ;
         }
         else
         {
            wb_table3_74_AW2( false) ;
         }
         return  ;
      }

      protected void wb_table3_74_AW2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_AW2e( true) ;
         }
         else
         {
            wb_table1_2_AW2e( false) ;
         }
      }

      protected void wb_table3_74_AW2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "Table100x100", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_77_AW2( true) ;
         }
         else
         {
            wb_table4_77_AW2( false) ;
         }
         return  ;
      }

      protected void wb_table4_77_AW2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_74_AW2e( true) ;
         }
         else
         {
            wb_table3_74_AW2e( false) ;
         }
      }

      protected void wb_table4_77_AW2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"DivS\" data-gxgridid=\"80\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtSolicitacoes_Codigo_Titleformat == 0 )
               {
                  context.SendWebValue( edtSolicitacoes_Codigo_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtSolicitacoes_Codigo_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratada_Codigo_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratada_Codigo_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratada_Codigo_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtSistema_Codigo_Titleformat == 0 )
               {
                  context.SendWebValue( edtSistema_Codigo_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtSistema_Codigo_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtServico_Codigo_Titleformat == 0 )
               {
                  context.SendWebValue( edtServico_Codigo_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtServico_Codigo_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( radSolicitacoes_Novo_Projeto_Titleformat == 0 )
               {
                  context.SendWebValue( radSolicitacoes_Novo_Projeto.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( radSolicitacoes_Novo_Projeto.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtSolicitacoes_Objetivo_Titleformat == 0 )
               {
                  context.SendWebValue( edtSolicitacoes_Objetivo_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtSolicitacoes_Objetivo_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtSolicitacoes_Usuario_Titleformat == 0 )
               {
                  context.SendWebValue( edtSolicitacoes_Usuario_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtSolicitacoes_Usuario_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtSolicitacoes_Data_Titleformat == 0 )
               {
                  context.SendWebValue( edtSolicitacoes_Data_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtSolicitacoes_Data_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtSolicitacoes_Usuario_Ult_Titleformat == 0 )
               {
                  context.SendWebValue( edtSolicitacoes_Usuario_Ult_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtSolicitacoes_Usuario_Ult_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtSolicitacoes_Data_Ult_Titleformat == 0 )
               {
                  context.SendWebValue( edtSolicitacoes_Data_Ult_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtSolicitacoes_Data_Ult_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( cmbSolicitacoes_Status_Titleformat == 0 )
               {
                  context.SendWebValue( cmbSolicitacoes_Status.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( cmbSolicitacoes_Status.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", "");
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV28Select));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavSelect_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A439Solicitacoes_Codigo), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtSolicitacoes_Codigo_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtSolicitacoes_Codigo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A39Contratada_Codigo), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratada_Codigo_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratada_Codigo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A127Sistema_Codigo), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtSistema_Codigo_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtSistema_Codigo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A155Servico_Codigo), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtServico_Codigo_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtServico_Codigo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A440Solicitacoes_Novo_Projeto));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( radSolicitacoes_Novo_Projeto.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(radSolicitacoes_Novo_Projeto_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A441Solicitacoes_Objetivo);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtSolicitacoes_Objetivo_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtSolicitacoes_Objetivo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A442Solicitacoes_Usuario), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtSolicitacoes_Usuario_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtSolicitacoes_Usuario_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.localUtil.TToC( A444Solicitacoes_Data, 10, 8, 0, 3, "/", ":", " "));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtSolicitacoes_Data_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtSolicitacoes_Data_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A443Solicitacoes_Usuario_Ult), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtSolicitacoes_Usuario_Ult_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtSolicitacoes_Usuario_Ult_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.localUtil.TToC( A445Solicitacoes_Data_Ult, 10, 8, 0, 3, "/", ":", " "));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtSolicitacoes_Data_Ult_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtSolicitacoes_Data_Ult_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A446Solicitacoes_Status));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( cmbSolicitacoes_Status.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbSolicitacoes_Status_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 80 )
         {
            wbEnd = 0;
            nRC_GXsfl_80 = (short)(nGXsfl_80_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_77_AW2e( true) ;
         }
         else
         {
            wb_table4_77_AW2e( false) ;
         }
      }

      protected void wb_table2_5_AW2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablesearch_Internalname, tblTablesearch_Internalname, "", "TableSearch", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='OrderedByDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblOrderedtext_Internalname, "Ordenado por", "", "", lblOrderedtext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "OrderedByDescription", 0, "", 1, 1, 0, "HLP_PromptSolicitacoes.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 10,'',false,'" + sGXsfl_80_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavOrderedby, cmbavOrderedby_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)), 1, cmbavOrderedby_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVORDEREDBY.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,10);\"", "", true, "HLP_PromptSolicitacoes.htm");
            cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", (String)(cmbavOrderedby.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 11,'',false,'" + sGXsfl_80_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV14OrderedDsc), StringUtil.BoolToStr( AV14OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,11);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_PromptSolicitacoes.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableFiltersCell'>") ;
            wb_table5_14_AW2( true) ;
         }
         else
         {
            wb_table5_14_AW2( false) ;
         }
         return  ;
      }

      protected void wb_table5_14_AW2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_AW2e( true) ;
         }
         else
         {
            wb_table2_5_AW2e( false) ;
         }
      }

      protected void wb_table5_14_AW2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='GridHeaderCellCleanFilters'>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 17,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgCleanfilters_Internalname, context.GetImagePath( "effedd9a-25cf-4497-952f-5a5ac032b8be", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Limpar filtros", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgCleanfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOCLEANFILTERS\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptSolicitacoes.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataFilterContentCell'>") ;
            wb_table6_19_AW2( true) ;
         }
         else
         {
            wb_table6_19_AW2( false) ;
         }
         return  ;
      }

      protected void wb_table6_19_AW2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblJsdynamicfilters_Internalname, lblJsdynamicfilters_Caption, "", "", lblJsdynamicfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "", 0, "", 1, 1, 1, "HLP_PromptSolicitacoes.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_14_AW2e( true) ;
         }
         else
         {
            wb_table5_14_AW2e( false) ;
         }
      }

      protected void wb_table6_19_AW2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabledynamicfilters_Internalname, tblTabledynamicfilters_Internalname, "", "TableDynamicFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix1_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptSolicitacoes.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 24,'',false,'" + sGXsfl_80_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector1, cmbavDynamicfiltersselector1_Internalname, StringUtil.RTrim( AV15DynamicFiltersSelector1), 1, cmbavDynamicfiltersselector1_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR1.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,24);\"", "", true, "HLP_PromptSolicitacoes.htm");
            cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", (String)(cmbavDynamicfiltersselector1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle1_Internalname, "valor", "", "", lblDynamicfiltersmiddle1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptSolicitacoes.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table7_28_AW2( true) ;
         }
         else
         {
            wb_table7_28_AW2( false) ;
         }
         return  ;
      }

      protected void wb_table7_28_AW2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 35,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters1_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters1_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptSolicitacoes.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 36,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters1_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters1_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptSolicitacoes.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow2\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix2_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptSolicitacoes.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 41,'',false,'" + sGXsfl_80_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector2, cmbavDynamicfiltersselector2_Internalname, StringUtil.RTrim( AV19DynamicFiltersSelector2), 1, cmbavDynamicfiltersselector2_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR2.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,41);\"", "", true, "HLP_PromptSolicitacoes.htm");
            cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", (String)(cmbavDynamicfiltersselector2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle2_Internalname, "valor", "", "", lblDynamicfiltersmiddle2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptSolicitacoes.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table8_45_AW2( true) ;
         }
         else
         {
            wb_table8_45_AW2( false) ;
         }
         return  ;
      }

      protected void wb_table8_45_AW2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 52,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters2_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters2_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptSolicitacoes.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 53,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters2_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters2_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptSolicitacoes.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow3\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix3_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptSolicitacoes.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 58,'',false,'" + sGXsfl_80_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector3, cmbavDynamicfiltersselector3_Internalname, StringUtil.RTrim( AV23DynamicFiltersSelector3), 1, cmbavDynamicfiltersselector3_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR3.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,58);\"", "", true, "HLP_PromptSolicitacoes.htm");
            cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", (String)(cmbavDynamicfiltersselector3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle3_Internalname, "valor", "", "", lblDynamicfiltersmiddle3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptSolicitacoes.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table9_62_AW2( true) ;
         }
         else
         {
            wb_table9_62_AW2( false) ;
         }
         return  ;
      }

      protected void wb_table9_62_AW2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 69,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters3_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters3_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS3\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptSolicitacoes.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_19_AW2e( true) ;
         }
         else
         {
            wb_table6_19_AW2e( false) ;
         }
      }

      protected void wb_table9_62_AW2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters3_Internalname, tblTablemergeddynamicfilters3_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 65,'',false,'" + sGXsfl_80_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator3, cmbavDynamicfiltersoperator3_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0)), 1, cmbavDynamicfiltersoperator3_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator3.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,65);\"", "", true, "HLP_PromptSolicitacoes.htm");
            cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24DynamicFiltersOperator3), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", (String)(cmbavDynamicfiltersoperator3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Radio button */
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 67,'',false,'" + sGXsfl_80_idx + "',0)\"";
            GxWebStd.gx_radio_ctrl( context, radavSolicitacoes_novo_projeto3, radavSolicitacoes_novo_projeto3_Internalname, StringUtil.RTrim( AV25Solicitacoes_Novo_Projeto3), "", radavSolicitacoes_novo_projeto3.Visible, 1, 0, 0, StyleString, ClassString, "", 0, radavSolicitacoes_novo_projeto3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", TempTags+" onclick=\"gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,67);\"", "HLP_PromptSolicitacoes.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table9_62_AW2e( true) ;
         }
         else
         {
            wb_table9_62_AW2e( false) ;
         }
      }

      protected void wb_table8_45_AW2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters2_Internalname, tblTablemergeddynamicfilters2_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 48,'',false,'" + sGXsfl_80_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator2, cmbavDynamicfiltersoperator2_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0)), 1, cmbavDynamicfiltersoperator2_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator2.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,48);\"", "", true, "HLP_PromptSolicitacoes.htm");
            cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV20DynamicFiltersOperator2), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", (String)(cmbavDynamicfiltersoperator2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Radio button */
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 50,'',false,'" + sGXsfl_80_idx + "',0)\"";
            GxWebStd.gx_radio_ctrl( context, radavSolicitacoes_novo_projeto2, radavSolicitacoes_novo_projeto2_Internalname, StringUtil.RTrim( AV21Solicitacoes_Novo_Projeto2), "", radavSolicitacoes_novo_projeto2.Visible, 1, 0, 0, StyleString, ClassString, "", 0, radavSolicitacoes_novo_projeto2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", TempTags+" onclick=\"gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,50);\"", "HLP_PromptSolicitacoes.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table8_45_AW2e( true) ;
         }
         else
         {
            wb_table8_45_AW2e( false) ;
         }
      }

      protected void wb_table7_28_AW2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters1_Internalname, tblTablemergeddynamicfilters1_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 31,'',false,'" + sGXsfl_80_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator1, cmbavDynamicfiltersoperator1_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0)), 1, cmbavDynamicfiltersoperator1_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator1.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,31);\"", "", true, "HLP_PromptSolicitacoes.htm");
            cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV16DynamicFiltersOperator1), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", (String)(cmbavDynamicfiltersoperator1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Radio button */
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 33,'',false,'" + sGXsfl_80_idx + "',0)\"";
            GxWebStd.gx_radio_ctrl( context, radavSolicitacoes_novo_projeto1, radavSolicitacoes_novo_projeto1_Internalname, StringUtil.RTrim( AV17Solicitacoes_Novo_Projeto1), "", radavSolicitacoes_novo_projeto1.Visible, 1, 0, 0, StyleString, ClassString, "", 0, radavSolicitacoes_novo_projeto1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", TempTags+" onclick=\"gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,33);\"", "HLP_PromptSolicitacoes.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_28_AW2e( true) ;
         }
         else
         {
            wb_table7_28_AW2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV7InOutSolicitacoes_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InOutSolicitacoes_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7InOutSolicitacoes_Codigo), 6, 0)));
         AV8InOutSolicitacoes_Novo_Projeto = (String)getParm(obj,1);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8InOutSolicitacoes_Novo_Projeto", AV8InOutSolicitacoes_Novo_Projeto);
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAAW2( ) ;
         WSAW2( ) ;
         WEAW2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?2249787");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?202042823192282");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("promptsolicitacoes.js", "?202042823192283");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_802( )
      {
         edtavSelect_Internalname = "vSELECT_"+sGXsfl_80_idx;
         edtSolicitacoes_Codigo_Internalname = "SOLICITACOES_CODIGO_"+sGXsfl_80_idx;
         edtContratada_Codigo_Internalname = "CONTRATADA_CODIGO_"+sGXsfl_80_idx;
         edtSistema_Codigo_Internalname = "SISTEMA_CODIGO_"+sGXsfl_80_idx;
         edtServico_Codigo_Internalname = "SERVICO_CODIGO_"+sGXsfl_80_idx;
         radSolicitacoes_Novo_Projeto_Internalname = "SOLICITACOES_NOVO_PROJETO_"+sGXsfl_80_idx;
         edtSolicitacoes_Objetivo_Internalname = "SOLICITACOES_OBJETIVO_"+sGXsfl_80_idx;
         edtSolicitacoes_Usuario_Internalname = "SOLICITACOES_USUARIO_"+sGXsfl_80_idx;
         edtSolicitacoes_Data_Internalname = "SOLICITACOES_DATA_"+sGXsfl_80_idx;
         edtSolicitacoes_Usuario_Ult_Internalname = "SOLICITACOES_USUARIO_ULT_"+sGXsfl_80_idx;
         edtSolicitacoes_Data_Ult_Internalname = "SOLICITACOES_DATA_ULT_"+sGXsfl_80_idx;
         cmbSolicitacoes_Status_Internalname = "SOLICITACOES_STATUS_"+sGXsfl_80_idx;
      }

      protected void SubsflControlProps_fel_802( )
      {
         edtavSelect_Internalname = "vSELECT_"+sGXsfl_80_fel_idx;
         edtSolicitacoes_Codigo_Internalname = "SOLICITACOES_CODIGO_"+sGXsfl_80_fel_idx;
         edtContratada_Codigo_Internalname = "CONTRATADA_CODIGO_"+sGXsfl_80_fel_idx;
         edtSistema_Codigo_Internalname = "SISTEMA_CODIGO_"+sGXsfl_80_fel_idx;
         edtServico_Codigo_Internalname = "SERVICO_CODIGO_"+sGXsfl_80_fel_idx;
         radSolicitacoes_Novo_Projeto_Internalname = "SOLICITACOES_NOVO_PROJETO_"+sGXsfl_80_fel_idx;
         edtSolicitacoes_Objetivo_Internalname = "SOLICITACOES_OBJETIVO_"+sGXsfl_80_fel_idx;
         edtSolicitacoes_Usuario_Internalname = "SOLICITACOES_USUARIO_"+sGXsfl_80_fel_idx;
         edtSolicitacoes_Data_Internalname = "SOLICITACOES_DATA_"+sGXsfl_80_fel_idx;
         edtSolicitacoes_Usuario_Ult_Internalname = "SOLICITACOES_USUARIO_ULT_"+sGXsfl_80_fel_idx;
         edtSolicitacoes_Data_Ult_Internalname = "SOLICITACOES_DATA_ULT_"+sGXsfl_80_fel_idx;
         cmbSolicitacoes_Status_Internalname = "SOLICITACOES_STATUS_"+sGXsfl_80_fel_idx;
      }

      protected void sendrow_802( )
      {
         SubsflControlProps_802( ) ;
         WBAW0( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_80_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_80_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_80_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Active Bitmap Variable */
            TempTags = " " + ((edtavSelect_Enabled!=0)&&(edtavSelect_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 81,'',false,'',80)\"" : " ");
            ClassString = "Image";
            StyleString = "";
            AV28Select_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV28Select))&&String.IsNullOrEmpty(StringUtil.RTrim( AV83Select_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV28Select)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavSelect_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV28Select)) ? AV83Select_GXI : context.PathToRelativeUrl( AV28Select)),(String)"",(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavSelect_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)5,(String)edtavSelect_Jsonclick,"'"+""+"'"+",false,"+"'"+"EENTER."+sGXsfl_80_idx+"'",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)""+TempTags,(String)"",(String)"",(short)1,(bool)AV28Select_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtSolicitacoes_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A439Solicitacoes_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A439Solicitacoes_Codigo), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtSolicitacoes_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)80,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratada_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A39Contratada_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A39Contratada_Codigo), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratada_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)80,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtSistema_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A127Sistema_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A127Sistema_Codigo), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtSistema_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)80,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtServico_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A155Servico_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A155Servico_Codigo), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtServico_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)80,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Radio button */
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            if ( ( nGXsfl_80_idx == 1 ) && isAjaxCallMode( ) )
            {
               GXCCtl = "SOLICITACOES_NOVO_PROJETO_" + sGXsfl_80_idx;
               radSolicitacoes_Novo_Projeto.Name = GXCCtl;
               radSolicitacoes_Novo_Projeto.WebTags = "";
               radSolicitacoes_Novo_Projeto.addItem("1", "Sim", 0);
               radSolicitacoes_Novo_Projeto.addItem("0", "N�o", 0);
            }
            GridRow.AddColumnProperties("radio", 2, isAjaxCallMode( ), new Object[] {(GXRadio)radSolicitacoes_Novo_Projeto,(String)radSolicitacoes_Novo_Projeto_Internalname,StringUtil.RTrim( A440Solicitacoes_Novo_Projeto),(String)"",(short)-1,(short)0,(short)0,(short)0,(String)StyleString,(String)ClassString,(String)"",(short)0,(String)radSolicitacoes_Novo_Projeto_Jsonclick,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)""});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtSolicitacoes_Objetivo_Internalname,(String)A441Solicitacoes_Objetivo,(String)A441Solicitacoes_Objetivo,(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtSolicitacoes_Objetivo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(int)2097152,(short)0,(short)0,(short)80,(short)1,(short)0,(short)-1,(bool)true,(String)"",(String)"left",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtSolicitacoes_Usuario_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A442Solicitacoes_Usuario), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A442Solicitacoes_Usuario), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtSolicitacoes_Usuario_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)80,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtSolicitacoes_Data_Internalname,context.localUtil.TToC( A444Solicitacoes_Data, 10, 8, 0, 3, "/", ":", " "),context.localUtil.Format( A444Solicitacoes_Data, "99/99/99 99:99"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtSolicitacoes_Data_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)14,(short)0,(short)0,(short)80,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtSolicitacoes_Usuario_Ult_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A443Solicitacoes_Usuario_Ult), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A443Solicitacoes_Usuario_Ult), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtSolicitacoes_Usuario_Ult_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)80,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtSolicitacoes_Data_Ult_Internalname,context.localUtil.TToC( A445Solicitacoes_Data_Ult, 10, 8, 0, 3, "/", ":", " "),context.localUtil.Format( A445Solicitacoes_Data_Ult, "99/99/99 99:99"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtSolicitacoes_Data_Ult_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)14,(short)0,(short)0,(short)80,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            if ( ( nGXsfl_80_idx == 1 ) && isAjaxCallMode( ) )
            {
               GXCCtl = "SOLICITACOES_STATUS_" + sGXsfl_80_idx;
               cmbSolicitacoes_Status.Name = GXCCtl;
               cmbSolicitacoes_Status.WebTags = "";
               cmbSolicitacoes_Status.addItem("A", "Ativo", 0);
               cmbSolicitacoes_Status.addItem("E", "Excluido", 0);
               cmbSolicitacoes_Status.addItem("R", "Rascunho", 0);
               if ( cmbSolicitacoes_Status.ItemCount > 0 )
               {
                  A446Solicitacoes_Status = cmbSolicitacoes_Status.getValidValue(A446Solicitacoes_Status);
               }
            }
            /* ComboBox */
            GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbSolicitacoes_Status,(String)cmbSolicitacoes_Status_Internalname,StringUtil.RTrim( A446Solicitacoes_Status),(short)1,(String)cmbSolicitacoes_Status_Jsonclick,(short)0,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"char",(String)"",(short)-1,(short)0,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"BootstrapAttribute",(String)"",(String)"",(String)"",(bool)true});
            cmbSolicitacoes_Status.CurrentValue = StringUtil.RTrim( A446Solicitacoes_Status);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbSolicitacoes_Status_Internalname, "Values", (String)(cmbSolicitacoes_Status.ToJavascriptSource()));
            GxWebStd.gx_hidden_field( context, "gxhash_SOLICITACOES_CODIGO"+"_"+sGXsfl_80_idx, GetSecureSignedToken( sGXsfl_80_idx, context.localUtil.Format( (decimal)(A439Solicitacoes_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_CONTRATADA_CODIGO"+"_"+sGXsfl_80_idx, GetSecureSignedToken( sGXsfl_80_idx, context.localUtil.Format( (decimal)(A39Contratada_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_SISTEMA_CODIGO"+"_"+sGXsfl_80_idx, GetSecureSignedToken( sGXsfl_80_idx, context.localUtil.Format( (decimal)(A127Sistema_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_SERVICO_CODIGO"+"_"+sGXsfl_80_idx, GetSecureSignedToken( sGXsfl_80_idx, context.localUtil.Format( (decimal)(A155Servico_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_SOLICITACOES_NOVO_PROJETO"+"_"+sGXsfl_80_idx, GetSecureSignedToken( sGXsfl_80_idx, StringUtil.RTrim( context.localUtil.Format( A440Solicitacoes_Novo_Projeto, ""))));
            GxWebStd.gx_hidden_field( context, "gxhash_SOLICITACOES_OBJETIVO"+"_"+sGXsfl_80_idx, GetSecureSignedToken( sGXsfl_80_idx, A441Solicitacoes_Objetivo));
            GxWebStd.gx_hidden_field( context, "gxhash_SOLICITACOES_USUARIO"+"_"+sGXsfl_80_idx, GetSecureSignedToken( sGXsfl_80_idx, context.localUtil.Format( (decimal)(A442Solicitacoes_Usuario), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_SOLICITACOES_DATA"+"_"+sGXsfl_80_idx, GetSecureSignedToken( sGXsfl_80_idx, context.localUtil.Format( A444Solicitacoes_Data, "99/99/99 99:99")));
            GxWebStd.gx_hidden_field( context, "gxhash_SOLICITACOES_USUARIO_ULT"+"_"+sGXsfl_80_idx, GetSecureSignedToken( sGXsfl_80_idx, context.localUtil.Format( (decimal)(A443Solicitacoes_Usuario_Ult), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_SOLICITACOES_DATA_ULT"+"_"+sGXsfl_80_idx, GetSecureSignedToken( sGXsfl_80_idx, context.localUtil.Format( A445Solicitacoes_Data_Ult, "99/99/99 99:99")));
            GxWebStd.gx_hidden_field( context, "gxhash_SOLICITACOES_STATUS"+"_"+sGXsfl_80_idx, GetSecureSignedToken( sGXsfl_80_idx, StringUtil.RTrim( context.localUtil.Format( A446Solicitacoes_Status, ""))));
            GridContainer.AddRow(GridRow);
            nGXsfl_80_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_80_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_80_idx+1));
            sGXsfl_80_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_80_idx), 4, 0)), 4, "0");
            SubsflControlProps_802( ) ;
         }
         /* End function sendrow_802 */
      }

      protected void init_default_properties( )
      {
         lblOrderedtext_Internalname = "ORDEREDTEXT";
         cmbavOrderedby_Internalname = "vORDEREDBY";
         edtavOrdereddsc_Internalname = "vORDEREDDSC";
         imgCleanfilters_Internalname = "CLEANFILTERS";
         lblDynamicfiltersprefix1_Internalname = "DYNAMICFILTERSPREFIX1";
         cmbavDynamicfiltersselector1_Internalname = "vDYNAMICFILTERSSELECTOR1";
         lblDynamicfiltersmiddle1_Internalname = "DYNAMICFILTERSMIDDLE1";
         cmbavDynamicfiltersoperator1_Internalname = "vDYNAMICFILTERSOPERATOR1";
         radavSolicitacoes_novo_projeto1_Internalname = "vSOLICITACOES_NOVO_PROJETO1";
         tblTablemergeddynamicfilters1_Internalname = "TABLEMERGEDDYNAMICFILTERS1";
         imgAdddynamicfilters1_Internalname = "ADDDYNAMICFILTERS1";
         imgRemovedynamicfilters1_Internalname = "REMOVEDYNAMICFILTERS1";
         lblDynamicfiltersprefix2_Internalname = "DYNAMICFILTERSPREFIX2";
         cmbavDynamicfiltersselector2_Internalname = "vDYNAMICFILTERSSELECTOR2";
         lblDynamicfiltersmiddle2_Internalname = "DYNAMICFILTERSMIDDLE2";
         cmbavDynamicfiltersoperator2_Internalname = "vDYNAMICFILTERSOPERATOR2";
         radavSolicitacoes_novo_projeto2_Internalname = "vSOLICITACOES_NOVO_PROJETO2";
         tblTablemergeddynamicfilters2_Internalname = "TABLEMERGEDDYNAMICFILTERS2";
         imgAdddynamicfilters2_Internalname = "ADDDYNAMICFILTERS2";
         imgRemovedynamicfilters2_Internalname = "REMOVEDYNAMICFILTERS2";
         lblDynamicfiltersprefix3_Internalname = "DYNAMICFILTERSPREFIX3";
         cmbavDynamicfiltersselector3_Internalname = "vDYNAMICFILTERSSELECTOR3";
         lblDynamicfiltersmiddle3_Internalname = "DYNAMICFILTERSMIDDLE3";
         cmbavDynamicfiltersoperator3_Internalname = "vDYNAMICFILTERSOPERATOR3";
         radavSolicitacoes_novo_projeto3_Internalname = "vSOLICITACOES_NOVO_PROJETO3";
         tblTablemergeddynamicfilters3_Internalname = "TABLEMERGEDDYNAMICFILTERS3";
         imgRemovedynamicfilters3_Internalname = "REMOVEDYNAMICFILTERS3";
         tblTabledynamicfilters_Internalname = "TABLEDYNAMICFILTERS";
         lblJsdynamicfilters_Internalname = "JSDYNAMICFILTERS";
         tblTablefilters_Internalname = "TABLEFILTERS";
         tblTablesearch_Internalname = "TABLESEARCH";
         edtavSelect_Internalname = "vSELECT";
         edtSolicitacoes_Codigo_Internalname = "SOLICITACOES_CODIGO";
         edtContratada_Codigo_Internalname = "CONTRATADA_CODIGO";
         edtSistema_Codigo_Internalname = "SISTEMA_CODIGO";
         edtServico_Codigo_Internalname = "SERVICO_CODIGO";
         radSolicitacoes_Novo_Projeto_Internalname = "SOLICITACOES_NOVO_PROJETO";
         edtSolicitacoes_Objetivo_Internalname = "SOLICITACOES_OBJETIVO";
         edtSolicitacoes_Usuario_Internalname = "SOLICITACOES_USUARIO";
         edtSolicitacoes_Data_Internalname = "SOLICITACOES_DATA";
         edtSolicitacoes_Usuario_Ult_Internalname = "SOLICITACOES_USUARIO_ULT";
         edtSolicitacoes_Data_Ult_Internalname = "SOLICITACOES_DATA_ULT";
         cmbSolicitacoes_Status_Internalname = "SOLICITACOES_STATUS";
         Gridpaginationbar_Internalname = "GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = "GRIDTABLEWITHPAGINATIONBAR";
         tblTablegridheader_Internalname = "TABLEGRIDHEADER";
         tblTablemain_Internalname = "TABLEMAIN";
         Workwithplusutilities1_Internalname = "WORKWITHPLUSUTILITIES1";
         chkavDynamicfiltersenabled2_Internalname = "vDYNAMICFILTERSENABLED2";
         chkavDynamicfiltersenabled3_Internalname = "vDYNAMICFILTERSENABLED3";
         edtavTfsolicitacoes_codigo_Internalname = "vTFSOLICITACOES_CODIGO";
         edtavTfsolicitacoes_codigo_to_Internalname = "vTFSOLICITACOES_CODIGO_TO";
         edtavTfcontratada_codigo_Internalname = "vTFCONTRATADA_CODIGO";
         edtavTfcontratada_codigo_to_Internalname = "vTFCONTRATADA_CODIGO_TO";
         edtavTfsistema_codigo_Internalname = "vTFSISTEMA_CODIGO";
         edtavTfsistema_codigo_to_Internalname = "vTFSISTEMA_CODIGO_TO";
         edtavTfservico_codigo_Internalname = "vTFSERVICO_CODIGO";
         edtavTfservico_codigo_to_Internalname = "vTFSERVICO_CODIGO_TO";
         edtavTfsolicitacoes_novo_projeto_sel_Internalname = "vTFSOLICITACOES_NOVO_PROJETO_SEL";
         edtavTfsolicitacoes_objetivo_Internalname = "vTFSOLICITACOES_OBJETIVO";
         edtavTfsolicitacoes_objetivo_sel_Internalname = "vTFSOLICITACOES_OBJETIVO_SEL";
         edtavTfsolicitacoes_usuario_Internalname = "vTFSOLICITACOES_USUARIO";
         edtavTfsolicitacoes_usuario_to_Internalname = "vTFSOLICITACOES_USUARIO_TO";
         edtavTfsolicitacoes_data_Internalname = "vTFSOLICITACOES_DATA";
         edtavTfsolicitacoes_data_to_Internalname = "vTFSOLICITACOES_DATA_TO";
         edtavDdo_solicitacoes_dataauxdate_Internalname = "vDDO_SOLICITACOES_DATAAUXDATE";
         edtavDdo_solicitacoes_dataauxdateto_Internalname = "vDDO_SOLICITACOES_DATAAUXDATETO";
         divDdo_solicitacoes_dataauxdates_Internalname = "DDO_SOLICITACOES_DATAAUXDATES";
         edtavTfsolicitacoes_usuario_ult_Internalname = "vTFSOLICITACOES_USUARIO_ULT";
         edtavTfsolicitacoes_usuario_ult_to_Internalname = "vTFSOLICITACOES_USUARIO_ULT_TO";
         edtavTfsolicitacoes_data_ult_Internalname = "vTFSOLICITACOES_DATA_ULT";
         edtavTfsolicitacoes_data_ult_to_Internalname = "vTFSOLICITACOES_DATA_ULT_TO";
         edtavDdo_solicitacoes_data_ultauxdate_Internalname = "vDDO_SOLICITACOES_DATA_ULTAUXDATE";
         edtavDdo_solicitacoes_data_ultauxdateto_Internalname = "vDDO_SOLICITACOES_DATA_ULTAUXDATETO";
         divDdo_solicitacoes_data_ultauxdates_Internalname = "DDO_SOLICITACOES_DATA_ULTAUXDATES";
         Ddo_solicitacoes_codigo_Internalname = "DDO_SOLICITACOES_CODIGO";
         edtavDdo_solicitacoes_codigotitlecontrolidtoreplace_Internalname = "vDDO_SOLICITACOES_CODIGOTITLECONTROLIDTOREPLACE";
         Ddo_contratada_codigo_Internalname = "DDO_CONTRATADA_CODIGO";
         edtavDdo_contratada_codigotitlecontrolidtoreplace_Internalname = "vDDO_CONTRATADA_CODIGOTITLECONTROLIDTOREPLACE";
         Ddo_sistema_codigo_Internalname = "DDO_SISTEMA_CODIGO";
         edtavDdo_sistema_codigotitlecontrolidtoreplace_Internalname = "vDDO_SISTEMA_CODIGOTITLECONTROLIDTOREPLACE";
         Ddo_servico_codigo_Internalname = "DDO_SERVICO_CODIGO";
         edtavDdo_servico_codigotitlecontrolidtoreplace_Internalname = "vDDO_SERVICO_CODIGOTITLECONTROLIDTOREPLACE";
         Ddo_solicitacoes_novo_projeto_Internalname = "DDO_SOLICITACOES_NOVO_PROJETO";
         edtavDdo_solicitacoes_novo_projetotitlecontrolidtoreplace_Internalname = "vDDO_SOLICITACOES_NOVO_PROJETOTITLECONTROLIDTOREPLACE";
         Ddo_solicitacoes_objetivo_Internalname = "DDO_SOLICITACOES_OBJETIVO";
         edtavDdo_solicitacoes_objetivotitlecontrolidtoreplace_Internalname = "vDDO_SOLICITACOES_OBJETIVOTITLECONTROLIDTOREPLACE";
         Ddo_solicitacoes_usuario_Internalname = "DDO_SOLICITACOES_USUARIO";
         edtavDdo_solicitacoes_usuariotitlecontrolidtoreplace_Internalname = "vDDO_SOLICITACOES_USUARIOTITLECONTROLIDTOREPLACE";
         Ddo_solicitacoes_data_Internalname = "DDO_SOLICITACOES_DATA";
         edtavDdo_solicitacoes_datatitlecontrolidtoreplace_Internalname = "vDDO_SOLICITACOES_DATATITLECONTROLIDTOREPLACE";
         Ddo_solicitacoes_usuario_ult_Internalname = "DDO_SOLICITACOES_USUARIO_ULT";
         edtavDdo_solicitacoes_usuario_ulttitlecontrolidtoreplace_Internalname = "vDDO_SOLICITACOES_USUARIO_ULTTITLECONTROLIDTOREPLACE";
         Ddo_solicitacoes_data_ult_Internalname = "DDO_SOLICITACOES_DATA_ULT";
         edtavDdo_solicitacoes_data_ulttitlecontrolidtoreplace_Internalname = "vDDO_SOLICITACOES_DATA_ULTTITLECONTROLIDTOREPLACE";
         Ddo_solicitacoes_status_Internalname = "DDO_SOLICITACOES_STATUS";
         edtavDdo_solicitacoes_statustitlecontrolidtoreplace_Internalname = "vDDO_SOLICITACOES_STATUSTITLECONTROLIDTOREPLACE";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         cmbSolicitacoes_Status_Jsonclick = "";
         edtSolicitacoes_Data_Ult_Jsonclick = "";
         edtSolicitacoes_Usuario_Ult_Jsonclick = "";
         edtSolicitacoes_Data_Jsonclick = "";
         edtSolicitacoes_Usuario_Jsonclick = "";
         edtSolicitacoes_Objetivo_Jsonclick = "";
         radSolicitacoes_Novo_Projeto_Jsonclick = "";
         edtServico_Codigo_Jsonclick = "";
         edtSistema_Codigo_Jsonclick = "";
         edtContratada_Codigo_Jsonclick = "";
         edtSolicitacoes_Codigo_Jsonclick = "";
         edtavSelect_Jsonclick = "";
         edtavSelect_Visible = -1;
         edtavSelect_Enabled = 1;
         radavSolicitacoes_novo_projeto1_Jsonclick = "";
         cmbavDynamicfiltersoperator1_Jsonclick = "";
         radavSolicitacoes_novo_projeto2_Jsonclick = "";
         cmbavDynamicfiltersoperator2_Jsonclick = "";
         radavSolicitacoes_novo_projeto3_Jsonclick = "";
         cmbavDynamicfiltersoperator3_Jsonclick = "";
         cmbavDynamicfiltersselector3_Jsonclick = "";
         imgRemovedynamicfilters2_Visible = 1;
         imgAdddynamicfilters2_Visible = 1;
         cmbavDynamicfiltersselector2_Jsonclick = "";
         imgRemovedynamicfilters1_Visible = 1;
         imgAdddynamicfilters1_Visible = 1;
         cmbavDynamicfiltersselector1_Jsonclick = "";
         edtavOrdereddsc_Jsonclick = "";
         cmbavOrderedby_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtavSelect_Tooltiptext = "Selecionar";
         cmbSolicitacoes_Status_Titleformat = 0;
         edtSolicitacoes_Data_Ult_Titleformat = 0;
         edtSolicitacoes_Usuario_Ult_Titleformat = 0;
         edtSolicitacoes_Data_Titleformat = 0;
         edtSolicitacoes_Usuario_Titleformat = 0;
         edtSolicitacoes_Objetivo_Titleformat = 0;
         radSolicitacoes_Novo_Projeto_Titleformat = 0;
         edtServico_Codigo_Titleformat = 0;
         edtSistema_Codigo_Titleformat = 0;
         edtContratada_Codigo_Titleformat = 0;
         edtSolicitacoes_Codigo_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         cmbavDynamicfiltersoperator3.Visible = 1;
         radavSolicitacoes_novo_projeto3.Visible = 1;
         cmbavDynamicfiltersoperator2.Visible = 1;
         radavSolicitacoes_novo_projeto2.Visible = 1;
         cmbavDynamicfiltersoperator1.Visible = 1;
         radavSolicitacoes_novo_projeto1.Visible = 1;
         cmbSolicitacoes_Status.Title.Text = "Solicitacoes_Status";
         edtSolicitacoes_Data_Ult_Title = "Ultima Modifica��o";
         edtSolicitacoes_Usuario_Ult_Title = "Ultima Modifica��o";
         edtSolicitacoes_Data_Title = "de inclus�o";
         edtSolicitacoes_Usuario_Title = "da Solicita��es";
         edtSolicitacoes_Objetivo_Title = "Objetivo";
         radSolicitacoes_Novo_Projeto.Title.Text = "Projeto";
         edtServico_Codigo_Title = "Servi�o";
         edtSistema_Codigo_Title = "Sistema";
         edtContratada_Codigo_Title = "Contratada";
         edtSolicitacoes_Codigo_Title = "Solicitacoes_Codigo";
         edtavOrdereddsc_Visible = 1;
         lblJsdynamicfilters_Caption = "JSDynamicFilters";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         chkavDynamicfiltersenabled3.Caption = "";
         chkavDynamicfiltersenabled2.Caption = "";
         edtavDdo_solicitacoes_statustitlecontrolidtoreplace_Visible = 1;
         edtavDdo_solicitacoes_data_ulttitlecontrolidtoreplace_Visible = 1;
         edtavDdo_solicitacoes_usuario_ulttitlecontrolidtoreplace_Visible = 1;
         edtavDdo_solicitacoes_datatitlecontrolidtoreplace_Visible = 1;
         edtavDdo_solicitacoes_usuariotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_solicitacoes_objetivotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_solicitacoes_novo_projetotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_servico_codigotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_sistema_codigotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratada_codigotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_solicitacoes_codigotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_solicitacoes_data_ultauxdateto_Jsonclick = "";
         edtavDdo_solicitacoes_data_ultauxdate_Jsonclick = "";
         edtavTfsolicitacoes_data_ult_to_Jsonclick = "";
         edtavTfsolicitacoes_data_ult_to_Visible = 1;
         edtavTfsolicitacoes_data_ult_Jsonclick = "";
         edtavTfsolicitacoes_data_ult_Visible = 1;
         edtavTfsolicitacoes_usuario_ult_to_Jsonclick = "";
         edtavTfsolicitacoes_usuario_ult_to_Visible = 1;
         edtavTfsolicitacoes_usuario_ult_Jsonclick = "";
         edtavTfsolicitacoes_usuario_ult_Visible = 1;
         edtavDdo_solicitacoes_dataauxdateto_Jsonclick = "";
         edtavDdo_solicitacoes_dataauxdate_Jsonclick = "";
         edtavTfsolicitacoes_data_to_Jsonclick = "";
         edtavTfsolicitacoes_data_to_Visible = 1;
         edtavTfsolicitacoes_data_Jsonclick = "";
         edtavTfsolicitacoes_data_Visible = 1;
         edtavTfsolicitacoes_usuario_to_Jsonclick = "";
         edtavTfsolicitacoes_usuario_to_Visible = 1;
         edtavTfsolicitacoes_usuario_Jsonclick = "";
         edtavTfsolicitacoes_usuario_Visible = 1;
         edtavTfsolicitacoes_objetivo_sel_Visible = 1;
         edtavTfsolicitacoes_objetivo_Visible = 1;
         edtavTfsolicitacoes_novo_projeto_sel_Jsonclick = "";
         edtavTfsolicitacoes_novo_projeto_sel_Visible = 1;
         edtavTfservico_codigo_to_Jsonclick = "";
         edtavTfservico_codigo_to_Visible = 1;
         edtavTfservico_codigo_Jsonclick = "";
         edtavTfservico_codigo_Visible = 1;
         edtavTfsistema_codigo_to_Jsonclick = "";
         edtavTfsistema_codigo_to_Visible = 1;
         edtavTfsistema_codigo_Jsonclick = "";
         edtavTfsistema_codigo_Visible = 1;
         edtavTfcontratada_codigo_to_Jsonclick = "";
         edtavTfcontratada_codigo_to_Visible = 1;
         edtavTfcontratada_codigo_Jsonclick = "";
         edtavTfcontratada_codigo_Visible = 1;
         edtavTfsolicitacoes_codigo_to_Jsonclick = "";
         edtavTfsolicitacoes_codigo_to_Visible = 1;
         edtavTfsolicitacoes_codigo_Jsonclick = "";
         edtavTfsolicitacoes_codigo_Visible = 1;
         chkavDynamicfiltersenabled3.Visible = 1;
         chkavDynamicfiltersenabled2.Visible = 1;
         Ddo_solicitacoes_status_Searchbuttontext = "Filtrar Selecionados";
         Ddo_solicitacoes_status_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_solicitacoes_status_Rangefilterto = "At�";
         Ddo_solicitacoes_status_Rangefilterfrom = "Desde";
         Ddo_solicitacoes_status_Cleanfilter = "Limpar pesquisa";
         Ddo_solicitacoes_status_Loadingdata = "Carregando dados...";
         Ddo_solicitacoes_status_Sortdsc = "Ordenar de Z � A";
         Ddo_solicitacoes_status_Sortasc = "Ordenar de A � Z";
         Ddo_solicitacoes_status_Datalistupdateminimumcharacters = 0;
         Ddo_solicitacoes_status_Datalistfixedvalues = "A:Ativo,E:Excluido,R:Rascunho";
         Ddo_solicitacoes_status_Allowmultipleselection = Convert.ToBoolean( -1);
         Ddo_solicitacoes_status_Datalisttype = "FixedValues";
         Ddo_solicitacoes_status_Includedatalist = Convert.ToBoolean( -1);
         Ddo_solicitacoes_status_Filterisrange = Convert.ToBoolean( 0);
         Ddo_solicitacoes_status_Includefilter = Convert.ToBoolean( 0);
         Ddo_solicitacoes_status_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_solicitacoes_status_Includesortasc = Convert.ToBoolean( -1);
         Ddo_solicitacoes_status_Titlecontrolidtoreplace = "";
         Ddo_solicitacoes_status_Dropdownoptionstype = "GridTitleSettings";
         Ddo_solicitacoes_status_Cls = "ColumnSettings";
         Ddo_solicitacoes_status_Tooltip = "Op��es";
         Ddo_solicitacoes_status_Caption = "";
         Ddo_solicitacoes_data_ult_Searchbuttontext = "Pesquisar";
         Ddo_solicitacoes_data_ult_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_solicitacoes_data_ult_Rangefilterto = "At�";
         Ddo_solicitacoes_data_ult_Rangefilterfrom = "Desde";
         Ddo_solicitacoes_data_ult_Cleanfilter = "Limpar pesquisa";
         Ddo_solicitacoes_data_ult_Loadingdata = "Carregando dados...";
         Ddo_solicitacoes_data_ult_Sortdsc = "Ordenar de Z � A";
         Ddo_solicitacoes_data_ult_Sortasc = "Ordenar de A � Z";
         Ddo_solicitacoes_data_ult_Datalistupdateminimumcharacters = 0;
         Ddo_solicitacoes_data_ult_Datalistfixedvalues = "";
         Ddo_solicitacoes_data_ult_Includedatalist = Convert.ToBoolean( 0);
         Ddo_solicitacoes_data_ult_Filterisrange = Convert.ToBoolean( -1);
         Ddo_solicitacoes_data_ult_Filtertype = "Date";
         Ddo_solicitacoes_data_ult_Includefilter = Convert.ToBoolean( -1);
         Ddo_solicitacoes_data_ult_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_solicitacoes_data_ult_Includesortasc = Convert.ToBoolean( -1);
         Ddo_solicitacoes_data_ult_Titlecontrolidtoreplace = "";
         Ddo_solicitacoes_data_ult_Dropdownoptionstype = "GridTitleSettings";
         Ddo_solicitacoes_data_ult_Cls = "ColumnSettings";
         Ddo_solicitacoes_data_ult_Tooltip = "Op��es";
         Ddo_solicitacoes_data_ult_Caption = "";
         Ddo_solicitacoes_usuario_ult_Searchbuttontext = "Pesquisar";
         Ddo_solicitacoes_usuario_ult_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_solicitacoes_usuario_ult_Rangefilterto = "At�";
         Ddo_solicitacoes_usuario_ult_Rangefilterfrom = "Desde";
         Ddo_solicitacoes_usuario_ult_Cleanfilter = "Limpar pesquisa";
         Ddo_solicitacoes_usuario_ult_Loadingdata = "Carregando dados...";
         Ddo_solicitacoes_usuario_ult_Sortdsc = "Ordenar de Z � A";
         Ddo_solicitacoes_usuario_ult_Sortasc = "Ordenar de A � Z";
         Ddo_solicitacoes_usuario_ult_Datalistupdateminimumcharacters = 0;
         Ddo_solicitacoes_usuario_ult_Datalistfixedvalues = "";
         Ddo_solicitacoes_usuario_ult_Includedatalist = Convert.ToBoolean( 0);
         Ddo_solicitacoes_usuario_ult_Filterisrange = Convert.ToBoolean( -1);
         Ddo_solicitacoes_usuario_ult_Filtertype = "Numeric";
         Ddo_solicitacoes_usuario_ult_Includefilter = Convert.ToBoolean( -1);
         Ddo_solicitacoes_usuario_ult_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_solicitacoes_usuario_ult_Includesortasc = Convert.ToBoolean( -1);
         Ddo_solicitacoes_usuario_ult_Titlecontrolidtoreplace = "";
         Ddo_solicitacoes_usuario_ult_Dropdownoptionstype = "GridTitleSettings";
         Ddo_solicitacoes_usuario_ult_Cls = "ColumnSettings";
         Ddo_solicitacoes_usuario_ult_Tooltip = "Op��es";
         Ddo_solicitacoes_usuario_ult_Caption = "";
         Ddo_solicitacoes_data_Searchbuttontext = "Pesquisar";
         Ddo_solicitacoes_data_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_solicitacoes_data_Rangefilterto = "At�";
         Ddo_solicitacoes_data_Rangefilterfrom = "Desde";
         Ddo_solicitacoes_data_Cleanfilter = "Limpar pesquisa";
         Ddo_solicitacoes_data_Loadingdata = "Carregando dados...";
         Ddo_solicitacoes_data_Sortdsc = "Ordenar de Z � A";
         Ddo_solicitacoes_data_Sortasc = "Ordenar de A � Z";
         Ddo_solicitacoes_data_Datalistupdateminimumcharacters = 0;
         Ddo_solicitacoes_data_Datalistfixedvalues = "";
         Ddo_solicitacoes_data_Includedatalist = Convert.ToBoolean( 0);
         Ddo_solicitacoes_data_Filterisrange = Convert.ToBoolean( -1);
         Ddo_solicitacoes_data_Filtertype = "Date";
         Ddo_solicitacoes_data_Includefilter = Convert.ToBoolean( -1);
         Ddo_solicitacoes_data_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_solicitacoes_data_Includesortasc = Convert.ToBoolean( -1);
         Ddo_solicitacoes_data_Titlecontrolidtoreplace = "";
         Ddo_solicitacoes_data_Dropdownoptionstype = "GridTitleSettings";
         Ddo_solicitacoes_data_Cls = "ColumnSettings";
         Ddo_solicitacoes_data_Tooltip = "Op��es";
         Ddo_solicitacoes_data_Caption = "";
         Ddo_solicitacoes_usuario_Searchbuttontext = "Pesquisar";
         Ddo_solicitacoes_usuario_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_solicitacoes_usuario_Rangefilterto = "At�";
         Ddo_solicitacoes_usuario_Rangefilterfrom = "Desde";
         Ddo_solicitacoes_usuario_Cleanfilter = "Limpar pesquisa";
         Ddo_solicitacoes_usuario_Loadingdata = "Carregando dados...";
         Ddo_solicitacoes_usuario_Sortdsc = "Ordenar de Z � A";
         Ddo_solicitacoes_usuario_Sortasc = "Ordenar de A � Z";
         Ddo_solicitacoes_usuario_Datalistupdateminimumcharacters = 0;
         Ddo_solicitacoes_usuario_Datalistfixedvalues = "";
         Ddo_solicitacoes_usuario_Includedatalist = Convert.ToBoolean( 0);
         Ddo_solicitacoes_usuario_Filterisrange = Convert.ToBoolean( -1);
         Ddo_solicitacoes_usuario_Filtertype = "Numeric";
         Ddo_solicitacoes_usuario_Includefilter = Convert.ToBoolean( -1);
         Ddo_solicitacoes_usuario_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_solicitacoes_usuario_Includesortasc = Convert.ToBoolean( -1);
         Ddo_solicitacoes_usuario_Titlecontrolidtoreplace = "";
         Ddo_solicitacoes_usuario_Dropdownoptionstype = "GridTitleSettings";
         Ddo_solicitacoes_usuario_Cls = "ColumnSettings";
         Ddo_solicitacoes_usuario_Tooltip = "Op��es";
         Ddo_solicitacoes_usuario_Caption = "";
         Ddo_solicitacoes_objetivo_Searchbuttontext = "Pesquisar";
         Ddo_solicitacoes_objetivo_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_solicitacoes_objetivo_Rangefilterto = "At�";
         Ddo_solicitacoes_objetivo_Rangefilterfrom = "Desde";
         Ddo_solicitacoes_objetivo_Cleanfilter = "Limpar pesquisa";
         Ddo_solicitacoes_objetivo_Loadingdata = "Carregando dados...";
         Ddo_solicitacoes_objetivo_Sortdsc = "Ordenar de Z � A";
         Ddo_solicitacoes_objetivo_Sortasc = "Ordenar de A � Z";
         Ddo_solicitacoes_objetivo_Datalistupdateminimumcharacters = 0;
         Ddo_solicitacoes_objetivo_Datalistproc = "GetPromptSolicitacoesFilterData";
         Ddo_solicitacoes_objetivo_Datalistfixedvalues = "";
         Ddo_solicitacoes_objetivo_Datalisttype = "Dynamic";
         Ddo_solicitacoes_objetivo_Includedatalist = Convert.ToBoolean( -1);
         Ddo_solicitacoes_objetivo_Filterisrange = Convert.ToBoolean( 0);
         Ddo_solicitacoes_objetivo_Filtertype = "Character";
         Ddo_solicitacoes_objetivo_Includefilter = Convert.ToBoolean( -1);
         Ddo_solicitacoes_objetivo_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_solicitacoes_objetivo_Includesortasc = Convert.ToBoolean( -1);
         Ddo_solicitacoes_objetivo_Titlecontrolidtoreplace = "";
         Ddo_solicitacoes_objetivo_Dropdownoptionstype = "GridTitleSettings";
         Ddo_solicitacoes_objetivo_Cls = "ColumnSettings";
         Ddo_solicitacoes_objetivo_Tooltip = "Op��es";
         Ddo_solicitacoes_objetivo_Caption = "";
         Ddo_solicitacoes_novo_projeto_Searchbuttontext = "Pesquisar";
         Ddo_solicitacoes_novo_projeto_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_solicitacoes_novo_projeto_Rangefilterto = "At�";
         Ddo_solicitacoes_novo_projeto_Rangefilterfrom = "Desde";
         Ddo_solicitacoes_novo_projeto_Cleanfilter = "Limpar pesquisa";
         Ddo_solicitacoes_novo_projeto_Loadingdata = "Carregando dados...";
         Ddo_solicitacoes_novo_projeto_Sortdsc = "Ordenar de Z � A";
         Ddo_solicitacoes_novo_projeto_Sortasc = "Ordenar de A � Z";
         Ddo_solicitacoes_novo_projeto_Datalistupdateminimumcharacters = 0;
         Ddo_solicitacoes_novo_projeto_Datalistproc = "GetPromptSolicitacoesFilterData";
         Ddo_solicitacoes_novo_projeto_Datalistfixedvalues = "";
         Ddo_solicitacoes_novo_projeto_Datalisttype = "Dynamic (with fixedValues)";
         Ddo_solicitacoes_novo_projeto_Includedatalist = Convert.ToBoolean( -1);
         Ddo_solicitacoes_novo_projeto_Filterisrange = Convert.ToBoolean( 0);
         Ddo_solicitacoes_novo_projeto_Filtertype = "Character";
         Ddo_solicitacoes_novo_projeto_Includefilter = Convert.ToBoolean( -1);
         Ddo_solicitacoes_novo_projeto_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_solicitacoes_novo_projeto_Includesortasc = Convert.ToBoolean( -1);
         Ddo_solicitacoes_novo_projeto_Titlecontrolidtoreplace = "";
         Ddo_solicitacoes_novo_projeto_Dropdownoptionstype = "GridTitleSettings";
         Ddo_solicitacoes_novo_projeto_Cls = "ColumnSettings";
         Ddo_solicitacoes_novo_projeto_Tooltip = "Op��es";
         Ddo_solicitacoes_novo_projeto_Caption = "";
         Ddo_servico_codigo_Searchbuttontext = "Pesquisar";
         Ddo_servico_codigo_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_servico_codigo_Rangefilterto = "At�";
         Ddo_servico_codigo_Rangefilterfrom = "Desde";
         Ddo_servico_codigo_Cleanfilter = "Limpar pesquisa";
         Ddo_servico_codigo_Loadingdata = "Carregando dados...";
         Ddo_servico_codigo_Sortdsc = "Ordenar de Z � A";
         Ddo_servico_codigo_Sortasc = "Ordenar de A � Z";
         Ddo_servico_codigo_Datalistupdateminimumcharacters = 0;
         Ddo_servico_codigo_Datalistfixedvalues = "";
         Ddo_servico_codigo_Includedatalist = Convert.ToBoolean( 0);
         Ddo_servico_codigo_Filterisrange = Convert.ToBoolean( -1);
         Ddo_servico_codigo_Filtertype = "Numeric";
         Ddo_servico_codigo_Includefilter = Convert.ToBoolean( -1);
         Ddo_servico_codigo_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_servico_codigo_Includesortasc = Convert.ToBoolean( -1);
         Ddo_servico_codigo_Titlecontrolidtoreplace = "";
         Ddo_servico_codigo_Dropdownoptionstype = "GridTitleSettings";
         Ddo_servico_codigo_Cls = "ColumnSettings";
         Ddo_servico_codigo_Tooltip = "Op��es";
         Ddo_servico_codigo_Caption = "";
         Ddo_sistema_codigo_Searchbuttontext = "Pesquisar";
         Ddo_sistema_codigo_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_sistema_codigo_Rangefilterto = "At�";
         Ddo_sistema_codigo_Rangefilterfrom = "Desde";
         Ddo_sistema_codigo_Cleanfilter = "Limpar pesquisa";
         Ddo_sistema_codigo_Loadingdata = "Carregando dados...";
         Ddo_sistema_codigo_Sortdsc = "Ordenar de Z � A";
         Ddo_sistema_codigo_Sortasc = "Ordenar de A � Z";
         Ddo_sistema_codigo_Datalistupdateminimumcharacters = 0;
         Ddo_sistema_codigo_Datalistfixedvalues = "";
         Ddo_sistema_codigo_Includedatalist = Convert.ToBoolean( 0);
         Ddo_sistema_codigo_Filterisrange = Convert.ToBoolean( -1);
         Ddo_sistema_codigo_Filtertype = "Numeric";
         Ddo_sistema_codigo_Includefilter = Convert.ToBoolean( -1);
         Ddo_sistema_codigo_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_sistema_codigo_Includesortasc = Convert.ToBoolean( -1);
         Ddo_sistema_codigo_Titlecontrolidtoreplace = "";
         Ddo_sistema_codigo_Dropdownoptionstype = "GridTitleSettings";
         Ddo_sistema_codigo_Cls = "ColumnSettings";
         Ddo_sistema_codigo_Tooltip = "Op��es";
         Ddo_sistema_codigo_Caption = "";
         Ddo_contratada_codigo_Searchbuttontext = "Pesquisar";
         Ddo_contratada_codigo_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_contratada_codigo_Rangefilterto = "At�";
         Ddo_contratada_codigo_Rangefilterfrom = "Desde";
         Ddo_contratada_codigo_Cleanfilter = "Limpar pesquisa";
         Ddo_contratada_codigo_Loadingdata = "Carregando dados...";
         Ddo_contratada_codigo_Sortdsc = "Ordenar de Z � A";
         Ddo_contratada_codigo_Sortasc = "Ordenar de A � Z";
         Ddo_contratada_codigo_Datalistupdateminimumcharacters = 0;
         Ddo_contratada_codigo_Datalistfixedvalues = "";
         Ddo_contratada_codigo_Includedatalist = Convert.ToBoolean( 0);
         Ddo_contratada_codigo_Filterisrange = Convert.ToBoolean( -1);
         Ddo_contratada_codigo_Filtertype = "Numeric";
         Ddo_contratada_codigo_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratada_codigo_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratada_codigo_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratada_codigo_Titlecontrolidtoreplace = "";
         Ddo_contratada_codigo_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratada_codigo_Cls = "ColumnSettings";
         Ddo_contratada_codigo_Tooltip = "Op��es";
         Ddo_contratada_codigo_Caption = "";
         Ddo_solicitacoes_codigo_Searchbuttontext = "Pesquisar";
         Ddo_solicitacoes_codigo_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_solicitacoes_codigo_Rangefilterto = "At�";
         Ddo_solicitacoes_codigo_Rangefilterfrom = "Desde";
         Ddo_solicitacoes_codigo_Cleanfilter = "Limpar pesquisa";
         Ddo_solicitacoes_codigo_Loadingdata = "Carregando dados...";
         Ddo_solicitacoes_codigo_Sortdsc = "Ordenar de Z � A";
         Ddo_solicitacoes_codigo_Sortasc = "Ordenar de A � Z";
         Ddo_solicitacoes_codigo_Datalistupdateminimumcharacters = 0;
         Ddo_solicitacoes_codigo_Datalistfixedvalues = "";
         Ddo_solicitacoes_codigo_Includedatalist = Convert.ToBoolean( 0);
         Ddo_solicitacoes_codigo_Filterisrange = Convert.ToBoolean( -1);
         Ddo_solicitacoes_codigo_Filtertype = "Numeric";
         Ddo_solicitacoes_codigo_Includefilter = Convert.ToBoolean( -1);
         Ddo_solicitacoes_codigo_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_solicitacoes_codigo_Includesortasc = Convert.ToBoolean( -1);
         Ddo_solicitacoes_codigo_Titlecontrolidtoreplace = "";
         Ddo_solicitacoes_codigo_Dropdownoptionstype = "GridTitleSettings";
         Ddo_solicitacoes_codigo_Cls = "ColumnSettings";
         Ddo_solicitacoes_codigo_Tooltip = "Op��es";
         Ddo_solicitacoes_codigo_Caption = "";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         Form.Caption = "Selecione Solicitacoes";
         subGrid_Rows = 0;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV33ddo_Solicitacoes_CodigoTitleControlIdToReplace',fld:'vDDO_SOLICITACOES_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_Contratada_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATADA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_Sistema_CodigoTitleControlIdToReplace',fld:'vDDO_SISTEMA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_Servico_CodigoTitleControlIdToReplace',fld:'vDDO_SERVICO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_Solicitacoes_Novo_ProjetoTitleControlIdToReplace',fld:'vDDO_SOLICITACOES_NOVO_PROJETOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_Solicitacoes_ObjetivoTitleControlIdToReplace',fld:'vDDO_SOLICITACOES_OBJETIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_Solicitacoes_UsuarioTitleControlIdToReplace',fld:'vDDO_SOLICITACOES_USUARIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV62ddo_Solicitacoes_DataTitleControlIdToReplace',fld:'vDDO_SOLICITACOES_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_Solicitacoes_Usuario_UltTitleControlIdToReplace',fld:'vDDO_SOLICITACOES_USUARIO_ULTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_Solicitacoes_Data_UltTitleControlIdToReplace',fld:'vDDO_SOLICITACOES_DATA_ULTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV76ddo_Solicitacoes_StatusTitleControlIdToReplace',fld:'vDDO_SOLICITACOES_STATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV31TFSolicitacoes_Codigo',fld:'vTFSOLICITACOES_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32TFSolicitacoes_Codigo_To',fld:'vTFSOLICITACOES_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV35TFContratada_Codigo',fld:'vTFCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV36TFContratada_Codigo_To',fld:'vTFCONTRATADA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFSistema_Codigo',fld:'vTFSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV40TFSistema_Codigo_To',fld:'vTFSISTEMA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV43TFServico_Codigo',fld:'vTFSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV44TFServico_Codigo_To',fld:'vTFSERVICO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV47TFSolicitacoes_Novo_Projeto_Sel',fld:'vTFSOLICITACOES_NOVO_PROJETO_SEL',pic:'',nv:''},{av:'AV50TFSolicitacoes_Objetivo',fld:'vTFSOLICITACOES_OBJETIVO',pic:'',nv:''},{av:'AV51TFSolicitacoes_Objetivo_Sel',fld:'vTFSOLICITACOES_OBJETIVO_SEL',pic:'',nv:''},{av:'AV54TFSolicitacoes_Usuario',fld:'vTFSOLICITACOES_USUARIO',pic:'ZZZZZ9',nv:0},{av:'AV55TFSolicitacoes_Usuario_To',fld:'vTFSOLICITACOES_USUARIO_TO',pic:'ZZZZZ9',nv:0},{av:'AV58TFSolicitacoes_Data',fld:'vTFSOLICITACOES_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV59TFSolicitacoes_Data_To',fld:'vTFSOLICITACOES_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV64TFSolicitacoes_Usuario_Ult',fld:'vTFSOLICITACOES_USUARIO_ULT',pic:'ZZZZZ9',nv:0},{av:'AV65TFSolicitacoes_Usuario_Ult_To',fld:'vTFSOLICITACOES_USUARIO_ULT_TO',pic:'ZZZZZ9',nv:0},{av:'AV68TFSolicitacoes_Data_Ult',fld:'vTFSOLICITACOES_DATA_ULT',pic:'99/99/99 99:99',nv:''},{av:'AV69TFSolicitacoes_Data_Ult_To',fld:'vTFSOLICITACOES_DATA_ULT_TO',pic:'99/99/99 99:99',nv:''},{av:'AV75TFSolicitacoes_Status_Sels',fld:'vTFSOLICITACOES_STATUS_SELS',pic:'',nv:null},{av:'AV84Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17Solicitacoes_Novo_Projeto1',fld:'vSOLICITACOES_NOVO_PROJETO1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV21Solicitacoes_Novo_Projeto2',fld:'vSOLICITACOES_NOVO_PROJETO2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV25Solicitacoes_Novo_Projeto3',fld:'vSOLICITACOES_NOVO_PROJETO3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0}],oparms:[{av:'AV30Solicitacoes_CodigoTitleFilterData',fld:'vSOLICITACOES_CODIGOTITLEFILTERDATA',pic:'',nv:null},{av:'AV34Contratada_CodigoTitleFilterData',fld:'vCONTRATADA_CODIGOTITLEFILTERDATA',pic:'',nv:null},{av:'AV38Sistema_CodigoTitleFilterData',fld:'vSISTEMA_CODIGOTITLEFILTERDATA',pic:'',nv:null},{av:'AV42Servico_CodigoTitleFilterData',fld:'vSERVICO_CODIGOTITLEFILTERDATA',pic:'',nv:null},{av:'AV46Solicitacoes_Novo_ProjetoTitleFilterData',fld:'vSOLICITACOES_NOVO_PROJETOTITLEFILTERDATA',pic:'',nv:null},{av:'AV49Solicitacoes_ObjetivoTitleFilterData',fld:'vSOLICITACOES_OBJETIVOTITLEFILTERDATA',pic:'',nv:null},{av:'AV53Solicitacoes_UsuarioTitleFilterData',fld:'vSOLICITACOES_USUARIOTITLEFILTERDATA',pic:'',nv:null},{av:'AV57Solicitacoes_DataTitleFilterData',fld:'vSOLICITACOES_DATATITLEFILTERDATA',pic:'',nv:null},{av:'AV63Solicitacoes_Usuario_UltTitleFilterData',fld:'vSOLICITACOES_USUARIO_ULTTITLEFILTERDATA',pic:'',nv:null},{av:'AV67Solicitacoes_Data_UltTitleFilterData',fld:'vSOLICITACOES_DATA_ULTTITLEFILTERDATA',pic:'',nv:null},{av:'AV73Solicitacoes_StatusTitleFilterData',fld:'vSOLICITACOES_STATUSTITLEFILTERDATA',pic:'',nv:null},{av:'edtSolicitacoes_Codigo_Titleformat',ctrl:'SOLICITACOES_CODIGO',prop:'Titleformat'},{av:'edtSolicitacoes_Codigo_Title',ctrl:'SOLICITACOES_CODIGO',prop:'Title'},{av:'edtContratada_Codigo_Titleformat',ctrl:'CONTRATADA_CODIGO',prop:'Titleformat'},{av:'edtContratada_Codigo_Title',ctrl:'CONTRATADA_CODIGO',prop:'Title'},{av:'edtSistema_Codigo_Titleformat',ctrl:'SISTEMA_CODIGO',prop:'Titleformat'},{av:'edtSistema_Codigo_Title',ctrl:'SISTEMA_CODIGO',prop:'Title'},{av:'edtServico_Codigo_Titleformat',ctrl:'SERVICO_CODIGO',prop:'Titleformat'},{av:'edtServico_Codigo_Title',ctrl:'SERVICO_CODIGO',prop:'Title'},{av:'radSolicitacoes_Novo_Projeto'},{av:'edtSolicitacoes_Objetivo_Titleformat',ctrl:'SOLICITACOES_OBJETIVO',prop:'Titleformat'},{av:'edtSolicitacoes_Objetivo_Title',ctrl:'SOLICITACOES_OBJETIVO',prop:'Title'},{av:'edtSolicitacoes_Usuario_Titleformat',ctrl:'SOLICITACOES_USUARIO',prop:'Titleformat'},{av:'edtSolicitacoes_Usuario_Title',ctrl:'SOLICITACOES_USUARIO',prop:'Title'},{av:'edtSolicitacoes_Data_Titleformat',ctrl:'SOLICITACOES_DATA',prop:'Titleformat'},{av:'edtSolicitacoes_Data_Title',ctrl:'SOLICITACOES_DATA',prop:'Title'},{av:'edtSolicitacoes_Usuario_Ult_Titleformat',ctrl:'SOLICITACOES_USUARIO_ULT',prop:'Titleformat'},{av:'edtSolicitacoes_Usuario_Ult_Title',ctrl:'SOLICITACOES_USUARIO_ULT',prop:'Title'},{av:'edtSolicitacoes_Data_Ult_Titleformat',ctrl:'SOLICITACOES_DATA_ULT',prop:'Titleformat'},{av:'edtSolicitacoes_Data_Ult_Title',ctrl:'SOLICITACOES_DATA_ULT',prop:'Title'},{av:'cmbSolicitacoes_Status'},{av:'AV79GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV80GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E11AW2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Solicitacoes_Novo_Projeto1',fld:'vSOLICITACOES_NOVO_PROJETO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21Solicitacoes_Novo_Projeto2',fld:'vSOLICITACOES_NOVO_PROJETO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25Solicitacoes_Novo_Projeto3',fld:'vSOLICITACOES_NOVO_PROJETO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31TFSolicitacoes_Codigo',fld:'vTFSOLICITACOES_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32TFSolicitacoes_Codigo_To',fld:'vTFSOLICITACOES_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV35TFContratada_Codigo',fld:'vTFCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV36TFContratada_Codigo_To',fld:'vTFCONTRATADA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFSistema_Codigo',fld:'vTFSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV40TFSistema_Codigo_To',fld:'vTFSISTEMA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV43TFServico_Codigo',fld:'vTFSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV44TFServico_Codigo_To',fld:'vTFSERVICO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV47TFSolicitacoes_Novo_Projeto_Sel',fld:'vTFSOLICITACOES_NOVO_PROJETO_SEL',pic:'',nv:''},{av:'AV50TFSolicitacoes_Objetivo',fld:'vTFSOLICITACOES_OBJETIVO',pic:'',nv:''},{av:'AV51TFSolicitacoes_Objetivo_Sel',fld:'vTFSOLICITACOES_OBJETIVO_SEL',pic:'',nv:''},{av:'AV54TFSolicitacoes_Usuario',fld:'vTFSOLICITACOES_USUARIO',pic:'ZZZZZ9',nv:0},{av:'AV55TFSolicitacoes_Usuario_To',fld:'vTFSOLICITACOES_USUARIO_TO',pic:'ZZZZZ9',nv:0},{av:'AV58TFSolicitacoes_Data',fld:'vTFSOLICITACOES_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV59TFSolicitacoes_Data_To',fld:'vTFSOLICITACOES_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV64TFSolicitacoes_Usuario_Ult',fld:'vTFSOLICITACOES_USUARIO_ULT',pic:'ZZZZZ9',nv:0},{av:'AV65TFSolicitacoes_Usuario_Ult_To',fld:'vTFSOLICITACOES_USUARIO_ULT_TO',pic:'ZZZZZ9',nv:0},{av:'AV68TFSolicitacoes_Data_Ult',fld:'vTFSOLICITACOES_DATA_ULT',pic:'99/99/99 99:99',nv:''},{av:'AV69TFSolicitacoes_Data_Ult_To',fld:'vTFSOLICITACOES_DATA_ULT_TO',pic:'99/99/99 99:99',nv:''},{av:'AV33ddo_Solicitacoes_CodigoTitleControlIdToReplace',fld:'vDDO_SOLICITACOES_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_Contratada_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATADA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_Sistema_CodigoTitleControlIdToReplace',fld:'vDDO_SISTEMA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_Servico_CodigoTitleControlIdToReplace',fld:'vDDO_SERVICO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_Solicitacoes_Novo_ProjetoTitleControlIdToReplace',fld:'vDDO_SOLICITACOES_NOVO_PROJETOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_Solicitacoes_ObjetivoTitleControlIdToReplace',fld:'vDDO_SOLICITACOES_OBJETIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_Solicitacoes_UsuarioTitleControlIdToReplace',fld:'vDDO_SOLICITACOES_USUARIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV62ddo_Solicitacoes_DataTitleControlIdToReplace',fld:'vDDO_SOLICITACOES_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_Solicitacoes_Usuario_UltTitleControlIdToReplace',fld:'vDDO_SOLICITACOES_USUARIO_ULTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_Solicitacoes_Data_UltTitleControlIdToReplace',fld:'vDDO_SOLICITACOES_DATA_ULTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV76ddo_Solicitacoes_StatusTitleControlIdToReplace',fld:'vDDO_SOLICITACOES_STATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75TFSolicitacoes_Status_Sels',fld:'vTFSOLICITACOES_STATUS_SELS',pic:'',nv:null},{av:'AV84Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("DDO_SOLICITACOES_CODIGO.ONOPTIONCLICKED","{handler:'E12AW2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Solicitacoes_Novo_Projeto1',fld:'vSOLICITACOES_NOVO_PROJETO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21Solicitacoes_Novo_Projeto2',fld:'vSOLICITACOES_NOVO_PROJETO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25Solicitacoes_Novo_Projeto3',fld:'vSOLICITACOES_NOVO_PROJETO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31TFSolicitacoes_Codigo',fld:'vTFSOLICITACOES_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32TFSolicitacoes_Codigo_To',fld:'vTFSOLICITACOES_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV35TFContratada_Codigo',fld:'vTFCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV36TFContratada_Codigo_To',fld:'vTFCONTRATADA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFSistema_Codigo',fld:'vTFSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV40TFSistema_Codigo_To',fld:'vTFSISTEMA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV43TFServico_Codigo',fld:'vTFSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV44TFServico_Codigo_To',fld:'vTFSERVICO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV47TFSolicitacoes_Novo_Projeto_Sel',fld:'vTFSOLICITACOES_NOVO_PROJETO_SEL',pic:'',nv:''},{av:'AV50TFSolicitacoes_Objetivo',fld:'vTFSOLICITACOES_OBJETIVO',pic:'',nv:''},{av:'AV51TFSolicitacoes_Objetivo_Sel',fld:'vTFSOLICITACOES_OBJETIVO_SEL',pic:'',nv:''},{av:'AV54TFSolicitacoes_Usuario',fld:'vTFSOLICITACOES_USUARIO',pic:'ZZZZZ9',nv:0},{av:'AV55TFSolicitacoes_Usuario_To',fld:'vTFSOLICITACOES_USUARIO_TO',pic:'ZZZZZ9',nv:0},{av:'AV58TFSolicitacoes_Data',fld:'vTFSOLICITACOES_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV59TFSolicitacoes_Data_To',fld:'vTFSOLICITACOES_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV64TFSolicitacoes_Usuario_Ult',fld:'vTFSOLICITACOES_USUARIO_ULT',pic:'ZZZZZ9',nv:0},{av:'AV65TFSolicitacoes_Usuario_Ult_To',fld:'vTFSOLICITACOES_USUARIO_ULT_TO',pic:'ZZZZZ9',nv:0},{av:'AV68TFSolicitacoes_Data_Ult',fld:'vTFSOLICITACOES_DATA_ULT',pic:'99/99/99 99:99',nv:''},{av:'AV69TFSolicitacoes_Data_Ult_To',fld:'vTFSOLICITACOES_DATA_ULT_TO',pic:'99/99/99 99:99',nv:''},{av:'AV33ddo_Solicitacoes_CodigoTitleControlIdToReplace',fld:'vDDO_SOLICITACOES_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_Contratada_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATADA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_Sistema_CodigoTitleControlIdToReplace',fld:'vDDO_SISTEMA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_Servico_CodigoTitleControlIdToReplace',fld:'vDDO_SERVICO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_Solicitacoes_Novo_ProjetoTitleControlIdToReplace',fld:'vDDO_SOLICITACOES_NOVO_PROJETOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_Solicitacoes_ObjetivoTitleControlIdToReplace',fld:'vDDO_SOLICITACOES_OBJETIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_Solicitacoes_UsuarioTitleControlIdToReplace',fld:'vDDO_SOLICITACOES_USUARIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV62ddo_Solicitacoes_DataTitleControlIdToReplace',fld:'vDDO_SOLICITACOES_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_Solicitacoes_Usuario_UltTitleControlIdToReplace',fld:'vDDO_SOLICITACOES_USUARIO_ULTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_Solicitacoes_Data_UltTitleControlIdToReplace',fld:'vDDO_SOLICITACOES_DATA_ULTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV76ddo_Solicitacoes_StatusTitleControlIdToReplace',fld:'vDDO_SOLICITACOES_STATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75TFSolicitacoes_Status_Sels',fld:'vTFSOLICITACOES_STATUS_SELS',pic:'',nv:null},{av:'AV84Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_solicitacoes_codigo_Activeeventkey',ctrl:'DDO_SOLICITACOES_CODIGO',prop:'ActiveEventKey'},{av:'Ddo_solicitacoes_codigo_Filteredtext_get',ctrl:'DDO_SOLICITACOES_CODIGO',prop:'FilteredText_get'},{av:'Ddo_solicitacoes_codigo_Filteredtextto_get',ctrl:'DDO_SOLICITACOES_CODIGO',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_solicitacoes_codigo_Sortedstatus',ctrl:'DDO_SOLICITACOES_CODIGO',prop:'SortedStatus'},{av:'AV31TFSolicitacoes_Codigo',fld:'vTFSOLICITACOES_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32TFSolicitacoes_Codigo_To',fld:'vTFSOLICITACOES_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_contratada_codigo_Sortedstatus',ctrl:'DDO_CONTRATADA_CODIGO',prop:'SortedStatus'},{av:'Ddo_sistema_codigo_Sortedstatus',ctrl:'DDO_SISTEMA_CODIGO',prop:'SortedStatus'},{av:'Ddo_servico_codigo_Sortedstatus',ctrl:'DDO_SERVICO_CODIGO',prop:'SortedStatus'},{av:'Ddo_solicitacoes_novo_projeto_Sortedstatus',ctrl:'DDO_SOLICITACOES_NOVO_PROJETO',prop:'SortedStatus'},{av:'Ddo_solicitacoes_objetivo_Sortedstatus',ctrl:'DDO_SOLICITACOES_OBJETIVO',prop:'SortedStatus'},{av:'Ddo_solicitacoes_usuario_Sortedstatus',ctrl:'DDO_SOLICITACOES_USUARIO',prop:'SortedStatus'},{av:'Ddo_solicitacoes_data_Sortedstatus',ctrl:'DDO_SOLICITACOES_DATA',prop:'SortedStatus'},{av:'Ddo_solicitacoes_usuario_ult_Sortedstatus',ctrl:'DDO_SOLICITACOES_USUARIO_ULT',prop:'SortedStatus'},{av:'Ddo_solicitacoes_data_ult_Sortedstatus',ctrl:'DDO_SOLICITACOES_DATA_ULT',prop:'SortedStatus'},{av:'Ddo_solicitacoes_status_Sortedstatus',ctrl:'DDO_SOLICITACOES_STATUS',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATADA_CODIGO.ONOPTIONCLICKED","{handler:'E13AW2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Solicitacoes_Novo_Projeto1',fld:'vSOLICITACOES_NOVO_PROJETO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21Solicitacoes_Novo_Projeto2',fld:'vSOLICITACOES_NOVO_PROJETO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25Solicitacoes_Novo_Projeto3',fld:'vSOLICITACOES_NOVO_PROJETO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31TFSolicitacoes_Codigo',fld:'vTFSOLICITACOES_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32TFSolicitacoes_Codigo_To',fld:'vTFSOLICITACOES_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV35TFContratada_Codigo',fld:'vTFCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV36TFContratada_Codigo_To',fld:'vTFCONTRATADA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFSistema_Codigo',fld:'vTFSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV40TFSistema_Codigo_To',fld:'vTFSISTEMA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV43TFServico_Codigo',fld:'vTFSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV44TFServico_Codigo_To',fld:'vTFSERVICO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV47TFSolicitacoes_Novo_Projeto_Sel',fld:'vTFSOLICITACOES_NOVO_PROJETO_SEL',pic:'',nv:''},{av:'AV50TFSolicitacoes_Objetivo',fld:'vTFSOLICITACOES_OBJETIVO',pic:'',nv:''},{av:'AV51TFSolicitacoes_Objetivo_Sel',fld:'vTFSOLICITACOES_OBJETIVO_SEL',pic:'',nv:''},{av:'AV54TFSolicitacoes_Usuario',fld:'vTFSOLICITACOES_USUARIO',pic:'ZZZZZ9',nv:0},{av:'AV55TFSolicitacoes_Usuario_To',fld:'vTFSOLICITACOES_USUARIO_TO',pic:'ZZZZZ9',nv:0},{av:'AV58TFSolicitacoes_Data',fld:'vTFSOLICITACOES_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV59TFSolicitacoes_Data_To',fld:'vTFSOLICITACOES_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV64TFSolicitacoes_Usuario_Ult',fld:'vTFSOLICITACOES_USUARIO_ULT',pic:'ZZZZZ9',nv:0},{av:'AV65TFSolicitacoes_Usuario_Ult_To',fld:'vTFSOLICITACOES_USUARIO_ULT_TO',pic:'ZZZZZ9',nv:0},{av:'AV68TFSolicitacoes_Data_Ult',fld:'vTFSOLICITACOES_DATA_ULT',pic:'99/99/99 99:99',nv:''},{av:'AV69TFSolicitacoes_Data_Ult_To',fld:'vTFSOLICITACOES_DATA_ULT_TO',pic:'99/99/99 99:99',nv:''},{av:'AV33ddo_Solicitacoes_CodigoTitleControlIdToReplace',fld:'vDDO_SOLICITACOES_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_Contratada_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATADA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_Sistema_CodigoTitleControlIdToReplace',fld:'vDDO_SISTEMA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_Servico_CodigoTitleControlIdToReplace',fld:'vDDO_SERVICO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_Solicitacoes_Novo_ProjetoTitleControlIdToReplace',fld:'vDDO_SOLICITACOES_NOVO_PROJETOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_Solicitacoes_ObjetivoTitleControlIdToReplace',fld:'vDDO_SOLICITACOES_OBJETIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_Solicitacoes_UsuarioTitleControlIdToReplace',fld:'vDDO_SOLICITACOES_USUARIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV62ddo_Solicitacoes_DataTitleControlIdToReplace',fld:'vDDO_SOLICITACOES_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_Solicitacoes_Usuario_UltTitleControlIdToReplace',fld:'vDDO_SOLICITACOES_USUARIO_ULTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_Solicitacoes_Data_UltTitleControlIdToReplace',fld:'vDDO_SOLICITACOES_DATA_ULTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV76ddo_Solicitacoes_StatusTitleControlIdToReplace',fld:'vDDO_SOLICITACOES_STATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75TFSolicitacoes_Status_Sels',fld:'vTFSOLICITACOES_STATUS_SELS',pic:'',nv:null},{av:'AV84Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_contratada_codigo_Activeeventkey',ctrl:'DDO_CONTRATADA_CODIGO',prop:'ActiveEventKey'},{av:'Ddo_contratada_codigo_Filteredtext_get',ctrl:'DDO_CONTRATADA_CODIGO',prop:'FilteredText_get'},{av:'Ddo_contratada_codigo_Filteredtextto_get',ctrl:'DDO_CONTRATADA_CODIGO',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratada_codigo_Sortedstatus',ctrl:'DDO_CONTRATADA_CODIGO',prop:'SortedStatus'},{av:'AV35TFContratada_Codigo',fld:'vTFCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV36TFContratada_Codigo_To',fld:'vTFCONTRATADA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_solicitacoes_codigo_Sortedstatus',ctrl:'DDO_SOLICITACOES_CODIGO',prop:'SortedStatus'},{av:'Ddo_sistema_codigo_Sortedstatus',ctrl:'DDO_SISTEMA_CODIGO',prop:'SortedStatus'},{av:'Ddo_servico_codigo_Sortedstatus',ctrl:'DDO_SERVICO_CODIGO',prop:'SortedStatus'},{av:'Ddo_solicitacoes_novo_projeto_Sortedstatus',ctrl:'DDO_SOLICITACOES_NOVO_PROJETO',prop:'SortedStatus'},{av:'Ddo_solicitacoes_objetivo_Sortedstatus',ctrl:'DDO_SOLICITACOES_OBJETIVO',prop:'SortedStatus'},{av:'Ddo_solicitacoes_usuario_Sortedstatus',ctrl:'DDO_SOLICITACOES_USUARIO',prop:'SortedStatus'},{av:'Ddo_solicitacoes_data_Sortedstatus',ctrl:'DDO_SOLICITACOES_DATA',prop:'SortedStatus'},{av:'Ddo_solicitacoes_usuario_ult_Sortedstatus',ctrl:'DDO_SOLICITACOES_USUARIO_ULT',prop:'SortedStatus'},{av:'Ddo_solicitacoes_data_ult_Sortedstatus',ctrl:'DDO_SOLICITACOES_DATA_ULT',prop:'SortedStatus'},{av:'Ddo_solicitacoes_status_Sortedstatus',ctrl:'DDO_SOLICITACOES_STATUS',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_SISTEMA_CODIGO.ONOPTIONCLICKED","{handler:'E14AW2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Solicitacoes_Novo_Projeto1',fld:'vSOLICITACOES_NOVO_PROJETO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21Solicitacoes_Novo_Projeto2',fld:'vSOLICITACOES_NOVO_PROJETO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25Solicitacoes_Novo_Projeto3',fld:'vSOLICITACOES_NOVO_PROJETO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31TFSolicitacoes_Codigo',fld:'vTFSOLICITACOES_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32TFSolicitacoes_Codigo_To',fld:'vTFSOLICITACOES_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV35TFContratada_Codigo',fld:'vTFCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV36TFContratada_Codigo_To',fld:'vTFCONTRATADA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFSistema_Codigo',fld:'vTFSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV40TFSistema_Codigo_To',fld:'vTFSISTEMA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV43TFServico_Codigo',fld:'vTFSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV44TFServico_Codigo_To',fld:'vTFSERVICO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV47TFSolicitacoes_Novo_Projeto_Sel',fld:'vTFSOLICITACOES_NOVO_PROJETO_SEL',pic:'',nv:''},{av:'AV50TFSolicitacoes_Objetivo',fld:'vTFSOLICITACOES_OBJETIVO',pic:'',nv:''},{av:'AV51TFSolicitacoes_Objetivo_Sel',fld:'vTFSOLICITACOES_OBJETIVO_SEL',pic:'',nv:''},{av:'AV54TFSolicitacoes_Usuario',fld:'vTFSOLICITACOES_USUARIO',pic:'ZZZZZ9',nv:0},{av:'AV55TFSolicitacoes_Usuario_To',fld:'vTFSOLICITACOES_USUARIO_TO',pic:'ZZZZZ9',nv:0},{av:'AV58TFSolicitacoes_Data',fld:'vTFSOLICITACOES_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV59TFSolicitacoes_Data_To',fld:'vTFSOLICITACOES_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV64TFSolicitacoes_Usuario_Ult',fld:'vTFSOLICITACOES_USUARIO_ULT',pic:'ZZZZZ9',nv:0},{av:'AV65TFSolicitacoes_Usuario_Ult_To',fld:'vTFSOLICITACOES_USUARIO_ULT_TO',pic:'ZZZZZ9',nv:0},{av:'AV68TFSolicitacoes_Data_Ult',fld:'vTFSOLICITACOES_DATA_ULT',pic:'99/99/99 99:99',nv:''},{av:'AV69TFSolicitacoes_Data_Ult_To',fld:'vTFSOLICITACOES_DATA_ULT_TO',pic:'99/99/99 99:99',nv:''},{av:'AV33ddo_Solicitacoes_CodigoTitleControlIdToReplace',fld:'vDDO_SOLICITACOES_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_Contratada_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATADA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_Sistema_CodigoTitleControlIdToReplace',fld:'vDDO_SISTEMA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_Servico_CodigoTitleControlIdToReplace',fld:'vDDO_SERVICO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_Solicitacoes_Novo_ProjetoTitleControlIdToReplace',fld:'vDDO_SOLICITACOES_NOVO_PROJETOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_Solicitacoes_ObjetivoTitleControlIdToReplace',fld:'vDDO_SOLICITACOES_OBJETIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_Solicitacoes_UsuarioTitleControlIdToReplace',fld:'vDDO_SOLICITACOES_USUARIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV62ddo_Solicitacoes_DataTitleControlIdToReplace',fld:'vDDO_SOLICITACOES_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_Solicitacoes_Usuario_UltTitleControlIdToReplace',fld:'vDDO_SOLICITACOES_USUARIO_ULTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_Solicitacoes_Data_UltTitleControlIdToReplace',fld:'vDDO_SOLICITACOES_DATA_ULTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV76ddo_Solicitacoes_StatusTitleControlIdToReplace',fld:'vDDO_SOLICITACOES_STATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75TFSolicitacoes_Status_Sels',fld:'vTFSOLICITACOES_STATUS_SELS',pic:'',nv:null},{av:'AV84Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_sistema_codigo_Activeeventkey',ctrl:'DDO_SISTEMA_CODIGO',prop:'ActiveEventKey'},{av:'Ddo_sistema_codigo_Filteredtext_get',ctrl:'DDO_SISTEMA_CODIGO',prop:'FilteredText_get'},{av:'Ddo_sistema_codigo_Filteredtextto_get',ctrl:'DDO_SISTEMA_CODIGO',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_sistema_codigo_Sortedstatus',ctrl:'DDO_SISTEMA_CODIGO',prop:'SortedStatus'},{av:'AV39TFSistema_Codigo',fld:'vTFSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV40TFSistema_Codigo_To',fld:'vTFSISTEMA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_solicitacoes_codigo_Sortedstatus',ctrl:'DDO_SOLICITACOES_CODIGO',prop:'SortedStatus'},{av:'Ddo_contratada_codigo_Sortedstatus',ctrl:'DDO_CONTRATADA_CODIGO',prop:'SortedStatus'},{av:'Ddo_servico_codigo_Sortedstatus',ctrl:'DDO_SERVICO_CODIGO',prop:'SortedStatus'},{av:'Ddo_solicitacoes_novo_projeto_Sortedstatus',ctrl:'DDO_SOLICITACOES_NOVO_PROJETO',prop:'SortedStatus'},{av:'Ddo_solicitacoes_objetivo_Sortedstatus',ctrl:'DDO_SOLICITACOES_OBJETIVO',prop:'SortedStatus'},{av:'Ddo_solicitacoes_usuario_Sortedstatus',ctrl:'DDO_SOLICITACOES_USUARIO',prop:'SortedStatus'},{av:'Ddo_solicitacoes_data_Sortedstatus',ctrl:'DDO_SOLICITACOES_DATA',prop:'SortedStatus'},{av:'Ddo_solicitacoes_usuario_ult_Sortedstatus',ctrl:'DDO_SOLICITACOES_USUARIO_ULT',prop:'SortedStatus'},{av:'Ddo_solicitacoes_data_ult_Sortedstatus',ctrl:'DDO_SOLICITACOES_DATA_ULT',prop:'SortedStatus'},{av:'Ddo_solicitacoes_status_Sortedstatus',ctrl:'DDO_SOLICITACOES_STATUS',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_SERVICO_CODIGO.ONOPTIONCLICKED","{handler:'E15AW2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Solicitacoes_Novo_Projeto1',fld:'vSOLICITACOES_NOVO_PROJETO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21Solicitacoes_Novo_Projeto2',fld:'vSOLICITACOES_NOVO_PROJETO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25Solicitacoes_Novo_Projeto3',fld:'vSOLICITACOES_NOVO_PROJETO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31TFSolicitacoes_Codigo',fld:'vTFSOLICITACOES_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32TFSolicitacoes_Codigo_To',fld:'vTFSOLICITACOES_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV35TFContratada_Codigo',fld:'vTFCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV36TFContratada_Codigo_To',fld:'vTFCONTRATADA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFSistema_Codigo',fld:'vTFSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV40TFSistema_Codigo_To',fld:'vTFSISTEMA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV43TFServico_Codigo',fld:'vTFSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV44TFServico_Codigo_To',fld:'vTFSERVICO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV47TFSolicitacoes_Novo_Projeto_Sel',fld:'vTFSOLICITACOES_NOVO_PROJETO_SEL',pic:'',nv:''},{av:'AV50TFSolicitacoes_Objetivo',fld:'vTFSOLICITACOES_OBJETIVO',pic:'',nv:''},{av:'AV51TFSolicitacoes_Objetivo_Sel',fld:'vTFSOLICITACOES_OBJETIVO_SEL',pic:'',nv:''},{av:'AV54TFSolicitacoes_Usuario',fld:'vTFSOLICITACOES_USUARIO',pic:'ZZZZZ9',nv:0},{av:'AV55TFSolicitacoes_Usuario_To',fld:'vTFSOLICITACOES_USUARIO_TO',pic:'ZZZZZ9',nv:0},{av:'AV58TFSolicitacoes_Data',fld:'vTFSOLICITACOES_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV59TFSolicitacoes_Data_To',fld:'vTFSOLICITACOES_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV64TFSolicitacoes_Usuario_Ult',fld:'vTFSOLICITACOES_USUARIO_ULT',pic:'ZZZZZ9',nv:0},{av:'AV65TFSolicitacoes_Usuario_Ult_To',fld:'vTFSOLICITACOES_USUARIO_ULT_TO',pic:'ZZZZZ9',nv:0},{av:'AV68TFSolicitacoes_Data_Ult',fld:'vTFSOLICITACOES_DATA_ULT',pic:'99/99/99 99:99',nv:''},{av:'AV69TFSolicitacoes_Data_Ult_To',fld:'vTFSOLICITACOES_DATA_ULT_TO',pic:'99/99/99 99:99',nv:''},{av:'AV33ddo_Solicitacoes_CodigoTitleControlIdToReplace',fld:'vDDO_SOLICITACOES_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_Contratada_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATADA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_Sistema_CodigoTitleControlIdToReplace',fld:'vDDO_SISTEMA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_Servico_CodigoTitleControlIdToReplace',fld:'vDDO_SERVICO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_Solicitacoes_Novo_ProjetoTitleControlIdToReplace',fld:'vDDO_SOLICITACOES_NOVO_PROJETOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_Solicitacoes_ObjetivoTitleControlIdToReplace',fld:'vDDO_SOLICITACOES_OBJETIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_Solicitacoes_UsuarioTitleControlIdToReplace',fld:'vDDO_SOLICITACOES_USUARIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV62ddo_Solicitacoes_DataTitleControlIdToReplace',fld:'vDDO_SOLICITACOES_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_Solicitacoes_Usuario_UltTitleControlIdToReplace',fld:'vDDO_SOLICITACOES_USUARIO_ULTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_Solicitacoes_Data_UltTitleControlIdToReplace',fld:'vDDO_SOLICITACOES_DATA_ULTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV76ddo_Solicitacoes_StatusTitleControlIdToReplace',fld:'vDDO_SOLICITACOES_STATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75TFSolicitacoes_Status_Sels',fld:'vTFSOLICITACOES_STATUS_SELS',pic:'',nv:null},{av:'AV84Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_servico_codigo_Activeeventkey',ctrl:'DDO_SERVICO_CODIGO',prop:'ActiveEventKey'},{av:'Ddo_servico_codigo_Filteredtext_get',ctrl:'DDO_SERVICO_CODIGO',prop:'FilteredText_get'},{av:'Ddo_servico_codigo_Filteredtextto_get',ctrl:'DDO_SERVICO_CODIGO',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_servico_codigo_Sortedstatus',ctrl:'DDO_SERVICO_CODIGO',prop:'SortedStatus'},{av:'AV43TFServico_Codigo',fld:'vTFSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV44TFServico_Codigo_To',fld:'vTFSERVICO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_solicitacoes_codigo_Sortedstatus',ctrl:'DDO_SOLICITACOES_CODIGO',prop:'SortedStatus'},{av:'Ddo_contratada_codigo_Sortedstatus',ctrl:'DDO_CONTRATADA_CODIGO',prop:'SortedStatus'},{av:'Ddo_sistema_codigo_Sortedstatus',ctrl:'DDO_SISTEMA_CODIGO',prop:'SortedStatus'},{av:'Ddo_solicitacoes_novo_projeto_Sortedstatus',ctrl:'DDO_SOLICITACOES_NOVO_PROJETO',prop:'SortedStatus'},{av:'Ddo_solicitacoes_objetivo_Sortedstatus',ctrl:'DDO_SOLICITACOES_OBJETIVO',prop:'SortedStatus'},{av:'Ddo_solicitacoes_usuario_Sortedstatus',ctrl:'DDO_SOLICITACOES_USUARIO',prop:'SortedStatus'},{av:'Ddo_solicitacoes_data_Sortedstatus',ctrl:'DDO_SOLICITACOES_DATA',prop:'SortedStatus'},{av:'Ddo_solicitacoes_usuario_ult_Sortedstatus',ctrl:'DDO_SOLICITACOES_USUARIO_ULT',prop:'SortedStatus'},{av:'Ddo_solicitacoes_data_ult_Sortedstatus',ctrl:'DDO_SOLICITACOES_DATA_ULT',prop:'SortedStatus'},{av:'Ddo_solicitacoes_status_Sortedstatus',ctrl:'DDO_SOLICITACOES_STATUS',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_SOLICITACOES_NOVO_PROJETO.ONOPTIONCLICKED","{handler:'E16AW2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Solicitacoes_Novo_Projeto1',fld:'vSOLICITACOES_NOVO_PROJETO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21Solicitacoes_Novo_Projeto2',fld:'vSOLICITACOES_NOVO_PROJETO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25Solicitacoes_Novo_Projeto3',fld:'vSOLICITACOES_NOVO_PROJETO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31TFSolicitacoes_Codigo',fld:'vTFSOLICITACOES_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32TFSolicitacoes_Codigo_To',fld:'vTFSOLICITACOES_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV35TFContratada_Codigo',fld:'vTFCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV36TFContratada_Codigo_To',fld:'vTFCONTRATADA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFSistema_Codigo',fld:'vTFSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV40TFSistema_Codigo_To',fld:'vTFSISTEMA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV43TFServico_Codigo',fld:'vTFSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV44TFServico_Codigo_To',fld:'vTFSERVICO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV47TFSolicitacoes_Novo_Projeto_Sel',fld:'vTFSOLICITACOES_NOVO_PROJETO_SEL',pic:'',nv:''},{av:'AV50TFSolicitacoes_Objetivo',fld:'vTFSOLICITACOES_OBJETIVO',pic:'',nv:''},{av:'AV51TFSolicitacoes_Objetivo_Sel',fld:'vTFSOLICITACOES_OBJETIVO_SEL',pic:'',nv:''},{av:'AV54TFSolicitacoes_Usuario',fld:'vTFSOLICITACOES_USUARIO',pic:'ZZZZZ9',nv:0},{av:'AV55TFSolicitacoes_Usuario_To',fld:'vTFSOLICITACOES_USUARIO_TO',pic:'ZZZZZ9',nv:0},{av:'AV58TFSolicitacoes_Data',fld:'vTFSOLICITACOES_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV59TFSolicitacoes_Data_To',fld:'vTFSOLICITACOES_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV64TFSolicitacoes_Usuario_Ult',fld:'vTFSOLICITACOES_USUARIO_ULT',pic:'ZZZZZ9',nv:0},{av:'AV65TFSolicitacoes_Usuario_Ult_To',fld:'vTFSOLICITACOES_USUARIO_ULT_TO',pic:'ZZZZZ9',nv:0},{av:'AV68TFSolicitacoes_Data_Ult',fld:'vTFSOLICITACOES_DATA_ULT',pic:'99/99/99 99:99',nv:''},{av:'AV69TFSolicitacoes_Data_Ult_To',fld:'vTFSOLICITACOES_DATA_ULT_TO',pic:'99/99/99 99:99',nv:''},{av:'AV33ddo_Solicitacoes_CodigoTitleControlIdToReplace',fld:'vDDO_SOLICITACOES_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_Contratada_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATADA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_Sistema_CodigoTitleControlIdToReplace',fld:'vDDO_SISTEMA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_Servico_CodigoTitleControlIdToReplace',fld:'vDDO_SERVICO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_Solicitacoes_Novo_ProjetoTitleControlIdToReplace',fld:'vDDO_SOLICITACOES_NOVO_PROJETOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_Solicitacoes_ObjetivoTitleControlIdToReplace',fld:'vDDO_SOLICITACOES_OBJETIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_Solicitacoes_UsuarioTitleControlIdToReplace',fld:'vDDO_SOLICITACOES_USUARIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV62ddo_Solicitacoes_DataTitleControlIdToReplace',fld:'vDDO_SOLICITACOES_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_Solicitacoes_Usuario_UltTitleControlIdToReplace',fld:'vDDO_SOLICITACOES_USUARIO_ULTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_Solicitacoes_Data_UltTitleControlIdToReplace',fld:'vDDO_SOLICITACOES_DATA_ULTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV76ddo_Solicitacoes_StatusTitleControlIdToReplace',fld:'vDDO_SOLICITACOES_STATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75TFSolicitacoes_Status_Sels',fld:'vTFSOLICITACOES_STATUS_SELS',pic:'',nv:null},{av:'AV84Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_solicitacoes_novo_projeto_Activeeventkey',ctrl:'DDO_SOLICITACOES_NOVO_PROJETO',prop:'ActiveEventKey'},{av:'Ddo_solicitacoes_novo_projeto_Selectedvalue_get',ctrl:'DDO_SOLICITACOES_NOVO_PROJETO',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_solicitacoes_novo_projeto_Sortedstatus',ctrl:'DDO_SOLICITACOES_NOVO_PROJETO',prop:'SortedStatus'},{av:'AV47TFSolicitacoes_Novo_Projeto_Sel',fld:'vTFSOLICITACOES_NOVO_PROJETO_SEL',pic:'',nv:''},{av:'Ddo_solicitacoes_codigo_Sortedstatus',ctrl:'DDO_SOLICITACOES_CODIGO',prop:'SortedStatus'},{av:'Ddo_contratada_codigo_Sortedstatus',ctrl:'DDO_CONTRATADA_CODIGO',prop:'SortedStatus'},{av:'Ddo_sistema_codigo_Sortedstatus',ctrl:'DDO_SISTEMA_CODIGO',prop:'SortedStatus'},{av:'Ddo_servico_codigo_Sortedstatus',ctrl:'DDO_SERVICO_CODIGO',prop:'SortedStatus'},{av:'Ddo_solicitacoes_objetivo_Sortedstatus',ctrl:'DDO_SOLICITACOES_OBJETIVO',prop:'SortedStatus'},{av:'Ddo_solicitacoes_usuario_Sortedstatus',ctrl:'DDO_SOLICITACOES_USUARIO',prop:'SortedStatus'},{av:'Ddo_solicitacoes_data_Sortedstatus',ctrl:'DDO_SOLICITACOES_DATA',prop:'SortedStatus'},{av:'Ddo_solicitacoes_usuario_ult_Sortedstatus',ctrl:'DDO_SOLICITACOES_USUARIO_ULT',prop:'SortedStatus'},{av:'Ddo_solicitacoes_data_ult_Sortedstatus',ctrl:'DDO_SOLICITACOES_DATA_ULT',prop:'SortedStatus'},{av:'Ddo_solicitacoes_status_Sortedstatus',ctrl:'DDO_SOLICITACOES_STATUS',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_SOLICITACOES_OBJETIVO.ONOPTIONCLICKED","{handler:'E17AW2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Solicitacoes_Novo_Projeto1',fld:'vSOLICITACOES_NOVO_PROJETO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21Solicitacoes_Novo_Projeto2',fld:'vSOLICITACOES_NOVO_PROJETO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25Solicitacoes_Novo_Projeto3',fld:'vSOLICITACOES_NOVO_PROJETO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31TFSolicitacoes_Codigo',fld:'vTFSOLICITACOES_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32TFSolicitacoes_Codigo_To',fld:'vTFSOLICITACOES_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV35TFContratada_Codigo',fld:'vTFCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV36TFContratada_Codigo_To',fld:'vTFCONTRATADA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFSistema_Codigo',fld:'vTFSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV40TFSistema_Codigo_To',fld:'vTFSISTEMA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV43TFServico_Codigo',fld:'vTFSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV44TFServico_Codigo_To',fld:'vTFSERVICO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV47TFSolicitacoes_Novo_Projeto_Sel',fld:'vTFSOLICITACOES_NOVO_PROJETO_SEL',pic:'',nv:''},{av:'AV50TFSolicitacoes_Objetivo',fld:'vTFSOLICITACOES_OBJETIVO',pic:'',nv:''},{av:'AV51TFSolicitacoes_Objetivo_Sel',fld:'vTFSOLICITACOES_OBJETIVO_SEL',pic:'',nv:''},{av:'AV54TFSolicitacoes_Usuario',fld:'vTFSOLICITACOES_USUARIO',pic:'ZZZZZ9',nv:0},{av:'AV55TFSolicitacoes_Usuario_To',fld:'vTFSOLICITACOES_USUARIO_TO',pic:'ZZZZZ9',nv:0},{av:'AV58TFSolicitacoes_Data',fld:'vTFSOLICITACOES_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV59TFSolicitacoes_Data_To',fld:'vTFSOLICITACOES_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV64TFSolicitacoes_Usuario_Ult',fld:'vTFSOLICITACOES_USUARIO_ULT',pic:'ZZZZZ9',nv:0},{av:'AV65TFSolicitacoes_Usuario_Ult_To',fld:'vTFSOLICITACOES_USUARIO_ULT_TO',pic:'ZZZZZ9',nv:0},{av:'AV68TFSolicitacoes_Data_Ult',fld:'vTFSOLICITACOES_DATA_ULT',pic:'99/99/99 99:99',nv:''},{av:'AV69TFSolicitacoes_Data_Ult_To',fld:'vTFSOLICITACOES_DATA_ULT_TO',pic:'99/99/99 99:99',nv:''},{av:'AV33ddo_Solicitacoes_CodigoTitleControlIdToReplace',fld:'vDDO_SOLICITACOES_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_Contratada_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATADA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_Sistema_CodigoTitleControlIdToReplace',fld:'vDDO_SISTEMA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_Servico_CodigoTitleControlIdToReplace',fld:'vDDO_SERVICO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_Solicitacoes_Novo_ProjetoTitleControlIdToReplace',fld:'vDDO_SOLICITACOES_NOVO_PROJETOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_Solicitacoes_ObjetivoTitleControlIdToReplace',fld:'vDDO_SOLICITACOES_OBJETIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_Solicitacoes_UsuarioTitleControlIdToReplace',fld:'vDDO_SOLICITACOES_USUARIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV62ddo_Solicitacoes_DataTitleControlIdToReplace',fld:'vDDO_SOLICITACOES_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_Solicitacoes_Usuario_UltTitleControlIdToReplace',fld:'vDDO_SOLICITACOES_USUARIO_ULTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_Solicitacoes_Data_UltTitleControlIdToReplace',fld:'vDDO_SOLICITACOES_DATA_ULTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV76ddo_Solicitacoes_StatusTitleControlIdToReplace',fld:'vDDO_SOLICITACOES_STATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75TFSolicitacoes_Status_Sels',fld:'vTFSOLICITACOES_STATUS_SELS',pic:'',nv:null},{av:'AV84Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_solicitacoes_objetivo_Activeeventkey',ctrl:'DDO_SOLICITACOES_OBJETIVO',prop:'ActiveEventKey'},{av:'Ddo_solicitacoes_objetivo_Filteredtext_get',ctrl:'DDO_SOLICITACOES_OBJETIVO',prop:'FilteredText_get'},{av:'Ddo_solicitacoes_objetivo_Selectedvalue_get',ctrl:'DDO_SOLICITACOES_OBJETIVO',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_solicitacoes_objetivo_Sortedstatus',ctrl:'DDO_SOLICITACOES_OBJETIVO',prop:'SortedStatus'},{av:'AV50TFSolicitacoes_Objetivo',fld:'vTFSOLICITACOES_OBJETIVO',pic:'',nv:''},{av:'AV51TFSolicitacoes_Objetivo_Sel',fld:'vTFSOLICITACOES_OBJETIVO_SEL',pic:'',nv:''},{av:'Ddo_solicitacoes_codigo_Sortedstatus',ctrl:'DDO_SOLICITACOES_CODIGO',prop:'SortedStatus'},{av:'Ddo_contratada_codigo_Sortedstatus',ctrl:'DDO_CONTRATADA_CODIGO',prop:'SortedStatus'},{av:'Ddo_sistema_codigo_Sortedstatus',ctrl:'DDO_SISTEMA_CODIGO',prop:'SortedStatus'},{av:'Ddo_servico_codigo_Sortedstatus',ctrl:'DDO_SERVICO_CODIGO',prop:'SortedStatus'},{av:'Ddo_solicitacoes_novo_projeto_Sortedstatus',ctrl:'DDO_SOLICITACOES_NOVO_PROJETO',prop:'SortedStatus'},{av:'Ddo_solicitacoes_usuario_Sortedstatus',ctrl:'DDO_SOLICITACOES_USUARIO',prop:'SortedStatus'},{av:'Ddo_solicitacoes_data_Sortedstatus',ctrl:'DDO_SOLICITACOES_DATA',prop:'SortedStatus'},{av:'Ddo_solicitacoes_usuario_ult_Sortedstatus',ctrl:'DDO_SOLICITACOES_USUARIO_ULT',prop:'SortedStatus'},{av:'Ddo_solicitacoes_data_ult_Sortedstatus',ctrl:'DDO_SOLICITACOES_DATA_ULT',prop:'SortedStatus'},{av:'Ddo_solicitacoes_status_Sortedstatus',ctrl:'DDO_SOLICITACOES_STATUS',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_SOLICITACOES_USUARIO.ONOPTIONCLICKED","{handler:'E18AW2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Solicitacoes_Novo_Projeto1',fld:'vSOLICITACOES_NOVO_PROJETO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21Solicitacoes_Novo_Projeto2',fld:'vSOLICITACOES_NOVO_PROJETO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25Solicitacoes_Novo_Projeto3',fld:'vSOLICITACOES_NOVO_PROJETO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31TFSolicitacoes_Codigo',fld:'vTFSOLICITACOES_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32TFSolicitacoes_Codigo_To',fld:'vTFSOLICITACOES_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV35TFContratada_Codigo',fld:'vTFCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV36TFContratada_Codigo_To',fld:'vTFCONTRATADA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFSistema_Codigo',fld:'vTFSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV40TFSistema_Codigo_To',fld:'vTFSISTEMA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV43TFServico_Codigo',fld:'vTFSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV44TFServico_Codigo_To',fld:'vTFSERVICO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV47TFSolicitacoes_Novo_Projeto_Sel',fld:'vTFSOLICITACOES_NOVO_PROJETO_SEL',pic:'',nv:''},{av:'AV50TFSolicitacoes_Objetivo',fld:'vTFSOLICITACOES_OBJETIVO',pic:'',nv:''},{av:'AV51TFSolicitacoes_Objetivo_Sel',fld:'vTFSOLICITACOES_OBJETIVO_SEL',pic:'',nv:''},{av:'AV54TFSolicitacoes_Usuario',fld:'vTFSOLICITACOES_USUARIO',pic:'ZZZZZ9',nv:0},{av:'AV55TFSolicitacoes_Usuario_To',fld:'vTFSOLICITACOES_USUARIO_TO',pic:'ZZZZZ9',nv:0},{av:'AV58TFSolicitacoes_Data',fld:'vTFSOLICITACOES_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV59TFSolicitacoes_Data_To',fld:'vTFSOLICITACOES_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV64TFSolicitacoes_Usuario_Ult',fld:'vTFSOLICITACOES_USUARIO_ULT',pic:'ZZZZZ9',nv:0},{av:'AV65TFSolicitacoes_Usuario_Ult_To',fld:'vTFSOLICITACOES_USUARIO_ULT_TO',pic:'ZZZZZ9',nv:0},{av:'AV68TFSolicitacoes_Data_Ult',fld:'vTFSOLICITACOES_DATA_ULT',pic:'99/99/99 99:99',nv:''},{av:'AV69TFSolicitacoes_Data_Ult_To',fld:'vTFSOLICITACOES_DATA_ULT_TO',pic:'99/99/99 99:99',nv:''},{av:'AV33ddo_Solicitacoes_CodigoTitleControlIdToReplace',fld:'vDDO_SOLICITACOES_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_Contratada_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATADA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_Sistema_CodigoTitleControlIdToReplace',fld:'vDDO_SISTEMA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_Servico_CodigoTitleControlIdToReplace',fld:'vDDO_SERVICO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_Solicitacoes_Novo_ProjetoTitleControlIdToReplace',fld:'vDDO_SOLICITACOES_NOVO_PROJETOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_Solicitacoes_ObjetivoTitleControlIdToReplace',fld:'vDDO_SOLICITACOES_OBJETIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_Solicitacoes_UsuarioTitleControlIdToReplace',fld:'vDDO_SOLICITACOES_USUARIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV62ddo_Solicitacoes_DataTitleControlIdToReplace',fld:'vDDO_SOLICITACOES_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_Solicitacoes_Usuario_UltTitleControlIdToReplace',fld:'vDDO_SOLICITACOES_USUARIO_ULTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_Solicitacoes_Data_UltTitleControlIdToReplace',fld:'vDDO_SOLICITACOES_DATA_ULTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV76ddo_Solicitacoes_StatusTitleControlIdToReplace',fld:'vDDO_SOLICITACOES_STATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75TFSolicitacoes_Status_Sels',fld:'vTFSOLICITACOES_STATUS_SELS',pic:'',nv:null},{av:'AV84Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_solicitacoes_usuario_Activeeventkey',ctrl:'DDO_SOLICITACOES_USUARIO',prop:'ActiveEventKey'},{av:'Ddo_solicitacoes_usuario_Filteredtext_get',ctrl:'DDO_SOLICITACOES_USUARIO',prop:'FilteredText_get'},{av:'Ddo_solicitacoes_usuario_Filteredtextto_get',ctrl:'DDO_SOLICITACOES_USUARIO',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_solicitacoes_usuario_Sortedstatus',ctrl:'DDO_SOLICITACOES_USUARIO',prop:'SortedStatus'},{av:'AV54TFSolicitacoes_Usuario',fld:'vTFSOLICITACOES_USUARIO',pic:'ZZZZZ9',nv:0},{av:'AV55TFSolicitacoes_Usuario_To',fld:'vTFSOLICITACOES_USUARIO_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_solicitacoes_codigo_Sortedstatus',ctrl:'DDO_SOLICITACOES_CODIGO',prop:'SortedStatus'},{av:'Ddo_contratada_codigo_Sortedstatus',ctrl:'DDO_CONTRATADA_CODIGO',prop:'SortedStatus'},{av:'Ddo_sistema_codigo_Sortedstatus',ctrl:'DDO_SISTEMA_CODIGO',prop:'SortedStatus'},{av:'Ddo_servico_codigo_Sortedstatus',ctrl:'DDO_SERVICO_CODIGO',prop:'SortedStatus'},{av:'Ddo_solicitacoes_novo_projeto_Sortedstatus',ctrl:'DDO_SOLICITACOES_NOVO_PROJETO',prop:'SortedStatus'},{av:'Ddo_solicitacoes_objetivo_Sortedstatus',ctrl:'DDO_SOLICITACOES_OBJETIVO',prop:'SortedStatus'},{av:'Ddo_solicitacoes_data_Sortedstatus',ctrl:'DDO_SOLICITACOES_DATA',prop:'SortedStatus'},{av:'Ddo_solicitacoes_usuario_ult_Sortedstatus',ctrl:'DDO_SOLICITACOES_USUARIO_ULT',prop:'SortedStatus'},{av:'Ddo_solicitacoes_data_ult_Sortedstatus',ctrl:'DDO_SOLICITACOES_DATA_ULT',prop:'SortedStatus'},{av:'Ddo_solicitacoes_status_Sortedstatus',ctrl:'DDO_SOLICITACOES_STATUS',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_SOLICITACOES_DATA.ONOPTIONCLICKED","{handler:'E19AW2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Solicitacoes_Novo_Projeto1',fld:'vSOLICITACOES_NOVO_PROJETO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21Solicitacoes_Novo_Projeto2',fld:'vSOLICITACOES_NOVO_PROJETO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25Solicitacoes_Novo_Projeto3',fld:'vSOLICITACOES_NOVO_PROJETO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31TFSolicitacoes_Codigo',fld:'vTFSOLICITACOES_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32TFSolicitacoes_Codigo_To',fld:'vTFSOLICITACOES_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV35TFContratada_Codigo',fld:'vTFCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV36TFContratada_Codigo_To',fld:'vTFCONTRATADA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFSistema_Codigo',fld:'vTFSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV40TFSistema_Codigo_To',fld:'vTFSISTEMA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV43TFServico_Codigo',fld:'vTFSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV44TFServico_Codigo_To',fld:'vTFSERVICO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV47TFSolicitacoes_Novo_Projeto_Sel',fld:'vTFSOLICITACOES_NOVO_PROJETO_SEL',pic:'',nv:''},{av:'AV50TFSolicitacoes_Objetivo',fld:'vTFSOLICITACOES_OBJETIVO',pic:'',nv:''},{av:'AV51TFSolicitacoes_Objetivo_Sel',fld:'vTFSOLICITACOES_OBJETIVO_SEL',pic:'',nv:''},{av:'AV54TFSolicitacoes_Usuario',fld:'vTFSOLICITACOES_USUARIO',pic:'ZZZZZ9',nv:0},{av:'AV55TFSolicitacoes_Usuario_To',fld:'vTFSOLICITACOES_USUARIO_TO',pic:'ZZZZZ9',nv:0},{av:'AV58TFSolicitacoes_Data',fld:'vTFSOLICITACOES_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV59TFSolicitacoes_Data_To',fld:'vTFSOLICITACOES_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV64TFSolicitacoes_Usuario_Ult',fld:'vTFSOLICITACOES_USUARIO_ULT',pic:'ZZZZZ9',nv:0},{av:'AV65TFSolicitacoes_Usuario_Ult_To',fld:'vTFSOLICITACOES_USUARIO_ULT_TO',pic:'ZZZZZ9',nv:0},{av:'AV68TFSolicitacoes_Data_Ult',fld:'vTFSOLICITACOES_DATA_ULT',pic:'99/99/99 99:99',nv:''},{av:'AV69TFSolicitacoes_Data_Ult_To',fld:'vTFSOLICITACOES_DATA_ULT_TO',pic:'99/99/99 99:99',nv:''},{av:'AV33ddo_Solicitacoes_CodigoTitleControlIdToReplace',fld:'vDDO_SOLICITACOES_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_Contratada_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATADA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_Sistema_CodigoTitleControlIdToReplace',fld:'vDDO_SISTEMA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_Servico_CodigoTitleControlIdToReplace',fld:'vDDO_SERVICO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_Solicitacoes_Novo_ProjetoTitleControlIdToReplace',fld:'vDDO_SOLICITACOES_NOVO_PROJETOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_Solicitacoes_ObjetivoTitleControlIdToReplace',fld:'vDDO_SOLICITACOES_OBJETIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_Solicitacoes_UsuarioTitleControlIdToReplace',fld:'vDDO_SOLICITACOES_USUARIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV62ddo_Solicitacoes_DataTitleControlIdToReplace',fld:'vDDO_SOLICITACOES_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_Solicitacoes_Usuario_UltTitleControlIdToReplace',fld:'vDDO_SOLICITACOES_USUARIO_ULTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_Solicitacoes_Data_UltTitleControlIdToReplace',fld:'vDDO_SOLICITACOES_DATA_ULTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV76ddo_Solicitacoes_StatusTitleControlIdToReplace',fld:'vDDO_SOLICITACOES_STATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75TFSolicitacoes_Status_Sels',fld:'vTFSOLICITACOES_STATUS_SELS',pic:'',nv:null},{av:'AV84Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_solicitacoes_data_Activeeventkey',ctrl:'DDO_SOLICITACOES_DATA',prop:'ActiveEventKey'},{av:'Ddo_solicitacoes_data_Filteredtext_get',ctrl:'DDO_SOLICITACOES_DATA',prop:'FilteredText_get'},{av:'Ddo_solicitacoes_data_Filteredtextto_get',ctrl:'DDO_SOLICITACOES_DATA',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_solicitacoes_data_Sortedstatus',ctrl:'DDO_SOLICITACOES_DATA',prop:'SortedStatus'},{av:'AV58TFSolicitacoes_Data',fld:'vTFSOLICITACOES_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV59TFSolicitacoes_Data_To',fld:'vTFSOLICITACOES_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'Ddo_solicitacoes_codigo_Sortedstatus',ctrl:'DDO_SOLICITACOES_CODIGO',prop:'SortedStatus'},{av:'Ddo_contratada_codigo_Sortedstatus',ctrl:'DDO_CONTRATADA_CODIGO',prop:'SortedStatus'},{av:'Ddo_sistema_codigo_Sortedstatus',ctrl:'DDO_SISTEMA_CODIGO',prop:'SortedStatus'},{av:'Ddo_servico_codigo_Sortedstatus',ctrl:'DDO_SERVICO_CODIGO',prop:'SortedStatus'},{av:'Ddo_solicitacoes_novo_projeto_Sortedstatus',ctrl:'DDO_SOLICITACOES_NOVO_PROJETO',prop:'SortedStatus'},{av:'Ddo_solicitacoes_objetivo_Sortedstatus',ctrl:'DDO_SOLICITACOES_OBJETIVO',prop:'SortedStatus'},{av:'Ddo_solicitacoes_usuario_Sortedstatus',ctrl:'DDO_SOLICITACOES_USUARIO',prop:'SortedStatus'},{av:'Ddo_solicitacoes_usuario_ult_Sortedstatus',ctrl:'DDO_SOLICITACOES_USUARIO_ULT',prop:'SortedStatus'},{av:'Ddo_solicitacoes_data_ult_Sortedstatus',ctrl:'DDO_SOLICITACOES_DATA_ULT',prop:'SortedStatus'},{av:'Ddo_solicitacoes_status_Sortedstatus',ctrl:'DDO_SOLICITACOES_STATUS',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_SOLICITACOES_USUARIO_ULT.ONOPTIONCLICKED","{handler:'E20AW2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Solicitacoes_Novo_Projeto1',fld:'vSOLICITACOES_NOVO_PROJETO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21Solicitacoes_Novo_Projeto2',fld:'vSOLICITACOES_NOVO_PROJETO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25Solicitacoes_Novo_Projeto3',fld:'vSOLICITACOES_NOVO_PROJETO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31TFSolicitacoes_Codigo',fld:'vTFSOLICITACOES_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32TFSolicitacoes_Codigo_To',fld:'vTFSOLICITACOES_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV35TFContratada_Codigo',fld:'vTFCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV36TFContratada_Codigo_To',fld:'vTFCONTRATADA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFSistema_Codigo',fld:'vTFSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV40TFSistema_Codigo_To',fld:'vTFSISTEMA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV43TFServico_Codigo',fld:'vTFSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV44TFServico_Codigo_To',fld:'vTFSERVICO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV47TFSolicitacoes_Novo_Projeto_Sel',fld:'vTFSOLICITACOES_NOVO_PROJETO_SEL',pic:'',nv:''},{av:'AV50TFSolicitacoes_Objetivo',fld:'vTFSOLICITACOES_OBJETIVO',pic:'',nv:''},{av:'AV51TFSolicitacoes_Objetivo_Sel',fld:'vTFSOLICITACOES_OBJETIVO_SEL',pic:'',nv:''},{av:'AV54TFSolicitacoes_Usuario',fld:'vTFSOLICITACOES_USUARIO',pic:'ZZZZZ9',nv:0},{av:'AV55TFSolicitacoes_Usuario_To',fld:'vTFSOLICITACOES_USUARIO_TO',pic:'ZZZZZ9',nv:0},{av:'AV58TFSolicitacoes_Data',fld:'vTFSOLICITACOES_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV59TFSolicitacoes_Data_To',fld:'vTFSOLICITACOES_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV64TFSolicitacoes_Usuario_Ult',fld:'vTFSOLICITACOES_USUARIO_ULT',pic:'ZZZZZ9',nv:0},{av:'AV65TFSolicitacoes_Usuario_Ult_To',fld:'vTFSOLICITACOES_USUARIO_ULT_TO',pic:'ZZZZZ9',nv:0},{av:'AV68TFSolicitacoes_Data_Ult',fld:'vTFSOLICITACOES_DATA_ULT',pic:'99/99/99 99:99',nv:''},{av:'AV69TFSolicitacoes_Data_Ult_To',fld:'vTFSOLICITACOES_DATA_ULT_TO',pic:'99/99/99 99:99',nv:''},{av:'AV33ddo_Solicitacoes_CodigoTitleControlIdToReplace',fld:'vDDO_SOLICITACOES_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_Contratada_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATADA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_Sistema_CodigoTitleControlIdToReplace',fld:'vDDO_SISTEMA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_Servico_CodigoTitleControlIdToReplace',fld:'vDDO_SERVICO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_Solicitacoes_Novo_ProjetoTitleControlIdToReplace',fld:'vDDO_SOLICITACOES_NOVO_PROJETOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_Solicitacoes_ObjetivoTitleControlIdToReplace',fld:'vDDO_SOLICITACOES_OBJETIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_Solicitacoes_UsuarioTitleControlIdToReplace',fld:'vDDO_SOLICITACOES_USUARIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV62ddo_Solicitacoes_DataTitleControlIdToReplace',fld:'vDDO_SOLICITACOES_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_Solicitacoes_Usuario_UltTitleControlIdToReplace',fld:'vDDO_SOLICITACOES_USUARIO_ULTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_Solicitacoes_Data_UltTitleControlIdToReplace',fld:'vDDO_SOLICITACOES_DATA_ULTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV76ddo_Solicitacoes_StatusTitleControlIdToReplace',fld:'vDDO_SOLICITACOES_STATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75TFSolicitacoes_Status_Sels',fld:'vTFSOLICITACOES_STATUS_SELS',pic:'',nv:null},{av:'AV84Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_solicitacoes_usuario_ult_Activeeventkey',ctrl:'DDO_SOLICITACOES_USUARIO_ULT',prop:'ActiveEventKey'},{av:'Ddo_solicitacoes_usuario_ult_Filteredtext_get',ctrl:'DDO_SOLICITACOES_USUARIO_ULT',prop:'FilteredText_get'},{av:'Ddo_solicitacoes_usuario_ult_Filteredtextto_get',ctrl:'DDO_SOLICITACOES_USUARIO_ULT',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_solicitacoes_usuario_ult_Sortedstatus',ctrl:'DDO_SOLICITACOES_USUARIO_ULT',prop:'SortedStatus'},{av:'AV64TFSolicitacoes_Usuario_Ult',fld:'vTFSOLICITACOES_USUARIO_ULT',pic:'ZZZZZ9',nv:0},{av:'AV65TFSolicitacoes_Usuario_Ult_To',fld:'vTFSOLICITACOES_USUARIO_ULT_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_solicitacoes_codigo_Sortedstatus',ctrl:'DDO_SOLICITACOES_CODIGO',prop:'SortedStatus'},{av:'Ddo_contratada_codigo_Sortedstatus',ctrl:'DDO_CONTRATADA_CODIGO',prop:'SortedStatus'},{av:'Ddo_sistema_codigo_Sortedstatus',ctrl:'DDO_SISTEMA_CODIGO',prop:'SortedStatus'},{av:'Ddo_servico_codigo_Sortedstatus',ctrl:'DDO_SERVICO_CODIGO',prop:'SortedStatus'},{av:'Ddo_solicitacoes_novo_projeto_Sortedstatus',ctrl:'DDO_SOLICITACOES_NOVO_PROJETO',prop:'SortedStatus'},{av:'Ddo_solicitacoes_objetivo_Sortedstatus',ctrl:'DDO_SOLICITACOES_OBJETIVO',prop:'SortedStatus'},{av:'Ddo_solicitacoes_usuario_Sortedstatus',ctrl:'DDO_SOLICITACOES_USUARIO',prop:'SortedStatus'},{av:'Ddo_solicitacoes_data_Sortedstatus',ctrl:'DDO_SOLICITACOES_DATA',prop:'SortedStatus'},{av:'Ddo_solicitacoes_data_ult_Sortedstatus',ctrl:'DDO_SOLICITACOES_DATA_ULT',prop:'SortedStatus'},{av:'Ddo_solicitacoes_status_Sortedstatus',ctrl:'DDO_SOLICITACOES_STATUS',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_SOLICITACOES_DATA_ULT.ONOPTIONCLICKED","{handler:'E21AW2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Solicitacoes_Novo_Projeto1',fld:'vSOLICITACOES_NOVO_PROJETO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21Solicitacoes_Novo_Projeto2',fld:'vSOLICITACOES_NOVO_PROJETO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25Solicitacoes_Novo_Projeto3',fld:'vSOLICITACOES_NOVO_PROJETO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31TFSolicitacoes_Codigo',fld:'vTFSOLICITACOES_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32TFSolicitacoes_Codigo_To',fld:'vTFSOLICITACOES_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV35TFContratada_Codigo',fld:'vTFCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV36TFContratada_Codigo_To',fld:'vTFCONTRATADA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFSistema_Codigo',fld:'vTFSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV40TFSistema_Codigo_To',fld:'vTFSISTEMA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV43TFServico_Codigo',fld:'vTFSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV44TFServico_Codigo_To',fld:'vTFSERVICO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV47TFSolicitacoes_Novo_Projeto_Sel',fld:'vTFSOLICITACOES_NOVO_PROJETO_SEL',pic:'',nv:''},{av:'AV50TFSolicitacoes_Objetivo',fld:'vTFSOLICITACOES_OBJETIVO',pic:'',nv:''},{av:'AV51TFSolicitacoes_Objetivo_Sel',fld:'vTFSOLICITACOES_OBJETIVO_SEL',pic:'',nv:''},{av:'AV54TFSolicitacoes_Usuario',fld:'vTFSOLICITACOES_USUARIO',pic:'ZZZZZ9',nv:0},{av:'AV55TFSolicitacoes_Usuario_To',fld:'vTFSOLICITACOES_USUARIO_TO',pic:'ZZZZZ9',nv:0},{av:'AV58TFSolicitacoes_Data',fld:'vTFSOLICITACOES_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV59TFSolicitacoes_Data_To',fld:'vTFSOLICITACOES_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV64TFSolicitacoes_Usuario_Ult',fld:'vTFSOLICITACOES_USUARIO_ULT',pic:'ZZZZZ9',nv:0},{av:'AV65TFSolicitacoes_Usuario_Ult_To',fld:'vTFSOLICITACOES_USUARIO_ULT_TO',pic:'ZZZZZ9',nv:0},{av:'AV68TFSolicitacoes_Data_Ult',fld:'vTFSOLICITACOES_DATA_ULT',pic:'99/99/99 99:99',nv:''},{av:'AV69TFSolicitacoes_Data_Ult_To',fld:'vTFSOLICITACOES_DATA_ULT_TO',pic:'99/99/99 99:99',nv:''},{av:'AV33ddo_Solicitacoes_CodigoTitleControlIdToReplace',fld:'vDDO_SOLICITACOES_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_Contratada_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATADA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_Sistema_CodigoTitleControlIdToReplace',fld:'vDDO_SISTEMA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_Servico_CodigoTitleControlIdToReplace',fld:'vDDO_SERVICO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_Solicitacoes_Novo_ProjetoTitleControlIdToReplace',fld:'vDDO_SOLICITACOES_NOVO_PROJETOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_Solicitacoes_ObjetivoTitleControlIdToReplace',fld:'vDDO_SOLICITACOES_OBJETIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_Solicitacoes_UsuarioTitleControlIdToReplace',fld:'vDDO_SOLICITACOES_USUARIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV62ddo_Solicitacoes_DataTitleControlIdToReplace',fld:'vDDO_SOLICITACOES_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_Solicitacoes_Usuario_UltTitleControlIdToReplace',fld:'vDDO_SOLICITACOES_USUARIO_ULTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_Solicitacoes_Data_UltTitleControlIdToReplace',fld:'vDDO_SOLICITACOES_DATA_ULTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV76ddo_Solicitacoes_StatusTitleControlIdToReplace',fld:'vDDO_SOLICITACOES_STATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75TFSolicitacoes_Status_Sels',fld:'vTFSOLICITACOES_STATUS_SELS',pic:'',nv:null},{av:'AV84Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_solicitacoes_data_ult_Activeeventkey',ctrl:'DDO_SOLICITACOES_DATA_ULT',prop:'ActiveEventKey'},{av:'Ddo_solicitacoes_data_ult_Filteredtext_get',ctrl:'DDO_SOLICITACOES_DATA_ULT',prop:'FilteredText_get'},{av:'Ddo_solicitacoes_data_ult_Filteredtextto_get',ctrl:'DDO_SOLICITACOES_DATA_ULT',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_solicitacoes_data_ult_Sortedstatus',ctrl:'DDO_SOLICITACOES_DATA_ULT',prop:'SortedStatus'},{av:'AV68TFSolicitacoes_Data_Ult',fld:'vTFSOLICITACOES_DATA_ULT',pic:'99/99/99 99:99',nv:''},{av:'AV69TFSolicitacoes_Data_Ult_To',fld:'vTFSOLICITACOES_DATA_ULT_TO',pic:'99/99/99 99:99',nv:''},{av:'Ddo_solicitacoes_codigo_Sortedstatus',ctrl:'DDO_SOLICITACOES_CODIGO',prop:'SortedStatus'},{av:'Ddo_contratada_codigo_Sortedstatus',ctrl:'DDO_CONTRATADA_CODIGO',prop:'SortedStatus'},{av:'Ddo_sistema_codigo_Sortedstatus',ctrl:'DDO_SISTEMA_CODIGO',prop:'SortedStatus'},{av:'Ddo_servico_codigo_Sortedstatus',ctrl:'DDO_SERVICO_CODIGO',prop:'SortedStatus'},{av:'Ddo_solicitacoes_novo_projeto_Sortedstatus',ctrl:'DDO_SOLICITACOES_NOVO_PROJETO',prop:'SortedStatus'},{av:'Ddo_solicitacoes_objetivo_Sortedstatus',ctrl:'DDO_SOLICITACOES_OBJETIVO',prop:'SortedStatus'},{av:'Ddo_solicitacoes_usuario_Sortedstatus',ctrl:'DDO_SOLICITACOES_USUARIO',prop:'SortedStatus'},{av:'Ddo_solicitacoes_data_Sortedstatus',ctrl:'DDO_SOLICITACOES_DATA',prop:'SortedStatus'},{av:'Ddo_solicitacoes_usuario_ult_Sortedstatus',ctrl:'DDO_SOLICITACOES_USUARIO_ULT',prop:'SortedStatus'},{av:'Ddo_solicitacoes_status_Sortedstatus',ctrl:'DDO_SOLICITACOES_STATUS',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_SOLICITACOES_STATUS.ONOPTIONCLICKED","{handler:'E22AW2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Solicitacoes_Novo_Projeto1',fld:'vSOLICITACOES_NOVO_PROJETO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21Solicitacoes_Novo_Projeto2',fld:'vSOLICITACOES_NOVO_PROJETO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25Solicitacoes_Novo_Projeto3',fld:'vSOLICITACOES_NOVO_PROJETO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31TFSolicitacoes_Codigo',fld:'vTFSOLICITACOES_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32TFSolicitacoes_Codigo_To',fld:'vTFSOLICITACOES_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV35TFContratada_Codigo',fld:'vTFCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV36TFContratada_Codigo_To',fld:'vTFCONTRATADA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFSistema_Codigo',fld:'vTFSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV40TFSistema_Codigo_To',fld:'vTFSISTEMA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV43TFServico_Codigo',fld:'vTFSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV44TFServico_Codigo_To',fld:'vTFSERVICO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV47TFSolicitacoes_Novo_Projeto_Sel',fld:'vTFSOLICITACOES_NOVO_PROJETO_SEL',pic:'',nv:''},{av:'AV50TFSolicitacoes_Objetivo',fld:'vTFSOLICITACOES_OBJETIVO',pic:'',nv:''},{av:'AV51TFSolicitacoes_Objetivo_Sel',fld:'vTFSOLICITACOES_OBJETIVO_SEL',pic:'',nv:''},{av:'AV54TFSolicitacoes_Usuario',fld:'vTFSOLICITACOES_USUARIO',pic:'ZZZZZ9',nv:0},{av:'AV55TFSolicitacoes_Usuario_To',fld:'vTFSOLICITACOES_USUARIO_TO',pic:'ZZZZZ9',nv:0},{av:'AV58TFSolicitacoes_Data',fld:'vTFSOLICITACOES_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV59TFSolicitacoes_Data_To',fld:'vTFSOLICITACOES_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV64TFSolicitacoes_Usuario_Ult',fld:'vTFSOLICITACOES_USUARIO_ULT',pic:'ZZZZZ9',nv:0},{av:'AV65TFSolicitacoes_Usuario_Ult_To',fld:'vTFSOLICITACOES_USUARIO_ULT_TO',pic:'ZZZZZ9',nv:0},{av:'AV68TFSolicitacoes_Data_Ult',fld:'vTFSOLICITACOES_DATA_ULT',pic:'99/99/99 99:99',nv:''},{av:'AV69TFSolicitacoes_Data_Ult_To',fld:'vTFSOLICITACOES_DATA_ULT_TO',pic:'99/99/99 99:99',nv:''},{av:'AV33ddo_Solicitacoes_CodigoTitleControlIdToReplace',fld:'vDDO_SOLICITACOES_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_Contratada_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATADA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_Sistema_CodigoTitleControlIdToReplace',fld:'vDDO_SISTEMA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_Servico_CodigoTitleControlIdToReplace',fld:'vDDO_SERVICO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_Solicitacoes_Novo_ProjetoTitleControlIdToReplace',fld:'vDDO_SOLICITACOES_NOVO_PROJETOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_Solicitacoes_ObjetivoTitleControlIdToReplace',fld:'vDDO_SOLICITACOES_OBJETIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_Solicitacoes_UsuarioTitleControlIdToReplace',fld:'vDDO_SOLICITACOES_USUARIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV62ddo_Solicitacoes_DataTitleControlIdToReplace',fld:'vDDO_SOLICITACOES_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_Solicitacoes_Usuario_UltTitleControlIdToReplace',fld:'vDDO_SOLICITACOES_USUARIO_ULTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_Solicitacoes_Data_UltTitleControlIdToReplace',fld:'vDDO_SOLICITACOES_DATA_ULTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV76ddo_Solicitacoes_StatusTitleControlIdToReplace',fld:'vDDO_SOLICITACOES_STATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75TFSolicitacoes_Status_Sels',fld:'vTFSOLICITACOES_STATUS_SELS',pic:'',nv:null},{av:'AV84Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_solicitacoes_status_Activeeventkey',ctrl:'DDO_SOLICITACOES_STATUS',prop:'ActiveEventKey'},{av:'Ddo_solicitacoes_status_Selectedvalue_get',ctrl:'DDO_SOLICITACOES_STATUS',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_solicitacoes_status_Sortedstatus',ctrl:'DDO_SOLICITACOES_STATUS',prop:'SortedStatus'},{av:'AV75TFSolicitacoes_Status_Sels',fld:'vTFSOLICITACOES_STATUS_SELS',pic:'',nv:null},{av:'Ddo_solicitacoes_codigo_Sortedstatus',ctrl:'DDO_SOLICITACOES_CODIGO',prop:'SortedStatus'},{av:'Ddo_contratada_codigo_Sortedstatus',ctrl:'DDO_CONTRATADA_CODIGO',prop:'SortedStatus'},{av:'Ddo_sistema_codigo_Sortedstatus',ctrl:'DDO_SISTEMA_CODIGO',prop:'SortedStatus'},{av:'Ddo_servico_codigo_Sortedstatus',ctrl:'DDO_SERVICO_CODIGO',prop:'SortedStatus'},{av:'Ddo_solicitacoes_novo_projeto_Sortedstatus',ctrl:'DDO_SOLICITACOES_NOVO_PROJETO',prop:'SortedStatus'},{av:'Ddo_solicitacoes_objetivo_Sortedstatus',ctrl:'DDO_SOLICITACOES_OBJETIVO',prop:'SortedStatus'},{av:'Ddo_solicitacoes_usuario_Sortedstatus',ctrl:'DDO_SOLICITACOES_USUARIO',prop:'SortedStatus'},{av:'Ddo_solicitacoes_data_Sortedstatus',ctrl:'DDO_SOLICITACOES_DATA',prop:'SortedStatus'},{av:'Ddo_solicitacoes_usuario_ult_Sortedstatus',ctrl:'DDO_SOLICITACOES_USUARIO_ULT',prop:'SortedStatus'},{av:'Ddo_solicitacoes_data_ult_Sortedstatus',ctrl:'DDO_SOLICITACOES_DATA_ULT',prop:'SortedStatus'}]}");
         setEventMetadata("GRID.LOAD","{handler:'E35AW2',iparms:[],oparms:[{av:'AV28Select',fld:'vSELECT',pic:'',nv:''},{av:'edtavSelect_Tooltiptext',ctrl:'vSELECT',prop:'Tooltiptext'}]}");
         setEventMetadata("ENTER","{handler:'E36AW2',iparms:[{av:'A439Solicitacoes_Codigo',fld:'SOLICITACOES_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A440Solicitacoes_Novo_Projeto',fld:'SOLICITACOES_NOVO_PROJETO',pic:'',hsh:true,nv:''}],oparms:[{av:'AV7InOutSolicitacoes_Codigo',fld:'vINOUTSOLICITACOES_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV8InOutSolicitacoes_Novo_Projeto',fld:'vINOUTSOLICITACOES_NOVO_PROJETO',pic:'',nv:''}]}");
         setEventMetadata("VORDEREDBY.CLICK","{handler:'E23AW2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Solicitacoes_Novo_Projeto1',fld:'vSOLICITACOES_NOVO_PROJETO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21Solicitacoes_Novo_Projeto2',fld:'vSOLICITACOES_NOVO_PROJETO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25Solicitacoes_Novo_Projeto3',fld:'vSOLICITACOES_NOVO_PROJETO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31TFSolicitacoes_Codigo',fld:'vTFSOLICITACOES_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32TFSolicitacoes_Codigo_To',fld:'vTFSOLICITACOES_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV35TFContratada_Codigo',fld:'vTFCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV36TFContratada_Codigo_To',fld:'vTFCONTRATADA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFSistema_Codigo',fld:'vTFSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV40TFSistema_Codigo_To',fld:'vTFSISTEMA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV43TFServico_Codigo',fld:'vTFSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV44TFServico_Codigo_To',fld:'vTFSERVICO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV47TFSolicitacoes_Novo_Projeto_Sel',fld:'vTFSOLICITACOES_NOVO_PROJETO_SEL',pic:'',nv:''},{av:'AV50TFSolicitacoes_Objetivo',fld:'vTFSOLICITACOES_OBJETIVO',pic:'',nv:''},{av:'AV51TFSolicitacoes_Objetivo_Sel',fld:'vTFSOLICITACOES_OBJETIVO_SEL',pic:'',nv:''},{av:'AV54TFSolicitacoes_Usuario',fld:'vTFSOLICITACOES_USUARIO',pic:'ZZZZZ9',nv:0},{av:'AV55TFSolicitacoes_Usuario_To',fld:'vTFSOLICITACOES_USUARIO_TO',pic:'ZZZZZ9',nv:0},{av:'AV58TFSolicitacoes_Data',fld:'vTFSOLICITACOES_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV59TFSolicitacoes_Data_To',fld:'vTFSOLICITACOES_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV64TFSolicitacoes_Usuario_Ult',fld:'vTFSOLICITACOES_USUARIO_ULT',pic:'ZZZZZ9',nv:0},{av:'AV65TFSolicitacoes_Usuario_Ult_To',fld:'vTFSOLICITACOES_USUARIO_ULT_TO',pic:'ZZZZZ9',nv:0},{av:'AV68TFSolicitacoes_Data_Ult',fld:'vTFSOLICITACOES_DATA_ULT',pic:'99/99/99 99:99',nv:''},{av:'AV69TFSolicitacoes_Data_Ult_To',fld:'vTFSOLICITACOES_DATA_ULT_TO',pic:'99/99/99 99:99',nv:''},{av:'AV33ddo_Solicitacoes_CodigoTitleControlIdToReplace',fld:'vDDO_SOLICITACOES_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_Contratada_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATADA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_Sistema_CodigoTitleControlIdToReplace',fld:'vDDO_SISTEMA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_Servico_CodigoTitleControlIdToReplace',fld:'vDDO_SERVICO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_Solicitacoes_Novo_ProjetoTitleControlIdToReplace',fld:'vDDO_SOLICITACOES_NOVO_PROJETOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_Solicitacoes_ObjetivoTitleControlIdToReplace',fld:'vDDO_SOLICITACOES_OBJETIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_Solicitacoes_UsuarioTitleControlIdToReplace',fld:'vDDO_SOLICITACOES_USUARIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV62ddo_Solicitacoes_DataTitleControlIdToReplace',fld:'vDDO_SOLICITACOES_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_Solicitacoes_Usuario_UltTitleControlIdToReplace',fld:'vDDO_SOLICITACOES_USUARIO_ULTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_Solicitacoes_Data_UltTitleControlIdToReplace',fld:'vDDO_SOLICITACOES_DATA_ULTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV76ddo_Solicitacoes_StatusTitleControlIdToReplace',fld:'vDDO_SOLICITACOES_STATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75TFSolicitacoes_Status_Sels',fld:'vTFSOLICITACOES_STATUS_SELS',pic:'',nv:null},{av:'AV84Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[]}");
         setEventMetadata("'ADDDYNAMICFILTERS1'","{handler:'E28AW2',iparms:[],oparms:[{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS1'","{handler:'E24AW2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Solicitacoes_Novo_Projeto1',fld:'vSOLICITACOES_NOVO_PROJETO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21Solicitacoes_Novo_Projeto2',fld:'vSOLICITACOES_NOVO_PROJETO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25Solicitacoes_Novo_Projeto3',fld:'vSOLICITACOES_NOVO_PROJETO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31TFSolicitacoes_Codigo',fld:'vTFSOLICITACOES_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32TFSolicitacoes_Codigo_To',fld:'vTFSOLICITACOES_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV35TFContratada_Codigo',fld:'vTFCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV36TFContratada_Codigo_To',fld:'vTFCONTRATADA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFSistema_Codigo',fld:'vTFSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV40TFSistema_Codigo_To',fld:'vTFSISTEMA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV43TFServico_Codigo',fld:'vTFSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV44TFServico_Codigo_To',fld:'vTFSERVICO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV47TFSolicitacoes_Novo_Projeto_Sel',fld:'vTFSOLICITACOES_NOVO_PROJETO_SEL',pic:'',nv:''},{av:'AV50TFSolicitacoes_Objetivo',fld:'vTFSOLICITACOES_OBJETIVO',pic:'',nv:''},{av:'AV51TFSolicitacoes_Objetivo_Sel',fld:'vTFSOLICITACOES_OBJETIVO_SEL',pic:'',nv:''},{av:'AV54TFSolicitacoes_Usuario',fld:'vTFSOLICITACOES_USUARIO',pic:'ZZZZZ9',nv:0},{av:'AV55TFSolicitacoes_Usuario_To',fld:'vTFSOLICITACOES_USUARIO_TO',pic:'ZZZZZ9',nv:0},{av:'AV58TFSolicitacoes_Data',fld:'vTFSOLICITACOES_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV59TFSolicitacoes_Data_To',fld:'vTFSOLICITACOES_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV64TFSolicitacoes_Usuario_Ult',fld:'vTFSOLICITACOES_USUARIO_ULT',pic:'ZZZZZ9',nv:0},{av:'AV65TFSolicitacoes_Usuario_Ult_To',fld:'vTFSOLICITACOES_USUARIO_ULT_TO',pic:'ZZZZZ9',nv:0},{av:'AV68TFSolicitacoes_Data_Ult',fld:'vTFSOLICITACOES_DATA_ULT',pic:'99/99/99 99:99',nv:''},{av:'AV69TFSolicitacoes_Data_Ult_To',fld:'vTFSOLICITACOES_DATA_ULT_TO',pic:'99/99/99 99:99',nv:''},{av:'AV33ddo_Solicitacoes_CodigoTitleControlIdToReplace',fld:'vDDO_SOLICITACOES_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_Contratada_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATADA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_Sistema_CodigoTitleControlIdToReplace',fld:'vDDO_SISTEMA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_Servico_CodigoTitleControlIdToReplace',fld:'vDDO_SERVICO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_Solicitacoes_Novo_ProjetoTitleControlIdToReplace',fld:'vDDO_SOLICITACOES_NOVO_PROJETOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_Solicitacoes_ObjetivoTitleControlIdToReplace',fld:'vDDO_SOLICITACOES_OBJETIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_Solicitacoes_UsuarioTitleControlIdToReplace',fld:'vDDO_SOLICITACOES_USUARIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV62ddo_Solicitacoes_DataTitleControlIdToReplace',fld:'vDDO_SOLICITACOES_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_Solicitacoes_Usuario_UltTitleControlIdToReplace',fld:'vDDO_SOLICITACOES_USUARIO_ULTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_Solicitacoes_Data_UltTitleControlIdToReplace',fld:'vDDO_SOLICITACOES_DATA_ULTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV76ddo_Solicitacoes_StatusTitleControlIdToReplace',fld:'vDDO_SOLICITACOES_STATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75TFSolicitacoes_Status_Sels',fld:'vTFSOLICITACOES_STATUS_SELS',pic:'',nv:null},{av:'AV84Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21Solicitacoes_Novo_Projeto2',fld:'vSOLICITACOES_NOVO_PROJETO2',pic:'',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25Solicitacoes_Novo_Projeto3',fld:'vSOLICITACOES_NOVO_PROJETO3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Solicitacoes_Novo_Projeto1',fld:'vSOLICITACOES_NOVO_PROJETO1',pic:'',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'radavSolicitacoes_novo_projeto2'},{av:'cmbavDynamicfiltersoperator2'},{av:'radavSolicitacoes_novo_projeto3'},{av:'cmbavDynamicfiltersoperator3'},{av:'radavSolicitacoes_novo_projeto1'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR1.CLICK","{handler:'E29AW2',iparms:[{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''}],oparms:[{av:'radavSolicitacoes_novo_projeto1'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("'ADDDYNAMICFILTERS2'","{handler:'E30AW2',iparms:[],oparms:[{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS2'","{handler:'E25AW2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Solicitacoes_Novo_Projeto1',fld:'vSOLICITACOES_NOVO_PROJETO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21Solicitacoes_Novo_Projeto2',fld:'vSOLICITACOES_NOVO_PROJETO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25Solicitacoes_Novo_Projeto3',fld:'vSOLICITACOES_NOVO_PROJETO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31TFSolicitacoes_Codigo',fld:'vTFSOLICITACOES_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32TFSolicitacoes_Codigo_To',fld:'vTFSOLICITACOES_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV35TFContratada_Codigo',fld:'vTFCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV36TFContratada_Codigo_To',fld:'vTFCONTRATADA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFSistema_Codigo',fld:'vTFSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV40TFSistema_Codigo_To',fld:'vTFSISTEMA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV43TFServico_Codigo',fld:'vTFSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV44TFServico_Codigo_To',fld:'vTFSERVICO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV47TFSolicitacoes_Novo_Projeto_Sel',fld:'vTFSOLICITACOES_NOVO_PROJETO_SEL',pic:'',nv:''},{av:'AV50TFSolicitacoes_Objetivo',fld:'vTFSOLICITACOES_OBJETIVO',pic:'',nv:''},{av:'AV51TFSolicitacoes_Objetivo_Sel',fld:'vTFSOLICITACOES_OBJETIVO_SEL',pic:'',nv:''},{av:'AV54TFSolicitacoes_Usuario',fld:'vTFSOLICITACOES_USUARIO',pic:'ZZZZZ9',nv:0},{av:'AV55TFSolicitacoes_Usuario_To',fld:'vTFSOLICITACOES_USUARIO_TO',pic:'ZZZZZ9',nv:0},{av:'AV58TFSolicitacoes_Data',fld:'vTFSOLICITACOES_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV59TFSolicitacoes_Data_To',fld:'vTFSOLICITACOES_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV64TFSolicitacoes_Usuario_Ult',fld:'vTFSOLICITACOES_USUARIO_ULT',pic:'ZZZZZ9',nv:0},{av:'AV65TFSolicitacoes_Usuario_Ult_To',fld:'vTFSOLICITACOES_USUARIO_ULT_TO',pic:'ZZZZZ9',nv:0},{av:'AV68TFSolicitacoes_Data_Ult',fld:'vTFSOLICITACOES_DATA_ULT',pic:'99/99/99 99:99',nv:''},{av:'AV69TFSolicitacoes_Data_Ult_To',fld:'vTFSOLICITACOES_DATA_ULT_TO',pic:'99/99/99 99:99',nv:''},{av:'AV33ddo_Solicitacoes_CodigoTitleControlIdToReplace',fld:'vDDO_SOLICITACOES_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_Contratada_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATADA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_Sistema_CodigoTitleControlIdToReplace',fld:'vDDO_SISTEMA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_Servico_CodigoTitleControlIdToReplace',fld:'vDDO_SERVICO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_Solicitacoes_Novo_ProjetoTitleControlIdToReplace',fld:'vDDO_SOLICITACOES_NOVO_PROJETOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_Solicitacoes_ObjetivoTitleControlIdToReplace',fld:'vDDO_SOLICITACOES_OBJETIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_Solicitacoes_UsuarioTitleControlIdToReplace',fld:'vDDO_SOLICITACOES_USUARIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV62ddo_Solicitacoes_DataTitleControlIdToReplace',fld:'vDDO_SOLICITACOES_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_Solicitacoes_Usuario_UltTitleControlIdToReplace',fld:'vDDO_SOLICITACOES_USUARIO_ULTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_Solicitacoes_Data_UltTitleControlIdToReplace',fld:'vDDO_SOLICITACOES_DATA_ULTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV76ddo_Solicitacoes_StatusTitleControlIdToReplace',fld:'vDDO_SOLICITACOES_STATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75TFSolicitacoes_Status_Sels',fld:'vTFSOLICITACOES_STATUS_SELS',pic:'',nv:null},{av:'AV84Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21Solicitacoes_Novo_Projeto2',fld:'vSOLICITACOES_NOVO_PROJETO2',pic:'',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25Solicitacoes_Novo_Projeto3',fld:'vSOLICITACOES_NOVO_PROJETO3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Solicitacoes_Novo_Projeto1',fld:'vSOLICITACOES_NOVO_PROJETO1',pic:'',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'radavSolicitacoes_novo_projeto2'},{av:'cmbavDynamicfiltersoperator2'},{av:'radavSolicitacoes_novo_projeto3'},{av:'cmbavDynamicfiltersoperator3'},{av:'radavSolicitacoes_novo_projeto1'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR2.CLICK","{handler:'E31AW2',iparms:[{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''}],oparms:[{av:'radavSolicitacoes_novo_projeto2'},{av:'cmbavDynamicfiltersoperator2'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS3'","{handler:'E26AW2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Solicitacoes_Novo_Projeto1',fld:'vSOLICITACOES_NOVO_PROJETO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21Solicitacoes_Novo_Projeto2',fld:'vSOLICITACOES_NOVO_PROJETO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25Solicitacoes_Novo_Projeto3',fld:'vSOLICITACOES_NOVO_PROJETO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31TFSolicitacoes_Codigo',fld:'vTFSOLICITACOES_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32TFSolicitacoes_Codigo_To',fld:'vTFSOLICITACOES_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV35TFContratada_Codigo',fld:'vTFCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV36TFContratada_Codigo_To',fld:'vTFCONTRATADA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFSistema_Codigo',fld:'vTFSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV40TFSistema_Codigo_To',fld:'vTFSISTEMA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV43TFServico_Codigo',fld:'vTFSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV44TFServico_Codigo_To',fld:'vTFSERVICO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV47TFSolicitacoes_Novo_Projeto_Sel',fld:'vTFSOLICITACOES_NOVO_PROJETO_SEL',pic:'',nv:''},{av:'AV50TFSolicitacoes_Objetivo',fld:'vTFSOLICITACOES_OBJETIVO',pic:'',nv:''},{av:'AV51TFSolicitacoes_Objetivo_Sel',fld:'vTFSOLICITACOES_OBJETIVO_SEL',pic:'',nv:''},{av:'AV54TFSolicitacoes_Usuario',fld:'vTFSOLICITACOES_USUARIO',pic:'ZZZZZ9',nv:0},{av:'AV55TFSolicitacoes_Usuario_To',fld:'vTFSOLICITACOES_USUARIO_TO',pic:'ZZZZZ9',nv:0},{av:'AV58TFSolicitacoes_Data',fld:'vTFSOLICITACOES_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV59TFSolicitacoes_Data_To',fld:'vTFSOLICITACOES_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV64TFSolicitacoes_Usuario_Ult',fld:'vTFSOLICITACOES_USUARIO_ULT',pic:'ZZZZZ9',nv:0},{av:'AV65TFSolicitacoes_Usuario_Ult_To',fld:'vTFSOLICITACOES_USUARIO_ULT_TO',pic:'ZZZZZ9',nv:0},{av:'AV68TFSolicitacoes_Data_Ult',fld:'vTFSOLICITACOES_DATA_ULT',pic:'99/99/99 99:99',nv:''},{av:'AV69TFSolicitacoes_Data_Ult_To',fld:'vTFSOLICITACOES_DATA_ULT_TO',pic:'99/99/99 99:99',nv:''},{av:'AV33ddo_Solicitacoes_CodigoTitleControlIdToReplace',fld:'vDDO_SOLICITACOES_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_Contratada_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATADA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_Sistema_CodigoTitleControlIdToReplace',fld:'vDDO_SISTEMA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_Servico_CodigoTitleControlIdToReplace',fld:'vDDO_SERVICO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_Solicitacoes_Novo_ProjetoTitleControlIdToReplace',fld:'vDDO_SOLICITACOES_NOVO_PROJETOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_Solicitacoes_ObjetivoTitleControlIdToReplace',fld:'vDDO_SOLICITACOES_OBJETIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_Solicitacoes_UsuarioTitleControlIdToReplace',fld:'vDDO_SOLICITACOES_USUARIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV62ddo_Solicitacoes_DataTitleControlIdToReplace',fld:'vDDO_SOLICITACOES_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_Solicitacoes_Usuario_UltTitleControlIdToReplace',fld:'vDDO_SOLICITACOES_USUARIO_ULTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_Solicitacoes_Data_UltTitleControlIdToReplace',fld:'vDDO_SOLICITACOES_DATA_ULTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV76ddo_Solicitacoes_StatusTitleControlIdToReplace',fld:'vDDO_SOLICITACOES_STATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75TFSolicitacoes_Status_Sels',fld:'vTFSOLICITACOES_STATUS_SELS',pic:'',nv:null},{av:'AV84Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21Solicitacoes_Novo_Projeto2',fld:'vSOLICITACOES_NOVO_PROJETO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25Solicitacoes_Novo_Projeto3',fld:'vSOLICITACOES_NOVO_PROJETO3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Solicitacoes_Novo_Projeto1',fld:'vSOLICITACOES_NOVO_PROJETO1',pic:'',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'radavSolicitacoes_novo_projeto2'},{av:'cmbavDynamicfiltersoperator2'},{av:'radavSolicitacoes_novo_projeto3'},{av:'cmbavDynamicfiltersoperator3'},{av:'radavSolicitacoes_novo_projeto1'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR3.CLICK","{handler:'E32AW2',iparms:[{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''}],oparms:[{av:'radavSolicitacoes_novo_projeto3'},{av:'cmbavDynamicfiltersoperator3'}]}");
         setEventMetadata("'DOCLEANFILTERS'","{handler:'E27AW2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Solicitacoes_Novo_Projeto1',fld:'vSOLICITACOES_NOVO_PROJETO1',pic:'',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21Solicitacoes_Novo_Projeto2',fld:'vSOLICITACOES_NOVO_PROJETO2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25Solicitacoes_Novo_Projeto3',fld:'vSOLICITACOES_NOVO_PROJETO3',pic:'',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31TFSolicitacoes_Codigo',fld:'vTFSOLICITACOES_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32TFSolicitacoes_Codigo_To',fld:'vTFSOLICITACOES_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV35TFContratada_Codigo',fld:'vTFCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV36TFContratada_Codigo_To',fld:'vTFCONTRATADA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFSistema_Codigo',fld:'vTFSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV40TFSistema_Codigo_To',fld:'vTFSISTEMA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV43TFServico_Codigo',fld:'vTFSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV44TFServico_Codigo_To',fld:'vTFSERVICO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV47TFSolicitacoes_Novo_Projeto_Sel',fld:'vTFSOLICITACOES_NOVO_PROJETO_SEL',pic:'',nv:''},{av:'AV50TFSolicitacoes_Objetivo',fld:'vTFSOLICITACOES_OBJETIVO',pic:'',nv:''},{av:'AV51TFSolicitacoes_Objetivo_Sel',fld:'vTFSOLICITACOES_OBJETIVO_SEL',pic:'',nv:''},{av:'AV54TFSolicitacoes_Usuario',fld:'vTFSOLICITACOES_USUARIO',pic:'ZZZZZ9',nv:0},{av:'AV55TFSolicitacoes_Usuario_To',fld:'vTFSOLICITACOES_USUARIO_TO',pic:'ZZZZZ9',nv:0},{av:'AV58TFSolicitacoes_Data',fld:'vTFSOLICITACOES_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV59TFSolicitacoes_Data_To',fld:'vTFSOLICITACOES_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV64TFSolicitacoes_Usuario_Ult',fld:'vTFSOLICITACOES_USUARIO_ULT',pic:'ZZZZZ9',nv:0},{av:'AV65TFSolicitacoes_Usuario_Ult_To',fld:'vTFSOLICITACOES_USUARIO_ULT_TO',pic:'ZZZZZ9',nv:0},{av:'AV68TFSolicitacoes_Data_Ult',fld:'vTFSOLICITACOES_DATA_ULT',pic:'99/99/99 99:99',nv:''},{av:'AV69TFSolicitacoes_Data_Ult_To',fld:'vTFSOLICITACOES_DATA_ULT_TO',pic:'99/99/99 99:99',nv:''},{av:'AV33ddo_Solicitacoes_CodigoTitleControlIdToReplace',fld:'vDDO_SOLICITACOES_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_Contratada_CodigoTitleControlIdToReplace',fld:'vDDO_CONTRATADA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_Sistema_CodigoTitleControlIdToReplace',fld:'vDDO_SISTEMA_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_Servico_CodigoTitleControlIdToReplace',fld:'vDDO_SERVICO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV48ddo_Solicitacoes_Novo_ProjetoTitleControlIdToReplace',fld:'vDDO_SOLICITACOES_NOVO_PROJETOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV52ddo_Solicitacoes_ObjetivoTitleControlIdToReplace',fld:'vDDO_SOLICITACOES_OBJETIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_Solicitacoes_UsuarioTitleControlIdToReplace',fld:'vDDO_SOLICITACOES_USUARIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV62ddo_Solicitacoes_DataTitleControlIdToReplace',fld:'vDDO_SOLICITACOES_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66ddo_Solicitacoes_Usuario_UltTitleControlIdToReplace',fld:'vDDO_SOLICITACOES_USUARIO_ULTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_Solicitacoes_Data_UltTitleControlIdToReplace',fld:'vDDO_SOLICITACOES_DATA_ULTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV76ddo_Solicitacoes_StatusTitleControlIdToReplace',fld:'vDDO_SOLICITACOES_STATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75TFSolicitacoes_Status_Sels',fld:'vTFSOLICITACOES_STATUS_SELS',pic:'',nv:null},{av:'AV84Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV31TFSolicitacoes_Codigo',fld:'vTFSOLICITACOES_CODIGO',pic:'ZZZZZ9',nv:0},{av:'Ddo_solicitacoes_codigo_Filteredtext_set',ctrl:'DDO_SOLICITACOES_CODIGO',prop:'FilteredText_set'},{av:'AV32TFSolicitacoes_Codigo_To',fld:'vTFSOLICITACOES_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_solicitacoes_codigo_Filteredtextto_set',ctrl:'DDO_SOLICITACOES_CODIGO',prop:'FilteredTextTo_set'},{av:'AV35TFContratada_Codigo',fld:'vTFCONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'Ddo_contratada_codigo_Filteredtext_set',ctrl:'DDO_CONTRATADA_CODIGO',prop:'FilteredText_set'},{av:'AV36TFContratada_Codigo_To',fld:'vTFCONTRATADA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_contratada_codigo_Filteredtextto_set',ctrl:'DDO_CONTRATADA_CODIGO',prop:'FilteredTextTo_set'},{av:'AV39TFSistema_Codigo',fld:'vTFSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'Ddo_sistema_codigo_Filteredtext_set',ctrl:'DDO_SISTEMA_CODIGO',prop:'FilteredText_set'},{av:'AV40TFSistema_Codigo_To',fld:'vTFSISTEMA_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_sistema_codigo_Filteredtextto_set',ctrl:'DDO_SISTEMA_CODIGO',prop:'FilteredTextTo_set'},{av:'AV43TFServico_Codigo',fld:'vTFSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'Ddo_servico_codigo_Filteredtext_set',ctrl:'DDO_SERVICO_CODIGO',prop:'FilteredText_set'},{av:'AV44TFServico_Codigo_To',fld:'vTFSERVICO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_servico_codigo_Filteredtextto_set',ctrl:'DDO_SERVICO_CODIGO',prop:'FilteredTextTo_set'},{av:'AV47TFSolicitacoes_Novo_Projeto_Sel',fld:'vTFSOLICITACOES_NOVO_PROJETO_SEL',pic:'',nv:''},{av:'Ddo_solicitacoes_novo_projeto_Selectedvalue_set',ctrl:'DDO_SOLICITACOES_NOVO_PROJETO',prop:'SelectedValue_set'},{av:'AV50TFSolicitacoes_Objetivo',fld:'vTFSOLICITACOES_OBJETIVO',pic:'',nv:''},{av:'Ddo_solicitacoes_objetivo_Filteredtext_set',ctrl:'DDO_SOLICITACOES_OBJETIVO',prop:'FilteredText_set'},{av:'AV51TFSolicitacoes_Objetivo_Sel',fld:'vTFSOLICITACOES_OBJETIVO_SEL',pic:'',nv:''},{av:'Ddo_solicitacoes_objetivo_Selectedvalue_set',ctrl:'DDO_SOLICITACOES_OBJETIVO',prop:'SelectedValue_set'},{av:'AV54TFSolicitacoes_Usuario',fld:'vTFSOLICITACOES_USUARIO',pic:'ZZZZZ9',nv:0},{av:'Ddo_solicitacoes_usuario_Filteredtext_set',ctrl:'DDO_SOLICITACOES_USUARIO',prop:'FilteredText_set'},{av:'AV55TFSolicitacoes_Usuario_To',fld:'vTFSOLICITACOES_USUARIO_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_solicitacoes_usuario_Filteredtextto_set',ctrl:'DDO_SOLICITACOES_USUARIO',prop:'FilteredTextTo_set'},{av:'AV58TFSolicitacoes_Data',fld:'vTFSOLICITACOES_DATA',pic:'99/99/99 99:99',nv:''},{av:'Ddo_solicitacoes_data_Filteredtext_set',ctrl:'DDO_SOLICITACOES_DATA',prop:'FilteredText_set'},{av:'AV59TFSolicitacoes_Data_To',fld:'vTFSOLICITACOES_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'Ddo_solicitacoes_data_Filteredtextto_set',ctrl:'DDO_SOLICITACOES_DATA',prop:'FilteredTextTo_set'},{av:'AV64TFSolicitacoes_Usuario_Ult',fld:'vTFSOLICITACOES_USUARIO_ULT',pic:'ZZZZZ9',nv:0},{av:'Ddo_solicitacoes_usuario_ult_Filteredtext_set',ctrl:'DDO_SOLICITACOES_USUARIO_ULT',prop:'FilteredText_set'},{av:'AV65TFSolicitacoes_Usuario_Ult_To',fld:'vTFSOLICITACOES_USUARIO_ULT_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_solicitacoes_usuario_ult_Filteredtextto_set',ctrl:'DDO_SOLICITACOES_USUARIO_ULT',prop:'FilteredTextTo_set'},{av:'AV68TFSolicitacoes_Data_Ult',fld:'vTFSOLICITACOES_DATA_ULT',pic:'99/99/99 99:99',nv:''},{av:'Ddo_solicitacoes_data_ult_Filteredtext_set',ctrl:'DDO_SOLICITACOES_DATA_ULT',prop:'FilteredText_set'},{av:'AV69TFSolicitacoes_Data_Ult_To',fld:'vTFSOLICITACOES_DATA_ULT_TO',pic:'99/99/99 99:99',nv:''},{av:'Ddo_solicitacoes_data_ult_Filteredtextto_set',ctrl:'DDO_SOLICITACOES_DATA_ULT',prop:'FilteredTextTo_set'},{av:'AV75TFSolicitacoes_Status_Sels',fld:'vTFSOLICITACOES_STATUS_SELS',pic:'',nv:null},{av:'Ddo_solicitacoes_status_Selectedvalue_set',ctrl:'DDO_SOLICITACOES_STATUS',prop:'SelectedValue_set'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV17Solicitacoes_Novo_Projeto1',fld:'vSOLICITACOES_NOVO_PROJETO1',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'radavSolicitacoes_novo_projeto1'},{av:'cmbavDynamicfiltersoperator1'},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV21Solicitacoes_Novo_Projeto2',fld:'vSOLICITACOES_NOVO_PROJETO2',pic:'',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV25Solicitacoes_Novo_Projeto3',fld:'vSOLICITACOES_NOVO_PROJETO3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'radavSolicitacoes_novo_projeto2'},{av:'cmbavDynamicfiltersoperator2'},{av:'radavSolicitacoes_novo_projeto3'},{av:'cmbavDynamicfiltersoperator3'}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         wcpOAV8InOutSolicitacoes_Novo_Projeto = "";
         Gridpaginationbar_Selectedpage = "";
         Ddo_solicitacoes_codigo_Activeeventkey = "";
         Ddo_solicitacoes_codigo_Filteredtext_get = "";
         Ddo_solicitacoes_codigo_Filteredtextto_get = "";
         Ddo_contratada_codigo_Activeeventkey = "";
         Ddo_contratada_codigo_Filteredtext_get = "";
         Ddo_contratada_codigo_Filteredtextto_get = "";
         Ddo_sistema_codigo_Activeeventkey = "";
         Ddo_sistema_codigo_Filteredtext_get = "";
         Ddo_sistema_codigo_Filteredtextto_get = "";
         Ddo_servico_codigo_Activeeventkey = "";
         Ddo_servico_codigo_Filteredtext_get = "";
         Ddo_servico_codigo_Filteredtextto_get = "";
         Ddo_solicitacoes_novo_projeto_Activeeventkey = "";
         Ddo_solicitacoes_novo_projeto_Selectedvalue_get = "";
         Ddo_solicitacoes_objetivo_Activeeventkey = "";
         Ddo_solicitacoes_objetivo_Filteredtext_get = "";
         Ddo_solicitacoes_objetivo_Selectedvalue_get = "";
         Ddo_solicitacoes_usuario_Activeeventkey = "";
         Ddo_solicitacoes_usuario_Filteredtext_get = "";
         Ddo_solicitacoes_usuario_Filteredtextto_get = "";
         Ddo_solicitacoes_data_Activeeventkey = "";
         Ddo_solicitacoes_data_Filteredtext_get = "";
         Ddo_solicitacoes_data_Filteredtextto_get = "";
         Ddo_solicitacoes_usuario_ult_Activeeventkey = "";
         Ddo_solicitacoes_usuario_ult_Filteredtext_get = "";
         Ddo_solicitacoes_usuario_ult_Filteredtextto_get = "";
         Ddo_solicitacoes_data_ult_Activeeventkey = "";
         Ddo_solicitacoes_data_ult_Filteredtext_get = "";
         Ddo_solicitacoes_data_ult_Filteredtextto_get = "";
         Ddo_solicitacoes_status_Activeeventkey = "";
         Ddo_solicitacoes_status_Selectedvalue_get = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV15DynamicFiltersSelector1 = "";
         AV17Solicitacoes_Novo_Projeto1 = "";
         AV19DynamicFiltersSelector2 = "";
         AV21Solicitacoes_Novo_Projeto2 = "";
         AV23DynamicFiltersSelector3 = "";
         AV25Solicitacoes_Novo_Projeto3 = "";
         AV47TFSolicitacoes_Novo_Projeto_Sel = "";
         AV50TFSolicitacoes_Objetivo = "";
         AV51TFSolicitacoes_Objetivo_Sel = "";
         AV58TFSolicitacoes_Data = (DateTime)(DateTime.MinValue);
         AV59TFSolicitacoes_Data_To = (DateTime)(DateTime.MinValue);
         AV68TFSolicitacoes_Data_Ult = (DateTime)(DateTime.MinValue);
         AV69TFSolicitacoes_Data_Ult_To = (DateTime)(DateTime.MinValue);
         AV33ddo_Solicitacoes_CodigoTitleControlIdToReplace = "";
         AV37ddo_Contratada_CodigoTitleControlIdToReplace = "";
         AV41ddo_Sistema_CodigoTitleControlIdToReplace = "";
         AV45ddo_Servico_CodigoTitleControlIdToReplace = "";
         AV48ddo_Solicitacoes_Novo_ProjetoTitleControlIdToReplace = "";
         AV52ddo_Solicitacoes_ObjetivoTitleControlIdToReplace = "";
         AV56ddo_Solicitacoes_UsuarioTitleControlIdToReplace = "";
         AV62ddo_Solicitacoes_DataTitleControlIdToReplace = "";
         AV66ddo_Solicitacoes_Usuario_UltTitleControlIdToReplace = "";
         AV72ddo_Solicitacoes_Data_UltTitleControlIdToReplace = "";
         AV76ddo_Solicitacoes_StatusTitleControlIdToReplace = "";
         AV75TFSolicitacoes_Status_Sels = new GxSimpleCollection();
         AV84Pgmname = "";
         AV10GridState = new wwpbaseobjects.SdtWWPGridState(context);
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV77DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV30Solicitacoes_CodigoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV34Contratada_CodigoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV38Sistema_CodigoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV42Servico_CodigoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV46Solicitacoes_Novo_ProjetoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV49Solicitacoes_ObjetivoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV53Solicitacoes_UsuarioTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV57Solicitacoes_DataTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV63Solicitacoes_Usuario_UltTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV67Solicitacoes_Data_UltTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV73Solicitacoes_StatusTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         Ddo_solicitacoes_codigo_Filteredtext_set = "";
         Ddo_solicitacoes_codigo_Filteredtextto_set = "";
         Ddo_solicitacoes_codigo_Sortedstatus = "";
         Ddo_contratada_codigo_Filteredtext_set = "";
         Ddo_contratada_codigo_Filteredtextto_set = "";
         Ddo_contratada_codigo_Sortedstatus = "";
         Ddo_sistema_codigo_Filteredtext_set = "";
         Ddo_sistema_codigo_Filteredtextto_set = "";
         Ddo_sistema_codigo_Sortedstatus = "";
         Ddo_servico_codigo_Filteredtext_set = "";
         Ddo_servico_codigo_Filteredtextto_set = "";
         Ddo_servico_codigo_Sortedstatus = "";
         Ddo_solicitacoes_novo_projeto_Selectedvalue_set = "";
         Ddo_solicitacoes_novo_projeto_Sortedstatus = "";
         Ddo_solicitacoes_objetivo_Filteredtext_set = "";
         Ddo_solicitacoes_objetivo_Selectedvalue_set = "";
         Ddo_solicitacoes_objetivo_Sortedstatus = "";
         Ddo_solicitacoes_usuario_Filteredtext_set = "";
         Ddo_solicitacoes_usuario_Filteredtextto_set = "";
         Ddo_solicitacoes_usuario_Sortedstatus = "";
         Ddo_solicitacoes_data_Filteredtext_set = "";
         Ddo_solicitacoes_data_Filteredtextto_set = "";
         Ddo_solicitacoes_data_Sortedstatus = "";
         Ddo_solicitacoes_usuario_ult_Filteredtext_set = "";
         Ddo_solicitacoes_usuario_ult_Filteredtextto_set = "";
         Ddo_solicitacoes_usuario_ult_Sortedstatus = "";
         Ddo_solicitacoes_data_ult_Filteredtext_set = "";
         Ddo_solicitacoes_data_ult_Filteredtextto_set = "";
         Ddo_solicitacoes_data_ult_Sortedstatus = "";
         Ddo_solicitacoes_status_Selectedvalue_set = "";
         Ddo_solicitacoes_status_Sortedstatus = "";
         GX_FocusControl = "";
         sPrefix = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         AV60DDO_Solicitacoes_DataAuxDate = DateTime.MinValue;
         AV61DDO_Solicitacoes_DataAuxDateTo = DateTime.MinValue;
         AV70DDO_Solicitacoes_Data_UltAuxDate = DateTime.MinValue;
         AV71DDO_Solicitacoes_Data_UltAuxDateTo = DateTime.MinValue;
         Form = new GXWebForm();
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV28Select = "";
         AV83Select_GXI = "";
         A440Solicitacoes_Novo_Projeto = "";
         A441Solicitacoes_Objetivo = "";
         A444Solicitacoes_Data = (DateTime)(DateTime.MinValue);
         A445Solicitacoes_Data_Ult = (DateTime)(DateTime.MinValue);
         A446Solicitacoes_Status = "";
         GXCCtl = "";
         GridContainer = new GXWebGrid( context);
         scmdbuf = "";
         lV17Solicitacoes_Novo_Projeto1 = "";
         lV21Solicitacoes_Novo_Projeto2 = "";
         lV25Solicitacoes_Novo_Projeto3 = "";
         lV50TFSolicitacoes_Objetivo = "";
         H00AW2_A446Solicitacoes_Status = new String[] {""} ;
         H00AW2_A445Solicitacoes_Data_Ult = new DateTime[] {DateTime.MinValue} ;
         H00AW2_A443Solicitacoes_Usuario_Ult = new int[1] ;
         H00AW2_A444Solicitacoes_Data = new DateTime[] {DateTime.MinValue} ;
         H00AW2_A442Solicitacoes_Usuario = new int[1] ;
         H00AW2_A441Solicitacoes_Objetivo = new String[] {""} ;
         H00AW2_n441Solicitacoes_Objetivo = new bool[] {false} ;
         H00AW2_A440Solicitacoes_Novo_Projeto = new String[] {""} ;
         H00AW2_A155Servico_Codigo = new int[1] ;
         H00AW2_A127Sistema_Codigo = new int[1] ;
         H00AW2_A39Contratada_Codigo = new int[1] ;
         H00AW2_A439Solicitacoes_Codigo = new int[1] ;
         H00AW3_AGRID_nRecordCount = new long[1] ;
         imgAdddynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters1_Jsonclick = "";
         imgAdddynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters3_Jsonclick = "";
         imgCleanfilters_Jsonclick = "";
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV74TFSolicitacoes_Status_SelsJson = "";
         GridRow = new GXWebRow();
         AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblOrderedtext_Jsonclick = "";
         lblJsdynamicfilters_Jsonclick = "";
         lblDynamicfiltersprefix1_Jsonclick = "";
         lblDynamicfiltersmiddle1_Jsonclick = "";
         lblDynamicfiltersprefix2_Jsonclick = "";
         lblDynamicfiltersmiddle2_Jsonclick = "";
         lblDynamicfiltersprefix3_Jsonclick = "";
         lblDynamicfiltersmiddle3_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.promptsolicitacoes__default(),
            new Object[][] {
                new Object[] {
               H00AW2_A446Solicitacoes_Status, H00AW2_A445Solicitacoes_Data_Ult, H00AW2_A443Solicitacoes_Usuario_Ult, H00AW2_A444Solicitacoes_Data, H00AW2_A442Solicitacoes_Usuario, H00AW2_A441Solicitacoes_Objetivo, H00AW2_n441Solicitacoes_Objetivo, H00AW2_A440Solicitacoes_Novo_Projeto, H00AW2_A155Servico_Codigo, H00AW2_A127Sistema_Codigo,
               H00AW2_A39Contratada_Codigo, H00AW2_A439Solicitacoes_Codigo
               }
               , new Object[] {
               H00AW3_AGRID_nRecordCount
               }
            }
         );
         AV84Pgmname = "PromptSolicitacoes";
         /* GeneXus formulas. */
         AV84Pgmname = "PromptSolicitacoes";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_80 ;
      private short nGXsfl_80_idx=1 ;
      private short AV13OrderedBy ;
      private short AV16DynamicFiltersOperator1 ;
      private short AV20DynamicFiltersOperator2 ;
      private short AV24DynamicFiltersOperator3 ;
      private short initialized ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_80_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short edtSolicitacoes_Codigo_Titleformat ;
      private short edtContratada_Codigo_Titleformat ;
      private short edtSistema_Codigo_Titleformat ;
      private short edtServico_Codigo_Titleformat ;
      private short radSolicitacoes_Novo_Projeto_Titleformat ;
      private short edtSolicitacoes_Objetivo_Titleformat ;
      private short edtSolicitacoes_Usuario_Titleformat ;
      private short edtSolicitacoes_Data_Titleformat ;
      private short edtSolicitacoes_Usuario_Ult_Titleformat ;
      private short edtSolicitacoes_Data_Ult_Titleformat ;
      private short cmbSolicitacoes_Status_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int AV7InOutSolicitacoes_Codigo ;
      private int wcpOAV7InOutSolicitacoes_Codigo ;
      private int subGrid_Rows ;
      private int AV31TFSolicitacoes_Codigo ;
      private int AV32TFSolicitacoes_Codigo_To ;
      private int AV35TFContratada_Codigo ;
      private int AV36TFContratada_Codigo_To ;
      private int AV39TFSistema_Codigo ;
      private int AV40TFSistema_Codigo_To ;
      private int AV43TFServico_Codigo ;
      private int AV44TFServico_Codigo_To ;
      private int AV54TFSolicitacoes_Usuario ;
      private int AV55TFSolicitacoes_Usuario_To ;
      private int AV64TFSolicitacoes_Usuario_Ult ;
      private int AV65TFSolicitacoes_Usuario_Ult_To ;
      private int Gridpaginationbar_Pagestoshow ;
      private int Ddo_solicitacoes_codigo_Datalistupdateminimumcharacters ;
      private int Ddo_contratada_codigo_Datalistupdateminimumcharacters ;
      private int Ddo_sistema_codigo_Datalistupdateminimumcharacters ;
      private int Ddo_servico_codigo_Datalistupdateminimumcharacters ;
      private int Ddo_solicitacoes_novo_projeto_Datalistupdateminimumcharacters ;
      private int Ddo_solicitacoes_objetivo_Datalistupdateminimumcharacters ;
      private int Ddo_solicitacoes_usuario_Datalistupdateminimumcharacters ;
      private int Ddo_solicitacoes_data_Datalistupdateminimumcharacters ;
      private int Ddo_solicitacoes_usuario_ult_Datalistupdateminimumcharacters ;
      private int Ddo_solicitacoes_data_ult_Datalistupdateminimumcharacters ;
      private int Ddo_solicitacoes_status_Datalistupdateminimumcharacters ;
      private int edtavTfsolicitacoes_codigo_Visible ;
      private int edtavTfsolicitacoes_codigo_to_Visible ;
      private int edtavTfcontratada_codigo_Visible ;
      private int edtavTfcontratada_codigo_to_Visible ;
      private int edtavTfsistema_codigo_Visible ;
      private int edtavTfsistema_codigo_to_Visible ;
      private int edtavTfservico_codigo_Visible ;
      private int edtavTfservico_codigo_to_Visible ;
      private int edtavTfsolicitacoes_novo_projeto_sel_Visible ;
      private int edtavTfsolicitacoes_objetivo_Visible ;
      private int edtavTfsolicitacoes_objetivo_sel_Visible ;
      private int edtavTfsolicitacoes_usuario_Visible ;
      private int edtavTfsolicitacoes_usuario_to_Visible ;
      private int edtavTfsolicitacoes_data_Visible ;
      private int edtavTfsolicitacoes_data_to_Visible ;
      private int edtavTfsolicitacoes_usuario_ult_Visible ;
      private int edtavTfsolicitacoes_usuario_ult_to_Visible ;
      private int edtavTfsolicitacoes_data_ult_Visible ;
      private int edtavTfsolicitacoes_data_ult_to_Visible ;
      private int edtavDdo_solicitacoes_codigotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratada_codigotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_sistema_codigotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_servico_codigotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_solicitacoes_novo_projetotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_solicitacoes_objetivotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_solicitacoes_usuariotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_solicitacoes_datatitlecontrolidtoreplace_Visible ;
      private int edtavDdo_solicitacoes_usuario_ulttitlecontrolidtoreplace_Visible ;
      private int edtavDdo_solicitacoes_data_ulttitlecontrolidtoreplace_Visible ;
      private int edtavDdo_solicitacoes_statustitlecontrolidtoreplace_Visible ;
      private int A439Solicitacoes_Codigo ;
      private int A39Contratada_Codigo ;
      private int A127Sistema_Codigo ;
      private int A155Servico_Codigo ;
      private int A442Solicitacoes_Usuario ;
      private int A443Solicitacoes_Usuario_Ult ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int AV75TFSolicitacoes_Status_Sels_Count ;
      private int edtavOrdereddsc_Visible ;
      private int AV78PageToGo ;
      private int imgAdddynamicfilters1_Visible ;
      private int imgRemovedynamicfilters1_Visible ;
      private int imgAdddynamicfilters2_Visible ;
      private int imgRemovedynamicfilters2_Visible ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private int edtavSelect_Enabled ;
      private int edtavSelect_Visible ;
      private long GRID_nFirstRecordOnPage ;
      private long AV79GridCurrentPage ;
      private long AV80GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private String AV8InOutSolicitacoes_Novo_Projeto ;
      private String wcpOAV8InOutSolicitacoes_Novo_Projeto ;
      private String Gridpaginationbar_Selectedpage ;
      private String Ddo_solicitacoes_codigo_Activeeventkey ;
      private String Ddo_solicitacoes_codigo_Filteredtext_get ;
      private String Ddo_solicitacoes_codigo_Filteredtextto_get ;
      private String Ddo_contratada_codigo_Activeeventkey ;
      private String Ddo_contratada_codigo_Filteredtext_get ;
      private String Ddo_contratada_codigo_Filteredtextto_get ;
      private String Ddo_sistema_codigo_Activeeventkey ;
      private String Ddo_sistema_codigo_Filteredtext_get ;
      private String Ddo_sistema_codigo_Filteredtextto_get ;
      private String Ddo_servico_codigo_Activeeventkey ;
      private String Ddo_servico_codigo_Filteredtext_get ;
      private String Ddo_servico_codigo_Filteredtextto_get ;
      private String Ddo_solicitacoes_novo_projeto_Activeeventkey ;
      private String Ddo_solicitacoes_novo_projeto_Selectedvalue_get ;
      private String Ddo_solicitacoes_objetivo_Activeeventkey ;
      private String Ddo_solicitacoes_objetivo_Filteredtext_get ;
      private String Ddo_solicitacoes_objetivo_Selectedvalue_get ;
      private String Ddo_solicitacoes_usuario_Activeeventkey ;
      private String Ddo_solicitacoes_usuario_Filteredtext_get ;
      private String Ddo_solicitacoes_usuario_Filteredtextto_get ;
      private String Ddo_solicitacoes_data_Activeeventkey ;
      private String Ddo_solicitacoes_data_Filteredtext_get ;
      private String Ddo_solicitacoes_data_Filteredtextto_get ;
      private String Ddo_solicitacoes_usuario_ult_Activeeventkey ;
      private String Ddo_solicitacoes_usuario_ult_Filteredtext_get ;
      private String Ddo_solicitacoes_usuario_ult_Filteredtextto_get ;
      private String Ddo_solicitacoes_data_ult_Activeeventkey ;
      private String Ddo_solicitacoes_data_ult_Filteredtext_get ;
      private String Ddo_solicitacoes_data_ult_Filteredtextto_get ;
      private String Ddo_solicitacoes_status_Activeeventkey ;
      private String Ddo_solicitacoes_status_Selectedvalue_get ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_80_idx="0001" ;
      private String AV17Solicitacoes_Novo_Projeto1 ;
      private String AV21Solicitacoes_Novo_Projeto2 ;
      private String AV25Solicitacoes_Novo_Projeto3 ;
      private String AV47TFSolicitacoes_Novo_Projeto_Sel ;
      private String AV84Pgmname ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Ddo_solicitacoes_codigo_Caption ;
      private String Ddo_solicitacoes_codigo_Tooltip ;
      private String Ddo_solicitacoes_codigo_Cls ;
      private String Ddo_solicitacoes_codigo_Filteredtext_set ;
      private String Ddo_solicitacoes_codigo_Filteredtextto_set ;
      private String Ddo_solicitacoes_codigo_Dropdownoptionstype ;
      private String Ddo_solicitacoes_codigo_Titlecontrolidtoreplace ;
      private String Ddo_solicitacoes_codigo_Sortedstatus ;
      private String Ddo_solicitacoes_codigo_Filtertype ;
      private String Ddo_solicitacoes_codigo_Datalistfixedvalues ;
      private String Ddo_solicitacoes_codigo_Sortasc ;
      private String Ddo_solicitacoes_codigo_Sortdsc ;
      private String Ddo_solicitacoes_codigo_Loadingdata ;
      private String Ddo_solicitacoes_codigo_Cleanfilter ;
      private String Ddo_solicitacoes_codigo_Rangefilterfrom ;
      private String Ddo_solicitacoes_codigo_Rangefilterto ;
      private String Ddo_solicitacoes_codigo_Noresultsfound ;
      private String Ddo_solicitacoes_codigo_Searchbuttontext ;
      private String Ddo_contratada_codigo_Caption ;
      private String Ddo_contratada_codigo_Tooltip ;
      private String Ddo_contratada_codigo_Cls ;
      private String Ddo_contratada_codigo_Filteredtext_set ;
      private String Ddo_contratada_codigo_Filteredtextto_set ;
      private String Ddo_contratada_codigo_Dropdownoptionstype ;
      private String Ddo_contratada_codigo_Titlecontrolidtoreplace ;
      private String Ddo_contratada_codigo_Sortedstatus ;
      private String Ddo_contratada_codigo_Filtertype ;
      private String Ddo_contratada_codigo_Datalistfixedvalues ;
      private String Ddo_contratada_codigo_Sortasc ;
      private String Ddo_contratada_codigo_Sortdsc ;
      private String Ddo_contratada_codigo_Loadingdata ;
      private String Ddo_contratada_codigo_Cleanfilter ;
      private String Ddo_contratada_codigo_Rangefilterfrom ;
      private String Ddo_contratada_codigo_Rangefilterto ;
      private String Ddo_contratada_codigo_Noresultsfound ;
      private String Ddo_contratada_codigo_Searchbuttontext ;
      private String Ddo_sistema_codigo_Caption ;
      private String Ddo_sistema_codigo_Tooltip ;
      private String Ddo_sistema_codigo_Cls ;
      private String Ddo_sistema_codigo_Filteredtext_set ;
      private String Ddo_sistema_codigo_Filteredtextto_set ;
      private String Ddo_sistema_codigo_Dropdownoptionstype ;
      private String Ddo_sistema_codigo_Titlecontrolidtoreplace ;
      private String Ddo_sistema_codigo_Sortedstatus ;
      private String Ddo_sistema_codigo_Filtertype ;
      private String Ddo_sistema_codigo_Datalistfixedvalues ;
      private String Ddo_sistema_codigo_Sortasc ;
      private String Ddo_sistema_codigo_Sortdsc ;
      private String Ddo_sistema_codigo_Loadingdata ;
      private String Ddo_sistema_codigo_Cleanfilter ;
      private String Ddo_sistema_codigo_Rangefilterfrom ;
      private String Ddo_sistema_codigo_Rangefilterto ;
      private String Ddo_sistema_codigo_Noresultsfound ;
      private String Ddo_sistema_codigo_Searchbuttontext ;
      private String Ddo_servico_codigo_Caption ;
      private String Ddo_servico_codigo_Tooltip ;
      private String Ddo_servico_codigo_Cls ;
      private String Ddo_servico_codigo_Filteredtext_set ;
      private String Ddo_servico_codigo_Filteredtextto_set ;
      private String Ddo_servico_codigo_Dropdownoptionstype ;
      private String Ddo_servico_codigo_Titlecontrolidtoreplace ;
      private String Ddo_servico_codigo_Sortedstatus ;
      private String Ddo_servico_codigo_Filtertype ;
      private String Ddo_servico_codigo_Datalistfixedvalues ;
      private String Ddo_servico_codigo_Sortasc ;
      private String Ddo_servico_codigo_Sortdsc ;
      private String Ddo_servico_codigo_Loadingdata ;
      private String Ddo_servico_codigo_Cleanfilter ;
      private String Ddo_servico_codigo_Rangefilterfrom ;
      private String Ddo_servico_codigo_Rangefilterto ;
      private String Ddo_servico_codigo_Noresultsfound ;
      private String Ddo_servico_codigo_Searchbuttontext ;
      private String Ddo_solicitacoes_novo_projeto_Caption ;
      private String Ddo_solicitacoes_novo_projeto_Tooltip ;
      private String Ddo_solicitacoes_novo_projeto_Cls ;
      private String Ddo_solicitacoes_novo_projeto_Selectedvalue_set ;
      private String Ddo_solicitacoes_novo_projeto_Dropdownoptionstype ;
      private String Ddo_solicitacoes_novo_projeto_Titlecontrolidtoreplace ;
      private String Ddo_solicitacoes_novo_projeto_Sortedstatus ;
      private String Ddo_solicitacoes_novo_projeto_Filtertype ;
      private String Ddo_solicitacoes_novo_projeto_Datalisttype ;
      private String Ddo_solicitacoes_novo_projeto_Datalistfixedvalues ;
      private String Ddo_solicitacoes_novo_projeto_Datalistproc ;
      private String Ddo_solicitacoes_novo_projeto_Sortasc ;
      private String Ddo_solicitacoes_novo_projeto_Sortdsc ;
      private String Ddo_solicitacoes_novo_projeto_Loadingdata ;
      private String Ddo_solicitacoes_novo_projeto_Cleanfilter ;
      private String Ddo_solicitacoes_novo_projeto_Rangefilterfrom ;
      private String Ddo_solicitacoes_novo_projeto_Rangefilterto ;
      private String Ddo_solicitacoes_novo_projeto_Noresultsfound ;
      private String Ddo_solicitacoes_novo_projeto_Searchbuttontext ;
      private String Ddo_solicitacoes_objetivo_Caption ;
      private String Ddo_solicitacoes_objetivo_Tooltip ;
      private String Ddo_solicitacoes_objetivo_Cls ;
      private String Ddo_solicitacoes_objetivo_Filteredtext_set ;
      private String Ddo_solicitacoes_objetivo_Selectedvalue_set ;
      private String Ddo_solicitacoes_objetivo_Dropdownoptionstype ;
      private String Ddo_solicitacoes_objetivo_Titlecontrolidtoreplace ;
      private String Ddo_solicitacoes_objetivo_Sortedstatus ;
      private String Ddo_solicitacoes_objetivo_Filtertype ;
      private String Ddo_solicitacoes_objetivo_Datalisttype ;
      private String Ddo_solicitacoes_objetivo_Datalistfixedvalues ;
      private String Ddo_solicitacoes_objetivo_Datalistproc ;
      private String Ddo_solicitacoes_objetivo_Sortasc ;
      private String Ddo_solicitacoes_objetivo_Sortdsc ;
      private String Ddo_solicitacoes_objetivo_Loadingdata ;
      private String Ddo_solicitacoes_objetivo_Cleanfilter ;
      private String Ddo_solicitacoes_objetivo_Rangefilterfrom ;
      private String Ddo_solicitacoes_objetivo_Rangefilterto ;
      private String Ddo_solicitacoes_objetivo_Noresultsfound ;
      private String Ddo_solicitacoes_objetivo_Searchbuttontext ;
      private String Ddo_solicitacoes_usuario_Caption ;
      private String Ddo_solicitacoes_usuario_Tooltip ;
      private String Ddo_solicitacoes_usuario_Cls ;
      private String Ddo_solicitacoes_usuario_Filteredtext_set ;
      private String Ddo_solicitacoes_usuario_Filteredtextto_set ;
      private String Ddo_solicitacoes_usuario_Dropdownoptionstype ;
      private String Ddo_solicitacoes_usuario_Titlecontrolidtoreplace ;
      private String Ddo_solicitacoes_usuario_Sortedstatus ;
      private String Ddo_solicitacoes_usuario_Filtertype ;
      private String Ddo_solicitacoes_usuario_Datalistfixedvalues ;
      private String Ddo_solicitacoes_usuario_Sortasc ;
      private String Ddo_solicitacoes_usuario_Sortdsc ;
      private String Ddo_solicitacoes_usuario_Loadingdata ;
      private String Ddo_solicitacoes_usuario_Cleanfilter ;
      private String Ddo_solicitacoes_usuario_Rangefilterfrom ;
      private String Ddo_solicitacoes_usuario_Rangefilterto ;
      private String Ddo_solicitacoes_usuario_Noresultsfound ;
      private String Ddo_solicitacoes_usuario_Searchbuttontext ;
      private String Ddo_solicitacoes_data_Caption ;
      private String Ddo_solicitacoes_data_Tooltip ;
      private String Ddo_solicitacoes_data_Cls ;
      private String Ddo_solicitacoes_data_Filteredtext_set ;
      private String Ddo_solicitacoes_data_Filteredtextto_set ;
      private String Ddo_solicitacoes_data_Dropdownoptionstype ;
      private String Ddo_solicitacoes_data_Titlecontrolidtoreplace ;
      private String Ddo_solicitacoes_data_Sortedstatus ;
      private String Ddo_solicitacoes_data_Filtertype ;
      private String Ddo_solicitacoes_data_Datalistfixedvalues ;
      private String Ddo_solicitacoes_data_Sortasc ;
      private String Ddo_solicitacoes_data_Sortdsc ;
      private String Ddo_solicitacoes_data_Loadingdata ;
      private String Ddo_solicitacoes_data_Cleanfilter ;
      private String Ddo_solicitacoes_data_Rangefilterfrom ;
      private String Ddo_solicitacoes_data_Rangefilterto ;
      private String Ddo_solicitacoes_data_Noresultsfound ;
      private String Ddo_solicitacoes_data_Searchbuttontext ;
      private String Ddo_solicitacoes_usuario_ult_Caption ;
      private String Ddo_solicitacoes_usuario_ult_Tooltip ;
      private String Ddo_solicitacoes_usuario_ult_Cls ;
      private String Ddo_solicitacoes_usuario_ult_Filteredtext_set ;
      private String Ddo_solicitacoes_usuario_ult_Filteredtextto_set ;
      private String Ddo_solicitacoes_usuario_ult_Dropdownoptionstype ;
      private String Ddo_solicitacoes_usuario_ult_Titlecontrolidtoreplace ;
      private String Ddo_solicitacoes_usuario_ult_Sortedstatus ;
      private String Ddo_solicitacoes_usuario_ult_Filtertype ;
      private String Ddo_solicitacoes_usuario_ult_Datalistfixedvalues ;
      private String Ddo_solicitacoes_usuario_ult_Sortasc ;
      private String Ddo_solicitacoes_usuario_ult_Sortdsc ;
      private String Ddo_solicitacoes_usuario_ult_Loadingdata ;
      private String Ddo_solicitacoes_usuario_ult_Cleanfilter ;
      private String Ddo_solicitacoes_usuario_ult_Rangefilterfrom ;
      private String Ddo_solicitacoes_usuario_ult_Rangefilterto ;
      private String Ddo_solicitacoes_usuario_ult_Noresultsfound ;
      private String Ddo_solicitacoes_usuario_ult_Searchbuttontext ;
      private String Ddo_solicitacoes_data_ult_Caption ;
      private String Ddo_solicitacoes_data_ult_Tooltip ;
      private String Ddo_solicitacoes_data_ult_Cls ;
      private String Ddo_solicitacoes_data_ult_Filteredtext_set ;
      private String Ddo_solicitacoes_data_ult_Filteredtextto_set ;
      private String Ddo_solicitacoes_data_ult_Dropdownoptionstype ;
      private String Ddo_solicitacoes_data_ult_Titlecontrolidtoreplace ;
      private String Ddo_solicitacoes_data_ult_Sortedstatus ;
      private String Ddo_solicitacoes_data_ult_Filtertype ;
      private String Ddo_solicitacoes_data_ult_Datalistfixedvalues ;
      private String Ddo_solicitacoes_data_ult_Sortasc ;
      private String Ddo_solicitacoes_data_ult_Sortdsc ;
      private String Ddo_solicitacoes_data_ult_Loadingdata ;
      private String Ddo_solicitacoes_data_ult_Cleanfilter ;
      private String Ddo_solicitacoes_data_ult_Rangefilterfrom ;
      private String Ddo_solicitacoes_data_ult_Rangefilterto ;
      private String Ddo_solicitacoes_data_ult_Noresultsfound ;
      private String Ddo_solicitacoes_data_ult_Searchbuttontext ;
      private String Ddo_solicitacoes_status_Caption ;
      private String Ddo_solicitacoes_status_Tooltip ;
      private String Ddo_solicitacoes_status_Cls ;
      private String Ddo_solicitacoes_status_Selectedvalue_set ;
      private String Ddo_solicitacoes_status_Dropdownoptionstype ;
      private String Ddo_solicitacoes_status_Titlecontrolidtoreplace ;
      private String Ddo_solicitacoes_status_Sortedstatus ;
      private String Ddo_solicitacoes_status_Datalisttype ;
      private String Ddo_solicitacoes_status_Datalistfixedvalues ;
      private String Ddo_solicitacoes_status_Sortasc ;
      private String Ddo_solicitacoes_status_Sortdsc ;
      private String Ddo_solicitacoes_status_Loadingdata ;
      private String Ddo_solicitacoes_status_Cleanfilter ;
      private String Ddo_solicitacoes_status_Rangefilterfrom ;
      private String Ddo_solicitacoes_status_Rangefilterto ;
      private String Ddo_solicitacoes_status_Noresultsfound ;
      private String Ddo_solicitacoes_status_Searchbuttontext ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String chkavDynamicfiltersenabled2_Internalname ;
      private String chkavDynamicfiltersenabled3_Internalname ;
      private String edtavTfsolicitacoes_codigo_Internalname ;
      private String edtavTfsolicitacoes_codigo_Jsonclick ;
      private String edtavTfsolicitacoes_codigo_to_Internalname ;
      private String edtavTfsolicitacoes_codigo_to_Jsonclick ;
      private String edtavTfcontratada_codigo_Internalname ;
      private String edtavTfcontratada_codigo_Jsonclick ;
      private String edtavTfcontratada_codigo_to_Internalname ;
      private String edtavTfcontratada_codigo_to_Jsonclick ;
      private String edtavTfsistema_codigo_Internalname ;
      private String edtavTfsistema_codigo_Jsonclick ;
      private String edtavTfsistema_codigo_to_Internalname ;
      private String edtavTfsistema_codigo_to_Jsonclick ;
      private String edtavTfservico_codigo_Internalname ;
      private String edtavTfservico_codigo_Jsonclick ;
      private String edtavTfservico_codigo_to_Internalname ;
      private String edtavTfservico_codigo_to_Jsonclick ;
      private String edtavTfsolicitacoes_novo_projeto_sel_Internalname ;
      private String edtavTfsolicitacoes_novo_projeto_sel_Jsonclick ;
      private String edtavTfsolicitacoes_objetivo_Internalname ;
      private String edtavTfsolicitacoes_objetivo_sel_Internalname ;
      private String edtavTfsolicitacoes_usuario_Internalname ;
      private String edtavTfsolicitacoes_usuario_Jsonclick ;
      private String edtavTfsolicitacoes_usuario_to_Internalname ;
      private String edtavTfsolicitacoes_usuario_to_Jsonclick ;
      private String edtavTfsolicitacoes_data_Internalname ;
      private String edtavTfsolicitacoes_data_Jsonclick ;
      private String edtavTfsolicitacoes_data_to_Internalname ;
      private String edtavTfsolicitacoes_data_to_Jsonclick ;
      private String divDdo_solicitacoes_dataauxdates_Internalname ;
      private String edtavDdo_solicitacoes_dataauxdate_Internalname ;
      private String edtavDdo_solicitacoes_dataauxdate_Jsonclick ;
      private String edtavDdo_solicitacoes_dataauxdateto_Internalname ;
      private String edtavDdo_solicitacoes_dataauxdateto_Jsonclick ;
      private String edtavTfsolicitacoes_usuario_ult_Internalname ;
      private String edtavTfsolicitacoes_usuario_ult_Jsonclick ;
      private String edtavTfsolicitacoes_usuario_ult_to_Internalname ;
      private String edtavTfsolicitacoes_usuario_ult_to_Jsonclick ;
      private String edtavTfsolicitacoes_data_ult_Internalname ;
      private String edtavTfsolicitacoes_data_ult_Jsonclick ;
      private String edtavTfsolicitacoes_data_ult_to_Internalname ;
      private String edtavTfsolicitacoes_data_ult_to_Jsonclick ;
      private String divDdo_solicitacoes_data_ultauxdates_Internalname ;
      private String edtavDdo_solicitacoes_data_ultauxdate_Internalname ;
      private String edtavDdo_solicitacoes_data_ultauxdate_Jsonclick ;
      private String edtavDdo_solicitacoes_data_ultauxdateto_Internalname ;
      private String edtavDdo_solicitacoes_data_ultauxdateto_Jsonclick ;
      private String edtavDdo_solicitacoes_codigotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratada_codigotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_sistema_codigotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_servico_codigotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_solicitacoes_novo_projetotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_solicitacoes_objetivotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_solicitacoes_usuariotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_solicitacoes_datatitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_solicitacoes_usuario_ulttitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_solicitacoes_data_ulttitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_solicitacoes_statustitlecontrolidtoreplace_Internalname ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavSelect_Internalname ;
      private String edtSolicitacoes_Codigo_Internalname ;
      private String edtContratada_Codigo_Internalname ;
      private String edtSistema_Codigo_Internalname ;
      private String edtServico_Codigo_Internalname ;
      private String A440Solicitacoes_Novo_Projeto ;
      private String radSolicitacoes_Novo_Projeto_Internalname ;
      private String edtSolicitacoes_Objetivo_Internalname ;
      private String edtSolicitacoes_Usuario_Internalname ;
      private String edtSolicitacoes_Data_Internalname ;
      private String edtSolicitacoes_Usuario_Ult_Internalname ;
      private String edtSolicitacoes_Data_Ult_Internalname ;
      private String cmbSolicitacoes_Status_Internalname ;
      private String A446Solicitacoes_Status ;
      private String GXCCtl ;
      private String cmbavOrderedby_Internalname ;
      private String scmdbuf ;
      private String lV17Solicitacoes_Novo_Projeto1 ;
      private String lV21Solicitacoes_Novo_Projeto2 ;
      private String lV25Solicitacoes_Novo_Projeto3 ;
      private String edtavOrdereddsc_Internalname ;
      private String cmbavDynamicfiltersselector1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Internalname ;
      private String radavSolicitacoes_novo_projeto1_Internalname ;
      private String cmbavDynamicfiltersselector2_Internalname ;
      private String cmbavDynamicfiltersoperator2_Internalname ;
      private String radavSolicitacoes_novo_projeto2_Internalname ;
      private String cmbavDynamicfiltersselector3_Internalname ;
      private String cmbavDynamicfiltersoperator3_Internalname ;
      private String radavSolicitacoes_novo_projeto3_Internalname ;
      private String lblJsdynamicfilters_Caption ;
      private String lblJsdynamicfilters_Internalname ;
      private String imgAdddynamicfilters1_Jsonclick ;
      private String imgAdddynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters1_Jsonclick ;
      private String imgRemovedynamicfilters1_Internalname ;
      private String imgAdddynamicfilters2_Jsonclick ;
      private String imgAdddynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters2_Jsonclick ;
      private String imgRemovedynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters3_Jsonclick ;
      private String imgRemovedynamicfilters3_Internalname ;
      private String subGrid_Internalname ;
      private String Ddo_solicitacoes_codigo_Internalname ;
      private String Ddo_contratada_codigo_Internalname ;
      private String Ddo_sistema_codigo_Internalname ;
      private String Ddo_servico_codigo_Internalname ;
      private String Ddo_solicitacoes_novo_projeto_Internalname ;
      private String Ddo_solicitacoes_objetivo_Internalname ;
      private String Ddo_solicitacoes_usuario_Internalname ;
      private String Ddo_solicitacoes_data_Internalname ;
      private String Ddo_solicitacoes_usuario_ult_Internalname ;
      private String Ddo_solicitacoes_data_ult_Internalname ;
      private String Ddo_solicitacoes_status_Internalname ;
      private String imgCleanfilters_Jsonclick ;
      private String imgCleanfilters_Internalname ;
      private String edtSolicitacoes_Codigo_Title ;
      private String edtContratada_Codigo_Title ;
      private String edtSistema_Codigo_Title ;
      private String edtServico_Codigo_Title ;
      private String edtSolicitacoes_Objetivo_Title ;
      private String edtSolicitacoes_Usuario_Title ;
      private String edtSolicitacoes_Data_Title ;
      private String edtSolicitacoes_Usuario_Ult_Title ;
      private String edtSolicitacoes_Data_Ult_Title ;
      private String edtavSelect_Tooltiptext ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTablegridheader_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTablesearch_Internalname ;
      private String lblOrderedtext_Internalname ;
      private String lblOrderedtext_Jsonclick ;
      private String cmbavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Jsonclick ;
      private String tblTablefilters_Internalname ;
      private String lblJsdynamicfilters_Jsonclick ;
      private String tblTabledynamicfilters_Internalname ;
      private String lblDynamicfiltersprefix1_Internalname ;
      private String lblDynamicfiltersprefix1_Jsonclick ;
      private String cmbavDynamicfiltersselector1_Jsonclick ;
      private String lblDynamicfiltersmiddle1_Internalname ;
      private String lblDynamicfiltersmiddle1_Jsonclick ;
      private String lblDynamicfiltersprefix2_Internalname ;
      private String lblDynamicfiltersprefix2_Jsonclick ;
      private String cmbavDynamicfiltersselector2_Jsonclick ;
      private String lblDynamicfiltersmiddle2_Internalname ;
      private String lblDynamicfiltersmiddle2_Jsonclick ;
      private String lblDynamicfiltersprefix3_Internalname ;
      private String lblDynamicfiltersprefix3_Jsonclick ;
      private String cmbavDynamicfiltersselector3_Jsonclick ;
      private String lblDynamicfiltersmiddle3_Internalname ;
      private String lblDynamicfiltersmiddle3_Jsonclick ;
      private String tblTablemergeddynamicfilters3_Internalname ;
      private String cmbavDynamicfiltersoperator3_Jsonclick ;
      private String radavSolicitacoes_novo_projeto3_Jsonclick ;
      private String tblTablemergeddynamicfilters2_Internalname ;
      private String cmbavDynamicfiltersoperator2_Jsonclick ;
      private String radavSolicitacoes_novo_projeto2_Jsonclick ;
      private String tblTablemergeddynamicfilters1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Jsonclick ;
      private String radavSolicitacoes_novo_projeto1_Jsonclick ;
      private String sGXsfl_80_fel_idx="0001" ;
      private String edtavSelect_Jsonclick ;
      private String ROClassString ;
      private String edtSolicitacoes_Codigo_Jsonclick ;
      private String edtContratada_Codigo_Jsonclick ;
      private String edtSistema_Codigo_Jsonclick ;
      private String edtServico_Codigo_Jsonclick ;
      private String radSolicitacoes_Novo_Projeto_Jsonclick ;
      private String edtSolicitacoes_Objetivo_Jsonclick ;
      private String edtSolicitacoes_Usuario_Jsonclick ;
      private String edtSolicitacoes_Data_Jsonclick ;
      private String edtSolicitacoes_Usuario_Ult_Jsonclick ;
      private String edtSolicitacoes_Data_Ult_Jsonclick ;
      private String cmbSolicitacoes_Status_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private DateTime AV58TFSolicitacoes_Data ;
      private DateTime AV59TFSolicitacoes_Data_To ;
      private DateTime AV68TFSolicitacoes_Data_Ult ;
      private DateTime AV69TFSolicitacoes_Data_Ult_To ;
      private DateTime A444Solicitacoes_Data ;
      private DateTime A445Solicitacoes_Data_Ult ;
      private DateTime AV60DDO_Solicitacoes_DataAuxDate ;
      private DateTime AV61DDO_Solicitacoes_DataAuxDateTo ;
      private DateTime AV70DDO_Solicitacoes_Data_UltAuxDate ;
      private DateTime AV71DDO_Solicitacoes_Data_UltAuxDateTo ;
      private bool entryPointCalled ;
      private bool AV14OrderedDsc ;
      private bool AV18DynamicFiltersEnabled2 ;
      private bool AV22DynamicFiltersEnabled3 ;
      private bool AV27DynamicFiltersIgnoreFirst ;
      private bool AV26DynamicFiltersRemoving ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool Ddo_solicitacoes_codigo_Includesortasc ;
      private bool Ddo_solicitacoes_codigo_Includesortdsc ;
      private bool Ddo_solicitacoes_codigo_Includefilter ;
      private bool Ddo_solicitacoes_codigo_Filterisrange ;
      private bool Ddo_solicitacoes_codigo_Includedatalist ;
      private bool Ddo_contratada_codigo_Includesortasc ;
      private bool Ddo_contratada_codigo_Includesortdsc ;
      private bool Ddo_contratada_codigo_Includefilter ;
      private bool Ddo_contratada_codigo_Filterisrange ;
      private bool Ddo_contratada_codigo_Includedatalist ;
      private bool Ddo_sistema_codigo_Includesortasc ;
      private bool Ddo_sistema_codigo_Includesortdsc ;
      private bool Ddo_sistema_codigo_Includefilter ;
      private bool Ddo_sistema_codigo_Filterisrange ;
      private bool Ddo_sistema_codigo_Includedatalist ;
      private bool Ddo_servico_codigo_Includesortasc ;
      private bool Ddo_servico_codigo_Includesortdsc ;
      private bool Ddo_servico_codigo_Includefilter ;
      private bool Ddo_servico_codigo_Filterisrange ;
      private bool Ddo_servico_codigo_Includedatalist ;
      private bool Ddo_solicitacoes_novo_projeto_Includesortasc ;
      private bool Ddo_solicitacoes_novo_projeto_Includesortdsc ;
      private bool Ddo_solicitacoes_novo_projeto_Includefilter ;
      private bool Ddo_solicitacoes_novo_projeto_Filterisrange ;
      private bool Ddo_solicitacoes_novo_projeto_Includedatalist ;
      private bool Ddo_solicitacoes_objetivo_Includesortasc ;
      private bool Ddo_solicitacoes_objetivo_Includesortdsc ;
      private bool Ddo_solicitacoes_objetivo_Includefilter ;
      private bool Ddo_solicitacoes_objetivo_Filterisrange ;
      private bool Ddo_solicitacoes_objetivo_Includedatalist ;
      private bool Ddo_solicitacoes_usuario_Includesortasc ;
      private bool Ddo_solicitacoes_usuario_Includesortdsc ;
      private bool Ddo_solicitacoes_usuario_Includefilter ;
      private bool Ddo_solicitacoes_usuario_Filterisrange ;
      private bool Ddo_solicitacoes_usuario_Includedatalist ;
      private bool Ddo_solicitacoes_data_Includesortasc ;
      private bool Ddo_solicitacoes_data_Includesortdsc ;
      private bool Ddo_solicitacoes_data_Includefilter ;
      private bool Ddo_solicitacoes_data_Filterisrange ;
      private bool Ddo_solicitacoes_data_Includedatalist ;
      private bool Ddo_solicitacoes_usuario_ult_Includesortasc ;
      private bool Ddo_solicitacoes_usuario_ult_Includesortdsc ;
      private bool Ddo_solicitacoes_usuario_ult_Includefilter ;
      private bool Ddo_solicitacoes_usuario_ult_Filterisrange ;
      private bool Ddo_solicitacoes_usuario_ult_Includedatalist ;
      private bool Ddo_solicitacoes_data_ult_Includesortasc ;
      private bool Ddo_solicitacoes_data_ult_Includesortdsc ;
      private bool Ddo_solicitacoes_data_ult_Includefilter ;
      private bool Ddo_solicitacoes_data_ult_Filterisrange ;
      private bool Ddo_solicitacoes_data_ult_Includedatalist ;
      private bool Ddo_solicitacoes_status_Includesortasc ;
      private bool Ddo_solicitacoes_status_Includesortdsc ;
      private bool Ddo_solicitacoes_status_Includefilter ;
      private bool Ddo_solicitacoes_status_Filterisrange ;
      private bool Ddo_solicitacoes_status_Includedatalist ;
      private bool Ddo_solicitacoes_status_Allowmultipleselection ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n441Solicitacoes_Objetivo ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV28Select_IsBlob ;
      private String A441Solicitacoes_Objetivo ;
      private String AV74TFSolicitacoes_Status_SelsJson ;
      private String AV15DynamicFiltersSelector1 ;
      private String AV19DynamicFiltersSelector2 ;
      private String AV23DynamicFiltersSelector3 ;
      private String AV50TFSolicitacoes_Objetivo ;
      private String AV51TFSolicitacoes_Objetivo_Sel ;
      private String AV33ddo_Solicitacoes_CodigoTitleControlIdToReplace ;
      private String AV37ddo_Contratada_CodigoTitleControlIdToReplace ;
      private String AV41ddo_Sistema_CodigoTitleControlIdToReplace ;
      private String AV45ddo_Servico_CodigoTitleControlIdToReplace ;
      private String AV48ddo_Solicitacoes_Novo_ProjetoTitleControlIdToReplace ;
      private String AV52ddo_Solicitacoes_ObjetivoTitleControlIdToReplace ;
      private String AV56ddo_Solicitacoes_UsuarioTitleControlIdToReplace ;
      private String AV62ddo_Solicitacoes_DataTitleControlIdToReplace ;
      private String AV66ddo_Solicitacoes_Usuario_UltTitleControlIdToReplace ;
      private String AV72ddo_Solicitacoes_Data_UltTitleControlIdToReplace ;
      private String AV76ddo_Solicitacoes_StatusTitleControlIdToReplace ;
      private String AV83Select_GXI ;
      private String lV50TFSolicitacoes_Objetivo ;
      private String AV28Select ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_InOutSolicitacoes_Codigo ;
      private String aP1_InOutSolicitacoes_Novo_Projeto ;
      private GXCombobox cmbavOrderedby ;
      private GXCombobox cmbavDynamicfiltersselector1 ;
      private GXCombobox cmbavDynamicfiltersoperator1 ;
      private GXRadio radavSolicitacoes_novo_projeto1 ;
      private GXCombobox cmbavDynamicfiltersselector2 ;
      private GXCombobox cmbavDynamicfiltersoperator2 ;
      private GXRadio radavSolicitacoes_novo_projeto2 ;
      private GXCombobox cmbavDynamicfiltersselector3 ;
      private GXCombobox cmbavDynamicfiltersoperator3 ;
      private GXRadio radavSolicitacoes_novo_projeto3 ;
      private GXRadio radSolicitacoes_Novo_Projeto ;
      private GXCombobox cmbSolicitacoes_Status ;
      private GXCheckbox chkavDynamicfiltersenabled2 ;
      private GXCheckbox chkavDynamicfiltersenabled3 ;
      private IDataStoreProvider pr_default ;
      private String[] H00AW2_A446Solicitacoes_Status ;
      private DateTime[] H00AW2_A445Solicitacoes_Data_Ult ;
      private int[] H00AW2_A443Solicitacoes_Usuario_Ult ;
      private DateTime[] H00AW2_A444Solicitacoes_Data ;
      private int[] H00AW2_A442Solicitacoes_Usuario ;
      private String[] H00AW2_A441Solicitacoes_Objetivo ;
      private bool[] H00AW2_n441Solicitacoes_Objetivo ;
      private String[] H00AW2_A440Solicitacoes_Novo_Projeto ;
      private int[] H00AW2_A155Servico_Codigo ;
      private int[] H00AW2_A127Sistema_Codigo ;
      private int[] H00AW2_A39Contratada_Codigo ;
      private int[] H00AW2_A439Solicitacoes_Codigo ;
      private long[] H00AW3_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV75TFSolicitacoes_Status_Sels ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV30Solicitacoes_CodigoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV34Contratada_CodigoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV38Sistema_CodigoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV42Servico_CodigoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV46Solicitacoes_Novo_ProjetoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV49Solicitacoes_ObjetivoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV53Solicitacoes_UsuarioTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV57Solicitacoes_DataTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV63Solicitacoes_Usuario_UltTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV67Solicitacoes_Data_UltTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV73Solicitacoes_StatusTitleFilterData ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV10GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV11GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV12GridStateDynamicFilter ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV77DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 ;
   }

   public class promptsolicitacoes__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00AW2( IGxContext context ,
                                             String A446Solicitacoes_Status ,
                                             IGxCollection AV75TFSolicitacoes_Status_Sels ,
                                             String AV15DynamicFiltersSelector1 ,
                                             short AV16DynamicFiltersOperator1 ,
                                             String AV17Solicitacoes_Novo_Projeto1 ,
                                             bool AV18DynamicFiltersEnabled2 ,
                                             String AV19DynamicFiltersSelector2 ,
                                             short AV20DynamicFiltersOperator2 ,
                                             String AV21Solicitacoes_Novo_Projeto2 ,
                                             bool AV22DynamicFiltersEnabled3 ,
                                             String AV23DynamicFiltersSelector3 ,
                                             short AV24DynamicFiltersOperator3 ,
                                             String AV25Solicitacoes_Novo_Projeto3 ,
                                             int AV31TFSolicitacoes_Codigo ,
                                             int AV32TFSolicitacoes_Codigo_To ,
                                             int AV35TFContratada_Codigo ,
                                             int AV36TFContratada_Codigo_To ,
                                             int AV39TFSistema_Codigo ,
                                             int AV40TFSistema_Codigo_To ,
                                             int AV43TFServico_Codigo ,
                                             int AV44TFServico_Codigo_To ,
                                             String AV47TFSolicitacoes_Novo_Projeto_Sel ,
                                             String AV51TFSolicitacoes_Objetivo_Sel ,
                                             String AV50TFSolicitacoes_Objetivo ,
                                             int AV54TFSolicitacoes_Usuario ,
                                             int AV55TFSolicitacoes_Usuario_To ,
                                             DateTime AV58TFSolicitacoes_Data ,
                                             DateTime AV59TFSolicitacoes_Data_To ,
                                             int AV64TFSolicitacoes_Usuario_Ult ,
                                             int AV65TFSolicitacoes_Usuario_Ult_To ,
                                             DateTime AV68TFSolicitacoes_Data_Ult ,
                                             DateTime AV69TFSolicitacoes_Data_Ult_To ,
                                             int AV75TFSolicitacoes_Status_Sels_Count ,
                                             String A440Solicitacoes_Novo_Projeto ,
                                             int A439Solicitacoes_Codigo ,
                                             int A39Contratada_Codigo ,
                                             int A127Sistema_Codigo ,
                                             int A155Servico_Codigo ,
                                             String A441Solicitacoes_Objetivo ,
                                             int A442Solicitacoes_Usuario ,
                                             DateTime A444Solicitacoes_Data ,
                                             int A443Solicitacoes_Usuario_Ult ,
                                             DateTime A445Solicitacoes_Data_Ult ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [30] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " [Solicitacoes_Status], [Solicitacoes_Data_Ult], [Solicitacoes_Usuario_Ult], [Solicitacoes_Data], [Solicitacoes_Usuario], [Solicitacoes_Objetivo], [Solicitacoes_Novo_Projeto], [Servico_Codigo], [Sistema_Codigo], [Contratada_Codigo], [Solicitacoes_Codigo]";
         sFromString = " FROM [Solicitacoes] WITH (NOLOCK)";
         sOrderString = "";
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "SOLICITACOES_NOVO_PROJETO") == 0 ) && ( AV16DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV17Solicitacoes_Novo_Projeto1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Solicitacoes_Novo_Projeto] like @lV17Solicitacoes_Novo_Projeto1)";
            }
            else
            {
               sWhereString = sWhereString + " ([Solicitacoes_Novo_Projeto] like @lV17Solicitacoes_Novo_Projeto1)";
            }
         }
         else
         {
            GXv_int2[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "SOLICITACOES_NOVO_PROJETO") == 0 ) && ( AV16DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV17Solicitacoes_Novo_Projeto1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Solicitacoes_Novo_Projeto] like '%' + @lV17Solicitacoes_Novo_Projeto1)";
            }
            else
            {
               sWhereString = sWhereString + " ([Solicitacoes_Novo_Projeto] like '%' + @lV17Solicitacoes_Novo_Projeto1)";
            }
         }
         else
         {
            GXv_int2[1] = 1;
         }
         if ( AV18DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "SOLICITACOES_NOVO_PROJETO") == 0 ) && ( AV20DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV21Solicitacoes_Novo_Projeto2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Solicitacoes_Novo_Projeto] like @lV21Solicitacoes_Novo_Projeto2)";
            }
            else
            {
               sWhereString = sWhereString + " ([Solicitacoes_Novo_Projeto] like @lV21Solicitacoes_Novo_Projeto2)";
            }
         }
         else
         {
            GXv_int2[2] = 1;
         }
         if ( AV18DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "SOLICITACOES_NOVO_PROJETO") == 0 ) && ( AV20DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV21Solicitacoes_Novo_Projeto2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Solicitacoes_Novo_Projeto] like '%' + @lV21Solicitacoes_Novo_Projeto2)";
            }
            else
            {
               sWhereString = sWhereString + " ([Solicitacoes_Novo_Projeto] like '%' + @lV21Solicitacoes_Novo_Projeto2)";
            }
         }
         else
         {
            GXv_int2[3] = 1;
         }
         if ( AV22DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "SOLICITACOES_NOVO_PROJETO") == 0 ) && ( AV24DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV25Solicitacoes_Novo_Projeto3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Solicitacoes_Novo_Projeto] like @lV25Solicitacoes_Novo_Projeto3)";
            }
            else
            {
               sWhereString = sWhereString + " ([Solicitacoes_Novo_Projeto] like @lV25Solicitacoes_Novo_Projeto3)";
            }
         }
         else
         {
            GXv_int2[4] = 1;
         }
         if ( AV22DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "SOLICITACOES_NOVO_PROJETO") == 0 ) && ( AV24DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV25Solicitacoes_Novo_Projeto3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Solicitacoes_Novo_Projeto] like '%' + @lV25Solicitacoes_Novo_Projeto3)";
            }
            else
            {
               sWhereString = sWhereString + " ([Solicitacoes_Novo_Projeto] like '%' + @lV25Solicitacoes_Novo_Projeto3)";
            }
         }
         else
         {
            GXv_int2[5] = 1;
         }
         if ( ! (0==AV31TFSolicitacoes_Codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Solicitacoes_Codigo] >= @AV31TFSolicitacoes_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " ([Solicitacoes_Codigo] >= @AV31TFSolicitacoes_Codigo)";
            }
         }
         else
         {
            GXv_int2[6] = 1;
         }
         if ( ! (0==AV32TFSolicitacoes_Codigo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Solicitacoes_Codigo] <= @AV32TFSolicitacoes_Codigo_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([Solicitacoes_Codigo] <= @AV32TFSolicitacoes_Codigo_To)";
            }
         }
         else
         {
            GXv_int2[7] = 1;
         }
         if ( ! (0==AV35TFContratada_Codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Contratada_Codigo] >= @AV35TFContratada_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " ([Contratada_Codigo] >= @AV35TFContratada_Codigo)";
            }
         }
         else
         {
            GXv_int2[8] = 1;
         }
         if ( ! (0==AV36TFContratada_Codigo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Contratada_Codigo] <= @AV36TFContratada_Codigo_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([Contratada_Codigo] <= @AV36TFContratada_Codigo_To)";
            }
         }
         else
         {
            GXv_int2[9] = 1;
         }
         if ( ! (0==AV39TFSistema_Codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Sistema_Codigo] >= @AV39TFSistema_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " ([Sistema_Codigo] >= @AV39TFSistema_Codigo)";
            }
         }
         else
         {
            GXv_int2[10] = 1;
         }
         if ( ! (0==AV40TFSistema_Codigo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Sistema_Codigo] <= @AV40TFSistema_Codigo_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([Sistema_Codigo] <= @AV40TFSistema_Codigo_To)";
            }
         }
         else
         {
            GXv_int2[11] = 1;
         }
         if ( ! (0==AV43TFServico_Codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Servico_Codigo] >= @AV43TFServico_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " ([Servico_Codigo] >= @AV43TFServico_Codigo)";
            }
         }
         else
         {
            GXv_int2[12] = 1;
         }
         if ( ! (0==AV44TFServico_Codigo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Servico_Codigo] <= @AV44TFServico_Codigo_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([Servico_Codigo] <= @AV44TFServico_Codigo_To)";
            }
         }
         else
         {
            GXv_int2[13] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV47TFSolicitacoes_Novo_Projeto_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Solicitacoes_Novo_Projeto] = @AV47TFSolicitacoes_Novo_Projeto_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([Solicitacoes_Novo_Projeto] = @AV47TFSolicitacoes_Novo_Projeto_Sel)";
            }
         }
         else
         {
            GXv_int2[14] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV51TFSolicitacoes_Objetivo_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV50TFSolicitacoes_Objetivo)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Solicitacoes_Objetivo] like @lV50TFSolicitacoes_Objetivo)";
            }
            else
            {
               sWhereString = sWhereString + " ([Solicitacoes_Objetivo] like @lV50TFSolicitacoes_Objetivo)";
            }
         }
         else
         {
            GXv_int2[15] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV51TFSolicitacoes_Objetivo_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Solicitacoes_Objetivo] = @AV51TFSolicitacoes_Objetivo_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([Solicitacoes_Objetivo] = @AV51TFSolicitacoes_Objetivo_Sel)";
            }
         }
         else
         {
            GXv_int2[16] = 1;
         }
         if ( ! (0==AV54TFSolicitacoes_Usuario) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Solicitacoes_Usuario] >= @AV54TFSolicitacoes_Usuario)";
            }
            else
            {
               sWhereString = sWhereString + " ([Solicitacoes_Usuario] >= @AV54TFSolicitacoes_Usuario)";
            }
         }
         else
         {
            GXv_int2[17] = 1;
         }
         if ( ! (0==AV55TFSolicitacoes_Usuario_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Solicitacoes_Usuario] <= @AV55TFSolicitacoes_Usuario_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([Solicitacoes_Usuario] <= @AV55TFSolicitacoes_Usuario_To)";
            }
         }
         else
         {
            GXv_int2[18] = 1;
         }
         if ( ! (DateTime.MinValue==AV58TFSolicitacoes_Data) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Solicitacoes_Data] >= @AV58TFSolicitacoes_Data)";
            }
            else
            {
               sWhereString = sWhereString + " ([Solicitacoes_Data] >= @AV58TFSolicitacoes_Data)";
            }
         }
         else
         {
            GXv_int2[19] = 1;
         }
         if ( ! (DateTime.MinValue==AV59TFSolicitacoes_Data_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Solicitacoes_Data] <= @AV59TFSolicitacoes_Data_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([Solicitacoes_Data] <= @AV59TFSolicitacoes_Data_To)";
            }
         }
         else
         {
            GXv_int2[20] = 1;
         }
         if ( ! (0==AV64TFSolicitacoes_Usuario_Ult) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Solicitacoes_Usuario_Ult] >= @AV64TFSolicitacoes_Usuario_Ult)";
            }
            else
            {
               sWhereString = sWhereString + " ([Solicitacoes_Usuario_Ult] >= @AV64TFSolicitacoes_Usuario_Ult)";
            }
         }
         else
         {
            GXv_int2[21] = 1;
         }
         if ( ! (0==AV65TFSolicitacoes_Usuario_Ult_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Solicitacoes_Usuario_Ult] <= @AV65TFSolicitacoes_Usuario_Ult_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([Solicitacoes_Usuario_Ult] <= @AV65TFSolicitacoes_Usuario_Ult_To)";
            }
         }
         else
         {
            GXv_int2[22] = 1;
         }
         if ( ! (DateTime.MinValue==AV68TFSolicitacoes_Data_Ult) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Solicitacoes_Data_Ult] >= @AV68TFSolicitacoes_Data_Ult)";
            }
            else
            {
               sWhereString = sWhereString + " ([Solicitacoes_Data_Ult] >= @AV68TFSolicitacoes_Data_Ult)";
            }
         }
         else
         {
            GXv_int2[23] = 1;
         }
         if ( ! (DateTime.MinValue==AV69TFSolicitacoes_Data_Ult_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Solicitacoes_Data_Ult] <= @AV69TFSolicitacoes_Data_Ult_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([Solicitacoes_Data_Ult] <= @AV69TFSolicitacoes_Data_Ult_To)";
            }
         }
         else
         {
            GXv_int2[24] = 1;
         }
         if ( AV75TFSolicitacoes_Status_Sels_Count > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV75TFSolicitacoes_Status_Sels, "[Solicitacoes_Status] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV75TFSolicitacoes_Status_Sels, "[Solicitacoes_Status] IN (", ")") + ")";
            }
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            sWhereString = " WHERE" + sWhereString;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [Solicitacoes_Novo_Projeto]";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [Solicitacoes_Novo_Projeto] DESC";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [Solicitacoes_Codigo]";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [Solicitacoes_Codigo] DESC";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [Contratada_Codigo]";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [Contratada_Codigo] DESC";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [Sistema_Codigo]";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [Sistema_Codigo] DESC";
         }
         else if ( ( AV13OrderedBy == 5 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [Servico_Codigo]";
         }
         else if ( ( AV13OrderedBy == 5 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [Servico_Codigo] DESC";
         }
         else if ( ( AV13OrderedBy == 6 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [Solicitacoes_Objetivo]";
         }
         else if ( ( AV13OrderedBy == 6 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [Solicitacoes_Objetivo] DESC";
         }
         else if ( ( AV13OrderedBy == 7 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [Solicitacoes_Usuario]";
         }
         else if ( ( AV13OrderedBy == 7 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [Solicitacoes_Usuario] DESC";
         }
         else if ( ( AV13OrderedBy == 8 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [Solicitacoes_Data]";
         }
         else if ( ( AV13OrderedBy == 8 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [Solicitacoes_Data] DESC";
         }
         else if ( ( AV13OrderedBy == 9 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [Solicitacoes_Usuario_Ult]";
         }
         else if ( ( AV13OrderedBy == 9 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [Solicitacoes_Usuario_Ult] DESC";
         }
         else if ( ( AV13OrderedBy == 10 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [Solicitacoes_Data_Ult]";
         }
         else if ( ( AV13OrderedBy == 10 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [Solicitacoes_Data_Ult] DESC";
         }
         else if ( ( AV13OrderedBy == 11 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [Solicitacoes_Status]";
         }
         else if ( ( AV13OrderedBy == 11 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [Solicitacoes_Status] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY [Solicitacoes_Codigo]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      protected Object[] conditional_H00AW3( IGxContext context ,
                                             String A446Solicitacoes_Status ,
                                             IGxCollection AV75TFSolicitacoes_Status_Sels ,
                                             String AV15DynamicFiltersSelector1 ,
                                             short AV16DynamicFiltersOperator1 ,
                                             String AV17Solicitacoes_Novo_Projeto1 ,
                                             bool AV18DynamicFiltersEnabled2 ,
                                             String AV19DynamicFiltersSelector2 ,
                                             short AV20DynamicFiltersOperator2 ,
                                             String AV21Solicitacoes_Novo_Projeto2 ,
                                             bool AV22DynamicFiltersEnabled3 ,
                                             String AV23DynamicFiltersSelector3 ,
                                             short AV24DynamicFiltersOperator3 ,
                                             String AV25Solicitacoes_Novo_Projeto3 ,
                                             int AV31TFSolicitacoes_Codigo ,
                                             int AV32TFSolicitacoes_Codigo_To ,
                                             int AV35TFContratada_Codigo ,
                                             int AV36TFContratada_Codigo_To ,
                                             int AV39TFSistema_Codigo ,
                                             int AV40TFSistema_Codigo_To ,
                                             int AV43TFServico_Codigo ,
                                             int AV44TFServico_Codigo_To ,
                                             String AV47TFSolicitacoes_Novo_Projeto_Sel ,
                                             String AV51TFSolicitacoes_Objetivo_Sel ,
                                             String AV50TFSolicitacoes_Objetivo ,
                                             int AV54TFSolicitacoes_Usuario ,
                                             int AV55TFSolicitacoes_Usuario_To ,
                                             DateTime AV58TFSolicitacoes_Data ,
                                             DateTime AV59TFSolicitacoes_Data_To ,
                                             int AV64TFSolicitacoes_Usuario_Ult ,
                                             int AV65TFSolicitacoes_Usuario_Ult_To ,
                                             DateTime AV68TFSolicitacoes_Data_Ult ,
                                             DateTime AV69TFSolicitacoes_Data_Ult_To ,
                                             int AV75TFSolicitacoes_Status_Sels_Count ,
                                             String A440Solicitacoes_Novo_Projeto ,
                                             int A439Solicitacoes_Codigo ,
                                             int A39Contratada_Codigo ,
                                             int A127Sistema_Codigo ,
                                             int A155Servico_Codigo ,
                                             String A441Solicitacoes_Objetivo ,
                                             int A442Solicitacoes_Usuario ,
                                             DateTime A444Solicitacoes_Data ,
                                             int A443Solicitacoes_Usuario_Ult ,
                                             DateTime A445Solicitacoes_Data_Ult ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [25] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM [Solicitacoes] WITH (NOLOCK)";
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "SOLICITACOES_NOVO_PROJETO") == 0 ) && ( AV16DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV17Solicitacoes_Novo_Projeto1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Solicitacoes_Novo_Projeto] like @lV17Solicitacoes_Novo_Projeto1)";
            }
            else
            {
               sWhereString = sWhereString + " ([Solicitacoes_Novo_Projeto] like @lV17Solicitacoes_Novo_Projeto1)";
            }
         }
         else
         {
            GXv_int4[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "SOLICITACOES_NOVO_PROJETO") == 0 ) && ( AV16DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV17Solicitacoes_Novo_Projeto1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Solicitacoes_Novo_Projeto] like '%' + @lV17Solicitacoes_Novo_Projeto1)";
            }
            else
            {
               sWhereString = sWhereString + " ([Solicitacoes_Novo_Projeto] like '%' + @lV17Solicitacoes_Novo_Projeto1)";
            }
         }
         else
         {
            GXv_int4[1] = 1;
         }
         if ( AV18DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "SOLICITACOES_NOVO_PROJETO") == 0 ) && ( AV20DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV21Solicitacoes_Novo_Projeto2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Solicitacoes_Novo_Projeto] like @lV21Solicitacoes_Novo_Projeto2)";
            }
            else
            {
               sWhereString = sWhereString + " ([Solicitacoes_Novo_Projeto] like @lV21Solicitacoes_Novo_Projeto2)";
            }
         }
         else
         {
            GXv_int4[2] = 1;
         }
         if ( AV18DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "SOLICITACOES_NOVO_PROJETO") == 0 ) && ( AV20DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV21Solicitacoes_Novo_Projeto2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Solicitacoes_Novo_Projeto] like '%' + @lV21Solicitacoes_Novo_Projeto2)";
            }
            else
            {
               sWhereString = sWhereString + " ([Solicitacoes_Novo_Projeto] like '%' + @lV21Solicitacoes_Novo_Projeto2)";
            }
         }
         else
         {
            GXv_int4[3] = 1;
         }
         if ( AV22DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "SOLICITACOES_NOVO_PROJETO") == 0 ) && ( AV24DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV25Solicitacoes_Novo_Projeto3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Solicitacoes_Novo_Projeto] like @lV25Solicitacoes_Novo_Projeto3)";
            }
            else
            {
               sWhereString = sWhereString + " ([Solicitacoes_Novo_Projeto] like @lV25Solicitacoes_Novo_Projeto3)";
            }
         }
         else
         {
            GXv_int4[4] = 1;
         }
         if ( AV22DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "SOLICITACOES_NOVO_PROJETO") == 0 ) && ( AV24DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV25Solicitacoes_Novo_Projeto3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Solicitacoes_Novo_Projeto] like '%' + @lV25Solicitacoes_Novo_Projeto3)";
            }
            else
            {
               sWhereString = sWhereString + " ([Solicitacoes_Novo_Projeto] like '%' + @lV25Solicitacoes_Novo_Projeto3)";
            }
         }
         else
         {
            GXv_int4[5] = 1;
         }
         if ( ! (0==AV31TFSolicitacoes_Codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Solicitacoes_Codigo] >= @AV31TFSolicitacoes_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " ([Solicitacoes_Codigo] >= @AV31TFSolicitacoes_Codigo)";
            }
         }
         else
         {
            GXv_int4[6] = 1;
         }
         if ( ! (0==AV32TFSolicitacoes_Codigo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Solicitacoes_Codigo] <= @AV32TFSolicitacoes_Codigo_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([Solicitacoes_Codigo] <= @AV32TFSolicitacoes_Codigo_To)";
            }
         }
         else
         {
            GXv_int4[7] = 1;
         }
         if ( ! (0==AV35TFContratada_Codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Contratada_Codigo] >= @AV35TFContratada_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " ([Contratada_Codigo] >= @AV35TFContratada_Codigo)";
            }
         }
         else
         {
            GXv_int4[8] = 1;
         }
         if ( ! (0==AV36TFContratada_Codigo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Contratada_Codigo] <= @AV36TFContratada_Codigo_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([Contratada_Codigo] <= @AV36TFContratada_Codigo_To)";
            }
         }
         else
         {
            GXv_int4[9] = 1;
         }
         if ( ! (0==AV39TFSistema_Codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Sistema_Codigo] >= @AV39TFSistema_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " ([Sistema_Codigo] >= @AV39TFSistema_Codigo)";
            }
         }
         else
         {
            GXv_int4[10] = 1;
         }
         if ( ! (0==AV40TFSistema_Codigo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Sistema_Codigo] <= @AV40TFSistema_Codigo_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([Sistema_Codigo] <= @AV40TFSistema_Codigo_To)";
            }
         }
         else
         {
            GXv_int4[11] = 1;
         }
         if ( ! (0==AV43TFServico_Codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Servico_Codigo] >= @AV43TFServico_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " ([Servico_Codigo] >= @AV43TFServico_Codigo)";
            }
         }
         else
         {
            GXv_int4[12] = 1;
         }
         if ( ! (0==AV44TFServico_Codigo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Servico_Codigo] <= @AV44TFServico_Codigo_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([Servico_Codigo] <= @AV44TFServico_Codigo_To)";
            }
         }
         else
         {
            GXv_int4[13] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV47TFSolicitacoes_Novo_Projeto_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Solicitacoes_Novo_Projeto] = @AV47TFSolicitacoes_Novo_Projeto_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([Solicitacoes_Novo_Projeto] = @AV47TFSolicitacoes_Novo_Projeto_Sel)";
            }
         }
         else
         {
            GXv_int4[14] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV51TFSolicitacoes_Objetivo_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV50TFSolicitacoes_Objetivo)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Solicitacoes_Objetivo] like @lV50TFSolicitacoes_Objetivo)";
            }
            else
            {
               sWhereString = sWhereString + " ([Solicitacoes_Objetivo] like @lV50TFSolicitacoes_Objetivo)";
            }
         }
         else
         {
            GXv_int4[15] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV51TFSolicitacoes_Objetivo_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Solicitacoes_Objetivo] = @AV51TFSolicitacoes_Objetivo_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([Solicitacoes_Objetivo] = @AV51TFSolicitacoes_Objetivo_Sel)";
            }
         }
         else
         {
            GXv_int4[16] = 1;
         }
         if ( ! (0==AV54TFSolicitacoes_Usuario) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Solicitacoes_Usuario] >= @AV54TFSolicitacoes_Usuario)";
            }
            else
            {
               sWhereString = sWhereString + " ([Solicitacoes_Usuario] >= @AV54TFSolicitacoes_Usuario)";
            }
         }
         else
         {
            GXv_int4[17] = 1;
         }
         if ( ! (0==AV55TFSolicitacoes_Usuario_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Solicitacoes_Usuario] <= @AV55TFSolicitacoes_Usuario_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([Solicitacoes_Usuario] <= @AV55TFSolicitacoes_Usuario_To)";
            }
         }
         else
         {
            GXv_int4[18] = 1;
         }
         if ( ! (DateTime.MinValue==AV58TFSolicitacoes_Data) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Solicitacoes_Data] >= @AV58TFSolicitacoes_Data)";
            }
            else
            {
               sWhereString = sWhereString + " ([Solicitacoes_Data] >= @AV58TFSolicitacoes_Data)";
            }
         }
         else
         {
            GXv_int4[19] = 1;
         }
         if ( ! (DateTime.MinValue==AV59TFSolicitacoes_Data_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Solicitacoes_Data] <= @AV59TFSolicitacoes_Data_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([Solicitacoes_Data] <= @AV59TFSolicitacoes_Data_To)";
            }
         }
         else
         {
            GXv_int4[20] = 1;
         }
         if ( ! (0==AV64TFSolicitacoes_Usuario_Ult) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Solicitacoes_Usuario_Ult] >= @AV64TFSolicitacoes_Usuario_Ult)";
            }
            else
            {
               sWhereString = sWhereString + " ([Solicitacoes_Usuario_Ult] >= @AV64TFSolicitacoes_Usuario_Ult)";
            }
         }
         else
         {
            GXv_int4[21] = 1;
         }
         if ( ! (0==AV65TFSolicitacoes_Usuario_Ult_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Solicitacoes_Usuario_Ult] <= @AV65TFSolicitacoes_Usuario_Ult_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([Solicitacoes_Usuario_Ult] <= @AV65TFSolicitacoes_Usuario_Ult_To)";
            }
         }
         else
         {
            GXv_int4[22] = 1;
         }
         if ( ! (DateTime.MinValue==AV68TFSolicitacoes_Data_Ult) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Solicitacoes_Data_Ult] >= @AV68TFSolicitacoes_Data_Ult)";
            }
            else
            {
               sWhereString = sWhereString + " ([Solicitacoes_Data_Ult] >= @AV68TFSolicitacoes_Data_Ult)";
            }
         }
         else
         {
            GXv_int4[23] = 1;
         }
         if ( ! (DateTime.MinValue==AV69TFSolicitacoes_Data_Ult_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Solicitacoes_Data_Ult] <= @AV69TFSolicitacoes_Data_Ult_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([Solicitacoes_Data_Ult] <= @AV69TFSolicitacoes_Data_Ult_To)";
            }
         }
         else
         {
            GXv_int4[24] = 1;
         }
         if ( AV75TFSolicitacoes_Status_Sels_Count > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV75TFSolicitacoes_Status_Sels, "[Solicitacoes_Status] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV75TFSolicitacoes_Status_Sels, "[Solicitacoes_Status] IN (", ")") + ")";
            }
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 5 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 5 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 6 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 6 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 7 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 7 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 8 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 8 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 9 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 9 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 10 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 10 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 11 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 11 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H00AW2(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (short)dynConstraints[3] , (String)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (short)dynConstraints[7] , (String)dynConstraints[8] , (bool)dynConstraints[9] , (String)dynConstraints[10] , (short)dynConstraints[11] , (String)dynConstraints[12] , (int)dynConstraints[13] , (int)dynConstraints[14] , (int)dynConstraints[15] , (int)dynConstraints[16] , (int)dynConstraints[17] , (int)dynConstraints[18] , (int)dynConstraints[19] , (int)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (String)dynConstraints[23] , (int)dynConstraints[24] , (int)dynConstraints[25] , (DateTime)dynConstraints[26] , (DateTime)dynConstraints[27] , (int)dynConstraints[28] , (int)dynConstraints[29] , (DateTime)dynConstraints[30] , (DateTime)dynConstraints[31] , (int)dynConstraints[32] , (String)dynConstraints[33] , (int)dynConstraints[34] , (int)dynConstraints[35] , (int)dynConstraints[36] , (int)dynConstraints[37] , (String)dynConstraints[38] , (int)dynConstraints[39] , (DateTime)dynConstraints[40] , (int)dynConstraints[41] , (DateTime)dynConstraints[42] , (short)dynConstraints[43] , (bool)dynConstraints[44] );
               case 1 :
                     return conditional_H00AW3(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (short)dynConstraints[3] , (String)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (short)dynConstraints[7] , (String)dynConstraints[8] , (bool)dynConstraints[9] , (String)dynConstraints[10] , (short)dynConstraints[11] , (String)dynConstraints[12] , (int)dynConstraints[13] , (int)dynConstraints[14] , (int)dynConstraints[15] , (int)dynConstraints[16] , (int)dynConstraints[17] , (int)dynConstraints[18] , (int)dynConstraints[19] , (int)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (String)dynConstraints[23] , (int)dynConstraints[24] , (int)dynConstraints[25] , (DateTime)dynConstraints[26] , (DateTime)dynConstraints[27] , (int)dynConstraints[28] , (int)dynConstraints[29] , (DateTime)dynConstraints[30] , (DateTime)dynConstraints[31] , (int)dynConstraints[32] , (String)dynConstraints[33] , (int)dynConstraints[34] , (int)dynConstraints[35] , (int)dynConstraints[36] , (int)dynConstraints[37] , (String)dynConstraints[38] , (int)dynConstraints[39] , (DateTime)dynConstraints[40] , (int)dynConstraints[41] , (DateTime)dynConstraints[42] , (short)dynConstraints[43] , (bool)dynConstraints[44] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00AW2 ;
          prmH00AW2 = new Object[] {
          new Object[] {"@lV17Solicitacoes_Novo_Projeto1",SqlDbType.Char,1,0} ,
          new Object[] {"@lV17Solicitacoes_Novo_Projeto1",SqlDbType.Char,1,0} ,
          new Object[] {"@lV21Solicitacoes_Novo_Projeto2",SqlDbType.Char,1,0} ,
          new Object[] {"@lV21Solicitacoes_Novo_Projeto2",SqlDbType.Char,1,0} ,
          new Object[] {"@lV25Solicitacoes_Novo_Projeto3",SqlDbType.Char,1,0} ,
          new Object[] {"@lV25Solicitacoes_Novo_Projeto3",SqlDbType.Char,1,0} ,
          new Object[] {"@AV31TFSolicitacoes_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV32TFSolicitacoes_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV35TFContratada_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV36TFContratada_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV39TFSistema_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV40TFSistema_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV43TFServico_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV44TFServico_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV47TFSolicitacoes_Novo_Projeto_Sel",SqlDbType.Char,1,0} ,
          new Object[] {"@lV50TFSolicitacoes_Objetivo",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV51TFSolicitacoes_Objetivo_Sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV54TFSolicitacoes_Usuario",SqlDbType.Int,6,0} ,
          new Object[] {"@AV55TFSolicitacoes_Usuario_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV58TFSolicitacoes_Data",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV59TFSolicitacoes_Data_To",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV64TFSolicitacoes_Usuario_Ult",SqlDbType.Int,6,0} ,
          new Object[] {"@AV65TFSolicitacoes_Usuario_Ult_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV68TFSolicitacoes_Data_Ult",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV69TFSolicitacoes_Data_Ult_To",SqlDbType.DateTime,8,5} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH00AW3 ;
          prmH00AW3 = new Object[] {
          new Object[] {"@lV17Solicitacoes_Novo_Projeto1",SqlDbType.Char,1,0} ,
          new Object[] {"@lV17Solicitacoes_Novo_Projeto1",SqlDbType.Char,1,0} ,
          new Object[] {"@lV21Solicitacoes_Novo_Projeto2",SqlDbType.Char,1,0} ,
          new Object[] {"@lV21Solicitacoes_Novo_Projeto2",SqlDbType.Char,1,0} ,
          new Object[] {"@lV25Solicitacoes_Novo_Projeto3",SqlDbType.Char,1,0} ,
          new Object[] {"@lV25Solicitacoes_Novo_Projeto3",SqlDbType.Char,1,0} ,
          new Object[] {"@AV31TFSolicitacoes_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV32TFSolicitacoes_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV35TFContratada_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV36TFContratada_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV39TFSistema_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV40TFSistema_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV43TFServico_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV44TFServico_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV47TFSolicitacoes_Novo_Projeto_Sel",SqlDbType.Char,1,0} ,
          new Object[] {"@lV50TFSolicitacoes_Objetivo",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV51TFSolicitacoes_Objetivo_Sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV54TFSolicitacoes_Usuario",SqlDbType.Int,6,0} ,
          new Object[] {"@AV55TFSolicitacoes_Usuario_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV58TFSolicitacoes_Data",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV59TFSolicitacoes_Data_To",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV64TFSolicitacoes_Usuario_Ult",SqlDbType.Int,6,0} ,
          new Object[] {"@AV65TFSolicitacoes_Usuario_Ult_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV68TFSolicitacoes_Data_Ult",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV69TFSolicitacoes_Data_Ult_To",SqlDbType.DateTime,8,5}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00AW2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00AW2,11,0,true,false )
             ,new CursorDef("H00AW3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00AW3,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getString(1, 1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDateTime(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((DateTime[]) buf[3])[0] = rslt.getGXDateTime(4) ;
                ((int[]) buf[4])[0] = rslt.getInt(5) ;
                ((String[]) buf[5])[0] = rslt.getLongVarchar(6) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(6);
                ((String[]) buf[7])[0] = rslt.getString(7, 1) ;
                ((int[]) buf[8])[0] = rslt.getInt(8) ;
                ((int[]) buf[9])[0] = rslt.getInt(9) ;
                ((int[]) buf[10])[0] = rslt.getInt(10) ;
                ((int[]) buf[11])[0] = rslt.getInt(11) ;
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[36]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[37]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[38]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[39]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[40]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[41]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[42]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[43]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[44]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[45]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[46]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[47]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[48]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[49]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[50]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[51]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[52]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[53]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[54]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[55]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[56]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[57]);
                }
                if ( (short)parms[28] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[58]);
                }
                if ( (short)parms[29] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[59]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[31]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[32]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[33]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[34]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[35]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[36]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[37]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[38]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[39]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[40]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[41]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[42]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[43]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[44]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[45]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[46]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[47]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[48]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[49]);
                }
                return;
       }
    }

 }

}
