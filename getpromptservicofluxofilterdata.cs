/*
               File: GetPromptServicoFluxoFilterData
        Description: Get Prompt Servico Fluxo Filter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/21/2020 18:5:32.28
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getpromptservicofluxofilterdata : GXProcedure
   {
      public getpromptservicofluxofilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getpromptservicofluxofilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV26DDOName = aP0_DDOName;
         this.AV24SearchTxt = aP1_SearchTxt;
         this.AV25SearchTxtTo = aP2_SearchTxtTo;
         this.AV30OptionsJson = "" ;
         this.AV33OptionsDescJson = "" ;
         this.AV35OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV30OptionsJson;
         aP4_OptionsDescJson=this.AV33OptionsDescJson;
         aP5_OptionIndexesJson=this.AV35OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV26DDOName = aP0_DDOName;
         this.AV24SearchTxt = aP1_SearchTxt;
         this.AV25SearchTxtTo = aP2_SearchTxtTo;
         this.AV30OptionsJson = "" ;
         this.AV33OptionsDescJson = "" ;
         this.AV35OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV30OptionsJson;
         aP4_OptionsDescJson=this.AV33OptionsDescJson;
         aP5_OptionIndexesJson=this.AV35OptionIndexesJson;
         return AV35OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getpromptservicofluxofilterdata objgetpromptservicofluxofilterdata;
         objgetpromptservicofluxofilterdata = new getpromptservicofluxofilterdata();
         objgetpromptservicofluxofilterdata.AV26DDOName = aP0_DDOName;
         objgetpromptservicofluxofilterdata.AV24SearchTxt = aP1_SearchTxt;
         objgetpromptservicofluxofilterdata.AV25SearchTxtTo = aP2_SearchTxtTo;
         objgetpromptservicofluxofilterdata.AV30OptionsJson = "" ;
         objgetpromptservicofluxofilterdata.AV33OptionsDescJson = "" ;
         objgetpromptservicofluxofilterdata.AV35OptionIndexesJson = "" ;
         objgetpromptservicofluxofilterdata.context.SetSubmitInitialConfig(context);
         objgetpromptservicofluxofilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetpromptservicofluxofilterdata);
         aP3_OptionsJson=this.AV30OptionsJson;
         aP4_OptionsDescJson=this.AV33OptionsDescJson;
         aP5_OptionIndexesJson=this.AV35OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getpromptservicofluxofilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV29Options = (IGxCollection)(new GxSimpleCollection());
         AV32OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV34OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV26DDOName), "DDO_SERVICOFLUXO_SERVICOSIGLA") == 0 )
         {
            /* Execute user subroutine: 'LOADSERVICOFLUXO_SERVICOSIGLAOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV26DDOName), "DDO_SERVICOFLUXO_SRVPOSSIGLA") == 0 )
         {
            /* Execute user subroutine: 'LOADSERVICOFLUXO_SRVPOSSIGLAOPTIONS' */
            S131 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV30OptionsJson = AV29Options.ToJSonString(false);
         AV33OptionsDescJson = AV32OptionsDesc.ToJSonString(false);
         AV35OptionIndexesJson = AV34OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV37Session.Get("PromptServicoFluxoGridState"), "") == 0 )
         {
            AV39GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "PromptServicoFluxoGridState"), "");
         }
         else
         {
            AV39GridState.FromXml(AV37Session.Get("PromptServicoFluxoGridState"), "");
         }
         AV55GXV1 = 1;
         while ( AV55GXV1 <= AV39GridState.gxTpr_Filtervalues.Count )
         {
            AV40GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV39GridState.gxTpr_Filtervalues.Item(AV55GXV1));
            if ( StringUtil.StrCmp(AV40GridStateFilterValue.gxTpr_Name, "TFSERVICOFLUXO_CODIGO") == 0 )
            {
               AV10TFServicoFluxo_Codigo = (int)(NumberUtil.Val( AV40GridStateFilterValue.gxTpr_Value, "."));
               AV11TFServicoFluxo_Codigo_To = (int)(NumberUtil.Val( AV40GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV40GridStateFilterValue.gxTpr_Name, "TFSERVICOFLUXO_SERVICOCOD") == 0 )
            {
               AV12TFServicoFluxo_ServicoCod = (int)(NumberUtil.Val( AV40GridStateFilterValue.gxTpr_Value, "."));
               AV13TFServicoFluxo_ServicoCod_To = (int)(NumberUtil.Val( AV40GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV40GridStateFilterValue.gxTpr_Name, "TFSERVICOFLUXO_SERVICOSIGLA") == 0 )
            {
               AV14TFServicoFluxo_ServicoSigla = AV40GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV40GridStateFilterValue.gxTpr_Name, "TFSERVICOFLUXO_SERVICOSIGLA_SEL") == 0 )
            {
               AV15TFServicoFluxo_ServicoSigla_Sel = AV40GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV40GridStateFilterValue.gxTpr_Name, "TFSERVICOFLUXO_SERVICOTPHRQ_SEL") == 0 )
            {
               AV16TFServicoFluxo_ServicoTpHrq_SelsJson = AV40GridStateFilterValue.gxTpr_Value;
               AV17TFServicoFluxo_ServicoTpHrq_Sels.FromJSonString(AV16TFServicoFluxo_ServicoTpHrq_SelsJson);
            }
            else if ( StringUtil.StrCmp(AV40GridStateFilterValue.gxTpr_Name, "TFSERVICOFLUXO_ORDEM") == 0 )
            {
               AV18TFServicoFluxo_Ordem = (short)(NumberUtil.Val( AV40GridStateFilterValue.gxTpr_Value, "."));
               AV19TFServicoFluxo_Ordem_To = (short)(NumberUtil.Val( AV40GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV40GridStateFilterValue.gxTpr_Name, "TFSERVICOFLUXO_SERVICOPOS") == 0 )
            {
               AV20TFServicoFluxo_ServicoPos = (int)(NumberUtil.Val( AV40GridStateFilterValue.gxTpr_Value, "."));
               AV21TFServicoFluxo_ServicoPos_To = (int)(NumberUtil.Val( AV40GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV40GridStateFilterValue.gxTpr_Name, "TFSERVICOFLUXO_SRVPOSSIGLA") == 0 )
            {
               AV22TFServicoFluxo_SrvPosSigla = AV40GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV40GridStateFilterValue.gxTpr_Name, "TFSERVICOFLUXO_SRVPOSSIGLA_SEL") == 0 )
            {
               AV23TFServicoFluxo_SrvPosSigla_Sel = AV40GridStateFilterValue.gxTpr_Value;
            }
            AV55GXV1 = (int)(AV55GXV1+1);
         }
         if ( AV39GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV41GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV39GridState.gxTpr_Dynamicfilters.Item(1));
            AV42DynamicFiltersSelector1 = AV41GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV42DynamicFiltersSelector1, "SERVICOFLUXO_SERVICOSIGLA") == 0 )
            {
               AV43DynamicFiltersOperator1 = AV41GridStateDynamicFilter.gxTpr_Operator;
               AV44ServicoFluxo_ServicoSigla1 = AV41GridStateDynamicFilter.gxTpr_Value;
            }
            if ( AV39GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV45DynamicFiltersEnabled2 = true;
               AV41GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV39GridState.gxTpr_Dynamicfilters.Item(2));
               AV46DynamicFiltersSelector2 = AV41GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV46DynamicFiltersSelector2, "SERVICOFLUXO_SERVICOSIGLA") == 0 )
               {
                  AV47DynamicFiltersOperator2 = AV41GridStateDynamicFilter.gxTpr_Operator;
                  AV48ServicoFluxo_ServicoSigla2 = AV41GridStateDynamicFilter.gxTpr_Value;
               }
               if ( AV39GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  AV49DynamicFiltersEnabled3 = true;
                  AV41GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV39GridState.gxTpr_Dynamicfilters.Item(3));
                  AV50DynamicFiltersSelector3 = AV41GridStateDynamicFilter.gxTpr_Selected;
                  if ( StringUtil.StrCmp(AV50DynamicFiltersSelector3, "SERVICOFLUXO_SERVICOSIGLA") == 0 )
                  {
                     AV51DynamicFiltersOperator3 = AV41GridStateDynamicFilter.gxTpr_Operator;
                     AV52ServicoFluxo_ServicoSigla3 = AV41GridStateDynamicFilter.gxTpr_Value;
                  }
               }
            }
         }
      }

      protected void S121( )
      {
         /* 'LOADSERVICOFLUXO_SERVICOSIGLAOPTIONS' Routine */
         AV14TFServicoFluxo_ServicoSigla = AV24SearchTxt;
         AV15TFServicoFluxo_ServicoSigla_Sel = "";
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              A1533ServicoFluxo_ServicoTpHrq ,
                                              AV17TFServicoFluxo_ServicoTpHrq_Sels ,
                                              AV42DynamicFiltersSelector1 ,
                                              AV43DynamicFiltersOperator1 ,
                                              AV44ServicoFluxo_ServicoSigla1 ,
                                              AV45DynamicFiltersEnabled2 ,
                                              AV46DynamicFiltersSelector2 ,
                                              AV47DynamicFiltersOperator2 ,
                                              AV48ServicoFluxo_ServicoSigla2 ,
                                              AV49DynamicFiltersEnabled3 ,
                                              AV50DynamicFiltersSelector3 ,
                                              AV51DynamicFiltersOperator3 ,
                                              AV52ServicoFluxo_ServicoSigla3 ,
                                              AV10TFServicoFluxo_Codigo ,
                                              AV11TFServicoFluxo_Codigo_To ,
                                              AV12TFServicoFluxo_ServicoCod ,
                                              AV13TFServicoFluxo_ServicoCod_To ,
                                              AV15TFServicoFluxo_ServicoSigla_Sel ,
                                              AV14TFServicoFluxo_ServicoSigla ,
                                              AV17TFServicoFluxo_ServicoTpHrq_Sels.Count ,
                                              AV18TFServicoFluxo_Ordem ,
                                              AV19TFServicoFluxo_Ordem_To ,
                                              AV20TFServicoFluxo_ServicoPos ,
                                              AV21TFServicoFluxo_ServicoPos_To ,
                                              AV23TFServicoFluxo_SrvPosSigla_Sel ,
                                              AV22TFServicoFluxo_SrvPosSigla ,
                                              A1523ServicoFluxo_ServicoSigla ,
                                              A1528ServicoFluxo_Codigo ,
                                              A1522ServicoFluxo_ServicoCod ,
                                              A1532ServicoFluxo_Ordem ,
                                              A1526ServicoFluxo_ServicoPos ,
                                              A1527ServicoFluxo_SrvPosSigla },
                                              new int[] {
                                              TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT,
                                              TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN
                                              }
         });
         lV44ServicoFluxo_ServicoSigla1 = StringUtil.PadR( StringUtil.RTrim( AV44ServicoFluxo_ServicoSigla1), 15, "%");
         lV44ServicoFluxo_ServicoSigla1 = StringUtil.PadR( StringUtil.RTrim( AV44ServicoFluxo_ServicoSigla1), 15, "%");
         lV48ServicoFluxo_ServicoSigla2 = StringUtil.PadR( StringUtil.RTrim( AV48ServicoFluxo_ServicoSigla2), 15, "%");
         lV48ServicoFluxo_ServicoSigla2 = StringUtil.PadR( StringUtil.RTrim( AV48ServicoFluxo_ServicoSigla2), 15, "%");
         lV52ServicoFluxo_ServicoSigla3 = StringUtil.PadR( StringUtil.RTrim( AV52ServicoFluxo_ServicoSigla3), 15, "%");
         lV52ServicoFluxo_ServicoSigla3 = StringUtil.PadR( StringUtil.RTrim( AV52ServicoFluxo_ServicoSigla3), 15, "%");
         lV14TFServicoFluxo_ServicoSigla = StringUtil.PadR( StringUtil.RTrim( AV14TFServicoFluxo_ServicoSigla), 15, "%");
         lV22TFServicoFluxo_SrvPosSigla = StringUtil.PadR( StringUtil.RTrim( AV22TFServicoFluxo_SrvPosSigla), 15, "%");
         /* Using cursor P00SE2 */
         pr_default.execute(0, new Object[] {lV44ServicoFluxo_ServicoSigla1, lV44ServicoFluxo_ServicoSigla1, lV48ServicoFluxo_ServicoSigla2, lV48ServicoFluxo_ServicoSigla2, lV52ServicoFluxo_ServicoSigla3, lV52ServicoFluxo_ServicoSigla3, AV10TFServicoFluxo_Codigo, AV11TFServicoFluxo_Codigo_To, AV12TFServicoFluxo_ServicoCod, AV13TFServicoFluxo_ServicoCod_To, lV14TFServicoFluxo_ServicoSigla, AV15TFServicoFluxo_ServicoSigla_Sel, AV18TFServicoFluxo_Ordem, AV19TFServicoFluxo_Ordem_To, AV20TFServicoFluxo_ServicoPos, AV21TFServicoFluxo_ServicoPos_To, lV22TFServicoFluxo_SrvPosSigla, AV23TFServicoFluxo_SrvPosSigla_Sel});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKSE2 = false;
            A1523ServicoFluxo_ServicoSigla = P00SE2_A1523ServicoFluxo_ServicoSigla[0];
            n1523ServicoFluxo_ServicoSigla = P00SE2_n1523ServicoFluxo_ServicoSigla[0];
            A1527ServicoFluxo_SrvPosSigla = P00SE2_A1527ServicoFluxo_SrvPosSigla[0];
            n1527ServicoFluxo_SrvPosSigla = P00SE2_n1527ServicoFluxo_SrvPosSigla[0];
            A1526ServicoFluxo_ServicoPos = P00SE2_A1526ServicoFluxo_ServicoPos[0];
            n1526ServicoFluxo_ServicoPos = P00SE2_n1526ServicoFluxo_ServicoPos[0];
            A1532ServicoFluxo_Ordem = P00SE2_A1532ServicoFluxo_Ordem[0];
            n1532ServicoFluxo_Ordem = P00SE2_n1532ServicoFluxo_Ordem[0];
            A1533ServicoFluxo_ServicoTpHrq = P00SE2_A1533ServicoFluxo_ServicoTpHrq[0];
            n1533ServicoFluxo_ServicoTpHrq = P00SE2_n1533ServicoFluxo_ServicoTpHrq[0];
            A1522ServicoFluxo_ServicoCod = P00SE2_A1522ServicoFluxo_ServicoCod[0];
            A1528ServicoFluxo_Codigo = P00SE2_A1528ServicoFluxo_Codigo[0];
            A1527ServicoFluxo_SrvPosSigla = P00SE2_A1527ServicoFluxo_SrvPosSigla[0];
            n1527ServicoFluxo_SrvPosSigla = P00SE2_n1527ServicoFluxo_SrvPosSigla[0];
            A1523ServicoFluxo_ServicoSigla = P00SE2_A1523ServicoFluxo_ServicoSigla[0];
            n1523ServicoFluxo_ServicoSigla = P00SE2_n1523ServicoFluxo_ServicoSigla[0];
            A1533ServicoFluxo_ServicoTpHrq = P00SE2_A1533ServicoFluxo_ServicoTpHrq[0];
            n1533ServicoFluxo_ServicoTpHrq = P00SE2_n1533ServicoFluxo_ServicoTpHrq[0];
            AV36count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( StringUtil.StrCmp(P00SE2_A1523ServicoFluxo_ServicoSigla[0], A1523ServicoFluxo_ServicoSigla) == 0 ) )
            {
               BRKSE2 = false;
               A1522ServicoFluxo_ServicoCod = P00SE2_A1522ServicoFluxo_ServicoCod[0];
               A1528ServicoFluxo_Codigo = P00SE2_A1528ServicoFluxo_Codigo[0];
               AV36count = (long)(AV36count+1);
               BRKSE2 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A1523ServicoFluxo_ServicoSigla)) )
            {
               AV28Option = A1523ServicoFluxo_ServicoSigla;
               AV29Options.Add(AV28Option, 0);
               AV34OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV36count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV29Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKSE2 )
            {
               BRKSE2 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      protected void S131( )
      {
         /* 'LOADSERVICOFLUXO_SRVPOSSIGLAOPTIONS' Routine */
         AV22TFServicoFluxo_SrvPosSigla = AV24SearchTxt;
         AV23TFServicoFluxo_SrvPosSigla_Sel = "";
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              A1533ServicoFluxo_ServicoTpHrq ,
                                              AV17TFServicoFluxo_ServicoTpHrq_Sels ,
                                              AV42DynamicFiltersSelector1 ,
                                              AV43DynamicFiltersOperator1 ,
                                              AV44ServicoFluxo_ServicoSigla1 ,
                                              AV45DynamicFiltersEnabled2 ,
                                              AV46DynamicFiltersSelector2 ,
                                              AV47DynamicFiltersOperator2 ,
                                              AV48ServicoFluxo_ServicoSigla2 ,
                                              AV49DynamicFiltersEnabled3 ,
                                              AV50DynamicFiltersSelector3 ,
                                              AV51DynamicFiltersOperator3 ,
                                              AV52ServicoFluxo_ServicoSigla3 ,
                                              AV10TFServicoFluxo_Codigo ,
                                              AV11TFServicoFluxo_Codigo_To ,
                                              AV12TFServicoFluxo_ServicoCod ,
                                              AV13TFServicoFluxo_ServicoCod_To ,
                                              AV15TFServicoFluxo_ServicoSigla_Sel ,
                                              AV14TFServicoFluxo_ServicoSigla ,
                                              AV17TFServicoFluxo_ServicoTpHrq_Sels.Count ,
                                              AV18TFServicoFluxo_Ordem ,
                                              AV19TFServicoFluxo_Ordem_To ,
                                              AV20TFServicoFluxo_ServicoPos ,
                                              AV21TFServicoFluxo_ServicoPos_To ,
                                              AV23TFServicoFluxo_SrvPosSigla_Sel ,
                                              AV22TFServicoFluxo_SrvPosSigla ,
                                              A1523ServicoFluxo_ServicoSigla ,
                                              A1528ServicoFluxo_Codigo ,
                                              A1522ServicoFluxo_ServicoCod ,
                                              A1532ServicoFluxo_Ordem ,
                                              A1526ServicoFluxo_ServicoPos ,
                                              A1527ServicoFluxo_SrvPosSigla },
                                              new int[] {
                                              TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT,
                                              TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN
                                              }
         });
         lV44ServicoFluxo_ServicoSigla1 = StringUtil.PadR( StringUtil.RTrim( AV44ServicoFluxo_ServicoSigla1), 15, "%");
         lV44ServicoFluxo_ServicoSigla1 = StringUtil.PadR( StringUtil.RTrim( AV44ServicoFluxo_ServicoSigla1), 15, "%");
         lV48ServicoFluxo_ServicoSigla2 = StringUtil.PadR( StringUtil.RTrim( AV48ServicoFluxo_ServicoSigla2), 15, "%");
         lV48ServicoFluxo_ServicoSigla2 = StringUtil.PadR( StringUtil.RTrim( AV48ServicoFluxo_ServicoSigla2), 15, "%");
         lV52ServicoFluxo_ServicoSigla3 = StringUtil.PadR( StringUtil.RTrim( AV52ServicoFluxo_ServicoSigla3), 15, "%");
         lV52ServicoFluxo_ServicoSigla3 = StringUtil.PadR( StringUtil.RTrim( AV52ServicoFluxo_ServicoSigla3), 15, "%");
         lV14TFServicoFluxo_ServicoSigla = StringUtil.PadR( StringUtil.RTrim( AV14TFServicoFluxo_ServicoSigla), 15, "%");
         lV22TFServicoFluxo_SrvPosSigla = StringUtil.PadR( StringUtil.RTrim( AV22TFServicoFluxo_SrvPosSigla), 15, "%");
         /* Using cursor P00SE3 */
         pr_default.execute(1, new Object[] {lV44ServicoFluxo_ServicoSigla1, lV44ServicoFluxo_ServicoSigla1, lV48ServicoFluxo_ServicoSigla2, lV48ServicoFluxo_ServicoSigla2, lV52ServicoFluxo_ServicoSigla3, lV52ServicoFluxo_ServicoSigla3, AV10TFServicoFluxo_Codigo, AV11TFServicoFluxo_Codigo_To, AV12TFServicoFluxo_ServicoCod, AV13TFServicoFluxo_ServicoCod_To, lV14TFServicoFluxo_ServicoSigla, AV15TFServicoFluxo_ServicoSigla_Sel, AV18TFServicoFluxo_Ordem, AV19TFServicoFluxo_Ordem_To, AV20TFServicoFluxo_ServicoPos, AV21TFServicoFluxo_ServicoPos_To, lV22TFServicoFluxo_SrvPosSigla, AV23TFServicoFluxo_SrvPosSigla_Sel});
         while ( (pr_default.getStatus(1) != 101) )
         {
            BRKSE4 = false;
            A1527ServicoFluxo_SrvPosSigla = P00SE3_A1527ServicoFluxo_SrvPosSigla[0];
            n1527ServicoFluxo_SrvPosSigla = P00SE3_n1527ServicoFluxo_SrvPosSigla[0];
            A1526ServicoFluxo_ServicoPos = P00SE3_A1526ServicoFluxo_ServicoPos[0];
            n1526ServicoFluxo_ServicoPos = P00SE3_n1526ServicoFluxo_ServicoPos[0];
            A1532ServicoFluxo_Ordem = P00SE3_A1532ServicoFluxo_Ordem[0];
            n1532ServicoFluxo_Ordem = P00SE3_n1532ServicoFluxo_Ordem[0];
            A1533ServicoFluxo_ServicoTpHrq = P00SE3_A1533ServicoFluxo_ServicoTpHrq[0];
            n1533ServicoFluxo_ServicoTpHrq = P00SE3_n1533ServicoFluxo_ServicoTpHrq[0];
            A1522ServicoFluxo_ServicoCod = P00SE3_A1522ServicoFluxo_ServicoCod[0];
            A1528ServicoFluxo_Codigo = P00SE3_A1528ServicoFluxo_Codigo[0];
            A1523ServicoFluxo_ServicoSigla = P00SE3_A1523ServicoFluxo_ServicoSigla[0];
            n1523ServicoFluxo_ServicoSigla = P00SE3_n1523ServicoFluxo_ServicoSigla[0];
            A1527ServicoFluxo_SrvPosSigla = P00SE3_A1527ServicoFluxo_SrvPosSigla[0];
            n1527ServicoFluxo_SrvPosSigla = P00SE3_n1527ServicoFluxo_SrvPosSigla[0];
            A1533ServicoFluxo_ServicoTpHrq = P00SE3_A1533ServicoFluxo_ServicoTpHrq[0];
            n1533ServicoFluxo_ServicoTpHrq = P00SE3_n1533ServicoFluxo_ServicoTpHrq[0];
            A1523ServicoFluxo_ServicoSigla = P00SE3_A1523ServicoFluxo_ServicoSigla[0];
            n1523ServicoFluxo_ServicoSigla = P00SE3_n1523ServicoFluxo_ServicoSigla[0];
            AV36count = 0;
            while ( (pr_default.getStatus(1) != 101) && ( StringUtil.StrCmp(P00SE3_A1527ServicoFluxo_SrvPosSigla[0], A1527ServicoFluxo_SrvPosSigla) == 0 ) )
            {
               BRKSE4 = false;
               A1526ServicoFluxo_ServicoPos = P00SE3_A1526ServicoFluxo_ServicoPos[0];
               n1526ServicoFluxo_ServicoPos = P00SE3_n1526ServicoFluxo_ServicoPos[0];
               A1528ServicoFluxo_Codigo = P00SE3_A1528ServicoFluxo_Codigo[0];
               AV36count = (long)(AV36count+1);
               BRKSE4 = true;
               pr_default.readNext(1);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A1527ServicoFluxo_SrvPosSigla)) )
            {
               AV28Option = A1527ServicoFluxo_SrvPosSigla;
               AV29Options.Add(AV28Option, 0);
               AV34OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV36count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV29Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKSE4 )
            {
               BRKSE4 = true;
               pr_default.readNext(1);
            }
         }
         pr_default.close(1);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV29Options = new GxSimpleCollection();
         AV32OptionsDesc = new GxSimpleCollection();
         AV34OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV37Session = context.GetSession();
         AV39GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV40GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV14TFServicoFluxo_ServicoSigla = "";
         AV15TFServicoFluxo_ServicoSigla_Sel = "";
         AV16TFServicoFluxo_ServicoTpHrq_SelsJson = "";
         AV17TFServicoFluxo_ServicoTpHrq_Sels = new GxSimpleCollection();
         AV22TFServicoFluxo_SrvPosSigla = "";
         AV23TFServicoFluxo_SrvPosSigla_Sel = "";
         AV41GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV42DynamicFiltersSelector1 = "";
         AV44ServicoFluxo_ServicoSigla1 = "";
         AV46DynamicFiltersSelector2 = "";
         AV48ServicoFluxo_ServicoSigla2 = "";
         AV50DynamicFiltersSelector3 = "";
         AV52ServicoFluxo_ServicoSigla3 = "";
         scmdbuf = "";
         lV44ServicoFluxo_ServicoSigla1 = "";
         lV48ServicoFluxo_ServicoSigla2 = "";
         lV52ServicoFluxo_ServicoSigla3 = "";
         lV14TFServicoFluxo_ServicoSigla = "";
         lV22TFServicoFluxo_SrvPosSigla = "";
         A1523ServicoFluxo_ServicoSigla = "";
         A1527ServicoFluxo_SrvPosSigla = "";
         P00SE2_A1523ServicoFluxo_ServicoSigla = new String[] {""} ;
         P00SE2_n1523ServicoFluxo_ServicoSigla = new bool[] {false} ;
         P00SE2_A1527ServicoFluxo_SrvPosSigla = new String[] {""} ;
         P00SE2_n1527ServicoFluxo_SrvPosSigla = new bool[] {false} ;
         P00SE2_A1526ServicoFluxo_ServicoPos = new int[1] ;
         P00SE2_n1526ServicoFluxo_ServicoPos = new bool[] {false} ;
         P00SE2_A1532ServicoFluxo_Ordem = new short[1] ;
         P00SE2_n1532ServicoFluxo_Ordem = new bool[] {false} ;
         P00SE2_A1533ServicoFluxo_ServicoTpHrq = new short[1] ;
         P00SE2_n1533ServicoFluxo_ServicoTpHrq = new bool[] {false} ;
         P00SE2_A1522ServicoFluxo_ServicoCod = new int[1] ;
         P00SE2_A1528ServicoFluxo_Codigo = new int[1] ;
         AV28Option = "";
         P00SE3_A1527ServicoFluxo_SrvPosSigla = new String[] {""} ;
         P00SE3_n1527ServicoFluxo_SrvPosSigla = new bool[] {false} ;
         P00SE3_A1526ServicoFluxo_ServicoPos = new int[1] ;
         P00SE3_n1526ServicoFluxo_ServicoPos = new bool[] {false} ;
         P00SE3_A1532ServicoFluxo_Ordem = new short[1] ;
         P00SE3_n1532ServicoFluxo_Ordem = new bool[] {false} ;
         P00SE3_A1533ServicoFluxo_ServicoTpHrq = new short[1] ;
         P00SE3_n1533ServicoFluxo_ServicoTpHrq = new bool[] {false} ;
         P00SE3_A1522ServicoFluxo_ServicoCod = new int[1] ;
         P00SE3_A1528ServicoFluxo_Codigo = new int[1] ;
         P00SE3_A1523ServicoFluxo_ServicoSigla = new String[] {""} ;
         P00SE3_n1523ServicoFluxo_ServicoSigla = new bool[] {false} ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getpromptservicofluxofilterdata__default(),
            new Object[][] {
                new Object[] {
               P00SE2_A1523ServicoFluxo_ServicoSigla, P00SE2_n1523ServicoFluxo_ServicoSigla, P00SE2_A1527ServicoFluxo_SrvPosSigla, P00SE2_n1527ServicoFluxo_SrvPosSigla, P00SE2_A1526ServicoFluxo_ServicoPos, P00SE2_n1526ServicoFluxo_ServicoPos, P00SE2_A1532ServicoFluxo_Ordem, P00SE2_n1532ServicoFluxo_Ordem, P00SE2_A1533ServicoFluxo_ServicoTpHrq, P00SE2_n1533ServicoFluxo_ServicoTpHrq,
               P00SE2_A1522ServicoFluxo_ServicoCod, P00SE2_A1528ServicoFluxo_Codigo
               }
               , new Object[] {
               P00SE3_A1527ServicoFluxo_SrvPosSigla, P00SE3_n1527ServicoFluxo_SrvPosSigla, P00SE3_A1526ServicoFluxo_ServicoPos, P00SE3_n1526ServicoFluxo_ServicoPos, P00SE3_A1532ServicoFluxo_Ordem, P00SE3_n1532ServicoFluxo_Ordem, P00SE3_A1533ServicoFluxo_ServicoTpHrq, P00SE3_n1533ServicoFluxo_ServicoTpHrq, P00SE3_A1522ServicoFluxo_ServicoCod, P00SE3_A1528ServicoFluxo_Codigo,
               P00SE3_A1523ServicoFluxo_ServicoSigla, P00SE3_n1523ServicoFluxo_ServicoSigla
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV18TFServicoFluxo_Ordem ;
      private short AV19TFServicoFluxo_Ordem_To ;
      private short AV43DynamicFiltersOperator1 ;
      private short AV47DynamicFiltersOperator2 ;
      private short AV51DynamicFiltersOperator3 ;
      private short A1533ServicoFluxo_ServicoTpHrq ;
      private short A1532ServicoFluxo_Ordem ;
      private int AV55GXV1 ;
      private int AV10TFServicoFluxo_Codigo ;
      private int AV11TFServicoFluxo_Codigo_To ;
      private int AV12TFServicoFluxo_ServicoCod ;
      private int AV13TFServicoFluxo_ServicoCod_To ;
      private int AV20TFServicoFluxo_ServicoPos ;
      private int AV21TFServicoFluxo_ServicoPos_To ;
      private int AV17TFServicoFluxo_ServicoTpHrq_Sels_Count ;
      private int A1528ServicoFluxo_Codigo ;
      private int A1522ServicoFluxo_ServicoCod ;
      private int A1526ServicoFluxo_ServicoPos ;
      private long AV36count ;
      private String AV14TFServicoFluxo_ServicoSigla ;
      private String AV15TFServicoFluxo_ServicoSigla_Sel ;
      private String AV22TFServicoFluxo_SrvPosSigla ;
      private String AV23TFServicoFluxo_SrvPosSigla_Sel ;
      private String AV44ServicoFluxo_ServicoSigla1 ;
      private String AV48ServicoFluxo_ServicoSigla2 ;
      private String AV52ServicoFluxo_ServicoSigla3 ;
      private String scmdbuf ;
      private String lV44ServicoFluxo_ServicoSigla1 ;
      private String lV48ServicoFluxo_ServicoSigla2 ;
      private String lV52ServicoFluxo_ServicoSigla3 ;
      private String lV14TFServicoFluxo_ServicoSigla ;
      private String lV22TFServicoFluxo_SrvPosSigla ;
      private String A1523ServicoFluxo_ServicoSigla ;
      private String A1527ServicoFluxo_SrvPosSigla ;
      private bool returnInSub ;
      private bool AV45DynamicFiltersEnabled2 ;
      private bool AV49DynamicFiltersEnabled3 ;
      private bool BRKSE2 ;
      private bool n1523ServicoFluxo_ServicoSigla ;
      private bool n1527ServicoFluxo_SrvPosSigla ;
      private bool n1526ServicoFluxo_ServicoPos ;
      private bool n1532ServicoFluxo_Ordem ;
      private bool n1533ServicoFluxo_ServicoTpHrq ;
      private bool BRKSE4 ;
      private String AV35OptionIndexesJson ;
      private String AV30OptionsJson ;
      private String AV33OptionsDescJson ;
      private String AV16TFServicoFluxo_ServicoTpHrq_SelsJson ;
      private String AV26DDOName ;
      private String AV24SearchTxt ;
      private String AV25SearchTxtTo ;
      private String AV42DynamicFiltersSelector1 ;
      private String AV46DynamicFiltersSelector2 ;
      private String AV50DynamicFiltersSelector3 ;
      private String AV28Option ;
      private IGxSession AV37Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private String[] P00SE2_A1523ServicoFluxo_ServicoSigla ;
      private bool[] P00SE2_n1523ServicoFluxo_ServicoSigla ;
      private String[] P00SE2_A1527ServicoFluxo_SrvPosSigla ;
      private bool[] P00SE2_n1527ServicoFluxo_SrvPosSigla ;
      private int[] P00SE2_A1526ServicoFluxo_ServicoPos ;
      private bool[] P00SE2_n1526ServicoFluxo_ServicoPos ;
      private short[] P00SE2_A1532ServicoFluxo_Ordem ;
      private bool[] P00SE2_n1532ServicoFluxo_Ordem ;
      private short[] P00SE2_A1533ServicoFluxo_ServicoTpHrq ;
      private bool[] P00SE2_n1533ServicoFluxo_ServicoTpHrq ;
      private int[] P00SE2_A1522ServicoFluxo_ServicoCod ;
      private int[] P00SE2_A1528ServicoFluxo_Codigo ;
      private String[] P00SE3_A1527ServicoFluxo_SrvPosSigla ;
      private bool[] P00SE3_n1527ServicoFluxo_SrvPosSigla ;
      private int[] P00SE3_A1526ServicoFluxo_ServicoPos ;
      private bool[] P00SE3_n1526ServicoFluxo_ServicoPos ;
      private short[] P00SE3_A1532ServicoFluxo_Ordem ;
      private bool[] P00SE3_n1532ServicoFluxo_Ordem ;
      private short[] P00SE3_A1533ServicoFluxo_ServicoTpHrq ;
      private bool[] P00SE3_n1533ServicoFluxo_ServicoTpHrq ;
      private int[] P00SE3_A1522ServicoFluxo_ServicoCod ;
      private int[] P00SE3_A1528ServicoFluxo_Codigo ;
      private String[] P00SE3_A1523ServicoFluxo_ServicoSigla ;
      private bool[] P00SE3_n1523ServicoFluxo_ServicoSigla ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV17TFServicoFluxo_ServicoTpHrq_Sels ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV29Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV32OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV34OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV39GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV40GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV41GridStateDynamicFilter ;
   }

   public class getpromptservicofluxofilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00SE2( IGxContext context ,
                                             short A1533ServicoFluxo_ServicoTpHrq ,
                                             IGxCollection AV17TFServicoFluxo_ServicoTpHrq_Sels ,
                                             String AV42DynamicFiltersSelector1 ,
                                             short AV43DynamicFiltersOperator1 ,
                                             String AV44ServicoFluxo_ServicoSigla1 ,
                                             bool AV45DynamicFiltersEnabled2 ,
                                             String AV46DynamicFiltersSelector2 ,
                                             short AV47DynamicFiltersOperator2 ,
                                             String AV48ServicoFluxo_ServicoSigla2 ,
                                             bool AV49DynamicFiltersEnabled3 ,
                                             String AV50DynamicFiltersSelector3 ,
                                             short AV51DynamicFiltersOperator3 ,
                                             String AV52ServicoFluxo_ServicoSigla3 ,
                                             int AV10TFServicoFluxo_Codigo ,
                                             int AV11TFServicoFluxo_Codigo_To ,
                                             int AV12TFServicoFluxo_ServicoCod ,
                                             int AV13TFServicoFluxo_ServicoCod_To ,
                                             String AV15TFServicoFluxo_ServicoSigla_Sel ,
                                             String AV14TFServicoFluxo_ServicoSigla ,
                                             int AV17TFServicoFluxo_ServicoTpHrq_Sels_Count ,
                                             short AV18TFServicoFluxo_Ordem ,
                                             short AV19TFServicoFluxo_Ordem_To ,
                                             int AV20TFServicoFluxo_ServicoPos ,
                                             int AV21TFServicoFluxo_ServicoPos_To ,
                                             String AV23TFServicoFluxo_SrvPosSigla_Sel ,
                                             String AV22TFServicoFluxo_SrvPosSigla ,
                                             String A1523ServicoFluxo_ServicoSigla ,
                                             int A1528ServicoFluxo_Codigo ,
                                             int A1522ServicoFluxo_ServicoCod ,
                                             short A1532ServicoFluxo_Ordem ,
                                             int A1526ServicoFluxo_ServicoPos ,
                                             String A1527ServicoFluxo_SrvPosSigla )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [18] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT T3.[Servico_Sigla] AS ServicoFluxo_ServicoSigla, T2.[Servico_Sigla] AS ServicoFluxo_SrvPosSigla, T1.[ServicoFluxo_ServicoPos] AS ServicoFluxo_ServicoPos, T1.[ServicoFluxo_Ordem], T3.[Servico_TipoHierarquia] AS ServicoFluxo_ServicoTpHrq, T1.[ServicoFluxo_ServicoCod] AS ServicoFluxo_ServicoCod, T1.[ServicoFluxo_Codigo] FROM (([ServicoFluxo] T1 WITH (NOLOCK) LEFT JOIN [Servico] T2 WITH (NOLOCK) ON T2.[Servico_Codigo] = T1.[ServicoFluxo_ServicoPos]) INNER JOIN [Servico] T3 WITH (NOLOCK) ON T3.[Servico_Codigo] = T1.[ServicoFluxo_ServicoCod])";
         if ( ( StringUtil.StrCmp(AV42DynamicFiltersSelector1, "SERVICOFLUXO_SERVICOSIGLA") == 0 ) && ( AV43DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV44ServicoFluxo_ServicoSigla1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Servico_Sigla] like @lV44ServicoFluxo_ServicoSigla1)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Servico_Sigla] like @lV44ServicoFluxo_ServicoSigla1)";
            }
         }
         else
         {
            GXv_int1[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV42DynamicFiltersSelector1, "SERVICOFLUXO_SERVICOSIGLA") == 0 ) && ( AV43DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV44ServicoFluxo_ServicoSigla1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Servico_Sigla] like '%' + @lV44ServicoFluxo_ServicoSigla1)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Servico_Sigla] like '%' + @lV44ServicoFluxo_ServicoSigla1)";
            }
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( AV45DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV46DynamicFiltersSelector2, "SERVICOFLUXO_SERVICOSIGLA") == 0 ) && ( AV47DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV48ServicoFluxo_ServicoSigla2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Servico_Sigla] like @lV48ServicoFluxo_ServicoSigla2)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Servico_Sigla] like @lV48ServicoFluxo_ServicoSigla2)";
            }
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( AV45DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV46DynamicFiltersSelector2, "SERVICOFLUXO_SERVICOSIGLA") == 0 ) && ( AV47DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV48ServicoFluxo_ServicoSigla2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Servico_Sigla] like '%' + @lV48ServicoFluxo_ServicoSigla2)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Servico_Sigla] like '%' + @lV48ServicoFluxo_ServicoSigla2)";
            }
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( AV49DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV50DynamicFiltersSelector3, "SERVICOFLUXO_SERVICOSIGLA") == 0 ) && ( AV51DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV52ServicoFluxo_ServicoSigla3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Servico_Sigla] like @lV52ServicoFluxo_ServicoSigla3)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Servico_Sigla] like @lV52ServicoFluxo_ServicoSigla3)";
            }
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( AV49DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV50DynamicFiltersSelector3, "SERVICOFLUXO_SERVICOSIGLA") == 0 ) && ( AV51DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV52ServicoFluxo_ServicoSigla3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Servico_Sigla] like '%' + @lV52ServicoFluxo_ServicoSigla3)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Servico_Sigla] like '%' + @lV52ServicoFluxo_ServicoSigla3)";
            }
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( ! (0==AV10TFServicoFluxo_Codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ServicoFluxo_Codigo] >= @AV10TFServicoFluxo_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ServicoFluxo_Codigo] >= @AV10TFServicoFluxo_Codigo)";
            }
         }
         else
         {
            GXv_int1[6] = 1;
         }
         if ( ! (0==AV11TFServicoFluxo_Codigo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ServicoFluxo_Codigo] <= @AV11TFServicoFluxo_Codigo_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ServicoFluxo_Codigo] <= @AV11TFServicoFluxo_Codigo_To)";
            }
         }
         else
         {
            GXv_int1[7] = 1;
         }
         if ( ! (0==AV12TFServicoFluxo_ServicoCod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ServicoFluxo_ServicoCod] >= @AV12TFServicoFluxo_ServicoCod)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ServicoFluxo_ServicoCod] >= @AV12TFServicoFluxo_ServicoCod)";
            }
         }
         else
         {
            GXv_int1[8] = 1;
         }
         if ( ! (0==AV13TFServicoFluxo_ServicoCod_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ServicoFluxo_ServicoCod] <= @AV13TFServicoFluxo_ServicoCod_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ServicoFluxo_ServicoCod] <= @AV13TFServicoFluxo_ServicoCod_To)";
            }
         }
         else
         {
            GXv_int1[9] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV15TFServicoFluxo_ServicoSigla_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV14TFServicoFluxo_ServicoSigla)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Servico_Sigla] like @lV14TFServicoFluxo_ServicoSigla)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Servico_Sigla] like @lV14TFServicoFluxo_ServicoSigla)";
            }
         }
         else
         {
            GXv_int1[10] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV15TFServicoFluxo_ServicoSigla_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Servico_Sigla] = @AV15TFServicoFluxo_ServicoSigla_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Servico_Sigla] = @AV15TFServicoFluxo_ServicoSigla_Sel)";
            }
         }
         else
         {
            GXv_int1[11] = 1;
         }
         if ( AV17TFServicoFluxo_ServicoTpHrq_Sels_Count > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV17TFServicoFluxo_ServicoTpHrq_Sels, "T3.[Servico_TipoHierarquia] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV17TFServicoFluxo_ServicoTpHrq_Sels, "T3.[Servico_TipoHierarquia] IN (", ")") + ")";
            }
         }
         if ( ! (0==AV18TFServicoFluxo_Ordem) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ServicoFluxo_Ordem] >= @AV18TFServicoFluxo_Ordem)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ServicoFluxo_Ordem] >= @AV18TFServicoFluxo_Ordem)";
            }
         }
         else
         {
            GXv_int1[12] = 1;
         }
         if ( ! (0==AV19TFServicoFluxo_Ordem_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ServicoFluxo_Ordem] <= @AV19TFServicoFluxo_Ordem_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ServicoFluxo_Ordem] <= @AV19TFServicoFluxo_Ordem_To)";
            }
         }
         else
         {
            GXv_int1[13] = 1;
         }
         if ( ! (0==AV20TFServicoFluxo_ServicoPos) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ServicoFluxo_ServicoPos] >= @AV20TFServicoFluxo_ServicoPos)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ServicoFluxo_ServicoPos] >= @AV20TFServicoFluxo_ServicoPos)";
            }
         }
         else
         {
            GXv_int1[14] = 1;
         }
         if ( ! (0==AV21TFServicoFluxo_ServicoPos_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ServicoFluxo_ServicoPos] <= @AV21TFServicoFluxo_ServicoPos_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ServicoFluxo_ServicoPos] <= @AV21TFServicoFluxo_ServicoPos_To)";
            }
         }
         else
         {
            GXv_int1[15] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV23TFServicoFluxo_SrvPosSigla_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV22TFServicoFluxo_SrvPosSigla)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Sigla] like @lV22TFServicoFluxo_SrvPosSigla)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Sigla] like @lV22TFServicoFluxo_SrvPosSigla)";
            }
         }
         else
         {
            GXv_int1[16] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV23TFServicoFluxo_SrvPosSigla_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Sigla] = @AV23TFServicoFluxo_SrvPosSigla_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Sigla] = @AV23TFServicoFluxo_SrvPosSigla_Sel)";
            }
         }
         else
         {
            GXv_int1[17] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T3.[Servico_Sigla]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      protected Object[] conditional_P00SE3( IGxContext context ,
                                             short A1533ServicoFluxo_ServicoTpHrq ,
                                             IGxCollection AV17TFServicoFluxo_ServicoTpHrq_Sels ,
                                             String AV42DynamicFiltersSelector1 ,
                                             short AV43DynamicFiltersOperator1 ,
                                             String AV44ServicoFluxo_ServicoSigla1 ,
                                             bool AV45DynamicFiltersEnabled2 ,
                                             String AV46DynamicFiltersSelector2 ,
                                             short AV47DynamicFiltersOperator2 ,
                                             String AV48ServicoFluxo_ServicoSigla2 ,
                                             bool AV49DynamicFiltersEnabled3 ,
                                             String AV50DynamicFiltersSelector3 ,
                                             short AV51DynamicFiltersOperator3 ,
                                             String AV52ServicoFluxo_ServicoSigla3 ,
                                             int AV10TFServicoFluxo_Codigo ,
                                             int AV11TFServicoFluxo_Codigo_To ,
                                             int AV12TFServicoFluxo_ServicoCod ,
                                             int AV13TFServicoFluxo_ServicoCod_To ,
                                             String AV15TFServicoFluxo_ServicoSigla_Sel ,
                                             String AV14TFServicoFluxo_ServicoSigla ,
                                             int AV17TFServicoFluxo_ServicoTpHrq_Sels_Count ,
                                             short AV18TFServicoFluxo_Ordem ,
                                             short AV19TFServicoFluxo_Ordem_To ,
                                             int AV20TFServicoFluxo_ServicoPos ,
                                             int AV21TFServicoFluxo_ServicoPos_To ,
                                             String AV23TFServicoFluxo_SrvPosSigla_Sel ,
                                             String AV22TFServicoFluxo_SrvPosSigla ,
                                             String A1523ServicoFluxo_ServicoSigla ,
                                             int A1528ServicoFluxo_Codigo ,
                                             int A1522ServicoFluxo_ServicoCod ,
                                             short A1532ServicoFluxo_Ordem ,
                                             int A1526ServicoFluxo_ServicoPos ,
                                             String A1527ServicoFluxo_SrvPosSigla )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [18] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT T2.[Servico_Sigla] AS ServicoFluxo_SrvPosSigla, T1.[ServicoFluxo_ServicoPos] AS ServicoFluxo_ServicoPos, T1.[ServicoFluxo_Ordem], T3.[Servico_TipoHierarquia] AS ServicoFluxo_ServicoTpHrq, T1.[ServicoFluxo_ServicoCod] AS ServicoFluxo_ServicoCod, T1.[ServicoFluxo_Codigo], T3.[Servico_Sigla] AS ServicoFluxo_ServicoSigla FROM (([ServicoFluxo] T1 WITH (NOLOCK) LEFT JOIN [Servico] T2 WITH (NOLOCK) ON T2.[Servico_Codigo] = T1.[ServicoFluxo_ServicoPos]) INNER JOIN [Servico] T3 WITH (NOLOCK) ON T3.[Servico_Codigo] = T1.[ServicoFluxo_ServicoCod])";
         if ( ( StringUtil.StrCmp(AV42DynamicFiltersSelector1, "SERVICOFLUXO_SERVICOSIGLA") == 0 ) && ( AV43DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV44ServicoFluxo_ServicoSigla1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Servico_Sigla] like @lV44ServicoFluxo_ServicoSigla1)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Servico_Sigla] like @lV44ServicoFluxo_ServicoSigla1)";
            }
         }
         else
         {
            GXv_int3[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV42DynamicFiltersSelector1, "SERVICOFLUXO_SERVICOSIGLA") == 0 ) && ( AV43DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV44ServicoFluxo_ServicoSigla1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Servico_Sigla] like '%' + @lV44ServicoFluxo_ServicoSigla1)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Servico_Sigla] like '%' + @lV44ServicoFluxo_ServicoSigla1)";
            }
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( AV45DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV46DynamicFiltersSelector2, "SERVICOFLUXO_SERVICOSIGLA") == 0 ) && ( AV47DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV48ServicoFluxo_ServicoSigla2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Servico_Sigla] like @lV48ServicoFluxo_ServicoSigla2)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Servico_Sigla] like @lV48ServicoFluxo_ServicoSigla2)";
            }
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( AV45DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV46DynamicFiltersSelector2, "SERVICOFLUXO_SERVICOSIGLA") == 0 ) && ( AV47DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV48ServicoFluxo_ServicoSigla2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Servico_Sigla] like '%' + @lV48ServicoFluxo_ServicoSigla2)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Servico_Sigla] like '%' + @lV48ServicoFluxo_ServicoSigla2)";
            }
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( AV49DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV50DynamicFiltersSelector3, "SERVICOFLUXO_SERVICOSIGLA") == 0 ) && ( AV51DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV52ServicoFluxo_ServicoSigla3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Servico_Sigla] like @lV52ServicoFluxo_ServicoSigla3)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Servico_Sigla] like @lV52ServicoFluxo_ServicoSigla3)";
            }
         }
         else
         {
            GXv_int3[4] = 1;
         }
         if ( AV49DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV50DynamicFiltersSelector3, "SERVICOFLUXO_SERVICOSIGLA") == 0 ) && ( AV51DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV52ServicoFluxo_ServicoSigla3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Servico_Sigla] like '%' + @lV52ServicoFluxo_ServicoSigla3)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Servico_Sigla] like '%' + @lV52ServicoFluxo_ServicoSigla3)";
            }
         }
         else
         {
            GXv_int3[5] = 1;
         }
         if ( ! (0==AV10TFServicoFluxo_Codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ServicoFluxo_Codigo] >= @AV10TFServicoFluxo_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ServicoFluxo_Codigo] >= @AV10TFServicoFluxo_Codigo)";
            }
         }
         else
         {
            GXv_int3[6] = 1;
         }
         if ( ! (0==AV11TFServicoFluxo_Codigo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ServicoFluxo_Codigo] <= @AV11TFServicoFluxo_Codigo_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ServicoFluxo_Codigo] <= @AV11TFServicoFluxo_Codigo_To)";
            }
         }
         else
         {
            GXv_int3[7] = 1;
         }
         if ( ! (0==AV12TFServicoFluxo_ServicoCod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ServicoFluxo_ServicoCod] >= @AV12TFServicoFluxo_ServicoCod)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ServicoFluxo_ServicoCod] >= @AV12TFServicoFluxo_ServicoCod)";
            }
         }
         else
         {
            GXv_int3[8] = 1;
         }
         if ( ! (0==AV13TFServicoFluxo_ServicoCod_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ServicoFluxo_ServicoCod] <= @AV13TFServicoFluxo_ServicoCod_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ServicoFluxo_ServicoCod] <= @AV13TFServicoFluxo_ServicoCod_To)";
            }
         }
         else
         {
            GXv_int3[9] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV15TFServicoFluxo_ServicoSigla_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV14TFServicoFluxo_ServicoSigla)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Servico_Sigla] like @lV14TFServicoFluxo_ServicoSigla)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Servico_Sigla] like @lV14TFServicoFluxo_ServicoSigla)";
            }
         }
         else
         {
            GXv_int3[10] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV15TFServicoFluxo_ServicoSigla_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Servico_Sigla] = @AV15TFServicoFluxo_ServicoSigla_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Servico_Sigla] = @AV15TFServicoFluxo_ServicoSigla_Sel)";
            }
         }
         else
         {
            GXv_int3[11] = 1;
         }
         if ( AV17TFServicoFluxo_ServicoTpHrq_Sels_Count > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV17TFServicoFluxo_ServicoTpHrq_Sels, "T3.[Servico_TipoHierarquia] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV17TFServicoFluxo_ServicoTpHrq_Sels, "T3.[Servico_TipoHierarquia] IN (", ")") + ")";
            }
         }
         if ( ! (0==AV18TFServicoFluxo_Ordem) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ServicoFluxo_Ordem] >= @AV18TFServicoFluxo_Ordem)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ServicoFluxo_Ordem] >= @AV18TFServicoFluxo_Ordem)";
            }
         }
         else
         {
            GXv_int3[12] = 1;
         }
         if ( ! (0==AV19TFServicoFluxo_Ordem_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ServicoFluxo_Ordem] <= @AV19TFServicoFluxo_Ordem_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ServicoFluxo_Ordem] <= @AV19TFServicoFluxo_Ordem_To)";
            }
         }
         else
         {
            GXv_int3[13] = 1;
         }
         if ( ! (0==AV20TFServicoFluxo_ServicoPos) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ServicoFluxo_ServicoPos] >= @AV20TFServicoFluxo_ServicoPos)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ServicoFluxo_ServicoPos] >= @AV20TFServicoFluxo_ServicoPos)";
            }
         }
         else
         {
            GXv_int3[14] = 1;
         }
         if ( ! (0==AV21TFServicoFluxo_ServicoPos_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ServicoFluxo_ServicoPos] <= @AV21TFServicoFluxo_ServicoPos_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ServicoFluxo_ServicoPos] <= @AV21TFServicoFluxo_ServicoPos_To)";
            }
         }
         else
         {
            GXv_int3[15] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV23TFServicoFluxo_SrvPosSigla_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV22TFServicoFluxo_SrvPosSigla)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Sigla] like @lV22TFServicoFluxo_SrvPosSigla)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Sigla] like @lV22TFServicoFluxo_SrvPosSigla)";
            }
         }
         else
         {
            GXv_int3[16] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV23TFServicoFluxo_SrvPosSigla_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Servico_Sigla] = @AV23TFServicoFluxo_SrvPosSigla_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Servico_Sigla] = @AV23TFServicoFluxo_SrvPosSigla_Sel)";
            }
         }
         else
         {
            GXv_int3[17] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T2.[Servico_Sigla]";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00SE2(context, (short)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (short)dynConstraints[3] , (String)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (short)dynConstraints[7] , (String)dynConstraints[8] , (bool)dynConstraints[9] , (String)dynConstraints[10] , (short)dynConstraints[11] , (String)dynConstraints[12] , (int)dynConstraints[13] , (int)dynConstraints[14] , (int)dynConstraints[15] , (int)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (int)dynConstraints[19] , (short)dynConstraints[20] , (short)dynConstraints[21] , (int)dynConstraints[22] , (int)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] , (String)dynConstraints[26] , (int)dynConstraints[27] , (int)dynConstraints[28] , (short)dynConstraints[29] , (int)dynConstraints[30] , (String)dynConstraints[31] );
               case 1 :
                     return conditional_P00SE3(context, (short)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (short)dynConstraints[3] , (String)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (short)dynConstraints[7] , (String)dynConstraints[8] , (bool)dynConstraints[9] , (String)dynConstraints[10] , (short)dynConstraints[11] , (String)dynConstraints[12] , (int)dynConstraints[13] , (int)dynConstraints[14] , (int)dynConstraints[15] , (int)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (int)dynConstraints[19] , (short)dynConstraints[20] , (short)dynConstraints[21] , (int)dynConstraints[22] , (int)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] , (String)dynConstraints[26] , (int)dynConstraints[27] , (int)dynConstraints[28] , (short)dynConstraints[29] , (int)dynConstraints[30] , (String)dynConstraints[31] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00SE2 ;
          prmP00SE2 = new Object[] {
          new Object[] {"@lV44ServicoFluxo_ServicoSigla1",SqlDbType.Char,15,0} ,
          new Object[] {"@lV44ServicoFluxo_ServicoSigla1",SqlDbType.Char,15,0} ,
          new Object[] {"@lV48ServicoFluxo_ServicoSigla2",SqlDbType.Char,15,0} ,
          new Object[] {"@lV48ServicoFluxo_ServicoSigla2",SqlDbType.Char,15,0} ,
          new Object[] {"@lV52ServicoFluxo_ServicoSigla3",SqlDbType.Char,15,0} ,
          new Object[] {"@lV52ServicoFluxo_ServicoSigla3",SqlDbType.Char,15,0} ,
          new Object[] {"@AV10TFServicoFluxo_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV11TFServicoFluxo_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV12TFServicoFluxo_ServicoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV13TFServicoFluxo_ServicoCod_To",SqlDbType.Int,6,0} ,
          new Object[] {"@lV14TFServicoFluxo_ServicoSigla",SqlDbType.Char,15,0} ,
          new Object[] {"@AV15TFServicoFluxo_ServicoSigla_Sel",SqlDbType.Char,15,0} ,
          new Object[] {"@AV18TFServicoFluxo_Ordem",SqlDbType.SmallInt,3,0} ,
          new Object[] {"@AV19TFServicoFluxo_Ordem_To",SqlDbType.SmallInt,3,0} ,
          new Object[] {"@AV20TFServicoFluxo_ServicoPos",SqlDbType.Int,6,0} ,
          new Object[] {"@AV21TFServicoFluxo_ServicoPos_To",SqlDbType.Int,6,0} ,
          new Object[] {"@lV22TFServicoFluxo_SrvPosSigla",SqlDbType.Char,15,0} ,
          new Object[] {"@AV23TFServicoFluxo_SrvPosSigla_Sel",SqlDbType.Char,15,0}
          } ;
          Object[] prmP00SE3 ;
          prmP00SE3 = new Object[] {
          new Object[] {"@lV44ServicoFluxo_ServicoSigla1",SqlDbType.Char,15,0} ,
          new Object[] {"@lV44ServicoFluxo_ServicoSigla1",SqlDbType.Char,15,0} ,
          new Object[] {"@lV48ServicoFluxo_ServicoSigla2",SqlDbType.Char,15,0} ,
          new Object[] {"@lV48ServicoFluxo_ServicoSigla2",SqlDbType.Char,15,0} ,
          new Object[] {"@lV52ServicoFluxo_ServicoSigla3",SqlDbType.Char,15,0} ,
          new Object[] {"@lV52ServicoFluxo_ServicoSigla3",SqlDbType.Char,15,0} ,
          new Object[] {"@AV10TFServicoFluxo_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV11TFServicoFluxo_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV12TFServicoFluxo_ServicoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV13TFServicoFluxo_ServicoCod_To",SqlDbType.Int,6,0} ,
          new Object[] {"@lV14TFServicoFluxo_ServicoSigla",SqlDbType.Char,15,0} ,
          new Object[] {"@AV15TFServicoFluxo_ServicoSigla_Sel",SqlDbType.Char,15,0} ,
          new Object[] {"@AV18TFServicoFluxo_Ordem",SqlDbType.SmallInt,3,0} ,
          new Object[] {"@AV19TFServicoFluxo_Ordem_To",SqlDbType.SmallInt,3,0} ,
          new Object[] {"@AV20TFServicoFluxo_ServicoPos",SqlDbType.Int,6,0} ,
          new Object[] {"@AV21TFServicoFluxo_ServicoPos_To",SqlDbType.Int,6,0} ,
          new Object[] {"@lV22TFServicoFluxo_SrvPosSigla",SqlDbType.Char,15,0} ,
          new Object[] {"@AV23TFServicoFluxo_SrvPosSigla_Sel",SqlDbType.Char,15,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00SE2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00SE2,100,0,true,false )
             ,new CursorDef("P00SE3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00SE3,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getString(1, 15) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 15) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((short[]) buf[6])[0] = rslt.getShort(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((short[]) buf[8])[0] = rslt.getShort(5) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                ((int[]) buf[10])[0] = rslt.getInt(6) ;
                ((int[]) buf[11])[0] = rslt.getInt(7) ;
                return;
             case 1 :
                ((String[]) buf[0])[0] = rslt.getString(1, 15) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((short[]) buf[4])[0] = rslt.getShort(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((short[]) buf[6])[0] = rslt.getShort(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((int[]) buf[8])[0] = rslt.getInt(5) ;
                ((int[]) buf[9])[0] = rslt.getInt(6) ;
                ((String[]) buf[10])[0] = rslt.getString(7, 15) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[24]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[25]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[26]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[27]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[30]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[31]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[32]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[33]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[24]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[25]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[26]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[27]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[30]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[31]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[32]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[33]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getpromptservicofluxofilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getpromptservicofluxofilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getpromptservicofluxofilterdata") )
          {
             return  ;
          }
          getpromptservicofluxofilterdata worker = new getpromptservicofluxofilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
