/*
               File: PRC_FDPF
        Description: Fun�o de dados Pontos de Fun��o
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:50:49.73
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_fdpf : GXProcedure
   {
      public prc_fdpf( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_fdpf( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_funcaoDados_Codigo ,
                           ref short aP1_FuncaoDados_PF )
      {
         this.AV8funcaoDados_Codigo = aP0_funcaoDados_Codigo;
         this.AV9FuncaoDados_PF = aP1_FuncaoDados_PF;
         initialize();
         executePrivate();
         aP0_funcaoDados_Codigo=this.AV8funcaoDados_Codigo;
         aP1_FuncaoDados_PF=this.AV9FuncaoDados_PF;
      }

      public short executeUdp( ref int aP0_funcaoDados_Codigo )
      {
         this.AV8funcaoDados_Codigo = aP0_funcaoDados_Codigo;
         this.AV9FuncaoDados_PF = aP1_FuncaoDados_PF;
         initialize();
         executePrivate();
         aP0_funcaoDados_Codigo=this.AV8funcaoDados_Codigo;
         aP1_FuncaoDados_PF=this.AV9FuncaoDados_PF;
         return AV9FuncaoDados_PF ;
      }

      public void executeSubmit( ref int aP0_funcaoDados_Codigo ,
                                 ref short aP1_FuncaoDados_PF )
      {
         prc_fdpf objprc_fdpf;
         objprc_fdpf = new prc_fdpf();
         objprc_fdpf.AV8funcaoDados_Codigo = aP0_funcaoDados_Codigo;
         objprc_fdpf.AV9FuncaoDados_PF = aP1_FuncaoDados_PF;
         objprc_fdpf.context.SetSubmitInitialConfig(context);
         objprc_fdpf.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_fdpf);
         aP0_funcaoDados_Codigo=this.AV8funcaoDados_Codigo;
         aP1_FuncaoDados_PF=this.AV9FuncaoDados_PF;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_fdpf)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV9FuncaoDados_PF = 0;
         /* Using cursor P001W2 */
         pr_default.execute(0, new Object[] {AV8funcaoDados_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A1147FuncaoDados_Tecnica = P001W2_A1147FuncaoDados_Tecnica[0];
            n1147FuncaoDados_Tecnica = P001W2_n1147FuncaoDados_Tecnica[0];
            A373FuncaoDados_Tipo = P001W2_A373FuncaoDados_Tipo[0];
            A391FuncaoDados_FuncaoDadosCod = P001W2_A391FuncaoDados_FuncaoDadosCod[0];
            n391FuncaoDados_FuncaoDadosCod = P001W2_n391FuncaoDados_FuncaoDadosCod[0];
            A368FuncaoDados_Codigo = P001W2_A368FuncaoDados_Codigo[0];
            GXt_char1 = A376FuncaoDados_Complexidade;
            new prc_fdcomplexidade(context ).execute( ref  A368FuncaoDados_Codigo, ref  GXt_char1) ;
            A376FuncaoDados_Complexidade = GXt_char1;
            if ( A1147FuncaoDados_Tecnica == 1 )
            {
               if ( StringUtil.StrCmp(A373FuncaoDados_Tipo, "ALI") == 0 )
               {
                  AV9FuncaoDados_PF = 35;
               }
               else if ( StringUtil.StrCmp(A373FuncaoDados_Tipo, "AIE") == 0 )
               {
                  AV9FuncaoDados_PF = 15;
               }
            }
            else if ( A1147FuncaoDados_Tecnica == 2 )
            {
               if ( StringUtil.StrCmp(A373FuncaoDados_Tipo, "ALI") == 0 )
               {
                  AV9FuncaoDados_PF = 7;
               }
               else if ( StringUtil.StrCmp(A373FuncaoDados_Tipo, "AIE") == 0 )
               {
                  AV9FuncaoDados_PF = 5;
               }
            }
            else
            {
               if ( (0==A391FuncaoDados_FuncaoDadosCod) )
               {
                  if ( StringUtil.StrCmp(A376FuncaoDados_Complexidade, "E") == 0 )
                  {
                  }
                  else if ( StringUtil.StrCmp(A373FuncaoDados_Tipo, "DC") == 0 )
                  {
                     AV9FuncaoDados_PF = 0;
                  }
                  else if ( StringUtil.StrCmp(A373FuncaoDados_Tipo, "ALI") == 0 )
                  {
                     if ( StringUtil.StrCmp(A376FuncaoDados_Complexidade, "B") == 0 )
                     {
                        AV9FuncaoDados_PF = 7;
                     }
                     else if ( StringUtil.StrCmp(A376FuncaoDados_Complexidade, "M") == 0 )
                     {
                        AV9FuncaoDados_PF = 10;
                     }
                     else if ( StringUtil.StrCmp(A376FuncaoDados_Complexidade, "A") == 0 )
                     {
                        AV9FuncaoDados_PF = 15;
                     }
                  }
               }
               else
               {
                  AV10FuncaoDados_FuncaoDadosCod = A391FuncaoDados_FuncaoDadosCod;
                  /* Execute user subroutine: 'FUNCAOEXTERNA' */
                  S111 ();
                  if ( returnInSub )
                  {
                     pr_default.close(0);
                     this.cleanup();
                     if (true) return;
                  }
               }
            }
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(0);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'FUNCAOEXTERNA' Routine */
         /* Using cursor P001W3 */
         pr_default.execute(1, new Object[] {AV10FuncaoDados_FuncaoDadosCod});
         while ( (pr_default.getStatus(1) != 101) )
         {
            A373FuncaoDados_Tipo = P001W3_A373FuncaoDados_Tipo[0];
            A368FuncaoDados_Codigo = P001W3_A368FuncaoDados_Codigo[0];
            GXt_char1 = A376FuncaoDados_Complexidade;
            new prc_fdcomplexidade(context ).execute( ref  A368FuncaoDados_Codigo, ref  GXt_char1) ;
            A376FuncaoDados_Complexidade = GXt_char1;
            if ( StringUtil.StrCmp(A373FuncaoDados_Tipo, "DC") == 0 )
            {
               AV9FuncaoDados_PF = 0;
            }
            else
            {
               if ( StringUtil.StrCmp(A376FuncaoDados_Complexidade, "B") == 0 )
               {
                  AV9FuncaoDados_PF = 5;
               }
               else if ( StringUtil.StrCmp(A376FuncaoDados_Complexidade, "M") == 0 )
               {
                  AV9FuncaoDados_PF = 7;
               }
               else if ( StringUtil.StrCmp(A376FuncaoDados_Complexidade, "A") == 0 )
               {
                  AV9FuncaoDados_PF = 10;
               }
            }
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(1);
         returnInSub = true;
         if (true) return;
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P001W2_A1147FuncaoDados_Tecnica = new short[1] ;
         P001W2_n1147FuncaoDados_Tecnica = new bool[] {false} ;
         P001W2_A373FuncaoDados_Tipo = new String[] {""} ;
         P001W2_A391FuncaoDados_FuncaoDadosCod = new int[1] ;
         P001W2_n391FuncaoDados_FuncaoDadosCod = new bool[] {false} ;
         P001W2_A368FuncaoDados_Codigo = new int[1] ;
         A373FuncaoDados_Tipo = "";
         A376FuncaoDados_Complexidade = "";
         P001W3_A373FuncaoDados_Tipo = new String[] {""} ;
         P001W3_A368FuncaoDados_Codigo = new int[1] ;
         GXt_char1 = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_fdpf__default(),
            new Object[][] {
                new Object[] {
               P001W2_A1147FuncaoDados_Tecnica, P001W2_n1147FuncaoDados_Tecnica, P001W2_A373FuncaoDados_Tipo, P001W2_A391FuncaoDados_FuncaoDadosCod, P001W2_n391FuncaoDados_FuncaoDadosCod, P001W2_A368FuncaoDados_Codigo
               }
               , new Object[] {
               P001W3_A373FuncaoDados_Tipo, P001W3_A368FuncaoDados_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV9FuncaoDados_PF ;
      private short A1147FuncaoDados_Tecnica ;
      private int AV8funcaoDados_Codigo ;
      private int A391FuncaoDados_FuncaoDadosCod ;
      private int A368FuncaoDados_Codigo ;
      private int AV10FuncaoDados_FuncaoDadosCod ;
      private String scmdbuf ;
      private String A373FuncaoDados_Tipo ;
      private String A376FuncaoDados_Complexidade ;
      private String GXt_char1 ;
      private bool n1147FuncaoDados_Tecnica ;
      private bool n391FuncaoDados_FuncaoDadosCod ;
      private bool returnInSub ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_funcaoDados_Codigo ;
      private short aP1_FuncaoDados_PF ;
      private IDataStoreProvider pr_default ;
      private short[] P001W2_A1147FuncaoDados_Tecnica ;
      private bool[] P001W2_n1147FuncaoDados_Tecnica ;
      private String[] P001W2_A373FuncaoDados_Tipo ;
      private int[] P001W2_A391FuncaoDados_FuncaoDadosCod ;
      private bool[] P001W2_n391FuncaoDados_FuncaoDadosCod ;
      private int[] P001W2_A368FuncaoDados_Codigo ;
      private String[] P001W3_A373FuncaoDados_Tipo ;
      private int[] P001W3_A368FuncaoDados_Codigo ;
   }

   public class prc_fdpf__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP001W2 ;
          prmP001W2 = new Object[] {
          new Object[] {"@AV8funcaoDados_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP001W3 ;
          prmP001W3 = new Object[] {
          new Object[] {"@AV10FuncaoDados_FuncaoDadosCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P001W2", "SELECT [FuncaoDados_Tecnica], [FuncaoDados_Tipo], [FuncaoDados_FuncaoDadosCod], [FuncaoDados_Codigo] FROM [FuncaoDados] WITH (NOLOCK) WHERE [FuncaoDados_Codigo] = @AV8funcaoDados_Codigo ORDER BY [FuncaoDados_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP001W2,1,0,true,true )
             ,new CursorDef("P001W3", "SELECT [FuncaoDados_Tipo], [FuncaoDados_Codigo] FROM [FuncaoDados] WITH (NOLOCK) WHERE [FuncaoDados_Codigo] = @AV10FuncaoDados_FuncaoDadosCod ORDER BY [FuncaoDados_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP001W3,1,0,true,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 3) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                return;
             case 1 :
                ((String[]) buf[0])[0] = rslt.getString(1, 3) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
