/*
               File: PessoaGeneral
        Description: Pessoa General
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:57:23.87
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class pessoageneral : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public pessoageneral( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public pessoageneral( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Pessoa_Codigo )
      {
         this.A34Pessoa_Codigo = aP0_Pessoa_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
         cmbPessoa_TipoPessoa = new GXCombobox();
         chkPessoa_Ativo = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  A34Pessoa_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A34Pessoa_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A34Pessoa_Codigo), 6, 0)));
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(int)A34Pessoa_Codigo});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PA322( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV14Pgmname = "PessoaGeneral";
               context.Gx_err = 0;
               WS322( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "Pessoa General") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?202042822572393");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
            context.WriteHtmlText( "<body") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("pessoageneral.aspx") + "?" + UrlEncode("" +A34Pessoa_Codigo)+"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOA34Pessoa_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOA34Pessoa_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_PESSOA_NOME", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A35Pessoa_Nome, "@!"))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_PESSOA_TIPOPESSOA", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A36Pessoa_TipoPessoa, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_PESSOA_DOCTO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A37Pessoa_Docto, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_PESSOA_IE", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A518Pessoa_IE, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_PESSOA_ENDERECO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A519Pessoa_Endereco, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_PESSOA_MUNICIPIOCOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A503Pessoa_MunicipioCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_PESSOA_CEP", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A521Pessoa_CEP, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_PESSOA_TELEFONE", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A522Pessoa_Telefone, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_PESSOA_FAX", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A523Pessoa_Fax, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_PESSOA_ATIVO", GetSecureSignedToken( sPrefix, A38Pessoa_Ativo));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseForm322( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("pessoageneral.js", "?202042822572396");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            context.WriteHtmlTextNl( "</form>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "PessoaGeneral" ;
      }

      public override String GetPgmdesc( )
      {
         return "Pessoa General" ;
      }

      protected void WB320( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "pessoageneral.aspx");
            }
            wb_table1_2_322( true) ;
         }
         else
         {
            wb_table1_2_322( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_322e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtPessoa_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A34Pessoa_Codigo), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A34Pessoa_Codigo), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtPessoa_Codigo_Jsonclick, 0, "Attribute", "", "", "", edtPessoa_Codigo_Visible, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_PessoaGeneral.htm");
         }
         wbLoad = true;
      }

      protected void START322( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "Pessoa General", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUP320( ) ;
            }
         }
      }

      protected void WS322( )
      {
         START322( ) ;
         EVT322( ) ;
      }

      protected void EVT322( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP320( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP320( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E11322 */
                                    E11322 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP320( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E12322 */
                                    E12322 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOUPDATE'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP320( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E13322 */
                                    E13322 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DODELETE'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP320( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E14322 */
                                    E14322 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP320( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                              }
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP320( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WE322( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseForm322( ) ;
            }
         }
      }

      protected void PA322( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            cmbPessoa_TipoPessoa.Name = "PESSOA_TIPOPESSOA";
            cmbPessoa_TipoPessoa.WebTags = "";
            cmbPessoa_TipoPessoa.addItem("", "(Nenhum)", 0);
            cmbPessoa_TipoPessoa.addItem("F", "F�sica", 0);
            cmbPessoa_TipoPessoa.addItem("J", "Jur�dica", 0);
            if ( cmbPessoa_TipoPessoa.ItemCount > 0 )
            {
               A36Pessoa_TipoPessoa = cmbPessoa_TipoPessoa.getValidValue(A36Pessoa_TipoPessoa);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A36Pessoa_TipoPessoa", A36Pessoa_TipoPessoa);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_PESSOA_TIPOPESSOA", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A36Pessoa_TipoPessoa, ""))));
            }
            chkPessoa_Ativo.Name = "PESSOA_ATIVO";
            chkPessoa_Ativo.WebTags = "";
            chkPessoa_Ativo.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkPessoa_Ativo_Internalname, "TitleCaption", chkPessoa_Ativo.Caption);
            chkPessoa_Ativo.CheckedValue = "false";
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbPessoa_TipoPessoa.ItemCount > 0 )
         {
            A36Pessoa_TipoPessoa = cmbPessoa_TipoPessoa.getValidValue(A36Pessoa_TipoPessoa);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A36Pessoa_TipoPessoa", A36Pessoa_TipoPessoa);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_PESSOA_TIPOPESSOA", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A36Pessoa_TipoPessoa, ""))));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RF322( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV14Pgmname = "PessoaGeneral";
         context.Gx_err = 0;
      }

      protected void RF322( )
      {
         initialize_formulas( ) ;
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Using cursor H00322 */
            pr_default.execute(0, new Object[] {A34Pessoa_Codigo});
            while ( (pr_default.getStatus(0) != 101) )
            {
               A38Pessoa_Ativo = H00322_A38Pessoa_Ativo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A38Pessoa_Ativo", A38Pessoa_Ativo);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_PESSOA_ATIVO", GetSecureSignedToken( sPrefix, A38Pessoa_Ativo));
               A523Pessoa_Fax = H00322_A523Pessoa_Fax[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A523Pessoa_Fax", A523Pessoa_Fax);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_PESSOA_FAX", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A523Pessoa_Fax, ""))));
               n523Pessoa_Fax = H00322_n523Pessoa_Fax[0];
               A522Pessoa_Telefone = H00322_A522Pessoa_Telefone[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A522Pessoa_Telefone", A522Pessoa_Telefone);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_PESSOA_TELEFONE", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A522Pessoa_Telefone, ""))));
               n522Pessoa_Telefone = H00322_n522Pessoa_Telefone[0];
               A521Pessoa_CEP = H00322_A521Pessoa_CEP[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A521Pessoa_CEP", A521Pessoa_CEP);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_PESSOA_CEP", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A521Pessoa_CEP, ""))));
               n521Pessoa_CEP = H00322_n521Pessoa_CEP[0];
               A520Pessoa_UF = H00322_A520Pessoa_UF[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A520Pessoa_UF", A520Pessoa_UF);
               n520Pessoa_UF = H00322_n520Pessoa_UF[0];
               A503Pessoa_MunicipioCod = H00322_A503Pessoa_MunicipioCod[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A503Pessoa_MunicipioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A503Pessoa_MunicipioCod), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_PESSOA_MUNICIPIOCOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A503Pessoa_MunicipioCod), "ZZZZZ9")));
               n503Pessoa_MunicipioCod = H00322_n503Pessoa_MunicipioCod[0];
               A519Pessoa_Endereco = H00322_A519Pessoa_Endereco[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A519Pessoa_Endereco", A519Pessoa_Endereco);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_PESSOA_ENDERECO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A519Pessoa_Endereco, ""))));
               n519Pessoa_Endereco = H00322_n519Pessoa_Endereco[0];
               A518Pessoa_IE = H00322_A518Pessoa_IE[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A518Pessoa_IE", A518Pessoa_IE);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_PESSOA_IE", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A518Pessoa_IE, ""))));
               n518Pessoa_IE = H00322_n518Pessoa_IE[0];
               A37Pessoa_Docto = H00322_A37Pessoa_Docto[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A37Pessoa_Docto", A37Pessoa_Docto);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_PESSOA_DOCTO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A37Pessoa_Docto, ""))));
               A36Pessoa_TipoPessoa = H00322_A36Pessoa_TipoPessoa[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A36Pessoa_TipoPessoa", A36Pessoa_TipoPessoa);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_PESSOA_TIPOPESSOA", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A36Pessoa_TipoPessoa, ""))));
               A35Pessoa_Nome = H00322_A35Pessoa_Nome[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A35Pessoa_Nome", A35Pessoa_Nome);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_PESSOA_NOME", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A35Pessoa_Nome, "@!"))));
               A520Pessoa_UF = H00322_A520Pessoa_UF[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A520Pessoa_UF", A520Pessoa_UF);
               n520Pessoa_UF = H00322_n520Pessoa_UF[0];
               /* Execute user event: E12322 */
               E12322 ();
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(0);
            WB320( ) ;
         }
      }

      protected void STRUP320( )
      {
         /* Before Start, stand alone formulas. */
         AV14Pgmname = "PessoaGeneral";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11322 */
         E11322 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            A35Pessoa_Nome = StringUtil.Upper( cgiGet( edtPessoa_Nome_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A35Pessoa_Nome", A35Pessoa_Nome);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_PESSOA_NOME", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A35Pessoa_Nome, "@!"))));
            cmbPessoa_TipoPessoa.CurrentValue = cgiGet( cmbPessoa_TipoPessoa_Internalname);
            A36Pessoa_TipoPessoa = cgiGet( cmbPessoa_TipoPessoa_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A36Pessoa_TipoPessoa", A36Pessoa_TipoPessoa);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_PESSOA_TIPOPESSOA", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A36Pessoa_TipoPessoa, ""))));
            A37Pessoa_Docto = cgiGet( edtPessoa_Docto_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A37Pessoa_Docto", A37Pessoa_Docto);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_PESSOA_DOCTO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A37Pessoa_Docto, ""))));
            A518Pessoa_IE = cgiGet( edtPessoa_IE_Internalname);
            n518Pessoa_IE = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A518Pessoa_IE", A518Pessoa_IE);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_PESSOA_IE", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A518Pessoa_IE, ""))));
            A519Pessoa_Endereco = cgiGet( edtPessoa_Endereco_Internalname);
            n519Pessoa_Endereco = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A519Pessoa_Endereco", A519Pessoa_Endereco);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_PESSOA_ENDERECO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A519Pessoa_Endereco, ""))));
            A503Pessoa_MunicipioCod = (int)(context.localUtil.CToN( cgiGet( edtPessoa_MunicipioCod_Internalname), ",", "."));
            n503Pessoa_MunicipioCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A503Pessoa_MunicipioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A503Pessoa_MunicipioCod), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_PESSOA_MUNICIPIOCOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A503Pessoa_MunicipioCod), "ZZZZZ9")));
            A520Pessoa_UF = StringUtil.Upper( cgiGet( edtPessoa_UF_Internalname));
            n520Pessoa_UF = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A520Pessoa_UF", A520Pessoa_UF);
            A521Pessoa_CEP = cgiGet( edtPessoa_CEP_Internalname);
            n521Pessoa_CEP = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A521Pessoa_CEP", A521Pessoa_CEP);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_PESSOA_CEP", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A521Pessoa_CEP, ""))));
            A522Pessoa_Telefone = cgiGet( edtPessoa_Telefone_Internalname);
            n522Pessoa_Telefone = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A522Pessoa_Telefone", A522Pessoa_Telefone);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_PESSOA_TELEFONE", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A522Pessoa_Telefone, ""))));
            A523Pessoa_Fax = cgiGet( edtPessoa_Fax_Internalname);
            n523Pessoa_Fax = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A523Pessoa_Fax", A523Pessoa_Fax);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_PESSOA_FAX", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A523Pessoa_Fax, ""))));
            A38Pessoa_Ativo = StringUtil.StrToBool( cgiGet( chkPessoa_Ativo_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A38Pessoa_Ativo", A38Pessoa_Ativo);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_PESSOA_ATIVO", GetSecureSignedToken( sPrefix, A38Pessoa_Ativo));
            /* Read saved values. */
            wcpOA34Pessoa_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA34Pessoa_Codigo"), ",", "."));
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E11322 */
         E11322 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E11322( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void nextLoad( )
      {
      }

      protected void E12322( )
      {
         /* Load Routine */
         edtPessoa_Codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtPessoa_Codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtPessoa_Codigo_Visible), 5, 0)));
         if ( ! ( ( AV6WWPContext.gxTpr_Update ) ) )
         {
            bttBtnupdate_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, bttBtnupdate_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnupdate_Enabled), 5, 0)));
         }
         if ( ! ( ( AV6WWPContext.gxTpr_Delete ) ) )
         {
            bttBtndelete_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, bttBtndelete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtndelete_Enabled), 5, 0)));
         }
      }

      protected void E13322( )
      {
         /* 'DoUpdate' Routine */
         context.wjLoc = formatLink("pessoa.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A34Pessoa_Codigo);
         context.wjLocDisableFrm = 1;
      }

      protected void E14322( )
      {
         /* 'DoDelete' Routine */
         context.wjLoc = formatLink("pessoa.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A34Pessoa_Codigo);
         context.wjLocDisableFrm = 1;
      }

      protected void S112( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8TrnContext.gxTpr_Callerobject = AV14Pgmname;
         AV8TrnContext.gxTpr_Callerondelete = false;
         AV8TrnContext.gxTpr_Callerurl = AV11HTTPRequest.ScriptName+"?"+AV11HTTPRequest.QueryString;
         AV8TrnContext.gxTpr_Transactionname = "Pessoa";
         AV9TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV9TrnContextAtt.gxTpr_Attributename = "Pessoa_Codigo";
         AV9TrnContextAtt.gxTpr_Attributevalue = StringUtil.Str( (decimal)(AV7Pessoa_Codigo), 6, 0);
         AV8TrnContext.gxTpr_Attributes.Add(AV9TrnContextAtt, 0);
         AV10Session.Set("TrnContext", AV8TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_MeetrikaVs3"));
      }

      protected void wb_table1_2_322( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, sPrefix, "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_322( true) ;
         }
         else
         {
            wb_table2_8_322( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_322e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_66_322( true) ;
         }
         else
         {
            wb_table3_66_322( false) ;
         }
         return  ;
      }

      protected void wb_table3_66_322e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_322e( true) ;
         }
         else
         {
            wb_table1_2_322e( false) ;
         }
      }

      protected void wb_table3_66_322( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 69,'" + sPrefix + "',false,'',0)\"";
            ClassString = "btn btn-success";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnupdate_Internalname, "", "Modifica", bttBtnupdate_Jsonclick, 5, "Modifica", "", StyleString, ClassString, 1, bttBtnupdate_Enabled, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOUPDATE\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_PessoaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 71,'" + sPrefix + "',false,'',0)\"";
            ClassString = "btn btn-default";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtndelete_Internalname, "", "Eliminar", bttBtndelete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, 1, bttBtndelete_Enabled, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DODELETE\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_PessoaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_66_322e( true) ;
         }
         else
         {
            wb_table3_66_322e( false) ;
         }
      }

      protected void wb_table2_8_322( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "TableViewGeneralAtts", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockpessoa_nome_Internalname, "Nome", "", "", lblTextblockpessoa_nome_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_PessoaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtPessoa_Nome_Internalname, StringUtil.RTrim( A35Pessoa_Nome), StringUtil.RTrim( context.localUtil.Format( A35Pessoa_Nome, "@!")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtPessoa_Nome_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "Nome100", "left", true, "HLP_PessoaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockpessoa_tipopessoa_Internalname, "Tipo da Pessoa", "", "", lblTextblockpessoa_tipopessoa_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_PessoaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbPessoa_TipoPessoa, cmbPessoa_TipoPessoa_Internalname, StringUtil.RTrim( A36Pessoa_TipoPessoa), 1, cmbPessoa_TipoPessoa_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "char", "", 1, 0, 0, 0, 0, "em", 0, "", "", "BootstrapAttributeGray", "", "", "", true, "HLP_PessoaGeneral.htm");
            cmbPessoa_TipoPessoa.CurrentValue = StringUtil.RTrim( A36Pessoa_TipoPessoa);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbPessoa_TipoPessoa_Internalname, "Values", (String)(cmbPessoa_TipoPessoa.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockpessoa_docto_Internalname, "Documento", "", "", lblTextblockpessoa_docto_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_PessoaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtPessoa_Docto_Internalname, A37Pessoa_Docto, StringUtil.RTrim( context.localUtil.Format( A37Pessoa_Docto, "")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtPessoa_Docto_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "Docto", "left", true, "HLP_PessoaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockpessoa_ie_Internalname, "Insc. Estadual", "", "", lblTextblockpessoa_ie_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_PessoaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtPessoa_IE_Internalname, StringUtil.RTrim( A518Pessoa_IE), StringUtil.RTrim( context.localUtil.Format( A518Pessoa_IE, "")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtPessoa_IE_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "IE", "left", true, "HLP_PessoaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockpessoa_endereco_Internalname, "Endere�o", "", "", lblTextblockpessoa_endereco_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_PessoaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtPessoa_Endereco_Internalname, A519Pessoa_Endereco, StringUtil.RTrim( context.localUtil.Format( A519Pessoa_Endereco, "")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtPessoa_Endereco_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PessoaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockpessoa_municipiocod_Internalname, "Municipio", "", "", lblTextblockpessoa_municipiocod_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_PessoaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtPessoa_MunicipioCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A503Pessoa_MunicipioCod), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A503Pessoa_MunicipioCod), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtPessoa_MunicipioCod_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_PessoaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockpessoa_uf_Internalname, "UF", "", "", lblTextblockpessoa_uf_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_PessoaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtPessoa_UF_Internalname, StringUtil.RTrim( A520Pessoa_UF), StringUtil.RTrim( context.localUtil.Format( A520Pessoa_UF, "@!")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtPessoa_UF_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 2, "chr", 1, "row", 2, 0, 0, 0, 1, -1, -1, true, "UF", "left", true, "HLP_PessoaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockpessoa_cep_Internalname, "CEP", "", "", lblTextblockpessoa_cep_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_PessoaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtPessoa_CEP_Internalname, StringUtil.RTrim( A521Pessoa_CEP), StringUtil.RTrim( context.localUtil.Format( A521Pessoa_CEP, "")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtPessoa_CEP_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 10, "chr", 1, "row", 10, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PessoaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockpessoa_telefone_Internalname, "Telefone", "", "", lblTextblockpessoa_telefone_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_PessoaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtPessoa_Telefone_Internalname, StringUtil.RTrim( A522Pessoa_Telefone), StringUtil.RTrim( context.localUtil.Format( A522Pessoa_Telefone, "")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtPessoa_Telefone_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PessoaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockpessoa_fax_Internalname, "Fax", "", "", lblTextblockpessoa_fax_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_PessoaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtPessoa_Fax_Internalname, StringUtil.RTrim( A523Pessoa_Fax), StringUtil.RTrim( context.localUtil.Format( A523Pessoa_Fax, "")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtPessoa_Fax_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PessoaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockpessoa_ativo_Internalname, "Ativo?", "", "", lblTextblockpessoa_ativo_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_PessoaGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Check box */
            ClassString = "BootstrapAttributeGray";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkPessoa_Ativo_Internalname, StringUtil.BoolToStr( A38Pessoa_Ativo), "", "", 1, 0, "true", "", StyleString, ClassString, "", "");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_322e( true) ;
         }
         else
         {
            wb_table2_8_322e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         A34Pessoa_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A34Pessoa_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A34Pessoa_Codigo), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA322( ) ;
         WS322( ) ;
         WE322( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlA34Pessoa_Codigo = (String)((String)getParm(obj,0));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PA322( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "pessoageneral");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PA322( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            A34Pessoa_Codigo = Convert.ToInt32(getParm(obj,2));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A34Pessoa_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A34Pessoa_Codigo), 6, 0)));
         }
         wcpOA34Pessoa_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA34Pessoa_Codigo"), ",", "."));
         if ( ! GetJustCreated( ) && ( ( A34Pessoa_Codigo != wcpOA34Pessoa_Codigo ) ) )
         {
            setjustcreated();
         }
         wcpOA34Pessoa_Codigo = A34Pessoa_Codigo;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlA34Pessoa_Codigo = cgiGet( sPrefix+"A34Pessoa_Codigo_CTRL");
         if ( StringUtil.Len( sCtrlA34Pessoa_Codigo) > 0 )
         {
            A34Pessoa_Codigo = (int)(context.localUtil.CToN( cgiGet( sCtrlA34Pessoa_Codigo), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A34Pessoa_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A34Pessoa_Codigo), 6, 0)));
         }
         else
         {
            A34Pessoa_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"A34Pessoa_Codigo_PARM"), ",", "."));
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PA322( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WS322( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WS322( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"A34Pessoa_Codigo_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(A34Pessoa_Codigo), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlA34Pessoa_Codigo)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"A34Pessoa_Codigo_CTRL", StringUtil.RTrim( sCtrlA34Pessoa_Codigo));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WE322( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?2249787");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?202042822572447");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("pessoageneral.js", "?202042822572447");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblockpessoa_nome_Internalname = sPrefix+"TEXTBLOCKPESSOA_NOME";
         edtPessoa_Nome_Internalname = sPrefix+"PESSOA_NOME";
         lblTextblockpessoa_tipopessoa_Internalname = sPrefix+"TEXTBLOCKPESSOA_TIPOPESSOA";
         cmbPessoa_TipoPessoa_Internalname = sPrefix+"PESSOA_TIPOPESSOA";
         lblTextblockpessoa_docto_Internalname = sPrefix+"TEXTBLOCKPESSOA_DOCTO";
         edtPessoa_Docto_Internalname = sPrefix+"PESSOA_DOCTO";
         lblTextblockpessoa_ie_Internalname = sPrefix+"TEXTBLOCKPESSOA_IE";
         edtPessoa_IE_Internalname = sPrefix+"PESSOA_IE";
         lblTextblockpessoa_endereco_Internalname = sPrefix+"TEXTBLOCKPESSOA_ENDERECO";
         edtPessoa_Endereco_Internalname = sPrefix+"PESSOA_ENDERECO";
         lblTextblockpessoa_municipiocod_Internalname = sPrefix+"TEXTBLOCKPESSOA_MUNICIPIOCOD";
         edtPessoa_MunicipioCod_Internalname = sPrefix+"PESSOA_MUNICIPIOCOD";
         lblTextblockpessoa_uf_Internalname = sPrefix+"TEXTBLOCKPESSOA_UF";
         edtPessoa_UF_Internalname = sPrefix+"PESSOA_UF";
         lblTextblockpessoa_cep_Internalname = sPrefix+"TEXTBLOCKPESSOA_CEP";
         edtPessoa_CEP_Internalname = sPrefix+"PESSOA_CEP";
         lblTextblockpessoa_telefone_Internalname = sPrefix+"TEXTBLOCKPESSOA_TELEFONE";
         edtPessoa_Telefone_Internalname = sPrefix+"PESSOA_TELEFONE";
         lblTextblockpessoa_fax_Internalname = sPrefix+"TEXTBLOCKPESSOA_FAX";
         edtPessoa_Fax_Internalname = sPrefix+"PESSOA_FAX";
         lblTextblockpessoa_ativo_Internalname = sPrefix+"TEXTBLOCKPESSOA_ATIVO";
         chkPessoa_Ativo_Internalname = sPrefix+"PESSOA_ATIVO";
         tblTableattributes_Internalname = sPrefix+"TABLEATTRIBUTES";
         bttBtnupdate_Internalname = sPrefix+"BTNUPDATE";
         bttBtndelete_Internalname = sPrefix+"BTNDELETE";
         tblTableactions_Internalname = sPrefix+"TABLEACTIONS";
         tblTablecontent_Internalname = sPrefix+"TABLECONTENT";
         edtPessoa_Codigo_Internalname = sPrefix+"PESSOA_CODIGO";
         Form.Internalname = sPrefix+"FORM";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         edtPessoa_Fax_Jsonclick = "";
         edtPessoa_Telefone_Jsonclick = "";
         edtPessoa_CEP_Jsonclick = "";
         edtPessoa_UF_Jsonclick = "";
         edtPessoa_MunicipioCod_Jsonclick = "";
         edtPessoa_Endereco_Jsonclick = "";
         edtPessoa_IE_Jsonclick = "";
         edtPessoa_Docto_Jsonclick = "";
         cmbPessoa_TipoPessoa_Jsonclick = "";
         edtPessoa_Nome_Jsonclick = "";
         bttBtndelete_Enabled = 1;
         bttBtnupdate_Enabled = 1;
         chkPessoa_Ativo.Caption = "";
         edtPessoa_Codigo_Jsonclick = "";
         edtPessoa_Codigo_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("'DOUPDATE'","{handler:'E13322',iparms:[{av:'A34Pessoa_Codigo',fld:'PESSOA_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         setEventMetadata("'DODELETE'","{handler:'E14322',iparms:[{av:'A34Pessoa_Codigo',fld:'PESSOA_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         AV14Pgmname = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         A35Pessoa_Nome = "";
         A36Pessoa_TipoPessoa = "";
         A37Pessoa_Docto = "";
         A518Pessoa_IE = "";
         A519Pessoa_Endereco = "";
         A521Pessoa_CEP = "";
         A522Pessoa_Telefone = "";
         A523Pessoa_Fax = "";
         GXKey = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         scmdbuf = "";
         H00322_A34Pessoa_Codigo = new int[1] ;
         H00322_A38Pessoa_Ativo = new bool[] {false} ;
         H00322_A523Pessoa_Fax = new String[] {""} ;
         H00322_n523Pessoa_Fax = new bool[] {false} ;
         H00322_A522Pessoa_Telefone = new String[] {""} ;
         H00322_n522Pessoa_Telefone = new bool[] {false} ;
         H00322_A521Pessoa_CEP = new String[] {""} ;
         H00322_n521Pessoa_CEP = new bool[] {false} ;
         H00322_A520Pessoa_UF = new String[] {""} ;
         H00322_n520Pessoa_UF = new bool[] {false} ;
         H00322_A503Pessoa_MunicipioCod = new int[1] ;
         H00322_n503Pessoa_MunicipioCod = new bool[] {false} ;
         H00322_A519Pessoa_Endereco = new String[] {""} ;
         H00322_n519Pessoa_Endereco = new bool[] {false} ;
         H00322_A518Pessoa_IE = new String[] {""} ;
         H00322_n518Pessoa_IE = new bool[] {false} ;
         H00322_A37Pessoa_Docto = new String[] {""} ;
         H00322_A36Pessoa_TipoPessoa = new String[] {""} ;
         H00322_A35Pessoa_Nome = new String[] {""} ;
         A520Pessoa_UF = "";
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV11HTTPRequest = new GxHttpRequest( context);
         AV9TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV10Session = context.GetSession();
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         bttBtnupdate_Jsonclick = "";
         bttBtndelete_Jsonclick = "";
         lblTextblockpessoa_nome_Jsonclick = "";
         lblTextblockpessoa_tipopessoa_Jsonclick = "";
         lblTextblockpessoa_docto_Jsonclick = "";
         lblTextblockpessoa_ie_Jsonclick = "";
         lblTextblockpessoa_endereco_Jsonclick = "";
         lblTextblockpessoa_municipiocod_Jsonclick = "";
         lblTextblockpessoa_uf_Jsonclick = "";
         lblTextblockpessoa_cep_Jsonclick = "";
         lblTextblockpessoa_telefone_Jsonclick = "";
         lblTextblockpessoa_fax_Jsonclick = "";
         lblTextblockpessoa_ativo_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlA34Pessoa_Codigo = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.pessoageneral__default(),
            new Object[][] {
                new Object[] {
               H00322_A34Pessoa_Codigo, H00322_A38Pessoa_Ativo, H00322_A523Pessoa_Fax, H00322_n523Pessoa_Fax, H00322_A522Pessoa_Telefone, H00322_n522Pessoa_Telefone, H00322_A521Pessoa_CEP, H00322_n521Pessoa_CEP, H00322_A520Pessoa_UF, H00322_n520Pessoa_UF,
               H00322_A503Pessoa_MunicipioCod, H00322_n503Pessoa_MunicipioCod, H00322_A519Pessoa_Endereco, H00322_n519Pessoa_Endereco, H00322_A518Pessoa_IE, H00322_n518Pessoa_IE, H00322_A37Pessoa_Docto, H00322_A36Pessoa_TipoPessoa, H00322_A35Pessoa_Nome
               }
            }
         );
         AV14Pgmname = "PessoaGeneral";
         /* GeneXus formulas. */
         AV14Pgmname = "PessoaGeneral";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short initialized ;
      private short wbEnd ;
      private short wbStart ;
      private short nDraw ;
      private short nDoneStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXWrapped ;
      private int A34Pessoa_Codigo ;
      private int wcpOA34Pessoa_Codigo ;
      private int A503Pessoa_MunicipioCod ;
      private int edtPessoa_Codigo_Visible ;
      private int bttBtnupdate_Enabled ;
      private int bttBtndelete_Enabled ;
      private int AV7Pessoa_Codigo ;
      private int idxLst ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String AV14Pgmname ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String A35Pessoa_Nome ;
      private String A36Pessoa_TipoPessoa ;
      private String A518Pessoa_IE ;
      private String A521Pessoa_CEP ;
      private String A522Pessoa_Telefone ;
      private String A523Pessoa_Fax ;
      private String GXKey ;
      private String GX_FocusControl ;
      private String edtPessoa_Codigo_Internalname ;
      private String edtPessoa_Codigo_Jsonclick ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String chkPessoa_Ativo_Internalname ;
      private String scmdbuf ;
      private String A520Pessoa_UF ;
      private String edtPessoa_Nome_Internalname ;
      private String cmbPessoa_TipoPessoa_Internalname ;
      private String edtPessoa_Docto_Internalname ;
      private String edtPessoa_IE_Internalname ;
      private String edtPessoa_Endereco_Internalname ;
      private String edtPessoa_MunicipioCod_Internalname ;
      private String edtPessoa_UF_Internalname ;
      private String edtPessoa_CEP_Internalname ;
      private String edtPessoa_Telefone_Internalname ;
      private String edtPessoa_Fax_Internalname ;
      private String bttBtnupdate_Internalname ;
      private String bttBtndelete_Internalname ;
      private String sStyleString ;
      private String tblTablecontent_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String tblTableactions_Internalname ;
      private String TempTags ;
      private String bttBtnupdate_Jsonclick ;
      private String bttBtndelete_Jsonclick ;
      private String tblTableattributes_Internalname ;
      private String lblTextblockpessoa_nome_Internalname ;
      private String lblTextblockpessoa_nome_Jsonclick ;
      private String edtPessoa_Nome_Jsonclick ;
      private String lblTextblockpessoa_tipopessoa_Internalname ;
      private String lblTextblockpessoa_tipopessoa_Jsonclick ;
      private String cmbPessoa_TipoPessoa_Jsonclick ;
      private String lblTextblockpessoa_docto_Internalname ;
      private String lblTextblockpessoa_docto_Jsonclick ;
      private String edtPessoa_Docto_Jsonclick ;
      private String lblTextblockpessoa_ie_Internalname ;
      private String lblTextblockpessoa_ie_Jsonclick ;
      private String edtPessoa_IE_Jsonclick ;
      private String lblTextblockpessoa_endereco_Internalname ;
      private String lblTextblockpessoa_endereco_Jsonclick ;
      private String edtPessoa_Endereco_Jsonclick ;
      private String lblTextblockpessoa_municipiocod_Internalname ;
      private String lblTextblockpessoa_municipiocod_Jsonclick ;
      private String edtPessoa_MunicipioCod_Jsonclick ;
      private String lblTextblockpessoa_uf_Internalname ;
      private String lblTextblockpessoa_uf_Jsonclick ;
      private String edtPessoa_UF_Jsonclick ;
      private String lblTextblockpessoa_cep_Internalname ;
      private String lblTextblockpessoa_cep_Jsonclick ;
      private String edtPessoa_CEP_Jsonclick ;
      private String lblTextblockpessoa_telefone_Internalname ;
      private String lblTextblockpessoa_telefone_Jsonclick ;
      private String edtPessoa_Telefone_Jsonclick ;
      private String lblTextblockpessoa_fax_Internalname ;
      private String lblTextblockpessoa_fax_Jsonclick ;
      private String edtPessoa_Fax_Jsonclick ;
      private String lblTextblockpessoa_ativo_Internalname ;
      private String lblTextblockpessoa_ativo_Jsonclick ;
      private String sCtrlA34Pessoa_Codigo ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool A38Pessoa_Ativo ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n523Pessoa_Fax ;
      private bool n522Pessoa_Telefone ;
      private bool n521Pessoa_CEP ;
      private bool n520Pessoa_UF ;
      private bool n503Pessoa_MunicipioCod ;
      private bool n519Pessoa_Endereco ;
      private bool n518Pessoa_IE ;
      private bool returnInSub ;
      private String A37Pessoa_Docto ;
      private String A519Pessoa_Endereco ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbPessoa_TipoPessoa ;
      private GXCheckbox chkPessoa_Ativo ;
      private IDataStoreProvider pr_default ;
      private int[] H00322_A34Pessoa_Codigo ;
      private bool[] H00322_A38Pessoa_Ativo ;
      private String[] H00322_A523Pessoa_Fax ;
      private bool[] H00322_n523Pessoa_Fax ;
      private String[] H00322_A522Pessoa_Telefone ;
      private bool[] H00322_n522Pessoa_Telefone ;
      private String[] H00322_A521Pessoa_CEP ;
      private bool[] H00322_n521Pessoa_CEP ;
      private String[] H00322_A520Pessoa_UF ;
      private bool[] H00322_n520Pessoa_UF ;
      private int[] H00322_A503Pessoa_MunicipioCod ;
      private bool[] H00322_n503Pessoa_MunicipioCod ;
      private String[] H00322_A519Pessoa_Endereco ;
      private bool[] H00322_n519Pessoa_Endereco ;
      private String[] H00322_A518Pessoa_IE ;
      private bool[] H00322_n518Pessoa_IE ;
      private String[] H00322_A37Pessoa_Docto ;
      private String[] H00322_A36Pessoa_TipoPessoa ;
      private String[] H00322_A35Pessoa_Nome ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV11HTTPRequest ;
      private IGxSession AV10Session ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV8TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV9TrnContextAtt ;
   }

   public class pessoageneral__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00322 ;
          prmH00322 = new Object[] {
          new Object[] {"@Pessoa_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00322", "SELECT T1.[Pessoa_Codigo], T1.[Pessoa_Ativo], T1.[Pessoa_Fax], T1.[Pessoa_Telefone], T1.[Pessoa_CEP], T2.[Estado_UF] AS Pessoa_UF, T1.[Pessoa_MunicipioCod] AS Pessoa_MunicipioCod, T1.[Pessoa_Endereco], T1.[Pessoa_IE], T1.[Pessoa_Docto], T1.[Pessoa_TipoPessoa], T1.[Pessoa_Nome] FROM ([Pessoa] T1 WITH (NOLOCK) LEFT JOIN [Municipio] T2 WITH (NOLOCK) ON T2.[Municipio_Codigo] = T1.[Pessoa_MunicipioCod]) WHERE T1.[Pessoa_Codigo] = @Pessoa_Codigo ORDER BY T1.[Pessoa_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00322,1,0,true,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.getBool(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 15) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((String[]) buf[4])[0] = rslt.getString(4, 15) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((String[]) buf[6])[0] = rslt.getString(5, 10) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((String[]) buf[8])[0] = rslt.getString(6, 2) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((int[]) buf[10])[0] = rslt.getInt(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((String[]) buf[12])[0] = rslt.getVarchar(8) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                ((String[]) buf[14])[0] = rslt.getString(9, 15) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(9);
                ((String[]) buf[16])[0] = rslt.getVarchar(10) ;
                ((String[]) buf[17])[0] = rslt.getString(11, 1) ;
                ((String[]) buf[18])[0] = rslt.getString(12, 100) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
