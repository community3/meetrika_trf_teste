/*
               File: GetExtraWWContagemResultadoEmGarantiaFilterData
        Description: Get Extra WWContagem Resultado Em Garantia Filter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/2/2020 5:21:38.46
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getextrawwcontagemresultadoemgarantiafilterdata : GXProcedure
   {
      public getextrawwcontagemresultadoemgarantiafilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getextrawwcontagemresultadoemgarantiafilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV37DDOName = aP0_DDOName;
         this.AV35SearchTxt = aP1_SearchTxt;
         this.AV36SearchTxtTo = aP2_SearchTxtTo;
         this.AV41OptionsJson = "" ;
         this.AV44OptionsDescJson = "" ;
         this.AV46OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV41OptionsJson;
         aP4_OptionsDescJson=this.AV44OptionsDescJson;
         aP5_OptionIndexesJson=this.AV46OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV37DDOName = aP0_DDOName;
         this.AV35SearchTxt = aP1_SearchTxt;
         this.AV36SearchTxtTo = aP2_SearchTxtTo;
         this.AV41OptionsJson = "" ;
         this.AV44OptionsDescJson = "" ;
         this.AV46OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV41OptionsJson;
         aP4_OptionsDescJson=this.AV44OptionsDescJson;
         aP5_OptionIndexesJson=this.AV46OptionIndexesJson;
         return AV46OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getextrawwcontagemresultadoemgarantiafilterdata objgetextrawwcontagemresultadoemgarantiafilterdata;
         objgetextrawwcontagemresultadoemgarantiafilterdata = new getextrawwcontagemresultadoemgarantiafilterdata();
         objgetextrawwcontagemresultadoemgarantiafilterdata.AV37DDOName = aP0_DDOName;
         objgetextrawwcontagemresultadoemgarantiafilterdata.AV35SearchTxt = aP1_SearchTxt;
         objgetextrawwcontagemresultadoemgarantiafilterdata.AV36SearchTxtTo = aP2_SearchTxtTo;
         objgetextrawwcontagemresultadoemgarantiafilterdata.AV41OptionsJson = "" ;
         objgetextrawwcontagemresultadoemgarantiafilterdata.AV44OptionsDescJson = "" ;
         objgetextrawwcontagemresultadoemgarantiafilterdata.AV46OptionIndexesJson = "" ;
         objgetextrawwcontagemresultadoemgarantiafilterdata.context.SetSubmitInitialConfig(context);
         objgetextrawwcontagemresultadoemgarantiafilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetextrawwcontagemresultadoemgarantiafilterdata);
         aP3_OptionsJson=this.AV41OptionsJson;
         aP4_OptionsDescJson=this.AV44OptionsDescJson;
         aP5_OptionIndexesJson=this.AV46OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getextrawwcontagemresultadoemgarantiafilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV40Options = (IGxCollection)(new GxSimpleCollection());
         AV43OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV45OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV37DDOName), "DDO_CONTAGEMRESULTADO_DEMANDAFM") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTAGEMRESULTADO_DEMANDAFMOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV37DDOName), "DDO_CONTAGEMRESULTADO_DEMANDA") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTAGEMRESULTADO_DEMANDAOPTIONS' */
            S131 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV37DDOName), "DDO_CONTAGEMRESULTADO_DESCRICAO") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTAGEMRESULTADO_DESCRICAOOPTIONS' */
            S141 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV37DDOName), "DDO_CONTAGEMRESULTADO_CONTRATADASIGLA") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTAGEMRESULTADO_CONTRATADASIGLAOPTIONS' */
            S151 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV37DDOName), "DDO_CONTAGEMRESULTADO_SERVICOSIGLA") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTAGEMRESULTADO_SERVICOSIGLAOPTIONS' */
            S161 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV41OptionsJson = AV40Options.ToJSonString(false);
         AV44OptionsDescJson = AV43OptionsDesc.ToJSonString(false);
         AV46OptionIndexesJson = AV45OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV48Session.Get("ExtraWWContagemResultadoEmGarantiaGridState"), "") == 0 )
         {
            AV50GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "ExtraWWContagemResultadoEmGarantiaGridState"), "");
         }
         else
         {
            AV50GridState.FromXml(AV48Session.Get("ExtraWWContagemResultadoEmGarantiaGridState"), "");
         }
         AV176GXV1 = 1;
         while ( AV176GXV1 <= AV50GridState.gxTpr_Filtervalues.Count )
         {
            AV51GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV50GridState.gxTpr_Filtervalues.Item(AV176GXV1));
            if ( StringUtil.StrCmp(AV51GridStateFilterValue.gxTpr_Name, "CONTRATADA_AREATRABALHOCOD") == 0 )
            {
               AV53Contratada_AreaTrabalhoCod = (int)(NumberUtil.Val( AV51GridStateFilterValue.gxTpr_Value, "."));
            }
            else if ( StringUtil.StrCmp(AV51GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADO_DEMANDAFM") == 0 )
            {
               AV12TFContagemResultado_DemandaFM = AV51GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV51GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADO_DEMANDAFM_SEL") == 0 )
            {
               AV13TFContagemResultado_DemandaFM_Sel = AV51GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV51GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADO_DEMANDA") == 0 )
            {
               AV14TFContagemResultado_Demanda = AV51GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV51GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADO_DEMANDA_SEL") == 0 )
            {
               AV15TFContagemResultado_Demanda_Sel = AV51GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV51GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADO_DESCRICAO") == 0 )
            {
               AV16TFContagemResultado_Descricao = AV51GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV51GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADO_DESCRICAO_SEL") == 0 )
            {
               AV17TFContagemResultado_Descricao_Sel = AV51GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV51GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADO_DATADMN") == 0 )
            {
               AV18TFContagemResultado_DataDmn = context.localUtil.CToD( AV51GridStateFilterValue.gxTpr_Value, 2);
               AV19TFContagemResultado_DataDmn_To = context.localUtil.CToD( AV51GridStateFilterValue.gxTpr_Valueto, 2);
            }
            else if ( StringUtil.StrCmp(AV51GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADO_DATAULTCNT") == 0 )
            {
               AV20TFContagemResultado_DataUltCnt = context.localUtil.CToD( AV51GridStateFilterValue.gxTpr_Value, 2);
               AV21TFContagemResultado_DataUltCnt_To = context.localUtil.CToD( AV51GridStateFilterValue.gxTpr_Valueto, 2);
            }
            else if ( StringUtil.StrCmp(AV51GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADO_CONTRATADASIGLA") == 0 )
            {
               AV22TFContagemResultado_ContratadaSigla = AV51GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV51GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADO_CONTRATADASIGLA_SEL") == 0 )
            {
               AV23TFContagemResultado_ContratadaSigla_Sel = AV51GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV51GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADO_STATUSDMN_SEL") == 0 )
            {
               AV26TFContagemResultado_StatusDmn_SelsJson = AV51GridStateFilterValue.gxTpr_Value;
               AV27TFContagemResultado_StatusDmn_Sels.FromJSonString(AV26TFContagemResultado_StatusDmn_SelsJson);
            }
            else if ( StringUtil.StrCmp(AV51GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADO_SERVICOSIGLA") == 0 )
            {
               AV29TFContagemResultado_ServicoSigla = AV51GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV51GridStateFilterValue.gxTpr_Name, "TFCONTAGEMRESULTADO_SERVICOSIGLA_SEL") == 0 )
            {
               AV30TFContagemResultado_ServicoSigla_Sel = AV51GridStateFilterValue.gxTpr_Value;
            }
            AV176GXV1 = (int)(AV176GXV1+1);
         }
         if ( AV50GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV52GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV50GridState.gxTpr_Dynamicfilters.Item(1));
            AV55DynamicFiltersSelector1 = AV52GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV55DynamicFiltersSelector1, "CONTAGEMRESULTADO_OSFSOSFM") == 0 )
            {
               AV56DynamicFiltersOperator1 = AV52GridStateDynamicFilter.gxTpr_Operator;
               AV60ContagemResultado_OsFsOsFm1 = AV52GridStateDynamicFilter.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV55DynamicFiltersSelector1, "CONTAGEMRESULTADO_DATADMN") == 0 )
            {
               AV61ContagemResultado_DataDmn1 = context.localUtil.CToD( AV52GridStateDynamicFilter.gxTpr_Value, 2);
               AV62ContagemResultado_DataDmn_To1 = context.localUtil.CToD( AV52GridStateDynamicFilter.gxTpr_Valueto, 2);
            }
            else if ( StringUtil.StrCmp(AV55DynamicFiltersSelector1, "CONTAGEMRESULTADO_DATACNT") == 0 )
            {
               AV57ContagemResultado_DataCnt1 = context.localUtil.CToD( AV52GridStateDynamicFilter.gxTpr_Value, 2);
               AV58ContagemResultado_DataCnt_To1 = context.localUtil.CToD( AV52GridStateDynamicFilter.gxTpr_Valueto, 2);
            }
            else if ( StringUtil.StrCmp(AV55DynamicFiltersSelector1, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 )
            {
               AV63ContagemResultado_DataPrevista1 = context.localUtil.CToT( AV52GridStateDynamicFilter.gxTpr_Value, 2);
               AV64ContagemResultado_DataPrevista_To1 = context.localUtil.CToT( AV52GridStateDynamicFilter.gxTpr_Valueto, 2);
            }
            else if ( StringUtil.StrCmp(AV55DynamicFiltersSelector1, "CONTAGEMRESULTADO_STATUSDMN") == 0 )
            {
               AV59ContagemResultado_StatusDmn1 = AV52GridStateDynamicFilter.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV55DynamicFiltersSelector1, "CONTAGEMRESULTADO_SERVICO") == 0 )
            {
               AV65ContagemResultado_Servico1 = (int)(NumberUtil.Val( AV52GridStateDynamicFilter.gxTpr_Value, "."));
            }
            else if ( StringUtil.StrCmp(AV55DynamicFiltersSelector1, "CONTAGEMRESULTADO_SISTEMACOD") == 0 )
            {
               AV67ContagemResultado_SistemaCod1 = (int)(NumberUtil.Val( AV52GridStateDynamicFilter.gxTpr_Value, "."));
            }
            else if ( StringUtil.StrCmp(AV55DynamicFiltersSelector1, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 )
            {
               AV68ContagemResultado_ContratadaCod1 = (int)(NumberUtil.Val( AV52GridStateDynamicFilter.gxTpr_Value, "."));
            }
            else if ( StringUtil.StrCmp(AV55DynamicFiltersSelector1, "CONTAGEMRESULTADO_DESCRICAO") == 0 )
            {
               AV75ContagemResultado_Descricao1 = AV52GridStateDynamicFilter.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV55DynamicFiltersSelector1, "CONTAGEMRESULTADO_OWNER") == 0 )
            {
               AV76ContagemResultado_Owner1 = (int)(NumberUtil.Val( AV52GridStateDynamicFilter.gxTpr_Value, "."));
            }
            if ( AV50GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV78DynamicFiltersEnabled2 = true;
               AV52GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV50GridState.gxTpr_Dynamicfilters.Item(2));
               AV79DynamicFiltersSelector2 = AV52GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV79DynamicFiltersSelector2, "CONTAGEMRESULTADO_OSFSOSFM") == 0 )
               {
                  AV80DynamicFiltersOperator2 = AV52GridStateDynamicFilter.gxTpr_Operator;
                  AV84ContagemResultado_OsFsOsFm2 = AV52GridStateDynamicFilter.gxTpr_Value;
               }
               else if ( StringUtil.StrCmp(AV79DynamicFiltersSelector2, "CONTAGEMRESULTADO_DATADMN") == 0 )
               {
                  AV85ContagemResultado_DataDmn2 = context.localUtil.CToD( AV52GridStateDynamicFilter.gxTpr_Value, 2);
                  AV86ContagemResultado_DataDmn_To2 = context.localUtil.CToD( AV52GridStateDynamicFilter.gxTpr_Valueto, 2);
               }
               else if ( StringUtil.StrCmp(AV79DynamicFiltersSelector2, "CONTAGEMRESULTADO_DATACNT") == 0 )
               {
                  AV81ContagemResultado_DataCnt2 = context.localUtil.CToD( AV52GridStateDynamicFilter.gxTpr_Value, 2);
                  AV82ContagemResultado_DataCnt_To2 = context.localUtil.CToD( AV52GridStateDynamicFilter.gxTpr_Valueto, 2);
               }
               else if ( StringUtil.StrCmp(AV79DynamicFiltersSelector2, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 )
               {
                  AV87ContagemResultado_DataPrevista2 = context.localUtil.CToT( AV52GridStateDynamicFilter.gxTpr_Value, 2);
                  AV88ContagemResultado_DataPrevista_To2 = context.localUtil.CToT( AV52GridStateDynamicFilter.gxTpr_Valueto, 2);
               }
               else if ( StringUtil.StrCmp(AV79DynamicFiltersSelector2, "CONTAGEMRESULTADO_STATUSDMN") == 0 )
               {
                  AV83ContagemResultado_StatusDmn2 = AV52GridStateDynamicFilter.gxTpr_Value;
               }
               else if ( StringUtil.StrCmp(AV79DynamicFiltersSelector2, "CONTAGEMRESULTADO_SERVICO") == 0 )
               {
                  AV89ContagemResultado_Servico2 = (int)(NumberUtil.Val( AV52GridStateDynamicFilter.gxTpr_Value, "."));
               }
               else if ( StringUtil.StrCmp(AV79DynamicFiltersSelector2, "CONTAGEMRESULTADO_SISTEMACOD") == 0 )
               {
                  AV91ContagemResultado_SistemaCod2 = (int)(NumberUtil.Val( AV52GridStateDynamicFilter.gxTpr_Value, "."));
               }
               else if ( StringUtil.StrCmp(AV79DynamicFiltersSelector2, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 )
               {
                  AV92ContagemResultado_ContratadaCod2 = (int)(NumberUtil.Val( AV52GridStateDynamicFilter.gxTpr_Value, "."));
               }
               else if ( StringUtil.StrCmp(AV79DynamicFiltersSelector2, "CONTAGEMRESULTADO_DESCRICAO") == 0 )
               {
                  AV99ContagemResultado_Descricao2 = AV52GridStateDynamicFilter.gxTpr_Value;
               }
               else if ( StringUtil.StrCmp(AV79DynamicFiltersSelector2, "CONTAGEMRESULTADO_OWNER") == 0 )
               {
                  AV100ContagemResultado_Owner2 = (int)(NumberUtil.Val( AV52GridStateDynamicFilter.gxTpr_Value, "."));
               }
               if ( AV50GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  AV102DynamicFiltersEnabled3 = true;
                  AV52GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV50GridState.gxTpr_Dynamicfilters.Item(3));
                  AV103DynamicFiltersSelector3 = AV52GridStateDynamicFilter.gxTpr_Selected;
                  if ( StringUtil.StrCmp(AV103DynamicFiltersSelector3, "CONTAGEMRESULTADO_OSFSOSFM") == 0 )
                  {
                     AV104DynamicFiltersOperator3 = AV52GridStateDynamicFilter.gxTpr_Operator;
                     AV108ContagemResultado_OsFsOsFm3 = AV52GridStateDynamicFilter.gxTpr_Value;
                  }
                  else if ( StringUtil.StrCmp(AV103DynamicFiltersSelector3, "CONTAGEMRESULTADO_DATADMN") == 0 )
                  {
                     AV109ContagemResultado_DataDmn3 = context.localUtil.CToD( AV52GridStateDynamicFilter.gxTpr_Value, 2);
                     AV110ContagemResultado_DataDmn_To3 = context.localUtil.CToD( AV52GridStateDynamicFilter.gxTpr_Valueto, 2);
                  }
                  else if ( StringUtil.StrCmp(AV103DynamicFiltersSelector3, "CONTAGEMRESULTADO_DATACNT") == 0 )
                  {
                     AV105ContagemResultado_DataCnt3 = context.localUtil.CToD( AV52GridStateDynamicFilter.gxTpr_Value, 2);
                     AV106ContagemResultado_DataCnt_To3 = context.localUtil.CToD( AV52GridStateDynamicFilter.gxTpr_Valueto, 2);
                  }
                  else if ( StringUtil.StrCmp(AV103DynamicFiltersSelector3, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 )
                  {
                     AV111ContagemResultado_DataPrevista3 = context.localUtil.CToT( AV52GridStateDynamicFilter.gxTpr_Value, 2);
                     AV112ContagemResultado_DataPrevista_To3 = context.localUtil.CToT( AV52GridStateDynamicFilter.gxTpr_Valueto, 2);
                  }
                  else if ( StringUtil.StrCmp(AV103DynamicFiltersSelector3, "CONTAGEMRESULTADO_STATUSDMN") == 0 )
                  {
                     AV107ContagemResultado_StatusDmn3 = AV52GridStateDynamicFilter.gxTpr_Value;
                  }
                  else if ( StringUtil.StrCmp(AV103DynamicFiltersSelector3, "CONTAGEMRESULTADO_SERVICO") == 0 )
                  {
                     AV113ContagemResultado_Servico3 = (int)(NumberUtil.Val( AV52GridStateDynamicFilter.gxTpr_Value, "."));
                  }
                  else if ( StringUtil.StrCmp(AV103DynamicFiltersSelector3, "CONTAGEMRESULTADO_SISTEMACOD") == 0 )
                  {
                     AV115ContagemResultado_SistemaCod3 = (int)(NumberUtil.Val( AV52GridStateDynamicFilter.gxTpr_Value, "."));
                  }
                  else if ( StringUtil.StrCmp(AV103DynamicFiltersSelector3, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 )
                  {
                     AV116ContagemResultado_ContratadaCod3 = (int)(NumberUtil.Val( AV52GridStateDynamicFilter.gxTpr_Value, "."));
                  }
                  else if ( StringUtil.StrCmp(AV103DynamicFiltersSelector3, "CONTAGEMRESULTADO_DESCRICAO") == 0 )
                  {
                     AV123ContagemResultado_Descricao3 = AV52GridStateDynamicFilter.gxTpr_Value;
                  }
                  else if ( StringUtil.StrCmp(AV103DynamicFiltersSelector3, "CONTAGEMRESULTADO_OWNER") == 0 )
                  {
                     AV124ContagemResultado_Owner3 = (int)(NumberUtil.Val( AV52GridStateDynamicFilter.gxTpr_Value, "."));
                  }
                  if ( AV50GridState.gxTpr_Dynamicfilters.Count >= 4 )
                  {
                     AV126DynamicFiltersEnabled4 = true;
                     AV52GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV50GridState.gxTpr_Dynamicfilters.Item(4));
                     AV127DynamicFiltersSelector4 = AV52GridStateDynamicFilter.gxTpr_Selected;
                     if ( StringUtil.StrCmp(AV127DynamicFiltersSelector4, "CONTAGEMRESULTADO_OSFSOSFM") == 0 )
                     {
                        AV128DynamicFiltersOperator4 = AV52GridStateDynamicFilter.gxTpr_Operator;
                        AV132ContagemResultado_OsFsOsFm4 = AV52GridStateDynamicFilter.gxTpr_Value;
                     }
                     else if ( StringUtil.StrCmp(AV127DynamicFiltersSelector4, "CONTAGEMRESULTADO_DATADMN") == 0 )
                     {
                        AV133ContagemResultado_DataDmn4 = context.localUtil.CToD( AV52GridStateDynamicFilter.gxTpr_Value, 2);
                        AV134ContagemResultado_DataDmn_To4 = context.localUtil.CToD( AV52GridStateDynamicFilter.gxTpr_Valueto, 2);
                     }
                     else if ( StringUtil.StrCmp(AV127DynamicFiltersSelector4, "CONTAGEMRESULTADO_DATACNT") == 0 )
                     {
                        AV129ContagemResultado_DataCnt4 = context.localUtil.CToD( AV52GridStateDynamicFilter.gxTpr_Value, 2);
                        AV130ContagemResultado_DataCnt_To4 = context.localUtil.CToD( AV52GridStateDynamicFilter.gxTpr_Valueto, 2);
                     }
                     else if ( StringUtil.StrCmp(AV127DynamicFiltersSelector4, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 )
                     {
                        AV135ContagemResultado_DataPrevista4 = context.localUtil.CToT( AV52GridStateDynamicFilter.gxTpr_Value, 2);
                        AV136ContagemResultado_DataPrevista_To4 = context.localUtil.CToT( AV52GridStateDynamicFilter.gxTpr_Valueto, 2);
                     }
                     else if ( StringUtil.StrCmp(AV127DynamicFiltersSelector4, "CONTAGEMRESULTADO_STATUSDMN") == 0 )
                     {
                        AV131ContagemResultado_StatusDmn4 = AV52GridStateDynamicFilter.gxTpr_Value;
                     }
                     else if ( StringUtil.StrCmp(AV127DynamicFiltersSelector4, "CONTAGEMRESULTADO_SERVICO") == 0 )
                     {
                        AV137ContagemResultado_Servico4 = (int)(NumberUtil.Val( AV52GridStateDynamicFilter.gxTpr_Value, "."));
                     }
                     else if ( StringUtil.StrCmp(AV127DynamicFiltersSelector4, "CONTAGEMRESULTADO_SISTEMACOD") == 0 )
                     {
                        AV139ContagemResultado_SistemaCod4 = (int)(NumberUtil.Val( AV52GridStateDynamicFilter.gxTpr_Value, "."));
                     }
                     else if ( StringUtil.StrCmp(AV127DynamicFiltersSelector4, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 )
                     {
                        AV140ContagemResultado_ContratadaCod4 = (int)(NumberUtil.Val( AV52GridStateDynamicFilter.gxTpr_Value, "."));
                     }
                     else if ( StringUtil.StrCmp(AV127DynamicFiltersSelector4, "CONTAGEMRESULTADO_DESCRICAO") == 0 )
                     {
                        AV147ContagemResultado_Descricao4 = AV52GridStateDynamicFilter.gxTpr_Value;
                     }
                     else if ( StringUtil.StrCmp(AV127DynamicFiltersSelector4, "CONTAGEMRESULTADO_OWNER") == 0 )
                     {
                        AV148ContagemResultado_Owner4 = (int)(NumberUtil.Val( AV52GridStateDynamicFilter.gxTpr_Value, "."));
                     }
                     if ( AV50GridState.gxTpr_Dynamicfilters.Count >= 5 )
                     {
                        AV150DynamicFiltersEnabled5 = true;
                        AV52GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV50GridState.gxTpr_Dynamicfilters.Item(5));
                        AV151DynamicFiltersSelector5 = AV52GridStateDynamicFilter.gxTpr_Selected;
                        if ( StringUtil.StrCmp(AV151DynamicFiltersSelector5, "CONTAGEMRESULTADO_OSFSOSFM") == 0 )
                        {
                           AV152DynamicFiltersOperator5 = AV52GridStateDynamicFilter.gxTpr_Operator;
                           AV156ContagemResultado_OsFsOsFm5 = AV52GridStateDynamicFilter.gxTpr_Value;
                        }
                        else if ( StringUtil.StrCmp(AV151DynamicFiltersSelector5, "CONTAGEMRESULTADO_DATADMN") == 0 )
                        {
                           AV157ContagemResultado_DataDmn5 = context.localUtil.CToD( AV52GridStateDynamicFilter.gxTpr_Value, 2);
                           AV158ContagemResultado_DataDmn_To5 = context.localUtil.CToD( AV52GridStateDynamicFilter.gxTpr_Valueto, 2);
                        }
                        else if ( StringUtil.StrCmp(AV151DynamicFiltersSelector5, "CONTAGEMRESULTADO_DATACNT") == 0 )
                        {
                           AV153ContagemResultado_DataCnt5 = context.localUtil.CToD( AV52GridStateDynamicFilter.gxTpr_Value, 2);
                           AV154ContagemResultado_DataCnt_To5 = context.localUtil.CToD( AV52GridStateDynamicFilter.gxTpr_Valueto, 2);
                        }
                        else if ( StringUtil.StrCmp(AV151DynamicFiltersSelector5, "CONTAGEMRESULTADO_DATAPREVISTA") == 0 )
                        {
                           AV159ContagemResultado_DataPrevista5 = context.localUtil.CToT( AV52GridStateDynamicFilter.gxTpr_Value, 2);
                           AV160ContagemResultado_DataPrevista_To5 = context.localUtil.CToT( AV52GridStateDynamicFilter.gxTpr_Valueto, 2);
                        }
                        else if ( StringUtil.StrCmp(AV151DynamicFiltersSelector5, "CONTAGEMRESULTADO_STATUSDMN") == 0 )
                        {
                           AV155ContagemResultado_StatusDmn5 = AV52GridStateDynamicFilter.gxTpr_Value;
                        }
                        else if ( StringUtil.StrCmp(AV151DynamicFiltersSelector5, "CONTAGEMRESULTADO_SERVICO") == 0 )
                        {
                           AV161ContagemResultado_Servico5 = (int)(NumberUtil.Val( AV52GridStateDynamicFilter.gxTpr_Value, "."));
                        }
                        else if ( StringUtil.StrCmp(AV151DynamicFiltersSelector5, "CONTAGEMRESULTADO_SISTEMACOD") == 0 )
                        {
                           AV163ContagemResultado_SistemaCod5 = (int)(NumberUtil.Val( AV52GridStateDynamicFilter.gxTpr_Value, "."));
                        }
                        else if ( StringUtil.StrCmp(AV151DynamicFiltersSelector5, "CONTAGEMRESULTADO_CONTRATADACOD") == 0 )
                        {
                           AV164ContagemResultado_ContratadaCod5 = (int)(NumberUtil.Val( AV52GridStateDynamicFilter.gxTpr_Value, "."));
                        }
                        else if ( StringUtil.StrCmp(AV151DynamicFiltersSelector5, "CONTAGEMRESULTADO_DESCRICAO") == 0 )
                        {
                           AV171ContagemResultado_Descricao5 = AV52GridStateDynamicFilter.gxTpr_Value;
                        }
                        else if ( StringUtil.StrCmp(AV151DynamicFiltersSelector5, "CONTAGEMRESULTADO_OWNER") == 0 )
                        {
                           AV172ContagemResultado_Owner5 = (int)(NumberUtil.Val( AV52GridStateDynamicFilter.gxTpr_Value, "."));
                        }
                     }
                  }
               }
            }
         }
      }

      protected void S121( )
      {
         /* 'LOADCONTAGEMRESULTADO_DEMANDAFMOPTIONS' Routine */
         AV12TFContagemResultado_DemandaFM = AV35SearchTxt;
         AV13TFContagemResultado_DemandaFM_Sel = "";
         /* Using cursor P00U12 */
         pr_default.execute(0);
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKU12 = false;
            A493ContagemResultado_DemandaFM = P00U12_A493ContagemResultado_DemandaFM[0];
            n493ContagemResultado_DemandaFM = P00U12_n493ContagemResultado_DemandaFM[0];
            A456ContagemResultado_Codigo = P00U12_A456ContagemResultado_Codigo[0];
            AV47count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( StringUtil.StrCmp(P00U12_A493ContagemResultado_DemandaFM[0], A493ContagemResultado_DemandaFM) == 0 ) )
            {
               BRKU12 = false;
               A456ContagemResultado_Codigo = P00U12_A456ContagemResultado_Codigo[0];
               AV47count = (long)(AV47count+1);
               BRKU12 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A493ContagemResultado_DemandaFM)) )
            {
               AV39Option = A493ContagemResultado_DemandaFM;
               AV40Options.Add(AV39Option, 0);
               AV45OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV47count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV40Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKU12 )
            {
               BRKU12 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      protected void S131( )
      {
         /* 'LOADCONTAGEMRESULTADO_DEMANDAOPTIONS' Routine */
         AV14TFContagemResultado_Demanda = AV35SearchTxt;
         AV15TFContagemResultado_Demanda_Sel = "";
         /* Using cursor P00U13 */
         pr_default.execute(1);
         while ( (pr_default.getStatus(1) != 101) )
         {
            BRKU14 = false;
            A457ContagemResultado_Demanda = P00U13_A457ContagemResultado_Demanda[0];
            n457ContagemResultado_Demanda = P00U13_n457ContagemResultado_Demanda[0];
            A456ContagemResultado_Codigo = P00U13_A456ContagemResultado_Codigo[0];
            AV47count = 0;
            while ( (pr_default.getStatus(1) != 101) && ( StringUtil.StrCmp(P00U13_A457ContagemResultado_Demanda[0], A457ContagemResultado_Demanda) == 0 ) )
            {
               BRKU14 = false;
               A456ContagemResultado_Codigo = P00U13_A456ContagemResultado_Codigo[0];
               AV47count = (long)(AV47count+1);
               BRKU14 = true;
               pr_default.readNext(1);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A457ContagemResultado_Demanda)) )
            {
               AV39Option = A457ContagemResultado_Demanda;
               AV42OptionDesc = StringUtil.Trim( StringUtil.RTrim( context.localUtil.Format( A457ContagemResultado_Demanda, "@!")));
               AV40Options.Add(AV39Option, 0);
               AV43OptionsDesc.Add(AV42OptionDesc, 0);
               AV45OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV47count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV40Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKU14 )
            {
               BRKU14 = true;
               pr_default.readNext(1);
            }
         }
         pr_default.close(1);
      }

      protected void S141( )
      {
         /* 'LOADCONTAGEMRESULTADO_DESCRICAOOPTIONS' Routine */
         AV16TFContagemResultado_Descricao = AV35SearchTxt;
         AV17TFContagemResultado_Descricao_Sel = "";
         /* Using cursor P00U14 */
         pr_default.execute(2);
         while ( (pr_default.getStatus(2) != 101) )
         {
            BRKU16 = false;
            A494ContagemResultado_Descricao = P00U14_A494ContagemResultado_Descricao[0];
            n494ContagemResultado_Descricao = P00U14_n494ContagemResultado_Descricao[0];
            A456ContagemResultado_Codigo = P00U14_A456ContagemResultado_Codigo[0];
            AV47count = 0;
            while ( (pr_default.getStatus(2) != 101) && ( StringUtil.StrCmp(P00U14_A494ContagemResultado_Descricao[0], A494ContagemResultado_Descricao) == 0 ) )
            {
               BRKU16 = false;
               A456ContagemResultado_Codigo = P00U14_A456ContagemResultado_Codigo[0];
               AV47count = (long)(AV47count+1);
               BRKU16 = true;
               pr_default.readNext(2);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A494ContagemResultado_Descricao)) )
            {
               AV39Option = A494ContagemResultado_Descricao;
               AV40Options.Add(AV39Option, 0);
               AV45OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV47count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV40Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKU16 )
            {
               BRKU16 = true;
               pr_default.readNext(2);
            }
         }
         pr_default.close(2);
      }

      protected void S151( )
      {
         /* 'LOADCONTAGEMRESULTADO_CONTRATADASIGLAOPTIONS' Routine */
         AV22TFContagemResultado_ContratadaSigla = AV35SearchTxt;
         AV23TFContagemResultado_ContratadaSigla_Sel = "";
         /* Using cursor P00U15 */
         pr_default.execute(3);
         while ( (pr_default.getStatus(3) != 101) )
         {
            BRKU18 = false;
            A490ContagemResultado_ContratadaCod = P00U15_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = P00U15_n490ContagemResultado_ContratadaCod[0];
            A803ContagemResultado_ContratadaSigla = P00U15_A803ContagemResultado_ContratadaSigla[0];
            n803ContagemResultado_ContratadaSigla = P00U15_n803ContagemResultado_ContratadaSigla[0];
            A456ContagemResultado_Codigo = P00U15_A456ContagemResultado_Codigo[0];
            A803ContagemResultado_ContratadaSigla = P00U15_A803ContagemResultado_ContratadaSigla[0];
            n803ContagemResultado_ContratadaSigla = P00U15_n803ContagemResultado_ContratadaSigla[0];
            AV47count = 0;
            while ( (pr_default.getStatus(3) != 101) && ( StringUtil.StrCmp(P00U15_A803ContagemResultado_ContratadaSigla[0], A803ContagemResultado_ContratadaSigla) == 0 ) )
            {
               BRKU18 = false;
               A490ContagemResultado_ContratadaCod = P00U15_A490ContagemResultado_ContratadaCod[0];
               n490ContagemResultado_ContratadaCod = P00U15_n490ContagemResultado_ContratadaCod[0];
               A456ContagemResultado_Codigo = P00U15_A456ContagemResultado_Codigo[0];
               AV47count = (long)(AV47count+1);
               BRKU18 = true;
               pr_default.readNext(3);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A803ContagemResultado_ContratadaSigla)) )
            {
               AV39Option = A803ContagemResultado_ContratadaSigla;
               AV40Options.Add(AV39Option, 0);
               AV45OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV47count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV40Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKU18 )
            {
               BRKU18 = true;
               pr_default.readNext(3);
            }
         }
         pr_default.close(3);
      }

      protected void S161( )
      {
         /* 'LOADCONTAGEMRESULTADO_SERVICOSIGLAOPTIONS' Routine */
         AV29TFContagemResultado_ServicoSigla = AV35SearchTxt;
         AV30TFContagemResultado_ServicoSigla_Sel = "";
         /* Using cursor P00U16 */
         pr_default.execute(4);
         while ( (pr_default.getStatus(4) != 101) )
         {
            BRKU110 = false;
            A1553ContagemResultado_CntSrvCod = P00U16_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = P00U16_n1553ContagemResultado_CntSrvCod[0];
            A601ContagemResultado_Servico = P00U16_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = P00U16_n601ContagemResultado_Servico[0];
            A801ContagemResultado_ServicoSigla = P00U16_A801ContagemResultado_ServicoSigla[0];
            n801ContagemResultado_ServicoSigla = P00U16_n801ContagemResultado_ServicoSigla[0];
            A456ContagemResultado_Codigo = P00U16_A456ContagemResultado_Codigo[0];
            A601ContagemResultado_Servico = P00U16_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = P00U16_n601ContagemResultado_Servico[0];
            A801ContagemResultado_ServicoSigla = P00U16_A801ContagemResultado_ServicoSigla[0];
            n801ContagemResultado_ServicoSigla = P00U16_n801ContagemResultado_ServicoSigla[0];
            AV47count = 0;
            while ( (pr_default.getStatus(4) != 101) && ( StringUtil.StrCmp(P00U16_A801ContagemResultado_ServicoSigla[0], A801ContagemResultado_ServicoSigla) == 0 ) )
            {
               BRKU110 = false;
               A1553ContagemResultado_CntSrvCod = P00U16_A1553ContagemResultado_CntSrvCod[0];
               n1553ContagemResultado_CntSrvCod = P00U16_n1553ContagemResultado_CntSrvCod[0];
               A601ContagemResultado_Servico = P00U16_A601ContagemResultado_Servico[0];
               n601ContagemResultado_Servico = P00U16_n601ContagemResultado_Servico[0];
               A456ContagemResultado_Codigo = P00U16_A456ContagemResultado_Codigo[0];
               A601ContagemResultado_Servico = P00U16_A601ContagemResultado_Servico[0];
               n601ContagemResultado_Servico = P00U16_n601ContagemResultado_Servico[0];
               AV47count = (long)(AV47count+1);
               BRKU110 = true;
               pr_default.readNext(4);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A801ContagemResultado_ServicoSigla)) )
            {
               AV39Option = A801ContagemResultado_ServicoSigla;
               AV40Options.Add(AV39Option, 0);
               AV45OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV47count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV40Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKU110 )
            {
               BRKU110 = true;
               pr_default.readNext(4);
            }
         }
         pr_default.close(4);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV40Options = new GxSimpleCollection();
         AV43OptionsDesc = new GxSimpleCollection();
         AV45OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV48Session = context.GetSession();
         AV50GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV51GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV12TFContagemResultado_DemandaFM = "";
         AV13TFContagemResultado_DemandaFM_Sel = "";
         AV14TFContagemResultado_Demanda = "";
         AV15TFContagemResultado_Demanda_Sel = "";
         AV16TFContagemResultado_Descricao = "";
         AV17TFContagemResultado_Descricao_Sel = "";
         AV18TFContagemResultado_DataDmn = DateTime.MinValue;
         AV19TFContagemResultado_DataDmn_To = DateTime.MinValue;
         AV20TFContagemResultado_DataUltCnt = DateTime.MinValue;
         AV21TFContagemResultado_DataUltCnt_To = DateTime.MinValue;
         AV22TFContagemResultado_ContratadaSigla = "";
         AV23TFContagemResultado_ContratadaSigla_Sel = "";
         AV26TFContagemResultado_StatusDmn_SelsJson = "";
         AV27TFContagemResultado_StatusDmn_Sels = new GxSimpleCollection();
         AV29TFContagemResultado_ServicoSigla = "";
         AV30TFContagemResultado_ServicoSigla_Sel = "";
         AV52GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV55DynamicFiltersSelector1 = "";
         AV60ContagemResultado_OsFsOsFm1 = "";
         AV61ContagemResultado_DataDmn1 = DateTime.MinValue;
         AV62ContagemResultado_DataDmn_To1 = DateTime.MinValue;
         AV57ContagemResultado_DataCnt1 = DateTime.MinValue;
         AV58ContagemResultado_DataCnt_To1 = DateTime.MinValue;
         AV63ContagemResultado_DataPrevista1 = (DateTime)(DateTime.MinValue);
         AV64ContagemResultado_DataPrevista_To1 = (DateTime)(DateTime.MinValue);
         AV59ContagemResultado_StatusDmn1 = "";
         AV75ContagemResultado_Descricao1 = "";
         AV79DynamicFiltersSelector2 = "";
         AV84ContagemResultado_OsFsOsFm2 = "";
         AV85ContagemResultado_DataDmn2 = DateTime.MinValue;
         AV86ContagemResultado_DataDmn_To2 = DateTime.MinValue;
         AV81ContagemResultado_DataCnt2 = DateTime.MinValue;
         AV82ContagemResultado_DataCnt_To2 = DateTime.MinValue;
         AV87ContagemResultado_DataPrevista2 = (DateTime)(DateTime.MinValue);
         AV88ContagemResultado_DataPrevista_To2 = (DateTime)(DateTime.MinValue);
         AV83ContagemResultado_StatusDmn2 = "";
         AV99ContagemResultado_Descricao2 = "";
         AV103DynamicFiltersSelector3 = "";
         AV108ContagemResultado_OsFsOsFm3 = "";
         AV109ContagemResultado_DataDmn3 = DateTime.MinValue;
         AV110ContagemResultado_DataDmn_To3 = DateTime.MinValue;
         AV105ContagemResultado_DataCnt3 = DateTime.MinValue;
         AV106ContagemResultado_DataCnt_To3 = DateTime.MinValue;
         AV111ContagemResultado_DataPrevista3 = (DateTime)(DateTime.MinValue);
         AV112ContagemResultado_DataPrevista_To3 = (DateTime)(DateTime.MinValue);
         AV107ContagemResultado_StatusDmn3 = "";
         AV123ContagemResultado_Descricao3 = "";
         AV127DynamicFiltersSelector4 = "";
         AV132ContagemResultado_OsFsOsFm4 = "";
         AV133ContagemResultado_DataDmn4 = DateTime.MinValue;
         AV134ContagemResultado_DataDmn_To4 = DateTime.MinValue;
         AV129ContagemResultado_DataCnt4 = DateTime.MinValue;
         AV130ContagemResultado_DataCnt_To4 = DateTime.MinValue;
         AV135ContagemResultado_DataPrevista4 = (DateTime)(DateTime.MinValue);
         AV136ContagemResultado_DataPrevista_To4 = (DateTime)(DateTime.MinValue);
         AV131ContagemResultado_StatusDmn4 = "";
         AV147ContagemResultado_Descricao4 = "";
         AV151DynamicFiltersSelector5 = "";
         AV156ContagemResultado_OsFsOsFm5 = "";
         AV157ContagemResultado_DataDmn5 = DateTime.MinValue;
         AV158ContagemResultado_DataDmn_To5 = DateTime.MinValue;
         AV153ContagemResultado_DataCnt5 = DateTime.MinValue;
         AV154ContagemResultado_DataCnt_To5 = DateTime.MinValue;
         AV159ContagemResultado_DataPrevista5 = (DateTime)(DateTime.MinValue);
         AV160ContagemResultado_DataPrevista_To5 = (DateTime)(DateTime.MinValue);
         AV155ContagemResultado_StatusDmn5 = "";
         AV171ContagemResultado_Descricao5 = "";
         scmdbuf = "";
         P00U12_A493ContagemResultado_DemandaFM = new String[] {""} ;
         P00U12_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         P00U12_A456ContagemResultado_Codigo = new int[1] ;
         A493ContagemResultado_DemandaFM = "";
         AV39Option = "";
         P00U13_A457ContagemResultado_Demanda = new String[] {""} ;
         P00U13_n457ContagemResultado_Demanda = new bool[] {false} ;
         P00U13_A456ContagemResultado_Codigo = new int[1] ;
         A457ContagemResultado_Demanda = "";
         AV42OptionDesc = "";
         P00U14_A494ContagemResultado_Descricao = new String[] {""} ;
         P00U14_n494ContagemResultado_Descricao = new bool[] {false} ;
         P00U14_A456ContagemResultado_Codigo = new int[1] ;
         A494ContagemResultado_Descricao = "";
         P00U15_A490ContagemResultado_ContratadaCod = new int[1] ;
         P00U15_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         P00U15_A803ContagemResultado_ContratadaSigla = new String[] {""} ;
         P00U15_n803ContagemResultado_ContratadaSigla = new bool[] {false} ;
         P00U15_A456ContagemResultado_Codigo = new int[1] ;
         A803ContagemResultado_ContratadaSigla = "";
         P00U16_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P00U16_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P00U16_A601ContagemResultado_Servico = new int[1] ;
         P00U16_n601ContagemResultado_Servico = new bool[] {false} ;
         P00U16_A801ContagemResultado_ServicoSigla = new String[] {""} ;
         P00U16_n801ContagemResultado_ServicoSigla = new bool[] {false} ;
         P00U16_A456ContagemResultado_Codigo = new int[1] ;
         A801ContagemResultado_ServicoSigla = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getextrawwcontagemresultadoemgarantiafilterdata__default(),
            new Object[][] {
                new Object[] {
               P00U12_A493ContagemResultado_DemandaFM, P00U12_n493ContagemResultado_DemandaFM, P00U12_A456ContagemResultado_Codigo
               }
               , new Object[] {
               P00U13_A457ContagemResultado_Demanda, P00U13_n457ContagemResultado_Demanda, P00U13_A456ContagemResultado_Codigo
               }
               , new Object[] {
               P00U14_A494ContagemResultado_Descricao, P00U14_n494ContagemResultado_Descricao, P00U14_A456ContagemResultado_Codigo
               }
               , new Object[] {
               P00U15_A490ContagemResultado_ContratadaCod, P00U15_n490ContagemResultado_ContratadaCod, P00U15_A803ContagemResultado_ContratadaSigla, P00U15_n803ContagemResultado_ContratadaSigla, P00U15_A456ContagemResultado_Codigo
               }
               , new Object[] {
               P00U16_A1553ContagemResultado_CntSrvCod, P00U16_n1553ContagemResultado_CntSrvCod, P00U16_A601ContagemResultado_Servico, P00U16_n601ContagemResultado_Servico, P00U16_A801ContagemResultado_ServicoSigla, P00U16_n801ContagemResultado_ServicoSigla, P00U16_A456ContagemResultado_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV56DynamicFiltersOperator1 ;
      private short AV80DynamicFiltersOperator2 ;
      private short AV104DynamicFiltersOperator3 ;
      private short AV128DynamicFiltersOperator4 ;
      private short AV152DynamicFiltersOperator5 ;
      private int AV176GXV1 ;
      private int AV53Contratada_AreaTrabalhoCod ;
      private int AV65ContagemResultado_Servico1 ;
      private int AV67ContagemResultado_SistemaCod1 ;
      private int AV68ContagemResultado_ContratadaCod1 ;
      private int AV76ContagemResultado_Owner1 ;
      private int AV89ContagemResultado_Servico2 ;
      private int AV91ContagemResultado_SistemaCod2 ;
      private int AV92ContagemResultado_ContratadaCod2 ;
      private int AV100ContagemResultado_Owner2 ;
      private int AV113ContagemResultado_Servico3 ;
      private int AV115ContagemResultado_SistemaCod3 ;
      private int AV116ContagemResultado_ContratadaCod3 ;
      private int AV124ContagemResultado_Owner3 ;
      private int AV137ContagemResultado_Servico4 ;
      private int AV139ContagemResultado_SistemaCod4 ;
      private int AV140ContagemResultado_ContratadaCod4 ;
      private int AV148ContagemResultado_Owner4 ;
      private int AV161ContagemResultado_Servico5 ;
      private int AV163ContagemResultado_SistemaCod5 ;
      private int AV164ContagemResultado_ContratadaCod5 ;
      private int AV172ContagemResultado_Owner5 ;
      private int A456ContagemResultado_Codigo ;
      private int A490ContagemResultado_ContratadaCod ;
      private int A1553ContagemResultado_CntSrvCod ;
      private int A601ContagemResultado_Servico ;
      private long AV47count ;
      private String AV22TFContagemResultado_ContratadaSigla ;
      private String AV23TFContagemResultado_ContratadaSigla_Sel ;
      private String AV29TFContagemResultado_ServicoSigla ;
      private String AV30TFContagemResultado_ServicoSigla_Sel ;
      private String AV59ContagemResultado_StatusDmn1 ;
      private String AV83ContagemResultado_StatusDmn2 ;
      private String AV107ContagemResultado_StatusDmn3 ;
      private String AV131ContagemResultado_StatusDmn4 ;
      private String AV155ContagemResultado_StatusDmn5 ;
      private String scmdbuf ;
      private String A803ContagemResultado_ContratadaSigla ;
      private String A801ContagemResultado_ServicoSigla ;
      private DateTime AV63ContagemResultado_DataPrevista1 ;
      private DateTime AV64ContagemResultado_DataPrevista_To1 ;
      private DateTime AV87ContagemResultado_DataPrevista2 ;
      private DateTime AV88ContagemResultado_DataPrevista_To2 ;
      private DateTime AV111ContagemResultado_DataPrevista3 ;
      private DateTime AV112ContagemResultado_DataPrevista_To3 ;
      private DateTime AV135ContagemResultado_DataPrevista4 ;
      private DateTime AV136ContagemResultado_DataPrevista_To4 ;
      private DateTime AV159ContagemResultado_DataPrevista5 ;
      private DateTime AV160ContagemResultado_DataPrevista_To5 ;
      private DateTime AV18TFContagemResultado_DataDmn ;
      private DateTime AV19TFContagemResultado_DataDmn_To ;
      private DateTime AV20TFContagemResultado_DataUltCnt ;
      private DateTime AV21TFContagemResultado_DataUltCnt_To ;
      private DateTime AV61ContagemResultado_DataDmn1 ;
      private DateTime AV62ContagemResultado_DataDmn_To1 ;
      private DateTime AV57ContagemResultado_DataCnt1 ;
      private DateTime AV58ContagemResultado_DataCnt_To1 ;
      private DateTime AV85ContagemResultado_DataDmn2 ;
      private DateTime AV86ContagemResultado_DataDmn_To2 ;
      private DateTime AV81ContagemResultado_DataCnt2 ;
      private DateTime AV82ContagemResultado_DataCnt_To2 ;
      private DateTime AV109ContagemResultado_DataDmn3 ;
      private DateTime AV110ContagemResultado_DataDmn_To3 ;
      private DateTime AV105ContagemResultado_DataCnt3 ;
      private DateTime AV106ContagemResultado_DataCnt_To3 ;
      private DateTime AV133ContagemResultado_DataDmn4 ;
      private DateTime AV134ContagemResultado_DataDmn_To4 ;
      private DateTime AV129ContagemResultado_DataCnt4 ;
      private DateTime AV130ContagemResultado_DataCnt_To4 ;
      private DateTime AV157ContagemResultado_DataDmn5 ;
      private DateTime AV158ContagemResultado_DataDmn_To5 ;
      private DateTime AV153ContagemResultado_DataCnt5 ;
      private DateTime AV154ContagemResultado_DataCnt_To5 ;
      private bool returnInSub ;
      private bool AV78DynamicFiltersEnabled2 ;
      private bool AV102DynamicFiltersEnabled3 ;
      private bool AV126DynamicFiltersEnabled4 ;
      private bool AV150DynamicFiltersEnabled5 ;
      private bool BRKU12 ;
      private bool n493ContagemResultado_DemandaFM ;
      private bool BRKU14 ;
      private bool n457ContagemResultado_Demanda ;
      private bool BRKU16 ;
      private bool n494ContagemResultado_Descricao ;
      private bool BRKU18 ;
      private bool n490ContagemResultado_ContratadaCod ;
      private bool n803ContagemResultado_ContratadaSigla ;
      private bool BRKU110 ;
      private bool n1553ContagemResultado_CntSrvCod ;
      private bool n601ContagemResultado_Servico ;
      private bool n801ContagemResultado_ServicoSigla ;
      private String AV46OptionIndexesJson ;
      private String AV41OptionsJson ;
      private String AV44OptionsDescJson ;
      private String AV26TFContagemResultado_StatusDmn_SelsJson ;
      private String AV37DDOName ;
      private String AV35SearchTxt ;
      private String AV36SearchTxtTo ;
      private String AV12TFContagemResultado_DemandaFM ;
      private String AV13TFContagemResultado_DemandaFM_Sel ;
      private String AV14TFContagemResultado_Demanda ;
      private String AV15TFContagemResultado_Demanda_Sel ;
      private String AV16TFContagemResultado_Descricao ;
      private String AV17TFContagemResultado_Descricao_Sel ;
      private String AV55DynamicFiltersSelector1 ;
      private String AV60ContagemResultado_OsFsOsFm1 ;
      private String AV75ContagemResultado_Descricao1 ;
      private String AV79DynamicFiltersSelector2 ;
      private String AV84ContagemResultado_OsFsOsFm2 ;
      private String AV99ContagemResultado_Descricao2 ;
      private String AV103DynamicFiltersSelector3 ;
      private String AV108ContagemResultado_OsFsOsFm3 ;
      private String AV123ContagemResultado_Descricao3 ;
      private String AV127DynamicFiltersSelector4 ;
      private String AV132ContagemResultado_OsFsOsFm4 ;
      private String AV147ContagemResultado_Descricao4 ;
      private String AV151DynamicFiltersSelector5 ;
      private String AV156ContagemResultado_OsFsOsFm5 ;
      private String AV171ContagemResultado_Descricao5 ;
      private String A493ContagemResultado_DemandaFM ;
      private String AV39Option ;
      private String A457ContagemResultado_Demanda ;
      private String AV42OptionDesc ;
      private String A494ContagemResultado_Descricao ;
      private IGxSession AV48Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private String[] P00U12_A493ContagemResultado_DemandaFM ;
      private bool[] P00U12_n493ContagemResultado_DemandaFM ;
      private int[] P00U12_A456ContagemResultado_Codigo ;
      private String[] P00U13_A457ContagemResultado_Demanda ;
      private bool[] P00U13_n457ContagemResultado_Demanda ;
      private int[] P00U13_A456ContagemResultado_Codigo ;
      private String[] P00U14_A494ContagemResultado_Descricao ;
      private bool[] P00U14_n494ContagemResultado_Descricao ;
      private int[] P00U14_A456ContagemResultado_Codigo ;
      private int[] P00U15_A490ContagemResultado_ContratadaCod ;
      private bool[] P00U15_n490ContagemResultado_ContratadaCod ;
      private String[] P00U15_A803ContagemResultado_ContratadaSigla ;
      private bool[] P00U15_n803ContagemResultado_ContratadaSigla ;
      private int[] P00U15_A456ContagemResultado_Codigo ;
      private int[] P00U16_A1553ContagemResultado_CntSrvCod ;
      private bool[] P00U16_n1553ContagemResultado_CntSrvCod ;
      private int[] P00U16_A601ContagemResultado_Servico ;
      private bool[] P00U16_n601ContagemResultado_Servico ;
      private String[] P00U16_A801ContagemResultado_ServicoSigla ;
      private bool[] P00U16_n801ContagemResultado_ServicoSigla ;
      private int[] P00U16_A456ContagemResultado_Codigo ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV27TFContagemResultado_StatusDmn_Sels ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV40Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV43OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV45OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV50GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV51GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV52GridStateDynamicFilter ;
   }

   public class getextrawwcontagemresultadoemgarantiafilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00U12 ;
          prmP00U12 = new Object[] {
          } ;
          Object[] prmP00U13 ;
          prmP00U13 = new Object[] {
          } ;
          Object[] prmP00U14 ;
          prmP00U14 = new Object[] {
          } ;
          Object[] prmP00U15 ;
          prmP00U15 = new Object[] {
          } ;
          Object[] prmP00U16 ;
          prmP00U16 = new Object[] {
          } ;
          def= new CursorDef[] {
              new CursorDef("P00U12", "SELECT [ContagemResultado_DemandaFM], [ContagemResultado_Codigo] FROM [ContagemResultado] WITH (NOLOCK) ORDER BY [ContagemResultado_DemandaFM] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00U12,100,0,true,false )
             ,new CursorDef("P00U13", "SELECT [ContagemResultado_Demanda], [ContagemResultado_Codigo] FROM [ContagemResultado] WITH (NOLOCK) ORDER BY [ContagemResultado_Demanda] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00U13,100,0,true,false )
             ,new CursorDef("P00U14", "SELECT [ContagemResultado_Descricao], [ContagemResultado_Codigo] FROM [ContagemResultado] WITH (NOLOCK) ORDER BY [ContagemResultado_Descricao] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00U14,100,0,true,false )
             ,new CursorDef("P00U15", "SELECT T1.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, T2.[Contratada_Sigla] AS ContagemResultado_ContratadaSigla, T1.[ContagemResultado_Codigo] FROM ([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [Contratada] T2 WITH (NOLOCK) ON T2.[Contratada_Codigo] = T1.[ContagemResultado_ContratadaCod]) ORDER BY T2.[Contratada_Sigla] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00U15,100,0,true,false )
             ,new CursorDef("P00U16", "SELECT T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T2.[Servico_Codigo] AS ContagemResultado_Servico, T3.[Servico_Sigla] AS ContagemResultado_ServicoSigla, T1.[ContagemResultado_Codigo] FROM (([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod]) LEFT JOIN [Servico] T3 WITH (NOLOCK) ON T3.[Servico_Codigo] = T2.[Servico_Codigo]) ORDER BY T3.[Servico_Sigla] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00U16,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                return;
             case 1 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                return;
             case 2 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 15) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getString(3, 15) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getextrawwcontagemresultadoemgarantiafilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getextrawwcontagemresultadoemgarantiafilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getextrawwcontagemresultadoemgarantiafilterdata") )
          {
             return  ;
          }
          getextrawwcontagemresultadoemgarantiafilterdata worker = new getextrawwcontagemresultadoemgarantiafilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
