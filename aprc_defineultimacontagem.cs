/*
               File: PRC_DefineUltimaContagem
        Description: Define Ultima Contagem
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/2/2020 16:11:31.73
       Program type: Main program
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class aprc_defineultimacontagem : GXWebProcedure, System.Web.SessionState.IRequiresSessionState
   {
      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize();
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
         }
         if ( GxWebError == 0 )
         {
            executePrivate();
         }
         cleanup();
      }

      public aprc_defineultimacontagem( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public aprc_defineultimacontagem( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         initialize();
         executePrivate();
      }

      public void executeSubmit( )
      {
         aprc_defineultimacontagem objaprc_defineultimacontagem;
         objaprc_defineultimacontagem = new aprc_defineultimacontagem();
         objaprc_defineultimacontagem.context.SetSubmitInitialConfig(context);
         objaprc_defineultimacontagem.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objaprc_defineultimacontagem);
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((aprc_defineultimacontagem)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P003H2 */
         pr_default.execute(0);
         while ( (pr_default.getStatus(0) != 101) )
         {
            A456ContagemResultado_Codigo = P003H2_A456ContagemResultado_Codigo[0];
            OV8ContagemResultado_HoraCnt = AV8ContagemResultado_HoraCnt;
            /* Using cursor P003H3 */
            pr_default.execute(1, new Object[] {A456ContagemResultado_Codigo});
            while ( (pr_default.getStatus(1) != 101) )
            {
               A517ContagemResultado_Ultima = P003H3_A517ContagemResultado_Ultima[0];
               A511ContagemResultado_HoraCnt = P003H3_A511ContagemResultado_HoraCnt[0];
               A473ContagemResultado_DataCnt = P003H3_A473ContagemResultado_DataCnt[0];
               AV10ContagemResultado_Codigo = A456ContagemResultado_Codigo;
               AV9ContagemResultado_DataCnt = A473ContagemResultado_DataCnt;
               AV8ContagemResultado_HoraCnt = A511ContagemResultado_HoraCnt;
               A517ContagemResultado_Ultima = false;
               /* Using cursor P003H4 */
               pr_default.execute(2, new Object[] {A517ContagemResultado_Ultima, A456ContagemResultado_Codigo, A473ContagemResultado_DataCnt, A511ContagemResultado_HoraCnt});
               pr_default.close(2);
               dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoContagens") ;
               pr_default.readNext(1);
            }
            pr_default.close(1);
            /* Execute user subroutine: 'ULTIMA' */
            S111 ();
            if ( returnInSub )
            {
               pr_default.close(0);
               this.cleanup();
               if (true) return;
            }
            pr_default.readNext(0);
         }
         pr_default.close(0);
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
         {
            context.Redirect( context.wjLoc );
            context.wjLoc = "";
         }
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'ULTIMA' Routine */
         if ( ! (0==AV10ContagemResultado_Codigo) )
         {
            /* Using cursor P003H5 */
            pr_default.execute(3, new Object[] {AV10ContagemResultado_Codigo, AV9ContagemResultado_DataCnt, AV8ContagemResultado_HoraCnt});
            while ( (pr_default.getStatus(3) != 101) )
            {
               A511ContagemResultado_HoraCnt = P003H5_A511ContagemResultado_HoraCnt[0];
               A473ContagemResultado_DataCnt = P003H5_A473ContagemResultado_DataCnt[0];
               A456ContagemResultado_Codigo = P003H5_A456ContagemResultado_Codigo[0];
               A517ContagemResultado_Ultima = P003H5_A517ContagemResultado_Ultima[0];
               A517ContagemResultado_Ultima = true;
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               /* Using cursor P003H6 */
               pr_default.execute(4, new Object[] {A517ContagemResultado_Ultima, A456ContagemResultado_Codigo, A473ContagemResultado_DataCnt, A511ContagemResultado_HoraCnt});
               pr_default.close(4);
               dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoContagens") ;
               if (true) break;
               /* Using cursor P003H7 */
               pr_default.execute(5, new Object[] {A517ContagemResultado_Ultima, A456ContagemResultado_Codigo, A473ContagemResultado_DataCnt, A511ContagemResultado_HoraCnt});
               pr_default.close(5);
               dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoContagens") ;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(3);
         }
      }

      public override void cleanup( )
      {
         context.CommitDataStores( "PRC_DefineUltimaContagem");
         CloseOpenCursors();
         base.cleanup();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         GXKey = "";
         gxfirstwebparm = "";
         scmdbuf = "";
         P003H2_A456ContagemResultado_Codigo = new int[1] ;
         OV8ContagemResultado_HoraCnt = "";
         AV8ContagemResultado_HoraCnt = context.localUtil.Time( );
         P003H3_A456ContagemResultado_Codigo = new int[1] ;
         P003H3_A517ContagemResultado_Ultima = new bool[] {false} ;
         P003H3_A511ContagemResultado_HoraCnt = new String[] {""} ;
         P003H3_A473ContagemResultado_DataCnt = new DateTime[] {DateTime.MinValue} ;
         A511ContagemResultado_HoraCnt = "";
         A473ContagemResultado_DataCnt = DateTime.MinValue;
         AV9ContagemResultado_DataCnt = DateTime.MinValue;
         P003H5_A511ContagemResultado_HoraCnt = new String[] {""} ;
         P003H5_A473ContagemResultado_DataCnt = new DateTime[] {DateTime.MinValue} ;
         P003H5_A456ContagemResultado_Codigo = new int[1] ;
         P003H5_A517ContagemResultado_Ultima = new bool[] {false} ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.aprc_defineultimacontagem__default(),
            new Object[][] {
                new Object[] {
               P003H2_A456ContagemResultado_Codigo
               }
               , new Object[] {
               P003H3_A456ContagemResultado_Codigo, P003H3_A517ContagemResultado_Ultima, P003H3_A511ContagemResultado_HoraCnt, P003H3_A473ContagemResultado_DataCnt
               }
               , new Object[] {
               }
               , new Object[] {
               P003H5_A511ContagemResultado_HoraCnt, P003H5_A473ContagemResultado_DataCnt, P003H5_A456ContagemResultado_Codigo, P003H5_A517ContagemResultado_Ultima
               }
               , new Object[] {
               }
               , new Object[] {
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short gxcookieaux ;
      private short nGotPars ;
      private short GxWebError ;
      private int A456ContagemResultado_Codigo ;
      private int AV10ContagemResultado_Codigo ;
      private String GXKey ;
      private String gxfirstwebparm ;
      private String scmdbuf ;
      private String OV8ContagemResultado_HoraCnt ;
      private String AV8ContagemResultado_HoraCnt ;
      private String A511ContagemResultado_HoraCnt ;
      private DateTime A473ContagemResultado_DataCnt ;
      private DateTime AV9ContagemResultado_DataCnt ;
      private bool entryPointCalled ;
      private bool A517ContagemResultado_Ultima ;
      private bool returnInSub ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P003H2_A456ContagemResultado_Codigo ;
      private int[] P003H3_A456ContagemResultado_Codigo ;
      private bool[] P003H3_A517ContagemResultado_Ultima ;
      private String[] P003H3_A511ContagemResultado_HoraCnt ;
      private DateTime[] P003H3_A473ContagemResultado_DataCnt ;
      private String[] P003H5_A511ContagemResultado_HoraCnt ;
      private DateTime[] P003H5_A473ContagemResultado_DataCnt ;
      private int[] P003H5_A456ContagemResultado_Codigo ;
      private bool[] P003H5_A517ContagemResultado_Ultima ;
   }

   public class aprc_defineultimacontagem__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new UpdateCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new UpdateCursor(def[4])
         ,new UpdateCursor(def[5])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP003H2 ;
          prmP003H2 = new Object[] {
          } ;
          Object[] prmP003H3 ;
          prmP003H3 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP003H4 ;
          prmP003H4 = new Object[] {
          new Object[] {"@ContagemResultado_Ultima",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_DataCnt",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_HoraCnt",SqlDbType.Char,5,0}
          } ;
          Object[] prmP003H5 ;
          prmP003H5 = new Object[] {
          new Object[] {"@AV10ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV9ContagemResultado_DataCnt",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV8ContagemResultado_HoraCnt",SqlDbType.Char,5,0}
          } ;
          Object[] prmP003H6 ;
          prmP003H6 = new Object[] {
          new Object[] {"@ContagemResultado_Ultima",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_DataCnt",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_HoraCnt",SqlDbType.Char,5,0}
          } ;
          Object[] prmP003H7 ;
          prmP003H7 = new Object[] {
          new Object[] {"@ContagemResultado_Ultima",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_DataCnt",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_HoraCnt",SqlDbType.Char,5,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P003H2", "SELECT [ContagemResultado_Codigo] FROM [ContagemResultado] WITH (NOLOCK) ORDER BY [ContagemResultado_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP003H2,100,0,true,false )
             ,new CursorDef("P003H3", "SELECT [ContagemResultado_Codigo], [ContagemResultado_Ultima], [ContagemResultado_HoraCnt], [ContagemResultado_DataCnt] FROM [ContagemResultadoContagens] WITH (UPDLOCK) WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo ORDER BY [ContagemResultado_Codigo], [ContagemResultado_DataCnt], [ContagemResultado_HoraCnt] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP003H3,1,0,true,false )
             ,new CursorDef("P003H4", "UPDATE [ContagemResultadoContagens] SET [ContagemResultado_Ultima]=@ContagemResultado_Ultima  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo AND [ContagemResultado_DataCnt] = @ContagemResultado_DataCnt AND [ContagemResultado_HoraCnt] = @ContagemResultado_HoraCnt", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP003H4)
             ,new CursorDef("P003H5", "SELECT TOP 1 [ContagemResultado_HoraCnt], [ContagemResultado_DataCnt], [ContagemResultado_Codigo], [ContagemResultado_Ultima] FROM [ContagemResultadoContagens] WITH (UPDLOCK) WHERE [ContagemResultado_Codigo] = @AV10ContagemResultado_Codigo and [ContagemResultado_DataCnt] = @AV9ContagemResultado_DataCnt and [ContagemResultado_HoraCnt] = @AV8ContagemResultado_HoraCnt ORDER BY [ContagemResultado_Codigo], [ContagemResultado_DataCnt], [ContagemResultado_HoraCnt] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP003H5,1,0,true,true )
             ,new CursorDef("P003H6", "UPDATE [ContagemResultadoContagens] SET [ContagemResultado_Ultima]=@ContagemResultado_Ultima  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo AND [ContagemResultado_DataCnt] = @ContagemResultado_DataCnt AND [ContagemResultado_HoraCnt] = @ContagemResultado_HoraCnt", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP003H6)
             ,new CursorDef("P003H7", "UPDATE [ContagemResultadoContagens] SET [ContagemResultado_Ultima]=@ContagemResultado_Ultima  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo AND [ContagemResultado_DataCnt] = @ContagemResultado_DataCnt AND [ContagemResultado_HoraCnt] = @ContagemResultado_HoraCnt", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP003H7)
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.getBool(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 5) ;
                ((DateTime[]) buf[3])[0] = rslt.getGXDate(4) ;
                return;
             case 3 :
                ((String[]) buf[0])[0] = rslt.getString(1, 5) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDate(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.getBool(4) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (bool)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (DateTime)parms[2]);
                stmt.SetParameter(4, (String)parms[3]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (DateTime)parms[1]);
                stmt.SetParameter(3, (String)parms[2]);
                return;
             case 4 :
                stmt.SetParameter(1, (bool)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (DateTime)parms[2]);
                stmt.SetParameter(4, (String)parms[3]);
                return;
             case 5 :
                stmt.SetParameter(1, (bool)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (DateTime)parms[2]);
                stmt.SetParameter(4, (String)parms[3]);
                return;
       }
    }

 }

}
