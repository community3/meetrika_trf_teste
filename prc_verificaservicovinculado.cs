/*
               File: PRC_VerificaServicoVinculado
        Description: Verifica se o Servi�o da Demanda selecionada possui Regra
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/2/2020 16:14:36.5
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_verificaservicovinculado : GXProcedure
   {
      public prc_verificaservicovinculado( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_verificaservicovinculado( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( IGxCollection aP0_ContagemResultado_Codigo ,
                           out bool aP1_IsServicoVinculado )
      {
         this.AV17ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         this.AV66IsServicoVinculado = false ;
         initialize();
         executePrivate();
         aP1_IsServicoVinculado=this.AV66IsServicoVinculado;
      }

      public bool executeUdp( IGxCollection aP0_ContagemResultado_Codigo )
      {
         this.AV17ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         this.AV66IsServicoVinculado = false ;
         initialize();
         executePrivate();
         aP1_IsServicoVinculado=this.AV66IsServicoVinculado;
         return AV66IsServicoVinculado ;
      }

      public void executeSubmit( IGxCollection aP0_ContagemResultado_Codigo ,
                                 out bool aP1_IsServicoVinculado )
      {
         prc_verificaservicovinculado objprc_verificaservicovinculado;
         objprc_verificaservicovinculado = new prc_verificaservicovinculado();
         objprc_verificaservicovinculado.AV17ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         objprc_verificaservicovinculado.AV66IsServicoVinculado = false ;
         objprc_verificaservicovinculado.context.SetSubmitInitialConfig(context);
         objprc_verificaservicovinculado.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_verificaservicovinculado);
         aP1_IsServicoVinculado=this.AV66IsServicoVinculado;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_verificaservicovinculado)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV66IsServicoVinculado = false;
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              A456ContagemResultado_Codigo ,
                                              AV17ContagemResultado_Codigo },
                                              new int[] {
                                              TypeConstants.INT
                                              }
         });
         /* Using cursor P00Y42 */
         pr_default.execute(0);
         while ( (pr_default.getStatus(0) != 101) )
         {
            A1553ContagemResultado_CntSrvCod = P00Y42_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = P00Y42_n1553ContagemResultado_CntSrvCod[0];
            A456ContagemResultado_Codigo = P00Y42_A456ContagemResultado_Codigo[0];
            /* Using cursor P00Y43 */
            pr_default.execute(1, new Object[] {n1553ContagemResultado_CntSrvCod, A1553ContagemResultado_CntSrvCod});
            while ( (pr_default.getStatus(1) != 101) )
            {
               A915ContratoSrvVnc_CntSrvCod = P00Y43_A915ContratoSrvVnc_CntSrvCod[0];
               A1453ContratoServicosVnc_Ativo = P00Y43_A1453ContratoServicosVnc_Ativo[0];
               n1453ContratoServicosVnc_Ativo = P00Y43_n1453ContratoServicosVnc_Ativo[0];
               A917ContratoSrvVnc_Codigo = P00Y43_A917ContratoSrvVnc_Codigo[0];
               AV66IsServicoVinculado = true;
               pr_default.readNext(1);
            }
            pr_default.close(1);
            pr_default.readNext(0);
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00Y42_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P00Y42_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P00Y42_A456ContagemResultado_Codigo = new int[1] ;
         P00Y43_A915ContratoSrvVnc_CntSrvCod = new int[1] ;
         P00Y43_A1453ContratoServicosVnc_Ativo = new bool[] {false} ;
         P00Y43_n1453ContratoServicosVnc_Ativo = new bool[] {false} ;
         P00Y43_A917ContratoSrvVnc_Codigo = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_verificaservicovinculado__default(),
            new Object[][] {
                new Object[] {
               P00Y42_A1553ContagemResultado_CntSrvCod, P00Y42_n1553ContagemResultado_CntSrvCod, P00Y42_A456ContagemResultado_Codigo
               }
               , new Object[] {
               P00Y43_A915ContratoSrvVnc_CntSrvCod, P00Y43_A1453ContratoServicosVnc_Ativo, P00Y43_n1453ContratoServicosVnc_Ativo, P00Y43_A917ContratoSrvVnc_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int A456ContagemResultado_Codigo ;
      private int A1553ContagemResultado_CntSrvCod ;
      private int A915ContratoSrvVnc_CntSrvCod ;
      private int A917ContratoSrvVnc_Codigo ;
      private String scmdbuf ;
      private bool AV66IsServicoVinculado ;
      private bool n1553ContagemResultado_CntSrvCod ;
      private bool A1453ContratoServicosVnc_Ativo ;
      private bool n1453ContratoServicosVnc_Ativo ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00Y42_A1553ContagemResultado_CntSrvCod ;
      private bool[] P00Y42_n1553ContagemResultado_CntSrvCod ;
      private int[] P00Y42_A456ContagemResultado_Codigo ;
      private int[] P00Y43_A915ContratoSrvVnc_CntSrvCod ;
      private bool[] P00Y43_A1453ContratoServicosVnc_Ativo ;
      private bool[] P00Y43_n1453ContratoServicosVnc_Ativo ;
      private int[] P00Y43_A917ContratoSrvVnc_Codigo ;
      private bool aP1_IsServicoVinculado ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV17ContagemResultado_Codigo ;
   }

   public class prc_verificaservicovinculado__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00Y42( IGxContext context ,
                                             int A456ContagemResultado_Codigo ,
                                             IGxCollection AV17ContagemResultado_Codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         Object[] GXv_Object1 ;
         GXv_Object1 = new Object [2] ;
         scmdbuf = "SELECT [ContagemResultado_CntSrvCod], [ContagemResultado_Codigo] FROM [ContagemResultado] WITH (NOLOCK)";
         scmdbuf = scmdbuf + " WHERE (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV17ContagemResultado_Codigo, "[ContagemResultado_Codigo] IN (", ")") + ")";
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY [ContagemResultado_Codigo]";
         GXv_Object1[0] = scmdbuf;
         return GXv_Object1 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00Y42(context, (int)dynConstraints[0] , (IGxCollection)dynConstraints[1] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00Y43 ;
          prmP00Y43 = new Object[] {
          new Object[] {"@ContagemResultado_CntSrvCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00Y42 ;
          prmP00Y42 = new Object[] {
          } ;
          def= new CursorDef[] {
              new CursorDef("P00Y42", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00Y42,100,0,true,false )
             ,new CursorDef("P00Y43", "SELECT [ContratoSrvVnc_CntSrvCod], [ContratoServicosVnc_Ativo], [ContratoSrvVnc_Codigo] FROM [ContratoServicosVnc] WITH (NOLOCK) WHERE ([ContratoSrvVnc_CntSrvCod] = @ContagemResultado_CntSrvCod) AND ([ContratoServicosVnc_Ativo] = 1) ORDER BY [ContratoSrvVnc_CntSrvCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00Y43,100,0,false,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.getBool(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 1 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
       }
    }

 }

}
