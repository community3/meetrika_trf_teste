/*
               File: WWProjetoDesenvolvimento
        Description:  Projeto Desenvolvimento
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 23:24:25.56
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wwprojetodesenvolvimento : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wwprojetodesenvolvimento( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wwprojetodesenvolvimento( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavOrderedby = new GXCombobox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
            {
               nRC_GXsfl_35 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_35_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_35_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
            {
               subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV12OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12OrderedBy), 4, 0)));
               AV13OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedDsc", AV13OrderedDsc);
               AV21TFProjetoDesenvolvimento_ProjetoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21TFProjetoDesenvolvimento_ProjetoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21TFProjetoDesenvolvimento_ProjetoCod), 6, 0)));
               AV22TFProjetoDesenvolvimento_ProjetoCod_To = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22TFProjetoDesenvolvimento_ProjetoCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22TFProjetoDesenvolvimento_ProjetoCod_To), 6, 0)));
               AV25TFProjetoDesenvolvimento_SistemaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25TFProjetoDesenvolvimento_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV25TFProjetoDesenvolvimento_SistemaCod), 6, 0)));
               AV26TFProjetoDesenvolvimento_SistemaCod_To = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26TFProjetoDesenvolvimento_SistemaCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26TFProjetoDesenvolvimento_SistemaCod_To), 6, 0)));
               AV23ddo_ProjetoDesenvolvimento_ProjetoCodTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23ddo_ProjetoDesenvolvimento_ProjetoCodTitleControlIdToReplace", AV23ddo_ProjetoDesenvolvimento_ProjetoCodTitleControlIdToReplace);
               AV27ddo_ProjetoDesenvolvimento_SistemaCodTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27ddo_ProjetoDesenvolvimento_SistemaCodTitleControlIdToReplace", AV27ddo_ProjetoDesenvolvimento_SistemaCodTitleControlIdToReplace);
               AV40Pgmname = GetNextPar( );
               A669ProjetoDesenvolvimento_ProjetoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               A670ProjetoDesenvolvimento_SistemaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV17Sistema_Tipo = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Sistema_Tipo", AV17Sistema_Tipo);
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid_refresh( subGrid_Rows, AV12OrderedBy, AV13OrderedDsc, AV21TFProjetoDesenvolvimento_ProjetoCod, AV22TFProjetoDesenvolvimento_ProjetoCod_To, AV25TFProjetoDesenvolvimento_SistemaCod, AV26TFProjetoDesenvolvimento_SistemaCod_To, AV23ddo_ProjetoDesenvolvimento_ProjetoCodTitleControlIdToReplace, AV27ddo_ProjetoDesenvolvimento_SistemaCodTitleControlIdToReplace, AV40Pgmname, A669ProjetoDesenvolvimento_ProjetoCod, A670ProjetoDesenvolvimento_SistemaCod, AV17Sistema_Tipo) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("workwithplusbootstrapmasterpage", "GeneXus.Programs.workwithplusbootstrapmasterpage", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PADL2( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            STARTDL2( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?202042823242567");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wwprojetodesenvolvimento.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV12OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDDSC", StringUtil.BoolToStr( AV13OrderedDsc));
         GxWebStd.gx_hidden_field( context, "GXH_vTFPROJETODESENVOLVIMENTO_PROJETOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV21TFProjetoDesenvolvimento_ProjetoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFPROJETODESENVOLVIMENTO_PROJETOCOD_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV22TFProjetoDesenvolvimento_ProjetoCod_To), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFPROJETODESENVOLVIMENTO_SISTEMACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV25TFProjetoDesenvolvimento_SistemaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFPROJETODESENVOLVIMENTO_SISTEMACOD_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV26TFProjetoDesenvolvimento_SistemaCod_To), 6, 0, ",", "")));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_35", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_35), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV30GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV31GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vDDO_TITLESETTINGSICONS", AV28DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vDDO_TITLESETTINGSICONS", AV28DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vPROJETODESENVOLVIMENTO_PROJETOCODTITLEFILTERDATA", AV20ProjetoDesenvolvimento_ProjetoCodTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vPROJETODESENVOLVIMENTO_PROJETOCODTITLEFILTERDATA", AV20ProjetoDesenvolvimento_ProjetoCodTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vPROJETODESENVOLVIMENTO_SISTEMACODTITLEFILTERDATA", AV24ProjetoDesenvolvimento_SistemaCodTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vPROJETODESENVOLVIMENTO_SISTEMACODTITLEFILTERDATA", AV24ProjetoDesenvolvimento_SistemaCodTitleFilterData);
         }
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV40Pgmname));
         GxWebStd.gx_hidden_field( context, "vSISTEMA_TIPO", StringUtil.RTrim( AV17Sistema_Tipo));
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETODESENVOLVIMENTO_PROJETOCOD_Caption", StringUtil.RTrim( Ddo_projetodesenvolvimento_projetocod_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETODESENVOLVIMENTO_PROJETOCOD_Tooltip", StringUtil.RTrim( Ddo_projetodesenvolvimento_projetocod_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETODESENVOLVIMENTO_PROJETOCOD_Cls", StringUtil.RTrim( Ddo_projetodesenvolvimento_projetocod_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETODESENVOLVIMENTO_PROJETOCOD_Filteredtext_set", StringUtil.RTrim( Ddo_projetodesenvolvimento_projetocod_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETODESENVOLVIMENTO_PROJETOCOD_Filteredtextto_set", StringUtil.RTrim( Ddo_projetodesenvolvimento_projetocod_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETODESENVOLVIMENTO_PROJETOCOD_Dropdownoptionstype", StringUtil.RTrim( Ddo_projetodesenvolvimento_projetocod_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETODESENVOLVIMENTO_PROJETOCOD_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_projetodesenvolvimento_projetocod_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETODESENVOLVIMENTO_PROJETOCOD_Includesortasc", StringUtil.BoolToStr( Ddo_projetodesenvolvimento_projetocod_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETODESENVOLVIMENTO_PROJETOCOD_Includesortdsc", StringUtil.BoolToStr( Ddo_projetodesenvolvimento_projetocod_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETODESENVOLVIMENTO_PROJETOCOD_Sortedstatus", StringUtil.RTrim( Ddo_projetodesenvolvimento_projetocod_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETODESENVOLVIMENTO_PROJETOCOD_Includefilter", StringUtil.BoolToStr( Ddo_projetodesenvolvimento_projetocod_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETODESENVOLVIMENTO_PROJETOCOD_Filtertype", StringUtil.RTrim( Ddo_projetodesenvolvimento_projetocod_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETODESENVOLVIMENTO_PROJETOCOD_Filterisrange", StringUtil.BoolToStr( Ddo_projetodesenvolvimento_projetocod_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETODESENVOLVIMENTO_PROJETOCOD_Includedatalist", StringUtil.BoolToStr( Ddo_projetodesenvolvimento_projetocod_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETODESENVOLVIMENTO_PROJETOCOD_Datalistfixedvalues", StringUtil.RTrim( Ddo_projetodesenvolvimento_projetocod_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETODESENVOLVIMENTO_PROJETOCOD_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_projetodesenvolvimento_projetocod_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETODESENVOLVIMENTO_PROJETOCOD_Sortasc", StringUtil.RTrim( Ddo_projetodesenvolvimento_projetocod_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETODESENVOLVIMENTO_PROJETOCOD_Sortdsc", StringUtil.RTrim( Ddo_projetodesenvolvimento_projetocod_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETODESENVOLVIMENTO_PROJETOCOD_Loadingdata", StringUtil.RTrim( Ddo_projetodesenvolvimento_projetocod_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETODESENVOLVIMENTO_PROJETOCOD_Cleanfilter", StringUtil.RTrim( Ddo_projetodesenvolvimento_projetocod_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETODESENVOLVIMENTO_PROJETOCOD_Rangefilterfrom", StringUtil.RTrim( Ddo_projetodesenvolvimento_projetocod_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETODESENVOLVIMENTO_PROJETOCOD_Rangefilterto", StringUtil.RTrim( Ddo_projetodesenvolvimento_projetocod_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETODESENVOLVIMENTO_PROJETOCOD_Noresultsfound", StringUtil.RTrim( Ddo_projetodesenvolvimento_projetocod_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETODESENVOLVIMENTO_PROJETOCOD_Searchbuttontext", StringUtil.RTrim( Ddo_projetodesenvolvimento_projetocod_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETODESENVOLVIMENTO_SISTEMACOD_Caption", StringUtil.RTrim( Ddo_projetodesenvolvimento_sistemacod_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETODESENVOLVIMENTO_SISTEMACOD_Tooltip", StringUtil.RTrim( Ddo_projetodesenvolvimento_sistemacod_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETODESENVOLVIMENTO_SISTEMACOD_Cls", StringUtil.RTrim( Ddo_projetodesenvolvimento_sistemacod_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETODESENVOLVIMENTO_SISTEMACOD_Filteredtext_set", StringUtil.RTrim( Ddo_projetodesenvolvimento_sistemacod_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETODESENVOLVIMENTO_SISTEMACOD_Filteredtextto_set", StringUtil.RTrim( Ddo_projetodesenvolvimento_sistemacod_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETODESENVOLVIMENTO_SISTEMACOD_Dropdownoptionstype", StringUtil.RTrim( Ddo_projetodesenvolvimento_sistemacod_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETODESENVOLVIMENTO_SISTEMACOD_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_projetodesenvolvimento_sistemacod_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETODESENVOLVIMENTO_SISTEMACOD_Includesortasc", StringUtil.BoolToStr( Ddo_projetodesenvolvimento_sistemacod_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETODESENVOLVIMENTO_SISTEMACOD_Includesortdsc", StringUtil.BoolToStr( Ddo_projetodesenvolvimento_sistemacod_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETODESENVOLVIMENTO_SISTEMACOD_Sortedstatus", StringUtil.RTrim( Ddo_projetodesenvolvimento_sistemacod_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETODESENVOLVIMENTO_SISTEMACOD_Includefilter", StringUtil.BoolToStr( Ddo_projetodesenvolvimento_sistemacod_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETODESENVOLVIMENTO_SISTEMACOD_Filtertype", StringUtil.RTrim( Ddo_projetodesenvolvimento_sistemacod_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETODESENVOLVIMENTO_SISTEMACOD_Filterisrange", StringUtil.BoolToStr( Ddo_projetodesenvolvimento_sistemacod_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETODESENVOLVIMENTO_SISTEMACOD_Includedatalist", StringUtil.BoolToStr( Ddo_projetodesenvolvimento_sistemacod_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETODESENVOLVIMENTO_SISTEMACOD_Datalistfixedvalues", StringUtil.RTrim( Ddo_projetodesenvolvimento_sistemacod_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETODESENVOLVIMENTO_SISTEMACOD_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_projetodesenvolvimento_sistemacod_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETODESENVOLVIMENTO_SISTEMACOD_Sortasc", StringUtil.RTrim( Ddo_projetodesenvolvimento_sistemacod_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETODESENVOLVIMENTO_SISTEMACOD_Sortdsc", StringUtil.RTrim( Ddo_projetodesenvolvimento_sistemacod_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETODESENVOLVIMENTO_SISTEMACOD_Loadingdata", StringUtil.RTrim( Ddo_projetodesenvolvimento_sistemacod_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETODESENVOLVIMENTO_SISTEMACOD_Cleanfilter", StringUtil.RTrim( Ddo_projetodesenvolvimento_sistemacod_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETODESENVOLVIMENTO_SISTEMACOD_Rangefilterfrom", StringUtil.RTrim( Ddo_projetodesenvolvimento_sistemacod_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETODESENVOLVIMENTO_SISTEMACOD_Rangefilterto", StringUtil.RTrim( Ddo_projetodesenvolvimento_sistemacod_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETODESENVOLVIMENTO_SISTEMACOD_Noresultsfound", StringUtil.RTrim( Ddo_projetodesenvolvimento_sistemacod_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETODESENVOLVIMENTO_SISTEMACOD_Searchbuttontext", StringUtil.RTrim( Ddo_projetodesenvolvimento_sistemacod_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETODESENVOLVIMENTO_PROJETOCOD_Activeeventkey", StringUtil.RTrim( Ddo_projetodesenvolvimento_projetocod_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETODESENVOLVIMENTO_PROJETOCOD_Filteredtext_get", StringUtil.RTrim( Ddo_projetodesenvolvimento_projetocod_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETODESENVOLVIMENTO_PROJETOCOD_Filteredtextto_get", StringUtil.RTrim( Ddo_projetodesenvolvimento_projetocod_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETODESENVOLVIMENTO_SISTEMACOD_Activeeventkey", StringUtil.RTrim( Ddo_projetodesenvolvimento_sistemacod_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETODESENVOLVIMENTO_SISTEMACOD_Filteredtext_get", StringUtil.RTrim( Ddo_projetodesenvolvimento_sistemacod_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_PROJETODESENVOLVIMENTO_SISTEMACOD_Filteredtextto_get", StringUtil.RTrim( Ddo_projetodesenvolvimento_sistemacod_Filteredtextto_get));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WEDL2( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVTDL2( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return false ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wwprojetodesenvolvimento.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "WWProjetoDesenvolvimento" ;
      }

      public override String GetPgmdesc( )
      {
         return " Projeto Desenvolvimento" ;
      }

      protected void WBDL0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            wb_table1_2_DL2( true) ;
         }
         else
         {
            wb_table1_2_DL2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_DL2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 44,'',false,'" + sGXsfl_35_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfprojetodesenvolvimento_projetocod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV21TFProjetoDesenvolvimento_ProjetoCod), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV21TFProjetoDesenvolvimento_ProjetoCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,44);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfprojetodesenvolvimento_projetocod_Jsonclick, 0, "Attribute", "", "", "", edtavTfprojetodesenvolvimento_projetocod_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWProjetoDesenvolvimento.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 45,'',false,'" + sGXsfl_35_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfprojetodesenvolvimento_projetocod_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV22TFProjetoDesenvolvimento_ProjetoCod_To), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV22TFProjetoDesenvolvimento_ProjetoCod_To), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,45);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfprojetodesenvolvimento_projetocod_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfprojetodesenvolvimento_projetocod_to_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWProjetoDesenvolvimento.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 46,'',false,'" + sGXsfl_35_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfprojetodesenvolvimento_sistemacod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV25TFProjetoDesenvolvimento_SistemaCod), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV25TFProjetoDesenvolvimento_SistemaCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,46);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfprojetodesenvolvimento_sistemacod_Jsonclick, 0, "Attribute", "", "", "", edtavTfprojetodesenvolvimento_sistemacod_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWProjetoDesenvolvimento.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 47,'',false,'" + sGXsfl_35_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfprojetodesenvolvimento_sistemacod_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV26TFProjetoDesenvolvimento_SistemaCod_To), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV26TFProjetoDesenvolvimento_SistemaCod_To), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,47);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfprojetodesenvolvimento_sistemacod_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfprojetodesenvolvimento_sistemacod_to_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWProjetoDesenvolvimento.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_PROJETODESENVOLVIMENTO_PROJETOCODContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 49,'',false,'" + sGXsfl_35_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_projetodesenvolvimento_projetocodtitlecontrolidtoreplace_Internalname, AV23ddo_ProjetoDesenvolvimento_ProjetoCodTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,49);\"", 0, edtavDdo_projetodesenvolvimento_projetocodtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWProjetoDesenvolvimento.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_PROJETODESENVOLVIMENTO_SISTEMACODContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 51,'',false,'" + sGXsfl_35_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_projetodesenvolvimento_sistemacodtitlecontrolidtoreplace_Internalname, AV27ddo_ProjetoDesenvolvimento_SistemaCodTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,51);\"", 0, edtavDdo_projetodesenvolvimento_sistemacodtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWProjetoDesenvolvimento.htm");
         }
         wbLoad = true;
      }

      protected void STARTDL2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", " Projeto Desenvolvimento", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPDL0( ) ;
      }

      protected void WSDL2( )
      {
         STARTDL2( ) ;
         EVTDL2( ) ;
      }

      protected void EVTDL2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E11DL2 */
                              E11DL2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_PROJETODESENVOLVIMENTO_PROJETOCOD.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E12DL2 */
                              E12DL2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_PROJETODESENVOLVIMENTO_SISTEMACOD.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E13DL2 */
                              E13DL2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VORDEREDBY.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E14DL2 */
                              E14DL2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOCLEANFILTERS'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E15DL2 */
                              E15DL2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOINSERT'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E16DL2 */
                              E16DL2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              nGXsfl_35_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_35_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_35_idx), 4, 0)), 4, "0");
                              SubsflControlProps_352( ) ;
                              AV14Update = cgiGet( edtavUpdate_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUpdate_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV14Update)) ? AV38Update_GXI : context.convertURL( context.PathToRelativeUrl( AV14Update))));
                              AV15Delete = cgiGet( edtavDelete_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDelete_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV15Delete)) ? AV39Delete_GXI : context.convertURL( context.PathToRelativeUrl( AV15Delete))));
                              A669ProjetoDesenvolvimento_ProjetoCod = (int)(context.localUtil.CToN( cgiGet( edtProjetoDesenvolvimento_ProjetoCod_Internalname), ",", "."));
                              A670ProjetoDesenvolvimento_SistemaCod = (int)(context.localUtil.CToN( cgiGet( edtProjetoDesenvolvimento_SistemaCod_Internalname), ",", "."));
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E17DL2 */
                                    E17DL2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E18DL2 */
                                    E18DL2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E19DL2 */
                                    E19DL2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       /* Set Refresh If Orderedby Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV12OrderedBy )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Ordereddsc Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV13OrderedDsc )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfprojetodesenvolvimento_projetocod Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFPROJETODESENVOLVIMENTO_PROJETOCOD"), ",", ".") != Convert.ToDecimal( AV21TFProjetoDesenvolvimento_ProjetoCod )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfprojetodesenvolvimento_projetocod_to Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFPROJETODESENVOLVIMENTO_PROJETOCOD_TO"), ",", ".") != Convert.ToDecimal( AV22TFProjetoDesenvolvimento_ProjetoCod_To )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfprojetodesenvolvimento_sistemacod Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFPROJETODESENVOLVIMENTO_SISTEMACOD"), ",", ".") != Convert.ToDecimal( AV25TFProjetoDesenvolvimento_SistemaCod )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfprojetodesenvolvimento_sistemacod_to Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFPROJETODESENVOLVIMENTO_SISTEMACOD_TO"), ",", ".") != Convert.ToDecimal( AV26TFProjetoDesenvolvimento_SistemaCod_To )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEDL2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PADL2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbavOrderedby.Name = "vORDEREDBY";
            cmbavOrderedby.WebTags = "";
            if ( cmbavOrderedby.ItemCount > 0 )
            {
               AV12OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV12OrderedBy), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12OrderedBy), 4, 0)));
            }
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_352( ) ;
         while ( nGXsfl_35_idx <= nRC_GXsfl_35 )
         {
            sendrow_352( ) ;
            nGXsfl_35_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_35_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_35_idx+1));
            sGXsfl_35_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_35_idx), 4, 0)), 4, "0");
            SubsflControlProps_352( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV12OrderedBy ,
                                       bool AV13OrderedDsc ,
                                       int AV21TFProjetoDesenvolvimento_ProjetoCod ,
                                       int AV22TFProjetoDesenvolvimento_ProjetoCod_To ,
                                       int AV25TFProjetoDesenvolvimento_SistemaCod ,
                                       int AV26TFProjetoDesenvolvimento_SistemaCod_To ,
                                       String AV23ddo_ProjetoDesenvolvimento_ProjetoCodTitleControlIdToReplace ,
                                       String AV27ddo_ProjetoDesenvolvimento_SistemaCodTitleControlIdToReplace ,
                                       String AV40Pgmname ,
                                       int A669ProjetoDesenvolvimento_ProjetoCod ,
                                       int A670ProjetoDesenvolvimento_SistemaCod ,
                                       String AV17Sistema_Tipo )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RFDL2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_PROJETODESENVOLVIMENTO_PROJETOCOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A669ProjetoDesenvolvimento_ProjetoCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "PROJETODESENVOLVIMENTO_PROJETOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A669ProjetoDesenvolvimento_ProjetoCod), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_PROJETODESENVOLVIMENTO_SISTEMACOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A670ProjetoDesenvolvimento_SistemaCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "PROJETODESENVOLVIMENTO_SISTEMACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A670ProjetoDesenvolvimento_SistemaCod), 6, 0, ".", "")));
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavOrderedby.ItemCount > 0 )
         {
            AV12OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV12OrderedBy), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12OrderedBy), 4, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFDL2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV40Pgmname = "WWProjetoDesenvolvimento";
         context.Gx_err = 0;
      }

      protected void RFDL2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 35;
         /* Execute user event: E18DL2 */
         E18DL2 ();
         nGXsfl_35_idx = 1;
         sGXsfl_35_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_35_idx), 4, 0)), 4, "0");
         SubsflControlProps_352( ) ;
         nGXsfl_35_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", "");
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_352( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 AV34WWProjetoDesenvolvimentoDS_1_Tfprojetodesenvolvimento_projetocod ,
                                                 AV35WWProjetoDesenvolvimentoDS_2_Tfprojetodesenvolvimento_projetocod_to ,
                                                 AV36WWProjetoDesenvolvimentoDS_3_Tfprojetodesenvolvimento_sistemacod ,
                                                 AV37WWProjetoDesenvolvimentoDS_4_Tfprojetodesenvolvimento_sistemacod_to ,
                                                 A669ProjetoDesenvolvimento_ProjetoCod ,
                                                 A670ProjetoDesenvolvimento_SistemaCod ,
                                                 AV12OrderedBy ,
                                                 AV13OrderedDsc },
                                                 new int[] {
                                                 TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                                 }
            });
            /* Using cursor H00DL2 */
            pr_default.execute(0, new Object[] {AV34WWProjetoDesenvolvimentoDS_1_Tfprojetodesenvolvimento_projetocod, AV35WWProjetoDesenvolvimentoDS_2_Tfprojetodesenvolvimento_projetocod_to, AV36WWProjetoDesenvolvimentoDS_3_Tfprojetodesenvolvimento_sistemacod, AV37WWProjetoDesenvolvimentoDS_4_Tfprojetodesenvolvimento_sistemacod_to, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_35_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A670ProjetoDesenvolvimento_SistemaCod = H00DL2_A670ProjetoDesenvolvimento_SistemaCod[0];
               A669ProjetoDesenvolvimento_ProjetoCod = H00DL2_A669ProjetoDesenvolvimento_ProjetoCod[0];
               /* Execute user event: E19DL2 */
               E19DL2 ();
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 35;
            WBDL0( ) ;
         }
         nGXsfl_35_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         AV34WWProjetoDesenvolvimentoDS_1_Tfprojetodesenvolvimento_projetocod = AV21TFProjetoDesenvolvimento_ProjetoCod;
         AV35WWProjetoDesenvolvimentoDS_2_Tfprojetodesenvolvimento_projetocod_to = AV22TFProjetoDesenvolvimento_ProjetoCod_To;
         AV36WWProjetoDesenvolvimentoDS_3_Tfprojetodesenvolvimento_sistemacod = AV25TFProjetoDesenvolvimento_SistemaCod;
         AV37WWProjetoDesenvolvimentoDS_4_Tfprojetodesenvolvimento_sistemacod_to = AV26TFProjetoDesenvolvimento_SistemaCod_To;
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV34WWProjetoDesenvolvimentoDS_1_Tfprojetodesenvolvimento_projetocod ,
                                              AV35WWProjetoDesenvolvimentoDS_2_Tfprojetodesenvolvimento_projetocod_to ,
                                              AV36WWProjetoDesenvolvimentoDS_3_Tfprojetodesenvolvimento_sistemacod ,
                                              AV37WWProjetoDesenvolvimentoDS_4_Tfprojetodesenvolvimento_sistemacod_to ,
                                              A669ProjetoDesenvolvimento_ProjetoCod ,
                                              A670ProjetoDesenvolvimento_SistemaCod ,
                                              AV12OrderedBy ,
                                              AV13OrderedDsc },
                                              new int[] {
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                              }
         });
         /* Using cursor H00DL3 */
         pr_default.execute(1, new Object[] {AV34WWProjetoDesenvolvimentoDS_1_Tfprojetodesenvolvimento_projetocod, AV35WWProjetoDesenvolvimentoDS_2_Tfprojetodesenvolvimento_projetocod_to, AV36WWProjetoDesenvolvimentoDS_3_Tfprojetodesenvolvimento_sistemacod, AV37WWProjetoDesenvolvimentoDS_4_Tfprojetodesenvolvimento_sistemacod_to});
         GRID_nRecordCount = H00DL3_AGRID_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         AV34WWProjetoDesenvolvimentoDS_1_Tfprojetodesenvolvimento_projetocod = AV21TFProjetoDesenvolvimento_ProjetoCod;
         AV35WWProjetoDesenvolvimentoDS_2_Tfprojetodesenvolvimento_projetocod_to = AV22TFProjetoDesenvolvimento_ProjetoCod_To;
         AV36WWProjetoDesenvolvimentoDS_3_Tfprojetodesenvolvimento_sistemacod = AV25TFProjetoDesenvolvimento_SistemaCod;
         AV37WWProjetoDesenvolvimentoDS_4_Tfprojetodesenvolvimento_sistemacod_to = AV26TFProjetoDesenvolvimento_SistemaCod_To;
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV12OrderedBy, AV13OrderedDsc, AV21TFProjetoDesenvolvimento_ProjetoCod, AV22TFProjetoDesenvolvimento_ProjetoCod_To, AV25TFProjetoDesenvolvimento_SistemaCod, AV26TFProjetoDesenvolvimento_SistemaCod_To, AV23ddo_ProjetoDesenvolvimento_ProjetoCodTitleControlIdToReplace, AV27ddo_ProjetoDesenvolvimento_SistemaCodTitleControlIdToReplace, AV40Pgmname, A669ProjetoDesenvolvimento_ProjetoCod, A670ProjetoDesenvolvimento_SistemaCod, AV17Sistema_Tipo) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         AV34WWProjetoDesenvolvimentoDS_1_Tfprojetodesenvolvimento_projetocod = AV21TFProjetoDesenvolvimento_ProjetoCod;
         AV35WWProjetoDesenvolvimentoDS_2_Tfprojetodesenvolvimento_projetocod_to = AV22TFProjetoDesenvolvimento_ProjetoCod_To;
         AV36WWProjetoDesenvolvimentoDS_3_Tfprojetodesenvolvimento_sistemacod = AV25TFProjetoDesenvolvimento_SistemaCod;
         AV37WWProjetoDesenvolvimentoDS_4_Tfprojetodesenvolvimento_sistemacod_to = AV26TFProjetoDesenvolvimento_SistemaCod_To;
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV12OrderedBy, AV13OrderedDsc, AV21TFProjetoDesenvolvimento_ProjetoCod, AV22TFProjetoDesenvolvimento_ProjetoCod_To, AV25TFProjetoDesenvolvimento_SistemaCod, AV26TFProjetoDesenvolvimento_SistemaCod_To, AV23ddo_ProjetoDesenvolvimento_ProjetoCodTitleControlIdToReplace, AV27ddo_ProjetoDesenvolvimento_SistemaCodTitleControlIdToReplace, AV40Pgmname, A669ProjetoDesenvolvimento_ProjetoCod, A670ProjetoDesenvolvimento_SistemaCod, AV17Sistema_Tipo) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         AV34WWProjetoDesenvolvimentoDS_1_Tfprojetodesenvolvimento_projetocod = AV21TFProjetoDesenvolvimento_ProjetoCod;
         AV35WWProjetoDesenvolvimentoDS_2_Tfprojetodesenvolvimento_projetocod_to = AV22TFProjetoDesenvolvimento_ProjetoCod_To;
         AV36WWProjetoDesenvolvimentoDS_3_Tfprojetodesenvolvimento_sistemacod = AV25TFProjetoDesenvolvimento_SistemaCod;
         AV37WWProjetoDesenvolvimentoDS_4_Tfprojetodesenvolvimento_sistemacod_to = AV26TFProjetoDesenvolvimento_SistemaCod_To;
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV12OrderedBy, AV13OrderedDsc, AV21TFProjetoDesenvolvimento_ProjetoCod, AV22TFProjetoDesenvolvimento_ProjetoCod_To, AV25TFProjetoDesenvolvimento_SistemaCod, AV26TFProjetoDesenvolvimento_SistemaCod_To, AV23ddo_ProjetoDesenvolvimento_ProjetoCodTitleControlIdToReplace, AV27ddo_ProjetoDesenvolvimento_SistemaCodTitleControlIdToReplace, AV40Pgmname, A669ProjetoDesenvolvimento_ProjetoCod, A670ProjetoDesenvolvimento_SistemaCod, AV17Sistema_Tipo) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         AV34WWProjetoDesenvolvimentoDS_1_Tfprojetodesenvolvimento_projetocod = AV21TFProjetoDesenvolvimento_ProjetoCod;
         AV35WWProjetoDesenvolvimentoDS_2_Tfprojetodesenvolvimento_projetocod_to = AV22TFProjetoDesenvolvimento_ProjetoCod_To;
         AV36WWProjetoDesenvolvimentoDS_3_Tfprojetodesenvolvimento_sistemacod = AV25TFProjetoDesenvolvimento_SistemaCod;
         AV37WWProjetoDesenvolvimentoDS_4_Tfprojetodesenvolvimento_sistemacod_to = AV26TFProjetoDesenvolvimento_SistemaCod_To;
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV12OrderedBy, AV13OrderedDsc, AV21TFProjetoDesenvolvimento_ProjetoCod, AV22TFProjetoDesenvolvimento_ProjetoCod_To, AV25TFProjetoDesenvolvimento_SistemaCod, AV26TFProjetoDesenvolvimento_SistemaCod_To, AV23ddo_ProjetoDesenvolvimento_ProjetoCodTitleControlIdToReplace, AV27ddo_ProjetoDesenvolvimento_SistemaCodTitleControlIdToReplace, AV40Pgmname, A669ProjetoDesenvolvimento_ProjetoCod, A670ProjetoDesenvolvimento_SistemaCod, AV17Sistema_Tipo) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         AV34WWProjetoDesenvolvimentoDS_1_Tfprojetodesenvolvimento_projetocod = AV21TFProjetoDesenvolvimento_ProjetoCod;
         AV35WWProjetoDesenvolvimentoDS_2_Tfprojetodesenvolvimento_projetocod_to = AV22TFProjetoDesenvolvimento_ProjetoCod_To;
         AV36WWProjetoDesenvolvimentoDS_3_Tfprojetodesenvolvimento_sistemacod = AV25TFProjetoDesenvolvimento_SistemaCod;
         AV37WWProjetoDesenvolvimentoDS_4_Tfprojetodesenvolvimento_sistemacod_to = AV26TFProjetoDesenvolvimento_SistemaCod_To;
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV12OrderedBy, AV13OrderedDsc, AV21TFProjetoDesenvolvimento_ProjetoCod, AV22TFProjetoDesenvolvimento_ProjetoCod_To, AV25TFProjetoDesenvolvimento_SistemaCod, AV26TFProjetoDesenvolvimento_SistemaCod_To, AV23ddo_ProjetoDesenvolvimento_ProjetoCodTitleControlIdToReplace, AV27ddo_ProjetoDesenvolvimento_SistemaCodTitleControlIdToReplace, AV40Pgmname, A669ProjetoDesenvolvimento_ProjetoCod, A670ProjetoDesenvolvimento_SistemaCod, AV17Sistema_Tipo) ;
         }
         return (int)(0) ;
      }

      protected void STRUPDL0( )
      {
         /* Before Start, stand alone formulas. */
         AV40Pgmname = "WWProjetoDesenvolvimento";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E17DL2 */
         E17DL2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "vDDO_TITLESETTINGSICONS"), AV28DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( "vPROJETODESENVOLVIMENTO_PROJETOCODTITLEFILTERDATA"), AV20ProjetoDesenvolvimento_ProjetoCodTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vPROJETODESENVOLVIMENTO_SISTEMACODTITLEFILTERDATA"), AV24ProjetoDesenvolvimento_SistemaCodTitleFilterData);
            /* Read variables values. */
            cmbavOrderedby.Name = cmbavOrderedby_Internalname;
            cmbavOrderedby.CurrentValue = cgiGet( cmbavOrderedby_Internalname);
            AV12OrderedBy = (short)(NumberUtil.Val( cgiGet( cmbavOrderedby_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12OrderedBy), 4, 0)));
            AV13OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedDsc", AV13OrderedDsc);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfprojetodesenvolvimento_projetocod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfprojetodesenvolvimento_projetocod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFPROJETODESENVOLVIMENTO_PROJETOCOD");
               GX_FocusControl = edtavTfprojetodesenvolvimento_projetocod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV21TFProjetoDesenvolvimento_ProjetoCod = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21TFProjetoDesenvolvimento_ProjetoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21TFProjetoDesenvolvimento_ProjetoCod), 6, 0)));
            }
            else
            {
               AV21TFProjetoDesenvolvimento_ProjetoCod = (int)(context.localUtil.CToN( cgiGet( edtavTfprojetodesenvolvimento_projetocod_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21TFProjetoDesenvolvimento_ProjetoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21TFProjetoDesenvolvimento_ProjetoCod), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfprojetodesenvolvimento_projetocod_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfprojetodesenvolvimento_projetocod_to_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFPROJETODESENVOLVIMENTO_PROJETOCOD_TO");
               GX_FocusControl = edtavTfprojetodesenvolvimento_projetocod_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV22TFProjetoDesenvolvimento_ProjetoCod_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22TFProjetoDesenvolvimento_ProjetoCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22TFProjetoDesenvolvimento_ProjetoCod_To), 6, 0)));
            }
            else
            {
               AV22TFProjetoDesenvolvimento_ProjetoCod_To = (int)(context.localUtil.CToN( cgiGet( edtavTfprojetodesenvolvimento_projetocod_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22TFProjetoDesenvolvimento_ProjetoCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22TFProjetoDesenvolvimento_ProjetoCod_To), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfprojetodesenvolvimento_sistemacod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfprojetodesenvolvimento_sistemacod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFPROJETODESENVOLVIMENTO_SISTEMACOD");
               GX_FocusControl = edtavTfprojetodesenvolvimento_sistemacod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV25TFProjetoDesenvolvimento_SistemaCod = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25TFProjetoDesenvolvimento_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV25TFProjetoDesenvolvimento_SistemaCod), 6, 0)));
            }
            else
            {
               AV25TFProjetoDesenvolvimento_SistemaCod = (int)(context.localUtil.CToN( cgiGet( edtavTfprojetodesenvolvimento_sistemacod_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25TFProjetoDesenvolvimento_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV25TFProjetoDesenvolvimento_SistemaCod), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfprojetodesenvolvimento_sistemacod_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfprojetodesenvolvimento_sistemacod_to_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFPROJETODESENVOLVIMENTO_SISTEMACOD_TO");
               GX_FocusControl = edtavTfprojetodesenvolvimento_sistemacod_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV26TFProjetoDesenvolvimento_SistemaCod_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26TFProjetoDesenvolvimento_SistemaCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26TFProjetoDesenvolvimento_SistemaCod_To), 6, 0)));
            }
            else
            {
               AV26TFProjetoDesenvolvimento_SistemaCod_To = (int)(context.localUtil.CToN( cgiGet( edtavTfprojetodesenvolvimento_sistemacod_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26TFProjetoDesenvolvimento_SistemaCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26TFProjetoDesenvolvimento_SistemaCod_To), 6, 0)));
            }
            AV23ddo_ProjetoDesenvolvimento_ProjetoCodTitleControlIdToReplace = cgiGet( edtavDdo_projetodesenvolvimento_projetocodtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23ddo_ProjetoDesenvolvimento_ProjetoCodTitleControlIdToReplace", AV23ddo_ProjetoDesenvolvimento_ProjetoCodTitleControlIdToReplace);
            AV27ddo_ProjetoDesenvolvimento_SistemaCodTitleControlIdToReplace = cgiGet( edtavDdo_projetodesenvolvimento_sistemacodtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27ddo_ProjetoDesenvolvimento_SistemaCodTitleControlIdToReplace", AV27ddo_ProjetoDesenvolvimento_SistemaCodTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_35 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_35"), ",", "."));
            AV30GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( "vGRIDCURRENTPAGE"), ",", "."));
            AV31GridPageCount = (long)(context.localUtil.CToN( cgiGet( "vGRIDPAGECOUNT"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( "GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( "GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( "GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( "GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( "GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( "GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( "GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( "GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( "GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( "GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( "GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( "GRIDPAGINATIONBAR_Emptygridcaption");
            Ddo_projetodesenvolvimento_projetocod_Caption = cgiGet( "DDO_PROJETODESENVOLVIMENTO_PROJETOCOD_Caption");
            Ddo_projetodesenvolvimento_projetocod_Tooltip = cgiGet( "DDO_PROJETODESENVOLVIMENTO_PROJETOCOD_Tooltip");
            Ddo_projetodesenvolvimento_projetocod_Cls = cgiGet( "DDO_PROJETODESENVOLVIMENTO_PROJETOCOD_Cls");
            Ddo_projetodesenvolvimento_projetocod_Filteredtext_set = cgiGet( "DDO_PROJETODESENVOLVIMENTO_PROJETOCOD_Filteredtext_set");
            Ddo_projetodesenvolvimento_projetocod_Filteredtextto_set = cgiGet( "DDO_PROJETODESENVOLVIMENTO_PROJETOCOD_Filteredtextto_set");
            Ddo_projetodesenvolvimento_projetocod_Dropdownoptionstype = cgiGet( "DDO_PROJETODESENVOLVIMENTO_PROJETOCOD_Dropdownoptionstype");
            Ddo_projetodesenvolvimento_projetocod_Titlecontrolidtoreplace = cgiGet( "DDO_PROJETODESENVOLVIMENTO_PROJETOCOD_Titlecontrolidtoreplace");
            Ddo_projetodesenvolvimento_projetocod_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_PROJETODESENVOLVIMENTO_PROJETOCOD_Includesortasc"));
            Ddo_projetodesenvolvimento_projetocod_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_PROJETODESENVOLVIMENTO_PROJETOCOD_Includesortdsc"));
            Ddo_projetodesenvolvimento_projetocod_Sortedstatus = cgiGet( "DDO_PROJETODESENVOLVIMENTO_PROJETOCOD_Sortedstatus");
            Ddo_projetodesenvolvimento_projetocod_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_PROJETODESENVOLVIMENTO_PROJETOCOD_Includefilter"));
            Ddo_projetodesenvolvimento_projetocod_Filtertype = cgiGet( "DDO_PROJETODESENVOLVIMENTO_PROJETOCOD_Filtertype");
            Ddo_projetodesenvolvimento_projetocod_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_PROJETODESENVOLVIMENTO_PROJETOCOD_Filterisrange"));
            Ddo_projetodesenvolvimento_projetocod_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_PROJETODESENVOLVIMENTO_PROJETOCOD_Includedatalist"));
            Ddo_projetodesenvolvimento_projetocod_Datalistfixedvalues = cgiGet( "DDO_PROJETODESENVOLVIMENTO_PROJETOCOD_Datalistfixedvalues");
            Ddo_projetodesenvolvimento_projetocod_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_PROJETODESENVOLVIMENTO_PROJETOCOD_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_projetodesenvolvimento_projetocod_Sortasc = cgiGet( "DDO_PROJETODESENVOLVIMENTO_PROJETOCOD_Sortasc");
            Ddo_projetodesenvolvimento_projetocod_Sortdsc = cgiGet( "DDO_PROJETODESENVOLVIMENTO_PROJETOCOD_Sortdsc");
            Ddo_projetodesenvolvimento_projetocod_Loadingdata = cgiGet( "DDO_PROJETODESENVOLVIMENTO_PROJETOCOD_Loadingdata");
            Ddo_projetodesenvolvimento_projetocod_Cleanfilter = cgiGet( "DDO_PROJETODESENVOLVIMENTO_PROJETOCOD_Cleanfilter");
            Ddo_projetodesenvolvimento_projetocod_Rangefilterfrom = cgiGet( "DDO_PROJETODESENVOLVIMENTO_PROJETOCOD_Rangefilterfrom");
            Ddo_projetodesenvolvimento_projetocod_Rangefilterto = cgiGet( "DDO_PROJETODESENVOLVIMENTO_PROJETOCOD_Rangefilterto");
            Ddo_projetodesenvolvimento_projetocod_Noresultsfound = cgiGet( "DDO_PROJETODESENVOLVIMENTO_PROJETOCOD_Noresultsfound");
            Ddo_projetodesenvolvimento_projetocod_Searchbuttontext = cgiGet( "DDO_PROJETODESENVOLVIMENTO_PROJETOCOD_Searchbuttontext");
            Ddo_projetodesenvolvimento_sistemacod_Caption = cgiGet( "DDO_PROJETODESENVOLVIMENTO_SISTEMACOD_Caption");
            Ddo_projetodesenvolvimento_sistemacod_Tooltip = cgiGet( "DDO_PROJETODESENVOLVIMENTO_SISTEMACOD_Tooltip");
            Ddo_projetodesenvolvimento_sistemacod_Cls = cgiGet( "DDO_PROJETODESENVOLVIMENTO_SISTEMACOD_Cls");
            Ddo_projetodesenvolvimento_sistemacod_Filteredtext_set = cgiGet( "DDO_PROJETODESENVOLVIMENTO_SISTEMACOD_Filteredtext_set");
            Ddo_projetodesenvolvimento_sistemacod_Filteredtextto_set = cgiGet( "DDO_PROJETODESENVOLVIMENTO_SISTEMACOD_Filteredtextto_set");
            Ddo_projetodesenvolvimento_sistemacod_Dropdownoptionstype = cgiGet( "DDO_PROJETODESENVOLVIMENTO_SISTEMACOD_Dropdownoptionstype");
            Ddo_projetodesenvolvimento_sistemacod_Titlecontrolidtoreplace = cgiGet( "DDO_PROJETODESENVOLVIMENTO_SISTEMACOD_Titlecontrolidtoreplace");
            Ddo_projetodesenvolvimento_sistemacod_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_PROJETODESENVOLVIMENTO_SISTEMACOD_Includesortasc"));
            Ddo_projetodesenvolvimento_sistemacod_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_PROJETODESENVOLVIMENTO_SISTEMACOD_Includesortdsc"));
            Ddo_projetodesenvolvimento_sistemacod_Sortedstatus = cgiGet( "DDO_PROJETODESENVOLVIMENTO_SISTEMACOD_Sortedstatus");
            Ddo_projetodesenvolvimento_sistemacod_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_PROJETODESENVOLVIMENTO_SISTEMACOD_Includefilter"));
            Ddo_projetodesenvolvimento_sistemacod_Filtertype = cgiGet( "DDO_PROJETODESENVOLVIMENTO_SISTEMACOD_Filtertype");
            Ddo_projetodesenvolvimento_sistemacod_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_PROJETODESENVOLVIMENTO_SISTEMACOD_Filterisrange"));
            Ddo_projetodesenvolvimento_sistemacod_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_PROJETODESENVOLVIMENTO_SISTEMACOD_Includedatalist"));
            Ddo_projetodesenvolvimento_sistemacod_Datalistfixedvalues = cgiGet( "DDO_PROJETODESENVOLVIMENTO_SISTEMACOD_Datalistfixedvalues");
            Ddo_projetodesenvolvimento_sistemacod_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_PROJETODESENVOLVIMENTO_SISTEMACOD_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_projetodesenvolvimento_sistemacod_Sortasc = cgiGet( "DDO_PROJETODESENVOLVIMENTO_SISTEMACOD_Sortasc");
            Ddo_projetodesenvolvimento_sistemacod_Sortdsc = cgiGet( "DDO_PROJETODESENVOLVIMENTO_SISTEMACOD_Sortdsc");
            Ddo_projetodesenvolvimento_sistemacod_Loadingdata = cgiGet( "DDO_PROJETODESENVOLVIMENTO_SISTEMACOD_Loadingdata");
            Ddo_projetodesenvolvimento_sistemacod_Cleanfilter = cgiGet( "DDO_PROJETODESENVOLVIMENTO_SISTEMACOD_Cleanfilter");
            Ddo_projetodesenvolvimento_sistemacod_Rangefilterfrom = cgiGet( "DDO_PROJETODESENVOLVIMENTO_SISTEMACOD_Rangefilterfrom");
            Ddo_projetodesenvolvimento_sistemacod_Rangefilterto = cgiGet( "DDO_PROJETODESENVOLVIMENTO_SISTEMACOD_Rangefilterto");
            Ddo_projetodesenvolvimento_sistemacod_Noresultsfound = cgiGet( "DDO_PROJETODESENVOLVIMENTO_SISTEMACOD_Noresultsfound");
            Ddo_projetodesenvolvimento_sistemacod_Searchbuttontext = cgiGet( "DDO_PROJETODESENVOLVIMENTO_SISTEMACOD_Searchbuttontext");
            Gridpaginationbar_Selectedpage = cgiGet( "GRIDPAGINATIONBAR_Selectedpage");
            Ddo_projetodesenvolvimento_projetocod_Activeeventkey = cgiGet( "DDO_PROJETODESENVOLVIMENTO_PROJETOCOD_Activeeventkey");
            Ddo_projetodesenvolvimento_projetocod_Filteredtext_get = cgiGet( "DDO_PROJETODESENVOLVIMENTO_PROJETOCOD_Filteredtext_get");
            Ddo_projetodesenvolvimento_projetocod_Filteredtextto_get = cgiGet( "DDO_PROJETODESENVOLVIMENTO_PROJETOCOD_Filteredtextto_get");
            Ddo_projetodesenvolvimento_sistemacod_Activeeventkey = cgiGet( "DDO_PROJETODESENVOLVIMENTO_SISTEMACOD_Activeeventkey");
            Ddo_projetodesenvolvimento_sistemacod_Filteredtext_get = cgiGet( "DDO_PROJETODESENVOLVIMENTO_SISTEMACOD_Filteredtext_get");
            Ddo_projetodesenvolvimento_sistemacod_Filteredtextto_get = cgiGet( "DDO_PROJETODESENVOLVIMENTO_SISTEMACOD_Filteredtextto_get");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV12OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV13OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFPROJETODESENVOLVIMENTO_PROJETOCOD"), ",", ".") != Convert.ToDecimal( AV21TFProjetoDesenvolvimento_ProjetoCod )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFPROJETODESENVOLVIMENTO_PROJETOCOD_TO"), ",", ".") != Convert.ToDecimal( AV22TFProjetoDesenvolvimento_ProjetoCod_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFPROJETODESENVOLVIMENTO_SISTEMACOD"), ",", ".") != Convert.ToDecimal( AV25TFProjetoDesenvolvimento_SistemaCod )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFPROJETODESENVOLVIMENTO_SISTEMACOD_TO"), ",", ".") != Convert.ToDecimal( AV26TFProjetoDesenvolvimento_SistemaCod_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E17DL2 */
         E17DL2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E17DL2( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         edtavTfprojetodesenvolvimento_projetocod_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfprojetodesenvolvimento_projetocod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfprojetodesenvolvimento_projetocod_Visible), 5, 0)));
         edtavTfprojetodesenvolvimento_projetocod_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfprojetodesenvolvimento_projetocod_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfprojetodesenvolvimento_projetocod_to_Visible), 5, 0)));
         edtavTfprojetodesenvolvimento_sistemacod_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfprojetodesenvolvimento_sistemacod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfprojetodesenvolvimento_sistemacod_Visible), 5, 0)));
         edtavTfprojetodesenvolvimento_sistemacod_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfprojetodesenvolvimento_sistemacod_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfprojetodesenvolvimento_sistemacod_to_Visible), 5, 0)));
         Ddo_projetodesenvolvimento_projetocod_Titlecontrolidtoreplace = subGrid_Internalname+"_ProjetoDesenvolvimento_ProjetoCod";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_projetodesenvolvimento_projetocod_Internalname, "TitleControlIdToReplace", Ddo_projetodesenvolvimento_projetocod_Titlecontrolidtoreplace);
         AV23ddo_ProjetoDesenvolvimento_ProjetoCodTitleControlIdToReplace = Ddo_projetodesenvolvimento_projetocod_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23ddo_ProjetoDesenvolvimento_ProjetoCodTitleControlIdToReplace", AV23ddo_ProjetoDesenvolvimento_ProjetoCodTitleControlIdToReplace);
         edtavDdo_projetodesenvolvimento_projetocodtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_projetodesenvolvimento_projetocodtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_projetodesenvolvimento_projetocodtitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_projetodesenvolvimento_sistemacod_Titlecontrolidtoreplace = subGrid_Internalname+"_ProjetoDesenvolvimento_SistemaCod";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_projetodesenvolvimento_sistemacod_Internalname, "TitleControlIdToReplace", Ddo_projetodesenvolvimento_sistemacod_Titlecontrolidtoreplace);
         AV27ddo_ProjetoDesenvolvimento_SistemaCodTitleControlIdToReplace = Ddo_projetodesenvolvimento_sistemacod_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27ddo_ProjetoDesenvolvimento_SistemaCodTitleControlIdToReplace", AV27ddo_ProjetoDesenvolvimento_SistemaCodTitleControlIdToReplace);
         edtavDdo_projetodesenvolvimento_sistemacodtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_projetodesenvolvimento_sistemacodtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_projetodesenvolvimento_sistemacodtitlecontrolidtoreplace_Visible), 5, 0)));
         Form.Caption = " Projeto Desenvolvimento";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavOrderedby.removeAllItems();
         cmbavOrderedby.addItem("1", "Projeto", 0);
         cmbavOrderedby.addItem("2", "Sistema", 0);
         if ( AV12OrderedBy < 1 )
         {
            AV12OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12OrderedBy), 4, 0)));
            /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
            S132 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = AV28DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1) ;
         AV28DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1;
      }

      protected void E18DL2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV20ProjetoDesenvolvimento_ProjetoCodTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV24ProjetoDesenvolvimento_SistemaCodTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtProjetoDesenvolvimento_ProjetoCod_Titleformat = 2;
         edtProjetoDesenvolvimento_ProjetoCod_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Projeto", AV23ddo_ProjetoDesenvolvimento_ProjetoCodTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtProjetoDesenvolvimento_ProjetoCod_Internalname, "Title", edtProjetoDesenvolvimento_ProjetoCod_Title);
         edtProjetoDesenvolvimento_SistemaCod_Titleformat = 2;
         edtProjetoDesenvolvimento_SistemaCod_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Sistema", AV27ddo_ProjetoDesenvolvimento_SistemaCodTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtProjetoDesenvolvimento_SistemaCod_Internalname, "Title", edtProjetoDesenvolvimento_SistemaCod_Title);
         AV30GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV30GridCurrentPage), 10, 0)));
         AV31GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV31GridPageCount), 10, 0)));
         AV34WWProjetoDesenvolvimentoDS_1_Tfprojetodesenvolvimento_projetocod = AV21TFProjetoDesenvolvimento_ProjetoCod;
         AV35WWProjetoDesenvolvimentoDS_2_Tfprojetodesenvolvimento_projetocod_to = AV22TFProjetoDesenvolvimento_ProjetoCod_To;
         AV36WWProjetoDesenvolvimentoDS_3_Tfprojetodesenvolvimento_sistemacod = AV25TFProjetoDesenvolvimento_SistemaCod;
         AV37WWProjetoDesenvolvimentoDS_4_Tfprojetodesenvolvimento_sistemacod_to = AV26TFProjetoDesenvolvimento_SistemaCod_To;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV20ProjetoDesenvolvimento_ProjetoCodTitleFilterData", AV20ProjetoDesenvolvimento_ProjetoCodTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV24ProjetoDesenvolvimento_SistemaCodTitleFilterData", AV24ProjetoDesenvolvimento_SistemaCodTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
      }

      protected void E11DL2( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV29PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV29PageToGo) ;
         }
      }

      protected void E12DL2( )
      {
         /* Ddo_projetodesenvolvimento_projetocod_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_projetodesenvolvimento_projetocod_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV12OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12OrderedBy), 4, 0)));
            AV13OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedDsc", AV13OrderedDsc);
            Ddo_projetodesenvolvimento_projetocod_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_projetodesenvolvimento_projetocod_Internalname, "SortedStatus", Ddo_projetodesenvolvimento_projetocod_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_projetodesenvolvimento_projetocod_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV12OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12OrderedBy), 4, 0)));
            AV13OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedDsc", AV13OrderedDsc);
            Ddo_projetodesenvolvimento_projetocod_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_projetodesenvolvimento_projetocod_Internalname, "SortedStatus", Ddo_projetodesenvolvimento_projetocod_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_projetodesenvolvimento_projetocod_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV21TFProjetoDesenvolvimento_ProjetoCod = (int)(NumberUtil.Val( Ddo_projetodesenvolvimento_projetocod_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21TFProjetoDesenvolvimento_ProjetoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21TFProjetoDesenvolvimento_ProjetoCod), 6, 0)));
            AV22TFProjetoDesenvolvimento_ProjetoCod_To = (int)(NumberUtil.Val( Ddo_projetodesenvolvimento_projetocod_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22TFProjetoDesenvolvimento_ProjetoCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22TFProjetoDesenvolvimento_ProjetoCod_To), 6, 0)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV12OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E13DL2( )
      {
         /* Ddo_projetodesenvolvimento_sistemacod_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_projetodesenvolvimento_sistemacod_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV12OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12OrderedBy), 4, 0)));
            AV13OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedDsc", AV13OrderedDsc);
            Ddo_projetodesenvolvimento_sistemacod_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_projetodesenvolvimento_sistemacod_Internalname, "SortedStatus", Ddo_projetodesenvolvimento_sistemacod_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_projetodesenvolvimento_sistemacod_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV12OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12OrderedBy), 4, 0)));
            AV13OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedDsc", AV13OrderedDsc);
            Ddo_projetodesenvolvimento_sistemacod_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_projetodesenvolvimento_sistemacod_Internalname, "SortedStatus", Ddo_projetodesenvolvimento_sistemacod_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_projetodesenvolvimento_sistemacod_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV25TFProjetoDesenvolvimento_SistemaCod = (int)(NumberUtil.Val( Ddo_projetodesenvolvimento_sistemacod_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25TFProjetoDesenvolvimento_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV25TFProjetoDesenvolvimento_SistemaCod), 6, 0)));
            AV26TFProjetoDesenvolvimento_SistemaCod_To = (int)(NumberUtil.Val( Ddo_projetodesenvolvimento_sistemacod_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26TFProjetoDesenvolvimento_SistemaCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26TFProjetoDesenvolvimento_SistemaCod_To), 6, 0)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV12OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      private void E19DL2( )
      {
         /* Grid_Load Routine */
         AV14Update = context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavUpdate_Internalname, AV14Update);
         AV38Update_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( )));
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = formatLink("projetodesenvolvimento.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A669ProjetoDesenvolvimento_ProjetoCod) + "," + UrlEncode("" +A670ProjetoDesenvolvimento_SistemaCod) + "," + UrlEncode(StringUtil.RTrim(AV17Sistema_Tipo));
         AV15Delete = context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDelete_Internalname, AV15Delete);
         AV39Delete_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )));
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = formatLink("projetodesenvolvimento.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A669ProjetoDesenvolvimento_ProjetoCod) + "," + UrlEncode("" +A670ProjetoDesenvolvimento_SistemaCod) + "," + UrlEncode(StringUtil.RTrim(AV17Sistema_Tipo));
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 35;
         }
         sendrow_352( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_35_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(35, GridRow);
         }
      }

      protected void E14DL2( )
      {
         /* Orderedby_Click Routine */
         context.DoAjaxRefresh();
      }

      protected void E15DL2( )
      {
         /* 'DoCleanFilters' Routine */
         /* Execute user subroutine: 'CLEANFILTERS' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_firstpage( ) ;
         context.DoAjaxRefresh();
      }

      protected void E16DL2( )
      {
         /* 'DoInsert' Routine */
         context.wjLoc = formatLink("projetodesenvolvimento.aspx") + "?" + UrlEncode(StringUtil.RTrim("INS")) + "," + UrlEncode("" +0) + "," + UrlEncode("" +0) + "," + UrlEncode(StringUtil.RTrim(AV17Sistema_Tipo));
         context.wjLocDisableFrm = 1;
      }

      protected void S152( )
      {
         /* 'RESETDDOSORTEDSTATUS' Routine */
         Ddo_projetodesenvolvimento_projetocod_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_projetodesenvolvimento_projetocod_Internalname, "SortedStatus", Ddo_projetodesenvolvimento_projetocod_Sortedstatus);
         Ddo_projetodesenvolvimento_sistemacod_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_projetodesenvolvimento_sistemacod_Internalname, "SortedStatus", Ddo_projetodesenvolvimento_sistemacod_Sortedstatus);
      }

      protected void S132( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV12OrderedBy == 1 )
         {
            Ddo_projetodesenvolvimento_projetocod_Sortedstatus = (AV13OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_projetodesenvolvimento_projetocod_Internalname, "SortedStatus", Ddo_projetodesenvolvimento_projetocod_Sortedstatus);
         }
         else if ( AV12OrderedBy == 2 )
         {
            Ddo_projetodesenvolvimento_sistemacod_Sortedstatus = (AV13OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_projetodesenvolvimento_sistemacod_Internalname, "SortedStatus", Ddo_projetodesenvolvimento_sistemacod_Sortedstatus);
         }
      }

      protected void S162( )
      {
         /* 'CLEANFILTERS' Routine */
         AV21TFProjetoDesenvolvimento_ProjetoCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21TFProjetoDesenvolvimento_ProjetoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21TFProjetoDesenvolvimento_ProjetoCod), 6, 0)));
         Ddo_projetodesenvolvimento_projetocod_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_projetodesenvolvimento_projetocod_Internalname, "FilteredText_set", Ddo_projetodesenvolvimento_projetocod_Filteredtext_set);
         AV22TFProjetoDesenvolvimento_ProjetoCod_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22TFProjetoDesenvolvimento_ProjetoCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22TFProjetoDesenvolvimento_ProjetoCod_To), 6, 0)));
         Ddo_projetodesenvolvimento_projetocod_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_projetodesenvolvimento_projetocod_Internalname, "FilteredTextTo_set", Ddo_projetodesenvolvimento_projetocod_Filteredtextto_set);
         AV25TFProjetoDesenvolvimento_SistemaCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25TFProjetoDesenvolvimento_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV25TFProjetoDesenvolvimento_SistemaCod), 6, 0)));
         Ddo_projetodesenvolvimento_sistemacod_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_projetodesenvolvimento_sistemacod_Internalname, "FilteredText_set", Ddo_projetodesenvolvimento_sistemacod_Filteredtext_set);
         AV26TFProjetoDesenvolvimento_SistemaCod_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26TFProjetoDesenvolvimento_SistemaCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26TFProjetoDesenvolvimento_SistemaCod_To), 6, 0)));
         Ddo_projetodesenvolvimento_sistemacod_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_projetodesenvolvimento_sistemacod_Internalname, "FilteredTextTo_set", Ddo_projetodesenvolvimento_sistemacod_Filteredtextto_set);
      }

      protected void S122( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV16Session.Get(AV40Pgmname+"GridState"), "") == 0 )
         {
            AV10GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  AV40Pgmname+"GridState"), "");
         }
         else
         {
            AV10GridState.FromXml(AV16Session.Get(AV40Pgmname+"GridState"), "");
         }
         AV12OrderedBy = AV10GridState.gxTpr_Orderedby;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12OrderedBy), 4, 0)));
         AV13OrderedDsc = AV10GridState.gxTpr_Ordereddsc;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedDsc", AV13OrderedDsc);
         /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADREGFILTERSSTATE' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_gotopage( AV10GridState.gxTpr_Currentpage) ;
      }

      protected void S172( )
      {
         /* 'LOADREGFILTERSSTATE' Routine */
         AV41GXV1 = 1;
         while ( AV41GXV1 <= AV10GridState.gxTpr_Filtervalues.Count )
         {
            AV11GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV10GridState.gxTpr_Filtervalues.Item(AV41GXV1));
            if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFPROJETODESENVOLVIMENTO_PROJETOCOD") == 0 )
            {
               AV21TFProjetoDesenvolvimento_ProjetoCod = (int)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21TFProjetoDesenvolvimento_ProjetoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21TFProjetoDesenvolvimento_ProjetoCod), 6, 0)));
               AV22TFProjetoDesenvolvimento_ProjetoCod_To = (int)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Valueto, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22TFProjetoDesenvolvimento_ProjetoCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22TFProjetoDesenvolvimento_ProjetoCod_To), 6, 0)));
               if ( ! (0==AV21TFProjetoDesenvolvimento_ProjetoCod) )
               {
                  Ddo_projetodesenvolvimento_projetocod_Filteredtext_set = StringUtil.Str( (decimal)(AV21TFProjetoDesenvolvimento_ProjetoCod), 6, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_projetodesenvolvimento_projetocod_Internalname, "FilteredText_set", Ddo_projetodesenvolvimento_projetocod_Filteredtext_set);
               }
               if ( ! (0==AV22TFProjetoDesenvolvimento_ProjetoCod_To) )
               {
                  Ddo_projetodesenvolvimento_projetocod_Filteredtextto_set = StringUtil.Str( (decimal)(AV22TFProjetoDesenvolvimento_ProjetoCod_To), 6, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_projetodesenvolvimento_projetocod_Internalname, "FilteredTextTo_set", Ddo_projetodesenvolvimento_projetocod_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFPROJETODESENVOLVIMENTO_SISTEMACOD") == 0 )
            {
               AV25TFProjetoDesenvolvimento_SistemaCod = (int)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25TFProjetoDesenvolvimento_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV25TFProjetoDesenvolvimento_SistemaCod), 6, 0)));
               AV26TFProjetoDesenvolvimento_SistemaCod_To = (int)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Valueto, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26TFProjetoDesenvolvimento_SistemaCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26TFProjetoDesenvolvimento_SistemaCod_To), 6, 0)));
               if ( ! (0==AV25TFProjetoDesenvolvimento_SistemaCod) )
               {
                  Ddo_projetodesenvolvimento_sistemacod_Filteredtext_set = StringUtil.Str( (decimal)(AV25TFProjetoDesenvolvimento_SistemaCod), 6, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_projetodesenvolvimento_sistemacod_Internalname, "FilteredText_set", Ddo_projetodesenvolvimento_sistemacod_Filteredtext_set);
               }
               if ( ! (0==AV26TFProjetoDesenvolvimento_SistemaCod_To) )
               {
                  Ddo_projetodesenvolvimento_sistemacod_Filteredtextto_set = StringUtil.Str( (decimal)(AV26TFProjetoDesenvolvimento_SistemaCod_To), 6, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_projetodesenvolvimento_sistemacod_Internalname, "FilteredTextTo_set", Ddo_projetodesenvolvimento_sistemacod_Filteredtextto_set);
               }
            }
            AV41GXV1 = (int)(AV41GXV1+1);
         }
      }

      protected void S142( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV10GridState.FromXml(AV16Session.Get(AV40Pgmname+"GridState"), "");
         AV10GridState.gxTpr_Orderedby = AV12OrderedBy;
         AV10GridState.gxTpr_Ordereddsc = AV13OrderedDsc;
         AV10GridState.gxTpr_Filtervalues.Clear();
         if ( ! ( (0==AV21TFProjetoDesenvolvimento_ProjetoCod) && (0==AV22TFProjetoDesenvolvimento_ProjetoCod_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFPROJETODESENVOLVIMENTO_PROJETOCOD";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV21TFProjetoDesenvolvimento_ProjetoCod), 6, 0);
            AV11GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( (decimal)(AV22TFProjetoDesenvolvimento_ProjetoCod_To), 6, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (0==AV25TFProjetoDesenvolvimento_SistemaCod) && (0==AV26TFProjetoDesenvolvimento_SistemaCod_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFPROJETODESENVOLVIMENTO_SISTEMACOD";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV25TFProjetoDesenvolvimento_SistemaCod), 6, 0);
            AV11GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( (decimal)(AV26TFProjetoDesenvolvimento_SistemaCod_To), 6, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         AV10GridState.gxTpr_Currentpage = (short)(subGrid_Currentpage( ));
         new wwpbaseobjects.savegridstate(context ).execute(  AV40Pgmname+"GridState",  AV10GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_MeetrikaVs3")) ;
      }

      protected void S112( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8TrnContext.gxTpr_Callerobject = AV40Pgmname;
         AV8TrnContext.gxTpr_Callerondelete = true;
         AV8TrnContext.gxTpr_Callerurl = AV7HTTPRequest.ScriptName+"?"+AV7HTTPRequest.QueryString;
         AV8TrnContext.gxTpr_Transactionname = "ProjetoDesenvolvimento";
         AV16Session.Set("TrnContext", AV8TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_MeetrikaVs3"));
      }

      protected void wb_table1_2_DL2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_DL2( true) ;
         }
         else
         {
            wb_table2_8_DL2( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_DL2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_29_DL2( true) ;
         }
         else
         {
            wb_table3_29_DL2( false) ;
         }
         return  ;
      }

      protected void wb_table3_29_DL2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_DL2e( true) ;
         }
         else
         {
            wb_table1_2_DL2e( false) ;
         }
      }

      protected void wb_table3_29_DL2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_32_DL2( true) ;
         }
         else
         {
            wb_table4_32_DL2( false) ;
         }
         return  ;
      }

      protected void wb_table4_32_DL2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_29_DL2e( true) ;
         }
         else
         {
            wb_table3_29_DL2e( false) ;
         }
      }

      protected void wb_table4_32_DL2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"DivS\" data-gxgridid=\"35\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtProjetoDesenvolvimento_ProjetoCod_Titleformat == 0 )
               {
                  context.SendWebValue( edtProjetoDesenvolvimento_ProjetoCod_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtProjetoDesenvolvimento_ProjetoCod_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtProjetoDesenvolvimento_SistemaCod_Titleformat == 0 )
               {
                  context.SendWebValue( edtProjetoDesenvolvimento_SistemaCod_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtProjetoDesenvolvimento_SistemaCod_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", "");
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV14Update));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavUpdate_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavUpdate_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV15Delete));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavDelete_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDelete_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A669ProjetoDesenvolvimento_ProjetoCod), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtProjetoDesenvolvimento_ProjetoCod_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtProjetoDesenvolvimento_ProjetoCod_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A670ProjetoDesenvolvimento_SistemaCod), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtProjetoDesenvolvimento_SistemaCod_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtProjetoDesenvolvimento_SistemaCod_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 35 )
         {
            wbEnd = 0;
            nRC_GXsfl_35 = (short)(nGXsfl_35_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_32_DL2e( true) ;
         }
         else
         {
            wb_table4_32_DL2e( false) ;
         }
      }

      protected void wb_table2_8_DL2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableheader_Internalname, tblTableheader_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='GridHeaderCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblProjetodesenvolvimentotitle_Internalname, "Projeto Desenvolvimento", "", "", lblProjetodesenvolvimentotitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_WWProjetoDesenvolvimento.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table5_13_DL2( true) ;
         }
         else
         {
            wb_table5_13_DL2( false) ;
         }
         return  ;
      }

      protected void wb_table5_13_DL2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='OrderedByDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblOrderedtext_Internalname, "Ordenado por", "", "", lblOrderedtext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "OrderedByDescription", 0, "", 1, 1, 0, "HLP_WWProjetoDesenvolvimento.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 20,'',false,'" + sGXsfl_35_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavOrderedby, cmbavOrderedby_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV12OrderedBy), 4, 0)), 1, cmbavOrderedby_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVORDEREDBY.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,20);\"", "", true, "HLP_WWProjetoDesenvolvimento.htm");
            cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV12OrderedBy), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", (String)(cmbavOrderedby.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'" + sGXsfl_35_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV13OrderedDsc), StringUtil.BoolToStr( AV13OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,21);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_WWProjetoDesenvolvimento.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableFiltersCell'>") ;
            wb_table6_23_DL2( true) ;
         }
         else
         {
            wb_table6_23_DL2( false) ;
         }
         return  ;
      }

      protected void wb_table6_23_DL2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_DL2e( true) ;
         }
         else
         {
            wb_table2_8_DL2e( false) ;
         }
      }

      protected void wb_table6_23_DL2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 26,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgCleanfilters_Internalname, context.GetImagePath( "effedd9a-25cf-4497-952f-5a5ac032b8be", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Limpar filtros", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgCleanfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOCLEANFILTERS\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWProjetoDesenvolvimento.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_23_DL2e( true) ;
         }
         else
         {
            wb_table6_23_DL2e( false) ;
         }
      }

      protected void wb_table5_13_DL2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "Table", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 16,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgInsert_Internalname, context.GetImagePath( "5649fbb8-8ce0-4810-a5ce-bd649ea83c3a", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Inserir", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgInsert_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOINSERT\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWProjetoDesenvolvimento.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_13_DL2e( true) ;
         }
         else
         {
            wb_table5_13_DL2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PADL2( ) ;
         WSDL2( ) ;
         WEDL2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?2249787");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?202042823242831");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wwprojetodesenvolvimento.js", "?202042823242831");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_352( )
      {
         edtavUpdate_Internalname = "vUPDATE_"+sGXsfl_35_idx;
         edtavDelete_Internalname = "vDELETE_"+sGXsfl_35_idx;
         edtProjetoDesenvolvimento_ProjetoCod_Internalname = "PROJETODESENVOLVIMENTO_PROJETOCOD_"+sGXsfl_35_idx;
         edtProjetoDesenvolvimento_SistemaCod_Internalname = "PROJETODESENVOLVIMENTO_SISTEMACOD_"+sGXsfl_35_idx;
      }

      protected void SubsflControlProps_fel_352( )
      {
         edtavUpdate_Internalname = "vUPDATE_"+sGXsfl_35_fel_idx;
         edtavDelete_Internalname = "vDELETE_"+sGXsfl_35_fel_idx;
         edtProjetoDesenvolvimento_ProjetoCod_Internalname = "PROJETODESENVOLVIMENTO_PROJETOCOD_"+sGXsfl_35_fel_idx;
         edtProjetoDesenvolvimento_SistemaCod_Internalname = "PROJETODESENVOLVIMENTO_SISTEMACOD_"+sGXsfl_35_fel_idx;
      }

      protected void sendrow_352( )
      {
         SubsflControlProps_352( ) ;
         WBDL0( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_35_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_35_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_35_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV14Update_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV14Update))&&String.IsNullOrEmpty(StringUtil.RTrim( AV38Update_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV14Update)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavUpdate_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV14Update)) ? AV38Update_GXI : context.PathToRelativeUrl( AV14Update)),(String)edtavUpdate_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavUpdate_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV14Update_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV15Delete_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV15Delete))&&String.IsNullOrEmpty(StringUtil.RTrim( AV39Delete_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV15Delete)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDelete_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV15Delete)) ? AV39Delete_GXI : context.PathToRelativeUrl( AV15Delete)),(String)edtavDelete_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavDelete_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV15Delete_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtProjetoDesenvolvimento_ProjetoCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A669ProjetoDesenvolvimento_ProjetoCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A669ProjetoDesenvolvimento_ProjetoCod), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtProjetoDesenvolvimento_ProjetoCod_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)35,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtProjetoDesenvolvimento_SistemaCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A670ProjetoDesenvolvimento_SistemaCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A670ProjetoDesenvolvimento_SistemaCod), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtProjetoDesenvolvimento_SistemaCod_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)35,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            GxWebStd.gx_hidden_field( context, "gxhash_PROJETODESENVOLVIMENTO_PROJETOCOD"+"_"+sGXsfl_35_idx, GetSecureSignedToken( sGXsfl_35_idx, context.localUtil.Format( (decimal)(A669ProjetoDesenvolvimento_ProjetoCod), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_PROJETODESENVOLVIMENTO_SISTEMACOD"+"_"+sGXsfl_35_idx, GetSecureSignedToken( sGXsfl_35_idx, context.localUtil.Format( (decimal)(A670ProjetoDesenvolvimento_SistemaCod), "ZZZZZ9")));
            GridContainer.AddRow(GridRow);
            nGXsfl_35_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_35_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_35_idx+1));
            sGXsfl_35_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_35_idx), 4, 0)), 4, "0");
            SubsflControlProps_352( ) ;
         }
         /* End function sendrow_352 */
      }

      protected void init_default_properties( )
      {
         lblProjetodesenvolvimentotitle_Internalname = "PROJETODESENVOLVIMENTOTITLE";
         imgInsert_Internalname = "INSERT";
         tblTableactions_Internalname = "TABLEACTIONS";
         lblOrderedtext_Internalname = "ORDEREDTEXT";
         cmbavOrderedby_Internalname = "vORDEREDBY";
         edtavOrdereddsc_Internalname = "vORDEREDDSC";
         imgCleanfilters_Internalname = "CLEANFILTERS";
         tblTablefilters_Internalname = "TABLEFILTERS";
         tblTableheader_Internalname = "TABLEHEADER";
         edtavUpdate_Internalname = "vUPDATE";
         edtavDelete_Internalname = "vDELETE";
         edtProjetoDesenvolvimento_ProjetoCod_Internalname = "PROJETODESENVOLVIMENTO_PROJETOCOD";
         edtProjetoDesenvolvimento_SistemaCod_Internalname = "PROJETODESENVOLVIMENTO_SISTEMACOD";
         Gridpaginationbar_Internalname = "GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = "GRIDTABLEWITHPAGINATIONBAR";
         tblTablegridheader_Internalname = "TABLEGRIDHEADER";
         tblTablemain_Internalname = "TABLEMAIN";
         Workwithplusutilities1_Internalname = "WORKWITHPLUSUTILITIES1";
         edtavTfprojetodesenvolvimento_projetocod_Internalname = "vTFPROJETODESENVOLVIMENTO_PROJETOCOD";
         edtavTfprojetodesenvolvimento_projetocod_to_Internalname = "vTFPROJETODESENVOLVIMENTO_PROJETOCOD_TO";
         edtavTfprojetodesenvolvimento_sistemacod_Internalname = "vTFPROJETODESENVOLVIMENTO_SISTEMACOD";
         edtavTfprojetodesenvolvimento_sistemacod_to_Internalname = "vTFPROJETODESENVOLVIMENTO_SISTEMACOD_TO";
         Ddo_projetodesenvolvimento_projetocod_Internalname = "DDO_PROJETODESENVOLVIMENTO_PROJETOCOD";
         edtavDdo_projetodesenvolvimento_projetocodtitlecontrolidtoreplace_Internalname = "vDDO_PROJETODESENVOLVIMENTO_PROJETOCODTITLECONTROLIDTOREPLACE";
         Ddo_projetodesenvolvimento_sistemacod_Internalname = "DDO_PROJETODESENVOLVIMENTO_SISTEMACOD";
         edtavDdo_projetodesenvolvimento_sistemacodtitlecontrolidtoreplace_Internalname = "vDDO_PROJETODESENVOLVIMENTO_SISTEMACODTITLECONTROLIDTOREPLACE";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtProjetoDesenvolvimento_SistemaCod_Jsonclick = "";
         edtProjetoDesenvolvimento_ProjetoCod_Jsonclick = "";
         edtavOrdereddsc_Jsonclick = "";
         cmbavOrderedby_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = "";
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = "";
         edtProjetoDesenvolvimento_SistemaCod_Titleformat = 0;
         edtProjetoDesenvolvimento_ProjetoCod_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         edtProjetoDesenvolvimento_SistemaCod_Title = "Sistema";
         edtProjetoDesenvolvimento_ProjetoCod_Title = "Projeto";
         edtavOrdereddsc_Visible = 1;
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         edtavDdo_projetodesenvolvimento_sistemacodtitlecontrolidtoreplace_Visible = 1;
         edtavDdo_projetodesenvolvimento_projetocodtitlecontrolidtoreplace_Visible = 1;
         edtavTfprojetodesenvolvimento_sistemacod_to_Jsonclick = "";
         edtavTfprojetodesenvolvimento_sistemacod_to_Visible = 1;
         edtavTfprojetodesenvolvimento_sistemacod_Jsonclick = "";
         edtavTfprojetodesenvolvimento_sistemacod_Visible = 1;
         edtavTfprojetodesenvolvimento_projetocod_to_Jsonclick = "";
         edtavTfprojetodesenvolvimento_projetocod_to_Visible = 1;
         edtavTfprojetodesenvolvimento_projetocod_Jsonclick = "";
         edtavTfprojetodesenvolvimento_projetocod_Visible = 1;
         Ddo_projetodesenvolvimento_sistemacod_Searchbuttontext = "Pesquisar";
         Ddo_projetodesenvolvimento_sistemacod_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_projetodesenvolvimento_sistemacod_Rangefilterto = "At�";
         Ddo_projetodesenvolvimento_sistemacod_Rangefilterfrom = "Desde";
         Ddo_projetodesenvolvimento_sistemacod_Cleanfilter = "Limpar pesquisa";
         Ddo_projetodesenvolvimento_sistemacod_Loadingdata = "Carregando dados...";
         Ddo_projetodesenvolvimento_sistemacod_Sortdsc = "Ordenar de Z � A";
         Ddo_projetodesenvolvimento_sistemacod_Sortasc = "Ordenar de A � Z";
         Ddo_projetodesenvolvimento_sistemacod_Datalistupdateminimumcharacters = 0;
         Ddo_projetodesenvolvimento_sistemacod_Datalistfixedvalues = "";
         Ddo_projetodesenvolvimento_sistemacod_Includedatalist = Convert.ToBoolean( 0);
         Ddo_projetodesenvolvimento_sistemacod_Filterisrange = Convert.ToBoolean( -1);
         Ddo_projetodesenvolvimento_sistemacod_Filtertype = "Numeric";
         Ddo_projetodesenvolvimento_sistemacod_Includefilter = Convert.ToBoolean( -1);
         Ddo_projetodesenvolvimento_sistemacod_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_projetodesenvolvimento_sistemacod_Includesortasc = Convert.ToBoolean( -1);
         Ddo_projetodesenvolvimento_sistemacod_Titlecontrolidtoreplace = "";
         Ddo_projetodesenvolvimento_sistemacod_Dropdownoptionstype = "GridTitleSettings";
         Ddo_projetodesenvolvimento_sistemacod_Cls = "ColumnSettings";
         Ddo_projetodesenvolvimento_sistemacod_Tooltip = "Op��es";
         Ddo_projetodesenvolvimento_sistemacod_Caption = "";
         Ddo_projetodesenvolvimento_projetocod_Searchbuttontext = "Pesquisar";
         Ddo_projetodesenvolvimento_projetocod_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_projetodesenvolvimento_projetocod_Rangefilterto = "At�";
         Ddo_projetodesenvolvimento_projetocod_Rangefilterfrom = "Desde";
         Ddo_projetodesenvolvimento_projetocod_Cleanfilter = "Limpar pesquisa";
         Ddo_projetodesenvolvimento_projetocod_Loadingdata = "Carregando dados...";
         Ddo_projetodesenvolvimento_projetocod_Sortdsc = "Ordenar de Z � A";
         Ddo_projetodesenvolvimento_projetocod_Sortasc = "Ordenar de A � Z";
         Ddo_projetodesenvolvimento_projetocod_Datalistupdateminimumcharacters = 0;
         Ddo_projetodesenvolvimento_projetocod_Datalistfixedvalues = "";
         Ddo_projetodesenvolvimento_projetocod_Includedatalist = Convert.ToBoolean( 0);
         Ddo_projetodesenvolvimento_projetocod_Filterisrange = Convert.ToBoolean( -1);
         Ddo_projetodesenvolvimento_projetocod_Filtertype = "Numeric";
         Ddo_projetodesenvolvimento_projetocod_Includefilter = Convert.ToBoolean( -1);
         Ddo_projetodesenvolvimento_projetocod_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_projetodesenvolvimento_projetocod_Includesortasc = Convert.ToBoolean( -1);
         Ddo_projetodesenvolvimento_projetocod_Titlecontrolidtoreplace = "";
         Ddo_projetodesenvolvimento_projetocod_Dropdownoptionstype = "GridTitleSettings";
         Ddo_projetodesenvolvimento_projetocod_Cls = "ColumnSettings";
         Ddo_projetodesenvolvimento_projetocod_Tooltip = "Op��es";
         Ddo_projetodesenvolvimento_projetocod_Caption = "";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = " Projeto Desenvolvimento";
         subGrid_Rows = 0;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A669ProjetoDesenvolvimento_ProjetoCod',fld:'PROJETODESENVOLVIMENTO_PROJETOCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A670ProjetoDesenvolvimento_SistemaCod',fld:'PROJETODESENVOLVIMENTO_SISTEMACOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV17Sistema_Tipo',fld:'vSISTEMA_TIPO',pic:'',nv:''},{av:'AV23ddo_ProjetoDesenvolvimento_ProjetoCodTitleControlIdToReplace',fld:'vDDO_PROJETODESENVOLVIMENTO_PROJETOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV27ddo_ProjetoDesenvolvimento_SistemaCodTitleControlIdToReplace',fld:'vDDO_PROJETODESENVOLVIMENTO_SISTEMACODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV21TFProjetoDesenvolvimento_ProjetoCod',fld:'vTFPROJETODESENVOLVIMENTO_PROJETOCOD',pic:'ZZZZZ9',nv:0},{av:'AV22TFProjetoDesenvolvimento_ProjetoCod_To',fld:'vTFPROJETODESENVOLVIMENTO_PROJETOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV25TFProjetoDesenvolvimento_SistemaCod',fld:'vTFPROJETODESENVOLVIMENTO_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'AV26TFProjetoDesenvolvimento_SistemaCod_To',fld:'vTFPROJETODESENVOLVIMENTO_SISTEMACOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV40Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV12OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false}],oparms:[{av:'AV20ProjetoDesenvolvimento_ProjetoCodTitleFilterData',fld:'vPROJETODESENVOLVIMENTO_PROJETOCODTITLEFILTERDATA',pic:'',nv:null},{av:'AV24ProjetoDesenvolvimento_SistemaCodTitleFilterData',fld:'vPROJETODESENVOLVIMENTO_SISTEMACODTITLEFILTERDATA',pic:'',nv:null},{av:'edtProjetoDesenvolvimento_ProjetoCod_Titleformat',ctrl:'PROJETODESENVOLVIMENTO_PROJETOCOD',prop:'Titleformat'},{av:'edtProjetoDesenvolvimento_ProjetoCod_Title',ctrl:'PROJETODESENVOLVIMENTO_PROJETOCOD',prop:'Title'},{av:'edtProjetoDesenvolvimento_SistemaCod_Titleformat',ctrl:'PROJETODESENVOLVIMENTO_SISTEMACOD',prop:'Titleformat'},{av:'edtProjetoDesenvolvimento_SistemaCod_Title',ctrl:'PROJETODESENVOLVIMENTO_SISTEMACOD',prop:'Title'},{av:'AV30GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV31GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E11DL2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV12OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV21TFProjetoDesenvolvimento_ProjetoCod',fld:'vTFPROJETODESENVOLVIMENTO_PROJETOCOD',pic:'ZZZZZ9',nv:0},{av:'AV22TFProjetoDesenvolvimento_ProjetoCod_To',fld:'vTFPROJETODESENVOLVIMENTO_PROJETOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV25TFProjetoDesenvolvimento_SistemaCod',fld:'vTFPROJETODESENVOLVIMENTO_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'AV26TFProjetoDesenvolvimento_SistemaCod_To',fld:'vTFPROJETODESENVOLVIMENTO_SISTEMACOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV23ddo_ProjetoDesenvolvimento_ProjetoCodTitleControlIdToReplace',fld:'vDDO_PROJETODESENVOLVIMENTO_PROJETOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV27ddo_ProjetoDesenvolvimento_SistemaCodTitleControlIdToReplace',fld:'vDDO_PROJETODESENVOLVIMENTO_SISTEMACODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'A669ProjetoDesenvolvimento_ProjetoCod',fld:'PROJETODESENVOLVIMENTO_PROJETOCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A670ProjetoDesenvolvimento_SistemaCod',fld:'PROJETODESENVOLVIMENTO_SISTEMACOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV17Sistema_Tipo',fld:'vSISTEMA_TIPO',pic:'',nv:''},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("DDO_PROJETODESENVOLVIMENTO_PROJETOCOD.ONOPTIONCLICKED","{handler:'E12DL2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV12OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV21TFProjetoDesenvolvimento_ProjetoCod',fld:'vTFPROJETODESENVOLVIMENTO_PROJETOCOD',pic:'ZZZZZ9',nv:0},{av:'AV22TFProjetoDesenvolvimento_ProjetoCod_To',fld:'vTFPROJETODESENVOLVIMENTO_PROJETOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV25TFProjetoDesenvolvimento_SistemaCod',fld:'vTFPROJETODESENVOLVIMENTO_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'AV26TFProjetoDesenvolvimento_SistemaCod_To',fld:'vTFPROJETODESENVOLVIMENTO_SISTEMACOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV23ddo_ProjetoDesenvolvimento_ProjetoCodTitleControlIdToReplace',fld:'vDDO_PROJETODESENVOLVIMENTO_PROJETOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV27ddo_ProjetoDesenvolvimento_SistemaCodTitleControlIdToReplace',fld:'vDDO_PROJETODESENVOLVIMENTO_SISTEMACODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'A669ProjetoDesenvolvimento_ProjetoCod',fld:'PROJETODESENVOLVIMENTO_PROJETOCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A670ProjetoDesenvolvimento_SistemaCod',fld:'PROJETODESENVOLVIMENTO_SISTEMACOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV17Sistema_Tipo',fld:'vSISTEMA_TIPO',pic:'',nv:''},{av:'Ddo_projetodesenvolvimento_projetocod_Activeeventkey',ctrl:'DDO_PROJETODESENVOLVIMENTO_PROJETOCOD',prop:'ActiveEventKey'},{av:'Ddo_projetodesenvolvimento_projetocod_Filteredtext_get',ctrl:'DDO_PROJETODESENVOLVIMENTO_PROJETOCOD',prop:'FilteredText_get'},{av:'Ddo_projetodesenvolvimento_projetocod_Filteredtextto_get',ctrl:'DDO_PROJETODESENVOLVIMENTO_PROJETOCOD',prop:'FilteredTextTo_get'}],oparms:[{av:'AV12OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_projetodesenvolvimento_projetocod_Sortedstatus',ctrl:'DDO_PROJETODESENVOLVIMENTO_PROJETOCOD',prop:'SortedStatus'},{av:'AV21TFProjetoDesenvolvimento_ProjetoCod',fld:'vTFPROJETODESENVOLVIMENTO_PROJETOCOD',pic:'ZZZZZ9',nv:0},{av:'AV22TFProjetoDesenvolvimento_ProjetoCod_To',fld:'vTFPROJETODESENVOLVIMENTO_PROJETOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_projetodesenvolvimento_sistemacod_Sortedstatus',ctrl:'DDO_PROJETODESENVOLVIMENTO_SISTEMACOD',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_PROJETODESENVOLVIMENTO_SISTEMACOD.ONOPTIONCLICKED","{handler:'E13DL2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV12OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV21TFProjetoDesenvolvimento_ProjetoCod',fld:'vTFPROJETODESENVOLVIMENTO_PROJETOCOD',pic:'ZZZZZ9',nv:0},{av:'AV22TFProjetoDesenvolvimento_ProjetoCod_To',fld:'vTFPROJETODESENVOLVIMENTO_PROJETOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV25TFProjetoDesenvolvimento_SistemaCod',fld:'vTFPROJETODESENVOLVIMENTO_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'AV26TFProjetoDesenvolvimento_SistemaCod_To',fld:'vTFPROJETODESENVOLVIMENTO_SISTEMACOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV23ddo_ProjetoDesenvolvimento_ProjetoCodTitleControlIdToReplace',fld:'vDDO_PROJETODESENVOLVIMENTO_PROJETOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV27ddo_ProjetoDesenvolvimento_SistemaCodTitleControlIdToReplace',fld:'vDDO_PROJETODESENVOLVIMENTO_SISTEMACODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'A669ProjetoDesenvolvimento_ProjetoCod',fld:'PROJETODESENVOLVIMENTO_PROJETOCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A670ProjetoDesenvolvimento_SistemaCod',fld:'PROJETODESENVOLVIMENTO_SISTEMACOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV17Sistema_Tipo',fld:'vSISTEMA_TIPO',pic:'',nv:''},{av:'Ddo_projetodesenvolvimento_sistemacod_Activeeventkey',ctrl:'DDO_PROJETODESENVOLVIMENTO_SISTEMACOD',prop:'ActiveEventKey'},{av:'Ddo_projetodesenvolvimento_sistemacod_Filteredtext_get',ctrl:'DDO_PROJETODESENVOLVIMENTO_SISTEMACOD',prop:'FilteredText_get'},{av:'Ddo_projetodesenvolvimento_sistemacod_Filteredtextto_get',ctrl:'DDO_PROJETODESENVOLVIMENTO_SISTEMACOD',prop:'FilteredTextTo_get'}],oparms:[{av:'AV12OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_projetodesenvolvimento_sistemacod_Sortedstatus',ctrl:'DDO_PROJETODESENVOLVIMENTO_SISTEMACOD',prop:'SortedStatus'},{av:'AV25TFProjetoDesenvolvimento_SistemaCod',fld:'vTFPROJETODESENVOLVIMENTO_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'AV26TFProjetoDesenvolvimento_SistemaCod_To',fld:'vTFPROJETODESENVOLVIMENTO_SISTEMACOD_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_projetodesenvolvimento_projetocod_Sortedstatus',ctrl:'DDO_PROJETODESENVOLVIMENTO_PROJETOCOD',prop:'SortedStatus'}]}");
         setEventMetadata("GRID.LOAD","{handler:'E19DL2',iparms:[{av:'A669ProjetoDesenvolvimento_ProjetoCod',fld:'PROJETODESENVOLVIMENTO_PROJETOCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A670ProjetoDesenvolvimento_SistemaCod',fld:'PROJETODESENVOLVIMENTO_SISTEMACOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV17Sistema_Tipo',fld:'vSISTEMA_TIPO',pic:'',nv:''}],oparms:[{av:'AV14Update',fld:'vUPDATE',pic:'',nv:''},{av:'edtavUpdate_Tooltiptext',ctrl:'vUPDATE',prop:'Tooltiptext'},{av:'edtavUpdate_Link',ctrl:'vUPDATE',prop:'Link'},{av:'AV15Delete',fld:'vDELETE',pic:'',nv:''},{av:'edtavDelete_Tooltiptext',ctrl:'vDELETE',prop:'Tooltiptext'},{av:'edtavDelete_Link',ctrl:'vDELETE',prop:'Link'}]}");
         setEventMetadata("VORDEREDBY.CLICK","{handler:'E14DL2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV12OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV21TFProjetoDesenvolvimento_ProjetoCod',fld:'vTFPROJETODESENVOLVIMENTO_PROJETOCOD',pic:'ZZZZZ9',nv:0},{av:'AV22TFProjetoDesenvolvimento_ProjetoCod_To',fld:'vTFPROJETODESENVOLVIMENTO_PROJETOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV25TFProjetoDesenvolvimento_SistemaCod',fld:'vTFPROJETODESENVOLVIMENTO_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'AV26TFProjetoDesenvolvimento_SistemaCod_To',fld:'vTFPROJETODESENVOLVIMENTO_SISTEMACOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV23ddo_ProjetoDesenvolvimento_ProjetoCodTitleControlIdToReplace',fld:'vDDO_PROJETODESENVOLVIMENTO_PROJETOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV27ddo_ProjetoDesenvolvimento_SistemaCodTitleControlIdToReplace',fld:'vDDO_PROJETODESENVOLVIMENTO_SISTEMACODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'A669ProjetoDesenvolvimento_ProjetoCod',fld:'PROJETODESENVOLVIMENTO_PROJETOCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A670ProjetoDesenvolvimento_SistemaCod',fld:'PROJETODESENVOLVIMENTO_SISTEMACOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV17Sistema_Tipo',fld:'vSISTEMA_TIPO',pic:'',nv:''}],oparms:[]}");
         setEventMetadata("'DOCLEANFILTERS'","{handler:'E15DL2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV12OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV21TFProjetoDesenvolvimento_ProjetoCod',fld:'vTFPROJETODESENVOLVIMENTO_PROJETOCOD',pic:'ZZZZZ9',nv:0},{av:'AV22TFProjetoDesenvolvimento_ProjetoCod_To',fld:'vTFPROJETODESENVOLVIMENTO_PROJETOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV25TFProjetoDesenvolvimento_SistemaCod',fld:'vTFPROJETODESENVOLVIMENTO_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'AV26TFProjetoDesenvolvimento_SistemaCod_To',fld:'vTFPROJETODESENVOLVIMENTO_SISTEMACOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV23ddo_ProjetoDesenvolvimento_ProjetoCodTitleControlIdToReplace',fld:'vDDO_PROJETODESENVOLVIMENTO_PROJETOCODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV27ddo_ProjetoDesenvolvimento_SistemaCodTitleControlIdToReplace',fld:'vDDO_PROJETODESENVOLVIMENTO_SISTEMACODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV40Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'A669ProjetoDesenvolvimento_ProjetoCod',fld:'PROJETODESENVOLVIMENTO_PROJETOCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A670ProjetoDesenvolvimento_SistemaCod',fld:'PROJETODESENVOLVIMENTO_SISTEMACOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV17Sistema_Tipo',fld:'vSISTEMA_TIPO',pic:'',nv:''}],oparms:[{av:'AV21TFProjetoDesenvolvimento_ProjetoCod',fld:'vTFPROJETODESENVOLVIMENTO_PROJETOCOD',pic:'ZZZZZ9',nv:0},{av:'Ddo_projetodesenvolvimento_projetocod_Filteredtext_set',ctrl:'DDO_PROJETODESENVOLVIMENTO_PROJETOCOD',prop:'FilteredText_set'},{av:'AV22TFProjetoDesenvolvimento_ProjetoCod_To',fld:'vTFPROJETODESENVOLVIMENTO_PROJETOCOD_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_projetodesenvolvimento_projetocod_Filteredtextto_set',ctrl:'DDO_PROJETODESENVOLVIMENTO_PROJETOCOD',prop:'FilteredTextTo_set'},{av:'AV25TFProjetoDesenvolvimento_SistemaCod',fld:'vTFPROJETODESENVOLVIMENTO_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'Ddo_projetodesenvolvimento_sistemacod_Filteredtext_set',ctrl:'DDO_PROJETODESENVOLVIMENTO_SISTEMACOD',prop:'FilteredText_set'},{av:'AV26TFProjetoDesenvolvimento_SistemaCod_To',fld:'vTFPROJETODESENVOLVIMENTO_SISTEMACOD_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_projetodesenvolvimento_sistemacod_Filteredtextto_set',ctrl:'DDO_PROJETODESENVOLVIMENTO_SISTEMACOD',prop:'FilteredTextTo_set'}]}");
         setEventMetadata("'DOINSERT'","{handler:'E16DL2',iparms:[{av:'A669ProjetoDesenvolvimento_ProjetoCod',fld:'PROJETODESENVOLVIMENTO_PROJETOCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A670ProjetoDesenvolvimento_SistemaCod',fld:'PROJETODESENVOLVIMENTO_SISTEMACOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV17Sistema_Tipo',fld:'vSISTEMA_TIPO',pic:'',nv:''}],oparms:[{av:'AV17Sistema_Tipo',fld:'vSISTEMA_TIPO',pic:'',nv:''}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Gridpaginationbar_Selectedpage = "";
         Ddo_projetodesenvolvimento_projetocod_Activeeventkey = "";
         Ddo_projetodesenvolvimento_projetocod_Filteredtext_get = "";
         Ddo_projetodesenvolvimento_projetocod_Filteredtextto_get = "";
         Ddo_projetodesenvolvimento_sistemacod_Activeeventkey = "";
         Ddo_projetodesenvolvimento_sistemacod_Filteredtext_get = "";
         Ddo_projetodesenvolvimento_sistemacod_Filteredtextto_get = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV23ddo_ProjetoDesenvolvimento_ProjetoCodTitleControlIdToReplace = "";
         AV27ddo_ProjetoDesenvolvimento_SistemaCodTitleControlIdToReplace = "";
         AV40Pgmname = "";
         AV17Sistema_Tipo = "A";
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV28DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV20ProjetoDesenvolvimento_ProjetoCodTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV24ProjetoDesenvolvimento_SistemaCodTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         Ddo_projetodesenvolvimento_projetocod_Filteredtext_set = "";
         Ddo_projetodesenvolvimento_projetocod_Filteredtextto_set = "";
         Ddo_projetodesenvolvimento_projetocod_Sortedstatus = "";
         Ddo_projetodesenvolvimento_sistemacod_Filteredtext_set = "";
         Ddo_projetodesenvolvimento_sistemacod_Filteredtextto_set = "";
         Ddo_projetodesenvolvimento_sistemacod_Sortedstatus = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV14Update = "";
         AV38Update_GXI = "";
         AV15Delete = "";
         AV39Delete_GXI = "";
         GridContainer = new GXWebGrid( context);
         scmdbuf = "";
         H00DL2_A670ProjetoDesenvolvimento_SistemaCod = new int[1] ;
         H00DL2_A669ProjetoDesenvolvimento_ProjetoCod = new int[1] ;
         H00DL3_AGRID_nRecordCount = new long[1] ;
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV10GridState = new wwpbaseobjects.SdtWWPGridState(context);
         GridRow = new GXWebRow();
         AV16Session = context.GetSession();
         AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV7HTTPRequest = new GxHttpRequest( context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblProjetodesenvolvimentotitle_Jsonclick = "";
         lblOrderedtext_Jsonclick = "";
         imgCleanfilters_Jsonclick = "";
         imgInsert_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wwprojetodesenvolvimento__default(),
            new Object[][] {
                new Object[] {
               H00DL2_A670ProjetoDesenvolvimento_SistemaCod, H00DL2_A669ProjetoDesenvolvimento_ProjetoCod
               }
               , new Object[] {
               H00DL3_AGRID_nRecordCount
               }
            }
         );
         AV40Pgmname = "WWProjetoDesenvolvimento";
         /* GeneXus formulas. */
         AV40Pgmname = "WWProjetoDesenvolvimento";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_35 ;
      private short nGXsfl_35_idx=1 ;
      private short AV12OrderedBy ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_35_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short edtProjetoDesenvolvimento_ProjetoCod_Titleformat ;
      private short edtProjetoDesenvolvimento_SistemaCod_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int subGrid_Rows ;
      private int AV21TFProjetoDesenvolvimento_ProjetoCod ;
      private int AV22TFProjetoDesenvolvimento_ProjetoCod_To ;
      private int AV25TFProjetoDesenvolvimento_SistemaCod ;
      private int AV26TFProjetoDesenvolvimento_SistemaCod_To ;
      private int A669ProjetoDesenvolvimento_ProjetoCod ;
      private int A670ProjetoDesenvolvimento_SistemaCod ;
      private int Gridpaginationbar_Pagestoshow ;
      private int Ddo_projetodesenvolvimento_projetocod_Datalistupdateminimumcharacters ;
      private int Ddo_projetodesenvolvimento_sistemacod_Datalistupdateminimumcharacters ;
      private int edtavTfprojetodesenvolvimento_projetocod_Visible ;
      private int edtavTfprojetodesenvolvimento_projetocod_to_Visible ;
      private int edtavTfprojetodesenvolvimento_sistemacod_Visible ;
      private int edtavTfprojetodesenvolvimento_sistemacod_to_Visible ;
      private int edtavDdo_projetodesenvolvimento_projetocodtitlecontrolidtoreplace_Visible ;
      private int edtavDdo_projetodesenvolvimento_sistemacodtitlecontrolidtoreplace_Visible ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int AV34WWProjetoDesenvolvimentoDS_1_Tfprojetodesenvolvimento_projetocod ;
      private int AV35WWProjetoDesenvolvimentoDS_2_Tfprojetodesenvolvimento_projetocod_to ;
      private int AV36WWProjetoDesenvolvimentoDS_3_Tfprojetodesenvolvimento_sistemacod ;
      private int AV37WWProjetoDesenvolvimentoDS_4_Tfprojetodesenvolvimento_sistemacod_to ;
      private int edtavOrdereddsc_Visible ;
      private int AV29PageToGo ;
      private int AV41GXV1 ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private long GRID_nFirstRecordOnPage ;
      private long AV30GridCurrentPage ;
      private long AV31GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private String Gridpaginationbar_Selectedpage ;
      private String Ddo_projetodesenvolvimento_projetocod_Activeeventkey ;
      private String Ddo_projetodesenvolvimento_projetocod_Filteredtext_get ;
      private String Ddo_projetodesenvolvimento_projetocod_Filteredtextto_get ;
      private String Ddo_projetodesenvolvimento_sistemacod_Activeeventkey ;
      private String Ddo_projetodesenvolvimento_sistemacod_Filteredtext_get ;
      private String Ddo_projetodesenvolvimento_sistemacod_Filteredtextto_get ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_35_idx="0001" ;
      private String AV40Pgmname ;
      private String AV17Sistema_Tipo ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Ddo_projetodesenvolvimento_projetocod_Caption ;
      private String Ddo_projetodesenvolvimento_projetocod_Tooltip ;
      private String Ddo_projetodesenvolvimento_projetocod_Cls ;
      private String Ddo_projetodesenvolvimento_projetocod_Filteredtext_set ;
      private String Ddo_projetodesenvolvimento_projetocod_Filteredtextto_set ;
      private String Ddo_projetodesenvolvimento_projetocod_Dropdownoptionstype ;
      private String Ddo_projetodesenvolvimento_projetocod_Titlecontrolidtoreplace ;
      private String Ddo_projetodesenvolvimento_projetocod_Sortedstatus ;
      private String Ddo_projetodesenvolvimento_projetocod_Filtertype ;
      private String Ddo_projetodesenvolvimento_projetocod_Datalistfixedvalues ;
      private String Ddo_projetodesenvolvimento_projetocod_Sortasc ;
      private String Ddo_projetodesenvolvimento_projetocod_Sortdsc ;
      private String Ddo_projetodesenvolvimento_projetocod_Loadingdata ;
      private String Ddo_projetodesenvolvimento_projetocod_Cleanfilter ;
      private String Ddo_projetodesenvolvimento_projetocod_Rangefilterfrom ;
      private String Ddo_projetodesenvolvimento_projetocod_Rangefilterto ;
      private String Ddo_projetodesenvolvimento_projetocod_Noresultsfound ;
      private String Ddo_projetodesenvolvimento_projetocod_Searchbuttontext ;
      private String Ddo_projetodesenvolvimento_sistemacod_Caption ;
      private String Ddo_projetodesenvolvimento_sistemacod_Tooltip ;
      private String Ddo_projetodesenvolvimento_sistemacod_Cls ;
      private String Ddo_projetodesenvolvimento_sistemacod_Filteredtext_set ;
      private String Ddo_projetodesenvolvimento_sistemacod_Filteredtextto_set ;
      private String Ddo_projetodesenvolvimento_sistemacod_Dropdownoptionstype ;
      private String Ddo_projetodesenvolvimento_sistemacod_Titlecontrolidtoreplace ;
      private String Ddo_projetodesenvolvimento_sistemacod_Sortedstatus ;
      private String Ddo_projetodesenvolvimento_sistemacod_Filtertype ;
      private String Ddo_projetodesenvolvimento_sistemacod_Datalistfixedvalues ;
      private String Ddo_projetodesenvolvimento_sistemacod_Sortasc ;
      private String Ddo_projetodesenvolvimento_sistemacod_Sortdsc ;
      private String Ddo_projetodesenvolvimento_sistemacod_Loadingdata ;
      private String Ddo_projetodesenvolvimento_sistemacod_Cleanfilter ;
      private String Ddo_projetodesenvolvimento_sistemacod_Rangefilterfrom ;
      private String Ddo_projetodesenvolvimento_sistemacod_Rangefilterto ;
      private String Ddo_projetodesenvolvimento_sistemacod_Noresultsfound ;
      private String Ddo_projetodesenvolvimento_sistemacod_Searchbuttontext ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String TempTags ;
      private String edtavTfprojetodesenvolvimento_projetocod_Internalname ;
      private String edtavTfprojetodesenvolvimento_projetocod_Jsonclick ;
      private String edtavTfprojetodesenvolvimento_projetocod_to_Internalname ;
      private String edtavTfprojetodesenvolvimento_projetocod_to_Jsonclick ;
      private String edtavTfprojetodesenvolvimento_sistemacod_Internalname ;
      private String edtavTfprojetodesenvolvimento_sistemacod_Jsonclick ;
      private String edtavTfprojetodesenvolvimento_sistemacod_to_Internalname ;
      private String edtavTfprojetodesenvolvimento_sistemacod_to_Jsonclick ;
      private String ClassString ;
      private String StyleString ;
      private String edtavDdo_projetodesenvolvimento_projetocodtitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_projetodesenvolvimento_sistemacodtitlecontrolidtoreplace_Internalname ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavUpdate_Internalname ;
      private String edtavDelete_Internalname ;
      private String edtProjetoDesenvolvimento_ProjetoCod_Internalname ;
      private String edtProjetoDesenvolvimento_SistemaCod_Internalname ;
      private String cmbavOrderedby_Internalname ;
      private String scmdbuf ;
      private String edtavOrdereddsc_Internalname ;
      private String subGrid_Internalname ;
      private String Ddo_projetodesenvolvimento_projetocod_Internalname ;
      private String Ddo_projetodesenvolvimento_sistemacod_Internalname ;
      private String edtProjetoDesenvolvimento_ProjetoCod_Title ;
      private String edtProjetoDesenvolvimento_SistemaCod_Title ;
      private String edtavUpdate_Tooltiptext ;
      private String edtavUpdate_Link ;
      private String edtavDelete_Tooltiptext ;
      private String edtavDelete_Link ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTablegridheader_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTableheader_Internalname ;
      private String lblProjetodesenvolvimentotitle_Internalname ;
      private String lblProjetodesenvolvimentotitle_Jsonclick ;
      private String lblOrderedtext_Internalname ;
      private String lblOrderedtext_Jsonclick ;
      private String cmbavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Jsonclick ;
      private String tblTablefilters_Internalname ;
      private String imgCleanfilters_Internalname ;
      private String imgCleanfilters_Jsonclick ;
      private String tblTableactions_Internalname ;
      private String imgInsert_Internalname ;
      private String imgInsert_Jsonclick ;
      private String sGXsfl_35_fel_idx="0001" ;
      private String ROClassString ;
      private String edtProjetoDesenvolvimento_ProjetoCod_Jsonclick ;
      private String edtProjetoDesenvolvimento_SistemaCod_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private bool entryPointCalled ;
      private bool AV13OrderedDsc ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool Ddo_projetodesenvolvimento_projetocod_Includesortasc ;
      private bool Ddo_projetodesenvolvimento_projetocod_Includesortdsc ;
      private bool Ddo_projetodesenvolvimento_projetocod_Includefilter ;
      private bool Ddo_projetodesenvolvimento_projetocod_Filterisrange ;
      private bool Ddo_projetodesenvolvimento_projetocod_Includedatalist ;
      private bool Ddo_projetodesenvolvimento_sistemacod_Includesortasc ;
      private bool Ddo_projetodesenvolvimento_sistemacod_Includesortdsc ;
      private bool Ddo_projetodesenvolvimento_sistemacod_Includefilter ;
      private bool Ddo_projetodesenvolvimento_sistemacod_Filterisrange ;
      private bool Ddo_projetodesenvolvimento_sistemacod_Includedatalist ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV14Update_IsBlob ;
      private bool AV15Delete_IsBlob ;
      private String AV23ddo_ProjetoDesenvolvimento_ProjetoCodTitleControlIdToReplace ;
      private String AV27ddo_ProjetoDesenvolvimento_SistemaCodTitleControlIdToReplace ;
      private String AV38Update_GXI ;
      private String AV39Delete_GXI ;
      private String AV14Update ;
      private String AV15Delete ;
      private IGxSession AV16Session ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbavOrderedby ;
      private IDataStoreProvider pr_default ;
      private int[] H00DL2_A670ProjetoDesenvolvimento_SistemaCod ;
      private int[] H00DL2_A669ProjetoDesenvolvimento_ProjetoCod ;
      private long[] H00DL3_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV7HTTPRequest ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV20ProjetoDesenvolvimento_ProjetoCodTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV24ProjetoDesenvolvimento_SistemaCodTitleFilterData ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV8TrnContext ;
      private wwpbaseobjects.SdtWWPGridState AV10GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV11GridStateFilterValue ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV28DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 ;
   }

   public class wwprojetodesenvolvimento__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00DL2( IGxContext context ,
                                             int AV34WWProjetoDesenvolvimentoDS_1_Tfprojetodesenvolvimento_projetocod ,
                                             int AV35WWProjetoDesenvolvimentoDS_2_Tfprojetodesenvolvimento_projetocod_to ,
                                             int AV36WWProjetoDesenvolvimentoDS_3_Tfprojetodesenvolvimento_sistemacod ,
                                             int AV37WWProjetoDesenvolvimentoDS_4_Tfprojetodesenvolvimento_sistemacod_to ,
                                             int A669ProjetoDesenvolvimento_ProjetoCod ,
                                             int A670ProjetoDesenvolvimento_SistemaCod ,
                                             short AV12OrderedBy ,
                                             bool AV13OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [9] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " [ProjetoDesenvolvimento_SistemaCod], [ProjetoDesenvolvimento_ProjetoCod]";
         sFromString = " FROM [ProjetoDesenvolvimento] WITH (NOLOCK)";
         sOrderString = "";
         if ( ! (0==AV34WWProjetoDesenvolvimentoDS_1_Tfprojetodesenvolvimento_projetocod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ProjetoDesenvolvimento_ProjetoCod] >= @AV34WWProjetoDesenvolvimentoDS_1_Tfprojetodesenvolvimento_projetocod)";
            }
            else
            {
               sWhereString = sWhereString + " ([ProjetoDesenvolvimento_ProjetoCod] >= @AV34WWProjetoDesenvolvimentoDS_1_Tfprojetodesenvolvimento_projetocod)";
            }
         }
         else
         {
            GXv_int2[0] = 1;
         }
         if ( ! (0==AV35WWProjetoDesenvolvimentoDS_2_Tfprojetodesenvolvimento_projetocod_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ProjetoDesenvolvimento_ProjetoCod] <= @AV35WWProjetoDesenvolvimentoDS_2_Tfprojetodesenvolvimento_projetocod_to)";
            }
            else
            {
               sWhereString = sWhereString + " ([ProjetoDesenvolvimento_ProjetoCod] <= @AV35WWProjetoDesenvolvimentoDS_2_Tfprojetodesenvolvimento_projetocod_to)";
            }
         }
         else
         {
            GXv_int2[1] = 1;
         }
         if ( ! (0==AV36WWProjetoDesenvolvimentoDS_3_Tfprojetodesenvolvimento_sistemacod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ProjetoDesenvolvimento_SistemaCod] >= @AV36WWProjetoDesenvolvimentoDS_3_Tfprojetodesenvolvimento_sistemacod)";
            }
            else
            {
               sWhereString = sWhereString + " ([ProjetoDesenvolvimento_SistemaCod] >= @AV36WWProjetoDesenvolvimentoDS_3_Tfprojetodesenvolvimento_sistemacod)";
            }
         }
         else
         {
            GXv_int2[2] = 1;
         }
         if ( ! (0==AV37WWProjetoDesenvolvimentoDS_4_Tfprojetodesenvolvimento_sistemacod_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ProjetoDesenvolvimento_SistemaCod] <= @AV37WWProjetoDesenvolvimentoDS_4_Tfprojetodesenvolvimento_sistemacod_to)";
            }
            else
            {
               sWhereString = sWhereString + " ([ProjetoDesenvolvimento_SistemaCod] <= @AV37WWProjetoDesenvolvimentoDS_4_Tfprojetodesenvolvimento_sistemacod_to)";
            }
         }
         else
         {
            GXv_int2[3] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            sWhereString = " WHERE" + sWhereString;
         }
         if ( ( AV12OrderedBy == 1 ) && ! AV13OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [ProjetoDesenvolvimento_ProjetoCod]";
         }
         else if ( ( AV12OrderedBy == 1 ) && ( AV13OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [ProjetoDesenvolvimento_ProjetoCod] DESC";
         }
         else if ( ( AV12OrderedBy == 2 ) && ! AV13OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [ProjetoDesenvolvimento_SistemaCod]";
         }
         else if ( ( AV12OrderedBy == 2 ) && ( AV13OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [ProjetoDesenvolvimento_SistemaCod] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY [ProjetoDesenvolvimento_ProjetoCod], [ProjetoDesenvolvimento_SistemaCod]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      protected Object[] conditional_H00DL3( IGxContext context ,
                                             int AV34WWProjetoDesenvolvimentoDS_1_Tfprojetodesenvolvimento_projetocod ,
                                             int AV35WWProjetoDesenvolvimentoDS_2_Tfprojetodesenvolvimento_projetocod_to ,
                                             int AV36WWProjetoDesenvolvimentoDS_3_Tfprojetodesenvolvimento_sistemacod ,
                                             int AV37WWProjetoDesenvolvimentoDS_4_Tfprojetodesenvolvimento_sistemacod_to ,
                                             int A669ProjetoDesenvolvimento_ProjetoCod ,
                                             int A670ProjetoDesenvolvimento_SistemaCod ,
                                             short AV12OrderedBy ,
                                             bool AV13OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [4] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM [ProjetoDesenvolvimento] WITH (NOLOCK)";
         if ( ! (0==AV34WWProjetoDesenvolvimentoDS_1_Tfprojetodesenvolvimento_projetocod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ProjetoDesenvolvimento_ProjetoCod] >= @AV34WWProjetoDesenvolvimentoDS_1_Tfprojetodesenvolvimento_projetocod)";
            }
            else
            {
               sWhereString = sWhereString + " ([ProjetoDesenvolvimento_ProjetoCod] >= @AV34WWProjetoDesenvolvimentoDS_1_Tfprojetodesenvolvimento_projetocod)";
            }
         }
         else
         {
            GXv_int4[0] = 1;
         }
         if ( ! (0==AV35WWProjetoDesenvolvimentoDS_2_Tfprojetodesenvolvimento_projetocod_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ProjetoDesenvolvimento_ProjetoCod] <= @AV35WWProjetoDesenvolvimentoDS_2_Tfprojetodesenvolvimento_projetocod_to)";
            }
            else
            {
               sWhereString = sWhereString + " ([ProjetoDesenvolvimento_ProjetoCod] <= @AV35WWProjetoDesenvolvimentoDS_2_Tfprojetodesenvolvimento_projetocod_to)";
            }
         }
         else
         {
            GXv_int4[1] = 1;
         }
         if ( ! (0==AV36WWProjetoDesenvolvimentoDS_3_Tfprojetodesenvolvimento_sistemacod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ProjetoDesenvolvimento_SistemaCod] >= @AV36WWProjetoDesenvolvimentoDS_3_Tfprojetodesenvolvimento_sistemacod)";
            }
            else
            {
               sWhereString = sWhereString + " ([ProjetoDesenvolvimento_SistemaCod] >= @AV36WWProjetoDesenvolvimentoDS_3_Tfprojetodesenvolvimento_sistemacod)";
            }
         }
         else
         {
            GXv_int4[2] = 1;
         }
         if ( ! (0==AV37WWProjetoDesenvolvimentoDS_4_Tfprojetodesenvolvimento_sistemacod_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([ProjetoDesenvolvimento_SistemaCod] <= @AV37WWProjetoDesenvolvimentoDS_4_Tfprojetodesenvolvimento_sistemacod_to)";
            }
            else
            {
               sWhereString = sWhereString + " ([ProjetoDesenvolvimento_SistemaCod] <= @AV37WWProjetoDesenvolvimentoDS_4_Tfprojetodesenvolvimento_sistemacod_to)";
            }
         }
         else
         {
            GXv_int4[3] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         if ( ( AV12OrderedBy == 1 ) && ! AV13OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV12OrderedBy == 1 ) && ( AV13OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV12OrderedBy == 2 ) && ! AV13OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV12OrderedBy == 2 ) && ( AV13OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H00DL2(context, (int)dynConstraints[0] , (int)dynConstraints[1] , (int)dynConstraints[2] , (int)dynConstraints[3] , (int)dynConstraints[4] , (int)dynConstraints[5] , (short)dynConstraints[6] , (bool)dynConstraints[7] );
               case 1 :
                     return conditional_H00DL3(context, (int)dynConstraints[0] , (int)dynConstraints[1] , (int)dynConstraints[2] , (int)dynConstraints[3] , (int)dynConstraints[4] , (int)dynConstraints[5] , (short)dynConstraints[6] , (bool)dynConstraints[7] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00DL2 ;
          prmH00DL2 = new Object[] {
          new Object[] {"@AV34WWProjetoDesenvolvimentoDS_1_Tfprojetodesenvolvimento_projetocod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV35WWProjetoDesenvolvimentoDS_2_Tfprojetodesenvolvimento_projetocod_to",SqlDbType.Int,6,0} ,
          new Object[] {"@AV36WWProjetoDesenvolvimentoDS_3_Tfprojetodesenvolvimento_sistemacod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV37WWProjetoDesenvolvimentoDS_4_Tfprojetodesenvolvimento_sistemacod_to",SqlDbType.Int,6,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH00DL3 ;
          prmH00DL3 = new Object[] {
          new Object[] {"@AV34WWProjetoDesenvolvimentoDS_1_Tfprojetodesenvolvimento_projetocod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV35WWProjetoDesenvolvimentoDS_2_Tfprojetodesenvolvimento_projetocod_to",SqlDbType.Int,6,0} ,
          new Object[] {"@AV36WWProjetoDesenvolvimentoDS_3_Tfprojetodesenvolvimento_sistemacod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV37WWProjetoDesenvolvimentoDS_4_Tfprojetodesenvolvimento_sistemacod_to",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00DL2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00DL2,11,0,true,false )
             ,new CursorDef("H00DL3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00DL3,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[9]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[10]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[11]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[12]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[13]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[14]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[15]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[16]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[17]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[4]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[5]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[6]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[7]);
                }
                return;
       }
    }

 }

}
