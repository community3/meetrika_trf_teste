/*
               File: GetWWMenuFilterData
        Description: Get WWMenu Filter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:54:49.47
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getwwmenufilterdata : GXProcedure
   {
      public getwwmenufilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getwwmenufilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV23DDOName = aP0_DDOName;
         this.AV21SearchTxt = aP1_SearchTxt;
         this.AV22SearchTxtTo = aP2_SearchTxtTo;
         this.AV27OptionsJson = "" ;
         this.AV30OptionsDescJson = "" ;
         this.AV32OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV27OptionsJson;
         aP4_OptionsDescJson=this.AV30OptionsDescJson;
         aP5_OptionIndexesJson=this.AV32OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV23DDOName = aP0_DDOName;
         this.AV21SearchTxt = aP1_SearchTxt;
         this.AV22SearchTxtTo = aP2_SearchTxtTo;
         this.AV27OptionsJson = "" ;
         this.AV30OptionsDescJson = "" ;
         this.AV32OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV27OptionsJson;
         aP4_OptionsDescJson=this.AV30OptionsDescJson;
         aP5_OptionIndexesJson=this.AV32OptionIndexesJson;
         return AV32OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getwwmenufilterdata objgetwwmenufilterdata;
         objgetwwmenufilterdata = new getwwmenufilterdata();
         objgetwwmenufilterdata.AV23DDOName = aP0_DDOName;
         objgetwwmenufilterdata.AV21SearchTxt = aP1_SearchTxt;
         objgetwwmenufilterdata.AV22SearchTxtTo = aP2_SearchTxtTo;
         objgetwwmenufilterdata.AV27OptionsJson = "" ;
         objgetwwmenufilterdata.AV30OptionsDescJson = "" ;
         objgetwwmenufilterdata.AV32OptionIndexesJson = "" ;
         objgetwwmenufilterdata.context.SetSubmitInitialConfig(context);
         objgetwwmenufilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetwwmenufilterdata);
         aP3_OptionsJson=this.AV27OptionsJson;
         aP4_OptionsDescJson=this.AV30OptionsDescJson;
         aP5_OptionIndexesJson=this.AV32OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getwwmenufilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV26Options = (IGxCollection)(new GxSimpleCollection());
         AV29OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV31OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV23DDOName), "DDO_MENU_NOME") == 0 )
         {
            /* Execute user subroutine: 'LOADMENU_NOMEOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV23DDOName), "DDO_MENU_DESCRICAO") == 0 )
         {
            /* Execute user subroutine: 'LOADMENU_DESCRICAOOPTIONS' */
            S131 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV23DDOName), "DDO_MENU_PAINOM") == 0 )
         {
            /* Execute user subroutine: 'LOADMENU_PAINOMOPTIONS' */
            S141 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV27OptionsJson = AV26Options.ToJSonString(false);
         AV30OptionsDescJson = AV29OptionsDesc.ToJSonString(false);
         AV32OptionIndexesJson = AV31OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV34Session.Get("WWMenuGridState"), "") == 0 )
         {
            AV36GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "WWMenuGridState"), "");
         }
         else
         {
            AV36GridState.FromXml(AV34Session.Get("WWMenuGridState"), "");
         }
         AV48GXV1 = 1;
         while ( AV48GXV1 <= AV36GridState.gxTpr_Filtervalues.Count )
         {
            AV37GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV36GridState.gxTpr_Filtervalues.Item(AV48GXV1));
            if ( StringUtil.StrCmp(AV37GridStateFilterValue.gxTpr_Name, "TFMENU_NOME") == 0 )
            {
               AV10TFMenu_Nome = AV37GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV37GridStateFilterValue.gxTpr_Name, "TFMENU_NOME_SEL") == 0 )
            {
               AV11TFMenu_Nome_Sel = AV37GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV37GridStateFilterValue.gxTpr_Name, "TFMENU_DESCRICAO") == 0 )
            {
               AV12TFMenu_Descricao = AV37GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV37GridStateFilterValue.gxTpr_Name, "TFMENU_DESCRICAO_SEL") == 0 )
            {
               AV13TFMenu_Descricao_Sel = AV37GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV37GridStateFilterValue.gxTpr_Name, "TFMENU_TIPO_SEL") == 0 )
            {
               AV14TFMenu_Tipo_SelsJson = AV37GridStateFilterValue.gxTpr_Value;
               AV15TFMenu_Tipo_Sels.FromJSonString(AV14TFMenu_Tipo_SelsJson);
            }
            else if ( StringUtil.StrCmp(AV37GridStateFilterValue.gxTpr_Name, "TFMENU_ORDEM") == 0 )
            {
               AV16TFMenu_Ordem = (short)(NumberUtil.Val( AV37GridStateFilterValue.gxTpr_Value, "."));
               AV17TFMenu_Ordem_To = (short)(NumberUtil.Val( AV37GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV37GridStateFilterValue.gxTpr_Name, "TFMENU_PAINOM") == 0 )
            {
               AV18TFMenu_PaiNom = AV37GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV37GridStateFilterValue.gxTpr_Name, "TFMENU_PAINOM_SEL") == 0 )
            {
               AV19TFMenu_PaiNom_Sel = AV37GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV37GridStateFilterValue.gxTpr_Name, "TFMENU_ATIVO_SEL") == 0 )
            {
               AV20TFMenu_Ativo_Sel = (short)(NumberUtil.Val( AV37GridStateFilterValue.gxTpr_Value, "."));
            }
            AV48GXV1 = (int)(AV48GXV1+1);
         }
         if ( AV36GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV38GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV36GridState.gxTpr_Dynamicfilters.Item(1));
            AV39DynamicFiltersSelector1 = AV38GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV39DynamicFiltersSelector1, "MENU_NOME") == 0 )
            {
               AV40Menu_Nome1 = AV38GridStateDynamicFilter.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV39DynamicFiltersSelector1, "MENU_PAINOM") == 0 )
            {
               AV41Menu_PaiNom1 = AV38GridStateDynamicFilter.gxTpr_Value;
            }
            if ( AV36GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV42DynamicFiltersEnabled2 = true;
               AV38GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV36GridState.gxTpr_Dynamicfilters.Item(2));
               AV43DynamicFiltersSelector2 = AV38GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV43DynamicFiltersSelector2, "MENU_NOME") == 0 )
               {
                  AV44Menu_Nome2 = AV38GridStateDynamicFilter.gxTpr_Value;
               }
               else if ( StringUtil.StrCmp(AV43DynamicFiltersSelector2, "MENU_PAINOM") == 0 )
               {
                  AV45Menu_PaiNom2 = AV38GridStateDynamicFilter.gxTpr_Value;
               }
            }
         }
      }

      protected void S121( )
      {
         /* 'LOADMENU_NOMEOPTIONS' Routine */
         AV10TFMenu_Nome = AV21SearchTxt;
         AV11TFMenu_Nome_Sel = "";
         AV50WWMenuDS_1_Dynamicfiltersselector1 = AV39DynamicFiltersSelector1;
         AV51WWMenuDS_2_Menu_nome1 = AV40Menu_Nome1;
         AV52WWMenuDS_3_Menu_painom1 = AV41Menu_PaiNom1;
         AV53WWMenuDS_4_Dynamicfiltersenabled2 = AV42DynamicFiltersEnabled2;
         AV54WWMenuDS_5_Dynamicfiltersselector2 = AV43DynamicFiltersSelector2;
         AV55WWMenuDS_6_Menu_nome2 = AV44Menu_Nome2;
         AV56WWMenuDS_7_Menu_painom2 = AV45Menu_PaiNom2;
         AV57WWMenuDS_8_Tfmenu_nome = AV10TFMenu_Nome;
         AV58WWMenuDS_9_Tfmenu_nome_sel = AV11TFMenu_Nome_Sel;
         AV59WWMenuDS_10_Tfmenu_descricao = AV12TFMenu_Descricao;
         AV60WWMenuDS_11_Tfmenu_descricao_sel = AV13TFMenu_Descricao_Sel;
         AV61WWMenuDS_12_Tfmenu_tipo_sels = AV15TFMenu_Tipo_Sels;
         AV62WWMenuDS_13_Tfmenu_ordem = AV16TFMenu_Ordem;
         AV63WWMenuDS_14_Tfmenu_ordem_to = AV17TFMenu_Ordem_To;
         AV64WWMenuDS_15_Tfmenu_painom = AV18TFMenu_PaiNom;
         AV65WWMenuDS_16_Tfmenu_painom_sel = AV19TFMenu_PaiNom_Sel;
         AV66WWMenuDS_17_Tfmenu_ativo_sel = AV20TFMenu_Ativo_Sel;
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              A280Menu_Tipo ,
                                              AV61WWMenuDS_12_Tfmenu_tipo_sels ,
                                              AV50WWMenuDS_1_Dynamicfiltersselector1 ,
                                              AV51WWMenuDS_2_Menu_nome1 ,
                                              AV52WWMenuDS_3_Menu_painom1 ,
                                              AV53WWMenuDS_4_Dynamicfiltersenabled2 ,
                                              AV54WWMenuDS_5_Dynamicfiltersselector2 ,
                                              AV55WWMenuDS_6_Menu_nome2 ,
                                              AV56WWMenuDS_7_Menu_painom2 ,
                                              AV58WWMenuDS_9_Tfmenu_nome_sel ,
                                              AV57WWMenuDS_8_Tfmenu_nome ,
                                              AV60WWMenuDS_11_Tfmenu_descricao_sel ,
                                              AV59WWMenuDS_10_Tfmenu_descricao ,
                                              AV61WWMenuDS_12_Tfmenu_tipo_sels.Count ,
                                              AV62WWMenuDS_13_Tfmenu_ordem ,
                                              AV63WWMenuDS_14_Tfmenu_ordem_to ,
                                              AV65WWMenuDS_16_Tfmenu_painom_sel ,
                                              AV64WWMenuDS_15_Tfmenu_painom ,
                                              AV66WWMenuDS_17_Tfmenu_ativo_sel ,
                                              A278Menu_Nome ,
                                              A286Menu_PaiNom ,
                                              A279Menu_Descricao ,
                                              A283Menu_Ordem ,
                                              A284Menu_Ativo },
                                              new int[] {
                                              TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                              }
         });
         lV51WWMenuDS_2_Menu_nome1 = StringUtil.PadR( StringUtil.RTrim( AV51WWMenuDS_2_Menu_nome1), 30, "%");
         lV52WWMenuDS_3_Menu_painom1 = StringUtil.PadR( StringUtil.RTrim( AV52WWMenuDS_3_Menu_painom1), 30, "%");
         lV55WWMenuDS_6_Menu_nome2 = StringUtil.PadR( StringUtil.RTrim( AV55WWMenuDS_6_Menu_nome2), 30, "%");
         lV56WWMenuDS_7_Menu_painom2 = StringUtil.PadR( StringUtil.RTrim( AV56WWMenuDS_7_Menu_painom2), 30, "%");
         lV57WWMenuDS_8_Tfmenu_nome = StringUtil.PadR( StringUtil.RTrim( AV57WWMenuDS_8_Tfmenu_nome), 30, "%");
         lV59WWMenuDS_10_Tfmenu_descricao = StringUtil.Concat( StringUtil.RTrim( AV59WWMenuDS_10_Tfmenu_descricao), "%", "");
         lV64WWMenuDS_15_Tfmenu_painom = StringUtil.PadR( StringUtil.RTrim( AV64WWMenuDS_15_Tfmenu_painom), 30, "%");
         /* Using cursor P00SP2 */
         pr_default.execute(0, new Object[] {lV51WWMenuDS_2_Menu_nome1, lV52WWMenuDS_3_Menu_painom1, lV55WWMenuDS_6_Menu_nome2, lV56WWMenuDS_7_Menu_painom2, lV57WWMenuDS_8_Tfmenu_nome, AV58WWMenuDS_9_Tfmenu_nome_sel, lV59WWMenuDS_10_Tfmenu_descricao, AV60WWMenuDS_11_Tfmenu_descricao_sel, AV62WWMenuDS_13_Tfmenu_ordem, AV63WWMenuDS_14_Tfmenu_ordem_to, lV64WWMenuDS_15_Tfmenu_painom, AV65WWMenuDS_16_Tfmenu_painom_sel});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKSP2 = false;
            A285Menu_PaiCod = P00SP2_A285Menu_PaiCod[0];
            n285Menu_PaiCod = P00SP2_n285Menu_PaiCod[0];
            A278Menu_Nome = P00SP2_A278Menu_Nome[0];
            A284Menu_Ativo = P00SP2_A284Menu_Ativo[0];
            A283Menu_Ordem = P00SP2_A283Menu_Ordem[0];
            A280Menu_Tipo = P00SP2_A280Menu_Tipo[0];
            A279Menu_Descricao = P00SP2_A279Menu_Descricao[0];
            n279Menu_Descricao = P00SP2_n279Menu_Descricao[0];
            A286Menu_PaiNom = P00SP2_A286Menu_PaiNom[0];
            n286Menu_PaiNom = P00SP2_n286Menu_PaiNom[0];
            A277Menu_Codigo = P00SP2_A277Menu_Codigo[0];
            A286Menu_PaiNom = P00SP2_A286Menu_PaiNom[0];
            n286Menu_PaiNom = P00SP2_n286Menu_PaiNom[0];
            AV33count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( StringUtil.StrCmp(P00SP2_A278Menu_Nome[0], A278Menu_Nome) == 0 ) )
            {
               BRKSP2 = false;
               A277Menu_Codigo = P00SP2_A277Menu_Codigo[0];
               AV33count = (long)(AV33count+1);
               BRKSP2 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A278Menu_Nome)) )
            {
               AV25Option = A278Menu_Nome;
               AV26Options.Add(AV25Option, 0);
               AV31OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV33count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV26Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKSP2 )
            {
               BRKSP2 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      protected void S131( )
      {
         /* 'LOADMENU_DESCRICAOOPTIONS' Routine */
         AV12TFMenu_Descricao = AV21SearchTxt;
         AV13TFMenu_Descricao_Sel = "";
         AV50WWMenuDS_1_Dynamicfiltersselector1 = AV39DynamicFiltersSelector1;
         AV51WWMenuDS_2_Menu_nome1 = AV40Menu_Nome1;
         AV52WWMenuDS_3_Menu_painom1 = AV41Menu_PaiNom1;
         AV53WWMenuDS_4_Dynamicfiltersenabled2 = AV42DynamicFiltersEnabled2;
         AV54WWMenuDS_5_Dynamicfiltersselector2 = AV43DynamicFiltersSelector2;
         AV55WWMenuDS_6_Menu_nome2 = AV44Menu_Nome2;
         AV56WWMenuDS_7_Menu_painom2 = AV45Menu_PaiNom2;
         AV57WWMenuDS_8_Tfmenu_nome = AV10TFMenu_Nome;
         AV58WWMenuDS_9_Tfmenu_nome_sel = AV11TFMenu_Nome_Sel;
         AV59WWMenuDS_10_Tfmenu_descricao = AV12TFMenu_Descricao;
         AV60WWMenuDS_11_Tfmenu_descricao_sel = AV13TFMenu_Descricao_Sel;
         AV61WWMenuDS_12_Tfmenu_tipo_sels = AV15TFMenu_Tipo_Sels;
         AV62WWMenuDS_13_Tfmenu_ordem = AV16TFMenu_Ordem;
         AV63WWMenuDS_14_Tfmenu_ordem_to = AV17TFMenu_Ordem_To;
         AV64WWMenuDS_15_Tfmenu_painom = AV18TFMenu_PaiNom;
         AV65WWMenuDS_16_Tfmenu_painom_sel = AV19TFMenu_PaiNom_Sel;
         AV66WWMenuDS_17_Tfmenu_ativo_sel = AV20TFMenu_Ativo_Sel;
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              A280Menu_Tipo ,
                                              AV61WWMenuDS_12_Tfmenu_tipo_sels ,
                                              AV50WWMenuDS_1_Dynamicfiltersselector1 ,
                                              AV51WWMenuDS_2_Menu_nome1 ,
                                              AV52WWMenuDS_3_Menu_painom1 ,
                                              AV53WWMenuDS_4_Dynamicfiltersenabled2 ,
                                              AV54WWMenuDS_5_Dynamicfiltersselector2 ,
                                              AV55WWMenuDS_6_Menu_nome2 ,
                                              AV56WWMenuDS_7_Menu_painom2 ,
                                              AV58WWMenuDS_9_Tfmenu_nome_sel ,
                                              AV57WWMenuDS_8_Tfmenu_nome ,
                                              AV60WWMenuDS_11_Tfmenu_descricao_sel ,
                                              AV59WWMenuDS_10_Tfmenu_descricao ,
                                              AV61WWMenuDS_12_Tfmenu_tipo_sels.Count ,
                                              AV62WWMenuDS_13_Tfmenu_ordem ,
                                              AV63WWMenuDS_14_Tfmenu_ordem_to ,
                                              AV65WWMenuDS_16_Tfmenu_painom_sel ,
                                              AV64WWMenuDS_15_Tfmenu_painom ,
                                              AV66WWMenuDS_17_Tfmenu_ativo_sel ,
                                              A278Menu_Nome ,
                                              A286Menu_PaiNom ,
                                              A279Menu_Descricao ,
                                              A283Menu_Ordem ,
                                              A284Menu_Ativo },
                                              new int[] {
                                              TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                              }
         });
         lV51WWMenuDS_2_Menu_nome1 = StringUtil.PadR( StringUtil.RTrim( AV51WWMenuDS_2_Menu_nome1), 30, "%");
         lV52WWMenuDS_3_Menu_painom1 = StringUtil.PadR( StringUtil.RTrim( AV52WWMenuDS_3_Menu_painom1), 30, "%");
         lV55WWMenuDS_6_Menu_nome2 = StringUtil.PadR( StringUtil.RTrim( AV55WWMenuDS_6_Menu_nome2), 30, "%");
         lV56WWMenuDS_7_Menu_painom2 = StringUtil.PadR( StringUtil.RTrim( AV56WWMenuDS_7_Menu_painom2), 30, "%");
         lV57WWMenuDS_8_Tfmenu_nome = StringUtil.PadR( StringUtil.RTrim( AV57WWMenuDS_8_Tfmenu_nome), 30, "%");
         lV59WWMenuDS_10_Tfmenu_descricao = StringUtil.Concat( StringUtil.RTrim( AV59WWMenuDS_10_Tfmenu_descricao), "%", "");
         lV64WWMenuDS_15_Tfmenu_painom = StringUtil.PadR( StringUtil.RTrim( AV64WWMenuDS_15_Tfmenu_painom), 30, "%");
         /* Using cursor P00SP3 */
         pr_default.execute(1, new Object[] {lV51WWMenuDS_2_Menu_nome1, lV52WWMenuDS_3_Menu_painom1, lV55WWMenuDS_6_Menu_nome2, lV56WWMenuDS_7_Menu_painom2, lV57WWMenuDS_8_Tfmenu_nome, AV58WWMenuDS_9_Tfmenu_nome_sel, lV59WWMenuDS_10_Tfmenu_descricao, AV60WWMenuDS_11_Tfmenu_descricao_sel, AV62WWMenuDS_13_Tfmenu_ordem, AV63WWMenuDS_14_Tfmenu_ordem_to, lV64WWMenuDS_15_Tfmenu_painom, AV65WWMenuDS_16_Tfmenu_painom_sel});
         while ( (pr_default.getStatus(1) != 101) )
         {
            BRKSP4 = false;
            A285Menu_PaiCod = P00SP3_A285Menu_PaiCod[0];
            n285Menu_PaiCod = P00SP3_n285Menu_PaiCod[0];
            A279Menu_Descricao = P00SP3_A279Menu_Descricao[0];
            n279Menu_Descricao = P00SP3_n279Menu_Descricao[0];
            A284Menu_Ativo = P00SP3_A284Menu_Ativo[0];
            A283Menu_Ordem = P00SP3_A283Menu_Ordem[0];
            A280Menu_Tipo = P00SP3_A280Menu_Tipo[0];
            A286Menu_PaiNom = P00SP3_A286Menu_PaiNom[0];
            n286Menu_PaiNom = P00SP3_n286Menu_PaiNom[0];
            A278Menu_Nome = P00SP3_A278Menu_Nome[0];
            A277Menu_Codigo = P00SP3_A277Menu_Codigo[0];
            A286Menu_PaiNom = P00SP3_A286Menu_PaiNom[0];
            n286Menu_PaiNom = P00SP3_n286Menu_PaiNom[0];
            AV33count = 0;
            while ( (pr_default.getStatus(1) != 101) && ( StringUtil.StrCmp(P00SP3_A279Menu_Descricao[0], A279Menu_Descricao) == 0 ) )
            {
               BRKSP4 = false;
               A277Menu_Codigo = P00SP3_A277Menu_Codigo[0];
               AV33count = (long)(AV33count+1);
               BRKSP4 = true;
               pr_default.readNext(1);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A279Menu_Descricao)) )
            {
               AV25Option = A279Menu_Descricao;
               AV26Options.Add(AV25Option, 0);
               AV31OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV33count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV26Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKSP4 )
            {
               BRKSP4 = true;
               pr_default.readNext(1);
            }
         }
         pr_default.close(1);
      }

      protected void S141( )
      {
         /* 'LOADMENU_PAINOMOPTIONS' Routine */
         AV18TFMenu_PaiNom = AV21SearchTxt;
         AV19TFMenu_PaiNom_Sel = "";
         AV50WWMenuDS_1_Dynamicfiltersselector1 = AV39DynamicFiltersSelector1;
         AV51WWMenuDS_2_Menu_nome1 = AV40Menu_Nome1;
         AV52WWMenuDS_3_Menu_painom1 = AV41Menu_PaiNom1;
         AV53WWMenuDS_4_Dynamicfiltersenabled2 = AV42DynamicFiltersEnabled2;
         AV54WWMenuDS_5_Dynamicfiltersselector2 = AV43DynamicFiltersSelector2;
         AV55WWMenuDS_6_Menu_nome2 = AV44Menu_Nome2;
         AV56WWMenuDS_7_Menu_painom2 = AV45Menu_PaiNom2;
         AV57WWMenuDS_8_Tfmenu_nome = AV10TFMenu_Nome;
         AV58WWMenuDS_9_Tfmenu_nome_sel = AV11TFMenu_Nome_Sel;
         AV59WWMenuDS_10_Tfmenu_descricao = AV12TFMenu_Descricao;
         AV60WWMenuDS_11_Tfmenu_descricao_sel = AV13TFMenu_Descricao_Sel;
         AV61WWMenuDS_12_Tfmenu_tipo_sels = AV15TFMenu_Tipo_Sels;
         AV62WWMenuDS_13_Tfmenu_ordem = AV16TFMenu_Ordem;
         AV63WWMenuDS_14_Tfmenu_ordem_to = AV17TFMenu_Ordem_To;
         AV64WWMenuDS_15_Tfmenu_painom = AV18TFMenu_PaiNom;
         AV65WWMenuDS_16_Tfmenu_painom_sel = AV19TFMenu_PaiNom_Sel;
         AV66WWMenuDS_17_Tfmenu_ativo_sel = AV20TFMenu_Ativo_Sel;
         pr_default.dynParam(2, new Object[]{ new Object[]{
                                              A280Menu_Tipo ,
                                              AV61WWMenuDS_12_Tfmenu_tipo_sels ,
                                              AV50WWMenuDS_1_Dynamicfiltersselector1 ,
                                              AV51WWMenuDS_2_Menu_nome1 ,
                                              AV52WWMenuDS_3_Menu_painom1 ,
                                              AV53WWMenuDS_4_Dynamicfiltersenabled2 ,
                                              AV54WWMenuDS_5_Dynamicfiltersselector2 ,
                                              AV55WWMenuDS_6_Menu_nome2 ,
                                              AV56WWMenuDS_7_Menu_painom2 ,
                                              AV58WWMenuDS_9_Tfmenu_nome_sel ,
                                              AV57WWMenuDS_8_Tfmenu_nome ,
                                              AV60WWMenuDS_11_Tfmenu_descricao_sel ,
                                              AV59WWMenuDS_10_Tfmenu_descricao ,
                                              AV61WWMenuDS_12_Tfmenu_tipo_sels.Count ,
                                              AV62WWMenuDS_13_Tfmenu_ordem ,
                                              AV63WWMenuDS_14_Tfmenu_ordem_to ,
                                              AV65WWMenuDS_16_Tfmenu_painom_sel ,
                                              AV64WWMenuDS_15_Tfmenu_painom ,
                                              AV66WWMenuDS_17_Tfmenu_ativo_sel ,
                                              A278Menu_Nome ,
                                              A286Menu_PaiNom ,
                                              A279Menu_Descricao ,
                                              A283Menu_Ordem ,
                                              A284Menu_Ativo },
                                              new int[] {
                                              TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                              }
         });
         lV51WWMenuDS_2_Menu_nome1 = StringUtil.PadR( StringUtil.RTrim( AV51WWMenuDS_2_Menu_nome1), 30, "%");
         lV52WWMenuDS_3_Menu_painom1 = StringUtil.PadR( StringUtil.RTrim( AV52WWMenuDS_3_Menu_painom1), 30, "%");
         lV55WWMenuDS_6_Menu_nome2 = StringUtil.PadR( StringUtil.RTrim( AV55WWMenuDS_6_Menu_nome2), 30, "%");
         lV56WWMenuDS_7_Menu_painom2 = StringUtil.PadR( StringUtil.RTrim( AV56WWMenuDS_7_Menu_painom2), 30, "%");
         lV57WWMenuDS_8_Tfmenu_nome = StringUtil.PadR( StringUtil.RTrim( AV57WWMenuDS_8_Tfmenu_nome), 30, "%");
         lV59WWMenuDS_10_Tfmenu_descricao = StringUtil.Concat( StringUtil.RTrim( AV59WWMenuDS_10_Tfmenu_descricao), "%", "");
         lV64WWMenuDS_15_Tfmenu_painom = StringUtil.PadR( StringUtil.RTrim( AV64WWMenuDS_15_Tfmenu_painom), 30, "%");
         /* Using cursor P00SP4 */
         pr_default.execute(2, new Object[] {lV51WWMenuDS_2_Menu_nome1, lV52WWMenuDS_3_Menu_painom1, lV55WWMenuDS_6_Menu_nome2, lV56WWMenuDS_7_Menu_painom2, lV57WWMenuDS_8_Tfmenu_nome, AV58WWMenuDS_9_Tfmenu_nome_sel, lV59WWMenuDS_10_Tfmenu_descricao, AV60WWMenuDS_11_Tfmenu_descricao_sel, AV62WWMenuDS_13_Tfmenu_ordem, AV63WWMenuDS_14_Tfmenu_ordem_to, lV64WWMenuDS_15_Tfmenu_painom, AV65WWMenuDS_16_Tfmenu_painom_sel});
         while ( (pr_default.getStatus(2) != 101) )
         {
            BRKSP6 = false;
            A285Menu_PaiCod = P00SP4_A285Menu_PaiCod[0];
            n285Menu_PaiCod = P00SP4_n285Menu_PaiCod[0];
            A286Menu_PaiNom = P00SP4_A286Menu_PaiNom[0];
            n286Menu_PaiNom = P00SP4_n286Menu_PaiNom[0];
            A284Menu_Ativo = P00SP4_A284Menu_Ativo[0];
            A283Menu_Ordem = P00SP4_A283Menu_Ordem[0];
            A280Menu_Tipo = P00SP4_A280Menu_Tipo[0];
            A279Menu_Descricao = P00SP4_A279Menu_Descricao[0];
            n279Menu_Descricao = P00SP4_n279Menu_Descricao[0];
            A278Menu_Nome = P00SP4_A278Menu_Nome[0];
            A277Menu_Codigo = P00SP4_A277Menu_Codigo[0];
            A286Menu_PaiNom = P00SP4_A286Menu_PaiNom[0];
            n286Menu_PaiNom = P00SP4_n286Menu_PaiNom[0];
            AV33count = 0;
            while ( (pr_default.getStatus(2) != 101) && ( StringUtil.StrCmp(P00SP4_A286Menu_PaiNom[0], A286Menu_PaiNom) == 0 ) )
            {
               BRKSP6 = false;
               A285Menu_PaiCod = P00SP4_A285Menu_PaiCod[0];
               n285Menu_PaiCod = P00SP4_n285Menu_PaiCod[0];
               A277Menu_Codigo = P00SP4_A277Menu_Codigo[0];
               AV33count = (long)(AV33count+1);
               BRKSP6 = true;
               pr_default.readNext(2);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A286Menu_PaiNom)) )
            {
               AV25Option = A286Menu_PaiNom;
               AV26Options.Add(AV25Option, 0);
               AV31OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV33count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV26Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKSP6 )
            {
               BRKSP6 = true;
               pr_default.readNext(2);
            }
         }
         pr_default.close(2);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV26Options = new GxSimpleCollection();
         AV29OptionsDesc = new GxSimpleCollection();
         AV31OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV34Session = context.GetSession();
         AV36GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV37GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV10TFMenu_Nome = "";
         AV11TFMenu_Nome_Sel = "";
         AV12TFMenu_Descricao = "";
         AV13TFMenu_Descricao_Sel = "";
         AV14TFMenu_Tipo_SelsJson = "";
         AV15TFMenu_Tipo_Sels = new GxSimpleCollection();
         AV16TFMenu_Ordem = 0;
         AV17TFMenu_Ordem_To = 0;
         AV18TFMenu_PaiNom = "";
         AV19TFMenu_PaiNom_Sel = "";
         AV38GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV39DynamicFiltersSelector1 = "";
         AV40Menu_Nome1 = "";
         AV41Menu_PaiNom1 = "";
         AV43DynamicFiltersSelector2 = "";
         AV44Menu_Nome2 = "";
         AV45Menu_PaiNom2 = "";
         AV50WWMenuDS_1_Dynamicfiltersselector1 = "";
         AV51WWMenuDS_2_Menu_nome1 = "";
         AV52WWMenuDS_3_Menu_painom1 = "";
         AV54WWMenuDS_5_Dynamicfiltersselector2 = "";
         AV55WWMenuDS_6_Menu_nome2 = "";
         AV56WWMenuDS_7_Menu_painom2 = "";
         AV57WWMenuDS_8_Tfmenu_nome = "";
         AV58WWMenuDS_9_Tfmenu_nome_sel = "";
         AV59WWMenuDS_10_Tfmenu_descricao = "";
         AV60WWMenuDS_11_Tfmenu_descricao_sel = "";
         AV61WWMenuDS_12_Tfmenu_tipo_sels = new GxSimpleCollection();
         AV64WWMenuDS_15_Tfmenu_painom = "";
         AV65WWMenuDS_16_Tfmenu_painom_sel = "";
         scmdbuf = "";
         lV51WWMenuDS_2_Menu_nome1 = "";
         lV52WWMenuDS_3_Menu_painom1 = "";
         lV55WWMenuDS_6_Menu_nome2 = "";
         lV56WWMenuDS_7_Menu_painom2 = "";
         lV57WWMenuDS_8_Tfmenu_nome = "";
         lV59WWMenuDS_10_Tfmenu_descricao = "";
         lV64WWMenuDS_15_Tfmenu_painom = "";
         A278Menu_Nome = "";
         A286Menu_PaiNom = "";
         A279Menu_Descricao = "";
         P00SP2_A285Menu_PaiCod = new int[1] ;
         P00SP2_n285Menu_PaiCod = new bool[] {false} ;
         P00SP2_A278Menu_Nome = new String[] {""} ;
         P00SP2_A284Menu_Ativo = new bool[] {false} ;
         P00SP2_A283Menu_Ordem = new short[1] ;
         P00SP2_A280Menu_Tipo = new short[1] ;
         P00SP2_A279Menu_Descricao = new String[] {""} ;
         P00SP2_n279Menu_Descricao = new bool[] {false} ;
         P00SP2_A286Menu_PaiNom = new String[] {""} ;
         P00SP2_n286Menu_PaiNom = new bool[] {false} ;
         P00SP2_A277Menu_Codigo = new int[1] ;
         AV25Option = "";
         P00SP3_A285Menu_PaiCod = new int[1] ;
         P00SP3_n285Menu_PaiCod = new bool[] {false} ;
         P00SP3_A279Menu_Descricao = new String[] {""} ;
         P00SP3_n279Menu_Descricao = new bool[] {false} ;
         P00SP3_A284Menu_Ativo = new bool[] {false} ;
         P00SP3_A283Menu_Ordem = new short[1] ;
         P00SP3_A280Menu_Tipo = new short[1] ;
         P00SP3_A286Menu_PaiNom = new String[] {""} ;
         P00SP3_n286Menu_PaiNom = new bool[] {false} ;
         P00SP3_A278Menu_Nome = new String[] {""} ;
         P00SP3_A277Menu_Codigo = new int[1] ;
         P00SP4_A285Menu_PaiCod = new int[1] ;
         P00SP4_n285Menu_PaiCod = new bool[] {false} ;
         P00SP4_A286Menu_PaiNom = new String[] {""} ;
         P00SP4_n286Menu_PaiNom = new bool[] {false} ;
         P00SP4_A284Menu_Ativo = new bool[] {false} ;
         P00SP4_A283Menu_Ordem = new short[1] ;
         P00SP4_A280Menu_Tipo = new short[1] ;
         P00SP4_A279Menu_Descricao = new String[] {""} ;
         P00SP4_n279Menu_Descricao = new bool[] {false} ;
         P00SP4_A278Menu_Nome = new String[] {""} ;
         P00SP4_A277Menu_Codigo = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getwwmenufilterdata__default(),
            new Object[][] {
                new Object[] {
               P00SP2_A285Menu_PaiCod, P00SP2_n285Menu_PaiCod, P00SP2_A278Menu_Nome, P00SP2_A284Menu_Ativo, P00SP2_A283Menu_Ordem, P00SP2_A280Menu_Tipo, P00SP2_A279Menu_Descricao, P00SP2_n279Menu_Descricao, P00SP2_A286Menu_PaiNom, P00SP2_n286Menu_PaiNom,
               P00SP2_A277Menu_Codigo
               }
               , new Object[] {
               P00SP3_A285Menu_PaiCod, P00SP3_n285Menu_PaiCod, P00SP3_A279Menu_Descricao, P00SP3_n279Menu_Descricao, P00SP3_A284Menu_Ativo, P00SP3_A283Menu_Ordem, P00SP3_A280Menu_Tipo, P00SP3_A286Menu_PaiNom, P00SP3_n286Menu_PaiNom, P00SP3_A278Menu_Nome,
               P00SP3_A277Menu_Codigo
               }
               , new Object[] {
               P00SP4_A285Menu_PaiCod, P00SP4_n285Menu_PaiCod, P00SP4_A286Menu_PaiNom, P00SP4_n286Menu_PaiNom, P00SP4_A284Menu_Ativo, P00SP4_A283Menu_Ordem, P00SP4_A280Menu_Tipo, P00SP4_A279Menu_Descricao, P00SP4_n279Menu_Descricao, P00SP4_A278Menu_Nome,
               P00SP4_A277Menu_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV16TFMenu_Ordem ;
      private short AV17TFMenu_Ordem_To ;
      private short AV20TFMenu_Ativo_Sel ;
      private short AV62WWMenuDS_13_Tfmenu_ordem ;
      private short AV63WWMenuDS_14_Tfmenu_ordem_to ;
      private short AV66WWMenuDS_17_Tfmenu_ativo_sel ;
      private short A280Menu_Tipo ;
      private short A283Menu_Ordem ;
      private int AV48GXV1 ;
      private int AV61WWMenuDS_12_Tfmenu_tipo_sels_Count ;
      private int A285Menu_PaiCod ;
      private int A277Menu_Codigo ;
      private long AV33count ;
      private String AV10TFMenu_Nome ;
      private String AV11TFMenu_Nome_Sel ;
      private String AV18TFMenu_PaiNom ;
      private String AV19TFMenu_PaiNom_Sel ;
      private String AV40Menu_Nome1 ;
      private String AV41Menu_PaiNom1 ;
      private String AV44Menu_Nome2 ;
      private String AV45Menu_PaiNom2 ;
      private String AV51WWMenuDS_2_Menu_nome1 ;
      private String AV52WWMenuDS_3_Menu_painom1 ;
      private String AV55WWMenuDS_6_Menu_nome2 ;
      private String AV56WWMenuDS_7_Menu_painom2 ;
      private String AV57WWMenuDS_8_Tfmenu_nome ;
      private String AV58WWMenuDS_9_Tfmenu_nome_sel ;
      private String AV64WWMenuDS_15_Tfmenu_painom ;
      private String AV65WWMenuDS_16_Tfmenu_painom_sel ;
      private String scmdbuf ;
      private String lV51WWMenuDS_2_Menu_nome1 ;
      private String lV52WWMenuDS_3_Menu_painom1 ;
      private String lV55WWMenuDS_6_Menu_nome2 ;
      private String lV56WWMenuDS_7_Menu_painom2 ;
      private String lV57WWMenuDS_8_Tfmenu_nome ;
      private String lV64WWMenuDS_15_Tfmenu_painom ;
      private String A278Menu_Nome ;
      private String A286Menu_PaiNom ;
      private bool returnInSub ;
      private bool AV42DynamicFiltersEnabled2 ;
      private bool AV53WWMenuDS_4_Dynamicfiltersenabled2 ;
      private bool A284Menu_Ativo ;
      private bool BRKSP2 ;
      private bool n285Menu_PaiCod ;
      private bool n279Menu_Descricao ;
      private bool n286Menu_PaiNom ;
      private bool BRKSP4 ;
      private bool BRKSP6 ;
      private String AV32OptionIndexesJson ;
      private String AV27OptionsJson ;
      private String AV30OptionsDescJson ;
      private String AV14TFMenu_Tipo_SelsJson ;
      private String AV23DDOName ;
      private String AV21SearchTxt ;
      private String AV22SearchTxtTo ;
      private String AV12TFMenu_Descricao ;
      private String AV13TFMenu_Descricao_Sel ;
      private String AV39DynamicFiltersSelector1 ;
      private String AV43DynamicFiltersSelector2 ;
      private String AV50WWMenuDS_1_Dynamicfiltersselector1 ;
      private String AV54WWMenuDS_5_Dynamicfiltersselector2 ;
      private String AV59WWMenuDS_10_Tfmenu_descricao ;
      private String AV60WWMenuDS_11_Tfmenu_descricao_sel ;
      private String lV59WWMenuDS_10_Tfmenu_descricao ;
      private String A279Menu_Descricao ;
      private String AV25Option ;
      private IGxSession AV34Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00SP2_A285Menu_PaiCod ;
      private bool[] P00SP2_n285Menu_PaiCod ;
      private String[] P00SP2_A278Menu_Nome ;
      private bool[] P00SP2_A284Menu_Ativo ;
      private short[] P00SP2_A283Menu_Ordem ;
      private short[] P00SP2_A280Menu_Tipo ;
      private String[] P00SP2_A279Menu_Descricao ;
      private bool[] P00SP2_n279Menu_Descricao ;
      private String[] P00SP2_A286Menu_PaiNom ;
      private bool[] P00SP2_n286Menu_PaiNom ;
      private int[] P00SP2_A277Menu_Codigo ;
      private int[] P00SP3_A285Menu_PaiCod ;
      private bool[] P00SP3_n285Menu_PaiCod ;
      private String[] P00SP3_A279Menu_Descricao ;
      private bool[] P00SP3_n279Menu_Descricao ;
      private bool[] P00SP3_A284Menu_Ativo ;
      private short[] P00SP3_A283Menu_Ordem ;
      private short[] P00SP3_A280Menu_Tipo ;
      private String[] P00SP3_A286Menu_PaiNom ;
      private bool[] P00SP3_n286Menu_PaiNom ;
      private String[] P00SP3_A278Menu_Nome ;
      private int[] P00SP3_A277Menu_Codigo ;
      private int[] P00SP4_A285Menu_PaiCod ;
      private bool[] P00SP4_n285Menu_PaiCod ;
      private String[] P00SP4_A286Menu_PaiNom ;
      private bool[] P00SP4_n286Menu_PaiNom ;
      private bool[] P00SP4_A284Menu_Ativo ;
      private short[] P00SP4_A283Menu_Ordem ;
      private short[] P00SP4_A280Menu_Tipo ;
      private String[] P00SP4_A279Menu_Descricao ;
      private bool[] P00SP4_n279Menu_Descricao ;
      private String[] P00SP4_A278Menu_Nome ;
      private int[] P00SP4_A277Menu_Codigo ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV15TFMenu_Tipo_Sels ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV61WWMenuDS_12_Tfmenu_tipo_sels ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV26Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV29OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV31OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV36GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV37GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV38GridStateDynamicFilter ;
   }

   public class getwwmenufilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00SP2( IGxContext context ,
                                             short A280Menu_Tipo ,
                                             IGxCollection AV61WWMenuDS_12_Tfmenu_tipo_sels ,
                                             String AV50WWMenuDS_1_Dynamicfiltersselector1 ,
                                             String AV51WWMenuDS_2_Menu_nome1 ,
                                             String AV52WWMenuDS_3_Menu_painom1 ,
                                             bool AV53WWMenuDS_4_Dynamicfiltersenabled2 ,
                                             String AV54WWMenuDS_5_Dynamicfiltersselector2 ,
                                             String AV55WWMenuDS_6_Menu_nome2 ,
                                             String AV56WWMenuDS_7_Menu_painom2 ,
                                             String AV58WWMenuDS_9_Tfmenu_nome_sel ,
                                             String AV57WWMenuDS_8_Tfmenu_nome ,
                                             String AV60WWMenuDS_11_Tfmenu_descricao_sel ,
                                             String AV59WWMenuDS_10_Tfmenu_descricao ,
                                             int AV61WWMenuDS_12_Tfmenu_tipo_sels_Count ,
                                             short AV62WWMenuDS_13_Tfmenu_ordem ,
                                             short AV63WWMenuDS_14_Tfmenu_ordem_to ,
                                             String AV65WWMenuDS_16_Tfmenu_painom_sel ,
                                             String AV64WWMenuDS_15_Tfmenu_painom ,
                                             short AV66WWMenuDS_17_Tfmenu_ativo_sel ,
                                             String A278Menu_Nome ,
                                             String A286Menu_PaiNom ,
                                             String A279Menu_Descricao ,
                                             short A283Menu_Ordem ,
                                             bool A284Menu_Ativo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [12] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT T1.[Menu_PaiCod] AS Menu_PaiCod, T1.[Menu_Nome], T1.[Menu_Ativo], T1.[Menu_Ordem], T1.[Menu_Tipo], T1.[Menu_Descricao], T2.[Menu_Nome] AS Menu_PaiNom, T1.[Menu_Codigo] FROM ([Menu] T1 WITH (NOLOCK) LEFT JOIN [Menu] T2 WITH (NOLOCK) ON T2.[Menu_Codigo] = T1.[Menu_PaiCod])";
         if ( ( StringUtil.StrCmp(AV50WWMenuDS_1_Dynamicfiltersselector1, "MENU_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV51WWMenuDS_2_Menu_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Menu_Nome] like '%' + @lV51WWMenuDS_2_Menu_nome1 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Menu_Nome] like '%' + @lV51WWMenuDS_2_Menu_nome1 + '%')";
            }
         }
         else
         {
            GXv_int1[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV50WWMenuDS_1_Dynamicfiltersselector1, "MENU_PAINOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV52WWMenuDS_3_Menu_painom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Menu_Nome] like '%' + @lV52WWMenuDS_3_Menu_painom1 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Menu_Nome] like '%' + @lV52WWMenuDS_3_Menu_painom1 + '%')";
            }
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( AV53WWMenuDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV54WWMenuDS_5_Dynamicfiltersselector2, "MENU_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV55WWMenuDS_6_Menu_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Menu_Nome] like '%' + @lV55WWMenuDS_6_Menu_nome2 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Menu_Nome] like '%' + @lV55WWMenuDS_6_Menu_nome2 + '%')";
            }
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( AV53WWMenuDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV54WWMenuDS_5_Dynamicfiltersselector2, "MENU_PAINOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV56WWMenuDS_7_Menu_painom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Menu_Nome] like '%' + @lV56WWMenuDS_7_Menu_painom2 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Menu_Nome] like '%' + @lV56WWMenuDS_7_Menu_painom2 + '%')";
            }
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV58WWMenuDS_9_Tfmenu_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV57WWMenuDS_8_Tfmenu_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Menu_Nome] like @lV57WWMenuDS_8_Tfmenu_nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Menu_Nome] like @lV57WWMenuDS_8_Tfmenu_nome)";
            }
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV58WWMenuDS_9_Tfmenu_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Menu_Nome] = @AV58WWMenuDS_9_Tfmenu_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Menu_Nome] = @AV58WWMenuDS_9_Tfmenu_nome_sel)";
            }
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV60WWMenuDS_11_Tfmenu_descricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV59WWMenuDS_10_Tfmenu_descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Menu_Descricao] like @lV59WWMenuDS_10_Tfmenu_descricao)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Menu_Descricao] like @lV59WWMenuDS_10_Tfmenu_descricao)";
            }
         }
         else
         {
            GXv_int1[6] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60WWMenuDS_11_Tfmenu_descricao_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Menu_Descricao] = @AV60WWMenuDS_11_Tfmenu_descricao_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Menu_Descricao] = @AV60WWMenuDS_11_Tfmenu_descricao_sel)";
            }
         }
         else
         {
            GXv_int1[7] = 1;
         }
         if ( AV61WWMenuDS_12_Tfmenu_tipo_sels_Count > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV61WWMenuDS_12_Tfmenu_tipo_sels, "T1.[Menu_Tipo] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV61WWMenuDS_12_Tfmenu_tipo_sels, "T1.[Menu_Tipo] IN (", ")") + ")";
            }
         }
         if ( ! (0==AV62WWMenuDS_13_Tfmenu_ordem) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Menu_Ordem] >= @AV62WWMenuDS_13_Tfmenu_ordem)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Menu_Ordem] >= @AV62WWMenuDS_13_Tfmenu_ordem)";
            }
         }
         else
         {
            GXv_int1[8] = 1;
         }
         if ( ! (0==AV63WWMenuDS_14_Tfmenu_ordem_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Menu_Ordem] <= @AV63WWMenuDS_14_Tfmenu_ordem_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Menu_Ordem] <= @AV63WWMenuDS_14_Tfmenu_ordem_to)";
            }
         }
         else
         {
            GXv_int1[9] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV65WWMenuDS_16_Tfmenu_painom_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV64WWMenuDS_15_Tfmenu_painom)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Menu_Nome] like @lV64WWMenuDS_15_Tfmenu_painom)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Menu_Nome] like @lV64WWMenuDS_15_Tfmenu_painom)";
            }
         }
         else
         {
            GXv_int1[10] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV65WWMenuDS_16_Tfmenu_painom_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Menu_Nome] = @AV65WWMenuDS_16_Tfmenu_painom_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Menu_Nome] = @AV65WWMenuDS_16_Tfmenu_painom_sel)";
            }
         }
         else
         {
            GXv_int1[11] = 1;
         }
         if ( AV66WWMenuDS_17_Tfmenu_ativo_sel == 1 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Menu_Ativo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Menu_Ativo] = 1)";
            }
         }
         if ( AV66WWMenuDS_17_Tfmenu_ativo_sel == 2 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Menu_Ativo] = 0)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Menu_Ativo] = 0)";
            }
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T1.[Menu_Nome]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      protected Object[] conditional_P00SP3( IGxContext context ,
                                             short A280Menu_Tipo ,
                                             IGxCollection AV61WWMenuDS_12_Tfmenu_tipo_sels ,
                                             String AV50WWMenuDS_1_Dynamicfiltersselector1 ,
                                             String AV51WWMenuDS_2_Menu_nome1 ,
                                             String AV52WWMenuDS_3_Menu_painom1 ,
                                             bool AV53WWMenuDS_4_Dynamicfiltersenabled2 ,
                                             String AV54WWMenuDS_5_Dynamicfiltersselector2 ,
                                             String AV55WWMenuDS_6_Menu_nome2 ,
                                             String AV56WWMenuDS_7_Menu_painom2 ,
                                             String AV58WWMenuDS_9_Tfmenu_nome_sel ,
                                             String AV57WWMenuDS_8_Tfmenu_nome ,
                                             String AV60WWMenuDS_11_Tfmenu_descricao_sel ,
                                             String AV59WWMenuDS_10_Tfmenu_descricao ,
                                             int AV61WWMenuDS_12_Tfmenu_tipo_sels_Count ,
                                             short AV62WWMenuDS_13_Tfmenu_ordem ,
                                             short AV63WWMenuDS_14_Tfmenu_ordem_to ,
                                             String AV65WWMenuDS_16_Tfmenu_painom_sel ,
                                             String AV64WWMenuDS_15_Tfmenu_painom ,
                                             short AV66WWMenuDS_17_Tfmenu_ativo_sel ,
                                             String A278Menu_Nome ,
                                             String A286Menu_PaiNom ,
                                             String A279Menu_Descricao ,
                                             short A283Menu_Ordem ,
                                             bool A284Menu_Ativo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [12] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT T1.[Menu_PaiCod] AS Menu_PaiCod, T1.[Menu_Descricao], T1.[Menu_Ativo], T1.[Menu_Ordem], T1.[Menu_Tipo], T2.[Menu_Nome] AS Menu_PaiNom, T1.[Menu_Nome], T1.[Menu_Codigo] FROM ([Menu] T1 WITH (NOLOCK) LEFT JOIN [Menu] T2 WITH (NOLOCK) ON T2.[Menu_Codigo] = T1.[Menu_PaiCod])";
         if ( ( StringUtil.StrCmp(AV50WWMenuDS_1_Dynamicfiltersselector1, "MENU_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV51WWMenuDS_2_Menu_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Menu_Nome] like '%' + @lV51WWMenuDS_2_Menu_nome1 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Menu_Nome] like '%' + @lV51WWMenuDS_2_Menu_nome1 + '%')";
            }
         }
         else
         {
            GXv_int3[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV50WWMenuDS_1_Dynamicfiltersselector1, "MENU_PAINOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV52WWMenuDS_3_Menu_painom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Menu_Nome] like '%' + @lV52WWMenuDS_3_Menu_painom1 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Menu_Nome] like '%' + @lV52WWMenuDS_3_Menu_painom1 + '%')";
            }
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( AV53WWMenuDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV54WWMenuDS_5_Dynamicfiltersselector2, "MENU_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV55WWMenuDS_6_Menu_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Menu_Nome] like '%' + @lV55WWMenuDS_6_Menu_nome2 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Menu_Nome] like '%' + @lV55WWMenuDS_6_Menu_nome2 + '%')";
            }
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( AV53WWMenuDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV54WWMenuDS_5_Dynamicfiltersselector2, "MENU_PAINOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV56WWMenuDS_7_Menu_painom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Menu_Nome] like '%' + @lV56WWMenuDS_7_Menu_painom2 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Menu_Nome] like '%' + @lV56WWMenuDS_7_Menu_painom2 + '%')";
            }
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV58WWMenuDS_9_Tfmenu_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV57WWMenuDS_8_Tfmenu_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Menu_Nome] like @lV57WWMenuDS_8_Tfmenu_nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Menu_Nome] like @lV57WWMenuDS_8_Tfmenu_nome)";
            }
         }
         else
         {
            GXv_int3[4] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV58WWMenuDS_9_Tfmenu_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Menu_Nome] = @AV58WWMenuDS_9_Tfmenu_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Menu_Nome] = @AV58WWMenuDS_9_Tfmenu_nome_sel)";
            }
         }
         else
         {
            GXv_int3[5] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV60WWMenuDS_11_Tfmenu_descricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV59WWMenuDS_10_Tfmenu_descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Menu_Descricao] like @lV59WWMenuDS_10_Tfmenu_descricao)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Menu_Descricao] like @lV59WWMenuDS_10_Tfmenu_descricao)";
            }
         }
         else
         {
            GXv_int3[6] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60WWMenuDS_11_Tfmenu_descricao_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Menu_Descricao] = @AV60WWMenuDS_11_Tfmenu_descricao_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Menu_Descricao] = @AV60WWMenuDS_11_Tfmenu_descricao_sel)";
            }
         }
         else
         {
            GXv_int3[7] = 1;
         }
         if ( AV61WWMenuDS_12_Tfmenu_tipo_sels_Count > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV61WWMenuDS_12_Tfmenu_tipo_sels, "T1.[Menu_Tipo] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV61WWMenuDS_12_Tfmenu_tipo_sels, "T1.[Menu_Tipo] IN (", ")") + ")";
            }
         }
         if ( ! (0==AV62WWMenuDS_13_Tfmenu_ordem) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Menu_Ordem] >= @AV62WWMenuDS_13_Tfmenu_ordem)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Menu_Ordem] >= @AV62WWMenuDS_13_Tfmenu_ordem)";
            }
         }
         else
         {
            GXv_int3[8] = 1;
         }
         if ( ! (0==AV63WWMenuDS_14_Tfmenu_ordem_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Menu_Ordem] <= @AV63WWMenuDS_14_Tfmenu_ordem_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Menu_Ordem] <= @AV63WWMenuDS_14_Tfmenu_ordem_to)";
            }
         }
         else
         {
            GXv_int3[9] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV65WWMenuDS_16_Tfmenu_painom_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV64WWMenuDS_15_Tfmenu_painom)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Menu_Nome] like @lV64WWMenuDS_15_Tfmenu_painom)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Menu_Nome] like @lV64WWMenuDS_15_Tfmenu_painom)";
            }
         }
         else
         {
            GXv_int3[10] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV65WWMenuDS_16_Tfmenu_painom_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Menu_Nome] = @AV65WWMenuDS_16_Tfmenu_painom_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Menu_Nome] = @AV65WWMenuDS_16_Tfmenu_painom_sel)";
            }
         }
         else
         {
            GXv_int3[11] = 1;
         }
         if ( AV66WWMenuDS_17_Tfmenu_ativo_sel == 1 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Menu_Ativo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Menu_Ativo] = 1)";
            }
         }
         if ( AV66WWMenuDS_17_Tfmenu_ativo_sel == 2 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Menu_Ativo] = 0)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Menu_Ativo] = 0)";
            }
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T1.[Menu_Descricao]";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      protected Object[] conditional_P00SP4( IGxContext context ,
                                             short A280Menu_Tipo ,
                                             IGxCollection AV61WWMenuDS_12_Tfmenu_tipo_sels ,
                                             String AV50WWMenuDS_1_Dynamicfiltersselector1 ,
                                             String AV51WWMenuDS_2_Menu_nome1 ,
                                             String AV52WWMenuDS_3_Menu_painom1 ,
                                             bool AV53WWMenuDS_4_Dynamicfiltersenabled2 ,
                                             String AV54WWMenuDS_5_Dynamicfiltersselector2 ,
                                             String AV55WWMenuDS_6_Menu_nome2 ,
                                             String AV56WWMenuDS_7_Menu_painom2 ,
                                             String AV58WWMenuDS_9_Tfmenu_nome_sel ,
                                             String AV57WWMenuDS_8_Tfmenu_nome ,
                                             String AV60WWMenuDS_11_Tfmenu_descricao_sel ,
                                             String AV59WWMenuDS_10_Tfmenu_descricao ,
                                             int AV61WWMenuDS_12_Tfmenu_tipo_sels_Count ,
                                             short AV62WWMenuDS_13_Tfmenu_ordem ,
                                             short AV63WWMenuDS_14_Tfmenu_ordem_to ,
                                             String AV65WWMenuDS_16_Tfmenu_painom_sel ,
                                             String AV64WWMenuDS_15_Tfmenu_painom ,
                                             short AV66WWMenuDS_17_Tfmenu_ativo_sel ,
                                             String A278Menu_Nome ,
                                             String A286Menu_PaiNom ,
                                             String A279Menu_Descricao ,
                                             short A283Menu_Ordem ,
                                             bool A284Menu_Ativo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int5 ;
         GXv_int5 = new short [12] ;
         Object[] GXv_Object6 ;
         GXv_Object6 = new Object [2] ;
         scmdbuf = "SELECT T1.[Menu_PaiCod] AS Menu_PaiCod, T2.[Menu_Nome] AS Menu_PaiNom, T1.[Menu_Ativo], T1.[Menu_Ordem], T1.[Menu_Tipo], T1.[Menu_Descricao], T1.[Menu_Nome], T1.[Menu_Codigo] FROM ([Menu] T1 WITH (NOLOCK) LEFT JOIN [Menu] T2 WITH (NOLOCK) ON T2.[Menu_Codigo] = T1.[Menu_PaiCod])";
         if ( ( StringUtil.StrCmp(AV50WWMenuDS_1_Dynamicfiltersselector1, "MENU_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV51WWMenuDS_2_Menu_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Menu_Nome] like '%' + @lV51WWMenuDS_2_Menu_nome1 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Menu_Nome] like '%' + @lV51WWMenuDS_2_Menu_nome1 + '%')";
            }
         }
         else
         {
            GXv_int5[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV50WWMenuDS_1_Dynamicfiltersselector1, "MENU_PAINOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV52WWMenuDS_3_Menu_painom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Menu_Nome] like '%' + @lV52WWMenuDS_3_Menu_painom1 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Menu_Nome] like '%' + @lV52WWMenuDS_3_Menu_painom1 + '%')";
            }
         }
         else
         {
            GXv_int5[1] = 1;
         }
         if ( AV53WWMenuDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV54WWMenuDS_5_Dynamicfiltersselector2, "MENU_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV55WWMenuDS_6_Menu_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Menu_Nome] like '%' + @lV55WWMenuDS_6_Menu_nome2 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Menu_Nome] like '%' + @lV55WWMenuDS_6_Menu_nome2 + '%')";
            }
         }
         else
         {
            GXv_int5[2] = 1;
         }
         if ( AV53WWMenuDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV54WWMenuDS_5_Dynamicfiltersselector2, "MENU_PAINOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV56WWMenuDS_7_Menu_painom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Menu_Nome] like '%' + @lV56WWMenuDS_7_Menu_painom2 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Menu_Nome] like '%' + @lV56WWMenuDS_7_Menu_painom2 + '%')";
            }
         }
         else
         {
            GXv_int5[3] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV58WWMenuDS_9_Tfmenu_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV57WWMenuDS_8_Tfmenu_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Menu_Nome] like @lV57WWMenuDS_8_Tfmenu_nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Menu_Nome] like @lV57WWMenuDS_8_Tfmenu_nome)";
            }
         }
         else
         {
            GXv_int5[4] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV58WWMenuDS_9_Tfmenu_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Menu_Nome] = @AV58WWMenuDS_9_Tfmenu_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Menu_Nome] = @AV58WWMenuDS_9_Tfmenu_nome_sel)";
            }
         }
         else
         {
            GXv_int5[5] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV60WWMenuDS_11_Tfmenu_descricao_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV59WWMenuDS_10_Tfmenu_descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Menu_Descricao] like @lV59WWMenuDS_10_Tfmenu_descricao)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Menu_Descricao] like @lV59WWMenuDS_10_Tfmenu_descricao)";
            }
         }
         else
         {
            GXv_int5[6] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60WWMenuDS_11_Tfmenu_descricao_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Menu_Descricao] = @AV60WWMenuDS_11_Tfmenu_descricao_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Menu_Descricao] = @AV60WWMenuDS_11_Tfmenu_descricao_sel)";
            }
         }
         else
         {
            GXv_int5[7] = 1;
         }
         if ( AV61WWMenuDS_12_Tfmenu_tipo_sels_Count > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV61WWMenuDS_12_Tfmenu_tipo_sels, "T1.[Menu_Tipo] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV61WWMenuDS_12_Tfmenu_tipo_sels, "T1.[Menu_Tipo] IN (", ")") + ")";
            }
         }
         if ( ! (0==AV62WWMenuDS_13_Tfmenu_ordem) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Menu_Ordem] >= @AV62WWMenuDS_13_Tfmenu_ordem)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Menu_Ordem] >= @AV62WWMenuDS_13_Tfmenu_ordem)";
            }
         }
         else
         {
            GXv_int5[8] = 1;
         }
         if ( ! (0==AV63WWMenuDS_14_Tfmenu_ordem_to) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Menu_Ordem] <= @AV63WWMenuDS_14_Tfmenu_ordem_to)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Menu_Ordem] <= @AV63WWMenuDS_14_Tfmenu_ordem_to)";
            }
         }
         else
         {
            GXv_int5[9] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV65WWMenuDS_16_Tfmenu_painom_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV64WWMenuDS_15_Tfmenu_painom)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Menu_Nome] like @lV64WWMenuDS_15_Tfmenu_painom)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Menu_Nome] like @lV64WWMenuDS_15_Tfmenu_painom)";
            }
         }
         else
         {
            GXv_int5[10] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV65WWMenuDS_16_Tfmenu_painom_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Menu_Nome] = @AV65WWMenuDS_16_Tfmenu_painom_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Menu_Nome] = @AV65WWMenuDS_16_Tfmenu_painom_sel)";
            }
         }
         else
         {
            GXv_int5[11] = 1;
         }
         if ( AV66WWMenuDS_17_Tfmenu_ativo_sel == 1 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Menu_Ativo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Menu_Ativo] = 1)";
            }
         }
         if ( AV66WWMenuDS_17_Tfmenu_ativo_sel == 2 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Menu_Ativo] = 0)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Menu_Ativo] = 0)";
            }
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T2.[Menu_Nome]";
         GXv_Object6[0] = scmdbuf;
         GXv_Object6[1] = GXv_int5;
         return GXv_Object6 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00SP2(context, (short)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (int)dynConstraints[13] , (short)dynConstraints[14] , (short)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (short)dynConstraints[18] , (String)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (short)dynConstraints[22] , (bool)dynConstraints[23] );
               case 1 :
                     return conditional_P00SP3(context, (short)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (int)dynConstraints[13] , (short)dynConstraints[14] , (short)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (short)dynConstraints[18] , (String)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (short)dynConstraints[22] , (bool)dynConstraints[23] );
               case 2 :
                     return conditional_P00SP4(context, (short)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (int)dynConstraints[13] , (short)dynConstraints[14] , (short)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (short)dynConstraints[18] , (String)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (short)dynConstraints[22] , (bool)dynConstraints[23] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00SP2 ;
          prmP00SP2 = new Object[] {
          new Object[] {"@lV51WWMenuDS_2_Menu_nome1",SqlDbType.Char,30,0} ,
          new Object[] {"@lV52WWMenuDS_3_Menu_painom1",SqlDbType.Char,30,0} ,
          new Object[] {"@lV55WWMenuDS_6_Menu_nome2",SqlDbType.Char,30,0} ,
          new Object[] {"@lV56WWMenuDS_7_Menu_painom2",SqlDbType.Char,30,0} ,
          new Object[] {"@lV57WWMenuDS_8_Tfmenu_nome",SqlDbType.Char,30,0} ,
          new Object[] {"@AV58WWMenuDS_9_Tfmenu_nome_sel",SqlDbType.Char,30,0} ,
          new Object[] {"@lV59WWMenuDS_10_Tfmenu_descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV60WWMenuDS_11_Tfmenu_descricao_sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV62WWMenuDS_13_Tfmenu_ordem",SqlDbType.SmallInt,3,0} ,
          new Object[] {"@AV63WWMenuDS_14_Tfmenu_ordem_to",SqlDbType.SmallInt,3,0} ,
          new Object[] {"@lV64WWMenuDS_15_Tfmenu_painom",SqlDbType.Char,30,0} ,
          new Object[] {"@AV65WWMenuDS_16_Tfmenu_painom_sel",SqlDbType.Char,30,0}
          } ;
          Object[] prmP00SP3 ;
          prmP00SP3 = new Object[] {
          new Object[] {"@lV51WWMenuDS_2_Menu_nome1",SqlDbType.Char,30,0} ,
          new Object[] {"@lV52WWMenuDS_3_Menu_painom1",SqlDbType.Char,30,0} ,
          new Object[] {"@lV55WWMenuDS_6_Menu_nome2",SqlDbType.Char,30,0} ,
          new Object[] {"@lV56WWMenuDS_7_Menu_painom2",SqlDbType.Char,30,0} ,
          new Object[] {"@lV57WWMenuDS_8_Tfmenu_nome",SqlDbType.Char,30,0} ,
          new Object[] {"@AV58WWMenuDS_9_Tfmenu_nome_sel",SqlDbType.Char,30,0} ,
          new Object[] {"@lV59WWMenuDS_10_Tfmenu_descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV60WWMenuDS_11_Tfmenu_descricao_sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV62WWMenuDS_13_Tfmenu_ordem",SqlDbType.SmallInt,3,0} ,
          new Object[] {"@AV63WWMenuDS_14_Tfmenu_ordem_to",SqlDbType.SmallInt,3,0} ,
          new Object[] {"@lV64WWMenuDS_15_Tfmenu_painom",SqlDbType.Char,30,0} ,
          new Object[] {"@AV65WWMenuDS_16_Tfmenu_painom_sel",SqlDbType.Char,30,0}
          } ;
          Object[] prmP00SP4 ;
          prmP00SP4 = new Object[] {
          new Object[] {"@lV51WWMenuDS_2_Menu_nome1",SqlDbType.Char,30,0} ,
          new Object[] {"@lV52WWMenuDS_3_Menu_painom1",SqlDbType.Char,30,0} ,
          new Object[] {"@lV55WWMenuDS_6_Menu_nome2",SqlDbType.Char,30,0} ,
          new Object[] {"@lV56WWMenuDS_7_Menu_painom2",SqlDbType.Char,30,0} ,
          new Object[] {"@lV57WWMenuDS_8_Tfmenu_nome",SqlDbType.Char,30,0} ,
          new Object[] {"@AV58WWMenuDS_9_Tfmenu_nome_sel",SqlDbType.Char,30,0} ,
          new Object[] {"@lV59WWMenuDS_10_Tfmenu_descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV60WWMenuDS_11_Tfmenu_descricao_sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV62WWMenuDS_13_Tfmenu_ordem",SqlDbType.SmallInt,3,0} ,
          new Object[] {"@AV63WWMenuDS_14_Tfmenu_ordem_to",SqlDbType.SmallInt,3,0} ,
          new Object[] {"@lV64WWMenuDS_15_Tfmenu_painom",SqlDbType.Char,30,0} ,
          new Object[] {"@AV65WWMenuDS_16_Tfmenu_painom_sel",SqlDbType.Char,30,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00SP2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00SP2,100,0,true,false )
             ,new CursorDef("P00SP3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00SP3,100,0,true,false )
             ,new CursorDef("P00SP4", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00SP4,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 30) ;
                ((bool[]) buf[3])[0] = rslt.getBool(3) ;
                ((short[]) buf[4])[0] = rslt.getShort(4) ;
                ((short[]) buf[5])[0] = rslt.getShort(5) ;
                ((String[]) buf[6])[0] = rslt.getVarchar(6) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(6);
                ((String[]) buf[8])[0] = rslt.getString(7, 30) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(7);
                ((int[]) buf[10])[0] = rslt.getInt(8) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((bool[]) buf[4])[0] = rslt.getBool(3) ;
                ((short[]) buf[5])[0] = rslt.getShort(4) ;
                ((short[]) buf[6])[0] = rslt.getShort(5) ;
                ((String[]) buf[7])[0] = rslt.getString(6, 30) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((String[]) buf[9])[0] = rslt.getString(7, 30) ;
                ((int[]) buf[10])[0] = rslt.getInt(8) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 30) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((bool[]) buf[4])[0] = rslt.getBool(3) ;
                ((short[]) buf[5])[0] = rslt.getShort(4) ;
                ((short[]) buf[6])[0] = rslt.getShort(5) ;
                ((String[]) buf[7])[0] = rslt.getVarchar(6) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((String[]) buf[9])[0] = rslt.getString(7, 30) ;
                ((int[]) buf[10])[0] = rslt.getInt(8) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[20]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[21]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[20]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[21]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                return;
             case 2 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[20]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[21]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getwwmenufilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getwwmenufilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getwwmenufilterdata") )
          {
             return  ;
          }
          getwwmenufilterdata worker = new getwwmenufilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
