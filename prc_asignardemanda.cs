/*
               File: PRC_AsignarDemanda
        Description: Asignar Demanda
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/2/2020 16:12:18.75
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_asignardemanda : GXProcedure
   {
      public prc_asignardemanda( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_asignardemanda( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_ContagemResultado_Codigo ,
                           int aP1_ContagemResultado_Responsavel ,
                           String aP2_Acao ,
                           String aP3_NovoStatus )
      {
         this.A456ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         this.AV8ContagemResultado_Responsavel = aP1_ContagemResultado_Responsavel;
         this.AV10Acao = aP2_Acao;
         this.AV11NovoStatus = aP3_NovoStatus;
         initialize();
         executePrivate();
         aP0_ContagemResultado_Codigo=this.A456ContagemResultado_Codigo;
      }

      public void executeSubmit( ref int aP0_ContagemResultado_Codigo ,
                                 int aP1_ContagemResultado_Responsavel ,
                                 String aP2_Acao ,
                                 String aP3_NovoStatus )
      {
         prc_asignardemanda objprc_asignardemanda;
         objprc_asignardemanda = new prc_asignardemanda();
         objprc_asignardemanda.A456ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         objprc_asignardemanda.AV8ContagemResultado_Responsavel = aP1_ContagemResultado_Responsavel;
         objprc_asignardemanda.AV10Acao = aP2_Acao;
         objprc_asignardemanda.AV11NovoStatus = aP3_NovoStatus;
         objprc_asignardemanda.context.SetSubmitInitialConfig(context);
         objprc_asignardemanda.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_asignardemanda);
         aP0_ContagemResultado_Codigo=this.A456ContagemResultado_Codigo;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_asignardemanda)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P006A2 */
         pr_default.execute(0, new Object[] {A456ContagemResultado_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A890ContagemResultado_Responsavel = P006A2_A890ContagemResultado_Responsavel[0];
            n890ContagemResultado_Responsavel = P006A2_n890ContagemResultado_Responsavel[0];
            A484ContagemResultado_StatusDmn = P006A2_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = P006A2_n484ContagemResultado_StatusDmn[0];
            if ( (0==AV8ContagemResultado_Responsavel) )
            {
               A890ContagemResultado_Responsavel = 0;
               n890ContagemResultado_Responsavel = false;
               n890ContagemResultado_Responsavel = true;
            }
            else
            {
               A890ContagemResultado_Responsavel = AV8ContagemResultado_Responsavel;
               n890ContagemResultado_Responsavel = false;
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV11NovoStatus)) )
            {
               A484ContagemResultado_StatusDmn = AV11NovoStatus;
               n484ContagemResultado_StatusDmn = false;
            }
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            /* Using cursor P006A3 */
            pr_default.execute(1, new Object[] {n890ContagemResultado_Responsavel, A890ContagemResultado_Responsavel, n484ContagemResultado_StatusDmn, A484ContagemResultado_StatusDmn, A456ContagemResultado_Codigo});
            pr_default.close(1);
            dsDefault.SmartCacheProvider.SetUpdated("ContagemResultado") ;
            if (true) break;
            /* Using cursor P006A4 */
            pr_default.execute(2, new Object[] {n890ContagemResultado_Responsavel, A890ContagemResultado_Responsavel, n484ContagemResultado_StatusDmn, A484ContagemResultado_StatusDmn, A456ContagemResultado_Codigo});
            pr_default.close(2);
            dsDefault.SmartCacheProvider.SetUpdated("ContagemResultado") ;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P006A2_A456ContagemResultado_Codigo = new int[1] ;
         P006A2_A890ContagemResultado_Responsavel = new int[1] ;
         P006A2_n890ContagemResultado_Responsavel = new bool[] {false} ;
         P006A2_A484ContagemResultado_StatusDmn = new String[] {""} ;
         P006A2_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         A484ContagemResultado_StatusDmn = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_asignardemanda__default(),
            new Object[][] {
                new Object[] {
               P006A2_A456ContagemResultado_Codigo, P006A2_A890ContagemResultado_Responsavel, P006A2_n890ContagemResultado_Responsavel, P006A2_A484ContagemResultado_StatusDmn, P006A2_n484ContagemResultado_StatusDmn
               }
               , new Object[] {
               }
               , new Object[] {
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int A456ContagemResultado_Codigo ;
      private int AV8ContagemResultado_Responsavel ;
      private int A890ContagemResultado_Responsavel ;
      private String AV10Acao ;
      private String AV11NovoStatus ;
      private String scmdbuf ;
      private String A484ContagemResultado_StatusDmn ;
      private bool n890ContagemResultado_Responsavel ;
      private bool n484ContagemResultado_StatusDmn ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_ContagemResultado_Codigo ;
      private IDataStoreProvider pr_default ;
      private int[] P006A2_A456ContagemResultado_Codigo ;
      private int[] P006A2_A890ContagemResultado_Responsavel ;
      private bool[] P006A2_n890ContagemResultado_Responsavel ;
      private String[] P006A2_A484ContagemResultado_StatusDmn ;
      private bool[] P006A2_n484ContagemResultado_StatusDmn ;
   }

   public class prc_asignardemanda__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new UpdateCursor(def[1])
         ,new UpdateCursor(def[2])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP006A2 ;
          prmP006A2 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP006A3 ;
          prmP006A3 = new Object[] {
          new Object[] {"@ContagemResultado_Responsavel",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_StatusDmn",SqlDbType.Char,1,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP006A4 ;
          prmP006A4 = new Object[] {
          new Object[] {"@ContagemResultado_Responsavel",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_StatusDmn",SqlDbType.Char,1,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P006A2", "SELECT TOP 1 [ContagemResultado_Codigo], [ContagemResultado_Responsavel], [ContagemResultado_StatusDmn] FROM [ContagemResultado] WITH (UPDLOCK) WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo ORDER BY [ContagemResultado_Codigo] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP006A2,1,0,true,true )
             ,new CursorDef("P006A3", "UPDATE [ContagemResultado] SET [ContagemResultado_Responsavel]=@ContagemResultado_Responsavel, [ContagemResultado_StatusDmn]=@ContagemResultado_StatusDmn  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP006A3)
             ,new CursorDef("P006A4", "UPDATE [ContagemResultado] SET [ContagemResultado_Responsavel]=@ContagemResultado_Responsavel, [ContagemResultado_StatusDmn]=@ContagemResultado_StatusDmn  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP006A4)
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getString(3, 1) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(2, (String)parms[3]);
                }
                stmt.SetParameter(3, (int)parms[4]);
                return;
             case 2 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(2, (String)parms[3]);
                }
                stmt.SetParameter(3, (int)parms[4]);
                return;
       }
    }

 }

}
