/*
               File: PRC_ServicoNome
        Description: Servico Nome
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:55:55.69
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_serviconome : GXProcedure
   {
      public prc_serviconome( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_serviconome( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_Servico_Codigo ,
                           out String aP1_Servico_Nome )
      {
         this.A155Servico_Codigo = aP0_Servico_Codigo;
         this.AV8Servico_Nome = "" ;
         initialize();
         executePrivate();
         aP0_Servico_Codigo=this.A155Servico_Codigo;
         aP1_Servico_Nome=this.AV8Servico_Nome;
      }

      public String executeUdp( ref int aP0_Servico_Codigo )
      {
         this.A155Servico_Codigo = aP0_Servico_Codigo;
         this.AV8Servico_Nome = "" ;
         initialize();
         executePrivate();
         aP0_Servico_Codigo=this.A155Servico_Codigo;
         aP1_Servico_Nome=this.AV8Servico_Nome;
         return AV8Servico_Nome ;
      }

      public void executeSubmit( ref int aP0_Servico_Codigo ,
                                 out String aP1_Servico_Nome )
      {
         prc_serviconome objprc_serviconome;
         objprc_serviconome = new prc_serviconome();
         objprc_serviconome.A155Servico_Codigo = aP0_Servico_Codigo;
         objprc_serviconome.AV8Servico_Nome = "" ;
         objprc_serviconome.context.SetSubmitInitialConfig(context);
         objprc_serviconome.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_serviconome);
         aP0_Servico_Codigo=this.A155Servico_Codigo;
         aP1_Servico_Nome=this.AV8Servico_Nome;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_serviconome)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P00VP2 */
         pr_default.execute(0, new Object[] {A155Servico_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A608Servico_Nome = P00VP2_A608Servico_Nome[0];
            AV8Servico_Nome = A608Servico_Nome;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00VP2_A155Servico_Codigo = new int[1] ;
         P00VP2_A608Servico_Nome = new String[] {""} ;
         A608Servico_Nome = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_serviconome__default(),
            new Object[][] {
                new Object[] {
               P00VP2_A155Servico_Codigo, P00VP2_A608Servico_Nome
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int A155Servico_Codigo ;
      private String AV8Servico_Nome ;
      private String scmdbuf ;
      private String A608Servico_Nome ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_Servico_Codigo ;
      private IDataStoreProvider pr_default ;
      private int[] P00VP2_A155Servico_Codigo ;
      private String[] P00VP2_A608Servico_Nome ;
      private String aP1_Servico_Nome ;
   }

   public class prc_serviconome__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00VP2 ;
          prmP00VP2 = new Object[] {
          new Object[] {"@Servico_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00VP2", "SELECT TOP 1 [Servico_Codigo], [Servico_Nome] FROM [Servico] WITH (NOLOCK) WHERE [Servico_Codigo] = @Servico_Codigo ORDER BY [Servico_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00VP2,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
