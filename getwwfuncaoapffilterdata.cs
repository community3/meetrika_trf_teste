/*
               File: GetWWFuncaoAPFFilterData
        Description: Get WWFuncao APFFilter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/3/2020 22:40:7.30
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getwwfuncaoapffilterdata : GXProcedure
   {
      public getwwfuncaoapffilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getwwfuncaoapffilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV28DDOName = aP0_DDOName;
         this.AV26SearchTxt = aP1_SearchTxt;
         this.AV27SearchTxtTo = aP2_SearchTxtTo;
         this.AV32OptionsJson = "" ;
         this.AV35OptionsDescJson = "" ;
         this.AV37OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV32OptionsJson;
         aP4_OptionsDescJson=this.AV35OptionsDescJson;
         aP5_OptionIndexesJson=this.AV37OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV28DDOName = aP0_DDOName;
         this.AV26SearchTxt = aP1_SearchTxt;
         this.AV27SearchTxtTo = aP2_SearchTxtTo;
         this.AV32OptionsJson = "" ;
         this.AV35OptionsDescJson = "" ;
         this.AV37OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV32OptionsJson;
         aP4_OptionsDescJson=this.AV35OptionsDescJson;
         aP5_OptionIndexesJson=this.AV37OptionIndexesJson;
         return AV37OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getwwfuncaoapffilterdata objgetwwfuncaoapffilterdata;
         objgetwwfuncaoapffilterdata = new getwwfuncaoapffilterdata();
         objgetwwfuncaoapffilterdata.AV28DDOName = aP0_DDOName;
         objgetwwfuncaoapffilterdata.AV26SearchTxt = aP1_SearchTxt;
         objgetwwfuncaoapffilterdata.AV27SearchTxtTo = aP2_SearchTxtTo;
         objgetwwfuncaoapffilterdata.AV32OptionsJson = "" ;
         objgetwwfuncaoapffilterdata.AV35OptionsDescJson = "" ;
         objgetwwfuncaoapffilterdata.AV37OptionIndexesJson = "" ;
         objgetwwfuncaoapffilterdata.context.SetSubmitInitialConfig(context);
         objgetwwfuncaoapffilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetwwfuncaoapffilterdata);
         aP3_OptionsJson=this.AV32OptionsJson;
         aP4_OptionsDescJson=this.AV35OptionsDescJson;
         aP5_OptionIndexesJson=this.AV37OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getwwfuncaoapffilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV31Options = (IGxCollection)(new GxSimpleCollection());
         AV34OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV36OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV28DDOName), "DDO_FUNCAOAPF_NOME") == 0 )
         {
            /* Execute user subroutine: 'LOADFUNCAOAPF_NOMEOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV28DDOName), "DDO_FUNCAOAPF_FUNAPFPAINOM") == 0 )
         {
            /* Execute user subroutine: 'LOADFUNCAOAPF_FUNAPFPAINOMOPTIONS' */
            S131 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV32OptionsJson = AV31Options.ToJSonString(false);
         AV35OptionsDescJson = AV34OptionsDesc.ToJSonString(false);
         AV37OptionIndexesJson = AV36OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV39Session.Get("WWFuncaoAPFGridState"), "") == 0 )
         {
            AV41GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "WWFuncaoAPFGridState"), "");
         }
         else
         {
            AV41GridState.FromXml(AV39Session.Get("WWFuncaoAPFGridState"), "");
         }
         AV89GXV1 = 1;
         while ( AV89GXV1 <= AV41GridState.gxTpr_Filtervalues.Count )
         {
            AV42GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV41GridState.gxTpr_Filtervalues.Item(AV89GXV1));
            if ( StringUtil.StrCmp(AV42GridStateFilterValue.gxTpr_Name, "TFFUNCAOAPF_NOME") == 0 )
            {
               AV10TFFuncaoAPF_Nome = AV42GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV42GridStateFilterValue.gxTpr_Name, "TFFUNCAOAPF_NOME_SEL") == 0 )
            {
               AV11TFFuncaoAPF_Nome_Sel = AV42GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV42GridStateFilterValue.gxTpr_Name, "TFFUNCAOAPF_TIPO_SEL") == 0 )
            {
               AV12TFFuncaoAPF_Tipo_SelsJson = AV42GridStateFilterValue.gxTpr_Value;
               AV13TFFuncaoAPF_Tipo_Sels.FromJSonString(AV12TFFuncaoAPF_Tipo_SelsJson);
            }
            else if ( StringUtil.StrCmp(AV42GridStateFilterValue.gxTpr_Name, "TFFUNCAOAPF_TD") == 0 )
            {
               AV14TFFuncaoAPF_TD = (short)(NumberUtil.Val( AV42GridStateFilterValue.gxTpr_Value, "."));
               AV15TFFuncaoAPF_TD_To = (short)(NumberUtil.Val( AV42GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV42GridStateFilterValue.gxTpr_Name, "TFFUNCAOAPF_AR") == 0 )
            {
               AV16TFFuncaoAPF_AR = (short)(NumberUtil.Val( AV42GridStateFilterValue.gxTpr_Value, "."));
               AV17TFFuncaoAPF_AR_To = (short)(NumberUtil.Val( AV42GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV42GridStateFilterValue.gxTpr_Name, "TFFUNCAOAPF_COMPLEXIDADE_SEL") == 0 )
            {
               AV18TFFuncaoAPF_Complexidade_SelsJson = AV42GridStateFilterValue.gxTpr_Value;
               AV19TFFuncaoAPF_Complexidade_Sels.FromJSonString(AV18TFFuncaoAPF_Complexidade_SelsJson);
            }
            else if ( StringUtil.StrCmp(AV42GridStateFilterValue.gxTpr_Name, "TFFUNCAOAPF_PF") == 0 )
            {
               AV20TFFuncaoAPF_PF = NumberUtil.Val( AV42GridStateFilterValue.gxTpr_Value, ".");
               AV21TFFuncaoAPF_PF_To = NumberUtil.Val( AV42GridStateFilterValue.gxTpr_Valueto, ".");
            }
            else if ( StringUtil.StrCmp(AV42GridStateFilterValue.gxTpr_Name, "TFFUNCAOAPF_FUNAPFPAINOM") == 0 )
            {
               AV22TFFuncaoAPF_FunAPFPaiNom = AV42GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV42GridStateFilterValue.gxTpr_Name, "TFFUNCAOAPF_FUNAPFPAINOM_SEL") == 0 )
            {
               AV23TFFuncaoAPF_FunAPFPaiNom_Sel = AV42GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV42GridStateFilterValue.gxTpr_Name, "TFFUNCAOAPF_ATIVO_SEL") == 0 )
            {
               AV24TFFuncaoAPF_Ativo_SelsJson = AV42GridStateFilterValue.gxTpr_Value;
               AV25TFFuncaoAPF_Ativo_Sels.FromJSonString(AV24TFFuncaoAPF_Ativo_SelsJson);
            }
            AV89GXV1 = (int)(AV89GXV1+1);
         }
         if ( AV41GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV43GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV41GridState.gxTpr_Dynamicfilters.Item(1));
            AV44DynamicFiltersSelector1 = AV43GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV44DynamicFiltersSelector1, "FUNCAOAPF_NOME") == 0 )
            {
               AV45FuncaoAPF_Nome1 = AV43GridStateDynamicFilter.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV44DynamicFiltersSelector1, "FUNCAOAPF_TIPO") == 0 )
            {
               AV46FuncaoAPF_Tipo1 = AV43GridStateDynamicFilter.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV44DynamicFiltersSelector1, "FUNCAOAPF_COMPLEXIDADE") == 0 )
            {
               AV47FuncaoAPF_Complexidade1 = AV43GridStateDynamicFilter.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV44DynamicFiltersSelector1, "FUNCAOAPF_ATIVO") == 0 )
            {
               AV48FuncaoAPF_Ativo1 = AV43GridStateDynamicFilter.gxTpr_Value;
            }
            if ( AV41GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV49DynamicFiltersEnabled2 = true;
               AV43GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV41GridState.gxTpr_Dynamicfilters.Item(2));
               AV50DynamicFiltersSelector2 = AV43GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV50DynamicFiltersSelector2, "FUNCAOAPF_NOME") == 0 )
               {
                  AV51FuncaoAPF_Nome2 = AV43GridStateDynamicFilter.gxTpr_Value;
               }
               else if ( StringUtil.StrCmp(AV50DynamicFiltersSelector2, "FUNCAOAPF_TIPO") == 0 )
               {
                  AV52FuncaoAPF_Tipo2 = AV43GridStateDynamicFilter.gxTpr_Value;
               }
               else if ( StringUtil.StrCmp(AV50DynamicFiltersSelector2, "FUNCAOAPF_COMPLEXIDADE") == 0 )
               {
                  AV53FuncaoAPF_Complexidade2 = AV43GridStateDynamicFilter.gxTpr_Value;
               }
               else if ( StringUtil.StrCmp(AV50DynamicFiltersSelector2, "FUNCAOAPF_ATIVO") == 0 )
               {
                  AV54FuncaoAPF_Ativo2 = AV43GridStateDynamicFilter.gxTpr_Value;
               }
               if ( AV41GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  AV55DynamicFiltersEnabled3 = true;
                  AV43GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV41GridState.gxTpr_Dynamicfilters.Item(3));
                  AV56DynamicFiltersSelector3 = AV43GridStateDynamicFilter.gxTpr_Selected;
                  if ( StringUtil.StrCmp(AV56DynamicFiltersSelector3, "FUNCAOAPF_NOME") == 0 )
                  {
                     AV57FuncaoAPF_Nome3 = AV43GridStateDynamicFilter.gxTpr_Value;
                  }
                  else if ( StringUtil.StrCmp(AV56DynamicFiltersSelector3, "FUNCAOAPF_TIPO") == 0 )
                  {
                     AV58FuncaoAPF_Tipo3 = AV43GridStateDynamicFilter.gxTpr_Value;
                  }
                  else if ( StringUtil.StrCmp(AV56DynamicFiltersSelector3, "FUNCAOAPF_COMPLEXIDADE") == 0 )
                  {
                     AV59FuncaoAPF_Complexidade3 = AV43GridStateDynamicFilter.gxTpr_Value;
                  }
                  else if ( StringUtil.StrCmp(AV56DynamicFiltersSelector3, "FUNCAOAPF_ATIVO") == 0 )
                  {
                     AV60FuncaoAPF_Ativo3 = AV43GridStateDynamicFilter.gxTpr_Value;
                  }
                  if ( AV41GridState.gxTpr_Dynamicfilters.Count >= 4 )
                  {
                     AV61DynamicFiltersEnabled4 = true;
                     AV43GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV41GridState.gxTpr_Dynamicfilters.Item(4));
                     AV62DynamicFiltersSelector4 = AV43GridStateDynamicFilter.gxTpr_Selected;
                     if ( StringUtil.StrCmp(AV62DynamicFiltersSelector4, "FUNCAOAPF_NOME") == 0 )
                     {
                        AV63FuncaoAPF_Nome4 = AV43GridStateDynamicFilter.gxTpr_Value;
                     }
                     else if ( StringUtil.StrCmp(AV62DynamicFiltersSelector4, "FUNCAOAPF_TIPO") == 0 )
                     {
                        AV64FuncaoAPF_Tipo4 = AV43GridStateDynamicFilter.gxTpr_Value;
                     }
                     else if ( StringUtil.StrCmp(AV62DynamicFiltersSelector4, "FUNCAOAPF_COMPLEXIDADE") == 0 )
                     {
                        AV65FuncaoAPF_Complexidade4 = AV43GridStateDynamicFilter.gxTpr_Value;
                     }
                     else if ( StringUtil.StrCmp(AV62DynamicFiltersSelector4, "FUNCAOAPF_ATIVO") == 0 )
                     {
                        AV66FuncaoAPF_Ativo4 = AV43GridStateDynamicFilter.gxTpr_Value;
                     }
                  }
               }
            }
         }
      }

      protected void S121( )
      {
         /* 'LOADFUNCAOAPF_NOMEOPTIONS' Routine */
         AV10TFFuncaoAPF_Nome = AV26SearchTxt;
         AV11TFFuncaoAPF_Nome_Sel = "";
         AV91WWFuncaoAPFDS_1_Dynamicfiltersselector1 = AV44DynamicFiltersSelector1;
         AV92WWFuncaoAPFDS_2_Funcaoapf_nome1 = AV45FuncaoAPF_Nome1;
         AV93WWFuncaoAPFDS_3_Funcaoapf_tipo1 = AV46FuncaoAPF_Tipo1;
         AV94WWFuncaoAPFDS_4_Funcaoapf_complexidade1 = AV47FuncaoAPF_Complexidade1;
         AV95WWFuncaoAPFDS_5_Funcaoapf_ativo1 = AV48FuncaoAPF_Ativo1;
         AV96WWFuncaoAPFDS_6_Dynamicfiltersenabled2 = AV49DynamicFiltersEnabled2;
         AV97WWFuncaoAPFDS_7_Dynamicfiltersselector2 = AV50DynamicFiltersSelector2;
         AV98WWFuncaoAPFDS_8_Funcaoapf_nome2 = AV51FuncaoAPF_Nome2;
         AV99WWFuncaoAPFDS_9_Funcaoapf_tipo2 = AV52FuncaoAPF_Tipo2;
         AV100WWFuncaoAPFDS_10_Funcaoapf_complexidade2 = AV53FuncaoAPF_Complexidade2;
         AV101WWFuncaoAPFDS_11_Funcaoapf_ativo2 = AV54FuncaoAPF_Ativo2;
         AV102WWFuncaoAPFDS_12_Dynamicfiltersenabled3 = AV55DynamicFiltersEnabled3;
         AV103WWFuncaoAPFDS_13_Dynamicfiltersselector3 = AV56DynamicFiltersSelector3;
         AV104WWFuncaoAPFDS_14_Funcaoapf_nome3 = AV57FuncaoAPF_Nome3;
         AV105WWFuncaoAPFDS_15_Funcaoapf_tipo3 = AV58FuncaoAPF_Tipo3;
         AV106WWFuncaoAPFDS_16_Funcaoapf_complexidade3 = AV59FuncaoAPF_Complexidade3;
         AV107WWFuncaoAPFDS_17_Funcaoapf_ativo3 = AV60FuncaoAPF_Ativo3;
         AV108WWFuncaoAPFDS_18_Dynamicfiltersenabled4 = AV61DynamicFiltersEnabled4;
         AV109WWFuncaoAPFDS_19_Dynamicfiltersselector4 = AV62DynamicFiltersSelector4;
         AV110WWFuncaoAPFDS_20_Funcaoapf_nome4 = AV63FuncaoAPF_Nome4;
         AV111WWFuncaoAPFDS_21_Funcaoapf_tipo4 = AV64FuncaoAPF_Tipo4;
         AV112WWFuncaoAPFDS_22_Funcaoapf_complexidade4 = AV65FuncaoAPF_Complexidade4;
         AV113WWFuncaoAPFDS_23_Funcaoapf_ativo4 = AV66FuncaoAPF_Ativo4;
         AV114WWFuncaoAPFDS_24_Tffuncaoapf_nome = AV10TFFuncaoAPF_Nome;
         AV115WWFuncaoAPFDS_25_Tffuncaoapf_nome_sel = AV11TFFuncaoAPF_Nome_Sel;
         AV116WWFuncaoAPFDS_26_Tffuncaoapf_tipo_sels = AV13TFFuncaoAPF_Tipo_Sels;
         AV117WWFuncaoAPFDS_27_Tffuncaoapf_td = AV14TFFuncaoAPF_TD;
         AV118WWFuncaoAPFDS_28_Tffuncaoapf_td_to = AV15TFFuncaoAPF_TD_To;
         AV119WWFuncaoAPFDS_29_Tffuncaoapf_ar = AV16TFFuncaoAPF_AR;
         AV120WWFuncaoAPFDS_30_Tffuncaoapf_ar_to = AV17TFFuncaoAPF_AR_To;
         AV121WWFuncaoAPFDS_31_Tffuncaoapf_complexidade_sels = AV19TFFuncaoAPF_Complexidade_Sels;
         AV122WWFuncaoAPFDS_32_Tffuncaoapf_pf = AV20TFFuncaoAPF_PF;
         AV123WWFuncaoAPFDS_33_Tffuncaoapf_pf_to = AV21TFFuncaoAPF_PF_To;
         AV124WWFuncaoAPFDS_34_Tffuncaoapf_funapfpainom = AV22TFFuncaoAPF_FunAPFPaiNom;
         AV125WWFuncaoAPFDS_35_Tffuncaoapf_funapfpainom_sel = AV23TFFuncaoAPF_FunAPFPaiNom_Sel;
         AV126WWFuncaoAPFDS_36_Tffuncaoapf_ativo_sels = AV25TFFuncaoAPF_Ativo_Sels;
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              A185FuncaoAPF_Complexidade ,
                                              AV121WWFuncaoAPFDS_31_Tffuncaoapf_complexidade_sels ,
                                              A184FuncaoAPF_Tipo ,
                                              AV116WWFuncaoAPFDS_26_Tffuncaoapf_tipo_sels ,
                                              A183FuncaoAPF_Ativo ,
                                              AV126WWFuncaoAPFDS_36_Tffuncaoapf_ativo_sels ,
                                              AV91WWFuncaoAPFDS_1_Dynamicfiltersselector1 ,
                                              AV92WWFuncaoAPFDS_2_Funcaoapf_nome1 ,
                                              AV93WWFuncaoAPFDS_3_Funcaoapf_tipo1 ,
                                              AV95WWFuncaoAPFDS_5_Funcaoapf_ativo1 ,
                                              AV96WWFuncaoAPFDS_6_Dynamicfiltersenabled2 ,
                                              AV97WWFuncaoAPFDS_7_Dynamicfiltersselector2 ,
                                              AV98WWFuncaoAPFDS_8_Funcaoapf_nome2 ,
                                              AV99WWFuncaoAPFDS_9_Funcaoapf_tipo2 ,
                                              AV101WWFuncaoAPFDS_11_Funcaoapf_ativo2 ,
                                              AV102WWFuncaoAPFDS_12_Dynamicfiltersenabled3 ,
                                              AV103WWFuncaoAPFDS_13_Dynamicfiltersselector3 ,
                                              AV104WWFuncaoAPFDS_14_Funcaoapf_nome3 ,
                                              AV105WWFuncaoAPFDS_15_Funcaoapf_tipo3 ,
                                              AV107WWFuncaoAPFDS_17_Funcaoapf_ativo3 ,
                                              AV108WWFuncaoAPFDS_18_Dynamicfiltersenabled4 ,
                                              AV109WWFuncaoAPFDS_19_Dynamicfiltersselector4 ,
                                              AV110WWFuncaoAPFDS_20_Funcaoapf_nome4 ,
                                              AV111WWFuncaoAPFDS_21_Funcaoapf_tipo4 ,
                                              AV113WWFuncaoAPFDS_23_Funcaoapf_ativo4 ,
                                              AV115WWFuncaoAPFDS_25_Tffuncaoapf_nome_sel ,
                                              AV114WWFuncaoAPFDS_24_Tffuncaoapf_nome ,
                                              AV116WWFuncaoAPFDS_26_Tffuncaoapf_tipo_sels.Count ,
                                              AV125WWFuncaoAPFDS_35_Tffuncaoapf_funapfpainom_sel ,
                                              AV124WWFuncaoAPFDS_34_Tffuncaoapf_funapfpainom ,
                                              AV126WWFuncaoAPFDS_36_Tffuncaoapf_ativo_sels.Count ,
                                              A166FuncaoAPF_Nome ,
                                              A363FuncaoAPF_FunAPFPaiNom ,
                                              AV94WWFuncaoAPFDS_4_Funcaoapf_complexidade1 ,
                                              AV100WWFuncaoAPFDS_10_Funcaoapf_complexidade2 ,
                                              AV106WWFuncaoAPFDS_16_Funcaoapf_complexidade3 ,
                                              AV112WWFuncaoAPFDS_22_Funcaoapf_complexidade4 ,
                                              AV117WWFuncaoAPFDS_27_Tffuncaoapf_td ,
                                              A388FuncaoAPF_TD ,
                                              AV118WWFuncaoAPFDS_28_Tffuncaoapf_td_to ,
                                              AV119WWFuncaoAPFDS_29_Tffuncaoapf_ar ,
                                              A387FuncaoAPF_AR ,
                                              AV120WWFuncaoAPFDS_30_Tffuncaoapf_ar_to ,
                                              AV121WWFuncaoAPFDS_31_Tffuncaoapf_complexidade_sels.Count ,
                                              AV122WWFuncaoAPFDS_32_Tffuncaoapf_pf ,
                                              A386FuncaoAPF_PF ,
                                              AV123WWFuncaoAPFDS_33_Tffuncaoapf_pf_to },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.SHORT,
                                              TypeConstants.SHORT, TypeConstants.INT, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL
                                              }
         });
         lV92WWFuncaoAPFDS_2_Funcaoapf_nome1 = StringUtil.Concat( StringUtil.RTrim( AV92WWFuncaoAPFDS_2_Funcaoapf_nome1), "%", "");
         lV98WWFuncaoAPFDS_8_Funcaoapf_nome2 = StringUtil.Concat( StringUtil.RTrim( AV98WWFuncaoAPFDS_8_Funcaoapf_nome2), "%", "");
         lV104WWFuncaoAPFDS_14_Funcaoapf_nome3 = StringUtil.Concat( StringUtil.RTrim( AV104WWFuncaoAPFDS_14_Funcaoapf_nome3), "%", "");
         lV110WWFuncaoAPFDS_20_Funcaoapf_nome4 = StringUtil.Concat( StringUtil.RTrim( AV110WWFuncaoAPFDS_20_Funcaoapf_nome4), "%", "");
         lV114WWFuncaoAPFDS_24_Tffuncaoapf_nome = StringUtil.Concat( StringUtil.RTrim( AV114WWFuncaoAPFDS_24_Tffuncaoapf_nome), "%", "");
         lV124WWFuncaoAPFDS_34_Tffuncaoapf_funapfpainom = StringUtil.Concat( StringUtil.RTrim( AV124WWFuncaoAPFDS_34_Tffuncaoapf_funapfpainom), "%", "");
         /* Using cursor P00GN2 */
         pr_default.execute(0, new Object[] {AV121WWFuncaoAPFDS_31_Tffuncaoapf_complexidade_sels.Count, lV92WWFuncaoAPFDS_2_Funcaoapf_nome1, AV93WWFuncaoAPFDS_3_Funcaoapf_tipo1, AV95WWFuncaoAPFDS_5_Funcaoapf_ativo1, lV98WWFuncaoAPFDS_8_Funcaoapf_nome2, AV99WWFuncaoAPFDS_9_Funcaoapf_tipo2, AV101WWFuncaoAPFDS_11_Funcaoapf_ativo2, lV104WWFuncaoAPFDS_14_Funcaoapf_nome3, AV105WWFuncaoAPFDS_15_Funcaoapf_tipo3, AV107WWFuncaoAPFDS_17_Funcaoapf_ativo3, lV110WWFuncaoAPFDS_20_Funcaoapf_nome4, AV111WWFuncaoAPFDS_21_Funcaoapf_tipo4, AV113WWFuncaoAPFDS_23_Funcaoapf_ativo4, lV114WWFuncaoAPFDS_24_Tffuncaoapf_nome, AV115WWFuncaoAPFDS_25_Tffuncaoapf_nome_sel, lV124WWFuncaoAPFDS_34_Tffuncaoapf_funapfpainom, AV125WWFuncaoAPFDS_35_Tffuncaoapf_funapfpainom_sel});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKGN2 = false;
            A358FuncaoAPF_FunAPFPaiCod = P00GN2_A358FuncaoAPF_FunAPFPaiCod[0];
            n358FuncaoAPF_FunAPFPaiCod = P00GN2_n358FuncaoAPF_FunAPFPaiCod[0];
            A166FuncaoAPF_Nome = P00GN2_A166FuncaoAPF_Nome[0];
            A363FuncaoAPF_FunAPFPaiNom = P00GN2_A363FuncaoAPF_FunAPFPaiNom[0];
            n363FuncaoAPF_FunAPFPaiNom = P00GN2_n363FuncaoAPF_FunAPFPaiNom[0];
            A183FuncaoAPF_Ativo = P00GN2_A183FuncaoAPF_Ativo[0];
            A184FuncaoAPF_Tipo = P00GN2_A184FuncaoAPF_Tipo[0];
            A165FuncaoAPF_Codigo = P00GN2_A165FuncaoAPF_Codigo[0];
            A363FuncaoAPF_FunAPFPaiNom = P00GN2_A363FuncaoAPF_FunAPFPaiNom[0];
            n363FuncaoAPF_FunAPFPaiNom = P00GN2_n363FuncaoAPF_FunAPFPaiNom[0];
            GXt_char1 = A185FuncaoAPF_Complexidade;
            new prc_fapfcomplexidade(context ).execute( ref  A165FuncaoAPF_Codigo, ref  GXt_char1) ;
            A185FuncaoAPF_Complexidade = GXt_char1;
            if ( ! ( ( StringUtil.StrCmp(AV91WWFuncaoAPFDS_1_Dynamicfiltersselector1, "FUNCAOAPF_COMPLEXIDADE") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV94WWFuncaoAPFDS_4_Funcaoapf_complexidade1)) ) ) || ( ( StringUtil.StrCmp(A185FuncaoAPF_Complexidade, AV94WWFuncaoAPFDS_4_Funcaoapf_complexidade1) == 0 ) ) )
            {
               if ( ! ( AV96WWFuncaoAPFDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV97WWFuncaoAPFDS_7_Dynamicfiltersselector2, "FUNCAOAPF_COMPLEXIDADE") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV100WWFuncaoAPFDS_10_Funcaoapf_complexidade2)) ) ) || ( ( StringUtil.StrCmp(A185FuncaoAPF_Complexidade, AV100WWFuncaoAPFDS_10_Funcaoapf_complexidade2) == 0 ) ) )
               {
                  if ( ! ( AV102WWFuncaoAPFDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV103WWFuncaoAPFDS_13_Dynamicfiltersselector3, "FUNCAOAPF_COMPLEXIDADE") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV106WWFuncaoAPFDS_16_Funcaoapf_complexidade3)) ) ) || ( ( StringUtil.StrCmp(A185FuncaoAPF_Complexidade, AV106WWFuncaoAPFDS_16_Funcaoapf_complexidade3) == 0 ) ) )
                  {
                     if ( ! ( AV108WWFuncaoAPFDS_18_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV109WWFuncaoAPFDS_19_Dynamicfiltersselector4, "FUNCAOAPF_COMPLEXIDADE") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV112WWFuncaoAPFDS_22_Funcaoapf_complexidade4)) ) ) || ( ( StringUtil.StrCmp(A185FuncaoAPF_Complexidade, AV112WWFuncaoAPFDS_22_Funcaoapf_complexidade4) == 0 ) ) )
                     {
                        if ( ( AV121WWFuncaoAPFDS_31_Tffuncaoapf_complexidade_sels.Count <= 0 ) || ( (AV121WWFuncaoAPFDS_31_Tffuncaoapf_complexidade_sels.IndexOf(StringUtil.RTrim( A185FuncaoAPF_Complexidade))>0) ) )
                        {
                           GXt_int2 = A388FuncaoAPF_TD;
                           new prc_funcaoapf_td(context ).execute(  A165FuncaoAPF_Codigo, out  GXt_int2) ;
                           A388FuncaoAPF_TD = GXt_int2;
                           if ( (0==AV117WWFuncaoAPFDS_27_Tffuncaoapf_td) || ( ( A388FuncaoAPF_TD >= AV117WWFuncaoAPFDS_27_Tffuncaoapf_td ) ) )
                           {
                              if ( (0==AV118WWFuncaoAPFDS_28_Tffuncaoapf_td_to) || ( ( A388FuncaoAPF_TD <= AV118WWFuncaoAPFDS_28_Tffuncaoapf_td_to ) ) )
                              {
                                 GXt_int2 = A387FuncaoAPF_AR;
                                 new prc_funcaoapf_ar(context ).execute( ref  A165FuncaoAPF_Codigo, ref  GXt_int2) ;
                                 A387FuncaoAPF_AR = GXt_int2;
                                 if ( (0==AV119WWFuncaoAPFDS_29_Tffuncaoapf_ar) || ( ( A387FuncaoAPF_AR >= AV119WWFuncaoAPFDS_29_Tffuncaoapf_ar ) ) )
                                 {
                                    if ( (0==AV120WWFuncaoAPFDS_30_Tffuncaoapf_ar_to) || ( ( A387FuncaoAPF_AR <= AV120WWFuncaoAPFDS_30_Tffuncaoapf_ar_to ) ) )
                                    {
                                       GXt_decimal3 = A386FuncaoAPF_PF;
                                       new prc_fapfpf(context ).execute( ref  A165FuncaoAPF_Codigo, ref  GXt_decimal3) ;
                                       A386FuncaoAPF_PF = GXt_decimal3;
                                       if ( (Convert.ToDecimal(0)==AV122WWFuncaoAPFDS_32_Tffuncaoapf_pf) || ( ( A386FuncaoAPF_PF >= AV122WWFuncaoAPFDS_32_Tffuncaoapf_pf ) ) )
                                       {
                                          if ( (Convert.ToDecimal(0)==AV123WWFuncaoAPFDS_33_Tffuncaoapf_pf_to) || ( ( A386FuncaoAPF_PF <= AV123WWFuncaoAPFDS_33_Tffuncaoapf_pf_to ) ) )
                                          {
                                             AV38count = 0;
                                             while ( (pr_default.getStatus(0) != 101) && ( StringUtil.StrCmp(P00GN2_A166FuncaoAPF_Nome[0], A166FuncaoAPF_Nome) == 0 ) )
                                             {
                                                BRKGN2 = false;
                                                A165FuncaoAPF_Codigo = P00GN2_A165FuncaoAPF_Codigo[0];
                                                AV38count = (long)(AV38count+1);
                                                BRKGN2 = true;
                                                pr_default.readNext(0);
                                             }
                                             if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A166FuncaoAPF_Nome)) )
                                             {
                                                AV30Option = A166FuncaoAPF_Nome;
                                                AV31Options.Add(AV30Option, 0);
                                                AV36OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV38count), "Z,ZZZ,ZZZ,ZZ9")), 0);
                                             }
                                             if ( AV31Options.Count == 50 )
                                             {
                                                /* Exit For each command. Update data (if necessary), close cursors & exit. */
                                                if (true) break;
                                             }
                                          }
                                       }
                                    }
                                 }
                              }
                           }
                        }
                     }
                  }
               }
            }
            if ( ! BRKGN2 )
            {
               BRKGN2 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      protected void S131( )
      {
         /* 'LOADFUNCAOAPF_FUNAPFPAINOMOPTIONS' Routine */
         AV22TFFuncaoAPF_FunAPFPaiNom = AV26SearchTxt;
         AV23TFFuncaoAPF_FunAPFPaiNom_Sel = "";
         AV91WWFuncaoAPFDS_1_Dynamicfiltersselector1 = AV44DynamicFiltersSelector1;
         AV92WWFuncaoAPFDS_2_Funcaoapf_nome1 = AV45FuncaoAPF_Nome1;
         AV93WWFuncaoAPFDS_3_Funcaoapf_tipo1 = AV46FuncaoAPF_Tipo1;
         AV94WWFuncaoAPFDS_4_Funcaoapf_complexidade1 = AV47FuncaoAPF_Complexidade1;
         AV95WWFuncaoAPFDS_5_Funcaoapf_ativo1 = AV48FuncaoAPF_Ativo1;
         AV96WWFuncaoAPFDS_6_Dynamicfiltersenabled2 = AV49DynamicFiltersEnabled2;
         AV97WWFuncaoAPFDS_7_Dynamicfiltersselector2 = AV50DynamicFiltersSelector2;
         AV98WWFuncaoAPFDS_8_Funcaoapf_nome2 = AV51FuncaoAPF_Nome2;
         AV99WWFuncaoAPFDS_9_Funcaoapf_tipo2 = AV52FuncaoAPF_Tipo2;
         AV100WWFuncaoAPFDS_10_Funcaoapf_complexidade2 = AV53FuncaoAPF_Complexidade2;
         AV101WWFuncaoAPFDS_11_Funcaoapf_ativo2 = AV54FuncaoAPF_Ativo2;
         AV102WWFuncaoAPFDS_12_Dynamicfiltersenabled3 = AV55DynamicFiltersEnabled3;
         AV103WWFuncaoAPFDS_13_Dynamicfiltersselector3 = AV56DynamicFiltersSelector3;
         AV104WWFuncaoAPFDS_14_Funcaoapf_nome3 = AV57FuncaoAPF_Nome3;
         AV105WWFuncaoAPFDS_15_Funcaoapf_tipo3 = AV58FuncaoAPF_Tipo3;
         AV106WWFuncaoAPFDS_16_Funcaoapf_complexidade3 = AV59FuncaoAPF_Complexidade3;
         AV107WWFuncaoAPFDS_17_Funcaoapf_ativo3 = AV60FuncaoAPF_Ativo3;
         AV108WWFuncaoAPFDS_18_Dynamicfiltersenabled4 = AV61DynamicFiltersEnabled4;
         AV109WWFuncaoAPFDS_19_Dynamicfiltersselector4 = AV62DynamicFiltersSelector4;
         AV110WWFuncaoAPFDS_20_Funcaoapf_nome4 = AV63FuncaoAPF_Nome4;
         AV111WWFuncaoAPFDS_21_Funcaoapf_tipo4 = AV64FuncaoAPF_Tipo4;
         AV112WWFuncaoAPFDS_22_Funcaoapf_complexidade4 = AV65FuncaoAPF_Complexidade4;
         AV113WWFuncaoAPFDS_23_Funcaoapf_ativo4 = AV66FuncaoAPF_Ativo4;
         AV114WWFuncaoAPFDS_24_Tffuncaoapf_nome = AV10TFFuncaoAPF_Nome;
         AV115WWFuncaoAPFDS_25_Tffuncaoapf_nome_sel = AV11TFFuncaoAPF_Nome_Sel;
         AV116WWFuncaoAPFDS_26_Tffuncaoapf_tipo_sels = AV13TFFuncaoAPF_Tipo_Sels;
         AV117WWFuncaoAPFDS_27_Tffuncaoapf_td = AV14TFFuncaoAPF_TD;
         AV118WWFuncaoAPFDS_28_Tffuncaoapf_td_to = AV15TFFuncaoAPF_TD_To;
         AV119WWFuncaoAPFDS_29_Tffuncaoapf_ar = AV16TFFuncaoAPF_AR;
         AV120WWFuncaoAPFDS_30_Tffuncaoapf_ar_to = AV17TFFuncaoAPF_AR_To;
         AV121WWFuncaoAPFDS_31_Tffuncaoapf_complexidade_sels = AV19TFFuncaoAPF_Complexidade_Sels;
         AV122WWFuncaoAPFDS_32_Tffuncaoapf_pf = AV20TFFuncaoAPF_PF;
         AV123WWFuncaoAPFDS_33_Tffuncaoapf_pf_to = AV21TFFuncaoAPF_PF_To;
         AV124WWFuncaoAPFDS_34_Tffuncaoapf_funapfpainom = AV22TFFuncaoAPF_FunAPFPaiNom;
         AV125WWFuncaoAPFDS_35_Tffuncaoapf_funapfpainom_sel = AV23TFFuncaoAPF_FunAPFPaiNom_Sel;
         AV126WWFuncaoAPFDS_36_Tffuncaoapf_ativo_sels = AV25TFFuncaoAPF_Ativo_Sels;
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              A185FuncaoAPF_Complexidade ,
                                              AV121WWFuncaoAPFDS_31_Tffuncaoapf_complexidade_sels ,
                                              A184FuncaoAPF_Tipo ,
                                              AV116WWFuncaoAPFDS_26_Tffuncaoapf_tipo_sels ,
                                              A183FuncaoAPF_Ativo ,
                                              AV126WWFuncaoAPFDS_36_Tffuncaoapf_ativo_sels ,
                                              AV91WWFuncaoAPFDS_1_Dynamicfiltersselector1 ,
                                              AV92WWFuncaoAPFDS_2_Funcaoapf_nome1 ,
                                              AV93WWFuncaoAPFDS_3_Funcaoapf_tipo1 ,
                                              AV95WWFuncaoAPFDS_5_Funcaoapf_ativo1 ,
                                              AV96WWFuncaoAPFDS_6_Dynamicfiltersenabled2 ,
                                              AV97WWFuncaoAPFDS_7_Dynamicfiltersselector2 ,
                                              AV98WWFuncaoAPFDS_8_Funcaoapf_nome2 ,
                                              AV99WWFuncaoAPFDS_9_Funcaoapf_tipo2 ,
                                              AV101WWFuncaoAPFDS_11_Funcaoapf_ativo2 ,
                                              AV102WWFuncaoAPFDS_12_Dynamicfiltersenabled3 ,
                                              AV103WWFuncaoAPFDS_13_Dynamicfiltersselector3 ,
                                              AV104WWFuncaoAPFDS_14_Funcaoapf_nome3 ,
                                              AV105WWFuncaoAPFDS_15_Funcaoapf_tipo3 ,
                                              AV107WWFuncaoAPFDS_17_Funcaoapf_ativo3 ,
                                              AV108WWFuncaoAPFDS_18_Dynamicfiltersenabled4 ,
                                              AV109WWFuncaoAPFDS_19_Dynamicfiltersselector4 ,
                                              AV110WWFuncaoAPFDS_20_Funcaoapf_nome4 ,
                                              AV111WWFuncaoAPFDS_21_Funcaoapf_tipo4 ,
                                              AV113WWFuncaoAPFDS_23_Funcaoapf_ativo4 ,
                                              AV115WWFuncaoAPFDS_25_Tffuncaoapf_nome_sel ,
                                              AV114WWFuncaoAPFDS_24_Tffuncaoapf_nome ,
                                              AV116WWFuncaoAPFDS_26_Tffuncaoapf_tipo_sels.Count ,
                                              AV125WWFuncaoAPFDS_35_Tffuncaoapf_funapfpainom_sel ,
                                              AV124WWFuncaoAPFDS_34_Tffuncaoapf_funapfpainom ,
                                              AV126WWFuncaoAPFDS_36_Tffuncaoapf_ativo_sels.Count ,
                                              A166FuncaoAPF_Nome ,
                                              A363FuncaoAPF_FunAPFPaiNom ,
                                              AV94WWFuncaoAPFDS_4_Funcaoapf_complexidade1 ,
                                              AV100WWFuncaoAPFDS_10_Funcaoapf_complexidade2 ,
                                              AV106WWFuncaoAPFDS_16_Funcaoapf_complexidade3 ,
                                              AV112WWFuncaoAPFDS_22_Funcaoapf_complexidade4 ,
                                              AV117WWFuncaoAPFDS_27_Tffuncaoapf_td ,
                                              A388FuncaoAPF_TD ,
                                              AV118WWFuncaoAPFDS_28_Tffuncaoapf_td_to ,
                                              AV119WWFuncaoAPFDS_29_Tffuncaoapf_ar ,
                                              A387FuncaoAPF_AR ,
                                              AV120WWFuncaoAPFDS_30_Tffuncaoapf_ar_to ,
                                              AV121WWFuncaoAPFDS_31_Tffuncaoapf_complexidade_sels.Count ,
                                              AV122WWFuncaoAPFDS_32_Tffuncaoapf_pf ,
                                              A386FuncaoAPF_PF ,
                                              AV123WWFuncaoAPFDS_33_Tffuncaoapf_pf_to },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.SHORT,
                                              TypeConstants.SHORT, TypeConstants.INT, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL
                                              }
         });
         lV92WWFuncaoAPFDS_2_Funcaoapf_nome1 = StringUtil.Concat( StringUtil.RTrim( AV92WWFuncaoAPFDS_2_Funcaoapf_nome1), "%", "");
         lV98WWFuncaoAPFDS_8_Funcaoapf_nome2 = StringUtil.Concat( StringUtil.RTrim( AV98WWFuncaoAPFDS_8_Funcaoapf_nome2), "%", "");
         lV104WWFuncaoAPFDS_14_Funcaoapf_nome3 = StringUtil.Concat( StringUtil.RTrim( AV104WWFuncaoAPFDS_14_Funcaoapf_nome3), "%", "");
         lV110WWFuncaoAPFDS_20_Funcaoapf_nome4 = StringUtil.Concat( StringUtil.RTrim( AV110WWFuncaoAPFDS_20_Funcaoapf_nome4), "%", "");
         lV114WWFuncaoAPFDS_24_Tffuncaoapf_nome = StringUtil.Concat( StringUtil.RTrim( AV114WWFuncaoAPFDS_24_Tffuncaoapf_nome), "%", "");
         lV124WWFuncaoAPFDS_34_Tffuncaoapf_funapfpainom = StringUtil.Concat( StringUtil.RTrim( AV124WWFuncaoAPFDS_34_Tffuncaoapf_funapfpainom), "%", "");
         /* Using cursor P00GN3 */
         pr_default.execute(1, new Object[] {AV121WWFuncaoAPFDS_31_Tffuncaoapf_complexidade_sels.Count, lV92WWFuncaoAPFDS_2_Funcaoapf_nome1, AV93WWFuncaoAPFDS_3_Funcaoapf_tipo1, AV95WWFuncaoAPFDS_5_Funcaoapf_ativo1, lV98WWFuncaoAPFDS_8_Funcaoapf_nome2, AV99WWFuncaoAPFDS_9_Funcaoapf_tipo2, AV101WWFuncaoAPFDS_11_Funcaoapf_ativo2, lV104WWFuncaoAPFDS_14_Funcaoapf_nome3, AV105WWFuncaoAPFDS_15_Funcaoapf_tipo3, AV107WWFuncaoAPFDS_17_Funcaoapf_ativo3, lV110WWFuncaoAPFDS_20_Funcaoapf_nome4, AV111WWFuncaoAPFDS_21_Funcaoapf_tipo4, AV113WWFuncaoAPFDS_23_Funcaoapf_ativo4, lV114WWFuncaoAPFDS_24_Tffuncaoapf_nome, AV115WWFuncaoAPFDS_25_Tffuncaoapf_nome_sel, lV124WWFuncaoAPFDS_34_Tffuncaoapf_funapfpainom, AV125WWFuncaoAPFDS_35_Tffuncaoapf_funapfpainom_sel});
         while ( (pr_default.getStatus(1) != 101) )
         {
            BRKGN4 = false;
            A358FuncaoAPF_FunAPFPaiCod = P00GN3_A358FuncaoAPF_FunAPFPaiCod[0];
            n358FuncaoAPF_FunAPFPaiCod = P00GN3_n358FuncaoAPF_FunAPFPaiCod[0];
            A363FuncaoAPF_FunAPFPaiNom = P00GN3_A363FuncaoAPF_FunAPFPaiNom[0];
            n363FuncaoAPF_FunAPFPaiNom = P00GN3_n363FuncaoAPF_FunAPFPaiNom[0];
            A183FuncaoAPF_Ativo = P00GN3_A183FuncaoAPF_Ativo[0];
            A184FuncaoAPF_Tipo = P00GN3_A184FuncaoAPF_Tipo[0];
            A166FuncaoAPF_Nome = P00GN3_A166FuncaoAPF_Nome[0];
            A165FuncaoAPF_Codigo = P00GN3_A165FuncaoAPF_Codigo[0];
            A363FuncaoAPF_FunAPFPaiNom = P00GN3_A363FuncaoAPF_FunAPFPaiNom[0];
            n363FuncaoAPF_FunAPFPaiNom = P00GN3_n363FuncaoAPF_FunAPFPaiNom[0];
            GXt_char1 = A185FuncaoAPF_Complexidade;
            new prc_fapfcomplexidade(context ).execute( ref  A165FuncaoAPF_Codigo, ref  GXt_char1) ;
            A185FuncaoAPF_Complexidade = GXt_char1;
            if ( ! ( ( StringUtil.StrCmp(AV91WWFuncaoAPFDS_1_Dynamicfiltersselector1, "FUNCAOAPF_COMPLEXIDADE") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV94WWFuncaoAPFDS_4_Funcaoapf_complexidade1)) ) ) || ( ( StringUtil.StrCmp(A185FuncaoAPF_Complexidade, AV94WWFuncaoAPFDS_4_Funcaoapf_complexidade1) == 0 ) ) )
            {
               if ( ! ( AV96WWFuncaoAPFDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV97WWFuncaoAPFDS_7_Dynamicfiltersselector2, "FUNCAOAPF_COMPLEXIDADE") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV100WWFuncaoAPFDS_10_Funcaoapf_complexidade2)) ) ) || ( ( StringUtil.StrCmp(A185FuncaoAPF_Complexidade, AV100WWFuncaoAPFDS_10_Funcaoapf_complexidade2) == 0 ) ) )
               {
                  if ( ! ( AV102WWFuncaoAPFDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV103WWFuncaoAPFDS_13_Dynamicfiltersselector3, "FUNCAOAPF_COMPLEXIDADE") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV106WWFuncaoAPFDS_16_Funcaoapf_complexidade3)) ) ) || ( ( StringUtil.StrCmp(A185FuncaoAPF_Complexidade, AV106WWFuncaoAPFDS_16_Funcaoapf_complexidade3) == 0 ) ) )
                  {
                     if ( ! ( AV108WWFuncaoAPFDS_18_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV109WWFuncaoAPFDS_19_Dynamicfiltersselector4, "FUNCAOAPF_COMPLEXIDADE") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV112WWFuncaoAPFDS_22_Funcaoapf_complexidade4)) ) ) || ( ( StringUtil.StrCmp(A185FuncaoAPF_Complexidade, AV112WWFuncaoAPFDS_22_Funcaoapf_complexidade4) == 0 ) ) )
                     {
                        if ( ( AV121WWFuncaoAPFDS_31_Tffuncaoapf_complexidade_sels.Count <= 0 ) || ( (AV121WWFuncaoAPFDS_31_Tffuncaoapf_complexidade_sels.IndexOf(StringUtil.RTrim( A185FuncaoAPF_Complexidade))>0) ) )
                        {
                           GXt_int2 = A388FuncaoAPF_TD;
                           new prc_funcaoapf_td(context ).execute(  A165FuncaoAPF_Codigo, out  GXt_int2) ;
                           A388FuncaoAPF_TD = GXt_int2;
                           if ( (0==AV117WWFuncaoAPFDS_27_Tffuncaoapf_td) || ( ( A388FuncaoAPF_TD >= AV117WWFuncaoAPFDS_27_Tffuncaoapf_td ) ) )
                           {
                              if ( (0==AV118WWFuncaoAPFDS_28_Tffuncaoapf_td_to) || ( ( A388FuncaoAPF_TD <= AV118WWFuncaoAPFDS_28_Tffuncaoapf_td_to ) ) )
                              {
                                 GXt_int2 = A387FuncaoAPF_AR;
                                 new prc_funcaoapf_ar(context ).execute( ref  A165FuncaoAPF_Codigo, ref  GXt_int2) ;
                                 A387FuncaoAPF_AR = GXt_int2;
                                 if ( (0==AV119WWFuncaoAPFDS_29_Tffuncaoapf_ar) || ( ( A387FuncaoAPF_AR >= AV119WWFuncaoAPFDS_29_Tffuncaoapf_ar ) ) )
                                 {
                                    if ( (0==AV120WWFuncaoAPFDS_30_Tffuncaoapf_ar_to) || ( ( A387FuncaoAPF_AR <= AV120WWFuncaoAPFDS_30_Tffuncaoapf_ar_to ) ) )
                                    {
                                       GXt_decimal3 = A386FuncaoAPF_PF;
                                       new prc_fapfpf(context ).execute( ref  A165FuncaoAPF_Codigo, ref  GXt_decimal3) ;
                                       A386FuncaoAPF_PF = GXt_decimal3;
                                       if ( (Convert.ToDecimal(0)==AV122WWFuncaoAPFDS_32_Tffuncaoapf_pf) || ( ( A386FuncaoAPF_PF >= AV122WWFuncaoAPFDS_32_Tffuncaoapf_pf ) ) )
                                       {
                                          if ( (Convert.ToDecimal(0)==AV123WWFuncaoAPFDS_33_Tffuncaoapf_pf_to) || ( ( A386FuncaoAPF_PF <= AV123WWFuncaoAPFDS_33_Tffuncaoapf_pf_to ) ) )
                                          {
                                             AV38count = 0;
                                             while ( (pr_default.getStatus(1) != 101) && ( StringUtil.StrCmp(P00GN3_A363FuncaoAPF_FunAPFPaiNom[0], A363FuncaoAPF_FunAPFPaiNom) == 0 ) )
                                             {
                                                BRKGN4 = false;
                                                A358FuncaoAPF_FunAPFPaiCod = P00GN3_A358FuncaoAPF_FunAPFPaiCod[0];
                                                n358FuncaoAPF_FunAPFPaiCod = P00GN3_n358FuncaoAPF_FunAPFPaiCod[0];
                                                A165FuncaoAPF_Codigo = P00GN3_A165FuncaoAPF_Codigo[0];
                                                AV38count = (long)(AV38count+1);
                                                BRKGN4 = true;
                                                pr_default.readNext(1);
                                             }
                                             if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A363FuncaoAPF_FunAPFPaiNom)) )
                                             {
                                                AV30Option = A363FuncaoAPF_FunAPFPaiNom;
                                                AV31Options.Add(AV30Option, 0);
                                                AV36OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV38count), "Z,ZZZ,ZZZ,ZZ9")), 0);
                                             }
                                             if ( AV31Options.Count == 50 )
                                             {
                                                /* Exit For each command. Update data (if necessary), close cursors & exit. */
                                                if (true) break;
                                             }
                                          }
                                       }
                                    }
                                 }
                              }
                           }
                        }
                     }
                  }
               }
            }
            if ( ! BRKGN4 )
            {
               BRKGN4 = true;
               pr_default.readNext(1);
            }
         }
         pr_default.close(1);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV31Options = new GxSimpleCollection();
         AV34OptionsDesc = new GxSimpleCollection();
         AV36OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV39Session = context.GetSession();
         AV41GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV42GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV10TFFuncaoAPF_Nome = "";
         AV11TFFuncaoAPF_Nome_Sel = "";
         AV12TFFuncaoAPF_Tipo_SelsJson = "";
         AV13TFFuncaoAPF_Tipo_Sels = new GxSimpleCollection();
         AV18TFFuncaoAPF_Complexidade_SelsJson = "";
         AV19TFFuncaoAPF_Complexidade_Sels = new GxSimpleCollection();
         AV22TFFuncaoAPF_FunAPFPaiNom = "";
         AV23TFFuncaoAPF_FunAPFPaiNom_Sel = "";
         AV24TFFuncaoAPF_Ativo_SelsJson = "";
         AV25TFFuncaoAPF_Ativo_Sels = new GxSimpleCollection();
         AV43GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV44DynamicFiltersSelector1 = "";
         AV45FuncaoAPF_Nome1 = "";
         AV46FuncaoAPF_Tipo1 = "";
         AV47FuncaoAPF_Complexidade1 = "";
         AV48FuncaoAPF_Ativo1 = "A";
         AV50DynamicFiltersSelector2 = "";
         AV51FuncaoAPF_Nome2 = "";
         AV52FuncaoAPF_Tipo2 = "";
         AV53FuncaoAPF_Complexidade2 = "";
         AV54FuncaoAPF_Ativo2 = "A";
         AV56DynamicFiltersSelector3 = "";
         AV57FuncaoAPF_Nome3 = "";
         AV58FuncaoAPF_Tipo3 = "";
         AV59FuncaoAPF_Complexidade3 = "";
         AV60FuncaoAPF_Ativo3 = "A";
         AV62DynamicFiltersSelector4 = "";
         AV63FuncaoAPF_Nome4 = "";
         AV64FuncaoAPF_Tipo4 = "";
         AV65FuncaoAPF_Complexidade4 = "";
         AV66FuncaoAPF_Ativo4 = "A";
         AV91WWFuncaoAPFDS_1_Dynamicfiltersselector1 = "";
         AV92WWFuncaoAPFDS_2_Funcaoapf_nome1 = "";
         AV93WWFuncaoAPFDS_3_Funcaoapf_tipo1 = "";
         AV94WWFuncaoAPFDS_4_Funcaoapf_complexidade1 = "";
         AV95WWFuncaoAPFDS_5_Funcaoapf_ativo1 = "";
         AV97WWFuncaoAPFDS_7_Dynamicfiltersselector2 = "";
         AV98WWFuncaoAPFDS_8_Funcaoapf_nome2 = "";
         AV99WWFuncaoAPFDS_9_Funcaoapf_tipo2 = "";
         AV100WWFuncaoAPFDS_10_Funcaoapf_complexidade2 = "";
         AV101WWFuncaoAPFDS_11_Funcaoapf_ativo2 = "";
         AV103WWFuncaoAPFDS_13_Dynamicfiltersselector3 = "";
         AV104WWFuncaoAPFDS_14_Funcaoapf_nome3 = "";
         AV105WWFuncaoAPFDS_15_Funcaoapf_tipo3 = "";
         AV106WWFuncaoAPFDS_16_Funcaoapf_complexidade3 = "";
         AV107WWFuncaoAPFDS_17_Funcaoapf_ativo3 = "";
         AV109WWFuncaoAPFDS_19_Dynamicfiltersselector4 = "";
         AV110WWFuncaoAPFDS_20_Funcaoapf_nome4 = "";
         AV111WWFuncaoAPFDS_21_Funcaoapf_tipo4 = "";
         AV112WWFuncaoAPFDS_22_Funcaoapf_complexidade4 = "";
         AV113WWFuncaoAPFDS_23_Funcaoapf_ativo4 = "";
         AV114WWFuncaoAPFDS_24_Tffuncaoapf_nome = "";
         AV115WWFuncaoAPFDS_25_Tffuncaoapf_nome_sel = "";
         AV116WWFuncaoAPFDS_26_Tffuncaoapf_tipo_sels = new GxSimpleCollection();
         AV121WWFuncaoAPFDS_31_Tffuncaoapf_complexidade_sels = new GxSimpleCollection();
         AV124WWFuncaoAPFDS_34_Tffuncaoapf_funapfpainom = "";
         AV125WWFuncaoAPFDS_35_Tffuncaoapf_funapfpainom_sel = "";
         AV126WWFuncaoAPFDS_36_Tffuncaoapf_ativo_sels = new GxSimpleCollection();
         scmdbuf = "";
         lV92WWFuncaoAPFDS_2_Funcaoapf_nome1 = "";
         lV98WWFuncaoAPFDS_8_Funcaoapf_nome2 = "";
         lV104WWFuncaoAPFDS_14_Funcaoapf_nome3 = "";
         lV110WWFuncaoAPFDS_20_Funcaoapf_nome4 = "";
         lV114WWFuncaoAPFDS_24_Tffuncaoapf_nome = "";
         lV124WWFuncaoAPFDS_34_Tffuncaoapf_funapfpainom = "";
         A185FuncaoAPF_Complexidade = "";
         A184FuncaoAPF_Tipo = "";
         A183FuncaoAPF_Ativo = "";
         A166FuncaoAPF_Nome = "";
         A363FuncaoAPF_FunAPFPaiNom = "";
         P00GN2_A358FuncaoAPF_FunAPFPaiCod = new int[1] ;
         P00GN2_n358FuncaoAPF_FunAPFPaiCod = new bool[] {false} ;
         P00GN2_A166FuncaoAPF_Nome = new String[] {""} ;
         P00GN2_A363FuncaoAPF_FunAPFPaiNom = new String[] {""} ;
         P00GN2_n363FuncaoAPF_FunAPFPaiNom = new bool[] {false} ;
         P00GN2_A183FuncaoAPF_Ativo = new String[] {""} ;
         P00GN2_A184FuncaoAPF_Tipo = new String[] {""} ;
         P00GN2_A165FuncaoAPF_Codigo = new int[1] ;
         AV30Option = "";
         P00GN3_A358FuncaoAPF_FunAPFPaiCod = new int[1] ;
         P00GN3_n358FuncaoAPF_FunAPFPaiCod = new bool[] {false} ;
         P00GN3_A363FuncaoAPF_FunAPFPaiNom = new String[] {""} ;
         P00GN3_n363FuncaoAPF_FunAPFPaiNom = new bool[] {false} ;
         P00GN3_A183FuncaoAPF_Ativo = new String[] {""} ;
         P00GN3_A184FuncaoAPF_Tipo = new String[] {""} ;
         P00GN3_A166FuncaoAPF_Nome = new String[] {""} ;
         P00GN3_A165FuncaoAPF_Codigo = new int[1] ;
         GXt_char1 = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getwwfuncaoapffilterdata__default(),
            new Object[][] {
                new Object[] {
               P00GN2_A358FuncaoAPF_FunAPFPaiCod, P00GN2_n358FuncaoAPF_FunAPFPaiCod, P00GN2_A166FuncaoAPF_Nome, P00GN2_A363FuncaoAPF_FunAPFPaiNom, P00GN2_n363FuncaoAPF_FunAPFPaiNom, P00GN2_A183FuncaoAPF_Ativo, P00GN2_A184FuncaoAPF_Tipo, P00GN2_A165FuncaoAPF_Codigo
               }
               , new Object[] {
               P00GN3_A358FuncaoAPF_FunAPFPaiCod, P00GN3_n358FuncaoAPF_FunAPFPaiCod, P00GN3_A363FuncaoAPF_FunAPFPaiNom, P00GN3_n363FuncaoAPF_FunAPFPaiNom, P00GN3_A183FuncaoAPF_Ativo, P00GN3_A184FuncaoAPF_Tipo, P00GN3_A166FuncaoAPF_Nome, P00GN3_A165FuncaoAPF_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV14TFFuncaoAPF_TD ;
      private short AV15TFFuncaoAPF_TD_To ;
      private short AV16TFFuncaoAPF_AR ;
      private short AV17TFFuncaoAPF_AR_To ;
      private short AV117WWFuncaoAPFDS_27_Tffuncaoapf_td ;
      private short AV118WWFuncaoAPFDS_28_Tffuncaoapf_td_to ;
      private short AV119WWFuncaoAPFDS_29_Tffuncaoapf_ar ;
      private short AV120WWFuncaoAPFDS_30_Tffuncaoapf_ar_to ;
      private short A388FuncaoAPF_TD ;
      private short A387FuncaoAPF_AR ;
      private short GXt_int2 ;
      private int AV89GXV1 ;
      private int AV116WWFuncaoAPFDS_26_Tffuncaoapf_tipo_sels_Count ;
      private int AV126WWFuncaoAPFDS_36_Tffuncaoapf_ativo_sels_Count ;
      private int AV121WWFuncaoAPFDS_31_Tffuncaoapf_complexidade_sels_Count ;
      private int A358FuncaoAPF_FunAPFPaiCod ;
      private int A165FuncaoAPF_Codigo ;
      private long AV38count ;
      private decimal AV20TFFuncaoAPF_PF ;
      private decimal AV21TFFuncaoAPF_PF_To ;
      private decimal AV122WWFuncaoAPFDS_32_Tffuncaoapf_pf ;
      private decimal AV123WWFuncaoAPFDS_33_Tffuncaoapf_pf_to ;
      private decimal A386FuncaoAPF_PF ;
      private decimal GXt_decimal3 ;
      private String AV46FuncaoAPF_Tipo1 ;
      private String AV47FuncaoAPF_Complexidade1 ;
      private String AV48FuncaoAPF_Ativo1 ;
      private String AV52FuncaoAPF_Tipo2 ;
      private String AV53FuncaoAPF_Complexidade2 ;
      private String AV54FuncaoAPF_Ativo2 ;
      private String AV58FuncaoAPF_Tipo3 ;
      private String AV59FuncaoAPF_Complexidade3 ;
      private String AV60FuncaoAPF_Ativo3 ;
      private String AV64FuncaoAPF_Tipo4 ;
      private String AV65FuncaoAPF_Complexidade4 ;
      private String AV66FuncaoAPF_Ativo4 ;
      private String AV93WWFuncaoAPFDS_3_Funcaoapf_tipo1 ;
      private String AV94WWFuncaoAPFDS_4_Funcaoapf_complexidade1 ;
      private String AV95WWFuncaoAPFDS_5_Funcaoapf_ativo1 ;
      private String AV99WWFuncaoAPFDS_9_Funcaoapf_tipo2 ;
      private String AV100WWFuncaoAPFDS_10_Funcaoapf_complexidade2 ;
      private String AV101WWFuncaoAPFDS_11_Funcaoapf_ativo2 ;
      private String AV105WWFuncaoAPFDS_15_Funcaoapf_tipo3 ;
      private String AV106WWFuncaoAPFDS_16_Funcaoapf_complexidade3 ;
      private String AV107WWFuncaoAPFDS_17_Funcaoapf_ativo3 ;
      private String AV111WWFuncaoAPFDS_21_Funcaoapf_tipo4 ;
      private String AV112WWFuncaoAPFDS_22_Funcaoapf_complexidade4 ;
      private String AV113WWFuncaoAPFDS_23_Funcaoapf_ativo4 ;
      private String scmdbuf ;
      private String A185FuncaoAPF_Complexidade ;
      private String A184FuncaoAPF_Tipo ;
      private String A183FuncaoAPF_Ativo ;
      private String GXt_char1 ;
      private bool returnInSub ;
      private bool AV49DynamicFiltersEnabled2 ;
      private bool AV55DynamicFiltersEnabled3 ;
      private bool AV61DynamicFiltersEnabled4 ;
      private bool AV96WWFuncaoAPFDS_6_Dynamicfiltersenabled2 ;
      private bool AV102WWFuncaoAPFDS_12_Dynamicfiltersenabled3 ;
      private bool AV108WWFuncaoAPFDS_18_Dynamicfiltersenabled4 ;
      private bool BRKGN2 ;
      private bool n358FuncaoAPF_FunAPFPaiCod ;
      private bool n363FuncaoAPF_FunAPFPaiNom ;
      private bool BRKGN4 ;
      private String AV37OptionIndexesJson ;
      private String AV32OptionsJson ;
      private String AV35OptionsDescJson ;
      private String AV12TFFuncaoAPF_Tipo_SelsJson ;
      private String AV18TFFuncaoAPF_Complexidade_SelsJson ;
      private String AV24TFFuncaoAPF_Ativo_SelsJson ;
      private String AV28DDOName ;
      private String AV26SearchTxt ;
      private String AV27SearchTxtTo ;
      private String AV10TFFuncaoAPF_Nome ;
      private String AV11TFFuncaoAPF_Nome_Sel ;
      private String AV22TFFuncaoAPF_FunAPFPaiNom ;
      private String AV23TFFuncaoAPF_FunAPFPaiNom_Sel ;
      private String AV44DynamicFiltersSelector1 ;
      private String AV45FuncaoAPF_Nome1 ;
      private String AV50DynamicFiltersSelector2 ;
      private String AV51FuncaoAPF_Nome2 ;
      private String AV56DynamicFiltersSelector3 ;
      private String AV57FuncaoAPF_Nome3 ;
      private String AV62DynamicFiltersSelector4 ;
      private String AV63FuncaoAPF_Nome4 ;
      private String AV91WWFuncaoAPFDS_1_Dynamicfiltersselector1 ;
      private String AV92WWFuncaoAPFDS_2_Funcaoapf_nome1 ;
      private String AV97WWFuncaoAPFDS_7_Dynamicfiltersselector2 ;
      private String AV98WWFuncaoAPFDS_8_Funcaoapf_nome2 ;
      private String AV103WWFuncaoAPFDS_13_Dynamicfiltersselector3 ;
      private String AV104WWFuncaoAPFDS_14_Funcaoapf_nome3 ;
      private String AV109WWFuncaoAPFDS_19_Dynamicfiltersselector4 ;
      private String AV110WWFuncaoAPFDS_20_Funcaoapf_nome4 ;
      private String AV114WWFuncaoAPFDS_24_Tffuncaoapf_nome ;
      private String AV115WWFuncaoAPFDS_25_Tffuncaoapf_nome_sel ;
      private String AV124WWFuncaoAPFDS_34_Tffuncaoapf_funapfpainom ;
      private String AV125WWFuncaoAPFDS_35_Tffuncaoapf_funapfpainom_sel ;
      private String lV92WWFuncaoAPFDS_2_Funcaoapf_nome1 ;
      private String lV98WWFuncaoAPFDS_8_Funcaoapf_nome2 ;
      private String lV104WWFuncaoAPFDS_14_Funcaoapf_nome3 ;
      private String lV110WWFuncaoAPFDS_20_Funcaoapf_nome4 ;
      private String lV114WWFuncaoAPFDS_24_Tffuncaoapf_nome ;
      private String lV124WWFuncaoAPFDS_34_Tffuncaoapf_funapfpainom ;
      private String A166FuncaoAPF_Nome ;
      private String A363FuncaoAPF_FunAPFPaiNom ;
      private String AV30Option ;
      private IGxSession AV39Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00GN2_A358FuncaoAPF_FunAPFPaiCod ;
      private bool[] P00GN2_n358FuncaoAPF_FunAPFPaiCod ;
      private String[] P00GN2_A166FuncaoAPF_Nome ;
      private String[] P00GN2_A363FuncaoAPF_FunAPFPaiNom ;
      private bool[] P00GN2_n363FuncaoAPF_FunAPFPaiNom ;
      private String[] P00GN2_A183FuncaoAPF_Ativo ;
      private String[] P00GN2_A184FuncaoAPF_Tipo ;
      private int[] P00GN2_A165FuncaoAPF_Codigo ;
      private int[] P00GN3_A358FuncaoAPF_FunAPFPaiCod ;
      private bool[] P00GN3_n358FuncaoAPF_FunAPFPaiCod ;
      private String[] P00GN3_A363FuncaoAPF_FunAPFPaiNom ;
      private bool[] P00GN3_n363FuncaoAPF_FunAPFPaiNom ;
      private String[] P00GN3_A183FuncaoAPF_Ativo ;
      private String[] P00GN3_A184FuncaoAPF_Tipo ;
      private String[] P00GN3_A166FuncaoAPF_Nome ;
      private int[] P00GN3_A165FuncaoAPF_Codigo ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV13TFFuncaoAPF_Tipo_Sels ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV19TFFuncaoAPF_Complexidade_Sels ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV25TFFuncaoAPF_Ativo_Sels ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV116WWFuncaoAPFDS_26_Tffuncaoapf_tipo_sels ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV121WWFuncaoAPFDS_31_Tffuncaoapf_complexidade_sels ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV126WWFuncaoAPFDS_36_Tffuncaoapf_ativo_sels ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV31Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV34OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV36OptionIndexes ;
      private wwpbaseobjects.SdtWWPGridState AV41GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV42GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV43GridStateDynamicFilter ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
   }

   public class getwwfuncaoapffilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00GN2( IGxContext context ,
                                             String A185FuncaoAPF_Complexidade ,
                                             IGxCollection AV121WWFuncaoAPFDS_31_Tffuncaoapf_complexidade_sels ,
                                             String A184FuncaoAPF_Tipo ,
                                             IGxCollection AV116WWFuncaoAPFDS_26_Tffuncaoapf_tipo_sels ,
                                             String A183FuncaoAPF_Ativo ,
                                             IGxCollection AV126WWFuncaoAPFDS_36_Tffuncaoapf_ativo_sels ,
                                             String AV91WWFuncaoAPFDS_1_Dynamicfiltersselector1 ,
                                             String AV92WWFuncaoAPFDS_2_Funcaoapf_nome1 ,
                                             String AV93WWFuncaoAPFDS_3_Funcaoapf_tipo1 ,
                                             String AV95WWFuncaoAPFDS_5_Funcaoapf_ativo1 ,
                                             bool AV96WWFuncaoAPFDS_6_Dynamicfiltersenabled2 ,
                                             String AV97WWFuncaoAPFDS_7_Dynamicfiltersselector2 ,
                                             String AV98WWFuncaoAPFDS_8_Funcaoapf_nome2 ,
                                             String AV99WWFuncaoAPFDS_9_Funcaoapf_tipo2 ,
                                             String AV101WWFuncaoAPFDS_11_Funcaoapf_ativo2 ,
                                             bool AV102WWFuncaoAPFDS_12_Dynamicfiltersenabled3 ,
                                             String AV103WWFuncaoAPFDS_13_Dynamicfiltersselector3 ,
                                             String AV104WWFuncaoAPFDS_14_Funcaoapf_nome3 ,
                                             String AV105WWFuncaoAPFDS_15_Funcaoapf_tipo3 ,
                                             String AV107WWFuncaoAPFDS_17_Funcaoapf_ativo3 ,
                                             bool AV108WWFuncaoAPFDS_18_Dynamicfiltersenabled4 ,
                                             String AV109WWFuncaoAPFDS_19_Dynamicfiltersselector4 ,
                                             String AV110WWFuncaoAPFDS_20_Funcaoapf_nome4 ,
                                             String AV111WWFuncaoAPFDS_21_Funcaoapf_tipo4 ,
                                             String AV113WWFuncaoAPFDS_23_Funcaoapf_ativo4 ,
                                             String AV115WWFuncaoAPFDS_25_Tffuncaoapf_nome_sel ,
                                             String AV114WWFuncaoAPFDS_24_Tffuncaoapf_nome ,
                                             int AV116WWFuncaoAPFDS_26_Tffuncaoapf_tipo_sels_Count ,
                                             String AV125WWFuncaoAPFDS_35_Tffuncaoapf_funapfpainom_sel ,
                                             String AV124WWFuncaoAPFDS_34_Tffuncaoapf_funapfpainom ,
                                             int AV126WWFuncaoAPFDS_36_Tffuncaoapf_ativo_sels_Count ,
                                             String A166FuncaoAPF_Nome ,
                                             String A363FuncaoAPF_FunAPFPaiNom ,
                                             String AV94WWFuncaoAPFDS_4_Funcaoapf_complexidade1 ,
                                             String AV100WWFuncaoAPFDS_10_Funcaoapf_complexidade2 ,
                                             String AV106WWFuncaoAPFDS_16_Funcaoapf_complexidade3 ,
                                             String AV112WWFuncaoAPFDS_22_Funcaoapf_complexidade4 ,
                                             short AV117WWFuncaoAPFDS_27_Tffuncaoapf_td ,
                                             short A388FuncaoAPF_TD ,
                                             short AV118WWFuncaoAPFDS_28_Tffuncaoapf_td_to ,
                                             short AV119WWFuncaoAPFDS_29_Tffuncaoapf_ar ,
                                             short A387FuncaoAPF_AR ,
                                             short AV120WWFuncaoAPFDS_30_Tffuncaoapf_ar_to ,
                                             int AV121WWFuncaoAPFDS_31_Tffuncaoapf_complexidade_sels_Count ,
                                             decimal AV122WWFuncaoAPFDS_32_Tffuncaoapf_pf ,
                                             decimal A386FuncaoAPF_PF ,
                                             decimal AV123WWFuncaoAPFDS_33_Tffuncaoapf_pf_to )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [17] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT T1.[FuncaoAPF_FunAPFPaiCod] AS FuncaoAPF_FunAPFPaiCod, T1.[FuncaoAPF_Nome], T2.[FuncaoAPF_Nome] AS FuncaoAPF_FunAPFPaiNom, T1.[FuncaoAPF_Ativo], T1.[FuncaoAPF_Tipo], T1.[FuncaoAPF_Codigo] FROM ([FuncoesAPF] T1 WITH (NOLOCK) LEFT JOIN [FuncoesAPF] T2 WITH (NOLOCK) ON T2.[FuncaoAPF_Codigo] = T1.[FuncaoAPF_FunAPFPaiCod])";
         if ( ( StringUtil.StrCmp(AV91WWFuncaoAPFDS_1_Dynamicfiltersselector1, "FUNCAOAPF_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV92WWFuncaoAPFDS_2_Funcaoapf_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoAPF_Nome] like @lV92WWFuncaoAPFDS_2_Funcaoapf_nome1 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoAPF_Nome] like @lV92WWFuncaoAPFDS_2_Funcaoapf_nome1 + '%')";
            }
         }
         else
         {
            GXv_int4[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV91WWFuncaoAPFDS_1_Dynamicfiltersselector1, "FUNCAOAPF_TIPO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV93WWFuncaoAPFDS_3_Funcaoapf_tipo1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoAPF_Tipo] = @AV93WWFuncaoAPFDS_3_Funcaoapf_tipo1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoAPF_Tipo] = @AV93WWFuncaoAPFDS_3_Funcaoapf_tipo1)";
            }
         }
         else
         {
            GXv_int4[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV91WWFuncaoAPFDS_1_Dynamicfiltersselector1, "FUNCAOAPF_ATIVO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV95WWFuncaoAPFDS_5_Funcaoapf_ativo1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoAPF_Ativo] = @AV95WWFuncaoAPFDS_5_Funcaoapf_ativo1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoAPF_Ativo] = @AV95WWFuncaoAPFDS_5_Funcaoapf_ativo1)";
            }
         }
         else
         {
            GXv_int4[2] = 1;
         }
         if ( AV96WWFuncaoAPFDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV97WWFuncaoAPFDS_7_Dynamicfiltersselector2, "FUNCAOAPF_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV98WWFuncaoAPFDS_8_Funcaoapf_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoAPF_Nome] like @lV98WWFuncaoAPFDS_8_Funcaoapf_nome2 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoAPF_Nome] like @lV98WWFuncaoAPFDS_8_Funcaoapf_nome2 + '%')";
            }
         }
         else
         {
            GXv_int4[3] = 1;
         }
         if ( AV96WWFuncaoAPFDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV97WWFuncaoAPFDS_7_Dynamicfiltersselector2, "FUNCAOAPF_TIPO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV99WWFuncaoAPFDS_9_Funcaoapf_tipo2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoAPF_Tipo] = @AV99WWFuncaoAPFDS_9_Funcaoapf_tipo2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoAPF_Tipo] = @AV99WWFuncaoAPFDS_9_Funcaoapf_tipo2)";
            }
         }
         else
         {
            GXv_int4[4] = 1;
         }
         if ( AV96WWFuncaoAPFDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV97WWFuncaoAPFDS_7_Dynamicfiltersselector2, "FUNCAOAPF_ATIVO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV101WWFuncaoAPFDS_11_Funcaoapf_ativo2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoAPF_Ativo] = @AV101WWFuncaoAPFDS_11_Funcaoapf_ativo2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoAPF_Ativo] = @AV101WWFuncaoAPFDS_11_Funcaoapf_ativo2)";
            }
         }
         else
         {
            GXv_int4[5] = 1;
         }
         if ( AV102WWFuncaoAPFDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV103WWFuncaoAPFDS_13_Dynamicfiltersselector3, "FUNCAOAPF_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV104WWFuncaoAPFDS_14_Funcaoapf_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoAPF_Nome] like @lV104WWFuncaoAPFDS_14_Funcaoapf_nome3 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoAPF_Nome] like @lV104WWFuncaoAPFDS_14_Funcaoapf_nome3 + '%')";
            }
         }
         else
         {
            GXv_int4[6] = 1;
         }
         if ( AV102WWFuncaoAPFDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV103WWFuncaoAPFDS_13_Dynamicfiltersselector3, "FUNCAOAPF_TIPO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV105WWFuncaoAPFDS_15_Funcaoapf_tipo3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoAPF_Tipo] = @AV105WWFuncaoAPFDS_15_Funcaoapf_tipo3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoAPF_Tipo] = @AV105WWFuncaoAPFDS_15_Funcaoapf_tipo3)";
            }
         }
         else
         {
            GXv_int4[7] = 1;
         }
         if ( AV102WWFuncaoAPFDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV103WWFuncaoAPFDS_13_Dynamicfiltersselector3, "FUNCAOAPF_ATIVO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV107WWFuncaoAPFDS_17_Funcaoapf_ativo3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoAPF_Ativo] = @AV107WWFuncaoAPFDS_17_Funcaoapf_ativo3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoAPF_Ativo] = @AV107WWFuncaoAPFDS_17_Funcaoapf_ativo3)";
            }
         }
         else
         {
            GXv_int4[8] = 1;
         }
         if ( AV108WWFuncaoAPFDS_18_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV109WWFuncaoAPFDS_19_Dynamicfiltersselector4, "FUNCAOAPF_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV110WWFuncaoAPFDS_20_Funcaoapf_nome4)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoAPF_Nome] like @lV110WWFuncaoAPFDS_20_Funcaoapf_nome4 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoAPF_Nome] like @lV110WWFuncaoAPFDS_20_Funcaoapf_nome4 + '%')";
            }
         }
         else
         {
            GXv_int4[9] = 1;
         }
         if ( AV108WWFuncaoAPFDS_18_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV109WWFuncaoAPFDS_19_Dynamicfiltersselector4, "FUNCAOAPF_TIPO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV111WWFuncaoAPFDS_21_Funcaoapf_tipo4)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoAPF_Tipo] = @AV111WWFuncaoAPFDS_21_Funcaoapf_tipo4)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoAPF_Tipo] = @AV111WWFuncaoAPFDS_21_Funcaoapf_tipo4)";
            }
         }
         else
         {
            GXv_int4[10] = 1;
         }
         if ( AV108WWFuncaoAPFDS_18_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV109WWFuncaoAPFDS_19_Dynamicfiltersselector4, "FUNCAOAPF_ATIVO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV113WWFuncaoAPFDS_23_Funcaoapf_ativo4)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoAPF_Ativo] = @AV113WWFuncaoAPFDS_23_Funcaoapf_ativo4)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoAPF_Ativo] = @AV113WWFuncaoAPFDS_23_Funcaoapf_ativo4)";
            }
         }
         else
         {
            GXv_int4[11] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV115WWFuncaoAPFDS_25_Tffuncaoapf_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV114WWFuncaoAPFDS_24_Tffuncaoapf_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoAPF_Nome] like @lV114WWFuncaoAPFDS_24_Tffuncaoapf_nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoAPF_Nome] like @lV114WWFuncaoAPFDS_24_Tffuncaoapf_nome)";
            }
         }
         else
         {
            GXv_int4[12] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV115WWFuncaoAPFDS_25_Tffuncaoapf_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoAPF_Nome] = @AV115WWFuncaoAPFDS_25_Tffuncaoapf_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoAPF_Nome] = @AV115WWFuncaoAPFDS_25_Tffuncaoapf_nome_sel)";
            }
         }
         else
         {
            GXv_int4[13] = 1;
         }
         if ( AV116WWFuncaoAPFDS_26_Tffuncaoapf_tipo_sels_Count > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV116WWFuncaoAPFDS_26_Tffuncaoapf_tipo_sels, "T1.[FuncaoAPF_Tipo] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV116WWFuncaoAPFDS_26_Tffuncaoapf_tipo_sels, "T1.[FuncaoAPF_Tipo] IN (", ")") + ")";
            }
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV125WWFuncaoAPFDS_35_Tffuncaoapf_funapfpainom_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV124WWFuncaoAPFDS_34_Tffuncaoapf_funapfpainom)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[FuncaoAPF_Nome] like @lV124WWFuncaoAPFDS_34_Tffuncaoapf_funapfpainom)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[FuncaoAPF_Nome] like @lV124WWFuncaoAPFDS_34_Tffuncaoapf_funapfpainom)";
            }
         }
         else
         {
            GXv_int4[14] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV125WWFuncaoAPFDS_35_Tffuncaoapf_funapfpainom_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[FuncaoAPF_Nome] = @AV125WWFuncaoAPFDS_35_Tffuncaoapf_funapfpainom_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[FuncaoAPF_Nome] = @AV125WWFuncaoAPFDS_35_Tffuncaoapf_funapfpainom_sel)";
            }
         }
         else
         {
            GXv_int4[15] = 1;
         }
         if ( AV126WWFuncaoAPFDS_36_Tffuncaoapf_ativo_sels_Count > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV126WWFuncaoAPFDS_36_Tffuncaoapf_ativo_sels, "T1.[FuncaoAPF_Ativo] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV126WWFuncaoAPFDS_36_Tffuncaoapf_ativo_sels, "T1.[FuncaoAPF_Ativo] IN (", ")") + ")";
            }
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T1.[FuncaoAPF_Nome]";
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      protected Object[] conditional_P00GN3( IGxContext context ,
                                             String A185FuncaoAPF_Complexidade ,
                                             IGxCollection AV121WWFuncaoAPFDS_31_Tffuncaoapf_complexidade_sels ,
                                             String A184FuncaoAPF_Tipo ,
                                             IGxCollection AV116WWFuncaoAPFDS_26_Tffuncaoapf_tipo_sels ,
                                             String A183FuncaoAPF_Ativo ,
                                             IGxCollection AV126WWFuncaoAPFDS_36_Tffuncaoapf_ativo_sels ,
                                             String AV91WWFuncaoAPFDS_1_Dynamicfiltersselector1 ,
                                             String AV92WWFuncaoAPFDS_2_Funcaoapf_nome1 ,
                                             String AV93WWFuncaoAPFDS_3_Funcaoapf_tipo1 ,
                                             String AV95WWFuncaoAPFDS_5_Funcaoapf_ativo1 ,
                                             bool AV96WWFuncaoAPFDS_6_Dynamicfiltersenabled2 ,
                                             String AV97WWFuncaoAPFDS_7_Dynamicfiltersselector2 ,
                                             String AV98WWFuncaoAPFDS_8_Funcaoapf_nome2 ,
                                             String AV99WWFuncaoAPFDS_9_Funcaoapf_tipo2 ,
                                             String AV101WWFuncaoAPFDS_11_Funcaoapf_ativo2 ,
                                             bool AV102WWFuncaoAPFDS_12_Dynamicfiltersenabled3 ,
                                             String AV103WWFuncaoAPFDS_13_Dynamicfiltersselector3 ,
                                             String AV104WWFuncaoAPFDS_14_Funcaoapf_nome3 ,
                                             String AV105WWFuncaoAPFDS_15_Funcaoapf_tipo3 ,
                                             String AV107WWFuncaoAPFDS_17_Funcaoapf_ativo3 ,
                                             bool AV108WWFuncaoAPFDS_18_Dynamicfiltersenabled4 ,
                                             String AV109WWFuncaoAPFDS_19_Dynamicfiltersselector4 ,
                                             String AV110WWFuncaoAPFDS_20_Funcaoapf_nome4 ,
                                             String AV111WWFuncaoAPFDS_21_Funcaoapf_tipo4 ,
                                             String AV113WWFuncaoAPFDS_23_Funcaoapf_ativo4 ,
                                             String AV115WWFuncaoAPFDS_25_Tffuncaoapf_nome_sel ,
                                             String AV114WWFuncaoAPFDS_24_Tffuncaoapf_nome ,
                                             int AV116WWFuncaoAPFDS_26_Tffuncaoapf_tipo_sels_Count ,
                                             String AV125WWFuncaoAPFDS_35_Tffuncaoapf_funapfpainom_sel ,
                                             String AV124WWFuncaoAPFDS_34_Tffuncaoapf_funapfpainom ,
                                             int AV126WWFuncaoAPFDS_36_Tffuncaoapf_ativo_sels_Count ,
                                             String A166FuncaoAPF_Nome ,
                                             String A363FuncaoAPF_FunAPFPaiNom ,
                                             String AV94WWFuncaoAPFDS_4_Funcaoapf_complexidade1 ,
                                             String AV100WWFuncaoAPFDS_10_Funcaoapf_complexidade2 ,
                                             String AV106WWFuncaoAPFDS_16_Funcaoapf_complexidade3 ,
                                             String AV112WWFuncaoAPFDS_22_Funcaoapf_complexidade4 ,
                                             short AV117WWFuncaoAPFDS_27_Tffuncaoapf_td ,
                                             short A388FuncaoAPF_TD ,
                                             short AV118WWFuncaoAPFDS_28_Tffuncaoapf_td_to ,
                                             short AV119WWFuncaoAPFDS_29_Tffuncaoapf_ar ,
                                             short A387FuncaoAPF_AR ,
                                             short AV120WWFuncaoAPFDS_30_Tffuncaoapf_ar_to ,
                                             int AV121WWFuncaoAPFDS_31_Tffuncaoapf_complexidade_sels_Count ,
                                             decimal AV122WWFuncaoAPFDS_32_Tffuncaoapf_pf ,
                                             decimal A386FuncaoAPF_PF ,
                                             decimal AV123WWFuncaoAPFDS_33_Tffuncaoapf_pf_to )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int6 ;
         GXv_int6 = new short [17] ;
         Object[] GXv_Object7 ;
         GXv_Object7 = new Object [2] ;
         scmdbuf = "SELECT T1.[FuncaoAPF_FunAPFPaiCod] AS FuncaoAPF_FunAPFPaiCod, T2.[FuncaoAPF_Nome] AS FuncaoAPF_FunAPFPaiNom, T1.[FuncaoAPF_Ativo], T1.[FuncaoAPF_Tipo], T1.[FuncaoAPF_Nome], T1.[FuncaoAPF_Codigo] FROM ([FuncoesAPF] T1 WITH (NOLOCK) LEFT JOIN [FuncoesAPF] T2 WITH (NOLOCK) ON T2.[FuncaoAPF_Codigo] = T1.[FuncaoAPF_FunAPFPaiCod])";
         if ( ( StringUtil.StrCmp(AV91WWFuncaoAPFDS_1_Dynamicfiltersselector1, "FUNCAOAPF_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV92WWFuncaoAPFDS_2_Funcaoapf_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoAPF_Nome] like @lV92WWFuncaoAPFDS_2_Funcaoapf_nome1 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoAPF_Nome] like @lV92WWFuncaoAPFDS_2_Funcaoapf_nome1 + '%')";
            }
         }
         else
         {
            GXv_int6[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV91WWFuncaoAPFDS_1_Dynamicfiltersselector1, "FUNCAOAPF_TIPO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV93WWFuncaoAPFDS_3_Funcaoapf_tipo1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoAPF_Tipo] = @AV93WWFuncaoAPFDS_3_Funcaoapf_tipo1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoAPF_Tipo] = @AV93WWFuncaoAPFDS_3_Funcaoapf_tipo1)";
            }
         }
         else
         {
            GXv_int6[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV91WWFuncaoAPFDS_1_Dynamicfiltersselector1, "FUNCAOAPF_ATIVO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV95WWFuncaoAPFDS_5_Funcaoapf_ativo1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoAPF_Ativo] = @AV95WWFuncaoAPFDS_5_Funcaoapf_ativo1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoAPF_Ativo] = @AV95WWFuncaoAPFDS_5_Funcaoapf_ativo1)";
            }
         }
         else
         {
            GXv_int6[2] = 1;
         }
         if ( AV96WWFuncaoAPFDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV97WWFuncaoAPFDS_7_Dynamicfiltersselector2, "FUNCAOAPF_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV98WWFuncaoAPFDS_8_Funcaoapf_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoAPF_Nome] like @lV98WWFuncaoAPFDS_8_Funcaoapf_nome2 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoAPF_Nome] like @lV98WWFuncaoAPFDS_8_Funcaoapf_nome2 + '%')";
            }
         }
         else
         {
            GXv_int6[3] = 1;
         }
         if ( AV96WWFuncaoAPFDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV97WWFuncaoAPFDS_7_Dynamicfiltersselector2, "FUNCAOAPF_TIPO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV99WWFuncaoAPFDS_9_Funcaoapf_tipo2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoAPF_Tipo] = @AV99WWFuncaoAPFDS_9_Funcaoapf_tipo2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoAPF_Tipo] = @AV99WWFuncaoAPFDS_9_Funcaoapf_tipo2)";
            }
         }
         else
         {
            GXv_int6[4] = 1;
         }
         if ( AV96WWFuncaoAPFDS_6_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV97WWFuncaoAPFDS_7_Dynamicfiltersselector2, "FUNCAOAPF_ATIVO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV101WWFuncaoAPFDS_11_Funcaoapf_ativo2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoAPF_Ativo] = @AV101WWFuncaoAPFDS_11_Funcaoapf_ativo2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoAPF_Ativo] = @AV101WWFuncaoAPFDS_11_Funcaoapf_ativo2)";
            }
         }
         else
         {
            GXv_int6[5] = 1;
         }
         if ( AV102WWFuncaoAPFDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV103WWFuncaoAPFDS_13_Dynamicfiltersselector3, "FUNCAOAPF_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV104WWFuncaoAPFDS_14_Funcaoapf_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoAPF_Nome] like @lV104WWFuncaoAPFDS_14_Funcaoapf_nome3 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoAPF_Nome] like @lV104WWFuncaoAPFDS_14_Funcaoapf_nome3 + '%')";
            }
         }
         else
         {
            GXv_int6[6] = 1;
         }
         if ( AV102WWFuncaoAPFDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV103WWFuncaoAPFDS_13_Dynamicfiltersselector3, "FUNCAOAPF_TIPO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV105WWFuncaoAPFDS_15_Funcaoapf_tipo3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoAPF_Tipo] = @AV105WWFuncaoAPFDS_15_Funcaoapf_tipo3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoAPF_Tipo] = @AV105WWFuncaoAPFDS_15_Funcaoapf_tipo3)";
            }
         }
         else
         {
            GXv_int6[7] = 1;
         }
         if ( AV102WWFuncaoAPFDS_12_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV103WWFuncaoAPFDS_13_Dynamicfiltersselector3, "FUNCAOAPF_ATIVO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV107WWFuncaoAPFDS_17_Funcaoapf_ativo3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoAPF_Ativo] = @AV107WWFuncaoAPFDS_17_Funcaoapf_ativo3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoAPF_Ativo] = @AV107WWFuncaoAPFDS_17_Funcaoapf_ativo3)";
            }
         }
         else
         {
            GXv_int6[8] = 1;
         }
         if ( AV108WWFuncaoAPFDS_18_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV109WWFuncaoAPFDS_19_Dynamicfiltersselector4, "FUNCAOAPF_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV110WWFuncaoAPFDS_20_Funcaoapf_nome4)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoAPF_Nome] like @lV110WWFuncaoAPFDS_20_Funcaoapf_nome4 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoAPF_Nome] like @lV110WWFuncaoAPFDS_20_Funcaoapf_nome4 + '%')";
            }
         }
         else
         {
            GXv_int6[9] = 1;
         }
         if ( AV108WWFuncaoAPFDS_18_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV109WWFuncaoAPFDS_19_Dynamicfiltersselector4, "FUNCAOAPF_TIPO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV111WWFuncaoAPFDS_21_Funcaoapf_tipo4)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoAPF_Tipo] = @AV111WWFuncaoAPFDS_21_Funcaoapf_tipo4)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoAPF_Tipo] = @AV111WWFuncaoAPFDS_21_Funcaoapf_tipo4)";
            }
         }
         else
         {
            GXv_int6[10] = 1;
         }
         if ( AV108WWFuncaoAPFDS_18_Dynamicfiltersenabled4 && ( StringUtil.StrCmp(AV109WWFuncaoAPFDS_19_Dynamicfiltersselector4, "FUNCAOAPF_ATIVO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV113WWFuncaoAPFDS_23_Funcaoapf_ativo4)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoAPF_Ativo] = @AV113WWFuncaoAPFDS_23_Funcaoapf_ativo4)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoAPF_Ativo] = @AV113WWFuncaoAPFDS_23_Funcaoapf_ativo4)";
            }
         }
         else
         {
            GXv_int6[11] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV115WWFuncaoAPFDS_25_Tffuncaoapf_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV114WWFuncaoAPFDS_24_Tffuncaoapf_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoAPF_Nome] like @lV114WWFuncaoAPFDS_24_Tffuncaoapf_nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoAPF_Nome] like @lV114WWFuncaoAPFDS_24_Tffuncaoapf_nome)";
            }
         }
         else
         {
            GXv_int6[12] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV115WWFuncaoAPFDS_25_Tffuncaoapf_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[FuncaoAPF_Nome] = @AV115WWFuncaoAPFDS_25_Tffuncaoapf_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[FuncaoAPF_Nome] = @AV115WWFuncaoAPFDS_25_Tffuncaoapf_nome_sel)";
            }
         }
         else
         {
            GXv_int6[13] = 1;
         }
         if ( AV116WWFuncaoAPFDS_26_Tffuncaoapf_tipo_sels_Count > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV116WWFuncaoAPFDS_26_Tffuncaoapf_tipo_sels, "T1.[FuncaoAPF_Tipo] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV116WWFuncaoAPFDS_26_Tffuncaoapf_tipo_sels, "T1.[FuncaoAPF_Tipo] IN (", ")") + ")";
            }
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV125WWFuncaoAPFDS_35_Tffuncaoapf_funapfpainom_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV124WWFuncaoAPFDS_34_Tffuncaoapf_funapfpainom)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[FuncaoAPF_Nome] like @lV124WWFuncaoAPFDS_34_Tffuncaoapf_funapfpainom)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[FuncaoAPF_Nome] like @lV124WWFuncaoAPFDS_34_Tffuncaoapf_funapfpainom)";
            }
         }
         else
         {
            GXv_int6[14] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV125WWFuncaoAPFDS_35_Tffuncaoapf_funapfpainom_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[FuncaoAPF_Nome] = @AV125WWFuncaoAPFDS_35_Tffuncaoapf_funapfpainom_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[FuncaoAPF_Nome] = @AV125WWFuncaoAPFDS_35_Tffuncaoapf_funapfpainom_sel)";
            }
         }
         else
         {
            GXv_int6[15] = 1;
         }
         if ( AV126WWFuncaoAPFDS_36_Tffuncaoapf_ativo_sels_Count > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV126WWFuncaoAPFDS_36_Tffuncaoapf_ativo_sels, "T1.[FuncaoAPF_Ativo] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV126WWFuncaoAPFDS_36_Tffuncaoapf_ativo_sels, "T1.[FuncaoAPF_Ativo] IN (", ")") + ")";
            }
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T2.[FuncaoAPF_Nome]";
         GXv_Object7[0] = scmdbuf;
         GXv_Object7[1] = GXv_int6;
         return GXv_Object7 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00GN2(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (IGxCollection)dynConstraints[3] , (String)dynConstraints[4] , (IGxCollection)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (bool)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (bool)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (bool)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] , (String)dynConstraints[26] , (int)dynConstraints[27] , (String)dynConstraints[28] , (String)dynConstraints[29] , (int)dynConstraints[30] , (String)dynConstraints[31] , (String)dynConstraints[32] , (String)dynConstraints[33] , (String)dynConstraints[34] , (String)dynConstraints[35] , (String)dynConstraints[36] , (short)dynConstraints[37] , (short)dynConstraints[38] , (short)dynConstraints[39] , (short)dynConstraints[40] , (short)dynConstraints[41] , (short)dynConstraints[42] , (int)dynConstraints[43] , (decimal)dynConstraints[44] , (decimal)dynConstraints[45] , (decimal)dynConstraints[46] );
               case 1 :
                     return conditional_P00GN3(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (IGxCollection)dynConstraints[3] , (String)dynConstraints[4] , (IGxCollection)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (bool)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (bool)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (bool)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] , (String)dynConstraints[26] , (int)dynConstraints[27] , (String)dynConstraints[28] , (String)dynConstraints[29] , (int)dynConstraints[30] , (String)dynConstraints[31] , (String)dynConstraints[32] , (String)dynConstraints[33] , (String)dynConstraints[34] , (String)dynConstraints[35] , (String)dynConstraints[36] , (short)dynConstraints[37] , (short)dynConstraints[38] , (short)dynConstraints[39] , (short)dynConstraints[40] , (short)dynConstraints[41] , (short)dynConstraints[42] , (int)dynConstraints[43] , (decimal)dynConstraints[44] , (decimal)dynConstraints[45] , (decimal)dynConstraints[46] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00GN2 ;
          prmP00GN2 = new Object[] {
          new Object[] {"@AV121WWFCount",SqlDbType.Int,9,0} ,
          new Object[] {"@lV92WWFuncaoAPFDS_2_Funcaoapf_nome1",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV93WWFuncaoAPFDS_3_Funcaoapf_tipo1",SqlDbType.Char,3,0} ,
          new Object[] {"@AV95WWFuncaoAPFDS_5_Funcaoapf_ativo1",SqlDbType.Char,1,0} ,
          new Object[] {"@lV98WWFuncaoAPFDS_8_Funcaoapf_nome2",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV99WWFuncaoAPFDS_9_Funcaoapf_tipo2",SqlDbType.Char,3,0} ,
          new Object[] {"@AV101WWFuncaoAPFDS_11_Funcaoapf_ativo2",SqlDbType.Char,1,0} ,
          new Object[] {"@lV104WWFuncaoAPFDS_14_Funcaoapf_nome3",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV105WWFuncaoAPFDS_15_Funcaoapf_tipo3",SqlDbType.Char,3,0} ,
          new Object[] {"@AV107WWFuncaoAPFDS_17_Funcaoapf_ativo3",SqlDbType.Char,1,0} ,
          new Object[] {"@lV110WWFuncaoAPFDS_20_Funcaoapf_nome4",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV111WWFuncaoAPFDS_21_Funcaoapf_tipo4",SqlDbType.Char,3,0} ,
          new Object[] {"@AV113WWFuncaoAPFDS_23_Funcaoapf_ativo4",SqlDbType.Char,1,0} ,
          new Object[] {"@lV114WWFuncaoAPFDS_24_Tffuncaoapf_nome",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV115WWFuncaoAPFDS_25_Tffuncaoapf_nome_sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV124WWFuncaoAPFDS_34_Tffuncaoapf_funapfpainom",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV125WWFuncaoAPFDS_35_Tffuncaoapf_funapfpainom_sel",SqlDbType.VarChar,200,0}
          } ;
          Object[] prmP00GN3 ;
          prmP00GN3 = new Object[] {
          new Object[] {"@AV121WWFCount",SqlDbType.Int,9,0} ,
          new Object[] {"@lV92WWFuncaoAPFDS_2_Funcaoapf_nome1",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV93WWFuncaoAPFDS_3_Funcaoapf_tipo1",SqlDbType.Char,3,0} ,
          new Object[] {"@AV95WWFuncaoAPFDS_5_Funcaoapf_ativo1",SqlDbType.Char,1,0} ,
          new Object[] {"@lV98WWFuncaoAPFDS_8_Funcaoapf_nome2",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV99WWFuncaoAPFDS_9_Funcaoapf_tipo2",SqlDbType.Char,3,0} ,
          new Object[] {"@AV101WWFuncaoAPFDS_11_Funcaoapf_ativo2",SqlDbType.Char,1,0} ,
          new Object[] {"@lV104WWFuncaoAPFDS_14_Funcaoapf_nome3",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV105WWFuncaoAPFDS_15_Funcaoapf_tipo3",SqlDbType.Char,3,0} ,
          new Object[] {"@AV107WWFuncaoAPFDS_17_Funcaoapf_ativo3",SqlDbType.Char,1,0} ,
          new Object[] {"@lV110WWFuncaoAPFDS_20_Funcaoapf_nome4",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV111WWFuncaoAPFDS_21_Funcaoapf_tipo4",SqlDbType.Char,3,0} ,
          new Object[] {"@AV113WWFuncaoAPFDS_23_Funcaoapf_ativo4",SqlDbType.Char,1,0} ,
          new Object[] {"@lV114WWFuncaoAPFDS_24_Tffuncaoapf_nome",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV115WWFuncaoAPFDS_25_Tffuncaoapf_nome_sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV124WWFuncaoAPFDS_34_Tffuncaoapf_funapfpainom",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV125WWFuncaoAPFDS_35_Tffuncaoapf_funapfpainom_sel",SqlDbType.VarChar,200,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00GN2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00GN2,100,0,true,false )
             ,new CursorDef("P00GN3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00GN3,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getVarchar(2) ;
                ((String[]) buf[3])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getString(4, 1) ;
                ((String[]) buf[6])[0] = rslt.getString(5, 3) ;
                ((int[]) buf[7])[0] = rslt.getInt(6) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getString(3, 1) ;
                ((String[]) buf[5])[0] = rslt.getString(4, 3) ;
                ((String[]) buf[6])[0] = rslt.getVarchar(5) ;
                ((int[]) buf[7])[0] = rslt.getInt(6) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[17]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[17]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getwwfuncaoapffilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getwwfuncaoapffilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getwwfuncaoapffilterdata") )
          {
             return  ;
          }
          getwwfuncaoapffilterdata worker = new getwwfuncaoapffilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
