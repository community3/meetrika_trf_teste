/*
               File: Email_Instancia_BC
        Description: Email_Instancia
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 23:8:51.88
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class email_instancia_bc : GXHttpHandler, IGxSilentTrn
   {
      public email_instancia_bc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public email_instancia_bc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      protected void INITTRN( )
      {
      }

      public void GetInsDefault( )
      {
         ReadRow45184( ) ;
         standaloneNotModal( ) ;
         InitializeNonKey45184( ) ;
         standaloneModal( ) ;
         AddRow45184( ) ;
         Gx_mode = "INS";
         return  ;
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               Z1666Email_Instancia_Guid = (Guid)(A1666Email_Instancia_Guid);
               SetMode( "UPD") ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      public bool Reindex( )
      {
         return true ;
      }

      protected void CONFIRM_450( )
      {
         BeforeValidate45184( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls45184( ) ;
            }
            else
            {
               CheckExtendedTable45184( ) ;
               if ( AnyError == 0 )
               {
                  ZM45184( 5) ;
               }
               CloseExtendedTableCursors45184( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
         }
      }

      protected void ZM45184( short GX_JID )
      {
         if ( ( GX_JID == 4 ) || ( GX_JID == 0 ) )
         {
            Z1673Email_Instancia_Titulo = A1673Email_Instancia_Titulo;
            Z1677Email_Instancia_AreaTrabalho = A1677Email_Instancia_AreaTrabalho;
            Z1665Email_Guid = (Guid)(A1665Email_Guid);
         }
         if ( ( GX_JID == 5 ) || ( GX_JID == 0 ) )
         {
            Z1669Email_Titulo = A1669Email_Titulo;
         }
         if ( GX_JID == -4 )
         {
            Z1666Email_Instancia_Guid = (Guid)(A1666Email_Instancia_Guid);
            Z1673Email_Instancia_Titulo = A1673Email_Instancia_Titulo;
            Z1674Email_Instancia_Corpo = A1674Email_Instancia_Corpo;
            Z1677Email_Instancia_AreaTrabalho = A1677Email_Instancia_AreaTrabalho;
            Z1678Email_Instancia_Parms = A1678Email_Instancia_Parms;
            Z1665Email_Guid = (Guid)(A1665Email_Guid);
            Z1669Email_Titulo = A1669Email_Titulo;
            Z1670Email_Corpo = A1670Email_Corpo;
         }
      }

      protected void standaloneNotModal( )
      {
      }

      protected void standaloneModal( )
      {
      }

      protected void Load45184( )
      {
         /* Using cursor BC00455 */
         pr_default.execute(3, new Object[] {A1666Email_Instancia_Guid});
         if ( (pr_default.getStatus(3) != 101) )
         {
            RcdFound184 = 1;
            A1669Email_Titulo = BC00455_A1669Email_Titulo[0];
            A1670Email_Corpo = BC00455_A1670Email_Corpo[0];
            A1673Email_Instancia_Titulo = BC00455_A1673Email_Instancia_Titulo[0];
            A1674Email_Instancia_Corpo = BC00455_A1674Email_Instancia_Corpo[0];
            A1677Email_Instancia_AreaTrabalho = BC00455_A1677Email_Instancia_AreaTrabalho[0];
            A1678Email_Instancia_Parms = BC00455_A1678Email_Instancia_Parms[0];
            A1665Email_Guid = (Guid)((Guid)(BC00455_A1665Email_Guid[0]));
            ZM45184( -4) ;
         }
         pr_default.close(3);
         OnLoadActions45184( ) ;
      }

      protected void OnLoadActions45184( )
      {
         if ( (System.Guid.Empty==A1666Email_Instancia_Guid) )
         {
            A1666Email_Instancia_Guid = (Guid)(Guid.NewGuid( ));
         }
      }

      protected void CheckExtendedTable45184( )
      {
         standaloneModal( ) ;
         if ( (System.Guid.Empty==A1666Email_Instancia_Guid) )
         {
            A1666Email_Instancia_Guid = (Guid)(Guid.NewGuid( ));
         }
         /* Using cursor BC00454 */
         pr_default.execute(2, new Object[] {A1665Email_Guid});
         if ( (pr_default.getStatus(2) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Email'.", "ForeignKeyNotFound", 1, "EMAIL_GUID");
            AnyError = 1;
         }
         A1669Email_Titulo = BC00454_A1669Email_Titulo[0];
         A1670Email_Corpo = BC00454_A1670Email_Corpo[0];
         pr_default.close(2);
      }

      protected void CloseExtendedTableCursors45184( )
      {
         pr_default.close(2);
      }

      protected void enableDisable( )
      {
      }

      protected void GetKey45184( )
      {
         /* Using cursor BC00456 */
         pr_default.execute(4, new Object[] {A1666Email_Instancia_Guid});
         if ( (pr_default.getStatus(4) != 101) )
         {
            RcdFound184 = 1;
         }
         else
         {
            RcdFound184 = 0;
         }
         pr_default.close(4);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor BC00453 */
         pr_default.execute(1, new Object[] {A1666Email_Instancia_Guid});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM45184( 4) ;
            RcdFound184 = 1;
            A1666Email_Instancia_Guid = (Guid)((Guid)(BC00453_A1666Email_Instancia_Guid[0]));
            A1673Email_Instancia_Titulo = BC00453_A1673Email_Instancia_Titulo[0];
            A1674Email_Instancia_Corpo = BC00453_A1674Email_Instancia_Corpo[0];
            A1677Email_Instancia_AreaTrabalho = BC00453_A1677Email_Instancia_AreaTrabalho[0];
            A1678Email_Instancia_Parms = BC00453_A1678Email_Instancia_Parms[0];
            A1665Email_Guid = (Guid)((Guid)(BC00453_A1665Email_Guid[0]));
            Z1666Email_Instancia_Guid = (Guid)(A1666Email_Instancia_Guid);
            sMode184 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal( ) ;
            Load45184( ) ;
            if ( AnyError == 1 )
            {
               RcdFound184 = 0;
               InitializeNonKey45184( ) ;
            }
            Gx_mode = sMode184;
         }
         else
         {
            RcdFound184 = 0;
            InitializeNonKey45184( ) ;
            sMode184 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal( ) ;
            Gx_mode = sMode184;
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey45184( ) ;
         if ( RcdFound184 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
         }
         getByPrimaryKey( ) ;
      }

      protected void insert_Check( )
      {
         CONFIRM_450( ) ;
         IsConfirmed = 0;
      }

      protected void update_Check( )
      {
         insert_Check( ) ;
      }

      protected void delete_Check( )
      {
         insert_Check( ) ;
      }

      protected void CheckOptimisticConcurrency45184( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor BC00452 */
            pr_default.execute(0, new Object[] {A1666Email_Instancia_Guid});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Email_Instancia"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) || ( StringUtil.StrCmp(Z1673Email_Instancia_Titulo, BC00452_A1673Email_Instancia_Titulo[0]) != 0 ) || ( Z1677Email_Instancia_AreaTrabalho != BC00452_A1677Email_Instancia_AreaTrabalho[0] ) || ( Z1665Email_Guid != BC00452_A1665Email_Guid[0] ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"Email_Instancia"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert45184( )
      {
         BeforeValidate45184( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable45184( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM45184( 0) ;
            CheckOptimisticConcurrency45184( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm45184( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert45184( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC00457 */
                     pr_default.execute(5, new Object[] {A1666Email_Instancia_Guid, A1673Email_Instancia_Titulo, A1674Email_Instancia_Corpo, A1677Email_Instancia_AreaTrabalho, A1678Email_Instancia_Parms, A1665Email_Guid});
                     pr_default.close(5);
                     dsDefault.SmartCacheProvider.SetUpdated("Email_Instancia") ;
                     if ( (pr_default.getStatus(5) == 1) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load45184( ) ;
            }
            EndLevel45184( ) ;
         }
         CloseExtendedTableCursors45184( ) ;
      }

      protected void Update45184( )
      {
         BeforeValidate45184( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable45184( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency45184( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm45184( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate45184( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC00458 */
                     pr_default.execute(6, new Object[] {A1673Email_Instancia_Titulo, A1674Email_Instancia_Corpo, A1677Email_Instancia_AreaTrabalho, A1678Email_Instancia_Parms, A1665Email_Guid, A1666Email_Instancia_Guid});
                     pr_default.close(6);
                     dsDefault.SmartCacheProvider.SetUpdated("Email_Instancia") ;
                     if ( (pr_default.getStatus(6) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Email_Instancia"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate45184( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           getByPrimaryKey( ) ;
                           GX_msglist.addItem(context.GetMessage( "GXM_sucupdated", ""), 0, "", true);
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel45184( ) ;
         }
         CloseExtendedTableCursors45184( ) ;
      }

      protected void DeferredUpdate45184( )
      {
      }

      protected void delete( )
      {
         Gx_mode = "DLT";
         BeforeValidate45184( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency45184( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls45184( ) ;
            AfterConfirm45184( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete45184( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor BC00459 */
                  pr_default.execute(7, new Object[] {A1666Email_Instancia_Guid});
                  pr_default.close(7);
                  dsDefault.SmartCacheProvider.SetUpdated("Email_Instancia") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_sucdeleted", ""), 0, "", true);
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode184 = Gx_mode;
         Gx_mode = "DLT";
         EndLevel45184( ) ;
         Gx_mode = sMode184;
      }

      protected void OnDeleteControls45184( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            /* Using cursor BC004510 */
            pr_default.execute(8, new Object[] {A1665Email_Guid});
            A1669Email_Titulo = BC004510_A1669Email_Titulo[0];
            A1670Email_Corpo = BC004510_A1670Email_Corpo[0];
            pr_default.close(8);
         }
         if ( AnyError == 0 )
         {
            /* Using cursor BC004511 */
            pr_default.execute(9, new Object[] {A1666Email_Instancia_Guid});
            if ( (pr_default.getStatus(9) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Email_Instancia_Destinatarios"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(9);
         }
      }

      protected void EndLevel45184( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete45184( ) ;
         }
         if ( AnyError == 0 )
         {
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanKeyStart45184( )
      {
         /* Using cursor BC004512 */
         pr_default.execute(10, new Object[] {A1666Email_Instancia_Guid});
         RcdFound184 = 0;
         if ( (pr_default.getStatus(10) != 101) )
         {
            RcdFound184 = 1;
            A1666Email_Instancia_Guid = (Guid)((Guid)(BC004512_A1666Email_Instancia_Guid[0]));
            A1669Email_Titulo = BC004512_A1669Email_Titulo[0];
            A1670Email_Corpo = BC004512_A1670Email_Corpo[0];
            A1673Email_Instancia_Titulo = BC004512_A1673Email_Instancia_Titulo[0];
            A1674Email_Instancia_Corpo = BC004512_A1674Email_Instancia_Corpo[0];
            A1677Email_Instancia_AreaTrabalho = BC004512_A1677Email_Instancia_AreaTrabalho[0];
            A1678Email_Instancia_Parms = BC004512_A1678Email_Instancia_Parms[0];
            A1665Email_Guid = (Guid)((Guid)(BC004512_A1665Email_Guid[0]));
         }
         /* Load Subordinate Levels */
      }

      protected void ScanKeyNext45184( )
      {
         /* Scan next routine */
         pr_default.readNext(10);
         RcdFound184 = 0;
         ScanKeyLoad45184( ) ;
      }

      protected void ScanKeyLoad45184( )
      {
         sMode184 = Gx_mode;
         Gx_mode = "DSP";
         if ( (pr_default.getStatus(10) != 101) )
         {
            RcdFound184 = 1;
            A1666Email_Instancia_Guid = (Guid)((Guid)(BC004512_A1666Email_Instancia_Guid[0]));
            A1669Email_Titulo = BC004512_A1669Email_Titulo[0];
            A1670Email_Corpo = BC004512_A1670Email_Corpo[0];
            A1673Email_Instancia_Titulo = BC004512_A1673Email_Instancia_Titulo[0];
            A1674Email_Instancia_Corpo = BC004512_A1674Email_Instancia_Corpo[0];
            A1677Email_Instancia_AreaTrabalho = BC004512_A1677Email_Instancia_AreaTrabalho[0];
            A1678Email_Instancia_Parms = BC004512_A1678Email_Instancia_Parms[0];
            A1665Email_Guid = (Guid)((Guid)(BC004512_A1665Email_Guid[0]));
         }
         Gx_mode = sMode184;
      }

      protected void ScanKeyEnd45184( )
      {
         pr_default.close(10);
      }

      protected void AfterConfirm45184( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert45184( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate45184( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete45184( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete45184( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate45184( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes45184( )
      {
      }

      protected void AddRow45184( )
      {
         VarsToRow184( bcEmail_Instancia) ;
      }

      protected void ReadRow45184( )
      {
         RowToVars184( bcEmail_Instancia, 1) ;
      }

      protected void InitializeNonKey45184( )
      {
         A1665Email_Guid = (Guid)(System.Guid.Empty);
         A1669Email_Titulo = "";
         A1670Email_Corpo = "";
         A1673Email_Instancia_Titulo = "";
         A1674Email_Instancia_Corpo = "";
         A1677Email_Instancia_AreaTrabalho = 0;
         A1678Email_Instancia_Parms = "";
         Z1673Email_Instancia_Titulo = "";
         Z1677Email_Instancia_AreaTrabalho = 0;
         Z1665Email_Guid = (Guid)(System.Guid.Empty);
      }

      protected void InitAll45184( )
      {
         A1666Email_Instancia_Guid = (Guid)(System.Guid.Empty);
         InitializeNonKey45184( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      public void VarsToRow184( SdtEmail_Instancia obj184 )
      {
         obj184.gxTpr_Mode = Gx_mode;
         obj184.gxTpr_Email_guid = (Guid)(A1665Email_Guid);
         obj184.gxTpr_Email_titulo = A1669Email_Titulo;
         obj184.gxTpr_Email_corpo = A1670Email_Corpo;
         obj184.gxTpr_Email_instancia_titulo = A1673Email_Instancia_Titulo;
         obj184.gxTpr_Email_instancia_corpo = A1674Email_Instancia_Corpo;
         obj184.gxTpr_Email_instancia_areatrabalho = A1677Email_Instancia_AreaTrabalho;
         obj184.gxTpr_Email_instancia_parms = A1678Email_Instancia_Parms;
         obj184.gxTpr_Email_instancia_guid = (Guid)(A1666Email_Instancia_Guid);
         obj184.gxTpr_Email_instancia_guid_Z = (Guid)(Z1666Email_Instancia_Guid);
         obj184.gxTpr_Email_guid_Z = (Guid)(Z1665Email_Guid);
         obj184.gxTpr_Email_titulo_Z = Z1669Email_Titulo;
         obj184.gxTpr_Email_instancia_titulo_Z = Z1673Email_Instancia_Titulo;
         obj184.gxTpr_Email_instancia_areatrabalho_Z = Z1677Email_Instancia_AreaTrabalho;
         obj184.gxTpr_Mode = Gx_mode;
         return  ;
      }

      public void KeyVarsToRow184( SdtEmail_Instancia obj184 )
      {
         obj184.gxTpr_Email_instancia_guid = (Guid)(A1666Email_Instancia_Guid);
         return  ;
      }

      public void RowToVars184( SdtEmail_Instancia obj184 ,
                                int forceLoad )
      {
         Gx_mode = obj184.gxTpr_Mode;
         A1665Email_Guid = (Guid)(obj184.gxTpr_Email_guid);
         A1669Email_Titulo = obj184.gxTpr_Email_titulo;
         A1670Email_Corpo = obj184.gxTpr_Email_corpo;
         A1673Email_Instancia_Titulo = obj184.gxTpr_Email_instancia_titulo;
         A1674Email_Instancia_Corpo = obj184.gxTpr_Email_instancia_corpo;
         A1677Email_Instancia_AreaTrabalho = obj184.gxTpr_Email_instancia_areatrabalho;
         A1678Email_Instancia_Parms = obj184.gxTpr_Email_instancia_parms;
         A1666Email_Instancia_Guid = (Guid)(obj184.gxTpr_Email_instancia_guid);
         Z1666Email_Instancia_Guid = (Guid)(obj184.gxTpr_Email_instancia_guid_Z);
         Z1665Email_Guid = (Guid)(obj184.gxTpr_Email_guid_Z);
         Z1669Email_Titulo = obj184.gxTpr_Email_titulo_Z;
         Z1673Email_Instancia_Titulo = obj184.gxTpr_Email_instancia_titulo_Z;
         Z1677Email_Instancia_AreaTrabalho = obj184.gxTpr_Email_instancia_areatrabalho_Z;
         Gx_mode = obj184.gxTpr_Mode;
         return  ;
      }

      public void LoadKey( Object[] obj )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         A1666Email_Instancia_Guid = (Guid)((Guid)getParm(obj,0));
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         InitializeNonKey45184( ) ;
         ScanKeyStart45184( ) ;
         if ( RcdFound184 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
            Z1666Email_Instancia_Guid = (Guid)(A1666Email_Instancia_Guid);
         }
         ZM45184( -4) ;
         OnLoadActions45184( ) ;
         AddRow45184( ) ;
         ScanKeyEnd45184( ) ;
         if ( RcdFound184 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "");
            AnyError = 1;
         }
         context.GX_msglist = BackMsgLst;
      }

      public void Load( )
      {
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         RowToVars184( bcEmail_Instancia, 0) ;
         ScanKeyStart45184( ) ;
         if ( RcdFound184 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
            Z1666Email_Instancia_Guid = (Guid)(A1666Email_Instancia_Guid);
         }
         ZM45184( -4) ;
         OnLoadActions45184( ) ;
         AddRow45184( ) ;
         ScanKeyEnd45184( ) ;
         if ( RcdFound184 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "");
            AnyError = 1;
         }
         context.GX_msglist = BackMsgLst;
      }

      public void Save( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         IsConfirmed = 1;
         RowToVars184( bcEmail_Instancia, 0) ;
         nKeyPressed = 1;
         GetKey45184( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            Insert45184( ) ;
         }
         else
         {
            if ( RcdFound184 == 1 )
            {
               if ( A1666Email_Instancia_Guid != Z1666Email_Instancia_Guid )
               {
                  A1666Email_Instancia_Guid = (Guid)(Z1666Email_Instancia_Guid);
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
               }
               else
               {
                  Gx_mode = "UPD";
                  /* Update record */
                  Update45184( ) ;
               }
            }
            else
            {
               if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else
               {
                  if ( A1666Email_Instancia_Guid != Z1666Email_Instancia_Guid )
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     else
                     {
                        Gx_mode = "INS";
                        /* Insert record */
                        Insert45184( ) ;
                     }
                  }
                  else
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                        AnyError = 1;
                     }
                     else
                     {
                        Gx_mode = "INS";
                        /* Insert record */
                        Insert45184( ) ;
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         VarsToRow184( bcEmail_Instancia) ;
         context.GX_msglist = BackMsgLst;
         return  ;
      }

      public void Check( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         RowToVars184( bcEmail_Instancia, 0) ;
         nKeyPressed = 3;
         IsConfirmed = 0;
         GetKey45184( ) ;
         if ( RcdFound184 == 1 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
               AnyError = 1;
            }
            else if ( A1666Email_Instancia_Guid != Z1666Email_Instancia_Guid )
            {
               A1666Email_Instancia_Guid = (Guid)(Z1666Email_Instancia_Guid);
               GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "DuplicatePrimaryKey", 1, "");
               AnyError = 1;
            }
            else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               delete_Check( ) ;
            }
            else
            {
               Gx_mode = "UPD";
               update_Check( ) ;
            }
         }
         else
         {
            if ( A1666Email_Instancia_Guid != Z1666Email_Instancia_Guid )
            {
               Gx_mode = "INS";
               insert_Check( ) ;
            }
            else
            {
               if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                  AnyError = 1;
               }
               else
               {
                  Gx_mode = "INS";
                  insert_Check( ) ;
               }
            }
         }
         pr_default.close(1);
         pr_default.close(8);
         context.RollbackDataStores( "Email_Instancia_BC");
         VarsToRow184( bcEmail_Instancia) ;
         context.GX_msglist = BackMsgLst;
         return  ;
      }

      public int Errors( )
      {
         if ( AnyError == 0 )
         {
            return (int)(0) ;
         }
         return (int)(1) ;
      }

      public msglist GetMessages( )
      {
         return LclMsgLst ;
      }

      public String GetMode( )
      {
         Gx_mode = bcEmail_Instancia.gxTpr_Mode;
         return Gx_mode ;
      }

      public void SetMode( String lMode )
      {
         Gx_mode = lMode;
         bcEmail_Instancia.gxTpr_Mode = Gx_mode;
         return  ;
      }

      public void SetSDT( GxSilentTrnSdt sdt ,
                          short sdtToBc )
      {
         if ( sdt != bcEmail_Instancia )
         {
            bcEmail_Instancia = (SdtEmail_Instancia)(sdt);
            if ( StringUtil.StrCmp(bcEmail_Instancia.gxTpr_Mode, "") == 0 )
            {
               bcEmail_Instancia.gxTpr_Mode = "INS";
            }
            if ( sdtToBc == 1 )
            {
               VarsToRow184( bcEmail_Instancia) ;
            }
            else
            {
               RowToVars184( bcEmail_Instancia, 1) ;
            }
         }
         else
         {
            if ( StringUtil.StrCmp(bcEmail_Instancia.gxTpr_Mode, "") == 0 )
            {
               bcEmail_Instancia.gxTpr_Mode = "INS";
            }
         }
         return  ;
      }

      public void ReloadFromSDT( )
      {
         RowToVars184( bcEmail_Instancia, 1) ;
         return  ;
      }

      public SdtEmail_Instancia Email_Instancia_BC
      {
         get {
            return bcEmail_Instancia ;
         }

      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         createObjects();
         initialize();
      }

      protected override void createObjects( )
      {
      }

      protected void Process( )
      {
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(8);
      }

      public override void initialize( )
      {
         scmdbuf = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Gx_mode = "";
         Z1666Email_Instancia_Guid = (Guid)(System.Guid.Empty);
         A1666Email_Instancia_Guid = (Guid)(System.Guid.Empty);
         Z1673Email_Instancia_Titulo = "";
         A1673Email_Instancia_Titulo = "";
         Z1665Email_Guid = (Guid)(System.Guid.Empty);
         A1665Email_Guid = (Guid)(System.Guid.Empty);
         Z1669Email_Titulo = "";
         A1669Email_Titulo = "";
         Z1674Email_Instancia_Corpo = "";
         A1674Email_Instancia_Corpo = "";
         Z1678Email_Instancia_Parms = "";
         A1678Email_Instancia_Parms = "";
         Z1670Email_Corpo = "";
         A1670Email_Corpo = "";
         BC00455_A1666Email_Instancia_Guid = new Guid[] {System.Guid.Empty} ;
         BC00455_A1669Email_Titulo = new String[] {""} ;
         BC00455_A1670Email_Corpo = new String[] {""} ;
         BC00455_A1673Email_Instancia_Titulo = new String[] {""} ;
         BC00455_A1674Email_Instancia_Corpo = new String[] {""} ;
         BC00455_A1677Email_Instancia_AreaTrabalho = new int[1] ;
         BC00455_A1678Email_Instancia_Parms = new String[] {""} ;
         BC00455_A1665Email_Guid = new Guid[] {System.Guid.Empty} ;
         BC00454_A1669Email_Titulo = new String[] {""} ;
         BC00454_A1670Email_Corpo = new String[] {""} ;
         BC00456_A1666Email_Instancia_Guid = new Guid[] {System.Guid.Empty} ;
         BC00453_A1666Email_Instancia_Guid = new Guid[] {System.Guid.Empty} ;
         BC00453_A1673Email_Instancia_Titulo = new String[] {""} ;
         BC00453_A1674Email_Instancia_Corpo = new String[] {""} ;
         BC00453_A1677Email_Instancia_AreaTrabalho = new int[1] ;
         BC00453_A1678Email_Instancia_Parms = new String[] {""} ;
         BC00453_A1665Email_Guid = new Guid[] {System.Guid.Empty} ;
         sMode184 = "";
         BC00452_A1666Email_Instancia_Guid = new Guid[] {System.Guid.Empty} ;
         BC00452_A1673Email_Instancia_Titulo = new String[] {""} ;
         BC00452_A1674Email_Instancia_Corpo = new String[] {""} ;
         BC00452_A1677Email_Instancia_AreaTrabalho = new int[1] ;
         BC00452_A1678Email_Instancia_Parms = new String[] {""} ;
         BC00452_A1665Email_Guid = new Guid[] {System.Guid.Empty} ;
         BC004510_A1669Email_Titulo = new String[] {""} ;
         BC004510_A1670Email_Corpo = new String[] {""} ;
         BC004511_A1666Email_Instancia_Guid = new Guid[] {System.Guid.Empty} ;
         BC004511_A1667Email_Instancia_Destinatarios_Guid = new Guid[] {System.Guid.Empty} ;
         BC004512_A1666Email_Instancia_Guid = new Guid[] {System.Guid.Empty} ;
         BC004512_A1669Email_Titulo = new String[] {""} ;
         BC004512_A1670Email_Corpo = new String[] {""} ;
         BC004512_A1673Email_Instancia_Titulo = new String[] {""} ;
         BC004512_A1674Email_Instancia_Corpo = new String[] {""} ;
         BC004512_A1677Email_Instancia_AreaTrabalho = new int[1] ;
         BC004512_A1678Email_Instancia_Parms = new String[] {""} ;
         BC004512_A1665Email_Guid = new Guid[] {System.Guid.Empty} ;
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.email_instancia_bc__default(),
            new Object[][] {
                new Object[] {
               BC00452_A1666Email_Instancia_Guid, BC00452_A1673Email_Instancia_Titulo, BC00452_A1674Email_Instancia_Corpo, BC00452_A1677Email_Instancia_AreaTrabalho, BC00452_A1678Email_Instancia_Parms, BC00452_A1665Email_Guid
               }
               , new Object[] {
               BC00453_A1666Email_Instancia_Guid, BC00453_A1673Email_Instancia_Titulo, BC00453_A1674Email_Instancia_Corpo, BC00453_A1677Email_Instancia_AreaTrabalho, BC00453_A1678Email_Instancia_Parms, BC00453_A1665Email_Guid
               }
               , new Object[] {
               BC00454_A1669Email_Titulo, BC00454_A1670Email_Corpo
               }
               , new Object[] {
               BC00455_A1666Email_Instancia_Guid, BC00455_A1669Email_Titulo, BC00455_A1670Email_Corpo, BC00455_A1673Email_Instancia_Titulo, BC00455_A1674Email_Instancia_Corpo, BC00455_A1677Email_Instancia_AreaTrabalho, BC00455_A1678Email_Instancia_Parms, BC00455_A1665Email_Guid
               }
               , new Object[] {
               BC00456_A1666Email_Instancia_Guid
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               BC004510_A1669Email_Titulo, BC004510_A1670Email_Corpo
               }
               , new Object[] {
               BC004511_A1666Email_Instancia_Guid, BC004511_A1667Email_Instancia_Destinatarios_Guid
               }
               , new Object[] {
               BC004512_A1666Email_Instancia_Guid, BC004512_A1669Email_Titulo, BC004512_A1670Email_Corpo, BC004512_A1673Email_Instancia_Titulo, BC004512_A1674Email_Instancia_Corpo, BC004512_A1677Email_Instancia_AreaTrabalho, BC004512_A1678Email_Instancia_Parms, BC004512_A1665Email_Guid
               }
            }
         );
         INITTRN();
         /* Execute Start event if defined. */
         standaloneNotModal( ) ;
      }

      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short GX_JID ;
      private short RcdFound184 ;
      private int trnEnded ;
      private int Z1677Email_Instancia_AreaTrabalho ;
      private int A1677Email_Instancia_AreaTrabalho ;
      private String scmdbuf ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String Gx_mode ;
      private String sMode184 ;
      private String Z1674Email_Instancia_Corpo ;
      private String A1674Email_Instancia_Corpo ;
      private String Z1678Email_Instancia_Parms ;
      private String A1678Email_Instancia_Parms ;
      private String Z1670Email_Corpo ;
      private String A1670Email_Corpo ;
      private String Z1673Email_Instancia_Titulo ;
      private String A1673Email_Instancia_Titulo ;
      private String Z1669Email_Titulo ;
      private String A1669Email_Titulo ;
      private Guid Z1666Email_Instancia_Guid ;
      private Guid A1666Email_Instancia_Guid ;
      private Guid Z1665Email_Guid ;
      private Guid A1665Email_Guid ;
      private SdtEmail_Instancia bcEmail_Instancia ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private Guid[] BC00455_A1666Email_Instancia_Guid ;
      private String[] BC00455_A1669Email_Titulo ;
      private String[] BC00455_A1670Email_Corpo ;
      private String[] BC00455_A1673Email_Instancia_Titulo ;
      private String[] BC00455_A1674Email_Instancia_Corpo ;
      private int[] BC00455_A1677Email_Instancia_AreaTrabalho ;
      private String[] BC00455_A1678Email_Instancia_Parms ;
      private Guid[] BC00455_A1665Email_Guid ;
      private String[] BC00454_A1669Email_Titulo ;
      private String[] BC00454_A1670Email_Corpo ;
      private Guid[] BC00456_A1666Email_Instancia_Guid ;
      private Guid[] BC00453_A1666Email_Instancia_Guid ;
      private String[] BC00453_A1673Email_Instancia_Titulo ;
      private String[] BC00453_A1674Email_Instancia_Corpo ;
      private int[] BC00453_A1677Email_Instancia_AreaTrabalho ;
      private String[] BC00453_A1678Email_Instancia_Parms ;
      private Guid[] BC00453_A1665Email_Guid ;
      private Guid[] BC00452_A1666Email_Instancia_Guid ;
      private String[] BC00452_A1673Email_Instancia_Titulo ;
      private String[] BC00452_A1674Email_Instancia_Corpo ;
      private int[] BC00452_A1677Email_Instancia_AreaTrabalho ;
      private String[] BC00452_A1678Email_Instancia_Parms ;
      private Guid[] BC00452_A1665Email_Guid ;
      private String[] BC004510_A1669Email_Titulo ;
      private String[] BC004510_A1670Email_Corpo ;
      private Guid[] BC004511_A1666Email_Instancia_Guid ;
      private Guid[] BC004511_A1667Email_Instancia_Destinatarios_Guid ;
      private Guid[] BC004512_A1666Email_Instancia_Guid ;
      private String[] BC004512_A1669Email_Titulo ;
      private String[] BC004512_A1670Email_Corpo ;
      private String[] BC004512_A1673Email_Instancia_Titulo ;
      private String[] BC004512_A1674Email_Instancia_Corpo ;
      private int[] BC004512_A1677Email_Instancia_AreaTrabalho ;
      private String[] BC004512_A1678Email_Instancia_Parms ;
      private Guid[] BC004512_A1665Email_Guid ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
   }

   public class email_instancia_bc__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new UpdateCursor(def[5])
         ,new UpdateCursor(def[6])
         ,new UpdateCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmBC00455 ;
          prmBC00455 = new Object[] {
          new Object[] {"@Email_Instancia_Guid",SqlDbType.UniqueIdentifier,4,0}
          } ;
          Object[] prmBC00454 ;
          prmBC00454 = new Object[] {
          new Object[] {"@Email_Guid",SqlDbType.UniqueIdentifier,4,0}
          } ;
          Object[] prmBC00456 ;
          prmBC00456 = new Object[] {
          new Object[] {"@Email_Instancia_Guid",SqlDbType.UniqueIdentifier,4,0}
          } ;
          Object[] prmBC00453 ;
          prmBC00453 = new Object[] {
          new Object[] {"@Email_Instancia_Guid",SqlDbType.UniqueIdentifier,4,0}
          } ;
          Object[] prmBC00452 ;
          prmBC00452 = new Object[] {
          new Object[] {"@Email_Instancia_Guid",SqlDbType.UniqueIdentifier,4,0}
          } ;
          Object[] prmBC00457 ;
          prmBC00457 = new Object[] {
          new Object[] {"@Email_Instancia_Guid",SqlDbType.UniqueIdentifier,4,0} ,
          new Object[] {"@Email_Instancia_Titulo",SqlDbType.VarChar,100,0} ,
          new Object[] {"@Email_Instancia_Corpo",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@Email_Instancia_AreaTrabalho",SqlDbType.Int,6,0} ,
          new Object[] {"@Email_Instancia_Parms",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@Email_Guid",SqlDbType.UniqueIdentifier,4,0}
          } ;
          Object[] prmBC00458 ;
          prmBC00458 = new Object[] {
          new Object[] {"@Email_Instancia_Titulo",SqlDbType.VarChar,100,0} ,
          new Object[] {"@Email_Instancia_Corpo",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@Email_Instancia_AreaTrabalho",SqlDbType.Int,6,0} ,
          new Object[] {"@Email_Instancia_Parms",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@Email_Guid",SqlDbType.UniqueIdentifier,4,0} ,
          new Object[] {"@Email_Instancia_Guid",SqlDbType.UniqueIdentifier,4,0}
          } ;
          Object[] prmBC00459 ;
          prmBC00459 = new Object[] {
          new Object[] {"@Email_Instancia_Guid",SqlDbType.UniqueIdentifier,4,0}
          } ;
          Object[] prmBC004510 ;
          prmBC004510 = new Object[] {
          new Object[] {"@Email_Guid",SqlDbType.UniqueIdentifier,4,0}
          } ;
          Object[] prmBC004511 ;
          prmBC004511 = new Object[] {
          new Object[] {"@Email_Instancia_Guid",SqlDbType.UniqueIdentifier,4,0}
          } ;
          Object[] prmBC004512 ;
          prmBC004512 = new Object[] {
          new Object[] {"@Email_Instancia_Guid",SqlDbType.UniqueIdentifier,4,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("BC00452", "SELECT [Email_Instancia_Guid], [Email_Instancia_Titulo], [Email_Instancia_Corpo], [Email_Instancia_AreaTrabalho], [Email_Instancia_Parms], [Email_Guid] FROM [Email_Instancia] WITH (UPDLOCK) WHERE [Email_Instancia_Guid] = @Email_Instancia_Guid ",true, GxErrorMask.GX_NOMASK, false, this,prmBC00452,1,0,true,false )
             ,new CursorDef("BC00453", "SELECT [Email_Instancia_Guid], [Email_Instancia_Titulo], [Email_Instancia_Corpo], [Email_Instancia_AreaTrabalho], [Email_Instancia_Parms], [Email_Guid] FROM [Email_Instancia] WITH (NOLOCK) WHERE [Email_Instancia_Guid] = @Email_Instancia_Guid ",true, GxErrorMask.GX_NOMASK, false, this,prmBC00453,1,0,true,false )
             ,new CursorDef("BC00454", "SELECT [Email_Titulo], [Email_Corpo] FROM [Email] WITH (NOLOCK) WHERE [Email_Guid] = @Email_Guid ",true, GxErrorMask.GX_NOMASK, false, this,prmBC00454,1,0,true,false )
             ,new CursorDef("BC00455", "SELECT TM1.[Email_Instancia_Guid], T2.[Email_Titulo], T2.[Email_Corpo], TM1.[Email_Instancia_Titulo], TM1.[Email_Instancia_Corpo], TM1.[Email_Instancia_AreaTrabalho], TM1.[Email_Instancia_Parms], TM1.[Email_Guid] FROM ([Email_Instancia] TM1 WITH (NOLOCK) INNER JOIN [Email] T2 WITH (NOLOCK) ON T2.[Email_Guid] = TM1.[Email_Guid]) WHERE TM1.[Email_Instancia_Guid] = @Email_Instancia_Guid ORDER BY TM1.[Email_Instancia_Guid]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmBC00455,100,0,true,false )
             ,new CursorDef("BC00456", "SELECT [Email_Instancia_Guid] FROM [Email_Instancia] WITH (NOLOCK) WHERE [Email_Instancia_Guid] = @Email_Instancia_Guid  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmBC00456,1,0,true,false )
             ,new CursorDef("BC00457", "INSERT INTO [Email_Instancia]([Email_Instancia_Guid], [Email_Instancia_Titulo], [Email_Instancia_Corpo], [Email_Instancia_AreaTrabalho], [Email_Instancia_Parms], [Email_Guid]) VALUES(@Email_Instancia_Guid, @Email_Instancia_Titulo, @Email_Instancia_Corpo, @Email_Instancia_AreaTrabalho, @Email_Instancia_Parms, @Email_Guid)", GxErrorMask.GX_NOMASK,prmBC00457)
             ,new CursorDef("BC00458", "UPDATE [Email_Instancia] SET [Email_Instancia_Titulo]=@Email_Instancia_Titulo, [Email_Instancia_Corpo]=@Email_Instancia_Corpo, [Email_Instancia_AreaTrabalho]=@Email_Instancia_AreaTrabalho, [Email_Instancia_Parms]=@Email_Instancia_Parms, [Email_Guid]=@Email_Guid  WHERE [Email_Instancia_Guid] = @Email_Instancia_Guid", GxErrorMask.GX_NOMASK,prmBC00458)
             ,new CursorDef("BC00459", "DELETE FROM [Email_Instancia]  WHERE [Email_Instancia_Guid] = @Email_Instancia_Guid", GxErrorMask.GX_NOMASK,prmBC00459)
             ,new CursorDef("BC004510", "SELECT [Email_Titulo], [Email_Corpo] FROM [Email] WITH (NOLOCK) WHERE [Email_Guid] = @Email_Guid ",true, GxErrorMask.GX_NOMASK, false, this,prmBC004510,1,0,true,false )
             ,new CursorDef("BC004511", "SELECT TOP 1 [Email_Instancia_Guid], [Email_Instancia_Destinatarios_Guid] FROM [Email_Instancia_Destinatarios] WITH (NOLOCK) WHERE [Email_Instancia_Guid] = @Email_Instancia_Guid ",true, GxErrorMask.GX_NOMASK, false, this,prmBC004511,1,0,true,true )
             ,new CursorDef("BC004512", "SELECT TM1.[Email_Instancia_Guid], T2.[Email_Titulo], T2.[Email_Corpo], TM1.[Email_Instancia_Titulo], TM1.[Email_Instancia_Corpo], TM1.[Email_Instancia_AreaTrabalho], TM1.[Email_Instancia_Parms], TM1.[Email_Guid] FROM ([Email_Instancia] TM1 WITH (NOLOCK) INNER JOIN [Email] T2 WITH (NOLOCK) ON T2.[Email_Guid] = TM1.[Email_Guid]) WHERE TM1.[Email_Instancia_Guid] = @Email_Instancia_Guid ORDER BY TM1.[Email_Instancia_Guid]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmBC004512,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((Guid[]) buf[0])[0] = rslt.getGuid(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((String[]) buf[2])[0] = rslt.getLongVarchar(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                ((String[]) buf[4])[0] = rslt.getLongVarchar(5) ;
                ((Guid[]) buf[5])[0] = rslt.getGuid(6) ;
                return;
             case 1 :
                ((Guid[]) buf[0])[0] = rslt.getGuid(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((String[]) buf[2])[0] = rslt.getLongVarchar(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                ((String[]) buf[4])[0] = rslt.getLongVarchar(5) ;
                ((Guid[]) buf[5])[0] = rslt.getGuid(6) ;
                return;
             case 2 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((String[]) buf[1])[0] = rslt.getLongVarchar(2) ;
                return;
             case 3 :
                ((Guid[]) buf[0])[0] = rslt.getGuid(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((String[]) buf[2])[0] = rslt.getLongVarchar(3) ;
                ((String[]) buf[3])[0] = rslt.getVarchar(4) ;
                ((String[]) buf[4])[0] = rslt.getLongVarchar(5) ;
                ((int[]) buf[5])[0] = rslt.getInt(6) ;
                ((String[]) buf[6])[0] = rslt.getLongVarchar(7) ;
                ((Guid[]) buf[7])[0] = rslt.getGuid(8) ;
                return;
             case 4 :
                ((Guid[]) buf[0])[0] = rslt.getGuid(1) ;
                return;
             case 8 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((String[]) buf[1])[0] = rslt.getLongVarchar(2) ;
                return;
             case 9 :
                ((Guid[]) buf[0])[0] = rslt.getGuid(1) ;
                ((Guid[]) buf[1])[0] = rslt.getGuid(2) ;
                return;
             case 10 :
                ((Guid[]) buf[0])[0] = rslt.getGuid(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((String[]) buf[2])[0] = rslt.getLongVarchar(3) ;
                ((String[]) buf[3])[0] = rslt.getVarchar(4) ;
                ((String[]) buf[4])[0] = rslt.getLongVarchar(5) ;
                ((int[]) buf[5])[0] = rslt.getInt(6) ;
                ((String[]) buf[6])[0] = rslt.getLongVarchar(7) ;
                ((Guid[]) buf[7])[0] = rslt.getGuid(8) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (Guid)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (Guid)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (Guid)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (Guid)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (Guid)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (Guid)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                stmt.SetParameter(3, (String)parms[2]);
                stmt.SetParameter(4, (int)parms[3]);
                stmt.SetParameter(5, (String)parms[4]);
                stmt.SetParameter(6, (Guid)parms[5]);
                return;
             case 6 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                stmt.SetParameter(4, (String)parms[3]);
                stmt.SetParameter(5, (Guid)parms[4]);
                stmt.SetParameter(6, (Guid)parms[5]);
                return;
             case 7 :
                stmt.SetParameter(1, (Guid)parms[0]);
                return;
             case 8 :
                stmt.SetParameter(1, (Guid)parms[0]);
                return;
             case 9 :
                stmt.SetParameter(1, (Guid)parms[0]);
                return;
             case 10 :
                stmt.SetParameter(1, (Guid)parms[0]);
                return;
       }
    }

 }

}
