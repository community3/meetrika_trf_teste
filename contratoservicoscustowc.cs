/*
               File: ContratoServicosCustoWC
        Description: Contrato Servicos Custo WC
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/29/2020 9:31:41.23
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class contratoservicoscustowc : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public contratoservicoscustowc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public contratoservicoscustowc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_ContratoServicosCusto_CntSrvCod )
      {
         this.AV7ContratoServicosCusto_CntSrvCod = aP0_ContratoServicosCusto_CntSrvCod;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  AV7ContratoServicosCusto_CntSrvCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7ContratoServicosCusto_CntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7ContratoServicosCusto_CntSrvCod), 6, 0)));
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(int)AV7ContratoServicosCusto_CntSrvCod});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
               {
                  nRC_GXsfl_5 = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  nGXsfl_5_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  sGXsfl_5_idx = GetNextPar( );
                  sPrefix = GetNextPar( );
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxnrGrid_newrow( ) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
               {
                  subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  AV13OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
                  AV14OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
                  AV20TFContratoServicosCusto_UsrPesNom = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20TFContratoServicosCusto_UsrPesNom", AV20TFContratoServicosCusto_UsrPesNom);
                  AV21TFContratoServicosCusto_UsrPesNom_Sel = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21TFContratoServicosCusto_UsrPesNom_Sel", AV21TFContratoServicosCusto_UsrPesNom_Sel);
                  AV24TFContratoServicosCusto_CstUntPrdNrm = NumberUtil.Val( GetNextPar( ), ".");
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24TFContratoServicosCusto_CstUntPrdNrm", StringUtil.LTrim( StringUtil.Str( AV24TFContratoServicosCusto_CstUntPrdNrm, 18, 5)));
                  AV25TFContratoServicosCusto_CstUntPrdNrm_To = NumberUtil.Val( GetNextPar( ), ".");
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25TFContratoServicosCusto_CstUntPrdNrm_To", StringUtil.LTrim( StringUtil.Str( AV25TFContratoServicosCusto_CstUntPrdNrm_To, 18, 5)));
                  AV28TFContratoServicosCusto_CstUntPrdExt = NumberUtil.Val( GetNextPar( ), ".");
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV28TFContratoServicosCusto_CstUntPrdExt", StringUtil.LTrim( StringUtil.Str( AV28TFContratoServicosCusto_CstUntPrdExt, 18, 5)));
                  AV29TFContratoServicosCusto_CstUntPrdExt_To = NumberUtil.Val( GetNextPar( ), ".");
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29TFContratoServicosCusto_CstUntPrdExt_To", StringUtil.LTrim( StringUtil.Str( AV29TFContratoServicosCusto_CstUntPrdExt_To, 18, 5)));
                  AV7ContratoServicosCusto_CntSrvCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7ContratoServicosCusto_CntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7ContratoServicosCusto_CntSrvCod), 6, 0)));
                  AV22ddo_ContratoServicosCusto_UsrPesNomTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22ddo_ContratoServicosCusto_UsrPesNomTitleControlIdToReplace", AV22ddo_ContratoServicosCusto_UsrPesNomTitleControlIdToReplace);
                  AV26ddo_ContratoServicosCusto_CstUntPrdNrmTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26ddo_ContratoServicosCusto_CstUntPrdNrmTitleControlIdToReplace", AV26ddo_ContratoServicosCusto_CstUntPrdNrmTitleControlIdToReplace);
                  AV30ddo_ContratoServicosCusto_CstUntPrdExtTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30ddo_ContratoServicosCusto_CstUntPrdExtTitleControlIdToReplace", AV30ddo_ContratoServicosCusto_CstUntPrdExtTitleControlIdToReplace);
                  AV36Pgmname = GetNextPar( );
                  sPrefix = GetNextPar( );
                  init_default_properties( ) ;
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV20TFContratoServicosCusto_UsrPesNom, AV21TFContratoServicosCusto_UsrPesNom_Sel, AV24TFContratoServicosCusto_CstUntPrdNrm, AV25TFContratoServicosCusto_CstUntPrdNrm_To, AV28TFContratoServicosCusto_CstUntPrdExt, AV29TFContratoServicosCusto_CstUntPrdExt_To, AV7ContratoServicosCusto_CntSrvCod, AV22ddo_ContratoServicosCusto_UsrPesNomTitleControlIdToReplace, AV26ddo_ContratoServicosCusto_CstUntPrdNrmTitleControlIdToReplace, AV30ddo_ContratoServicosCusto_CstUntPrdExtTitleControlIdToReplace, AV36Pgmname, sPrefix) ;
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
                  return  ;
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PAKZ2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV36Pgmname = "ContratoServicosCustoWC";
               context.Gx_err = 0;
               WSKZ2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "Contrato Servicos Custo WC") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20205299314146");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
            context.WriteHtmlText( "<body") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("contratoservicoscustowc.aspx") + "?" + UrlEncode("" +AV7ContratoServicosCusto_CntSrvCod)+"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vORDEREDDSC", StringUtil.BoolToStr( AV14OrderedDsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFCONTRATOSERVICOSCUSTO_USRPESNOM", StringUtil.RTrim( AV20TFContratoServicosCusto_UsrPesNom));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFCONTRATOSERVICOSCUSTO_USRPESNOM_SEL", StringUtil.RTrim( AV21TFContratoServicosCusto_UsrPesNom_Sel));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFCONTRATOSERVICOSCUSTO_CSTUNTPRDNRM", StringUtil.LTrim( StringUtil.NToC( AV24TFContratoServicosCusto_CstUntPrdNrm, 18, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFCONTRATOSERVICOSCUSTO_CSTUNTPRDNRM_TO", StringUtil.LTrim( StringUtil.NToC( AV25TFContratoServicosCusto_CstUntPrdNrm_To, 18, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFCONTRATOSERVICOSCUSTO_CSTUNTPRDEXT", StringUtil.LTrim( StringUtil.NToC( AV28TFContratoServicosCusto_CstUntPrdExt, 18, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFCONTRATOSERVICOSCUSTO_CSTUNTPRDEXT_TO", StringUtil.LTrim( StringUtil.NToC( AV29TFContratoServicosCusto_CstUntPrdExt_To, 18, 5, ",", "")));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, sPrefix+"nRC_GXsfl_5", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_5), 4, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vDDO_TITLESETTINGSICONS", AV31DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vDDO_TITLESETTINGSICONS", AV31DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vCONTRATOSERVICOSCUSTO_USRPESNOMTITLEFILTERDATA", AV19ContratoServicosCusto_UsrPesNomTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vCONTRATOSERVICOSCUSTO_USRPESNOMTITLEFILTERDATA", AV19ContratoServicosCusto_UsrPesNomTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vCONTRATOSERVICOSCUSTO_CSTUNTPRDNRMTITLEFILTERDATA", AV23ContratoServicosCusto_CstUntPrdNrmTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vCONTRATOSERVICOSCUSTO_CSTUNTPRDNRMTITLEFILTERDATA", AV23ContratoServicosCusto_CstUntPrdNrmTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vCONTRATOSERVICOSCUSTO_CSTUNTPRDEXTTITLEFILTERDATA", AV27ContratoServicosCusto_CstUntPrdExtTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vCONTRATOSERVICOSCUSTO_CSTUNTPRDEXTTITLEFILTERDATA", AV27ContratoServicosCusto_CstUntPrdExtTitleFilterData);
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOAV7ContratoServicosCusto_CntSrvCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOAV7ContratoServicosCusto_CntSrvCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vCONTRATOSERVICOSCUSTO_CNTSRVCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7ContratoServicosCusto_CntSrvCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vPGMNAME", StringUtil.RTrim( AV36Pgmname));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSCUSTO_USRPESNOM_Caption", StringUtil.RTrim( Ddo_contratoservicoscusto_usrpesnom_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSCUSTO_USRPESNOM_Tooltip", StringUtil.RTrim( Ddo_contratoservicoscusto_usrpesnom_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSCUSTO_USRPESNOM_Cls", StringUtil.RTrim( Ddo_contratoservicoscusto_usrpesnom_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSCUSTO_USRPESNOM_Filteredtext_set", StringUtil.RTrim( Ddo_contratoservicoscusto_usrpesnom_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSCUSTO_USRPESNOM_Selectedvalue_set", StringUtil.RTrim( Ddo_contratoservicoscusto_usrpesnom_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSCUSTO_USRPESNOM_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratoservicoscusto_usrpesnom_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSCUSTO_USRPESNOM_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratoservicoscusto_usrpesnom_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSCUSTO_USRPESNOM_Includesortasc", StringUtil.BoolToStr( Ddo_contratoservicoscusto_usrpesnom_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSCUSTO_USRPESNOM_Includesortdsc", StringUtil.BoolToStr( Ddo_contratoservicoscusto_usrpesnom_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSCUSTO_USRPESNOM_Sortedstatus", StringUtil.RTrim( Ddo_contratoservicoscusto_usrpesnom_Sortedstatus));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSCUSTO_USRPESNOM_Includefilter", StringUtil.BoolToStr( Ddo_contratoservicoscusto_usrpesnom_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSCUSTO_USRPESNOM_Filtertype", StringUtil.RTrim( Ddo_contratoservicoscusto_usrpesnom_Filtertype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSCUSTO_USRPESNOM_Filterisrange", StringUtil.BoolToStr( Ddo_contratoservicoscusto_usrpesnom_Filterisrange));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSCUSTO_USRPESNOM_Includedatalist", StringUtil.BoolToStr( Ddo_contratoservicoscusto_usrpesnom_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSCUSTO_USRPESNOM_Datalisttype", StringUtil.RTrim( Ddo_contratoservicoscusto_usrpesnom_Datalisttype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSCUSTO_USRPESNOM_Datalistproc", StringUtil.RTrim( Ddo_contratoservicoscusto_usrpesnom_Datalistproc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSCUSTO_USRPESNOM_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_contratoservicoscusto_usrpesnom_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSCUSTO_USRPESNOM_Sortasc", StringUtil.RTrim( Ddo_contratoservicoscusto_usrpesnom_Sortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSCUSTO_USRPESNOM_Sortdsc", StringUtil.RTrim( Ddo_contratoservicoscusto_usrpesnom_Sortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSCUSTO_USRPESNOM_Loadingdata", StringUtil.RTrim( Ddo_contratoservicoscusto_usrpesnom_Loadingdata));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSCUSTO_USRPESNOM_Cleanfilter", StringUtil.RTrim( Ddo_contratoservicoscusto_usrpesnom_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSCUSTO_USRPESNOM_Noresultsfound", StringUtil.RTrim( Ddo_contratoservicoscusto_usrpesnom_Noresultsfound));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSCUSTO_USRPESNOM_Searchbuttontext", StringUtil.RTrim( Ddo_contratoservicoscusto_usrpesnom_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM_Caption", StringUtil.RTrim( Ddo_contratoservicoscusto_cstuntprdnrm_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM_Tooltip", StringUtil.RTrim( Ddo_contratoservicoscusto_cstuntprdnrm_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM_Cls", StringUtil.RTrim( Ddo_contratoservicoscusto_cstuntprdnrm_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM_Filteredtext_set", StringUtil.RTrim( Ddo_contratoservicoscusto_cstuntprdnrm_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM_Filteredtextto_set", StringUtil.RTrim( Ddo_contratoservicoscusto_cstuntprdnrm_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratoservicoscusto_cstuntprdnrm_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratoservicoscusto_cstuntprdnrm_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM_Includesortasc", StringUtil.BoolToStr( Ddo_contratoservicoscusto_cstuntprdnrm_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM_Includesortdsc", StringUtil.BoolToStr( Ddo_contratoservicoscusto_cstuntprdnrm_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM_Sortedstatus", StringUtil.RTrim( Ddo_contratoservicoscusto_cstuntprdnrm_Sortedstatus));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM_Includefilter", StringUtil.BoolToStr( Ddo_contratoservicoscusto_cstuntprdnrm_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM_Filtertype", StringUtil.RTrim( Ddo_contratoservicoscusto_cstuntprdnrm_Filtertype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM_Filterisrange", StringUtil.BoolToStr( Ddo_contratoservicoscusto_cstuntprdnrm_Filterisrange));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM_Includedatalist", StringUtil.BoolToStr( Ddo_contratoservicoscusto_cstuntprdnrm_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM_Sortasc", StringUtil.RTrim( Ddo_contratoservicoscusto_cstuntprdnrm_Sortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM_Sortdsc", StringUtil.RTrim( Ddo_contratoservicoscusto_cstuntprdnrm_Sortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM_Cleanfilter", StringUtil.RTrim( Ddo_contratoservicoscusto_cstuntprdnrm_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM_Rangefilterfrom", StringUtil.RTrim( Ddo_contratoservicoscusto_cstuntprdnrm_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM_Rangefilterto", StringUtil.RTrim( Ddo_contratoservicoscusto_cstuntprdnrm_Rangefilterto));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM_Searchbuttontext", StringUtil.RTrim( Ddo_contratoservicoscusto_cstuntprdnrm_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDEXT_Caption", StringUtil.RTrim( Ddo_contratoservicoscusto_cstuntprdext_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDEXT_Tooltip", StringUtil.RTrim( Ddo_contratoservicoscusto_cstuntprdext_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDEXT_Cls", StringUtil.RTrim( Ddo_contratoservicoscusto_cstuntprdext_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDEXT_Filteredtext_set", StringUtil.RTrim( Ddo_contratoservicoscusto_cstuntprdext_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDEXT_Filteredtextto_set", StringUtil.RTrim( Ddo_contratoservicoscusto_cstuntprdext_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDEXT_Dropdownoptionstype", StringUtil.RTrim( Ddo_contratoservicoscusto_cstuntprdext_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDEXT_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_contratoservicoscusto_cstuntprdext_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDEXT_Includesortasc", StringUtil.BoolToStr( Ddo_contratoservicoscusto_cstuntprdext_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDEXT_Includesortdsc", StringUtil.BoolToStr( Ddo_contratoservicoscusto_cstuntprdext_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDEXT_Sortedstatus", StringUtil.RTrim( Ddo_contratoservicoscusto_cstuntprdext_Sortedstatus));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDEXT_Includefilter", StringUtil.BoolToStr( Ddo_contratoservicoscusto_cstuntprdext_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDEXT_Filtertype", StringUtil.RTrim( Ddo_contratoservicoscusto_cstuntprdext_Filtertype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDEXT_Filterisrange", StringUtil.BoolToStr( Ddo_contratoservicoscusto_cstuntprdext_Filterisrange));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDEXT_Includedatalist", StringUtil.BoolToStr( Ddo_contratoservicoscusto_cstuntprdext_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDEXT_Sortasc", StringUtil.RTrim( Ddo_contratoservicoscusto_cstuntprdext_Sortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDEXT_Sortdsc", StringUtil.RTrim( Ddo_contratoservicoscusto_cstuntprdext_Sortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDEXT_Cleanfilter", StringUtil.RTrim( Ddo_contratoservicoscusto_cstuntprdext_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDEXT_Rangefilterfrom", StringUtil.RTrim( Ddo_contratoservicoscusto_cstuntprdext_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDEXT_Rangefilterto", StringUtil.RTrim( Ddo_contratoservicoscusto_cstuntprdext_Rangefilterto));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDEXT_Searchbuttontext", StringUtil.RTrim( Ddo_contratoservicoscusto_cstuntprdext_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSCUSTO_USRPESNOM_Activeeventkey", StringUtil.RTrim( Ddo_contratoservicoscusto_usrpesnom_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSCUSTO_USRPESNOM_Filteredtext_get", StringUtil.RTrim( Ddo_contratoservicoscusto_usrpesnom_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSCUSTO_USRPESNOM_Selectedvalue_get", StringUtil.RTrim( Ddo_contratoservicoscusto_usrpesnom_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM_Activeeventkey", StringUtil.RTrim( Ddo_contratoservicoscusto_cstuntprdnrm_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM_Filteredtext_get", StringUtil.RTrim( Ddo_contratoservicoscusto_cstuntprdnrm_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM_Filteredtextto_get", StringUtil.RTrim( Ddo_contratoservicoscusto_cstuntprdnrm_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDEXT_Activeeventkey", StringUtil.RTrim( Ddo_contratoservicoscusto_cstuntprdext_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDEXT_Filteredtext_get", StringUtil.RTrim( Ddo_contratoservicoscusto_cstuntprdext_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDEXT_Filteredtextto_get", StringUtil.RTrim( Ddo_contratoservicoscusto_cstuntprdext_Filteredtextto_get));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseFormKZ2( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("contratoservicoscustowc.js", "?20205299314281");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            context.WriteHtmlTextNl( "</form>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "ContratoServicosCustoWC" ;
      }

      public override String GetPgmdesc( )
      {
         return "Contrato Servicos Custo WC" ;
      }

      protected void WBKZ0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "contratoservicoscustowc.aspx");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
            }
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, "", "", sPrefix, "false");
            wb_table1_2_KZ2( true) ;
         }
         else
         {
            wb_table1_2_KZ2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_KZ2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratoServicosCusto_CntSrvCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1471ContratoServicosCusto_CntSrvCod), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A1471ContratoServicosCusto_CntSrvCod), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoServicosCusto_CntSrvCod_Jsonclick, 0, "Attribute", "", "", "", edtContratoServicosCusto_CntSrvCod_Visible, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContratoServicosCustoWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 16,'" + sPrefix + "',false,'" + sGXsfl_5_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrderedby_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13OrderedBy), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV13OrderedBy), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,16);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrderedby_Jsonclick, 0, "Attribute", "", "", "", edtavOrderedby_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoServicosCustoWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 17,'" + sPrefix + "',false,'" + sGXsfl_5_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV14OrderedDsc), StringUtil.BoolToStr( AV14OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,17);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_ContratoServicosCustoWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'" + sPrefix + "',false,'" + sGXsfl_5_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoservicoscusto_usrpesnom_Internalname, StringUtil.RTrim( AV20TFContratoServicosCusto_UsrPesNom), StringUtil.RTrim( context.localUtil.Format( AV20TFContratoServicosCusto_UsrPesNom, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,18);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoservicoscusto_usrpesnom_Jsonclick, 0, "BootstrapAttribute100", "", "", "", edtavTfcontratoservicoscusto_usrpesnom_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ContratoServicosCustoWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 19,'" + sPrefix + "',false,'" + sGXsfl_5_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoservicoscusto_usrpesnom_sel_Internalname, StringUtil.RTrim( AV21TFContratoServicosCusto_UsrPesNom_Sel), StringUtil.RTrim( context.localUtil.Format( AV21TFContratoServicosCusto_UsrPesNom_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,19);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoservicoscusto_usrpesnom_sel_Jsonclick, 0, "BootstrapAttribute100", "", "", "", edtavTfcontratoservicoscusto_usrpesnom_sel_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ContratoServicosCustoWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 20,'" + sPrefix + "',false,'" + sGXsfl_5_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoservicoscusto_cstuntprdnrm_Internalname, StringUtil.LTrim( StringUtil.NToC( AV24TFContratoServicosCusto_CstUntPrdNrm, 18, 5, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV24TFContratoServicosCusto_CstUntPrdNrm, "ZZZ,ZZZ,ZZZ,ZZ9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,20);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoservicoscusto_cstuntprdnrm_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoservicoscusto_cstuntprdnrm_Visible, 1, 0, "text", "", 80, "px", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoServicosCustoWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'" + sPrefix + "',false,'" + sGXsfl_5_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoservicoscusto_cstuntprdnrm_to_Internalname, StringUtil.LTrim( StringUtil.NToC( AV25TFContratoServicosCusto_CstUntPrdNrm_To, 18, 5, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV25TFContratoServicosCusto_CstUntPrdNrm_To, "ZZZ,ZZZ,ZZZ,ZZ9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,21);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoservicoscusto_cstuntprdnrm_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoservicoscusto_cstuntprdnrm_to_Visible, 1, 0, "text", "", 80, "px", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoServicosCustoWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 22,'" + sPrefix + "',false,'" + sGXsfl_5_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoservicoscusto_cstuntprdext_Internalname, StringUtil.LTrim( StringUtil.NToC( AV28TFContratoServicosCusto_CstUntPrdExt, 18, 5, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV28TFContratoServicosCusto_CstUntPrdExt, "ZZZ,ZZZ,ZZZ,ZZ9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,22);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoservicoscusto_cstuntprdext_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoservicoscusto_cstuntprdext_Visible, 1, 0, "text", "", 80, "px", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoServicosCustoWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'" + sPrefix + "',false,'" + sGXsfl_5_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfcontratoservicoscusto_cstuntprdext_to_Internalname, StringUtil.LTrim( StringUtil.NToC( AV29TFContratoServicosCusto_CstUntPrdExt_To, 18, 5, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV29TFContratoServicosCusto_CstUntPrdExt_To, "ZZZ,ZZZ,ZZZ,ZZ9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,23);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfcontratoservicoscusto_cstuntprdext_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfcontratoservicoscusto_cstuntprdext_to_Visible, 1, 0, "text", "", 80, "px", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoServicosCustoWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_CONTRATOSERVICOSCUSTO_USRPESNOMContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 25,'" + sPrefix + "',false,'" + sGXsfl_5_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratoservicoscusto_usrpesnomtitlecontrolidtoreplace_Internalname, AV22ddo_ContratoServicosCusto_UsrPesNomTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,25);\"", 0, edtavDdo_contratoservicoscusto_usrpesnomtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_ContratoServicosCustoWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDNRMContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 27,'" + sPrefix + "',false,'" + sGXsfl_5_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratoservicoscusto_cstuntprdnrmtitlecontrolidtoreplace_Internalname, AV26ddo_ContratoServicosCusto_CstUntPrdNrmTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,27);\"", 0, edtavDdo_contratoservicoscusto_cstuntprdnrmtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_ContratoServicosCustoWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDEXTContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 29,'" + sPrefix + "',false,'" + sGXsfl_5_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_contratoservicoscusto_cstuntprdexttitlecontrolidtoreplace_Internalname, AV30ddo_ContratoServicosCusto_CstUntPrdExtTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,29);\"", 0, edtavDdo_contratoservicoscusto_cstuntprdexttitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_ContratoServicosCustoWC.htm");
         }
         wbLoad = true;
      }

      protected void STARTKZ2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "Contrato Servicos Custo WC", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUPKZ0( ) ;
            }
         }
      }

      protected void WSKZ2( )
      {
         STARTKZ2( ) ;
         EVTKZ2( ) ;
      }

      protected void EVTKZ2( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPKZ0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATOSERVICOSCUSTO_USRPESNOM.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPKZ0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E11KZ2 */
                                    E11KZ2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPKZ0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E12KZ2 */
                                    E12KZ2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDEXT.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPKZ0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E13KZ2 */
                                    E13KZ2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOINSERT'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPKZ0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E14KZ2 */
                                    E14KZ2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPKZ0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    GX_FocusControl = edtavOrderedby_Internalname;
                                    context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGING") == 0 )
                           {
                              context.wbHandled = 1;
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPKZ0( ) ;
                              }
                              sEvt = cgiGet( sPrefix+"GRIDPAGING");
                              if ( StringUtil.StrCmp(sEvt, "FIRST") == 0 )
                              {
                                 subgrid_firstpage( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "PREV") == 0 )
                              {
                                 subgrid_previouspage( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "NEXT") == 0 )
                              {
                                 subgrid_nextpage( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "LAST") == 0 )
                              {
                                 subgrid_lastpage( ) ;
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 10), "'DOUPDATE'") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 10), "'DODELETE'") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 10), "'DOUPDATE'") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 10), "'DODELETE'") == 0 ) )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPKZ0( ) ;
                              }
                              nGXsfl_5_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_5_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_5_idx), 4, 0)), 4, "0");
                              SubsflControlProps_52( ) ;
                              AV15Update = cgiGet( edtavUpdate_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavUpdate_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV15Update)) ? AV34Update_GXI : context.convertURL( context.PathToRelativeUrl( AV15Update))));
                              AV17Delete = cgiGet( edtavDelete_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDelete_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV17Delete)) ? AV35Delete_GXI : context.convertURL( context.PathToRelativeUrl( AV17Delete))));
                              A1473ContratoServicosCusto_Codigo = (int)(context.localUtil.CToN( cgiGet( edtContratoServicosCusto_Codigo_Internalname), ",", "."));
                              A1477ContratoServicosCusto_UsrPesNom = StringUtil.Upper( cgiGet( edtContratoServicosCusto_UsrPesNom_Internalname));
                              n1477ContratoServicosCusto_UsrPesNom = false;
                              A1474ContratoServicosCusto_CstUntPrdNrm = context.localUtil.CToN( cgiGet( edtContratoServicosCusto_CstUntPrdNrm_Internalname), ",", ".");
                              A1475ContratoServicosCusto_CstUntPrdExt = context.localUtil.CToN( cgiGet( edtContratoServicosCusto_CstUntPrdExt_Internalname), ",", ".");
                              n1475ContratoServicosCusto_CstUntPrdExt = false;
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E15KZ2 */
                                          E15KZ2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E16KZ2 */
                                          E16KZ2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E17KZ2 */
                                          E17KZ2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "'DOUPDATE'") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E18KZ2 */
                                          E18KZ2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "'DODELETE'") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E19KZ2 */
                                          E19KZ2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          if ( ! wbErr )
                                          {
                                             Rfr0gs = false;
                                             /* Set Refresh If Orderedby Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Ordereddsc Changed */
                                             if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vORDEREDDSC")) != AV14OrderedDsc )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfcontratoservicoscusto_usrpesnom Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFCONTRATOSERVICOSCUSTO_USRPESNOM"), AV20TFContratoServicosCusto_UsrPesNom) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfcontratoservicoscusto_usrpesnom_sel Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFCONTRATOSERVICOSCUSTO_USRPESNOM_SEL"), AV21TFContratoServicosCusto_UsrPesNom_Sel) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfcontratoservicoscusto_cstuntprdnrm Changed */
                                             if ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFCONTRATOSERVICOSCUSTO_CSTUNTPRDNRM"), ",", ".") != AV24TFContratoServicosCusto_CstUntPrdNrm )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfcontratoservicoscusto_cstuntprdnrm_to Changed */
                                             if ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFCONTRATOSERVICOSCUSTO_CSTUNTPRDNRM_TO"), ",", ".") != AV25TFContratoServicosCusto_CstUntPrdNrm_To )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfcontratoservicoscusto_cstuntprdext Changed */
                                             if ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFCONTRATOSERVICOSCUSTO_CSTUNTPRDEXT"), ",", ".") != AV28TFContratoServicosCusto_CstUntPrdExt )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfcontratoservicoscusto_cstuntprdext_to Changed */
                                             if ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFCONTRATOSERVICOSCUSTO_CSTUNTPRDEXT_TO"), ",", ".") != AV29TFContratoServicosCusto_CstUntPrdExt_To )
                                             {
                                                Rfr0gs = true;
                                             }
                                             if ( ! Rfr0gs )
                                             {
                                             }
                                             dynload_actions( ) ;
                                          }
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                                    {
                                       STRUPKZ0( ) ;
                                    }
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEKZ2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormKZ2( ) ;
            }
         }
      }

      protected void PAKZ2( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = edtavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_52( ) ;
         while ( nGXsfl_5_idx <= nRC_GXsfl_5 )
         {
            sendrow_52( ) ;
            nGXsfl_5_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_5_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_5_idx+1));
            sGXsfl_5_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_5_idx), 4, 0)), 4, "0");
            SubsflControlProps_52( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV13OrderedBy ,
                                       bool AV14OrderedDsc ,
                                       String AV20TFContratoServicosCusto_UsrPesNom ,
                                       String AV21TFContratoServicosCusto_UsrPesNom_Sel ,
                                       decimal AV24TFContratoServicosCusto_CstUntPrdNrm ,
                                       decimal AV25TFContratoServicosCusto_CstUntPrdNrm_To ,
                                       decimal AV28TFContratoServicosCusto_CstUntPrdExt ,
                                       decimal AV29TFContratoServicosCusto_CstUntPrdExt_To ,
                                       int AV7ContratoServicosCusto_CntSrvCod ,
                                       String AV22ddo_ContratoServicosCusto_UsrPesNomTitleControlIdToReplace ,
                                       String AV26ddo_ContratoServicosCusto_CstUntPrdNrmTitleControlIdToReplace ,
                                       String AV30ddo_ContratoServicosCusto_CstUntPrdExtTitleControlIdToReplace ,
                                       String AV36Pgmname ,
                                       String sPrefix )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RFKZ2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOSERVICOSCUSTO_CODIGO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1473ContratoServicosCusto_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTRATOSERVICOSCUSTO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1473ContratoServicosCusto_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM", GetSecureSignedToken( sPrefix, context.localUtil.Format( A1474ContratoServicosCusto_CstUntPrdNrm, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM", StringUtil.LTrim( StringUtil.NToC( A1474ContratoServicosCusto_CstUntPrdNrm, 18, 5, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOSERVICOSCUSTO_CSTUNTPRDEXT", GetSecureSignedToken( sPrefix, context.localUtil.Format( A1475ContratoServicosCusto_CstUntPrdExt, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
         GxWebStd.gx_hidden_field( context, sPrefix+"CONTRATOSERVICOSCUSTO_CSTUNTPRDEXT", StringUtil.LTrim( StringUtil.NToC( A1475ContratoServicosCusto_CstUntPrdExt, 18, 5, ".", "")));
      }

      protected void fix_multi_value_controls( )
      {
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFKZ2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV36Pgmname = "ContratoServicosCustoWC";
         context.Gx_err = 0;
      }

      protected void RFKZ2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 5;
         /* Execute user event: E16KZ2 */
         E16KZ2 ();
         nGXsfl_5_idx = 1;
         sGXsfl_5_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_5_idx), 4, 0)), 4, "0");
         SubsflControlProps_52( ) ;
         nGXsfl_5_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", sPrefix);
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_52( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 AV21TFContratoServicosCusto_UsrPesNom_Sel ,
                                                 AV20TFContratoServicosCusto_UsrPesNom ,
                                                 AV24TFContratoServicosCusto_CstUntPrdNrm ,
                                                 AV25TFContratoServicosCusto_CstUntPrdNrm_To ,
                                                 AV28TFContratoServicosCusto_CstUntPrdExt ,
                                                 AV29TFContratoServicosCusto_CstUntPrdExt_To ,
                                                 A1477ContratoServicosCusto_UsrPesNom ,
                                                 A1474ContratoServicosCusto_CstUntPrdNrm ,
                                                 A1475ContratoServicosCusto_CstUntPrdExt ,
                                                 AV13OrderedBy ,
                                                 AV14OrderedDsc ,
                                                 A1471ContratoServicosCusto_CntSrvCod ,
                                                 AV7ContratoServicosCusto_CntSrvCod },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.DECIMAL,
                                                 TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT
                                                 }
            });
            lV20TFContratoServicosCusto_UsrPesNom = StringUtil.PadR( StringUtil.RTrim( AV20TFContratoServicosCusto_UsrPesNom), 100, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20TFContratoServicosCusto_UsrPesNom", AV20TFContratoServicosCusto_UsrPesNom);
            /* Using cursor H00KZ2 */
            pr_default.execute(0, new Object[] {AV7ContratoServicosCusto_CntSrvCod, lV20TFContratoServicosCusto_UsrPesNom, AV21TFContratoServicosCusto_UsrPesNom_Sel, AV24TFContratoServicosCusto_CstUntPrdNrm, AV25TFContratoServicosCusto_CstUntPrdNrm_To, AV28TFContratoServicosCusto_CstUntPrdExt, AV29TFContratoServicosCusto_CstUntPrdExt_To, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_5_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A1472ContratoServicosCusto_UsuarioCod = H00KZ2_A1472ContratoServicosCusto_UsuarioCod[0];
               A1476ContratoServicosCusto_UsrPesCod = H00KZ2_A1476ContratoServicosCusto_UsrPesCod[0];
               n1476ContratoServicosCusto_UsrPesCod = H00KZ2_n1476ContratoServicosCusto_UsrPesCod[0];
               A1471ContratoServicosCusto_CntSrvCod = H00KZ2_A1471ContratoServicosCusto_CntSrvCod[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1471ContratoServicosCusto_CntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1471ContratoServicosCusto_CntSrvCod), 6, 0)));
               A1475ContratoServicosCusto_CstUntPrdExt = H00KZ2_A1475ContratoServicosCusto_CstUntPrdExt[0];
               n1475ContratoServicosCusto_CstUntPrdExt = H00KZ2_n1475ContratoServicosCusto_CstUntPrdExt[0];
               A1474ContratoServicosCusto_CstUntPrdNrm = H00KZ2_A1474ContratoServicosCusto_CstUntPrdNrm[0];
               A1477ContratoServicosCusto_UsrPesNom = H00KZ2_A1477ContratoServicosCusto_UsrPesNom[0];
               n1477ContratoServicosCusto_UsrPesNom = H00KZ2_n1477ContratoServicosCusto_UsrPesNom[0];
               A1473ContratoServicosCusto_Codigo = H00KZ2_A1473ContratoServicosCusto_Codigo[0];
               A1476ContratoServicosCusto_UsrPesCod = H00KZ2_A1476ContratoServicosCusto_UsrPesCod[0];
               n1476ContratoServicosCusto_UsrPesCod = H00KZ2_n1476ContratoServicosCusto_UsrPesCod[0];
               A1477ContratoServicosCusto_UsrPesNom = H00KZ2_A1477ContratoServicosCusto_UsrPesNom[0];
               n1477ContratoServicosCusto_UsrPesNom = H00KZ2_n1477ContratoServicosCusto_UsrPesNom[0];
               /* Execute user event: E17KZ2 */
               E17KZ2 ();
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 5;
            WBKZ0( ) ;
         }
         nGXsfl_5_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV21TFContratoServicosCusto_UsrPesNom_Sel ,
                                              AV20TFContratoServicosCusto_UsrPesNom ,
                                              AV24TFContratoServicosCusto_CstUntPrdNrm ,
                                              AV25TFContratoServicosCusto_CstUntPrdNrm_To ,
                                              AV28TFContratoServicosCusto_CstUntPrdExt ,
                                              AV29TFContratoServicosCusto_CstUntPrdExt_To ,
                                              A1477ContratoServicosCusto_UsrPesNom ,
                                              A1474ContratoServicosCusto_CstUntPrdNrm ,
                                              A1475ContratoServicosCusto_CstUntPrdExt ,
                                              AV13OrderedBy ,
                                              AV14OrderedDsc ,
                                              A1471ContratoServicosCusto_CntSrvCod ,
                                              AV7ContratoServicosCusto_CntSrvCod },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.DECIMAL,
                                              TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT
                                              }
         });
         lV20TFContratoServicosCusto_UsrPesNom = StringUtil.PadR( StringUtil.RTrim( AV20TFContratoServicosCusto_UsrPesNom), 100, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20TFContratoServicosCusto_UsrPesNom", AV20TFContratoServicosCusto_UsrPesNom);
         /* Using cursor H00KZ3 */
         pr_default.execute(1, new Object[] {AV7ContratoServicosCusto_CntSrvCod, lV20TFContratoServicosCusto_UsrPesNom, AV21TFContratoServicosCusto_UsrPesNom_Sel, AV24TFContratoServicosCusto_CstUntPrdNrm, AV25TFContratoServicosCusto_CstUntPrdNrm_To, AV28TFContratoServicosCusto_CstUntPrdExt, AV29TFContratoServicosCusto_CstUntPrdExt_To});
         GRID_nRecordCount = H00KZ3_AGRID_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV20TFContratoServicosCusto_UsrPesNom, AV21TFContratoServicosCusto_UsrPesNom_Sel, AV24TFContratoServicosCusto_CstUntPrdNrm, AV25TFContratoServicosCusto_CstUntPrdNrm_To, AV28TFContratoServicosCusto_CstUntPrdExt, AV29TFContratoServicosCusto_CstUntPrdExt_To, AV7ContratoServicosCusto_CntSrvCod, AV22ddo_ContratoServicosCusto_UsrPesNomTitleControlIdToReplace, AV26ddo_ContratoServicosCusto_CstUntPrdNrmTitleControlIdToReplace, AV30ddo_ContratoServicosCusto_CstUntPrdExtTitleControlIdToReplace, AV36Pgmname, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV20TFContratoServicosCusto_UsrPesNom, AV21TFContratoServicosCusto_UsrPesNom_Sel, AV24TFContratoServicosCusto_CstUntPrdNrm, AV25TFContratoServicosCusto_CstUntPrdNrm_To, AV28TFContratoServicosCusto_CstUntPrdExt, AV29TFContratoServicosCusto_CstUntPrdExt_To, AV7ContratoServicosCusto_CntSrvCod, AV22ddo_ContratoServicosCusto_UsrPesNomTitleControlIdToReplace, AV26ddo_ContratoServicosCusto_CstUntPrdNrmTitleControlIdToReplace, AV30ddo_ContratoServicosCusto_CstUntPrdExtTitleControlIdToReplace, AV36Pgmname, sPrefix) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV20TFContratoServicosCusto_UsrPesNom, AV21TFContratoServicosCusto_UsrPesNom_Sel, AV24TFContratoServicosCusto_CstUntPrdNrm, AV25TFContratoServicosCusto_CstUntPrdNrm_To, AV28TFContratoServicosCusto_CstUntPrdExt, AV29TFContratoServicosCusto_CstUntPrdExt_To, AV7ContratoServicosCusto_CntSrvCod, AV22ddo_ContratoServicosCusto_UsrPesNomTitleControlIdToReplace, AV26ddo_ContratoServicosCusto_CstUntPrdNrmTitleControlIdToReplace, AV30ddo_ContratoServicosCusto_CstUntPrdExtTitleControlIdToReplace, AV36Pgmname, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV20TFContratoServicosCusto_UsrPesNom, AV21TFContratoServicosCusto_UsrPesNom_Sel, AV24TFContratoServicosCusto_CstUntPrdNrm, AV25TFContratoServicosCusto_CstUntPrdNrm_To, AV28TFContratoServicosCusto_CstUntPrdExt, AV29TFContratoServicosCusto_CstUntPrdExt_To, AV7ContratoServicosCusto_CntSrvCod, AV22ddo_ContratoServicosCusto_UsrPesNomTitleControlIdToReplace, AV26ddo_ContratoServicosCusto_CstUntPrdNrmTitleControlIdToReplace, AV30ddo_ContratoServicosCusto_CstUntPrdExtTitleControlIdToReplace, AV36Pgmname, sPrefix) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV20TFContratoServicosCusto_UsrPesNom, AV21TFContratoServicosCusto_UsrPesNom_Sel, AV24TFContratoServicosCusto_CstUntPrdNrm, AV25TFContratoServicosCusto_CstUntPrdNrm_To, AV28TFContratoServicosCusto_CstUntPrdExt, AV29TFContratoServicosCusto_CstUntPrdExt_To, AV7ContratoServicosCusto_CntSrvCod, AV22ddo_ContratoServicosCusto_UsrPesNomTitleControlIdToReplace, AV26ddo_ContratoServicosCusto_CstUntPrdNrmTitleControlIdToReplace, AV30ddo_ContratoServicosCusto_CstUntPrdExtTitleControlIdToReplace, AV36Pgmname, sPrefix) ;
         }
         return (int)(0) ;
      }

      protected void STRUPKZ0( )
      {
         /* Before Start, stand alone formulas. */
         AV36Pgmname = "ContratoServicosCustoWC";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E15KZ2 */
         E15KZ2 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vDDO_TITLESETTINGSICONS"), AV31DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vCONTRATOSERVICOSCUSTO_USRPESNOMTITLEFILTERDATA"), AV19ContratoServicosCusto_UsrPesNomTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vCONTRATOSERVICOSCUSTO_CSTUNTPRDNRMTITLEFILTERDATA"), AV23ContratoServicosCusto_CstUntPrdNrmTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vCONTRATOSERVICOSCUSTO_CSTUNTPRDEXTTITLEFILTERDATA"), AV27ContratoServicosCusto_CstUntPrdExtTitleFilterData);
            /* Read variables values. */
            A1471ContratoServicosCusto_CntSrvCod = (int)(context.localUtil.CToN( cgiGet( edtContratoServicosCusto_CntSrvCod_Internalname), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1471ContratoServicosCusto_CntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1471ContratoServicosCusto_CntSrvCod), 6, 0)));
            if ( ( ( context.localUtil.CToN( cgiGet( edtavOrderedby_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavOrderedby_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vORDEREDBY");
               GX_FocusControl = edtavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV13OrderedBy = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            }
            else
            {
               AV13OrderedBy = (short)(context.localUtil.CToN( cgiGet( edtavOrderedby_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            }
            AV14OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            AV20TFContratoServicosCusto_UsrPesNom = StringUtil.Upper( cgiGet( edtavTfcontratoservicoscusto_usrpesnom_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20TFContratoServicosCusto_UsrPesNom", AV20TFContratoServicosCusto_UsrPesNom);
            AV21TFContratoServicosCusto_UsrPesNom_Sel = StringUtil.Upper( cgiGet( edtavTfcontratoservicoscusto_usrpesnom_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21TFContratoServicosCusto_UsrPesNom_Sel", AV21TFContratoServicosCusto_UsrPesNom_Sel);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicoscusto_cstuntprdnrm_Internalname), ",", ".") < -99999999999.99999m ) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicoscusto_cstuntprdnrm_Internalname), ",", ".") > 999999999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATOSERVICOSCUSTO_CSTUNTPRDNRM");
               GX_FocusControl = edtavTfcontratoservicoscusto_cstuntprdnrm_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV24TFContratoServicosCusto_CstUntPrdNrm = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24TFContratoServicosCusto_CstUntPrdNrm", StringUtil.LTrim( StringUtil.Str( AV24TFContratoServicosCusto_CstUntPrdNrm, 18, 5)));
            }
            else
            {
               AV24TFContratoServicosCusto_CstUntPrdNrm = context.localUtil.CToN( cgiGet( edtavTfcontratoservicoscusto_cstuntprdnrm_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24TFContratoServicosCusto_CstUntPrdNrm", StringUtil.LTrim( StringUtil.Str( AV24TFContratoServicosCusto_CstUntPrdNrm, 18, 5)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicoscusto_cstuntprdnrm_to_Internalname), ",", ".") < -99999999999.99999m ) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicoscusto_cstuntprdnrm_to_Internalname), ",", ".") > 999999999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATOSERVICOSCUSTO_CSTUNTPRDNRM_TO");
               GX_FocusControl = edtavTfcontratoservicoscusto_cstuntprdnrm_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV25TFContratoServicosCusto_CstUntPrdNrm_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25TFContratoServicosCusto_CstUntPrdNrm_To", StringUtil.LTrim( StringUtil.Str( AV25TFContratoServicosCusto_CstUntPrdNrm_To, 18, 5)));
            }
            else
            {
               AV25TFContratoServicosCusto_CstUntPrdNrm_To = context.localUtil.CToN( cgiGet( edtavTfcontratoservicoscusto_cstuntprdnrm_to_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25TFContratoServicosCusto_CstUntPrdNrm_To", StringUtil.LTrim( StringUtil.Str( AV25TFContratoServicosCusto_CstUntPrdNrm_To, 18, 5)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicoscusto_cstuntprdext_Internalname), ",", ".") < -99999999999.99999m ) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicoscusto_cstuntprdext_Internalname), ",", ".") > 999999999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATOSERVICOSCUSTO_CSTUNTPRDEXT");
               GX_FocusControl = edtavTfcontratoservicoscusto_cstuntprdext_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV28TFContratoServicosCusto_CstUntPrdExt = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV28TFContratoServicosCusto_CstUntPrdExt", StringUtil.LTrim( StringUtil.Str( AV28TFContratoServicosCusto_CstUntPrdExt, 18, 5)));
            }
            else
            {
               AV28TFContratoServicosCusto_CstUntPrdExt = context.localUtil.CToN( cgiGet( edtavTfcontratoservicoscusto_cstuntprdext_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV28TFContratoServicosCusto_CstUntPrdExt", StringUtil.LTrim( StringUtil.Str( AV28TFContratoServicosCusto_CstUntPrdExt, 18, 5)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicoscusto_cstuntprdext_to_Internalname), ",", ".") < -99999999999.99999m ) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfcontratoservicoscusto_cstuntprdext_to_Internalname), ",", ".") > 999999999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFCONTRATOSERVICOSCUSTO_CSTUNTPRDEXT_TO");
               GX_FocusControl = edtavTfcontratoservicoscusto_cstuntprdext_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV29TFContratoServicosCusto_CstUntPrdExt_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29TFContratoServicosCusto_CstUntPrdExt_To", StringUtil.LTrim( StringUtil.Str( AV29TFContratoServicosCusto_CstUntPrdExt_To, 18, 5)));
            }
            else
            {
               AV29TFContratoServicosCusto_CstUntPrdExt_To = context.localUtil.CToN( cgiGet( edtavTfcontratoservicoscusto_cstuntprdext_to_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29TFContratoServicosCusto_CstUntPrdExt_To", StringUtil.LTrim( StringUtil.Str( AV29TFContratoServicosCusto_CstUntPrdExt_To, 18, 5)));
            }
            AV22ddo_ContratoServicosCusto_UsrPesNomTitleControlIdToReplace = cgiGet( edtavDdo_contratoservicoscusto_usrpesnomtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22ddo_ContratoServicosCusto_UsrPesNomTitleControlIdToReplace", AV22ddo_ContratoServicosCusto_UsrPesNomTitleControlIdToReplace);
            AV26ddo_ContratoServicosCusto_CstUntPrdNrmTitleControlIdToReplace = cgiGet( edtavDdo_contratoservicoscusto_cstuntprdnrmtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26ddo_ContratoServicosCusto_CstUntPrdNrmTitleControlIdToReplace", AV26ddo_ContratoServicosCusto_CstUntPrdNrmTitleControlIdToReplace);
            AV30ddo_ContratoServicosCusto_CstUntPrdExtTitleControlIdToReplace = cgiGet( edtavDdo_contratoservicoscusto_cstuntprdexttitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30ddo_ContratoServicosCusto_CstUntPrdExtTitleControlIdToReplace", AV30ddo_ContratoServicosCusto_CstUntPrdExtTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_5 = (short)(context.localUtil.CToN( cgiGet( sPrefix+"nRC_GXsfl_5"), ",", "."));
            wcpOAV7ContratoServicosCusto_CntSrvCod = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV7ContratoServicosCusto_CntSrvCod"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Ddo_contratoservicoscusto_usrpesnom_Caption = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSCUSTO_USRPESNOM_Caption");
            Ddo_contratoservicoscusto_usrpesnom_Tooltip = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSCUSTO_USRPESNOM_Tooltip");
            Ddo_contratoservicoscusto_usrpesnom_Cls = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSCUSTO_USRPESNOM_Cls");
            Ddo_contratoservicoscusto_usrpesnom_Filteredtext_set = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSCUSTO_USRPESNOM_Filteredtext_set");
            Ddo_contratoservicoscusto_usrpesnom_Selectedvalue_set = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSCUSTO_USRPESNOM_Selectedvalue_set");
            Ddo_contratoservicoscusto_usrpesnom_Dropdownoptionstype = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSCUSTO_USRPESNOM_Dropdownoptionstype");
            Ddo_contratoservicoscusto_usrpesnom_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSCUSTO_USRPESNOM_Titlecontrolidtoreplace");
            Ddo_contratoservicoscusto_usrpesnom_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOSERVICOSCUSTO_USRPESNOM_Includesortasc"));
            Ddo_contratoservicoscusto_usrpesnom_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOSERVICOSCUSTO_USRPESNOM_Includesortdsc"));
            Ddo_contratoservicoscusto_usrpesnom_Sortedstatus = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSCUSTO_USRPESNOM_Sortedstatus");
            Ddo_contratoservicoscusto_usrpesnom_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOSERVICOSCUSTO_USRPESNOM_Includefilter"));
            Ddo_contratoservicoscusto_usrpesnom_Filtertype = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSCUSTO_USRPESNOM_Filtertype");
            Ddo_contratoservicoscusto_usrpesnom_Filterisrange = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOSERVICOSCUSTO_USRPESNOM_Filterisrange"));
            Ddo_contratoservicoscusto_usrpesnom_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOSERVICOSCUSTO_USRPESNOM_Includedatalist"));
            Ddo_contratoservicoscusto_usrpesnom_Datalisttype = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSCUSTO_USRPESNOM_Datalisttype");
            Ddo_contratoservicoscusto_usrpesnom_Datalistproc = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSCUSTO_USRPESNOM_Datalistproc");
            Ddo_contratoservicoscusto_usrpesnom_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( sPrefix+"DDO_CONTRATOSERVICOSCUSTO_USRPESNOM_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_contratoservicoscusto_usrpesnom_Sortasc = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSCUSTO_USRPESNOM_Sortasc");
            Ddo_contratoservicoscusto_usrpesnom_Sortdsc = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSCUSTO_USRPESNOM_Sortdsc");
            Ddo_contratoservicoscusto_usrpesnom_Loadingdata = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSCUSTO_USRPESNOM_Loadingdata");
            Ddo_contratoservicoscusto_usrpesnom_Cleanfilter = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSCUSTO_USRPESNOM_Cleanfilter");
            Ddo_contratoservicoscusto_usrpesnom_Noresultsfound = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSCUSTO_USRPESNOM_Noresultsfound");
            Ddo_contratoservicoscusto_usrpesnom_Searchbuttontext = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSCUSTO_USRPESNOM_Searchbuttontext");
            Ddo_contratoservicoscusto_cstuntprdnrm_Caption = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM_Caption");
            Ddo_contratoservicoscusto_cstuntprdnrm_Tooltip = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM_Tooltip");
            Ddo_contratoservicoscusto_cstuntprdnrm_Cls = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM_Cls");
            Ddo_contratoservicoscusto_cstuntprdnrm_Filteredtext_set = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM_Filteredtext_set");
            Ddo_contratoservicoscusto_cstuntprdnrm_Filteredtextto_set = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM_Filteredtextto_set");
            Ddo_contratoservicoscusto_cstuntprdnrm_Dropdownoptionstype = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM_Dropdownoptionstype");
            Ddo_contratoservicoscusto_cstuntprdnrm_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM_Titlecontrolidtoreplace");
            Ddo_contratoservicoscusto_cstuntprdnrm_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM_Includesortasc"));
            Ddo_contratoservicoscusto_cstuntprdnrm_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM_Includesortdsc"));
            Ddo_contratoservicoscusto_cstuntprdnrm_Sortedstatus = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM_Sortedstatus");
            Ddo_contratoservicoscusto_cstuntprdnrm_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM_Includefilter"));
            Ddo_contratoservicoscusto_cstuntprdnrm_Filtertype = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM_Filtertype");
            Ddo_contratoservicoscusto_cstuntprdnrm_Filterisrange = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM_Filterisrange"));
            Ddo_contratoservicoscusto_cstuntprdnrm_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM_Includedatalist"));
            Ddo_contratoservicoscusto_cstuntprdnrm_Sortasc = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM_Sortasc");
            Ddo_contratoservicoscusto_cstuntprdnrm_Sortdsc = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM_Sortdsc");
            Ddo_contratoservicoscusto_cstuntprdnrm_Cleanfilter = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM_Cleanfilter");
            Ddo_contratoservicoscusto_cstuntprdnrm_Rangefilterfrom = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM_Rangefilterfrom");
            Ddo_contratoservicoscusto_cstuntprdnrm_Rangefilterto = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM_Rangefilterto");
            Ddo_contratoservicoscusto_cstuntprdnrm_Searchbuttontext = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM_Searchbuttontext");
            Ddo_contratoservicoscusto_cstuntprdext_Caption = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDEXT_Caption");
            Ddo_contratoservicoscusto_cstuntprdext_Tooltip = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDEXT_Tooltip");
            Ddo_contratoservicoscusto_cstuntprdext_Cls = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDEXT_Cls");
            Ddo_contratoservicoscusto_cstuntprdext_Filteredtext_set = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDEXT_Filteredtext_set");
            Ddo_contratoservicoscusto_cstuntprdext_Filteredtextto_set = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDEXT_Filteredtextto_set");
            Ddo_contratoservicoscusto_cstuntprdext_Dropdownoptionstype = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDEXT_Dropdownoptionstype");
            Ddo_contratoservicoscusto_cstuntprdext_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDEXT_Titlecontrolidtoreplace");
            Ddo_contratoservicoscusto_cstuntprdext_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDEXT_Includesortasc"));
            Ddo_contratoservicoscusto_cstuntprdext_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDEXT_Includesortdsc"));
            Ddo_contratoservicoscusto_cstuntprdext_Sortedstatus = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDEXT_Sortedstatus");
            Ddo_contratoservicoscusto_cstuntprdext_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDEXT_Includefilter"));
            Ddo_contratoservicoscusto_cstuntprdext_Filtertype = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDEXT_Filtertype");
            Ddo_contratoservicoscusto_cstuntprdext_Filterisrange = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDEXT_Filterisrange"));
            Ddo_contratoservicoscusto_cstuntprdext_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDEXT_Includedatalist"));
            Ddo_contratoservicoscusto_cstuntprdext_Sortasc = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDEXT_Sortasc");
            Ddo_contratoservicoscusto_cstuntprdext_Sortdsc = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDEXT_Sortdsc");
            Ddo_contratoservicoscusto_cstuntprdext_Cleanfilter = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDEXT_Cleanfilter");
            Ddo_contratoservicoscusto_cstuntprdext_Rangefilterfrom = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDEXT_Rangefilterfrom");
            Ddo_contratoservicoscusto_cstuntprdext_Rangefilterto = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDEXT_Rangefilterto");
            Ddo_contratoservicoscusto_cstuntprdext_Searchbuttontext = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDEXT_Searchbuttontext");
            Ddo_contratoservicoscusto_usrpesnom_Activeeventkey = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSCUSTO_USRPESNOM_Activeeventkey");
            Ddo_contratoservicoscusto_usrpesnom_Filteredtext_get = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSCUSTO_USRPESNOM_Filteredtext_get");
            Ddo_contratoservicoscusto_usrpesnom_Selectedvalue_get = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSCUSTO_USRPESNOM_Selectedvalue_get");
            Ddo_contratoservicoscusto_cstuntprdnrm_Activeeventkey = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM_Activeeventkey");
            Ddo_contratoservicoscusto_cstuntprdnrm_Filteredtext_get = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM_Filteredtext_get");
            Ddo_contratoservicoscusto_cstuntprdnrm_Filteredtextto_get = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM_Filteredtextto_get");
            Ddo_contratoservicoscusto_cstuntprdext_Activeeventkey = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDEXT_Activeeventkey");
            Ddo_contratoservicoscusto_cstuntprdext_Filteredtext_get = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDEXT_Filteredtext_get");
            Ddo_contratoservicoscusto_cstuntprdext_Filteredtextto_get = cgiGet( sPrefix+"DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDEXT_Filteredtextto_get");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vORDEREDDSC")) != AV14OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFCONTRATOSERVICOSCUSTO_USRPESNOM"), AV20TFContratoServicosCusto_UsrPesNom) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFCONTRATOSERVICOSCUSTO_USRPESNOM_SEL"), AV21TFContratoServicosCusto_UsrPesNom_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFCONTRATOSERVICOSCUSTO_CSTUNTPRDNRM"), ",", ".") != AV24TFContratoServicosCusto_CstUntPrdNrm )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFCONTRATOSERVICOSCUSTO_CSTUNTPRDNRM_TO"), ",", ".") != AV25TFContratoServicosCusto_CstUntPrdNrm_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFCONTRATOSERVICOSCUSTO_CSTUNTPRDEXT"), ",", ".") != AV28TFContratoServicosCusto_CstUntPrdExt )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFCONTRATOSERVICOSCUSTO_CSTUNTPRDEXT_TO"), ",", ".") != AV29TFContratoServicosCusto_CstUntPrdExt_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E15KZ2 */
         E15KZ2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E15KZ2( )
      {
         /* Start Routine */
         subGrid_Rows = 0;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         edtavTfcontratoservicoscusto_usrpesnom_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfcontratoservicoscusto_usrpesnom_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoservicoscusto_usrpesnom_Visible), 5, 0)));
         edtavTfcontratoservicoscusto_usrpesnom_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfcontratoservicoscusto_usrpesnom_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoservicoscusto_usrpesnom_sel_Visible), 5, 0)));
         edtavTfcontratoservicoscusto_cstuntprdnrm_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfcontratoservicoscusto_cstuntprdnrm_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoservicoscusto_cstuntprdnrm_Visible), 5, 0)));
         edtavTfcontratoservicoscusto_cstuntprdnrm_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfcontratoservicoscusto_cstuntprdnrm_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoservicoscusto_cstuntprdnrm_to_Visible), 5, 0)));
         edtavTfcontratoservicoscusto_cstuntprdext_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfcontratoservicoscusto_cstuntprdext_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoservicoscusto_cstuntprdext_Visible), 5, 0)));
         edtavTfcontratoservicoscusto_cstuntprdext_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfcontratoservicoscusto_cstuntprdext_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfcontratoservicoscusto_cstuntprdext_to_Visible), 5, 0)));
         Ddo_contratoservicoscusto_usrpesnom_Titlecontrolidtoreplace = subGrid_Internalname+"_ContratoServicosCusto_UsrPesNom";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicoscusto_usrpesnom_Internalname, "TitleControlIdToReplace", Ddo_contratoservicoscusto_usrpesnom_Titlecontrolidtoreplace);
         AV22ddo_ContratoServicosCusto_UsrPesNomTitleControlIdToReplace = Ddo_contratoservicoscusto_usrpesnom_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22ddo_ContratoServicosCusto_UsrPesNomTitleControlIdToReplace", AV22ddo_ContratoServicosCusto_UsrPesNomTitleControlIdToReplace);
         edtavDdo_contratoservicoscusto_usrpesnomtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_contratoservicoscusto_usrpesnomtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratoservicoscusto_usrpesnomtitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratoservicoscusto_cstuntprdnrm_Titlecontrolidtoreplace = subGrid_Internalname+"_ContratoServicosCusto_CstUntPrdNrm";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicoscusto_cstuntprdnrm_Internalname, "TitleControlIdToReplace", Ddo_contratoservicoscusto_cstuntprdnrm_Titlecontrolidtoreplace);
         AV26ddo_ContratoServicosCusto_CstUntPrdNrmTitleControlIdToReplace = Ddo_contratoservicoscusto_cstuntprdnrm_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26ddo_ContratoServicosCusto_CstUntPrdNrmTitleControlIdToReplace", AV26ddo_ContratoServicosCusto_CstUntPrdNrmTitleControlIdToReplace);
         edtavDdo_contratoservicoscusto_cstuntprdnrmtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_contratoservicoscusto_cstuntprdnrmtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratoservicoscusto_cstuntprdnrmtitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_contratoservicoscusto_cstuntprdext_Titlecontrolidtoreplace = subGrid_Internalname+"_ContratoServicosCusto_CstUntPrdExt";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicoscusto_cstuntprdext_Internalname, "TitleControlIdToReplace", Ddo_contratoservicoscusto_cstuntprdext_Titlecontrolidtoreplace);
         AV30ddo_ContratoServicosCusto_CstUntPrdExtTitleControlIdToReplace = Ddo_contratoservicoscusto_cstuntprdext_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30ddo_ContratoServicosCusto_CstUntPrdExtTitleControlIdToReplace", AV30ddo_ContratoServicosCusto_CstUntPrdExtTitleControlIdToReplace);
         edtavDdo_contratoservicoscusto_cstuntprdexttitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_contratoservicoscusto_cstuntprdexttitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_contratoservicoscusto_cstuntprdexttitlecontrolidtoreplace_Visible), 5, 0)));
         edtContratoServicosCusto_CntSrvCod_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContratoServicosCusto_CntSrvCod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoServicosCusto_CntSrvCod_Visible), 5, 0)));
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtavOrderedby_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavOrderedby_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrderedby_Visible), 5, 0)));
         if ( AV13OrderedBy < 1 )
         {
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
            S132 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = AV31DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1) ;
         AV31DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1;
      }

      protected void E16KZ2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV19ContratoServicosCusto_UsrPesNomTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV23ContratoServicosCusto_CstUntPrdNrmTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV27ContratoServicosCusto_CstUntPrdExtTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtContratoServicosCusto_UsrPesNom_Titleformat = 2;
         edtContratoServicosCusto_UsrPesNom_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Colaborador", AV22ddo_ContratoServicosCusto_UsrPesNomTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContratoServicosCusto_UsrPesNom_Internalname, "Title", edtContratoServicosCusto_UsrPesNom_Title);
         edtContratoServicosCusto_CstUntPrdNrm_Titleformat = 2;
         edtContratoServicosCusto_CstUntPrdNrm_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Normal R$", AV26ddo_ContratoServicosCusto_CstUntPrdNrmTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContratoServicosCusto_CstUntPrdNrm_Internalname, "Title", edtContratoServicosCusto_CstUntPrdNrm_Title);
         edtContratoServicosCusto_CstUntPrdExt_Titleformat = 2;
         edtContratoServicosCusto_CstUntPrdExt_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Extra R$", AV30ddo_ContratoServicosCusto_CstUntPrdExtTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContratoServicosCusto_CstUntPrdExt_Internalname, "Title", edtContratoServicosCusto_CstUntPrdExt_Title);
         edtavUpdate_Visible = (AV6WWPContext.gxTpr_Update&&AV6WWPContext.gxTpr_Userehgestor ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavUpdate_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUpdate_Visible), 5, 0)));
         edtavDelete_Visible = (AV6WWPContext.gxTpr_Delete&&AV6WWPContext.gxTpr_Userehgestor ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDelete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDelete_Visible), 5, 0)));
         imgInsert_Visible = (AV6WWPContext.gxTpr_Insert&&AV6WWPContext.gxTpr_Userehgestor ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgInsert_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgInsert_Visible), 5, 0)));
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV19ContratoServicosCusto_UsrPesNomTitleFilterData", AV19ContratoServicosCusto_UsrPesNomTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV23ContratoServicosCusto_CstUntPrdNrmTitleFilterData", AV23ContratoServicosCusto_CstUntPrdNrmTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV27ContratoServicosCusto_CstUntPrdExtTitleFilterData", AV27ContratoServicosCusto_CstUntPrdExtTitleFilterData);
      }

      protected void E11KZ2( )
      {
         /* Ddo_contratoservicoscusto_usrpesnom_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratoservicoscusto_usrpesnom_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratoservicoscusto_usrpesnom_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicoscusto_usrpesnom_Internalname, "SortedStatus", Ddo_contratoservicoscusto_usrpesnom_Sortedstatus);
            subgrid_firstpage( ) ;
         }
         else if ( StringUtil.StrCmp(Ddo_contratoservicoscusto_usrpesnom_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratoservicoscusto_usrpesnom_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicoscusto_usrpesnom_Internalname, "SortedStatus", Ddo_contratoservicoscusto_usrpesnom_Sortedstatus);
            subgrid_firstpage( ) ;
         }
         else if ( StringUtil.StrCmp(Ddo_contratoservicoscusto_usrpesnom_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV20TFContratoServicosCusto_UsrPesNom = Ddo_contratoservicoscusto_usrpesnom_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20TFContratoServicosCusto_UsrPesNom", AV20TFContratoServicosCusto_UsrPesNom);
            AV21TFContratoServicosCusto_UsrPesNom_Sel = Ddo_contratoservicoscusto_usrpesnom_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21TFContratoServicosCusto_UsrPesNom_Sel", AV21TFContratoServicosCusto_UsrPesNom_Sel);
            subgrid_firstpage( ) ;
         }
      }

      protected void E12KZ2( )
      {
         /* Ddo_contratoservicoscusto_cstuntprdnrm_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratoservicoscusto_cstuntprdnrm_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratoservicoscusto_cstuntprdnrm_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicoscusto_cstuntprdnrm_Internalname, "SortedStatus", Ddo_contratoservicoscusto_cstuntprdnrm_Sortedstatus);
            subgrid_firstpage( ) ;
         }
         else if ( StringUtil.StrCmp(Ddo_contratoservicoscusto_cstuntprdnrm_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratoservicoscusto_cstuntprdnrm_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicoscusto_cstuntprdnrm_Internalname, "SortedStatus", Ddo_contratoservicoscusto_cstuntprdnrm_Sortedstatus);
            subgrid_firstpage( ) ;
         }
         else if ( StringUtil.StrCmp(Ddo_contratoservicoscusto_cstuntprdnrm_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV24TFContratoServicosCusto_CstUntPrdNrm = NumberUtil.Val( Ddo_contratoservicoscusto_cstuntprdnrm_Filteredtext_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24TFContratoServicosCusto_CstUntPrdNrm", StringUtil.LTrim( StringUtil.Str( AV24TFContratoServicosCusto_CstUntPrdNrm, 18, 5)));
            AV25TFContratoServicosCusto_CstUntPrdNrm_To = NumberUtil.Val( Ddo_contratoservicoscusto_cstuntprdnrm_Filteredtextto_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25TFContratoServicosCusto_CstUntPrdNrm_To", StringUtil.LTrim( StringUtil.Str( AV25TFContratoServicosCusto_CstUntPrdNrm_To, 18, 5)));
            subgrid_firstpage( ) ;
         }
      }

      protected void E13KZ2( )
      {
         /* Ddo_contratoservicoscusto_cstuntprdext_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_contratoservicoscusto_cstuntprdext_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratoservicoscusto_cstuntprdext_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicoscusto_cstuntprdext_Internalname, "SortedStatus", Ddo_contratoservicoscusto_cstuntprdext_Sortedstatus);
            subgrid_firstpage( ) ;
         }
         else if ( StringUtil.StrCmp(Ddo_contratoservicoscusto_cstuntprdext_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_contratoservicoscusto_cstuntprdext_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicoscusto_cstuntprdext_Internalname, "SortedStatus", Ddo_contratoservicoscusto_cstuntprdext_Sortedstatus);
            subgrid_firstpage( ) ;
         }
         else if ( StringUtil.StrCmp(Ddo_contratoservicoscusto_cstuntprdext_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV28TFContratoServicosCusto_CstUntPrdExt = NumberUtil.Val( Ddo_contratoservicoscusto_cstuntprdext_Filteredtext_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV28TFContratoServicosCusto_CstUntPrdExt", StringUtil.LTrim( StringUtil.Str( AV28TFContratoServicosCusto_CstUntPrdExt, 18, 5)));
            AV29TFContratoServicosCusto_CstUntPrdExt_To = NumberUtil.Val( Ddo_contratoservicoscusto_cstuntprdext_Filteredtextto_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29TFContratoServicosCusto_CstUntPrdExt_To", StringUtil.LTrim( StringUtil.Str( AV29TFContratoServicosCusto_CstUntPrdExt_To, 18, 5)));
            subgrid_firstpage( ) ;
         }
      }

      private void E17KZ2( )
      {
         /* Grid_Load Routine */
         AV15Update = context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavUpdate_Internalname, AV15Update);
         AV34Update_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( )));
         edtavUpdate_Tooltiptext = "Modifica";
         AV17Delete = context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavDelete_Internalname, AV17Delete);
         AV35Delete_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )));
         edtavDelete_Tooltiptext = "Eliminar";
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 5;
         }
         sendrow_52( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_5_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(5, GridRow);
         }
      }

      protected void E14KZ2( )
      {
         /* 'DoInsert' Routine */
         context.PopUp(formatLink("contratoservicoscusto.aspx") + "?" + UrlEncode(StringUtil.RTrim("INS")) + "," + UrlEncode("" +0) + "," + UrlEncode("" +AV7ContratoServicosCusto_CntSrvCod), new Object[] {"AV7ContratoServicosCusto_CntSrvCod"});
         context.DoAjaxRefreshCmp(sPrefix);
      }

      protected void E18KZ2( )
      {
         /* 'DoUpdate' Routine */
         context.PopUp(formatLink("contratoservicoscusto.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A1473ContratoServicosCusto_Codigo) + "," + UrlEncode("" +AV7ContratoServicosCusto_CntSrvCod), new Object[] {"AV7ContratoServicosCusto_CntSrvCod"});
         context.DoAjaxRefreshCmp(sPrefix);
      }

      protected void E19KZ2( )
      {
         /* 'DoDelete' Routine */
         context.PopUp(formatLink("contratoservicoscusto.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A1473ContratoServicosCusto_Codigo) + "," + UrlEncode("" +AV7ContratoServicosCusto_CntSrvCod), new Object[] {"AV7ContratoServicosCusto_CntSrvCod"});
         context.DoAjaxRefreshCmp(sPrefix);
      }

      protected void S152( )
      {
         /* 'RESETDDOSORTEDSTATUS' Routine */
         Ddo_contratoservicoscusto_usrpesnom_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicoscusto_usrpesnom_Internalname, "SortedStatus", Ddo_contratoservicoscusto_usrpesnom_Sortedstatus);
         Ddo_contratoservicoscusto_cstuntprdnrm_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicoscusto_cstuntprdnrm_Internalname, "SortedStatus", Ddo_contratoservicoscusto_cstuntprdnrm_Sortedstatus);
         Ddo_contratoservicoscusto_cstuntprdext_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicoscusto_cstuntprdext_Internalname, "SortedStatus", Ddo_contratoservicoscusto_cstuntprdext_Sortedstatus);
      }

      protected void S132( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV13OrderedBy == 1 )
         {
            Ddo_contratoservicoscusto_usrpesnom_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicoscusto_usrpesnom_Internalname, "SortedStatus", Ddo_contratoservicoscusto_usrpesnom_Sortedstatus);
         }
         else if ( AV13OrderedBy == 2 )
         {
            Ddo_contratoservicoscusto_cstuntprdnrm_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicoscusto_cstuntprdnrm_Internalname, "SortedStatus", Ddo_contratoservicoscusto_cstuntprdnrm_Sortedstatus);
         }
         else if ( AV13OrderedBy == 3 )
         {
            Ddo_contratoservicoscusto_cstuntprdext_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicoscusto_cstuntprdext_Internalname, "SortedStatus", Ddo_contratoservicoscusto_cstuntprdext_Sortedstatus);
         }
      }

      protected void S122( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV18Session.Get(AV36Pgmname+"GridState"), "") == 0 )
         {
            AV11GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  AV36Pgmname+"GridState"), "");
         }
         else
         {
            AV11GridState.FromXml(AV18Session.Get(AV36Pgmname+"GridState"), "");
         }
         AV13OrderedBy = AV11GridState.gxTpr_Orderedby;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         AV14OrderedDsc = AV11GridState.gxTpr_Ordereddsc;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
         /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV37GXV1 = 1;
         while ( AV37GXV1 <= AV11GridState.gxTpr_Filtervalues.Count )
         {
            AV12GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV11GridState.gxTpr_Filtervalues.Item(AV37GXV1));
            if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFCONTRATOSERVICOSCUSTO_USRPESNOM") == 0 )
            {
               AV20TFContratoServicosCusto_UsrPesNom = AV12GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20TFContratoServicosCusto_UsrPesNom", AV20TFContratoServicosCusto_UsrPesNom);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV20TFContratoServicosCusto_UsrPesNom)) )
               {
                  Ddo_contratoservicoscusto_usrpesnom_Filteredtext_set = AV20TFContratoServicosCusto_UsrPesNom;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicoscusto_usrpesnom_Internalname, "FilteredText_set", Ddo_contratoservicoscusto_usrpesnom_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFCONTRATOSERVICOSCUSTO_USRPESNOM_SEL") == 0 )
            {
               AV21TFContratoServicosCusto_UsrPesNom_Sel = AV12GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21TFContratoServicosCusto_UsrPesNom_Sel", AV21TFContratoServicosCusto_UsrPesNom_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV21TFContratoServicosCusto_UsrPesNom_Sel)) )
               {
                  Ddo_contratoservicoscusto_usrpesnom_Selectedvalue_set = AV21TFContratoServicosCusto_UsrPesNom_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicoscusto_usrpesnom_Internalname, "SelectedValue_set", Ddo_contratoservicoscusto_usrpesnom_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFCONTRATOSERVICOSCUSTO_CSTUNTPRDNRM") == 0 )
            {
               AV24TFContratoServicosCusto_CstUntPrdNrm = NumberUtil.Val( AV12GridStateFilterValue.gxTpr_Value, ".");
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24TFContratoServicosCusto_CstUntPrdNrm", StringUtil.LTrim( StringUtil.Str( AV24TFContratoServicosCusto_CstUntPrdNrm, 18, 5)));
               AV25TFContratoServicosCusto_CstUntPrdNrm_To = NumberUtil.Val( AV12GridStateFilterValue.gxTpr_Valueto, ".");
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25TFContratoServicosCusto_CstUntPrdNrm_To", StringUtil.LTrim( StringUtil.Str( AV25TFContratoServicosCusto_CstUntPrdNrm_To, 18, 5)));
               if ( ! (Convert.ToDecimal(0)==AV24TFContratoServicosCusto_CstUntPrdNrm) )
               {
                  Ddo_contratoservicoscusto_cstuntprdnrm_Filteredtext_set = StringUtil.Str( AV24TFContratoServicosCusto_CstUntPrdNrm, 18, 5);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicoscusto_cstuntprdnrm_Internalname, "FilteredText_set", Ddo_contratoservicoscusto_cstuntprdnrm_Filteredtext_set);
               }
               if ( ! (Convert.ToDecimal(0)==AV25TFContratoServicosCusto_CstUntPrdNrm_To) )
               {
                  Ddo_contratoservicoscusto_cstuntprdnrm_Filteredtextto_set = StringUtil.Str( AV25TFContratoServicosCusto_CstUntPrdNrm_To, 18, 5);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicoscusto_cstuntprdnrm_Internalname, "FilteredTextTo_set", Ddo_contratoservicoscusto_cstuntprdnrm_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFCONTRATOSERVICOSCUSTO_CSTUNTPRDEXT") == 0 )
            {
               AV28TFContratoServicosCusto_CstUntPrdExt = NumberUtil.Val( AV12GridStateFilterValue.gxTpr_Value, ".");
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV28TFContratoServicosCusto_CstUntPrdExt", StringUtil.LTrim( StringUtil.Str( AV28TFContratoServicosCusto_CstUntPrdExt, 18, 5)));
               AV29TFContratoServicosCusto_CstUntPrdExt_To = NumberUtil.Val( AV12GridStateFilterValue.gxTpr_Valueto, ".");
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29TFContratoServicosCusto_CstUntPrdExt_To", StringUtil.LTrim( StringUtil.Str( AV29TFContratoServicosCusto_CstUntPrdExt_To, 18, 5)));
               if ( ! (Convert.ToDecimal(0)==AV28TFContratoServicosCusto_CstUntPrdExt) )
               {
                  Ddo_contratoservicoscusto_cstuntprdext_Filteredtext_set = StringUtil.Str( AV28TFContratoServicosCusto_CstUntPrdExt, 18, 5);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicoscusto_cstuntprdext_Internalname, "FilteredText_set", Ddo_contratoservicoscusto_cstuntprdext_Filteredtext_set);
               }
               if ( ! (Convert.ToDecimal(0)==AV29TFContratoServicosCusto_CstUntPrdExt_To) )
               {
                  Ddo_contratoservicoscusto_cstuntprdext_Filteredtextto_set = StringUtil.Str( AV29TFContratoServicosCusto_CstUntPrdExt_To, 18, 5);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_contratoservicoscusto_cstuntprdext_Internalname, "FilteredTextTo_set", Ddo_contratoservicoscusto_cstuntprdext_Filteredtextto_set);
               }
            }
            AV37GXV1 = (int)(AV37GXV1+1);
         }
      }

      protected void S142( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV11GridState.FromXml(AV18Session.Get(AV36Pgmname+"GridState"), "");
         AV11GridState.gxTpr_Orderedby = AV13OrderedBy;
         AV11GridState.gxTpr_Ordereddsc = AV14OrderedDsc;
         AV11GridState.gxTpr_Filtervalues.Clear();
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV20TFContratoServicosCusto_UsrPesNom)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFCONTRATOSERVICOSCUSTO_USRPESNOM";
            AV12GridStateFilterValue.gxTpr_Value = AV20TFContratoServicosCusto_UsrPesNom;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV21TFContratoServicosCusto_UsrPesNom_Sel)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFCONTRATOSERVICOSCUSTO_USRPESNOM_SEL";
            AV12GridStateFilterValue.gxTpr_Value = AV21TFContratoServicosCusto_UsrPesNom_Sel;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! ( (Convert.ToDecimal(0)==AV24TFContratoServicosCusto_CstUntPrdNrm) && (Convert.ToDecimal(0)==AV25TFContratoServicosCusto_CstUntPrdNrm_To) ) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFCONTRATOSERVICOSCUSTO_CSTUNTPRDNRM";
            AV12GridStateFilterValue.gxTpr_Value = StringUtil.Str( AV24TFContratoServicosCusto_CstUntPrdNrm, 18, 5);
            AV12GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( AV25TFContratoServicosCusto_CstUntPrdNrm_To, 18, 5);
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! ( (Convert.ToDecimal(0)==AV28TFContratoServicosCusto_CstUntPrdExt) && (Convert.ToDecimal(0)==AV29TFContratoServicosCusto_CstUntPrdExt_To) ) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFCONTRATOSERVICOSCUSTO_CSTUNTPRDEXT";
            AV12GridStateFilterValue.gxTpr_Value = StringUtil.Str( AV28TFContratoServicosCusto_CstUntPrdExt, 18, 5);
            AV12GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( AV29TFContratoServicosCusto_CstUntPrdExt_To, 18, 5);
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! (0==AV7ContratoServicosCusto_CntSrvCod) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "PARM_&CONTRATOSERVICOSCUSTO_CNTSRVCOD";
            AV12GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV7ContratoServicosCusto_CntSrvCod), 6, 0);
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         new wwpbaseobjects.savegridstate(context ).execute(  AV36Pgmname+"GridState",  AV11GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_MeetrikaVs3")) ;
      }

      protected void S112( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV9TrnContext.gxTpr_Callerobject = AV36Pgmname;
         AV9TrnContext.gxTpr_Callerondelete = true;
         AV9TrnContext.gxTpr_Callerurl = AV8HTTPRequest.ScriptName+"?"+AV8HTTPRequest.QueryString;
         AV9TrnContext.gxTpr_Transactionname = "ContratoServicos";
         AV10TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV10TrnContextAtt.gxTpr_Attributename = "ContratoServicosCusto_CntSrvCod";
         AV10TrnContextAtt.gxTpr_Attributevalue = StringUtil.Str( (decimal)(AV7ContratoServicosCusto_CntSrvCod), 6, 0);
         AV9TrnContext.gxTpr_Attributes.Add(AV10TrnContextAtt, 0);
         AV18Session.Set("TrnContext", AV9TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_MeetrikaVs3"));
      }

      protected void wb_table1_2_KZ2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable1_Internalname, tblUnnamedtable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"DivS\" data-gxgridid=\"5\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+((edtavUpdate_Visible==0) ? "display:none;" : "")+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+((edtavDelete_Visible==0) ? "display:none;" : "")+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Contrato Servicos Custo_Codigo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratoServicosCusto_UsrPesNom_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratoServicosCusto_UsrPesNom_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratoServicosCusto_UsrPesNom_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(118), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratoServicosCusto_CstUntPrdNrm_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratoServicosCusto_CstUntPrdNrm_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratoServicosCusto_CstUntPrdNrm_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(118), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtContratoServicosCusto_CstUntPrdExt_Titleformat == 0 )
               {
                  context.SendWebValue( edtContratoServicosCusto_CstUntPrdExt_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtContratoServicosCusto_CstUntPrdExt_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", sPrefix);
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV15Update));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavUpdate_Tooltiptext));
               GridColumn.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavUpdate_Visible), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV17Delete));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDelete_Tooltiptext));
               GridColumn.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavDelete_Visible), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1473ContratoServicosCusto_Codigo), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A1477ContratoServicosCusto_UsrPesNom));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratoServicosCusto_UsrPesNom_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoServicosCusto_UsrPesNom_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( A1474ContratoServicosCusto_CstUntPrdNrm, 18, 5, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratoServicosCusto_CstUntPrdNrm_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoServicosCusto_CstUntPrdNrm_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( A1475ContratoServicosCusto_CstUntPrdExt, 18, 5, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtContratoServicosCusto_CstUntPrdExt_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtContratoServicosCusto_CstUntPrdExt_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 5 )
         {
            wbEnd = 0;
            nRC_GXsfl_5 = (short)(nGXsfl_5_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               GridContainer.AddObjectProperty("GRID_nEOF", GRID_nEOF);
               GridContainer.AddObjectProperty("GRID_nFirstRecordOnPage", GRID_nFirstRecordOnPage);
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid(sPrefix+"_"+"Grid", GridContainer);
               if ( ! isAjaxCallMode( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+sPrefix+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 13,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgInsert_Internalname, context.GetImagePath( "5649fbb8-8ce0-4810-a5ce-bd649ea83c3a", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgInsert_Visible, 1, "", "Inserir", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgInsert_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOINSERT\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContratoServicosCustoWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_KZ2e( true) ;
         }
         else
         {
            wb_table1_2_KZ2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV7ContratoServicosCusto_CntSrvCod = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7ContratoServicosCusto_CntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7ContratoServicosCusto_CntSrvCod), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAKZ2( ) ;
         WSKZ2( ) ;
         WEKZ2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlAV7ContratoServicosCusto_CntSrvCod = (String)((String)getParm(obj,0));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PAKZ2( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "contratoservicoscustowc");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PAKZ2( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            AV7ContratoServicosCusto_CntSrvCod = Convert.ToInt32(getParm(obj,2));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7ContratoServicosCusto_CntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7ContratoServicosCusto_CntSrvCod), 6, 0)));
         }
         wcpOAV7ContratoServicosCusto_CntSrvCod = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV7ContratoServicosCusto_CntSrvCod"), ",", "."));
         if ( ! GetJustCreated( ) && ( ( AV7ContratoServicosCusto_CntSrvCod != wcpOAV7ContratoServicosCusto_CntSrvCod ) ) )
         {
            setjustcreated();
         }
         wcpOAV7ContratoServicosCusto_CntSrvCod = AV7ContratoServicosCusto_CntSrvCod;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlAV7ContratoServicosCusto_CntSrvCod = cgiGet( sPrefix+"AV7ContratoServicosCusto_CntSrvCod_CTRL");
         if ( StringUtil.Len( sCtrlAV7ContratoServicosCusto_CntSrvCod) > 0 )
         {
            AV7ContratoServicosCusto_CntSrvCod = (int)(context.localUtil.CToN( cgiGet( sCtrlAV7ContratoServicosCusto_CntSrvCod), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7ContratoServicosCusto_CntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7ContratoServicosCusto_CntSrvCod), 6, 0)));
         }
         else
         {
            AV7ContratoServicosCusto_CntSrvCod = (int)(context.localUtil.CToN( cgiGet( sPrefix+"AV7ContratoServicosCusto_CntSrvCod_PARM"), ",", "."));
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PAKZ2( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WSKZ2( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WSKZ2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"AV7ContratoServicosCusto_CntSrvCod_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7ContratoServicosCusto_CntSrvCod), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlAV7ContratoServicosCusto_CntSrvCod)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"AV7ContratoServicosCusto_CntSrvCod_CTRL", StringUtil.RTrim( sCtrlAV7ContratoServicosCusto_CntSrvCod));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WEKZ2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?2020529931476");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("contratoservicoscustowc.js", "?2020529931476");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_52( )
      {
         edtavUpdate_Internalname = sPrefix+"vUPDATE_"+sGXsfl_5_idx;
         edtavDelete_Internalname = sPrefix+"vDELETE_"+sGXsfl_5_idx;
         edtContratoServicosCusto_Codigo_Internalname = sPrefix+"CONTRATOSERVICOSCUSTO_CODIGO_"+sGXsfl_5_idx;
         edtContratoServicosCusto_UsrPesNom_Internalname = sPrefix+"CONTRATOSERVICOSCUSTO_USRPESNOM_"+sGXsfl_5_idx;
         edtContratoServicosCusto_CstUntPrdNrm_Internalname = sPrefix+"CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM_"+sGXsfl_5_idx;
         edtContratoServicosCusto_CstUntPrdExt_Internalname = sPrefix+"CONTRATOSERVICOSCUSTO_CSTUNTPRDEXT_"+sGXsfl_5_idx;
      }

      protected void SubsflControlProps_fel_52( )
      {
         edtavUpdate_Internalname = sPrefix+"vUPDATE_"+sGXsfl_5_fel_idx;
         edtavDelete_Internalname = sPrefix+"vDELETE_"+sGXsfl_5_fel_idx;
         edtContratoServicosCusto_Codigo_Internalname = sPrefix+"CONTRATOSERVICOSCUSTO_CODIGO_"+sGXsfl_5_fel_idx;
         edtContratoServicosCusto_UsrPesNom_Internalname = sPrefix+"CONTRATOSERVICOSCUSTO_USRPESNOM_"+sGXsfl_5_fel_idx;
         edtContratoServicosCusto_CstUntPrdNrm_Internalname = sPrefix+"CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM_"+sGXsfl_5_fel_idx;
         edtContratoServicosCusto_CstUntPrdExt_Internalname = sPrefix+"CONTRATOSERVICOSCUSTO_CSTUNTPRDEXT_"+sGXsfl_5_fel_idx;
      }

      protected void sendrow_52( )
      {
         SubsflControlProps_52( ) ;
         WBKZ0( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_5_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_5_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_5_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+((edtavUpdate_Visible==0) ? "display:none;" : "")+"\">") ;
            }
            /* Active Bitmap Variable */
            TempTags = " " + ((edtavUpdate_Enabled!=0)&&(edtavUpdate_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 6,'"+sPrefix+"',false,'',5)\"" : " ");
            ClassString = "Image";
            StyleString = "";
            AV15Update_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV15Update))&&String.IsNullOrEmpty(StringUtil.RTrim( AV34Update_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV15Update)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavUpdate_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV15Update)) ? AV34Update_GXI : context.PathToRelativeUrl( AV15Update)),(String)"",(String)"",(String)"",context.GetTheme( ),(int)edtavUpdate_Visible,(short)1,(String)"",(String)edtavUpdate_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)5,(String)edtavUpdate_Jsonclick,"'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOUPDATE\\'."+sGXsfl_5_idx+"'",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)""+TempTags,(String)"",(String)"",(short)1,(bool)AV15Update_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+((edtavDelete_Visible==0) ? "display:none;" : "")+"\">") ;
            }
            /* Active Bitmap Variable */
            TempTags = " " + ((edtavDelete_Enabled!=0)&&(edtavDelete_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 7,'"+sPrefix+"',false,'',5)\"" : " ");
            ClassString = "Image";
            StyleString = "";
            AV17Delete_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV17Delete))&&String.IsNullOrEmpty(StringUtil.RTrim( AV35Delete_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV17Delete)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDelete_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV17Delete)) ? AV35Delete_GXI : context.PathToRelativeUrl( AV17Delete)),(String)"",(String)"",(String)"",context.GetTheme( ),(int)edtavDelete_Visible,(short)1,(String)"",(String)edtavDelete_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)5,(String)edtavDelete_Jsonclick,"'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DODELETE\\'."+sGXsfl_5_idx+"'",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)""+TempTags,(String)"",(String)"",(short)1,(bool)AV17Delete_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoServicosCusto_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A1473ContratoServicosCusto_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A1473ContratoServicosCusto_Codigo), "ZZZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoServicosCusto_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)5,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoServicosCusto_UsrPesNom_Internalname,StringUtil.RTrim( A1477ContratoServicosCusto_UsrPesNom),StringUtil.RTrim( context.localUtil.Format( A1477ContratoServicosCusto_UsrPesNom, "@!")),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoServicosCusto_UsrPesNom_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)100,(short)0,(short)0,(short)5,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome100",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoServicosCusto_CstUntPrdNrm_Internalname,StringUtil.LTrim( StringUtil.NToC( A1474ContratoServicosCusto_CstUntPrdNrm, 18, 5, ",", "")),context.localUtil.Format( A1474ContratoServicosCusto_CstUntPrdNrm, "ZZZ,ZZZ,ZZZ,ZZ9.99"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoServicosCusto_CstUntPrdNrm_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)118,(String)"px",(short)17,(String)"px",(short)18,(short)0,(short)0,(short)5,(short)1,(short)-1,(short)0,(bool)true,(String)"Valor",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtContratoServicosCusto_CstUntPrdExt_Internalname,StringUtil.LTrim( StringUtil.NToC( A1475ContratoServicosCusto_CstUntPrdExt, 18, 5, ",", "")),context.localUtil.Format( A1475ContratoServicosCusto_CstUntPrdExt, "ZZZ,ZZZ,ZZZ,ZZ9.99"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtContratoServicosCusto_CstUntPrdExt_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)118,(String)"px",(short)17,(String)"px",(short)18,(short)0,(short)0,(short)5,(short)1,(short)-1,(short)0,(bool)true,(String)"Valor",(String)"right",(bool)false});
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOSERVICOSCUSTO_CODIGO"+"_"+sGXsfl_5_idx, GetSecureSignedToken( sPrefix+sGXsfl_5_idx, context.localUtil.Format( (decimal)(A1473ContratoServicosCusto_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM"+"_"+sGXsfl_5_idx, GetSecureSignedToken( sPrefix+sGXsfl_5_idx, context.localUtil.Format( A1474ContratoServicosCusto_CstUntPrdNrm, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CONTRATOSERVICOSCUSTO_CSTUNTPRDEXT"+"_"+sGXsfl_5_idx, GetSecureSignedToken( sPrefix+sGXsfl_5_idx, context.localUtil.Format( A1475ContratoServicosCusto_CstUntPrdExt, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
            GridContainer.AddRow(GridRow);
            nGXsfl_5_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_5_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_5_idx+1));
            sGXsfl_5_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_5_idx), 4, 0)), 4, "0");
            SubsflControlProps_52( ) ;
         }
         /* End function sendrow_52 */
      }

      protected void init_default_properties( )
      {
         edtavUpdate_Internalname = sPrefix+"vUPDATE";
         edtavDelete_Internalname = sPrefix+"vDELETE";
         edtContratoServicosCusto_Codigo_Internalname = sPrefix+"CONTRATOSERVICOSCUSTO_CODIGO";
         edtContratoServicosCusto_UsrPesNom_Internalname = sPrefix+"CONTRATOSERVICOSCUSTO_USRPESNOM";
         edtContratoServicosCusto_CstUntPrdNrm_Internalname = sPrefix+"CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM";
         edtContratoServicosCusto_CstUntPrdExt_Internalname = sPrefix+"CONTRATOSERVICOSCUSTO_CSTUNTPRDEXT";
         imgInsert_Internalname = sPrefix+"INSERT";
         tblUnnamedtable1_Internalname = sPrefix+"UNNAMEDTABLE1";
         edtContratoServicosCusto_CntSrvCod_Internalname = sPrefix+"CONTRATOSERVICOSCUSTO_CNTSRVCOD";
         Workwithplusutilities1_Internalname = sPrefix+"WORKWITHPLUSUTILITIES1";
         edtavOrderedby_Internalname = sPrefix+"vORDEREDBY";
         edtavOrdereddsc_Internalname = sPrefix+"vORDEREDDSC";
         edtavTfcontratoservicoscusto_usrpesnom_Internalname = sPrefix+"vTFCONTRATOSERVICOSCUSTO_USRPESNOM";
         edtavTfcontratoservicoscusto_usrpesnom_sel_Internalname = sPrefix+"vTFCONTRATOSERVICOSCUSTO_USRPESNOM_SEL";
         edtavTfcontratoservicoscusto_cstuntprdnrm_Internalname = sPrefix+"vTFCONTRATOSERVICOSCUSTO_CSTUNTPRDNRM";
         edtavTfcontratoservicoscusto_cstuntprdnrm_to_Internalname = sPrefix+"vTFCONTRATOSERVICOSCUSTO_CSTUNTPRDNRM_TO";
         edtavTfcontratoservicoscusto_cstuntprdext_Internalname = sPrefix+"vTFCONTRATOSERVICOSCUSTO_CSTUNTPRDEXT";
         edtavTfcontratoservicoscusto_cstuntprdext_to_Internalname = sPrefix+"vTFCONTRATOSERVICOSCUSTO_CSTUNTPRDEXT_TO";
         Ddo_contratoservicoscusto_usrpesnom_Internalname = sPrefix+"DDO_CONTRATOSERVICOSCUSTO_USRPESNOM";
         edtavDdo_contratoservicoscusto_usrpesnomtitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_CONTRATOSERVICOSCUSTO_USRPESNOMTITLECONTROLIDTOREPLACE";
         Ddo_contratoservicoscusto_cstuntprdnrm_Internalname = sPrefix+"DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM";
         edtavDdo_contratoservicoscusto_cstuntprdnrmtitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDNRMTITLECONTROLIDTOREPLACE";
         Ddo_contratoservicoscusto_cstuntprdext_Internalname = sPrefix+"DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDEXT";
         edtavDdo_contratoservicoscusto_cstuntprdexttitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDEXTTITLECONTROLIDTOREPLACE";
         Form.Internalname = sPrefix+"FORM";
         subGrid_Internalname = sPrefix+"GRID";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         edtContratoServicosCusto_CstUntPrdExt_Jsonclick = "";
         edtContratoServicosCusto_CstUntPrdNrm_Jsonclick = "";
         edtContratoServicosCusto_UsrPesNom_Jsonclick = "";
         edtContratoServicosCusto_Codigo_Jsonclick = "";
         edtavDelete_Jsonclick = "";
         edtavDelete_Enabled = 1;
         edtavUpdate_Jsonclick = "";
         edtavUpdate_Enabled = 1;
         imgInsert_Visible = 1;
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtavDelete_Tooltiptext = "Eliminar";
         edtavUpdate_Tooltiptext = "Modifica";
         edtContratoServicosCusto_CstUntPrdExt_Titleformat = 0;
         edtContratoServicosCusto_CstUntPrdNrm_Titleformat = 0;
         edtContratoServicosCusto_UsrPesNom_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         edtavDelete_Visible = -1;
         edtavUpdate_Visible = -1;
         edtContratoServicosCusto_CstUntPrdExt_Title = "Extra R$";
         edtContratoServicosCusto_CstUntPrdNrm_Title = "Normal R$";
         edtContratoServicosCusto_UsrPesNom_Title = "Colaborador";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         edtavDdo_contratoservicoscusto_cstuntprdexttitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratoservicoscusto_cstuntprdnrmtitlecontrolidtoreplace_Visible = 1;
         edtavDdo_contratoservicoscusto_usrpesnomtitlecontrolidtoreplace_Visible = 1;
         edtavTfcontratoservicoscusto_cstuntprdext_to_Jsonclick = "";
         edtavTfcontratoservicoscusto_cstuntprdext_to_Visible = 1;
         edtavTfcontratoservicoscusto_cstuntprdext_Jsonclick = "";
         edtavTfcontratoservicoscusto_cstuntprdext_Visible = 1;
         edtavTfcontratoservicoscusto_cstuntprdnrm_to_Jsonclick = "";
         edtavTfcontratoservicoscusto_cstuntprdnrm_to_Visible = 1;
         edtavTfcontratoservicoscusto_cstuntprdnrm_Jsonclick = "";
         edtavTfcontratoservicoscusto_cstuntprdnrm_Visible = 1;
         edtavTfcontratoservicoscusto_usrpesnom_sel_Jsonclick = "";
         edtavTfcontratoservicoscusto_usrpesnom_sel_Visible = 1;
         edtavTfcontratoservicoscusto_usrpesnom_Jsonclick = "";
         edtavTfcontratoservicoscusto_usrpesnom_Visible = 1;
         edtavOrdereddsc_Jsonclick = "";
         edtavOrdereddsc_Visible = 1;
         edtavOrderedby_Jsonclick = "";
         edtavOrderedby_Visible = 1;
         edtContratoServicosCusto_CntSrvCod_Jsonclick = "";
         edtContratoServicosCusto_CntSrvCod_Visible = 1;
         Ddo_contratoservicoscusto_cstuntprdext_Searchbuttontext = "Pesquisar";
         Ddo_contratoservicoscusto_cstuntprdext_Rangefilterto = "At�";
         Ddo_contratoservicoscusto_cstuntprdext_Rangefilterfrom = "Desde";
         Ddo_contratoservicoscusto_cstuntprdext_Cleanfilter = "Limpar pesquisa";
         Ddo_contratoservicoscusto_cstuntprdext_Sortdsc = "Ordenar de Z � A";
         Ddo_contratoservicoscusto_cstuntprdext_Sortasc = "Ordenar de A � Z";
         Ddo_contratoservicoscusto_cstuntprdext_Includedatalist = Convert.ToBoolean( 0);
         Ddo_contratoservicoscusto_cstuntprdext_Filterisrange = Convert.ToBoolean( -1);
         Ddo_contratoservicoscusto_cstuntprdext_Filtertype = "Numeric";
         Ddo_contratoservicoscusto_cstuntprdext_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratoservicoscusto_cstuntprdext_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratoservicoscusto_cstuntprdext_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratoservicoscusto_cstuntprdext_Titlecontrolidtoreplace = "";
         Ddo_contratoservicoscusto_cstuntprdext_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratoservicoscusto_cstuntprdext_Cls = "ColumnSettings";
         Ddo_contratoservicoscusto_cstuntprdext_Tooltip = "Op��es";
         Ddo_contratoservicoscusto_cstuntprdext_Caption = "";
         Ddo_contratoservicoscusto_cstuntprdnrm_Searchbuttontext = "Pesquisar";
         Ddo_contratoservicoscusto_cstuntprdnrm_Rangefilterto = "At�";
         Ddo_contratoservicoscusto_cstuntprdnrm_Rangefilterfrom = "Desde";
         Ddo_contratoservicoscusto_cstuntprdnrm_Cleanfilter = "Limpar pesquisa";
         Ddo_contratoservicoscusto_cstuntprdnrm_Sortdsc = "Ordenar de Z � A";
         Ddo_contratoservicoscusto_cstuntprdnrm_Sortasc = "Ordenar de A � Z";
         Ddo_contratoservicoscusto_cstuntprdnrm_Includedatalist = Convert.ToBoolean( 0);
         Ddo_contratoservicoscusto_cstuntprdnrm_Filterisrange = Convert.ToBoolean( -1);
         Ddo_contratoservicoscusto_cstuntprdnrm_Filtertype = "Numeric";
         Ddo_contratoservicoscusto_cstuntprdnrm_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratoservicoscusto_cstuntprdnrm_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratoservicoscusto_cstuntprdnrm_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratoservicoscusto_cstuntprdnrm_Titlecontrolidtoreplace = "";
         Ddo_contratoservicoscusto_cstuntprdnrm_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratoservicoscusto_cstuntprdnrm_Cls = "ColumnSettings";
         Ddo_contratoservicoscusto_cstuntprdnrm_Tooltip = "Op��es";
         Ddo_contratoservicoscusto_cstuntprdnrm_Caption = "";
         Ddo_contratoservicoscusto_usrpesnom_Searchbuttontext = "Pesquisar";
         Ddo_contratoservicoscusto_usrpesnom_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_contratoservicoscusto_usrpesnom_Cleanfilter = "Limpar pesquisa";
         Ddo_contratoservicoscusto_usrpesnom_Loadingdata = "Carregando dados...";
         Ddo_contratoservicoscusto_usrpesnom_Sortdsc = "Ordenar de Z � A";
         Ddo_contratoservicoscusto_usrpesnom_Sortasc = "Ordenar de A � Z";
         Ddo_contratoservicoscusto_usrpesnom_Datalistupdateminimumcharacters = 0;
         Ddo_contratoservicoscusto_usrpesnom_Datalistproc = "GetContratoServicosCustoWCFilterData";
         Ddo_contratoservicoscusto_usrpesnom_Datalisttype = "Dynamic";
         Ddo_contratoservicoscusto_usrpesnom_Includedatalist = Convert.ToBoolean( -1);
         Ddo_contratoservicoscusto_usrpesnom_Filterisrange = Convert.ToBoolean( 0);
         Ddo_contratoservicoscusto_usrpesnom_Filtertype = "Character";
         Ddo_contratoservicoscusto_usrpesnom_Includefilter = Convert.ToBoolean( -1);
         Ddo_contratoservicoscusto_usrpesnom_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_contratoservicoscusto_usrpesnom_Includesortasc = Convert.ToBoolean( -1);
         Ddo_contratoservicoscusto_usrpesnom_Titlecontrolidtoreplace = "";
         Ddo_contratoservicoscusto_usrpesnom_Dropdownoptionstype = "GridTitleSettings";
         Ddo_contratoservicoscusto_usrpesnom_Cls = "ColumnSettings";
         Ddo_contratoservicoscusto_usrpesnom_Tooltip = "Op��es";
         Ddo_contratoservicoscusto_usrpesnom_Caption = "";
         subGrid_Rows = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'sPrefix',nv:''},{av:'AV22ddo_ContratoServicosCusto_UsrPesNomTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSCUSTO_USRPESNOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV26ddo_ContratoServicosCusto_CstUntPrdNrmTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDNRMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV30ddo_ContratoServicosCusto_CstUntPrdExtTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDEXTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV36Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV20TFContratoServicosCusto_UsrPesNom',fld:'vTFCONTRATOSERVICOSCUSTO_USRPESNOM',pic:'@!',nv:''},{av:'AV21TFContratoServicosCusto_UsrPesNom_Sel',fld:'vTFCONTRATOSERVICOSCUSTO_USRPESNOM_SEL',pic:'@!',nv:''},{av:'AV24TFContratoServicosCusto_CstUntPrdNrm',fld:'vTFCONTRATOSERVICOSCUSTO_CSTUNTPRDNRM',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV25TFContratoServicosCusto_CstUntPrdNrm_To',fld:'vTFCONTRATOSERVICOSCUSTO_CSTUNTPRDNRM_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV28TFContratoServicosCusto_CstUntPrdExt',fld:'vTFCONTRATOSERVICOSCUSTO_CSTUNTPRDEXT',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV29TFContratoServicosCusto_CstUntPrdExt_To',fld:'vTFCONTRATOSERVICOSCUSTO_CSTUNTPRDEXT_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV7ContratoServicosCusto_CntSrvCod',fld:'vCONTRATOSERVICOSCUSTO_CNTSRVCOD',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV19ContratoServicosCusto_UsrPesNomTitleFilterData',fld:'vCONTRATOSERVICOSCUSTO_USRPESNOMTITLEFILTERDATA',pic:'',nv:null},{av:'AV23ContratoServicosCusto_CstUntPrdNrmTitleFilterData',fld:'vCONTRATOSERVICOSCUSTO_CSTUNTPRDNRMTITLEFILTERDATA',pic:'',nv:null},{av:'AV27ContratoServicosCusto_CstUntPrdExtTitleFilterData',fld:'vCONTRATOSERVICOSCUSTO_CSTUNTPRDEXTTITLEFILTERDATA',pic:'',nv:null},{av:'edtContratoServicosCusto_UsrPesNom_Titleformat',ctrl:'CONTRATOSERVICOSCUSTO_USRPESNOM',prop:'Titleformat'},{av:'edtContratoServicosCusto_UsrPesNom_Title',ctrl:'CONTRATOSERVICOSCUSTO_USRPESNOM',prop:'Title'},{av:'edtContratoServicosCusto_CstUntPrdNrm_Titleformat',ctrl:'CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM',prop:'Titleformat'},{av:'edtContratoServicosCusto_CstUntPrdNrm_Title',ctrl:'CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM',prop:'Title'},{av:'edtContratoServicosCusto_CstUntPrdExt_Titleformat',ctrl:'CONTRATOSERVICOSCUSTO_CSTUNTPRDEXT',prop:'Titleformat'},{av:'edtContratoServicosCusto_CstUntPrdExt_Title',ctrl:'CONTRATOSERVICOSCUSTO_CSTUNTPRDEXT',prop:'Title'},{av:'edtavUpdate_Visible',ctrl:'vUPDATE',prop:'Visible'},{av:'edtavDelete_Visible',ctrl:'vDELETE',prop:'Visible'},{av:'imgInsert_Visible',ctrl:'INSERT',prop:'Visible'}]}");
         setEventMetadata("DDO_CONTRATOSERVICOSCUSTO_USRPESNOM.ONOPTIONCLICKED","{handler:'E11KZ2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV20TFContratoServicosCusto_UsrPesNom',fld:'vTFCONTRATOSERVICOSCUSTO_USRPESNOM',pic:'@!',nv:''},{av:'AV21TFContratoServicosCusto_UsrPesNom_Sel',fld:'vTFCONTRATOSERVICOSCUSTO_USRPESNOM_SEL',pic:'@!',nv:''},{av:'AV24TFContratoServicosCusto_CstUntPrdNrm',fld:'vTFCONTRATOSERVICOSCUSTO_CSTUNTPRDNRM',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV25TFContratoServicosCusto_CstUntPrdNrm_To',fld:'vTFCONTRATOSERVICOSCUSTO_CSTUNTPRDNRM_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV28TFContratoServicosCusto_CstUntPrdExt',fld:'vTFCONTRATOSERVICOSCUSTO_CSTUNTPRDEXT',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV29TFContratoServicosCusto_CstUntPrdExt_To',fld:'vTFCONTRATOSERVICOSCUSTO_CSTUNTPRDEXT_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV7ContratoServicosCusto_CntSrvCod',fld:'vCONTRATOSERVICOSCUSTO_CNTSRVCOD',pic:'ZZZZZ9',nv:0},{av:'AV22ddo_ContratoServicosCusto_UsrPesNomTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSCUSTO_USRPESNOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV26ddo_ContratoServicosCusto_CstUntPrdNrmTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDNRMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV30ddo_ContratoServicosCusto_CstUntPrdExtTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDEXTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV36Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'sPrefix',nv:''},{av:'Ddo_contratoservicoscusto_usrpesnom_Activeeventkey',ctrl:'DDO_CONTRATOSERVICOSCUSTO_USRPESNOM',prop:'ActiveEventKey'},{av:'Ddo_contratoservicoscusto_usrpesnom_Filteredtext_get',ctrl:'DDO_CONTRATOSERVICOSCUSTO_USRPESNOM',prop:'FilteredText_get'},{av:'Ddo_contratoservicoscusto_usrpesnom_Selectedvalue_get',ctrl:'DDO_CONTRATOSERVICOSCUSTO_USRPESNOM',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratoservicoscusto_usrpesnom_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSCUSTO_USRPESNOM',prop:'SortedStatus'},{av:'AV20TFContratoServicosCusto_UsrPesNom',fld:'vTFCONTRATOSERVICOSCUSTO_USRPESNOM',pic:'@!',nv:''},{av:'AV21TFContratoServicosCusto_UsrPesNom_Sel',fld:'vTFCONTRATOSERVICOSCUSTO_USRPESNOM_SEL',pic:'@!',nv:''},{av:'Ddo_contratoservicoscusto_cstuntprdnrm_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM',prop:'SortedStatus'},{av:'Ddo_contratoservicoscusto_cstuntprdext_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDEXT',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM.ONOPTIONCLICKED","{handler:'E12KZ2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV20TFContratoServicosCusto_UsrPesNom',fld:'vTFCONTRATOSERVICOSCUSTO_USRPESNOM',pic:'@!',nv:''},{av:'AV21TFContratoServicosCusto_UsrPesNom_Sel',fld:'vTFCONTRATOSERVICOSCUSTO_USRPESNOM_SEL',pic:'@!',nv:''},{av:'AV24TFContratoServicosCusto_CstUntPrdNrm',fld:'vTFCONTRATOSERVICOSCUSTO_CSTUNTPRDNRM',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV25TFContratoServicosCusto_CstUntPrdNrm_To',fld:'vTFCONTRATOSERVICOSCUSTO_CSTUNTPRDNRM_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV28TFContratoServicosCusto_CstUntPrdExt',fld:'vTFCONTRATOSERVICOSCUSTO_CSTUNTPRDEXT',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV29TFContratoServicosCusto_CstUntPrdExt_To',fld:'vTFCONTRATOSERVICOSCUSTO_CSTUNTPRDEXT_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV7ContratoServicosCusto_CntSrvCod',fld:'vCONTRATOSERVICOSCUSTO_CNTSRVCOD',pic:'ZZZZZ9',nv:0},{av:'AV22ddo_ContratoServicosCusto_UsrPesNomTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSCUSTO_USRPESNOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV26ddo_ContratoServicosCusto_CstUntPrdNrmTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDNRMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV30ddo_ContratoServicosCusto_CstUntPrdExtTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDEXTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV36Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'sPrefix',nv:''},{av:'Ddo_contratoservicoscusto_cstuntprdnrm_Activeeventkey',ctrl:'DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM',prop:'ActiveEventKey'},{av:'Ddo_contratoservicoscusto_cstuntprdnrm_Filteredtext_get',ctrl:'DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM',prop:'FilteredText_get'},{av:'Ddo_contratoservicoscusto_cstuntprdnrm_Filteredtextto_get',ctrl:'DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratoservicoscusto_cstuntprdnrm_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM',prop:'SortedStatus'},{av:'AV24TFContratoServicosCusto_CstUntPrdNrm',fld:'vTFCONTRATOSERVICOSCUSTO_CSTUNTPRDNRM',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV25TFContratoServicosCusto_CstUntPrdNrm_To',fld:'vTFCONTRATOSERVICOSCUSTO_CSTUNTPRDNRM_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'Ddo_contratoservicoscusto_usrpesnom_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSCUSTO_USRPESNOM',prop:'SortedStatus'},{av:'Ddo_contratoservicoscusto_cstuntprdext_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDEXT',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDEXT.ONOPTIONCLICKED","{handler:'E13KZ2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV20TFContratoServicosCusto_UsrPesNom',fld:'vTFCONTRATOSERVICOSCUSTO_USRPESNOM',pic:'@!',nv:''},{av:'AV21TFContratoServicosCusto_UsrPesNom_Sel',fld:'vTFCONTRATOSERVICOSCUSTO_USRPESNOM_SEL',pic:'@!',nv:''},{av:'AV24TFContratoServicosCusto_CstUntPrdNrm',fld:'vTFCONTRATOSERVICOSCUSTO_CSTUNTPRDNRM',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV25TFContratoServicosCusto_CstUntPrdNrm_To',fld:'vTFCONTRATOSERVICOSCUSTO_CSTUNTPRDNRM_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV28TFContratoServicosCusto_CstUntPrdExt',fld:'vTFCONTRATOSERVICOSCUSTO_CSTUNTPRDEXT',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV29TFContratoServicosCusto_CstUntPrdExt_To',fld:'vTFCONTRATOSERVICOSCUSTO_CSTUNTPRDEXT_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV7ContratoServicosCusto_CntSrvCod',fld:'vCONTRATOSERVICOSCUSTO_CNTSRVCOD',pic:'ZZZZZ9',nv:0},{av:'AV22ddo_ContratoServicosCusto_UsrPesNomTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSCUSTO_USRPESNOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV26ddo_ContratoServicosCusto_CstUntPrdNrmTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDNRMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV30ddo_ContratoServicosCusto_CstUntPrdExtTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDEXTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV36Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'sPrefix',nv:''},{av:'Ddo_contratoservicoscusto_cstuntprdext_Activeeventkey',ctrl:'DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDEXT',prop:'ActiveEventKey'},{av:'Ddo_contratoservicoscusto_cstuntprdext_Filteredtext_get',ctrl:'DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDEXT',prop:'FilteredText_get'},{av:'Ddo_contratoservicoscusto_cstuntprdext_Filteredtextto_get',ctrl:'DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDEXT',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_contratoservicoscusto_cstuntprdext_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDEXT',prop:'SortedStatus'},{av:'AV28TFContratoServicosCusto_CstUntPrdExt',fld:'vTFCONTRATOSERVICOSCUSTO_CSTUNTPRDEXT',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV29TFContratoServicosCusto_CstUntPrdExt_To',fld:'vTFCONTRATOSERVICOSCUSTO_CSTUNTPRDEXT_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'Ddo_contratoservicoscusto_usrpesnom_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSCUSTO_USRPESNOM',prop:'SortedStatus'},{av:'Ddo_contratoservicoscusto_cstuntprdnrm_Sortedstatus',ctrl:'DDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM',prop:'SortedStatus'}]}");
         setEventMetadata("GRID.LOAD","{handler:'E17KZ2',iparms:[],oparms:[{av:'AV15Update',fld:'vUPDATE',pic:'',nv:''},{av:'edtavUpdate_Tooltiptext',ctrl:'vUPDATE',prop:'Tooltiptext'},{av:'AV17Delete',fld:'vDELETE',pic:'',nv:''},{av:'edtavDelete_Tooltiptext',ctrl:'vDELETE',prop:'Tooltiptext'}]}");
         setEventMetadata("'DOINSERT'","{handler:'E14KZ2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV20TFContratoServicosCusto_UsrPesNom',fld:'vTFCONTRATOSERVICOSCUSTO_USRPESNOM',pic:'@!',nv:''},{av:'AV21TFContratoServicosCusto_UsrPesNom_Sel',fld:'vTFCONTRATOSERVICOSCUSTO_USRPESNOM_SEL',pic:'@!',nv:''},{av:'AV24TFContratoServicosCusto_CstUntPrdNrm',fld:'vTFCONTRATOSERVICOSCUSTO_CSTUNTPRDNRM',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV25TFContratoServicosCusto_CstUntPrdNrm_To',fld:'vTFCONTRATOSERVICOSCUSTO_CSTUNTPRDNRM_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV28TFContratoServicosCusto_CstUntPrdExt',fld:'vTFCONTRATOSERVICOSCUSTO_CSTUNTPRDEXT',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV29TFContratoServicosCusto_CstUntPrdExt_To',fld:'vTFCONTRATOSERVICOSCUSTO_CSTUNTPRDEXT_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV7ContratoServicosCusto_CntSrvCod',fld:'vCONTRATOSERVICOSCUSTO_CNTSRVCOD',pic:'ZZZZZ9',nv:0},{av:'AV22ddo_ContratoServicosCusto_UsrPesNomTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSCUSTO_USRPESNOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV26ddo_ContratoServicosCusto_CstUntPrdNrmTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDNRMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV30ddo_ContratoServicosCusto_CstUntPrdExtTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDEXTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV36Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'sPrefix',nv:''}],oparms:[{av:'AV7ContratoServicosCusto_CntSrvCod',fld:'vCONTRATOSERVICOSCUSTO_CNTSRVCOD',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("'DOUPDATE'","{handler:'E18KZ2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV20TFContratoServicosCusto_UsrPesNom',fld:'vTFCONTRATOSERVICOSCUSTO_USRPESNOM',pic:'@!',nv:''},{av:'AV21TFContratoServicosCusto_UsrPesNom_Sel',fld:'vTFCONTRATOSERVICOSCUSTO_USRPESNOM_SEL',pic:'@!',nv:''},{av:'AV24TFContratoServicosCusto_CstUntPrdNrm',fld:'vTFCONTRATOSERVICOSCUSTO_CSTUNTPRDNRM',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV25TFContratoServicosCusto_CstUntPrdNrm_To',fld:'vTFCONTRATOSERVICOSCUSTO_CSTUNTPRDNRM_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV28TFContratoServicosCusto_CstUntPrdExt',fld:'vTFCONTRATOSERVICOSCUSTO_CSTUNTPRDEXT',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV29TFContratoServicosCusto_CstUntPrdExt_To',fld:'vTFCONTRATOSERVICOSCUSTO_CSTUNTPRDEXT_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV7ContratoServicosCusto_CntSrvCod',fld:'vCONTRATOSERVICOSCUSTO_CNTSRVCOD',pic:'ZZZZZ9',nv:0},{av:'AV22ddo_ContratoServicosCusto_UsrPesNomTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSCUSTO_USRPESNOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV26ddo_ContratoServicosCusto_CstUntPrdNrmTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDNRMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV30ddo_ContratoServicosCusto_CstUntPrdExtTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDEXTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV36Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'sPrefix',nv:''},{av:'A1473ContratoServicosCusto_Codigo',fld:'CONTRATOSERVICOSCUSTO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV7ContratoServicosCusto_CntSrvCod',fld:'vCONTRATOSERVICOSCUSTO_CNTSRVCOD',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("'DODELETE'","{handler:'E19KZ2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV20TFContratoServicosCusto_UsrPesNom',fld:'vTFCONTRATOSERVICOSCUSTO_USRPESNOM',pic:'@!',nv:''},{av:'AV21TFContratoServicosCusto_UsrPesNom_Sel',fld:'vTFCONTRATOSERVICOSCUSTO_USRPESNOM_SEL',pic:'@!',nv:''},{av:'AV24TFContratoServicosCusto_CstUntPrdNrm',fld:'vTFCONTRATOSERVICOSCUSTO_CSTUNTPRDNRM',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV25TFContratoServicosCusto_CstUntPrdNrm_To',fld:'vTFCONTRATOSERVICOSCUSTO_CSTUNTPRDNRM_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV28TFContratoServicosCusto_CstUntPrdExt',fld:'vTFCONTRATOSERVICOSCUSTO_CSTUNTPRDEXT',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV29TFContratoServicosCusto_CstUntPrdExt_To',fld:'vTFCONTRATOSERVICOSCUSTO_CSTUNTPRDEXT_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV7ContratoServicosCusto_CntSrvCod',fld:'vCONTRATOSERVICOSCUSTO_CNTSRVCOD',pic:'ZZZZZ9',nv:0},{av:'AV22ddo_ContratoServicosCusto_UsrPesNomTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSCUSTO_USRPESNOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV26ddo_ContratoServicosCusto_CstUntPrdNrmTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDNRMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV30ddo_ContratoServicosCusto_CstUntPrdExtTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDEXTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV36Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'sPrefix',nv:''},{av:'A1473ContratoServicosCusto_Codigo',fld:'CONTRATOSERVICOSCUSTO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV7ContratoServicosCusto_CntSrvCod',fld:'vCONTRATOSERVICOSCUSTO_CNTSRVCOD',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("GRID_FIRSTPAGE","{handler:'subgrid_firstpage',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'sPrefix',nv:''},{av:'AV22ddo_ContratoServicosCusto_UsrPesNomTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSCUSTO_USRPESNOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV26ddo_ContratoServicosCusto_CstUntPrdNrmTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDNRMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV30ddo_ContratoServicosCusto_CstUntPrdExtTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDEXTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV36Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV20TFContratoServicosCusto_UsrPesNom',fld:'vTFCONTRATOSERVICOSCUSTO_USRPESNOM',pic:'@!',nv:''},{av:'AV21TFContratoServicosCusto_UsrPesNom_Sel',fld:'vTFCONTRATOSERVICOSCUSTO_USRPESNOM_SEL',pic:'@!',nv:''},{av:'AV24TFContratoServicosCusto_CstUntPrdNrm',fld:'vTFCONTRATOSERVICOSCUSTO_CSTUNTPRDNRM',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV25TFContratoServicosCusto_CstUntPrdNrm_To',fld:'vTFCONTRATOSERVICOSCUSTO_CSTUNTPRDNRM_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV28TFContratoServicosCusto_CstUntPrdExt',fld:'vTFCONTRATOSERVICOSCUSTO_CSTUNTPRDEXT',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV29TFContratoServicosCusto_CstUntPrdExt_To',fld:'vTFCONTRATOSERVICOSCUSTO_CSTUNTPRDEXT_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV7ContratoServicosCusto_CntSrvCod',fld:'vCONTRATOSERVICOSCUSTO_CNTSRVCOD',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV19ContratoServicosCusto_UsrPesNomTitleFilterData',fld:'vCONTRATOSERVICOSCUSTO_USRPESNOMTITLEFILTERDATA',pic:'',nv:null},{av:'AV23ContratoServicosCusto_CstUntPrdNrmTitleFilterData',fld:'vCONTRATOSERVICOSCUSTO_CSTUNTPRDNRMTITLEFILTERDATA',pic:'',nv:null},{av:'AV27ContratoServicosCusto_CstUntPrdExtTitleFilterData',fld:'vCONTRATOSERVICOSCUSTO_CSTUNTPRDEXTTITLEFILTERDATA',pic:'',nv:null},{av:'edtContratoServicosCusto_UsrPesNom_Titleformat',ctrl:'CONTRATOSERVICOSCUSTO_USRPESNOM',prop:'Titleformat'},{av:'edtContratoServicosCusto_UsrPesNom_Title',ctrl:'CONTRATOSERVICOSCUSTO_USRPESNOM',prop:'Title'},{av:'edtContratoServicosCusto_CstUntPrdNrm_Titleformat',ctrl:'CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM',prop:'Titleformat'},{av:'edtContratoServicosCusto_CstUntPrdNrm_Title',ctrl:'CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM',prop:'Title'},{av:'edtContratoServicosCusto_CstUntPrdExt_Titleformat',ctrl:'CONTRATOSERVICOSCUSTO_CSTUNTPRDEXT',prop:'Titleformat'},{av:'edtContratoServicosCusto_CstUntPrdExt_Title',ctrl:'CONTRATOSERVICOSCUSTO_CSTUNTPRDEXT',prop:'Title'},{av:'edtavUpdate_Visible',ctrl:'vUPDATE',prop:'Visible'},{av:'edtavDelete_Visible',ctrl:'vDELETE',prop:'Visible'},{av:'imgInsert_Visible',ctrl:'INSERT',prop:'Visible'}]}");
         setEventMetadata("GRID_PREVPAGE","{handler:'subgrid_previouspage',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'sPrefix',nv:''},{av:'AV22ddo_ContratoServicosCusto_UsrPesNomTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSCUSTO_USRPESNOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV26ddo_ContratoServicosCusto_CstUntPrdNrmTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDNRMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV30ddo_ContratoServicosCusto_CstUntPrdExtTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDEXTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV36Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV20TFContratoServicosCusto_UsrPesNom',fld:'vTFCONTRATOSERVICOSCUSTO_USRPESNOM',pic:'@!',nv:''},{av:'AV21TFContratoServicosCusto_UsrPesNom_Sel',fld:'vTFCONTRATOSERVICOSCUSTO_USRPESNOM_SEL',pic:'@!',nv:''},{av:'AV24TFContratoServicosCusto_CstUntPrdNrm',fld:'vTFCONTRATOSERVICOSCUSTO_CSTUNTPRDNRM',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV25TFContratoServicosCusto_CstUntPrdNrm_To',fld:'vTFCONTRATOSERVICOSCUSTO_CSTUNTPRDNRM_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV28TFContratoServicosCusto_CstUntPrdExt',fld:'vTFCONTRATOSERVICOSCUSTO_CSTUNTPRDEXT',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV29TFContratoServicosCusto_CstUntPrdExt_To',fld:'vTFCONTRATOSERVICOSCUSTO_CSTUNTPRDEXT_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV7ContratoServicosCusto_CntSrvCod',fld:'vCONTRATOSERVICOSCUSTO_CNTSRVCOD',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV19ContratoServicosCusto_UsrPesNomTitleFilterData',fld:'vCONTRATOSERVICOSCUSTO_USRPESNOMTITLEFILTERDATA',pic:'',nv:null},{av:'AV23ContratoServicosCusto_CstUntPrdNrmTitleFilterData',fld:'vCONTRATOSERVICOSCUSTO_CSTUNTPRDNRMTITLEFILTERDATA',pic:'',nv:null},{av:'AV27ContratoServicosCusto_CstUntPrdExtTitleFilterData',fld:'vCONTRATOSERVICOSCUSTO_CSTUNTPRDEXTTITLEFILTERDATA',pic:'',nv:null},{av:'edtContratoServicosCusto_UsrPesNom_Titleformat',ctrl:'CONTRATOSERVICOSCUSTO_USRPESNOM',prop:'Titleformat'},{av:'edtContratoServicosCusto_UsrPesNom_Title',ctrl:'CONTRATOSERVICOSCUSTO_USRPESNOM',prop:'Title'},{av:'edtContratoServicosCusto_CstUntPrdNrm_Titleformat',ctrl:'CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM',prop:'Titleformat'},{av:'edtContratoServicosCusto_CstUntPrdNrm_Title',ctrl:'CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM',prop:'Title'},{av:'edtContratoServicosCusto_CstUntPrdExt_Titleformat',ctrl:'CONTRATOSERVICOSCUSTO_CSTUNTPRDEXT',prop:'Titleformat'},{av:'edtContratoServicosCusto_CstUntPrdExt_Title',ctrl:'CONTRATOSERVICOSCUSTO_CSTUNTPRDEXT',prop:'Title'},{av:'edtavUpdate_Visible',ctrl:'vUPDATE',prop:'Visible'},{av:'edtavDelete_Visible',ctrl:'vDELETE',prop:'Visible'},{av:'imgInsert_Visible',ctrl:'INSERT',prop:'Visible'}]}");
         setEventMetadata("GRID_NEXTPAGE","{handler:'subgrid_nextpage',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'sPrefix',nv:''},{av:'AV22ddo_ContratoServicosCusto_UsrPesNomTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSCUSTO_USRPESNOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV26ddo_ContratoServicosCusto_CstUntPrdNrmTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDNRMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV30ddo_ContratoServicosCusto_CstUntPrdExtTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDEXTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV36Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV20TFContratoServicosCusto_UsrPesNom',fld:'vTFCONTRATOSERVICOSCUSTO_USRPESNOM',pic:'@!',nv:''},{av:'AV21TFContratoServicosCusto_UsrPesNom_Sel',fld:'vTFCONTRATOSERVICOSCUSTO_USRPESNOM_SEL',pic:'@!',nv:''},{av:'AV24TFContratoServicosCusto_CstUntPrdNrm',fld:'vTFCONTRATOSERVICOSCUSTO_CSTUNTPRDNRM',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV25TFContratoServicosCusto_CstUntPrdNrm_To',fld:'vTFCONTRATOSERVICOSCUSTO_CSTUNTPRDNRM_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV28TFContratoServicosCusto_CstUntPrdExt',fld:'vTFCONTRATOSERVICOSCUSTO_CSTUNTPRDEXT',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV29TFContratoServicosCusto_CstUntPrdExt_To',fld:'vTFCONTRATOSERVICOSCUSTO_CSTUNTPRDEXT_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV7ContratoServicosCusto_CntSrvCod',fld:'vCONTRATOSERVICOSCUSTO_CNTSRVCOD',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV19ContratoServicosCusto_UsrPesNomTitleFilterData',fld:'vCONTRATOSERVICOSCUSTO_USRPESNOMTITLEFILTERDATA',pic:'',nv:null},{av:'AV23ContratoServicosCusto_CstUntPrdNrmTitleFilterData',fld:'vCONTRATOSERVICOSCUSTO_CSTUNTPRDNRMTITLEFILTERDATA',pic:'',nv:null},{av:'AV27ContratoServicosCusto_CstUntPrdExtTitleFilterData',fld:'vCONTRATOSERVICOSCUSTO_CSTUNTPRDEXTTITLEFILTERDATA',pic:'',nv:null},{av:'edtContratoServicosCusto_UsrPesNom_Titleformat',ctrl:'CONTRATOSERVICOSCUSTO_USRPESNOM',prop:'Titleformat'},{av:'edtContratoServicosCusto_UsrPesNom_Title',ctrl:'CONTRATOSERVICOSCUSTO_USRPESNOM',prop:'Title'},{av:'edtContratoServicosCusto_CstUntPrdNrm_Titleformat',ctrl:'CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM',prop:'Titleformat'},{av:'edtContratoServicosCusto_CstUntPrdNrm_Title',ctrl:'CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM',prop:'Title'},{av:'edtContratoServicosCusto_CstUntPrdExt_Titleformat',ctrl:'CONTRATOSERVICOSCUSTO_CSTUNTPRDEXT',prop:'Titleformat'},{av:'edtContratoServicosCusto_CstUntPrdExt_Title',ctrl:'CONTRATOSERVICOSCUSTO_CSTUNTPRDEXT',prop:'Title'},{av:'edtavUpdate_Visible',ctrl:'vUPDATE',prop:'Visible'},{av:'edtavDelete_Visible',ctrl:'vDELETE',prop:'Visible'},{av:'imgInsert_Visible',ctrl:'INSERT',prop:'Visible'}]}");
         setEventMetadata("GRID_LASTPAGE","{handler:'subgrid_lastpage',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'sPrefix',nv:''},{av:'AV22ddo_ContratoServicosCusto_UsrPesNomTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSCUSTO_USRPESNOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV26ddo_ContratoServicosCusto_CstUntPrdNrmTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDNRMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV30ddo_ContratoServicosCusto_CstUntPrdExtTitleControlIdToReplace',fld:'vDDO_CONTRATOSERVICOSCUSTO_CSTUNTPRDEXTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV36Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV20TFContratoServicosCusto_UsrPesNom',fld:'vTFCONTRATOSERVICOSCUSTO_USRPESNOM',pic:'@!',nv:''},{av:'AV21TFContratoServicosCusto_UsrPesNom_Sel',fld:'vTFCONTRATOSERVICOSCUSTO_USRPESNOM_SEL',pic:'@!',nv:''},{av:'AV24TFContratoServicosCusto_CstUntPrdNrm',fld:'vTFCONTRATOSERVICOSCUSTO_CSTUNTPRDNRM',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV25TFContratoServicosCusto_CstUntPrdNrm_To',fld:'vTFCONTRATOSERVICOSCUSTO_CSTUNTPRDNRM_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV28TFContratoServicosCusto_CstUntPrdExt',fld:'vTFCONTRATOSERVICOSCUSTO_CSTUNTPRDEXT',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV29TFContratoServicosCusto_CstUntPrdExt_To',fld:'vTFCONTRATOSERVICOSCUSTO_CSTUNTPRDEXT_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV7ContratoServicosCusto_CntSrvCod',fld:'vCONTRATOSERVICOSCUSTO_CNTSRVCOD',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV19ContratoServicosCusto_UsrPesNomTitleFilterData',fld:'vCONTRATOSERVICOSCUSTO_USRPESNOMTITLEFILTERDATA',pic:'',nv:null},{av:'AV23ContratoServicosCusto_CstUntPrdNrmTitleFilterData',fld:'vCONTRATOSERVICOSCUSTO_CSTUNTPRDNRMTITLEFILTERDATA',pic:'',nv:null},{av:'AV27ContratoServicosCusto_CstUntPrdExtTitleFilterData',fld:'vCONTRATOSERVICOSCUSTO_CSTUNTPRDEXTTITLEFILTERDATA',pic:'',nv:null},{av:'edtContratoServicosCusto_UsrPesNom_Titleformat',ctrl:'CONTRATOSERVICOSCUSTO_USRPESNOM',prop:'Titleformat'},{av:'edtContratoServicosCusto_UsrPesNom_Title',ctrl:'CONTRATOSERVICOSCUSTO_USRPESNOM',prop:'Title'},{av:'edtContratoServicosCusto_CstUntPrdNrm_Titleformat',ctrl:'CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM',prop:'Titleformat'},{av:'edtContratoServicosCusto_CstUntPrdNrm_Title',ctrl:'CONTRATOSERVICOSCUSTO_CSTUNTPRDNRM',prop:'Title'},{av:'edtContratoServicosCusto_CstUntPrdExt_Titleformat',ctrl:'CONTRATOSERVICOSCUSTO_CSTUNTPRDEXT',prop:'Titleformat'},{av:'edtContratoServicosCusto_CstUntPrdExt_Title',ctrl:'CONTRATOSERVICOSCUSTO_CSTUNTPRDEXT',prop:'Title'},{av:'edtavUpdate_Visible',ctrl:'vUPDATE',prop:'Visible'},{av:'edtavDelete_Visible',ctrl:'vDELETE',prop:'Visible'},{av:'imgInsert_Visible',ctrl:'INSERT',prop:'Visible'}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Ddo_contratoservicoscusto_usrpesnom_Activeeventkey = "";
         Ddo_contratoservicoscusto_usrpesnom_Filteredtext_get = "";
         Ddo_contratoservicoscusto_usrpesnom_Selectedvalue_get = "";
         Ddo_contratoservicoscusto_cstuntprdnrm_Activeeventkey = "";
         Ddo_contratoservicoscusto_cstuntprdnrm_Filteredtext_get = "";
         Ddo_contratoservicoscusto_cstuntprdnrm_Filteredtextto_get = "";
         Ddo_contratoservicoscusto_cstuntprdext_Activeeventkey = "";
         Ddo_contratoservicoscusto_cstuntprdext_Filteredtext_get = "";
         Ddo_contratoservicoscusto_cstuntprdext_Filteredtextto_get = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         AV20TFContratoServicosCusto_UsrPesNom = "";
         AV21TFContratoServicosCusto_UsrPesNom_Sel = "";
         AV22ddo_ContratoServicosCusto_UsrPesNomTitleControlIdToReplace = "";
         AV26ddo_ContratoServicosCusto_CstUntPrdNrmTitleControlIdToReplace = "";
         AV30ddo_ContratoServicosCusto_CstUntPrdExtTitleControlIdToReplace = "";
         AV36Pgmname = "";
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV31DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV19ContratoServicosCusto_UsrPesNomTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV23ContratoServicosCusto_CstUntPrdNrmTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV27ContratoServicosCusto_CstUntPrdExtTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         Ddo_contratoservicoscusto_usrpesnom_Filteredtext_set = "";
         Ddo_contratoservicoscusto_usrpesnom_Selectedvalue_set = "";
         Ddo_contratoservicoscusto_usrpesnom_Sortedstatus = "";
         Ddo_contratoservicoscusto_cstuntprdnrm_Filteredtext_set = "";
         Ddo_contratoservicoscusto_cstuntprdnrm_Filteredtextto_set = "";
         Ddo_contratoservicoscusto_cstuntprdnrm_Sortedstatus = "";
         Ddo_contratoservicoscusto_cstuntprdext_Filteredtext_set = "";
         Ddo_contratoservicoscusto_cstuntprdext_Filteredtextto_set = "";
         Ddo_contratoservicoscusto_cstuntprdext_Sortedstatus = "";
         GX_FocusControl = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV15Update = "";
         AV34Update_GXI = "";
         AV17Delete = "";
         AV35Delete_GXI = "";
         A1477ContratoServicosCusto_UsrPesNom = "";
         GridContainer = new GXWebGrid( context);
         scmdbuf = "";
         lV20TFContratoServicosCusto_UsrPesNom = "";
         H00KZ2_A1472ContratoServicosCusto_UsuarioCod = new int[1] ;
         H00KZ2_A1476ContratoServicosCusto_UsrPesCod = new int[1] ;
         H00KZ2_n1476ContratoServicosCusto_UsrPesCod = new bool[] {false} ;
         H00KZ2_A1471ContratoServicosCusto_CntSrvCod = new int[1] ;
         H00KZ2_A1475ContratoServicosCusto_CstUntPrdExt = new decimal[1] ;
         H00KZ2_n1475ContratoServicosCusto_CstUntPrdExt = new bool[] {false} ;
         H00KZ2_A1474ContratoServicosCusto_CstUntPrdNrm = new decimal[1] ;
         H00KZ2_A1477ContratoServicosCusto_UsrPesNom = new String[] {""} ;
         H00KZ2_n1477ContratoServicosCusto_UsrPesNom = new bool[] {false} ;
         H00KZ2_A1473ContratoServicosCusto_Codigo = new int[1] ;
         H00KZ3_AGRID_nRecordCount = new long[1] ;
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         GridRow = new GXWebRow();
         AV18Session = context.GetSession();
         AV11GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8HTTPRequest = new GxHttpRequest( context);
         AV10TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         imgInsert_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlAV7ContratoServicosCusto_CntSrvCod = "";
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.contratoservicoscustowc__default(),
            new Object[][] {
                new Object[] {
               H00KZ2_A1472ContratoServicosCusto_UsuarioCod, H00KZ2_A1476ContratoServicosCusto_UsrPesCod, H00KZ2_n1476ContratoServicosCusto_UsrPesCod, H00KZ2_A1471ContratoServicosCusto_CntSrvCod, H00KZ2_A1475ContratoServicosCusto_CstUntPrdExt, H00KZ2_n1475ContratoServicosCusto_CstUntPrdExt, H00KZ2_A1474ContratoServicosCusto_CstUntPrdNrm, H00KZ2_A1477ContratoServicosCusto_UsrPesNom, H00KZ2_n1477ContratoServicosCusto_UsrPesNom, H00KZ2_A1473ContratoServicosCusto_Codigo
               }
               , new Object[] {
               H00KZ3_AGRID_nRecordCount
               }
            }
         );
         AV36Pgmname = "ContratoServicosCustoWC";
         /* GeneXus formulas. */
         AV36Pgmname = "ContratoServicosCustoWC";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short nRC_GXsfl_5 ;
      private short nGXsfl_5_idx=1 ;
      private short AV13OrderedBy ;
      private short initialized ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDraw ;
      private short nDoneStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_5_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short edtContratoServicosCusto_UsrPesNom_Titleformat ;
      private short edtContratoServicosCusto_CstUntPrdNrm_Titleformat ;
      private short edtContratoServicosCusto_CstUntPrdExt_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int AV7ContratoServicosCusto_CntSrvCod ;
      private int wcpOAV7ContratoServicosCusto_CntSrvCod ;
      private int subGrid_Rows ;
      private int Ddo_contratoservicoscusto_usrpesnom_Datalistupdateminimumcharacters ;
      private int A1471ContratoServicosCusto_CntSrvCod ;
      private int edtContratoServicosCusto_CntSrvCod_Visible ;
      private int edtavOrderedby_Visible ;
      private int edtavOrdereddsc_Visible ;
      private int edtavTfcontratoservicoscusto_usrpesnom_Visible ;
      private int edtavTfcontratoservicoscusto_usrpesnom_sel_Visible ;
      private int edtavTfcontratoservicoscusto_cstuntprdnrm_Visible ;
      private int edtavTfcontratoservicoscusto_cstuntprdnrm_to_Visible ;
      private int edtavTfcontratoservicoscusto_cstuntprdext_Visible ;
      private int edtavTfcontratoservicoscusto_cstuntprdext_to_Visible ;
      private int edtavDdo_contratoservicoscusto_usrpesnomtitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratoservicoscusto_cstuntprdnrmtitlecontrolidtoreplace_Visible ;
      private int edtavDdo_contratoservicoscusto_cstuntprdexttitlecontrolidtoreplace_Visible ;
      private int A1473ContratoServicosCusto_Codigo ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int A1472ContratoServicosCusto_UsuarioCod ;
      private int A1476ContratoServicosCusto_UsrPesCod ;
      private int edtavUpdate_Visible ;
      private int edtavDelete_Visible ;
      private int imgInsert_Visible ;
      private int AV37GXV1 ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private int edtavUpdate_Enabled ;
      private int edtavDelete_Enabled ;
      private long GRID_nFirstRecordOnPage ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private decimal AV24TFContratoServicosCusto_CstUntPrdNrm ;
      private decimal AV25TFContratoServicosCusto_CstUntPrdNrm_To ;
      private decimal AV28TFContratoServicosCusto_CstUntPrdExt ;
      private decimal AV29TFContratoServicosCusto_CstUntPrdExt_To ;
      private decimal A1474ContratoServicosCusto_CstUntPrdNrm ;
      private decimal A1475ContratoServicosCusto_CstUntPrdExt ;
      private String Ddo_contratoservicoscusto_usrpesnom_Activeeventkey ;
      private String Ddo_contratoservicoscusto_usrpesnom_Filteredtext_get ;
      private String Ddo_contratoservicoscusto_usrpesnom_Selectedvalue_get ;
      private String Ddo_contratoservicoscusto_cstuntprdnrm_Activeeventkey ;
      private String Ddo_contratoservicoscusto_cstuntprdnrm_Filteredtext_get ;
      private String Ddo_contratoservicoscusto_cstuntprdnrm_Filteredtextto_get ;
      private String Ddo_contratoservicoscusto_cstuntprdext_Activeeventkey ;
      private String Ddo_contratoservicoscusto_cstuntprdext_Filteredtext_get ;
      private String Ddo_contratoservicoscusto_cstuntprdext_Filteredtextto_get ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String sGXsfl_5_idx="0001" ;
      private String AV20TFContratoServicosCusto_UsrPesNom ;
      private String AV21TFContratoServicosCusto_UsrPesNom_Sel ;
      private String AV36Pgmname ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Ddo_contratoservicoscusto_usrpesnom_Caption ;
      private String Ddo_contratoservicoscusto_usrpesnom_Tooltip ;
      private String Ddo_contratoservicoscusto_usrpesnom_Cls ;
      private String Ddo_contratoservicoscusto_usrpesnom_Filteredtext_set ;
      private String Ddo_contratoservicoscusto_usrpesnom_Selectedvalue_set ;
      private String Ddo_contratoservicoscusto_usrpesnom_Dropdownoptionstype ;
      private String Ddo_contratoservicoscusto_usrpesnom_Titlecontrolidtoreplace ;
      private String Ddo_contratoservicoscusto_usrpesnom_Sortedstatus ;
      private String Ddo_contratoservicoscusto_usrpesnom_Filtertype ;
      private String Ddo_contratoservicoscusto_usrpesnom_Datalisttype ;
      private String Ddo_contratoservicoscusto_usrpesnom_Datalistproc ;
      private String Ddo_contratoservicoscusto_usrpesnom_Sortasc ;
      private String Ddo_contratoservicoscusto_usrpesnom_Sortdsc ;
      private String Ddo_contratoservicoscusto_usrpesnom_Loadingdata ;
      private String Ddo_contratoservicoscusto_usrpesnom_Cleanfilter ;
      private String Ddo_contratoservicoscusto_usrpesnom_Noresultsfound ;
      private String Ddo_contratoservicoscusto_usrpesnom_Searchbuttontext ;
      private String Ddo_contratoservicoscusto_cstuntprdnrm_Caption ;
      private String Ddo_contratoservicoscusto_cstuntprdnrm_Tooltip ;
      private String Ddo_contratoservicoscusto_cstuntprdnrm_Cls ;
      private String Ddo_contratoservicoscusto_cstuntprdnrm_Filteredtext_set ;
      private String Ddo_contratoservicoscusto_cstuntprdnrm_Filteredtextto_set ;
      private String Ddo_contratoservicoscusto_cstuntprdnrm_Dropdownoptionstype ;
      private String Ddo_contratoservicoscusto_cstuntprdnrm_Titlecontrolidtoreplace ;
      private String Ddo_contratoservicoscusto_cstuntprdnrm_Sortedstatus ;
      private String Ddo_contratoservicoscusto_cstuntprdnrm_Filtertype ;
      private String Ddo_contratoservicoscusto_cstuntprdnrm_Sortasc ;
      private String Ddo_contratoservicoscusto_cstuntprdnrm_Sortdsc ;
      private String Ddo_contratoservicoscusto_cstuntprdnrm_Cleanfilter ;
      private String Ddo_contratoservicoscusto_cstuntprdnrm_Rangefilterfrom ;
      private String Ddo_contratoservicoscusto_cstuntprdnrm_Rangefilterto ;
      private String Ddo_contratoservicoscusto_cstuntprdnrm_Searchbuttontext ;
      private String Ddo_contratoservicoscusto_cstuntprdext_Caption ;
      private String Ddo_contratoservicoscusto_cstuntprdext_Tooltip ;
      private String Ddo_contratoservicoscusto_cstuntprdext_Cls ;
      private String Ddo_contratoservicoscusto_cstuntprdext_Filteredtext_set ;
      private String Ddo_contratoservicoscusto_cstuntprdext_Filteredtextto_set ;
      private String Ddo_contratoservicoscusto_cstuntprdext_Dropdownoptionstype ;
      private String Ddo_contratoservicoscusto_cstuntprdext_Titlecontrolidtoreplace ;
      private String Ddo_contratoservicoscusto_cstuntprdext_Sortedstatus ;
      private String Ddo_contratoservicoscusto_cstuntprdext_Filtertype ;
      private String Ddo_contratoservicoscusto_cstuntprdext_Sortasc ;
      private String Ddo_contratoservicoscusto_cstuntprdext_Sortdsc ;
      private String Ddo_contratoservicoscusto_cstuntprdext_Cleanfilter ;
      private String Ddo_contratoservicoscusto_cstuntprdext_Rangefilterfrom ;
      private String Ddo_contratoservicoscusto_cstuntprdext_Rangefilterto ;
      private String Ddo_contratoservicoscusto_cstuntprdext_Searchbuttontext ;
      private String GX_FocusControl ;
      private String edtContratoServicosCusto_CntSrvCod_Internalname ;
      private String edtContratoServicosCusto_CntSrvCod_Jsonclick ;
      private String TempTags ;
      private String edtavOrderedby_Internalname ;
      private String edtavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Internalname ;
      private String edtavOrdereddsc_Jsonclick ;
      private String edtavTfcontratoservicoscusto_usrpesnom_Internalname ;
      private String edtavTfcontratoservicoscusto_usrpesnom_Jsonclick ;
      private String edtavTfcontratoservicoscusto_usrpesnom_sel_Internalname ;
      private String edtavTfcontratoservicoscusto_usrpesnom_sel_Jsonclick ;
      private String edtavTfcontratoservicoscusto_cstuntprdnrm_Internalname ;
      private String edtavTfcontratoservicoscusto_cstuntprdnrm_Jsonclick ;
      private String edtavTfcontratoservicoscusto_cstuntprdnrm_to_Internalname ;
      private String edtavTfcontratoservicoscusto_cstuntprdnrm_to_Jsonclick ;
      private String edtavTfcontratoservicoscusto_cstuntprdext_Internalname ;
      private String edtavTfcontratoservicoscusto_cstuntprdext_Jsonclick ;
      private String edtavTfcontratoservicoscusto_cstuntprdext_to_Internalname ;
      private String edtavTfcontratoservicoscusto_cstuntprdext_to_Jsonclick ;
      private String ClassString ;
      private String StyleString ;
      private String edtavDdo_contratoservicoscusto_usrpesnomtitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratoservicoscusto_cstuntprdnrmtitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_contratoservicoscusto_cstuntprdexttitlecontrolidtoreplace_Internalname ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavUpdate_Internalname ;
      private String edtavDelete_Internalname ;
      private String edtContratoServicosCusto_Codigo_Internalname ;
      private String A1477ContratoServicosCusto_UsrPesNom ;
      private String edtContratoServicosCusto_UsrPesNom_Internalname ;
      private String edtContratoServicosCusto_CstUntPrdNrm_Internalname ;
      private String edtContratoServicosCusto_CstUntPrdExt_Internalname ;
      private String scmdbuf ;
      private String lV20TFContratoServicosCusto_UsrPesNom ;
      private String subGrid_Internalname ;
      private String Ddo_contratoservicoscusto_usrpesnom_Internalname ;
      private String Ddo_contratoservicoscusto_cstuntprdnrm_Internalname ;
      private String Ddo_contratoservicoscusto_cstuntprdext_Internalname ;
      private String edtContratoServicosCusto_UsrPesNom_Title ;
      private String edtContratoServicosCusto_CstUntPrdNrm_Title ;
      private String edtContratoServicosCusto_CstUntPrdExt_Title ;
      private String imgInsert_Internalname ;
      private String edtavUpdate_Tooltiptext ;
      private String edtavDelete_Tooltiptext ;
      private String sStyleString ;
      private String tblUnnamedtable1_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String imgInsert_Jsonclick ;
      private String sCtrlAV7ContratoServicosCusto_CntSrvCod ;
      private String sGXsfl_5_fel_idx="0001" ;
      private String edtavUpdate_Jsonclick ;
      private String edtavDelete_Jsonclick ;
      private String ROClassString ;
      private String edtContratoServicosCusto_Codigo_Jsonclick ;
      private String edtContratoServicosCusto_UsrPesNom_Jsonclick ;
      private String edtContratoServicosCusto_CstUntPrdNrm_Jsonclick ;
      private String edtContratoServicosCusto_CstUntPrdExt_Jsonclick ;
      private String Workwithplusutilities1_Internalname ;
      private bool entryPointCalled ;
      private bool AV14OrderedDsc ;
      private bool toggleJsOutput ;
      private bool Ddo_contratoservicoscusto_usrpesnom_Includesortasc ;
      private bool Ddo_contratoservicoscusto_usrpesnom_Includesortdsc ;
      private bool Ddo_contratoservicoscusto_usrpesnom_Includefilter ;
      private bool Ddo_contratoservicoscusto_usrpesnom_Filterisrange ;
      private bool Ddo_contratoservicoscusto_usrpesnom_Includedatalist ;
      private bool Ddo_contratoservicoscusto_cstuntprdnrm_Includesortasc ;
      private bool Ddo_contratoservicoscusto_cstuntprdnrm_Includesortdsc ;
      private bool Ddo_contratoservicoscusto_cstuntprdnrm_Includefilter ;
      private bool Ddo_contratoservicoscusto_cstuntprdnrm_Filterisrange ;
      private bool Ddo_contratoservicoscusto_cstuntprdnrm_Includedatalist ;
      private bool Ddo_contratoservicoscusto_cstuntprdext_Includesortasc ;
      private bool Ddo_contratoservicoscusto_cstuntprdext_Includesortdsc ;
      private bool Ddo_contratoservicoscusto_cstuntprdext_Includefilter ;
      private bool Ddo_contratoservicoscusto_cstuntprdext_Filterisrange ;
      private bool Ddo_contratoservicoscusto_cstuntprdext_Includedatalist ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n1477ContratoServicosCusto_UsrPesNom ;
      private bool n1475ContratoServicosCusto_CstUntPrdExt ;
      private bool n1476ContratoServicosCusto_UsrPesCod ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV15Update_IsBlob ;
      private bool AV17Delete_IsBlob ;
      private String AV22ddo_ContratoServicosCusto_UsrPesNomTitleControlIdToReplace ;
      private String AV26ddo_ContratoServicosCusto_CstUntPrdNrmTitleControlIdToReplace ;
      private String AV30ddo_ContratoServicosCusto_CstUntPrdExtTitleControlIdToReplace ;
      private String AV34Update_GXI ;
      private String AV35Delete_GXI ;
      private String AV15Update ;
      private String AV17Delete ;
      private IGxSession AV18Session ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] H00KZ2_A1472ContratoServicosCusto_UsuarioCod ;
      private int[] H00KZ2_A1476ContratoServicosCusto_UsrPesCod ;
      private bool[] H00KZ2_n1476ContratoServicosCusto_UsrPesCod ;
      private int[] H00KZ2_A1471ContratoServicosCusto_CntSrvCod ;
      private decimal[] H00KZ2_A1475ContratoServicosCusto_CstUntPrdExt ;
      private bool[] H00KZ2_n1475ContratoServicosCusto_CstUntPrdExt ;
      private decimal[] H00KZ2_A1474ContratoServicosCusto_CstUntPrdNrm ;
      private String[] H00KZ2_A1477ContratoServicosCusto_UsrPesNom ;
      private bool[] H00KZ2_n1477ContratoServicosCusto_UsrPesNom ;
      private int[] H00KZ2_A1473ContratoServicosCusto_Codigo ;
      private long[] H00KZ3_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV8HTTPRequest ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV19ContratoServicosCusto_UsrPesNomTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV23ContratoServicosCusto_CstUntPrdNrmTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV27ContratoServicosCusto_CstUntPrdExtTitleFilterData ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV10TrnContextAtt ;
      private wwpbaseobjects.SdtWWPGridState AV11GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV12GridStateFilterValue ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV31DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 ;
   }

   public class contratoservicoscustowc__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00KZ2( IGxContext context ,
                                             String AV21TFContratoServicosCusto_UsrPesNom_Sel ,
                                             String AV20TFContratoServicosCusto_UsrPesNom ,
                                             decimal AV24TFContratoServicosCusto_CstUntPrdNrm ,
                                             decimal AV25TFContratoServicosCusto_CstUntPrdNrm_To ,
                                             decimal AV28TFContratoServicosCusto_CstUntPrdExt ,
                                             decimal AV29TFContratoServicosCusto_CstUntPrdExt_To ,
                                             String A1477ContratoServicosCusto_UsrPesNom ,
                                             decimal A1474ContratoServicosCusto_CstUntPrdNrm ,
                                             decimal A1475ContratoServicosCusto_CstUntPrdExt ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc ,
                                             int A1471ContratoServicosCusto_CntSrvCod ,
                                             int AV7ContratoServicosCusto_CntSrvCod )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [12] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " T1.[ContratoServicosCusto_UsuarioCod] AS ContratoServicosCusto_UsuarioCod, T2.[Usuario_PessoaCod] AS ContratoServicosCusto_UsrPesCod, T1.[ContratoServicosCusto_CntSrvCod], T1.[ContratoServicosCusto_CstUntPrdExt], T1.[ContratoServicosCusto_CstUntPrdNrm], T3.[Pessoa_Nome] AS ContratoServicosCusto_UsrPesNom, T1.[ContratoServicosCusto_Codigo]";
         sFromString = " FROM (([ContratoServicosCusto] T1 WITH (NOLOCK) INNER JOIN [Usuario] T2 WITH (NOLOCK) ON T2.[Usuario_Codigo] = T1.[ContratoServicosCusto_UsuarioCod]) LEFT JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Usuario_PessoaCod])";
         sOrderString = "";
         sWhereString = sWhereString + " WHERE (T1.[ContratoServicosCusto_CntSrvCod] = @AV7ContratoServicosCusto_CntSrvCod)";
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV21TFContratoServicosCusto_UsrPesNom_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV20TFContratoServicosCusto_UsrPesNom)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV20TFContratoServicosCusto_UsrPesNom)";
         }
         else
         {
            GXv_int2[1] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV21TFContratoServicosCusto_UsrPesNom_Sel)) )
         {
            sWhereString = sWhereString + " and (T3.[Pessoa_Nome] = @AV21TFContratoServicosCusto_UsrPesNom_Sel)";
         }
         else
         {
            GXv_int2[2] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV24TFContratoServicosCusto_CstUntPrdNrm) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosCusto_CstUntPrdNrm] >= @AV24TFContratoServicosCusto_CstUntPrdNrm)";
         }
         else
         {
            GXv_int2[3] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV25TFContratoServicosCusto_CstUntPrdNrm_To) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosCusto_CstUntPrdNrm] <= @AV25TFContratoServicosCusto_CstUntPrdNrm_To)";
         }
         else
         {
            GXv_int2[4] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV28TFContratoServicosCusto_CstUntPrdExt) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosCusto_CstUntPrdExt] >= @AV28TFContratoServicosCusto_CstUntPrdExt)";
         }
         else
         {
            GXv_int2[5] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV29TFContratoServicosCusto_CstUntPrdExt_To) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosCusto_CstUntPrdExt] <= @AV29TFContratoServicosCusto_CstUntPrdExt_To)";
         }
         else
         {
            GXv_int2[6] = 1;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoServicosCusto_CntSrvCod], T3.[Pessoa_Nome]";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoServicosCusto_CntSrvCod] DESC, T3.[Pessoa_Nome] DESC";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoServicosCusto_CntSrvCod], T1.[ContratoServicosCusto_CstUntPrdNrm]";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoServicosCusto_CntSrvCod] DESC, T1.[ContratoServicosCusto_CstUntPrdNrm] DESC";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoServicosCusto_CntSrvCod], T1.[ContratoServicosCusto_CstUntPrdExt]";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoServicosCusto_CntSrvCod] DESC, T1.[ContratoServicosCusto_CstUntPrdExt] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY T1.[ContratoServicosCusto_Codigo]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      protected Object[] conditional_H00KZ3( IGxContext context ,
                                             String AV21TFContratoServicosCusto_UsrPesNom_Sel ,
                                             String AV20TFContratoServicosCusto_UsrPesNom ,
                                             decimal AV24TFContratoServicosCusto_CstUntPrdNrm ,
                                             decimal AV25TFContratoServicosCusto_CstUntPrdNrm_To ,
                                             decimal AV28TFContratoServicosCusto_CstUntPrdExt ,
                                             decimal AV29TFContratoServicosCusto_CstUntPrdExt_To ,
                                             String A1477ContratoServicosCusto_UsrPesNom ,
                                             decimal A1474ContratoServicosCusto_CstUntPrdNrm ,
                                             decimal A1475ContratoServicosCusto_CstUntPrdExt ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc ,
                                             int A1471ContratoServicosCusto_CntSrvCod ,
                                             int AV7ContratoServicosCusto_CntSrvCod )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [7] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM (([ContratoServicosCusto] T1 WITH (NOLOCK) INNER JOIN [Usuario] T2 WITH (NOLOCK) ON T2.[Usuario_Codigo] = T1.[ContratoServicosCusto_UsuarioCod]) LEFT JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Usuario_PessoaCod])";
         scmdbuf = scmdbuf + " WHERE (T1.[ContratoServicosCusto_CntSrvCod] = @AV7ContratoServicosCusto_CntSrvCod)";
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV21TFContratoServicosCusto_UsrPesNom_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV20TFContratoServicosCusto_UsrPesNom)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV20TFContratoServicosCusto_UsrPesNom)";
         }
         else
         {
            GXv_int4[1] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV21TFContratoServicosCusto_UsrPesNom_Sel)) )
         {
            sWhereString = sWhereString + " and (T3.[Pessoa_Nome] = @AV21TFContratoServicosCusto_UsrPesNom_Sel)";
         }
         else
         {
            GXv_int4[2] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV24TFContratoServicosCusto_CstUntPrdNrm) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosCusto_CstUntPrdNrm] >= @AV24TFContratoServicosCusto_CstUntPrdNrm)";
         }
         else
         {
            GXv_int4[3] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV25TFContratoServicosCusto_CstUntPrdNrm_To) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosCusto_CstUntPrdNrm] <= @AV25TFContratoServicosCusto_CstUntPrdNrm_To)";
         }
         else
         {
            GXv_int4[4] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV28TFContratoServicosCusto_CstUntPrdExt) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosCusto_CstUntPrdExt] >= @AV28TFContratoServicosCusto_CstUntPrdExt)";
         }
         else
         {
            GXv_int4[5] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV29TFContratoServicosCusto_CstUntPrdExt_To) )
         {
            sWhereString = sWhereString + " and (T1.[ContratoServicosCusto_CstUntPrdExt] <= @AV29TFContratoServicosCusto_CstUntPrdExt_To)";
         }
         else
         {
            GXv_int4[6] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H00KZ2(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (decimal)dynConstraints[2] , (decimal)dynConstraints[3] , (decimal)dynConstraints[4] , (decimal)dynConstraints[5] , (String)dynConstraints[6] , (decimal)dynConstraints[7] , (decimal)dynConstraints[8] , (short)dynConstraints[9] , (bool)dynConstraints[10] , (int)dynConstraints[11] , (int)dynConstraints[12] );
               case 1 :
                     return conditional_H00KZ3(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (decimal)dynConstraints[2] , (decimal)dynConstraints[3] , (decimal)dynConstraints[4] , (decimal)dynConstraints[5] , (String)dynConstraints[6] , (decimal)dynConstraints[7] , (decimal)dynConstraints[8] , (short)dynConstraints[9] , (bool)dynConstraints[10] , (int)dynConstraints[11] , (int)dynConstraints[12] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00KZ2 ;
          prmH00KZ2 = new Object[] {
          new Object[] {"@AV7ContratoServicosCusto_CntSrvCod",SqlDbType.Int,6,0} ,
          new Object[] {"@lV20TFContratoServicosCusto_UsrPesNom",SqlDbType.Char,100,0} ,
          new Object[] {"@AV21TFContratoServicosCusto_UsrPesNom_Sel",SqlDbType.Char,100,0} ,
          new Object[] {"@AV24TFContratoServicosCusto_CstUntPrdNrm",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV25TFContratoServicosCusto_CstUntPrdNrm_To",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV28TFContratoServicosCusto_CstUntPrdExt",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV29TFContratoServicosCusto_CstUntPrdExt_To",SqlDbType.Decimal,18,5} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH00KZ3 ;
          prmH00KZ3 = new Object[] {
          new Object[] {"@AV7ContratoServicosCusto_CntSrvCod",SqlDbType.Int,6,0} ,
          new Object[] {"@lV20TFContratoServicosCusto_UsrPesNom",SqlDbType.Char,100,0} ,
          new Object[] {"@AV21TFContratoServicosCusto_UsrPesNom_Sel",SqlDbType.Char,100,0} ,
          new Object[] {"@AV24TFContratoServicosCusto_CstUntPrdNrm",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV25TFContratoServicosCusto_CstUntPrdNrm_To",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV28TFContratoServicosCusto_CstUntPrdExt",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV29TFContratoServicosCusto_CstUntPrdExt_To",SqlDbType.Decimal,18,5}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00KZ2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00KZ2,11,0,true,false )
             ,new CursorDef("H00KZ3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00KZ3,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((decimal[]) buf[4])[0] = rslt.getDecimal(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((decimal[]) buf[6])[0] = rslt.getDecimal(5) ;
                ((String[]) buf[7])[0] = rslt.getString(6, 100) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((int[]) buf[9])[0] = rslt.getInt(7) ;
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[12]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[15]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[16]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[17]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[18]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[19]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[20]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[21]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[22]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[23]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[7]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[8]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[9]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[10]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[11]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[12]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[13]);
                }
                return;
       }
    }

 }

}
