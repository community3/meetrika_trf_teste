/*
               File: PRC_DiasParaEntrega
        Description: Dias Para Entrega
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:51:45.47
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_diasparaentrega : GXProcedure
   {
      public prc_diasparaentrega( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_diasparaentrega( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_ContratoServicos_Codigo ,
                           decimal aP1_Unidades ,
                           short aP2_DiasComplexidade ,
                           out short aP3_Dias )
      {
         this.A160ContratoServicos_Codigo = aP0_ContratoServicos_Codigo;
         this.AV11Unidades = aP1_Unidades;
         this.AV20DiasComplexidade = aP2_DiasComplexidade;
         this.AV10Dias = 0 ;
         initialize();
         executePrivate();
         aP0_ContratoServicos_Codigo=this.A160ContratoServicos_Codigo;
         aP3_Dias=this.AV10Dias;
      }

      public short executeUdp( ref int aP0_ContratoServicos_Codigo ,
                               decimal aP1_Unidades ,
                               short aP2_DiasComplexidade )
      {
         this.A160ContratoServicos_Codigo = aP0_ContratoServicos_Codigo;
         this.AV11Unidades = aP1_Unidades;
         this.AV20DiasComplexidade = aP2_DiasComplexidade;
         this.AV10Dias = 0 ;
         initialize();
         executePrivate();
         aP0_ContratoServicos_Codigo=this.A160ContratoServicos_Codigo;
         aP3_Dias=this.AV10Dias;
         return AV10Dias ;
      }

      public void executeSubmit( ref int aP0_ContratoServicos_Codigo ,
                                 decimal aP1_Unidades ,
                                 short aP2_DiasComplexidade ,
                                 out short aP3_Dias )
      {
         prc_diasparaentrega objprc_diasparaentrega;
         objprc_diasparaentrega = new prc_diasparaentrega();
         objprc_diasparaentrega.A160ContratoServicos_Codigo = aP0_ContratoServicos_Codigo;
         objprc_diasparaentrega.AV11Unidades = aP1_Unidades;
         objprc_diasparaentrega.AV20DiasComplexidade = aP2_DiasComplexidade;
         objprc_diasparaentrega.AV10Dias = 0 ;
         objprc_diasparaentrega.context.SetSubmitInitialConfig(context);
         objprc_diasparaentrega.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_diasparaentrega);
         aP0_ContratoServicos_Codigo=this.A160ContratoServicos_Codigo;
         aP3_Dias=this.AV10Dias;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_diasparaentrega)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P007P2 */
         pr_default.execute(0, new Object[] {A160ContratoServicos_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A1152ContratoServicos_PrazoAnalise = P007P2_A1152ContratoServicos_PrazoAnalise[0];
            n1152ContratoServicos_PrazoAnalise = P007P2_n1152ContratoServicos_PrazoAnalise[0];
            AV10Dias = A1152ContratoServicos_PrazoAnalise;
            GXt_int1 = AV10Dias;
            new prc_diasentregadotpprz(context ).execute( ref  A160ContratoServicos_Codigo,  AV11Unidades,  AV20DiasComplexidade, out  GXt_int1) ;
            AV10Dias = (short)(AV10Dias+(GXt_int1));
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(0);
         if ( (0==AV10Dias) )
         {
            AV10Dias = 2;
         }
         this.cleanup();
         if (true) return;
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P007P2_A160ContratoServicos_Codigo = new int[1] ;
         P007P2_A1152ContratoServicos_PrazoAnalise = new short[1] ;
         P007P2_n1152ContratoServicos_PrazoAnalise = new bool[] {false} ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_diasparaentrega__default(),
            new Object[][] {
                new Object[] {
               P007P2_A160ContratoServicos_Codigo, P007P2_A1152ContratoServicos_PrazoAnalise, P007P2_n1152ContratoServicos_PrazoAnalise
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV20DiasComplexidade ;
      private short AV10Dias ;
      private short A1152ContratoServicos_PrazoAnalise ;
      private short GXt_int1 ;
      private int A160ContratoServicos_Codigo ;
      private decimal AV11Unidades ;
      private String scmdbuf ;
      private bool n1152ContratoServicos_PrazoAnalise ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_ContratoServicos_Codigo ;
      private IDataStoreProvider pr_default ;
      private int[] P007P2_A160ContratoServicos_Codigo ;
      private short[] P007P2_A1152ContratoServicos_PrazoAnalise ;
      private bool[] P007P2_n1152ContratoServicos_PrazoAnalise ;
      private short aP3_Dias ;
   }

   public class prc_diasparaentrega__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP007P2 ;
          prmP007P2 = new Object[] {
          new Object[] {"@ContratoServicos_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P007P2", "SELECT TOP 1 [ContratoServicos_Codigo], [ContratoServicos_PrazoAnalise] FROM [ContratoServicos] WITH (NOLOCK) WHERE [ContratoServicos_Codigo] = @ContratoServicos_Codigo ORDER BY [ContratoServicos_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP007P2,1,0,true,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
