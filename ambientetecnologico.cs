/*
               File: AmbienteTecnologico
        Description: Ambiente Tecnologico
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/4/2020 8:39:0.81
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class ambientetecnologico : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_12") == 0 )
         {
            A728AmbienteTecnologico_AreaTrabalhoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A728AmbienteTecnologico_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A728AmbienteTecnologico_AreaTrabalhoCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_12( A728AmbienteTecnologico_AreaTrabalhoCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
         {
            Gx_mode = gxfirstwebparm;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
            {
               AV12AmbienteTecnologico_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12AmbienteTecnologico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12AmbienteTecnologico_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vAMBIENTETECNOLOGICO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV12AmbienteTecnologico_Codigo), "ZZZZZ9")));
            }
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         chkAmbienteTecnologico_Ativo.Name = "AMBIENTETECNOLOGICO_ATIVO";
         chkAmbienteTecnologico_Ativo.WebTags = "";
         chkAmbienteTecnologico_Ativo.Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkAmbienteTecnologico_Ativo_Internalname, "TitleCaption", chkAmbienteTecnologico_Ativo.Caption);
         chkAmbienteTecnologico_Ativo.CheckedValue = "false";
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Ambiente Tecnologico", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = edtAmbienteTecnologico_Descricao_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public ambientetecnologico( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public ambientetecnologico( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_Gx_mode ,
                           int aP1_AmbienteTecnologico_Codigo )
      {
         this.Gx_mode = aP0_Gx_mode;
         this.AV12AmbienteTecnologico_Codigo = aP1_AmbienteTecnologico_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         chkAmbienteTecnologico_Ativo = new GXCheckbox();
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            wb_table1_2_1N62( true) ;
         }
         return  ;
      }

      protected void wb_table1_2_1N62e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtAmbienteTecnologico_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A351AmbienteTecnologico_Codigo), 6, 0, ",", "")), ((edtAmbienteTecnologico_Codigo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A351AmbienteTecnologico_Codigo), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A351AmbienteTecnologico_Codigo), "ZZZZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtAmbienteTecnologico_Codigo_Jsonclick, 0, "Attribute", "", "", "", edtAmbienteTecnologico_Codigo_Visible, edtAmbienteTecnologico_Codigo_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_AmbienteTecnologico.htm");
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void wb_table1_2_1N62( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_5_1N62( true) ;
         }
         return  ;
      }

      protected void wb_table2_5_1N62e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_56_1N62( true) ;
         }
         return  ;
      }

      protected void wb_table3_56_1N62e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_1N62e( true) ;
         }
         else
         {
            wb_table1_2_1N62e( false) ;
         }
      }

      protected void wb_table3_56_1N62( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 59,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_enter_Internalname, "", "Confirmar", bttBtn_trn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_trn_enter_Visible, bttBtn_trn_enter_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_AmbienteTecnologico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 61,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_cancel_Internalname, "", "Fechar", bttBtn_trn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, bttBtn_trn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_AmbienteTecnologico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 63,'',false,'',0)\"";
            ClassString = "btn btn-danger";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_delete_Internalname, "", "Eliminar", bttBtn_trn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_trn_delete_Visible, bttBtn_trn_delete_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_AmbienteTecnologico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_56_1N62e( true) ;
         }
         else
         {
            wb_table3_56_1N62e( false) ;
         }
      }

      protected void wb_table2_5_1N62( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "TableContent", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"Body"+"\" style=\"display:none;\">") ;
            wb_table4_13_1N62( true) ;
         }
         return  ;
      }

      protected void wb_table4_13_1N62e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_1N62e( true) ;
         }
         else
         {
            wb_table2_5_1N62e( false) ;
         }
      }

      protected void wb_table4_13_1N62( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "TableData", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockambientetecnologico_descricao_Internalname, "Nome", "", "", lblTextblockambientetecnologico_descricao_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_AmbienteTecnologico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='RequiredDataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtAmbienteTecnologico_Descricao_Internalname, A352AmbienteTecnologico_Descricao, StringUtil.RTrim( context.localUtil.Format( A352AmbienteTecnologico_Descricao, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,18);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtAmbienteTecnologico_Descricao_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtAmbienteTecnologico_Descricao_Enabled, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Descricao", "left", true, "HLP_AmbienteTecnologico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockambientetecnologico_taxaentregadsnv_Internalname, "Taxa de Entrega (Desenvolvimento)", "", "", lblTextblockambientetecnologico_taxaentregadsnv_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_AmbienteTecnologico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            wb_table5_23_1N62( true) ;
         }
         return  ;
      }

      protected void wb_table5_23_1N62e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockambientetecnologico_taxaentregamlhr_Internalname, "Taxa de Entrega (Melhoria)", "", "", lblTextblockambientetecnologico_taxaentregamlhr_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_AmbienteTecnologico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            wb_table6_33_1N62( true) ;
         }
         return  ;
      }

      protected void wb_table6_33_1N62e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockambientetecnologico_locpf_Internalname, "LOC/PF", "", "", lblTextblockambientetecnologico_locpf_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_AmbienteTecnologico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 43,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtAmbienteTecnologico_LocPF_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A978AmbienteTecnologico_LocPF), 4, 0, ",", "")), ((edtAmbienteTecnologico_LocPF_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A978AmbienteTecnologico_LocPF), "ZZZ9")) : context.localUtil.Format( (decimal)(A978AmbienteTecnologico_LocPF), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,43);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtAmbienteTecnologico_LocPF_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtAmbienteTecnologico_LocPF_Enabled, 0, "text", "", 40, "px", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_AmbienteTecnologico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockambientetecnologico_fatorajuste_Internalname, "Fator de Ajuste", "", "", lblTextblockambientetecnologico_fatorajuste_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_AmbienteTecnologico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 48,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtAmbienteTecnologico_FatorAjuste_Internalname, StringUtil.LTrim( StringUtil.NToC( A1423AmbienteTecnologico_FatorAjuste, 8, 4, ",", "")), ((edtAmbienteTecnologico_FatorAjuste_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( A1423AmbienteTecnologico_FatorAjuste, "ZZ9.9999")) : context.localUtil.Format( A1423AmbienteTecnologico_FatorAjuste, "ZZ9.9999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','4');"+";gx.evt.onblur(this,48);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtAmbienteTecnologico_FatorAjuste_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtAmbienteTecnologico_FatorAjuste_Enabled, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_AmbienteTecnologico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockambientetecnologico_ativo_Internalname, "Ativo", "", "", lblTextblockambientetecnologico_ativo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", lblTextblockambientetecnologico_ativo_Visible, 1, 0, "HLP_AmbienteTecnologico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 53,'',false,'',0)\"";
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkAmbienteTecnologico_Ativo_Internalname, StringUtil.BoolToStr( A353AmbienteTecnologico_Ativo), "", "", chkAmbienteTecnologico_Ativo.Visible, chkAmbienteTecnologico_Ativo.Enabled, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(53, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,53);\"");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_13_1N62e( true) ;
         }
         else
         {
            wb_table4_13_1N62e( false) ;
         }
      }

      protected void wb_table6_33_1N62( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedambientetecnologico_taxaentregamlhr_Internalname, tblTablemergedambientetecnologico_taxaentregamlhr_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 36,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtAmbienteTecnologico_TaxaEntregaMlhr_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A977AmbienteTecnologico_TaxaEntregaMlhr), 4, 0, ",", "")), ((edtAmbienteTecnologico_TaxaEntregaMlhr_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A977AmbienteTecnologico_TaxaEntregaMlhr), "ZZZ9")) : context.localUtil.Format( (decimal)(A977AmbienteTecnologico_TaxaEntregaMlhr), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,36);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtAmbienteTecnologico_TaxaEntregaMlhr_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtAmbienteTecnologico_TaxaEntregaMlhr_Enabled, 0, "text", "", 40, "px", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_AmbienteTecnologico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblAmbientetecnologico_taxaentregamlhr_righttext_Internalname, "H/PF", "", "", lblAmbientetecnologico_taxaentregamlhr_righttext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_AmbienteTecnologico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_33_1N62e( true) ;
         }
         else
         {
            wb_table6_33_1N62e( false) ;
         }
      }

      protected void wb_table5_23_1N62( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedambientetecnologico_taxaentregadsnv_Internalname, tblTablemergedambientetecnologico_taxaentregadsnv_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 26,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtAmbienteTecnologico_TaxaEntregaDsnv_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A976AmbienteTecnologico_TaxaEntregaDsnv), 4, 0, ",", "")), ((edtAmbienteTecnologico_TaxaEntregaDsnv_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A976AmbienteTecnologico_TaxaEntregaDsnv), "ZZZ9")) : context.localUtil.Format( (decimal)(A976AmbienteTecnologico_TaxaEntregaDsnv), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,26);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtAmbienteTecnologico_TaxaEntregaDsnv_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtAmbienteTecnologico_TaxaEntregaDsnv_Enabled, 0, "text", "", 40, "px", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_AmbienteTecnologico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblAmbientetecnologico_taxaentregadsnv_righttext_Internalname, "H/PF", "", "", lblAmbientetecnologico_taxaentregadsnv_righttext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_AmbienteTecnologico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_23_1N62e( true) ;
         }
         else
         {
            wb_table5_23_1N62e( false) ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E111N2 */
         E111N2 ();
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               A352AmbienteTecnologico_Descricao = StringUtil.Upper( cgiGet( edtAmbienteTecnologico_Descricao_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A352AmbienteTecnologico_Descricao", A352AmbienteTecnologico_Descricao);
               if ( ( ( context.localUtil.CToN( cgiGet( edtAmbienteTecnologico_TaxaEntregaDsnv_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtAmbienteTecnologico_TaxaEntregaDsnv_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "AMBIENTETECNOLOGICO_TAXAENTREGADSNV");
                  AnyError = 1;
                  GX_FocusControl = edtAmbienteTecnologico_TaxaEntregaDsnv_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A976AmbienteTecnologico_TaxaEntregaDsnv = 0;
                  n976AmbienteTecnologico_TaxaEntregaDsnv = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A976AmbienteTecnologico_TaxaEntregaDsnv", StringUtil.LTrim( StringUtil.Str( (decimal)(A976AmbienteTecnologico_TaxaEntregaDsnv), 4, 0)));
               }
               else
               {
                  A976AmbienteTecnologico_TaxaEntregaDsnv = (short)(context.localUtil.CToN( cgiGet( edtAmbienteTecnologico_TaxaEntregaDsnv_Internalname), ",", "."));
                  n976AmbienteTecnologico_TaxaEntregaDsnv = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A976AmbienteTecnologico_TaxaEntregaDsnv", StringUtil.LTrim( StringUtil.Str( (decimal)(A976AmbienteTecnologico_TaxaEntregaDsnv), 4, 0)));
               }
               n976AmbienteTecnologico_TaxaEntregaDsnv = ((0==A976AmbienteTecnologico_TaxaEntregaDsnv) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtAmbienteTecnologico_TaxaEntregaMlhr_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtAmbienteTecnologico_TaxaEntregaMlhr_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "AMBIENTETECNOLOGICO_TAXAENTREGAMLHR");
                  AnyError = 1;
                  GX_FocusControl = edtAmbienteTecnologico_TaxaEntregaMlhr_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A977AmbienteTecnologico_TaxaEntregaMlhr = 0;
                  n977AmbienteTecnologico_TaxaEntregaMlhr = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A977AmbienteTecnologico_TaxaEntregaMlhr", StringUtil.LTrim( StringUtil.Str( (decimal)(A977AmbienteTecnologico_TaxaEntregaMlhr), 4, 0)));
               }
               else
               {
                  A977AmbienteTecnologico_TaxaEntregaMlhr = (short)(context.localUtil.CToN( cgiGet( edtAmbienteTecnologico_TaxaEntregaMlhr_Internalname), ",", "."));
                  n977AmbienteTecnologico_TaxaEntregaMlhr = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A977AmbienteTecnologico_TaxaEntregaMlhr", StringUtil.LTrim( StringUtil.Str( (decimal)(A977AmbienteTecnologico_TaxaEntregaMlhr), 4, 0)));
               }
               n977AmbienteTecnologico_TaxaEntregaMlhr = ((0==A977AmbienteTecnologico_TaxaEntregaMlhr) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtAmbienteTecnologico_LocPF_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtAmbienteTecnologico_LocPF_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "AMBIENTETECNOLOGICO_LOCPF");
                  AnyError = 1;
                  GX_FocusControl = edtAmbienteTecnologico_LocPF_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A978AmbienteTecnologico_LocPF = 0;
                  n978AmbienteTecnologico_LocPF = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A978AmbienteTecnologico_LocPF", StringUtil.LTrim( StringUtil.Str( (decimal)(A978AmbienteTecnologico_LocPF), 4, 0)));
               }
               else
               {
                  A978AmbienteTecnologico_LocPF = (short)(context.localUtil.CToN( cgiGet( edtAmbienteTecnologico_LocPF_Internalname), ",", "."));
                  n978AmbienteTecnologico_LocPF = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A978AmbienteTecnologico_LocPF", StringUtil.LTrim( StringUtil.Str( (decimal)(A978AmbienteTecnologico_LocPF), 4, 0)));
               }
               n978AmbienteTecnologico_LocPF = ((0==A978AmbienteTecnologico_LocPF) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtAmbienteTecnologico_FatorAjuste_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtAmbienteTecnologico_FatorAjuste_Internalname), ",", ".") > 999.9999m ) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "AMBIENTETECNOLOGICO_FATORAJUSTE");
                  AnyError = 1;
                  GX_FocusControl = edtAmbienteTecnologico_FatorAjuste_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1423AmbienteTecnologico_FatorAjuste = 0;
                  n1423AmbienteTecnologico_FatorAjuste = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1423AmbienteTecnologico_FatorAjuste", StringUtil.LTrim( StringUtil.Str( A1423AmbienteTecnologico_FatorAjuste, 8, 4)));
               }
               else
               {
                  A1423AmbienteTecnologico_FatorAjuste = context.localUtil.CToN( cgiGet( edtAmbienteTecnologico_FatorAjuste_Internalname), ",", ".");
                  n1423AmbienteTecnologico_FatorAjuste = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1423AmbienteTecnologico_FatorAjuste", StringUtil.LTrim( StringUtil.Str( A1423AmbienteTecnologico_FatorAjuste, 8, 4)));
               }
               n1423AmbienteTecnologico_FatorAjuste = ((Convert.ToDecimal(0)==A1423AmbienteTecnologico_FatorAjuste) ? true : false);
               A353AmbienteTecnologico_Ativo = StringUtil.StrToBool( cgiGet( chkAmbienteTecnologico_Ativo_Internalname));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A353AmbienteTecnologico_Ativo", A353AmbienteTecnologico_Ativo);
               A351AmbienteTecnologico_Codigo = (int)(context.localUtil.CToN( cgiGet( edtAmbienteTecnologico_Codigo_Internalname), ",", "."));
               n351AmbienteTecnologico_Codigo = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A351AmbienteTecnologico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A351AmbienteTecnologico_Codigo), 6, 0)));
               /* Read saved values. */
               Z351AmbienteTecnologico_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z351AmbienteTecnologico_Codigo"), ",", "."));
               Z352AmbienteTecnologico_Descricao = cgiGet( "Z352AmbienteTecnologico_Descricao");
               Z976AmbienteTecnologico_TaxaEntregaDsnv = (short)(context.localUtil.CToN( cgiGet( "Z976AmbienteTecnologico_TaxaEntregaDsnv"), ",", "."));
               n976AmbienteTecnologico_TaxaEntregaDsnv = ((0==A976AmbienteTecnologico_TaxaEntregaDsnv) ? true : false);
               Z977AmbienteTecnologico_TaxaEntregaMlhr = (short)(context.localUtil.CToN( cgiGet( "Z977AmbienteTecnologico_TaxaEntregaMlhr"), ",", "."));
               n977AmbienteTecnologico_TaxaEntregaMlhr = ((0==A977AmbienteTecnologico_TaxaEntregaMlhr) ? true : false);
               Z978AmbienteTecnologico_LocPF = (short)(context.localUtil.CToN( cgiGet( "Z978AmbienteTecnologico_LocPF"), ",", "."));
               n978AmbienteTecnologico_LocPF = ((0==A978AmbienteTecnologico_LocPF) ? true : false);
               Z1423AmbienteTecnologico_FatorAjuste = context.localUtil.CToN( cgiGet( "Z1423AmbienteTecnologico_FatorAjuste"), ",", ".");
               n1423AmbienteTecnologico_FatorAjuste = ((Convert.ToDecimal(0)==A1423AmbienteTecnologico_FatorAjuste) ? true : false);
               Z353AmbienteTecnologico_Ativo = StringUtil.StrToBool( cgiGet( "Z353AmbienteTecnologico_Ativo"));
               Z728AmbienteTecnologico_AreaTrabalhoCod = (int)(context.localUtil.CToN( cgiGet( "Z728AmbienteTecnologico_AreaTrabalhoCod"), ",", "."));
               A728AmbienteTecnologico_AreaTrabalhoCod = (int)(context.localUtil.CToN( cgiGet( "Z728AmbienteTecnologico_AreaTrabalhoCod"), ",", "."));
               O352AmbienteTecnologico_Descricao = cgiGet( "O352AmbienteTecnologico_Descricao");
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
               Gx_mode = cgiGet( "Mode");
               N728AmbienteTecnologico_AreaTrabalhoCod = (int)(context.localUtil.CToN( cgiGet( "N728AmbienteTecnologico_AreaTrabalhoCod"), ",", "."));
               AV12AmbienteTecnologico_Codigo = (int)(context.localUtil.CToN( cgiGet( "vAMBIENTETECNOLOGICO_CODIGO"), ",", "."));
               AV13Insert_AmbienteTecnologico_AreaTrabalhoCod = (int)(context.localUtil.CToN( cgiGet( "vINSERT_AMBIENTETECNOLOGICO_AREATRABALHOCOD"), ",", "."));
               A728AmbienteTecnologico_AreaTrabalhoCod = (int)(context.localUtil.CToN( cgiGet( "AMBIENTETECNOLOGICO_AREATRABALHOCOD"), ",", "."));
               Gx_BScreen = (short)(context.localUtil.CToN( cgiGet( "vGXBSCREEN"), ",", "."));
               A729AmbienteTecnologico_AreaTrabalhoDes = cgiGet( "AMBIENTETECNOLOGICO_AREATRABALHODES");
               n729AmbienteTecnologico_AreaTrabalhoDes = false;
               AV14Pgmname = cgiGet( "vPGMNAME");
               Gx_mode = cgiGet( "vMODE");
               Dvpanel_tableattributes_Width = cgiGet( "DVPANEL_TABLEATTRIBUTES_Width");
               Dvpanel_tableattributes_Height = cgiGet( "DVPANEL_TABLEATTRIBUTES_Height");
               Dvpanel_tableattributes_Cls = cgiGet( "DVPANEL_TABLEATTRIBUTES_Cls");
               Dvpanel_tableattributes_Title = cgiGet( "DVPANEL_TABLEATTRIBUTES_Title");
               Dvpanel_tableattributes_Collapsible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsible"));
               Dvpanel_tableattributes_Collapsed = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsed"));
               Dvpanel_tableattributes_Enabled = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Enabled"));
               Dvpanel_tableattributes_Class = cgiGet( "DVPANEL_TABLEATTRIBUTES_Class");
               Dvpanel_tableattributes_Autowidth = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autowidth"));
               Dvpanel_tableattributes_Autoheight = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoheight"));
               Dvpanel_tableattributes_Showheader = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showheader"));
               Dvpanel_tableattributes_Showcollapseicon = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showcollapseicon"));
               Dvpanel_tableattributes_Iconposition = cgiGet( "DVPANEL_TABLEATTRIBUTES_Iconposition");
               Dvpanel_tableattributes_Autoscroll = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoscroll"));
               Dvpanel_tableattributes_Visible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Visible"));
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               forbiddenHiddens = "hsh" + "AmbienteTecnologico";
               A351AmbienteTecnologico_Codigo = (int)(context.localUtil.CToN( cgiGet( edtAmbienteTecnologico_Codigo_Internalname), ",", "."));
               n351AmbienteTecnologico_Codigo = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A351AmbienteTecnologico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A351AmbienteTecnologico_Codigo), 6, 0)));
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A351AmbienteTecnologico_Codigo), "ZZZZZ9");
               forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
               hsh = cgiGet( "hsh");
               if ( ( ! ( ( A351AmbienteTecnologico_Codigo != Z351AmbienteTecnologico_Codigo ) ) || ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ) && ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
               {
                  GXUtil.WriteLog("ambientetecnologico:[SecurityCheckFailed value for]"+"AmbienteTecnologico_Codigo:"+context.localUtil.Format( (decimal)(A351AmbienteTecnologico_Codigo), "ZZZZZ9"));
                  GXUtil.WriteLog("ambientetecnologico:[SecurityCheckFailed value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
                  GxWebError = 1;
                  context.HttpContext.Response.StatusDescription = 403.ToString();
                  context.HttpContext.Response.StatusCode = 403;
                  context.WriteHtmlText( "<title>403 Forbidden</title>") ;
                  context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
                  context.WriteHtmlText( "<p /><hr />") ;
                  GXUtil.WriteLog("send_http_error_code " + 403.ToString());
                  AnyError = 1;
                  return  ;
               }
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  A351AmbienteTecnologico_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  n351AmbienteTecnologico_Codigo = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A351AmbienteTecnologico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A351AmbienteTecnologico_Codigo), 6, 0)));
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  disable_std_buttons( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
                  {
                     sMode62 = Gx_mode;
                     Gx_mode = "UPD";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                     Gx_mode = sMode62;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  }
                  standaloneModal( ) ;
                  if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
                  {
                     getByPrimaryKey( ) ;
                     if ( RcdFound62 == 1 )
                     {
                        if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
                        {
                           /* Confirm record */
                           CONFIRM_1N0( ) ;
                           if ( AnyError == 0 )
                           {
                              GX_FocusControl = bttBtn_trn_enter_Internalname;
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noinsert", ""), 1, "AMBIENTETECNOLOGICO_CODIGO");
                        AnyError = 1;
                        GX_FocusControl = edtAmbienteTecnologico_Codigo_Internalname;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E111N2 */
                           E111N2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "AFTER TRN") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E121N2 */
                           E121N2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           if ( StringUtil.StrCmp(Gx_mode, "DSP") != 0 )
                           {
                              btn_enter( ) ;
                           }
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            /* Execute user event: E121N2 */
            E121N2 ();
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll1N62( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         bttBtn_trn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
         if ( ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            bttBtn_trn_delete_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
            if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
            {
               bttBtn_trn_enter_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Visible), 5, 0)));
            }
            DisableAttributes1N62( ) ;
         }
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void CONFIRM_1N0( )
      {
         BeforeValidate1N62( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls1N62( ) ;
            }
            else
            {
               CheckExtendedTable1N62( ) ;
               CloseExtendedTableCursors1N62( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         }
      }

      protected void ResetCaption1N0( )
      {
      }

      protected void E111N2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV7WWPContext) ;
         AV8TrnContext.FromXml(AV9WebSession.Get("TrnContext"), "");
         if ( ( StringUtil.StrCmp(AV8TrnContext.gxTpr_Transactionname, AV14Pgmname) == 0 ) && ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) )
         {
            AV15GXV1 = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15GXV1), 8, 0)));
            while ( AV15GXV1 <= AV8TrnContext.gxTpr_Attributes.Count )
            {
               AV11TrnContextAtt = ((wwpbaseobjects.SdtWWPTransactionContext_Attribute)AV8TrnContext.gxTpr_Attributes.Item(AV15GXV1));
               if ( StringUtil.StrCmp(AV11TrnContextAtt.gxTpr_Attributename, "AmbienteTecnologico_AreaTrabalhoCod") == 0 )
               {
                  AV13Insert_AmbienteTecnologico_AreaTrabalhoCod = (int)(NumberUtil.Val( AV11TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13Insert_AmbienteTecnologico_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13Insert_AmbienteTecnologico_AreaTrabalhoCod), 6, 0)));
               }
               AV15GXV1 = (int)(AV15GXV1+1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15GXV1), 8, 0)));
            }
         }
         edtAmbienteTecnologico_Codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtAmbienteTecnologico_Codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtAmbienteTecnologico_Codigo_Visible), 5, 0)));
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            lblTextblockambientetecnologico_ativo_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTextblockambientetecnologico_ativo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTextblockambientetecnologico_ativo_Visible), 5, 0)));
            chkAmbienteTecnologico_Ativo.Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkAmbienteTecnologico_Ativo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkAmbienteTecnologico_Ativo.Visible), 5, 0)));
         }
      }

      protected void E121N2( )
      {
         /* After Trn Routine */
         if ( ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) && ! AV8TrnContext.gxTpr_Callerondelete )
         {
            context.wjLoc = formatLink("wwambientetecnologico.aspx") ;
            context.wjLocDisableFrm = 1;
         }
         context.setWebReturnParms(new Object[] {});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void ZM1N62( short GX_JID )
      {
         if ( ( GX_JID == 11 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z352AmbienteTecnologico_Descricao = T001N3_A352AmbienteTecnologico_Descricao[0];
               Z976AmbienteTecnologico_TaxaEntregaDsnv = T001N3_A976AmbienteTecnologico_TaxaEntregaDsnv[0];
               Z977AmbienteTecnologico_TaxaEntregaMlhr = T001N3_A977AmbienteTecnologico_TaxaEntregaMlhr[0];
               Z978AmbienteTecnologico_LocPF = T001N3_A978AmbienteTecnologico_LocPF[0];
               Z1423AmbienteTecnologico_FatorAjuste = T001N3_A1423AmbienteTecnologico_FatorAjuste[0];
               Z353AmbienteTecnologico_Ativo = T001N3_A353AmbienteTecnologico_Ativo[0];
               Z728AmbienteTecnologico_AreaTrabalhoCod = T001N3_A728AmbienteTecnologico_AreaTrabalhoCod[0];
            }
            else
            {
               Z352AmbienteTecnologico_Descricao = A352AmbienteTecnologico_Descricao;
               Z976AmbienteTecnologico_TaxaEntregaDsnv = A976AmbienteTecnologico_TaxaEntregaDsnv;
               Z977AmbienteTecnologico_TaxaEntregaMlhr = A977AmbienteTecnologico_TaxaEntregaMlhr;
               Z978AmbienteTecnologico_LocPF = A978AmbienteTecnologico_LocPF;
               Z1423AmbienteTecnologico_FatorAjuste = A1423AmbienteTecnologico_FatorAjuste;
               Z353AmbienteTecnologico_Ativo = A353AmbienteTecnologico_Ativo;
               Z728AmbienteTecnologico_AreaTrabalhoCod = A728AmbienteTecnologico_AreaTrabalhoCod;
            }
         }
         if ( GX_JID == -11 )
         {
            Z351AmbienteTecnologico_Codigo = A351AmbienteTecnologico_Codigo;
            Z352AmbienteTecnologico_Descricao = A352AmbienteTecnologico_Descricao;
            Z976AmbienteTecnologico_TaxaEntregaDsnv = A976AmbienteTecnologico_TaxaEntregaDsnv;
            Z977AmbienteTecnologico_TaxaEntregaMlhr = A977AmbienteTecnologico_TaxaEntregaMlhr;
            Z978AmbienteTecnologico_LocPF = A978AmbienteTecnologico_LocPF;
            Z1423AmbienteTecnologico_FatorAjuste = A1423AmbienteTecnologico_FatorAjuste;
            Z353AmbienteTecnologico_Ativo = A353AmbienteTecnologico_Ativo;
            Z728AmbienteTecnologico_AreaTrabalhoCod = A728AmbienteTecnologico_AreaTrabalhoCod;
            Z729AmbienteTecnologico_AreaTrabalhoDes = A729AmbienteTecnologico_AreaTrabalhoDes;
         }
      }

      protected void standaloneNotModal( )
      {
         edtAmbienteTecnologico_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtAmbienteTecnologico_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtAmbienteTecnologico_Codigo_Enabled), 5, 0)));
         Gx_BScreen = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_BScreen", StringUtil.Str( (decimal)(Gx_BScreen), 1, 0));
         AV14Pgmname = "AmbienteTecnologico";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14Pgmname", AV14Pgmname);
         edtAmbienteTecnologico_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtAmbienteTecnologico_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtAmbienteTecnologico_Codigo_Enabled), 5, 0)));
         bttBtn_trn_delete_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Enabled), 5, 0)));
         if ( ! (0==AV12AmbienteTecnologico_Codigo) )
         {
            A351AmbienteTecnologico_Codigo = AV12AmbienteTecnologico_Codigo;
            n351AmbienteTecnologico_Codigo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A351AmbienteTecnologico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A351AmbienteTecnologico_Codigo), 6, 0)));
         }
      }

      protected void standaloneModal( )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV13Insert_AmbienteTecnologico_AreaTrabalhoCod) )
         {
            A728AmbienteTecnologico_AreaTrabalhoCod = AV13Insert_AmbienteTecnologico_AreaTrabalhoCod;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A728AmbienteTecnologico_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A728AmbienteTecnologico_AreaTrabalhoCod), 6, 0)));
         }
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            bttBtn_trn_enter_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         else
         {
            bttBtn_trn_enter_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (false==A353AmbienteTecnologico_Ativo) && ( Gx_BScreen == 0 ) )
         {
            A353AmbienteTecnologico_Ativo = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A353AmbienteTecnologico_Ativo", A353AmbienteTecnologico_Ativo);
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ( Gx_BScreen == 0 ) )
         {
            /* Using cursor T001N4 */
            pr_default.execute(2, new Object[] {A728AmbienteTecnologico_AreaTrabalhoCod});
            A729AmbienteTecnologico_AreaTrabalhoDes = T001N4_A729AmbienteTecnologico_AreaTrabalhoDes[0];
            n729AmbienteTecnologico_AreaTrabalhoDes = T001N4_n729AmbienteTecnologico_AreaTrabalhoDes[0];
            pr_default.close(2);
         }
      }

      protected void Load1N62( )
      {
         /* Using cursor T001N5 */
         pr_default.execute(3, new Object[] {n351AmbienteTecnologico_Codigo, A351AmbienteTecnologico_Codigo});
         if ( (pr_default.getStatus(3) != 101) )
         {
            RcdFound62 = 1;
            A352AmbienteTecnologico_Descricao = T001N5_A352AmbienteTecnologico_Descricao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A352AmbienteTecnologico_Descricao", A352AmbienteTecnologico_Descricao);
            A729AmbienteTecnologico_AreaTrabalhoDes = T001N5_A729AmbienteTecnologico_AreaTrabalhoDes[0];
            n729AmbienteTecnologico_AreaTrabalhoDes = T001N5_n729AmbienteTecnologico_AreaTrabalhoDes[0];
            A976AmbienteTecnologico_TaxaEntregaDsnv = T001N5_A976AmbienteTecnologico_TaxaEntregaDsnv[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A976AmbienteTecnologico_TaxaEntregaDsnv", StringUtil.LTrim( StringUtil.Str( (decimal)(A976AmbienteTecnologico_TaxaEntregaDsnv), 4, 0)));
            n976AmbienteTecnologico_TaxaEntregaDsnv = T001N5_n976AmbienteTecnologico_TaxaEntregaDsnv[0];
            A977AmbienteTecnologico_TaxaEntregaMlhr = T001N5_A977AmbienteTecnologico_TaxaEntregaMlhr[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A977AmbienteTecnologico_TaxaEntregaMlhr", StringUtil.LTrim( StringUtil.Str( (decimal)(A977AmbienteTecnologico_TaxaEntregaMlhr), 4, 0)));
            n977AmbienteTecnologico_TaxaEntregaMlhr = T001N5_n977AmbienteTecnologico_TaxaEntregaMlhr[0];
            A978AmbienteTecnologico_LocPF = T001N5_A978AmbienteTecnologico_LocPF[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A978AmbienteTecnologico_LocPF", StringUtil.LTrim( StringUtil.Str( (decimal)(A978AmbienteTecnologico_LocPF), 4, 0)));
            n978AmbienteTecnologico_LocPF = T001N5_n978AmbienteTecnologico_LocPF[0];
            A1423AmbienteTecnologico_FatorAjuste = T001N5_A1423AmbienteTecnologico_FatorAjuste[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1423AmbienteTecnologico_FatorAjuste", StringUtil.LTrim( StringUtil.Str( A1423AmbienteTecnologico_FatorAjuste, 8, 4)));
            n1423AmbienteTecnologico_FatorAjuste = T001N5_n1423AmbienteTecnologico_FatorAjuste[0];
            A353AmbienteTecnologico_Ativo = T001N5_A353AmbienteTecnologico_Ativo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A353AmbienteTecnologico_Ativo", A353AmbienteTecnologico_Ativo);
            A728AmbienteTecnologico_AreaTrabalhoCod = T001N5_A728AmbienteTecnologico_AreaTrabalhoCod[0];
            ZM1N62( -11) ;
         }
         pr_default.close(3);
         OnLoadActions1N62( ) ;
      }

      protected void OnLoadActions1N62( )
      {
      }

      protected void CheckExtendedTable1N62( )
      {
         Gx_BScreen = 1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_BScreen", StringUtil.Str( (decimal)(Gx_BScreen), 1, 0));
         standaloneModal( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( A352AmbienteTecnologico_Descricao)) )
         {
            GX_msglist.addItem("Descri��o do Ambiente Tecnol�gico � obrigat�rio!", 1, "AMBIENTETECNOLOGICO_DESCRICAO");
            AnyError = 1;
            GX_FocusControl = edtAmbienteTecnologico_Descricao_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         /* Using cursor T001N4 */
         pr_default.execute(2, new Object[] {A728AmbienteTecnologico_AreaTrabalhoCod});
         if ( (pr_default.getStatus(2) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Ambiente Tecnoologico_Area Trabalho'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A729AmbienteTecnologico_AreaTrabalhoDes = T001N4_A729AmbienteTecnologico_AreaTrabalhoDes[0];
         n729AmbienteTecnologico_AreaTrabalhoDes = T001N4_n729AmbienteTecnologico_AreaTrabalhoDes[0];
         pr_default.close(2);
      }

      protected void CloseExtendedTableCursors1N62( )
      {
         pr_default.close(2);
      }

      protected void enableDisable( )
      {
      }

      protected void gxLoad_12( int A728AmbienteTecnologico_AreaTrabalhoCod )
      {
         /* Using cursor T001N6 */
         pr_default.execute(4, new Object[] {A728AmbienteTecnologico_AreaTrabalhoCod});
         if ( (pr_default.getStatus(4) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Ambiente Tecnoologico_Area Trabalho'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A729AmbienteTecnologico_AreaTrabalhoDes = T001N6_A729AmbienteTecnologico_AreaTrabalhoDes[0];
         n729AmbienteTecnologico_AreaTrabalhoDes = T001N6_n729AmbienteTecnologico_AreaTrabalhoDes[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( A729AmbienteTecnologico_AreaTrabalhoDes)+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(4) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(4);
      }

      protected void GetKey1N62( )
      {
         /* Using cursor T001N7 */
         pr_default.execute(5, new Object[] {n351AmbienteTecnologico_Codigo, A351AmbienteTecnologico_Codigo});
         if ( (pr_default.getStatus(5) != 101) )
         {
            RcdFound62 = 1;
         }
         else
         {
            RcdFound62 = 0;
         }
         pr_default.close(5);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T001N3 */
         pr_default.execute(1, new Object[] {n351AmbienteTecnologico_Codigo, A351AmbienteTecnologico_Codigo});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM1N62( 11) ;
            RcdFound62 = 1;
            A351AmbienteTecnologico_Codigo = T001N3_A351AmbienteTecnologico_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A351AmbienteTecnologico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A351AmbienteTecnologico_Codigo), 6, 0)));
            n351AmbienteTecnologico_Codigo = T001N3_n351AmbienteTecnologico_Codigo[0];
            A352AmbienteTecnologico_Descricao = T001N3_A352AmbienteTecnologico_Descricao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A352AmbienteTecnologico_Descricao", A352AmbienteTecnologico_Descricao);
            A976AmbienteTecnologico_TaxaEntregaDsnv = T001N3_A976AmbienteTecnologico_TaxaEntregaDsnv[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A976AmbienteTecnologico_TaxaEntregaDsnv", StringUtil.LTrim( StringUtil.Str( (decimal)(A976AmbienteTecnologico_TaxaEntregaDsnv), 4, 0)));
            n976AmbienteTecnologico_TaxaEntregaDsnv = T001N3_n976AmbienteTecnologico_TaxaEntregaDsnv[0];
            A977AmbienteTecnologico_TaxaEntregaMlhr = T001N3_A977AmbienteTecnologico_TaxaEntregaMlhr[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A977AmbienteTecnologico_TaxaEntregaMlhr", StringUtil.LTrim( StringUtil.Str( (decimal)(A977AmbienteTecnologico_TaxaEntregaMlhr), 4, 0)));
            n977AmbienteTecnologico_TaxaEntregaMlhr = T001N3_n977AmbienteTecnologico_TaxaEntregaMlhr[0];
            A978AmbienteTecnologico_LocPF = T001N3_A978AmbienteTecnologico_LocPF[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A978AmbienteTecnologico_LocPF", StringUtil.LTrim( StringUtil.Str( (decimal)(A978AmbienteTecnologico_LocPF), 4, 0)));
            n978AmbienteTecnologico_LocPF = T001N3_n978AmbienteTecnologico_LocPF[0];
            A1423AmbienteTecnologico_FatorAjuste = T001N3_A1423AmbienteTecnologico_FatorAjuste[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1423AmbienteTecnologico_FatorAjuste", StringUtil.LTrim( StringUtil.Str( A1423AmbienteTecnologico_FatorAjuste, 8, 4)));
            n1423AmbienteTecnologico_FatorAjuste = T001N3_n1423AmbienteTecnologico_FatorAjuste[0];
            A353AmbienteTecnologico_Ativo = T001N3_A353AmbienteTecnologico_Ativo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A353AmbienteTecnologico_Ativo", A353AmbienteTecnologico_Ativo);
            A728AmbienteTecnologico_AreaTrabalhoCod = T001N3_A728AmbienteTecnologico_AreaTrabalhoCod[0];
            O352AmbienteTecnologico_Descricao = A352AmbienteTecnologico_Descricao;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A352AmbienteTecnologico_Descricao", A352AmbienteTecnologico_Descricao);
            Z351AmbienteTecnologico_Codigo = A351AmbienteTecnologico_Codigo;
            sMode62 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            Load1N62( ) ;
            if ( AnyError == 1 )
            {
               RcdFound62 = 0;
               InitializeNonKey1N62( ) ;
            }
            Gx_mode = sMode62;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         else
         {
            RcdFound62 = 0;
            InitializeNonKey1N62( ) ;
            sMode62 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            standaloneModal( ) ;
            Gx_mode = sMode62;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey1N62( ) ;
         if ( RcdFound62 == 0 )
         {
         }
         else
         {
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound62 = 0;
         /* Using cursor T001N8 */
         pr_default.execute(6, new Object[] {n351AmbienteTecnologico_Codigo, A351AmbienteTecnologico_Codigo});
         if ( (pr_default.getStatus(6) != 101) )
         {
            while ( (pr_default.getStatus(6) != 101) && ( ( T001N8_A351AmbienteTecnologico_Codigo[0] < A351AmbienteTecnologico_Codigo ) ) )
            {
               pr_default.readNext(6);
            }
            if ( (pr_default.getStatus(6) != 101) && ( ( T001N8_A351AmbienteTecnologico_Codigo[0] > A351AmbienteTecnologico_Codigo ) ) )
            {
               A351AmbienteTecnologico_Codigo = T001N8_A351AmbienteTecnologico_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A351AmbienteTecnologico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A351AmbienteTecnologico_Codigo), 6, 0)));
               n351AmbienteTecnologico_Codigo = T001N8_n351AmbienteTecnologico_Codigo[0];
               RcdFound62 = 1;
            }
         }
         pr_default.close(6);
      }

      protected void move_previous( )
      {
         RcdFound62 = 0;
         /* Using cursor T001N9 */
         pr_default.execute(7, new Object[] {n351AmbienteTecnologico_Codigo, A351AmbienteTecnologico_Codigo});
         if ( (pr_default.getStatus(7) != 101) )
         {
            while ( (pr_default.getStatus(7) != 101) && ( ( T001N9_A351AmbienteTecnologico_Codigo[0] > A351AmbienteTecnologico_Codigo ) ) )
            {
               pr_default.readNext(7);
            }
            if ( (pr_default.getStatus(7) != 101) && ( ( T001N9_A351AmbienteTecnologico_Codigo[0] < A351AmbienteTecnologico_Codigo ) ) )
            {
               A351AmbienteTecnologico_Codigo = T001N9_A351AmbienteTecnologico_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A351AmbienteTecnologico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A351AmbienteTecnologico_Codigo), 6, 0)));
               n351AmbienteTecnologico_Codigo = T001N9_n351AmbienteTecnologico_Codigo[0];
               RcdFound62 = 1;
            }
         }
         pr_default.close(7);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey1N62( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = edtAmbienteTecnologico_Descricao_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert1N62( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound62 == 1 )
            {
               if ( A351AmbienteTecnologico_Codigo != Z351AmbienteTecnologico_Codigo )
               {
                  A351AmbienteTecnologico_Codigo = Z351AmbienteTecnologico_Codigo;
                  n351AmbienteTecnologico_Codigo = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A351AmbienteTecnologico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A351AmbienteTecnologico_Codigo), 6, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "AMBIENTETECNOLOGICO_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtAmbienteTecnologico_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = edtAmbienteTecnologico_Descricao_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  /* Update record */
                  Update1N62( ) ;
                  GX_FocusControl = edtAmbienteTecnologico_Descricao_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( A351AmbienteTecnologico_Codigo != Z351AmbienteTecnologico_Codigo )
               {
                  /* Insert record */
                  GX_FocusControl = edtAmbienteTecnologico_Descricao_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert1N62( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "AMBIENTETECNOLOGICO_CODIGO");
                     AnyError = 1;
                     GX_FocusControl = edtAmbienteTecnologico_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     /* Insert record */
                     GX_FocusControl = edtAmbienteTecnologico_Descricao_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert1N62( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            if ( AnyError == 0 )
            {
               context.nUserReturn = 1;
            }
         }
      }

      protected void btn_delete( )
      {
         if ( A351AmbienteTecnologico_Codigo != Z351AmbienteTecnologico_Codigo )
         {
            A351AmbienteTecnologico_Codigo = Z351AmbienteTecnologico_Codigo;
            n351AmbienteTecnologico_Codigo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A351AmbienteTecnologico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A351AmbienteTecnologico_Codigo), 6, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "AMBIENTETECNOLOGICO_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtAmbienteTecnologico_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = edtAmbienteTecnologico_Descricao_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
         }
      }

      protected void CheckOptimisticConcurrency1N62( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T001N2 */
            pr_default.execute(0, new Object[] {n351AmbienteTecnologico_Codigo, A351AmbienteTecnologico_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"AmbienteTecnologico"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            Gx_longc = false;
            if ( (pr_default.getStatus(0) == 101) || ( StringUtil.StrCmp(Z352AmbienteTecnologico_Descricao, T001N2_A352AmbienteTecnologico_Descricao[0]) != 0 ) || ( Z976AmbienteTecnologico_TaxaEntregaDsnv != T001N2_A976AmbienteTecnologico_TaxaEntregaDsnv[0] ) || ( Z977AmbienteTecnologico_TaxaEntregaMlhr != T001N2_A977AmbienteTecnologico_TaxaEntregaMlhr[0] ) || ( Z978AmbienteTecnologico_LocPF != T001N2_A978AmbienteTecnologico_LocPF[0] ) || ( Z1423AmbienteTecnologico_FatorAjuste != T001N2_A1423AmbienteTecnologico_FatorAjuste[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z353AmbienteTecnologico_Ativo != T001N2_A353AmbienteTecnologico_Ativo[0] ) || ( Z728AmbienteTecnologico_AreaTrabalhoCod != T001N2_A728AmbienteTecnologico_AreaTrabalhoCod[0] ) )
            {
               if ( StringUtil.StrCmp(Z352AmbienteTecnologico_Descricao, T001N2_A352AmbienteTecnologico_Descricao[0]) != 0 )
               {
                  GXUtil.WriteLog("ambientetecnologico:[seudo value changed for attri]"+"AmbienteTecnologico_Descricao");
                  GXUtil.WriteLogRaw("Old: ",Z352AmbienteTecnologico_Descricao);
                  GXUtil.WriteLogRaw("Current: ",T001N2_A352AmbienteTecnologico_Descricao[0]);
               }
               if ( Z976AmbienteTecnologico_TaxaEntregaDsnv != T001N2_A976AmbienteTecnologico_TaxaEntregaDsnv[0] )
               {
                  GXUtil.WriteLog("ambientetecnologico:[seudo value changed for attri]"+"AmbienteTecnologico_TaxaEntregaDsnv");
                  GXUtil.WriteLogRaw("Old: ",Z976AmbienteTecnologico_TaxaEntregaDsnv);
                  GXUtil.WriteLogRaw("Current: ",T001N2_A976AmbienteTecnologico_TaxaEntregaDsnv[0]);
               }
               if ( Z977AmbienteTecnologico_TaxaEntregaMlhr != T001N2_A977AmbienteTecnologico_TaxaEntregaMlhr[0] )
               {
                  GXUtil.WriteLog("ambientetecnologico:[seudo value changed for attri]"+"AmbienteTecnologico_TaxaEntregaMlhr");
                  GXUtil.WriteLogRaw("Old: ",Z977AmbienteTecnologico_TaxaEntregaMlhr);
                  GXUtil.WriteLogRaw("Current: ",T001N2_A977AmbienteTecnologico_TaxaEntregaMlhr[0]);
               }
               if ( Z978AmbienteTecnologico_LocPF != T001N2_A978AmbienteTecnologico_LocPF[0] )
               {
                  GXUtil.WriteLog("ambientetecnologico:[seudo value changed for attri]"+"AmbienteTecnologico_LocPF");
                  GXUtil.WriteLogRaw("Old: ",Z978AmbienteTecnologico_LocPF);
                  GXUtil.WriteLogRaw("Current: ",T001N2_A978AmbienteTecnologico_LocPF[0]);
               }
               if ( Z1423AmbienteTecnologico_FatorAjuste != T001N2_A1423AmbienteTecnologico_FatorAjuste[0] )
               {
                  GXUtil.WriteLog("ambientetecnologico:[seudo value changed for attri]"+"AmbienteTecnologico_FatorAjuste");
                  GXUtil.WriteLogRaw("Old: ",Z1423AmbienteTecnologico_FatorAjuste);
                  GXUtil.WriteLogRaw("Current: ",T001N2_A1423AmbienteTecnologico_FatorAjuste[0]);
               }
               if ( Z353AmbienteTecnologico_Ativo != T001N2_A353AmbienteTecnologico_Ativo[0] )
               {
                  GXUtil.WriteLog("ambientetecnologico:[seudo value changed for attri]"+"AmbienteTecnologico_Ativo");
                  GXUtil.WriteLogRaw("Old: ",Z353AmbienteTecnologico_Ativo);
                  GXUtil.WriteLogRaw("Current: ",T001N2_A353AmbienteTecnologico_Ativo[0]);
               }
               if ( Z728AmbienteTecnologico_AreaTrabalhoCod != T001N2_A728AmbienteTecnologico_AreaTrabalhoCod[0] )
               {
                  GXUtil.WriteLog("ambientetecnologico:[seudo value changed for attri]"+"AmbienteTecnologico_AreaTrabalhoCod");
                  GXUtil.WriteLogRaw("Old: ",Z728AmbienteTecnologico_AreaTrabalhoCod);
                  GXUtil.WriteLogRaw("Current: ",T001N2_A728AmbienteTecnologico_AreaTrabalhoCod[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"AmbienteTecnologico"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert1N62( )
      {
         BeforeValidate1N62( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable1N62( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM1N62( 0) ;
            CheckOptimisticConcurrency1N62( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm1N62( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert1N62( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T001N10 */
                     pr_default.execute(8, new Object[] {A352AmbienteTecnologico_Descricao, n976AmbienteTecnologico_TaxaEntregaDsnv, A976AmbienteTecnologico_TaxaEntregaDsnv, n977AmbienteTecnologico_TaxaEntregaMlhr, A977AmbienteTecnologico_TaxaEntregaMlhr, n978AmbienteTecnologico_LocPF, A978AmbienteTecnologico_LocPF, n1423AmbienteTecnologico_FatorAjuste, A1423AmbienteTecnologico_FatorAjuste, A353AmbienteTecnologico_Ativo, A728AmbienteTecnologico_AreaTrabalhoCod});
                     A351AmbienteTecnologico_Codigo = T001N10_A351AmbienteTecnologico_Codigo[0];
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A351AmbienteTecnologico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A351AmbienteTecnologico_Codigo), 6, 0)));
                     n351AmbienteTecnologico_Codigo = T001N10_n351AmbienteTecnologico_Codigo[0];
                     pr_default.close(8);
                     dsDefault.SmartCacheProvider.SetUpdated("AmbienteTecnologico") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                           ResetCaption1N0( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load1N62( ) ;
            }
            EndLevel1N62( ) ;
         }
         CloseExtendedTableCursors1N62( ) ;
      }

      protected void Update1N62( )
      {
         BeforeValidate1N62( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable1N62( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency1N62( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm1N62( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate1N62( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T001N11 */
                     pr_default.execute(9, new Object[] {A352AmbienteTecnologico_Descricao, n976AmbienteTecnologico_TaxaEntregaDsnv, A976AmbienteTecnologico_TaxaEntregaDsnv, n977AmbienteTecnologico_TaxaEntregaMlhr, A977AmbienteTecnologico_TaxaEntregaMlhr, n978AmbienteTecnologico_LocPF, A978AmbienteTecnologico_LocPF, n1423AmbienteTecnologico_FatorAjuste, A1423AmbienteTecnologico_FatorAjuste, A353AmbienteTecnologico_Ativo, A728AmbienteTecnologico_AreaTrabalhoCod, n351AmbienteTecnologico_Codigo, A351AmbienteTecnologico_Codigo});
                     pr_default.close(9);
                     dsDefault.SmartCacheProvider.SetUpdated("AmbienteTecnologico") ;
                     if ( (pr_default.getStatus(9) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"AmbienteTecnologico"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate1N62( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                           {
                              if ( AnyError == 0 )
                              {
                                 context.nUserReturn = 1;
                              }
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel1N62( ) ;
         }
         CloseExtendedTableCursors1N62( ) ;
      }

      protected void DeferredUpdate1N62( )
      {
      }

      protected void delete( )
      {
         BeforeValidate1N62( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency1N62( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls1N62( ) ;
            AfterConfirm1N62( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete1N62( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T001N12 */
                  pr_default.execute(10, new Object[] {n351AmbienteTecnologico_Codigo, A351AmbienteTecnologico_Codigo});
                  pr_default.close(10);
                  dsDefault.SmartCacheProvider.SetUpdated("AmbienteTecnologico") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                        {
                           if ( AnyError == 0 )
                           {
                              context.nUserReturn = 1;
                           }
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode62 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         EndLevel1N62( ) ;
         Gx_mode = sMode62;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
      }

      protected void OnDeleteControls1N62( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            /* Using cursor T001N13 */
            pr_default.execute(11, new Object[] {A728AmbienteTecnologico_AreaTrabalhoCod});
            A729AmbienteTecnologico_AreaTrabalhoDes = T001N13_A729AmbienteTecnologico_AreaTrabalhoDes[0];
            n729AmbienteTecnologico_AreaTrabalhoDes = T001N13_n729AmbienteTecnologico_AreaTrabalhoDes[0];
            pr_default.close(11);
         }
         if ( AnyError == 0 )
         {
            /* Using cursor T001N14 */
            pr_default.execute(12, new Object[] {n351AmbienteTecnologico_Codigo, A351AmbienteTecnologico_Codigo});
            if ( (pr_default.getStatus(12) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Sistema"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(12);
            /* Using cursor T001N15 */
            pr_default.execute(13, new Object[] {n351AmbienteTecnologico_Codigo, A351AmbienteTecnologico_Codigo});
            if ( (pr_default.getStatus(13) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Ambiente Tecnologico Tecnologias"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(13);
         }
      }

      protected void EndLevel1N62( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete1N62( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(1);
            pr_default.close(11);
            context.CommitDataStores( "AmbienteTecnologico");
            if ( AnyError == 0 )
            {
               ConfirmValues1N0( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(1);
            pr_default.close(11);
            context.RollbackDataStores( "AmbienteTecnologico");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart1N62( )
      {
         /* Scan By routine */
         /* Using cursor T001N16 */
         pr_default.execute(14);
         RcdFound62 = 0;
         if ( (pr_default.getStatus(14) != 101) )
         {
            RcdFound62 = 1;
            A351AmbienteTecnologico_Codigo = T001N16_A351AmbienteTecnologico_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A351AmbienteTecnologico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A351AmbienteTecnologico_Codigo), 6, 0)));
            n351AmbienteTecnologico_Codigo = T001N16_n351AmbienteTecnologico_Codigo[0];
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext1N62( )
      {
         /* Scan next routine */
         pr_default.readNext(14);
         RcdFound62 = 0;
         if ( (pr_default.getStatus(14) != 101) )
         {
            RcdFound62 = 1;
            A351AmbienteTecnologico_Codigo = T001N16_A351AmbienteTecnologico_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A351AmbienteTecnologico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A351AmbienteTecnologico_Codigo), 6, 0)));
            n351AmbienteTecnologico_Codigo = T001N16_n351AmbienteTecnologico_Codigo[0];
         }
      }

      protected void ScanEnd1N62( )
      {
         pr_default.close(14);
      }

      protected void AfterConfirm1N62( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert1N62( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate1N62( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete1N62( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete1N62( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate1N62( )
      {
         /* Before Validate Rules */
         A728AmbienteTecnologico_AreaTrabalhoCod = AV7WWPContext.gxTpr_Areatrabalho_codigo;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A728AmbienteTecnologico_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A728AmbienteTecnologico_AreaTrabalhoCod), 6, 0)));
         if ( ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  || ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )  ) && ( StringUtil.StrCmp(A352AmbienteTecnologico_Descricao, O352AmbienteTecnologico_Descricao) != 0 ) && new prc_existenomecadastrado(context).executeUdp(  "AmbTec",  A352AmbienteTecnologico_Descricao) )
         {
            GX_msglist.addItem("Essa descri��o j� foi cadastrada!", 1, "AMBIENTETECNOLOGICO_DESCRICAO");
            AnyError = 1;
            GX_FocusControl = edtAmbienteTecnologico_Descricao_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
      }

      protected void DisableAttributes1N62( )
      {
         edtAmbienteTecnologico_Descricao_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtAmbienteTecnologico_Descricao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtAmbienteTecnologico_Descricao_Enabled), 5, 0)));
         edtAmbienteTecnologico_TaxaEntregaDsnv_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtAmbienteTecnologico_TaxaEntregaDsnv_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtAmbienteTecnologico_TaxaEntregaDsnv_Enabled), 5, 0)));
         edtAmbienteTecnologico_TaxaEntregaMlhr_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtAmbienteTecnologico_TaxaEntregaMlhr_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtAmbienteTecnologico_TaxaEntregaMlhr_Enabled), 5, 0)));
         edtAmbienteTecnologico_LocPF_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtAmbienteTecnologico_LocPF_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtAmbienteTecnologico_LocPF_Enabled), 5, 0)));
         edtAmbienteTecnologico_FatorAjuste_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtAmbienteTecnologico_FatorAjuste_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtAmbienteTecnologico_FatorAjuste_Enabled), 5, 0)));
         chkAmbienteTecnologico_Ativo.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkAmbienteTecnologico_Ativo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(chkAmbienteTecnologico_Ativo.Enabled), 5, 0)));
         edtAmbienteTecnologico_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtAmbienteTecnologico_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtAmbienteTecnologico_Codigo_Enabled), 5, 0)));
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues1N0( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?202054839246");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("ambientetecnologico.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV12AmbienteTecnologico_Codigo)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "Z351AmbienteTecnologico_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z351AmbienteTecnologico_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z352AmbienteTecnologico_Descricao", Z352AmbienteTecnologico_Descricao);
         GxWebStd.gx_hidden_field( context, "Z976AmbienteTecnologico_TaxaEntregaDsnv", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z976AmbienteTecnologico_TaxaEntregaDsnv), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z977AmbienteTecnologico_TaxaEntregaMlhr", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z977AmbienteTecnologico_TaxaEntregaMlhr), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z978AmbienteTecnologico_LocPF", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z978AmbienteTecnologico_LocPF), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1423AmbienteTecnologico_FatorAjuste", StringUtil.LTrim( StringUtil.NToC( Z1423AmbienteTecnologico_FatorAjuste, 8, 4, ",", "")));
         GxWebStd.gx_boolean_hidden_field( context, "Z353AmbienteTecnologico_Ativo", Z353AmbienteTecnologico_Ativo);
         GxWebStd.gx_hidden_field( context, "Z728AmbienteTecnologico_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z728AmbienteTecnologico_AreaTrabalhoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "O352AmbienteTecnologico_Descricao", O352AmbienteTecnologico_Descricao);
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "N728AmbienteTecnologico_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(A728AmbienteTecnologico_AreaTrabalhoCod), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTRNCONTEXT", AV8TrnContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTRNCONTEXT", AV8TrnContext);
         }
         GxWebStd.gx_hidden_field( context, "vAMBIENTETECNOLOGICO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV12AmbienteTecnologico_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINSERT_AMBIENTETECNOLOGICO_AREATRABALHOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13Insert_AmbienteTecnologico_AreaTrabalhoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "AMBIENTETECNOLOGICO_AREATRABALHOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A728AmbienteTecnologico_AreaTrabalhoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGXBSCREEN", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gx_BScreen), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "AMBIENTETECNOLOGICO_AREATRABALHODES", A729AmbienteTecnologico_AreaTrabalhoDes);
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV14Pgmname));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vAMBIENTETECNOLOGICO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV12AmbienteTecnologico_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Width", StringUtil.RTrim( Dvpanel_tableattributes_Width));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Cls", StringUtil.RTrim( Dvpanel_tableattributes_Cls));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Title", StringUtil.RTrim( Dvpanel_tableattributes_Title));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsible", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsible));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsed", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsed));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Enabled", StringUtil.BoolToStr( Dvpanel_tableattributes_Enabled));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autowidth", StringUtil.BoolToStr( Dvpanel_tableattributes_Autowidth));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoheight", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoheight));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Showcollapseicon", StringUtil.BoolToStr( Dvpanel_tableattributes_Showcollapseicon));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Iconposition", StringUtil.RTrim( Dvpanel_tableattributes_Iconposition));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoscroll", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoscroll));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = "hsh" + "AmbienteTecnologico";
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A351AmbienteTecnologico_Codigo), "ZZZZZ9");
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
         GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("ambientetecnologico:[SendSecurityCheck value for]"+"AmbienteTecnologico_Codigo:"+context.localUtil.Format( (decimal)(A351AmbienteTecnologico_Codigo), "ZZZZZ9"));
         GXUtil.WriteLog("ambientetecnologico:[SendSecurityCheck value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("ambientetecnologico.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV12AmbienteTecnologico_Codigo) ;
      }

      public override String GetPgmname( )
      {
         return "AmbienteTecnologico" ;
      }

      public override String GetPgmdesc( )
      {
         return "Ambiente Tecnologico" ;
      }

      protected void InitializeNonKey1N62( )
      {
         A728AmbienteTecnologico_AreaTrabalhoCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A728AmbienteTecnologico_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A728AmbienteTecnologico_AreaTrabalhoCod), 6, 0)));
         A352AmbienteTecnologico_Descricao = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A352AmbienteTecnologico_Descricao", A352AmbienteTecnologico_Descricao);
         A729AmbienteTecnologico_AreaTrabalhoDes = "";
         n729AmbienteTecnologico_AreaTrabalhoDes = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A729AmbienteTecnologico_AreaTrabalhoDes", A729AmbienteTecnologico_AreaTrabalhoDes);
         A976AmbienteTecnologico_TaxaEntregaDsnv = 0;
         n976AmbienteTecnologico_TaxaEntregaDsnv = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A976AmbienteTecnologico_TaxaEntregaDsnv", StringUtil.LTrim( StringUtil.Str( (decimal)(A976AmbienteTecnologico_TaxaEntregaDsnv), 4, 0)));
         n976AmbienteTecnologico_TaxaEntregaDsnv = ((0==A976AmbienteTecnologico_TaxaEntregaDsnv) ? true : false);
         A977AmbienteTecnologico_TaxaEntregaMlhr = 0;
         n977AmbienteTecnologico_TaxaEntregaMlhr = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A977AmbienteTecnologico_TaxaEntregaMlhr", StringUtil.LTrim( StringUtil.Str( (decimal)(A977AmbienteTecnologico_TaxaEntregaMlhr), 4, 0)));
         n977AmbienteTecnologico_TaxaEntregaMlhr = ((0==A977AmbienteTecnologico_TaxaEntregaMlhr) ? true : false);
         A978AmbienteTecnologico_LocPF = 0;
         n978AmbienteTecnologico_LocPF = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A978AmbienteTecnologico_LocPF", StringUtil.LTrim( StringUtil.Str( (decimal)(A978AmbienteTecnologico_LocPF), 4, 0)));
         n978AmbienteTecnologico_LocPF = ((0==A978AmbienteTecnologico_LocPF) ? true : false);
         A1423AmbienteTecnologico_FatorAjuste = 0;
         n1423AmbienteTecnologico_FatorAjuste = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1423AmbienteTecnologico_FatorAjuste", StringUtil.LTrim( StringUtil.Str( A1423AmbienteTecnologico_FatorAjuste, 8, 4)));
         n1423AmbienteTecnologico_FatorAjuste = ((Convert.ToDecimal(0)==A1423AmbienteTecnologico_FatorAjuste) ? true : false);
         A353AmbienteTecnologico_Ativo = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A353AmbienteTecnologico_Ativo", A353AmbienteTecnologico_Ativo);
         O352AmbienteTecnologico_Descricao = A352AmbienteTecnologico_Descricao;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A352AmbienteTecnologico_Descricao", A352AmbienteTecnologico_Descricao);
         Z352AmbienteTecnologico_Descricao = "";
         Z976AmbienteTecnologico_TaxaEntregaDsnv = 0;
         Z977AmbienteTecnologico_TaxaEntregaMlhr = 0;
         Z978AmbienteTecnologico_LocPF = 0;
         Z1423AmbienteTecnologico_FatorAjuste = 0;
         Z353AmbienteTecnologico_Ativo = false;
         Z728AmbienteTecnologico_AreaTrabalhoCod = 0;
      }

      protected void InitAll1N62( )
      {
         A351AmbienteTecnologico_Codigo = 0;
         n351AmbienteTecnologico_Codigo = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A351AmbienteTecnologico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A351AmbienteTecnologico_Codigo), 6, 0)));
         InitializeNonKey1N62( ) ;
      }

      protected void StandaloneModalInsert( )
      {
         A353AmbienteTecnologico_Ativo = i353AmbienteTecnologico_Ativo;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A353AmbienteTecnologico_Ativo", A353AmbienteTecnologico_Ativo);
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?202054839274");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("ambientetecnologico.js", "?202054839274");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblockambientetecnologico_descricao_Internalname = "TEXTBLOCKAMBIENTETECNOLOGICO_DESCRICAO";
         edtAmbienteTecnologico_Descricao_Internalname = "AMBIENTETECNOLOGICO_DESCRICAO";
         lblTextblockambientetecnologico_taxaentregadsnv_Internalname = "TEXTBLOCKAMBIENTETECNOLOGICO_TAXAENTREGADSNV";
         edtAmbienteTecnologico_TaxaEntregaDsnv_Internalname = "AMBIENTETECNOLOGICO_TAXAENTREGADSNV";
         lblAmbientetecnologico_taxaentregadsnv_righttext_Internalname = "AMBIENTETECNOLOGICO_TAXAENTREGADSNV_RIGHTTEXT";
         tblTablemergedambientetecnologico_taxaentregadsnv_Internalname = "TABLEMERGEDAMBIENTETECNOLOGICO_TAXAENTREGADSNV";
         lblTextblockambientetecnologico_taxaentregamlhr_Internalname = "TEXTBLOCKAMBIENTETECNOLOGICO_TAXAENTREGAMLHR";
         edtAmbienteTecnologico_TaxaEntregaMlhr_Internalname = "AMBIENTETECNOLOGICO_TAXAENTREGAMLHR";
         lblAmbientetecnologico_taxaentregamlhr_righttext_Internalname = "AMBIENTETECNOLOGICO_TAXAENTREGAMLHR_RIGHTTEXT";
         tblTablemergedambientetecnologico_taxaentregamlhr_Internalname = "TABLEMERGEDAMBIENTETECNOLOGICO_TAXAENTREGAMLHR";
         lblTextblockambientetecnologico_locpf_Internalname = "TEXTBLOCKAMBIENTETECNOLOGICO_LOCPF";
         edtAmbienteTecnologico_LocPF_Internalname = "AMBIENTETECNOLOGICO_LOCPF";
         lblTextblockambientetecnologico_fatorajuste_Internalname = "TEXTBLOCKAMBIENTETECNOLOGICO_FATORAJUSTE";
         edtAmbienteTecnologico_FatorAjuste_Internalname = "AMBIENTETECNOLOGICO_FATORAJUSTE";
         lblTextblockambientetecnologico_ativo_Internalname = "TEXTBLOCKAMBIENTETECNOLOGICO_ATIVO";
         chkAmbienteTecnologico_Ativo_Internalname = "AMBIENTETECNOLOGICO_ATIVO";
         tblTableattributes_Internalname = "TABLEATTRIBUTES";
         Dvpanel_tableattributes_Internalname = "DVPANEL_TABLEATTRIBUTES";
         tblTablecontent_Internalname = "TABLECONTENT";
         bttBtn_trn_enter_Internalname = "BTN_TRN_ENTER";
         bttBtn_trn_cancel_Internalname = "BTN_TRN_CANCEL";
         bttBtn_trn_delete_Internalname = "BTN_TRN_DELETE";
         tblTableactions_Internalname = "TABLEACTIONS";
         tblTablemain_Internalname = "TABLEMAIN";
         edtAmbienteTecnologico_Codigo_Internalname = "AMBIENTETECNOLOGICO_CODIGO";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Dvpanel_tableattributes_Autoscroll = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Iconposition = "left";
         Dvpanel_tableattributes_Showcollapseicon = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Autoheight = Convert.ToBoolean( -1);
         Dvpanel_tableattributes_Autowidth = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsed = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsible = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Title = "Ambiente Operacional";
         Dvpanel_tableattributes_Cls = "GXUI-DVelop-Panel";
         Dvpanel_tableattributes_Width = "100%";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Ambiente Tecnologico";
         edtAmbienteTecnologico_TaxaEntregaDsnv_Jsonclick = "";
         edtAmbienteTecnologico_TaxaEntregaDsnv_Enabled = 1;
         edtAmbienteTecnologico_TaxaEntregaMlhr_Jsonclick = "";
         edtAmbienteTecnologico_TaxaEntregaMlhr_Enabled = 1;
         chkAmbienteTecnologico_Ativo.Enabled = 1;
         chkAmbienteTecnologico_Ativo.Visible = 1;
         lblTextblockambientetecnologico_ativo_Visible = 1;
         edtAmbienteTecnologico_FatorAjuste_Jsonclick = "";
         edtAmbienteTecnologico_FatorAjuste_Enabled = 1;
         edtAmbienteTecnologico_LocPF_Jsonclick = "";
         edtAmbienteTecnologico_LocPF_Enabled = 1;
         edtAmbienteTecnologico_Descricao_Jsonclick = "";
         edtAmbienteTecnologico_Descricao_Enabled = 1;
         bttBtn_trn_delete_Enabled = 0;
         bttBtn_trn_delete_Visible = 1;
         bttBtn_trn_cancel_Visible = 1;
         bttBtn_trn_enter_Enabled = 1;
         bttBtn_trn_enter_Visible = 1;
         edtAmbienteTecnologico_Codigo_Jsonclick = "";
         edtAmbienteTecnologico_Codigo_Enabled = 0;
         edtAmbienteTecnologico_Codigo_Visible = 1;
         chkAmbienteTecnologico_Ativo.Caption = "";
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true},{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV12AmbienteTecnologico_Codigo',fld:'vAMBIENTETECNOLOGICO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("AFTER TRN","{handler:'E121N2',iparms:[{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV8TrnContext',fld:'vTRNCONTEXT',pic:'',nv:null}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(11);
      }

      public override void initialize( )
      {
         sPrefix = "";
         wcpOGx_mode = "";
         Z352AmbienteTecnologico_Descricao = "";
         O352AmbienteTecnologico_Descricao = "";
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         sStyleString = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         bttBtn_trn_enter_Jsonclick = "";
         bttBtn_trn_cancel_Jsonclick = "";
         bttBtn_trn_delete_Jsonclick = "";
         lblTextblockambientetecnologico_descricao_Jsonclick = "";
         A352AmbienteTecnologico_Descricao = "";
         lblTextblockambientetecnologico_taxaentregadsnv_Jsonclick = "";
         lblTextblockambientetecnologico_taxaentregamlhr_Jsonclick = "";
         lblTextblockambientetecnologico_locpf_Jsonclick = "";
         lblTextblockambientetecnologico_fatorajuste_Jsonclick = "";
         lblTextblockambientetecnologico_ativo_Jsonclick = "";
         lblAmbientetecnologico_taxaentregamlhr_righttext_Jsonclick = "";
         lblAmbientetecnologico_taxaentregadsnv_righttext_Jsonclick = "";
         A729AmbienteTecnologico_AreaTrabalhoDes = "";
         AV14Pgmname = "";
         Dvpanel_tableattributes_Height = "";
         Dvpanel_tableattributes_Class = "";
         forbiddenHiddens = "";
         hsh = "";
         sMode62 = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV7WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV9WebSession = context.GetSession();
         AV11TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         Z729AmbienteTecnologico_AreaTrabalhoDes = "";
         T001N4_A729AmbienteTecnologico_AreaTrabalhoDes = new String[] {""} ;
         T001N4_n729AmbienteTecnologico_AreaTrabalhoDes = new bool[] {false} ;
         T001N5_A351AmbienteTecnologico_Codigo = new int[1] ;
         T001N5_n351AmbienteTecnologico_Codigo = new bool[] {false} ;
         T001N5_A352AmbienteTecnologico_Descricao = new String[] {""} ;
         T001N5_A729AmbienteTecnologico_AreaTrabalhoDes = new String[] {""} ;
         T001N5_n729AmbienteTecnologico_AreaTrabalhoDes = new bool[] {false} ;
         T001N5_A976AmbienteTecnologico_TaxaEntregaDsnv = new short[1] ;
         T001N5_n976AmbienteTecnologico_TaxaEntregaDsnv = new bool[] {false} ;
         T001N5_A977AmbienteTecnologico_TaxaEntregaMlhr = new short[1] ;
         T001N5_n977AmbienteTecnologico_TaxaEntregaMlhr = new bool[] {false} ;
         T001N5_A978AmbienteTecnologico_LocPF = new short[1] ;
         T001N5_n978AmbienteTecnologico_LocPF = new bool[] {false} ;
         T001N5_A1423AmbienteTecnologico_FatorAjuste = new decimal[1] ;
         T001N5_n1423AmbienteTecnologico_FatorAjuste = new bool[] {false} ;
         T001N5_A353AmbienteTecnologico_Ativo = new bool[] {false} ;
         T001N5_A728AmbienteTecnologico_AreaTrabalhoCod = new int[1] ;
         T001N6_A729AmbienteTecnologico_AreaTrabalhoDes = new String[] {""} ;
         T001N6_n729AmbienteTecnologico_AreaTrabalhoDes = new bool[] {false} ;
         T001N7_A351AmbienteTecnologico_Codigo = new int[1] ;
         T001N7_n351AmbienteTecnologico_Codigo = new bool[] {false} ;
         T001N3_A351AmbienteTecnologico_Codigo = new int[1] ;
         T001N3_n351AmbienteTecnologico_Codigo = new bool[] {false} ;
         T001N3_A352AmbienteTecnologico_Descricao = new String[] {""} ;
         T001N3_A976AmbienteTecnologico_TaxaEntregaDsnv = new short[1] ;
         T001N3_n976AmbienteTecnologico_TaxaEntregaDsnv = new bool[] {false} ;
         T001N3_A977AmbienteTecnologico_TaxaEntregaMlhr = new short[1] ;
         T001N3_n977AmbienteTecnologico_TaxaEntregaMlhr = new bool[] {false} ;
         T001N3_A978AmbienteTecnologico_LocPF = new short[1] ;
         T001N3_n978AmbienteTecnologico_LocPF = new bool[] {false} ;
         T001N3_A1423AmbienteTecnologico_FatorAjuste = new decimal[1] ;
         T001N3_n1423AmbienteTecnologico_FatorAjuste = new bool[] {false} ;
         T001N3_A353AmbienteTecnologico_Ativo = new bool[] {false} ;
         T001N3_A728AmbienteTecnologico_AreaTrabalhoCod = new int[1] ;
         T001N8_A351AmbienteTecnologico_Codigo = new int[1] ;
         T001N8_n351AmbienteTecnologico_Codigo = new bool[] {false} ;
         T001N9_A351AmbienteTecnologico_Codigo = new int[1] ;
         T001N9_n351AmbienteTecnologico_Codigo = new bool[] {false} ;
         T001N2_A351AmbienteTecnologico_Codigo = new int[1] ;
         T001N2_n351AmbienteTecnologico_Codigo = new bool[] {false} ;
         T001N2_A352AmbienteTecnologico_Descricao = new String[] {""} ;
         T001N2_A976AmbienteTecnologico_TaxaEntregaDsnv = new short[1] ;
         T001N2_n976AmbienteTecnologico_TaxaEntregaDsnv = new bool[] {false} ;
         T001N2_A977AmbienteTecnologico_TaxaEntregaMlhr = new short[1] ;
         T001N2_n977AmbienteTecnologico_TaxaEntregaMlhr = new bool[] {false} ;
         T001N2_A978AmbienteTecnologico_LocPF = new short[1] ;
         T001N2_n978AmbienteTecnologico_LocPF = new bool[] {false} ;
         T001N2_A1423AmbienteTecnologico_FatorAjuste = new decimal[1] ;
         T001N2_n1423AmbienteTecnologico_FatorAjuste = new bool[] {false} ;
         T001N2_A353AmbienteTecnologico_Ativo = new bool[] {false} ;
         T001N2_A728AmbienteTecnologico_AreaTrabalhoCod = new int[1] ;
         T001N10_A351AmbienteTecnologico_Codigo = new int[1] ;
         T001N10_n351AmbienteTecnologico_Codigo = new bool[] {false} ;
         T001N13_A729AmbienteTecnologico_AreaTrabalhoDes = new String[] {""} ;
         T001N13_n729AmbienteTecnologico_AreaTrabalhoDes = new bool[] {false} ;
         T001N14_A127Sistema_Codigo = new int[1] ;
         T001N15_A351AmbienteTecnologico_Codigo = new int[1] ;
         T001N15_n351AmbienteTecnologico_Codigo = new bool[] {false} ;
         T001N15_A131Tecnologia_Codigo = new int[1] ;
         T001N16_A351AmbienteTecnologico_Codigo = new int[1] ;
         T001N16_n351AmbienteTecnologico_Codigo = new bool[] {false} ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.ambientetecnologico__default(),
            new Object[][] {
                new Object[] {
               T001N2_A351AmbienteTecnologico_Codigo, T001N2_A352AmbienteTecnologico_Descricao, T001N2_A976AmbienteTecnologico_TaxaEntregaDsnv, T001N2_n976AmbienteTecnologico_TaxaEntregaDsnv, T001N2_A977AmbienteTecnologico_TaxaEntregaMlhr, T001N2_n977AmbienteTecnologico_TaxaEntregaMlhr, T001N2_A978AmbienteTecnologico_LocPF, T001N2_n978AmbienteTecnologico_LocPF, T001N2_A1423AmbienteTecnologico_FatorAjuste, T001N2_n1423AmbienteTecnologico_FatorAjuste,
               T001N2_A353AmbienteTecnologico_Ativo, T001N2_A728AmbienteTecnologico_AreaTrabalhoCod
               }
               , new Object[] {
               T001N3_A351AmbienteTecnologico_Codigo, T001N3_A352AmbienteTecnologico_Descricao, T001N3_A976AmbienteTecnologico_TaxaEntregaDsnv, T001N3_n976AmbienteTecnologico_TaxaEntregaDsnv, T001N3_A977AmbienteTecnologico_TaxaEntregaMlhr, T001N3_n977AmbienteTecnologico_TaxaEntregaMlhr, T001N3_A978AmbienteTecnologico_LocPF, T001N3_n978AmbienteTecnologico_LocPF, T001N3_A1423AmbienteTecnologico_FatorAjuste, T001N3_n1423AmbienteTecnologico_FatorAjuste,
               T001N3_A353AmbienteTecnologico_Ativo, T001N3_A728AmbienteTecnologico_AreaTrabalhoCod
               }
               , new Object[] {
               T001N4_A729AmbienteTecnologico_AreaTrabalhoDes, T001N4_n729AmbienteTecnologico_AreaTrabalhoDes
               }
               , new Object[] {
               T001N5_A351AmbienteTecnologico_Codigo, T001N5_A352AmbienteTecnologico_Descricao, T001N5_A729AmbienteTecnologico_AreaTrabalhoDes, T001N5_n729AmbienteTecnologico_AreaTrabalhoDes, T001N5_A976AmbienteTecnologico_TaxaEntregaDsnv, T001N5_n976AmbienteTecnologico_TaxaEntregaDsnv, T001N5_A977AmbienteTecnologico_TaxaEntregaMlhr, T001N5_n977AmbienteTecnologico_TaxaEntregaMlhr, T001N5_A978AmbienteTecnologico_LocPF, T001N5_n978AmbienteTecnologico_LocPF,
               T001N5_A1423AmbienteTecnologico_FatorAjuste, T001N5_n1423AmbienteTecnologico_FatorAjuste, T001N5_A353AmbienteTecnologico_Ativo, T001N5_A728AmbienteTecnologico_AreaTrabalhoCod
               }
               , new Object[] {
               T001N6_A729AmbienteTecnologico_AreaTrabalhoDes, T001N6_n729AmbienteTecnologico_AreaTrabalhoDes
               }
               , new Object[] {
               T001N7_A351AmbienteTecnologico_Codigo
               }
               , new Object[] {
               T001N8_A351AmbienteTecnologico_Codigo
               }
               , new Object[] {
               T001N9_A351AmbienteTecnologico_Codigo
               }
               , new Object[] {
               T001N10_A351AmbienteTecnologico_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T001N13_A729AmbienteTecnologico_AreaTrabalhoDes, T001N13_n729AmbienteTecnologico_AreaTrabalhoDes
               }
               , new Object[] {
               T001N14_A127Sistema_Codigo
               }
               , new Object[] {
               T001N15_A351AmbienteTecnologico_Codigo, T001N15_A131Tecnologia_Codigo
               }
               , new Object[] {
               T001N16_A351AmbienteTecnologico_Codigo
               }
            }
         );
         Z353AmbienteTecnologico_Ativo = true;
         A353AmbienteTecnologico_Ativo = true;
         i353AmbienteTecnologico_Ativo = true;
         AV14Pgmname = "AmbienteTecnologico";
      }

      private short Z976AmbienteTecnologico_TaxaEntregaDsnv ;
      private short Z977AmbienteTecnologico_TaxaEntregaMlhr ;
      private short Z978AmbienteTecnologico_LocPF ;
      private short GxWebError ;
      private short gxcookieaux ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short A978AmbienteTecnologico_LocPF ;
      private short A977AmbienteTecnologico_TaxaEntregaMlhr ;
      private short A976AmbienteTecnologico_TaxaEntregaDsnv ;
      private short Gx_BScreen ;
      private short RcdFound62 ;
      private short GX_JID ;
      private short gxajaxcallmode ;
      private int wcpOAV12AmbienteTecnologico_Codigo ;
      private int Z351AmbienteTecnologico_Codigo ;
      private int Z728AmbienteTecnologico_AreaTrabalhoCod ;
      private int N728AmbienteTecnologico_AreaTrabalhoCod ;
      private int A728AmbienteTecnologico_AreaTrabalhoCod ;
      private int AV12AmbienteTecnologico_Codigo ;
      private int trnEnded ;
      private int A351AmbienteTecnologico_Codigo ;
      private int edtAmbienteTecnologico_Codigo_Enabled ;
      private int edtAmbienteTecnologico_Codigo_Visible ;
      private int bttBtn_trn_enter_Visible ;
      private int bttBtn_trn_enter_Enabled ;
      private int bttBtn_trn_cancel_Visible ;
      private int bttBtn_trn_delete_Visible ;
      private int bttBtn_trn_delete_Enabled ;
      private int edtAmbienteTecnologico_Descricao_Enabled ;
      private int edtAmbienteTecnologico_LocPF_Enabled ;
      private int edtAmbienteTecnologico_FatorAjuste_Enabled ;
      private int lblTextblockambientetecnologico_ativo_Visible ;
      private int edtAmbienteTecnologico_TaxaEntregaMlhr_Enabled ;
      private int edtAmbienteTecnologico_TaxaEntregaDsnv_Enabled ;
      private int AV13Insert_AmbienteTecnologico_AreaTrabalhoCod ;
      private int AV15GXV1 ;
      private int idxLst ;
      private decimal Z1423AmbienteTecnologico_FatorAjuste ;
      private decimal A1423AmbienteTecnologico_FatorAjuste ;
      private String sPrefix ;
      private String wcpOGx_mode ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String Gx_mode ;
      private String GXKey ;
      private String chkAmbienteTecnologico_Ativo_Internalname ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String edtAmbienteTecnologico_Descricao_Internalname ;
      private String edtAmbienteTecnologico_Codigo_Internalname ;
      private String edtAmbienteTecnologico_Codigo_Jsonclick ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTableactions_Internalname ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String bttBtn_trn_enter_Internalname ;
      private String bttBtn_trn_enter_Jsonclick ;
      private String bttBtn_trn_cancel_Internalname ;
      private String bttBtn_trn_cancel_Jsonclick ;
      private String bttBtn_trn_delete_Internalname ;
      private String bttBtn_trn_delete_Jsonclick ;
      private String tblTablecontent_Internalname ;
      private String tblTableattributes_Internalname ;
      private String lblTextblockambientetecnologico_descricao_Internalname ;
      private String lblTextblockambientetecnologico_descricao_Jsonclick ;
      private String edtAmbienteTecnologico_Descricao_Jsonclick ;
      private String lblTextblockambientetecnologico_taxaentregadsnv_Internalname ;
      private String lblTextblockambientetecnologico_taxaentregadsnv_Jsonclick ;
      private String lblTextblockambientetecnologico_taxaentregamlhr_Internalname ;
      private String lblTextblockambientetecnologico_taxaentregamlhr_Jsonclick ;
      private String lblTextblockambientetecnologico_locpf_Internalname ;
      private String lblTextblockambientetecnologico_locpf_Jsonclick ;
      private String edtAmbienteTecnologico_LocPF_Internalname ;
      private String edtAmbienteTecnologico_LocPF_Jsonclick ;
      private String lblTextblockambientetecnologico_fatorajuste_Internalname ;
      private String lblTextblockambientetecnologico_fatorajuste_Jsonclick ;
      private String edtAmbienteTecnologico_FatorAjuste_Internalname ;
      private String edtAmbienteTecnologico_FatorAjuste_Jsonclick ;
      private String lblTextblockambientetecnologico_ativo_Internalname ;
      private String lblTextblockambientetecnologico_ativo_Jsonclick ;
      private String tblTablemergedambientetecnologico_taxaentregamlhr_Internalname ;
      private String edtAmbienteTecnologico_TaxaEntregaMlhr_Internalname ;
      private String edtAmbienteTecnologico_TaxaEntregaMlhr_Jsonclick ;
      private String lblAmbientetecnologico_taxaentregamlhr_righttext_Internalname ;
      private String lblAmbientetecnologico_taxaentregamlhr_righttext_Jsonclick ;
      private String tblTablemergedambientetecnologico_taxaentregadsnv_Internalname ;
      private String edtAmbienteTecnologico_TaxaEntregaDsnv_Internalname ;
      private String edtAmbienteTecnologico_TaxaEntregaDsnv_Jsonclick ;
      private String lblAmbientetecnologico_taxaentregadsnv_righttext_Internalname ;
      private String lblAmbientetecnologico_taxaentregadsnv_righttext_Jsonclick ;
      private String AV14Pgmname ;
      private String Dvpanel_tableattributes_Width ;
      private String Dvpanel_tableattributes_Height ;
      private String Dvpanel_tableattributes_Cls ;
      private String Dvpanel_tableattributes_Title ;
      private String Dvpanel_tableattributes_Class ;
      private String Dvpanel_tableattributes_Iconposition ;
      private String forbiddenHiddens ;
      private String hsh ;
      private String sMode62 ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Dvpanel_tableattributes_Internalname ;
      private bool Z353AmbienteTecnologico_Ativo ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbErr ;
      private bool A353AmbienteTecnologico_Ativo ;
      private bool n976AmbienteTecnologico_TaxaEntregaDsnv ;
      private bool n977AmbienteTecnologico_TaxaEntregaMlhr ;
      private bool n978AmbienteTecnologico_LocPF ;
      private bool n1423AmbienteTecnologico_FatorAjuste ;
      private bool n351AmbienteTecnologico_Codigo ;
      private bool n729AmbienteTecnologico_AreaTrabalhoDes ;
      private bool Dvpanel_tableattributes_Collapsible ;
      private bool Dvpanel_tableattributes_Collapsed ;
      private bool Dvpanel_tableattributes_Enabled ;
      private bool Dvpanel_tableattributes_Autowidth ;
      private bool Dvpanel_tableattributes_Autoheight ;
      private bool Dvpanel_tableattributes_Showheader ;
      private bool Dvpanel_tableattributes_Showcollapseicon ;
      private bool Dvpanel_tableattributes_Autoscroll ;
      private bool Dvpanel_tableattributes_Visible ;
      private bool returnInSub ;
      private bool Gx_longc ;
      private bool i353AmbienteTecnologico_Ativo ;
      private String Z352AmbienteTecnologico_Descricao ;
      private String O352AmbienteTecnologico_Descricao ;
      private String A352AmbienteTecnologico_Descricao ;
      private String A729AmbienteTecnologico_AreaTrabalhoDes ;
      private String Z729AmbienteTecnologico_AreaTrabalhoDes ;
      private IGxSession AV9WebSession ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCheckbox chkAmbienteTecnologico_Ativo ;
      private IDataStoreProvider pr_default ;
      private String[] T001N4_A729AmbienteTecnologico_AreaTrabalhoDes ;
      private bool[] T001N4_n729AmbienteTecnologico_AreaTrabalhoDes ;
      private int[] T001N5_A351AmbienteTecnologico_Codigo ;
      private bool[] T001N5_n351AmbienteTecnologico_Codigo ;
      private String[] T001N5_A352AmbienteTecnologico_Descricao ;
      private String[] T001N5_A729AmbienteTecnologico_AreaTrabalhoDes ;
      private bool[] T001N5_n729AmbienteTecnologico_AreaTrabalhoDes ;
      private short[] T001N5_A976AmbienteTecnologico_TaxaEntregaDsnv ;
      private bool[] T001N5_n976AmbienteTecnologico_TaxaEntregaDsnv ;
      private short[] T001N5_A977AmbienteTecnologico_TaxaEntregaMlhr ;
      private bool[] T001N5_n977AmbienteTecnologico_TaxaEntregaMlhr ;
      private short[] T001N5_A978AmbienteTecnologico_LocPF ;
      private bool[] T001N5_n978AmbienteTecnologico_LocPF ;
      private decimal[] T001N5_A1423AmbienteTecnologico_FatorAjuste ;
      private bool[] T001N5_n1423AmbienteTecnologico_FatorAjuste ;
      private bool[] T001N5_A353AmbienteTecnologico_Ativo ;
      private int[] T001N5_A728AmbienteTecnologico_AreaTrabalhoCod ;
      private String[] T001N6_A729AmbienteTecnologico_AreaTrabalhoDes ;
      private bool[] T001N6_n729AmbienteTecnologico_AreaTrabalhoDes ;
      private int[] T001N7_A351AmbienteTecnologico_Codigo ;
      private bool[] T001N7_n351AmbienteTecnologico_Codigo ;
      private int[] T001N3_A351AmbienteTecnologico_Codigo ;
      private bool[] T001N3_n351AmbienteTecnologico_Codigo ;
      private String[] T001N3_A352AmbienteTecnologico_Descricao ;
      private short[] T001N3_A976AmbienteTecnologico_TaxaEntregaDsnv ;
      private bool[] T001N3_n976AmbienteTecnologico_TaxaEntregaDsnv ;
      private short[] T001N3_A977AmbienteTecnologico_TaxaEntregaMlhr ;
      private bool[] T001N3_n977AmbienteTecnologico_TaxaEntregaMlhr ;
      private short[] T001N3_A978AmbienteTecnologico_LocPF ;
      private bool[] T001N3_n978AmbienteTecnologico_LocPF ;
      private decimal[] T001N3_A1423AmbienteTecnologico_FatorAjuste ;
      private bool[] T001N3_n1423AmbienteTecnologico_FatorAjuste ;
      private bool[] T001N3_A353AmbienteTecnologico_Ativo ;
      private int[] T001N3_A728AmbienteTecnologico_AreaTrabalhoCod ;
      private int[] T001N8_A351AmbienteTecnologico_Codigo ;
      private bool[] T001N8_n351AmbienteTecnologico_Codigo ;
      private int[] T001N9_A351AmbienteTecnologico_Codigo ;
      private bool[] T001N9_n351AmbienteTecnologico_Codigo ;
      private int[] T001N2_A351AmbienteTecnologico_Codigo ;
      private bool[] T001N2_n351AmbienteTecnologico_Codigo ;
      private String[] T001N2_A352AmbienteTecnologico_Descricao ;
      private short[] T001N2_A976AmbienteTecnologico_TaxaEntregaDsnv ;
      private bool[] T001N2_n976AmbienteTecnologico_TaxaEntregaDsnv ;
      private short[] T001N2_A977AmbienteTecnologico_TaxaEntregaMlhr ;
      private bool[] T001N2_n977AmbienteTecnologico_TaxaEntregaMlhr ;
      private short[] T001N2_A978AmbienteTecnologico_LocPF ;
      private bool[] T001N2_n978AmbienteTecnologico_LocPF ;
      private decimal[] T001N2_A1423AmbienteTecnologico_FatorAjuste ;
      private bool[] T001N2_n1423AmbienteTecnologico_FatorAjuste ;
      private bool[] T001N2_A353AmbienteTecnologico_Ativo ;
      private int[] T001N2_A728AmbienteTecnologico_AreaTrabalhoCod ;
      private int[] T001N10_A351AmbienteTecnologico_Codigo ;
      private bool[] T001N10_n351AmbienteTecnologico_Codigo ;
      private String[] T001N13_A729AmbienteTecnologico_AreaTrabalhoDes ;
      private bool[] T001N13_n729AmbienteTecnologico_AreaTrabalhoDes ;
      private int[] T001N14_A127Sistema_Codigo ;
      private int[] T001N15_A351AmbienteTecnologico_Codigo ;
      private bool[] T001N15_n351AmbienteTecnologico_Codigo ;
      private int[] T001N15_A131Tecnologia_Codigo ;
      private int[] T001N16_A351AmbienteTecnologico_Codigo ;
      private bool[] T001N16_n351AmbienteTecnologico_Codigo ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV7WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV8TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV11TrnContextAtt ;
   }

   public class ambientetecnologico__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new UpdateCursor(def[9])
         ,new UpdateCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new ForEachCursor(def[12])
         ,new ForEachCursor(def[13])
         ,new ForEachCursor(def[14])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT001N5 ;
          prmT001N5 = new Object[] {
          new Object[] {"@AmbienteTecnologico_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001N4 ;
          prmT001N4 = new Object[] {
          new Object[] {"@AmbienteTecnologico_AreaTrabalhoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001N6 ;
          prmT001N6 = new Object[] {
          new Object[] {"@AmbienteTecnologico_AreaTrabalhoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001N7 ;
          prmT001N7 = new Object[] {
          new Object[] {"@AmbienteTecnologico_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001N3 ;
          prmT001N3 = new Object[] {
          new Object[] {"@AmbienteTecnologico_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001N8 ;
          prmT001N8 = new Object[] {
          new Object[] {"@AmbienteTecnologico_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001N9 ;
          prmT001N9 = new Object[] {
          new Object[] {"@AmbienteTecnologico_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001N2 ;
          prmT001N2 = new Object[] {
          new Object[] {"@AmbienteTecnologico_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001N10 ;
          prmT001N10 = new Object[] {
          new Object[] {"@AmbienteTecnologico_Descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AmbienteTecnologico_TaxaEntregaDsnv",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AmbienteTecnologico_TaxaEntregaMlhr",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AmbienteTecnologico_LocPF",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AmbienteTecnologico_FatorAjuste",SqlDbType.Decimal,8,4} ,
          new Object[] {"@AmbienteTecnologico_Ativo",SqlDbType.Bit,4,0} ,
          new Object[] {"@AmbienteTecnologico_AreaTrabalhoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001N11 ;
          prmT001N11 = new Object[] {
          new Object[] {"@AmbienteTecnologico_Descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AmbienteTecnologico_TaxaEntregaDsnv",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AmbienteTecnologico_TaxaEntregaMlhr",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AmbienteTecnologico_LocPF",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AmbienteTecnologico_FatorAjuste",SqlDbType.Decimal,8,4} ,
          new Object[] {"@AmbienteTecnologico_Ativo",SqlDbType.Bit,4,0} ,
          new Object[] {"@AmbienteTecnologico_AreaTrabalhoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AmbienteTecnologico_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001N12 ;
          prmT001N12 = new Object[] {
          new Object[] {"@AmbienteTecnologico_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001N13 ;
          prmT001N13 = new Object[] {
          new Object[] {"@AmbienteTecnologico_AreaTrabalhoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001N14 ;
          prmT001N14 = new Object[] {
          new Object[] {"@AmbienteTecnologico_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001N15 ;
          prmT001N15 = new Object[] {
          new Object[] {"@AmbienteTecnologico_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001N16 ;
          prmT001N16 = new Object[] {
          } ;
          def= new CursorDef[] {
              new CursorDef("T001N2", "SELECT [AmbienteTecnologico_Codigo], [AmbienteTecnologico_Descricao], [AmbienteTecnologico_TaxaEntregaDsnv], [AmbienteTecnologico_TaxaEntregaMlhr], [AmbienteTecnologico_LocPF], [AmbienteTecnologico_FatorAjuste], [AmbienteTecnologico_Ativo], [AmbienteTecnologico_AreaTrabalhoCod] AS AmbienteTecnologico_AreaTrabalhoCod FROM [AmbienteTecnologico] WITH (UPDLOCK) WHERE [AmbienteTecnologico_Codigo] = @AmbienteTecnologico_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT001N2,1,0,true,false )
             ,new CursorDef("T001N3", "SELECT [AmbienteTecnologico_Codigo], [AmbienteTecnologico_Descricao], [AmbienteTecnologico_TaxaEntregaDsnv], [AmbienteTecnologico_TaxaEntregaMlhr], [AmbienteTecnologico_LocPF], [AmbienteTecnologico_FatorAjuste], [AmbienteTecnologico_Ativo], [AmbienteTecnologico_AreaTrabalhoCod] AS AmbienteTecnologico_AreaTrabalhoCod FROM [AmbienteTecnologico] WITH (NOLOCK) WHERE [AmbienteTecnologico_Codigo] = @AmbienteTecnologico_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT001N3,1,0,true,false )
             ,new CursorDef("T001N4", "SELECT [AreaTrabalho_Descricao] AS AmbienteTecnologico_AreaTrabalhoDes FROM [AreaTrabalho] WITH (NOLOCK) WHERE [AreaTrabalho_Codigo] = @AmbienteTecnologico_AreaTrabalhoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT001N4,1,0,true,false )
             ,new CursorDef("T001N5", "SELECT TM1.[AmbienteTecnologico_Codigo], TM1.[AmbienteTecnologico_Descricao], T2.[AreaTrabalho_Descricao] AS AmbienteTecnologico_AreaTrabalhoDes, TM1.[AmbienteTecnologico_TaxaEntregaDsnv], TM1.[AmbienteTecnologico_TaxaEntregaMlhr], TM1.[AmbienteTecnologico_LocPF], TM1.[AmbienteTecnologico_FatorAjuste], TM1.[AmbienteTecnologico_Ativo], TM1.[AmbienteTecnologico_AreaTrabalhoCod] AS AmbienteTecnologico_AreaTrabalhoCod FROM ([AmbienteTecnologico] TM1 WITH (NOLOCK) INNER JOIN [AreaTrabalho] T2 WITH (NOLOCK) ON T2.[AreaTrabalho_Codigo] = TM1.[AmbienteTecnologico_AreaTrabalhoCod]) WHERE TM1.[AmbienteTecnologico_Codigo] = @AmbienteTecnologico_Codigo ORDER BY TM1.[AmbienteTecnologico_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT001N5,100,0,true,false )
             ,new CursorDef("T001N6", "SELECT [AreaTrabalho_Descricao] AS AmbienteTecnologico_AreaTrabalhoDes FROM [AreaTrabalho] WITH (NOLOCK) WHERE [AreaTrabalho_Codigo] = @AmbienteTecnologico_AreaTrabalhoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT001N6,1,0,true,false )
             ,new CursorDef("T001N7", "SELECT [AmbienteTecnologico_Codigo] FROM [AmbienteTecnologico] WITH (NOLOCK) WHERE [AmbienteTecnologico_Codigo] = @AmbienteTecnologico_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT001N7,1,0,true,false )
             ,new CursorDef("T001N8", "SELECT TOP 1 [AmbienteTecnologico_Codigo] FROM [AmbienteTecnologico] WITH (NOLOCK) WHERE ( [AmbienteTecnologico_Codigo] > @AmbienteTecnologico_Codigo) ORDER BY [AmbienteTecnologico_Codigo]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT001N8,1,0,true,true )
             ,new CursorDef("T001N9", "SELECT TOP 1 [AmbienteTecnologico_Codigo] FROM [AmbienteTecnologico] WITH (NOLOCK) WHERE ( [AmbienteTecnologico_Codigo] < @AmbienteTecnologico_Codigo) ORDER BY [AmbienteTecnologico_Codigo] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT001N9,1,0,true,true )
             ,new CursorDef("T001N10", "INSERT INTO [AmbienteTecnologico]([AmbienteTecnologico_Descricao], [AmbienteTecnologico_TaxaEntregaDsnv], [AmbienteTecnologico_TaxaEntregaMlhr], [AmbienteTecnologico_LocPF], [AmbienteTecnologico_FatorAjuste], [AmbienteTecnologico_Ativo], [AmbienteTecnologico_AreaTrabalhoCod]) VALUES(@AmbienteTecnologico_Descricao, @AmbienteTecnologico_TaxaEntregaDsnv, @AmbienteTecnologico_TaxaEntregaMlhr, @AmbienteTecnologico_LocPF, @AmbienteTecnologico_FatorAjuste, @AmbienteTecnologico_Ativo, @AmbienteTecnologico_AreaTrabalhoCod); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmT001N10)
             ,new CursorDef("T001N11", "UPDATE [AmbienteTecnologico] SET [AmbienteTecnologico_Descricao]=@AmbienteTecnologico_Descricao, [AmbienteTecnologico_TaxaEntregaDsnv]=@AmbienteTecnologico_TaxaEntregaDsnv, [AmbienteTecnologico_TaxaEntregaMlhr]=@AmbienteTecnologico_TaxaEntregaMlhr, [AmbienteTecnologico_LocPF]=@AmbienteTecnologico_LocPF, [AmbienteTecnologico_FatorAjuste]=@AmbienteTecnologico_FatorAjuste, [AmbienteTecnologico_Ativo]=@AmbienteTecnologico_Ativo, [AmbienteTecnologico_AreaTrabalhoCod]=@AmbienteTecnologico_AreaTrabalhoCod  WHERE [AmbienteTecnologico_Codigo] = @AmbienteTecnologico_Codigo", GxErrorMask.GX_NOMASK,prmT001N11)
             ,new CursorDef("T001N12", "DELETE FROM [AmbienteTecnologico]  WHERE [AmbienteTecnologico_Codigo] = @AmbienteTecnologico_Codigo", GxErrorMask.GX_NOMASK,prmT001N12)
             ,new CursorDef("T001N13", "SELECT [AreaTrabalho_Descricao] AS AmbienteTecnologico_AreaTrabalhoDes FROM [AreaTrabalho] WITH (NOLOCK) WHERE [AreaTrabalho_Codigo] = @AmbienteTecnologico_AreaTrabalhoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT001N13,1,0,true,false )
             ,new CursorDef("T001N14", "SELECT TOP 1 [Sistema_Codigo] FROM [Sistema] WITH (NOLOCK) WHERE [AmbienteTecnologico_Codigo] = @AmbienteTecnologico_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT001N14,1,0,true,true )
             ,new CursorDef("T001N15", "SELECT TOP 1 [AmbienteTecnologico_Codigo], [Tecnologia_Codigo] FROM [AmbienteTecnologicoTecnologias] WITH (NOLOCK) WHERE [AmbienteTecnologico_Codigo] = @AmbienteTecnologico_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT001N15,1,0,true,true )
             ,new CursorDef("T001N16", "SELECT [AmbienteTecnologico_Codigo] FROM [AmbienteTecnologico] WITH (NOLOCK) ORDER BY [AmbienteTecnologico_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT001N16,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((short[]) buf[2])[0] = rslt.getShort(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((short[]) buf[4])[0] = rslt.getShort(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((short[]) buf[6])[0] = rslt.getShort(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((decimal[]) buf[8])[0] = rslt.getDecimal(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((bool[]) buf[10])[0] = rslt.getBool(7) ;
                ((int[]) buf[11])[0] = rslt.getInt(8) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((short[]) buf[2])[0] = rslt.getShort(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((short[]) buf[4])[0] = rslt.getShort(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((short[]) buf[6])[0] = rslt.getShort(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((decimal[]) buf[8])[0] = rslt.getDecimal(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((bool[]) buf[10])[0] = rslt.getBool(7) ;
                ((int[]) buf[11])[0] = rslt.getInt(8) ;
                return;
             case 2 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((short[]) buf[4])[0] = rslt.getShort(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((short[]) buf[6])[0] = rslt.getShort(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((short[]) buf[8])[0] = rslt.getShort(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((decimal[]) buf[10])[0] = rslt.getDecimal(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((bool[]) buf[12])[0] = rslt.getBool(8) ;
                ((int[]) buf[13])[0] = rslt.getInt(9) ;
                return;
             case 4 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 11 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 12 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 13 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 14 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 1 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 6 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 7 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 8 :
                stmt.SetParameter(1, (String)parms[0]);
                if ( (bool)parms[1] )
                {
                   stmt.setNull( 2 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(2, (short)parms[2]);
                }
                if ( (bool)parms[3] )
                {
                   stmt.setNull( 3 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(3, (short)parms[4]);
                }
                if ( (bool)parms[5] )
                {
                   stmt.setNull( 4 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(4, (short)parms[6]);
                }
                if ( (bool)parms[7] )
                {
                   stmt.setNull( 5 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(5, (decimal)parms[8]);
                }
                stmt.SetParameter(6, (bool)parms[9]);
                stmt.SetParameter(7, (int)parms[10]);
                return;
             case 9 :
                stmt.SetParameter(1, (String)parms[0]);
                if ( (bool)parms[1] )
                {
                   stmt.setNull( 2 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(2, (short)parms[2]);
                }
                if ( (bool)parms[3] )
                {
                   stmt.setNull( 3 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(3, (short)parms[4]);
                }
                if ( (bool)parms[5] )
                {
                   stmt.setNull( 4 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(4, (short)parms[6]);
                }
                if ( (bool)parms[7] )
                {
                   stmt.setNull( 5 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(5, (decimal)parms[8]);
                }
                stmt.SetParameter(6, (bool)parms[9]);
                stmt.SetParameter(7, (int)parms[10]);
                if ( (bool)parms[11] )
                {
                   stmt.setNull( 8 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(8, (int)parms[12]);
                }
                return;
             case 10 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 11 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 12 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 13 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
       }
    }

 }

}
