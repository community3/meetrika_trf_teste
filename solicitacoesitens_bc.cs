/*
               File: SolicitacoesItens_BC
        Description: Solicitacoes Itens
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 23:1:35.36
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class solicitacoesitens_bc : GXHttpHandler, IGxSilentTrn
   {
      public solicitacoesitens_bc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public solicitacoesitens_bc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      protected void INITTRN( )
      {
      }

      public void GetInsDefault( )
      {
         ReadRow1T68( ) ;
         standaloneNotModal( ) ;
         InitializeNonKey1T68( ) ;
         standaloneModal( ) ;
         AddRow1T68( ) ;
         Gx_mode = "INS";
         return  ;
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               Z447SolicitacoesItens_Codigo = A447SolicitacoesItens_Codigo;
               SetMode( "UPD") ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      public bool Reindex( )
      {
         return true ;
      }

      protected void CONFIRM_1T0( )
      {
         BeforeValidate1T68( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls1T68( ) ;
            }
            else
            {
               CheckExtendedTable1T68( ) ;
               if ( AnyError == 0 )
               {
                  ZM1T68( 2) ;
                  ZM1T68( 3) ;
               }
               CloseExtendedTableCursors1T68( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
         }
      }

      protected void ZM1T68( short GX_JID )
      {
         if ( ( GX_JID == 1 ) || ( GX_JID == 0 ) )
         {
            Z439Solicitacoes_Codigo = A439Solicitacoes_Codigo;
            Z161FuncaoUsuario_Codigo = A161FuncaoUsuario_Codigo;
         }
         if ( ( GX_JID == 2 ) || ( GX_JID == 0 ) )
         {
         }
         if ( ( GX_JID == 3 ) || ( GX_JID == 0 ) )
         {
         }
         if ( GX_JID == -1 )
         {
            Z447SolicitacoesItens_Codigo = A447SolicitacoesItens_Codigo;
            Z448SolicitacoesItens_descricao = A448SolicitacoesItens_descricao;
            Z449SolicitacoesItens_Arquivo = A449SolicitacoesItens_Arquivo;
            Z439Solicitacoes_Codigo = A439Solicitacoes_Codigo;
            Z161FuncaoUsuario_Codigo = A161FuncaoUsuario_Codigo;
         }
      }

      protected void standaloneNotModal( )
      {
      }

      protected void standaloneModal( )
      {
      }

      protected void Load1T68( )
      {
         /* Using cursor BC001T6 */
         pr_default.execute(4, new Object[] {A447SolicitacoesItens_Codigo});
         if ( (pr_default.getStatus(4) != 101) )
         {
            RcdFound68 = 1;
            A448SolicitacoesItens_descricao = BC001T6_A448SolicitacoesItens_descricao[0];
            n448SolicitacoesItens_descricao = BC001T6_n448SolicitacoesItens_descricao[0];
            A439Solicitacoes_Codigo = BC001T6_A439Solicitacoes_Codigo[0];
            A161FuncaoUsuario_Codigo = BC001T6_A161FuncaoUsuario_Codigo[0];
            A449SolicitacoesItens_Arquivo = BC001T6_A449SolicitacoesItens_Arquivo[0];
            n449SolicitacoesItens_Arquivo = BC001T6_n449SolicitacoesItens_Arquivo[0];
            ZM1T68( -1) ;
         }
         pr_default.close(4);
         OnLoadActions1T68( ) ;
      }

      protected void OnLoadActions1T68( )
      {
      }

      protected void CheckExtendedTable1T68( )
      {
         standaloneModal( ) ;
         /* Using cursor BC001T4 */
         pr_default.execute(2, new Object[] {A439Solicitacoes_Codigo});
         if ( (pr_default.getStatus(2) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Solicitacoes'.", "ForeignKeyNotFound", 1, "SOLICITACOES_CODIGO");
            AnyError = 1;
         }
         pr_default.close(2);
         /* Using cursor BC001T5 */
         pr_default.execute(3, new Object[] {A161FuncaoUsuario_Codigo});
         if ( (pr_default.getStatus(3) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Modulo Funcoes'.", "ForeignKeyNotFound", 1, "FUNCAOUSUARIO_CODIGO");
            AnyError = 1;
         }
         pr_default.close(3);
      }

      protected void CloseExtendedTableCursors1T68( )
      {
         pr_default.close(2);
         pr_default.close(3);
      }

      protected void enableDisable( )
      {
      }

      protected void GetKey1T68( )
      {
         /* Using cursor BC001T7 */
         pr_default.execute(5, new Object[] {A447SolicitacoesItens_Codigo});
         if ( (pr_default.getStatus(5) != 101) )
         {
            RcdFound68 = 1;
         }
         else
         {
            RcdFound68 = 0;
         }
         pr_default.close(5);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor BC001T3 */
         pr_default.execute(1, new Object[] {A447SolicitacoesItens_Codigo});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM1T68( 1) ;
            RcdFound68 = 1;
            A447SolicitacoesItens_Codigo = BC001T3_A447SolicitacoesItens_Codigo[0];
            A448SolicitacoesItens_descricao = BC001T3_A448SolicitacoesItens_descricao[0];
            n448SolicitacoesItens_descricao = BC001T3_n448SolicitacoesItens_descricao[0];
            A439Solicitacoes_Codigo = BC001T3_A439Solicitacoes_Codigo[0];
            A161FuncaoUsuario_Codigo = BC001T3_A161FuncaoUsuario_Codigo[0];
            A449SolicitacoesItens_Arquivo = BC001T3_A449SolicitacoesItens_Arquivo[0];
            n449SolicitacoesItens_Arquivo = BC001T3_n449SolicitacoesItens_Arquivo[0];
            Z447SolicitacoesItens_Codigo = A447SolicitacoesItens_Codigo;
            sMode68 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal( ) ;
            Load1T68( ) ;
            if ( AnyError == 1 )
            {
               RcdFound68 = 0;
               InitializeNonKey1T68( ) ;
            }
            Gx_mode = sMode68;
         }
         else
         {
            RcdFound68 = 0;
            InitializeNonKey1T68( ) ;
            sMode68 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal( ) ;
            Gx_mode = sMode68;
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey1T68( ) ;
         if ( RcdFound68 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
         }
         getByPrimaryKey( ) ;
      }

      protected void insert_Check( )
      {
         CONFIRM_1T0( ) ;
         IsConfirmed = 0;
      }

      protected void update_Check( )
      {
         insert_Check( ) ;
      }

      protected void delete_Check( )
      {
         insert_Check( ) ;
      }

      protected void CheckOptimisticConcurrency1T68( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor BC001T2 */
            pr_default.execute(0, new Object[] {A447SolicitacoesItens_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"SolicitacoesItens"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) || ( Z439Solicitacoes_Codigo != BC001T2_A439Solicitacoes_Codigo[0] ) || ( Z161FuncaoUsuario_Codigo != BC001T2_A161FuncaoUsuario_Codigo[0] ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"SolicitacoesItens"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert1T68( )
      {
         BeforeValidate1T68( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable1T68( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM1T68( 0) ;
            CheckOptimisticConcurrency1T68( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm1T68( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert1T68( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC001T8 */
                     pr_default.execute(6, new Object[] {n448SolicitacoesItens_descricao, A448SolicitacoesItens_descricao, n449SolicitacoesItens_Arquivo, A449SolicitacoesItens_Arquivo, A439Solicitacoes_Codigo, A161FuncaoUsuario_Codigo});
                     A447SolicitacoesItens_Codigo = BC001T8_A447SolicitacoesItens_Codigo[0];
                     pr_default.close(6);
                     dsDefault.SmartCacheProvider.SetUpdated("SolicitacoesItens") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load1T68( ) ;
            }
            EndLevel1T68( ) ;
         }
         CloseExtendedTableCursors1T68( ) ;
      }

      protected void Update1T68( )
      {
         BeforeValidate1T68( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable1T68( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency1T68( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm1T68( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate1T68( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC001T9 */
                     pr_default.execute(7, new Object[] {n448SolicitacoesItens_descricao, A448SolicitacoesItens_descricao, A439Solicitacoes_Codigo, A161FuncaoUsuario_Codigo, A447SolicitacoesItens_Codigo});
                     pr_default.close(7);
                     dsDefault.SmartCacheProvider.SetUpdated("SolicitacoesItens") ;
                     if ( (pr_default.getStatus(7) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"SolicitacoesItens"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate1T68( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           getByPrimaryKey( ) ;
                           GX_msglist.addItem(context.GetMessage( "GXM_sucupdated", ""), 0, "", true);
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel1T68( ) ;
         }
         CloseExtendedTableCursors1T68( ) ;
      }

      protected void DeferredUpdate1T68( )
      {
         if ( AnyError == 0 )
         {
            /* Using cursor BC001T10 */
            pr_default.execute(8, new Object[] {n449SolicitacoesItens_Arquivo, A449SolicitacoesItens_Arquivo, A447SolicitacoesItens_Codigo});
            pr_default.close(8);
            dsDefault.SmartCacheProvider.SetUpdated("SolicitacoesItens") ;
         }
      }

      protected void delete( )
      {
         Gx_mode = "DLT";
         BeforeValidate1T68( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency1T68( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls1T68( ) ;
            AfterConfirm1T68( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete1T68( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor BC001T11 */
                  pr_default.execute(9, new Object[] {A447SolicitacoesItens_Codigo});
                  pr_default.close(9);
                  dsDefault.SmartCacheProvider.SetUpdated("SolicitacoesItens") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_sucdeleted", ""), 0, "", true);
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode68 = Gx_mode;
         Gx_mode = "DLT";
         EndLevel1T68( ) ;
         Gx_mode = sMode68;
      }

      protected void OnDeleteControls1T68( )
      {
         standaloneModal( ) ;
         /* No delete mode formulas found. */
      }

      protected void EndLevel1T68( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete1T68( ) ;
         }
         if ( AnyError == 0 )
         {
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanKeyStart1T68( )
      {
         /* Using cursor BC001T12 */
         pr_default.execute(10, new Object[] {A447SolicitacoesItens_Codigo});
         RcdFound68 = 0;
         if ( (pr_default.getStatus(10) != 101) )
         {
            RcdFound68 = 1;
            A447SolicitacoesItens_Codigo = BC001T12_A447SolicitacoesItens_Codigo[0];
            A448SolicitacoesItens_descricao = BC001T12_A448SolicitacoesItens_descricao[0];
            n448SolicitacoesItens_descricao = BC001T12_n448SolicitacoesItens_descricao[0];
            A439Solicitacoes_Codigo = BC001T12_A439Solicitacoes_Codigo[0];
            A161FuncaoUsuario_Codigo = BC001T12_A161FuncaoUsuario_Codigo[0];
            A449SolicitacoesItens_Arquivo = BC001T12_A449SolicitacoesItens_Arquivo[0];
            n449SolicitacoesItens_Arquivo = BC001T12_n449SolicitacoesItens_Arquivo[0];
         }
         /* Load Subordinate Levels */
      }

      protected void ScanKeyNext1T68( )
      {
         /* Scan next routine */
         pr_default.readNext(10);
         RcdFound68 = 0;
         ScanKeyLoad1T68( ) ;
      }

      protected void ScanKeyLoad1T68( )
      {
         sMode68 = Gx_mode;
         Gx_mode = "DSP";
         if ( (pr_default.getStatus(10) != 101) )
         {
            RcdFound68 = 1;
            A447SolicitacoesItens_Codigo = BC001T12_A447SolicitacoesItens_Codigo[0];
            A448SolicitacoesItens_descricao = BC001T12_A448SolicitacoesItens_descricao[0];
            n448SolicitacoesItens_descricao = BC001T12_n448SolicitacoesItens_descricao[0];
            A439Solicitacoes_Codigo = BC001T12_A439Solicitacoes_Codigo[0];
            A161FuncaoUsuario_Codigo = BC001T12_A161FuncaoUsuario_Codigo[0];
            A449SolicitacoesItens_Arquivo = BC001T12_A449SolicitacoesItens_Arquivo[0];
            n449SolicitacoesItens_Arquivo = BC001T12_n449SolicitacoesItens_Arquivo[0];
         }
         Gx_mode = sMode68;
      }

      protected void ScanKeyEnd1T68( )
      {
         pr_default.close(10);
      }

      protected void AfterConfirm1T68( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert1T68( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate1T68( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete1T68( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete1T68( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate1T68( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes1T68( )
      {
      }

      protected void AddRow1T68( )
      {
         VarsToRow68( bcSolicitacoesItens) ;
      }

      protected void ReadRow1T68( )
      {
         RowToVars68( bcSolicitacoesItens, 1) ;
      }

      protected void InitializeNonKey1T68( )
      {
         A439Solicitacoes_Codigo = 0;
         A448SolicitacoesItens_descricao = "";
         n448SolicitacoesItens_descricao = false;
         A449SolicitacoesItens_Arquivo = "";
         n449SolicitacoesItens_Arquivo = false;
         A161FuncaoUsuario_Codigo = 0;
         Z439Solicitacoes_Codigo = 0;
         Z161FuncaoUsuario_Codigo = 0;
      }

      protected void InitAll1T68( )
      {
         A447SolicitacoesItens_Codigo = 0;
         InitializeNonKey1T68( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      public void VarsToRow68( SdtSolicitacoesItens obj68 )
      {
         obj68.gxTpr_Mode = Gx_mode;
         obj68.gxTpr_Solicitacoes_codigo = A439Solicitacoes_Codigo;
         obj68.gxTpr_Solicitacoesitens_descricao = A448SolicitacoesItens_descricao;
         obj68.gxTpr_Solicitacoesitens_arquivo = A449SolicitacoesItens_Arquivo;
         obj68.gxTpr_Funcaousuario_codigo = A161FuncaoUsuario_Codigo;
         obj68.gxTpr_Solicitacoesitens_codigo = A447SolicitacoesItens_Codigo;
         obj68.gxTpr_Solicitacoesitens_codigo_Z = Z447SolicitacoesItens_Codigo;
         obj68.gxTpr_Solicitacoes_codigo_Z = Z439Solicitacoes_Codigo;
         obj68.gxTpr_Funcaousuario_codigo_Z = Z161FuncaoUsuario_Codigo;
         obj68.gxTpr_Solicitacoesitens_descricao_N = (short)(Convert.ToInt16(n448SolicitacoesItens_descricao));
         obj68.gxTpr_Solicitacoesitens_arquivo_N = (short)(Convert.ToInt16(n449SolicitacoesItens_Arquivo));
         obj68.gxTpr_Mode = Gx_mode;
         return  ;
      }

      public void KeyVarsToRow68( SdtSolicitacoesItens obj68 )
      {
         obj68.gxTpr_Solicitacoesitens_codigo = A447SolicitacoesItens_Codigo;
         return  ;
      }

      public void RowToVars68( SdtSolicitacoesItens obj68 ,
                               int forceLoad )
      {
         Gx_mode = obj68.gxTpr_Mode;
         A439Solicitacoes_Codigo = obj68.gxTpr_Solicitacoes_codigo;
         A448SolicitacoesItens_descricao = obj68.gxTpr_Solicitacoesitens_descricao;
         n448SolicitacoesItens_descricao = false;
         A449SolicitacoesItens_Arquivo = obj68.gxTpr_Solicitacoesitens_arquivo;
         n449SolicitacoesItens_Arquivo = false;
         A161FuncaoUsuario_Codigo = obj68.gxTpr_Funcaousuario_codigo;
         A447SolicitacoesItens_Codigo = obj68.gxTpr_Solicitacoesitens_codigo;
         Z447SolicitacoesItens_Codigo = obj68.gxTpr_Solicitacoesitens_codigo_Z;
         Z439Solicitacoes_Codigo = obj68.gxTpr_Solicitacoes_codigo_Z;
         Z161FuncaoUsuario_Codigo = obj68.gxTpr_Funcaousuario_codigo_Z;
         n448SolicitacoesItens_descricao = (bool)(Convert.ToBoolean(obj68.gxTpr_Solicitacoesitens_descricao_N));
         n449SolicitacoesItens_Arquivo = (bool)(Convert.ToBoolean(obj68.gxTpr_Solicitacoesitens_arquivo_N));
         Gx_mode = obj68.gxTpr_Mode;
         return  ;
      }

      public void LoadKey( Object[] obj )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         A447SolicitacoesItens_Codigo = (int)getParm(obj,0);
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         InitializeNonKey1T68( ) ;
         ScanKeyStart1T68( ) ;
         if ( RcdFound68 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
            Z447SolicitacoesItens_Codigo = A447SolicitacoesItens_Codigo;
         }
         ZM1T68( -1) ;
         OnLoadActions1T68( ) ;
         AddRow1T68( ) ;
         ScanKeyEnd1T68( ) ;
         if ( RcdFound68 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "");
            AnyError = 1;
         }
         context.GX_msglist = BackMsgLst;
      }

      public void Load( )
      {
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         RowToVars68( bcSolicitacoesItens, 0) ;
         ScanKeyStart1T68( ) ;
         if ( RcdFound68 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
            Z447SolicitacoesItens_Codigo = A447SolicitacoesItens_Codigo;
         }
         ZM1T68( -1) ;
         OnLoadActions1T68( ) ;
         AddRow1T68( ) ;
         ScanKeyEnd1T68( ) ;
         if ( RcdFound68 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "");
            AnyError = 1;
         }
         context.GX_msglist = BackMsgLst;
      }

      public void Save( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         IsConfirmed = 1;
         RowToVars68( bcSolicitacoesItens, 0) ;
         nKeyPressed = 1;
         GetKey1T68( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            Insert1T68( ) ;
         }
         else
         {
            if ( RcdFound68 == 1 )
            {
               if ( A447SolicitacoesItens_Codigo != Z447SolicitacoesItens_Codigo )
               {
                  A447SolicitacoesItens_Codigo = Z447SolicitacoesItens_Codigo;
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
               }
               else
               {
                  Gx_mode = "UPD";
                  /* Update record */
                  Update1T68( ) ;
               }
            }
            else
            {
               if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else
               {
                  if ( A447SolicitacoesItens_Codigo != Z447SolicitacoesItens_Codigo )
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     else
                     {
                        Gx_mode = "INS";
                        /* Insert record */
                        Insert1T68( ) ;
                     }
                  }
                  else
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                        AnyError = 1;
                     }
                     else
                     {
                        Gx_mode = "INS";
                        /* Insert record */
                        Insert1T68( ) ;
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         VarsToRow68( bcSolicitacoesItens) ;
         context.GX_msglist = BackMsgLst;
         return  ;
      }

      public void Check( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         RowToVars68( bcSolicitacoesItens, 0) ;
         nKeyPressed = 3;
         IsConfirmed = 0;
         GetKey1T68( ) ;
         if ( RcdFound68 == 1 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
               AnyError = 1;
            }
            else if ( A447SolicitacoesItens_Codigo != Z447SolicitacoesItens_Codigo )
            {
               A447SolicitacoesItens_Codigo = Z447SolicitacoesItens_Codigo;
               GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "DuplicatePrimaryKey", 1, "");
               AnyError = 1;
            }
            else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               delete_Check( ) ;
            }
            else
            {
               Gx_mode = "UPD";
               update_Check( ) ;
            }
         }
         else
         {
            if ( A447SolicitacoesItens_Codigo != Z447SolicitacoesItens_Codigo )
            {
               Gx_mode = "INS";
               insert_Check( ) ;
            }
            else
            {
               if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                  AnyError = 1;
               }
               else
               {
                  Gx_mode = "INS";
                  insert_Check( ) ;
               }
            }
         }
         pr_default.close(1);
         context.RollbackDataStores( "SolicitacoesItens_BC");
         VarsToRow68( bcSolicitacoesItens) ;
         context.GX_msglist = BackMsgLst;
         return  ;
      }

      public int Errors( )
      {
         if ( AnyError == 0 )
         {
            return (int)(0) ;
         }
         return (int)(1) ;
      }

      public msglist GetMessages( )
      {
         return LclMsgLst ;
      }

      public String GetMode( )
      {
         Gx_mode = bcSolicitacoesItens.gxTpr_Mode;
         return Gx_mode ;
      }

      public void SetMode( String lMode )
      {
         Gx_mode = lMode;
         bcSolicitacoesItens.gxTpr_Mode = Gx_mode;
         return  ;
      }

      public void SetSDT( GxSilentTrnSdt sdt ,
                          short sdtToBc )
      {
         if ( sdt != bcSolicitacoesItens )
         {
            bcSolicitacoesItens = (SdtSolicitacoesItens)(sdt);
            if ( StringUtil.StrCmp(bcSolicitacoesItens.gxTpr_Mode, "") == 0 )
            {
               bcSolicitacoesItens.gxTpr_Mode = "INS";
            }
            if ( sdtToBc == 1 )
            {
               VarsToRow68( bcSolicitacoesItens) ;
            }
            else
            {
               RowToVars68( bcSolicitacoesItens, 1) ;
            }
         }
         else
         {
            if ( StringUtil.StrCmp(bcSolicitacoesItens.gxTpr_Mode, "") == 0 )
            {
               bcSolicitacoesItens.gxTpr_Mode = "INS";
            }
         }
         return  ;
      }

      public void ReloadFromSDT( )
      {
         RowToVars68( bcSolicitacoesItens, 1) ;
         return  ;
      }

      public SdtSolicitacoesItens SolicitacoesItens_BC
      {
         get {
            return bcSolicitacoesItens ;
         }

      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         createObjects();
         initialize();
      }

      protected override void createObjects( )
      {
      }

      protected void Process( )
      {
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
      }

      public override void initialize( )
      {
         scmdbuf = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Gx_mode = "";
         Z448SolicitacoesItens_descricao = "";
         A448SolicitacoesItens_descricao = "";
         Z449SolicitacoesItens_Arquivo = "";
         A449SolicitacoesItens_Arquivo = "";
         BC001T6_A447SolicitacoesItens_Codigo = new int[1] ;
         BC001T6_A448SolicitacoesItens_descricao = new String[] {""} ;
         BC001T6_n448SolicitacoesItens_descricao = new bool[] {false} ;
         BC001T6_A439Solicitacoes_Codigo = new int[1] ;
         BC001T6_A161FuncaoUsuario_Codigo = new int[1] ;
         BC001T6_A449SolicitacoesItens_Arquivo = new String[] {""} ;
         BC001T6_n449SolicitacoesItens_Arquivo = new bool[] {false} ;
         BC001T4_A439Solicitacoes_Codigo = new int[1] ;
         BC001T5_A161FuncaoUsuario_Codigo = new int[1] ;
         BC001T7_A447SolicitacoesItens_Codigo = new int[1] ;
         BC001T3_A447SolicitacoesItens_Codigo = new int[1] ;
         BC001T3_A448SolicitacoesItens_descricao = new String[] {""} ;
         BC001T3_n448SolicitacoesItens_descricao = new bool[] {false} ;
         BC001T3_A439Solicitacoes_Codigo = new int[1] ;
         BC001T3_A161FuncaoUsuario_Codigo = new int[1] ;
         BC001T3_A449SolicitacoesItens_Arquivo = new String[] {""} ;
         BC001T3_n449SolicitacoesItens_Arquivo = new bool[] {false} ;
         sMode68 = "";
         BC001T2_A447SolicitacoesItens_Codigo = new int[1] ;
         BC001T2_A448SolicitacoesItens_descricao = new String[] {""} ;
         BC001T2_n448SolicitacoesItens_descricao = new bool[] {false} ;
         BC001T2_A439Solicitacoes_Codigo = new int[1] ;
         BC001T2_A161FuncaoUsuario_Codigo = new int[1] ;
         BC001T2_A449SolicitacoesItens_Arquivo = new String[] {""} ;
         BC001T2_n449SolicitacoesItens_Arquivo = new bool[] {false} ;
         BC001T8_A447SolicitacoesItens_Codigo = new int[1] ;
         BC001T12_A447SolicitacoesItens_Codigo = new int[1] ;
         BC001T12_A448SolicitacoesItens_descricao = new String[] {""} ;
         BC001T12_n448SolicitacoesItens_descricao = new bool[] {false} ;
         BC001T12_A439Solicitacoes_Codigo = new int[1] ;
         BC001T12_A161FuncaoUsuario_Codigo = new int[1] ;
         BC001T12_A449SolicitacoesItens_Arquivo = new String[] {""} ;
         BC001T12_n449SolicitacoesItens_Arquivo = new bool[] {false} ;
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.solicitacoesitens_bc__default(),
            new Object[][] {
                new Object[] {
               BC001T2_A447SolicitacoesItens_Codigo, BC001T2_A448SolicitacoesItens_descricao, BC001T2_n448SolicitacoesItens_descricao, BC001T2_A439Solicitacoes_Codigo, BC001T2_A161FuncaoUsuario_Codigo, BC001T2_A449SolicitacoesItens_Arquivo, BC001T2_n449SolicitacoesItens_Arquivo
               }
               , new Object[] {
               BC001T3_A447SolicitacoesItens_Codigo, BC001T3_A448SolicitacoesItens_descricao, BC001T3_n448SolicitacoesItens_descricao, BC001T3_A439Solicitacoes_Codigo, BC001T3_A161FuncaoUsuario_Codigo, BC001T3_A449SolicitacoesItens_Arquivo, BC001T3_n449SolicitacoesItens_Arquivo
               }
               , new Object[] {
               BC001T4_A439Solicitacoes_Codigo
               }
               , new Object[] {
               BC001T5_A161FuncaoUsuario_Codigo
               }
               , new Object[] {
               BC001T6_A447SolicitacoesItens_Codigo, BC001T6_A448SolicitacoesItens_descricao, BC001T6_n448SolicitacoesItens_descricao, BC001T6_A439Solicitacoes_Codigo, BC001T6_A161FuncaoUsuario_Codigo, BC001T6_A449SolicitacoesItens_Arquivo, BC001T6_n449SolicitacoesItens_Arquivo
               }
               , new Object[] {
               BC001T7_A447SolicitacoesItens_Codigo
               }
               , new Object[] {
               BC001T8_A447SolicitacoesItens_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               BC001T12_A447SolicitacoesItens_Codigo, BC001T12_A448SolicitacoesItens_descricao, BC001T12_n448SolicitacoesItens_descricao, BC001T12_A439Solicitacoes_Codigo, BC001T12_A161FuncaoUsuario_Codigo, BC001T12_A449SolicitacoesItens_Arquivo, BC001T12_n449SolicitacoesItens_Arquivo
               }
            }
         );
         INITTRN();
         /* Execute Start event if defined. */
         standaloneNotModal( ) ;
      }

      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short GX_JID ;
      private short RcdFound68 ;
      private int trnEnded ;
      private int Z447SolicitacoesItens_Codigo ;
      private int A447SolicitacoesItens_Codigo ;
      private int Z439Solicitacoes_Codigo ;
      private int A439Solicitacoes_Codigo ;
      private int Z161FuncaoUsuario_Codigo ;
      private int A161FuncaoUsuario_Codigo ;
      private String scmdbuf ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String Gx_mode ;
      private String sMode68 ;
      private bool n448SolicitacoesItens_descricao ;
      private bool n449SolicitacoesItens_Arquivo ;
      private String Z448SolicitacoesItens_descricao ;
      private String A448SolicitacoesItens_descricao ;
      private String Z449SolicitacoesItens_Arquivo ;
      private String A449SolicitacoesItens_Arquivo ;
      private SdtSolicitacoesItens bcSolicitacoesItens ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] BC001T6_A447SolicitacoesItens_Codigo ;
      private String[] BC001T6_A448SolicitacoesItens_descricao ;
      private bool[] BC001T6_n448SolicitacoesItens_descricao ;
      private int[] BC001T6_A439Solicitacoes_Codigo ;
      private int[] BC001T6_A161FuncaoUsuario_Codigo ;
      private String[] BC001T6_A449SolicitacoesItens_Arquivo ;
      private bool[] BC001T6_n449SolicitacoesItens_Arquivo ;
      private int[] BC001T4_A439Solicitacoes_Codigo ;
      private int[] BC001T5_A161FuncaoUsuario_Codigo ;
      private int[] BC001T7_A447SolicitacoesItens_Codigo ;
      private int[] BC001T3_A447SolicitacoesItens_Codigo ;
      private String[] BC001T3_A448SolicitacoesItens_descricao ;
      private bool[] BC001T3_n448SolicitacoesItens_descricao ;
      private int[] BC001T3_A439Solicitacoes_Codigo ;
      private int[] BC001T3_A161FuncaoUsuario_Codigo ;
      private String[] BC001T3_A449SolicitacoesItens_Arquivo ;
      private bool[] BC001T3_n449SolicitacoesItens_Arquivo ;
      private int[] BC001T2_A447SolicitacoesItens_Codigo ;
      private String[] BC001T2_A448SolicitacoesItens_descricao ;
      private bool[] BC001T2_n448SolicitacoesItens_descricao ;
      private int[] BC001T2_A439Solicitacoes_Codigo ;
      private int[] BC001T2_A161FuncaoUsuario_Codigo ;
      private String[] BC001T2_A449SolicitacoesItens_Arquivo ;
      private bool[] BC001T2_n449SolicitacoesItens_Arquivo ;
      private int[] BC001T8_A447SolicitacoesItens_Codigo ;
      private int[] BC001T12_A447SolicitacoesItens_Codigo ;
      private String[] BC001T12_A448SolicitacoesItens_descricao ;
      private bool[] BC001T12_n448SolicitacoesItens_descricao ;
      private int[] BC001T12_A439Solicitacoes_Codigo ;
      private int[] BC001T12_A161FuncaoUsuario_Codigo ;
      private String[] BC001T12_A449SolicitacoesItens_Arquivo ;
      private bool[] BC001T12_n449SolicitacoesItens_Arquivo ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
   }

   public class solicitacoesitens_bc__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new UpdateCursor(def[7])
         ,new UpdateCursor(def[8])
         ,new UpdateCursor(def[9])
         ,new ForEachCursor(def[10])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmBC001T6 ;
          prmBC001T6 = new Object[] {
          new Object[] {"@SolicitacoesItens_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC001T4 ;
          prmBC001T4 = new Object[] {
          new Object[] {"@Solicitacoes_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC001T5 ;
          prmBC001T5 = new Object[] {
          new Object[] {"@FuncaoUsuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC001T7 ;
          prmBC001T7 = new Object[] {
          new Object[] {"@SolicitacoesItens_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC001T3 ;
          prmBC001T3 = new Object[] {
          new Object[] {"@SolicitacoesItens_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC001T2 ;
          prmBC001T2 = new Object[] {
          new Object[] {"@SolicitacoesItens_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC001T8 ;
          prmBC001T8 = new Object[] {
          new Object[] {"@SolicitacoesItens_descricao",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@SolicitacoesItens_Arquivo",SqlDbType.VarBinary,1024,0} ,
          new Object[] {"@Solicitacoes_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@FuncaoUsuario_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC001T9 ;
          prmBC001T9 = new Object[] {
          new Object[] {"@SolicitacoesItens_descricao",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@Solicitacoes_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@FuncaoUsuario_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@SolicitacoesItens_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC001T10 ;
          prmBC001T10 = new Object[] {
          new Object[] {"@SolicitacoesItens_Arquivo",SqlDbType.VarBinary,1024,0} ,
          new Object[] {"@SolicitacoesItens_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC001T11 ;
          prmBC001T11 = new Object[] {
          new Object[] {"@SolicitacoesItens_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC001T12 ;
          prmBC001T12 = new Object[] {
          new Object[] {"@SolicitacoesItens_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("BC001T2", "SELECT [SolicitacoesItens_Codigo], [SolicitacoesItens_descricao], [Solicitacoes_Codigo], [FuncaoUsuario_Codigo], [SolicitacoesItens_Arquivo] FROM [SolicitacoesItens] WITH (UPDLOCK) WHERE [SolicitacoesItens_Codigo] = @SolicitacoesItens_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC001T2,1,0,true,false )
             ,new CursorDef("BC001T3", "SELECT [SolicitacoesItens_Codigo], [SolicitacoesItens_descricao], [Solicitacoes_Codigo], [FuncaoUsuario_Codigo], [SolicitacoesItens_Arquivo] FROM [SolicitacoesItens] WITH (NOLOCK) WHERE [SolicitacoesItens_Codigo] = @SolicitacoesItens_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC001T3,1,0,true,false )
             ,new CursorDef("BC001T4", "SELECT [Solicitacoes_Codigo] FROM [Solicitacoes] WITH (NOLOCK) WHERE [Solicitacoes_Codigo] = @Solicitacoes_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC001T4,1,0,true,false )
             ,new CursorDef("BC001T5", "SELECT [FuncaoUsuario_Codigo] FROM [ModuloFuncoes] WITH (NOLOCK) WHERE [FuncaoUsuario_Codigo] = @FuncaoUsuario_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC001T5,1,0,true,false )
             ,new CursorDef("BC001T6", "SELECT TM1.[SolicitacoesItens_Codigo], TM1.[SolicitacoesItens_descricao], TM1.[Solicitacoes_Codigo], TM1.[FuncaoUsuario_Codigo], TM1.[SolicitacoesItens_Arquivo] FROM [SolicitacoesItens] TM1 WITH (NOLOCK) WHERE TM1.[SolicitacoesItens_Codigo] = @SolicitacoesItens_Codigo ORDER BY TM1.[SolicitacoesItens_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmBC001T6,100,0,true,false )
             ,new CursorDef("BC001T7", "SELECT [SolicitacoesItens_Codigo] FROM [SolicitacoesItens] WITH (NOLOCK) WHERE [SolicitacoesItens_Codigo] = @SolicitacoesItens_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmBC001T7,1,0,true,false )
             ,new CursorDef("BC001T8", "INSERT INTO [SolicitacoesItens]([SolicitacoesItens_descricao], [SolicitacoesItens_Arquivo], [Solicitacoes_Codigo], [FuncaoUsuario_Codigo]) VALUES(@SolicitacoesItens_descricao, @SolicitacoesItens_Arquivo, @Solicitacoes_Codigo, @FuncaoUsuario_Codigo); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmBC001T8)
             ,new CursorDef("BC001T9", "UPDATE [SolicitacoesItens] SET [SolicitacoesItens_descricao]=@SolicitacoesItens_descricao, [Solicitacoes_Codigo]=@Solicitacoes_Codigo, [FuncaoUsuario_Codigo]=@FuncaoUsuario_Codigo  WHERE [SolicitacoesItens_Codigo] = @SolicitacoesItens_Codigo", GxErrorMask.GX_NOMASK,prmBC001T9)
             ,new CursorDef("BC001T10", "UPDATE [SolicitacoesItens] SET [SolicitacoesItens_Arquivo]=@SolicitacoesItens_Arquivo  WHERE [SolicitacoesItens_Codigo] = @SolicitacoesItens_Codigo", GxErrorMask.GX_NOMASK,prmBC001T10)
             ,new CursorDef("BC001T11", "DELETE FROM [SolicitacoesItens]  WHERE [SolicitacoesItens_Codigo] = @SolicitacoesItens_Codigo", GxErrorMask.GX_NOMASK,prmBC001T11)
             ,new CursorDef("BC001T12", "SELECT TM1.[SolicitacoesItens_Codigo], TM1.[SolicitacoesItens_descricao], TM1.[Solicitacoes_Codigo], TM1.[FuncaoUsuario_Codigo], TM1.[SolicitacoesItens_Arquivo] FROM [SolicitacoesItens] TM1 WITH (NOLOCK) WHERE TM1.[SolicitacoesItens_Codigo] = @SolicitacoesItens_Codigo ORDER BY TM1.[SolicitacoesItens_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmBC001T12,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getLongVarchar(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((String[]) buf[5])[0] = rslt.getBLOBFile(5, "tmp", "") ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getLongVarchar(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((String[]) buf[5])[0] = rslt.getBLOBFile(5, "tmp", "") ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getLongVarchar(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((String[]) buf[5])[0] = rslt.getBLOBFile(5, "tmp", "") ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 10 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getLongVarchar(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((String[]) buf[5])[0] = rslt.getBLOBFile(5, "tmp", "") ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 2 , SqlDbType.VarBinary );
                }
                else
                {
                   stmt.SetParameter(2, (String)parms[3]);
                }
                stmt.SetParameter(3, (int)parms[4]);
                stmt.SetParameter(4, (int)parms[5]);
                return;
             case 7 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                stmt.SetParameter(3, (int)parms[3]);
                stmt.SetParameter(4, (int)parms[4]);
                return;
             case 8 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.VarBinary );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 10 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
