/*
               File: Geral_Tp_UOGeral_UnidadeOrganizacionalWC
        Description: Geral_Tp_UOGeral_Unidade Organizacional WC
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/18/2020 12:50:48.25
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class geral_tp_uogeral_unidadeorganizacionalwc : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public geral_tp_uogeral_unidadeorganizacionalwc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public geral_tp_uogeral_unidadeorganizacionalwc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_TpUo_Codigo )
      {
         this.AV7TpUo_Codigo = aP0_TpUo_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
         cmbavOrderedby = new GXCombobox();
         chkavUnidadeorganizacional_ativo = new GXCheckbox();
         cmbavDynamicfiltersselector1 = new GXCombobox();
         dynavEstado_uf1 = new GXCombobox();
         dynavUnidadeorganizacional_vinculada1 = new GXCombobox();
         cmbavDynamicfiltersselector2 = new GXCombobox();
         dynavEstado_uf2 = new GXCombobox();
         dynavUnidadeorganizacional_vinculada2 = new GXCombobox();
         cmbavDynamicfiltersselector3 = new GXCombobox();
         dynavEstado_uf3 = new GXCombobox();
         dynavUnidadeorganizacional_vinculada3 = new GXCombobox();
         dynUnidadeOrganizacional_Vinculada = new GXCombobox();
         chkavDynamicfiltersenabled2 = new GXCheckbox();
         chkavDynamicfiltersenabled3 = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  AV7TpUo_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7TpUo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7TpUo_Codigo), 6, 0)));
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(int)AV7TpUo_Codigo});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vESTADO_UF1") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  GXDLVvESTADO_UF1D82( ) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vUNIDADEORGANIZACIONAL_VINCULADA1") == 0 )
               {
                  AV64Unidadeorganizacional_codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  GXDLVvUNIDADEORGANIZACIONAL_VINCULADA1D82( AV64Unidadeorganizacional_codigo) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vESTADO_UF2") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  GXDLVvESTADO_UF2D82( ) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vUNIDADEORGANIZACIONAL_VINCULADA2") == 0 )
               {
                  AV64Unidadeorganizacional_codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  GXDLVvUNIDADEORGANIZACIONAL_VINCULADA2D82( AV64Unidadeorganizacional_codigo) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vESTADO_UF3") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  GXDLVvESTADO_UF3D82( ) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vUNIDADEORGANIZACIONAL_VINCULADA3") == 0 )
               {
                  AV64Unidadeorganizacional_codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  GXDLVvUNIDADEORGANIZACIONAL_VINCULADA3D82( AV64Unidadeorganizacional_codigo) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"UNIDADEORGANIZACIONAL_VINCULADA") == 0 )
               {
                  AV64Unidadeorganizacional_codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  GXDLAUNIDADEORGANIZACIONAL_VINCULADAD82( AV64Unidadeorganizacional_codigo) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
               {
                  nRC_GXsfl_83 = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  nGXsfl_83_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  sGXsfl_83_idx = GetNextPar( );
                  sPrefix = GetNextPar( );
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxnrGrid_newrow( ) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
               {
                  subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  AV14OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
                  AV15OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
                  AV17DynamicFiltersSelector1 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17DynamicFiltersSelector1", AV17DynamicFiltersSelector1);
                  AV18UnidadeOrganizacional_Nome1 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18UnidadeOrganizacional_Nome1", AV18UnidadeOrganizacional_Nome1);
                  AV19Estado_UF1 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19Estado_UF1", AV19Estado_UF1);
                  AV20UnidadeOrganizacional_Vinculada1 = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20UnidadeOrganizacional_Vinculada1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20UnidadeOrganizacional_Vinculada1), 6, 0)));
                  AV22DynamicFiltersSelector2 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22DynamicFiltersSelector2", AV22DynamicFiltersSelector2);
                  AV23UnidadeOrganizacional_Nome2 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23UnidadeOrganizacional_Nome2", AV23UnidadeOrganizacional_Nome2);
                  AV24Estado_UF2 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24Estado_UF2", AV24Estado_UF2);
                  AV25UnidadeOrganizacional_Vinculada2 = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25UnidadeOrganizacional_Vinculada2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV25UnidadeOrganizacional_Vinculada2), 6, 0)));
                  AV27DynamicFiltersSelector3 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27DynamicFiltersSelector3", AV27DynamicFiltersSelector3);
                  AV28UnidadeOrganizacional_Nome3 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV28UnidadeOrganizacional_Nome3", AV28UnidadeOrganizacional_Nome3);
                  AV29Estado_UF3 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29Estado_UF3", AV29Estado_UF3);
                  AV30UnidadeOrganizacional_Vinculada3 = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30UnidadeOrganizacional_Vinculada3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV30UnidadeOrganizacional_Vinculada3), 6, 0)));
                  AV21DynamicFiltersEnabled2 = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21DynamicFiltersEnabled2", AV21DynamicFiltersEnabled2);
                  AV26DynamicFiltersEnabled3 = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26DynamicFiltersEnabled3", AV26DynamicFiltersEnabled3);
                  AV40TFUnidadeOrganizacional_Nome = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV40TFUnidadeOrganizacional_Nome", AV40TFUnidadeOrganizacional_Nome);
                  AV41TFUnidadeOrganizacional_Nome_Sel = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV41TFUnidadeOrganizacional_Nome_Sel", AV41TFUnidadeOrganizacional_Nome_Sel);
                  AV44TFEstado_UF = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV44TFEstado_UF", AV44TFEstado_UF);
                  AV45TFEstado_UF_Sel = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV45TFEstado_UF_Sel", AV45TFEstado_UF_Sel);
                  AV48TFUnidadeOrganizacional_Vinculada = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV48TFUnidadeOrganizacional_Vinculada", AV48TFUnidadeOrganizacional_Vinculada);
                  AV49TFUnidadeOrganizacional_Vinculada_Sel = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV49TFUnidadeOrganizacional_Vinculada_Sel", StringUtil.LTrim( StringUtil.Str( (decimal)(AV49TFUnidadeOrganizacional_Vinculada_Sel), 6, 0)));
                  AV7TpUo_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7TpUo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7TpUo_Codigo), 6, 0)));
                  AV42ddo_UnidadeOrganizacional_NomeTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV42ddo_UnidadeOrganizacional_NomeTitleControlIdToReplace", AV42ddo_UnidadeOrganizacional_NomeTitleControlIdToReplace);
                  AV46ddo_Estado_UFTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV46ddo_Estado_UFTitleControlIdToReplace", AV46ddo_Estado_UFTitleControlIdToReplace);
                  AV50ddo_UnidadeOrganizacional_VinculadaTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV50ddo_UnidadeOrganizacional_VinculadaTitleControlIdToReplace", AV50ddo_UnidadeOrganizacional_VinculadaTitleControlIdToReplace);
                  ajax_req_read_hidden_sdt(GetNextPar( ), AV6WWPContext);
                  AV62Pgmname = GetNextPar( );
                  AV16UnidadeOrganizacional_Ativo = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16UnidadeOrganizacional_Ativo", AV16UnidadeOrganizacional_Ativo);
                  AV52TFUnidadeOrganizacional_Vinculada_SelDsc = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV52TFUnidadeOrganizacional_Vinculada_SelDsc", AV52TFUnidadeOrganizacional_Vinculada_SelDsc);
                  ajax_req_read_hidden_sdt(GetNextPar( ), AV11GridState);
                  AV32DynamicFiltersIgnoreFirst = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV32DynamicFiltersIgnoreFirst", AV32DynamicFiltersIgnoreFirst);
                  AV31DynamicFiltersRemoving = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV31DynamicFiltersRemoving", AV31DynamicFiltersRemoving);
                  A611UnidadeOrganizacional_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  AV64Unidadeorganizacional_codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  sPrefix = GetNextPar( );
                  init_default_properties( ) ;
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV17DynamicFiltersSelector1, AV18UnidadeOrganizacional_Nome1, AV19Estado_UF1, AV20UnidadeOrganizacional_Vinculada1, AV22DynamicFiltersSelector2, AV23UnidadeOrganizacional_Nome2, AV24Estado_UF2, AV25UnidadeOrganizacional_Vinculada2, AV27DynamicFiltersSelector3, AV28UnidadeOrganizacional_Nome3, AV29Estado_UF3, AV30UnidadeOrganizacional_Vinculada3, AV21DynamicFiltersEnabled2, AV26DynamicFiltersEnabled3, AV40TFUnidadeOrganizacional_Nome, AV41TFUnidadeOrganizacional_Nome_Sel, AV44TFEstado_UF, AV45TFEstado_UF_Sel, AV48TFUnidadeOrganizacional_Vinculada, AV49TFUnidadeOrganizacional_Vinculada_Sel, AV7TpUo_Codigo, AV42ddo_UnidadeOrganizacional_NomeTitleControlIdToReplace, AV46ddo_Estado_UFTitleControlIdToReplace, AV50ddo_UnidadeOrganizacional_VinculadaTitleControlIdToReplace, AV6WWPContext, AV62Pgmname, AV16UnidadeOrganizacional_Ativo, AV52TFUnidadeOrganizacional_Vinculada_SelDsc, AV11GridState, AV32DynamicFiltersIgnoreFirst, AV31DynamicFiltersRemoving, A611UnidadeOrganizacional_Codigo, AV64Unidadeorganizacional_codigo, sPrefix) ;
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
                  return  ;
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PAD82( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV62Pgmname = "Geral_Tp_UOGeral_UnidadeOrganizacionalWC";
               context.Gx_err = 0;
               GXVvESTADO_UF1_htmlD82( ) ;
               GXVvESTADO_UF2_htmlD82( ) ;
               GXVvESTADO_UF3_htmlD82( ) ;
               WSD82( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "Geral_Tp_UOGeral_Unidade Organizacional WC") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?202051812504894");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
            context.WriteHtmlText( "<body") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("geral_tp_uogeral_unidadeorganizacionalwc.aspx") + "?" + UrlEncode("" +AV7TpUo_Codigo)+"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV14OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vORDEREDDSC", StringUtil.BoolToStr( AV15OrderedDsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vDYNAMICFILTERSSELECTOR1", AV17DynamicFiltersSelector1);
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vUNIDADEORGANIZACIONAL_NOME1", StringUtil.RTrim( AV18UnidadeOrganizacional_Nome1));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vESTADO_UF1", StringUtil.RTrim( AV19Estado_UF1));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vUNIDADEORGANIZACIONAL_VINCULADA1", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV20UnidadeOrganizacional_Vinculada1), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vDYNAMICFILTERSSELECTOR2", AV22DynamicFiltersSelector2);
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vUNIDADEORGANIZACIONAL_NOME2", StringUtil.RTrim( AV23UnidadeOrganizacional_Nome2));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vESTADO_UF2", StringUtil.RTrim( AV24Estado_UF2));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vUNIDADEORGANIZACIONAL_VINCULADA2", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV25UnidadeOrganizacional_Vinculada2), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vDYNAMICFILTERSSELECTOR3", AV27DynamicFiltersSelector3);
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vUNIDADEORGANIZACIONAL_NOME3", StringUtil.RTrim( AV28UnidadeOrganizacional_Nome3));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vESTADO_UF3", StringUtil.RTrim( AV29Estado_UF3));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vUNIDADEORGANIZACIONAL_VINCULADA3", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV30UnidadeOrganizacional_Vinculada3), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vDYNAMICFILTERSENABLED2", StringUtil.BoolToStr( AV21DynamicFiltersEnabled2));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vDYNAMICFILTERSENABLED3", StringUtil.BoolToStr( AV26DynamicFiltersEnabled3));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFUNIDADEORGANIZACIONAL_NOME", StringUtil.RTrim( AV40TFUnidadeOrganizacional_Nome));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFUNIDADEORGANIZACIONAL_NOME_SEL", StringUtil.RTrim( AV41TFUnidadeOrganizacional_Nome_Sel));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFESTADO_UF", StringUtil.RTrim( AV44TFEstado_UF));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFESTADO_UF_SEL", StringUtil.RTrim( AV45TFEstado_UF_Sel));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFUNIDADEORGANIZACIONAL_VINCULADA", StringUtil.RTrim( AV48TFUnidadeOrganizacional_Vinculada));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFUNIDADEORGANIZACIONAL_VINCULADA_SEL", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV49TFUnidadeOrganizacional_Vinculada_Sel), 6, 0, ",", "")));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, sPrefix+"nRC_GXsfl_83", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_83), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV54GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV55GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vDDO_TITLESETTINGSICONS", AV51DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vDDO_TITLESETTINGSICONS", AV51DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vUNIDADEORGANIZACIONAL_NOMETITLEFILTERDATA", AV39UnidadeOrganizacional_NomeTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vUNIDADEORGANIZACIONAL_NOMETITLEFILTERDATA", AV39UnidadeOrganizacional_NomeTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vESTADO_UFTITLEFILTERDATA", AV43Estado_UFTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vESTADO_UFTITLEFILTERDATA", AV43Estado_UFTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vUNIDADEORGANIZACIONAL_VINCULADATITLEFILTERDATA", AV47UnidadeOrganizacional_VinculadaTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vUNIDADEORGANIZACIONAL_VINCULADATITLEFILTERDATA", AV47UnidadeOrganizacional_VinculadaTitleFilterData);
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOAV7TpUo_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOAV7TpUo_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vTPUO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7TpUo_Codigo), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vWWPCONTEXT", AV6WWPContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vWWPCONTEXT", AV6WWPContext);
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"vPGMNAME", StringUtil.RTrim( AV62Pgmname));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vGRIDSTATE", AV11GridState);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vGRIDSTATE", AV11GridState);
         }
         GxWebStd.gx_boolean_hidden_field( context, sPrefix+"vDYNAMICFILTERSIGNOREFIRST", AV32DynamicFiltersIgnoreFirst);
         GxWebStd.gx_boolean_hidden_field( context, sPrefix+"vDYNAMICFILTERSREMOVING", AV31DynamicFiltersRemoving);
         GxWebStd.gx_hidden_field( context, sPrefix+"vCODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV56Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vUNIDADEORGANIZACIONAL_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV64Unidadeorganizacional_codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_UNIDADEORGANIZACIONAL_NOME_Caption", StringUtil.RTrim( Ddo_unidadeorganizacional_nome_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_UNIDADEORGANIZACIONAL_NOME_Tooltip", StringUtil.RTrim( Ddo_unidadeorganizacional_nome_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_UNIDADEORGANIZACIONAL_NOME_Cls", StringUtil.RTrim( Ddo_unidadeorganizacional_nome_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_UNIDADEORGANIZACIONAL_NOME_Filteredtext_set", StringUtil.RTrim( Ddo_unidadeorganizacional_nome_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_UNIDADEORGANIZACIONAL_NOME_Selectedvalue_set", StringUtil.RTrim( Ddo_unidadeorganizacional_nome_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_UNIDADEORGANIZACIONAL_NOME_Dropdownoptionstype", StringUtil.RTrim( Ddo_unidadeorganizacional_nome_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_UNIDADEORGANIZACIONAL_NOME_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_unidadeorganizacional_nome_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_UNIDADEORGANIZACIONAL_NOME_Includesortasc", StringUtil.BoolToStr( Ddo_unidadeorganizacional_nome_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_UNIDADEORGANIZACIONAL_NOME_Includesortdsc", StringUtil.BoolToStr( Ddo_unidadeorganizacional_nome_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_UNIDADEORGANIZACIONAL_NOME_Sortedstatus", StringUtil.RTrim( Ddo_unidadeorganizacional_nome_Sortedstatus));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_UNIDADEORGANIZACIONAL_NOME_Includefilter", StringUtil.BoolToStr( Ddo_unidadeorganizacional_nome_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_UNIDADEORGANIZACIONAL_NOME_Filtertype", StringUtil.RTrim( Ddo_unidadeorganizacional_nome_Filtertype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_UNIDADEORGANIZACIONAL_NOME_Filterisrange", StringUtil.BoolToStr( Ddo_unidadeorganizacional_nome_Filterisrange));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_UNIDADEORGANIZACIONAL_NOME_Includedatalist", StringUtil.BoolToStr( Ddo_unidadeorganizacional_nome_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_UNIDADEORGANIZACIONAL_NOME_Datalisttype", StringUtil.RTrim( Ddo_unidadeorganizacional_nome_Datalisttype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_UNIDADEORGANIZACIONAL_NOME_Datalistproc", StringUtil.RTrim( Ddo_unidadeorganizacional_nome_Datalistproc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_UNIDADEORGANIZACIONAL_NOME_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_unidadeorganizacional_nome_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_UNIDADEORGANIZACIONAL_NOME_Sortasc", StringUtil.RTrim( Ddo_unidadeorganizacional_nome_Sortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_UNIDADEORGANIZACIONAL_NOME_Sortdsc", StringUtil.RTrim( Ddo_unidadeorganizacional_nome_Sortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_UNIDADEORGANIZACIONAL_NOME_Loadingdata", StringUtil.RTrim( Ddo_unidadeorganizacional_nome_Loadingdata));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_UNIDADEORGANIZACIONAL_NOME_Cleanfilter", StringUtil.RTrim( Ddo_unidadeorganizacional_nome_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_UNIDADEORGANIZACIONAL_NOME_Noresultsfound", StringUtil.RTrim( Ddo_unidadeorganizacional_nome_Noresultsfound));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_UNIDADEORGANIZACIONAL_NOME_Searchbuttontext", StringUtil.RTrim( Ddo_unidadeorganizacional_nome_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ESTADO_UF_Caption", StringUtil.RTrim( Ddo_estado_uf_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ESTADO_UF_Tooltip", StringUtil.RTrim( Ddo_estado_uf_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ESTADO_UF_Cls", StringUtil.RTrim( Ddo_estado_uf_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ESTADO_UF_Filteredtext_set", StringUtil.RTrim( Ddo_estado_uf_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ESTADO_UF_Selectedvalue_set", StringUtil.RTrim( Ddo_estado_uf_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ESTADO_UF_Dropdownoptionstype", StringUtil.RTrim( Ddo_estado_uf_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ESTADO_UF_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_estado_uf_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ESTADO_UF_Includesortasc", StringUtil.BoolToStr( Ddo_estado_uf_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ESTADO_UF_Includesortdsc", StringUtil.BoolToStr( Ddo_estado_uf_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ESTADO_UF_Sortedstatus", StringUtil.RTrim( Ddo_estado_uf_Sortedstatus));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ESTADO_UF_Includefilter", StringUtil.BoolToStr( Ddo_estado_uf_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ESTADO_UF_Filtertype", StringUtil.RTrim( Ddo_estado_uf_Filtertype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ESTADO_UF_Filterisrange", StringUtil.BoolToStr( Ddo_estado_uf_Filterisrange));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ESTADO_UF_Includedatalist", StringUtil.BoolToStr( Ddo_estado_uf_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ESTADO_UF_Datalisttype", StringUtil.RTrim( Ddo_estado_uf_Datalisttype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ESTADO_UF_Datalistproc", StringUtil.RTrim( Ddo_estado_uf_Datalistproc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ESTADO_UF_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_estado_uf_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ESTADO_UF_Sortasc", StringUtil.RTrim( Ddo_estado_uf_Sortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ESTADO_UF_Sortdsc", StringUtil.RTrim( Ddo_estado_uf_Sortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ESTADO_UF_Loadingdata", StringUtil.RTrim( Ddo_estado_uf_Loadingdata));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ESTADO_UF_Cleanfilter", StringUtil.RTrim( Ddo_estado_uf_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ESTADO_UF_Noresultsfound", StringUtil.RTrim( Ddo_estado_uf_Noresultsfound));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ESTADO_UF_Searchbuttontext", StringUtil.RTrim( Ddo_estado_uf_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_UNIDADEORGANIZACIONAL_VINCULADA_Caption", StringUtil.RTrim( Ddo_unidadeorganizacional_vinculada_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_UNIDADEORGANIZACIONAL_VINCULADA_Tooltip", StringUtil.RTrim( Ddo_unidadeorganizacional_vinculada_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_UNIDADEORGANIZACIONAL_VINCULADA_Cls", StringUtil.RTrim( Ddo_unidadeorganizacional_vinculada_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_UNIDADEORGANIZACIONAL_VINCULADA_Filteredtext_set", StringUtil.RTrim( Ddo_unidadeorganizacional_vinculada_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_UNIDADEORGANIZACIONAL_VINCULADA_Selectedvalue_set", StringUtil.RTrim( Ddo_unidadeorganizacional_vinculada_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_UNIDADEORGANIZACIONAL_VINCULADA_Selectedtext_set", StringUtil.RTrim( Ddo_unidadeorganizacional_vinculada_Selectedtext_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_UNIDADEORGANIZACIONAL_VINCULADA_Dropdownoptionstype", StringUtil.RTrim( Ddo_unidadeorganizacional_vinculada_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_UNIDADEORGANIZACIONAL_VINCULADA_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_unidadeorganizacional_vinculada_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_UNIDADEORGANIZACIONAL_VINCULADA_Includesortasc", StringUtil.BoolToStr( Ddo_unidadeorganizacional_vinculada_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_UNIDADEORGANIZACIONAL_VINCULADA_Includesortdsc", StringUtil.BoolToStr( Ddo_unidadeorganizacional_vinculada_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_UNIDADEORGANIZACIONAL_VINCULADA_Sortedstatus", StringUtil.RTrim( Ddo_unidadeorganizacional_vinculada_Sortedstatus));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_UNIDADEORGANIZACIONAL_VINCULADA_Includefilter", StringUtil.BoolToStr( Ddo_unidadeorganizacional_vinculada_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_UNIDADEORGANIZACIONAL_VINCULADA_Filtertype", StringUtil.RTrim( Ddo_unidadeorganizacional_vinculada_Filtertype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_UNIDADEORGANIZACIONAL_VINCULADA_Filterisrange", StringUtil.BoolToStr( Ddo_unidadeorganizacional_vinculada_Filterisrange));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_UNIDADEORGANIZACIONAL_VINCULADA_Includedatalist", StringUtil.BoolToStr( Ddo_unidadeorganizacional_vinculada_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_UNIDADEORGANIZACIONAL_VINCULADA_Datalisttype", StringUtil.RTrim( Ddo_unidadeorganizacional_vinculada_Datalisttype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_UNIDADEORGANIZACIONAL_VINCULADA_Datalistproc", StringUtil.RTrim( Ddo_unidadeorganizacional_vinculada_Datalistproc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_UNIDADEORGANIZACIONAL_VINCULADA_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_unidadeorganizacional_vinculada_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_UNIDADEORGANIZACIONAL_VINCULADA_Sortasc", StringUtil.RTrim( Ddo_unidadeorganizacional_vinculada_Sortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_UNIDADEORGANIZACIONAL_VINCULADA_Sortdsc", StringUtil.RTrim( Ddo_unidadeorganizacional_vinculada_Sortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_UNIDADEORGANIZACIONAL_VINCULADA_Loadingdata", StringUtil.RTrim( Ddo_unidadeorganizacional_vinculada_Loadingdata));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_UNIDADEORGANIZACIONAL_VINCULADA_Cleanfilter", StringUtil.RTrim( Ddo_unidadeorganizacional_vinculada_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_UNIDADEORGANIZACIONAL_VINCULADA_Noresultsfound", StringUtil.RTrim( Ddo_unidadeorganizacional_vinculada_Noresultsfound));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_UNIDADEORGANIZACIONAL_VINCULADA_Searchbuttontext", StringUtil.RTrim( Ddo_unidadeorganizacional_vinculada_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_UNIDADEORGANIZACIONAL_NOME_Activeeventkey", StringUtil.RTrim( Ddo_unidadeorganizacional_nome_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_UNIDADEORGANIZACIONAL_NOME_Filteredtext_get", StringUtil.RTrim( Ddo_unidadeorganizacional_nome_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_UNIDADEORGANIZACIONAL_NOME_Selectedvalue_get", StringUtil.RTrim( Ddo_unidadeorganizacional_nome_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ESTADO_UF_Activeeventkey", StringUtil.RTrim( Ddo_estado_uf_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ESTADO_UF_Filteredtext_get", StringUtil.RTrim( Ddo_estado_uf_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_ESTADO_UF_Selectedvalue_get", StringUtil.RTrim( Ddo_estado_uf_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_UNIDADEORGANIZACIONAL_VINCULADA_Activeeventkey", StringUtil.RTrim( Ddo_unidadeorganizacional_vinculada_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_UNIDADEORGANIZACIONAL_VINCULADA_Filteredtext_get", StringUtil.RTrim( Ddo_unidadeorganizacional_vinculada_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_UNIDADEORGANIZACIONAL_VINCULADA_Selectedvalue_get", StringUtil.RTrim( Ddo_unidadeorganizacional_vinculada_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_UNIDADEORGANIZACIONAL_VINCULADA_Selectedtext_get", StringUtil.RTrim( Ddo_unidadeorganizacional_vinculada_Selectedtext_get));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseFormD82( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("geral_tp_uogeral_unidadeorganizacionalwc.js", "?202051812505078");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            context.WriteHtmlTextNl( "</form>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "Geral_Tp_UOGeral_UnidadeOrganizacionalWC" ;
      }

      public override String GetPgmdesc( )
      {
         return "Geral_Tp_UOGeral_Unidade Organizacional WC" ;
      }

      protected void WBD80( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "geral_tp_uogeral_unidadeorganizacionalwc.aspx");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
               context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
            }
            wb_table1_2_D82( true) ;
         }
         else
         {
            wb_table1_2_D82( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_D82e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtTpUo_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A609TpUo_Codigo), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A609TpUo_Codigo), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtTpUo_Codigo_Jsonclick, 0, "Attribute", "", "", "", edtTpUo_Codigo_Visible, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_Geral_Tp_UOGeral_UnidadeOrganizacionalWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 96,'" + sPrefix + "',false,'" + sGXsfl_83_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled2_Internalname, StringUtil.BoolToStr( AV21DynamicFiltersEnabled2), "", "", chkavDynamicfiltersenabled2.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(96, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,96);\"");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 97,'" + sPrefix + "',false,'" + sGXsfl_83_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled3_Internalname, StringUtil.BoolToStr( AV26DynamicFiltersEnabled3), "", "", chkavDynamicfiltersenabled3.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(97, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,97);\"");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 98,'" + sPrefix + "',false,'" + sGXsfl_83_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfunidadeorganizacional_nome_Internalname, StringUtil.RTrim( AV40TFUnidadeOrganizacional_Nome), StringUtil.RTrim( context.localUtil.Format( AV40TFUnidadeOrganizacional_Nome, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,98);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfunidadeorganizacional_nome_Jsonclick, 0, "Attribute", "", "", "", edtavTfunidadeorganizacional_nome_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_Geral_Tp_UOGeral_UnidadeOrganizacionalWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 99,'" + sPrefix + "',false,'" + sGXsfl_83_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfunidadeorganizacional_nome_sel_Internalname, StringUtil.RTrim( AV41TFUnidadeOrganizacional_Nome_Sel), StringUtil.RTrim( context.localUtil.Format( AV41TFUnidadeOrganizacional_Nome_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,99);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfunidadeorganizacional_nome_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfunidadeorganizacional_nome_sel_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_Geral_Tp_UOGeral_UnidadeOrganizacionalWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 100,'" + sPrefix + "',false,'" + sGXsfl_83_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfestado_uf_Internalname, StringUtil.RTrim( AV44TFEstado_UF), StringUtil.RTrim( context.localUtil.Format( AV44TFEstado_UF, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,100);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfestado_uf_Jsonclick, 0, "Attribute", "", "", "", edtavTfestado_uf_Visible, 1, 0, "text", "", 2, "chr", 1, "row", 2, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_Geral_Tp_UOGeral_UnidadeOrganizacionalWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 101,'" + sPrefix + "',false,'" + sGXsfl_83_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfestado_uf_sel_Internalname, StringUtil.RTrim( AV45TFEstado_UF_Sel), StringUtil.RTrim( context.localUtil.Format( AV45TFEstado_UF_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,101);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfestado_uf_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfestado_uf_sel_Visible, 1, 0, "text", "", 2, "chr", 1, "row", 2, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_Geral_Tp_UOGeral_UnidadeOrganizacionalWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 102,'" + sPrefix + "',false,'" + sGXsfl_83_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfunidadeorganizacional_vinculada_Internalname, StringUtil.RTrim( AV48TFUnidadeOrganizacional_Vinculada), StringUtil.RTrim( context.localUtil.Format( AV48TFUnidadeOrganizacional_Vinculada, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,102);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfunidadeorganizacional_vinculada_Jsonclick, 0, "Attribute", "", "", "", edtavTfunidadeorganizacional_vinculada_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_Geral_Tp_UOGeral_UnidadeOrganizacionalWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 103,'" + sPrefix + "',false,'" + sGXsfl_83_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfunidadeorganizacional_vinculada_sel_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV49TFUnidadeOrganizacional_Vinculada_Sel), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV49TFUnidadeOrganizacional_Vinculada_Sel), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,103);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfunidadeorganizacional_vinculada_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfunidadeorganizacional_vinculada_sel_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_Geral_Tp_UOGeral_UnidadeOrganizacionalWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 104,'" + sPrefix + "',false,'" + sGXsfl_83_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfunidadeorganizacional_vinculada_seldsc_Internalname, AV52TFUnidadeOrganizacional_Vinculada_SelDsc, StringUtil.RTrim( context.localUtil.Format( AV52TFUnidadeOrganizacional_Vinculada_SelDsc, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,104);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfunidadeorganizacional_vinculada_seldsc_Jsonclick, 0, "Attribute", "", "", "", edtavTfunidadeorganizacional_vinculada_seldsc_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_Geral_Tp_UOGeral_UnidadeOrganizacionalWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_UNIDADEORGANIZACIONAL_NOMEContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 106,'" + sPrefix + "',false,'" + sGXsfl_83_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_unidadeorganizacional_nometitlecontrolidtoreplace_Internalname, AV42ddo_UnidadeOrganizacional_NomeTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,106);\"", 0, edtavDdo_unidadeorganizacional_nometitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_Geral_Tp_UOGeral_UnidadeOrganizacionalWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_ESTADO_UFContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 108,'" + sPrefix + "',false,'" + sGXsfl_83_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_estado_uftitlecontrolidtoreplace_Internalname, AV46ddo_Estado_UFTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,108);\"", 0, edtavDdo_estado_uftitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_Geral_Tp_UOGeral_UnidadeOrganizacionalWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_UNIDADEORGANIZACIONAL_VINCULADAContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 110,'" + sPrefix + "',false,'" + sGXsfl_83_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_unidadeorganizacional_vinculadatitlecontrolidtoreplace_Internalname, AV50ddo_UnidadeOrganizacional_VinculadaTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,110);\"", 0, edtavDdo_unidadeorganizacional_vinculadatitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_Geral_Tp_UOGeral_UnidadeOrganizacionalWC.htm");
         }
         wbLoad = true;
      }

      protected void STARTD82( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "Geral_Tp_UOGeral_Unidade Organizacional WC", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUPD80( ) ;
            }
         }
      }

      protected void WSD82( )
      {
         STARTD82( ) ;
         EVTD82( ) ;
      }

      protected void EVTD82( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPD80( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPD80( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E11D82 */
                                    E11D82 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_UNIDADEORGANIZACIONAL_NOME.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPD80( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E12D82 */
                                    E12D82 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_ESTADO_UF.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPD80( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E13D82 */
                                    E13D82 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_UNIDADEORGANIZACIONAL_VINCULADA.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPD80( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E14D82 */
                                    E14D82 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VORDEREDBY.CLICK") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPD80( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E15D82 */
                                    E15D82 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS1'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPD80( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E16D82 */
                                    E16D82 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS2'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPD80( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E17D82 */
                                    E17D82 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS3'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPD80( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E18D82 */
                                    E18D82 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOCLEANFILTERS'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPD80( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E19D82 */
                                    E19D82 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOINSERT'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPD80( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E20D82 */
                                    E20D82 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS1'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPD80( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E21D82 */
                                    E21D82 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR1.CLICK") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPD80( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E22D82 */
                                    E22D82 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS2'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPD80( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E23D82 */
                                    E23D82 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR2.CLICK") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPD80( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E24D82 */
                                    E24D82 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR3.CLICK") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPD80( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E25D82 */
                                    E25D82 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPD80( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    GX_FocusControl = cmbavOrderedby_Internalname;
                                    context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                 }
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPD80( ) ;
                              }
                              nGXsfl_83_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_83_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_83_idx), 4, 0)), 4, "0");
                              SubsflControlProps_832( ) ;
                              AV33Update = cgiGet( edtavUpdate_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavUpdate_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV33Update)) ? AV59Update_GXI : context.convertURL( context.PathToRelativeUrl( AV33Update))));
                              AV34Delete = cgiGet( edtavDelete_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDelete_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV34Delete)) ? AV60Delete_GXI : context.convertURL( context.PathToRelativeUrl( AV34Delete))));
                              AV35Display = cgiGet( edtavDisplay_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDisplay_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV35Display)) ? AV61Display_GXI : context.convertURL( context.PathToRelativeUrl( AV35Display))));
                              A611UnidadeOrganizacional_Codigo = (int)(context.localUtil.CToN( cgiGet( edtUnidadeOrganizacional_Codigo_Internalname), ",", "."));
                              A612UnidadeOrganizacional_Nome = StringUtil.Upper( cgiGet( edtUnidadeOrganizacional_Nome_Internalname));
                              A23Estado_UF = StringUtil.Upper( cgiGet( edtEstado_UF_Internalname));
                              dynUnidadeOrganizacional_Vinculada.Name = dynUnidadeOrganizacional_Vinculada_Internalname;
                              dynUnidadeOrganizacional_Vinculada.CurrentValue = cgiGet( dynUnidadeOrganizacional_Vinculada_Internalname);
                              A613UnidadeOrganizacional_Vinculada = (int)(NumberUtil.Val( cgiGet( dynUnidadeOrganizacional_Vinculada_Internalname), "."));
                              n613UnidadeOrganizacional_Vinculada = false;
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = cmbavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E26D82 */
                                          E26D82 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = cmbavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E27D82 */
                                          E27D82 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = cmbavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E28D82 */
                                          E28D82 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          if ( ! wbErr )
                                          {
                                             Rfr0gs = false;
                                             /* Set Refresh If Orderedby Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV14OrderedBy )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Ordereddsc Changed */
                                             if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vORDEREDDSC")) != AV15OrderedDsc )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Dynamicfiltersselector1 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vDYNAMICFILTERSSELECTOR1"), AV17DynamicFiltersSelector1) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Unidadeorganizacional_nome1 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vUNIDADEORGANIZACIONAL_NOME1"), AV18UnidadeOrganizacional_Nome1) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Estado_uf1 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vESTADO_UF1"), AV19Estado_UF1) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Unidadeorganizacional_vinculada1 Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vUNIDADEORGANIZACIONAL_VINCULADA1"), ",", ".") != Convert.ToDecimal( AV20UnidadeOrganizacional_Vinculada1 )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Dynamicfiltersselector2 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vDYNAMICFILTERSSELECTOR2"), AV22DynamicFiltersSelector2) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Unidadeorganizacional_nome2 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vUNIDADEORGANIZACIONAL_NOME2"), AV23UnidadeOrganizacional_Nome2) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Estado_uf2 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vESTADO_UF2"), AV24Estado_UF2) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Unidadeorganizacional_vinculada2 Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vUNIDADEORGANIZACIONAL_VINCULADA2"), ",", ".") != Convert.ToDecimal( AV25UnidadeOrganizacional_Vinculada2 )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Dynamicfiltersselector3 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vDYNAMICFILTERSSELECTOR3"), AV27DynamicFiltersSelector3) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Unidadeorganizacional_nome3 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vUNIDADEORGANIZACIONAL_NOME3"), AV28UnidadeOrganizacional_Nome3) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Estado_uf3 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vESTADO_UF3"), AV29Estado_UF3) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Unidadeorganizacional_vinculada3 Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vUNIDADEORGANIZACIONAL_VINCULADA3"), ",", ".") != Convert.ToDecimal( AV30UnidadeOrganizacional_Vinculada3 )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Dynamicfiltersenabled2 Changed */
                                             if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vDYNAMICFILTERSENABLED2")) != AV21DynamicFiltersEnabled2 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Dynamicfiltersenabled3 Changed */
                                             if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vDYNAMICFILTERSENABLED3")) != AV26DynamicFiltersEnabled3 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfunidadeorganizacional_nome Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFUNIDADEORGANIZACIONAL_NOME"), AV40TFUnidadeOrganizacional_Nome) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfunidadeorganizacional_nome_sel Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFUNIDADEORGANIZACIONAL_NOME_SEL"), AV41TFUnidadeOrganizacional_Nome_Sel) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfestado_uf Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFESTADO_UF"), AV44TFEstado_UF) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfestado_uf_sel Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFESTADO_UF_SEL"), AV45TFEstado_UF_Sel) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfunidadeorganizacional_vinculada Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFUNIDADEORGANIZACIONAL_VINCULADA"), AV48TFUnidadeOrganizacional_Vinculada) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfunidadeorganizacional_vinculada_sel Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFUNIDADEORGANIZACIONAL_VINCULADA_SEL"), ",", ".") != Convert.ToDecimal( AV49TFUnidadeOrganizacional_Vinculada_Sel )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             if ( ! Rfr0gs )
                                             {
                                             }
                                             dynload_actions( ) ;
                                          }
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = cmbavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                                    {
                                       STRUPD80( ) ;
                                    }
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = cmbavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WED82( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormD82( ) ;
            }
         }
      }

      protected void PAD82( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            cmbavOrderedby.Name = "vORDEREDBY";
            cmbavOrderedby.WebTags = "";
            if ( cmbavOrderedby.ItemCount > 0 )
            {
               AV14OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            }
            chkavUnidadeorganizacional_ativo.Name = "vUNIDADEORGANIZACIONAL_ATIVO";
            chkavUnidadeorganizacional_ativo.WebTags = "";
            chkavUnidadeorganizacional_ativo.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkavUnidadeorganizacional_ativo_Internalname, "TitleCaption", chkavUnidadeorganizacional_ativo.Caption);
            chkavUnidadeorganizacional_ativo.CheckedValue = "false";
            cmbavDynamicfiltersselector1.Name = "vDYNAMICFILTERSSELECTOR1";
            cmbavDynamicfiltersselector1.WebTags = "";
            cmbavDynamicfiltersselector1.addItem("UNIDADEORGANIZACIONAL_NOME", "Nome", 0);
            cmbavDynamicfiltersselector1.addItem("ESTADO_UF", "UF", 0);
            cmbavDynamicfiltersselector1.addItem("UNIDADEORGANIZACIONAL_VINCULADA", "UO Vinculada", 0);
            if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
            {
               AV17DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV17DynamicFiltersSelector1);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17DynamicFiltersSelector1", AV17DynamicFiltersSelector1);
            }
            dynavEstado_uf1.Name = "vESTADO_UF1";
            dynavEstado_uf1.WebTags = "";
            dynavUnidadeorganizacional_vinculada1.Name = "vUNIDADEORGANIZACIONAL_VINCULADA1";
            dynavUnidadeorganizacional_vinculada1.WebTags = "";
            cmbavDynamicfiltersselector2.Name = "vDYNAMICFILTERSSELECTOR2";
            cmbavDynamicfiltersselector2.WebTags = "";
            cmbavDynamicfiltersselector2.addItem("UNIDADEORGANIZACIONAL_NOME", "Nome", 0);
            cmbavDynamicfiltersselector2.addItem("ESTADO_UF", "UF", 0);
            cmbavDynamicfiltersselector2.addItem("UNIDADEORGANIZACIONAL_VINCULADA", "UO Vinculada", 0);
            if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
            {
               AV22DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV22DynamicFiltersSelector2);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22DynamicFiltersSelector2", AV22DynamicFiltersSelector2);
            }
            dynavEstado_uf2.Name = "vESTADO_UF2";
            dynavEstado_uf2.WebTags = "";
            dynavUnidadeorganizacional_vinculada2.Name = "vUNIDADEORGANIZACIONAL_VINCULADA2";
            dynavUnidadeorganizacional_vinculada2.WebTags = "";
            cmbavDynamicfiltersselector3.Name = "vDYNAMICFILTERSSELECTOR3";
            cmbavDynamicfiltersselector3.WebTags = "";
            cmbavDynamicfiltersselector3.addItem("UNIDADEORGANIZACIONAL_NOME", "Nome", 0);
            cmbavDynamicfiltersselector3.addItem("ESTADO_UF", "UF", 0);
            cmbavDynamicfiltersselector3.addItem("UNIDADEORGANIZACIONAL_VINCULADA", "UO Vinculada", 0);
            if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
            {
               AV27DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV27DynamicFiltersSelector3);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27DynamicFiltersSelector3", AV27DynamicFiltersSelector3);
            }
            dynavEstado_uf3.Name = "vESTADO_UF3";
            dynavEstado_uf3.WebTags = "";
            dynavUnidadeorganizacional_vinculada3.Name = "vUNIDADEORGANIZACIONAL_VINCULADA3";
            dynavUnidadeorganizacional_vinculada3.WebTags = "";
            GXCCtl = "UNIDADEORGANIZACIONAL_VINCULADA_" + sGXsfl_83_idx;
            dynUnidadeOrganizacional_Vinculada.Name = GXCCtl;
            dynUnidadeOrganizacional_Vinculada.WebTags = "";
            chkavDynamicfiltersenabled2.Name = "vDYNAMICFILTERSENABLED2";
            chkavDynamicfiltersenabled2.WebTags = "";
            chkavDynamicfiltersenabled2.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkavDynamicfiltersenabled2_Internalname, "TitleCaption", chkavDynamicfiltersenabled2.Caption);
            chkavDynamicfiltersenabled2.CheckedValue = "false";
            chkavDynamicfiltersenabled3.Name = "vDYNAMICFILTERSENABLED3";
            chkavDynamicfiltersenabled3.WebTags = "";
            chkavDynamicfiltersenabled3.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkavDynamicfiltersenabled3_Internalname, "TitleCaption", chkavDynamicfiltersenabled3.Caption);
            chkavDynamicfiltersenabled3.CheckedValue = "false";
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void GXDLVvESTADO_UF1D82( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvESTADO_UF1_dataD82( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvESTADO_UF1_htmlD82( )
      {
         String gxdynajaxvalue ;
         GXDLVvESTADO_UF1_dataD82( ) ;
         gxdynajaxindex = 1;
         dynavEstado_uf1.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex));
            dynavEstado_uf1.addItem(gxdynajaxvalue, ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavEstado_uf1.ItemCount > 0 )
         {
            AV19Estado_UF1 = dynavEstado_uf1.getValidValue(AV19Estado_UF1);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19Estado_UF1", AV19Estado_UF1);
         }
      }

      protected void GXDLVvESTADO_UF1_dataD82( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add("");
         gxdynajaxctrldescr.Add("(Nenhum)");
         /* Using cursor H00D82 */
         pr_default.execute(0);
         while ( (pr_default.getStatus(0) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.RTrim( H00D82_A23Estado_UF[0]));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00D82_A24Estado_Nome[0]));
            pr_default.readNext(0);
         }
         pr_default.close(0);
      }

      protected void GXDLVvUNIDADEORGANIZACIONAL_VINCULADA1D82( int AV64Unidadeorganizacional_codigo )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvUNIDADEORGANIZACIONAL_VINCULADA1_dataD82( AV64Unidadeorganizacional_codigo) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvUNIDADEORGANIZACIONAL_VINCULADA1_htmlD82( int AV64Unidadeorganizacional_codigo )
      {
         int gxdynajaxvalue ;
         GXDLVvUNIDADEORGANIZACIONAL_VINCULADA1_dataD82( AV64Unidadeorganizacional_codigo) ;
         gxdynajaxindex = 1;
         dynavUnidadeorganizacional_vinculada1.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavUnidadeorganizacional_vinculada1.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavUnidadeorganizacional_vinculada1.ItemCount > 0 )
         {
            AV20UnidadeOrganizacional_Vinculada1 = (int)(NumberUtil.Val( dynavUnidadeorganizacional_vinculada1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV20UnidadeOrganizacional_Vinculada1), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20UnidadeOrganizacional_Vinculada1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20UnidadeOrganizacional_Vinculada1), 6, 0)));
         }
      }

      protected void GXDLVvUNIDADEORGANIZACIONAL_VINCULADA1_dataD82( int AV64Unidadeorganizacional_codigo )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("(Nenhum)");
         /* Using cursor H00D83 */
         pr_default.execute(1, new Object[] {AV64Unidadeorganizacional_codigo});
         while ( (pr_default.getStatus(1) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00D83_A613UnidadeOrganizacional_Vinculada[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00D83_A612UnidadeOrganizacional_Nome[0]));
            pr_default.readNext(1);
         }
         pr_default.close(1);
      }

      protected void GXDLVvESTADO_UF2D82( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvESTADO_UF2_dataD82( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvESTADO_UF2_htmlD82( )
      {
         String gxdynajaxvalue ;
         GXDLVvESTADO_UF2_dataD82( ) ;
         gxdynajaxindex = 1;
         dynavEstado_uf2.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex));
            dynavEstado_uf2.addItem(gxdynajaxvalue, ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavEstado_uf2.ItemCount > 0 )
         {
            AV24Estado_UF2 = dynavEstado_uf2.getValidValue(AV24Estado_UF2);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24Estado_UF2", AV24Estado_UF2);
         }
      }

      protected void GXDLVvESTADO_UF2_dataD82( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add("");
         gxdynajaxctrldescr.Add("(Nenhum)");
         /* Using cursor H00D84 */
         pr_default.execute(2);
         while ( (pr_default.getStatus(2) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.RTrim( H00D84_A23Estado_UF[0]));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00D84_A24Estado_Nome[0]));
            pr_default.readNext(2);
         }
         pr_default.close(2);
      }

      protected void GXDLVvUNIDADEORGANIZACIONAL_VINCULADA2D82( int AV64Unidadeorganizacional_codigo )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvUNIDADEORGANIZACIONAL_VINCULADA2_dataD82( AV64Unidadeorganizacional_codigo) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvUNIDADEORGANIZACIONAL_VINCULADA2_htmlD82( int AV64Unidadeorganizacional_codigo )
      {
         int gxdynajaxvalue ;
         GXDLVvUNIDADEORGANIZACIONAL_VINCULADA2_dataD82( AV64Unidadeorganizacional_codigo) ;
         gxdynajaxindex = 1;
         dynavUnidadeorganizacional_vinculada2.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavUnidadeorganizacional_vinculada2.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavUnidadeorganizacional_vinculada2.ItemCount > 0 )
         {
            AV25UnidadeOrganizacional_Vinculada2 = (int)(NumberUtil.Val( dynavUnidadeorganizacional_vinculada2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV25UnidadeOrganizacional_Vinculada2), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25UnidadeOrganizacional_Vinculada2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV25UnidadeOrganizacional_Vinculada2), 6, 0)));
         }
      }

      protected void GXDLVvUNIDADEORGANIZACIONAL_VINCULADA2_dataD82( int AV64Unidadeorganizacional_codigo )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("(Nenhum)");
         /* Using cursor H00D85 */
         pr_default.execute(3, new Object[] {AV64Unidadeorganizacional_codigo});
         while ( (pr_default.getStatus(3) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00D85_A613UnidadeOrganizacional_Vinculada[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00D85_A612UnidadeOrganizacional_Nome[0]));
            pr_default.readNext(3);
         }
         pr_default.close(3);
      }

      protected void GXDLVvESTADO_UF3D82( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvESTADO_UF3_dataD82( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvESTADO_UF3_htmlD82( )
      {
         String gxdynajaxvalue ;
         GXDLVvESTADO_UF3_dataD82( ) ;
         gxdynajaxindex = 1;
         dynavEstado_uf3.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex));
            dynavEstado_uf3.addItem(gxdynajaxvalue, ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavEstado_uf3.ItemCount > 0 )
         {
            AV29Estado_UF3 = dynavEstado_uf3.getValidValue(AV29Estado_UF3);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29Estado_UF3", AV29Estado_UF3);
         }
      }

      protected void GXDLVvESTADO_UF3_dataD82( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add("");
         gxdynajaxctrldescr.Add("(Nenhum)");
         /* Using cursor H00D86 */
         pr_default.execute(4);
         while ( (pr_default.getStatus(4) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.RTrim( H00D86_A23Estado_UF[0]));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00D86_A24Estado_Nome[0]));
            pr_default.readNext(4);
         }
         pr_default.close(4);
      }

      protected void GXDLVvUNIDADEORGANIZACIONAL_VINCULADA3D82( int AV64Unidadeorganizacional_codigo )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvUNIDADEORGANIZACIONAL_VINCULADA3_dataD82( AV64Unidadeorganizacional_codigo) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvUNIDADEORGANIZACIONAL_VINCULADA3_htmlD82( int AV64Unidadeorganizacional_codigo )
      {
         int gxdynajaxvalue ;
         GXDLVvUNIDADEORGANIZACIONAL_VINCULADA3_dataD82( AV64Unidadeorganizacional_codigo) ;
         gxdynajaxindex = 1;
         dynavUnidadeorganizacional_vinculada3.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavUnidadeorganizacional_vinculada3.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavUnidadeorganizacional_vinculada3.ItemCount > 0 )
         {
            AV30UnidadeOrganizacional_Vinculada3 = (int)(NumberUtil.Val( dynavUnidadeorganizacional_vinculada3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV30UnidadeOrganizacional_Vinculada3), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30UnidadeOrganizacional_Vinculada3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV30UnidadeOrganizacional_Vinculada3), 6, 0)));
         }
      }

      protected void GXDLVvUNIDADEORGANIZACIONAL_VINCULADA3_dataD82( int AV64Unidadeorganizacional_codigo )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("(Nenhum)");
         /* Using cursor H00D87 */
         pr_default.execute(5, new Object[] {AV64Unidadeorganizacional_codigo});
         while ( (pr_default.getStatus(5) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00D87_A613UnidadeOrganizacional_Vinculada[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00D87_A612UnidadeOrganizacional_Nome[0]));
            pr_default.readNext(5);
         }
         pr_default.close(5);
      }

      protected void GXDLAUNIDADEORGANIZACIONAL_VINCULADAD82( int AV64Unidadeorganizacional_codigo )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLAUNIDADEORGANIZACIONAL_VINCULADA_dataD82( AV64Unidadeorganizacional_codigo) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXAUNIDADEORGANIZACIONAL_VINCULADA_htmlD82( int AV64Unidadeorganizacional_codigo )
      {
         int gxdynajaxvalue ;
         GXDLAUNIDADEORGANIZACIONAL_VINCULADA_dataD82( AV64Unidadeorganizacional_codigo) ;
         gxdynajaxindex = 1;
         dynUnidadeOrganizacional_Vinculada.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynUnidadeOrganizacional_Vinculada.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
      }

      protected void GXDLAUNIDADEORGANIZACIONAL_VINCULADA_dataD82( int AV64Unidadeorganizacional_codigo )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("(Nenhum)");
         /* Using cursor H00D88 */
         pr_default.execute(6, new Object[] {AV64Unidadeorganizacional_codigo});
         while ( (pr_default.getStatus(6) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H00D88_A611UnidadeOrganizacional_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( H00D88_A612UnidadeOrganizacional_Nome[0]));
            pr_default.readNext(6);
         }
         pr_default.close(6);
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_832( ) ;
         while ( nGXsfl_83_idx <= nRC_GXsfl_83 )
         {
            sendrow_832( ) ;
            nGXsfl_83_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_83_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_83_idx+1));
            sGXsfl_83_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_83_idx), 4, 0)), 4, "0");
            SubsflControlProps_832( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV14OrderedBy ,
                                       bool AV15OrderedDsc ,
                                       String AV17DynamicFiltersSelector1 ,
                                       String AV18UnidadeOrganizacional_Nome1 ,
                                       String AV19Estado_UF1 ,
                                       int AV20UnidadeOrganizacional_Vinculada1 ,
                                       String AV22DynamicFiltersSelector2 ,
                                       String AV23UnidadeOrganizacional_Nome2 ,
                                       String AV24Estado_UF2 ,
                                       int AV25UnidadeOrganizacional_Vinculada2 ,
                                       String AV27DynamicFiltersSelector3 ,
                                       String AV28UnidadeOrganizacional_Nome3 ,
                                       String AV29Estado_UF3 ,
                                       int AV30UnidadeOrganizacional_Vinculada3 ,
                                       bool AV21DynamicFiltersEnabled2 ,
                                       bool AV26DynamicFiltersEnabled3 ,
                                       String AV40TFUnidadeOrganizacional_Nome ,
                                       String AV41TFUnidadeOrganizacional_Nome_Sel ,
                                       String AV44TFEstado_UF ,
                                       String AV45TFEstado_UF_Sel ,
                                       String AV48TFUnidadeOrganizacional_Vinculada ,
                                       int AV49TFUnidadeOrganizacional_Vinculada_Sel ,
                                       int AV7TpUo_Codigo ,
                                       String AV42ddo_UnidadeOrganizacional_NomeTitleControlIdToReplace ,
                                       String AV46ddo_Estado_UFTitleControlIdToReplace ,
                                       String AV50ddo_UnidadeOrganizacional_VinculadaTitleControlIdToReplace ,
                                       wwpbaseobjects.SdtWWPContext AV6WWPContext ,
                                       String AV62Pgmname ,
                                       bool AV16UnidadeOrganizacional_Ativo ,
                                       String AV52TFUnidadeOrganizacional_Vinculada_SelDsc ,
                                       wwpbaseobjects.SdtWWPGridState AV11GridState ,
                                       bool AV32DynamicFiltersIgnoreFirst ,
                                       bool AV31DynamicFiltersRemoving ,
                                       int A611UnidadeOrganizacional_Codigo ,
                                       int AV64Unidadeorganizacional_codigo ,
                                       String sPrefix )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RFD82( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_UNIDADEORGANIZACIONAL_CODIGO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A611UnidadeOrganizacional_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"UNIDADEORGANIZACIONAL_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A611UnidadeOrganizacional_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_UNIDADEORGANIZACIONAL_NOME", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A612UnidadeOrganizacional_Nome, "@!"))));
         GxWebStd.gx_hidden_field( context, sPrefix+"UNIDADEORGANIZACIONAL_NOME", StringUtil.RTrim( A612UnidadeOrganizacional_Nome));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_ESTADO_UF", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A23Estado_UF, "@!"))));
         GxWebStd.gx_hidden_field( context, sPrefix+"ESTADO_UF", StringUtil.RTrim( A23Estado_UF));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_UNIDADEORGANIZACIONAL_VINCULADA", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A613UnidadeOrganizacional_Vinculada), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"UNIDADEORGANIZACIONAL_VINCULADA", StringUtil.LTrim( StringUtil.NToC( (decimal)(A613UnidadeOrganizacional_Vinculada), 6, 0, ".", "")));
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavOrderedby.ItemCount > 0 )
         {
            AV14OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
         {
            AV17DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV17DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17DynamicFiltersSelector1", AV17DynamicFiltersSelector1);
         }
         if ( dynavEstado_uf1.ItemCount > 0 )
         {
            AV19Estado_UF1 = dynavEstado_uf1.getValidValue(AV19Estado_UF1);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19Estado_UF1", AV19Estado_UF1);
         }
         if ( dynavUnidadeorganizacional_vinculada1.ItemCount > 0 )
         {
            AV20UnidadeOrganizacional_Vinculada1 = (int)(NumberUtil.Val( dynavUnidadeorganizacional_vinculada1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV20UnidadeOrganizacional_Vinculada1), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20UnidadeOrganizacional_Vinculada1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20UnidadeOrganizacional_Vinculada1), 6, 0)));
         }
         if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
         {
            AV22DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV22DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22DynamicFiltersSelector2", AV22DynamicFiltersSelector2);
         }
         if ( dynavEstado_uf2.ItemCount > 0 )
         {
            AV24Estado_UF2 = dynavEstado_uf2.getValidValue(AV24Estado_UF2);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24Estado_UF2", AV24Estado_UF2);
         }
         if ( dynavUnidadeorganizacional_vinculada2.ItemCount > 0 )
         {
            AV25UnidadeOrganizacional_Vinculada2 = (int)(NumberUtil.Val( dynavUnidadeorganizacional_vinculada2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV25UnidadeOrganizacional_Vinculada2), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25UnidadeOrganizacional_Vinculada2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV25UnidadeOrganizacional_Vinculada2), 6, 0)));
         }
         if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
         {
            AV27DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV27DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27DynamicFiltersSelector3", AV27DynamicFiltersSelector3);
         }
         if ( dynavEstado_uf3.ItemCount > 0 )
         {
            AV29Estado_UF3 = dynavEstado_uf3.getValidValue(AV29Estado_UF3);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29Estado_UF3", AV29Estado_UF3);
         }
         if ( dynavUnidadeorganizacional_vinculada3.ItemCount > 0 )
         {
            AV30UnidadeOrganizacional_Vinculada3 = (int)(NumberUtil.Val( dynavUnidadeorganizacional_vinculada3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV30UnidadeOrganizacional_Vinculada3), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30UnidadeOrganizacional_Vinculada3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV30UnidadeOrganizacional_Vinculada3), 6, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFD82( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV62Pgmname = "Geral_Tp_UOGeral_UnidadeOrganizacionalWC";
         context.Gx_err = 0;
      }

      protected void RFD82( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 83;
         /* Execute user event: E27D82 */
         E27D82 ();
         nGXsfl_83_idx = 1;
         sGXsfl_83_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_83_idx), 4, 0)), 4, "0");
         SubsflControlProps_832( ) ;
         nGXsfl_83_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", sPrefix);
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_832( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(7, new Object[]{ new Object[]{
                                                 AV17DynamicFiltersSelector1 ,
                                                 AV18UnidadeOrganizacional_Nome1 ,
                                                 AV19Estado_UF1 ,
                                                 AV20UnidadeOrganizacional_Vinculada1 ,
                                                 AV21DynamicFiltersEnabled2 ,
                                                 AV22DynamicFiltersSelector2 ,
                                                 AV23UnidadeOrganizacional_Nome2 ,
                                                 AV24Estado_UF2 ,
                                                 AV25UnidadeOrganizacional_Vinculada2 ,
                                                 AV26DynamicFiltersEnabled3 ,
                                                 AV27DynamicFiltersSelector3 ,
                                                 AV28UnidadeOrganizacional_Nome3 ,
                                                 AV29Estado_UF3 ,
                                                 AV30UnidadeOrganizacional_Vinculada3 ,
                                                 AV41TFUnidadeOrganizacional_Nome_Sel ,
                                                 AV40TFUnidadeOrganizacional_Nome ,
                                                 AV45TFEstado_UF_Sel ,
                                                 AV44TFEstado_UF ,
                                                 AV49TFUnidadeOrganizacional_Vinculada_Sel ,
                                                 AV48TFUnidadeOrganizacional_Vinculada ,
                                                 A612UnidadeOrganizacional_Nome ,
                                                 A23Estado_UF ,
                                                 A613UnidadeOrganizacional_Vinculada ,
                                                 AV14OrderedBy ,
                                                 AV15OrderedDsc ,
                                                 A609TpUo_Codigo ,
                                                 AV7TpUo_Codigo ,
                                                 A629UnidadeOrganizacional_Ativo },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN,
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING,
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN
                                                 }
            });
            lV18UnidadeOrganizacional_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV18UnidadeOrganizacional_Nome1), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18UnidadeOrganizacional_Nome1", AV18UnidadeOrganizacional_Nome1);
            lV23UnidadeOrganizacional_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV23UnidadeOrganizacional_Nome2), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23UnidadeOrganizacional_Nome2", AV23UnidadeOrganizacional_Nome2);
            lV28UnidadeOrganizacional_Nome3 = StringUtil.PadR( StringUtil.RTrim( AV28UnidadeOrganizacional_Nome3), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV28UnidadeOrganizacional_Nome3", AV28UnidadeOrganizacional_Nome3);
            lV40TFUnidadeOrganizacional_Nome = StringUtil.PadR( StringUtil.RTrim( AV40TFUnidadeOrganizacional_Nome), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV40TFUnidadeOrganizacional_Nome", AV40TFUnidadeOrganizacional_Nome);
            lV44TFEstado_UF = StringUtil.PadR( StringUtil.RTrim( AV44TFEstado_UF), 2, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV44TFEstado_UF", AV44TFEstado_UF);
            lV48TFUnidadeOrganizacional_Vinculada = StringUtil.PadR( StringUtil.RTrim( AV48TFUnidadeOrganizacional_Vinculada), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV48TFUnidadeOrganizacional_Vinculada", AV48TFUnidadeOrganizacional_Vinculada);
            /* Using cursor H00D89 */
            pr_default.execute(7, new Object[] {AV7TpUo_Codigo, lV18UnidadeOrganizacional_Nome1, AV19Estado_UF1, AV20UnidadeOrganizacional_Vinculada1, lV23UnidadeOrganizacional_Nome2, AV24Estado_UF2, AV25UnidadeOrganizacional_Vinculada2, lV28UnidadeOrganizacional_Nome3, AV29Estado_UF3, AV30UnidadeOrganizacional_Vinculada3, lV40TFUnidadeOrganizacional_Nome, AV41TFUnidadeOrganizacional_Nome_Sel, lV44TFEstado_UF, AV45TFEstado_UF_Sel, lV48TFUnidadeOrganizacional_Vinculada, AV49TFUnidadeOrganizacional_Vinculada_Sel, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_83_idx = 1;
            while ( ( (pr_default.getStatus(7) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A629UnidadeOrganizacional_Ativo = H00D89_A629UnidadeOrganizacional_Ativo[0];
               A609TpUo_Codigo = H00D89_A609TpUo_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A609TpUo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A609TpUo_Codigo), 6, 0)));
               A613UnidadeOrganizacional_Vinculada = H00D89_A613UnidadeOrganizacional_Vinculada[0];
               n613UnidadeOrganizacional_Vinculada = H00D89_n613UnidadeOrganizacional_Vinculada[0];
               A23Estado_UF = H00D89_A23Estado_UF[0];
               A612UnidadeOrganizacional_Nome = H00D89_A612UnidadeOrganizacional_Nome[0];
               A611UnidadeOrganizacional_Codigo = H00D89_A611UnidadeOrganizacional_Codigo[0];
               /* Execute user event: E28D82 */
               E28D82 ();
               pr_default.readNext(7);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(7) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(7);
            wbEnd = 83;
            WBD80( ) ;
         }
         nGXsfl_83_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         pr_default.dynParam(8, new Object[]{ new Object[]{
                                              AV17DynamicFiltersSelector1 ,
                                              AV18UnidadeOrganizacional_Nome1 ,
                                              AV19Estado_UF1 ,
                                              AV20UnidadeOrganizacional_Vinculada1 ,
                                              AV21DynamicFiltersEnabled2 ,
                                              AV22DynamicFiltersSelector2 ,
                                              AV23UnidadeOrganizacional_Nome2 ,
                                              AV24Estado_UF2 ,
                                              AV25UnidadeOrganizacional_Vinculada2 ,
                                              AV26DynamicFiltersEnabled3 ,
                                              AV27DynamicFiltersSelector3 ,
                                              AV28UnidadeOrganizacional_Nome3 ,
                                              AV29Estado_UF3 ,
                                              AV30UnidadeOrganizacional_Vinculada3 ,
                                              AV41TFUnidadeOrganizacional_Nome_Sel ,
                                              AV40TFUnidadeOrganizacional_Nome ,
                                              AV45TFEstado_UF_Sel ,
                                              AV44TFEstado_UF ,
                                              AV49TFUnidadeOrganizacional_Vinculada_Sel ,
                                              AV48TFUnidadeOrganizacional_Vinculada ,
                                              A612UnidadeOrganizacional_Nome ,
                                              A23Estado_UF ,
                                              A613UnidadeOrganizacional_Vinculada ,
                                              AV14OrderedBy ,
                                              AV15OrderedDsc ,
                                              A609TpUo_Codigo ,
                                              AV7TpUo_Codigo ,
                                              A629UnidadeOrganizacional_Ativo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN
                                              }
         });
         lV18UnidadeOrganizacional_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV18UnidadeOrganizacional_Nome1), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18UnidadeOrganizacional_Nome1", AV18UnidadeOrganizacional_Nome1);
         lV23UnidadeOrganizacional_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV23UnidadeOrganizacional_Nome2), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23UnidadeOrganizacional_Nome2", AV23UnidadeOrganizacional_Nome2);
         lV28UnidadeOrganizacional_Nome3 = StringUtil.PadR( StringUtil.RTrim( AV28UnidadeOrganizacional_Nome3), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV28UnidadeOrganizacional_Nome3", AV28UnidadeOrganizacional_Nome3);
         lV40TFUnidadeOrganizacional_Nome = StringUtil.PadR( StringUtil.RTrim( AV40TFUnidadeOrganizacional_Nome), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV40TFUnidadeOrganizacional_Nome", AV40TFUnidadeOrganizacional_Nome);
         lV44TFEstado_UF = StringUtil.PadR( StringUtil.RTrim( AV44TFEstado_UF), 2, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV44TFEstado_UF", AV44TFEstado_UF);
         lV48TFUnidadeOrganizacional_Vinculada = StringUtil.PadR( StringUtil.RTrim( AV48TFUnidadeOrganizacional_Vinculada), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV48TFUnidadeOrganizacional_Vinculada", AV48TFUnidadeOrganizacional_Vinculada);
         /* Using cursor H00D810 */
         pr_default.execute(8, new Object[] {AV7TpUo_Codigo, lV18UnidadeOrganizacional_Nome1, AV19Estado_UF1, AV20UnidadeOrganizacional_Vinculada1, lV23UnidadeOrganizacional_Nome2, AV24Estado_UF2, AV25UnidadeOrganizacional_Vinculada2, lV28UnidadeOrganizacional_Nome3, AV29Estado_UF3, AV30UnidadeOrganizacional_Vinculada3, lV40TFUnidadeOrganizacional_Nome, AV41TFUnidadeOrganizacional_Nome_Sel, lV44TFEstado_UF, AV45TFEstado_UF_Sel, lV48TFUnidadeOrganizacional_Vinculada, AV49TFUnidadeOrganizacional_Vinculada_Sel});
         GRID_nRecordCount = H00D810_AGRID_nRecordCount[0];
         pr_default.close(8);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV17DynamicFiltersSelector1, AV18UnidadeOrganizacional_Nome1, AV19Estado_UF1, AV20UnidadeOrganizacional_Vinculada1, AV22DynamicFiltersSelector2, AV23UnidadeOrganizacional_Nome2, AV24Estado_UF2, AV25UnidadeOrganizacional_Vinculada2, AV27DynamicFiltersSelector3, AV28UnidadeOrganizacional_Nome3, AV29Estado_UF3, AV30UnidadeOrganizacional_Vinculada3, AV21DynamicFiltersEnabled2, AV26DynamicFiltersEnabled3, AV40TFUnidadeOrganizacional_Nome, AV41TFUnidadeOrganizacional_Nome_Sel, AV44TFEstado_UF, AV45TFEstado_UF_Sel, AV48TFUnidadeOrganizacional_Vinculada, AV49TFUnidadeOrganizacional_Vinculada_Sel, AV7TpUo_Codigo, AV42ddo_UnidadeOrganizacional_NomeTitleControlIdToReplace, AV46ddo_Estado_UFTitleControlIdToReplace, AV50ddo_UnidadeOrganizacional_VinculadaTitleControlIdToReplace, AV6WWPContext, AV62Pgmname, AV16UnidadeOrganizacional_Ativo, AV52TFUnidadeOrganizacional_Vinculada_SelDsc, AV11GridState, AV32DynamicFiltersIgnoreFirst, AV31DynamicFiltersRemoving, A611UnidadeOrganizacional_Codigo, AV64Unidadeorganizacional_codigo, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV17DynamicFiltersSelector1, AV18UnidadeOrganizacional_Nome1, AV19Estado_UF1, AV20UnidadeOrganizacional_Vinculada1, AV22DynamicFiltersSelector2, AV23UnidadeOrganizacional_Nome2, AV24Estado_UF2, AV25UnidadeOrganizacional_Vinculada2, AV27DynamicFiltersSelector3, AV28UnidadeOrganizacional_Nome3, AV29Estado_UF3, AV30UnidadeOrganizacional_Vinculada3, AV21DynamicFiltersEnabled2, AV26DynamicFiltersEnabled3, AV40TFUnidadeOrganizacional_Nome, AV41TFUnidadeOrganizacional_Nome_Sel, AV44TFEstado_UF, AV45TFEstado_UF_Sel, AV48TFUnidadeOrganizacional_Vinculada, AV49TFUnidadeOrganizacional_Vinculada_Sel, AV7TpUo_Codigo, AV42ddo_UnidadeOrganizacional_NomeTitleControlIdToReplace, AV46ddo_Estado_UFTitleControlIdToReplace, AV50ddo_UnidadeOrganizacional_VinculadaTitleControlIdToReplace, AV6WWPContext, AV62Pgmname, AV16UnidadeOrganizacional_Ativo, AV52TFUnidadeOrganizacional_Vinculada_SelDsc, AV11GridState, AV32DynamicFiltersIgnoreFirst, AV31DynamicFiltersRemoving, A611UnidadeOrganizacional_Codigo, AV64Unidadeorganizacional_codigo, sPrefix) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV17DynamicFiltersSelector1, AV18UnidadeOrganizacional_Nome1, AV19Estado_UF1, AV20UnidadeOrganizacional_Vinculada1, AV22DynamicFiltersSelector2, AV23UnidadeOrganizacional_Nome2, AV24Estado_UF2, AV25UnidadeOrganizacional_Vinculada2, AV27DynamicFiltersSelector3, AV28UnidadeOrganizacional_Nome3, AV29Estado_UF3, AV30UnidadeOrganizacional_Vinculada3, AV21DynamicFiltersEnabled2, AV26DynamicFiltersEnabled3, AV40TFUnidadeOrganizacional_Nome, AV41TFUnidadeOrganizacional_Nome_Sel, AV44TFEstado_UF, AV45TFEstado_UF_Sel, AV48TFUnidadeOrganizacional_Vinculada, AV49TFUnidadeOrganizacional_Vinculada_Sel, AV7TpUo_Codigo, AV42ddo_UnidadeOrganizacional_NomeTitleControlIdToReplace, AV46ddo_Estado_UFTitleControlIdToReplace, AV50ddo_UnidadeOrganizacional_VinculadaTitleControlIdToReplace, AV6WWPContext, AV62Pgmname, AV16UnidadeOrganizacional_Ativo, AV52TFUnidadeOrganizacional_Vinculada_SelDsc, AV11GridState, AV32DynamicFiltersIgnoreFirst, AV31DynamicFiltersRemoving, A611UnidadeOrganizacional_Codigo, AV64Unidadeorganizacional_codigo, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV17DynamicFiltersSelector1, AV18UnidadeOrganizacional_Nome1, AV19Estado_UF1, AV20UnidadeOrganizacional_Vinculada1, AV22DynamicFiltersSelector2, AV23UnidadeOrganizacional_Nome2, AV24Estado_UF2, AV25UnidadeOrganizacional_Vinculada2, AV27DynamicFiltersSelector3, AV28UnidadeOrganizacional_Nome3, AV29Estado_UF3, AV30UnidadeOrganizacional_Vinculada3, AV21DynamicFiltersEnabled2, AV26DynamicFiltersEnabled3, AV40TFUnidadeOrganizacional_Nome, AV41TFUnidadeOrganizacional_Nome_Sel, AV44TFEstado_UF, AV45TFEstado_UF_Sel, AV48TFUnidadeOrganizacional_Vinculada, AV49TFUnidadeOrganizacional_Vinculada_Sel, AV7TpUo_Codigo, AV42ddo_UnidadeOrganizacional_NomeTitleControlIdToReplace, AV46ddo_Estado_UFTitleControlIdToReplace, AV50ddo_UnidadeOrganizacional_VinculadaTitleControlIdToReplace, AV6WWPContext, AV62Pgmname, AV16UnidadeOrganizacional_Ativo, AV52TFUnidadeOrganizacional_Vinculada_SelDsc, AV11GridState, AV32DynamicFiltersIgnoreFirst, AV31DynamicFiltersRemoving, A611UnidadeOrganizacional_Codigo, AV64Unidadeorganizacional_codigo, sPrefix) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV17DynamicFiltersSelector1, AV18UnidadeOrganizacional_Nome1, AV19Estado_UF1, AV20UnidadeOrganizacional_Vinculada1, AV22DynamicFiltersSelector2, AV23UnidadeOrganizacional_Nome2, AV24Estado_UF2, AV25UnidadeOrganizacional_Vinculada2, AV27DynamicFiltersSelector3, AV28UnidadeOrganizacional_Nome3, AV29Estado_UF3, AV30UnidadeOrganizacional_Vinculada3, AV21DynamicFiltersEnabled2, AV26DynamicFiltersEnabled3, AV40TFUnidadeOrganizacional_Nome, AV41TFUnidadeOrganizacional_Nome_Sel, AV44TFEstado_UF, AV45TFEstado_UF_Sel, AV48TFUnidadeOrganizacional_Vinculada, AV49TFUnidadeOrganizacional_Vinculada_Sel, AV7TpUo_Codigo, AV42ddo_UnidadeOrganizacional_NomeTitleControlIdToReplace, AV46ddo_Estado_UFTitleControlIdToReplace, AV50ddo_UnidadeOrganizacional_VinculadaTitleControlIdToReplace, AV6WWPContext, AV62Pgmname, AV16UnidadeOrganizacional_Ativo, AV52TFUnidadeOrganizacional_Vinculada_SelDsc, AV11GridState, AV32DynamicFiltersIgnoreFirst, AV31DynamicFiltersRemoving, A611UnidadeOrganizacional_Codigo, AV64Unidadeorganizacional_codigo, sPrefix) ;
         }
         return (int)(0) ;
      }

      protected void STRUPD80( )
      {
         /* Before Start, stand alone formulas. */
         AV62Pgmname = "Geral_Tp_UOGeral_UnidadeOrganizacionalWC";
         context.Gx_err = 0;
         GXVvESTADO_UF1_htmlD82( ) ;
         GXVvESTADO_UF2_htmlD82( ) ;
         GXVvESTADO_UF3_htmlD82( ) ;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E26D82 */
         E26D82 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vDDO_TITLESETTINGSICONS"), AV51DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vUNIDADEORGANIZACIONAL_NOMETITLEFILTERDATA"), AV39UnidadeOrganizacional_NomeTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vESTADO_UFTITLEFILTERDATA"), AV43Estado_UFTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vUNIDADEORGANIZACIONAL_VINCULADATITLEFILTERDATA"), AV47UnidadeOrganizacional_VinculadaTitleFilterData);
            /* Read variables values. */
            cmbavOrderedby.Name = cmbavOrderedby_Internalname;
            cmbavOrderedby.CurrentValue = cgiGet( cmbavOrderedby_Internalname);
            AV14OrderedBy = (short)(NumberUtil.Val( cgiGet( cmbavOrderedby_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
            AV16UnidadeOrganizacional_Ativo = StringUtil.StrToBool( cgiGet( chkavUnidadeorganizacional_ativo_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16UnidadeOrganizacional_Ativo", AV16UnidadeOrganizacional_Ativo);
            cmbavDynamicfiltersselector1.Name = cmbavDynamicfiltersselector1_Internalname;
            cmbavDynamicfiltersselector1.CurrentValue = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            AV17DynamicFiltersSelector1 = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17DynamicFiltersSelector1", AV17DynamicFiltersSelector1);
            AV18UnidadeOrganizacional_Nome1 = StringUtil.Upper( cgiGet( edtavUnidadeorganizacional_nome1_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18UnidadeOrganizacional_Nome1", AV18UnidadeOrganizacional_Nome1);
            dynavEstado_uf1.Name = dynavEstado_uf1_Internalname;
            dynavEstado_uf1.CurrentValue = cgiGet( dynavEstado_uf1_Internalname);
            AV19Estado_UF1 = cgiGet( dynavEstado_uf1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19Estado_UF1", AV19Estado_UF1);
            dynavUnidadeorganizacional_vinculada1.Name = dynavUnidadeorganizacional_vinculada1_Internalname;
            dynavUnidadeorganizacional_vinculada1.CurrentValue = cgiGet( dynavUnidadeorganizacional_vinculada1_Internalname);
            AV20UnidadeOrganizacional_Vinculada1 = (int)(NumberUtil.Val( cgiGet( dynavUnidadeorganizacional_vinculada1_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20UnidadeOrganizacional_Vinculada1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20UnidadeOrganizacional_Vinculada1), 6, 0)));
            cmbavDynamicfiltersselector2.Name = cmbavDynamicfiltersselector2_Internalname;
            cmbavDynamicfiltersselector2.CurrentValue = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            AV22DynamicFiltersSelector2 = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22DynamicFiltersSelector2", AV22DynamicFiltersSelector2);
            AV23UnidadeOrganizacional_Nome2 = StringUtil.Upper( cgiGet( edtavUnidadeorganizacional_nome2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23UnidadeOrganizacional_Nome2", AV23UnidadeOrganizacional_Nome2);
            dynavEstado_uf2.Name = dynavEstado_uf2_Internalname;
            dynavEstado_uf2.CurrentValue = cgiGet( dynavEstado_uf2_Internalname);
            AV24Estado_UF2 = cgiGet( dynavEstado_uf2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24Estado_UF2", AV24Estado_UF2);
            dynavUnidadeorganizacional_vinculada2.Name = dynavUnidadeorganizacional_vinculada2_Internalname;
            dynavUnidadeorganizacional_vinculada2.CurrentValue = cgiGet( dynavUnidadeorganizacional_vinculada2_Internalname);
            AV25UnidadeOrganizacional_Vinculada2 = (int)(NumberUtil.Val( cgiGet( dynavUnidadeorganizacional_vinculada2_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25UnidadeOrganizacional_Vinculada2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV25UnidadeOrganizacional_Vinculada2), 6, 0)));
            cmbavDynamicfiltersselector3.Name = cmbavDynamicfiltersselector3_Internalname;
            cmbavDynamicfiltersselector3.CurrentValue = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            AV27DynamicFiltersSelector3 = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27DynamicFiltersSelector3", AV27DynamicFiltersSelector3);
            AV28UnidadeOrganizacional_Nome3 = StringUtil.Upper( cgiGet( edtavUnidadeorganizacional_nome3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV28UnidadeOrganizacional_Nome3", AV28UnidadeOrganizacional_Nome3);
            dynavEstado_uf3.Name = dynavEstado_uf3_Internalname;
            dynavEstado_uf3.CurrentValue = cgiGet( dynavEstado_uf3_Internalname);
            AV29Estado_UF3 = cgiGet( dynavEstado_uf3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29Estado_UF3", AV29Estado_UF3);
            dynavUnidadeorganizacional_vinculada3.Name = dynavUnidadeorganizacional_vinculada3_Internalname;
            dynavUnidadeorganizacional_vinculada3.CurrentValue = cgiGet( dynavUnidadeorganizacional_vinculada3_Internalname);
            AV30UnidadeOrganizacional_Vinculada3 = (int)(NumberUtil.Val( cgiGet( dynavUnidadeorganizacional_vinculada3_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30UnidadeOrganizacional_Vinculada3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV30UnidadeOrganizacional_Vinculada3), 6, 0)));
            A609TpUo_Codigo = (int)(context.localUtil.CToN( cgiGet( edtTpUo_Codigo_Internalname), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A609TpUo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A609TpUo_Codigo), 6, 0)));
            AV21DynamicFiltersEnabled2 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21DynamicFiltersEnabled2", AV21DynamicFiltersEnabled2);
            AV26DynamicFiltersEnabled3 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26DynamicFiltersEnabled3", AV26DynamicFiltersEnabled3);
            AV40TFUnidadeOrganizacional_Nome = StringUtil.Upper( cgiGet( edtavTfunidadeorganizacional_nome_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV40TFUnidadeOrganizacional_Nome", AV40TFUnidadeOrganizacional_Nome);
            AV41TFUnidadeOrganizacional_Nome_Sel = StringUtil.Upper( cgiGet( edtavTfunidadeorganizacional_nome_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV41TFUnidadeOrganizacional_Nome_Sel", AV41TFUnidadeOrganizacional_Nome_Sel);
            AV44TFEstado_UF = StringUtil.Upper( cgiGet( edtavTfestado_uf_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV44TFEstado_UF", AV44TFEstado_UF);
            AV45TFEstado_UF_Sel = StringUtil.Upper( cgiGet( edtavTfestado_uf_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV45TFEstado_UF_Sel", AV45TFEstado_UF_Sel);
            AV48TFUnidadeOrganizacional_Vinculada = StringUtil.Upper( cgiGet( edtavTfunidadeorganizacional_vinculada_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV48TFUnidadeOrganizacional_Vinculada", AV48TFUnidadeOrganizacional_Vinculada);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfunidadeorganizacional_vinculada_sel_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfunidadeorganizacional_vinculada_sel_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFUNIDADEORGANIZACIONAL_VINCULADA_SEL");
               GX_FocusControl = edtavTfunidadeorganizacional_vinculada_sel_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV49TFUnidadeOrganizacional_Vinculada_Sel = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV49TFUnidadeOrganizacional_Vinculada_Sel", StringUtil.LTrim( StringUtil.Str( (decimal)(AV49TFUnidadeOrganizacional_Vinculada_Sel), 6, 0)));
            }
            else
            {
               AV49TFUnidadeOrganizacional_Vinculada_Sel = (int)(context.localUtil.CToN( cgiGet( edtavTfunidadeorganizacional_vinculada_sel_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV49TFUnidadeOrganizacional_Vinculada_Sel", StringUtil.LTrim( StringUtil.Str( (decimal)(AV49TFUnidadeOrganizacional_Vinculada_Sel), 6, 0)));
            }
            AV52TFUnidadeOrganizacional_Vinculada_SelDsc = cgiGet( edtavTfunidadeorganizacional_vinculada_seldsc_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV52TFUnidadeOrganizacional_Vinculada_SelDsc", AV52TFUnidadeOrganizacional_Vinculada_SelDsc);
            AV42ddo_UnidadeOrganizacional_NomeTitleControlIdToReplace = cgiGet( edtavDdo_unidadeorganizacional_nometitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV42ddo_UnidadeOrganizacional_NomeTitleControlIdToReplace", AV42ddo_UnidadeOrganizacional_NomeTitleControlIdToReplace);
            AV46ddo_Estado_UFTitleControlIdToReplace = cgiGet( edtavDdo_estado_uftitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV46ddo_Estado_UFTitleControlIdToReplace", AV46ddo_Estado_UFTitleControlIdToReplace);
            AV50ddo_UnidadeOrganizacional_VinculadaTitleControlIdToReplace = cgiGet( edtavDdo_unidadeorganizacional_vinculadatitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV50ddo_UnidadeOrganizacional_VinculadaTitleControlIdToReplace", AV50ddo_UnidadeOrganizacional_VinculadaTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_83 = (short)(context.localUtil.CToN( cgiGet( sPrefix+"nRC_GXsfl_83"), ",", "."));
            AV54GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( sPrefix+"vGRIDCURRENTPAGE"), ",", "."));
            AV55GridPageCount = (long)(context.localUtil.CToN( cgiGet( sPrefix+"vGRIDPAGECOUNT"), ",", "."));
            wcpOAV7TpUo_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV7TpUo_Codigo"), ",", "."));
            AV56Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"vCODIGO"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( sPrefix+"GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Emptygridcaption");
            Ddo_unidadeorganizacional_nome_Caption = cgiGet( sPrefix+"DDO_UNIDADEORGANIZACIONAL_NOME_Caption");
            Ddo_unidadeorganizacional_nome_Tooltip = cgiGet( sPrefix+"DDO_UNIDADEORGANIZACIONAL_NOME_Tooltip");
            Ddo_unidadeorganizacional_nome_Cls = cgiGet( sPrefix+"DDO_UNIDADEORGANIZACIONAL_NOME_Cls");
            Ddo_unidadeorganizacional_nome_Filteredtext_set = cgiGet( sPrefix+"DDO_UNIDADEORGANIZACIONAL_NOME_Filteredtext_set");
            Ddo_unidadeorganizacional_nome_Selectedvalue_set = cgiGet( sPrefix+"DDO_UNIDADEORGANIZACIONAL_NOME_Selectedvalue_set");
            Ddo_unidadeorganizacional_nome_Dropdownoptionstype = cgiGet( sPrefix+"DDO_UNIDADEORGANIZACIONAL_NOME_Dropdownoptionstype");
            Ddo_unidadeorganizacional_nome_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_UNIDADEORGANIZACIONAL_NOME_Titlecontrolidtoreplace");
            Ddo_unidadeorganizacional_nome_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_UNIDADEORGANIZACIONAL_NOME_Includesortasc"));
            Ddo_unidadeorganizacional_nome_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_UNIDADEORGANIZACIONAL_NOME_Includesortdsc"));
            Ddo_unidadeorganizacional_nome_Sortedstatus = cgiGet( sPrefix+"DDO_UNIDADEORGANIZACIONAL_NOME_Sortedstatus");
            Ddo_unidadeorganizacional_nome_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_UNIDADEORGANIZACIONAL_NOME_Includefilter"));
            Ddo_unidadeorganizacional_nome_Filtertype = cgiGet( sPrefix+"DDO_UNIDADEORGANIZACIONAL_NOME_Filtertype");
            Ddo_unidadeorganizacional_nome_Filterisrange = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_UNIDADEORGANIZACIONAL_NOME_Filterisrange"));
            Ddo_unidadeorganizacional_nome_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_UNIDADEORGANIZACIONAL_NOME_Includedatalist"));
            Ddo_unidadeorganizacional_nome_Datalisttype = cgiGet( sPrefix+"DDO_UNIDADEORGANIZACIONAL_NOME_Datalisttype");
            Ddo_unidadeorganizacional_nome_Datalistproc = cgiGet( sPrefix+"DDO_UNIDADEORGANIZACIONAL_NOME_Datalistproc");
            Ddo_unidadeorganizacional_nome_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( sPrefix+"DDO_UNIDADEORGANIZACIONAL_NOME_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_unidadeorganizacional_nome_Sortasc = cgiGet( sPrefix+"DDO_UNIDADEORGANIZACIONAL_NOME_Sortasc");
            Ddo_unidadeorganizacional_nome_Sortdsc = cgiGet( sPrefix+"DDO_UNIDADEORGANIZACIONAL_NOME_Sortdsc");
            Ddo_unidadeorganizacional_nome_Loadingdata = cgiGet( sPrefix+"DDO_UNIDADEORGANIZACIONAL_NOME_Loadingdata");
            Ddo_unidadeorganizacional_nome_Cleanfilter = cgiGet( sPrefix+"DDO_UNIDADEORGANIZACIONAL_NOME_Cleanfilter");
            Ddo_unidadeorganizacional_nome_Noresultsfound = cgiGet( sPrefix+"DDO_UNIDADEORGANIZACIONAL_NOME_Noresultsfound");
            Ddo_unidadeorganizacional_nome_Searchbuttontext = cgiGet( sPrefix+"DDO_UNIDADEORGANIZACIONAL_NOME_Searchbuttontext");
            Ddo_estado_uf_Caption = cgiGet( sPrefix+"DDO_ESTADO_UF_Caption");
            Ddo_estado_uf_Tooltip = cgiGet( sPrefix+"DDO_ESTADO_UF_Tooltip");
            Ddo_estado_uf_Cls = cgiGet( sPrefix+"DDO_ESTADO_UF_Cls");
            Ddo_estado_uf_Filteredtext_set = cgiGet( sPrefix+"DDO_ESTADO_UF_Filteredtext_set");
            Ddo_estado_uf_Selectedvalue_set = cgiGet( sPrefix+"DDO_ESTADO_UF_Selectedvalue_set");
            Ddo_estado_uf_Dropdownoptionstype = cgiGet( sPrefix+"DDO_ESTADO_UF_Dropdownoptionstype");
            Ddo_estado_uf_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_ESTADO_UF_Titlecontrolidtoreplace");
            Ddo_estado_uf_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_ESTADO_UF_Includesortasc"));
            Ddo_estado_uf_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_ESTADO_UF_Includesortdsc"));
            Ddo_estado_uf_Sortedstatus = cgiGet( sPrefix+"DDO_ESTADO_UF_Sortedstatus");
            Ddo_estado_uf_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_ESTADO_UF_Includefilter"));
            Ddo_estado_uf_Filtertype = cgiGet( sPrefix+"DDO_ESTADO_UF_Filtertype");
            Ddo_estado_uf_Filterisrange = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_ESTADO_UF_Filterisrange"));
            Ddo_estado_uf_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_ESTADO_UF_Includedatalist"));
            Ddo_estado_uf_Datalisttype = cgiGet( sPrefix+"DDO_ESTADO_UF_Datalisttype");
            Ddo_estado_uf_Datalistproc = cgiGet( sPrefix+"DDO_ESTADO_UF_Datalistproc");
            Ddo_estado_uf_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( sPrefix+"DDO_ESTADO_UF_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_estado_uf_Sortasc = cgiGet( sPrefix+"DDO_ESTADO_UF_Sortasc");
            Ddo_estado_uf_Sortdsc = cgiGet( sPrefix+"DDO_ESTADO_UF_Sortdsc");
            Ddo_estado_uf_Loadingdata = cgiGet( sPrefix+"DDO_ESTADO_UF_Loadingdata");
            Ddo_estado_uf_Cleanfilter = cgiGet( sPrefix+"DDO_ESTADO_UF_Cleanfilter");
            Ddo_estado_uf_Noresultsfound = cgiGet( sPrefix+"DDO_ESTADO_UF_Noresultsfound");
            Ddo_estado_uf_Searchbuttontext = cgiGet( sPrefix+"DDO_ESTADO_UF_Searchbuttontext");
            Ddo_unidadeorganizacional_vinculada_Caption = cgiGet( sPrefix+"DDO_UNIDADEORGANIZACIONAL_VINCULADA_Caption");
            Ddo_unidadeorganizacional_vinculada_Tooltip = cgiGet( sPrefix+"DDO_UNIDADEORGANIZACIONAL_VINCULADA_Tooltip");
            Ddo_unidadeorganizacional_vinculada_Cls = cgiGet( sPrefix+"DDO_UNIDADEORGANIZACIONAL_VINCULADA_Cls");
            Ddo_unidadeorganizacional_vinculada_Filteredtext_set = cgiGet( sPrefix+"DDO_UNIDADEORGANIZACIONAL_VINCULADA_Filteredtext_set");
            Ddo_unidadeorganizacional_vinculada_Selectedvalue_set = cgiGet( sPrefix+"DDO_UNIDADEORGANIZACIONAL_VINCULADA_Selectedvalue_set");
            Ddo_unidadeorganizacional_vinculada_Selectedtext_set = cgiGet( sPrefix+"DDO_UNIDADEORGANIZACIONAL_VINCULADA_Selectedtext_set");
            Ddo_unidadeorganizacional_vinculada_Dropdownoptionstype = cgiGet( sPrefix+"DDO_UNIDADEORGANIZACIONAL_VINCULADA_Dropdownoptionstype");
            Ddo_unidadeorganizacional_vinculada_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_UNIDADEORGANIZACIONAL_VINCULADA_Titlecontrolidtoreplace");
            Ddo_unidadeorganizacional_vinculada_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_UNIDADEORGANIZACIONAL_VINCULADA_Includesortasc"));
            Ddo_unidadeorganizacional_vinculada_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_UNIDADEORGANIZACIONAL_VINCULADA_Includesortdsc"));
            Ddo_unidadeorganizacional_vinculada_Sortedstatus = cgiGet( sPrefix+"DDO_UNIDADEORGANIZACIONAL_VINCULADA_Sortedstatus");
            Ddo_unidadeorganizacional_vinculada_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_UNIDADEORGANIZACIONAL_VINCULADA_Includefilter"));
            Ddo_unidadeorganizacional_vinculada_Filtertype = cgiGet( sPrefix+"DDO_UNIDADEORGANIZACIONAL_VINCULADA_Filtertype");
            Ddo_unidadeorganizacional_vinculada_Filterisrange = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_UNIDADEORGANIZACIONAL_VINCULADA_Filterisrange"));
            Ddo_unidadeorganizacional_vinculada_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_UNIDADEORGANIZACIONAL_VINCULADA_Includedatalist"));
            Ddo_unidadeorganizacional_vinculada_Datalisttype = cgiGet( sPrefix+"DDO_UNIDADEORGANIZACIONAL_VINCULADA_Datalisttype");
            Ddo_unidadeorganizacional_vinculada_Datalistproc = cgiGet( sPrefix+"DDO_UNIDADEORGANIZACIONAL_VINCULADA_Datalistproc");
            Ddo_unidadeorganizacional_vinculada_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( sPrefix+"DDO_UNIDADEORGANIZACIONAL_VINCULADA_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_unidadeorganizacional_vinculada_Sortasc = cgiGet( sPrefix+"DDO_UNIDADEORGANIZACIONAL_VINCULADA_Sortasc");
            Ddo_unidadeorganizacional_vinculada_Sortdsc = cgiGet( sPrefix+"DDO_UNIDADEORGANIZACIONAL_VINCULADA_Sortdsc");
            Ddo_unidadeorganizacional_vinculada_Loadingdata = cgiGet( sPrefix+"DDO_UNIDADEORGANIZACIONAL_VINCULADA_Loadingdata");
            Ddo_unidadeorganizacional_vinculada_Cleanfilter = cgiGet( sPrefix+"DDO_UNIDADEORGANIZACIONAL_VINCULADA_Cleanfilter");
            Ddo_unidadeorganizacional_vinculada_Noresultsfound = cgiGet( sPrefix+"DDO_UNIDADEORGANIZACIONAL_VINCULADA_Noresultsfound");
            Ddo_unidadeorganizacional_vinculada_Searchbuttontext = cgiGet( sPrefix+"DDO_UNIDADEORGANIZACIONAL_VINCULADA_Searchbuttontext");
            Gridpaginationbar_Selectedpage = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Selectedpage");
            Ddo_unidadeorganizacional_nome_Activeeventkey = cgiGet( sPrefix+"DDO_UNIDADEORGANIZACIONAL_NOME_Activeeventkey");
            Ddo_unidadeorganizacional_nome_Filteredtext_get = cgiGet( sPrefix+"DDO_UNIDADEORGANIZACIONAL_NOME_Filteredtext_get");
            Ddo_unidadeorganizacional_nome_Selectedvalue_get = cgiGet( sPrefix+"DDO_UNIDADEORGANIZACIONAL_NOME_Selectedvalue_get");
            Ddo_estado_uf_Activeeventkey = cgiGet( sPrefix+"DDO_ESTADO_UF_Activeeventkey");
            Ddo_estado_uf_Filteredtext_get = cgiGet( sPrefix+"DDO_ESTADO_UF_Filteredtext_get");
            Ddo_estado_uf_Selectedvalue_get = cgiGet( sPrefix+"DDO_ESTADO_UF_Selectedvalue_get");
            Ddo_unidadeorganizacional_vinculada_Activeeventkey = cgiGet( sPrefix+"DDO_UNIDADEORGANIZACIONAL_VINCULADA_Activeeventkey");
            Ddo_unidadeorganizacional_vinculada_Filteredtext_get = cgiGet( sPrefix+"DDO_UNIDADEORGANIZACIONAL_VINCULADA_Filteredtext_get");
            Ddo_unidadeorganizacional_vinculada_Selectedvalue_get = cgiGet( sPrefix+"DDO_UNIDADEORGANIZACIONAL_VINCULADA_Selectedvalue_get");
            Ddo_unidadeorganizacional_vinculada_Selectedtext_get = cgiGet( sPrefix+"DDO_UNIDADEORGANIZACIONAL_VINCULADA_Selectedtext_get");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV14OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vORDEREDDSC")) != AV15OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vDYNAMICFILTERSSELECTOR1"), AV17DynamicFiltersSelector1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vUNIDADEORGANIZACIONAL_NOME1"), AV18UnidadeOrganizacional_Nome1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vESTADO_UF1"), AV19Estado_UF1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vUNIDADEORGANIZACIONAL_VINCULADA1"), ",", ".") != Convert.ToDecimal( AV20UnidadeOrganizacional_Vinculada1 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vDYNAMICFILTERSSELECTOR2"), AV22DynamicFiltersSelector2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vUNIDADEORGANIZACIONAL_NOME2"), AV23UnidadeOrganizacional_Nome2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vESTADO_UF2"), AV24Estado_UF2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vUNIDADEORGANIZACIONAL_VINCULADA2"), ",", ".") != Convert.ToDecimal( AV25UnidadeOrganizacional_Vinculada2 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vDYNAMICFILTERSSELECTOR3"), AV27DynamicFiltersSelector3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vUNIDADEORGANIZACIONAL_NOME3"), AV28UnidadeOrganizacional_Nome3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vESTADO_UF3"), AV29Estado_UF3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vUNIDADEORGANIZACIONAL_VINCULADA3"), ",", ".") != Convert.ToDecimal( AV30UnidadeOrganizacional_Vinculada3 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vDYNAMICFILTERSENABLED2")) != AV21DynamicFiltersEnabled2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vDYNAMICFILTERSENABLED3")) != AV26DynamicFiltersEnabled3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFUNIDADEORGANIZACIONAL_NOME"), AV40TFUnidadeOrganizacional_Nome) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFUNIDADEORGANIZACIONAL_NOME_SEL"), AV41TFUnidadeOrganizacional_Nome_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFESTADO_UF"), AV44TFEstado_UF) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFESTADO_UF_SEL"), AV45TFEstado_UF_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFUNIDADEORGANIZACIONAL_VINCULADA"), AV48TFUnidadeOrganizacional_Vinculada) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFUNIDADEORGANIZACIONAL_VINCULADA_SEL"), ",", ".") != Convert.ToDecimal( AV49TFUnidadeOrganizacional_Vinculada_Sel )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E26D82 */
         E26D82 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E26D82( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         lblJsdynamicfilters_Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         chkavDynamicfiltersenabled2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkavDynamicfiltersenabled2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled2.Visible), 5, 0)));
         chkavDynamicfiltersenabled3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkavDynamicfiltersenabled3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled3.Visible), 5, 0)));
         AV17DynamicFiltersSelector1 = "UNIDADEORGANIZACIONAL_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17DynamicFiltersSelector1", AV17DynamicFiltersSelector1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV22DynamicFiltersSelector2 = "UNIDADEORGANIZACIONAL_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22DynamicFiltersSelector2", AV22DynamicFiltersSelector2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV27DynamicFiltersSelector3 = "UNIDADEORGANIZACIONAL_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27DynamicFiltersSelector3", AV27DynamicFiltersSelector3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         imgAdddynamicfilters1_Jsonclick = "WWPDynFilterShow(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgAdddynamicfilters1_Internalname, "Jsonclick", imgAdddynamicfilters1_Jsonclick);
         imgRemovedynamicfilters1_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters1_Internalname, "Jsonclick", imgRemovedynamicfilters1_Jsonclick);
         imgAdddynamicfilters2_Jsonclick = "WWPDynFilterShow(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgAdddynamicfilters2_Internalname, "Jsonclick", imgAdddynamicfilters2_Jsonclick);
         imgRemovedynamicfilters2_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters2_Internalname, "Jsonclick", imgRemovedynamicfilters2_Jsonclick);
         imgRemovedynamicfilters3_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters3_Internalname, "Jsonclick", imgRemovedynamicfilters3_Jsonclick);
         edtavTfunidadeorganizacional_nome_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfunidadeorganizacional_nome_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfunidadeorganizacional_nome_Visible), 5, 0)));
         edtavTfunidadeorganizacional_nome_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfunidadeorganizacional_nome_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfunidadeorganizacional_nome_sel_Visible), 5, 0)));
         edtavTfestado_uf_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfestado_uf_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfestado_uf_Visible), 5, 0)));
         edtavTfestado_uf_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfestado_uf_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfestado_uf_sel_Visible), 5, 0)));
         edtavTfunidadeorganizacional_vinculada_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfunidadeorganizacional_vinculada_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfunidadeorganizacional_vinculada_Visible), 5, 0)));
         edtavTfunidadeorganizacional_vinculada_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfunidadeorganizacional_vinculada_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfunidadeorganizacional_vinculada_sel_Visible), 5, 0)));
         edtavTfunidadeorganizacional_vinculada_seldsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfunidadeorganizacional_vinculada_seldsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfunidadeorganizacional_vinculada_seldsc_Visible), 5, 0)));
         Ddo_unidadeorganizacional_nome_Titlecontrolidtoreplace = subGrid_Internalname+"_UnidadeOrganizacional_Nome";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_unidadeorganizacional_nome_Internalname, "TitleControlIdToReplace", Ddo_unidadeorganizacional_nome_Titlecontrolidtoreplace);
         AV42ddo_UnidadeOrganizacional_NomeTitleControlIdToReplace = Ddo_unidadeorganizacional_nome_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV42ddo_UnidadeOrganizacional_NomeTitleControlIdToReplace", AV42ddo_UnidadeOrganizacional_NomeTitleControlIdToReplace);
         edtavDdo_unidadeorganizacional_nometitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_unidadeorganizacional_nometitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_unidadeorganizacional_nometitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_estado_uf_Titlecontrolidtoreplace = subGrid_Internalname+"_Estado_UF";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_estado_uf_Internalname, "TitleControlIdToReplace", Ddo_estado_uf_Titlecontrolidtoreplace);
         AV46ddo_Estado_UFTitleControlIdToReplace = Ddo_estado_uf_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV46ddo_Estado_UFTitleControlIdToReplace", AV46ddo_Estado_UFTitleControlIdToReplace);
         edtavDdo_estado_uftitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_estado_uftitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_estado_uftitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_unidadeorganizacional_vinculada_Titlecontrolidtoreplace = subGrid_Internalname+"_UnidadeOrganizacional_Vinculada";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_unidadeorganizacional_vinculada_Internalname, "TitleControlIdToReplace", Ddo_unidadeorganizacional_vinculada_Titlecontrolidtoreplace);
         AV50ddo_UnidadeOrganizacional_VinculadaTitleControlIdToReplace = Ddo_unidadeorganizacional_vinculada_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV50ddo_UnidadeOrganizacional_VinculadaTitleControlIdToReplace", AV50ddo_UnidadeOrganizacional_VinculadaTitleControlIdToReplace);
         edtavDdo_unidadeorganizacional_vinculadatitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_unidadeorganizacional_vinculadatitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_unidadeorganizacional_vinculadatitlecontrolidtoreplace_Visible), 5, 0)));
         edtTpUo_Codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtTpUo_Codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtTpUo_Codigo_Visible), 5, 0)));
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavOrderedby.removeAllItems();
         cmbavOrderedby.addItem("1", "Organizacional", 0);
         cmbavOrderedby.addItem("2", "UF", 0);
         cmbavOrderedby.addItem("3", "UO Vinculada", 0);
         if ( AV14OrderedBy < 1 )
         {
            AV14OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         imgCleanfilters_Jsonclick = "WWPDynFilterHideAll(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgCleanfilters_Internalname, "Jsonclick", imgCleanfilters_Jsonclick);
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = AV51DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1) ;
         AV51DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1;
      }

      protected void E27D82( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV39UnidadeOrganizacional_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV43Estado_UFTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV47UnidadeOrganizacional_VinculadaTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'CHECKSECURITYFORACTIONS' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtUnidadeOrganizacional_Nome_Titleformat = 2;
         edtUnidadeOrganizacional_Nome_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Nome", AV42ddo_UnidadeOrganizacional_NomeTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtUnidadeOrganizacional_Nome_Internalname, "Title", edtUnidadeOrganizacional_Nome_Title);
         edtEstado_UF_Titleformat = 2;
         edtEstado_UF_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "UF", AV46ddo_Estado_UFTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtEstado_UF_Internalname, "Title", edtEstado_UF_Title);
         dynUnidadeOrganizacional_Vinculada_Titleformat = 2;
         dynUnidadeOrganizacional_Vinculada.Title.Text = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "UO Vinculada", AV50ddo_UnidadeOrganizacional_VinculadaTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, dynUnidadeOrganizacional_Vinculada_Internalname, "Title", dynUnidadeOrganizacional_Vinculada.Title.Text);
         AV54GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV54GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV54GridCurrentPage), 10, 0)));
         AV55GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV55GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV55GridPageCount), 10, 0)));
         imgInsert_Visible = (AV6WWPContext.gxTpr_Userehadministradorgam||AV6WWPContext.gxTpr_Insert ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgInsert_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgInsert_Visible), 5, 0)));
         edtavUpdate_Visible = (AV6WWPContext.gxTpr_Userehadministradorgam||AV6WWPContext.gxTpr_Update ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavUpdate_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUpdate_Visible), 5, 0)));
         edtavDelete_Visible = (AV6WWPContext.gxTpr_Userehadministradorgam||AV6WWPContext.gxTpr_Delete ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDelete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDelete_Visible), 5, 0)));
         edtavDisplay_Visible = (AV6WWPContext.gxTpr_Userehadministradorgam||AV6WWPContext.gxTpr_Display ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDisplay_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDisplay_Visible), 5, 0)));
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV39UnidadeOrganizacional_NomeTitleFilterData", AV39UnidadeOrganizacional_NomeTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV43Estado_UFTitleFilterData", AV43Estado_UFTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV47UnidadeOrganizacional_VinculadaTitleFilterData", AV47UnidadeOrganizacional_VinculadaTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV6WWPContext", AV6WWPContext);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV11GridState", AV11GridState);
      }

      protected void E11D82( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV53PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV53PageToGo) ;
         }
      }

      protected void E12D82( )
      {
         /* Ddo_unidadeorganizacional_nome_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_unidadeorganizacional_nome_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S192 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_unidadeorganizacional_nome_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_unidadeorganizacional_nome_Internalname, "SortedStatus", Ddo_unidadeorganizacional_nome_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_unidadeorganizacional_nome_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S192 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_unidadeorganizacional_nome_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_unidadeorganizacional_nome_Internalname, "SortedStatus", Ddo_unidadeorganizacional_nome_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_unidadeorganizacional_nome_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV40TFUnidadeOrganizacional_Nome = Ddo_unidadeorganizacional_nome_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV40TFUnidadeOrganizacional_Nome", AV40TFUnidadeOrganizacional_Nome);
            AV41TFUnidadeOrganizacional_Nome_Sel = Ddo_unidadeorganizacional_nome_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV41TFUnidadeOrganizacional_Nome_Sel", AV41TFUnidadeOrganizacional_Nome_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E13D82( )
      {
         /* Ddo_estado_uf_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_estado_uf_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S192 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_estado_uf_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_estado_uf_Internalname, "SortedStatus", Ddo_estado_uf_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_estado_uf_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S192 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_estado_uf_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_estado_uf_Internalname, "SortedStatus", Ddo_estado_uf_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_estado_uf_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV44TFEstado_UF = Ddo_estado_uf_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV44TFEstado_UF", AV44TFEstado_UF);
            AV45TFEstado_UF_Sel = Ddo_estado_uf_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV45TFEstado_UF_Sel", AV45TFEstado_UF_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E14D82( )
      {
         /* Ddo_unidadeorganizacional_vinculada_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_unidadeorganizacional_vinculada_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S192 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_unidadeorganizacional_vinculada_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_unidadeorganizacional_vinculada_Internalname, "SortedStatus", Ddo_unidadeorganizacional_vinculada_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_unidadeorganizacional_vinculada_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S192 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_unidadeorganizacional_vinculada_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_unidadeorganizacional_vinculada_Internalname, "SortedStatus", Ddo_unidadeorganizacional_vinculada_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_unidadeorganizacional_vinculada_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV48TFUnidadeOrganizacional_Vinculada = Ddo_unidadeorganizacional_vinculada_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV48TFUnidadeOrganizacional_Vinculada", AV48TFUnidadeOrganizacional_Vinculada);
            AV49TFUnidadeOrganizacional_Vinculada_Sel = (int)(NumberUtil.Val( Ddo_unidadeorganizacional_vinculada_Selectedvalue_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV49TFUnidadeOrganizacional_Vinculada_Sel", StringUtil.LTrim( StringUtil.Str( (decimal)(AV49TFUnidadeOrganizacional_Vinculada_Sel), 6, 0)));
            AV52TFUnidadeOrganizacional_Vinculada_SelDsc = Ddo_unidadeorganizacional_vinculada_Selectedtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV52TFUnidadeOrganizacional_Vinculada_SelDsc", AV52TFUnidadeOrganizacional_Vinculada_SelDsc);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      private void E28D82( )
      {
         /* Grid_Load Routine */
         edtavUpdate_Tooltiptext = "Modifica";
         if ( ( AV6WWPContext.gxTpr_Insert && AV6WWPContext.gxTpr_Update && AV6WWPContext.gxTpr_Delete ) || AV6WWPContext.gxTpr_Userehadministradorgam )
         {
            edtavUpdate_Link = formatLink("geral_unidadeorganizacional.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A611UnidadeOrganizacional_Codigo);
            AV33Update = context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavUpdate_Internalname, AV33Update);
            AV59Update_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( )));
            edtavUpdate_Enabled = 1;
         }
         else
         {
            edtavUpdate_Link = "";
            AV33Update = context.GetImagePath( "0211baea-a1e2-4bb5-849c-f3afb69b09ee", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavUpdate_Internalname, AV33Update);
            AV59Update_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "0211baea-a1e2-4bb5-849c-f3afb69b09ee", "", context.GetTheme( )));
            edtavUpdate_Enabled = 0;
         }
         edtavDelete_Tooltiptext = "Eliminar";
         if ( ( AV6WWPContext.gxTpr_Insert && AV6WWPContext.gxTpr_Update && AV6WWPContext.gxTpr_Delete ) || AV6WWPContext.gxTpr_Userehadministradorgam )
         {
            edtavDelete_Link = formatLink("geral_unidadeorganizacional.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A611UnidadeOrganizacional_Codigo);
            AV34Delete = context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavDelete_Internalname, AV34Delete);
            AV60Delete_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )));
            edtavDelete_Enabled = 1;
         }
         else
         {
            edtavDelete_Link = "";
            AV34Delete = context.GetImagePath( "138fdfa8-ae9b-4664-8007-e04d47468418", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavDelete_Internalname, AV34Delete);
            AV60Delete_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "138fdfa8-ae9b-4664-8007-e04d47468418", "", context.GetTheme( )));
            edtavDelete_Enabled = 0;
         }
         AV35Display = context.GetImagePath( "f11923b6-6acd-4a79-bfc0-0cfc6f3bced5", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavDisplay_Internalname, AV35Display);
         AV61Display_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "f11923b6-6acd-4a79-bfc0-0cfc6f3bced5", "", context.GetTheme( )));
         edtavDisplay_Tooltiptext = "Mostrar";
         edtavDisplay_Link = formatLink("viewgeral_unidadeorganizacional.aspx") + "?" + UrlEncode("" +A611UnidadeOrganizacional_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
         edtUnidadeOrganizacional_Nome_Link = formatLink("viewgeral_unidadeorganizacional.aspx") + "?" + UrlEncode("" +A611UnidadeOrganizacional_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 83;
         }
         sendrow_832( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_83_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(83, GridRow);
         }
      }

      protected void E15D82( )
      {
         /* Orderedby_Click Routine */
         context.DoAjaxRefreshCmp(sPrefix);
      }

      protected void E21D82( )
      {
         /* 'AddDynamicFilters1' Routine */
         AV21DynamicFiltersEnabled2 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21DynamicFiltersEnabled2", AV21DynamicFiltersEnabled2);
         imgAdddynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
      }

      protected void E16D82( )
      {
         /* 'RemoveDynamicFilters1' Routine */
         AV31DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV31DynamicFiltersRemoving", AV31DynamicFiltersRemoving);
         AV32DynamicFiltersIgnoreFirst = true;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV32DynamicFiltersIgnoreFirst", AV32DynamicFiltersIgnoreFirst);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S222 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV31DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV31DynamicFiltersRemoving", AV31DynamicFiltersRemoving);
         AV32DynamicFiltersIgnoreFirst = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV32DynamicFiltersIgnoreFirst", AV32DynamicFiltersIgnoreFirst);
         gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV17DynamicFiltersSelector1, AV18UnidadeOrganizacional_Nome1, AV19Estado_UF1, AV20UnidadeOrganizacional_Vinculada1, AV22DynamicFiltersSelector2, AV23UnidadeOrganizacional_Nome2, AV24Estado_UF2, AV25UnidadeOrganizacional_Vinculada2, AV27DynamicFiltersSelector3, AV28UnidadeOrganizacional_Nome3, AV29Estado_UF3, AV30UnidadeOrganizacional_Vinculada3, AV21DynamicFiltersEnabled2, AV26DynamicFiltersEnabled3, AV40TFUnidadeOrganizacional_Nome, AV41TFUnidadeOrganizacional_Nome_Sel, AV44TFEstado_UF, AV45TFEstado_UF_Sel, AV48TFUnidadeOrganizacional_Vinculada, AV49TFUnidadeOrganizacional_Vinculada_Sel, AV7TpUo_Codigo, AV42ddo_UnidadeOrganizacional_NomeTitleControlIdToReplace, AV46ddo_Estado_UFTitleControlIdToReplace, AV50ddo_UnidadeOrganizacional_VinculadaTitleControlIdToReplace, AV6WWPContext, AV62Pgmname, AV16UnidadeOrganizacional_Ativo, AV52TFUnidadeOrganizacional_Vinculada_SelDsc, AV11GridState, AV32DynamicFiltersIgnoreFirst, AV31DynamicFiltersRemoving, A611UnidadeOrganizacional_Codigo, AV64Unidadeorganizacional_codigo, sPrefix) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV11GridState", AV11GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV22DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV27DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV17DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         dynavEstado_uf1.CurrentValue = StringUtil.RTrim( AV19Estado_UF1);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, dynavEstado_uf1_Internalname, "Values", dynavEstado_uf1.ToJavascriptSource());
         dynavUnidadeorganizacional_vinculada1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV20UnidadeOrganizacional_Vinculada1), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, dynavUnidadeorganizacional_vinculada1_Internalname, "Values", dynavUnidadeorganizacional_vinculada1.ToJavascriptSource());
         dynavEstado_uf2.CurrentValue = StringUtil.RTrim( AV24Estado_UF2);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, dynavEstado_uf2_Internalname, "Values", dynavEstado_uf2.ToJavascriptSource());
         dynavUnidadeorganizacional_vinculada2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV25UnidadeOrganizacional_Vinculada2), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, dynavUnidadeorganizacional_vinculada2_Internalname, "Values", dynavUnidadeorganizacional_vinculada2.ToJavascriptSource());
         dynavEstado_uf3.CurrentValue = StringUtil.RTrim( AV29Estado_UF3);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, dynavEstado_uf3_Internalname, "Values", dynavEstado_uf3.ToJavascriptSource());
         dynavUnidadeorganizacional_vinculada3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV30UnidadeOrganizacional_Vinculada3), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, dynavUnidadeorganizacional_vinculada3_Internalname, "Values", dynavUnidadeorganizacional_vinculada3.ToJavascriptSource());
      }

      protected void E22D82( )
      {
         /* Dynamicfiltersselector1_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E23D82( )
      {
         /* 'AddDynamicFilters2' Routine */
         AV26DynamicFiltersEnabled3 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26DynamicFiltersEnabled3", AV26DynamicFiltersEnabled3);
         imgAdddynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
      }

      protected void E17D82( )
      {
         /* 'RemoveDynamicFilters2' Routine */
         AV31DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV31DynamicFiltersRemoving", AV31DynamicFiltersRemoving);
         AV21DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21DynamicFiltersEnabled2", AV21DynamicFiltersEnabled2);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S222 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV31DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV31DynamicFiltersRemoving", AV31DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV17DynamicFiltersSelector1, AV18UnidadeOrganizacional_Nome1, AV19Estado_UF1, AV20UnidadeOrganizacional_Vinculada1, AV22DynamicFiltersSelector2, AV23UnidadeOrganizacional_Nome2, AV24Estado_UF2, AV25UnidadeOrganizacional_Vinculada2, AV27DynamicFiltersSelector3, AV28UnidadeOrganizacional_Nome3, AV29Estado_UF3, AV30UnidadeOrganizacional_Vinculada3, AV21DynamicFiltersEnabled2, AV26DynamicFiltersEnabled3, AV40TFUnidadeOrganizacional_Nome, AV41TFUnidadeOrganizacional_Nome_Sel, AV44TFEstado_UF, AV45TFEstado_UF_Sel, AV48TFUnidadeOrganizacional_Vinculada, AV49TFUnidadeOrganizacional_Vinculada_Sel, AV7TpUo_Codigo, AV42ddo_UnidadeOrganizacional_NomeTitleControlIdToReplace, AV46ddo_Estado_UFTitleControlIdToReplace, AV50ddo_UnidadeOrganizacional_VinculadaTitleControlIdToReplace, AV6WWPContext, AV62Pgmname, AV16UnidadeOrganizacional_Ativo, AV52TFUnidadeOrganizacional_Vinculada_SelDsc, AV11GridState, AV32DynamicFiltersIgnoreFirst, AV31DynamicFiltersRemoving, A611UnidadeOrganizacional_Codigo, AV64Unidadeorganizacional_codigo, sPrefix) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV11GridState", AV11GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV22DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV27DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV17DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         dynavEstado_uf1.CurrentValue = StringUtil.RTrim( AV19Estado_UF1);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, dynavEstado_uf1_Internalname, "Values", dynavEstado_uf1.ToJavascriptSource());
         dynavUnidadeorganizacional_vinculada1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV20UnidadeOrganizacional_Vinculada1), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, dynavUnidadeorganizacional_vinculada1_Internalname, "Values", dynavUnidadeorganizacional_vinculada1.ToJavascriptSource());
         dynavEstado_uf2.CurrentValue = StringUtil.RTrim( AV24Estado_UF2);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, dynavEstado_uf2_Internalname, "Values", dynavEstado_uf2.ToJavascriptSource());
         dynavUnidadeorganizacional_vinculada2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV25UnidadeOrganizacional_Vinculada2), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, dynavUnidadeorganizacional_vinculada2_Internalname, "Values", dynavUnidadeorganizacional_vinculada2.ToJavascriptSource());
         dynavEstado_uf3.CurrentValue = StringUtil.RTrim( AV29Estado_UF3);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, dynavEstado_uf3_Internalname, "Values", dynavEstado_uf3.ToJavascriptSource());
         dynavUnidadeorganizacional_vinculada3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV30UnidadeOrganizacional_Vinculada3), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, dynavUnidadeorganizacional_vinculada3_Internalname, "Values", dynavUnidadeorganizacional_vinculada3.ToJavascriptSource());
      }

      protected void E24D82( )
      {
         /* Dynamicfiltersselector2_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E18D82( )
      {
         /* 'RemoveDynamicFilters3' Routine */
         AV31DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV31DynamicFiltersRemoving", AV31DynamicFiltersRemoving);
         AV26DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26DynamicFiltersEnabled3", AV26DynamicFiltersEnabled3);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S222 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV31DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV31DynamicFiltersRemoving", AV31DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV17DynamicFiltersSelector1, AV18UnidadeOrganizacional_Nome1, AV19Estado_UF1, AV20UnidadeOrganizacional_Vinculada1, AV22DynamicFiltersSelector2, AV23UnidadeOrganizacional_Nome2, AV24Estado_UF2, AV25UnidadeOrganizacional_Vinculada2, AV27DynamicFiltersSelector3, AV28UnidadeOrganizacional_Nome3, AV29Estado_UF3, AV30UnidadeOrganizacional_Vinculada3, AV21DynamicFiltersEnabled2, AV26DynamicFiltersEnabled3, AV40TFUnidadeOrganizacional_Nome, AV41TFUnidadeOrganizacional_Nome_Sel, AV44TFEstado_UF, AV45TFEstado_UF_Sel, AV48TFUnidadeOrganizacional_Vinculada, AV49TFUnidadeOrganizacional_Vinculada_Sel, AV7TpUo_Codigo, AV42ddo_UnidadeOrganizacional_NomeTitleControlIdToReplace, AV46ddo_Estado_UFTitleControlIdToReplace, AV50ddo_UnidadeOrganizacional_VinculadaTitleControlIdToReplace, AV6WWPContext, AV62Pgmname, AV16UnidadeOrganizacional_Ativo, AV52TFUnidadeOrganizacional_Vinculada_SelDsc, AV11GridState, AV32DynamicFiltersIgnoreFirst, AV31DynamicFiltersRemoving, A611UnidadeOrganizacional_Codigo, AV64Unidadeorganizacional_codigo, sPrefix) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV11GridState", AV11GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV22DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV27DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV17DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         dynavEstado_uf1.CurrentValue = StringUtil.RTrim( AV19Estado_UF1);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, dynavEstado_uf1_Internalname, "Values", dynavEstado_uf1.ToJavascriptSource());
         dynavUnidadeorganizacional_vinculada1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV20UnidadeOrganizacional_Vinculada1), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, dynavUnidadeorganizacional_vinculada1_Internalname, "Values", dynavUnidadeorganizacional_vinculada1.ToJavascriptSource());
         dynavEstado_uf2.CurrentValue = StringUtil.RTrim( AV24Estado_UF2);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, dynavEstado_uf2_Internalname, "Values", dynavEstado_uf2.ToJavascriptSource());
         dynavUnidadeorganizacional_vinculada2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV25UnidadeOrganizacional_Vinculada2), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, dynavUnidadeorganizacional_vinculada2_Internalname, "Values", dynavUnidadeorganizacional_vinculada2.ToJavascriptSource());
         dynavEstado_uf3.CurrentValue = StringUtil.RTrim( AV29Estado_UF3);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, dynavEstado_uf3_Internalname, "Values", dynavEstado_uf3.ToJavascriptSource());
         dynavUnidadeorganizacional_vinculada3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV30UnidadeOrganizacional_Vinculada3), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, dynavUnidadeorganizacional_vinculada3_Internalname, "Values", dynavUnidadeorganizacional_vinculada3.ToJavascriptSource());
      }

      protected void E25D82( )
      {
         /* Dynamicfiltersselector3_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E19D82( )
      {
         /* 'DoCleanFilters' Routine */
         /* Execute user subroutine: 'CLEANFILTERS' */
         S232 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_firstpage( ) ;
         context.DoAjaxRefreshCmp(sPrefix);
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV17DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV11GridState", AV11GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV22DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV27DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         dynavEstado_uf1.CurrentValue = StringUtil.RTrim( AV19Estado_UF1);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, dynavEstado_uf1_Internalname, "Values", dynavEstado_uf1.ToJavascriptSource());
         dynavUnidadeorganizacional_vinculada1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV20UnidadeOrganizacional_Vinculada1), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, dynavUnidadeorganizacional_vinculada1_Internalname, "Values", dynavUnidadeorganizacional_vinculada1.ToJavascriptSource());
         dynavEstado_uf2.CurrentValue = StringUtil.RTrim( AV24Estado_UF2);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, dynavEstado_uf2_Internalname, "Values", dynavEstado_uf2.ToJavascriptSource());
         dynavUnidadeorganizacional_vinculada2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV25UnidadeOrganizacional_Vinculada2), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, dynavUnidadeorganizacional_vinculada2_Internalname, "Values", dynavUnidadeorganizacional_vinculada2.ToJavascriptSource());
         dynavEstado_uf3.CurrentValue = StringUtil.RTrim( AV29Estado_UF3);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, dynavEstado_uf3_Internalname, "Values", dynavEstado_uf3.ToJavascriptSource());
         dynavUnidadeorganizacional_vinculada3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV30UnidadeOrganizacional_Vinculada3), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, dynavUnidadeorganizacional_vinculada3_Internalname, "Values", dynavUnidadeorganizacional_vinculada3.ToJavascriptSource());
      }

      protected void E20D82( )
      {
         /* 'DoInsert' Routine */
         context.wjLoc = formatLink("geral_unidadeorganizacional.aspx") + "?" + UrlEncode(StringUtil.RTrim("INS")) + "," + UrlEncode("" +0);
         context.wjLocDisableFrm = 1;
      }

      protected void S192( )
      {
         /* 'RESETDDOSORTEDSTATUS' Routine */
         Ddo_unidadeorganizacional_nome_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_unidadeorganizacional_nome_Internalname, "SortedStatus", Ddo_unidadeorganizacional_nome_Sortedstatus);
         Ddo_estado_uf_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_estado_uf_Internalname, "SortedStatus", Ddo_estado_uf_Sortedstatus);
         Ddo_unidadeorganizacional_vinculada_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_unidadeorganizacional_vinculada_Internalname, "SortedStatus", Ddo_unidadeorganizacional_vinculada_Sortedstatus);
      }

      protected void S162( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV14OrderedBy == 1 )
         {
            Ddo_unidadeorganizacional_nome_Sortedstatus = (AV15OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_unidadeorganizacional_nome_Internalname, "SortedStatus", Ddo_unidadeorganizacional_nome_Sortedstatus);
         }
         else if ( AV14OrderedBy == 2 )
         {
            Ddo_estado_uf_Sortedstatus = (AV15OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_estado_uf_Internalname, "SortedStatus", Ddo_estado_uf_Sortedstatus);
         }
         else if ( AV14OrderedBy == 3 )
         {
            Ddo_unidadeorganizacional_vinculada_Sortedstatus = (AV15OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_unidadeorganizacional_vinculada_Internalname, "SortedStatus", Ddo_unidadeorganizacional_vinculada_Sortedstatus);
         }
      }

      protected void S172( )
      {
         /* 'CHECKSECURITYFORACTIONS' Routine */
         if ( ! ( ( AV6WWPContext.gxTpr_Insert && AV6WWPContext.gxTpr_Update && AV6WWPContext.gxTpr_Delete ) || AV6WWPContext.gxTpr_Userehadministradorgam ) )
         {
            imgInsert_Link = "";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgInsert_Internalname, "Link", imgInsert_Link);
            imgInsert_Bitmap = context.GetImagePath( "13d28f37-c579-4dd9-a404-ba51ab71d1e3", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgInsert_Internalname, "Bitmap", context.convertURL( context.PathToRelativeUrl( imgInsert_Bitmap)));
            imgInsert_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgInsert_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgInsert_Enabled), 5, 0)));
         }
      }

      protected void S112( )
      {
         /* 'ENABLEDYNAMICFILTERS1' Routine */
         edtavUnidadeorganizacional_nome1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavUnidadeorganizacional_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUnidadeorganizacional_nome1_Visible), 5, 0)));
         dynavEstado_uf1.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, dynavEstado_uf1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavEstado_uf1.Visible), 5, 0)));
         dynavUnidadeorganizacional_vinculada1.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, dynavUnidadeorganizacional_vinculada1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavUnidadeorganizacional_vinculada1.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV17DynamicFiltersSelector1, "UNIDADEORGANIZACIONAL_NOME") == 0 )
         {
            edtavUnidadeorganizacional_nome1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavUnidadeorganizacional_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUnidadeorganizacional_nome1_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV17DynamicFiltersSelector1, "ESTADO_UF") == 0 )
         {
            dynavEstado_uf1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, dynavEstado_uf1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavEstado_uf1.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV17DynamicFiltersSelector1, "UNIDADEORGANIZACIONAL_VINCULADA") == 0 )
         {
            dynavUnidadeorganizacional_vinculada1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, dynavUnidadeorganizacional_vinculada1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavUnidadeorganizacional_vinculada1.Visible), 5, 0)));
         }
      }

      protected void S122( )
      {
         /* 'ENABLEDYNAMICFILTERS2' Routine */
         edtavUnidadeorganizacional_nome2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavUnidadeorganizacional_nome2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUnidadeorganizacional_nome2_Visible), 5, 0)));
         dynavEstado_uf2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, dynavEstado_uf2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavEstado_uf2.Visible), 5, 0)));
         dynavUnidadeorganizacional_vinculada2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, dynavUnidadeorganizacional_vinculada2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavUnidadeorganizacional_vinculada2.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV22DynamicFiltersSelector2, "UNIDADEORGANIZACIONAL_NOME") == 0 )
         {
            edtavUnidadeorganizacional_nome2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavUnidadeorganizacional_nome2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUnidadeorganizacional_nome2_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV22DynamicFiltersSelector2, "ESTADO_UF") == 0 )
         {
            dynavEstado_uf2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, dynavEstado_uf2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavEstado_uf2.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV22DynamicFiltersSelector2, "UNIDADEORGANIZACIONAL_VINCULADA") == 0 )
         {
            dynavUnidadeorganizacional_vinculada2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, dynavUnidadeorganizacional_vinculada2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavUnidadeorganizacional_vinculada2.Visible), 5, 0)));
         }
      }

      protected void S132( )
      {
         /* 'ENABLEDYNAMICFILTERS3' Routine */
         edtavUnidadeorganizacional_nome3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavUnidadeorganizacional_nome3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUnidadeorganizacional_nome3_Visible), 5, 0)));
         dynavEstado_uf3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, dynavEstado_uf3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavEstado_uf3.Visible), 5, 0)));
         dynavUnidadeorganizacional_vinculada3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, dynavUnidadeorganizacional_vinculada3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavUnidadeorganizacional_vinculada3.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV27DynamicFiltersSelector3, "UNIDADEORGANIZACIONAL_NOME") == 0 )
         {
            edtavUnidadeorganizacional_nome3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavUnidadeorganizacional_nome3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUnidadeorganizacional_nome3_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV27DynamicFiltersSelector3, "ESTADO_UF") == 0 )
         {
            dynavEstado_uf3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, dynavEstado_uf3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavEstado_uf3.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV27DynamicFiltersSelector3, "UNIDADEORGANIZACIONAL_VINCULADA") == 0 )
         {
            dynavUnidadeorganizacional_vinculada3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, dynavUnidadeorganizacional_vinculada3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(dynavUnidadeorganizacional_vinculada3.Visible), 5, 0)));
         }
      }

      protected void S212( )
      {
         /* 'RESETDYNFILTERS' Routine */
         AV21DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21DynamicFiltersEnabled2", AV21DynamicFiltersEnabled2);
         AV22DynamicFiltersSelector2 = "UNIDADEORGANIZACIONAL_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22DynamicFiltersSelector2", AV22DynamicFiltersSelector2);
         AV23UnidadeOrganizacional_Nome2 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23UnidadeOrganizacional_Nome2", AV23UnidadeOrganizacional_Nome2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26DynamicFiltersEnabled3", AV26DynamicFiltersEnabled3);
         AV27DynamicFiltersSelector3 = "UNIDADEORGANIZACIONAL_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27DynamicFiltersSelector3", AV27DynamicFiltersSelector3);
         AV28UnidadeOrganizacional_Nome3 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV28UnidadeOrganizacional_Nome3", AV28UnidadeOrganizacional_Nome3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S232( )
      {
         /* 'CLEANFILTERS' Routine */
         AV16UnidadeOrganizacional_Ativo = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16UnidadeOrganizacional_Ativo", AV16UnidadeOrganizacional_Ativo);
         AV40TFUnidadeOrganizacional_Nome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV40TFUnidadeOrganizacional_Nome", AV40TFUnidadeOrganizacional_Nome);
         Ddo_unidadeorganizacional_nome_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_unidadeorganizacional_nome_Internalname, "FilteredText_set", Ddo_unidadeorganizacional_nome_Filteredtext_set);
         AV41TFUnidadeOrganizacional_Nome_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV41TFUnidadeOrganizacional_Nome_Sel", AV41TFUnidadeOrganizacional_Nome_Sel);
         Ddo_unidadeorganizacional_nome_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_unidadeorganizacional_nome_Internalname, "SelectedValue_set", Ddo_unidadeorganizacional_nome_Selectedvalue_set);
         AV44TFEstado_UF = "";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV44TFEstado_UF", AV44TFEstado_UF);
         Ddo_estado_uf_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_estado_uf_Internalname, "FilteredText_set", Ddo_estado_uf_Filteredtext_set);
         AV45TFEstado_UF_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV45TFEstado_UF_Sel", AV45TFEstado_UF_Sel);
         Ddo_estado_uf_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_estado_uf_Internalname, "SelectedValue_set", Ddo_estado_uf_Selectedvalue_set);
         AV48TFUnidadeOrganizacional_Vinculada = "";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV48TFUnidadeOrganizacional_Vinculada", AV48TFUnidadeOrganizacional_Vinculada);
         Ddo_unidadeorganizacional_vinculada_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_unidadeorganizacional_vinculada_Internalname, "FilteredText_set", Ddo_unidadeorganizacional_vinculada_Filteredtext_set);
         AV49TFUnidadeOrganizacional_Vinculada_Sel = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV49TFUnidadeOrganizacional_Vinculada_Sel", StringUtil.LTrim( StringUtil.Str( (decimal)(AV49TFUnidadeOrganizacional_Vinculada_Sel), 6, 0)));
         Ddo_unidadeorganizacional_vinculada_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_unidadeorganizacional_vinculada_Internalname, "SelectedValue_set", Ddo_unidadeorganizacional_vinculada_Selectedvalue_set);
         AV17DynamicFiltersSelector1 = "UNIDADEORGANIZACIONAL_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17DynamicFiltersSelector1", AV17DynamicFiltersSelector1);
         AV18UnidadeOrganizacional_Nome1 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18UnidadeOrganizacional_Nome1", AV18UnidadeOrganizacional_Nome1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV11GridState.gxTpr_Dynamicfilters.Clear();
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S222 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S152( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV36Session.Get(AV62Pgmname+"GridState"), "") == 0 )
         {
            AV11GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  AV62Pgmname+"GridState"), "");
         }
         else
         {
            AV11GridState.FromXml(AV36Session.Get(AV62Pgmname+"GridState"), "");
         }
         AV14OrderedBy = AV11GridState.gxTpr_Orderedby;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
         AV15OrderedDsc = AV11GridState.gxTpr_Ordereddsc;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15OrderedDsc", AV15OrderedDsc);
         /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADREGFILTERSSTATE' */
         S242 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S222 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S242( )
      {
         /* 'LOADREGFILTERSSTATE' Routine */
         AV63GXV1 = 1;
         while ( AV63GXV1 <= AV11GridState.gxTpr_Filtervalues.Count )
         {
            AV12GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV11GridState.gxTpr_Filtervalues.Item(AV63GXV1));
            if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "UNIDADEORGANIZACIONAL_ATIVO") == 0 )
            {
               AV16UnidadeOrganizacional_Ativo = BooleanUtil.Val( AV12GridStateFilterValue.gxTpr_Value);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16UnidadeOrganizacional_Ativo", AV16UnidadeOrganizacional_Ativo);
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFUNIDADEORGANIZACIONAL_NOME") == 0 )
            {
               AV40TFUnidadeOrganizacional_Nome = AV12GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV40TFUnidadeOrganizacional_Nome", AV40TFUnidadeOrganizacional_Nome);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV40TFUnidadeOrganizacional_Nome)) )
               {
                  Ddo_unidadeorganizacional_nome_Filteredtext_set = AV40TFUnidadeOrganizacional_Nome;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_unidadeorganizacional_nome_Internalname, "FilteredText_set", Ddo_unidadeorganizacional_nome_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFUNIDADEORGANIZACIONAL_NOME_SEL") == 0 )
            {
               AV41TFUnidadeOrganizacional_Nome_Sel = AV12GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV41TFUnidadeOrganizacional_Nome_Sel", AV41TFUnidadeOrganizacional_Nome_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV41TFUnidadeOrganizacional_Nome_Sel)) )
               {
                  Ddo_unidadeorganizacional_nome_Selectedvalue_set = AV41TFUnidadeOrganizacional_Nome_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_unidadeorganizacional_nome_Internalname, "SelectedValue_set", Ddo_unidadeorganizacional_nome_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFESTADO_UF") == 0 )
            {
               AV44TFEstado_UF = AV12GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV44TFEstado_UF", AV44TFEstado_UF);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV44TFEstado_UF)) )
               {
                  Ddo_estado_uf_Filteredtext_set = AV44TFEstado_UF;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_estado_uf_Internalname, "FilteredText_set", Ddo_estado_uf_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFESTADO_UF_SEL") == 0 )
            {
               AV45TFEstado_UF_Sel = AV12GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV45TFEstado_UF_Sel", AV45TFEstado_UF_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV45TFEstado_UF_Sel)) )
               {
                  Ddo_estado_uf_Selectedvalue_set = AV45TFEstado_UF_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_estado_uf_Internalname, "SelectedValue_set", Ddo_estado_uf_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFUNIDADEORGANIZACIONAL_VINCULADA") == 0 )
            {
               AV48TFUnidadeOrganizacional_Vinculada = AV12GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV48TFUnidadeOrganizacional_Vinculada", AV48TFUnidadeOrganizacional_Vinculada);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV48TFUnidadeOrganizacional_Vinculada)) )
               {
                  Ddo_unidadeorganizacional_vinculada_Filteredtext_set = AV48TFUnidadeOrganizacional_Vinculada;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_unidadeorganizacional_vinculada_Internalname, "FilteredText_set", Ddo_unidadeorganizacional_vinculada_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFUNIDADEORGANIZACIONAL_VINCULADA_SEL") == 0 )
            {
               AV49TFUnidadeOrganizacional_Vinculada_Sel = (int)(NumberUtil.Val( AV12GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV49TFUnidadeOrganizacional_Vinculada_Sel", StringUtil.LTrim( StringUtil.Str( (decimal)(AV49TFUnidadeOrganizacional_Vinculada_Sel), 6, 0)));
               if ( ! (0==AV49TFUnidadeOrganizacional_Vinculada_Sel) )
               {
                  Ddo_unidadeorganizacional_vinculada_Selectedvalue_set = StringUtil.Str( (decimal)(AV49TFUnidadeOrganizacional_Vinculada_Sel), 6, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_unidadeorganizacional_vinculada_Internalname, "SelectedValue_set", Ddo_unidadeorganizacional_vinculada_Selectedvalue_set);
                  AV52TFUnidadeOrganizacional_Vinculada_SelDsc = AV12GridStateFilterValue.gxTpr_Valueto;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV52TFUnidadeOrganizacional_Vinculada_SelDsc", AV52TFUnidadeOrganizacional_Vinculada_SelDsc);
                  Ddo_unidadeorganizacional_vinculada_Selectedtext_set = AV52TFUnidadeOrganizacional_Vinculada_SelDsc;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_unidadeorganizacional_vinculada_Internalname, "SelectedText_set", Ddo_unidadeorganizacional_vinculada_Selectedtext_set);
               }
            }
            AV63GXV1 = (int)(AV63GXV1+1);
         }
      }

      protected void S222( )
      {
         /* 'LOADDYNFILTERSSTATE' Routine */
         imgAdddynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         imgAdddynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
         if ( AV11GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV13GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV11GridState.gxTpr_Dynamicfilters.Item(1));
            AV17DynamicFiltersSelector1 = AV13GridStateDynamicFilter.gxTpr_Selected;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17DynamicFiltersSelector1", AV17DynamicFiltersSelector1);
            if ( StringUtil.StrCmp(AV17DynamicFiltersSelector1, "UNIDADEORGANIZACIONAL_NOME") == 0 )
            {
               AV18UnidadeOrganizacional_Nome1 = AV13GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18UnidadeOrganizacional_Nome1", AV18UnidadeOrganizacional_Nome1);
            }
            else if ( StringUtil.StrCmp(AV17DynamicFiltersSelector1, "ESTADO_UF") == 0 )
            {
               AV19Estado_UF1 = AV13GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19Estado_UF1", AV19Estado_UF1);
            }
            else if ( StringUtil.StrCmp(AV17DynamicFiltersSelector1, "UNIDADEORGANIZACIONAL_VINCULADA") == 0 )
            {
               AV20UnidadeOrganizacional_Vinculada1 = (int)(NumberUtil.Val( AV13GridStateDynamicFilter.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20UnidadeOrganizacional_Vinculada1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20UnidadeOrganizacional_Vinculada1), 6, 0)));
            }
            /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            if ( AV11GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               lblJsdynamicfilters_Caption = "<script type=\"text/javascript\">";
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(2);";
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               imgAdddynamicfilters1_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
               imgRemovedynamicfilters1_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
               AV21DynamicFiltersEnabled2 = true;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21DynamicFiltersEnabled2", AV21DynamicFiltersEnabled2);
               AV13GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV11GridState.gxTpr_Dynamicfilters.Item(2));
               AV22DynamicFiltersSelector2 = AV13GridStateDynamicFilter.gxTpr_Selected;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22DynamicFiltersSelector2", AV22DynamicFiltersSelector2);
               if ( StringUtil.StrCmp(AV22DynamicFiltersSelector2, "UNIDADEORGANIZACIONAL_NOME") == 0 )
               {
                  AV23UnidadeOrganizacional_Nome2 = AV13GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23UnidadeOrganizacional_Nome2", AV23UnidadeOrganizacional_Nome2);
               }
               else if ( StringUtil.StrCmp(AV22DynamicFiltersSelector2, "ESTADO_UF") == 0 )
               {
                  AV24Estado_UF2 = AV13GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24Estado_UF2", AV24Estado_UF2);
               }
               else if ( StringUtil.StrCmp(AV22DynamicFiltersSelector2, "UNIDADEORGANIZACIONAL_VINCULADA") == 0 )
               {
                  AV25UnidadeOrganizacional_Vinculada2 = (int)(NumberUtil.Val( AV13GridStateDynamicFilter.gxTpr_Value, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25UnidadeOrganizacional_Vinculada2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV25UnidadeOrganizacional_Vinculada2), 6, 0)));
               }
               /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
               S122 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               if ( AV11GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(3);";
                  context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
                  imgAdddynamicfilters2_Visible = 0;
                  context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
                  imgRemovedynamicfilters2_Visible = 1;
                  context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
                  AV26DynamicFiltersEnabled3 = true;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26DynamicFiltersEnabled3", AV26DynamicFiltersEnabled3);
                  AV13GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV11GridState.gxTpr_Dynamicfilters.Item(3));
                  AV27DynamicFiltersSelector3 = AV13GridStateDynamicFilter.gxTpr_Selected;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27DynamicFiltersSelector3", AV27DynamicFiltersSelector3);
                  if ( StringUtil.StrCmp(AV27DynamicFiltersSelector3, "UNIDADEORGANIZACIONAL_NOME") == 0 )
                  {
                     AV28UnidadeOrganizacional_Nome3 = AV13GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV28UnidadeOrganizacional_Nome3", AV28UnidadeOrganizacional_Nome3);
                  }
                  else if ( StringUtil.StrCmp(AV27DynamicFiltersSelector3, "ESTADO_UF") == 0 )
                  {
                     AV29Estado_UF3 = AV13GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29Estado_UF3", AV29Estado_UF3);
                  }
                  else if ( StringUtil.StrCmp(AV27DynamicFiltersSelector3, "UNIDADEORGANIZACIONAL_VINCULADA") == 0 )
                  {
                     AV30UnidadeOrganizacional_Vinculada3 = (int)(NumberUtil.Val( AV13GridStateDynamicFilter.gxTpr_Value, "."));
                     context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30UnidadeOrganizacional_Vinculada3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV30UnidadeOrganizacional_Vinculada3), 6, 0)));
                  }
                  /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
                  S132 ();
                  if ( returnInSub )
                  {
                     returnInSub = true;
                     if (true) return;
                  }
               }
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"</script>";
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
            }
         }
         if ( AV31DynamicFiltersRemoving )
         {
            lblJsdynamicfilters_Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         }
      }

      protected void S182( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV11GridState.FromXml(AV36Session.Get(AV62Pgmname+"GridState"), "");
         AV11GridState.gxTpr_Orderedby = AV14OrderedBy;
         AV11GridState.gxTpr_Ordereddsc = AV15OrderedDsc;
         AV11GridState.gxTpr_Filtervalues.Clear();
         if ( ! (false==AV16UnidadeOrganizacional_Ativo) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "UNIDADEORGANIZACIONAL_ATIVO";
            AV12GridStateFilterValue.gxTpr_Value = StringUtil.BoolToStr( AV16UnidadeOrganizacional_Ativo);
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV40TFUnidadeOrganizacional_Nome)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFUNIDADEORGANIZACIONAL_NOME";
            AV12GridStateFilterValue.gxTpr_Value = AV40TFUnidadeOrganizacional_Nome;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV41TFUnidadeOrganizacional_Nome_Sel)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFUNIDADEORGANIZACIONAL_NOME_SEL";
            AV12GridStateFilterValue.gxTpr_Value = AV41TFUnidadeOrganizacional_Nome_Sel;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV44TFEstado_UF)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFESTADO_UF";
            AV12GridStateFilterValue.gxTpr_Value = AV44TFEstado_UF;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV45TFEstado_UF_Sel)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFESTADO_UF_SEL";
            AV12GridStateFilterValue.gxTpr_Value = AV45TFEstado_UF_Sel;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV48TFUnidadeOrganizacional_Vinculada)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFUNIDADEORGANIZACIONAL_VINCULADA";
            AV12GridStateFilterValue.gxTpr_Value = AV48TFUnidadeOrganizacional_Vinculada;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! (0==AV49TFUnidadeOrganizacional_Vinculada_Sel) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFUNIDADEORGANIZACIONAL_VINCULADA_SEL";
            AV12GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV49TFUnidadeOrganizacional_Vinculada_Sel), 6, 0);
            AV12GridStateFilterValue.gxTpr_Valueto = AV52TFUnidadeOrganizacional_Vinculada_SelDsc;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! (0==AV7TpUo_Codigo) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "PARM_&TPUO_CODIGO";
            AV12GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV7TpUo_Codigo), 6, 0);
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         new wwpbaseobjects.savegridstate(context ).execute(  AV62Pgmname+"GridState",  AV11GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_MeetrikaVs3")) ;
      }

      protected void S202( )
      {
         /* 'SAVEDYNFILTERSSTATE' Routine */
         AV11GridState.gxTpr_Dynamicfilters.Clear();
         if ( ! AV32DynamicFiltersIgnoreFirst )
         {
            AV13GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV13GridStateDynamicFilter.gxTpr_Selected = AV17DynamicFiltersSelector1;
            if ( ( StringUtil.StrCmp(AV17DynamicFiltersSelector1, "UNIDADEORGANIZACIONAL_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV18UnidadeOrganizacional_Nome1)) )
            {
               AV13GridStateDynamicFilter.gxTpr_Value = AV18UnidadeOrganizacional_Nome1;
            }
            else if ( ( StringUtil.StrCmp(AV17DynamicFiltersSelector1, "ESTADO_UF") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV19Estado_UF1)) )
            {
               AV13GridStateDynamicFilter.gxTpr_Value = AV19Estado_UF1;
            }
            else if ( ( StringUtil.StrCmp(AV17DynamicFiltersSelector1, "UNIDADEORGANIZACIONAL_VINCULADA") == 0 ) && ! (0==AV20UnidadeOrganizacional_Vinculada1) )
            {
               AV13GridStateDynamicFilter.gxTpr_Value = StringUtil.Str( (decimal)(AV20UnidadeOrganizacional_Vinculada1), 6, 0);
            }
            if ( AV31DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV13GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV11GridState.gxTpr_Dynamicfilters.Add(AV13GridStateDynamicFilter, 0);
            }
         }
         if ( AV21DynamicFiltersEnabled2 )
         {
            AV13GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV13GridStateDynamicFilter.gxTpr_Selected = AV22DynamicFiltersSelector2;
            if ( ( StringUtil.StrCmp(AV22DynamicFiltersSelector2, "UNIDADEORGANIZACIONAL_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV23UnidadeOrganizacional_Nome2)) )
            {
               AV13GridStateDynamicFilter.gxTpr_Value = AV23UnidadeOrganizacional_Nome2;
            }
            else if ( ( StringUtil.StrCmp(AV22DynamicFiltersSelector2, "ESTADO_UF") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV24Estado_UF2)) )
            {
               AV13GridStateDynamicFilter.gxTpr_Value = AV24Estado_UF2;
            }
            else if ( ( StringUtil.StrCmp(AV22DynamicFiltersSelector2, "UNIDADEORGANIZACIONAL_VINCULADA") == 0 ) && ! (0==AV25UnidadeOrganizacional_Vinculada2) )
            {
               AV13GridStateDynamicFilter.gxTpr_Value = StringUtil.Str( (decimal)(AV25UnidadeOrganizacional_Vinculada2), 6, 0);
            }
            if ( AV31DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV13GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV11GridState.gxTpr_Dynamicfilters.Add(AV13GridStateDynamicFilter, 0);
            }
         }
         if ( AV26DynamicFiltersEnabled3 )
         {
            AV13GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV13GridStateDynamicFilter.gxTpr_Selected = AV27DynamicFiltersSelector3;
            if ( ( StringUtil.StrCmp(AV27DynamicFiltersSelector3, "UNIDADEORGANIZACIONAL_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV28UnidadeOrganizacional_Nome3)) )
            {
               AV13GridStateDynamicFilter.gxTpr_Value = AV28UnidadeOrganizacional_Nome3;
            }
            else if ( ( StringUtil.StrCmp(AV27DynamicFiltersSelector3, "ESTADO_UF") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV29Estado_UF3)) )
            {
               AV13GridStateDynamicFilter.gxTpr_Value = AV29Estado_UF3;
            }
            else if ( ( StringUtil.StrCmp(AV27DynamicFiltersSelector3, "UNIDADEORGANIZACIONAL_VINCULADA") == 0 ) && ! (0==AV30UnidadeOrganizacional_Vinculada3) )
            {
               AV13GridStateDynamicFilter.gxTpr_Value = StringUtil.Str( (decimal)(AV30UnidadeOrganizacional_Vinculada3), 6, 0);
            }
            if ( AV31DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV13GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV11GridState.gxTpr_Dynamicfilters.Add(AV13GridStateDynamicFilter, 0);
            }
         }
      }

      protected void S142( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV9TrnContext.gxTpr_Callerobject = AV62Pgmname;
         AV9TrnContext.gxTpr_Callerondelete = true;
         AV9TrnContext.gxTpr_Callerurl = AV8HTTPRequest.ScriptName+"?"+AV8HTTPRequest.QueryString;
         AV9TrnContext.gxTpr_Transactionname = "Geral_UnidadeOrganizacional";
         AV10TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV10TrnContextAtt.gxTpr_Attributename = "TpUo_Codigo";
         AV10TrnContextAtt.gxTpr_Attributevalue = StringUtil.Str( (decimal)(AV7TpUo_Codigo), 6, 0);
         AV9TrnContext.gxTpr_Attributes.Add(AV10TrnContextAtt, 0);
         AV36Session.Set("TrnContext", AV9TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_MeetrikaVs3"));
      }

      protected void wb_table1_2_D82( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, sPrefix, "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='TableSearchCell'>") ;
            wb_table2_8_D82( true) ;
         }
         else
         {
            wb_table2_8_D82( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_D82e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            wb_table3_80_D82( true) ;
         }
         else
         {
            wb_table3_80_D82( false) ;
         }
         return  ;
      }

      protected void wb_table3_80_D82e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_D82e( true) ;
         }
         else
         {
            wb_table1_2_D82e( false) ;
         }
      }

      protected void wb_table3_80_D82( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"DivS\" data-gxgridid=\"83\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+((edtavUpdate_Visible==0) ? "display:none;" : "")+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+((edtavDelete_Visible==0) ? "display:none;" : "")+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+((edtavDisplay_Visible==0) ? "display:none;" : "")+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Organizacional_Codigo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtUnidadeOrganizacional_Nome_Titleformat == 0 )
               {
                  context.SendWebValue( edtUnidadeOrganizacional_Nome_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtUnidadeOrganizacional_Nome_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtEstado_UF_Titleformat == 0 )
               {
                  context.SendWebValue( edtEstado_UF_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtEstado_UF_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( dynUnidadeOrganizacional_Vinculada_Titleformat == 0 )
               {
                  context.SendWebValue( dynUnidadeOrganizacional_Vinculada.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( dynUnidadeOrganizacional_Vinculada.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", sPrefix);
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV33Update));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavUpdate_Enabled), 5, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavUpdate_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavUpdate_Tooltiptext));
               GridColumn.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavUpdate_Visible), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV34Delete));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavDelete_Enabled), 5, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavDelete_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDelete_Tooltiptext));
               GridColumn.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavDelete_Visible), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV35Display));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavDisplay_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDisplay_Tooltiptext));
               GridColumn.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavDisplay_Visible), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A611UnidadeOrganizacional_Codigo), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A612UnidadeOrganizacional_Nome));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtUnidadeOrganizacional_Nome_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtUnidadeOrganizacional_Nome_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtUnidadeOrganizacional_Nome_Link));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A23Estado_UF));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtEstado_UF_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtEstado_UF_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A613UnidadeOrganizacional_Vinculada), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( dynUnidadeOrganizacional_Vinculada.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(dynUnidadeOrganizacional_Vinculada_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 83 )
         {
            wbEnd = 0;
            nRC_GXsfl_83 = (short)(nGXsfl_83_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid(sPrefix+"_"+"Grid", GridContainer);
               if ( ! isAjaxCallMode( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+sPrefix+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_80_D82e( true) ;
         }
         else
         {
            wb_table3_80_D82e( false) ;
         }
      }

      protected void wb_table2_8_D82( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablesearch_Internalname, tblTablesearch_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='Width100'>") ;
            wb_table4_11_D82( true) ;
         }
         else
         {
            wb_table4_11_D82( false) ;
         }
         return  ;
      }

      protected void wb_table4_11_D82e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table5_16_D82( true) ;
         }
         else
         {
            wb_table5_16_D82( false) ;
         }
         return  ;
      }

      protected void wb_table5_16_D82e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='OrderedByDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblOrderedtext_Internalname, "Ordenado por", "", "", lblOrderedtext_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "OrderedByDescription", 0, "", 1, 1, 0, "HLP_Geral_Tp_UOGeral_UnidadeOrganizacionalWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'" + sPrefix + "',false,'" + sGXsfl_83_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavOrderedby, cmbavOrderedby_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)), 1, cmbavOrderedby_Jsonclick, 5, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"EVORDEREDBY.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,23);\"", "", true, "HLP_Geral_Tp_UOGeral_UnidadeOrganizacionalWC.htm");
            cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavOrderedby_Internalname, "Values", (String)(cmbavOrderedby.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 24,'" + sPrefix + "',false,'" + sGXsfl_83_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV15OrderedDsc), StringUtil.BoolToStr( AV15OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,24);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_Geral_Tp_UOGeral_UnidadeOrganizacionalWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableFiltersCell'>") ;
            wb_table6_26_D82( true) ;
         }
         else
         {
            wb_table6_26_D82( false) ;
         }
         return  ;
      }

      protected void wb_table6_26_D82e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_D82e( true) ;
         }
         else
         {
            wb_table2_8_D82e( false) ;
         }
      }

      protected void wb_table6_26_D82( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 29,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgCleanfilters_Internalname, context.GetImagePath( "effedd9a-25cf-4497-952f-5a5ac032b8be", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Limpar filtros", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgCleanfilters_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOCLEANFILTERS\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Geral_Tp_UOGeral_UnidadeOrganizacionalWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Invisible'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblFiltertextunidadeorganizacional_ativo_Internalname, "Ativa?", "", "", lblFiltertextunidadeorganizacional_ativo_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_Geral_Tp_UOGeral_UnidadeOrganizacionalWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Invisible'>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 33,'" + sPrefix + "',false,'" + sGXsfl_83_idx + "',0)\"";
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavUnidadeorganizacional_ativo_Internalname, StringUtil.BoolToStr( AV16UnidadeOrganizacional_Ativo), "", "", 1, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(33, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,33);\"");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataFilterContentCell'>") ;
            wb_table7_35_D82( true) ;
         }
         else
         {
            wb_table7_35_D82( false) ;
         }
         return  ;
      }

      protected void wb_table7_35_D82e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblJsdynamicfilters_Internalname, lblJsdynamicfilters_Caption, "", "", lblJsdynamicfilters_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", 0, "", 1, 1, 1, "HLP_Geral_Tp_UOGeral_UnidadeOrganizacionalWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_26_D82e( true) ;
         }
         else
         {
            wb_table6_26_D82e( false) ;
         }
      }

      protected void wb_table7_35_D82( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabledynamicfilters_Internalname, tblTabledynamicfilters_Internalname, "", "TableDynamicFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix1_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix1_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_Geral_Tp_UOGeral_UnidadeOrganizacionalWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 40,'" + sPrefix + "',false,'" + sGXsfl_83_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector1, cmbavDynamicfiltersselector1_Internalname, StringUtil.RTrim( AV17DynamicFiltersSelector1), 1, cmbavDynamicfiltersselector1_Jsonclick, 5, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"EVDYNAMICFILTERSSELECTOR1.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,40);\"", "", true, "HLP_Geral_Tp_UOGeral_UnidadeOrganizacionalWC.htm");
            cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV17DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector1_Internalname, "Values", (String)(cmbavDynamicfiltersselector1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle1_Internalname, "valor", "", "", lblDynamicfiltersmiddle1_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_Geral_Tp_UOGeral_UnidadeOrganizacionalWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 44,'" + sPrefix + "',false,'" + sGXsfl_83_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavUnidadeorganizacional_nome1_Internalname, StringUtil.RTrim( AV18UnidadeOrganizacional_Nome1), StringUtil.RTrim( context.localUtil.Format( AV18UnidadeOrganizacional_Nome1, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,44);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavUnidadeorganizacional_nome1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavUnidadeorganizacional_nome1_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_Geral_Tp_UOGeral_UnidadeOrganizacionalWC.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 45,'" + sPrefix + "',false,'" + sGXsfl_83_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavEstado_uf1, dynavEstado_uf1_Internalname, StringUtil.RTrim( AV19Estado_UF1), 1, dynavEstado_uf1_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "char", "", dynavEstado_uf1.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,45);\"", "", true, "HLP_Geral_Tp_UOGeral_UnidadeOrganizacionalWC.htm");
            dynavEstado_uf1.CurrentValue = StringUtil.RTrim( AV19Estado_UF1);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, dynavEstado_uf1_Internalname, "Values", (String)(dynavEstado_uf1.ToJavascriptSource()));
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 46,'" + sPrefix + "',false,'" + sGXsfl_83_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavUnidadeorganizacional_vinculada1, dynavUnidadeorganizacional_vinculada1_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV20UnidadeOrganizacional_Vinculada1), 6, 0)), 1, dynavUnidadeorganizacional_vinculada1_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "int", "", dynavUnidadeorganizacional_vinculada1.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,46);\"", "", true, "HLP_Geral_Tp_UOGeral_UnidadeOrganizacionalWC.htm");
            dynavUnidadeorganizacional_vinculada1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV20UnidadeOrganizacional_Vinculada1), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, dynavUnidadeorganizacional_vinculada1_Internalname, "Values", (String)(dynavUnidadeorganizacional_vinculada1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 48,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters1_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters1_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters1_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'ADDDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Geral_Tp_UOGeral_UnidadeOrganizacionalWC.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 49,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters1_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters1_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters1_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'REMOVEDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Geral_Tp_UOGeral_UnidadeOrganizacionalWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow2\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix2_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix2_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_Geral_Tp_UOGeral_UnidadeOrganizacionalWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 54,'" + sPrefix + "',false,'" + sGXsfl_83_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector2, cmbavDynamicfiltersselector2_Internalname, StringUtil.RTrim( AV22DynamicFiltersSelector2), 1, cmbavDynamicfiltersselector2_Jsonclick, 5, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"EVDYNAMICFILTERSSELECTOR2.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,54);\"", "", true, "HLP_Geral_Tp_UOGeral_UnidadeOrganizacionalWC.htm");
            cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV22DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector2_Internalname, "Values", (String)(cmbavDynamicfiltersselector2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle2_Internalname, "valor", "", "", lblDynamicfiltersmiddle2_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_Geral_Tp_UOGeral_UnidadeOrganizacionalWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 58,'" + sPrefix + "',false,'" + sGXsfl_83_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavUnidadeorganizacional_nome2_Internalname, StringUtil.RTrim( AV23UnidadeOrganizacional_Nome2), StringUtil.RTrim( context.localUtil.Format( AV23UnidadeOrganizacional_Nome2, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,58);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavUnidadeorganizacional_nome2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavUnidadeorganizacional_nome2_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_Geral_Tp_UOGeral_UnidadeOrganizacionalWC.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 59,'" + sPrefix + "',false,'" + sGXsfl_83_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavEstado_uf2, dynavEstado_uf2_Internalname, StringUtil.RTrim( AV24Estado_UF2), 1, dynavEstado_uf2_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "char", "", dynavEstado_uf2.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,59);\"", "", true, "HLP_Geral_Tp_UOGeral_UnidadeOrganizacionalWC.htm");
            dynavEstado_uf2.CurrentValue = StringUtil.RTrim( AV24Estado_UF2);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, dynavEstado_uf2_Internalname, "Values", (String)(dynavEstado_uf2.ToJavascriptSource()));
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 60,'" + sPrefix + "',false,'" + sGXsfl_83_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavUnidadeorganizacional_vinculada2, dynavUnidadeorganizacional_vinculada2_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV25UnidadeOrganizacional_Vinculada2), 6, 0)), 1, dynavUnidadeorganizacional_vinculada2_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "int", "", dynavUnidadeorganizacional_vinculada2.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,60);\"", "", true, "HLP_Geral_Tp_UOGeral_UnidadeOrganizacionalWC.htm");
            dynavUnidadeorganizacional_vinculada2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV25UnidadeOrganizacional_Vinculada2), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, dynavUnidadeorganizacional_vinculada2_Internalname, "Values", (String)(dynavUnidadeorganizacional_vinculada2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 62,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters2_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters2_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters2_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'ADDDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Geral_Tp_UOGeral_UnidadeOrganizacionalWC.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 63,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters2_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters2_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters2_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'REMOVEDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Geral_Tp_UOGeral_UnidadeOrganizacionalWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow3\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix3_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix3_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_Geral_Tp_UOGeral_UnidadeOrganizacionalWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 68,'" + sPrefix + "',false,'" + sGXsfl_83_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector3, cmbavDynamicfiltersselector3_Internalname, StringUtil.RTrim( AV27DynamicFiltersSelector3), 1, cmbavDynamicfiltersselector3_Jsonclick, 5, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"EVDYNAMICFILTERSSELECTOR3.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,68);\"", "", true, "HLP_Geral_Tp_UOGeral_UnidadeOrganizacionalWC.htm");
            cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV27DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector3_Internalname, "Values", (String)(cmbavDynamicfiltersselector3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle3_Internalname, "valor", "", "", lblDynamicfiltersmiddle3_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_Geral_Tp_UOGeral_UnidadeOrganizacionalWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 72,'" + sPrefix + "',false,'" + sGXsfl_83_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavUnidadeorganizacional_nome3_Internalname, StringUtil.RTrim( AV28UnidadeOrganizacional_Nome3), StringUtil.RTrim( context.localUtil.Format( AV28UnidadeOrganizacional_Nome3, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,72);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavUnidadeorganizacional_nome3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavUnidadeorganizacional_nome3_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_Geral_Tp_UOGeral_UnidadeOrganizacionalWC.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 73,'" + sPrefix + "',false,'" + sGXsfl_83_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavEstado_uf3, dynavEstado_uf3_Internalname, StringUtil.RTrim( AV29Estado_UF3), 1, dynavEstado_uf3_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "char", "", dynavEstado_uf3.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,73);\"", "", true, "HLP_Geral_Tp_UOGeral_UnidadeOrganizacionalWC.htm");
            dynavEstado_uf3.CurrentValue = StringUtil.RTrim( AV29Estado_UF3);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, dynavEstado_uf3_Internalname, "Values", (String)(dynavEstado_uf3.ToJavascriptSource()));
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 74,'" + sPrefix + "',false,'" + sGXsfl_83_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavUnidadeorganizacional_vinculada3, dynavUnidadeorganizacional_vinculada3_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV30UnidadeOrganizacional_Vinculada3), 6, 0)), 1, dynavUnidadeorganizacional_vinculada3_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "int", "", dynavUnidadeorganizacional_vinculada3.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,74);\"", "", true, "HLP_Geral_Tp_UOGeral_UnidadeOrganizacionalWC.htm");
            dynavUnidadeorganizacional_vinculada3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV30UnidadeOrganizacional_Vinculada3), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, dynavUnidadeorganizacional_vinculada3_Internalname, "Values", (String)(dynavUnidadeorganizacional_vinculada3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 76,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters3_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters3_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'REMOVEDYNAMICFILTERS3\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Geral_Tp_UOGeral_UnidadeOrganizacionalWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_35_D82e( true) ;
         }
         else
         {
            wb_table7_35_D82e( false) ;
         }
      }

      protected void wb_table5_16_D82( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable1_Internalname, tblUnnamedtable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 19,'" + sPrefix + "',false,'',0)\"";
            ClassString = "btn-default";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnselectuo_Internalname, "gx.evt.setGridEvt("+StringUtil.Str( (decimal)(83), 2, 0)+","+"null"+");", "Visualizar", bttBtnselectuo_Jsonclick, 7, "Visualizar estrutura organizacional", "", StyleString, ClassString, 1, 1, "standard", "'"+sPrefix+"'"+",false,"+"'"+"e29d81_client"+"'", TempTags, "", 2, "HLP_Geral_Tp_UOGeral_UnidadeOrganizacionalWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_16_D82e( true) ;
         }
         else
         {
            wb_table5_16_D82e( false) ;
         }
      }

      protected void wb_table4_11_D82( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "Table", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 14,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgInsert_Internalname, imgInsert_Bitmap, "", "", "", context.GetTheme( ), imgInsert_Visible, imgInsert_Enabled, "", "Inserir", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgInsert_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOINSERT\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_Geral_Tp_UOGeral_UnidadeOrganizacionalWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_11_D82e( true) ;
         }
         else
         {
            wb_table4_11_D82e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV7TpUo_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7TpUo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7TpUo_Codigo), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAD82( ) ;
         WSD82( ) ;
         WED82( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlAV7TpUo_Codigo = (String)((String)getParm(obj,0));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PAD82( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "geral_tp_uogeral_unidadeorganizacionalwc");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PAD82( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            AV7TpUo_Codigo = Convert.ToInt32(getParm(obj,2));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7TpUo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7TpUo_Codigo), 6, 0)));
         }
         wcpOAV7TpUo_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV7TpUo_Codigo"), ",", "."));
         if ( ! GetJustCreated( ) && ( ( AV7TpUo_Codigo != wcpOAV7TpUo_Codigo ) ) )
         {
            setjustcreated();
         }
         wcpOAV7TpUo_Codigo = AV7TpUo_Codigo;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlAV7TpUo_Codigo = cgiGet( sPrefix+"AV7TpUo_Codigo_CTRL");
         if ( StringUtil.Len( sCtrlAV7TpUo_Codigo) > 0 )
         {
            AV7TpUo_Codigo = (int)(context.localUtil.CToN( cgiGet( sCtrlAV7TpUo_Codigo), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7TpUo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7TpUo_Codigo), 6, 0)));
         }
         else
         {
            AV7TpUo_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"AV7TpUo_Codigo_PARM"), ",", "."));
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PAD82( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WSD82( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WSD82( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"AV7TpUo_Codigo_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7TpUo_Codigo), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlAV7TpUo_Codigo)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"AV7TpUo_Codigo_CTRL", StringUtil.RTrim( sCtrlAV7TpUo_Codigo));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WED82( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?202051812505941");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("geral_tp_uogeral_unidadeorganizacionalwc.js", "?202051812505942");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_832( )
      {
         edtavUpdate_Internalname = sPrefix+"vUPDATE_"+sGXsfl_83_idx;
         edtavDelete_Internalname = sPrefix+"vDELETE_"+sGXsfl_83_idx;
         edtavDisplay_Internalname = sPrefix+"vDISPLAY_"+sGXsfl_83_idx;
         edtUnidadeOrganizacional_Codigo_Internalname = sPrefix+"UNIDADEORGANIZACIONAL_CODIGO_"+sGXsfl_83_idx;
         edtUnidadeOrganizacional_Nome_Internalname = sPrefix+"UNIDADEORGANIZACIONAL_NOME_"+sGXsfl_83_idx;
         edtEstado_UF_Internalname = sPrefix+"ESTADO_UF_"+sGXsfl_83_idx;
         dynUnidadeOrganizacional_Vinculada_Internalname = sPrefix+"UNIDADEORGANIZACIONAL_VINCULADA_"+sGXsfl_83_idx;
      }

      protected void SubsflControlProps_fel_832( )
      {
         edtavUpdate_Internalname = sPrefix+"vUPDATE_"+sGXsfl_83_fel_idx;
         edtavDelete_Internalname = sPrefix+"vDELETE_"+sGXsfl_83_fel_idx;
         edtavDisplay_Internalname = sPrefix+"vDISPLAY_"+sGXsfl_83_fel_idx;
         edtUnidadeOrganizacional_Codigo_Internalname = sPrefix+"UNIDADEORGANIZACIONAL_CODIGO_"+sGXsfl_83_fel_idx;
         edtUnidadeOrganizacional_Nome_Internalname = sPrefix+"UNIDADEORGANIZACIONAL_NOME_"+sGXsfl_83_fel_idx;
         edtEstado_UF_Internalname = sPrefix+"ESTADO_UF_"+sGXsfl_83_fel_idx;
         dynUnidadeOrganizacional_Vinculada_Internalname = sPrefix+"UNIDADEORGANIZACIONAL_VINCULADA_"+sGXsfl_83_fel_idx;
      }

      protected void sendrow_832( )
      {
         SubsflControlProps_832( ) ;
         WBD80( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_83_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_83_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_83_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+((edtavUpdate_Visible==0) ? "display:none;" : "")+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV33Update_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV33Update))&&String.IsNullOrEmpty(StringUtil.RTrim( AV59Update_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV33Update)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavUpdate_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV33Update)) ? AV59Update_GXI : context.PathToRelativeUrl( AV33Update)),(String)edtavUpdate_Link,(String)"",(String)"",context.GetTheme( ),(int)edtavUpdate_Visible,(int)edtavUpdate_Enabled,(String)"",(String)edtavUpdate_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV33Update_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+((edtavDelete_Visible==0) ? "display:none;" : "")+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV34Delete_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV34Delete))&&String.IsNullOrEmpty(StringUtil.RTrim( AV60Delete_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV34Delete)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDelete_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV34Delete)) ? AV60Delete_GXI : context.PathToRelativeUrl( AV34Delete)),(String)edtavDelete_Link,(String)"",(String)"",context.GetTheme( ),(int)edtavDelete_Visible,(int)edtavDelete_Enabled,(String)"",(String)edtavDelete_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV34Delete_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+((edtavDisplay_Visible==0) ? "display:none;" : "")+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV35Display_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV35Display))&&String.IsNullOrEmpty(StringUtil.RTrim( AV61Display_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV35Display)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDisplay_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV35Display)) ? AV61Display_GXI : context.PathToRelativeUrl( AV35Display)),(String)edtavDisplay_Link,(String)"",(String)"",context.GetTheme( ),(int)edtavDisplay_Visible,(short)1,(String)"",(String)edtavDisplay_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV35Display_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtUnidadeOrganizacional_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A611UnidadeOrganizacional_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A611UnidadeOrganizacional_Codigo), "ZZZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtUnidadeOrganizacional_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)83,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtUnidadeOrganizacional_Nome_Internalname,StringUtil.RTrim( A612UnidadeOrganizacional_Nome),StringUtil.RTrim( context.localUtil.Format( A612UnidadeOrganizacional_Nome, "@!")),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)edtUnidadeOrganizacional_Nome_Link,(String)"",(String)"",(String)"",(String)edtUnidadeOrganizacional_Nome_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)83,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtEstado_UF_Internalname,StringUtil.RTrim( A23Estado_UF),StringUtil.RTrim( context.localUtil.Format( A23Estado_UF, "@!")),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtEstado_UF_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)2,(short)0,(short)0,(short)83,(short)1,(short)-1,(short)-1,(bool)true,(String)"UF",(String)"left",(bool)true});
            GXAUNIDADEORGANIZACIONAL_VINCULADA_htmlD82( AV64Unidadeorganizacional_codigo) ;
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            if ( ( nGXsfl_83_idx == 1 ) && isAjaxCallMode( ) )
            {
               GXCCtl = "UNIDADEORGANIZACIONAL_VINCULADA_" + sGXsfl_83_idx;
               dynUnidadeOrganizacional_Vinculada.Name = GXCCtl;
               dynUnidadeOrganizacional_Vinculada.WebTags = "";
            }
            /* ComboBox */
            GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)dynUnidadeOrganizacional_Vinculada,(String)dynUnidadeOrganizacional_Vinculada_Internalname,StringUtil.Trim( StringUtil.Str( (decimal)(A613UnidadeOrganizacional_Vinculada), 6, 0)),(short)1,(String)dynUnidadeOrganizacional_Vinculada_Jsonclick,(short)0,(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"int",(String)"",(short)-1,(short)0,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"BootstrapAttribute",(String)"",(String)"",(String)"",(bool)true});
            dynUnidadeOrganizacional_Vinculada.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A613UnidadeOrganizacional_Vinculada), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, dynUnidadeOrganizacional_Vinculada_Internalname, "Values", (String)(dynUnidadeOrganizacional_Vinculada.ToJavascriptSource()));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_UNIDADEORGANIZACIONAL_CODIGO"+"_"+sGXsfl_83_idx, GetSecureSignedToken( sPrefix+sGXsfl_83_idx, context.localUtil.Format( (decimal)(A611UnidadeOrganizacional_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_UNIDADEORGANIZACIONAL_NOME"+"_"+sGXsfl_83_idx, GetSecureSignedToken( sPrefix+sGXsfl_83_idx, StringUtil.RTrim( context.localUtil.Format( A612UnidadeOrganizacional_Nome, "@!"))));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_ESTADO_UF"+"_"+sGXsfl_83_idx, GetSecureSignedToken( sPrefix+sGXsfl_83_idx, StringUtil.RTrim( context.localUtil.Format( A23Estado_UF, "@!"))));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_UNIDADEORGANIZACIONAL_VINCULADA"+"_"+sGXsfl_83_idx, GetSecureSignedToken( sPrefix+sGXsfl_83_idx, context.localUtil.Format( (decimal)(A613UnidadeOrganizacional_Vinculada), "ZZZZZ9")));
            GridContainer.AddRow(GridRow);
            nGXsfl_83_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_83_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_83_idx+1));
            sGXsfl_83_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_83_idx), 4, 0)), 4, "0");
            SubsflControlProps_832( ) ;
         }
         /* End function sendrow_832 */
      }

      protected void init_default_properties( )
      {
         imgInsert_Internalname = sPrefix+"INSERT";
         tblTableactions_Internalname = sPrefix+"TABLEACTIONS";
         bttBtnselectuo_Internalname = sPrefix+"BTNSELECTUO";
         tblUnnamedtable1_Internalname = sPrefix+"UNNAMEDTABLE1";
         lblOrderedtext_Internalname = sPrefix+"ORDEREDTEXT";
         cmbavOrderedby_Internalname = sPrefix+"vORDEREDBY";
         edtavOrdereddsc_Internalname = sPrefix+"vORDEREDDSC";
         imgCleanfilters_Internalname = sPrefix+"CLEANFILTERS";
         lblFiltertextunidadeorganizacional_ativo_Internalname = sPrefix+"FILTERTEXTUNIDADEORGANIZACIONAL_ATIVO";
         chkavUnidadeorganizacional_ativo_Internalname = sPrefix+"vUNIDADEORGANIZACIONAL_ATIVO";
         lblDynamicfiltersprefix1_Internalname = sPrefix+"DYNAMICFILTERSPREFIX1";
         cmbavDynamicfiltersselector1_Internalname = sPrefix+"vDYNAMICFILTERSSELECTOR1";
         lblDynamicfiltersmiddle1_Internalname = sPrefix+"DYNAMICFILTERSMIDDLE1";
         edtavUnidadeorganizacional_nome1_Internalname = sPrefix+"vUNIDADEORGANIZACIONAL_NOME1";
         dynavEstado_uf1_Internalname = sPrefix+"vESTADO_UF1";
         dynavUnidadeorganizacional_vinculada1_Internalname = sPrefix+"vUNIDADEORGANIZACIONAL_VINCULADA1";
         imgAdddynamicfilters1_Internalname = sPrefix+"ADDDYNAMICFILTERS1";
         imgRemovedynamicfilters1_Internalname = sPrefix+"REMOVEDYNAMICFILTERS1";
         lblDynamicfiltersprefix2_Internalname = sPrefix+"DYNAMICFILTERSPREFIX2";
         cmbavDynamicfiltersselector2_Internalname = sPrefix+"vDYNAMICFILTERSSELECTOR2";
         lblDynamicfiltersmiddle2_Internalname = sPrefix+"DYNAMICFILTERSMIDDLE2";
         edtavUnidadeorganizacional_nome2_Internalname = sPrefix+"vUNIDADEORGANIZACIONAL_NOME2";
         dynavEstado_uf2_Internalname = sPrefix+"vESTADO_UF2";
         dynavUnidadeorganizacional_vinculada2_Internalname = sPrefix+"vUNIDADEORGANIZACIONAL_VINCULADA2";
         imgAdddynamicfilters2_Internalname = sPrefix+"ADDDYNAMICFILTERS2";
         imgRemovedynamicfilters2_Internalname = sPrefix+"REMOVEDYNAMICFILTERS2";
         lblDynamicfiltersprefix3_Internalname = sPrefix+"DYNAMICFILTERSPREFIX3";
         cmbavDynamicfiltersselector3_Internalname = sPrefix+"vDYNAMICFILTERSSELECTOR3";
         lblDynamicfiltersmiddle3_Internalname = sPrefix+"DYNAMICFILTERSMIDDLE3";
         edtavUnidadeorganizacional_nome3_Internalname = sPrefix+"vUNIDADEORGANIZACIONAL_NOME3";
         dynavEstado_uf3_Internalname = sPrefix+"vESTADO_UF3";
         dynavUnidadeorganizacional_vinculada3_Internalname = sPrefix+"vUNIDADEORGANIZACIONAL_VINCULADA3";
         imgRemovedynamicfilters3_Internalname = sPrefix+"REMOVEDYNAMICFILTERS3";
         tblTabledynamicfilters_Internalname = sPrefix+"TABLEDYNAMICFILTERS";
         lblJsdynamicfilters_Internalname = sPrefix+"JSDYNAMICFILTERS";
         tblTablefilters_Internalname = sPrefix+"TABLEFILTERS";
         tblTablesearch_Internalname = sPrefix+"TABLESEARCH";
         edtavUpdate_Internalname = sPrefix+"vUPDATE";
         edtavDelete_Internalname = sPrefix+"vDELETE";
         edtavDisplay_Internalname = sPrefix+"vDISPLAY";
         edtUnidadeOrganizacional_Codigo_Internalname = sPrefix+"UNIDADEORGANIZACIONAL_CODIGO";
         edtUnidadeOrganizacional_Nome_Internalname = sPrefix+"UNIDADEORGANIZACIONAL_NOME";
         edtEstado_UF_Internalname = sPrefix+"ESTADO_UF";
         dynUnidadeOrganizacional_Vinculada_Internalname = sPrefix+"UNIDADEORGANIZACIONAL_VINCULADA";
         Gridpaginationbar_Internalname = sPrefix+"GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = sPrefix+"GRIDTABLEWITHPAGINATIONBAR";
         tblTablegridheader_Internalname = sPrefix+"TABLEGRIDHEADER";
         edtTpUo_Codigo_Internalname = sPrefix+"TPUO_CODIGO";
         Workwithplusutilities1_Internalname = sPrefix+"WORKWITHPLUSUTILITIES1";
         chkavDynamicfiltersenabled2_Internalname = sPrefix+"vDYNAMICFILTERSENABLED2";
         chkavDynamicfiltersenabled3_Internalname = sPrefix+"vDYNAMICFILTERSENABLED3";
         edtavTfunidadeorganizacional_nome_Internalname = sPrefix+"vTFUNIDADEORGANIZACIONAL_NOME";
         edtavTfunidadeorganizacional_nome_sel_Internalname = sPrefix+"vTFUNIDADEORGANIZACIONAL_NOME_SEL";
         edtavTfestado_uf_Internalname = sPrefix+"vTFESTADO_UF";
         edtavTfestado_uf_sel_Internalname = sPrefix+"vTFESTADO_UF_SEL";
         edtavTfunidadeorganizacional_vinculada_Internalname = sPrefix+"vTFUNIDADEORGANIZACIONAL_VINCULADA";
         edtavTfunidadeorganizacional_vinculada_sel_Internalname = sPrefix+"vTFUNIDADEORGANIZACIONAL_VINCULADA_SEL";
         edtavTfunidadeorganizacional_vinculada_seldsc_Internalname = sPrefix+"vTFUNIDADEORGANIZACIONAL_VINCULADA_SELDSC";
         Ddo_unidadeorganizacional_nome_Internalname = sPrefix+"DDO_UNIDADEORGANIZACIONAL_NOME";
         edtavDdo_unidadeorganizacional_nometitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_UNIDADEORGANIZACIONAL_NOMETITLECONTROLIDTOREPLACE";
         Ddo_estado_uf_Internalname = sPrefix+"DDO_ESTADO_UF";
         edtavDdo_estado_uftitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_ESTADO_UFTITLECONTROLIDTOREPLACE";
         Ddo_unidadeorganizacional_vinculada_Internalname = sPrefix+"DDO_UNIDADEORGANIZACIONAL_VINCULADA";
         edtavDdo_unidadeorganizacional_vinculadatitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_UNIDADEORGANIZACIONAL_VINCULADATITLECONTROLIDTOREPLACE";
         Form.Internalname = sPrefix+"FORM";
         subGrid_Internalname = sPrefix+"GRID";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         dynUnidadeOrganizacional_Vinculada_Jsonclick = "";
         edtEstado_UF_Jsonclick = "";
         edtUnidadeOrganizacional_Nome_Jsonclick = "";
         edtUnidadeOrganizacional_Codigo_Jsonclick = "";
         imgInsert_Enabled = 1;
         imgInsert_Visible = 1;
         imgInsert_Bitmap = (String)(context.GetImagePath( "5649fbb8-8ce0-4810-a5ce-bd649ea83c3a", "", context.GetTheme( )));
         dynavUnidadeorganizacional_vinculada3_Jsonclick = "";
         dynavEstado_uf3_Jsonclick = "";
         edtavUnidadeorganizacional_nome3_Jsonclick = "";
         cmbavDynamicfiltersselector3_Jsonclick = "";
         imgRemovedynamicfilters2_Visible = 1;
         imgAdddynamicfilters2_Visible = 1;
         dynavUnidadeorganizacional_vinculada2_Jsonclick = "";
         dynavEstado_uf2_Jsonclick = "";
         edtavUnidadeorganizacional_nome2_Jsonclick = "";
         cmbavDynamicfiltersselector2_Jsonclick = "";
         imgRemovedynamicfilters1_Visible = 1;
         imgAdddynamicfilters1_Visible = 1;
         dynavUnidadeorganizacional_vinculada1_Jsonclick = "";
         dynavEstado_uf1_Jsonclick = "";
         edtavUnidadeorganizacional_nome1_Jsonclick = "";
         cmbavDynamicfiltersselector1_Jsonclick = "";
         edtavOrdereddsc_Jsonclick = "";
         cmbavOrderedby_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtUnidadeOrganizacional_Nome_Link = "";
         edtavDisplay_Tooltiptext = "Mostrar";
         edtavDisplay_Link = "";
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = "";
         edtavDelete_Enabled = 1;
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = "";
         edtavUpdate_Enabled = 1;
         dynUnidadeOrganizacional_Vinculada_Titleformat = 0;
         edtEstado_UF_Titleformat = 0;
         edtUnidadeOrganizacional_Nome_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         dynavUnidadeorganizacional_vinculada3.Visible = 1;
         dynavEstado_uf3.Visible = 1;
         edtavUnidadeorganizacional_nome3_Visible = 1;
         dynavUnidadeorganizacional_vinculada2.Visible = 1;
         dynavEstado_uf2.Visible = 1;
         edtavUnidadeorganizacional_nome2_Visible = 1;
         dynavUnidadeorganizacional_vinculada1.Visible = 1;
         dynavEstado_uf1.Visible = 1;
         edtavUnidadeorganizacional_nome1_Visible = 1;
         edtavDisplay_Visible = -1;
         edtavDelete_Visible = -1;
         edtavUpdate_Visible = -1;
         dynUnidadeOrganizacional_Vinculada.Title.Text = "UO Vinculada";
         edtEstado_UF_Title = "UF";
         edtUnidadeOrganizacional_Nome_Title = "Nome";
         edtavOrdereddsc_Visible = 1;
         lblJsdynamicfilters_Caption = "JSDynamicFilters";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         chkavDynamicfiltersenabled3.Caption = "";
         chkavDynamicfiltersenabled2.Caption = "";
         chkavUnidadeorganizacional_ativo.Caption = "";
         edtavDdo_unidadeorganizacional_vinculadatitlecontrolidtoreplace_Visible = 1;
         edtavDdo_estado_uftitlecontrolidtoreplace_Visible = 1;
         edtavDdo_unidadeorganizacional_nometitlecontrolidtoreplace_Visible = 1;
         edtavTfunidadeorganizacional_vinculada_seldsc_Jsonclick = "";
         edtavTfunidadeorganizacional_vinculada_seldsc_Visible = 1;
         edtavTfunidadeorganizacional_vinculada_sel_Jsonclick = "";
         edtavTfunidadeorganizacional_vinculada_sel_Visible = 1;
         edtavTfunidadeorganizacional_vinculada_Jsonclick = "";
         edtavTfunidadeorganizacional_vinculada_Visible = 1;
         edtavTfestado_uf_sel_Jsonclick = "";
         edtavTfestado_uf_sel_Visible = 1;
         edtavTfestado_uf_Jsonclick = "";
         edtavTfestado_uf_Visible = 1;
         edtavTfunidadeorganizacional_nome_sel_Jsonclick = "";
         edtavTfunidadeorganizacional_nome_sel_Visible = 1;
         edtavTfunidadeorganizacional_nome_Jsonclick = "";
         edtavTfunidadeorganizacional_nome_Visible = 1;
         chkavDynamicfiltersenabled3.Visible = 1;
         chkavDynamicfiltersenabled2.Visible = 1;
         edtTpUo_Codigo_Jsonclick = "";
         edtTpUo_Codigo_Visible = 1;
         Ddo_unidadeorganizacional_vinculada_Searchbuttontext = "Pesquisar";
         Ddo_unidadeorganizacional_vinculada_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_unidadeorganizacional_vinculada_Cleanfilter = "Limpar pesquisa";
         Ddo_unidadeorganizacional_vinculada_Loadingdata = "Carregando dados...";
         Ddo_unidadeorganizacional_vinculada_Sortdsc = "Ordenar de Z � A";
         Ddo_unidadeorganizacional_vinculada_Sortasc = "Ordenar de A � Z";
         Ddo_unidadeorganizacional_vinculada_Datalistupdateminimumcharacters = 0;
         Ddo_unidadeorganizacional_vinculada_Datalistproc = "GetGeral_Tp_UOGeral_UnidadeOrganizacionalWCFilterData";
         Ddo_unidadeorganizacional_vinculada_Datalisttype = "Dynamic";
         Ddo_unidadeorganizacional_vinculada_Includedatalist = Convert.ToBoolean( -1);
         Ddo_unidadeorganizacional_vinculada_Filterisrange = Convert.ToBoolean( 0);
         Ddo_unidadeorganizacional_vinculada_Filtertype = "Character";
         Ddo_unidadeorganizacional_vinculada_Includefilter = Convert.ToBoolean( -1);
         Ddo_unidadeorganizacional_vinculada_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_unidadeorganizacional_vinculada_Includesortasc = Convert.ToBoolean( -1);
         Ddo_unidadeorganizacional_vinculada_Titlecontrolidtoreplace = "";
         Ddo_unidadeorganizacional_vinculada_Dropdownoptionstype = "GridTitleSettings";
         Ddo_unidadeorganizacional_vinculada_Cls = "ColumnSettings";
         Ddo_unidadeorganizacional_vinculada_Tooltip = "Op��es";
         Ddo_unidadeorganizacional_vinculada_Caption = "";
         Ddo_estado_uf_Searchbuttontext = "Pesquisar";
         Ddo_estado_uf_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_estado_uf_Cleanfilter = "Limpar pesquisa";
         Ddo_estado_uf_Loadingdata = "Carregando dados...";
         Ddo_estado_uf_Sortdsc = "Ordenar de Z � A";
         Ddo_estado_uf_Sortasc = "Ordenar de A � Z";
         Ddo_estado_uf_Datalistupdateminimumcharacters = 0;
         Ddo_estado_uf_Datalistproc = "GetGeral_Tp_UOGeral_UnidadeOrganizacionalWCFilterData";
         Ddo_estado_uf_Datalisttype = "Dynamic";
         Ddo_estado_uf_Includedatalist = Convert.ToBoolean( -1);
         Ddo_estado_uf_Filterisrange = Convert.ToBoolean( 0);
         Ddo_estado_uf_Filtertype = "Character";
         Ddo_estado_uf_Includefilter = Convert.ToBoolean( -1);
         Ddo_estado_uf_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_estado_uf_Includesortasc = Convert.ToBoolean( -1);
         Ddo_estado_uf_Titlecontrolidtoreplace = "";
         Ddo_estado_uf_Dropdownoptionstype = "GridTitleSettings";
         Ddo_estado_uf_Cls = "ColumnSettings";
         Ddo_estado_uf_Tooltip = "Op��es";
         Ddo_estado_uf_Caption = "";
         Ddo_unidadeorganizacional_nome_Searchbuttontext = "Pesquisar";
         Ddo_unidadeorganizacional_nome_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_unidadeorganizacional_nome_Cleanfilter = "Limpar pesquisa";
         Ddo_unidadeorganizacional_nome_Loadingdata = "Carregando dados...";
         Ddo_unidadeorganizacional_nome_Sortdsc = "Ordenar de Z � A";
         Ddo_unidadeorganizacional_nome_Sortasc = "Ordenar de A � Z";
         Ddo_unidadeorganizacional_nome_Datalistupdateminimumcharacters = 0;
         Ddo_unidadeorganizacional_nome_Datalistproc = "GetGeral_Tp_UOGeral_UnidadeOrganizacionalWCFilterData";
         Ddo_unidadeorganizacional_nome_Datalisttype = "Dynamic";
         Ddo_unidadeorganizacional_nome_Includedatalist = Convert.ToBoolean( -1);
         Ddo_unidadeorganizacional_nome_Filterisrange = Convert.ToBoolean( 0);
         Ddo_unidadeorganizacional_nome_Filtertype = "Character";
         Ddo_unidadeorganizacional_nome_Includefilter = Convert.ToBoolean( -1);
         Ddo_unidadeorganizacional_nome_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_unidadeorganizacional_nome_Includesortasc = Convert.ToBoolean( -1);
         Ddo_unidadeorganizacional_nome_Titlecontrolidtoreplace = "";
         Ddo_unidadeorganizacional_nome_Dropdownoptionstype = "GridTitleSettings";
         Ddo_unidadeorganizacional_nome_Cls = "ColumnSettings";
         Ddo_unidadeorganizacional_nome_Tooltip = "Op��es";
         Ddo_unidadeorganizacional_nome_Caption = "";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         subGrid_Rows = 0;
         context.GX_msglist.DisplayMode = 1;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A611UnidadeOrganizacional_Codigo',fld:'UNIDADEORGANIZACIONAL_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV64Unidadeorganizacional_codigo',fld:'vUNIDADEORGANIZACIONAL_CODIGO',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''},{av:'AV42ddo_UnidadeOrganizacional_NomeTitleControlIdToReplace',fld:'vDDO_UNIDADEORGANIZACIONAL_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_Estado_UFTitleControlIdToReplace',fld:'vDDO_ESTADO_UFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_UnidadeOrganizacional_VinculadaTitleControlIdToReplace',fld:'vDDO_UNIDADEORGANIZACIONAL_VINCULADATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV62Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16UnidadeOrganizacional_Ativo',fld:'vUNIDADEORGANIZACIONAL_ATIVO',pic:'',nv:false},{av:'AV40TFUnidadeOrganizacional_Nome',fld:'vTFUNIDADEORGANIZACIONAL_NOME',pic:'@!',nv:''},{av:'AV41TFUnidadeOrganizacional_Nome_Sel',fld:'vTFUNIDADEORGANIZACIONAL_NOME_SEL',pic:'@!',nv:''},{av:'AV44TFEstado_UF',fld:'vTFESTADO_UF',pic:'@!',nv:''},{av:'AV45TFEstado_UF_Sel',fld:'vTFESTADO_UF_SEL',pic:'@!',nv:''},{av:'AV48TFUnidadeOrganizacional_Vinculada',fld:'vTFUNIDADEORGANIZACIONAL_VINCULADA',pic:'@!',nv:''},{av:'AV49TFUnidadeOrganizacional_Vinculada_Sel',fld:'vTFUNIDADEORGANIZACIONAL_VINCULADA_SEL',pic:'ZZZZZ9',nv:0},{av:'AV52TFUnidadeOrganizacional_Vinculada_SelDsc',fld:'vTFUNIDADEORGANIZACIONAL_VINCULADA_SELDSC',pic:'',nv:''},{av:'AV7TpUo_Codigo',fld:'vTPUO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV32DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV17DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV18UnidadeOrganizacional_Nome1',fld:'vUNIDADEORGANIZACIONAL_NOME1',pic:'@!',nv:''},{av:'AV19Estado_UF1',fld:'vESTADO_UF1',pic:'@!',nv:''},{av:'AV20UnidadeOrganizacional_Vinculada1',fld:'vUNIDADEORGANIZACIONAL_VINCULADA1',pic:'ZZZZZ9',nv:0},{av:'AV31DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV21DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV23UnidadeOrganizacional_Nome2',fld:'vUNIDADEORGANIZACIONAL_NOME2',pic:'@!',nv:''},{av:'AV24Estado_UF2',fld:'vESTADO_UF2',pic:'@!',nv:''},{av:'AV25UnidadeOrganizacional_Vinculada2',fld:'vUNIDADEORGANIZACIONAL_VINCULADA2',pic:'ZZZZZ9',nv:0},{av:'AV26DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV27DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV28UnidadeOrganizacional_Nome3',fld:'vUNIDADEORGANIZACIONAL_NOME3',pic:'@!',nv:''},{av:'AV29Estado_UF3',fld:'vESTADO_UF3',pic:'@!',nv:''},{av:'AV30UnidadeOrganizacional_Vinculada3',fld:'vUNIDADEORGANIZACIONAL_VINCULADA3',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV39UnidadeOrganizacional_NomeTitleFilterData',fld:'vUNIDADEORGANIZACIONAL_NOMETITLEFILTERDATA',pic:'',nv:null},{av:'AV43Estado_UFTitleFilterData',fld:'vESTADO_UFTITLEFILTERDATA',pic:'',nv:null},{av:'AV47UnidadeOrganizacional_VinculadaTitleFilterData',fld:'vUNIDADEORGANIZACIONAL_VINCULADATITLEFILTERDATA',pic:'',nv:null},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'edtUnidadeOrganizacional_Nome_Titleformat',ctrl:'UNIDADEORGANIZACIONAL_NOME',prop:'Titleformat'},{av:'edtUnidadeOrganizacional_Nome_Title',ctrl:'UNIDADEORGANIZACIONAL_NOME',prop:'Title'},{av:'edtEstado_UF_Titleformat',ctrl:'ESTADO_UF',prop:'Titleformat'},{av:'edtEstado_UF_Title',ctrl:'ESTADO_UF',prop:'Title'},{av:'dynUnidadeOrganizacional_Vinculada'},{av:'AV54GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV55GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'imgInsert_Visible',ctrl:'INSERT',prop:'Visible'},{av:'edtavUpdate_Visible',ctrl:'vUPDATE',prop:'Visible'},{av:'edtavDelete_Visible',ctrl:'vDELETE',prop:'Visible'},{av:'edtavDisplay_Visible',ctrl:'vDISPLAY',prop:'Visible'},{av:'imgInsert_Link',ctrl:'INSERT',prop:'Link'},{av:'imgInsert_Enabled',ctrl:'INSERT',prop:'Enabled'},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E11D82',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV17DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV18UnidadeOrganizacional_Nome1',fld:'vUNIDADEORGANIZACIONAL_NOME1',pic:'@!',nv:''},{av:'AV19Estado_UF1',fld:'vESTADO_UF1',pic:'@!',nv:''},{av:'AV20UnidadeOrganizacional_Vinculada1',fld:'vUNIDADEORGANIZACIONAL_VINCULADA1',pic:'ZZZZZ9',nv:0},{av:'AV22DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV23UnidadeOrganizacional_Nome2',fld:'vUNIDADEORGANIZACIONAL_NOME2',pic:'@!',nv:''},{av:'AV24Estado_UF2',fld:'vESTADO_UF2',pic:'@!',nv:''},{av:'AV25UnidadeOrganizacional_Vinculada2',fld:'vUNIDADEORGANIZACIONAL_VINCULADA2',pic:'ZZZZZ9',nv:0},{av:'AV27DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV28UnidadeOrganizacional_Nome3',fld:'vUNIDADEORGANIZACIONAL_NOME3',pic:'@!',nv:''},{av:'AV29Estado_UF3',fld:'vESTADO_UF3',pic:'@!',nv:''},{av:'AV30UnidadeOrganizacional_Vinculada3',fld:'vUNIDADEORGANIZACIONAL_VINCULADA3',pic:'ZZZZZ9',nv:0},{av:'AV21DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV26DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV40TFUnidadeOrganizacional_Nome',fld:'vTFUNIDADEORGANIZACIONAL_NOME',pic:'@!',nv:''},{av:'AV41TFUnidadeOrganizacional_Nome_Sel',fld:'vTFUNIDADEORGANIZACIONAL_NOME_SEL',pic:'@!',nv:''},{av:'AV44TFEstado_UF',fld:'vTFESTADO_UF',pic:'@!',nv:''},{av:'AV45TFEstado_UF_Sel',fld:'vTFESTADO_UF_SEL',pic:'@!',nv:''},{av:'AV48TFUnidadeOrganizacional_Vinculada',fld:'vTFUNIDADEORGANIZACIONAL_VINCULADA',pic:'@!',nv:''},{av:'AV49TFUnidadeOrganizacional_Vinculada_Sel',fld:'vTFUNIDADEORGANIZACIONAL_VINCULADA_SEL',pic:'ZZZZZ9',nv:0},{av:'AV7TpUo_Codigo',fld:'vTPUO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV42ddo_UnidadeOrganizacional_NomeTitleControlIdToReplace',fld:'vDDO_UNIDADEORGANIZACIONAL_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_Estado_UFTitleControlIdToReplace',fld:'vDDO_ESTADO_UFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_UnidadeOrganizacional_VinculadaTitleControlIdToReplace',fld:'vDDO_UNIDADEORGANIZACIONAL_VINCULADATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV62Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV16UnidadeOrganizacional_Ativo',fld:'vUNIDADEORGANIZACIONAL_ATIVO',pic:'',nv:false},{av:'AV52TFUnidadeOrganizacional_Vinculada_SelDsc',fld:'vTFUNIDADEORGANIZACIONAL_VINCULADA_SELDSC',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV32DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV31DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A611UnidadeOrganizacional_Codigo',fld:'UNIDADEORGANIZACIONAL_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV64Unidadeorganizacional_codigo',fld:'vUNIDADEORGANIZACIONAL_CODIGO',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("DDO_UNIDADEORGANIZACIONAL_NOME.ONOPTIONCLICKED","{handler:'E12D82',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV17DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV18UnidadeOrganizacional_Nome1',fld:'vUNIDADEORGANIZACIONAL_NOME1',pic:'@!',nv:''},{av:'AV19Estado_UF1',fld:'vESTADO_UF1',pic:'@!',nv:''},{av:'AV20UnidadeOrganizacional_Vinculada1',fld:'vUNIDADEORGANIZACIONAL_VINCULADA1',pic:'ZZZZZ9',nv:0},{av:'AV22DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV23UnidadeOrganizacional_Nome2',fld:'vUNIDADEORGANIZACIONAL_NOME2',pic:'@!',nv:''},{av:'AV24Estado_UF2',fld:'vESTADO_UF2',pic:'@!',nv:''},{av:'AV25UnidadeOrganizacional_Vinculada2',fld:'vUNIDADEORGANIZACIONAL_VINCULADA2',pic:'ZZZZZ9',nv:0},{av:'AV27DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV28UnidadeOrganizacional_Nome3',fld:'vUNIDADEORGANIZACIONAL_NOME3',pic:'@!',nv:''},{av:'AV29Estado_UF3',fld:'vESTADO_UF3',pic:'@!',nv:''},{av:'AV30UnidadeOrganizacional_Vinculada3',fld:'vUNIDADEORGANIZACIONAL_VINCULADA3',pic:'ZZZZZ9',nv:0},{av:'AV21DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV26DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV40TFUnidadeOrganizacional_Nome',fld:'vTFUNIDADEORGANIZACIONAL_NOME',pic:'@!',nv:''},{av:'AV41TFUnidadeOrganizacional_Nome_Sel',fld:'vTFUNIDADEORGANIZACIONAL_NOME_SEL',pic:'@!',nv:''},{av:'AV44TFEstado_UF',fld:'vTFESTADO_UF',pic:'@!',nv:''},{av:'AV45TFEstado_UF_Sel',fld:'vTFESTADO_UF_SEL',pic:'@!',nv:''},{av:'AV48TFUnidadeOrganizacional_Vinculada',fld:'vTFUNIDADEORGANIZACIONAL_VINCULADA',pic:'@!',nv:''},{av:'AV49TFUnidadeOrganizacional_Vinculada_Sel',fld:'vTFUNIDADEORGANIZACIONAL_VINCULADA_SEL',pic:'ZZZZZ9',nv:0},{av:'AV7TpUo_Codigo',fld:'vTPUO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV42ddo_UnidadeOrganizacional_NomeTitleControlIdToReplace',fld:'vDDO_UNIDADEORGANIZACIONAL_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_Estado_UFTitleControlIdToReplace',fld:'vDDO_ESTADO_UFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_UnidadeOrganizacional_VinculadaTitleControlIdToReplace',fld:'vDDO_UNIDADEORGANIZACIONAL_VINCULADATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV62Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV16UnidadeOrganizacional_Ativo',fld:'vUNIDADEORGANIZACIONAL_ATIVO',pic:'',nv:false},{av:'AV52TFUnidadeOrganizacional_Vinculada_SelDsc',fld:'vTFUNIDADEORGANIZACIONAL_VINCULADA_SELDSC',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV32DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV31DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A611UnidadeOrganizacional_Codigo',fld:'UNIDADEORGANIZACIONAL_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV64Unidadeorganizacional_codigo',fld:'vUNIDADEORGANIZACIONAL_CODIGO',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''},{av:'Ddo_unidadeorganizacional_nome_Activeeventkey',ctrl:'DDO_UNIDADEORGANIZACIONAL_NOME',prop:'ActiveEventKey'},{av:'Ddo_unidadeorganizacional_nome_Filteredtext_get',ctrl:'DDO_UNIDADEORGANIZACIONAL_NOME',prop:'FilteredText_get'},{av:'Ddo_unidadeorganizacional_nome_Selectedvalue_get',ctrl:'DDO_UNIDADEORGANIZACIONAL_NOME',prop:'SelectedValue_get'}],oparms:[{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_unidadeorganizacional_nome_Sortedstatus',ctrl:'DDO_UNIDADEORGANIZACIONAL_NOME',prop:'SortedStatus'},{av:'AV40TFUnidadeOrganizacional_Nome',fld:'vTFUNIDADEORGANIZACIONAL_NOME',pic:'@!',nv:''},{av:'AV41TFUnidadeOrganizacional_Nome_Sel',fld:'vTFUNIDADEORGANIZACIONAL_NOME_SEL',pic:'@!',nv:''},{av:'Ddo_estado_uf_Sortedstatus',ctrl:'DDO_ESTADO_UF',prop:'SortedStatus'},{av:'Ddo_unidadeorganizacional_vinculada_Sortedstatus',ctrl:'DDO_UNIDADEORGANIZACIONAL_VINCULADA',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_ESTADO_UF.ONOPTIONCLICKED","{handler:'E13D82',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV17DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV18UnidadeOrganizacional_Nome1',fld:'vUNIDADEORGANIZACIONAL_NOME1',pic:'@!',nv:''},{av:'AV19Estado_UF1',fld:'vESTADO_UF1',pic:'@!',nv:''},{av:'AV20UnidadeOrganizacional_Vinculada1',fld:'vUNIDADEORGANIZACIONAL_VINCULADA1',pic:'ZZZZZ9',nv:0},{av:'AV22DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV23UnidadeOrganizacional_Nome2',fld:'vUNIDADEORGANIZACIONAL_NOME2',pic:'@!',nv:''},{av:'AV24Estado_UF2',fld:'vESTADO_UF2',pic:'@!',nv:''},{av:'AV25UnidadeOrganizacional_Vinculada2',fld:'vUNIDADEORGANIZACIONAL_VINCULADA2',pic:'ZZZZZ9',nv:0},{av:'AV27DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV28UnidadeOrganizacional_Nome3',fld:'vUNIDADEORGANIZACIONAL_NOME3',pic:'@!',nv:''},{av:'AV29Estado_UF3',fld:'vESTADO_UF3',pic:'@!',nv:''},{av:'AV30UnidadeOrganizacional_Vinculada3',fld:'vUNIDADEORGANIZACIONAL_VINCULADA3',pic:'ZZZZZ9',nv:0},{av:'AV21DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV26DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV40TFUnidadeOrganizacional_Nome',fld:'vTFUNIDADEORGANIZACIONAL_NOME',pic:'@!',nv:''},{av:'AV41TFUnidadeOrganizacional_Nome_Sel',fld:'vTFUNIDADEORGANIZACIONAL_NOME_SEL',pic:'@!',nv:''},{av:'AV44TFEstado_UF',fld:'vTFESTADO_UF',pic:'@!',nv:''},{av:'AV45TFEstado_UF_Sel',fld:'vTFESTADO_UF_SEL',pic:'@!',nv:''},{av:'AV48TFUnidadeOrganizacional_Vinculada',fld:'vTFUNIDADEORGANIZACIONAL_VINCULADA',pic:'@!',nv:''},{av:'AV49TFUnidadeOrganizacional_Vinculada_Sel',fld:'vTFUNIDADEORGANIZACIONAL_VINCULADA_SEL',pic:'ZZZZZ9',nv:0},{av:'AV7TpUo_Codigo',fld:'vTPUO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV42ddo_UnidadeOrganizacional_NomeTitleControlIdToReplace',fld:'vDDO_UNIDADEORGANIZACIONAL_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_Estado_UFTitleControlIdToReplace',fld:'vDDO_ESTADO_UFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_UnidadeOrganizacional_VinculadaTitleControlIdToReplace',fld:'vDDO_UNIDADEORGANIZACIONAL_VINCULADATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV62Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV16UnidadeOrganizacional_Ativo',fld:'vUNIDADEORGANIZACIONAL_ATIVO',pic:'',nv:false},{av:'AV52TFUnidadeOrganizacional_Vinculada_SelDsc',fld:'vTFUNIDADEORGANIZACIONAL_VINCULADA_SELDSC',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV32DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV31DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A611UnidadeOrganizacional_Codigo',fld:'UNIDADEORGANIZACIONAL_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV64Unidadeorganizacional_codigo',fld:'vUNIDADEORGANIZACIONAL_CODIGO',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''},{av:'Ddo_estado_uf_Activeeventkey',ctrl:'DDO_ESTADO_UF',prop:'ActiveEventKey'},{av:'Ddo_estado_uf_Filteredtext_get',ctrl:'DDO_ESTADO_UF',prop:'FilteredText_get'},{av:'Ddo_estado_uf_Selectedvalue_get',ctrl:'DDO_ESTADO_UF',prop:'SelectedValue_get'}],oparms:[{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_estado_uf_Sortedstatus',ctrl:'DDO_ESTADO_UF',prop:'SortedStatus'},{av:'AV44TFEstado_UF',fld:'vTFESTADO_UF',pic:'@!',nv:''},{av:'AV45TFEstado_UF_Sel',fld:'vTFESTADO_UF_SEL',pic:'@!',nv:''},{av:'Ddo_unidadeorganizacional_nome_Sortedstatus',ctrl:'DDO_UNIDADEORGANIZACIONAL_NOME',prop:'SortedStatus'},{av:'Ddo_unidadeorganizacional_vinculada_Sortedstatus',ctrl:'DDO_UNIDADEORGANIZACIONAL_VINCULADA',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_UNIDADEORGANIZACIONAL_VINCULADA.ONOPTIONCLICKED","{handler:'E14D82',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV17DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV18UnidadeOrganizacional_Nome1',fld:'vUNIDADEORGANIZACIONAL_NOME1',pic:'@!',nv:''},{av:'AV19Estado_UF1',fld:'vESTADO_UF1',pic:'@!',nv:''},{av:'AV20UnidadeOrganizacional_Vinculada1',fld:'vUNIDADEORGANIZACIONAL_VINCULADA1',pic:'ZZZZZ9',nv:0},{av:'AV22DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV23UnidadeOrganizacional_Nome2',fld:'vUNIDADEORGANIZACIONAL_NOME2',pic:'@!',nv:''},{av:'AV24Estado_UF2',fld:'vESTADO_UF2',pic:'@!',nv:''},{av:'AV25UnidadeOrganizacional_Vinculada2',fld:'vUNIDADEORGANIZACIONAL_VINCULADA2',pic:'ZZZZZ9',nv:0},{av:'AV27DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV28UnidadeOrganizacional_Nome3',fld:'vUNIDADEORGANIZACIONAL_NOME3',pic:'@!',nv:''},{av:'AV29Estado_UF3',fld:'vESTADO_UF3',pic:'@!',nv:''},{av:'AV30UnidadeOrganizacional_Vinculada3',fld:'vUNIDADEORGANIZACIONAL_VINCULADA3',pic:'ZZZZZ9',nv:0},{av:'AV21DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV26DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV40TFUnidadeOrganizacional_Nome',fld:'vTFUNIDADEORGANIZACIONAL_NOME',pic:'@!',nv:''},{av:'AV41TFUnidadeOrganizacional_Nome_Sel',fld:'vTFUNIDADEORGANIZACIONAL_NOME_SEL',pic:'@!',nv:''},{av:'AV44TFEstado_UF',fld:'vTFESTADO_UF',pic:'@!',nv:''},{av:'AV45TFEstado_UF_Sel',fld:'vTFESTADO_UF_SEL',pic:'@!',nv:''},{av:'AV48TFUnidadeOrganizacional_Vinculada',fld:'vTFUNIDADEORGANIZACIONAL_VINCULADA',pic:'@!',nv:''},{av:'AV49TFUnidadeOrganizacional_Vinculada_Sel',fld:'vTFUNIDADEORGANIZACIONAL_VINCULADA_SEL',pic:'ZZZZZ9',nv:0},{av:'AV7TpUo_Codigo',fld:'vTPUO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV42ddo_UnidadeOrganizacional_NomeTitleControlIdToReplace',fld:'vDDO_UNIDADEORGANIZACIONAL_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_Estado_UFTitleControlIdToReplace',fld:'vDDO_ESTADO_UFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_UnidadeOrganizacional_VinculadaTitleControlIdToReplace',fld:'vDDO_UNIDADEORGANIZACIONAL_VINCULADATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV62Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV16UnidadeOrganizacional_Ativo',fld:'vUNIDADEORGANIZACIONAL_ATIVO',pic:'',nv:false},{av:'AV52TFUnidadeOrganizacional_Vinculada_SelDsc',fld:'vTFUNIDADEORGANIZACIONAL_VINCULADA_SELDSC',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV32DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV31DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A611UnidadeOrganizacional_Codigo',fld:'UNIDADEORGANIZACIONAL_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV64Unidadeorganizacional_codigo',fld:'vUNIDADEORGANIZACIONAL_CODIGO',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''},{av:'Ddo_unidadeorganizacional_vinculada_Activeeventkey',ctrl:'DDO_UNIDADEORGANIZACIONAL_VINCULADA',prop:'ActiveEventKey'},{av:'Ddo_unidadeorganizacional_vinculada_Filteredtext_get',ctrl:'DDO_UNIDADEORGANIZACIONAL_VINCULADA',prop:'FilteredText_get'},{av:'Ddo_unidadeorganizacional_vinculada_Selectedvalue_get',ctrl:'DDO_UNIDADEORGANIZACIONAL_VINCULADA',prop:'SelectedValue_get'},{av:'Ddo_unidadeorganizacional_vinculada_Selectedtext_get',ctrl:'DDO_UNIDADEORGANIZACIONAL_VINCULADA',prop:'SelectedText_get'}],oparms:[{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_unidadeorganizacional_vinculada_Sortedstatus',ctrl:'DDO_UNIDADEORGANIZACIONAL_VINCULADA',prop:'SortedStatus'},{av:'AV48TFUnidadeOrganizacional_Vinculada',fld:'vTFUNIDADEORGANIZACIONAL_VINCULADA',pic:'@!',nv:''},{av:'AV49TFUnidadeOrganizacional_Vinculada_Sel',fld:'vTFUNIDADEORGANIZACIONAL_VINCULADA_SEL',pic:'ZZZZZ9',nv:0},{av:'AV52TFUnidadeOrganizacional_Vinculada_SelDsc',fld:'vTFUNIDADEORGANIZACIONAL_VINCULADA_SELDSC',pic:'',nv:''},{av:'Ddo_unidadeorganizacional_nome_Sortedstatus',ctrl:'DDO_UNIDADEORGANIZACIONAL_NOME',prop:'SortedStatus'},{av:'Ddo_estado_uf_Sortedstatus',ctrl:'DDO_ESTADO_UF',prop:'SortedStatus'}]}");
         setEventMetadata("GRID.LOAD","{handler:'E28D82',iparms:[{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A611UnidadeOrganizacional_Codigo',fld:'UNIDADEORGANIZACIONAL_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'edtavUpdate_Tooltiptext',ctrl:'vUPDATE',prop:'Tooltiptext'},{av:'edtavUpdate_Link',ctrl:'vUPDATE',prop:'Link'},{av:'AV33Update',fld:'vUPDATE',pic:'',nv:''},{av:'edtavUpdate_Enabled',ctrl:'vUPDATE',prop:'Enabled'},{av:'edtavDelete_Tooltiptext',ctrl:'vDELETE',prop:'Tooltiptext'},{av:'edtavDelete_Link',ctrl:'vDELETE',prop:'Link'},{av:'AV34Delete',fld:'vDELETE',pic:'',nv:''},{av:'edtavDelete_Enabled',ctrl:'vDELETE',prop:'Enabled'},{av:'AV35Display',fld:'vDISPLAY',pic:'',nv:''},{av:'edtavDisplay_Tooltiptext',ctrl:'vDISPLAY',prop:'Tooltiptext'},{av:'edtavDisplay_Link',ctrl:'vDISPLAY',prop:'Link'},{av:'edtUnidadeOrganizacional_Nome_Link',ctrl:'UNIDADEORGANIZACIONAL_NOME',prop:'Link'}]}");
         setEventMetadata("VORDEREDBY.CLICK","{handler:'E15D82',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV17DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV18UnidadeOrganizacional_Nome1',fld:'vUNIDADEORGANIZACIONAL_NOME1',pic:'@!',nv:''},{av:'AV19Estado_UF1',fld:'vESTADO_UF1',pic:'@!',nv:''},{av:'AV20UnidadeOrganizacional_Vinculada1',fld:'vUNIDADEORGANIZACIONAL_VINCULADA1',pic:'ZZZZZ9',nv:0},{av:'AV22DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV23UnidadeOrganizacional_Nome2',fld:'vUNIDADEORGANIZACIONAL_NOME2',pic:'@!',nv:''},{av:'AV24Estado_UF2',fld:'vESTADO_UF2',pic:'@!',nv:''},{av:'AV25UnidadeOrganizacional_Vinculada2',fld:'vUNIDADEORGANIZACIONAL_VINCULADA2',pic:'ZZZZZ9',nv:0},{av:'AV27DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV28UnidadeOrganizacional_Nome3',fld:'vUNIDADEORGANIZACIONAL_NOME3',pic:'@!',nv:''},{av:'AV29Estado_UF3',fld:'vESTADO_UF3',pic:'@!',nv:''},{av:'AV30UnidadeOrganizacional_Vinculada3',fld:'vUNIDADEORGANIZACIONAL_VINCULADA3',pic:'ZZZZZ9',nv:0},{av:'AV21DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV26DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV40TFUnidadeOrganizacional_Nome',fld:'vTFUNIDADEORGANIZACIONAL_NOME',pic:'@!',nv:''},{av:'AV41TFUnidadeOrganizacional_Nome_Sel',fld:'vTFUNIDADEORGANIZACIONAL_NOME_SEL',pic:'@!',nv:''},{av:'AV44TFEstado_UF',fld:'vTFESTADO_UF',pic:'@!',nv:''},{av:'AV45TFEstado_UF_Sel',fld:'vTFESTADO_UF_SEL',pic:'@!',nv:''},{av:'AV48TFUnidadeOrganizacional_Vinculada',fld:'vTFUNIDADEORGANIZACIONAL_VINCULADA',pic:'@!',nv:''},{av:'AV49TFUnidadeOrganizacional_Vinculada_Sel',fld:'vTFUNIDADEORGANIZACIONAL_VINCULADA_SEL',pic:'ZZZZZ9',nv:0},{av:'AV7TpUo_Codigo',fld:'vTPUO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV42ddo_UnidadeOrganizacional_NomeTitleControlIdToReplace',fld:'vDDO_UNIDADEORGANIZACIONAL_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_Estado_UFTitleControlIdToReplace',fld:'vDDO_ESTADO_UFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_UnidadeOrganizacional_VinculadaTitleControlIdToReplace',fld:'vDDO_UNIDADEORGANIZACIONAL_VINCULADATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV62Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV16UnidadeOrganizacional_Ativo',fld:'vUNIDADEORGANIZACIONAL_ATIVO',pic:'',nv:false},{av:'AV52TFUnidadeOrganizacional_Vinculada_SelDsc',fld:'vTFUNIDADEORGANIZACIONAL_VINCULADA_SELDSC',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV32DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV31DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A611UnidadeOrganizacional_Codigo',fld:'UNIDADEORGANIZACIONAL_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV64Unidadeorganizacional_codigo',fld:'vUNIDADEORGANIZACIONAL_CODIGO',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''}],oparms:[]}");
         setEventMetadata("'ADDDYNAMICFILTERS1'","{handler:'E21D82',iparms:[],oparms:[{av:'AV21DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS1'","{handler:'E16D82',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV17DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV18UnidadeOrganizacional_Nome1',fld:'vUNIDADEORGANIZACIONAL_NOME1',pic:'@!',nv:''},{av:'AV19Estado_UF1',fld:'vESTADO_UF1',pic:'@!',nv:''},{av:'AV20UnidadeOrganizacional_Vinculada1',fld:'vUNIDADEORGANIZACIONAL_VINCULADA1',pic:'ZZZZZ9',nv:0},{av:'AV22DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV23UnidadeOrganizacional_Nome2',fld:'vUNIDADEORGANIZACIONAL_NOME2',pic:'@!',nv:''},{av:'AV24Estado_UF2',fld:'vESTADO_UF2',pic:'@!',nv:''},{av:'AV25UnidadeOrganizacional_Vinculada2',fld:'vUNIDADEORGANIZACIONAL_VINCULADA2',pic:'ZZZZZ9',nv:0},{av:'AV27DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV28UnidadeOrganizacional_Nome3',fld:'vUNIDADEORGANIZACIONAL_NOME3',pic:'@!',nv:''},{av:'AV29Estado_UF3',fld:'vESTADO_UF3',pic:'@!',nv:''},{av:'AV30UnidadeOrganizacional_Vinculada3',fld:'vUNIDADEORGANIZACIONAL_VINCULADA3',pic:'ZZZZZ9',nv:0},{av:'AV21DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV26DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV40TFUnidadeOrganizacional_Nome',fld:'vTFUNIDADEORGANIZACIONAL_NOME',pic:'@!',nv:''},{av:'AV41TFUnidadeOrganizacional_Nome_Sel',fld:'vTFUNIDADEORGANIZACIONAL_NOME_SEL',pic:'@!',nv:''},{av:'AV44TFEstado_UF',fld:'vTFESTADO_UF',pic:'@!',nv:''},{av:'AV45TFEstado_UF_Sel',fld:'vTFESTADO_UF_SEL',pic:'@!',nv:''},{av:'AV48TFUnidadeOrganizacional_Vinculada',fld:'vTFUNIDADEORGANIZACIONAL_VINCULADA',pic:'@!',nv:''},{av:'AV49TFUnidadeOrganizacional_Vinculada_Sel',fld:'vTFUNIDADEORGANIZACIONAL_VINCULADA_SEL',pic:'ZZZZZ9',nv:0},{av:'AV7TpUo_Codigo',fld:'vTPUO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV42ddo_UnidadeOrganizacional_NomeTitleControlIdToReplace',fld:'vDDO_UNIDADEORGANIZACIONAL_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_Estado_UFTitleControlIdToReplace',fld:'vDDO_ESTADO_UFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_UnidadeOrganizacional_VinculadaTitleControlIdToReplace',fld:'vDDO_UNIDADEORGANIZACIONAL_VINCULADATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV62Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV16UnidadeOrganizacional_Ativo',fld:'vUNIDADEORGANIZACIONAL_ATIVO',pic:'',nv:false},{av:'AV52TFUnidadeOrganizacional_Vinculada_SelDsc',fld:'vTFUNIDADEORGANIZACIONAL_VINCULADA_SELDSC',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV32DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV31DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A611UnidadeOrganizacional_Codigo',fld:'UNIDADEORGANIZACIONAL_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV64Unidadeorganizacional_codigo',fld:'vUNIDADEORGANIZACIONAL_CODIGO',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''}],oparms:[{av:'AV31DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV32DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV21DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV23UnidadeOrganizacional_Nome2',fld:'vUNIDADEORGANIZACIONAL_NOME2',pic:'@!',nv:''},{av:'AV26DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV27DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV28UnidadeOrganizacional_Nome3',fld:'vUNIDADEORGANIZACIONAL_NOME3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV17DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV18UnidadeOrganizacional_Nome1',fld:'vUNIDADEORGANIZACIONAL_NOME1',pic:'@!',nv:''},{av:'AV19Estado_UF1',fld:'vESTADO_UF1',pic:'@!',nv:''},{av:'AV20UnidadeOrganizacional_Vinculada1',fld:'vUNIDADEORGANIZACIONAL_VINCULADA1',pic:'ZZZZZ9',nv:0},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV24Estado_UF2',fld:'vESTADO_UF2',pic:'@!',nv:''},{av:'AV25UnidadeOrganizacional_Vinculada2',fld:'vUNIDADEORGANIZACIONAL_VINCULADA2',pic:'ZZZZZ9',nv:0},{av:'AV29Estado_UF3',fld:'vESTADO_UF3',pic:'@!',nv:''},{av:'AV30UnidadeOrganizacional_Vinculada3',fld:'vUNIDADEORGANIZACIONAL_VINCULADA3',pic:'ZZZZZ9',nv:0},{av:'edtavUnidadeorganizacional_nome2_Visible',ctrl:'vUNIDADEORGANIZACIONAL_NOME2',prop:'Visible'},{av:'dynavEstado_uf2'},{av:'dynavUnidadeorganizacional_vinculada2'},{av:'edtavUnidadeorganizacional_nome3_Visible',ctrl:'vUNIDADEORGANIZACIONAL_NOME3',prop:'Visible'},{av:'dynavEstado_uf3'},{av:'dynavUnidadeorganizacional_vinculada3'},{av:'edtavUnidadeorganizacional_nome1_Visible',ctrl:'vUNIDADEORGANIZACIONAL_NOME1',prop:'Visible'},{av:'dynavEstado_uf1'},{av:'dynavUnidadeorganizacional_vinculada1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR1.CLICK","{handler:'E22D82',iparms:[{av:'AV17DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''}],oparms:[{av:'edtavUnidadeorganizacional_nome1_Visible',ctrl:'vUNIDADEORGANIZACIONAL_NOME1',prop:'Visible'},{av:'dynavEstado_uf1'},{av:'dynavUnidadeorganizacional_vinculada1'}]}");
         setEventMetadata("'ADDDYNAMICFILTERS2'","{handler:'E23D82',iparms:[],oparms:[{av:'AV26DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS2'","{handler:'E17D82',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV17DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV18UnidadeOrganizacional_Nome1',fld:'vUNIDADEORGANIZACIONAL_NOME1',pic:'@!',nv:''},{av:'AV19Estado_UF1',fld:'vESTADO_UF1',pic:'@!',nv:''},{av:'AV20UnidadeOrganizacional_Vinculada1',fld:'vUNIDADEORGANIZACIONAL_VINCULADA1',pic:'ZZZZZ9',nv:0},{av:'AV22DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV23UnidadeOrganizacional_Nome2',fld:'vUNIDADEORGANIZACIONAL_NOME2',pic:'@!',nv:''},{av:'AV24Estado_UF2',fld:'vESTADO_UF2',pic:'@!',nv:''},{av:'AV25UnidadeOrganizacional_Vinculada2',fld:'vUNIDADEORGANIZACIONAL_VINCULADA2',pic:'ZZZZZ9',nv:0},{av:'AV27DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV28UnidadeOrganizacional_Nome3',fld:'vUNIDADEORGANIZACIONAL_NOME3',pic:'@!',nv:''},{av:'AV29Estado_UF3',fld:'vESTADO_UF3',pic:'@!',nv:''},{av:'AV30UnidadeOrganizacional_Vinculada3',fld:'vUNIDADEORGANIZACIONAL_VINCULADA3',pic:'ZZZZZ9',nv:0},{av:'AV21DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV26DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV40TFUnidadeOrganizacional_Nome',fld:'vTFUNIDADEORGANIZACIONAL_NOME',pic:'@!',nv:''},{av:'AV41TFUnidadeOrganizacional_Nome_Sel',fld:'vTFUNIDADEORGANIZACIONAL_NOME_SEL',pic:'@!',nv:''},{av:'AV44TFEstado_UF',fld:'vTFESTADO_UF',pic:'@!',nv:''},{av:'AV45TFEstado_UF_Sel',fld:'vTFESTADO_UF_SEL',pic:'@!',nv:''},{av:'AV48TFUnidadeOrganizacional_Vinculada',fld:'vTFUNIDADEORGANIZACIONAL_VINCULADA',pic:'@!',nv:''},{av:'AV49TFUnidadeOrganizacional_Vinculada_Sel',fld:'vTFUNIDADEORGANIZACIONAL_VINCULADA_SEL',pic:'ZZZZZ9',nv:0},{av:'AV7TpUo_Codigo',fld:'vTPUO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV42ddo_UnidadeOrganizacional_NomeTitleControlIdToReplace',fld:'vDDO_UNIDADEORGANIZACIONAL_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_Estado_UFTitleControlIdToReplace',fld:'vDDO_ESTADO_UFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_UnidadeOrganizacional_VinculadaTitleControlIdToReplace',fld:'vDDO_UNIDADEORGANIZACIONAL_VINCULADATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV62Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV16UnidadeOrganizacional_Ativo',fld:'vUNIDADEORGANIZACIONAL_ATIVO',pic:'',nv:false},{av:'AV52TFUnidadeOrganizacional_Vinculada_SelDsc',fld:'vTFUNIDADEORGANIZACIONAL_VINCULADA_SELDSC',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV32DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV31DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A611UnidadeOrganizacional_Codigo',fld:'UNIDADEORGANIZACIONAL_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV64Unidadeorganizacional_codigo',fld:'vUNIDADEORGANIZACIONAL_CODIGO',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''}],oparms:[{av:'AV31DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV21DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV22DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV23UnidadeOrganizacional_Nome2',fld:'vUNIDADEORGANIZACIONAL_NOME2',pic:'@!',nv:''},{av:'AV26DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV27DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV28UnidadeOrganizacional_Nome3',fld:'vUNIDADEORGANIZACIONAL_NOME3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV17DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV18UnidadeOrganizacional_Nome1',fld:'vUNIDADEORGANIZACIONAL_NOME1',pic:'@!',nv:''},{av:'AV19Estado_UF1',fld:'vESTADO_UF1',pic:'@!',nv:''},{av:'AV20UnidadeOrganizacional_Vinculada1',fld:'vUNIDADEORGANIZACIONAL_VINCULADA1',pic:'ZZZZZ9',nv:0},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV24Estado_UF2',fld:'vESTADO_UF2',pic:'@!',nv:''},{av:'AV25UnidadeOrganizacional_Vinculada2',fld:'vUNIDADEORGANIZACIONAL_VINCULADA2',pic:'ZZZZZ9',nv:0},{av:'AV29Estado_UF3',fld:'vESTADO_UF3',pic:'@!',nv:''},{av:'AV30UnidadeOrganizacional_Vinculada3',fld:'vUNIDADEORGANIZACIONAL_VINCULADA3',pic:'ZZZZZ9',nv:0},{av:'edtavUnidadeorganizacional_nome2_Visible',ctrl:'vUNIDADEORGANIZACIONAL_NOME2',prop:'Visible'},{av:'dynavEstado_uf2'},{av:'dynavUnidadeorganizacional_vinculada2'},{av:'edtavUnidadeorganizacional_nome3_Visible',ctrl:'vUNIDADEORGANIZACIONAL_NOME3',prop:'Visible'},{av:'dynavEstado_uf3'},{av:'dynavUnidadeorganizacional_vinculada3'},{av:'edtavUnidadeorganizacional_nome1_Visible',ctrl:'vUNIDADEORGANIZACIONAL_NOME1',prop:'Visible'},{av:'dynavEstado_uf1'},{av:'dynavUnidadeorganizacional_vinculada1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR2.CLICK","{handler:'E24D82',iparms:[{av:'AV22DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''}],oparms:[{av:'edtavUnidadeorganizacional_nome2_Visible',ctrl:'vUNIDADEORGANIZACIONAL_NOME2',prop:'Visible'},{av:'dynavEstado_uf2'},{av:'dynavUnidadeorganizacional_vinculada2'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS3'","{handler:'E18D82',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV17DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV18UnidadeOrganizacional_Nome1',fld:'vUNIDADEORGANIZACIONAL_NOME1',pic:'@!',nv:''},{av:'AV19Estado_UF1',fld:'vESTADO_UF1',pic:'@!',nv:''},{av:'AV20UnidadeOrganizacional_Vinculada1',fld:'vUNIDADEORGANIZACIONAL_VINCULADA1',pic:'ZZZZZ9',nv:0},{av:'AV22DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV23UnidadeOrganizacional_Nome2',fld:'vUNIDADEORGANIZACIONAL_NOME2',pic:'@!',nv:''},{av:'AV24Estado_UF2',fld:'vESTADO_UF2',pic:'@!',nv:''},{av:'AV25UnidadeOrganizacional_Vinculada2',fld:'vUNIDADEORGANIZACIONAL_VINCULADA2',pic:'ZZZZZ9',nv:0},{av:'AV27DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV28UnidadeOrganizacional_Nome3',fld:'vUNIDADEORGANIZACIONAL_NOME3',pic:'@!',nv:''},{av:'AV29Estado_UF3',fld:'vESTADO_UF3',pic:'@!',nv:''},{av:'AV30UnidadeOrganizacional_Vinculada3',fld:'vUNIDADEORGANIZACIONAL_VINCULADA3',pic:'ZZZZZ9',nv:0},{av:'AV21DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV26DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV40TFUnidadeOrganizacional_Nome',fld:'vTFUNIDADEORGANIZACIONAL_NOME',pic:'@!',nv:''},{av:'AV41TFUnidadeOrganizacional_Nome_Sel',fld:'vTFUNIDADEORGANIZACIONAL_NOME_SEL',pic:'@!',nv:''},{av:'AV44TFEstado_UF',fld:'vTFESTADO_UF',pic:'@!',nv:''},{av:'AV45TFEstado_UF_Sel',fld:'vTFESTADO_UF_SEL',pic:'@!',nv:''},{av:'AV48TFUnidadeOrganizacional_Vinculada',fld:'vTFUNIDADEORGANIZACIONAL_VINCULADA',pic:'@!',nv:''},{av:'AV49TFUnidadeOrganizacional_Vinculada_Sel',fld:'vTFUNIDADEORGANIZACIONAL_VINCULADA_SEL',pic:'ZZZZZ9',nv:0},{av:'AV7TpUo_Codigo',fld:'vTPUO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV42ddo_UnidadeOrganizacional_NomeTitleControlIdToReplace',fld:'vDDO_UNIDADEORGANIZACIONAL_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_Estado_UFTitleControlIdToReplace',fld:'vDDO_ESTADO_UFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_UnidadeOrganizacional_VinculadaTitleControlIdToReplace',fld:'vDDO_UNIDADEORGANIZACIONAL_VINCULADATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV62Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV16UnidadeOrganizacional_Ativo',fld:'vUNIDADEORGANIZACIONAL_ATIVO',pic:'',nv:false},{av:'AV52TFUnidadeOrganizacional_Vinculada_SelDsc',fld:'vTFUNIDADEORGANIZACIONAL_VINCULADA_SELDSC',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV32DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV31DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A611UnidadeOrganizacional_Codigo',fld:'UNIDADEORGANIZACIONAL_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV64Unidadeorganizacional_codigo',fld:'vUNIDADEORGANIZACIONAL_CODIGO',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''}],oparms:[{av:'AV31DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV26DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV21DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV23UnidadeOrganizacional_Nome2',fld:'vUNIDADEORGANIZACIONAL_NOME2',pic:'@!',nv:''},{av:'AV27DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV28UnidadeOrganizacional_Nome3',fld:'vUNIDADEORGANIZACIONAL_NOME3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV17DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV18UnidadeOrganizacional_Nome1',fld:'vUNIDADEORGANIZACIONAL_NOME1',pic:'@!',nv:''},{av:'AV19Estado_UF1',fld:'vESTADO_UF1',pic:'@!',nv:''},{av:'AV20UnidadeOrganizacional_Vinculada1',fld:'vUNIDADEORGANIZACIONAL_VINCULADA1',pic:'ZZZZZ9',nv:0},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV24Estado_UF2',fld:'vESTADO_UF2',pic:'@!',nv:''},{av:'AV25UnidadeOrganizacional_Vinculada2',fld:'vUNIDADEORGANIZACIONAL_VINCULADA2',pic:'ZZZZZ9',nv:0},{av:'AV29Estado_UF3',fld:'vESTADO_UF3',pic:'@!',nv:''},{av:'AV30UnidadeOrganizacional_Vinculada3',fld:'vUNIDADEORGANIZACIONAL_VINCULADA3',pic:'ZZZZZ9',nv:0},{av:'edtavUnidadeorganizacional_nome2_Visible',ctrl:'vUNIDADEORGANIZACIONAL_NOME2',prop:'Visible'},{av:'dynavEstado_uf2'},{av:'dynavUnidadeorganizacional_vinculada2'},{av:'edtavUnidadeorganizacional_nome3_Visible',ctrl:'vUNIDADEORGANIZACIONAL_NOME3',prop:'Visible'},{av:'dynavEstado_uf3'},{av:'dynavUnidadeorganizacional_vinculada3'},{av:'edtavUnidadeorganizacional_nome1_Visible',ctrl:'vUNIDADEORGANIZACIONAL_NOME1',prop:'Visible'},{av:'dynavEstado_uf1'},{av:'dynavUnidadeorganizacional_vinculada1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR3.CLICK","{handler:'E25D82',iparms:[{av:'AV27DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''}],oparms:[{av:'edtavUnidadeorganizacional_nome3_Visible',ctrl:'vUNIDADEORGANIZACIONAL_NOME3',prop:'Visible'},{av:'dynavEstado_uf3'},{av:'dynavUnidadeorganizacional_vinculada3'}]}");
         setEventMetadata("'DOCLEANFILTERS'","{handler:'E19D82',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV17DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV18UnidadeOrganizacional_Nome1',fld:'vUNIDADEORGANIZACIONAL_NOME1',pic:'@!',nv:''},{av:'AV19Estado_UF1',fld:'vESTADO_UF1',pic:'@!',nv:''},{av:'AV20UnidadeOrganizacional_Vinculada1',fld:'vUNIDADEORGANIZACIONAL_VINCULADA1',pic:'ZZZZZ9',nv:0},{av:'AV22DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV23UnidadeOrganizacional_Nome2',fld:'vUNIDADEORGANIZACIONAL_NOME2',pic:'@!',nv:''},{av:'AV24Estado_UF2',fld:'vESTADO_UF2',pic:'@!',nv:''},{av:'AV25UnidadeOrganizacional_Vinculada2',fld:'vUNIDADEORGANIZACIONAL_VINCULADA2',pic:'ZZZZZ9',nv:0},{av:'AV27DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV28UnidadeOrganizacional_Nome3',fld:'vUNIDADEORGANIZACIONAL_NOME3',pic:'@!',nv:''},{av:'AV29Estado_UF3',fld:'vESTADO_UF3',pic:'@!',nv:''},{av:'AV30UnidadeOrganizacional_Vinculada3',fld:'vUNIDADEORGANIZACIONAL_VINCULADA3',pic:'ZZZZZ9',nv:0},{av:'AV21DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV26DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV40TFUnidadeOrganizacional_Nome',fld:'vTFUNIDADEORGANIZACIONAL_NOME',pic:'@!',nv:''},{av:'AV41TFUnidadeOrganizacional_Nome_Sel',fld:'vTFUNIDADEORGANIZACIONAL_NOME_SEL',pic:'@!',nv:''},{av:'AV44TFEstado_UF',fld:'vTFESTADO_UF',pic:'@!',nv:''},{av:'AV45TFEstado_UF_Sel',fld:'vTFESTADO_UF_SEL',pic:'@!',nv:''},{av:'AV48TFUnidadeOrganizacional_Vinculada',fld:'vTFUNIDADEORGANIZACIONAL_VINCULADA',pic:'@!',nv:''},{av:'AV49TFUnidadeOrganizacional_Vinculada_Sel',fld:'vTFUNIDADEORGANIZACIONAL_VINCULADA_SEL',pic:'ZZZZZ9',nv:0},{av:'AV7TpUo_Codigo',fld:'vTPUO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV42ddo_UnidadeOrganizacional_NomeTitleControlIdToReplace',fld:'vDDO_UNIDADEORGANIZACIONAL_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV46ddo_Estado_UFTitleControlIdToReplace',fld:'vDDO_ESTADO_UFTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV50ddo_UnidadeOrganizacional_VinculadaTitleControlIdToReplace',fld:'vDDO_UNIDADEORGANIZACIONAL_VINCULADATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV62Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV16UnidadeOrganizacional_Ativo',fld:'vUNIDADEORGANIZACIONAL_ATIVO',pic:'',nv:false},{av:'AV52TFUnidadeOrganizacional_Vinculada_SelDsc',fld:'vTFUNIDADEORGANIZACIONAL_VINCULADA_SELDSC',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV32DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV31DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A611UnidadeOrganizacional_Codigo',fld:'UNIDADEORGANIZACIONAL_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV64Unidadeorganizacional_codigo',fld:'vUNIDADEORGANIZACIONAL_CODIGO',pic:'ZZZZZ9',nv:0},{av:'sPrefix',nv:''}],oparms:[{av:'AV16UnidadeOrganizacional_Ativo',fld:'vUNIDADEORGANIZACIONAL_ATIVO',pic:'',nv:false},{av:'AV40TFUnidadeOrganizacional_Nome',fld:'vTFUNIDADEORGANIZACIONAL_NOME',pic:'@!',nv:''},{av:'Ddo_unidadeorganizacional_nome_Filteredtext_set',ctrl:'DDO_UNIDADEORGANIZACIONAL_NOME',prop:'FilteredText_set'},{av:'AV41TFUnidadeOrganizacional_Nome_Sel',fld:'vTFUNIDADEORGANIZACIONAL_NOME_SEL',pic:'@!',nv:''},{av:'Ddo_unidadeorganizacional_nome_Selectedvalue_set',ctrl:'DDO_UNIDADEORGANIZACIONAL_NOME',prop:'SelectedValue_set'},{av:'AV44TFEstado_UF',fld:'vTFESTADO_UF',pic:'@!',nv:''},{av:'Ddo_estado_uf_Filteredtext_set',ctrl:'DDO_ESTADO_UF',prop:'FilteredText_set'},{av:'AV45TFEstado_UF_Sel',fld:'vTFESTADO_UF_SEL',pic:'@!',nv:''},{av:'Ddo_estado_uf_Selectedvalue_set',ctrl:'DDO_ESTADO_UF',prop:'SelectedValue_set'},{av:'AV48TFUnidadeOrganizacional_Vinculada',fld:'vTFUNIDADEORGANIZACIONAL_VINCULADA',pic:'@!',nv:''},{av:'Ddo_unidadeorganizacional_vinculada_Filteredtext_set',ctrl:'DDO_UNIDADEORGANIZACIONAL_VINCULADA',prop:'FilteredText_set'},{av:'AV49TFUnidadeOrganizacional_Vinculada_Sel',fld:'vTFUNIDADEORGANIZACIONAL_VINCULADA_SEL',pic:'ZZZZZ9',nv:0},{av:'Ddo_unidadeorganizacional_vinculada_Selectedvalue_set',ctrl:'DDO_UNIDADEORGANIZACIONAL_VINCULADA',prop:'SelectedValue_set'},{av:'AV17DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV18UnidadeOrganizacional_Nome1',fld:'vUNIDADEORGANIZACIONAL_NOME1',pic:'@!',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'edtavUnidadeorganizacional_nome1_Visible',ctrl:'vUNIDADEORGANIZACIONAL_NOME1',prop:'Visible'},{av:'dynavEstado_uf1'},{av:'dynavUnidadeorganizacional_vinculada1'},{av:'AV21DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV23UnidadeOrganizacional_Nome2',fld:'vUNIDADEORGANIZACIONAL_NOME2',pic:'@!',nv:''},{av:'AV26DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV27DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV28UnidadeOrganizacional_Nome3',fld:'vUNIDADEORGANIZACIONAL_NOME3',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV19Estado_UF1',fld:'vESTADO_UF1',pic:'@!',nv:''},{av:'AV20UnidadeOrganizacional_Vinculada1',fld:'vUNIDADEORGANIZACIONAL_VINCULADA1',pic:'ZZZZZ9',nv:0},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV24Estado_UF2',fld:'vESTADO_UF2',pic:'@!',nv:''},{av:'AV25UnidadeOrganizacional_Vinculada2',fld:'vUNIDADEORGANIZACIONAL_VINCULADA2',pic:'ZZZZZ9',nv:0},{av:'AV29Estado_UF3',fld:'vESTADO_UF3',pic:'@!',nv:''},{av:'AV30UnidadeOrganizacional_Vinculada3',fld:'vUNIDADEORGANIZACIONAL_VINCULADA3',pic:'ZZZZZ9',nv:0},{av:'edtavUnidadeorganizacional_nome2_Visible',ctrl:'vUNIDADEORGANIZACIONAL_NOME2',prop:'Visible'},{av:'dynavEstado_uf2'},{av:'dynavUnidadeorganizacional_vinculada2'},{av:'edtavUnidadeorganizacional_nome3_Visible',ctrl:'vUNIDADEORGANIZACIONAL_NOME3',prop:'Visible'},{av:'dynavEstado_uf3'},{av:'dynavUnidadeorganizacional_vinculada3'}]}");
         setEventMetadata("'DOSELECTUO'","{handler:'E29D81',iparms:[{av:'AV56Codigo',fld:'vCODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV56Codigo',fld:'vCODIGO',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("'DOINSERT'","{handler:'E20D82',iparms:[{av:'A611UnidadeOrganizacional_Codigo',fld:'UNIDADEORGANIZACIONAL_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Gridpaginationbar_Selectedpage = "";
         Ddo_unidadeorganizacional_nome_Activeeventkey = "";
         Ddo_unidadeorganizacional_nome_Filteredtext_get = "";
         Ddo_unidadeorganizacional_nome_Selectedvalue_get = "";
         Ddo_estado_uf_Activeeventkey = "";
         Ddo_estado_uf_Filteredtext_get = "";
         Ddo_estado_uf_Selectedvalue_get = "";
         Ddo_unidadeorganizacional_vinculada_Activeeventkey = "";
         Ddo_unidadeorganizacional_vinculada_Filteredtext_get = "";
         Ddo_unidadeorganizacional_vinculada_Selectedvalue_get = "";
         Ddo_unidadeorganizacional_vinculada_Selectedtext_get = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         AV17DynamicFiltersSelector1 = "";
         AV18UnidadeOrganizacional_Nome1 = "";
         AV19Estado_UF1 = "";
         AV22DynamicFiltersSelector2 = "";
         AV23UnidadeOrganizacional_Nome2 = "";
         AV24Estado_UF2 = "";
         AV27DynamicFiltersSelector3 = "";
         AV28UnidadeOrganizacional_Nome3 = "";
         AV29Estado_UF3 = "";
         AV40TFUnidadeOrganizacional_Nome = "";
         AV41TFUnidadeOrganizacional_Nome_Sel = "";
         AV44TFEstado_UF = "";
         AV45TFEstado_UF_Sel = "";
         AV48TFUnidadeOrganizacional_Vinculada = "";
         AV42ddo_UnidadeOrganizacional_NomeTitleControlIdToReplace = "";
         AV46ddo_Estado_UFTitleControlIdToReplace = "";
         AV50ddo_UnidadeOrganizacional_VinculadaTitleControlIdToReplace = "";
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV62Pgmname = "";
         AV16UnidadeOrganizacional_Ativo = true;
         AV52TFUnidadeOrganizacional_Vinculada_SelDsc = "";
         AV11GridState = new wwpbaseobjects.SdtWWPGridState(context);
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV51DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV39UnidadeOrganizacional_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV43Estado_UFTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV47UnidadeOrganizacional_VinculadaTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         Ddo_unidadeorganizacional_nome_Filteredtext_set = "";
         Ddo_unidadeorganizacional_nome_Selectedvalue_set = "";
         Ddo_unidadeorganizacional_nome_Sortedstatus = "";
         Ddo_estado_uf_Filteredtext_set = "";
         Ddo_estado_uf_Selectedvalue_set = "";
         Ddo_estado_uf_Sortedstatus = "";
         Ddo_unidadeorganizacional_vinculada_Filteredtext_set = "";
         Ddo_unidadeorganizacional_vinculada_Selectedvalue_set = "";
         Ddo_unidadeorganizacional_vinculada_Selectedtext_set = "";
         Ddo_unidadeorganizacional_vinculada_Sortedstatus = "";
         GX_FocusControl = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV33Update = "";
         AV59Update_GXI = "";
         AV34Delete = "";
         AV60Delete_GXI = "";
         AV35Display = "";
         AV61Display_GXI = "";
         A612UnidadeOrganizacional_Nome = "";
         A23Estado_UF = "";
         GXCCtl = "";
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         scmdbuf = "";
         H00D82_A23Estado_UF = new String[] {""} ;
         H00D82_A24Estado_Nome = new String[] {""} ;
         H00D83_A613UnidadeOrganizacional_Vinculada = new int[1] ;
         H00D83_n613UnidadeOrganizacional_Vinculada = new bool[] {false} ;
         H00D83_A612UnidadeOrganizacional_Nome = new String[] {""} ;
         H00D83_A611UnidadeOrganizacional_Codigo = new int[1] ;
         H00D83_A629UnidadeOrganizacional_Ativo = new bool[] {false} ;
         H00D84_A23Estado_UF = new String[] {""} ;
         H00D84_A24Estado_Nome = new String[] {""} ;
         H00D85_A613UnidadeOrganizacional_Vinculada = new int[1] ;
         H00D85_n613UnidadeOrganizacional_Vinculada = new bool[] {false} ;
         H00D85_A612UnidadeOrganizacional_Nome = new String[] {""} ;
         H00D85_A611UnidadeOrganizacional_Codigo = new int[1] ;
         H00D85_A629UnidadeOrganizacional_Ativo = new bool[] {false} ;
         H00D86_A23Estado_UF = new String[] {""} ;
         H00D86_A24Estado_Nome = new String[] {""} ;
         H00D87_A613UnidadeOrganizacional_Vinculada = new int[1] ;
         H00D87_n613UnidadeOrganizacional_Vinculada = new bool[] {false} ;
         H00D87_A612UnidadeOrganizacional_Nome = new String[] {""} ;
         H00D87_A611UnidadeOrganizacional_Codigo = new int[1] ;
         H00D87_A629UnidadeOrganizacional_Ativo = new bool[] {false} ;
         H00D88_A611UnidadeOrganizacional_Codigo = new int[1] ;
         H00D88_A612UnidadeOrganizacional_Nome = new String[] {""} ;
         H00D88_A629UnidadeOrganizacional_Ativo = new bool[] {false} ;
         GridContainer = new GXWebGrid( context);
         lV18UnidadeOrganizacional_Nome1 = "";
         lV23UnidadeOrganizacional_Nome2 = "";
         lV28UnidadeOrganizacional_Nome3 = "";
         lV40TFUnidadeOrganizacional_Nome = "";
         lV44TFEstado_UF = "";
         lV48TFUnidadeOrganizacional_Vinculada = "";
         H00D89_A629UnidadeOrganizacional_Ativo = new bool[] {false} ;
         H00D89_A609TpUo_Codigo = new int[1] ;
         H00D89_A613UnidadeOrganizacional_Vinculada = new int[1] ;
         H00D89_n613UnidadeOrganizacional_Vinculada = new bool[] {false} ;
         H00D89_A23Estado_UF = new String[] {""} ;
         H00D89_A612UnidadeOrganizacional_Nome = new String[] {""} ;
         H00D89_A611UnidadeOrganizacional_Codigo = new int[1] ;
         H00D810_AGRID_nRecordCount = new long[1] ;
         imgAdddynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters1_Jsonclick = "";
         imgAdddynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters3_Jsonclick = "";
         imgCleanfilters_Jsonclick = "";
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         GridRow = new GXWebRow();
         imgInsert_Link = "";
         AV36Session = context.GetSession();
         AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV13GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8HTTPRequest = new GxHttpRequest( context);
         AV10TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblOrderedtext_Jsonclick = "";
         lblFiltertextunidadeorganizacional_ativo_Jsonclick = "";
         lblJsdynamicfilters_Jsonclick = "";
         lblDynamicfiltersprefix1_Jsonclick = "";
         lblDynamicfiltersmiddle1_Jsonclick = "";
         lblDynamicfiltersprefix2_Jsonclick = "";
         lblDynamicfiltersmiddle2_Jsonclick = "";
         lblDynamicfiltersprefix3_Jsonclick = "";
         lblDynamicfiltersmiddle3_Jsonclick = "";
         bttBtnselectuo_Jsonclick = "";
         imgInsert_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlAV7TpUo_Codigo = "";
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.geral_tp_uogeral_unidadeorganizacionalwc__default(),
            new Object[][] {
                new Object[] {
               H00D82_A23Estado_UF, H00D82_A24Estado_Nome
               }
               , new Object[] {
               H00D83_A613UnidadeOrganizacional_Vinculada, H00D83_n613UnidadeOrganizacional_Vinculada, H00D83_A612UnidadeOrganizacional_Nome, H00D83_A611UnidadeOrganizacional_Codigo, H00D83_A629UnidadeOrganizacional_Ativo
               }
               , new Object[] {
               H00D84_A23Estado_UF, H00D84_A24Estado_Nome
               }
               , new Object[] {
               H00D85_A613UnidadeOrganizacional_Vinculada, H00D85_n613UnidadeOrganizacional_Vinculada, H00D85_A612UnidadeOrganizacional_Nome, H00D85_A611UnidadeOrganizacional_Codigo, H00D85_A629UnidadeOrganizacional_Ativo
               }
               , new Object[] {
               H00D86_A23Estado_UF, H00D86_A24Estado_Nome
               }
               , new Object[] {
               H00D87_A613UnidadeOrganizacional_Vinculada, H00D87_n613UnidadeOrganizacional_Vinculada, H00D87_A612UnidadeOrganizacional_Nome, H00D87_A611UnidadeOrganizacional_Codigo, H00D87_A629UnidadeOrganizacional_Ativo
               }
               , new Object[] {
               H00D88_A611UnidadeOrganizacional_Codigo, H00D88_A612UnidadeOrganizacional_Nome, H00D88_A629UnidadeOrganizacional_Ativo
               }
               , new Object[] {
               H00D89_A629UnidadeOrganizacional_Ativo, H00D89_A609TpUo_Codigo, H00D89_A613UnidadeOrganizacional_Vinculada, H00D89_n613UnidadeOrganizacional_Vinculada, H00D89_A23Estado_UF, H00D89_A612UnidadeOrganizacional_Nome, H00D89_A611UnidadeOrganizacional_Codigo
               }
               , new Object[] {
               H00D810_AGRID_nRecordCount
               }
            }
         );
         AV62Pgmname = "Geral_Tp_UOGeral_UnidadeOrganizacionalWC";
         /* GeneXus formulas. */
         AV62Pgmname = "Geral_Tp_UOGeral_UnidadeOrganizacionalWC";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short nRC_GXsfl_83 ;
      private short nGXsfl_83_idx=1 ;
      private short AV14OrderedBy ;
      private short initialized ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDraw ;
      private short nDoneStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_83_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short edtUnidadeOrganizacional_Nome_Titleformat ;
      private short edtEstado_UF_Titleformat ;
      private short dynUnidadeOrganizacional_Vinculada_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int AV7TpUo_Codigo ;
      private int wcpOAV7TpUo_Codigo ;
      private int AV64Unidadeorganizacional_codigo ;
      private int subGrid_Rows ;
      private int AV20UnidadeOrganizacional_Vinculada1 ;
      private int AV25UnidadeOrganizacional_Vinculada2 ;
      private int AV30UnidadeOrganizacional_Vinculada3 ;
      private int AV49TFUnidadeOrganizacional_Vinculada_Sel ;
      private int A611UnidadeOrganizacional_Codigo ;
      private int AV56Codigo ;
      private int Gridpaginationbar_Pagestoshow ;
      private int Ddo_unidadeorganizacional_nome_Datalistupdateminimumcharacters ;
      private int Ddo_estado_uf_Datalistupdateminimumcharacters ;
      private int Ddo_unidadeorganizacional_vinculada_Datalistupdateminimumcharacters ;
      private int A609TpUo_Codigo ;
      private int edtTpUo_Codigo_Visible ;
      private int edtavTfunidadeorganizacional_nome_Visible ;
      private int edtavTfunidadeorganizacional_nome_sel_Visible ;
      private int edtavTfestado_uf_Visible ;
      private int edtavTfestado_uf_sel_Visible ;
      private int edtavTfunidadeorganizacional_vinculada_Visible ;
      private int edtavTfunidadeorganizacional_vinculada_sel_Visible ;
      private int edtavTfunidadeorganizacional_vinculada_seldsc_Visible ;
      private int edtavDdo_unidadeorganizacional_nometitlecontrolidtoreplace_Visible ;
      private int edtavDdo_estado_uftitlecontrolidtoreplace_Visible ;
      private int edtavDdo_unidadeorganizacional_vinculadatitlecontrolidtoreplace_Visible ;
      private int A613UnidadeOrganizacional_Vinculada ;
      private int gxdynajaxindex ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int edtavOrdereddsc_Visible ;
      private int imgInsert_Visible ;
      private int edtavUpdate_Visible ;
      private int edtavDelete_Visible ;
      private int edtavDisplay_Visible ;
      private int AV53PageToGo ;
      private int edtavUpdate_Enabled ;
      private int edtavDelete_Enabled ;
      private int imgAdddynamicfilters1_Visible ;
      private int imgRemovedynamicfilters1_Visible ;
      private int imgAdddynamicfilters2_Visible ;
      private int imgRemovedynamicfilters2_Visible ;
      private int imgInsert_Enabled ;
      private int edtavUnidadeorganizacional_nome1_Visible ;
      private int edtavUnidadeorganizacional_nome2_Visible ;
      private int edtavUnidadeorganizacional_nome3_Visible ;
      private int AV63GXV1 ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private long GRID_nFirstRecordOnPage ;
      private long AV54GridCurrentPage ;
      private long AV55GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private String Gridpaginationbar_Selectedpage ;
      private String Ddo_unidadeorganizacional_nome_Activeeventkey ;
      private String Ddo_unidadeorganizacional_nome_Filteredtext_get ;
      private String Ddo_unidadeorganizacional_nome_Selectedvalue_get ;
      private String Ddo_estado_uf_Activeeventkey ;
      private String Ddo_estado_uf_Filteredtext_get ;
      private String Ddo_estado_uf_Selectedvalue_get ;
      private String Ddo_unidadeorganizacional_vinculada_Activeeventkey ;
      private String Ddo_unidadeorganizacional_vinculada_Filteredtext_get ;
      private String Ddo_unidadeorganizacional_vinculada_Selectedvalue_get ;
      private String Ddo_unidadeorganizacional_vinculada_Selectedtext_get ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String sGXsfl_83_idx="0001" ;
      private String AV18UnidadeOrganizacional_Nome1 ;
      private String AV19Estado_UF1 ;
      private String AV23UnidadeOrganizacional_Nome2 ;
      private String AV24Estado_UF2 ;
      private String AV28UnidadeOrganizacional_Nome3 ;
      private String AV29Estado_UF3 ;
      private String AV40TFUnidadeOrganizacional_Nome ;
      private String AV41TFUnidadeOrganizacional_Nome_Sel ;
      private String AV44TFEstado_UF ;
      private String AV45TFEstado_UF_Sel ;
      private String AV48TFUnidadeOrganizacional_Vinculada ;
      private String AV62Pgmname ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Ddo_unidadeorganizacional_nome_Caption ;
      private String Ddo_unidadeorganizacional_nome_Tooltip ;
      private String Ddo_unidadeorganizacional_nome_Cls ;
      private String Ddo_unidadeorganizacional_nome_Filteredtext_set ;
      private String Ddo_unidadeorganizacional_nome_Selectedvalue_set ;
      private String Ddo_unidadeorganizacional_nome_Dropdownoptionstype ;
      private String Ddo_unidadeorganizacional_nome_Titlecontrolidtoreplace ;
      private String Ddo_unidadeorganizacional_nome_Sortedstatus ;
      private String Ddo_unidadeorganizacional_nome_Filtertype ;
      private String Ddo_unidadeorganizacional_nome_Datalisttype ;
      private String Ddo_unidadeorganizacional_nome_Datalistproc ;
      private String Ddo_unidadeorganizacional_nome_Sortasc ;
      private String Ddo_unidadeorganizacional_nome_Sortdsc ;
      private String Ddo_unidadeorganizacional_nome_Loadingdata ;
      private String Ddo_unidadeorganizacional_nome_Cleanfilter ;
      private String Ddo_unidadeorganizacional_nome_Noresultsfound ;
      private String Ddo_unidadeorganizacional_nome_Searchbuttontext ;
      private String Ddo_estado_uf_Caption ;
      private String Ddo_estado_uf_Tooltip ;
      private String Ddo_estado_uf_Cls ;
      private String Ddo_estado_uf_Filteredtext_set ;
      private String Ddo_estado_uf_Selectedvalue_set ;
      private String Ddo_estado_uf_Dropdownoptionstype ;
      private String Ddo_estado_uf_Titlecontrolidtoreplace ;
      private String Ddo_estado_uf_Sortedstatus ;
      private String Ddo_estado_uf_Filtertype ;
      private String Ddo_estado_uf_Datalisttype ;
      private String Ddo_estado_uf_Datalistproc ;
      private String Ddo_estado_uf_Sortasc ;
      private String Ddo_estado_uf_Sortdsc ;
      private String Ddo_estado_uf_Loadingdata ;
      private String Ddo_estado_uf_Cleanfilter ;
      private String Ddo_estado_uf_Noresultsfound ;
      private String Ddo_estado_uf_Searchbuttontext ;
      private String Ddo_unidadeorganizacional_vinculada_Caption ;
      private String Ddo_unidadeorganizacional_vinculada_Tooltip ;
      private String Ddo_unidadeorganizacional_vinculada_Cls ;
      private String Ddo_unidadeorganizacional_vinculada_Filteredtext_set ;
      private String Ddo_unidadeorganizacional_vinculada_Selectedvalue_set ;
      private String Ddo_unidadeorganizacional_vinculada_Selectedtext_set ;
      private String Ddo_unidadeorganizacional_vinculada_Dropdownoptionstype ;
      private String Ddo_unidadeorganizacional_vinculada_Titlecontrolidtoreplace ;
      private String Ddo_unidadeorganizacional_vinculada_Sortedstatus ;
      private String Ddo_unidadeorganizacional_vinculada_Filtertype ;
      private String Ddo_unidadeorganizacional_vinculada_Datalisttype ;
      private String Ddo_unidadeorganizacional_vinculada_Datalistproc ;
      private String Ddo_unidadeorganizacional_vinculada_Sortasc ;
      private String Ddo_unidadeorganizacional_vinculada_Sortdsc ;
      private String Ddo_unidadeorganizacional_vinculada_Loadingdata ;
      private String Ddo_unidadeorganizacional_vinculada_Cleanfilter ;
      private String Ddo_unidadeorganizacional_vinculada_Noresultsfound ;
      private String Ddo_unidadeorganizacional_vinculada_Searchbuttontext ;
      private String GX_FocusControl ;
      private String edtTpUo_Codigo_Internalname ;
      private String edtTpUo_Codigo_Jsonclick ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String chkavDynamicfiltersenabled2_Internalname ;
      private String chkavDynamicfiltersenabled3_Internalname ;
      private String edtavTfunidadeorganizacional_nome_Internalname ;
      private String edtavTfunidadeorganizacional_nome_Jsonclick ;
      private String edtavTfunidadeorganizacional_nome_sel_Internalname ;
      private String edtavTfunidadeorganizacional_nome_sel_Jsonclick ;
      private String edtavTfestado_uf_Internalname ;
      private String edtavTfestado_uf_Jsonclick ;
      private String edtavTfestado_uf_sel_Internalname ;
      private String edtavTfestado_uf_sel_Jsonclick ;
      private String edtavTfunidadeorganizacional_vinculada_Internalname ;
      private String edtavTfunidadeorganizacional_vinculada_Jsonclick ;
      private String edtavTfunidadeorganizacional_vinculada_sel_Internalname ;
      private String edtavTfunidadeorganizacional_vinculada_sel_Jsonclick ;
      private String edtavTfunidadeorganizacional_vinculada_seldsc_Internalname ;
      private String edtavTfunidadeorganizacional_vinculada_seldsc_Jsonclick ;
      private String edtavDdo_unidadeorganizacional_nometitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_estado_uftitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_unidadeorganizacional_vinculadatitlecontrolidtoreplace_Internalname ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String cmbavOrderedby_Internalname ;
      private String edtavUpdate_Internalname ;
      private String edtavDelete_Internalname ;
      private String edtavDisplay_Internalname ;
      private String edtUnidadeOrganizacional_Codigo_Internalname ;
      private String A612UnidadeOrganizacional_Nome ;
      private String edtUnidadeOrganizacional_Nome_Internalname ;
      private String A23Estado_UF ;
      private String edtEstado_UF_Internalname ;
      private String dynUnidadeOrganizacional_Vinculada_Internalname ;
      private String chkavUnidadeorganizacional_ativo_Internalname ;
      private String GXCCtl ;
      private String gxwrpcisep ;
      private String scmdbuf ;
      private String lV18UnidadeOrganizacional_Nome1 ;
      private String lV23UnidadeOrganizacional_Nome2 ;
      private String lV28UnidadeOrganizacional_Nome3 ;
      private String lV40TFUnidadeOrganizacional_Nome ;
      private String lV44TFEstado_UF ;
      private String lV48TFUnidadeOrganizacional_Vinculada ;
      private String edtavOrdereddsc_Internalname ;
      private String cmbavDynamicfiltersselector1_Internalname ;
      private String edtavUnidadeorganizacional_nome1_Internalname ;
      private String dynavEstado_uf1_Internalname ;
      private String dynavUnidadeorganizacional_vinculada1_Internalname ;
      private String cmbavDynamicfiltersselector2_Internalname ;
      private String edtavUnidadeorganizacional_nome2_Internalname ;
      private String dynavEstado_uf2_Internalname ;
      private String dynavUnidadeorganizacional_vinculada2_Internalname ;
      private String cmbavDynamicfiltersselector3_Internalname ;
      private String edtavUnidadeorganizacional_nome3_Internalname ;
      private String dynavEstado_uf3_Internalname ;
      private String dynavUnidadeorganizacional_vinculada3_Internalname ;
      private String lblJsdynamicfilters_Caption ;
      private String lblJsdynamicfilters_Internalname ;
      private String imgAdddynamicfilters1_Jsonclick ;
      private String imgAdddynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters1_Jsonclick ;
      private String imgRemovedynamicfilters1_Internalname ;
      private String imgAdddynamicfilters2_Jsonclick ;
      private String imgAdddynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters2_Jsonclick ;
      private String imgRemovedynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters3_Jsonclick ;
      private String imgRemovedynamicfilters3_Internalname ;
      private String subGrid_Internalname ;
      private String Ddo_unidadeorganizacional_nome_Internalname ;
      private String Ddo_estado_uf_Internalname ;
      private String Ddo_unidadeorganizacional_vinculada_Internalname ;
      private String imgCleanfilters_Jsonclick ;
      private String imgCleanfilters_Internalname ;
      private String edtUnidadeOrganizacional_Nome_Title ;
      private String edtEstado_UF_Title ;
      private String imgInsert_Internalname ;
      private String edtavUpdate_Tooltiptext ;
      private String edtavUpdate_Link ;
      private String edtavDelete_Tooltiptext ;
      private String edtavDelete_Link ;
      private String edtavDisplay_Tooltiptext ;
      private String edtavDisplay_Link ;
      private String edtUnidadeOrganizacional_Nome_Link ;
      private String imgInsert_Link ;
      private String sStyleString ;
      private String tblTablegridheader_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTablesearch_Internalname ;
      private String lblOrderedtext_Internalname ;
      private String lblOrderedtext_Jsonclick ;
      private String cmbavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Jsonclick ;
      private String tblTablefilters_Internalname ;
      private String lblFiltertextunidadeorganizacional_ativo_Internalname ;
      private String lblFiltertextunidadeorganizacional_ativo_Jsonclick ;
      private String lblJsdynamicfilters_Jsonclick ;
      private String tblTabledynamicfilters_Internalname ;
      private String lblDynamicfiltersprefix1_Internalname ;
      private String lblDynamicfiltersprefix1_Jsonclick ;
      private String cmbavDynamicfiltersselector1_Jsonclick ;
      private String lblDynamicfiltersmiddle1_Internalname ;
      private String lblDynamicfiltersmiddle1_Jsonclick ;
      private String edtavUnidadeorganizacional_nome1_Jsonclick ;
      private String dynavEstado_uf1_Jsonclick ;
      private String dynavUnidadeorganizacional_vinculada1_Jsonclick ;
      private String lblDynamicfiltersprefix2_Internalname ;
      private String lblDynamicfiltersprefix2_Jsonclick ;
      private String cmbavDynamicfiltersselector2_Jsonclick ;
      private String lblDynamicfiltersmiddle2_Internalname ;
      private String lblDynamicfiltersmiddle2_Jsonclick ;
      private String edtavUnidadeorganizacional_nome2_Jsonclick ;
      private String dynavEstado_uf2_Jsonclick ;
      private String dynavUnidadeorganizacional_vinculada2_Jsonclick ;
      private String lblDynamicfiltersprefix3_Internalname ;
      private String lblDynamicfiltersprefix3_Jsonclick ;
      private String cmbavDynamicfiltersselector3_Jsonclick ;
      private String lblDynamicfiltersmiddle3_Internalname ;
      private String lblDynamicfiltersmiddle3_Jsonclick ;
      private String edtavUnidadeorganizacional_nome3_Jsonclick ;
      private String dynavEstado_uf3_Jsonclick ;
      private String dynavUnidadeorganizacional_vinculada3_Jsonclick ;
      private String tblUnnamedtable1_Internalname ;
      private String bttBtnselectuo_Internalname ;
      private String bttBtnselectuo_Jsonclick ;
      private String tblTableactions_Internalname ;
      private String imgInsert_Jsonclick ;
      private String sCtrlAV7TpUo_Codigo ;
      private String sGXsfl_83_fel_idx="0001" ;
      private String ROClassString ;
      private String edtUnidadeOrganizacional_Codigo_Jsonclick ;
      private String edtUnidadeOrganizacional_Nome_Jsonclick ;
      private String edtEstado_UF_Jsonclick ;
      private String dynUnidadeOrganizacional_Vinculada_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private bool entryPointCalled ;
      private bool AV15OrderedDsc ;
      private bool AV21DynamicFiltersEnabled2 ;
      private bool AV26DynamicFiltersEnabled3 ;
      private bool AV16UnidadeOrganizacional_Ativo ;
      private bool AV32DynamicFiltersIgnoreFirst ;
      private bool AV31DynamicFiltersRemoving ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool Ddo_unidadeorganizacional_nome_Includesortasc ;
      private bool Ddo_unidadeorganizacional_nome_Includesortdsc ;
      private bool Ddo_unidadeorganizacional_nome_Includefilter ;
      private bool Ddo_unidadeorganizacional_nome_Filterisrange ;
      private bool Ddo_unidadeorganizacional_nome_Includedatalist ;
      private bool Ddo_estado_uf_Includesortasc ;
      private bool Ddo_estado_uf_Includesortdsc ;
      private bool Ddo_estado_uf_Includefilter ;
      private bool Ddo_estado_uf_Filterisrange ;
      private bool Ddo_estado_uf_Includedatalist ;
      private bool Ddo_unidadeorganizacional_vinculada_Includesortasc ;
      private bool Ddo_unidadeorganizacional_vinculada_Includesortdsc ;
      private bool Ddo_unidadeorganizacional_vinculada_Includefilter ;
      private bool Ddo_unidadeorganizacional_vinculada_Filterisrange ;
      private bool Ddo_unidadeorganizacional_vinculada_Includedatalist ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n613UnidadeOrganizacional_Vinculada ;
      private bool A629UnidadeOrganizacional_Ativo ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV33Update_IsBlob ;
      private bool AV34Delete_IsBlob ;
      private bool AV35Display_IsBlob ;
      private String AV17DynamicFiltersSelector1 ;
      private String AV22DynamicFiltersSelector2 ;
      private String AV27DynamicFiltersSelector3 ;
      private String AV42ddo_UnidadeOrganizacional_NomeTitleControlIdToReplace ;
      private String AV46ddo_Estado_UFTitleControlIdToReplace ;
      private String AV50ddo_UnidadeOrganizacional_VinculadaTitleControlIdToReplace ;
      private String AV52TFUnidadeOrganizacional_Vinculada_SelDsc ;
      private String AV59Update_GXI ;
      private String AV60Delete_GXI ;
      private String AV61Display_GXI ;
      private String AV33Update ;
      private String AV34Delete ;
      private String AV35Display ;
      private String imgInsert_Bitmap ;
      private IGxSession AV36Session ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbavOrderedby ;
      private GXCheckbox chkavUnidadeorganizacional_ativo ;
      private GXCombobox cmbavDynamicfiltersselector1 ;
      private GXCombobox dynavEstado_uf1 ;
      private GXCombobox dynavUnidadeorganizacional_vinculada1 ;
      private GXCombobox cmbavDynamicfiltersselector2 ;
      private GXCombobox dynavEstado_uf2 ;
      private GXCombobox dynavUnidadeorganizacional_vinculada2 ;
      private GXCombobox cmbavDynamicfiltersselector3 ;
      private GXCombobox dynavEstado_uf3 ;
      private GXCombobox dynavUnidadeorganizacional_vinculada3 ;
      private GXCombobox dynUnidadeOrganizacional_Vinculada ;
      private GXCheckbox chkavDynamicfiltersenabled2 ;
      private GXCheckbox chkavDynamicfiltersenabled3 ;
      private IDataStoreProvider pr_default ;
      private String[] H00D82_A23Estado_UF ;
      private String[] H00D82_A24Estado_Nome ;
      private int[] H00D83_A613UnidadeOrganizacional_Vinculada ;
      private bool[] H00D83_n613UnidadeOrganizacional_Vinculada ;
      private String[] H00D83_A612UnidadeOrganizacional_Nome ;
      private int[] H00D83_A611UnidadeOrganizacional_Codigo ;
      private bool[] H00D83_A629UnidadeOrganizacional_Ativo ;
      private String[] H00D84_A23Estado_UF ;
      private String[] H00D84_A24Estado_Nome ;
      private int[] H00D85_A613UnidadeOrganizacional_Vinculada ;
      private bool[] H00D85_n613UnidadeOrganizacional_Vinculada ;
      private String[] H00D85_A612UnidadeOrganizacional_Nome ;
      private int[] H00D85_A611UnidadeOrganizacional_Codigo ;
      private bool[] H00D85_A629UnidadeOrganizacional_Ativo ;
      private String[] H00D86_A23Estado_UF ;
      private String[] H00D86_A24Estado_Nome ;
      private int[] H00D87_A613UnidadeOrganizacional_Vinculada ;
      private bool[] H00D87_n613UnidadeOrganizacional_Vinculada ;
      private String[] H00D87_A612UnidadeOrganizacional_Nome ;
      private int[] H00D87_A611UnidadeOrganizacional_Codigo ;
      private bool[] H00D87_A629UnidadeOrganizacional_Ativo ;
      private int[] H00D88_A611UnidadeOrganizacional_Codigo ;
      private String[] H00D88_A612UnidadeOrganizacional_Nome ;
      private bool[] H00D88_A629UnidadeOrganizacional_Ativo ;
      private bool[] H00D89_A629UnidadeOrganizacional_Ativo ;
      private int[] H00D89_A609TpUo_Codigo ;
      private int[] H00D89_A613UnidadeOrganizacional_Vinculada ;
      private bool[] H00D89_n613UnidadeOrganizacional_Vinculada ;
      private String[] H00D89_A23Estado_UF ;
      private String[] H00D89_A612UnidadeOrganizacional_Nome ;
      private int[] H00D89_A611UnidadeOrganizacional_Codigo ;
      private long[] H00D810_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV8HTTPRequest ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV39UnidadeOrganizacional_NomeTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV43Estado_UFTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV47UnidadeOrganizacional_VinculadaTitleFilterData ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV10TrnContextAtt ;
      private wwpbaseobjects.SdtWWPGridState AV11GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV12GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV13GridStateDynamicFilter ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV51DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 ;
   }

   public class geral_tp_uogeral_unidadeorganizacionalwc__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00D89( IGxContext context ,
                                             String AV17DynamicFiltersSelector1 ,
                                             String AV18UnidadeOrganizacional_Nome1 ,
                                             String AV19Estado_UF1 ,
                                             int AV20UnidadeOrganizacional_Vinculada1 ,
                                             bool AV21DynamicFiltersEnabled2 ,
                                             String AV22DynamicFiltersSelector2 ,
                                             String AV23UnidadeOrganizacional_Nome2 ,
                                             String AV24Estado_UF2 ,
                                             int AV25UnidadeOrganizacional_Vinculada2 ,
                                             bool AV26DynamicFiltersEnabled3 ,
                                             String AV27DynamicFiltersSelector3 ,
                                             String AV28UnidadeOrganizacional_Nome3 ,
                                             String AV29Estado_UF3 ,
                                             int AV30UnidadeOrganizacional_Vinculada3 ,
                                             String AV41TFUnidadeOrganizacional_Nome_Sel ,
                                             String AV40TFUnidadeOrganizacional_Nome ,
                                             String AV45TFEstado_UF_Sel ,
                                             String AV44TFEstado_UF ,
                                             int AV49TFUnidadeOrganizacional_Vinculada_Sel ,
                                             String AV48TFUnidadeOrganizacional_Vinculada ,
                                             String A612UnidadeOrganizacional_Nome ,
                                             String A23Estado_UF ,
                                             int A613UnidadeOrganizacional_Vinculada ,
                                             short AV14OrderedBy ,
                                             bool AV15OrderedDsc ,
                                             int A609TpUo_Codigo ,
                                             int AV7TpUo_Codigo ,
                                             bool A629UnidadeOrganizacional_Ativo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [21] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " [UnidadeOrganizacional_Ativo], [TpUo_Codigo], [UnidadeOrganizacional_Vinculada], [Estado_UF], [UnidadeOrganizacional_Nome], [UnidadeOrganizacional_Codigo]";
         sFromString = " FROM [Geral_UnidadeOrganizacional] WITH (NOLOCK)";
         sOrderString = "";
         sWhereString = sWhereString + " WHERE ([TpUo_Codigo] = @AV7TpUo_Codigo)";
         sWhereString = sWhereString + " and ([UnidadeOrganizacional_Ativo] = 1)";
         if ( ( StringUtil.StrCmp(AV17DynamicFiltersSelector1, "UNIDADEORGANIZACIONAL_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV18UnidadeOrganizacional_Nome1)) ) )
         {
            sWhereString = sWhereString + " and ([UnidadeOrganizacional_Nome] like '%' + @lV18UnidadeOrganizacional_Nome1 + '%')";
         }
         else
         {
            GXv_int2[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV17DynamicFiltersSelector1, "ESTADO_UF") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV19Estado_UF1)) ) )
         {
            sWhereString = sWhereString + " and ([Estado_UF] = @AV19Estado_UF1)";
         }
         else
         {
            GXv_int2[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV17DynamicFiltersSelector1, "UNIDADEORGANIZACIONAL_VINCULADA") == 0 ) && ( ! (0==AV20UnidadeOrganizacional_Vinculada1) ) )
         {
            sWhereString = sWhereString + " and ([UnidadeOrganizacional_Vinculada] = @AV20UnidadeOrganizacional_Vinculada1)";
         }
         else
         {
            GXv_int2[3] = 1;
         }
         if ( AV21DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV22DynamicFiltersSelector2, "UNIDADEORGANIZACIONAL_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV23UnidadeOrganizacional_Nome2)) ) )
         {
            sWhereString = sWhereString + " and ([UnidadeOrganizacional_Nome] like '%' + @lV23UnidadeOrganizacional_Nome2 + '%')";
         }
         else
         {
            GXv_int2[4] = 1;
         }
         if ( AV21DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV22DynamicFiltersSelector2, "ESTADO_UF") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV24Estado_UF2)) ) )
         {
            sWhereString = sWhereString + " and ([Estado_UF] = @AV24Estado_UF2)";
         }
         else
         {
            GXv_int2[5] = 1;
         }
         if ( AV21DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV22DynamicFiltersSelector2, "UNIDADEORGANIZACIONAL_VINCULADA") == 0 ) && ( ! (0==AV25UnidadeOrganizacional_Vinculada2) ) )
         {
            sWhereString = sWhereString + " and ([UnidadeOrganizacional_Vinculada] = @AV25UnidadeOrganizacional_Vinculada2)";
         }
         else
         {
            GXv_int2[6] = 1;
         }
         if ( AV26DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV27DynamicFiltersSelector3, "UNIDADEORGANIZACIONAL_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV28UnidadeOrganizacional_Nome3)) ) )
         {
            sWhereString = sWhereString + " and ([UnidadeOrganizacional_Nome] like '%' + @lV28UnidadeOrganizacional_Nome3 + '%')";
         }
         else
         {
            GXv_int2[7] = 1;
         }
         if ( AV26DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV27DynamicFiltersSelector3, "ESTADO_UF") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV29Estado_UF3)) ) )
         {
            sWhereString = sWhereString + " and ([Estado_UF] = @AV29Estado_UF3)";
         }
         else
         {
            GXv_int2[8] = 1;
         }
         if ( AV26DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV27DynamicFiltersSelector3, "UNIDADEORGANIZACIONAL_VINCULADA") == 0 ) && ( ! (0==AV30UnidadeOrganizacional_Vinculada3) ) )
         {
            sWhereString = sWhereString + " and ([UnidadeOrganizacional_Vinculada] = @AV30UnidadeOrganizacional_Vinculada3)";
         }
         else
         {
            GXv_int2[9] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV41TFUnidadeOrganizacional_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV40TFUnidadeOrganizacional_Nome)) ) )
         {
            sWhereString = sWhereString + " and ([UnidadeOrganizacional_Nome] like @lV40TFUnidadeOrganizacional_Nome)";
         }
         else
         {
            GXv_int2[10] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV41TFUnidadeOrganizacional_Nome_Sel)) )
         {
            sWhereString = sWhereString + " and ([UnidadeOrganizacional_Nome] = @AV41TFUnidadeOrganizacional_Nome_Sel)";
         }
         else
         {
            GXv_int2[11] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV45TFEstado_UF_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV44TFEstado_UF)) ) )
         {
            sWhereString = sWhereString + " and ([Estado_UF] like @lV44TFEstado_UF)";
         }
         else
         {
            GXv_int2[12] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV45TFEstado_UF_Sel)) )
         {
            sWhereString = sWhereString + " and ([Estado_UF] = @AV45TFEstado_UF_Sel)";
         }
         else
         {
            GXv_int2[13] = 1;
         }
         if ( (0==AV49TFUnidadeOrganizacional_Vinculada_Sel) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV48TFUnidadeOrganizacional_Vinculada)) ) )
         {
            sWhereString = sWhereString + " and ([UnidadeOrganizacional_Nome] like @lV48TFUnidadeOrganizacional_Vinculada)";
         }
         else
         {
            GXv_int2[14] = 1;
         }
         if ( ! (0==AV49TFUnidadeOrganizacional_Vinculada_Sel) )
         {
            sWhereString = sWhereString + " and ([UnidadeOrganizacional_Vinculada] = @AV49TFUnidadeOrganizacional_Vinculada_Sel)";
         }
         else
         {
            GXv_int2[15] = 1;
         }
         if ( ( AV14OrderedBy == 1 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [TpUo_Codigo], [UnidadeOrganizacional_Nome]";
         }
         else if ( ( AV14OrderedBy == 1 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [TpUo_Codigo] DESC, [UnidadeOrganizacional_Nome] DESC";
         }
         else if ( ( AV14OrderedBy == 2 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [TpUo_Codigo], [Estado_UF]";
         }
         else if ( ( AV14OrderedBy == 2 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [TpUo_Codigo] DESC, [Estado_UF] DESC";
         }
         else if ( ( AV14OrderedBy == 3 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [TpUo_Codigo], [UnidadeOrganizacional_Vinculada]";
         }
         else if ( ( AV14OrderedBy == 3 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [TpUo_Codigo] DESC, [UnidadeOrganizacional_Vinculada] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY [UnidadeOrganizacional_Codigo]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      protected Object[] conditional_H00D810( IGxContext context ,
                                              String AV17DynamicFiltersSelector1 ,
                                              String AV18UnidadeOrganizacional_Nome1 ,
                                              String AV19Estado_UF1 ,
                                              int AV20UnidadeOrganizacional_Vinculada1 ,
                                              bool AV21DynamicFiltersEnabled2 ,
                                              String AV22DynamicFiltersSelector2 ,
                                              String AV23UnidadeOrganizacional_Nome2 ,
                                              String AV24Estado_UF2 ,
                                              int AV25UnidadeOrganizacional_Vinculada2 ,
                                              bool AV26DynamicFiltersEnabled3 ,
                                              String AV27DynamicFiltersSelector3 ,
                                              String AV28UnidadeOrganizacional_Nome3 ,
                                              String AV29Estado_UF3 ,
                                              int AV30UnidadeOrganizacional_Vinculada3 ,
                                              String AV41TFUnidadeOrganizacional_Nome_Sel ,
                                              String AV40TFUnidadeOrganizacional_Nome ,
                                              String AV45TFEstado_UF_Sel ,
                                              String AV44TFEstado_UF ,
                                              int AV49TFUnidadeOrganizacional_Vinculada_Sel ,
                                              String AV48TFUnidadeOrganizacional_Vinculada ,
                                              String A612UnidadeOrganizacional_Nome ,
                                              String A23Estado_UF ,
                                              int A613UnidadeOrganizacional_Vinculada ,
                                              short AV14OrderedBy ,
                                              bool AV15OrderedDsc ,
                                              int A609TpUo_Codigo ,
                                              int AV7TpUo_Codigo ,
                                              bool A629UnidadeOrganizacional_Ativo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [16] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM [Geral_UnidadeOrganizacional] WITH (NOLOCK)";
         scmdbuf = scmdbuf + " WHERE ([TpUo_Codigo] = @AV7TpUo_Codigo)";
         scmdbuf = scmdbuf + " and ([UnidadeOrganizacional_Ativo] = 1)";
         if ( ( StringUtil.StrCmp(AV17DynamicFiltersSelector1, "UNIDADEORGANIZACIONAL_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV18UnidadeOrganizacional_Nome1)) ) )
         {
            sWhereString = sWhereString + " and ([UnidadeOrganizacional_Nome] like '%' + @lV18UnidadeOrganizacional_Nome1 + '%')";
         }
         else
         {
            GXv_int4[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV17DynamicFiltersSelector1, "ESTADO_UF") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV19Estado_UF1)) ) )
         {
            sWhereString = sWhereString + " and ([Estado_UF] = @AV19Estado_UF1)";
         }
         else
         {
            GXv_int4[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV17DynamicFiltersSelector1, "UNIDADEORGANIZACIONAL_VINCULADA") == 0 ) && ( ! (0==AV20UnidadeOrganizacional_Vinculada1) ) )
         {
            sWhereString = sWhereString + " and ([UnidadeOrganizacional_Vinculada] = @AV20UnidadeOrganizacional_Vinculada1)";
         }
         else
         {
            GXv_int4[3] = 1;
         }
         if ( AV21DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV22DynamicFiltersSelector2, "UNIDADEORGANIZACIONAL_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV23UnidadeOrganizacional_Nome2)) ) )
         {
            sWhereString = sWhereString + " and ([UnidadeOrganizacional_Nome] like '%' + @lV23UnidadeOrganizacional_Nome2 + '%')";
         }
         else
         {
            GXv_int4[4] = 1;
         }
         if ( AV21DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV22DynamicFiltersSelector2, "ESTADO_UF") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV24Estado_UF2)) ) )
         {
            sWhereString = sWhereString + " and ([Estado_UF] = @AV24Estado_UF2)";
         }
         else
         {
            GXv_int4[5] = 1;
         }
         if ( AV21DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV22DynamicFiltersSelector2, "UNIDADEORGANIZACIONAL_VINCULADA") == 0 ) && ( ! (0==AV25UnidadeOrganizacional_Vinculada2) ) )
         {
            sWhereString = sWhereString + " and ([UnidadeOrganizacional_Vinculada] = @AV25UnidadeOrganizacional_Vinculada2)";
         }
         else
         {
            GXv_int4[6] = 1;
         }
         if ( AV26DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV27DynamicFiltersSelector3, "UNIDADEORGANIZACIONAL_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV28UnidadeOrganizacional_Nome3)) ) )
         {
            sWhereString = sWhereString + " and ([UnidadeOrganizacional_Nome] like '%' + @lV28UnidadeOrganizacional_Nome3 + '%')";
         }
         else
         {
            GXv_int4[7] = 1;
         }
         if ( AV26DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV27DynamicFiltersSelector3, "ESTADO_UF") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV29Estado_UF3)) ) )
         {
            sWhereString = sWhereString + " and ([Estado_UF] = @AV29Estado_UF3)";
         }
         else
         {
            GXv_int4[8] = 1;
         }
         if ( AV26DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV27DynamicFiltersSelector3, "UNIDADEORGANIZACIONAL_VINCULADA") == 0 ) && ( ! (0==AV30UnidadeOrganizacional_Vinculada3) ) )
         {
            sWhereString = sWhereString + " and ([UnidadeOrganizacional_Vinculada] = @AV30UnidadeOrganizacional_Vinculada3)";
         }
         else
         {
            GXv_int4[9] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV41TFUnidadeOrganizacional_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV40TFUnidadeOrganizacional_Nome)) ) )
         {
            sWhereString = sWhereString + " and ([UnidadeOrganizacional_Nome] like @lV40TFUnidadeOrganizacional_Nome)";
         }
         else
         {
            GXv_int4[10] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV41TFUnidadeOrganizacional_Nome_Sel)) )
         {
            sWhereString = sWhereString + " and ([UnidadeOrganizacional_Nome] = @AV41TFUnidadeOrganizacional_Nome_Sel)";
         }
         else
         {
            GXv_int4[11] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV45TFEstado_UF_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV44TFEstado_UF)) ) )
         {
            sWhereString = sWhereString + " and ([Estado_UF] like @lV44TFEstado_UF)";
         }
         else
         {
            GXv_int4[12] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV45TFEstado_UF_Sel)) )
         {
            sWhereString = sWhereString + " and ([Estado_UF] = @AV45TFEstado_UF_Sel)";
         }
         else
         {
            GXv_int4[13] = 1;
         }
         if ( (0==AV49TFUnidadeOrganizacional_Vinculada_Sel) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV48TFUnidadeOrganizacional_Vinculada)) ) )
         {
            sWhereString = sWhereString + " and ([UnidadeOrganizacional_Nome] like @lV48TFUnidadeOrganizacional_Vinculada)";
         }
         else
         {
            GXv_int4[14] = 1;
         }
         if ( ! (0==AV49TFUnidadeOrganizacional_Vinculada_Sel) )
         {
            sWhereString = sWhereString + " and ([UnidadeOrganizacional_Vinculada] = @AV49TFUnidadeOrganizacional_Vinculada_Sel)";
         }
         else
         {
            GXv_int4[15] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         if ( ( AV14OrderedBy == 1 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 1 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 2 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 2 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 3 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 3 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 7 :
                     return conditional_H00D89(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (String)dynConstraints[2] , (int)dynConstraints[3] , (bool)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (int)dynConstraints[8] , (bool)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (int)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (int)dynConstraints[18] , (String)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (int)dynConstraints[22] , (short)dynConstraints[23] , (bool)dynConstraints[24] , (int)dynConstraints[25] , (int)dynConstraints[26] , (bool)dynConstraints[27] );
               case 8 :
                     return conditional_H00D810(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (String)dynConstraints[2] , (int)dynConstraints[3] , (bool)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (int)dynConstraints[8] , (bool)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (int)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (int)dynConstraints[18] , (String)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (int)dynConstraints[22] , (short)dynConstraints[23] , (bool)dynConstraints[24] , (int)dynConstraints[25] , (int)dynConstraints[26] , (bool)dynConstraints[27] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00D82 ;
          prmH00D82 = new Object[] {
          } ;
          Object[] prmH00D83 ;
          prmH00D83 = new Object[] {
          new Object[] {"@AV64Unidadeorganizacional_codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00D84 ;
          prmH00D84 = new Object[] {
          } ;
          Object[] prmH00D85 ;
          prmH00D85 = new Object[] {
          new Object[] {"@AV64Unidadeorganizacional_codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00D86 ;
          prmH00D86 = new Object[] {
          } ;
          Object[] prmH00D87 ;
          prmH00D87 = new Object[] {
          new Object[] {"@AV64Unidadeorganizacional_codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00D88 ;
          prmH00D88 = new Object[] {
          new Object[] {"@AV64Unidadeorganizacional_codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00D89 ;
          prmH00D89 = new Object[] {
          new Object[] {"@AV7TpUo_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV18UnidadeOrganizacional_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@AV19Estado_UF1",SqlDbType.Char,2,0} ,
          new Object[] {"@AV20UnidadeOrganizacional_Vinculada1",SqlDbType.Int,6,0} ,
          new Object[] {"@lV23UnidadeOrganizacional_Nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@AV24Estado_UF2",SqlDbType.Char,2,0} ,
          new Object[] {"@AV25UnidadeOrganizacional_Vinculada2",SqlDbType.Int,6,0} ,
          new Object[] {"@lV28UnidadeOrganizacional_Nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@AV29Estado_UF3",SqlDbType.Char,2,0} ,
          new Object[] {"@AV30UnidadeOrganizacional_Vinculada3",SqlDbType.Int,6,0} ,
          new Object[] {"@lV40TFUnidadeOrganizacional_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV41TFUnidadeOrganizacional_Nome_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV44TFEstado_UF",SqlDbType.Char,2,0} ,
          new Object[] {"@AV45TFEstado_UF_Sel",SqlDbType.Char,2,0} ,
          new Object[] {"@lV48TFUnidadeOrganizacional_Vinculada",SqlDbType.Char,50,0} ,
          new Object[] {"@AV49TFUnidadeOrganizacional_Vinculada_Sel",SqlDbType.Int,6,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH00D810 ;
          prmH00D810 = new Object[] {
          new Object[] {"@AV7TpUo_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV18UnidadeOrganizacional_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@AV19Estado_UF1",SqlDbType.Char,2,0} ,
          new Object[] {"@AV20UnidadeOrganizacional_Vinculada1",SqlDbType.Int,6,0} ,
          new Object[] {"@lV23UnidadeOrganizacional_Nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@AV24Estado_UF2",SqlDbType.Char,2,0} ,
          new Object[] {"@AV25UnidadeOrganizacional_Vinculada2",SqlDbType.Int,6,0} ,
          new Object[] {"@lV28UnidadeOrganizacional_Nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@AV29Estado_UF3",SqlDbType.Char,2,0} ,
          new Object[] {"@AV30UnidadeOrganizacional_Vinculada3",SqlDbType.Int,6,0} ,
          new Object[] {"@lV40TFUnidadeOrganizacional_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV41TFUnidadeOrganizacional_Nome_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV44TFEstado_UF",SqlDbType.Char,2,0} ,
          new Object[] {"@AV45TFEstado_UF_Sel",SqlDbType.Char,2,0} ,
          new Object[] {"@lV48TFUnidadeOrganizacional_Vinculada",SqlDbType.Char,50,0} ,
          new Object[] {"@AV49TFUnidadeOrganizacional_Vinculada_Sel",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00D82", "SELECT [Estado_UF], [Estado_Nome] FROM [Estado] WITH (NOLOCK) ORDER BY [Estado_Nome] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00D82,0,0,true,false )
             ,new CursorDef("H00D83", "SELECT [UnidadeOrganizacional_Vinculada], [UnidadeOrganizacional_Nome], [UnidadeOrganizacional_Codigo], [UnidadeOrganizacional_Ativo] FROM [Geral_UnidadeOrganizacional] WITH (NOLOCK) WHERE ([UnidadeOrganizacional_Codigo] <> @AV64Unidadeorganizacional_codigo) AND ([UnidadeOrganizacional_Ativo] = 1) ORDER BY [UnidadeOrganizacional_Nome] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00D83,0,0,true,false )
             ,new CursorDef("H00D84", "SELECT [Estado_UF], [Estado_Nome] FROM [Estado] WITH (NOLOCK) ORDER BY [Estado_Nome] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00D84,0,0,true,false )
             ,new CursorDef("H00D85", "SELECT [UnidadeOrganizacional_Vinculada], [UnidadeOrganizacional_Nome], [UnidadeOrganizacional_Codigo], [UnidadeOrganizacional_Ativo] FROM [Geral_UnidadeOrganizacional] WITH (NOLOCK) WHERE ([UnidadeOrganizacional_Codigo] <> @AV64Unidadeorganizacional_codigo) AND ([UnidadeOrganizacional_Ativo] = 1) ORDER BY [UnidadeOrganizacional_Nome] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00D85,0,0,true,false )
             ,new CursorDef("H00D86", "SELECT [Estado_UF], [Estado_Nome] FROM [Estado] WITH (NOLOCK) ORDER BY [Estado_Nome] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00D86,0,0,true,false )
             ,new CursorDef("H00D87", "SELECT [UnidadeOrganizacional_Vinculada], [UnidadeOrganizacional_Nome], [UnidadeOrganizacional_Codigo], [UnidadeOrganizacional_Ativo] FROM [Geral_UnidadeOrganizacional] WITH (NOLOCK) WHERE ([UnidadeOrganizacional_Codigo] <> @AV64Unidadeorganizacional_codigo) AND ([UnidadeOrganizacional_Ativo] = 1) ORDER BY [UnidadeOrganizacional_Nome] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00D87,0,0,true,false )
             ,new CursorDef("H00D88", "SELECT [UnidadeOrganizacional_Codigo], [UnidadeOrganizacional_Nome], [UnidadeOrganizacional_Ativo] FROM [Geral_UnidadeOrganizacional] WITH (NOLOCK) WHERE ([UnidadeOrganizacional_Codigo] <> @AV64Unidadeorganizacional_codigo) AND ([UnidadeOrganizacional_Ativo] = 1) ORDER BY [UnidadeOrganizacional_Nome] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00D88,0,0,true,false )
             ,new CursorDef("H00D89", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00D89,11,0,true,false )
             ,new CursorDef("H00D810", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00D810,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getString(1, 2) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 50) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.getBool(4) ;
                return;
             case 2 :
                ((String[]) buf[0])[0] = rslt.getString(1, 2) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 50) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.getBool(4) ;
                return;
             case 4 :
                ((String[]) buf[0])[0] = rslt.getString(1, 2) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 50) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.getBool(4) ;
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((bool[]) buf[2])[0] = rslt.getBool(3) ;
                return;
             case 7 :
                ((bool[]) buf[0])[0] = rslt.getBool(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((String[]) buf[4])[0] = rslt.getString(4, 2) ;
                ((String[]) buf[5])[0] = rslt.getString(5, 50) ;
                ((int[]) buf[6])[0] = rslt.getInt(6) ;
                return;
             case 8 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 7 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[21]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[24]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[27]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[30]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[36]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[37]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[38]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[39]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[40]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[41]);
                }
                return;
             case 8 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[16]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[19]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[22]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[25]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[31]);
                }
                return;
       }
    }

 }

}
