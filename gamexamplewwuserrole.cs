/*
               File: GAMExampleWWUserRole
        Description: User`s roles
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 23:11:46.94
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class gamexamplewwuserrole : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public gamexamplewwuserrole( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("GeneXusXEv2");
      }

      public gamexamplewwuserrole( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref String aP0_UserId )
      {
         this.AV23UserId = aP0_UserId;
         executePrivate();
         aP0_UserId=this.AV23UserId;
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         chkavDisplayinheritroles = new GXCheckbox();
         cmbavRolesid = new GXCombobox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("GeneXusXEv2");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Gridww") == 0 )
            {
               nRC_GXsfl_36 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_36_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_36_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGridww_newrow( ) ;
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               AV23UserId = gxfirstwebparm;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23UserId", AV23UserId);
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("gammasterpagepopup", "GeneXus.Programs.gammasterpagepopup", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PA242( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            START242( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?202042823114918");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         FormProcess = ((nGXWrapped==0) ? " data-HasEnter=\"false\" data-Skiponenter=\"false\"" : "");
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         if ( nGXWrapped != 1 )
         {
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("gamexamplewwuserrole.aspx") + "?" + UrlEncode(StringUtil.RTrim(AV23UserId))+"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         }
         toggleJsOutput = isJsOutputEnabled( );
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_36", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_36), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vUSERID", StringUtil.RTrim( AV23UserId));
         GxWebStd.gx_boolean_hidden_field( context, "vEXISTS", AV12Exists);
         GxWebStd.gx_hidden_field( context, "GRIDWW_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDWW_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRIDWW_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRIDWW_nEOF), 1, 0, ",", "")));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( nGXWrapped != 1 )
         {
            context.WriteHtmlTextNl( "</form>") ;
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WE242( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVT242( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return false ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("gamexamplewwuserrole.aspx") + "?" + UrlEncode(StringUtil.RTrim(AV23UserId)) ;
      }

      public override String GetPgmname( )
      {
         return "GAMExampleWWUserRole" ;
      }

      public override String GetPgmdesc( )
      {
         return "User`s roles" ;
      }

      protected void WB240( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, "", "", "", "false");
            wb_table1_2_242( true) ;
         }
         else
         {
            wb_table1_2_242( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_242e( bool wbgen )
      {
         if ( wbgen )
         {
         }
         wbLoad = true;
      }

      protected void START242( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "User`s roles", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUP240( ) ;
      }

      protected void WS242( )
      {
         START242( ) ;
         EVT242( ) ;
      }

      protected void EVT242( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDNEW'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E11242 */
                              E11242 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDWWPAGING") == 0 )
                           {
                              context.wbHandled = 1;
                              sEvt = cgiGet( "GRIDWWPAGING");
                              if ( StringUtil.StrCmp(sEvt, "FIRST") == 0 )
                              {
                                 subgridww_firstpage( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "PREV") == 0 )
                              {
                                 subgridww_previouspage( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "NEXT") == 0 )
                              {
                                 subgridww_nextpage( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "LAST") == 0 )
                              {
                                 subgridww_lastpage( ) ;
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 11), "GRIDWW.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 13), "VBTNDLT.CLICK") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 18), "VBTNMAINROLE.CLICK") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 13), "VBTNDLT.CLICK") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 18), "VBTNMAINROLE.CLICK") == 0 ) )
                           {
                              nGXsfl_36_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_36_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_36_idx), 4, 0)), 4, "0");
                              SubsflControlProps_362( ) ;
                              if ( ( ( context.localUtil.CToN( cgiGet( edtavId_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavId_Internalname), ",", ".") > Convert.ToDecimal( 999999999999L )) ) )
                              {
                                 GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vID");
                                 GX_FocusControl = edtavId_Internalname;
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                                 wbErr = true;
                                 AV16Id = 0;
                              }
                              else
                              {
                                 AV16Id = (long)(context.localUtil.CToN( cgiGet( edtavId_Internalname), ",", "."));
                              }
                              AV15GUID = cgiGet( edtavGuid_Internalname);
                              AV7BtnDlt = cgiGet( edtavBtndlt_Internalname);
                              AV8BtnMainRole = cgiGet( edtavBtnmainrole_Internalname);
                              AV18Name = cgiGet( edtavName_Internalname);
                              CheckSecurityRow2436( sGXsfl_36_idx) ;
                              if ( GxWebError != 0 )
                              {
                                 return  ;
                              }
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E12242 */
                                    E12242 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRIDWW.LOAD") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E13242 */
                                    E13242 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "VBTNDLT.CLICK") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E14242 */
                                    E14242 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "VBTNMAINROLE.CLICK") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E15242 */
                                    E15242 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WE242( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PA242( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            chkavDisplayinheritroles.Name = "vDISPLAYINHERITROLES";
            chkavDisplayinheritroles.WebTags = "";
            chkavDisplayinheritroles.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDisplayinheritroles_Internalname, "TitleCaption", chkavDisplayinheritroles.Caption);
            chkavDisplayinheritroles.CheckedValue = "false";
            cmbavRolesid.Name = "vROLESID";
            cmbavRolesid.WebTags = "";
            if ( cmbavRolesid.ItemCount > 0 )
            {
               AV22RolesId = (long)(NumberUtil.Val( cmbavRolesid.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV22RolesId), 12, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22RolesId", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22RolesId), 12, 0)));
            }
            if ( toggleJsOutput )
            {
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = edtavUsername_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGridww_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_362( ) ;
         while ( nGXsfl_36_idx <= nRC_GXsfl_36 )
         {
            sendrow_362( ) ;
            sendsecurityrow_362( ) ;
            nGXsfl_36_idx = (short)(((subGridww_Islastpage==1)&&(nGXsfl_36_idx+1>subGridww_Recordsperpage( )) ? 1 : nGXsfl_36_idx+1));
            sGXsfl_36_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_36_idx), 4, 0)), 4, "0");
            SubsflControlProps_362( ) ;
         }
         context.GX_webresponse.AddString(GridwwContainer.ToJavascriptSource());
         /* End function gxnrGridww_newrow */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavRolesid.ItemCount > 0 )
         {
            AV22RolesId = (long)(NumberUtil.Val( cmbavRolesid.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV22RolesId), 12, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22RolesId", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22RolesId), 12, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RF242( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
         edtavUsername_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUsername_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUsername_Enabled), 5, 0)));
         edtavId_Enabled = 0;
         edtavGuid_Enabled = 0;
         edtavBtnmainrole_Enabled = 0;
         edtavName_Enabled = 0;
      }

      protected void RF242( )
      {
         if ( isAjaxCallMode( ) )
         {
            GridwwContainer.ClearRows();
         }
         wbStart = 36;
         /* Execute user event: E12242 */
         E12242 ();
         nGXsfl_36_idx = 1;
         sGXsfl_36_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_36_idx), 4, 0)), 4, "0");
         SubsflControlProps_362( ) ;
         nGXsfl_36_Refreshing = 1;
         GridwwContainer.AddObjectProperty("GridName", "Gridww");
         GridwwContainer.AddObjectProperty("CmpContext", "");
         GridwwContainer.AddObjectProperty("InMasterPage", "false");
         GridwwContainer.AddObjectProperty("Class", "WorkWith");
         GridwwContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(1), 4, 0, ".", "")));
         GridwwContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(2), 4, 0, ".", "")));
         GridwwContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridww_Backcolorstyle), 1, 0, ".", "")));
         GridwwContainer.PageSize = subGridww_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_362( ) ;
            /* Execute user event: E13242 */
            E13242 ();
            if ( ( GRIDWW_nCurrentRecord > 0 ) && ( GRIDWW_nGridOutOfScope == 0 ) && ( nGXsfl_36_idx == 1 ) )
            {
               GRIDWW_nCurrentRecord = 0;
               GRIDWW_nGridOutOfScope = 1;
               subgridww_firstpage( ) ;
               /* Execute user event: E13242 */
               E13242 ();
            }
            wbEnd = 36;
            WB240( ) ;
         }
         nGXsfl_36_Refreshing = 0;
      }

      protected void CheckSecurityRow2436( String sGXChecksfl_idx )
      {
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected int subGridww_Pagecount( )
      {
         GRIDWW_nRecordCount = subGridww_Recordcount( );
         if ( ((int)((GRIDWW_nRecordCount) % (subGridww_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRIDWW_nRecordCount/ (decimal)(subGridww_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRIDWW_nRecordCount/ (decimal)(subGridww_Recordsperpage( ))))+1) ;
      }

      protected int subGridww_Recordcount( )
      {
         return (int)(-1) ;
      }

      protected int subGridww_Recordsperpage( )
      {
         return (int)(15*1) ;
      }

      protected int subGridww_Currentpage( )
      {
         return (int)(-1) ;
      }

      protected short subgridww_firstpage( )
      {
         GRIDWW_nFirstRecordOnPage = 0;
         return 0 ;
      }

      protected short subgridww_nextpage( )
      {
         if ( GRIDWW_nEOF == 0 )
         {
            GRIDWW_nFirstRecordOnPage = (long)(GRIDWW_nFirstRecordOnPage+subGridww_Recordsperpage( ));
         }
         return (short)(((GRIDWW_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgridww_previouspage( )
      {
         if ( GRIDWW_nFirstRecordOnPage >= subGridww_Recordsperpage( ) )
         {
            GRIDWW_nFirstRecordOnPage = (long)(GRIDWW_nFirstRecordOnPage-subGridww_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         return 0 ;
      }

      protected short subgridww_lastpage( )
      {
         subGridww_Islastpage = 1;
         return 0 ;
      }

      protected int subgridww_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRIDWW_nFirstRecordOnPage = (long)(subGridww_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRIDWW_nFirstRecordOnPage = 0;
         }
         return (int)(0) ;
      }

      protected void STRUP240( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         edtavUsername_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUsername_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUsername_Enabled), 5, 0)));
         edtavId_Enabled = 0;
         edtavGuid_Enabled = 0;
         edtavBtnmainrole_Enabled = 0;
         edtavName_Enabled = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            AV25UserName = cgiGet( edtavUsername_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25UserName", AV25UserName);
            AV9DisplayInheritRoles = StringUtil.StrToBool( cgiGet( chkavDisplayinheritroles_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9DisplayInheritRoles", AV9DisplayInheritRoles);
            cmbavRolesid.Name = cmbavRolesid_Internalname;
            cmbavRolesid.CurrentValue = cgiGet( cmbavRolesid_Internalname);
            AV22RolesId = (long)(NumberUtil.Val( cgiGet( cmbavRolesid_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22RolesId", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22RolesId), 12, 0)));
            /* Read saved values. */
            nRC_GXsfl_36 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_36"), ",", "."));
            GRIDWW_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRIDWW_nFirstRecordOnPage"), ",", "."));
            GRIDWW_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRIDWW_nEOF"), ",", "."));
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void E12242( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV5GAMUser.load( AV23UserId);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23UserId", AV23UserId);
         AV25UserName = AV5GAMUser.gxTpr_Firstname + AV5GAMUser.gxTpr_Lastname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25UserName", AV25UserName);
         AV26UserRoles = AV5GAMUser.getroles(out  AV11Errors);
         cmbavRolesid.removeAllItems();
         cmbavRolesid.addItem("0", "(Select a Role)", 0);
         AV31GXV2 = 1;
         AV30GXV1 = new SdtGAMRepository(context).getroles(AV21RoleFilter, out  AV11Errors);
         while ( AV31GXV2 <= AV30GXV1.Count )
         {
            AV13GAMRole = ((SdtGAMRole)AV30GXV1.Item(AV31GXV2));
            /* Execute user subroutine: 'EXISTSROLE' */
            S122 ();
            if (returnInSub) return;
            if ( ! AV12Exists )
            {
               cmbavRolesid.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(AV13GAMRole.gxTpr_Id), 12, 0)), AV13GAMRole.gxTpr_Name, 0);
            }
            AV31GXV2 = (int)(AV31GXV2+1);
         }
         if ( cmbavRolesid.ItemCount == 1 )
         {
            lblTbadd_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbadd_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTbadd_Visible), 5, 0)));
            bttBtnadd_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnadd_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnadd_Visible), 5, 0)));
            cmbavRolesid.Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavRolesid_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavRolesid.Visible), 5, 0)));
         }
         else
         {
            lblTbadd_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbadd_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTbadd_Visible), 5, 0)));
            bttBtnadd_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnadd_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnadd_Visible), 5, 0)));
            cmbavRolesid.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavRolesid_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavRolesid.Visible), 5, 0)));
         }
      }

      private void E13242( )
      {
         /* Gridww_Load Routine */
         if ( ! AV9DisplayInheritRoles )
         {
            AV33GXV4 = 1;
            AV32GXV3 = AV5GAMUser.getroles(out  AV11Errors);
            while ( AV33GXV4 <= AV32GXV3.Count )
            {
               AV13GAMRole = ((SdtGAMRole)AV32GXV3.Item(AV33GXV4));
               AV7BtnDlt = context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( ));
               AV34Btndlt_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )));
               edtavBtndlt_Visible = 1;
               if ( AV13GAMRole.gxTpr_Id == AV5GAMUser.gxTpr_Defaultroleid )
               {
                  AV8BtnMainRole = "";
               }
               else
               {
                  AV8BtnMainRole = "Set Main";
               }
               AV15GUID = AV20Role.gxTpr_Guid;
               AV16Id = AV13GAMRole.gxTpr_Id;
               AV18Name = AV13GAMRole.gxTpr_Name;
               /* Load Method */
               if ( wbStart != -1 )
               {
                  wbStart = 36;
               }
               if ( ( subGridww_Islastpage == 1 ) || ( 15 == 0 ) || ( ( GRIDWW_nCurrentRecord >= GRIDWW_nFirstRecordOnPage ) && ( GRIDWW_nCurrentRecord < GRIDWW_nFirstRecordOnPage + subGridww_Recordsperpage( ) ) ) )
               {
                  sendrow_362( ) ;
                  sendsecurityrow_362( ) ;
                  GRIDWW_nEOF = 1;
                  if ( ( subGridww_Islastpage == 1 ) && ( ((int)((GRIDWW_nCurrentRecord) % (subGridww_Recordsperpage( )))) == 0 ) )
                  {
                     GRIDWW_nFirstRecordOnPage = GRIDWW_nCurrentRecord;
                  }
               }
               if ( GRIDWW_nCurrentRecord >= GRIDWW_nFirstRecordOnPage + subGridww_Recordsperpage( ) )
               {
                  GRIDWW_nEOF = 0;
               }
               GRIDWW_nCurrentRecord = (long)(GRIDWW_nCurrentRecord+1);
               AV33GXV4 = (int)(AV33GXV4+1);
            }
         }
         else
         {
            AV14GAMRolesDirect = AV5GAMUser.getroles(out  AV11Errors);
            if ( AV14GAMRolesDirect.Count > 0 )
            {
               AV36GXV6 = 1;
               AV35GXV5 = AV5GAMUser.getallroles(out  AV11Errors);
               while ( AV36GXV6 <= AV35GXV5.Count )
               {
                  AV13GAMRole = ((SdtGAMRole)AV35GXV5.Item(AV36GXV6));
                  AV7BtnDlt = context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( ));
                  AV34Btndlt_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )));
                  AV17isDirectRole = false;
                  AV37GXV7 = 1;
                  while ( AV37GXV7 <= AV14GAMRolesDirect.Count )
                  {
                     AV27GAMRoleAux = ((SdtGAMRole)AV14GAMRolesDirect.Item(AV37GXV7));
                     if ( AV13GAMRole.gxTpr_Id == AV27GAMRoleAux.gxTpr_Id )
                     {
                        AV17isDirectRole = true;
                        if (true) break;
                     }
                     AV37GXV7 = (int)(AV37GXV7+1);
                  }
                  if ( AV17isDirectRole )
                  {
                     edtavBtndlt_Visible = 1;
                     if ( AV13GAMRole.gxTpr_Id == AV5GAMUser.gxTpr_Defaultroleid )
                     {
                        AV8BtnMainRole = "";
                     }
                     else
                     {
                        AV8BtnMainRole = "Set Main";
                     }
                  }
                  else
                  {
                     edtavBtndlt_Visible = 0;
                     AV8BtnMainRole = "";
                  }
                  AV15GUID = AV20Role.gxTpr_Guid;
                  AV16Id = AV13GAMRole.gxTpr_Id;
                  AV18Name = AV13GAMRole.gxTpr_Name;
                  /* Load Method */
                  if ( wbStart != -1 )
                  {
                     wbStart = 36;
                  }
                  if ( ( subGridww_Islastpage == 1 ) || ( 15 == 0 ) || ( ( GRIDWW_nCurrentRecord >= GRIDWW_nFirstRecordOnPage ) && ( GRIDWW_nCurrentRecord < GRIDWW_nFirstRecordOnPage + subGridww_Recordsperpage( ) ) ) )
                  {
                     sendrow_362( ) ;
                     sendsecurityrow_362( ) ;
                     GRIDWW_nEOF = 1;
                     if ( ( subGridww_Islastpage == 1 ) && ( ((int)((GRIDWW_nCurrentRecord) % (subGridww_Recordsperpage( )))) == 0 ) )
                     {
                        GRIDWW_nFirstRecordOnPage = GRIDWW_nCurrentRecord;
                     }
                  }
                  if ( GRIDWW_nCurrentRecord >= GRIDWW_nFirstRecordOnPage + subGridww_Recordsperpage( ) )
                  {
                     GRIDWW_nEOF = 0;
                  }
                  GRIDWW_nCurrentRecord = (long)(GRIDWW_nCurrentRecord+1);
                  AV36GXV6 = (int)(AV36GXV6+1);
               }
            }
         }
      }

      protected void E11242( )
      {
         /* 'AddNew' Routine */
         if ( ! (0==AV22RolesId) )
         {
            AV5GAMUser.load( AV23UserId);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23UserId", AV23UserId);
            AV6isOK = AV5GAMUser.addrolebyid(AV22RolesId, out  AV11Errors);
            if ( AV6isOK )
            {
               context.CommitDataStores( "GAMExampleWWUserRole");
            }
            else
            {
               /* Execute user subroutine: 'DISPLAYERRORS' */
               S112 ();
               if (returnInSub) return;
            }
         }
      }

      protected void E14242( )
      {
         /* Btndlt_Click Routine */
         AV5GAMUser.load( AV23UserId);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23UserId", AV23UserId);
         AV6isOK = AV5GAMUser.deleterolebyid(AV16Id, out  AV11Errors);
         if ( AV6isOK )
         {
            context.CommitDataStores( "GAMExampleWWUserRole");
         }
         else
         {
            /* Execute user subroutine: 'DISPLAYERRORS' */
            S112 ();
            if (returnInSub) return;
         }
      }

      protected void E15242( )
      {
         /* Btnmainrole_Click Routine */
         AV5GAMUser.load( AV23UserId);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23UserId", AV23UserId);
         AV6isOK = AV5GAMUser.setmainrolebyid(AV16Id, out  AV11Errors);
         if ( AV6isOK )
         {
            context.CommitDataStores( "GAMExampleWWUserRole");
         }
         else
         {
            /* Execute user subroutine: 'DISPLAYERRORS' */
            S112 ();
            if (returnInSub) return;
         }
      }

      protected void S122( )
      {
         /* 'EXISTSROLE' Routine */
         AV12Exists = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12Exists", AV12Exists);
         AV38GXV8 = 1;
         while ( AV38GXV8 <= AV26UserRoles.Count )
         {
            AV20Role = ((SdtGAMRole)AV26UserRoles.Item(AV38GXV8));
            /* Execute user subroutine: 'DISPLAYERRORS' */
            S112 ();
            if (returnInSub) return;
            if ( AV20Role.gxTpr_Id == AV13GAMRole.gxTpr_Id )
            {
               AV12Exists = true;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12Exists", AV12Exists);
               if (true) break;
            }
            AV38GXV8 = (int)(AV38GXV8+1);
         }
      }

      protected void S112( )
      {
         /* 'DISPLAYERRORS' Routine */
         if ( AV11Errors.Count > 0 )
         {
            AV39GXV9 = 1;
            while ( AV39GXV9 <= AV11Errors.Count )
            {
               AV10Error = ((SdtGAMError)AV11Errors.Item(AV39GXV9));
               GX_msglist.addItem(StringUtil.Format( "%1 (GAM%2)", AV10Error.gxTpr_Message, StringUtil.LTrim( StringUtil.Str( (decimal)(AV10Error.gxTpr_Code), 12, 0)), "", "", "", "", "", "", ""));
               AV39GXV9 = (int)(AV39GXV9+1);
            }
         }
      }

      protected void wb_table1_2_242( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " height: " + StringUtil.LTrim( StringUtil.Str( (decimal)(500), 10, 0)) + "px" + ";";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(350), 10, 0)) + "px" + ";";
            GxWebStd.gx_table_start( context, tblTblpage_Internalname, tblTblpage_Internalname, "", "Table", 0, "center", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:bottom;height:75px")+"\">") ;
            wb_table2_5_242( true) ;
         }
         else
         {
            wb_table2_5_242( false) ;
         }
         return  ;
      }

      protected void wb_table2_5_242e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center;vertical-align:top")+"\">") ;
            /*  Grid Control  */
            GridwwContainer.SetWrapped(nGXWrapped);
            if ( GridwwContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridwwContainer"+"DivS\" data-gxgridid=\"36\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGridww_Internalname, subGridww_Internalname, "", "WorkWith", 0, "", "", 1, 2, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGridww_Backcolorstyle == 0 )
               {
                  subGridww_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGridww_Class) > 0 )
                  {
                     subGridww_Linesclass = subGridww_Class+"Title";
                  }
               }
               else
               {
                  subGridww_Titlebackstyle = 1;
                  if ( subGridww_Backcolorstyle == 1 )
                  {
                     subGridww_Titlebackcolor = subGridww_Allbackcolor;
                     if ( StringUtil.Len( subGridww_Class) > 0 )
                     {
                        subGridww_Linesclass = subGridww_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGridww_Class) > 0 )
                     {
                        subGridww_Linesclass = subGridww_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGridww_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Id") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGridww_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "GUID") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGridww_Linesclass+"\" "+" style=\""+((edtavBtndlt_Visible==0) ? "display:none;" : "")+""+"\" "+">") ;
               context.SendWebValue( "Delete") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(60), 4, 0))+"px"+" class=\""+subGridww_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Main") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(350), 4, 0))+"px"+" class=\""+subGridww_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Role") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridwwContainer.AddObjectProperty("GridName", "Gridww");
            }
            else
            {
               GridwwContainer.AddObjectProperty("GridName", "Gridww");
               GridwwContainer.AddObjectProperty("Class", "WorkWith");
               GridwwContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(1), 4, 0, ".", "")));
               GridwwContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(2), 4, 0, ".", "")));
               GridwwContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridww_Backcolorstyle), 1, 0, ".", "")));
               GridwwContainer.AddObjectProperty("CmpContext", "");
               GridwwContainer.AddObjectProperty("InMasterPage", "false");
               GridwwColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridwwColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV16Id), 12, 0, ".", "")));
               GridwwColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavId_Enabled), 5, 0, ".", "")));
               GridwwContainer.AddColumnProperties(GridwwColumn);
               GridwwColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridwwColumn.AddObjectProperty("Value", StringUtil.RTrim( AV15GUID));
               GridwwColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavGuid_Enabled), 5, 0, ".", "")));
               GridwwContainer.AddColumnProperties(GridwwColumn);
               GridwwColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridwwColumn.AddObjectProperty("Value", context.convertURL( AV7BtnDlt));
               GridwwColumn.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavBtndlt_Visible), 5, 0, ".", "")));
               GridwwContainer.AddColumnProperties(GridwwColumn);
               GridwwColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridwwColumn.AddObjectProperty("Value", StringUtil.RTrim( AV8BtnMainRole));
               GridwwColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavBtnmainrole_Enabled), 5, 0, ".", "")));
               GridwwContainer.AddColumnProperties(GridwwColumn);
               GridwwColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridwwColumn.AddObjectProperty("Value", StringUtil.RTrim( AV18Name));
               GridwwColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavName_Enabled), 5, 0, ".", "")));
               GridwwContainer.AddColumnProperties(GridwwColumn);
               GridwwContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridww_Allowselection), 1, 0, ".", "")));
               GridwwContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridww_Selectioncolor), 9, 0, ".", "")));
               GridwwContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridww_Allowhovering), 1, 0, ".", "")));
               GridwwContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridww_Hoveringcolor), 9, 0, ".", "")));
               GridwwContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridww_Allowcollapsing), 1, 0, ".", "")));
               GridwwContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridww_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 36 )
         {
            wbEnd = 0;
            nRC_GXsfl_36 = (short)(nGXsfl_36_idx-1);
            if ( GridwwContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               GridwwContainer.AddObjectProperty("GRIDWW_nEOF", GRIDWW_nEOF);
               GridwwContainer.AddObjectProperty("GRIDWW_nFirstRecordOnPage", GRIDWW_nFirstRecordOnPage);
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridwwContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Gridww", GridwwContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridwwContainerData", GridwwContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridwwContainerData"+"V", GridwwContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridwwContainerData"+"V"+"\" value='"+GridwwContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_242e( true) ;
         }
         else
         {
            wb_table1_2_242e( false) ;
         }
      }

      protected void wb_table2_5_242( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblTbladd_Internalname, tblTbladd_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td data-align=\"right\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-right;text-align:-moz-right;text-align:-webkit-right;height:5px;width:30%")+"\">") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"2\" >") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td data-align=\"right\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-right;text-align:-moz-right;text-align:-webkit-right")+"\">") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbuserlogin_Internalname, "User", "", "", lblTbuserlogin_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_GAMExampleWWUserRole.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"2\" >") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 13,'',false,'" + sGXsfl_36_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavUsername_Internalname, StringUtil.RTrim( AV25UserName), StringUtil.RTrim( context.localUtil.Format( AV25UserName, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,13);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavUsername_Jsonclick, 0, "Attribute", "", "", "", 1, edtavUsername_Enabled, 0, "text", "", 80, "chr", 1, "row", 120, 0, 0, 0, 1, -1, -1, true, "GAMDescriptionMedium", "left", true, "HLP_GAMExampleWWUserRole.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "height:15px")+"\">") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"2\" >") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbdspinhroles_Internalname, "Display inherited roles", "", "", lblTbdspinhroles_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_GAMExampleWWUserRole.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "height:5px")+"\">") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'" + sGXsfl_36_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDisplayinheritroles_Internalname, StringUtil.BoolToStr( AV9DisplayInheritRoles), "", "", 1, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(21, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,21);\"");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnsearch_Internalname, "gx.evt.setGridEvt("+StringUtil.Str( (decimal)(36), 2, 0)+","+"null"+");", "Search", bttBtnsearch_Jsonclick, 7, "Search", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"e16241_client"+"'", TempTags, "", 2, "HLP_GAMExampleWWUserRole.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "height:5px")+"\">") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"2\" >") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td data-align=\"right\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-right;text-align:-moz-right;text-align:-webkit-right;height:22px")+"\">") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbadd_Internalname, "Roles to add", "", "", lblTbadd_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", lblTbadd_Visible, 1, 0, "HLP_GAMExampleWWUserRole.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 31,'',false,'" + sGXsfl_36_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavRolesid, cmbavRolesid_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV22RolesId), 12, 0)), 1, cmbavRolesid_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavRolesid.Visible, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,31);\"", "", true, "HLP_GAMExampleWWUserRole.htm");
            cmbavRolesid.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV22RolesId), 12, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavRolesid_Internalname, "Values", (String)(cmbavRolesid.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "width:90%")+"\">") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 33,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnadd_Internalname, "gx.evt.setGridEvt("+StringUtil.Str( (decimal)(36), 2, 0)+","+"null"+");", "Add", bttBtnadd_Jsonclick, 5, "Add", "", StyleString, ClassString, bttBtnadd_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"E\\'ADDNEW\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_GAMExampleWWUserRole.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_242e( true) ;
         }
         else
         {
            wb_table2_5_242e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV23UserId = (String)getParm(obj,0);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23UserId", AV23UserId);
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("GeneXusXEv2");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA242( ) ;
         WS242( ) ;
         WE242( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?2249785");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?202042823115122");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         if ( nGXWrapped != 1 )
         {
            context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
            context.AddJavascriptSource("gamexamplewwuserrole.js", "?202042823115123");
         }
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_362( )
      {
         edtavId_Internalname = "vID_"+sGXsfl_36_idx;
         edtavGuid_Internalname = "vGUID_"+sGXsfl_36_idx;
         edtavBtndlt_Internalname = "vBTNDLT_"+sGXsfl_36_idx;
         edtavBtnmainrole_Internalname = "vBTNMAINROLE_"+sGXsfl_36_idx;
         edtavName_Internalname = "vNAME_"+sGXsfl_36_idx;
      }

      protected void SubsflControlProps_fel_362( )
      {
         edtavId_Internalname = "vID_"+sGXsfl_36_fel_idx;
         edtavGuid_Internalname = "vGUID_"+sGXsfl_36_fel_idx;
         edtavBtndlt_Internalname = "vBTNDLT_"+sGXsfl_36_fel_idx;
         edtavBtnmainrole_Internalname = "vBTNMAINROLE_"+sGXsfl_36_fel_idx;
         edtavName_Internalname = "vNAME_"+sGXsfl_36_fel_idx;
      }

      protected void sendrow_362( )
      {
         SubsflControlProps_362( ) ;
         WB240( ) ;
         if ( ( 15 * 1 == 0 ) || ( nGXsfl_36_idx <= subGridww_Recordsperpage( ) * 1 ) )
         {
            GridwwRow = GXWebRow.GetNew(context,GridwwContainer);
            if ( subGridww_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGridww_Backstyle = 0;
               if ( StringUtil.StrCmp(subGridww_Class, "") != 0 )
               {
                  subGridww_Linesclass = subGridww_Class+"Odd";
               }
            }
            else if ( subGridww_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGridww_Backstyle = 0;
               subGridww_Backcolor = subGridww_Allbackcolor;
               if ( StringUtil.StrCmp(subGridww_Class, "") != 0 )
               {
                  subGridww_Linesclass = subGridww_Class+"Uniform";
               }
            }
            else if ( subGridww_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGridww_Backstyle = 1;
               if ( StringUtil.StrCmp(subGridww_Class, "") != 0 )
               {
                  subGridww_Linesclass = subGridww_Class+"Odd";
               }
               subGridww_Backcolor = (int)(0x0);
            }
            else if ( subGridww_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGridww_Backstyle = 1;
               if ( ((int)((nGXsfl_36_idx) % (2))) == 0 )
               {
                  subGridww_Backcolor = (int)(0x0);
                  if ( StringUtil.StrCmp(subGridww_Class, "") != 0 )
                  {
                     subGridww_Linesclass = subGridww_Class+"Even";
                  }
               }
               else
               {
                  subGridww_Backcolor = (int)(0x0);
                  if ( StringUtil.StrCmp(subGridww_Class, "") != 0 )
                  {
                     subGridww_Linesclass = subGridww_Class+"Odd";
                  }
               }
            }
            if ( GridwwContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGridww_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_36_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridwwContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            TempTags = " " + ((edtavId_Enabled!=0)&&(edtavId_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 37,'',false,'"+sGXsfl_36_idx+"',36)\"" : " ");
            ROClassString = "Attribute";
            GridwwRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavId_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(AV16Id), 12, 0, ",", "")),((edtavId_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV16Id), "ZZZZZZZZZZZ9")) : context.localUtil.Format( (decimal)(AV16Id), "ZZZZZZZZZZZ9")),TempTags+((edtavId_Enabled!=0)&&(edtavId_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavId_Enabled!=0)&&(edtavId_Visible!=0) ? " onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,37);\"" : " "),(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavId_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)0,(int)edtavId_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)12,(short)0,(short)0,(short)36,(short)1,(short)-1,(short)0,(bool)true,(String)"GAMKeyNumLong",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridwwContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            TempTags = " " + ((edtavGuid_Enabled!=0)&&(edtavGuid_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 38,'',false,'"+sGXsfl_36_idx+"',36)\"" : " ");
            ROClassString = "Attribute";
            GridwwRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavGuid_Internalname,StringUtil.RTrim( AV15GUID),(String)"",TempTags+((edtavGuid_Enabled!=0)&&(edtavGuid_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavGuid_Enabled!=0)&&(edtavGuid_Visible!=0) ? " onblur=\""+""+";gx.evt.onblur(this,38);\"" : " "),(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavGuid_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)0,(int)edtavGuid_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)40,(short)0,(short)0,(short)36,(short)1,(short)-1,(short)0,(bool)true,(String)"GAMGUID",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridwwContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+((edtavBtndlt_Visible==0) ? "display:none;" : "")+"\">") ;
            }
            /* Active Bitmap Variable */
            TempTags = " " + ((edtavBtndlt_Enabled!=0)&&(edtavBtndlt_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 39,'',false,'',36)\"" : " ");
            ClassString = "Image";
            StyleString = "";
            AV7BtnDlt_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV7BtnDlt))&&String.IsNullOrEmpty(StringUtil.RTrim( AV34Btndlt_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV7BtnDlt)));
            GridwwRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavBtndlt_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV7BtnDlt)) ? AV34Btndlt_GXI : context.PathToRelativeUrl( AV7BtnDlt)),(String)"",(String)"",(String)"",context.GetTheme( ),(int)edtavBtndlt_Visible,(short)1,(String)"",(String)"",(short)0,(short)-1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)5,(String)edtavBtndlt_Jsonclick,"'"+""+"'"+",false,"+"'"+"EVBTNDLT.CLICK."+sGXsfl_36_idx+"'",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)""+TempTags,(String)"",(String)"",(short)1,(bool)AV7BtnDlt_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridwwContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            TempTags = " " + ((edtavBtnmainrole_Enabled!=0)&&(edtavBtnmainrole_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 40,'',false,'"+sGXsfl_36_idx+"',36)\"" : " ");
            ROClassString = "Attribute";
            GridwwRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavBtnmainrole_Internalname,StringUtil.RTrim( AV8BtnMainRole),(String)"",TempTags+((edtavBtnmainrole_Enabled!=0)&&(edtavBtnmainrole_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavBtnmainrole_Enabled!=0)&&(edtavBtnmainrole_Visible!=0) ? " onblur=\""+""+";gx.evt.onblur(this,40);\"" : " "),"'"+""+"'"+",false,"+"'"+"EVBTNMAINROLE.CLICK."+sGXsfl_36_idx+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavBtnmainrole_Jsonclick,(short)5,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavBtnmainrole_Enabled,(short)0,(String)"text",(String)"",(short)60,(String)"px",(short)17,(String)"px",(short)10,(short)0,(short)0,(short)36,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridwwContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            TempTags = " " + ((edtavName_Enabled!=0)&&(edtavName_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 41,'',false,'"+sGXsfl_36_idx+"',36)\"" : " ");
            ROClassString = "Attribute";
            GridwwRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavName_Internalname,StringUtil.RTrim( AV18Name),(String)"",TempTags+((edtavName_Enabled!=0)&&(edtavName_Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((edtavName_Enabled!=0)&&(edtavName_Visible!=0) ? " onblur=\""+""+";gx.evt.onblur(this,41);\"" : " "),(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavName_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavName_Enabled,(short)0,(String)"text",(String)"",(short)350,(String)"px",(short)17,(String)"px",(short)120,(short)0,(short)0,(short)36,(short)1,(short)-1,(short)-1,(bool)true,(String)"GAMDescriptionMedium",(String)"left",(bool)true});
            GridwwContainer.AddRow(GridwwRow);
            sendsecurityrow_362( ) ;
            nGXsfl_36_idx = (short)(((subGridww_Islastpage==1)&&(nGXsfl_36_idx+1>subGridww_Recordsperpage( )) ? 1 : nGXsfl_36_idx+1));
            sGXsfl_36_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_36_idx), 4, 0)), 4, "0");
            SubsflControlProps_362( ) ;
         }
         /* End function sendrow_362 */
      }

      protected void sendsecurityrow_362( )
      {
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         /* End function sendsecurityrow_362 */
      }

      protected void init_default_properties( )
      {
         lblTbuserlogin_Internalname = "TBUSERLOGIN";
         edtavUsername_Internalname = "vUSERNAME";
         lblTbdspinhroles_Internalname = "TBDSPINHROLES";
         chkavDisplayinheritroles_Internalname = "vDISPLAYINHERITROLES";
         bttBtnsearch_Internalname = "BTNSEARCH";
         lblTbadd_Internalname = "TBADD";
         cmbavRolesid_Internalname = "vROLESID";
         bttBtnadd_Internalname = "BTNADD";
         tblTbladd_Internalname = "TBLADD";
         tblTblpage_Internalname = "TBLPAGE";
         Form.Internalname = "FORM";
         subGridww_Internalname = "GRIDWW";
      }

      public override void initialize_properties( )
      {
         init_default_properties( ) ;
         edtavName_Jsonclick = "";
         edtavName_Visible = -1;
         edtavBtnmainrole_Jsonclick = "";
         edtavBtnmainrole_Visible = -1;
         edtavBtndlt_Jsonclick = "";
         edtavBtndlt_Enabled = 1;
         edtavGuid_Jsonclick = "";
         edtavGuid_Visible = 0;
         edtavId_Jsonclick = "";
         edtavId_Visible = 0;
         bttBtnadd_Visible = 1;
         cmbavRolesid_Jsonclick = "";
         lblTbadd_Visible = 1;
         edtavUsername_Jsonclick = "";
         edtavUsername_Enabled = 1;
         subGridww_Allowcollapsing = 0;
         subGridww_Allowselection = 0;
         edtavName_Enabled = 1;
         edtavBtnmainrole_Enabled = 1;
         edtavGuid_Enabled = 1;
         edtavId_Enabled = 1;
         edtavBtndlt_Visible = -1;
         subGridww_Class = "WorkWith";
         cmbavRolesid.Visible = 1;
         subGridww_Backcolorstyle = 0;
         chkavDisplayinheritroles.Caption = "";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "User`s roles";
      }

      protected override bool IsSpaSupported( )
      {
         return false ;
      }

      public override void InitializeDynEvents( )
      {
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         wcpOAV23UserId = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         GXKey = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         edtavId_Internalname = "";
         AV15GUID = "";
         edtavGuid_Internalname = "";
         AV7BtnDlt = "";
         edtavBtndlt_Internalname = "";
         AV8BtnMainRole = "";
         edtavBtnmainrole_Internalname = "";
         AV18Name = "";
         edtavName_Internalname = "";
         GridwwContainer = new GXWebGrid( context);
         AV25UserName = "";
         AV5GAMUser = new SdtGAMUser(context);
         AV26UserRoles = new GxExternalCollection( context, "SdtGAMRole", "GeneXus.Programs");
         AV11Errors = new GxExternalCollection( context, "SdtGAMError", "GeneXus.Programs");
         AV30GXV1 = new GxExternalCollection( context, "SdtGAMRole", "GeneXus.Programs");
         AV21RoleFilter = new SdtGAMRoleFilter(context);
         AV13GAMRole = new SdtGAMRole(context);
         AV32GXV3 = new GxExternalCollection( context, "SdtGAMRole", "GeneXus.Programs");
         AV34Btndlt_GXI = "";
         AV20Role = new SdtGAMRole(context);
         AV14GAMRolesDirect = new GxExternalCollection( context, "SdtGAMRole", "GeneXus.Programs");
         AV35GXV5 = new GxExternalCollection( context, "SdtGAMRole", "GeneXus.Programs");
         AV27GAMRoleAux = new SdtGAMRole(context);
         AV10Error = new SdtGAMError(context);
         sStyleString = "";
         subGridww_Linesclass = "";
         GridwwColumn = new GXWebColumn();
         lblTbuserlogin_Jsonclick = "";
         TempTags = "";
         lblTbdspinhroles_Jsonclick = "";
         ClassString = "";
         StyleString = "";
         bttBtnsearch_Jsonclick = "";
         lblTbadd_Jsonclick = "";
         bttBtnadd_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         GridwwRow = new GXWebRow();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.gamexamplewwuserrole__default(),
            new Object[][] {
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
         edtavUsername_Enabled = 0;
         edtavId_Enabled = 0;
         edtavGuid_Enabled = 0;
         edtavBtnmainrole_Enabled = 0;
         edtavName_Enabled = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_36 ;
      private short nGXsfl_36_idx=1 ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short nGXWrapped ;
      private short GRIDWW_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_36_Refreshing=0 ;
      private short subGridww_Backcolorstyle ;
      private short subGridww_Titlebackstyle ;
      private short subGridww_Allowselection ;
      private short subGridww_Allowhovering ;
      private short subGridww_Allowcollapsing ;
      private short subGridww_Collapsed ;
      private short subGridww_Backstyle ;
      private int subGridww_Islastpage ;
      private int edtavUsername_Enabled ;
      private int edtavId_Enabled ;
      private int edtavGuid_Enabled ;
      private int edtavBtnmainrole_Enabled ;
      private int edtavName_Enabled ;
      private int GRIDWW_nGridOutOfScope ;
      private int AV31GXV2 ;
      private int lblTbadd_Visible ;
      private int bttBtnadd_Visible ;
      private int AV33GXV4 ;
      private int edtavBtndlt_Visible ;
      private int AV36GXV6 ;
      private int AV37GXV7 ;
      private int AV38GXV8 ;
      private int AV39GXV9 ;
      private int subGridww_Titlebackcolor ;
      private int subGridww_Allbackcolor ;
      private int subGridww_Selectioncolor ;
      private int subGridww_Hoveringcolor ;
      private int idxLst ;
      private int subGridww_Backcolor ;
      private int edtavId_Visible ;
      private int edtavGuid_Visible ;
      private int edtavBtndlt_Enabled ;
      private int edtavBtnmainrole_Visible ;
      private int edtavName_Visible ;
      private int subGridww_Rows ;
      private long GRIDWW_nFirstRecordOnPage ;
      private long AV16Id ;
      private long AV22RolesId ;
      private long GRIDWW_nCurrentRecord ;
      private long GRIDWW_nRecordCount ;
      private String AV23UserId ;
      private String wcpOAV23UserId ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_36_idx="0001" ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String GXKey ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavId_Internalname ;
      private String AV15GUID ;
      private String edtavGuid_Internalname ;
      private String edtavBtndlt_Internalname ;
      private String AV8BtnMainRole ;
      private String edtavBtnmainrole_Internalname ;
      private String AV18Name ;
      private String edtavName_Internalname ;
      private String chkavDisplayinheritroles_Internalname ;
      private String edtavUsername_Internalname ;
      private String AV25UserName ;
      private String cmbavRolesid_Internalname ;
      private String lblTbadd_Internalname ;
      private String bttBtnadd_Internalname ;
      private String sStyleString ;
      private String tblTblpage_Internalname ;
      private String subGridww_Internalname ;
      private String subGridww_Class ;
      private String subGridww_Linesclass ;
      private String tblTbladd_Internalname ;
      private String lblTbuserlogin_Internalname ;
      private String lblTbuserlogin_Jsonclick ;
      private String TempTags ;
      private String edtavUsername_Jsonclick ;
      private String lblTbdspinhroles_Internalname ;
      private String lblTbdspinhroles_Jsonclick ;
      private String ClassString ;
      private String StyleString ;
      private String bttBtnsearch_Internalname ;
      private String bttBtnsearch_Jsonclick ;
      private String lblTbadd_Jsonclick ;
      private String cmbavRolesid_Jsonclick ;
      private String bttBtnadd_Jsonclick ;
      private String sGXsfl_36_fel_idx="0001" ;
      private String ROClassString ;
      private String edtavId_Jsonclick ;
      private String edtavGuid_Jsonclick ;
      private String edtavBtndlt_Jsonclick ;
      private String edtavBtnmainrole_Jsonclick ;
      private String edtavName_Jsonclick ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool AV12Exists ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool AV9DisplayInheritRoles ;
      private bool gx_refresh_fired ;
      private bool returnInSub ;
      private bool AV17isDirectRole ;
      private bool AV6isOK ;
      private bool AV7BtnDlt_IsBlob ;
      private String AV34Btndlt_GXI ;
      private String AV7BtnDlt ;
      private GXWebGrid GridwwContainer ;
      private GXWebRow GridwwRow ;
      private GXWebColumn GridwwColumn ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private String aP0_UserId ;
      private GXCheckbox chkavDisplayinheritroles ;
      private GXCombobox cmbavRolesid ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private IDataStoreProvider pr_default ;
      [ObjectCollection(ItemType=typeof( SdtGAMRole ))]
      private IGxCollection AV26UserRoles ;
      [ObjectCollection(ItemType=typeof( SdtGAMRole ))]
      private IGxCollection AV30GXV1 ;
      [ObjectCollection(ItemType=typeof( SdtGAMRole ))]
      private IGxCollection AV32GXV3 ;
      [ObjectCollection(ItemType=typeof( SdtGAMRole ))]
      private IGxCollection AV14GAMRolesDirect ;
      [ObjectCollection(ItemType=typeof( SdtGAMRole ))]
      private IGxCollection AV35GXV5 ;
      [ObjectCollection(ItemType=typeof( SdtGAMError ))]
      private IGxCollection AV11Errors ;
      private GXWebForm Form ;
      private SdtGAMUser AV5GAMUser ;
      private SdtGAMRole AV13GAMRole ;
      private SdtGAMRole AV20Role ;
      private SdtGAMRole AV27GAMRoleAux ;
      private SdtGAMError AV10Error ;
      private SdtGAMRoleFilter AV21RoleFilter ;
   }

   public class gamexamplewwuserrole__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          def= new CursorDef[] {
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
       }
    }

 }

}
