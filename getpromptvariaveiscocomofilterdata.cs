/*
               File: GetPromptVariaveisCocomoFilterData
        Description: Get Prompt Variaveis Cocomo Filter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/6/2020 23:52:35.80
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getpromptvariaveiscocomofilterdata : GXProcedure
   {
      public getpromptvariaveiscocomofilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getpromptvariaveiscocomofilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV40DDOName = aP0_DDOName;
         this.AV38SearchTxt = aP1_SearchTxt;
         this.AV39SearchTxtTo = aP2_SearchTxtTo;
         this.AV44OptionsJson = "" ;
         this.AV47OptionsDescJson = "" ;
         this.AV49OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV44OptionsJson;
         aP4_OptionsDescJson=this.AV47OptionsDescJson;
         aP5_OptionIndexesJson=this.AV49OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV40DDOName = aP0_DDOName;
         this.AV38SearchTxt = aP1_SearchTxt;
         this.AV39SearchTxtTo = aP2_SearchTxtTo;
         this.AV44OptionsJson = "" ;
         this.AV47OptionsDescJson = "" ;
         this.AV49OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV44OptionsJson;
         aP4_OptionsDescJson=this.AV47OptionsDescJson;
         aP5_OptionIndexesJson=this.AV49OptionIndexesJson;
         return AV49OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getpromptvariaveiscocomofilterdata objgetpromptvariaveiscocomofilterdata;
         objgetpromptvariaveiscocomofilterdata = new getpromptvariaveiscocomofilterdata();
         objgetpromptvariaveiscocomofilterdata.AV40DDOName = aP0_DDOName;
         objgetpromptvariaveiscocomofilterdata.AV38SearchTxt = aP1_SearchTxt;
         objgetpromptvariaveiscocomofilterdata.AV39SearchTxtTo = aP2_SearchTxtTo;
         objgetpromptvariaveiscocomofilterdata.AV44OptionsJson = "" ;
         objgetpromptvariaveiscocomofilterdata.AV47OptionsDescJson = "" ;
         objgetpromptvariaveiscocomofilterdata.AV49OptionIndexesJson = "" ;
         objgetpromptvariaveiscocomofilterdata.context.SetSubmitInitialConfig(context);
         objgetpromptvariaveiscocomofilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetpromptvariaveiscocomofilterdata);
         aP3_OptionsJson=this.AV44OptionsJson;
         aP4_OptionsDescJson=this.AV47OptionsDescJson;
         aP5_OptionIndexesJson=this.AV49OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getpromptvariaveiscocomofilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV43Options = (IGxCollection)(new GxSimpleCollection());
         AV46OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV48OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV40DDOName), "DDO_VARIAVELCOCOMO_SIGLA") == 0 )
         {
            /* Execute user subroutine: 'LOADVARIAVELCOCOMO_SIGLAOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV40DDOName), "DDO_VARIAVELCOCOMO_NOME") == 0 )
         {
            /* Execute user subroutine: 'LOADVARIAVELCOCOMO_NOMEOPTIONS' */
            S131 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV44OptionsJson = AV43Options.ToJSonString(false);
         AV47OptionsDescJson = AV46OptionsDesc.ToJSonString(false);
         AV49OptionIndexesJson = AV48OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV51Session.Get("PromptVariaveisCocomoGridState"), "") == 0 )
         {
            AV53GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "PromptVariaveisCocomoGridState"), "");
         }
         else
         {
            AV53GridState.FromXml(AV51Session.Get("PromptVariaveisCocomoGridState"), "");
         }
         AV69GXV1 = 1;
         while ( AV69GXV1 <= AV53GridState.gxTpr_Filtervalues.Count )
         {
            AV54GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV53GridState.gxTpr_Filtervalues.Item(AV69GXV1));
            if ( StringUtil.StrCmp(AV54GridStateFilterValue.gxTpr_Name, "TFVARIAVELCOCOMO_AREATRABALHOCOD") == 0 )
            {
               AV10TFVariavelCocomo_AreaTrabalhoCod = (int)(NumberUtil.Val( AV54GridStateFilterValue.gxTpr_Value, "."));
               AV11TFVariavelCocomo_AreaTrabalhoCod_To = (int)(NumberUtil.Val( AV54GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV54GridStateFilterValue.gxTpr_Name, "TFVARIAVELCOCOMO_SIGLA") == 0 )
            {
               AV12TFVariavelCocomo_Sigla = AV54GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV54GridStateFilterValue.gxTpr_Name, "TFVARIAVELCOCOMO_SIGLA_SEL") == 0 )
            {
               AV13TFVariavelCocomo_Sigla_Sel = AV54GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV54GridStateFilterValue.gxTpr_Name, "TFVARIAVELCOCOMO_NOME") == 0 )
            {
               AV14TFVariavelCocomo_Nome = AV54GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV54GridStateFilterValue.gxTpr_Name, "TFVARIAVELCOCOMO_NOME_SEL") == 0 )
            {
               AV15TFVariavelCocomo_Nome_Sel = AV54GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV54GridStateFilterValue.gxTpr_Name, "TFVARIAVELCOCOMO_TIPO_SEL") == 0 )
            {
               AV16TFVariavelCocomo_Tipo_SelsJson = AV54GridStateFilterValue.gxTpr_Value;
               AV17TFVariavelCocomo_Tipo_Sels.FromJSonString(AV16TFVariavelCocomo_Tipo_SelsJson);
            }
            else if ( StringUtil.StrCmp(AV54GridStateFilterValue.gxTpr_Name, "TFVARIAVELCOCOMO_PROJETOCOD") == 0 )
            {
               AV18TFVariavelCocomo_ProjetoCod = (int)(NumberUtil.Val( AV54GridStateFilterValue.gxTpr_Value, "."));
               AV19TFVariavelCocomo_ProjetoCod_To = (int)(NumberUtil.Val( AV54GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV54GridStateFilterValue.gxTpr_Name, "TFVARIAVELCOCOMO_DATA") == 0 )
            {
               AV20TFVariavelCocomo_Data = context.localUtil.CToD( AV54GridStateFilterValue.gxTpr_Value, 2);
               AV21TFVariavelCocomo_Data_To = context.localUtil.CToD( AV54GridStateFilterValue.gxTpr_Valueto, 2);
            }
            else if ( StringUtil.StrCmp(AV54GridStateFilterValue.gxTpr_Name, "TFVARIAVELCOCOMO_EXTRABAIXO") == 0 )
            {
               AV22TFVariavelCocomo_ExtraBaixo = NumberUtil.Val( AV54GridStateFilterValue.gxTpr_Value, ".");
               AV23TFVariavelCocomo_ExtraBaixo_To = NumberUtil.Val( AV54GridStateFilterValue.gxTpr_Valueto, ".");
            }
            else if ( StringUtil.StrCmp(AV54GridStateFilterValue.gxTpr_Name, "TFVARIAVELCOCOMO_MUITOBAIXO") == 0 )
            {
               AV24TFVariavelCocomo_MuitoBaixo = NumberUtil.Val( AV54GridStateFilterValue.gxTpr_Value, ".");
               AV25TFVariavelCocomo_MuitoBaixo_To = NumberUtil.Val( AV54GridStateFilterValue.gxTpr_Valueto, ".");
            }
            else if ( StringUtil.StrCmp(AV54GridStateFilterValue.gxTpr_Name, "TFVARIAVELCOCOMO_BAIXO") == 0 )
            {
               AV26TFVariavelCocomo_Baixo = NumberUtil.Val( AV54GridStateFilterValue.gxTpr_Value, ".");
               AV27TFVariavelCocomo_Baixo_To = NumberUtil.Val( AV54GridStateFilterValue.gxTpr_Valueto, ".");
            }
            else if ( StringUtil.StrCmp(AV54GridStateFilterValue.gxTpr_Name, "TFVARIAVELCOCOMO_NOMINAL") == 0 )
            {
               AV28TFVariavelCocomo_Nominal = NumberUtil.Val( AV54GridStateFilterValue.gxTpr_Value, ".");
               AV29TFVariavelCocomo_Nominal_To = NumberUtil.Val( AV54GridStateFilterValue.gxTpr_Valueto, ".");
            }
            else if ( StringUtil.StrCmp(AV54GridStateFilterValue.gxTpr_Name, "TFVARIAVELCOCOMO_ALTO") == 0 )
            {
               AV30TFVariavelCocomo_Alto = NumberUtil.Val( AV54GridStateFilterValue.gxTpr_Value, ".");
               AV31TFVariavelCocomo_Alto_To = NumberUtil.Val( AV54GridStateFilterValue.gxTpr_Valueto, ".");
            }
            else if ( StringUtil.StrCmp(AV54GridStateFilterValue.gxTpr_Name, "TFVARIAVELCOCOMO_MUITOALTO") == 0 )
            {
               AV32TFVariavelCocomo_MuitoAlto = NumberUtil.Val( AV54GridStateFilterValue.gxTpr_Value, ".");
               AV33TFVariavelCocomo_MuitoAlto_To = NumberUtil.Val( AV54GridStateFilterValue.gxTpr_Valueto, ".");
            }
            else if ( StringUtil.StrCmp(AV54GridStateFilterValue.gxTpr_Name, "TFVARIAVELCOCOMO_EXTRAALTO") == 0 )
            {
               AV34TFVariavelCocomo_ExtraAlto = NumberUtil.Val( AV54GridStateFilterValue.gxTpr_Value, ".");
               AV35TFVariavelCocomo_ExtraAlto_To = NumberUtil.Val( AV54GridStateFilterValue.gxTpr_Valueto, ".");
            }
            else if ( StringUtil.StrCmp(AV54GridStateFilterValue.gxTpr_Name, "TFVARIAVELCOCOMO_USADO") == 0 )
            {
               AV36TFVariavelCocomo_Usado = NumberUtil.Val( AV54GridStateFilterValue.gxTpr_Value, ".");
               AV37TFVariavelCocomo_Usado_To = NumberUtil.Val( AV54GridStateFilterValue.gxTpr_Valueto, ".");
            }
            AV69GXV1 = (int)(AV69GXV1+1);
         }
         if ( AV53GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV55GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV53GridState.gxTpr_Dynamicfilters.Item(1));
            AV56DynamicFiltersSelector1 = AV55GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV56DynamicFiltersSelector1, "VARIAVELCOCOMO_NOME") == 0 )
            {
               AV57DynamicFiltersOperator1 = AV55GridStateDynamicFilter.gxTpr_Operator;
               AV58VariavelCocomo_Nome1 = AV55GridStateDynamicFilter.gxTpr_Value;
            }
            if ( AV53GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV59DynamicFiltersEnabled2 = true;
               AV55GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV53GridState.gxTpr_Dynamicfilters.Item(2));
               AV60DynamicFiltersSelector2 = AV55GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV60DynamicFiltersSelector2, "VARIAVELCOCOMO_NOME") == 0 )
               {
                  AV61DynamicFiltersOperator2 = AV55GridStateDynamicFilter.gxTpr_Operator;
                  AV62VariavelCocomo_Nome2 = AV55GridStateDynamicFilter.gxTpr_Value;
               }
               if ( AV53GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  AV63DynamicFiltersEnabled3 = true;
                  AV55GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV53GridState.gxTpr_Dynamicfilters.Item(3));
                  AV64DynamicFiltersSelector3 = AV55GridStateDynamicFilter.gxTpr_Selected;
                  if ( StringUtil.StrCmp(AV64DynamicFiltersSelector3, "VARIAVELCOCOMO_NOME") == 0 )
                  {
                     AV65DynamicFiltersOperator3 = AV55GridStateDynamicFilter.gxTpr_Operator;
                     AV66VariavelCocomo_Nome3 = AV55GridStateDynamicFilter.gxTpr_Value;
                  }
               }
            }
         }
      }

      protected void S121( )
      {
         /* 'LOADVARIAVELCOCOMO_SIGLAOPTIONS' Routine */
         AV12TFVariavelCocomo_Sigla = AV38SearchTxt;
         AV13TFVariavelCocomo_Sigla_Sel = "";
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              A964VariavelCocomo_Tipo ,
                                              AV17TFVariavelCocomo_Tipo_Sels ,
                                              AV56DynamicFiltersSelector1 ,
                                              AV57DynamicFiltersOperator1 ,
                                              AV58VariavelCocomo_Nome1 ,
                                              AV59DynamicFiltersEnabled2 ,
                                              AV60DynamicFiltersSelector2 ,
                                              AV61DynamicFiltersOperator2 ,
                                              AV62VariavelCocomo_Nome2 ,
                                              AV63DynamicFiltersEnabled3 ,
                                              AV64DynamicFiltersSelector3 ,
                                              AV65DynamicFiltersOperator3 ,
                                              AV66VariavelCocomo_Nome3 ,
                                              AV10TFVariavelCocomo_AreaTrabalhoCod ,
                                              AV11TFVariavelCocomo_AreaTrabalhoCod_To ,
                                              AV13TFVariavelCocomo_Sigla_Sel ,
                                              AV12TFVariavelCocomo_Sigla ,
                                              AV15TFVariavelCocomo_Nome_Sel ,
                                              AV14TFVariavelCocomo_Nome ,
                                              AV17TFVariavelCocomo_Tipo_Sels.Count ,
                                              AV18TFVariavelCocomo_ProjetoCod ,
                                              AV19TFVariavelCocomo_ProjetoCod_To ,
                                              AV20TFVariavelCocomo_Data ,
                                              AV21TFVariavelCocomo_Data_To ,
                                              AV22TFVariavelCocomo_ExtraBaixo ,
                                              AV23TFVariavelCocomo_ExtraBaixo_To ,
                                              AV24TFVariavelCocomo_MuitoBaixo ,
                                              AV25TFVariavelCocomo_MuitoBaixo_To ,
                                              AV26TFVariavelCocomo_Baixo ,
                                              AV27TFVariavelCocomo_Baixo_To ,
                                              AV28TFVariavelCocomo_Nominal ,
                                              AV29TFVariavelCocomo_Nominal_To ,
                                              AV30TFVariavelCocomo_Alto ,
                                              AV31TFVariavelCocomo_Alto_To ,
                                              AV32TFVariavelCocomo_MuitoAlto ,
                                              AV33TFVariavelCocomo_MuitoAlto_To ,
                                              AV34TFVariavelCocomo_ExtraAlto ,
                                              AV35TFVariavelCocomo_ExtraAlto_To ,
                                              AV36TFVariavelCocomo_Usado ,
                                              AV37TFVariavelCocomo_Usado_To ,
                                              A963VariavelCocomo_Nome ,
                                              A961VariavelCocomo_AreaTrabalhoCod ,
                                              A962VariavelCocomo_Sigla ,
                                              A965VariavelCocomo_ProjetoCod ,
                                              A966VariavelCocomo_Data ,
                                              A986VariavelCocomo_ExtraBaixo ,
                                              A967VariavelCocomo_MuitoBaixo ,
                                              A968VariavelCocomo_Baixo ,
                                              A969VariavelCocomo_Nominal ,
                                              A970VariavelCocomo_Alto ,
                                              A971VariavelCocomo_MuitoAlto ,
                                              A972VariavelCocomo_ExtraAlto ,
                                              A973VariavelCocomo_Usado },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING,
                                              TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.INT, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL,
                                              TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.STRING,
                                              TypeConstants.INT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.BOOLEAN,
                                              TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.BOOLEAN,
                                              TypeConstants.DECIMAL, TypeConstants.BOOLEAN
                                              }
         });
         lV58VariavelCocomo_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV58VariavelCocomo_Nome1), 50, "%");
         lV58VariavelCocomo_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV58VariavelCocomo_Nome1), 50, "%");
         lV62VariavelCocomo_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV62VariavelCocomo_Nome2), 50, "%");
         lV62VariavelCocomo_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV62VariavelCocomo_Nome2), 50, "%");
         lV66VariavelCocomo_Nome3 = StringUtil.PadR( StringUtil.RTrim( AV66VariavelCocomo_Nome3), 50, "%");
         lV66VariavelCocomo_Nome3 = StringUtil.PadR( StringUtil.RTrim( AV66VariavelCocomo_Nome3), 50, "%");
         lV12TFVariavelCocomo_Sigla = StringUtil.PadR( StringUtil.RTrim( AV12TFVariavelCocomo_Sigla), 15, "%");
         lV14TFVariavelCocomo_Nome = StringUtil.PadR( StringUtil.RTrim( AV14TFVariavelCocomo_Nome), 50, "%");
         /* Using cursor P00PI2 */
         pr_default.execute(0, new Object[] {lV58VariavelCocomo_Nome1, lV58VariavelCocomo_Nome1, lV62VariavelCocomo_Nome2, lV62VariavelCocomo_Nome2, lV66VariavelCocomo_Nome3, lV66VariavelCocomo_Nome3, AV10TFVariavelCocomo_AreaTrabalhoCod, AV11TFVariavelCocomo_AreaTrabalhoCod_To, lV12TFVariavelCocomo_Sigla, AV13TFVariavelCocomo_Sigla_Sel, lV14TFVariavelCocomo_Nome, AV15TFVariavelCocomo_Nome_Sel, AV18TFVariavelCocomo_ProjetoCod, AV19TFVariavelCocomo_ProjetoCod_To, AV20TFVariavelCocomo_Data, AV21TFVariavelCocomo_Data_To, AV22TFVariavelCocomo_ExtraBaixo, AV23TFVariavelCocomo_ExtraBaixo_To, AV24TFVariavelCocomo_MuitoBaixo, AV25TFVariavelCocomo_MuitoBaixo_To, AV26TFVariavelCocomo_Baixo, AV27TFVariavelCocomo_Baixo_To, AV28TFVariavelCocomo_Nominal, AV29TFVariavelCocomo_Nominal_To, AV30TFVariavelCocomo_Alto, AV31TFVariavelCocomo_Alto_To, AV32TFVariavelCocomo_MuitoAlto, AV33TFVariavelCocomo_MuitoAlto_To, AV34TFVariavelCocomo_ExtraAlto, AV35TFVariavelCocomo_ExtraAlto_To, AV36TFVariavelCocomo_Usado, AV37TFVariavelCocomo_Usado_To});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKPI2 = false;
            A962VariavelCocomo_Sigla = P00PI2_A962VariavelCocomo_Sigla[0];
            A973VariavelCocomo_Usado = P00PI2_A973VariavelCocomo_Usado[0];
            n973VariavelCocomo_Usado = P00PI2_n973VariavelCocomo_Usado[0];
            A972VariavelCocomo_ExtraAlto = P00PI2_A972VariavelCocomo_ExtraAlto[0];
            n972VariavelCocomo_ExtraAlto = P00PI2_n972VariavelCocomo_ExtraAlto[0];
            A971VariavelCocomo_MuitoAlto = P00PI2_A971VariavelCocomo_MuitoAlto[0];
            n971VariavelCocomo_MuitoAlto = P00PI2_n971VariavelCocomo_MuitoAlto[0];
            A970VariavelCocomo_Alto = P00PI2_A970VariavelCocomo_Alto[0];
            n970VariavelCocomo_Alto = P00PI2_n970VariavelCocomo_Alto[0];
            A969VariavelCocomo_Nominal = P00PI2_A969VariavelCocomo_Nominal[0];
            n969VariavelCocomo_Nominal = P00PI2_n969VariavelCocomo_Nominal[0];
            A968VariavelCocomo_Baixo = P00PI2_A968VariavelCocomo_Baixo[0];
            n968VariavelCocomo_Baixo = P00PI2_n968VariavelCocomo_Baixo[0];
            A967VariavelCocomo_MuitoBaixo = P00PI2_A967VariavelCocomo_MuitoBaixo[0];
            n967VariavelCocomo_MuitoBaixo = P00PI2_n967VariavelCocomo_MuitoBaixo[0];
            A986VariavelCocomo_ExtraBaixo = P00PI2_A986VariavelCocomo_ExtraBaixo[0];
            n986VariavelCocomo_ExtraBaixo = P00PI2_n986VariavelCocomo_ExtraBaixo[0];
            A966VariavelCocomo_Data = P00PI2_A966VariavelCocomo_Data[0];
            n966VariavelCocomo_Data = P00PI2_n966VariavelCocomo_Data[0];
            A965VariavelCocomo_ProjetoCod = P00PI2_A965VariavelCocomo_ProjetoCod[0];
            n965VariavelCocomo_ProjetoCod = P00PI2_n965VariavelCocomo_ProjetoCod[0];
            A964VariavelCocomo_Tipo = P00PI2_A964VariavelCocomo_Tipo[0];
            A961VariavelCocomo_AreaTrabalhoCod = P00PI2_A961VariavelCocomo_AreaTrabalhoCod[0];
            A963VariavelCocomo_Nome = P00PI2_A963VariavelCocomo_Nome[0];
            A992VariavelCocomo_Sequencial = P00PI2_A992VariavelCocomo_Sequencial[0];
            AV50count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( StringUtil.StrCmp(P00PI2_A962VariavelCocomo_Sigla[0], A962VariavelCocomo_Sigla) == 0 ) )
            {
               BRKPI2 = false;
               A964VariavelCocomo_Tipo = P00PI2_A964VariavelCocomo_Tipo[0];
               A961VariavelCocomo_AreaTrabalhoCod = P00PI2_A961VariavelCocomo_AreaTrabalhoCod[0];
               A992VariavelCocomo_Sequencial = P00PI2_A992VariavelCocomo_Sequencial[0];
               AV50count = (long)(AV50count+1);
               BRKPI2 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A962VariavelCocomo_Sigla)) )
            {
               AV42Option = A962VariavelCocomo_Sigla;
               AV43Options.Add(AV42Option, 0);
               AV48OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV50count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV43Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKPI2 )
            {
               BRKPI2 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      protected void S131( )
      {
         /* 'LOADVARIAVELCOCOMO_NOMEOPTIONS' Routine */
         AV14TFVariavelCocomo_Nome = AV38SearchTxt;
         AV15TFVariavelCocomo_Nome_Sel = "";
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              A964VariavelCocomo_Tipo ,
                                              AV17TFVariavelCocomo_Tipo_Sels ,
                                              AV56DynamicFiltersSelector1 ,
                                              AV57DynamicFiltersOperator1 ,
                                              AV58VariavelCocomo_Nome1 ,
                                              AV59DynamicFiltersEnabled2 ,
                                              AV60DynamicFiltersSelector2 ,
                                              AV61DynamicFiltersOperator2 ,
                                              AV62VariavelCocomo_Nome2 ,
                                              AV63DynamicFiltersEnabled3 ,
                                              AV64DynamicFiltersSelector3 ,
                                              AV65DynamicFiltersOperator3 ,
                                              AV66VariavelCocomo_Nome3 ,
                                              AV10TFVariavelCocomo_AreaTrabalhoCod ,
                                              AV11TFVariavelCocomo_AreaTrabalhoCod_To ,
                                              AV13TFVariavelCocomo_Sigla_Sel ,
                                              AV12TFVariavelCocomo_Sigla ,
                                              AV15TFVariavelCocomo_Nome_Sel ,
                                              AV14TFVariavelCocomo_Nome ,
                                              AV17TFVariavelCocomo_Tipo_Sels.Count ,
                                              AV18TFVariavelCocomo_ProjetoCod ,
                                              AV19TFVariavelCocomo_ProjetoCod_To ,
                                              AV20TFVariavelCocomo_Data ,
                                              AV21TFVariavelCocomo_Data_To ,
                                              AV22TFVariavelCocomo_ExtraBaixo ,
                                              AV23TFVariavelCocomo_ExtraBaixo_To ,
                                              AV24TFVariavelCocomo_MuitoBaixo ,
                                              AV25TFVariavelCocomo_MuitoBaixo_To ,
                                              AV26TFVariavelCocomo_Baixo ,
                                              AV27TFVariavelCocomo_Baixo_To ,
                                              AV28TFVariavelCocomo_Nominal ,
                                              AV29TFVariavelCocomo_Nominal_To ,
                                              AV30TFVariavelCocomo_Alto ,
                                              AV31TFVariavelCocomo_Alto_To ,
                                              AV32TFVariavelCocomo_MuitoAlto ,
                                              AV33TFVariavelCocomo_MuitoAlto_To ,
                                              AV34TFVariavelCocomo_ExtraAlto ,
                                              AV35TFVariavelCocomo_ExtraAlto_To ,
                                              AV36TFVariavelCocomo_Usado ,
                                              AV37TFVariavelCocomo_Usado_To ,
                                              A963VariavelCocomo_Nome ,
                                              A961VariavelCocomo_AreaTrabalhoCod ,
                                              A962VariavelCocomo_Sigla ,
                                              A965VariavelCocomo_ProjetoCod ,
                                              A966VariavelCocomo_Data ,
                                              A986VariavelCocomo_ExtraBaixo ,
                                              A967VariavelCocomo_MuitoBaixo ,
                                              A968VariavelCocomo_Baixo ,
                                              A969VariavelCocomo_Nominal ,
                                              A970VariavelCocomo_Alto ,
                                              A971VariavelCocomo_MuitoAlto ,
                                              A972VariavelCocomo_ExtraAlto ,
                                              A973VariavelCocomo_Usado },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING,
                                              TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.INT, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL,
                                              TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.STRING,
                                              TypeConstants.INT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.BOOLEAN,
                                              TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.BOOLEAN,
                                              TypeConstants.DECIMAL, TypeConstants.BOOLEAN
                                              }
         });
         lV58VariavelCocomo_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV58VariavelCocomo_Nome1), 50, "%");
         lV58VariavelCocomo_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV58VariavelCocomo_Nome1), 50, "%");
         lV62VariavelCocomo_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV62VariavelCocomo_Nome2), 50, "%");
         lV62VariavelCocomo_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV62VariavelCocomo_Nome2), 50, "%");
         lV66VariavelCocomo_Nome3 = StringUtil.PadR( StringUtil.RTrim( AV66VariavelCocomo_Nome3), 50, "%");
         lV66VariavelCocomo_Nome3 = StringUtil.PadR( StringUtil.RTrim( AV66VariavelCocomo_Nome3), 50, "%");
         lV12TFVariavelCocomo_Sigla = StringUtil.PadR( StringUtil.RTrim( AV12TFVariavelCocomo_Sigla), 15, "%");
         lV14TFVariavelCocomo_Nome = StringUtil.PadR( StringUtil.RTrim( AV14TFVariavelCocomo_Nome), 50, "%");
         /* Using cursor P00PI3 */
         pr_default.execute(1, new Object[] {lV58VariavelCocomo_Nome1, lV58VariavelCocomo_Nome1, lV62VariavelCocomo_Nome2, lV62VariavelCocomo_Nome2, lV66VariavelCocomo_Nome3, lV66VariavelCocomo_Nome3, AV10TFVariavelCocomo_AreaTrabalhoCod, AV11TFVariavelCocomo_AreaTrabalhoCod_To, lV12TFVariavelCocomo_Sigla, AV13TFVariavelCocomo_Sigla_Sel, lV14TFVariavelCocomo_Nome, AV15TFVariavelCocomo_Nome_Sel, AV18TFVariavelCocomo_ProjetoCod, AV19TFVariavelCocomo_ProjetoCod_To, AV20TFVariavelCocomo_Data, AV21TFVariavelCocomo_Data_To, AV22TFVariavelCocomo_ExtraBaixo, AV23TFVariavelCocomo_ExtraBaixo_To, AV24TFVariavelCocomo_MuitoBaixo, AV25TFVariavelCocomo_MuitoBaixo_To, AV26TFVariavelCocomo_Baixo, AV27TFVariavelCocomo_Baixo_To, AV28TFVariavelCocomo_Nominal, AV29TFVariavelCocomo_Nominal_To, AV30TFVariavelCocomo_Alto, AV31TFVariavelCocomo_Alto_To, AV32TFVariavelCocomo_MuitoAlto, AV33TFVariavelCocomo_MuitoAlto_To, AV34TFVariavelCocomo_ExtraAlto, AV35TFVariavelCocomo_ExtraAlto_To, AV36TFVariavelCocomo_Usado, AV37TFVariavelCocomo_Usado_To});
         while ( (pr_default.getStatus(1) != 101) )
         {
            BRKPI4 = false;
            A963VariavelCocomo_Nome = P00PI3_A963VariavelCocomo_Nome[0];
            A973VariavelCocomo_Usado = P00PI3_A973VariavelCocomo_Usado[0];
            n973VariavelCocomo_Usado = P00PI3_n973VariavelCocomo_Usado[0];
            A972VariavelCocomo_ExtraAlto = P00PI3_A972VariavelCocomo_ExtraAlto[0];
            n972VariavelCocomo_ExtraAlto = P00PI3_n972VariavelCocomo_ExtraAlto[0];
            A971VariavelCocomo_MuitoAlto = P00PI3_A971VariavelCocomo_MuitoAlto[0];
            n971VariavelCocomo_MuitoAlto = P00PI3_n971VariavelCocomo_MuitoAlto[0];
            A970VariavelCocomo_Alto = P00PI3_A970VariavelCocomo_Alto[0];
            n970VariavelCocomo_Alto = P00PI3_n970VariavelCocomo_Alto[0];
            A969VariavelCocomo_Nominal = P00PI3_A969VariavelCocomo_Nominal[0];
            n969VariavelCocomo_Nominal = P00PI3_n969VariavelCocomo_Nominal[0];
            A968VariavelCocomo_Baixo = P00PI3_A968VariavelCocomo_Baixo[0];
            n968VariavelCocomo_Baixo = P00PI3_n968VariavelCocomo_Baixo[0];
            A967VariavelCocomo_MuitoBaixo = P00PI3_A967VariavelCocomo_MuitoBaixo[0];
            n967VariavelCocomo_MuitoBaixo = P00PI3_n967VariavelCocomo_MuitoBaixo[0];
            A986VariavelCocomo_ExtraBaixo = P00PI3_A986VariavelCocomo_ExtraBaixo[0];
            n986VariavelCocomo_ExtraBaixo = P00PI3_n986VariavelCocomo_ExtraBaixo[0];
            A966VariavelCocomo_Data = P00PI3_A966VariavelCocomo_Data[0];
            n966VariavelCocomo_Data = P00PI3_n966VariavelCocomo_Data[0];
            A965VariavelCocomo_ProjetoCod = P00PI3_A965VariavelCocomo_ProjetoCod[0];
            n965VariavelCocomo_ProjetoCod = P00PI3_n965VariavelCocomo_ProjetoCod[0];
            A964VariavelCocomo_Tipo = P00PI3_A964VariavelCocomo_Tipo[0];
            A962VariavelCocomo_Sigla = P00PI3_A962VariavelCocomo_Sigla[0];
            A961VariavelCocomo_AreaTrabalhoCod = P00PI3_A961VariavelCocomo_AreaTrabalhoCod[0];
            A992VariavelCocomo_Sequencial = P00PI3_A992VariavelCocomo_Sequencial[0];
            AV50count = 0;
            while ( (pr_default.getStatus(1) != 101) && ( StringUtil.StrCmp(P00PI3_A963VariavelCocomo_Nome[0], A963VariavelCocomo_Nome) == 0 ) )
            {
               BRKPI4 = false;
               A964VariavelCocomo_Tipo = P00PI3_A964VariavelCocomo_Tipo[0];
               A962VariavelCocomo_Sigla = P00PI3_A962VariavelCocomo_Sigla[0];
               A961VariavelCocomo_AreaTrabalhoCod = P00PI3_A961VariavelCocomo_AreaTrabalhoCod[0];
               A992VariavelCocomo_Sequencial = P00PI3_A992VariavelCocomo_Sequencial[0];
               AV50count = (long)(AV50count+1);
               BRKPI4 = true;
               pr_default.readNext(1);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A963VariavelCocomo_Nome)) )
            {
               AV42Option = A963VariavelCocomo_Nome;
               AV43Options.Add(AV42Option, 0);
               AV48OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV50count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV43Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKPI4 )
            {
               BRKPI4 = true;
               pr_default.readNext(1);
            }
         }
         pr_default.close(1);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV43Options = new GxSimpleCollection();
         AV46OptionsDesc = new GxSimpleCollection();
         AV48OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV51Session = context.GetSession();
         AV53GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV54GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV12TFVariavelCocomo_Sigla = "";
         AV13TFVariavelCocomo_Sigla_Sel = "";
         AV14TFVariavelCocomo_Nome = "";
         AV15TFVariavelCocomo_Nome_Sel = "";
         AV16TFVariavelCocomo_Tipo_SelsJson = "";
         AV17TFVariavelCocomo_Tipo_Sels = new GxSimpleCollection();
         AV20TFVariavelCocomo_Data = DateTime.MinValue;
         AV21TFVariavelCocomo_Data_To = DateTime.MinValue;
         AV55GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV56DynamicFiltersSelector1 = "";
         AV58VariavelCocomo_Nome1 = "";
         AV60DynamicFiltersSelector2 = "";
         AV62VariavelCocomo_Nome2 = "";
         AV64DynamicFiltersSelector3 = "";
         AV66VariavelCocomo_Nome3 = "";
         scmdbuf = "";
         lV58VariavelCocomo_Nome1 = "";
         lV62VariavelCocomo_Nome2 = "";
         lV66VariavelCocomo_Nome3 = "";
         lV12TFVariavelCocomo_Sigla = "";
         lV14TFVariavelCocomo_Nome = "";
         A964VariavelCocomo_Tipo = "";
         A963VariavelCocomo_Nome = "";
         A962VariavelCocomo_Sigla = "";
         A966VariavelCocomo_Data = DateTime.MinValue;
         P00PI2_A962VariavelCocomo_Sigla = new String[] {""} ;
         P00PI2_A973VariavelCocomo_Usado = new decimal[1] ;
         P00PI2_n973VariavelCocomo_Usado = new bool[] {false} ;
         P00PI2_A972VariavelCocomo_ExtraAlto = new decimal[1] ;
         P00PI2_n972VariavelCocomo_ExtraAlto = new bool[] {false} ;
         P00PI2_A971VariavelCocomo_MuitoAlto = new decimal[1] ;
         P00PI2_n971VariavelCocomo_MuitoAlto = new bool[] {false} ;
         P00PI2_A970VariavelCocomo_Alto = new decimal[1] ;
         P00PI2_n970VariavelCocomo_Alto = new bool[] {false} ;
         P00PI2_A969VariavelCocomo_Nominal = new decimal[1] ;
         P00PI2_n969VariavelCocomo_Nominal = new bool[] {false} ;
         P00PI2_A968VariavelCocomo_Baixo = new decimal[1] ;
         P00PI2_n968VariavelCocomo_Baixo = new bool[] {false} ;
         P00PI2_A967VariavelCocomo_MuitoBaixo = new decimal[1] ;
         P00PI2_n967VariavelCocomo_MuitoBaixo = new bool[] {false} ;
         P00PI2_A986VariavelCocomo_ExtraBaixo = new decimal[1] ;
         P00PI2_n986VariavelCocomo_ExtraBaixo = new bool[] {false} ;
         P00PI2_A966VariavelCocomo_Data = new DateTime[] {DateTime.MinValue} ;
         P00PI2_n966VariavelCocomo_Data = new bool[] {false} ;
         P00PI2_A965VariavelCocomo_ProjetoCod = new int[1] ;
         P00PI2_n965VariavelCocomo_ProjetoCod = new bool[] {false} ;
         P00PI2_A964VariavelCocomo_Tipo = new String[] {""} ;
         P00PI2_A961VariavelCocomo_AreaTrabalhoCod = new int[1] ;
         P00PI2_A963VariavelCocomo_Nome = new String[] {""} ;
         P00PI2_A992VariavelCocomo_Sequencial = new short[1] ;
         AV42Option = "";
         P00PI3_A963VariavelCocomo_Nome = new String[] {""} ;
         P00PI3_A973VariavelCocomo_Usado = new decimal[1] ;
         P00PI3_n973VariavelCocomo_Usado = new bool[] {false} ;
         P00PI3_A972VariavelCocomo_ExtraAlto = new decimal[1] ;
         P00PI3_n972VariavelCocomo_ExtraAlto = new bool[] {false} ;
         P00PI3_A971VariavelCocomo_MuitoAlto = new decimal[1] ;
         P00PI3_n971VariavelCocomo_MuitoAlto = new bool[] {false} ;
         P00PI3_A970VariavelCocomo_Alto = new decimal[1] ;
         P00PI3_n970VariavelCocomo_Alto = new bool[] {false} ;
         P00PI3_A969VariavelCocomo_Nominal = new decimal[1] ;
         P00PI3_n969VariavelCocomo_Nominal = new bool[] {false} ;
         P00PI3_A968VariavelCocomo_Baixo = new decimal[1] ;
         P00PI3_n968VariavelCocomo_Baixo = new bool[] {false} ;
         P00PI3_A967VariavelCocomo_MuitoBaixo = new decimal[1] ;
         P00PI3_n967VariavelCocomo_MuitoBaixo = new bool[] {false} ;
         P00PI3_A986VariavelCocomo_ExtraBaixo = new decimal[1] ;
         P00PI3_n986VariavelCocomo_ExtraBaixo = new bool[] {false} ;
         P00PI3_A966VariavelCocomo_Data = new DateTime[] {DateTime.MinValue} ;
         P00PI3_n966VariavelCocomo_Data = new bool[] {false} ;
         P00PI3_A965VariavelCocomo_ProjetoCod = new int[1] ;
         P00PI3_n965VariavelCocomo_ProjetoCod = new bool[] {false} ;
         P00PI3_A964VariavelCocomo_Tipo = new String[] {""} ;
         P00PI3_A962VariavelCocomo_Sigla = new String[] {""} ;
         P00PI3_A961VariavelCocomo_AreaTrabalhoCod = new int[1] ;
         P00PI3_A992VariavelCocomo_Sequencial = new short[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getpromptvariaveiscocomofilterdata__default(),
            new Object[][] {
                new Object[] {
               P00PI2_A962VariavelCocomo_Sigla, P00PI2_A973VariavelCocomo_Usado, P00PI2_n973VariavelCocomo_Usado, P00PI2_A972VariavelCocomo_ExtraAlto, P00PI2_n972VariavelCocomo_ExtraAlto, P00PI2_A971VariavelCocomo_MuitoAlto, P00PI2_n971VariavelCocomo_MuitoAlto, P00PI2_A970VariavelCocomo_Alto, P00PI2_n970VariavelCocomo_Alto, P00PI2_A969VariavelCocomo_Nominal,
               P00PI2_n969VariavelCocomo_Nominal, P00PI2_A968VariavelCocomo_Baixo, P00PI2_n968VariavelCocomo_Baixo, P00PI2_A967VariavelCocomo_MuitoBaixo, P00PI2_n967VariavelCocomo_MuitoBaixo, P00PI2_A986VariavelCocomo_ExtraBaixo, P00PI2_n986VariavelCocomo_ExtraBaixo, P00PI2_A966VariavelCocomo_Data, P00PI2_n966VariavelCocomo_Data, P00PI2_A965VariavelCocomo_ProjetoCod,
               P00PI2_n965VariavelCocomo_ProjetoCod, P00PI2_A964VariavelCocomo_Tipo, P00PI2_A961VariavelCocomo_AreaTrabalhoCod, P00PI2_A963VariavelCocomo_Nome, P00PI2_A992VariavelCocomo_Sequencial
               }
               , new Object[] {
               P00PI3_A963VariavelCocomo_Nome, P00PI3_A973VariavelCocomo_Usado, P00PI3_n973VariavelCocomo_Usado, P00PI3_A972VariavelCocomo_ExtraAlto, P00PI3_n972VariavelCocomo_ExtraAlto, P00PI3_A971VariavelCocomo_MuitoAlto, P00PI3_n971VariavelCocomo_MuitoAlto, P00PI3_A970VariavelCocomo_Alto, P00PI3_n970VariavelCocomo_Alto, P00PI3_A969VariavelCocomo_Nominal,
               P00PI3_n969VariavelCocomo_Nominal, P00PI3_A968VariavelCocomo_Baixo, P00PI3_n968VariavelCocomo_Baixo, P00PI3_A967VariavelCocomo_MuitoBaixo, P00PI3_n967VariavelCocomo_MuitoBaixo, P00PI3_A986VariavelCocomo_ExtraBaixo, P00PI3_n986VariavelCocomo_ExtraBaixo, P00PI3_A966VariavelCocomo_Data, P00PI3_n966VariavelCocomo_Data, P00PI3_A965VariavelCocomo_ProjetoCod,
               P00PI3_n965VariavelCocomo_ProjetoCod, P00PI3_A964VariavelCocomo_Tipo, P00PI3_A962VariavelCocomo_Sigla, P00PI3_A961VariavelCocomo_AreaTrabalhoCod, P00PI3_A992VariavelCocomo_Sequencial
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV57DynamicFiltersOperator1 ;
      private short AV61DynamicFiltersOperator2 ;
      private short AV65DynamicFiltersOperator3 ;
      private short A992VariavelCocomo_Sequencial ;
      private int AV69GXV1 ;
      private int AV10TFVariavelCocomo_AreaTrabalhoCod ;
      private int AV11TFVariavelCocomo_AreaTrabalhoCod_To ;
      private int AV18TFVariavelCocomo_ProjetoCod ;
      private int AV19TFVariavelCocomo_ProjetoCod_To ;
      private int AV17TFVariavelCocomo_Tipo_Sels_Count ;
      private int A961VariavelCocomo_AreaTrabalhoCod ;
      private int A965VariavelCocomo_ProjetoCod ;
      private long AV50count ;
      private decimal AV22TFVariavelCocomo_ExtraBaixo ;
      private decimal AV23TFVariavelCocomo_ExtraBaixo_To ;
      private decimal AV24TFVariavelCocomo_MuitoBaixo ;
      private decimal AV25TFVariavelCocomo_MuitoBaixo_To ;
      private decimal AV26TFVariavelCocomo_Baixo ;
      private decimal AV27TFVariavelCocomo_Baixo_To ;
      private decimal AV28TFVariavelCocomo_Nominal ;
      private decimal AV29TFVariavelCocomo_Nominal_To ;
      private decimal AV30TFVariavelCocomo_Alto ;
      private decimal AV31TFVariavelCocomo_Alto_To ;
      private decimal AV32TFVariavelCocomo_MuitoAlto ;
      private decimal AV33TFVariavelCocomo_MuitoAlto_To ;
      private decimal AV34TFVariavelCocomo_ExtraAlto ;
      private decimal AV35TFVariavelCocomo_ExtraAlto_To ;
      private decimal AV36TFVariavelCocomo_Usado ;
      private decimal AV37TFVariavelCocomo_Usado_To ;
      private decimal A986VariavelCocomo_ExtraBaixo ;
      private decimal A967VariavelCocomo_MuitoBaixo ;
      private decimal A968VariavelCocomo_Baixo ;
      private decimal A969VariavelCocomo_Nominal ;
      private decimal A970VariavelCocomo_Alto ;
      private decimal A971VariavelCocomo_MuitoAlto ;
      private decimal A972VariavelCocomo_ExtraAlto ;
      private decimal A973VariavelCocomo_Usado ;
      private String AV12TFVariavelCocomo_Sigla ;
      private String AV13TFVariavelCocomo_Sigla_Sel ;
      private String AV14TFVariavelCocomo_Nome ;
      private String AV15TFVariavelCocomo_Nome_Sel ;
      private String AV58VariavelCocomo_Nome1 ;
      private String AV62VariavelCocomo_Nome2 ;
      private String AV66VariavelCocomo_Nome3 ;
      private String scmdbuf ;
      private String lV58VariavelCocomo_Nome1 ;
      private String lV62VariavelCocomo_Nome2 ;
      private String lV66VariavelCocomo_Nome3 ;
      private String lV12TFVariavelCocomo_Sigla ;
      private String lV14TFVariavelCocomo_Nome ;
      private String A964VariavelCocomo_Tipo ;
      private String A963VariavelCocomo_Nome ;
      private String A962VariavelCocomo_Sigla ;
      private DateTime AV20TFVariavelCocomo_Data ;
      private DateTime AV21TFVariavelCocomo_Data_To ;
      private DateTime A966VariavelCocomo_Data ;
      private bool returnInSub ;
      private bool AV59DynamicFiltersEnabled2 ;
      private bool AV63DynamicFiltersEnabled3 ;
      private bool BRKPI2 ;
      private bool n973VariavelCocomo_Usado ;
      private bool n972VariavelCocomo_ExtraAlto ;
      private bool n971VariavelCocomo_MuitoAlto ;
      private bool n970VariavelCocomo_Alto ;
      private bool n969VariavelCocomo_Nominal ;
      private bool n968VariavelCocomo_Baixo ;
      private bool n967VariavelCocomo_MuitoBaixo ;
      private bool n986VariavelCocomo_ExtraBaixo ;
      private bool n966VariavelCocomo_Data ;
      private bool n965VariavelCocomo_ProjetoCod ;
      private bool BRKPI4 ;
      private String AV49OptionIndexesJson ;
      private String AV44OptionsJson ;
      private String AV47OptionsDescJson ;
      private String AV16TFVariavelCocomo_Tipo_SelsJson ;
      private String AV40DDOName ;
      private String AV38SearchTxt ;
      private String AV39SearchTxtTo ;
      private String AV56DynamicFiltersSelector1 ;
      private String AV60DynamicFiltersSelector2 ;
      private String AV64DynamicFiltersSelector3 ;
      private String AV42Option ;
      private IGxSession AV51Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private String[] P00PI2_A962VariavelCocomo_Sigla ;
      private decimal[] P00PI2_A973VariavelCocomo_Usado ;
      private bool[] P00PI2_n973VariavelCocomo_Usado ;
      private decimal[] P00PI2_A972VariavelCocomo_ExtraAlto ;
      private bool[] P00PI2_n972VariavelCocomo_ExtraAlto ;
      private decimal[] P00PI2_A971VariavelCocomo_MuitoAlto ;
      private bool[] P00PI2_n971VariavelCocomo_MuitoAlto ;
      private decimal[] P00PI2_A970VariavelCocomo_Alto ;
      private bool[] P00PI2_n970VariavelCocomo_Alto ;
      private decimal[] P00PI2_A969VariavelCocomo_Nominal ;
      private bool[] P00PI2_n969VariavelCocomo_Nominal ;
      private decimal[] P00PI2_A968VariavelCocomo_Baixo ;
      private bool[] P00PI2_n968VariavelCocomo_Baixo ;
      private decimal[] P00PI2_A967VariavelCocomo_MuitoBaixo ;
      private bool[] P00PI2_n967VariavelCocomo_MuitoBaixo ;
      private decimal[] P00PI2_A986VariavelCocomo_ExtraBaixo ;
      private bool[] P00PI2_n986VariavelCocomo_ExtraBaixo ;
      private DateTime[] P00PI2_A966VariavelCocomo_Data ;
      private bool[] P00PI2_n966VariavelCocomo_Data ;
      private int[] P00PI2_A965VariavelCocomo_ProjetoCod ;
      private bool[] P00PI2_n965VariavelCocomo_ProjetoCod ;
      private String[] P00PI2_A964VariavelCocomo_Tipo ;
      private int[] P00PI2_A961VariavelCocomo_AreaTrabalhoCod ;
      private String[] P00PI2_A963VariavelCocomo_Nome ;
      private short[] P00PI2_A992VariavelCocomo_Sequencial ;
      private String[] P00PI3_A963VariavelCocomo_Nome ;
      private decimal[] P00PI3_A973VariavelCocomo_Usado ;
      private bool[] P00PI3_n973VariavelCocomo_Usado ;
      private decimal[] P00PI3_A972VariavelCocomo_ExtraAlto ;
      private bool[] P00PI3_n972VariavelCocomo_ExtraAlto ;
      private decimal[] P00PI3_A971VariavelCocomo_MuitoAlto ;
      private bool[] P00PI3_n971VariavelCocomo_MuitoAlto ;
      private decimal[] P00PI3_A970VariavelCocomo_Alto ;
      private bool[] P00PI3_n970VariavelCocomo_Alto ;
      private decimal[] P00PI3_A969VariavelCocomo_Nominal ;
      private bool[] P00PI3_n969VariavelCocomo_Nominal ;
      private decimal[] P00PI3_A968VariavelCocomo_Baixo ;
      private bool[] P00PI3_n968VariavelCocomo_Baixo ;
      private decimal[] P00PI3_A967VariavelCocomo_MuitoBaixo ;
      private bool[] P00PI3_n967VariavelCocomo_MuitoBaixo ;
      private decimal[] P00PI3_A986VariavelCocomo_ExtraBaixo ;
      private bool[] P00PI3_n986VariavelCocomo_ExtraBaixo ;
      private DateTime[] P00PI3_A966VariavelCocomo_Data ;
      private bool[] P00PI3_n966VariavelCocomo_Data ;
      private int[] P00PI3_A965VariavelCocomo_ProjetoCod ;
      private bool[] P00PI3_n965VariavelCocomo_ProjetoCod ;
      private String[] P00PI3_A964VariavelCocomo_Tipo ;
      private String[] P00PI3_A962VariavelCocomo_Sigla ;
      private int[] P00PI3_A961VariavelCocomo_AreaTrabalhoCod ;
      private short[] P00PI3_A992VariavelCocomo_Sequencial ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV17TFVariavelCocomo_Tipo_Sels ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV43Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV46OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV48OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV53GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV54GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV55GridStateDynamicFilter ;
   }

   public class getpromptvariaveiscocomofilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00PI2( IGxContext context ,
                                             String A964VariavelCocomo_Tipo ,
                                             IGxCollection AV17TFVariavelCocomo_Tipo_Sels ,
                                             String AV56DynamicFiltersSelector1 ,
                                             short AV57DynamicFiltersOperator1 ,
                                             String AV58VariavelCocomo_Nome1 ,
                                             bool AV59DynamicFiltersEnabled2 ,
                                             String AV60DynamicFiltersSelector2 ,
                                             short AV61DynamicFiltersOperator2 ,
                                             String AV62VariavelCocomo_Nome2 ,
                                             bool AV63DynamicFiltersEnabled3 ,
                                             String AV64DynamicFiltersSelector3 ,
                                             short AV65DynamicFiltersOperator3 ,
                                             String AV66VariavelCocomo_Nome3 ,
                                             int AV10TFVariavelCocomo_AreaTrabalhoCod ,
                                             int AV11TFVariavelCocomo_AreaTrabalhoCod_To ,
                                             String AV13TFVariavelCocomo_Sigla_Sel ,
                                             String AV12TFVariavelCocomo_Sigla ,
                                             String AV15TFVariavelCocomo_Nome_Sel ,
                                             String AV14TFVariavelCocomo_Nome ,
                                             int AV17TFVariavelCocomo_Tipo_Sels_Count ,
                                             int AV18TFVariavelCocomo_ProjetoCod ,
                                             int AV19TFVariavelCocomo_ProjetoCod_To ,
                                             DateTime AV20TFVariavelCocomo_Data ,
                                             DateTime AV21TFVariavelCocomo_Data_To ,
                                             decimal AV22TFVariavelCocomo_ExtraBaixo ,
                                             decimal AV23TFVariavelCocomo_ExtraBaixo_To ,
                                             decimal AV24TFVariavelCocomo_MuitoBaixo ,
                                             decimal AV25TFVariavelCocomo_MuitoBaixo_To ,
                                             decimal AV26TFVariavelCocomo_Baixo ,
                                             decimal AV27TFVariavelCocomo_Baixo_To ,
                                             decimal AV28TFVariavelCocomo_Nominal ,
                                             decimal AV29TFVariavelCocomo_Nominal_To ,
                                             decimal AV30TFVariavelCocomo_Alto ,
                                             decimal AV31TFVariavelCocomo_Alto_To ,
                                             decimal AV32TFVariavelCocomo_MuitoAlto ,
                                             decimal AV33TFVariavelCocomo_MuitoAlto_To ,
                                             decimal AV34TFVariavelCocomo_ExtraAlto ,
                                             decimal AV35TFVariavelCocomo_ExtraAlto_To ,
                                             decimal AV36TFVariavelCocomo_Usado ,
                                             decimal AV37TFVariavelCocomo_Usado_To ,
                                             String A963VariavelCocomo_Nome ,
                                             int A961VariavelCocomo_AreaTrabalhoCod ,
                                             String A962VariavelCocomo_Sigla ,
                                             int A965VariavelCocomo_ProjetoCod ,
                                             DateTime A966VariavelCocomo_Data ,
                                             decimal A986VariavelCocomo_ExtraBaixo ,
                                             decimal A967VariavelCocomo_MuitoBaixo ,
                                             decimal A968VariavelCocomo_Baixo ,
                                             decimal A969VariavelCocomo_Nominal ,
                                             decimal A970VariavelCocomo_Alto ,
                                             decimal A971VariavelCocomo_MuitoAlto ,
                                             decimal A972VariavelCocomo_ExtraAlto ,
                                             decimal A973VariavelCocomo_Usado )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [32] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT [VariavelCocomo_Sigla], [VariavelCocomo_Usado], [VariavelCocomo_ExtraAlto], [VariavelCocomo_MuitoAlto], [VariavelCocomo_Alto], [VariavelCocomo_Nominal], [VariavelCocomo_Baixo], [VariavelCocomo_MuitoBaixo], [VariavelCocomo_ExtraBaixo], [VariavelCocomo_Data], [VariavelCocomo_ProjetoCod], [VariavelCocomo_Tipo], [VariavelCocomo_AreaTrabalhoCod], [VariavelCocomo_Nome], [VariavelCocomo_Sequencial] FROM [VariaveisCocomo] WITH (NOLOCK)";
         if ( ( StringUtil.StrCmp(AV56DynamicFiltersSelector1, "VARIAVELCOCOMO_NOME") == 0 ) && ( AV57DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV58VariavelCocomo_Nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([VariavelCocomo_Nome] like @lV58VariavelCocomo_Nome1)";
            }
            else
            {
               sWhereString = sWhereString + " ([VariavelCocomo_Nome] like @lV58VariavelCocomo_Nome1)";
            }
         }
         else
         {
            GXv_int1[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV56DynamicFiltersSelector1, "VARIAVELCOCOMO_NOME") == 0 ) && ( AV57DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV58VariavelCocomo_Nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([VariavelCocomo_Nome] like '%' + @lV58VariavelCocomo_Nome1)";
            }
            else
            {
               sWhereString = sWhereString + " ([VariavelCocomo_Nome] like '%' + @lV58VariavelCocomo_Nome1)";
            }
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( AV59DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV60DynamicFiltersSelector2, "VARIAVELCOCOMO_NOME") == 0 ) && ( AV61DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV62VariavelCocomo_Nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([VariavelCocomo_Nome] like @lV62VariavelCocomo_Nome2)";
            }
            else
            {
               sWhereString = sWhereString + " ([VariavelCocomo_Nome] like @lV62VariavelCocomo_Nome2)";
            }
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( AV59DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV60DynamicFiltersSelector2, "VARIAVELCOCOMO_NOME") == 0 ) && ( AV61DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV62VariavelCocomo_Nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([VariavelCocomo_Nome] like '%' + @lV62VariavelCocomo_Nome2)";
            }
            else
            {
               sWhereString = sWhereString + " ([VariavelCocomo_Nome] like '%' + @lV62VariavelCocomo_Nome2)";
            }
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( AV63DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV64DynamicFiltersSelector3, "VARIAVELCOCOMO_NOME") == 0 ) && ( AV65DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV66VariavelCocomo_Nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([VariavelCocomo_Nome] like @lV66VariavelCocomo_Nome3)";
            }
            else
            {
               sWhereString = sWhereString + " ([VariavelCocomo_Nome] like @lV66VariavelCocomo_Nome3)";
            }
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( AV63DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV64DynamicFiltersSelector3, "VARIAVELCOCOMO_NOME") == 0 ) && ( AV65DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV66VariavelCocomo_Nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([VariavelCocomo_Nome] like '%' + @lV66VariavelCocomo_Nome3)";
            }
            else
            {
               sWhereString = sWhereString + " ([VariavelCocomo_Nome] like '%' + @lV66VariavelCocomo_Nome3)";
            }
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( ! (0==AV10TFVariavelCocomo_AreaTrabalhoCod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([VariavelCocomo_AreaTrabalhoCod] >= @AV10TFVariavelCocomo_AreaTrabalhoCod)";
            }
            else
            {
               sWhereString = sWhereString + " ([VariavelCocomo_AreaTrabalhoCod] >= @AV10TFVariavelCocomo_AreaTrabalhoCod)";
            }
         }
         else
         {
            GXv_int1[6] = 1;
         }
         if ( ! (0==AV11TFVariavelCocomo_AreaTrabalhoCod_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([VariavelCocomo_AreaTrabalhoCod] <= @AV11TFVariavelCocomo_AreaTrabalhoCod_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([VariavelCocomo_AreaTrabalhoCod] <= @AV11TFVariavelCocomo_AreaTrabalhoCod_To)";
            }
         }
         else
         {
            GXv_int1[7] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV13TFVariavelCocomo_Sigla_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV12TFVariavelCocomo_Sigla)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([VariavelCocomo_Sigla] like @lV12TFVariavelCocomo_Sigla)";
            }
            else
            {
               sWhereString = sWhereString + " ([VariavelCocomo_Sigla] like @lV12TFVariavelCocomo_Sigla)";
            }
         }
         else
         {
            GXv_int1[8] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV13TFVariavelCocomo_Sigla_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([VariavelCocomo_Sigla] = @AV13TFVariavelCocomo_Sigla_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([VariavelCocomo_Sigla] = @AV13TFVariavelCocomo_Sigla_Sel)";
            }
         }
         else
         {
            GXv_int1[9] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV15TFVariavelCocomo_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV14TFVariavelCocomo_Nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([VariavelCocomo_Nome] like @lV14TFVariavelCocomo_Nome)";
            }
            else
            {
               sWhereString = sWhereString + " ([VariavelCocomo_Nome] like @lV14TFVariavelCocomo_Nome)";
            }
         }
         else
         {
            GXv_int1[10] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV15TFVariavelCocomo_Nome_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([VariavelCocomo_Nome] = @AV15TFVariavelCocomo_Nome_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([VariavelCocomo_Nome] = @AV15TFVariavelCocomo_Nome_Sel)";
            }
         }
         else
         {
            GXv_int1[11] = 1;
         }
         if ( AV17TFVariavelCocomo_Tipo_Sels_Count > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV17TFVariavelCocomo_Tipo_Sels, "[VariavelCocomo_Tipo] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV17TFVariavelCocomo_Tipo_Sels, "[VariavelCocomo_Tipo] IN (", ")") + ")";
            }
         }
         if ( ! (0==AV18TFVariavelCocomo_ProjetoCod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([VariavelCocomo_ProjetoCod] >= @AV18TFVariavelCocomo_ProjetoCod)";
            }
            else
            {
               sWhereString = sWhereString + " ([VariavelCocomo_ProjetoCod] >= @AV18TFVariavelCocomo_ProjetoCod)";
            }
         }
         else
         {
            GXv_int1[12] = 1;
         }
         if ( ! (0==AV19TFVariavelCocomo_ProjetoCod_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([VariavelCocomo_ProjetoCod] <= @AV19TFVariavelCocomo_ProjetoCod_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([VariavelCocomo_ProjetoCod] <= @AV19TFVariavelCocomo_ProjetoCod_To)";
            }
         }
         else
         {
            GXv_int1[13] = 1;
         }
         if ( ! (DateTime.MinValue==AV20TFVariavelCocomo_Data) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([VariavelCocomo_Data] >= @AV20TFVariavelCocomo_Data)";
            }
            else
            {
               sWhereString = sWhereString + " ([VariavelCocomo_Data] >= @AV20TFVariavelCocomo_Data)";
            }
         }
         else
         {
            GXv_int1[14] = 1;
         }
         if ( ! (DateTime.MinValue==AV21TFVariavelCocomo_Data_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([VariavelCocomo_Data] <= @AV21TFVariavelCocomo_Data_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([VariavelCocomo_Data] <= @AV21TFVariavelCocomo_Data_To)";
            }
         }
         else
         {
            GXv_int1[15] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV22TFVariavelCocomo_ExtraBaixo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([VariavelCocomo_ExtraBaixo] >= @AV22TFVariavelCocomo_ExtraBaixo)";
            }
            else
            {
               sWhereString = sWhereString + " ([VariavelCocomo_ExtraBaixo] >= @AV22TFVariavelCocomo_ExtraBaixo)";
            }
         }
         else
         {
            GXv_int1[16] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV23TFVariavelCocomo_ExtraBaixo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([VariavelCocomo_ExtraBaixo] <= @AV23TFVariavelCocomo_ExtraBaixo_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([VariavelCocomo_ExtraBaixo] <= @AV23TFVariavelCocomo_ExtraBaixo_To)";
            }
         }
         else
         {
            GXv_int1[17] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV24TFVariavelCocomo_MuitoBaixo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([VariavelCocomo_MuitoBaixo] >= @AV24TFVariavelCocomo_MuitoBaixo)";
            }
            else
            {
               sWhereString = sWhereString + " ([VariavelCocomo_MuitoBaixo] >= @AV24TFVariavelCocomo_MuitoBaixo)";
            }
         }
         else
         {
            GXv_int1[18] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV25TFVariavelCocomo_MuitoBaixo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([VariavelCocomo_MuitoBaixo] <= @AV25TFVariavelCocomo_MuitoBaixo_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([VariavelCocomo_MuitoBaixo] <= @AV25TFVariavelCocomo_MuitoBaixo_To)";
            }
         }
         else
         {
            GXv_int1[19] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV26TFVariavelCocomo_Baixo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([VariavelCocomo_Baixo] >= @AV26TFVariavelCocomo_Baixo)";
            }
            else
            {
               sWhereString = sWhereString + " ([VariavelCocomo_Baixo] >= @AV26TFVariavelCocomo_Baixo)";
            }
         }
         else
         {
            GXv_int1[20] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV27TFVariavelCocomo_Baixo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([VariavelCocomo_Baixo] <= @AV27TFVariavelCocomo_Baixo_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([VariavelCocomo_Baixo] <= @AV27TFVariavelCocomo_Baixo_To)";
            }
         }
         else
         {
            GXv_int1[21] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV28TFVariavelCocomo_Nominal) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([VariavelCocomo_Nominal] >= @AV28TFVariavelCocomo_Nominal)";
            }
            else
            {
               sWhereString = sWhereString + " ([VariavelCocomo_Nominal] >= @AV28TFVariavelCocomo_Nominal)";
            }
         }
         else
         {
            GXv_int1[22] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV29TFVariavelCocomo_Nominal_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([VariavelCocomo_Nominal] <= @AV29TFVariavelCocomo_Nominal_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([VariavelCocomo_Nominal] <= @AV29TFVariavelCocomo_Nominal_To)";
            }
         }
         else
         {
            GXv_int1[23] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV30TFVariavelCocomo_Alto) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([VariavelCocomo_Alto] >= @AV30TFVariavelCocomo_Alto)";
            }
            else
            {
               sWhereString = sWhereString + " ([VariavelCocomo_Alto] >= @AV30TFVariavelCocomo_Alto)";
            }
         }
         else
         {
            GXv_int1[24] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV31TFVariavelCocomo_Alto_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([VariavelCocomo_Alto] <= @AV31TFVariavelCocomo_Alto_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([VariavelCocomo_Alto] <= @AV31TFVariavelCocomo_Alto_To)";
            }
         }
         else
         {
            GXv_int1[25] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV32TFVariavelCocomo_MuitoAlto) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([VariavelCocomo_MuitoAlto] >= @AV32TFVariavelCocomo_MuitoAlto)";
            }
            else
            {
               sWhereString = sWhereString + " ([VariavelCocomo_MuitoAlto] >= @AV32TFVariavelCocomo_MuitoAlto)";
            }
         }
         else
         {
            GXv_int1[26] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV33TFVariavelCocomo_MuitoAlto_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([VariavelCocomo_MuitoAlto] <= @AV33TFVariavelCocomo_MuitoAlto_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([VariavelCocomo_MuitoAlto] <= @AV33TFVariavelCocomo_MuitoAlto_To)";
            }
         }
         else
         {
            GXv_int1[27] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV34TFVariavelCocomo_ExtraAlto) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([VariavelCocomo_ExtraAlto] >= @AV34TFVariavelCocomo_ExtraAlto)";
            }
            else
            {
               sWhereString = sWhereString + " ([VariavelCocomo_ExtraAlto] >= @AV34TFVariavelCocomo_ExtraAlto)";
            }
         }
         else
         {
            GXv_int1[28] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV35TFVariavelCocomo_ExtraAlto_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([VariavelCocomo_ExtraAlto] <= @AV35TFVariavelCocomo_ExtraAlto_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([VariavelCocomo_ExtraAlto] <= @AV35TFVariavelCocomo_ExtraAlto_To)";
            }
         }
         else
         {
            GXv_int1[29] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV36TFVariavelCocomo_Usado) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([VariavelCocomo_Usado] >= @AV36TFVariavelCocomo_Usado)";
            }
            else
            {
               sWhereString = sWhereString + " ([VariavelCocomo_Usado] >= @AV36TFVariavelCocomo_Usado)";
            }
         }
         else
         {
            GXv_int1[30] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV37TFVariavelCocomo_Usado_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([VariavelCocomo_Usado] <= @AV37TFVariavelCocomo_Usado_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([VariavelCocomo_Usado] <= @AV37TFVariavelCocomo_Usado_To)";
            }
         }
         else
         {
            GXv_int1[31] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY [VariavelCocomo_Sigla]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      protected Object[] conditional_P00PI3( IGxContext context ,
                                             String A964VariavelCocomo_Tipo ,
                                             IGxCollection AV17TFVariavelCocomo_Tipo_Sels ,
                                             String AV56DynamicFiltersSelector1 ,
                                             short AV57DynamicFiltersOperator1 ,
                                             String AV58VariavelCocomo_Nome1 ,
                                             bool AV59DynamicFiltersEnabled2 ,
                                             String AV60DynamicFiltersSelector2 ,
                                             short AV61DynamicFiltersOperator2 ,
                                             String AV62VariavelCocomo_Nome2 ,
                                             bool AV63DynamicFiltersEnabled3 ,
                                             String AV64DynamicFiltersSelector3 ,
                                             short AV65DynamicFiltersOperator3 ,
                                             String AV66VariavelCocomo_Nome3 ,
                                             int AV10TFVariavelCocomo_AreaTrabalhoCod ,
                                             int AV11TFVariavelCocomo_AreaTrabalhoCod_To ,
                                             String AV13TFVariavelCocomo_Sigla_Sel ,
                                             String AV12TFVariavelCocomo_Sigla ,
                                             String AV15TFVariavelCocomo_Nome_Sel ,
                                             String AV14TFVariavelCocomo_Nome ,
                                             int AV17TFVariavelCocomo_Tipo_Sels_Count ,
                                             int AV18TFVariavelCocomo_ProjetoCod ,
                                             int AV19TFVariavelCocomo_ProjetoCod_To ,
                                             DateTime AV20TFVariavelCocomo_Data ,
                                             DateTime AV21TFVariavelCocomo_Data_To ,
                                             decimal AV22TFVariavelCocomo_ExtraBaixo ,
                                             decimal AV23TFVariavelCocomo_ExtraBaixo_To ,
                                             decimal AV24TFVariavelCocomo_MuitoBaixo ,
                                             decimal AV25TFVariavelCocomo_MuitoBaixo_To ,
                                             decimal AV26TFVariavelCocomo_Baixo ,
                                             decimal AV27TFVariavelCocomo_Baixo_To ,
                                             decimal AV28TFVariavelCocomo_Nominal ,
                                             decimal AV29TFVariavelCocomo_Nominal_To ,
                                             decimal AV30TFVariavelCocomo_Alto ,
                                             decimal AV31TFVariavelCocomo_Alto_To ,
                                             decimal AV32TFVariavelCocomo_MuitoAlto ,
                                             decimal AV33TFVariavelCocomo_MuitoAlto_To ,
                                             decimal AV34TFVariavelCocomo_ExtraAlto ,
                                             decimal AV35TFVariavelCocomo_ExtraAlto_To ,
                                             decimal AV36TFVariavelCocomo_Usado ,
                                             decimal AV37TFVariavelCocomo_Usado_To ,
                                             String A963VariavelCocomo_Nome ,
                                             int A961VariavelCocomo_AreaTrabalhoCod ,
                                             String A962VariavelCocomo_Sigla ,
                                             int A965VariavelCocomo_ProjetoCod ,
                                             DateTime A966VariavelCocomo_Data ,
                                             decimal A986VariavelCocomo_ExtraBaixo ,
                                             decimal A967VariavelCocomo_MuitoBaixo ,
                                             decimal A968VariavelCocomo_Baixo ,
                                             decimal A969VariavelCocomo_Nominal ,
                                             decimal A970VariavelCocomo_Alto ,
                                             decimal A971VariavelCocomo_MuitoAlto ,
                                             decimal A972VariavelCocomo_ExtraAlto ,
                                             decimal A973VariavelCocomo_Usado )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [32] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT [VariavelCocomo_Nome], [VariavelCocomo_Usado], [VariavelCocomo_ExtraAlto], [VariavelCocomo_MuitoAlto], [VariavelCocomo_Alto], [VariavelCocomo_Nominal], [VariavelCocomo_Baixo], [VariavelCocomo_MuitoBaixo], [VariavelCocomo_ExtraBaixo], [VariavelCocomo_Data], [VariavelCocomo_ProjetoCod], [VariavelCocomo_Tipo], [VariavelCocomo_Sigla], [VariavelCocomo_AreaTrabalhoCod], [VariavelCocomo_Sequencial] FROM [VariaveisCocomo] WITH (NOLOCK)";
         if ( ( StringUtil.StrCmp(AV56DynamicFiltersSelector1, "VARIAVELCOCOMO_NOME") == 0 ) && ( AV57DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV58VariavelCocomo_Nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([VariavelCocomo_Nome] like @lV58VariavelCocomo_Nome1)";
            }
            else
            {
               sWhereString = sWhereString + " ([VariavelCocomo_Nome] like @lV58VariavelCocomo_Nome1)";
            }
         }
         else
         {
            GXv_int3[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV56DynamicFiltersSelector1, "VARIAVELCOCOMO_NOME") == 0 ) && ( AV57DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV58VariavelCocomo_Nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([VariavelCocomo_Nome] like '%' + @lV58VariavelCocomo_Nome1)";
            }
            else
            {
               sWhereString = sWhereString + " ([VariavelCocomo_Nome] like '%' + @lV58VariavelCocomo_Nome1)";
            }
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( AV59DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV60DynamicFiltersSelector2, "VARIAVELCOCOMO_NOME") == 0 ) && ( AV61DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV62VariavelCocomo_Nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([VariavelCocomo_Nome] like @lV62VariavelCocomo_Nome2)";
            }
            else
            {
               sWhereString = sWhereString + " ([VariavelCocomo_Nome] like @lV62VariavelCocomo_Nome2)";
            }
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( AV59DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV60DynamicFiltersSelector2, "VARIAVELCOCOMO_NOME") == 0 ) && ( AV61DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV62VariavelCocomo_Nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([VariavelCocomo_Nome] like '%' + @lV62VariavelCocomo_Nome2)";
            }
            else
            {
               sWhereString = sWhereString + " ([VariavelCocomo_Nome] like '%' + @lV62VariavelCocomo_Nome2)";
            }
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( AV63DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV64DynamicFiltersSelector3, "VARIAVELCOCOMO_NOME") == 0 ) && ( AV65DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV66VariavelCocomo_Nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([VariavelCocomo_Nome] like @lV66VariavelCocomo_Nome3)";
            }
            else
            {
               sWhereString = sWhereString + " ([VariavelCocomo_Nome] like @lV66VariavelCocomo_Nome3)";
            }
         }
         else
         {
            GXv_int3[4] = 1;
         }
         if ( AV63DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV64DynamicFiltersSelector3, "VARIAVELCOCOMO_NOME") == 0 ) && ( AV65DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV66VariavelCocomo_Nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([VariavelCocomo_Nome] like '%' + @lV66VariavelCocomo_Nome3)";
            }
            else
            {
               sWhereString = sWhereString + " ([VariavelCocomo_Nome] like '%' + @lV66VariavelCocomo_Nome3)";
            }
         }
         else
         {
            GXv_int3[5] = 1;
         }
         if ( ! (0==AV10TFVariavelCocomo_AreaTrabalhoCod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([VariavelCocomo_AreaTrabalhoCod] >= @AV10TFVariavelCocomo_AreaTrabalhoCod)";
            }
            else
            {
               sWhereString = sWhereString + " ([VariavelCocomo_AreaTrabalhoCod] >= @AV10TFVariavelCocomo_AreaTrabalhoCod)";
            }
         }
         else
         {
            GXv_int3[6] = 1;
         }
         if ( ! (0==AV11TFVariavelCocomo_AreaTrabalhoCod_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([VariavelCocomo_AreaTrabalhoCod] <= @AV11TFVariavelCocomo_AreaTrabalhoCod_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([VariavelCocomo_AreaTrabalhoCod] <= @AV11TFVariavelCocomo_AreaTrabalhoCod_To)";
            }
         }
         else
         {
            GXv_int3[7] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV13TFVariavelCocomo_Sigla_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV12TFVariavelCocomo_Sigla)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([VariavelCocomo_Sigla] like @lV12TFVariavelCocomo_Sigla)";
            }
            else
            {
               sWhereString = sWhereString + " ([VariavelCocomo_Sigla] like @lV12TFVariavelCocomo_Sigla)";
            }
         }
         else
         {
            GXv_int3[8] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV13TFVariavelCocomo_Sigla_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([VariavelCocomo_Sigla] = @AV13TFVariavelCocomo_Sigla_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([VariavelCocomo_Sigla] = @AV13TFVariavelCocomo_Sigla_Sel)";
            }
         }
         else
         {
            GXv_int3[9] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV15TFVariavelCocomo_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV14TFVariavelCocomo_Nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([VariavelCocomo_Nome] like @lV14TFVariavelCocomo_Nome)";
            }
            else
            {
               sWhereString = sWhereString + " ([VariavelCocomo_Nome] like @lV14TFVariavelCocomo_Nome)";
            }
         }
         else
         {
            GXv_int3[10] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV15TFVariavelCocomo_Nome_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([VariavelCocomo_Nome] = @AV15TFVariavelCocomo_Nome_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([VariavelCocomo_Nome] = @AV15TFVariavelCocomo_Nome_Sel)";
            }
         }
         else
         {
            GXv_int3[11] = 1;
         }
         if ( AV17TFVariavelCocomo_Tipo_Sels_Count > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV17TFVariavelCocomo_Tipo_Sels, "[VariavelCocomo_Tipo] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV17TFVariavelCocomo_Tipo_Sels, "[VariavelCocomo_Tipo] IN (", ")") + ")";
            }
         }
         if ( ! (0==AV18TFVariavelCocomo_ProjetoCod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([VariavelCocomo_ProjetoCod] >= @AV18TFVariavelCocomo_ProjetoCod)";
            }
            else
            {
               sWhereString = sWhereString + " ([VariavelCocomo_ProjetoCod] >= @AV18TFVariavelCocomo_ProjetoCod)";
            }
         }
         else
         {
            GXv_int3[12] = 1;
         }
         if ( ! (0==AV19TFVariavelCocomo_ProjetoCod_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([VariavelCocomo_ProjetoCod] <= @AV19TFVariavelCocomo_ProjetoCod_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([VariavelCocomo_ProjetoCod] <= @AV19TFVariavelCocomo_ProjetoCod_To)";
            }
         }
         else
         {
            GXv_int3[13] = 1;
         }
         if ( ! (DateTime.MinValue==AV20TFVariavelCocomo_Data) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([VariavelCocomo_Data] >= @AV20TFVariavelCocomo_Data)";
            }
            else
            {
               sWhereString = sWhereString + " ([VariavelCocomo_Data] >= @AV20TFVariavelCocomo_Data)";
            }
         }
         else
         {
            GXv_int3[14] = 1;
         }
         if ( ! (DateTime.MinValue==AV21TFVariavelCocomo_Data_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([VariavelCocomo_Data] <= @AV21TFVariavelCocomo_Data_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([VariavelCocomo_Data] <= @AV21TFVariavelCocomo_Data_To)";
            }
         }
         else
         {
            GXv_int3[15] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV22TFVariavelCocomo_ExtraBaixo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([VariavelCocomo_ExtraBaixo] >= @AV22TFVariavelCocomo_ExtraBaixo)";
            }
            else
            {
               sWhereString = sWhereString + " ([VariavelCocomo_ExtraBaixo] >= @AV22TFVariavelCocomo_ExtraBaixo)";
            }
         }
         else
         {
            GXv_int3[16] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV23TFVariavelCocomo_ExtraBaixo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([VariavelCocomo_ExtraBaixo] <= @AV23TFVariavelCocomo_ExtraBaixo_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([VariavelCocomo_ExtraBaixo] <= @AV23TFVariavelCocomo_ExtraBaixo_To)";
            }
         }
         else
         {
            GXv_int3[17] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV24TFVariavelCocomo_MuitoBaixo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([VariavelCocomo_MuitoBaixo] >= @AV24TFVariavelCocomo_MuitoBaixo)";
            }
            else
            {
               sWhereString = sWhereString + " ([VariavelCocomo_MuitoBaixo] >= @AV24TFVariavelCocomo_MuitoBaixo)";
            }
         }
         else
         {
            GXv_int3[18] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV25TFVariavelCocomo_MuitoBaixo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([VariavelCocomo_MuitoBaixo] <= @AV25TFVariavelCocomo_MuitoBaixo_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([VariavelCocomo_MuitoBaixo] <= @AV25TFVariavelCocomo_MuitoBaixo_To)";
            }
         }
         else
         {
            GXv_int3[19] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV26TFVariavelCocomo_Baixo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([VariavelCocomo_Baixo] >= @AV26TFVariavelCocomo_Baixo)";
            }
            else
            {
               sWhereString = sWhereString + " ([VariavelCocomo_Baixo] >= @AV26TFVariavelCocomo_Baixo)";
            }
         }
         else
         {
            GXv_int3[20] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV27TFVariavelCocomo_Baixo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([VariavelCocomo_Baixo] <= @AV27TFVariavelCocomo_Baixo_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([VariavelCocomo_Baixo] <= @AV27TFVariavelCocomo_Baixo_To)";
            }
         }
         else
         {
            GXv_int3[21] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV28TFVariavelCocomo_Nominal) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([VariavelCocomo_Nominal] >= @AV28TFVariavelCocomo_Nominal)";
            }
            else
            {
               sWhereString = sWhereString + " ([VariavelCocomo_Nominal] >= @AV28TFVariavelCocomo_Nominal)";
            }
         }
         else
         {
            GXv_int3[22] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV29TFVariavelCocomo_Nominal_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([VariavelCocomo_Nominal] <= @AV29TFVariavelCocomo_Nominal_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([VariavelCocomo_Nominal] <= @AV29TFVariavelCocomo_Nominal_To)";
            }
         }
         else
         {
            GXv_int3[23] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV30TFVariavelCocomo_Alto) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([VariavelCocomo_Alto] >= @AV30TFVariavelCocomo_Alto)";
            }
            else
            {
               sWhereString = sWhereString + " ([VariavelCocomo_Alto] >= @AV30TFVariavelCocomo_Alto)";
            }
         }
         else
         {
            GXv_int3[24] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV31TFVariavelCocomo_Alto_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([VariavelCocomo_Alto] <= @AV31TFVariavelCocomo_Alto_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([VariavelCocomo_Alto] <= @AV31TFVariavelCocomo_Alto_To)";
            }
         }
         else
         {
            GXv_int3[25] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV32TFVariavelCocomo_MuitoAlto) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([VariavelCocomo_MuitoAlto] >= @AV32TFVariavelCocomo_MuitoAlto)";
            }
            else
            {
               sWhereString = sWhereString + " ([VariavelCocomo_MuitoAlto] >= @AV32TFVariavelCocomo_MuitoAlto)";
            }
         }
         else
         {
            GXv_int3[26] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV33TFVariavelCocomo_MuitoAlto_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([VariavelCocomo_MuitoAlto] <= @AV33TFVariavelCocomo_MuitoAlto_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([VariavelCocomo_MuitoAlto] <= @AV33TFVariavelCocomo_MuitoAlto_To)";
            }
         }
         else
         {
            GXv_int3[27] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV34TFVariavelCocomo_ExtraAlto) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([VariavelCocomo_ExtraAlto] >= @AV34TFVariavelCocomo_ExtraAlto)";
            }
            else
            {
               sWhereString = sWhereString + " ([VariavelCocomo_ExtraAlto] >= @AV34TFVariavelCocomo_ExtraAlto)";
            }
         }
         else
         {
            GXv_int3[28] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV35TFVariavelCocomo_ExtraAlto_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([VariavelCocomo_ExtraAlto] <= @AV35TFVariavelCocomo_ExtraAlto_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([VariavelCocomo_ExtraAlto] <= @AV35TFVariavelCocomo_ExtraAlto_To)";
            }
         }
         else
         {
            GXv_int3[29] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV36TFVariavelCocomo_Usado) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([VariavelCocomo_Usado] >= @AV36TFVariavelCocomo_Usado)";
            }
            else
            {
               sWhereString = sWhereString + " ([VariavelCocomo_Usado] >= @AV36TFVariavelCocomo_Usado)";
            }
         }
         else
         {
            GXv_int3[30] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV37TFVariavelCocomo_Usado_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([VariavelCocomo_Usado] <= @AV37TFVariavelCocomo_Usado_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([VariavelCocomo_Usado] <= @AV37TFVariavelCocomo_Usado_To)";
            }
         }
         else
         {
            GXv_int3[31] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY [VariavelCocomo_Nome]";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00PI2(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (short)dynConstraints[3] , (String)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (short)dynConstraints[7] , (String)dynConstraints[8] , (bool)dynConstraints[9] , (String)dynConstraints[10] , (short)dynConstraints[11] , (String)dynConstraints[12] , (int)dynConstraints[13] , (int)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (int)dynConstraints[19] , (int)dynConstraints[20] , (int)dynConstraints[21] , (DateTime)dynConstraints[22] , (DateTime)dynConstraints[23] , (decimal)dynConstraints[24] , (decimal)dynConstraints[25] , (decimal)dynConstraints[26] , (decimal)dynConstraints[27] , (decimal)dynConstraints[28] , (decimal)dynConstraints[29] , (decimal)dynConstraints[30] , (decimal)dynConstraints[31] , (decimal)dynConstraints[32] , (decimal)dynConstraints[33] , (decimal)dynConstraints[34] , (decimal)dynConstraints[35] , (decimal)dynConstraints[36] , (decimal)dynConstraints[37] , (decimal)dynConstraints[38] , (decimal)dynConstraints[39] , (String)dynConstraints[40] , (int)dynConstraints[41] , (String)dynConstraints[42] , (int)dynConstraints[43] , (DateTime)dynConstraints[44] , (decimal)dynConstraints[45] , (decimal)dynConstraints[46] , (decimal)dynConstraints[47] , (decimal)dynConstraints[48] , (decimal)dynConstraints[49] , (decimal)dynConstraints[50] , (decimal)dynConstraints[51] , (decimal)dynConstraints[52] );
               case 1 :
                     return conditional_P00PI3(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (short)dynConstraints[3] , (String)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (short)dynConstraints[7] , (String)dynConstraints[8] , (bool)dynConstraints[9] , (String)dynConstraints[10] , (short)dynConstraints[11] , (String)dynConstraints[12] , (int)dynConstraints[13] , (int)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (int)dynConstraints[19] , (int)dynConstraints[20] , (int)dynConstraints[21] , (DateTime)dynConstraints[22] , (DateTime)dynConstraints[23] , (decimal)dynConstraints[24] , (decimal)dynConstraints[25] , (decimal)dynConstraints[26] , (decimal)dynConstraints[27] , (decimal)dynConstraints[28] , (decimal)dynConstraints[29] , (decimal)dynConstraints[30] , (decimal)dynConstraints[31] , (decimal)dynConstraints[32] , (decimal)dynConstraints[33] , (decimal)dynConstraints[34] , (decimal)dynConstraints[35] , (decimal)dynConstraints[36] , (decimal)dynConstraints[37] , (decimal)dynConstraints[38] , (decimal)dynConstraints[39] , (String)dynConstraints[40] , (int)dynConstraints[41] , (String)dynConstraints[42] , (int)dynConstraints[43] , (DateTime)dynConstraints[44] , (decimal)dynConstraints[45] , (decimal)dynConstraints[46] , (decimal)dynConstraints[47] , (decimal)dynConstraints[48] , (decimal)dynConstraints[49] , (decimal)dynConstraints[50] , (decimal)dynConstraints[51] , (decimal)dynConstraints[52] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00PI2 ;
          prmP00PI2 = new Object[] {
          new Object[] {"@lV58VariavelCocomo_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV58VariavelCocomo_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV62VariavelCocomo_Nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV62VariavelCocomo_Nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV66VariavelCocomo_Nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV66VariavelCocomo_Nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@AV10TFVariavelCocomo_AreaTrabalhoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV11TFVariavelCocomo_AreaTrabalhoCod_To",SqlDbType.Int,6,0} ,
          new Object[] {"@lV12TFVariavelCocomo_Sigla",SqlDbType.Char,15,0} ,
          new Object[] {"@AV13TFVariavelCocomo_Sigla_Sel",SqlDbType.Char,15,0} ,
          new Object[] {"@lV14TFVariavelCocomo_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV15TFVariavelCocomo_Nome_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@AV18TFVariavelCocomo_ProjetoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV19TFVariavelCocomo_ProjetoCod_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV20TFVariavelCocomo_Data",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV21TFVariavelCocomo_Data_To",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV22TFVariavelCocomo_ExtraBaixo",SqlDbType.Decimal,12,2} ,
          new Object[] {"@AV23TFVariavelCocomo_ExtraBaixo_To",SqlDbType.Decimal,12,2} ,
          new Object[] {"@AV24TFVariavelCocomo_MuitoBaixo",SqlDbType.Decimal,12,2} ,
          new Object[] {"@AV25TFVariavelCocomo_MuitoBaixo_To",SqlDbType.Decimal,12,2} ,
          new Object[] {"@AV26TFVariavelCocomo_Baixo",SqlDbType.Decimal,12,2} ,
          new Object[] {"@AV27TFVariavelCocomo_Baixo_To",SqlDbType.Decimal,12,2} ,
          new Object[] {"@AV28TFVariavelCocomo_Nominal",SqlDbType.Decimal,12,2} ,
          new Object[] {"@AV29TFVariavelCocomo_Nominal_To",SqlDbType.Decimal,12,2} ,
          new Object[] {"@AV30TFVariavelCocomo_Alto",SqlDbType.Decimal,12,2} ,
          new Object[] {"@AV31TFVariavelCocomo_Alto_To",SqlDbType.Decimal,12,2} ,
          new Object[] {"@AV32TFVariavelCocomo_MuitoAlto",SqlDbType.Decimal,12,2} ,
          new Object[] {"@AV33TFVariavelCocomo_MuitoAlto_To",SqlDbType.Decimal,12,2} ,
          new Object[] {"@AV34TFVariavelCocomo_ExtraAlto",SqlDbType.Decimal,12,2} ,
          new Object[] {"@AV35TFVariavelCocomo_ExtraAlto_To",SqlDbType.Decimal,12,2} ,
          new Object[] {"@AV36TFVariavelCocomo_Usado",SqlDbType.Decimal,12,2} ,
          new Object[] {"@AV37TFVariavelCocomo_Usado_To",SqlDbType.Decimal,12,2}
          } ;
          Object[] prmP00PI3 ;
          prmP00PI3 = new Object[] {
          new Object[] {"@lV58VariavelCocomo_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV58VariavelCocomo_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV62VariavelCocomo_Nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV62VariavelCocomo_Nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV66VariavelCocomo_Nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV66VariavelCocomo_Nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@AV10TFVariavelCocomo_AreaTrabalhoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV11TFVariavelCocomo_AreaTrabalhoCod_To",SqlDbType.Int,6,0} ,
          new Object[] {"@lV12TFVariavelCocomo_Sigla",SqlDbType.Char,15,0} ,
          new Object[] {"@AV13TFVariavelCocomo_Sigla_Sel",SqlDbType.Char,15,0} ,
          new Object[] {"@lV14TFVariavelCocomo_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV15TFVariavelCocomo_Nome_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@AV18TFVariavelCocomo_ProjetoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV19TFVariavelCocomo_ProjetoCod_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV20TFVariavelCocomo_Data",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV21TFVariavelCocomo_Data_To",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV22TFVariavelCocomo_ExtraBaixo",SqlDbType.Decimal,12,2} ,
          new Object[] {"@AV23TFVariavelCocomo_ExtraBaixo_To",SqlDbType.Decimal,12,2} ,
          new Object[] {"@AV24TFVariavelCocomo_MuitoBaixo",SqlDbType.Decimal,12,2} ,
          new Object[] {"@AV25TFVariavelCocomo_MuitoBaixo_To",SqlDbType.Decimal,12,2} ,
          new Object[] {"@AV26TFVariavelCocomo_Baixo",SqlDbType.Decimal,12,2} ,
          new Object[] {"@AV27TFVariavelCocomo_Baixo_To",SqlDbType.Decimal,12,2} ,
          new Object[] {"@AV28TFVariavelCocomo_Nominal",SqlDbType.Decimal,12,2} ,
          new Object[] {"@AV29TFVariavelCocomo_Nominal_To",SqlDbType.Decimal,12,2} ,
          new Object[] {"@AV30TFVariavelCocomo_Alto",SqlDbType.Decimal,12,2} ,
          new Object[] {"@AV31TFVariavelCocomo_Alto_To",SqlDbType.Decimal,12,2} ,
          new Object[] {"@AV32TFVariavelCocomo_MuitoAlto",SqlDbType.Decimal,12,2} ,
          new Object[] {"@AV33TFVariavelCocomo_MuitoAlto_To",SqlDbType.Decimal,12,2} ,
          new Object[] {"@AV34TFVariavelCocomo_ExtraAlto",SqlDbType.Decimal,12,2} ,
          new Object[] {"@AV35TFVariavelCocomo_ExtraAlto_To",SqlDbType.Decimal,12,2} ,
          new Object[] {"@AV36TFVariavelCocomo_Usado",SqlDbType.Decimal,12,2} ,
          new Object[] {"@AV37TFVariavelCocomo_Usado_To",SqlDbType.Decimal,12,2}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00PI2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00PI2,100,0,true,false )
             ,new CursorDef("P00PI3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00PI3,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getString(1, 15) ;
                ((decimal[]) buf[1])[0] = rslt.getDecimal(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((decimal[]) buf[3])[0] = rslt.getDecimal(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((decimal[]) buf[5])[0] = rslt.getDecimal(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((decimal[]) buf[7])[0] = rslt.getDecimal(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((decimal[]) buf[9])[0] = rslt.getDecimal(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((decimal[]) buf[11])[0] = rslt.getDecimal(7) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((decimal[]) buf[13])[0] = rslt.getDecimal(8) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(8);
                ((decimal[]) buf[15])[0] = rslt.getDecimal(9) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(9);
                ((DateTime[]) buf[17])[0] = rslt.getGXDate(10) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(10);
                ((int[]) buf[19])[0] = rslt.getInt(11) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(11);
                ((String[]) buf[21])[0] = rslt.getString(12, 1) ;
                ((int[]) buf[22])[0] = rslt.getInt(13) ;
                ((String[]) buf[23])[0] = rslt.getString(14, 50) ;
                ((short[]) buf[24])[0] = rslt.getShort(15) ;
                return;
             case 1 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((decimal[]) buf[1])[0] = rslt.getDecimal(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((decimal[]) buf[3])[0] = rslt.getDecimal(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((decimal[]) buf[5])[0] = rslt.getDecimal(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((decimal[]) buf[7])[0] = rslt.getDecimal(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((decimal[]) buf[9])[0] = rslt.getDecimal(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((decimal[]) buf[11])[0] = rslt.getDecimal(7) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((decimal[]) buf[13])[0] = rslt.getDecimal(8) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(8);
                ((decimal[]) buf[15])[0] = rslt.getDecimal(9) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(9);
                ((DateTime[]) buf[17])[0] = rslt.getGXDate(10) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(10);
                ((int[]) buf[19])[0] = rslt.getInt(11) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(11);
                ((String[]) buf[21])[0] = rslt.getString(12, 1) ;
                ((String[]) buf[22])[0] = rslt.getString(13, 15) ;
                ((int[]) buf[23])[0] = rslt.getInt(14) ;
                ((short[]) buf[24])[0] = rslt.getShort(15) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[38]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[39]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[40]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[41]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[42]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[43]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[44]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[45]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[46]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[47]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[48]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[49]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[50]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[51]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[52]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[53]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[54]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[55]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[56]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[57]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[58]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[59]);
                }
                if ( (short)parms[28] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[60]);
                }
                if ( (short)parms[29] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[61]);
                }
                if ( (short)parms[30] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[62]);
                }
                if ( (short)parms[31] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[63]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[38]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[39]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[40]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[41]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[42]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[43]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[44]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[45]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[46]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[47]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[48]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[49]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[50]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[51]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[52]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[53]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[54]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[55]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[56]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[57]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[58]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[59]);
                }
                if ( (short)parms[28] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[60]);
                }
                if ( (short)parms[29] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[61]);
                }
                if ( (short)parms[30] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[62]);
                }
                if ( (short)parms[31] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[63]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getpromptvariaveiscocomofilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getpromptvariaveiscocomofilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getpromptvariaveiscocomofilterdata") )
          {
             return  ;
          }
          getpromptvariaveiscocomofilterdata worker = new getpromptvariaveiscocomofilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
