/*
               File: PRC_TemGestor
        Description: Tem Gestor
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:52:3.22
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_temgestor : GXProcedure
   {
      public prc_temgestor( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_temgestor( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_Contrato_Codigo ,
                           out bool aP1_TemGestor )
      {
         this.A74Contrato_Codigo = aP0_Contrato_Codigo;
         this.AV8TemGestor = false ;
         initialize();
         executePrivate();
         aP0_Contrato_Codigo=this.A74Contrato_Codigo;
         aP1_TemGestor=this.AV8TemGestor;
      }

      public bool executeUdp( ref int aP0_Contrato_Codigo )
      {
         this.A74Contrato_Codigo = aP0_Contrato_Codigo;
         this.AV8TemGestor = false ;
         initialize();
         executePrivate();
         aP0_Contrato_Codigo=this.A74Contrato_Codigo;
         aP1_TemGestor=this.AV8TemGestor;
         return AV8TemGestor ;
      }

      public void executeSubmit( ref int aP0_Contrato_Codigo ,
                                 out bool aP1_TemGestor )
      {
         prc_temgestor objprc_temgestor;
         objprc_temgestor = new prc_temgestor();
         objprc_temgestor.A74Contrato_Codigo = aP0_Contrato_Codigo;
         objprc_temgestor.AV8TemGestor = false ;
         objprc_temgestor.context.SetSubmitInitialConfig(context);
         objprc_temgestor.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_temgestor);
         aP0_Contrato_Codigo=this.A74Contrato_Codigo;
         aP1_TemGestor=this.AV8TemGestor;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_temgestor)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P009P2 */
         pr_default.execute(0, new Object[] {A74Contrato_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A1013Contrato_PrepostoCod = P009P2_A1013Contrato_PrepostoCod[0];
            n1013Contrato_PrepostoCod = P009P2_n1013Contrato_PrepostoCod[0];
            if ( A1013Contrato_PrepostoCod > 0 )
            {
               AV8TemGestor = true;
            }
            else
            {
               AV12GXLvl6 = 0;
               /* Using cursor P009P3 */
               pr_default.execute(1, new Object[] {A74Contrato_Codigo});
               while ( (pr_default.getStatus(1) != 101) )
               {
                  A1078ContratoGestor_ContratoCod = P009P3_A1078ContratoGestor_ContratoCod[0];
                  A1079ContratoGestor_UsuarioCod = P009P3_A1079ContratoGestor_UsuarioCod[0];
                  AV12GXLvl6 = 1;
                  AV8TemGestor = true;
                  /* Exit For each command. Update data (if necessary), close cursors & exit. */
                  if (true) break;
                  pr_default.readNext(1);
               }
               pr_default.close(1);
               if ( AV12GXLvl6 == 0 )
               {
                  /* Using cursor P009P4 */
                  pr_default.execute(2, new Object[] {A74Contrato_Codigo});
                  while ( (pr_default.getStatus(2) != 101) )
                  {
                     A1824ContratoAuxiliar_ContratoCod = P009P4_A1824ContratoAuxiliar_ContratoCod[0];
                     A1825ContratoAuxiliar_UsuarioCod = P009P4_A1825ContratoAuxiliar_UsuarioCod[0];
                     AV8TemGestor = true;
                     /* Exit For each command. Update data (if necessary), close cursors & exit. */
                     if (true) break;
                     pr_default.readNext(2);
                  }
                  pr_default.close(2);
               }
            }
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P009P2_A74Contrato_Codigo = new int[1] ;
         P009P2_A1013Contrato_PrepostoCod = new int[1] ;
         P009P2_n1013Contrato_PrepostoCod = new bool[] {false} ;
         P009P3_A1078ContratoGestor_ContratoCod = new int[1] ;
         P009P3_A1079ContratoGestor_UsuarioCod = new int[1] ;
         P009P4_A1824ContratoAuxiliar_ContratoCod = new int[1] ;
         P009P4_A1825ContratoAuxiliar_UsuarioCod = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_temgestor__default(),
            new Object[][] {
                new Object[] {
               P009P2_A74Contrato_Codigo, P009P2_A1013Contrato_PrepostoCod, P009P2_n1013Contrato_PrepostoCod
               }
               , new Object[] {
               P009P3_A1078ContratoGestor_ContratoCod, P009P3_A1079ContratoGestor_UsuarioCod
               }
               , new Object[] {
               P009P4_A1824ContratoAuxiliar_ContratoCod, P009P4_A1825ContratoAuxiliar_UsuarioCod
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV12GXLvl6 ;
      private int A74Contrato_Codigo ;
      private int A1013Contrato_PrepostoCod ;
      private int A1078ContratoGestor_ContratoCod ;
      private int A1079ContratoGestor_UsuarioCod ;
      private int A1824ContratoAuxiliar_ContratoCod ;
      private int A1825ContratoAuxiliar_UsuarioCod ;
      private String scmdbuf ;
      private bool AV8TemGestor ;
      private bool n1013Contrato_PrepostoCod ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_Contrato_Codigo ;
      private IDataStoreProvider pr_default ;
      private int[] P009P2_A74Contrato_Codigo ;
      private int[] P009P2_A1013Contrato_PrepostoCod ;
      private bool[] P009P2_n1013Contrato_PrepostoCod ;
      private int[] P009P3_A1078ContratoGestor_ContratoCod ;
      private int[] P009P3_A1079ContratoGestor_UsuarioCod ;
      private int[] P009P4_A1824ContratoAuxiliar_ContratoCod ;
      private int[] P009P4_A1825ContratoAuxiliar_UsuarioCod ;
      private bool aP1_TemGestor ;
   }

   public class prc_temgestor__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP009P2 ;
          prmP009P2 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP009P3 ;
          prmP009P3 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP009P4 ;
          prmP009P4 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P009P2", "SELECT TOP 1 [Contrato_Codigo], [Contrato_PrepostoCod] FROM [Contrato] WITH (NOLOCK) WHERE [Contrato_Codigo] = @Contrato_Codigo ORDER BY [Contrato_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP009P2,1,0,true,true )
             ,new CursorDef("P009P3", "SELECT TOP 1 [ContratoGestor_ContratoCod], [ContratoGestor_UsuarioCod] FROM [ContratoGestor] WITH (NOLOCK) WHERE [ContratoGestor_ContratoCod] = @Contrato_Codigo ORDER BY [ContratoGestor_ContratoCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP009P3,1,0,false,true )
             ,new CursorDef("P009P4", "SELECT TOP 1 [ContratoAuxiliar_ContratoCod], [ContratoAuxiliar_UsuarioCod] FROM [ContratoAuxiliar] WITH (NOLOCK) WHERE [ContratoAuxiliar_ContratoCod] = @Contrato_Codigo ORDER BY [ContratoAuxiliar_ContratoCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP009P4,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
