/*
               File: ServicoArtefatos_BC
        Description: Artefatos
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/29/2020 9:31:58.19
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class servicoartefatos_bc : GXHttpHandler, IGxSilentTrn
   {
      public servicoartefatos_bc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public servicoartefatos_bc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      protected void INITTRN( )
      {
      }

      public void GetInsDefault( )
      {
         ReadRow4F195( ) ;
         standaloneNotModal( ) ;
         InitializeNonKey4F195( ) ;
         standaloneModal( ) ;
         AddRow4F195( ) ;
         Gx_mode = "INS";
         return  ;
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            /* Execute user event: E114F2 */
            E114F2 ();
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               Z155Servico_Codigo = A155Servico_Codigo;
               Z1766ServicoArtefato_ArtefatoCod = A1766ServicoArtefato_ArtefatoCod;
               SetMode( "UPD") ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      public bool Reindex( )
      {
         return true ;
      }

      protected void CONFIRM_4F0( )
      {
         BeforeValidate4F195( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls4F195( ) ;
            }
            else
            {
               CheckExtendedTable4F195( ) ;
               if ( AnyError == 0 )
               {
                  ZM4F195( 2) ;
                  ZM4F195( 3) ;
               }
               CloseExtendedTableCursors4F195( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
         }
      }

      protected void E124F2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV13WWPContext) ;
         AV10TrnContext.FromXml(AV12WebSession.Get("TrnContext"), "");
      }

      protected void E114F2( )
      {
         /* After Trn Routine */
      }

      protected void ZM4F195( short GX_JID )
      {
         if ( ( GX_JID == 1 ) || ( GX_JID == 0 ) )
         {
         }
         if ( ( GX_JID == 2 ) || ( GX_JID == 0 ) )
         {
         }
         if ( ( GX_JID == 3 ) || ( GX_JID == 0 ) )
         {
            Z1767ServicoArtefato_ArtefatoDsc = A1767ServicoArtefato_ArtefatoDsc;
         }
         if ( GX_JID == -1 )
         {
            Z155Servico_Codigo = A155Servico_Codigo;
            Z1766ServicoArtefato_ArtefatoCod = A1766ServicoArtefato_ArtefatoCod;
            Z1767ServicoArtefato_ArtefatoDsc = A1767ServicoArtefato_ArtefatoDsc;
         }
      }

      protected void standaloneNotModal( )
      {
      }

      protected void standaloneModal( )
      {
      }

      protected void Load4F195( )
      {
         /* Using cursor BC004F6 */
         pr_default.execute(4, new Object[] {A155Servico_Codigo, A1766ServicoArtefato_ArtefatoCod});
         if ( (pr_default.getStatus(4) != 101) )
         {
            RcdFound195 = 1;
            A1767ServicoArtefato_ArtefatoDsc = BC004F6_A1767ServicoArtefato_ArtefatoDsc[0];
            n1767ServicoArtefato_ArtefatoDsc = BC004F6_n1767ServicoArtefato_ArtefatoDsc[0];
            ZM4F195( -1) ;
         }
         pr_default.close(4);
         OnLoadActions4F195( ) ;
      }

      protected void OnLoadActions4F195( )
      {
      }

      protected void CheckExtendedTable4F195( )
      {
         standaloneModal( ) ;
         /* Using cursor BC004F4 */
         pr_default.execute(2, new Object[] {A155Servico_Codigo});
         if ( (pr_default.getStatus(2) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Servico'.", "ForeignKeyNotFound", 1, "SERVICO_CODIGO");
            AnyError = 1;
         }
         pr_default.close(2);
         /* Using cursor BC004F5 */
         pr_default.execute(3, new Object[] {A1766ServicoArtefato_ArtefatoCod});
         if ( (pr_default.getStatus(3) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Servico_Artefatos'.", "ForeignKeyNotFound", 1, "SERVICOARTEFATO_ARTEFATOCOD");
            AnyError = 1;
         }
         A1767ServicoArtefato_ArtefatoDsc = BC004F5_A1767ServicoArtefato_ArtefatoDsc[0];
         n1767ServicoArtefato_ArtefatoDsc = BC004F5_n1767ServicoArtefato_ArtefatoDsc[0];
         pr_default.close(3);
      }

      protected void CloseExtendedTableCursors4F195( )
      {
         pr_default.close(2);
         pr_default.close(3);
      }

      protected void enableDisable( )
      {
      }

      protected void GetKey4F195( )
      {
         /* Using cursor BC004F7 */
         pr_default.execute(5, new Object[] {A155Servico_Codigo, A1766ServicoArtefato_ArtefatoCod});
         if ( (pr_default.getStatus(5) != 101) )
         {
            RcdFound195 = 1;
         }
         else
         {
            RcdFound195 = 0;
         }
         pr_default.close(5);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor BC004F3 */
         pr_default.execute(1, new Object[] {A155Servico_Codigo, A1766ServicoArtefato_ArtefatoCod});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM4F195( 1) ;
            RcdFound195 = 1;
            A155Servico_Codigo = BC004F3_A155Servico_Codigo[0];
            A1766ServicoArtefato_ArtefatoCod = BC004F3_A1766ServicoArtefato_ArtefatoCod[0];
            Z155Servico_Codigo = A155Servico_Codigo;
            Z1766ServicoArtefato_ArtefatoCod = A1766ServicoArtefato_ArtefatoCod;
            sMode195 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal( ) ;
            Load4F195( ) ;
            if ( AnyError == 1 )
            {
               RcdFound195 = 0;
               InitializeNonKey4F195( ) ;
            }
            Gx_mode = sMode195;
         }
         else
         {
            RcdFound195 = 0;
            InitializeNonKey4F195( ) ;
            sMode195 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal( ) ;
            Gx_mode = sMode195;
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey4F195( ) ;
         if ( RcdFound195 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
         }
         getByPrimaryKey( ) ;
      }

      protected void insert_Check( )
      {
         CONFIRM_4F0( ) ;
         IsConfirmed = 0;
      }

      protected void update_Check( )
      {
         insert_Check( ) ;
      }

      protected void delete_Check( )
      {
         insert_Check( ) ;
      }

      protected void CheckOptimisticConcurrency4F195( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor BC004F2 */
            pr_default.execute(0, new Object[] {A155Servico_Codigo, A1766ServicoArtefato_ArtefatoCod});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ServicoArtefatos"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"ServicoArtefatos"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert4F195( )
      {
         BeforeValidate4F195( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable4F195( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM4F195( 0) ;
            CheckOptimisticConcurrency4F195( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm4F195( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert4F195( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC004F8 */
                     pr_default.execute(6, new Object[] {A155Servico_Codigo, A1766ServicoArtefato_ArtefatoCod});
                     pr_default.close(6);
                     dsDefault.SmartCacheProvider.SetUpdated("ServicoArtefatos") ;
                     if ( (pr_default.getStatus(6) == 1) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load4F195( ) ;
            }
            EndLevel4F195( ) ;
         }
         CloseExtendedTableCursors4F195( ) ;
      }

      protected void Update4F195( )
      {
         BeforeValidate4F195( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable4F195( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency4F195( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm4F195( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate4F195( ) ;
                  if ( AnyError == 0 )
                  {
                     /* No attributes to update on table [ServicoArtefatos] */
                     DeferredUpdate4F195( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           getByPrimaryKey( ) ;
                           GX_msglist.addItem(context.GetMessage( "GXM_sucupdated", ""), 0, "", true);
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel4F195( ) ;
         }
         CloseExtendedTableCursors4F195( ) ;
      }

      protected void DeferredUpdate4F195( )
      {
      }

      protected void delete( )
      {
         Gx_mode = "DLT";
         BeforeValidate4F195( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency4F195( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls4F195( ) ;
            AfterConfirm4F195( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete4F195( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor BC004F9 */
                  pr_default.execute(7, new Object[] {A155Servico_Codigo, A1766ServicoArtefato_ArtefatoCod});
                  pr_default.close(7);
                  dsDefault.SmartCacheProvider.SetUpdated("ServicoArtefatos") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_sucdeleted", ""), 0, "", true);
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode195 = Gx_mode;
         Gx_mode = "DLT";
         EndLevel4F195( ) ;
         Gx_mode = sMode195;
      }

      protected void OnDeleteControls4F195( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            /* Using cursor BC004F10 */
            pr_default.execute(8, new Object[] {A1766ServicoArtefato_ArtefatoCod});
            A1767ServicoArtefato_ArtefatoDsc = BC004F10_A1767ServicoArtefato_ArtefatoDsc[0];
            n1767ServicoArtefato_ArtefatoDsc = BC004F10_n1767ServicoArtefato_ArtefatoDsc[0];
            pr_default.close(8);
         }
      }

      protected void EndLevel4F195( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete4F195( ) ;
         }
         if ( AnyError == 0 )
         {
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanKeyStart4F195( )
      {
         /* Scan By routine */
         /* Using cursor BC004F11 */
         pr_default.execute(9, new Object[] {A155Servico_Codigo, A1766ServicoArtefato_ArtefatoCod});
         RcdFound195 = 0;
         if ( (pr_default.getStatus(9) != 101) )
         {
            RcdFound195 = 1;
            A1767ServicoArtefato_ArtefatoDsc = BC004F11_A1767ServicoArtefato_ArtefatoDsc[0];
            n1767ServicoArtefato_ArtefatoDsc = BC004F11_n1767ServicoArtefato_ArtefatoDsc[0];
            A155Servico_Codigo = BC004F11_A155Servico_Codigo[0];
            A1766ServicoArtefato_ArtefatoCod = BC004F11_A1766ServicoArtefato_ArtefatoCod[0];
         }
         /* Load Subordinate Levels */
      }

      protected void ScanKeyNext4F195( )
      {
         /* Scan next routine */
         pr_default.readNext(9);
         RcdFound195 = 0;
         ScanKeyLoad4F195( ) ;
      }

      protected void ScanKeyLoad4F195( )
      {
         sMode195 = Gx_mode;
         Gx_mode = "DSP";
         if ( (pr_default.getStatus(9) != 101) )
         {
            RcdFound195 = 1;
            A1767ServicoArtefato_ArtefatoDsc = BC004F11_A1767ServicoArtefato_ArtefatoDsc[0];
            n1767ServicoArtefato_ArtefatoDsc = BC004F11_n1767ServicoArtefato_ArtefatoDsc[0];
            A155Servico_Codigo = BC004F11_A155Servico_Codigo[0];
            A1766ServicoArtefato_ArtefatoCod = BC004F11_A1766ServicoArtefato_ArtefatoCod[0];
         }
         Gx_mode = sMode195;
      }

      protected void ScanKeyEnd4F195( )
      {
         pr_default.close(9);
      }

      protected void AfterConfirm4F195( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert4F195( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate4F195( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete4F195( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete4F195( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate4F195( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes4F195( )
      {
      }

      protected void AddRow4F195( )
      {
         VarsToRow195( bcServicoArtefatos) ;
      }

      protected void ReadRow4F195( )
      {
         RowToVars195( bcServicoArtefatos, 1) ;
      }

      protected void InitializeNonKey4F195( )
      {
         A1767ServicoArtefato_ArtefatoDsc = "";
         n1767ServicoArtefato_ArtefatoDsc = false;
      }

      protected void InitAll4F195( )
      {
         A155Servico_Codigo = 0;
         A1766ServicoArtefato_ArtefatoCod = 0;
         InitializeNonKey4F195( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      public void VarsToRow195( SdtServicoArtefatos obj195 )
      {
         obj195.gxTpr_Mode = Gx_mode;
         obj195.gxTpr_Servicoartefato_artefatodsc = A1767ServicoArtefato_ArtefatoDsc;
         obj195.gxTpr_Servico_codigo = A155Servico_Codigo;
         obj195.gxTpr_Servicoartefato_artefatocod = A1766ServicoArtefato_ArtefatoCod;
         obj195.gxTpr_Servico_codigo_Z = Z155Servico_Codigo;
         obj195.gxTpr_Servicoartefato_artefatocod_Z = Z1766ServicoArtefato_ArtefatoCod;
         obj195.gxTpr_Servicoartefato_artefatodsc_Z = Z1767ServicoArtefato_ArtefatoDsc;
         obj195.gxTpr_Servicoartefato_artefatodsc_N = (short)(Convert.ToInt16(n1767ServicoArtefato_ArtefatoDsc));
         obj195.gxTpr_Mode = Gx_mode;
         return  ;
      }

      public void KeyVarsToRow195( SdtServicoArtefatos obj195 )
      {
         obj195.gxTpr_Servico_codigo = A155Servico_Codigo;
         obj195.gxTpr_Servicoartefato_artefatocod = A1766ServicoArtefato_ArtefatoCod;
         return  ;
      }

      public void RowToVars195( SdtServicoArtefatos obj195 ,
                                int forceLoad )
      {
         Gx_mode = obj195.gxTpr_Mode;
         A1767ServicoArtefato_ArtefatoDsc = obj195.gxTpr_Servicoartefato_artefatodsc;
         n1767ServicoArtefato_ArtefatoDsc = false;
         A155Servico_Codigo = obj195.gxTpr_Servico_codigo;
         A1766ServicoArtefato_ArtefatoCod = obj195.gxTpr_Servicoartefato_artefatocod;
         Z155Servico_Codigo = obj195.gxTpr_Servico_codigo_Z;
         Z1766ServicoArtefato_ArtefatoCod = obj195.gxTpr_Servicoartefato_artefatocod_Z;
         Z1767ServicoArtefato_ArtefatoDsc = obj195.gxTpr_Servicoartefato_artefatodsc_Z;
         n1767ServicoArtefato_ArtefatoDsc = (bool)(Convert.ToBoolean(obj195.gxTpr_Servicoartefato_artefatodsc_N));
         Gx_mode = obj195.gxTpr_Mode;
         return  ;
      }

      public void LoadKey( Object[] obj )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         A155Servico_Codigo = (int)getParm(obj,0);
         A1766ServicoArtefato_ArtefatoCod = (int)getParm(obj,1);
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         InitializeNonKey4F195( ) ;
         ScanKeyStart4F195( ) ;
         if ( RcdFound195 == 0 )
         {
            Gx_mode = "INS";
            /* Using cursor BC004F12 */
            pr_default.execute(10, new Object[] {A155Servico_Codigo});
            if ( (pr_default.getStatus(10) == 101) )
            {
               GX_msglist.addItem("N�o existe 'Servico'.", "ForeignKeyNotFound", 1, "SERVICO_CODIGO");
               AnyError = 1;
            }
            pr_default.close(10);
            /* Using cursor BC004F10 */
            pr_default.execute(8, new Object[] {A1766ServicoArtefato_ArtefatoCod});
            if ( (pr_default.getStatus(8) == 101) )
            {
               GX_msglist.addItem("N�o existe 'Servico_Artefatos'.", "ForeignKeyNotFound", 1, "SERVICOARTEFATO_ARTEFATOCOD");
               AnyError = 1;
            }
            A1767ServicoArtefato_ArtefatoDsc = BC004F10_A1767ServicoArtefato_ArtefatoDsc[0];
            n1767ServicoArtefato_ArtefatoDsc = BC004F10_n1767ServicoArtefato_ArtefatoDsc[0];
            pr_default.close(8);
         }
         else
         {
            Gx_mode = "UPD";
            Z155Servico_Codigo = A155Servico_Codigo;
            Z1766ServicoArtefato_ArtefatoCod = A1766ServicoArtefato_ArtefatoCod;
         }
         ZM4F195( -1) ;
         OnLoadActions4F195( ) ;
         AddRow4F195( ) ;
         ScanKeyEnd4F195( ) ;
         if ( RcdFound195 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "");
            AnyError = 1;
         }
         context.GX_msglist = BackMsgLst;
      }

      public void Load( )
      {
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         RowToVars195( bcServicoArtefatos, 0) ;
         ScanKeyStart4F195( ) ;
         if ( RcdFound195 == 0 )
         {
            Gx_mode = "INS";
            /* Using cursor BC004F12 */
            pr_default.execute(10, new Object[] {A155Servico_Codigo});
            if ( (pr_default.getStatus(10) == 101) )
            {
               GX_msglist.addItem("N�o existe 'Servico'.", "ForeignKeyNotFound", 1, "SERVICO_CODIGO");
               AnyError = 1;
            }
            pr_default.close(10);
            /* Using cursor BC004F10 */
            pr_default.execute(8, new Object[] {A1766ServicoArtefato_ArtefatoCod});
            if ( (pr_default.getStatus(8) == 101) )
            {
               GX_msglist.addItem("N�o existe 'Servico_Artefatos'.", "ForeignKeyNotFound", 1, "SERVICOARTEFATO_ARTEFATOCOD");
               AnyError = 1;
            }
            A1767ServicoArtefato_ArtefatoDsc = BC004F10_A1767ServicoArtefato_ArtefatoDsc[0];
            n1767ServicoArtefato_ArtefatoDsc = BC004F10_n1767ServicoArtefato_ArtefatoDsc[0];
            pr_default.close(8);
         }
         else
         {
            Gx_mode = "UPD";
            Z155Servico_Codigo = A155Servico_Codigo;
            Z1766ServicoArtefato_ArtefatoCod = A1766ServicoArtefato_ArtefatoCod;
         }
         ZM4F195( -1) ;
         OnLoadActions4F195( ) ;
         AddRow4F195( ) ;
         ScanKeyEnd4F195( ) ;
         if ( RcdFound195 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "");
            AnyError = 1;
         }
         context.GX_msglist = BackMsgLst;
      }

      public void Save( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         IsConfirmed = 1;
         RowToVars195( bcServicoArtefatos, 0) ;
         nKeyPressed = 1;
         GetKey4F195( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            Insert4F195( ) ;
         }
         else
         {
            if ( RcdFound195 == 1 )
            {
               if ( ( A155Servico_Codigo != Z155Servico_Codigo ) || ( A1766ServicoArtefato_ArtefatoCod != Z1766ServicoArtefato_ArtefatoCod ) )
               {
                  A155Servico_Codigo = Z155Servico_Codigo;
                  A1766ServicoArtefato_ArtefatoCod = Z1766ServicoArtefato_ArtefatoCod;
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
               }
               else
               {
                  Gx_mode = "UPD";
                  /* Update record */
                  Update4F195( ) ;
               }
            }
            else
            {
               if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else
               {
                  if ( ( A155Servico_Codigo != Z155Servico_Codigo ) || ( A1766ServicoArtefato_ArtefatoCod != Z1766ServicoArtefato_ArtefatoCod ) )
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     else
                     {
                        Gx_mode = "INS";
                        /* Insert record */
                        Insert4F195( ) ;
                     }
                  }
                  else
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                        AnyError = 1;
                     }
                     else
                     {
                        Gx_mode = "INS";
                        /* Insert record */
                        Insert4F195( ) ;
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         VarsToRow195( bcServicoArtefatos) ;
         context.GX_msglist = BackMsgLst;
         return  ;
      }

      public void Check( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         RowToVars195( bcServicoArtefatos, 0) ;
         nKeyPressed = 3;
         IsConfirmed = 0;
         GetKey4F195( ) ;
         if ( RcdFound195 == 1 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
               AnyError = 1;
            }
            else if ( ( A155Servico_Codigo != Z155Servico_Codigo ) || ( A1766ServicoArtefato_ArtefatoCod != Z1766ServicoArtefato_ArtefatoCod ) )
            {
               A155Servico_Codigo = Z155Servico_Codigo;
               A1766ServicoArtefato_ArtefatoCod = Z1766ServicoArtefato_ArtefatoCod;
               GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "DuplicatePrimaryKey", 1, "");
               AnyError = 1;
            }
            else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               delete_Check( ) ;
            }
            else
            {
               Gx_mode = "UPD";
               update_Check( ) ;
            }
         }
         else
         {
            if ( ( A155Servico_Codigo != Z155Servico_Codigo ) || ( A1766ServicoArtefato_ArtefatoCod != Z1766ServicoArtefato_ArtefatoCod ) )
            {
               Gx_mode = "INS";
               insert_Check( ) ;
            }
            else
            {
               if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                  AnyError = 1;
               }
               else
               {
                  Gx_mode = "INS";
                  insert_Check( ) ;
               }
            }
         }
         pr_default.close(1);
         pr_default.close(10);
         pr_default.close(8);
         context.RollbackDataStores( "ServicoArtefatos_BC");
         VarsToRow195( bcServicoArtefatos) ;
         context.GX_msglist = BackMsgLst;
         return  ;
      }

      public int Errors( )
      {
         if ( AnyError == 0 )
         {
            return (int)(0) ;
         }
         return (int)(1) ;
      }

      public msglist GetMessages( )
      {
         return LclMsgLst ;
      }

      public String GetMode( )
      {
         Gx_mode = bcServicoArtefatos.gxTpr_Mode;
         return Gx_mode ;
      }

      public void SetMode( String lMode )
      {
         Gx_mode = lMode;
         bcServicoArtefatos.gxTpr_Mode = Gx_mode;
         return  ;
      }

      public void SetSDT( GxSilentTrnSdt sdt ,
                          short sdtToBc )
      {
         if ( sdt != bcServicoArtefatos )
         {
            bcServicoArtefatos = (SdtServicoArtefatos)(sdt);
            if ( StringUtil.StrCmp(bcServicoArtefatos.gxTpr_Mode, "") == 0 )
            {
               bcServicoArtefatos.gxTpr_Mode = "INS";
            }
            if ( sdtToBc == 1 )
            {
               VarsToRow195( bcServicoArtefatos) ;
            }
            else
            {
               RowToVars195( bcServicoArtefatos, 1) ;
            }
         }
         else
         {
            if ( StringUtil.StrCmp(bcServicoArtefatos.gxTpr_Mode, "") == 0 )
            {
               bcServicoArtefatos.gxTpr_Mode = "INS";
            }
         }
         return  ;
      }

      public void ReloadFromSDT( )
      {
         RowToVars195( bcServicoArtefatos, 1) ;
         return  ;
      }

      public SdtServicoArtefatos ServicoArtefatos_BC
      {
         get {
            return bcServicoArtefatos ;
         }

      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         createObjects();
         initialize();
      }

      protected override void createObjects( )
      {
      }

      protected void Process( )
      {
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(10);
         pr_default.close(8);
      }

      public override void initialize( )
      {
         scmdbuf = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Gx_mode = "";
         AV13WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV10TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV12WebSession = context.GetSession();
         Z1767ServicoArtefato_ArtefatoDsc = "";
         A1767ServicoArtefato_ArtefatoDsc = "";
         BC004F6_A1767ServicoArtefato_ArtefatoDsc = new String[] {""} ;
         BC004F6_n1767ServicoArtefato_ArtefatoDsc = new bool[] {false} ;
         BC004F6_A155Servico_Codigo = new int[1] ;
         BC004F6_A1766ServicoArtefato_ArtefatoCod = new int[1] ;
         BC004F4_A155Servico_Codigo = new int[1] ;
         BC004F5_A1767ServicoArtefato_ArtefatoDsc = new String[] {""} ;
         BC004F5_n1767ServicoArtefato_ArtefatoDsc = new bool[] {false} ;
         BC004F7_A155Servico_Codigo = new int[1] ;
         BC004F7_A1766ServicoArtefato_ArtefatoCod = new int[1] ;
         BC004F3_A155Servico_Codigo = new int[1] ;
         BC004F3_A1766ServicoArtefato_ArtefatoCod = new int[1] ;
         sMode195 = "";
         BC004F2_A155Servico_Codigo = new int[1] ;
         BC004F2_A1766ServicoArtefato_ArtefatoCod = new int[1] ;
         BC004F10_A1767ServicoArtefato_ArtefatoDsc = new String[] {""} ;
         BC004F10_n1767ServicoArtefato_ArtefatoDsc = new bool[] {false} ;
         BC004F11_A1767ServicoArtefato_ArtefatoDsc = new String[] {""} ;
         BC004F11_n1767ServicoArtefato_ArtefatoDsc = new bool[] {false} ;
         BC004F11_A155Servico_Codigo = new int[1] ;
         BC004F11_A1766ServicoArtefato_ArtefatoCod = new int[1] ;
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         BC004F12_A155Servico_Codigo = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.servicoartefatos_bc__default(),
            new Object[][] {
                new Object[] {
               BC004F2_A155Servico_Codigo, BC004F2_A1766ServicoArtefato_ArtefatoCod
               }
               , new Object[] {
               BC004F3_A155Servico_Codigo, BC004F3_A1766ServicoArtefato_ArtefatoCod
               }
               , new Object[] {
               BC004F4_A155Servico_Codigo
               }
               , new Object[] {
               BC004F5_A1767ServicoArtefato_ArtefatoDsc, BC004F5_n1767ServicoArtefato_ArtefatoDsc
               }
               , new Object[] {
               BC004F6_A1767ServicoArtefato_ArtefatoDsc, BC004F6_n1767ServicoArtefato_ArtefatoDsc, BC004F6_A155Servico_Codigo, BC004F6_A1766ServicoArtefato_ArtefatoCod
               }
               , new Object[] {
               BC004F7_A155Servico_Codigo, BC004F7_A1766ServicoArtefato_ArtefatoCod
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               BC004F10_A1767ServicoArtefato_ArtefatoDsc, BC004F10_n1767ServicoArtefato_ArtefatoDsc
               }
               , new Object[] {
               BC004F11_A1767ServicoArtefato_ArtefatoDsc, BC004F11_n1767ServicoArtefato_ArtefatoDsc, BC004F11_A155Servico_Codigo, BC004F11_A1766ServicoArtefato_ArtefatoCod
               }
               , new Object[] {
               BC004F12_A155Servico_Codigo
               }
            }
         );
         INITTRN();
         /* Execute Start event if defined. */
         /* Execute user event: E124F2 */
         E124F2 ();
         standaloneNotModal( ) ;
      }

      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short GX_JID ;
      private short RcdFound195 ;
      private int trnEnded ;
      private int Z155Servico_Codigo ;
      private int A155Servico_Codigo ;
      private int Z1766ServicoArtefato_ArtefatoCod ;
      private int A1766ServicoArtefato_ArtefatoCod ;
      private String scmdbuf ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String Gx_mode ;
      private String sMode195 ;
      private bool n1767ServicoArtefato_ArtefatoDsc ;
      private String Z1767ServicoArtefato_ArtefatoDsc ;
      private String A1767ServicoArtefato_ArtefatoDsc ;
      private IGxSession AV12WebSession ;
      private SdtServicoArtefatos bcServicoArtefatos ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private String[] BC004F6_A1767ServicoArtefato_ArtefatoDsc ;
      private bool[] BC004F6_n1767ServicoArtefato_ArtefatoDsc ;
      private int[] BC004F6_A155Servico_Codigo ;
      private int[] BC004F6_A1766ServicoArtefato_ArtefatoCod ;
      private int[] BC004F4_A155Servico_Codigo ;
      private String[] BC004F5_A1767ServicoArtefato_ArtefatoDsc ;
      private bool[] BC004F5_n1767ServicoArtefato_ArtefatoDsc ;
      private int[] BC004F7_A155Servico_Codigo ;
      private int[] BC004F7_A1766ServicoArtefato_ArtefatoCod ;
      private int[] BC004F3_A155Servico_Codigo ;
      private int[] BC004F3_A1766ServicoArtefato_ArtefatoCod ;
      private int[] BC004F2_A155Servico_Codigo ;
      private int[] BC004F2_A1766ServicoArtefato_ArtefatoCod ;
      private String[] BC004F10_A1767ServicoArtefato_ArtefatoDsc ;
      private bool[] BC004F10_n1767ServicoArtefato_ArtefatoDsc ;
      private String[] BC004F11_A1767ServicoArtefato_ArtefatoDsc ;
      private bool[] BC004F11_n1767ServicoArtefato_ArtefatoDsc ;
      private int[] BC004F11_A155Servico_Codigo ;
      private int[] BC004F11_A1766ServicoArtefato_ArtefatoCod ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private int[] BC004F12_A155Servico_Codigo ;
      private wwpbaseobjects.SdtWWPTransactionContext AV10TrnContext ;
      private wwpbaseobjects.SdtWWPContext AV13WWPContext ;
   }

   public class servicoartefatos_bc__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new UpdateCursor(def[6])
         ,new UpdateCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmBC004F6 ;
          prmBC004F6 = new Object[] {
          new Object[] {"@Servico_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ServicoArtefato_ArtefatoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004F4 ;
          prmBC004F4 = new Object[] {
          new Object[] {"@Servico_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004F5 ;
          prmBC004F5 = new Object[] {
          new Object[] {"@ServicoArtefato_ArtefatoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004F7 ;
          prmBC004F7 = new Object[] {
          new Object[] {"@Servico_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ServicoArtefato_ArtefatoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004F3 ;
          prmBC004F3 = new Object[] {
          new Object[] {"@Servico_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ServicoArtefato_ArtefatoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004F2 ;
          prmBC004F2 = new Object[] {
          new Object[] {"@Servico_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ServicoArtefato_ArtefatoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004F8 ;
          prmBC004F8 = new Object[] {
          new Object[] {"@Servico_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ServicoArtefato_ArtefatoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004F9 ;
          prmBC004F9 = new Object[] {
          new Object[] {"@Servico_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ServicoArtefato_ArtefatoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004F11 ;
          prmBC004F11 = new Object[] {
          new Object[] {"@Servico_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ServicoArtefato_ArtefatoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004F12 ;
          prmBC004F12 = new Object[] {
          new Object[] {"@Servico_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmBC004F10 ;
          prmBC004F10 = new Object[] {
          new Object[] {"@ServicoArtefato_ArtefatoCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("BC004F2", "SELECT [Servico_Codigo], [ServicoArtefato_ArtefatoCod] AS ServicoArtefato_ArtefatoCod FROM [ServicoArtefatos] WITH (UPDLOCK) WHERE [Servico_Codigo] = @Servico_Codigo AND [ServicoArtefato_ArtefatoCod] = @ServicoArtefato_ArtefatoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC004F2,1,0,true,false )
             ,new CursorDef("BC004F3", "SELECT [Servico_Codigo], [ServicoArtefato_ArtefatoCod] AS ServicoArtefato_ArtefatoCod FROM [ServicoArtefatos] WITH (NOLOCK) WHERE [Servico_Codigo] = @Servico_Codigo AND [ServicoArtefato_ArtefatoCod] = @ServicoArtefato_ArtefatoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC004F3,1,0,true,false )
             ,new CursorDef("BC004F4", "SELECT [Servico_Codigo] FROM [Servico] WITH (NOLOCK) WHERE [Servico_Codigo] = @Servico_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC004F4,1,0,true,false )
             ,new CursorDef("BC004F5", "SELECT [Artefatos_Descricao] AS ServicoArtefato_ArtefatoDsc FROM [Artefatos] WITH (NOLOCK) WHERE [Artefatos_Codigo] = @ServicoArtefato_ArtefatoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC004F5,1,0,true,false )
             ,new CursorDef("BC004F6", "SELECT T2.[Artefatos_Descricao] AS ServicoArtefato_ArtefatoDsc, TM1.[Servico_Codigo], TM1.[ServicoArtefato_ArtefatoCod] AS ServicoArtefato_ArtefatoCod FROM ([ServicoArtefatos] TM1 WITH (NOLOCK) INNER JOIN [Artefatos] T2 WITH (NOLOCK) ON T2.[Artefatos_Codigo] = TM1.[ServicoArtefato_ArtefatoCod]) WHERE TM1.[Servico_Codigo] = @Servico_Codigo and TM1.[ServicoArtefato_ArtefatoCod] = @ServicoArtefato_ArtefatoCod ORDER BY TM1.[Servico_Codigo], TM1.[ServicoArtefato_ArtefatoCod]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmBC004F6,100,0,true,false )
             ,new CursorDef("BC004F7", "SELECT [Servico_Codigo], [ServicoArtefato_ArtefatoCod] AS ServicoArtefato_ArtefatoCod FROM [ServicoArtefatos] WITH (NOLOCK) WHERE [Servico_Codigo] = @Servico_Codigo AND [ServicoArtefato_ArtefatoCod] = @ServicoArtefato_ArtefatoCod  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmBC004F7,1,0,true,false )
             ,new CursorDef("BC004F8", "INSERT INTO [ServicoArtefatos]([Servico_Codigo], [ServicoArtefato_ArtefatoCod]) VALUES(@Servico_Codigo, @ServicoArtefato_ArtefatoCod)", GxErrorMask.GX_NOMASK,prmBC004F8)
             ,new CursorDef("BC004F9", "DELETE FROM [ServicoArtefatos]  WHERE [Servico_Codigo] = @Servico_Codigo AND [ServicoArtefato_ArtefatoCod] = @ServicoArtefato_ArtefatoCod", GxErrorMask.GX_NOMASK,prmBC004F9)
             ,new CursorDef("BC004F10", "SELECT [Artefatos_Descricao] AS ServicoArtefato_ArtefatoDsc FROM [Artefatos] WITH (NOLOCK) WHERE [Artefatos_Codigo] = @ServicoArtefato_ArtefatoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmBC004F10,1,0,true,false )
             ,new CursorDef("BC004F11", "SELECT T2.[Artefatos_Descricao] AS ServicoArtefato_ArtefatoDsc, TM1.[Servico_Codigo], TM1.[ServicoArtefato_ArtefatoCod] AS ServicoArtefato_ArtefatoCod FROM ([ServicoArtefatos] TM1 WITH (NOLOCK) INNER JOIN [Artefatos] T2 WITH (NOLOCK) ON T2.[Artefatos_Codigo] = TM1.[ServicoArtefato_ArtefatoCod]) WHERE TM1.[Servico_Codigo] = @Servico_Codigo and TM1.[ServicoArtefato_ArtefatoCod] = @ServicoArtefato_ArtefatoCod ORDER BY TM1.[Servico_Codigo], TM1.[ServicoArtefato_ArtefatoCod]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmBC004F11,100,0,true,false )
             ,new CursorDef("BC004F12", "SELECT [Servico_Codigo] FROM [Servico] WITH (NOLOCK) WHERE [Servico_Codigo] = @Servico_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmBC004F12,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 3 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 4 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 8 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 9 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                return;
             case 10 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 10 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
