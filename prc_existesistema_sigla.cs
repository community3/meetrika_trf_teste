/*
               File: PRC_ExisteSistema_Sigla
        Description: Confere se j� existe a Sigla do Sistema
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:51:7.7
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_existesistema_sigla : GXProcedure
   {
      public prc_existesistema_sigla( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_existesistema_sigla( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Sistema_Codigo ,
                           String aP1_Sistema_Sigla ,
                           int aP2_Sistema_AreaTrabalhoCod ,
                           out bool aP3_Existe )
      {
         this.AV9Sistema_Codigo = aP0_Sistema_Codigo;
         this.AV11Sistema_Sigla = aP1_Sistema_Sigla;
         this.A135Sistema_AreaTrabalhoCod = aP2_Sistema_AreaTrabalhoCod;
         this.AV8Existe = false ;
         initialize();
         executePrivate();
         aP3_Existe=this.AV8Existe;
      }

      public bool executeUdp( int aP0_Sistema_Codigo ,
                              String aP1_Sistema_Sigla ,
                              int aP2_Sistema_AreaTrabalhoCod )
      {
         this.AV9Sistema_Codigo = aP0_Sistema_Codigo;
         this.AV11Sistema_Sigla = aP1_Sistema_Sigla;
         this.A135Sistema_AreaTrabalhoCod = aP2_Sistema_AreaTrabalhoCod;
         this.AV8Existe = false ;
         initialize();
         executePrivate();
         aP3_Existe=this.AV8Existe;
         return AV8Existe ;
      }

      public void executeSubmit( int aP0_Sistema_Codigo ,
                                 String aP1_Sistema_Sigla ,
                                 int aP2_Sistema_AreaTrabalhoCod ,
                                 out bool aP3_Existe )
      {
         prc_existesistema_sigla objprc_existesistema_sigla;
         objprc_existesistema_sigla = new prc_existesistema_sigla();
         objprc_existesistema_sigla.AV9Sistema_Codigo = aP0_Sistema_Codigo;
         objprc_existesistema_sigla.AV11Sistema_Sigla = aP1_Sistema_Sigla;
         objprc_existesistema_sigla.A135Sistema_AreaTrabalhoCod = aP2_Sistema_AreaTrabalhoCod;
         objprc_existesistema_sigla.AV8Existe = false ;
         objprc_existesistema_sigla.context.SetSubmitInitialConfig(context);
         objprc_existesistema_sigla.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_existesistema_sigla);
         aP3_Existe=this.AV8Existe;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_existesistema_sigla)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         GXt_char1 = AV10PadSistema_Sigla;
         new prc_padronizastring(context ).execute(  AV11Sistema_Sigla, out  GXt_char1) ;
         AV10PadSistema_Sigla = GXt_char1;
         AV8Existe = false;
         /* Using cursor P00402 */
         pr_default.execute(0, new Object[] {A135Sistema_AreaTrabalhoCod, AV9Sistema_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A129Sistema_Sigla = P00402_A129Sistema_Sigla[0];
            A127Sistema_Codigo = P00402_A127Sistema_Codigo[0];
            if ( StringUtil.StrCmp(new prc_padronizastring(context).executeUdp(  A129Sistema_Sigla), AV10PadSistema_Sigla) != 0 )
            {
               AV8Existe = true;
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            pr_default.readNext(0);
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV10PadSistema_Sigla = "";
         GXt_char1 = "";
         scmdbuf = "";
         P00402_A135Sistema_AreaTrabalhoCod = new int[1] ;
         P00402_A129Sistema_Sigla = new String[] {""} ;
         P00402_A127Sistema_Codigo = new int[1] ;
         A129Sistema_Sigla = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_existesistema_sigla__default(),
            new Object[][] {
                new Object[] {
               P00402_A135Sistema_AreaTrabalhoCod, P00402_A129Sistema_Sigla, P00402_A127Sistema_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV9Sistema_Codigo ;
      private int A135Sistema_AreaTrabalhoCod ;
      private int A127Sistema_Codigo ;
      private String AV11Sistema_Sigla ;
      private String AV10PadSistema_Sigla ;
      private String GXt_char1 ;
      private String scmdbuf ;
      private String A129Sistema_Sigla ;
      private bool AV8Existe ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00402_A135Sistema_AreaTrabalhoCod ;
      private String[] P00402_A129Sistema_Sigla ;
      private int[] P00402_A127Sistema_Codigo ;
      private bool aP3_Existe ;
   }

   public class prc_existesistema_sigla__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00402 ;
          prmP00402 = new Object[] {
          new Object[] {"@Sistema_AreaTrabalhoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV9Sistema_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00402", "SELECT [Sistema_AreaTrabalhoCod], [Sistema_Sigla], [Sistema_Codigo] FROM [Sistema] WITH (NOLOCK) WHERE ([Sistema_AreaTrabalhoCod] = @Sistema_AreaTrabalhoCod) AND ([Sistema_Codigo] <> @AV9Sistema_Codigo) ORDER BY [Sistema_AreaTrabalhoCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00402,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 25) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
       }
    }

 }

}
