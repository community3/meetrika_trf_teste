/*
               File: PRC_ContratanteUsuario_EhFiscal
        Description: Contratante Usuario Eh Fiscal
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:52:44.20
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_contratanteusuario_ehfiscal : GXProcedure
   {
      public prc_contratanteusuario_ehfiscal( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_contratanteusuario_ehfiscal( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_AreaTrabalho_Codigo ,
                           int aP1_ContratanteUsuario_ContratanteCod ,
                           ref int aP2_ContratanteUsuario_UsuarioCod ,
                           ref bool aP3_ContratanteUsuario_EhFiscal ,
                           String aP4_Acao )
      {
         this.AV11AreaTrabalho_Codigo = aP0_AreaTrabalho_Codigo;
         this.AV10ContratanteUsuario_ContratanteCod = aP1_ContratanteUsuario_ContratanteCod;
         this.A60ContratanteUsuario_UsuarioCod = aP2_ContratanteUsuario_UsuarioCod;
         this.AV8ContratanteUsuario_EhFiscal = aP3_ContratanteUsuario_EhFiscal;
         this.AV9Acao = aP4_Acao;
         initialize();
         executePrivate();
         aP2_ContratanteUsuario_UsuarioCod=this.A60ContratanteUsuario_UsuarioCod;
         aP3_ContratanteUsuario_EhFiscal=this.AV8ContratanteUsuario_EhFiscal;
      }

      public void executeSubmit( int aP0_AreaTrabalho_Codigo ,
                                 int aP1_ContratanteUsuario_ContratanteCod ,
                                 ref int aP2_ContratanteUsuario_UsuarioCod ,
                                 ref bool aP3_ContratanteUsuario_EhFiscal ,
                                 String aP4_Acao )
      {
         prc_contratanteusuario_ehfiscal objprc_contratanteusuario_ehfiscal;
         objprc_contratanteusuario_ehfiscal = new prc_contratanteusuario_ehfiscal();
         objprc_contratanteusuario_ehfiscal.AV11AreaTrabalho_Codigo = aP0_AreaTrabalho_Codigo;
         objprc_contratanteusuario_ehfiscal.AV10ContratanteUsuario_ContratanteCod = aP1_ContratanteUsuario_ContratanteCod;
         objprc_contratanteusuario_ehfiscal.A60ContratanteUsuario_UsuarioCod = aP2_ContratanteUsuario_UsuarioCod;
         objprc_contratanteusuario_ehfiscal.AV8ContratanteUsuario_EhFiscal = aP3_ContratanteUsuario_EhFiscal;
         objprc_contratanteusuario_ehfiscal.AV9Acao = aP4_Acao;
         objprc_contratanteusuario_ehfiscal.context.SetSubmitInitialConfig(context);
         objprc_contratanteusuario_ehfiscal.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_contratanteusuario_ehfiscal);
         aP2_ContratanteUsuario_UsuarioCod=this.A60ContratanteUsuario_UsuarioCod;
         aP3_ContratanteUsuario_EhFiscal=this.AV8ContratanteUsuario_EhFiscal;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_contratanteusuario_ehfiscal)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              AV10ContratanteUsuario_ContratanteCod ,
                                              A63ContratanteUsuario_ContratanteCod ,
                                              A1020ContratanteUsuario_AreaTrabalhoCod ,
                                              AV11AreaTrabalho_Codigo ,
                                              A60ContratanteUsuario_UsuarioCod },
                                              new int[] {
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT
                                              }
         });
         /* Using cursor P00CC3 */
         pr_default.execute(0, new Object[] {A60ContratanteUsuario_UsuarioCod, AV10ContratanteUsuario_ContratanteCod, AV11AreaTrabalho_Codigo, AV10ContratanteUsuario_ContratanteCod});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A63ContratanteUsuario_ContratanteCod = P00CC3_A63ContratanteUsuario_ContratanteCod[0];
            A1728ContratanteUsuario_EhFiscal = P00CC3_A1728ContratanteUsuario_EhFiscal[0];
            n1728ContratanteUsuario_EhFiscal = P00CC3_n1728ContratanteUsuario_EhFiscal[0];
            A1020ContratanteUsuario_AreaTrabalhoCod = P00CC3_A1020ContratanteUsuario_AreaTrabalhoCod[0];
            n1020ContratanteUsuario_AreaTrabalhoCod = P00CC3_n1020ContratanteUsuario_AreaTrabalhoCod[0];
            A1020ContratanteUsuario_AreaTrabalhoCod = P00CC3_A1020ContratanteUsuario_AreaTrabalhoCod[0];
            n1020ContratanteUsuario_AreaTrabalhoCod = P00CC3_n1020ContratanteUsuario_AreaTrabalhoCod[0];
            if ( StringUtil.StrCmp(AV9Acao, "Read") == 0 )
            {
               AV8ContratanteUsuario_EhFiscal = A1728ContratanteUsuario_EhFiscal;
            }
            else if ( StringUtil.StrCmp(AV9Acao, "Save") == 0 )
            {
               A1728ContratanteUsuario_EhFiscal = AV8ContratanteUsuario_EhFiscal;
               n1728ContratanteUsuario_EhFiscal = false;
            }
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            /* Using cursor P00CC4 */
            pr_default.execute(1, new Object[] {n1728ContratanteUsuario_EhFiscal, A1728ContratanteUsuario_EhFiscal, A63ContratanteUsuario_ContratanteCod, A60ContratanteUsuario_UsuarioCod});
            pr_default.close(1);
            dsDefault.SmartCacheProvider.SetUpdated("ContratanteUsuario") ;
            if (true) break;
            /* Using cursor P00CC5 */
            pr_default.execute(2, new Object[] {n1728ContratanteUsuario_EhFiscal, A1728ContratanteUsuario_EhFiscal, A63ContratanteUsuario_ContratanteCod, A60ContratanteUsuario_UsuarioCod});
            pr_default.close(2);
            dsDefault.SmartCacheProvider.SetUpdated("ContratanteUsuario") ;
            pr_default.readNext(0);
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         context.CommitDataStores( "PRC_ContratanteUsuario_EhFiscal");
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00CC3_A60ContratanteUsuario_UsuarioCod = new int[1] ;
         P00CC3_A63ContratanteUsuario_ContratanteCod = new int[1] ;
         P00CC3_A1728ContratanteUsuario_EhFiscal = new bool[] {false} ;
         P00CC3_n1728ContratanteUsuario_EhFiscal = new bool[] {false} ;
         P00CC3_A1020ContratanteUsuario_AreaTrabalhoCod = new int[1] ;
         P00CC3_n1020ContratanteUsuario_AreaTrabalhoCod = new bool[] {false} ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_contratanteusuario_ehfiscal__default(),
            new Object[][] {
                new Object[] {
               P00CC3_A60ContratanteUsuario_UsuarioCod, P00CC3_A63ContratanteUsuario_ContratanteCod, P00CC3_A1728ContratanteUsuario_EhFiscal, P00CC3_n1728ContratanteUsuario_EhFiscal, P00CC3_A1020ContratanteUsuario_AreaTrabalhoCod, P00CC3_n1020ContratanteUsuario_AreaTrabalhoCod
               }
               , new Object[] {
               }
               , new Object[] {
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV11AreaTrabalho_Codigo ;
      private int AV10ContratanteUsuario_ContratanteCod ;
      private int A60ContratanteUsuario_UsuarioCod ;
      private int A63ContratanteUsuario_ContratanteCod ;
      private int A1020ContratanteUsuario_AreaTrabalhoCod ;
      private String AV9Acao ;
      private String scmdbuf ;
      private bool AV8ContratanteUsuario_EhFiscal ;
      private bool A1728ContratanteUsuario_EhFiscal ;
      private bool n1728ContratanteUsuario_EhFiscal ;
      private bool n1020ContratanteUsuario_AreaTrabalhoCod ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP2_ContratanteUsuario_UsuarioCod ;
      private bool aP3_ContratanteUsuario_EhFiscal ;
      private IDataStoreProvider pr_default ;
      private int[] P00CC3_A60ContratanteUsuario_UsuarioCod ;
      private int[] P00CC3_A63ContratanteUsuario_ContratanteCod ;
      private bool[] P00CC3_A1728ContratanteUsuario_EhFiscal ;
      private bool[] P00CC3_n1728ContratanteUsuario_EhFiscal ;
      private int[] P00CC3_A1020ContratanteUsuario_AreaTrabalhoCod ;
      private bool[] P00CC3_n1020ContratanteUsuario_AreaTrabalhoCod ;
   }

   public class prc_contratanteusuario_ehfiscal__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00CC3( IGxContext context ,
                                             int AV10ContratanteUsuario_ContratanteCod ,
                                             int A63ContratanteUsuario_ContratanteCod ,
                                             int A1020ContratanteUsuario_AreaTrabalhoCod ,
                                             int AV11AreaTrabalho_Codigo ,
                                             int A60ContratanteUsuario_UsuarioCod )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [4] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT TOP 1 T1.[ContratanteUsuario_UsuarioCod], T1.[ContratanteUsuario_ContratanteCod], T1.[ContratanteUsuario_EhFiscal], COALESCE( T2.[ContratanteUsuario_AreaTrabalhoCod], 0) AS ContratanteUsuario_AreaTrabalhoCod FROM ([ContratanteUsuario] T1 WITH (UPDLOCK) LEFT JOIN (SELECT MIN(T3.[AreaTrabalho_Codigo]) AS ContratanteUsuario_AreaTrabalhoCod, T4.[ContratanteUsuario_ContratanteCod], T4.[ContratanteUsuario_UsuarioCod] FROM [AreaTrabalho] T3 WITH (NOLOCK),  [ContratanteUsuario] T4 WITH (UPDLOCK) WHERE T3.[Contratante_Codigo] = T4.[ContratanteUsuario_ContratanteCod] GROUP BY T4.[ContratanteUsuario_ContratanteCod], T4.[ContratanteUsuario_UsuarioCod] ) T2 ON T2.[ContratanteUsuario_ContratanteCod] = T1.[ContratanteUsuario_ContratanteCod] AND T2.[ContratanteUsuario_UsuarioCod] = T1.[ContratanteUsuario_UsuarioCod])";
         scmdbuf = scmdbuf + " WHERE (T1.[ContratanteUsuario_UsuarioCod] = @ContratanteUsuario_UsuarioCod)";
         scmdbuf = scmdbuf + " and (Not ( (@AV10ContratanteUsuario_ContratanteCod = convert(int, 0))) or ( COALESCE( T2.[ContratanteUsuario_AreaTrabalhoCod], 0) = @AV11AreaTrabalho_Codigo))";
         if ( AV10ContratanteUsuario_ContratanteCod > 0 )
         {
            sWhereString = sWhereString + " and (T1.[ContratanteUsuario_ContratanteCod] = @AV10ContratanteUsuario_ContratanteCod)";
         }
         else
         {
            GXv_int1[3] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[ContratanteUsuario_UsuarioCod]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00CC3(context, (int)dynConstraints[0] , (int)dynConstraints[1] , (int)dynConstraints[2] , (int)dynConstraints[3] , (int)dynConstraints[4] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new UpdateCursor(def[1])
         ,new UpdateCursor(def[2])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00CC4 ;
          prmP00CC4 = new Object[] {
          new Object[] {"@ContratanteUsuario_EhFiscal",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContratanteUsuario_ContratanteCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratanteUsuario_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00CC5 ;
          prmP00CC5 = new Object[] {
          new Object[] {"@ContratanteUsuario_EhFiscal",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContratanteUsuario_ContratanteCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratanteUsuario_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00CC3 ;
          prmP00CC3 = new Object[] {
          new Object[] {"@ContratanteUsuario_UsuarioCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV10ContratanteUsuario_ContratanteCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV11AreaTrabalho_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV10ContratanteUsuario_ContratanteCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00CC3", "scmdbuf",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00CC3,1,0,true,true )
             ,new CursorDef("P00CC4", "UPDATE [ContratanteUsuario] SET [ContratanteUsuario_EhFiscal]=@ContratanteUsuario_EhFiscal  WHERE [ContratanteUsuario_ContratanteCod] = @ContratanteUsuario_ContratanteCod AND [ContratanteUsuario_UsuarioCod] = @ContratanteUsuario_UsuarioCod", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00CC4)
             ,new CursorDef("P00CC5", "UPDATE [ContratanteUsuario] SET [ContratanteUsuario_EhFiscal]=@ContratanteUsuario_EhFiscal  WHERE [ContratanteUsuario_ContratanteCod] = @ContratanteUsuario_ContratanteCod AND [ContratanteUsuario_UsuarioCod] = @ContratanteUsuario_UsuarioCod", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00CC5)
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.getBool(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[4]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[5]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[6]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[7]);
                }
                return;
             case 1 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(1, (bool)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                stmt.SetParameter(3, (int)parms[3]);
                return;
             case 2 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(1, (bool)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                stmt.SetParameter(3, (int)parms[3]);
                return;
       }
    }

 }

}
