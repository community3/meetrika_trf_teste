/*
               File: AtributosGeneral
        Description: Atributos General
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:59:45.44
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class atributosgeneral : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public atributosgeneral( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public atributosgeneral( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Atributos_Codigo )
      {
         this.A176Atributos_Codigo = aP0_Atributos_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
         cmbAtributos_TipoDados = new GXCombobox();
         chkAtributos_PK = new GXCheckbox();
         chkAtributos_FK = new GXCheckbox();
         chkAtributos_Ativo = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  A176Atributos_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A176Atributos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A176Atributos_Codigo), 6, 0)));
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(int)A176Atributos_Codigo});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PA4E2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV15Pgmname = "AtributosGeneral";
               context.Gx_err = 0;
               WS4E2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "Atributos General") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?202042822594549");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
            context.WriteHtmlText( "<body") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("atributosgeneral.aspx") + "?" + UrlEncode("" +A176Atributos_Codigo)+"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOA176Atributos_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOA176Atributos_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vSISTEMA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV12Sistema_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_ATRIBUTOS_NOME", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A177Atributos_Nome, "@!"))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_ATRIBUTOS_TIPODADOS", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A178Atributos_TipoDados, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_ATRIBUTOS_DETALHES", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A390Atributos_Detalhes, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_ATRIBUTOS_DESCRICAO", GetSecureSignedToken( sPrefix, A179Atributos_Descricao));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_ATRIBUTOS_PK", GetSecureSignedToken( sPrefix, A400Atributos_PK));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_ATRIBUTOS_FK", GetSecureSignedToken( sPrefix, A401Atributos_FK));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_ATRIBUTOS_ATIVO", GetSecureSignedToken( sPrefix, A180Atributos_Ativo));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_ATRIBUTOS_TABELACOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A356Atributos_TabelaCod), "ZZZZZ9")));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = sPrefix + "hsh" + "AtributosGeneral";
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A356Atributos_TabelaCod), "ZZZZZ9");
         GxWebStd.gx_hidden_field( context, sPrefix+"hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("atributosgeneral:[SendSecurityCheck value for]"+"Atributos_TabelaCod:"+context.localUtil.Format( (decimal)(A356Atributos_TabelaCod), "ZZZZZ9"));
      }

      protected void RenderHtmlCloseForm4E2( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("atributosgeneral.js", "?202042822594551");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            context.WriteHtmlTextNl( "</form>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "AtributosGeneral" ;
      }

      public override String GetPgmdesc( )
      {
         return "Atributos General" ;
      }

      protected void WB4E0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "atributosgeneral.aspx");
            }
            wb_table1_2_4E2( true) ;
         }
         else
         {
            wb_table1_2_4E2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_4E2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtAtributos_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A176Atributos_Codigo), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A176Atributos_Codigo), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtAtributos_Codigo_Jsonclick, 0, "Attribute", "", "", "", edtAtributos_Codigo_Visible, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_AtributosGeneral.htm");
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtAtributos_TabelaCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A356Atributos_TabelaCod), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A356Atributos_TabelaCod), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtAtributos_TabelaCod_Jsonclick, 0, "Attribute", "", "", "", edtAtributos_TabelaCod_Visible, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_AtributosGeneral.htm");
         }
         wbLoad = true;
      }

      protected void START4E2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "Atributos General", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUP4E0( ) ;
            }
         }
      }

      protected void WS4E2( )
      {
         START4E2( ) ;
         EVT4E2( ) ;
      }

      protected void EVT4E2( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP4E0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP4E0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E114E2 */
                                    E114E2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP4E0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E124E2 */
                                    E124E2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOUPDATE'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP4E0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E134E2 */
                                    E134E2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DODELETE'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP4E0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E144E2 */
                                    E144E2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOFECHAR'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP4E0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E154E2 */
                                    E154E2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP4E0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                              }
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP4E0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WE4E2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseForm4E2( ) ;
            }
         }
      }

      protected void PA4E2( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            cmbAtributos_TipoDados.Name = "ATRIBUTOS_TIPODADOS";
            cmbAtributos_TipoDados.WebTags = "";
            cmbAtributos_TipoDados.addItem("", "Desconhecido", 0);
            cmbAtributos_TipoDados.addItem("N", "Numeric", 0);
            cmbAtributos_TipoDados.addItem("C", "Character", 0);
            cmbAtributos_TipoDados.addItem("VC", "Varchar", 0);
            cmbAtributos_TipoDados.addItem("D", "Date", 0);
            cmbAtributos_TipoDados.addItem("DT", "Date Time", 0);
            cmbAtributos_TipoDados.addItem("Bool", "Boolean", 0);
            cmbAtributos_TipoDados.addItem("Blob", "Blob", 0);
            cmbAtributos_TipoDados.addItem("Outr", "Outros", 0);
            if ( cmbAtributos_TipoDados.ItemCount > 0 )
            {
               A178Atributos_TipoDados = cmbAtributos_TipoDados.getValidValue(A178Atributos_TipoDados);
               n178Atributos_TipoDados = false;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A178Atributos_TipoDados", A178Atributos_TipoDados);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_ATRIBUTOS_TIPODADOS", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A178Atributos_TipoDados, ""))));
            }
            chkAtributos_PK.Name = "ATRIBUTOS_PK";
            chkAtributos_PK.WebTags = "";
            chkAtributos_PK.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkAtributos_PK_Internalname, "TitleCaption", chkAtributos_PK.Caption);
            chkAtributos_PK.CheckedValue = "false";
            chkAtributos_FK.Name = "ATRIBUTOS_FK";
            chkAtributos_FK.WebTags = "";
            chkAtributos_FK.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkAtributos_FK_Internalname, "TitleCaption", chkAtributos_FK.Caption);
            chkAtributos_FK.CheckedValue = "false";
            chkAtributos_Ativo.Name = "ATRIBUTOS_ATIVO";
            chkAtributos_Ativo.WebTags = "";
            chkAtributos_Ativo.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkAtributos_Ativo_Internalname, "TitleCaption", chkAtributos_Ativo.Caption);
            chkAtributos_Ativo.CheckedValue = "false";
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbAtributos_TipoDados.ItemCount > 0 )
         {
            A178Atributos_TipoDados = cmbAtributos_TipoDados.getValidValue(A178Atributos_TipoDados);
            n178Atributos_TipoDados = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A178Atributos_TipoDados", A178Atributos_TipoDados);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_ATRIBUTOS_TIPODADOS", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A178Atributos_TipoDados, ""))));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RF4E2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV15Pgmname = "AtributosGeneral";
         context.Gx_err = 0;
      }

      protected void RF4E2( )
      {
         initialize_formulas( ) ;
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Using cursor H004E2 */
            pr_default.execute(0, new Object[] {A176Atributos_Codigo});
            while ( (pr_default.getStatus(0) != 101) )
            {
               A190Tabela_SistemaCod = H004E2_A190Tabela_SistemaCod[0];
               n190Tabela_SistemaCod = H004E2_n190Tabela_SistemaCod[0];
               A356Atributos_TabelaCod = H004E2_A356Atributos_TabelaCod[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A356Atributos_TabelaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A356Atributos_TabelaCod), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_ATRIBUTOS_TABELACOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A356Atributos_TabelaCod), "ZZZZZ9")));
               A180Atributos_Ativo = H004E2_A180Atributos_Ativo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A180Atributos_Ativo", A180Atributos_Ativo);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_ATRIBUTOS_ATIVO", GetSecureSignedToken( sPrefix, A180Atributos_Ativo));
               A401Atributos_FK = H004E2_A401Atributos_FK[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A401Atributos_FK", A401Atributos_FK);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_ATRIBUTOS_FK", GetSecureSignedToken( sPrefix, A401Atributos_FK));
               n401Atributos_FK = H004E2_n401Atributos_FK[0];
               A400Atributos_PK = H004E2_A400Atributos_PK[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A400Atributos_PK", A400Atributos_PK);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_ATRIBUTOS_PK", GetSecureSignedToken( sPrefix, A400Atributos_PK));
               n400Atributos_PK = H004E2_n400Atributos_PK[0];
               A357Atributos_TabelaNom = H004E2_A357Atributos_TabelaNom[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A357Atributos_TabelaNom", A357Atributos_TabelaNom);
               n357Atributos_TabelaNom = H004E2_n357Atributos_TabelaNom[0];
               A179Atributos_Descricao = H004E2_A179Atributos_Descricao[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A179Atributos_Descricao", A179Atributos_Descricao);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_ATRIBUTOS_DESCRICAO", GetSecureSignedToken( sPrefix, A179Atributos_Descricao));
               n179Atributos_Descricao = H004E2_n179Atributos_Descricao[0];
               A390Atributos_Detalhes = H004E2_A390Atributos_Detalhes[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A390Atributos_Detalhes", A390Atributos_Detalhes);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_ATRIBUTOS_DETALHES", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A390Atributos_Detalhes, ""))));
               n390Atributos_Detalhes = H004E2_n390Atributos_Detalhes[0];
               A178Atributos_TipoDados = H004E2_A178Atributos_TipoDados[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A178Atributos_TipoDados", A178Atributos_TipoDados);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_ATRIBUTOS_TIPODADOS", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A178Atributos_TipoDados, ""))));
               n178Atributos_TipoDados = H004E2_n178Atributos_TipoDados[0];
               A177Atributos_Nome = H004E2_A177Atributos_Nome[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A177Atributos_Nome", A177Atributos_Nome);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_ATRIBUTOS_NOME", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A177Atributos_Nome, "@!"))));
               A190Tabela_SistemaCod = H004E2_A190Tabela_SistemaCod[0];
               n190Tabela_SistemaCod = H004E2_n190Tabela_SistemaCod[0];
               A357Atributos_TabelaNom = H004E2_A357Atributos_TabelaNom[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A357Atributos_TabelaNom", A357Atributos_TabelaNom);
               n357Atributos_TabelaNom = H004E2_n357Atributos_TabelaNom[0];
               /* Execute user event: E124E2 */
               E124E2 ();
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(0);
            WB4E0( ) ;
         }
      }

      protected void STRUP4E0( )
      {
         /* Before Start, stand alone formulas. */
         AV15Pgmname = "AtributosGeneral";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E114E2 */
         E114E2 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            A177Atributos_Nome = StringUtil.Upper( cgiGet( edtAtributos_Nome_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A177Atributos_Nome", A177Atributos_Nome);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_ATRIBUTOS_NOME", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A177Atributos_Nome, "@!"))));
            cmbAtributos_TipoDados.CurrentValue = cgiGet( cmbAtributos_TipoDados_Internalname);
            A178Atributos_TipoDados = cgiGet( cmbAtributos_TipoDados_Internalname);
            n178Atributos_TipoDados = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A178Atributos_TipoDados", A178Atributos_TipoDados);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_ATRIBUTOS_TIPODADOS", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A178Atributos_TipoDados, ""))));
            A390Atributos_Detalhes = cgiGet( edtAtributos_Detalhes_Internalname);
            n390Atributos_Detalhes = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A390Atributos_Detalhes", A390Atributos_Detalhes);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_ATRIBUTOS_DETALHES", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A390Atributos_Detalhes, ""))));
            A179Atributos_Descricao = cgiGet( edtAtributos_Descricao_Internalname);
            n179Atributos_Descricao = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A179Atributos_Descricao", A179Atributos_Descricao);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_ATRIBUTOS_DESCRICAO", GetSecureSignedToken( sPrefix, A179Atributos_Descricao));
            A357Atributos_TabelaNom = StringUtil.Upper( cgiGet( edtAtributos_TabelaNom_Internalname));
            n357Atributos_TabelaNom = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A357Atributos_TabelaNom", A357Atributos_TabelaNom);
            A400Atributos_PK = StringUtil.StrToBool( cgiGet( chkAtributos_PK_Internalname));
            n400Atributos_PK = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A400Atributos_PK", A400Atributos_PK);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_ATRIBUTOS_PK", GetSecureSignedToken( sPrefix, A400Atributos_PK));
            A401Atributos_FK = StringUtil.StrToBool( cgiGet( chkAtributos_FK_Internalname));
            n401Atributos_FK = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A401Atributos_FK", A401Atributos_FK);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_ATRIBUTOS_FK", GetSecureSignedToken( sPrefix, A401Atributos_FK));
            A180Atributos_Ativo = StringUtil.StrToBool( cgiGet( chkAtributos_Ativo_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A180Atributos_Ativo", A180Atributos_Ativo);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_ATRIBUTOS_ATIVO", GetSecureSignedToken( sPrefix, A180Atributos_Ativo));
            A356Atributos_TabelaCod = (int)(context.localUtil.CToN( cgiGet( edtAtributos_TabelaCod_Internalname), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A356Atributos_TabelaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A356Atributos_TabelaCod), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_ATRIBUTOS_TABELACOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A356Atributos_TabelaCod), "ZZZZZ9")));
            /* Read saved values. */
            wcpOA176Atributos_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA176Atributos_Codigo"), ",", "."));
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            forbiddenHiddens = sPrefix + "hsh" + "AtributosGeneral";
            A356Atributos_TabelaCod = (int)(context.localUtil.CToN( cgiGet( edtAtributos_TabelaCod_Internalname), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A356Atributos_TabelaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A356Atributos_TabelaCod), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_ATRIBUTOS_TABELACOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A356Atributos_TabelaCod), "ZZZZZ9")));
            forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A356Atributos_TabelaCod), "ZZZZZ9");
            hsh = cgiGet( sPrefix+"hsh");
            if ( ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
            {
               GXUtil.WriteLog("atributosgeneral:[SecurityCheckFailed value for]"+"Atributos_TabelaCod:"+context.localUtil.Format( (decimal)(A356Atributos_TabelaCod), "ZZZZZ9"));
               GxWebError = 1;
               context.HttpContext.Response.StatusDescription = 403.ToString();
               context.HttpContext.Response.StatusCode = 403;
               context.WriteHtmlText( "<title>403 Forbidden</title>") ;
               context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
               context.WriteHtmlText( "<p /><hr />") ;
               GXUtil.WriteLog("send_http_error_code " + 403.ToString());
               return  ;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E114E2 */
         E114E2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E114E2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         bttBtnupdate_Visible = (AV6WWPContext.gxTpr_Update ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, bttBtnupdate_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnupdate_Visible), 5, 0)));
         bttBtndelete_Visible = (AV6WWPContext.gxTpr_Delete ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, bttBtndelete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtndelete_Visible), 5, 0)));
      }

      protected void nextLoad( )
      {
      }

      protected void E124E2( )
      {
         /* Load Routine */
         edtAtributos_TabelaNom_Link = formatLink("viewtabela.aspx") + "?" + UrlEncode("" +A356Atributos_TabelaCod) + "," + UrlEncode(StringUtil.RTrim(""));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtAtributos_TabelaNom_Internalname, "Link", edtAtributos_TabelaNom_Link);
         edtAtributos_Codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtAtributos_Codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtAtributos_Codigo_Visible), 5, 0)));
         edtAtributos_TabelaCod_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtAtributos_TabelaCod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtAtributos_TabelaCod_Visible), 5, 0)));
         if ( ! ( ( AV6WWPContext.gxTpr_Update ) ) )
         {
            bttBtnupdate_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, bttBtnupdate_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnupdate_Enabled), 5, 0)));
         }
         if ( ! ( ( AV6WWPContext.gxTpr_Delete ) ) )
         {
            bttBtndelete_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, bttBtndelete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtndelete_Enabled), 5, 0)));
         }
         AV12Sistema_Codigo = A190Tabela_SistemaCod;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV12Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12Sistema_Codigo), 6, 0)));
      }

      protected void E134E2( )
      {
         /* 'DoUpdate' Routine */
         context.wjLoc = formatLink("atributos.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A176Atributos_Codigo) + "," + UrlEncode("" +AV12Sistema_Codigo);
         context.wjLocDisableFrm = 1;
      }

      protected void E144E2( )
      {
         /* 'DoDelete' Routine */
         context.wjLoc = formatLink("atributos.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A176Atributos_Codigo) + "," + UrlEncode("" +AV12Sistema_Codigo);
         context.wjLocDisableFrm = 1;
      }

      protected void E154E2( )
      {
         /* 'DoFechar' Routine */
         context.wjLoc = formatLink("wwatributos.aspx") ;
         context.wjLocDisableFrm = 1;
      }

      protected void S112( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8TrnContext.gxTpr_Callerobject = AV15Pgmname;
         AV8TrnContext.gxTpr_Callerondelete = false;
         AV8TrnContext.gxTpr_Callerurl = AV11HTTPRequest.ScriptName+"?"+AV11HTTPRequest.QueryString;
         AV8TrnContext.gxTpr_Transactionname = "Atributos";
         AV9TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV9TrnContextAtt.gxTpr_Attributename = "Atributos_Codigo";
         AV9TrnContextAtt.gxTpr_Attributevalue = StringUtil.Str( (decimal)(AV7Atributos_Codigo), 6, 0);
         AV8TrnContext.gxTpr_Attributes.Add(AV9TrnContextAtt, 0);
         AV10Session.Set("TrnContext", AV8TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_MeetrikaVs3"));
      }

      protected void wb_table1_2_4E2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, sPrefix, "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_4E2( true) ;
         }
         else
         {
            wb_table2_8_4E2( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_4E2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_54_4E2( true) ;
         }
         else
         {
            wb_table3_54_4E2( false) ;
         }
         return  ;
      }

      protected void wb_table3_54_4E2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_4E2e( true) ;
         }
         else
         {
            wb_table1_2_4E2e( false) ;
         }
      }

      protected void wb_table3_54_4E2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 57,'" + sPrefix + "',false,'',0)\"";
            ClassString = "btn btn-success";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnupdate_Internalname, "", "Modifica", bttBtnupdate_Jsonclick, 5, "Modifica", "", StyleString, ClassString, bttBtnupdate_Visible, bttBtnupdate_Enabled, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOUPDATE\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_AtributosGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 59,'" + sPrefix + "',false,'',0)\"";
            ClassString = "btn btn-default";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtndelete_Internalname, "", "Eliminar", bttBtndelete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtndelete_Visible, bttBtndelete_Enabled, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DODELETE\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_AtributosGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 61,'" + sPrefix + "',false,'',0)\"";
            ClassString = "btn";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnfechar_Internalname, "", "Fechar", bttBtnfechar_Jsonclick, 5, "Fechar", "", StyleString, ClassString, 1, 1, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOFECHAR\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_AtributosGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_54_4E2e( true) ;
         }
         else
         {
            wb_table3_54_4E2e( false) ;
         }
      }

      protected void wb_table2_8_4E2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "TableViewGeneralAtts", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockatributos_nome_Internalname, "Nome", "", "", lblTextblockatributos_nome_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_AtributosGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtAtributos_Nome_Internalname, StringUtil.RTrim( A177Atributos_Nome), StringUtil.RTrim( context.localUtil.Format( A177Atributos_Nome, "@!")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtAtributos_Nome_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Nome", "left", true, "HLP_AtributosGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockatributos_tipodados_Internalname, "Tipo de Dados", "", "", lblTextblockatributos_tipodados_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_AtributosGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbAtributos_TipoDados, cmbAtributos_TipoDados_Internalname, StringUtil.RTrim( A178Atributos_TipoDados), 1, cmbAtributos_TipoDados_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "char", "", 1, 0, 0, 0, 0, "em", 0, "", "", "BootstrapAttributeGray", "", "", "", true, "HLP_AtributosGeneral.htm");
            cmbAtributos_TipoDados.CurrentValue = StringUtil.RTrim( A178Atributos_TipoDados);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbAtributos_TipoDados_Internalname, "Values", (String)(cmbAtributos_TipoDados.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtAtributos_Detalhes_Internalname, StringUtil.RTrim( A390Atributos_Detalhes), StringUtil.RTrim( context.localUtil.Format( A390Atributos_Detalhes, "")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtAtributos_Detalhes_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 10, "chr", 1, "row", 10, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_AtributosGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockatributos_descricao_Internalname, "Descri��o", "", "", lblTextblockatributos_descricao_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_AtributosGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Multiple line edit */
            ClassString = "BootstrapAttributeGray";
            StyleString = "";
            ClassString = "BootstrapAttributeGray";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtAtributos_Descricao_Internalname, A179Atributos_Descricao, "", "", 0, 1, 0, 0, 80, "chr", 7, "row", StyleString, ClassString, "", "500", -1, "", "", -1, true, "DescricaoLonga", "HLP_AtributosGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockatributos_tabelanom_Internalname, "Tabela", "", "", lblTextblockatributos_tabelanom_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_AtributosGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"3\"  class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtAtributos_TabelaNom_Internalname, StringUtil.RTrim( A357Atributos_TabelaNom), StringUtil.RTrim( context.localUtil.Format( A357Atributos_TabelaNom, "@!")), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", edtAtributos_TabelaNom_Link, "", "", "", edtAtributos_TabelaNom_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Nome", "left", true, "HLP_AtributosGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockatributos_pk_Internalname, "PK", "", "", lblTextblockatributos_pk_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_AtributosGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Check box */
            ClassString = "BootstrapAttributeGray";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkAtributos_PK_Internalname, StringUtil.BoolToStr( A400Atributos_PK), "", "", 1, 0, "true", "", StyleString, ClassString, "", "");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockatributos_fk_Internalname, "FK", "", "", lblTextblockatributos_fk_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_AtributosGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Check box */
            ClassString = "BootstrapAttributeGray";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkAtributos_FK_Internalname, StringUtil.BoolToStr( A401Atributos_FK), "", "", 1, 0, "true", "", StyleString, ClassString, "", "");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockatributos_ativo_Internalname, "Ativo", "", "", lblTextblockatributos_ativo_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_AtributosGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Check box */
            ClassString = "BootstrapAttributeGray";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkAtributos_Ativo_Internalname, StringUtil.BoolToStr( A180Atributos_Ativo), "", "", 1, 0, "true", "", StyleString, ClassString, "", "");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_4E2e( true) ;
         }
         else
         {
            wb_table2_8_4E2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         A176Atributos_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A176Atributos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A176Atributos_Codigo), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA4E2( ) ;
         WS4E2( ) ;
         WE4E2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlA176Atributos_Codigo = (String)((String)getParm(obj,0));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PA4E2( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "atributosgeneral");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PA4E2( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            A176Atributos_Codigo = Convert.ToInt32(getParm(obj,2));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A176Atributos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A176Atributos_Codigo), 6, 0)));
         }
         wcpOA176Atributos_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA176Atributos_Codigo"), ",", "."));
         if ( ! GetJustCreated( ) && ( ( A176Atributos_Codigo != wcpOA176Atributos_Codigo ) ) )
         {
            setjustcreated();
         }
         wcpOA176Atributos_Codigo = A176Atributos_Codigo;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlA176Atributos_Codigo = cgiGet( sPrefix+"A176Atributos_Codigo_CTRL");
         if ( StringUtil.Len( sCtrlA176Atributos_Codigo) > 0 )
         {
            A176Atributos_Codigo = (int)(context.localUtil.CToN( cgiGet( sCtrlA176Atributos_Codigo), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A176Atributos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A176Atributos_Codigo), 6, 0)));
         }
         else
         {
            A176Atributos_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"A176Atributos_Codigo_PARM"), ",", "."));
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PA4E2( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WS4E2( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WS4E2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"A176Atributos_Codigo_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(A176Atributos_Codigo), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlA176Atributos_Codigo)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"A176Atributos_Codigo_CTRL", StringUtil.RTrim( sCtrlA176Atributos_Codigo));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WE4E2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?2249787");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?202042822594587");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("atributosgeneral.js", "?202042822594587");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblockatributos_nome_Internalname = sPrefix+"TEXTBLOCKATRIBUTOS_NOME";
         edtAtributos_Nome_Internalname = sPrefix+"ATRIBUTOS_NOME";
         lblTextblockatributos_tipodados_Internalname = sPrefix+"TEXTBLOCKATRIBUTOS_TIPODADOS";
         cmbAtributos_TipoDados_Internalname = sPrefix+"ATRIBUTOS_TIPODADOS";
         edtAtributos_Detalhes_Internalname = sPrefix+"ATRIBUTOS_DETALHES";
         lblTextblockatributos_descricao_Internalname = sPrefix+"TEXTBLOCKATRIBUTOS_DESCRICAO";
         edtAtributos_Descricao_Internalname = sPrefix+"ATRIBUTOS_DESCRICAO";
         lblTextblockatributos_tabelanom_Internalname = sPrefix+"TEXTBLOCKATRIBUTOS_TABELANOM";
         edtAtributos_TabelaNom_Internalname = sPrefix+"ATRIBUTOS_TABELANOM";
         lblTextblockatributos_pk_Internalname = sPrefix+"TEXTBLOCKATRIBUTOS_PK";
         chkAtributos_PK_Internalname = sPrefix+"ATRIBUTOS_PK";
         lblTextblockatributos_fk_Internalname = sPrefix+"TEXTBLOCKATRIBUTOS_FK";
         chkAtributos_FK_Internalname = sPrefix+"ATRIBUTOS_FK";
         lblTextblockatributos_ativo_Internalname = sPrefix+"TEXTBLOCKATRIBUTOS_ATIVO";
         chkAtributos_Ativo_Internalname = sPrefix+"ATRIBUTOS_ATIVO";
         tblTableattributes_Internalname = sPrefix+"TABLEATTRIBUTES";
         bttBtnupdate_Internalname = sPrefix+"BTNUPDATE";
         bttBtndelete_Internalname = sPrefix+"BTNDELETE";
         bttBtnfechar_Internalname = sPrefix+"BTNFECHAR";
         tblTableactions_Internalname = sPrefix+"TABLEACTIONS";
         tblTablecontent_Internalname = sPrefix+"TABLECONTENT";
         edtAtributos_Codigo_Internalname = sPrefix+"ATRIBUTOS_CODIGO";
         edtAtributos_TabelaCod_Internalname = sPrefix+"ATRIBUTOS_TABELACOD";
         Form.Internalname = sPrefix+"FORM";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         edtAtributos_TabelaNom_Jsonclick = "";
         edtAtributos_Detalhes_Jsonclick = "";
         cmbAtributos_TipoDados_Jsonclick = "";
         edtAtributos_Nome_Jsonclick = "";
         bttBtndelete_Enabled = 1;
         bttBtndelete_Visible = 1;
         bttBtnupdate_Enabled = 1;
         bttBtnupdate_Visible = 1;
         edtAtributos_TabelaNom_Link = "";
         chkAtributos_Ativo.Caption = "";
         chkAtributos_FK.Caption = "";
         chkAtributos_PK.Caption = "";
         edtAtributos_TabelaCod_Jsonclick = "";
         edtAtributos_TabelaCod_Visible = 1;
         edtAtributos_Codigo_Jsonclick = "";
         edtAtributos_Codigo_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("'DOUPDATE'","{handler:'E134E2',iparms:[{av:'A176Atributos_Codigo',fld:'ATRIBUTOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV12Sistema_Codigo',fld:'vSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV12Sistema_Codigo',fld:'vSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("'DODELETE'","{handler:'E144E2',iparms:[{av:'A176Atributos_Codigo',fld:'ATRIBUTOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV12Sistema_Codigo',fld:'vSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV12Sistema_Codigo',fld:'vSISTEMA_CODIGO',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("'DOFECHAR'","{handler:'E154E2',iparms:[],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         AV15Pgmname = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         A177Atributos_Nome = "";
         A178Atributos_TipoDados = "";
         A390Atributos_Detalhes = "";
         A179Atributos_Descricao = "";
         GXKey = "";
         forbiddenHiddens = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         scmdbuf = "";
         H004E2_A176Atributos_Codigo = new int[1] ;
         H004E2_A190Tabela_SistemaCod = new int[1] ;
         H004E2_n190Tabela_SistemaCod = new bool[] {false} ;
         H004E2_A356Atributos_TabelaCod = new int[1] ;
         H004E2_A180Atributos_Ativo = new bool[] {false} ;
         H004E2_A401Atributos_FK = new bool[] {false} ;
         H004E2_n401Atributos_FK = new bool[] {false} ;
         H004E2_A400Atributos_PK = new bool[] {false} ;
         H004E2_n400Atributos_PK = new bool[] {false} ;
         H004E2_A357Atributos_TabelaNom = new String[] {""} ;
         H004E2_n357Atributos_TabelaNom = new bool[] {false} ;
         H004E2_A179Atributos_Descricao = new String[] {""} ;
         H004E2_n179Atributos_Descricao = new bool[] {false} ;
         H004E2_A390Atributos_Detalhes = new String[] {""} ;
         H004E2_n390Atributos_Detalhes = new bool[] {false} ;
         H004E2_A178Atributos_TipoDados = new String[] {""} ;
         H004E2_n178Atributos_TipoDados = new bool[] {false} ;
         H004E2_A177Atributos_Nome = new String[] {""} ;
         A357Atributos_TabelaNom = "";
         hsh = "";
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV11HTTPRequest = new GxHttpRequest( context);
         AV9TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV10Session = context.GetSession();
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         bttBtnupdate_Jsonclick = "";
         bttBtndelete_Jsonclick = "";
         bttBtnfechar_Jsonclick = "";
         lblTextblockatributos_nome_Jsonclick = "";
         lblTextblockatributos_tipodados_Jsonclick = "";
         lblTextblockatributos_descricao_Jsonclick = "";
         lblTextblockatributos_tabelanom_Jsonclick = "";
         lblTextblockatributos_pk_Jsonclick = "";
         lblTextblockatributos_fk_Jsonclick = "";
         lblTextblockatributos_ativo_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlA176Atributos_Codigo = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.atributosgeneral__default(),
            new Object[][] {
                new Object[] {
               H004E2_A176Atributos_Codigo, H004E2_A190Tabela_SistemaCod, H004E2_n190Tabela_SistemaCod, H004E2_A356Atributos_TabelaCod, H004E2_A180Atributos_Ativo, H004E2_A401Atributos_FK, H004E2_n401Atributos_FK, H004E2_A400Atributos_PK, H004E2_n400Atributos_PK, H004E2_A357Atributos_TabelaNom,
               H004E2_n357Atributos_TabelaNom, H004E2_A179Atributos_Descricao, H004E2_n179Atributos_Descricao, H004E2_A390Atributos_Detalhes, H004E2_n390Atributos_Detalhes, H004E2_A178Atributos_TipoDados, H004E2_n178Atributos_TipoDados, H004E2_A177Atributos_Nome
               }
            }
         );
         AV15Pgmname = "AtributosGeneral";
         /* GeneXus formulas. */
         AV15Pgmname = "AtributosGeneral";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short initialized ;
      private short wbEnd ;
      private short wbStart ;
      private short nDraw ;
      private short nDoneStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXWrapped ;
      private int A176Atributos_Codigo ;
      private int wcpOA176Atributos_Codigo ;
      private int AV12Sistema_Codigo ;
      private int A356Atributos_TabelaCod ;
      private int edtAtributos_Codigo_Visible ;
      private int edtAtributos_TabelaCod_Visible ;
      private int A190Tabela_SistemaCod ;
      private int bttBtnupdate_Visible ;
      private int bttBtndelete_Visible ;
      private int bttBtnupdate_Enabled ;
      private int bttBtndelete_Enabled ;
      private int AV7Atributos_Codigo ;
      private int idxLst ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String AV15Pgmname ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String A177Atributos_Nome ;
      private String A178Atributos_TipoDados ;
      private String A390Atributos_Detalhes ;
      private String GXKey ;
      private String forbiddenHiddens ;
      private String GX_FocusControl ;
      private String edtAtributos_Codigo_Internalname ;
      private String edtAtributos_Codigo_Jsonclick ;
      private String edtAtributos_TabelaCod_Internalname ;
      private String edtAtributos_TabelaCod_Jsonclick ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String chkAtributos_PK_Internalname ;
      private String chkAtributos_FK_Internalname ;
      private String chkAtributos_Ativo_Internalname ;
      private String scmdbuf ;
      private String A357Atributos_TabelaNom ;
      private String edtAtributos_Nome_Internalname ;
      private String cmbAtributos_TipoDados_Internalname ;
      private String edtAtributos_Detalhes_Internalname ;
      private String edtAtributos_Descricao_Internalname ;
      private String edtAtributos_TabelaNom_Internalname ;
      private String hsh ;
      private String bttBtnupdate_Internalname ;
      private String bttBtndelete_Internalname ;
      private String edtAtributos_TabelaNom_Link ;
      private String sStyleString ;
      private String tblTablecontent_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String tblTableactions_Internalname ;
      private String TempTags ;
      private String bttBtnupdate_Jsonclick ;
      private String bttBtndelete_Jsonclick ;
      private String bttBtnfechar_Internalname ;
      private String bttBtnfechar_Jsonclick ;
      private String tblTableattributes_Internalname ;
      private String lblTextblockatributos_nome_Internalname ;
      private String lblTextblockatributos_nome_Jsonclick ;
      private String edtAtributos_Nome_Jsonclick ;
      private String lblTextblockatributos_tipodados_Internalname ;
      private String lblTextblockatributos_tipodados_Jsonclick ;
      private String cmbAtributos_TipoDados_Jsonclick ;
      private String edtAtributos_Detalhes_Jsonclick ;
      private String lblTextblockatributos_descricao_Internalname ;
      private String lblTextblockatributos_descricao_Jsonclick ;
      private String lblTextblockatributos_tabelanom_Internalname ;
      private String lblTextblockatributos_tabelanom_Jsonclick ;
      private String edtAtributos_TabelaNom_Jsonclick ;
      private String lblTextblockatributos_pk_Internalname ;
      private String lblTextblockatributos_pk_Jsonclick ;
      private String lblTextblockatributos_fk_Internalname ;
      private String lblTextblockatributos_fk_Jsonclick ;
      private String lblTextblockatributos_ativo_Internalname ;
      private String lblTextblockatributos_ativo_Jsonclick ;
      private String sCtrlA176Atributos_Codigo ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool A400Atributos_PK ;
      private bool A401Atributos_FK ;
      private bool A180Atributos_Ativo ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n178Atributos_TipoDados ;
      private bool n190Tabela_SistemaCod ;
      private bool n401Atributos_FK ;
      private bool n400Atributos_PK ;
      private bool n357Atributos_TabelaNom ;
      private bool n179Atributos_Descricao ;
      private bool n390Atributos_Detalhes ;
      private bool returnInSub ;
      private String A179Atributos_Descricao ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbAtributos_TipoDados ;
      private GXCheckbox chkAtributos_PK ;
      private GXCheckbox chkAtributos_FK ;
      private GXCheckbox chkAtributos_Ativo ;
      private IDataStoreProvider pr_default ;
      private int[] H004E2_A176Atributos_Codigo ;
      private int[] H004E2_A190Tabela_SistemaCod ;
      private bool[] H004E2_n190Tabela_SistemaCod ;
      private int[] H004E2_A356Atributos_TabelaCod ;
      private bool[] H004E2_A180Atributos_Ativo ;
      private bool[] H004E2_A401Atributos_FK ;
      private bool[] H004E2_n401Atributos_FK ;
      private bool[] H004E2_A400Atributos_PK ;
      private bool[] H004E2_n400Atributos_PK ;
      private String[] H004E2_A357Atributos_TabelaNom ;
      private bool[] H004E2_n357Atributos_TabelaNom ;
      private String[] H004E2_A179Atributos_Descricao ;
      private bool[] H004E2_n179Atributos_Descricao ;
      private String[] H004E2_A390Atributos_Detalhes ;
      private bool[] H004E2_n390Atributos_Detalhes ;
      private String[] H004E2_A178Atributos_TipoDados ;
      private bool[] H004E2_n178Atributos_TipoDados ;
      private String[] H004E2_A177Atributos_Nome ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV11HTTPRequest ;
      private IGxSession AV10Session ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV8TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV9TrnContextAtt ;
   }

   public class atributosgeneral__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH004E2 ;
          prmH004E2 = new Object[] {
          new Object[] {"@Atributos_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H004E2", "SELECT T1.[Atributos_Codigo], T2.[Tabela_SistemaCod], T1.[Atributos_TabelaCod] AS Atributos_TabelaCod, T1.[Atributos_Ativo], T1.[Atributos_FK], T1.[Atributos_PK], T2.[Tabela_Nome] AS Atributos_TabelaNom, T1.[Atributos_Descricao], T1.[Atributos_Detalhes], T1.[Atributos_TipoDados], T1.[Atributos_Nome] FROM ([Atributos] T1 WITH (NOLOCK) INNER JOIN [Tabela] T2 WITH (NOLOCK) ON T2.[Tabela_Codigo] = T1.[Atributos_TabelaCod]) WHERE T1.[Atributos_Codigo] = @Atributos_Codigo ORDER BY T1.[Atributos_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH004E2,1,0,true,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.getBool(4) ;
                ((bool[]) buf[5])[0] = rslt.getBool(5) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((bool[]) buf[7])[0] = rslt.getBool(6) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((String[]) buf[9])[0] = rslt.getString(7, 50) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(7);
                ((String[]) buf[11])[0] = rslt.getLongVarchar(8) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(8);
                ((String[]) buf[13])[0] = rslt.getString(9, 10) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(9);
                ((String[]) buf[15])[0] = rslt.getString(10, 4) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(10);
                ((String[]) buf[17])[0] = rslt.getString(11, 50) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
