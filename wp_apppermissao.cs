/*
               File: WP_AppPermissao
        Description: Manuten��o da Permissao
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 23:17:4.28
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wp_apppermissao : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wp_apppermissao( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wp_apppermissao( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref String aP0_Gx_mode ,
                           ref long aP1_ApplicationId ,
                           ref String aP2_GUID )
      {
         this.Gx_mode = aP0_Gx_mode;
         this.AV8ApplicationId = aP1_ApplicationId;
         this.AV14GUID = aP2_GUID;
         executePrivate();
         aP0_Gx_mode=this.Gx_mode;
         aP1_ApplicationId=this.AV8ApplicationId;
         aP2_GUID=this.AV14GUID;
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavAccesstype = new GXCombobox();
         chkavIsparent = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               Gx_mode = gxfirstwebparm;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
               {
                  AV8ApplicationId = (long)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8ApplicationId", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8ApplicationId), 12, 0)));
                  AV14GUID = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14GUID", AV14GUID);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vGUID", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV14GUID, ""))));
               }
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("workwithplusbootstrapmasterpage", "GeneXus.Programs.workwithplusbootstrapmasterpage", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PA8T2( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            START8T2( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20204282317439");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wp_apppermissao.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV8ApplicationId) + "," + UrlEncode(StringUtil.RTrim(AV14GUID))+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "vAPPLICATIONID", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV8ApplicationId), 12, 0, ",", "")));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = "hsh" + "WP_AppPermissao";
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( AV5GAMApplication.gxTpr_Name, "ZZZZZ9"));
         GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("wp_apppermissao:[SendSecurityCheck value for]"+"GXV1:"+StringUtil.RTrim( context.localUtil.Format( AV5GAMApplication.gxTpr_Name, "ZZZZZ9")));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         context.WriteHtmlTextNl( "</form>") ;
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WE8T2( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVT8T2( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wp_apppermissao.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV8ApplicationId) + "," + UrlEncode(StringUtil.RTrim(AV14GUID)) ;
      }

      public override String GetPgmname( )
      {
         return "WP_AppPermissao" ;
      }

      public override String GetPgmdesc( )
      {
         return "Manuten��o da Permissao" ;
      }

      protected void WB8T0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, "", "", "", "false");
            wb_table1_2_8T2( true) ;
         }
         else
         {
            wb_table1_2_8T2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_8T2e( bool wbgen )
      {
         if ( wbgen )
         {
         }
         wbLoad = true;
      }

      protected void START8T2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Manuten��o da Permissao", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUP8T0( ) ;
      }

      protected void WS8T2( )
      {
         START8T2( ) ;
         EVT8T2( ) ;
      }

      protected void EVT8T2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E118T2 */
                              E118T2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              context.wbHandled = 1;
                              if ( ! wbErr )
                              {
                                 Rfr0gs = false;
                                 if ( ! Rfr0gs )
                                 {
                                    /* Execute user event: E128T2 */
                                    E128T2 ();
                                 }
                                 dynload_actions( ) ;
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'CLOSE'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E138T2 */
                              E138T2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E148T2 */
                              E148T2 ();
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WE8T2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PA8T2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            cmbavAccesstype.Name = "vACCESSTYPE";
            cmbavAccesstype.WebTags = "";
            cmbavAccesstype.addItem("A", "Allow", 0);
            cmbavAccesstype.addItem("R", "Restricted", 0);
            if ( cmbavAccesstype.ItemCount > 0 )
            {
               AV20AccessType = cmbavAccesstype.getValidValue(AV20AccessType);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20AccessType", AV20AccessType);
            }
            chkavIsparent.Name = "vISPARENT";
            chkavIsparent.WebTags = "";
            chkavIsparent.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavIsparent_Internalname, "TitleCaption", chkavIsparent.Caption);
            chkavIsparent.CheckedValue = "false";
            if ( toggleJsOutput )
            {
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = edtavCtlname_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavAccesstype.ItemCount > 0 )
         {
            AV20AccessType = cmbavAccesstype.getValidValue(AV20AccessType);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20AccessType", AV20AccessType);
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RF8T2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
         edtavCtlname_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCtlname_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCtlname_Enabled), 5, 0)));
         edtavGuid_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavGuid_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavGuid_Enabled), 5, 0)));
      }

      protected void RF8T2( )
      {
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Execute user event: E148T2 */
            E148T2 ();
            WB8T0( ) ;
         }
      }

      protected void STRUP8T0( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         edtavCtlname_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCtlname_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCtlname_Enabled), 5, 0)));
         edtavGuid_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavGuid_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavGuid_Enabled), 5, 0)));
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E118T2 */
         E118T2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            AV5GAMApplication.gxTpr_Name = cgiGet( edtavCtlname_Internalname);
            AV14GUID = cgiGet( edtavGuid_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14GUID", AV14GUID);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vGUID", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV14GUID, ""))));
            AV16Name = cgiGet( edtavName_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16Name", AV16Name);
            AV10Dsc = cgiGet( edtavDsc_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10Dsc", AV10Dsc);
            cmbavAccesstype.CurrentValue = cgiGet( cmbavAccesstype_Internalname);
            AV20AccessType = cgiGet( cmbavAccesstype_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20AccessType", AV20AccessType);
            AV15IsParent = StringUtil.StrToBool( cgiGet( chkavIsparent_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15IsParent", AV15IsParent);
            /* Read saved values. */
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            forbiddenHiddens = "hsh" + "WP_AppPermissao";
            AV5GAMApplication.gxTpr_Name = cgiGet( edtavCtlname_Internalname);
            forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( AV5GAMApplication.gxTpr_Name, "ZZZZZ9"));
            hsh = cgiGet( "hsh");
            if ( ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
            {
               GXUtil.WriteLog("wp_apppermissao:[SecurityCheckFailed value for]"+"GXV1:"+StringUtil.RTrim( context.localUtil.Format( AV5GAMApplication.gxTpr_Name, "ZZZZZ9")));
               GxWebError = 1;
               context.HttpContext.Response.StatusDescription = 403.ToString();
               context.HttpContext.Response.StatusCode = 403;
               context.WriteHtmlText( "<title>403 Forbidden</title>") ;
               context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
               context.WriteHtmlText( "<p /><hr />") ;
               GXUtil.WriteLog("send_http_error_code " + 403.ToString());
               return  ;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E118T2 */
         E118T2 ();
         if (returnInSub) return;
      }

      protected void E118T2( )
      {
         /* Start Routine */
         AV5GAMApplication.load( AV8ApplicationId);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8ApplicationId", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8ApplicationId), 12, 0)));
         chkavIsparent.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavIsparent_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavIsparent.Enabled), 5, 0)));
         AV9ApplicationPermission = AV5GAMApplication.getpermissionbyguid(AV14GUID, out  AV12Errors);
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            AV14GUID = AV9ApplicationPermission.gxTpr_Guid;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14GUID", AV14GUID);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vGUID", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV14GUID, ""))));
            AV16Name = AV9ApplicationPermission.gxTpr_Name;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16Name", AV16Name);
            AV10Dsc = AV9ApplicationPermission.gxTpr_Description;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10Dsc", AV10Dsc);
            AV20AccessType = AV9ApplicationPermission.gxTpr_Accesstype;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20AccessType", AV20AccessType);
            AV15IsParent = AV9ApplicationPermission.gxTpr_Isparent;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15IsParent", AV15IsParent);
         }
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            bttBtnconfirm_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnconfirm_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnconfirm_Visible), 5, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            edtavName_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavName_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavName_Enabled), 5, 0)));
            edtavDsc_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDsc_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDsc_Enabled), 5, 0)));
            cmbavAccesstype.Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavAccesstype_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavAccesstype.Enabled), 5, 0)));
            bttBtnconfirm_Caption = "Delete";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnconfirm_Internalname, "Caption", bttBtnconfirm_Caption);
         }
      }

      public void GXEnter( )
      {
         /* Execute user event: E128T2 */
         E128T2 ();
         if (returnInSub) return;
      }

      protected void E128T2( )
      {
         /* Enter Routine */
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            AV9ApplicationPermission.gxTpr_Name = AV16Name;
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV9ApplicationPermission", AV9ApplicationPermission);
            AV9ApplicationPermission.gxTpr_Description = AV10Dsc;
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV9ApplicationPermission", AV9ApplicationPermission);
            AV9ApplicationPermission.gxTpr_Accesstype = AV20AccessType;
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV9ApplicationPermission", AV9ApplicationPermission);
            AV6isOK = AV5GAMApplication.addpermission(AV9ApplicationPermission, out  AV12Errors);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6isOK", AV6isOK);
         }
         else if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
         {
            AV9ApplicationPermission.gxTpr_Name = AV16Name;
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV9ApplicationPermission", AV9ApplicationPermission);
            AV9ApplicationPermission.gxTpr_Description = AV10Dsc;
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV9ApplicationPermission", AV9ApplicationPermission);
            AV9ApplicationPermission.gxTpr_Accesstype = AV20AccessType;
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV9ApplicationPermission", AV9ApplicationPermission);
            AV6isOK = AV5GAMApplication.updatepermission(AV9ApplicationPermission, out  AV12Errors);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6isOK", AV6isOK);
         }
         else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
         {
            AV6isOK = AV5GAMApplication.deletepermission(AV9ApplicationPermission, out  AV12Errors);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6isOK", AV6isOK);
         }
         if ( AV6isOK )
         {
            context.CommitDataStores( "WP_AppPermissao");
            context.setWebReturnParms(new Object[] {(String)Gx_mode,(long)AV8ApplicationId,(String)AV14GUID});
            context.wjLocDisableFrm = 1;
            context.nUserReturn = 1;
            returnInSub = true;
            if (true) return;
         }
         else
         {
            AV25GXV2 = 1;
            while ( AV25GXV2 <= AV12Errors.Count )
            {
               AV11Error = ((SdtGAMError)AV12Errors.Item(AV25GXV2));
               GX_msglist.addItem(StringUtil.Format( "%1 (GAM%2)", AV11Error.gxTpr_Message, StringUtil.LTrim( StringUtil.Str( (decimal)(AV11Error.gxTpr_Code), 12, 0)), "", "", "", "", "", "", ""));
               AV25GXV2 = (int)(AV25GXV2+1);
            }
         }
      }

      protected void E138T2( )
      {
         /* 'Close' Routine */
         context.setWebReturnParms(new Object[] {(String)Gx_mode,(long)AV8ApplicationId,(String)AV14GUID});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void nextLoad( )
      {
      }

      protected void E148T2( )
      {
         /* Load Routine */
      }

      protected void wb_table1_2_8T2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTblmain_Internalname, tblTblmain_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "height:17px;width:150px")+"\" class='Table'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbapplication_Internalname, "Applicativo", "", "", lblTbapplication_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_WP_AppPermissao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "width:300px")+"\" class='Table'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtavCtlname_Internalname, StringUtil.RTrim( AV5GAMApplication.gxTpr_Name), StringUtil.RTrim( context.localUtil.Format( AV5GAMApplication.gxTpr_Name, "ZZZZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavCtlname_Jsonclick, 0, "Attribute", "", "", "", 1, edtavCtlname_Enabled, 0, "text", "", 421, "px", 1, "row", 254, 0, 0, 0, 1, 0, -1, true, "", "", true, "HLP_WP_AppPermissao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbguid_Internalname, "GUID", "", "", lblTbguid_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_WP_AppPermissao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtavGuid_Internalname, StringUtil.RTrim( AV14GUID), StringUtil.RTrim( context.localUtil.Format( AV14GUID, "")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavGuid_Jsonclick, 0, "Attribute", "", "", "", 1, edtavGuid_Enabled, 0, "text", "", 40, "chr", 1, "row", 40, 0, 0, 0, 1, -1, 0, true, "GAMGUID", "left", true, "HLP_WP_AppPermissao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "height:16px")+"\" class='Table'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbname_Internalname, "Nome", "", "", lblTbname_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_WP_AppPermissao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 17,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavName_Internalname, StringUtil.RTrim( AV16Name), StringUtil.RTrim( context.localUtil.Format( AV16Name, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,17);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavName_Jsonclick, 0, "Attribute", "", "", "", 1, edtavName_Enabled, 1, "text", "", 421, "px", 1, "row", 254, 0, 0, 0, 1, -1, -1, true, "GAMDescriptionLong", "left", true, "HLP_WP_AppPermissao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "height:16px")+"\" class='Table'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbdsc_Internalname, "Descri��o", "", "", lblTbdsc_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_WP_AppPermissao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 22,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavDsc_Internalname, StringUtil.RTrim( AV10Dsc), StringUtil.RTrim( context.localUtil.Format( AV10Dsc, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,22);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDsc_Jsonclick, 0, "Attribute", "", "", "", 1, edtavDsc_Enabled, 1, "text", "", 421, "px", 1, "row", 254, 0, 0, 0, 1, -1, -1, true, "GAMDescriptionLong", "left", true, "HLP_WP_AppPermissao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbddefaulttype_Internalname, "Acesso Default", "", "", lblTbddefaulttype_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_WP_AppPermissao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 27,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavAccesstype, cmbavAccesstype_Internalname, StringUtil.RTrim( AV20AccessType), 1, cmbavAccesstype_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", 1, cmbavAccesstype.Enabled, 1, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,27);\"", "", true, "HLP_WP_AppPermissao.htm");
            cmbavAccesstype.CurrentValue = StringUtil.RTrim( AV20AccessType);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavAccesstype_Internalname, "Values", (String)(cmbavAccesstype.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbisparent_Internalname, "� uma permiss�o Pai?", "", "", lblTbisparent_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_WP_AppPermissao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 32,'',false,'',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavIsparent_Internalname, StringUtil.BoolToStr( AV15IsParent), "", "", 1, chkavIsparent.Enabled, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(32, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,32);\"");
            context.WriteHtmlText( "&nbsp;&nbsp;&nbsp;&nbsp; ") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr class='Table'>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Table'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 39,'',false,'',0)\"";
            ClassString = "Button";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnconfirm_Internalname, "", bttBtnconfirm_Caption, bttBtnconfirm_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtnconfirm_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_AppPermissao.htm");
            context.WriteHtmlText( "&nbsp;&nbsp;&nbsp;&nbsp; ") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 40,'',false,'',0)\"";
            ClassString = "Button";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnclose_Internalname, "", "Voltar", bttBtnclose_Jsonclick, 5, "Voltar", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"E\\'CLOSE\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_AppPermissao.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_8T2e( true) ;
         }
         else
         {
            wb_table1_2_8T2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         Gx_mode = (String)getParm(obj,0);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         AV8ApplicationId = Convert.ToInt64(getParm(obj,1));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8ApplicationId", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8ApplicationId), 12, 0)));
         AV14GUID = (String)getParm(obj,2);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14GUID", AV14GUID);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vGUID", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV14GUID, ""))));
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA8T2( ) ;
         WS8T2( ) ;
         WE8T2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?2249787");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?2020428231757");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wp_apppermissao.js", "?2020428231757");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTbapplication_Internalname = "TBAPPLICATION";
         edtavCtlname_Internalname = "CTLNAME";
         lblTbguid_Internalname = "TBGUID";
         edtavGuid_Internalname = "vGUID";
         lblTbname_Internalname = "TBNAME";
         edtavName_Internalname = "vNAME";
         lblTbdsc_Internalname = "TBDSC";
         edtavDsc_Internalname = "vDSC";
         lblTbddefaulttype_Internalname = "TBDDEFAULTTYPE";
         cmbavAccesstype_Internalname = "vACCESSTYPE";
         lblTbisparent_Internalname = "TBISPARENT";
         chkavIsparent_Internalname = "vISPARENT";
         bttBtnconfirm_Internalname = "BTNCONFIRM";
         bttBtnclose_Internalname = "BTNCLOSE";
         tblTblmain_Internalname = "TBLMAIN";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         init_default_properties( ) ;
         bttBtnconfirm_Visible = 1;
         cmbavAccesstype_Jsonclick = "";
         edtavDsc_Jsonclick = "";
         edtavName_Jsonclick = "";
         edtavGuid_Jsonclick = "";
         edtavGuid_Enabled = 0;
         edtavCtlname_Jsonclick = "";
         edtavCtlname_Enabled = 0;
         bttBtnconfirm_Caption = "Confirmar";
         cmbavAccesstype.Enabled = 1;
         edtavDsc_Enabled = 1;
         edtavName_Enabled = 1;
         chkavIsparent.Enabled = 1;
         edtavCtlname_Enabled = -1;
         chkavIsparent.Caption = "";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Manuten��o da Permissao";
      }

      protected override bool IsSpaSupported( )
      {
         return false ;
      }

      public override void InitializeDynEvents( )
      {
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         wcpOGx_mode = "";
         wcpOAV14GUID = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         GXKey = "";
         forbiddenHiddens = "";
         AV5GAMApplication = new SdtGAMApplication(context);
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV20AccessType = "";
         AV16Name = "";
         AV10Dsc = "";
         hsh = "";
         AV9ApplicationPermission = new SdtGAMApplicationPermission(context);
         AV12Errors = new GxExternalCollection( context, "SdtGAMError", "GeneXus.Programs");
         AV11Error = new SdtGAMError(context);
         sStyleString = "";
         lblTbapplication_Jsonclick = "";
         lblTbguid_Jsonclick = "";
         lblTbname_Jsonclick = "";
         TempTags = "";
         lblTbdsc_Jsonclick = "";
         lblTbddefaulttype_Jsonclick = "";
         lblTbisparent_Jsonclick = "";
         ClassString = "";
         StyleString = "";
         bttBtnconfirm_Jsonclick = "";
         bttBtnclose_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wp_apppermissao__default(),
            new Object[][] {
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
         edtavCtlname_Enabled = 0;
         edtavGuid_Enabled = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXWrapped ;
      private int edtavCtlname_Enabled ;
      private int edtavGuid_Enabled ;
      private int bttBtnconfirm_Visible ;
      private int edtavName_Enabled ;
      private int edtavDsc_Enabled ;
      private int AV25GXV2 ;
      private int idxLst ;
      private long AV8ApplicationId ;
      private long wcpOAV8ApplicationId ;
      private String Gx_mode ;
      private String AV14GUID ;
      private String wcpOGx_mode ;
      private String wcpOAV14GUID ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String GXKey ;
      private String forbiddenHiddens ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String AV20AccessType ;
      private String chkavIsparent_Internalname ;
      private String edtavCtlname_Internalname ;
      private String edtavGuid_Internalname ;
      private String AV16Name ;
      private String edtavName_Internalname ;
      private String AV10Dsc ;
      private String edtavDsc_Internalname ;
      private String cmbavAccesstype_Internalname ;
      private String hsh ;
      private String bttBtnconfirm_Internalname ;
      private String bttBtnconfirm_Caption ;
      private String sStyleString ;
      private String tblTblmain_Internalname ;
      private String lblTbapplication_Internalname ;
      private String lblTbapplication_Jsonclick ;
      private String edtavCtlname_Jsonclick ;
      private String lblTbguid_Internalname ;
      private String lblTbguid_Jsonclick ;
      private String edtavGuid_Jsonclick ;
      private String lblTbname_Internalname ;
      private String lblTbname_Jsonclick ;
      private String TempTags ;
      private String edtavName_Jsonclick ;
      private String lblTbdsc_Internalname ;
      private String lblTbdsc_Jsonclick ;
      private String edtavDsc_Jsonclick ;
      private String lblTbddefaulttype_Internalname ;
      private String lblTbddefaulttype_Jsonclick ;
      private String cmbavAccesstype_Jsonclick ;
      private String lblTbisparent_Internalname ;
      private String lblTbisparent_Jsonclick ;
      private String ClassString ;
      private String StyleString ;
      private String bttBtnconfirm_Jsonclick ;
      private String bttBtnclose_Internalname ;
      private String bttBtnclose_Jsonclick ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool AV15IsParent ;
      private bool returnInSub ;
      private bool AV6isOK ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private String aP0_Gx_mode ;
      private long aP1_ApplicationId ;
      private String aP2_GUID ;
      private GXCombobox cmbavAccesstype ;
      private GXCheckbox chkavIsparent ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private IDataStoreProvider pr_default ;
      [ObjectCollection(ItemType=typeof( SdtGAMError ))]
      private IGxCollection AV12Errors ;
      private GXWebForm Form ;
      private SdtGAMApplication AV5GAMApplication ;
      private SdtGAMApplicationPermission AV9ApplicationPermission ;
      private SdtGAMError AV11Error ;
   }

   public class wp_apppermissao__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          def= new CursorDef[] {
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
       }
    }

 }

}
