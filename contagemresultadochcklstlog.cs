/*
               File: ContagemResultadoChckLstLog
        Description: Contagem Resultado Chck Lst Log
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/2/2020 16:17:17.27
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class contagemresultadochcklstlog : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"NAOCONFORMIDADE_CODIGO") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            GXDLANAOCONFORMIDADE_CODIGO2L207( ) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_5") == 0 )
         {
            A1853ContagemResultadoChckLstLog_OSCodigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1853ContagemResultadoChckLstLog_OSCodigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1853ContagemResultadoChckLstLog_OSCodigo), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_5( A1853ContagemResultadoChckLstLog_OSCodigo) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_6") == 0 )
         {
            A811ContagemResultadoChckLstLog_ChckLstCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n811ContagemResultadoChckLstLog_ChckLstCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A811ContagemResultadoChckLstLog_ChckLstCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A811ContagemResultadoChckLstLog_ChckLstCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_6( A811ContagemResultadoChckLstLog_ChckLstCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_4") == 0 )
         {
            A426NaoConformidade_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n426NaoConformidade_Codigo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A426NaoConformidade_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A426NaoConformidade_Codigo), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_4( A426NaoConformidade_Codigo) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_7") == 0 )
         {
            A822ContagemResultadoChckLstLog_UsuarioCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A822ContagemResultadoChckLstLog_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A822ContagemResultadoChckLstLog_UsuarioCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_7( A822ContagemResultadoChckLstLog_UsuarioCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_8") == 0 )
         {
            A823ContagemResultadoChckLstLog_PessoaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n823ContagemResultadoChckLstLog_PessoaCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A823ContagemResultadoChckLstLog_PessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A823ContagemResultadoChckLstLog_PessoaCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_8( A823ContagemResultadoChckLstLog_PessoaCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         dynNaoConformidade_Codigo.Name = "NAOCONFORMIDADE_CODIGO";
         dynNaoConformidade_Codigo.WebTags = "";
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Contagem Resultado Chck Lst Log", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = edtContagemResultadoChckLstLog_Codigo_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public contagemresultadochcklstlog( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public contagemresultadochcklstlog( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         dynNaoConformidade_Codigo = new GXCombobox();
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
         if ( dynNaoConformidade_Codigo.ItemCount > 0 )
         {
            A426NaoConformidade_Codigo = (int)(NumberUtil.Val( dynNaoConformidade_Codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A426NaoConformidade_Codigo), 6, 0))), "."));
            n426NaoConformidade_Codigo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A426NaoConformidade_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A426NaoConformidade_Codigo), 6, 0)));
         }
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            wb_table1_2_2L207( true) ;
         }
         return  ;
      }

      protected void wb_table1_2_2L207e( bool wbgen )
      {
         if ( wbgen )
         {
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void wb_table1_2_2L207( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableBorder100x100", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_5_2L207( true) ;
         }
         return  ;
      }

      protected void wb_table2_5_2L207e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Control Group */
            GxWebStd.gx_group_start( context, grpGroupdata_Internalname, "Contagem Resultado Chck Lst Log", 1, 0, "px", 0, "px", "Group", " "+"style=\"-moz-border-radius: 3pt;;\""+" ", "HLP_ContagemResultadoChckLstLog.htm");
            wb_table3_28_2L207( true) ;
         }
         return  ;
      }

      protected void wb_table3_28_2L207e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</fieldset>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_2L207e( true) ;
         }
         else
         {
            wb_table1_2_2L207e( false) ;
         }
      }

      protected void wb_table3_28_2L207( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable1_Internalname, tblTable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_34_2L207( true) ;
         }
         return  ;
      }

      protected void wb_table4_34_2L207e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 87,'',false,'',0)\"";
            ClassString = "BtnEnter";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_enter_Internalname, "", "Confirmar", bttBtn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_enter_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContagemResultadoChckLstLog.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 88,'',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_cancel_Internalname, "", "Fechar", bttBtn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, bttBtn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContagemResultadoChckLstLog.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 89,'',false,'',0)\"";
            ClassString = "BtnDelete";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_delete_Internalname, "", "Eliminar", bttBtn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_delete_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContagemResultadoChckLstLog.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_28_2L207e( true) ;
         }
         else
         {
            wb_table3_28_2L207e( false) ;
         }
      }

      protected void wb_table4_34_2L207( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable2_Internalname, tblTable2_Internalname, "", "Container", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultadochcklstlog_codigo_Internalname, "Id", "", "", lblTextblockcontagemresultadochcklstlog_codigo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ContagemResultadoChckLstLog.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 39,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContagemResultadoChckLstLog_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A820ContagemResultadoChckLstLog_Codigo), 6, 0, ",", "")), ((edtContagemResultadoChckLstLog_Codigo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A820ContagemResultadoChckLstLog_Codigo), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A820ContagemResultadoChckLstLog_Codigo), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,39);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultadoChckLstLog_Codigo_Jsonclick, 0, "Attribute", "", "", "", 1, edtContagemResultadoChckLstLog_Codigo_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContagemResultadoChckLstLog.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultadochcklstlog_oscodigo_Internalname, "Codigo", "", "", lblTextblockcontagemresultadochcklstlog_oscodigo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ContagemResultadoChckLstLog.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 44,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContagemResultadoChckLstLog_OSCodigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1853ContagemResultadoChckLstLog_OSCodigo), 6, 0, ",", "")), ((edtContagemResultadoChckLstLog_OSCodigo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1853ContagemResultadoChckLstLog_OSCodigo), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A1853ContagemResultadoChckLstLog_OSCodigo), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,44);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultadoChckLstLog_OSCodigo_Jsonclick, 0, "Attribute", "", "", "", 1, edtContagemResultadoChckLstLog_OSCodigo_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContagemResultadoChckLstLog.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultadochcklstlog_datahora_Internalname, "Data", "", "", lblTextblockcontagemresultadochcklstlog_datahora_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ContagemResultadoChckLstLog.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 49,'',false,'',0)\"";
            context.WriteHtmlText( "<div id=\""+edtContagemResultadoChckLstLog_DataHora_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtContagemResultadoChckLstLog_DataHora_Internalname, context.localUtil.TToC( A814ContagemResultadoChckLstLog_DataHora, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( A814ContagemResultadoChckLstLog_DataHora, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,49);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultadoChckLstLog_DataHora_Jsonclick, 0, "Attribute", "", "", "", 1, edtContagemResultadoChckLstLog_DataHora_Enabled, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "DataHora", "right", false, "HLP_ContagemResultadoChckLstLog.htm");
            GxWebStd.gx_bitmap( context, edtContagemResultadoChckLstLog_DataHora_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(edtContagemResultadoChckLstLog_DataHora_Enabled==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_ContagemResultadoChckLstLog.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultadochcklstlog_chcklstcod_Internalname, "List", "", "", lblTextblockcontagemresultadochcklstlog_chcklstcod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ContagemResultadoChckLstLog.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 54,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContagemResultadoChckLstLog_ChckLstCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A811ContagemResultadoChckLstLog_ChckLstCod), 6, 0, ",", "")), ((edtContagemResultadoChckLstLog_ChckLstCod_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A811ContagemResultadoChckLstLog_ChckLstCod), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A811ContagemResultadoChckLstLog_ChckLstCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,54);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultadoChckLstLog_ChckLstCod_Jsonclick, 0, "Attribute", "", "", "", 1, edtContagemResultadoChckLstLog_ChckLstCod_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContagemResultadoChckLstLog.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultadochcklstlog_chcklstdes_Internalname, "List", "", "", lblTextblockcontagemresultadochcklstlog_chcklstdes_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ContagemResultadoChckLstLog.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Multiple line edit */
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtContagemResultadoChckLstLog_ChckLstDes_Internalname, A812ContagemResultadoChckLstLog_ChckLstDes, "", "", 0, 1, edtContagemResultadoChckLstLog_ChckLstDes_Enabled, 0, 80, "chr", 3, "row", StyleString, ClassString, "", "500", 1, "", "", -1, true, "DescricaoLonga", "HLP_ContagemResultadoChckLstLog.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocknaoconformidade_codigo_Internalname, "N�o Conformidade", "", "", lblTextblocknaoconformidade_codigo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ContagemResultadoChckLstLog.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 64,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynNaoConformidade_Codigo, dynNaoConformidade_Codigo_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A426NaoConformidade_Codigo), 6, 0)), 1, dynNaoConformidade_Codigo_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, dynNaoConformidade_Codigo.Enabled, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,64);\"", "", true, "HLP_ContagemResultadoChckLstLog.htm");
            dynNaoConformidade_Codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A426NaoConformidade_Codigo), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynNaoConformidade_Codigo_Internalname, "Values", (String)(dynNaoConformidade_Codigo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultadochcklstlog_usuariocod_Internalname, "Id", "", "", lblTextblockcontagemresultadochcklstlog_usuariocod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ContagemResultadoChckLstLog.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 69,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContagemResultadoChckLstLog_UsuarioCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A822ContagemResultadoChckLstLog_UsuarioCod), 6, 0, ",", "")), ((edtContagemResultadoChckLstLog_UsuarioCod_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A822ContagemResultadoChckLstLog_UsuarioCod), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A822ContagemResultadoChckLstLog_UsuarioCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,69);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultadoChckLstLog_UsuarioCod_Jsonclick, 0, "Attribute", "", "", "", 1, edtContagemResultadoChckLstLog_UsuarioCod_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContagemResultadoChckLstLog.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultadochcklstlog_pessoacod_Internalname, "Id", "", "", lblTextblockcontagemresultadochcklstlog_pessoacod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ContagemResultadoChckLstLog.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagemResultadoChckLstLog_PessoaCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A823ContagemResultadoChckLstLog_PessoaCod), 6, 0, ",", "")), ((edtContagemResultadoChckLstLog_PessoaCod_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A823ContagemResultadoChckLstLog_PessoaCod), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A823ContagemResultadoChckLstLog_PessoaCod), "ZZZZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultadoChckLstLog_PessoaCod_Jsonclick, 0, "Attribute", "", "", "", 1, edtContagemResultadoChckLstLog_PessoaCod_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContagemResultadoChckLstLog.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultadochcklstlog_usuarionome_Internalname, "Usu�rio", "", "", lblTextblockcontagemresultadochcklstlog_usuarionome_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ContagemResultadoChckLstLog.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContagemResultadoChckLstLog_UsuarioNome_Internalname, StringUtil.RTrim( A817ContagemResultadoChckLstLog_UsuarioNome), StringUtil.RTrim( context.localUtil.Format( A817ContagemResultadoChckLstLog_UsuarioNome, "@!")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultadoChckLstLog_UsuarioNome_Jsonclick, 0, "BootstrapAttribute100", "", "", "", 1, edtContagemResultadoChckLstLog_UsuarioNome_Enabled, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "Nome100", "left", true, "HLP_ContagemResultadoChckLstLog.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultadochcklstlog_etapa_Internalname, "Lst Log_Etapa", "", "", lblTextblockcontagemresultadochcklstlog_etapa_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ContagemResultadoChckLstLog.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 84,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContagemResultadoChckLstLog_Etapa_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A824ContagemResultadoChckLstLog_Etapa), 1, 0, ",", "")), ((edtContagemResultadoChckLstLog_Etapa_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A824ContagemResultadoChckLstLog_Etapa), "9")) : context.localUtil.Format( (decimal)(A824ContagemResultadoChckLstLog_Etapa), "9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,84);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultadoChckLstLog_Etapa_Jsonclick, 0, "Attribute", "", "", "", 1, edtContagemResultadoChckLstLog_Etapa_Enabled, 0, "text", "", 1, "chr", 1, "row", 1, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContagemResultadoChckLstLog.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_34_2L207e( true) ;
         }
         else
         {
            wb_table4_34_2L207e( false) ;
         }
      }

      protected void wb_table2_5_2L207( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabletoolbar_Internalname, tblTabletoolbar_Internalname, "", "ViewTable", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divSectiontoolbar_Internalname, 1, 0, "px", 0, "px", "ToolbarMain", "left", "top", "", "WHITE-SPACE: nowrap;", "div");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 9,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_first_Internalname, context.GetImagePath( "b9e06284-17ac-4c88-8937-5dbd84ad5d80", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_first_Visible, 1, "", "Primeiro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_first_Jsonclick, "'"+""+"'"+",false,"+"'"+"EFIRST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoChckLstLog.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 10,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_first_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_first_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_first_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EFIRST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoChckLstLog.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 11,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_previous_Internalname, context.GetImagePath( "7d212604-db7b-4785-9c0d-5faffe71aa33", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_previous_Visible, 1, "", "Anterior", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_previous_Jsonclick, "'"+""+"'"+",false,"+"'"+"EPREVIOUS."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoChckLstLog.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 12,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_previous_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_previous_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_previous_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EPREVIOUS."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoChckLstLog.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 13,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_next_Internalname, context.GetImagePath( "1ae947cf-1354-41a9-8d59-d91daebf554f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_next_Visible, 1, "", "Pr�ximo", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_next_Jsonclick, "'"+""+"'"+",false,"+"'"+"ENEXT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoChckLstLog.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 14,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_next_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_next_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_next_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ENEXT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoChckLstLog.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 15,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_last_Internalname, context.GetImagePath( "29211874-e613-48e5-9011-8017d984217e", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_last_Visible, 1, "", "�ltimo", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_last_Jsonclick, "'"+""+"'"+",false,"+"'"+"ELAST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoChckLstLog.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 16,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_last_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_last_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_last_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ELAST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoChckLstLog.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 17,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_select_Internalname, context.GetImagePath( "1ca03f75-9947-4d2c-90a4-e8ab9c5cedea", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_select_Visible, 1, "", "Selecionar", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_select_Jsonclick, "'"+""+"'"+",false,"+"'"+"ESELECT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoChckLstLog.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_select_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_select_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_select_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ESELECT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoChckLstLog.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 19,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_enter2_Internalname, context.GetImagePath( "2061cf2c-bd33-4433-a13e-34af954142e9", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_enter2_Visible, imgBtn_enter2_Enabled, "", "Confirmar", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_enter2_Jsonclick, "'"+""+"'"+",false,"+"'"+"EENTER."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoChckLstLog.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 20,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_enter2_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_enter2_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_enter2_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EENTER."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoChckLstLog.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_cancel2_Internalname, context.GetImagePath( "0e94ced8-bc34-47ff-9a53-bc683736a686", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_cancel2_Visible, 1, "", "Fechar", 0, 0, 0, "px", 0, "px", 0, 0, 1, imgBtn_cancel2_Jsonclick, "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoChckLstLog.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 22,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_cancel2_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_cancel2_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 1, imgBtn_cancel2_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoChckLstLog.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_delete2_Internalname, context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_delete2_Visible, imgBtn_delete2_Enabled, "", "Eliminar", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_delete2_Jsonclick, "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoChckLstLog.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 24,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_delete2_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_delete2_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_delete2_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoChckLstLog.htm");
            GxWebStd.gx_div_end( context, "left", "top");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_2L207e( true) ;
         }
         else
         {
            wb_table2_5_2L207e( false) ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               if ( ( ( context.localUtil.CToN( cgiGet( edtContagemResultadoChckLstLog_Codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContagemResultadoChckLstLog_Codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTAGEMRESULTADOCHCKLSTLOG_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtContagemResultadoChckLstLog_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A820ContagemResultadoChckLstLog_Codigo = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A820ContagemResultadoChckLstLog_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A820ContagemResultadoChckLstLog_Codigo), 6, 0)));
               }
               else
               {
                  A820ContagemResultadoChckLstLog_Codigo = (int)(context.localUtil.CToN( cgiGet( edtContagemResultadoChckLstLog_Codigo_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A820ContagemResultadoChckLstLog_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A820ContagemResultadoChckLstLog_Codigo), 6, 0)));
               }
               if ( ( ( context.localUtil.CToN( cgiGet( edtContagemResultadoChckLstLog_OSCodigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContagemResultadoChckLstLog_OSCodigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTAGEMRESULTADOCHCKLSTLOG_OSCODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtContagemResultadoChckLstLog_OSCodigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1853ContagemResultadoChckLstLog_OSCodigo = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1853ContagemResultadoChckLstLog_OSCodigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1853ContagemResultadoChckLstLog_OSCodigo), 6, 0)));
               }
               else
               {
                  A1853ContagemResultadoChckLstLog_OSCodigo = (int)(context.localUtil.CToN( cgiGet( edtContagemResultadoChckLstLog_OSCodigo_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1853ContagemResultadoChckLstLog_OSCodigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1853ContagemResultadoChckLstLog_OSCodigo), 6, 0)));
               }
               if ( context.localUtil.VCDateTime( cgiGet( edtContagemResultadoChckLstLog_DataHora_Internalname), 2, 0) == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"Data"}), 1, "CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA");
                  AnyError = 1;
                  GX_FocusControl = edtContagemResultadoChckLstLog_DataHora_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A814ContagemResultadoChckLstLog_DataHora = (DateTime)(DateTime.MinValue);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A814ContagemResultadoChckLstLog_DataHora", context.localUtil.TToC( A814ContagemResultadoChckLstLog_DataHora, 8, 5, 0, 3, "/", ":", " "));
               }
               else
               {
                  A814ContagemResultadoChckLstLog_DataHora = context.localUtil.CToT( cgiGet( edtContagemResultadoChckLstLog_DataHora_Internalname));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A814ContagemResultadoChckLstLog_DataHora", context.localUtil.TToC( A814ContagemResultadoChckLstLog_DataHora, 8, 5, 0, 3, "/", ":", " "));
               }
               if ( ( ( context.localUtil.CToN( cgiGet( edtContagemResultadoChckLstLog_ChckLstCod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContagemResultadoChckLstLog_ChckLstCod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTAGEMRESULTADOCHCKLSTLOG_CHCKLSTCOD");
                  AnyError = 1;
                  GX_FocusControl = edtContagemResultadoChckLstLog_ChckLstCod_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A811ContagemResultadoChckLstLog_ChckLstCod = 0;
                  n811ContagemResultadoChckLstLog_ChckLstCod = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A811ContagemResultadoChckLstLog_ChckLstCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A811ContagemResultadoChckLstLog_ChckLstCod), 6, 0)));
               }
               else
               {
                  A811ContagemResultadoChckLstLog_ChckLstCod = (int)(context.localUtil.CToN( cgiGet( edtContagemResultadoChckLstLog_ChckLstCod_Internalname), ",", "."));
                  n811ContagemResultadoChckLstLog_ChckLstCod = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A811ContagemResultadoChckLstLog_ChckLstCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A811ContagemResultadoChckLstLog_ChckLstCod), 6, 0)));
               }
               n811ContagemResultadoChckLstLog_ChckLstCod = ((0==A811ContagemResultadoChckLstLog_ChckLstCod) ? true : false);
               A812ContagemResultadoChckLstLog_ChckLstDes = cgiGet( edtContagemResultadoChckLstLog_ChckLstDes_Internalname);
               n812ContagemResultadoChckLstLog_ChckLstDes = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A812ContagemResultadoChckLstLog_ChckLstDes", A812ContagemResultadoChckLstLog_ChckLstDes);
               dynNaoConformidade_Codigo.CurrentValue = cgiGet( dynNaoConformidade_Codigo_Internalname);
               A426NaoConformidade_Codigo = (int)(NumberUtil.Val( cgiGet( dynNaoConformidade_Codigo_Internalname), "."));
               n426NaoConformidade_Codigo = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A426NaoConformidade_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A426NaoConformidade_Codigo), 6, 0)));
               n426NaoConformidade_Codigo = ((0==A426NaoConformidade_Codigo) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtContagemResultadoChckLstLog_UsuarioCod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContagemResultadoChckLstLog_UsuarioCod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTAGEMRESULTADOCHCKLSTLOG_USUARIOCOD");
                  AnyError = 1;
                  GX_FocusControl = edtContagemResultadoChckLstLog_UsuarioCod_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A822ContagemResultadoChckLstLog_UsuarioCod = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A822ContagemResultadoChckLstLog_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A822ContagemResultadoChckLstLog_UsuarioCod), 6, 0)));
               }
               else
               {
                  A822ContagemResultadoChckLstLog_UsuarioCod = (int)(context.localUtil.CToN( cgiGet( edtContagemResultadoChckLstLog_UsuarioCod_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A822ContagemResultadoChckLstLog_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A822ContagemResultadoChckLstLog_UsuarioCod), 6, 0)));
               }
               A823ContagemResultadoChckLstLog_PessoaCod = (int)(context.localUtil.CToN( cgiGet( edtContagemResultadoChckLstLog_PessoaCod_Internalname), ",", "."));
               n823ContagemResultadoChckLstLog_PessoaCod = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A823ContagemResultadoChckLstLog_PessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A823ContagemResultadoChckLstLog_PessoaCod), 6, 0)));
               A817ContagemResultadoChckLstLog_UsuarioNome = StringUtil.Upper( cgiGet( edtContagemResultadoChckLstLog_UsuarioNome_Internalname));
               n817ContagemResultadoChckLstLog_UsuarioNome = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A817ContagemResultadoChckLstLog_UsuarioNome", A817ContagemResultadoChckLstLog_UsuarioNome);
               if ( ( ( context.localUtil.CToN( cgiGet( edtContagemResultadoChckLstLog_Etapa_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContagemResultadoChckLstLog_Etapa_Internalname), ",", ".") > Convert.ToDecimal( 9 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTAGEMRESULTADOCHCKLSTLOG_ETAPA");
                  AnyError = 1;
                  GX_FocusControl = edtContagemResultadoChckLstLog_Etapa_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A824ContagemResultadoChckLstLog_Etapa = 0;
                  n824ContagemResultadoChckLstLog_Etapa = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A824ContagemResultadoChckLstLog_Etapa", StringUtil.Str( (decimal)(A824ContagemResultadoChckLstLog_Etapa), 1, 0));
               }
               else
               {
                  A824ContagemResultadoChckLstLog_Etapa = (short)(context.localUtil.CToN( cgiGet( edtContagemResultadoChckLstLog_Etapa_Internalname), ",", "."));
                  n824ContagemResultadoChckLstLog_Etapa = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A824ContagemResultadoChckLstLog_Etapa", StringUtil.Str( (decimal)(A824ContagemResultadoChckLstLog_Etapa), 1, 0));
               }
               n824ContagemResultadoChckLstLog_Etapa = ((0==A824ContagemResultadoChckLstLog_Etapa) ? true : false);
               /* Read saved values. */
               Z820ContagemResultadoChckLstLog_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z820ContagemResultadoChckLstLog_Codigo"), ",", "."));
               Z814ContagemResultadoChckLstLog_DataHora = context.localUtil.CToT( cgiGet( "Z814ContagemResultadoChckLstLog_DataHora"), 0);
               Z824ContagemResultadoChckLstLog_Etapa = (short)(context.localUtil.CToN( cgiGet( "Z824ContagemResultadoChckLstLog_Etapa"), ",", "."));
               n824ContagemResultadoChckLstLog_Etapa = ((0==A824ContagemResultadoChckLstLog_Etapa) ? true : false);
               Z426NaoConformidade_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z426NaoConformidade_Codigo"), ",", "."));
               n426NaoConformidade_Codigo = ((0==A426NaoConformidade_Codigo) ? true : false);
               Z1853ContagemResultadoChckLstLog_OSCodigo = (int)(context.localUtil.CToN( cgiGet( "Z1853ContagemResultadoChckLstLog_OSCodigo"), ",", "."));
               Z811ContagemResultadoChckLstLog_ChckLstCod = (int)(context.localUtil.CToN( cgiGet( "Z811ContagemResultadoChckLstLog_ChckLstCod"), ",", "."));
               n811ContagemResultadoChckLstLog_ChckLstCod = ((0==A811ContagemResultadoChckLstLog_ChckLstCod) ? true : false);
               Z822ContagemResultadoChckLstLog_UsuarioCod = (int)(context.localUtil.CToN( cgiGet( "Z822ContagemResultadoChckLstLog_UsuarioCod"), ",", "."));
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
               Gx_mode = cgiGet( "Mode");
               Gx_mode = cgiGet( "vMODE");
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  A820ContagemResultadoChckLstLog_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A820ContagemResultadoChckLstLog_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A820ContagemResultadoChckLstLog_Codigo), 6, 0)));
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  disable_std_buttons_dsp( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  Gx_mode = "INS";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  standaloneModal( ) ;
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_enter( ) ;
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                        else if ( StringUtil.StrCmp(sEvt, "FIRST") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_first( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "PREVIOUS") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_previous( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "NEXT") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_next( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LAST") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_last( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "SELECT") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_select( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DELETE") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_delete( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                        {
                           context.wbHandled = 1;
                           AfterKeyLoadScreen( ) ;
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll2L207( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            imgBtn_delete2_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Enabled), 5, 0)));
         }
      }

      protected void disable_std_buttons_dsp( )
      {
         imgBtn_delete2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Visible), 5, 0)));
         imgBtn_delete2_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_separator_Visible), 5, 0)));
         bttBtn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Visible), 5, 0)));
         imgBtn_first_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_first_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_first_Visible), 5, 0)));
         imgBtn_first_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_first_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_first_separator_Visible), 5, 0)));
         imgBtn_previous_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_previous_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_previous_Visible), 5, 0)));
         imgBtn_previous_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_previous_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_previous_separator_Visible), 5, 0)));
         imgBtn_next_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_next_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_next_Visible), 5, 0)));
         imgBtn_next_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_next_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_next_separator_Visible), 5, 0)));
         imgBtn_last_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_last_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_last_Visible), 5, 0)));
         imgBtn_last_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_last_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_last_separator_Visible), 5, 0)));
         imgBtn_select_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_select_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_select_Visible), 5, 0)));
         imgBtn_select_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_select_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_select_separator_Visible), 5, 0)));
         imgBtn_delete2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Visible), 5, 0)));
         imgBtn_delete2_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_separator_Visible), 5, 0)));
         bttBtn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Visible), 5, 0)));
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            imgBtn_enter2_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_Visible), 5, 0)));
            imgBtn_enter2_separator_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_separator_Visible), 5, 0)));
            bttBtn_enter_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_enter_Visible), 5, 0)));
         }
         DisableAttributes2L207( ) ;
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void ResetCaption2L0( )
      {
      }

      protected void ZM2L207( short GX_JID )
      {
         if ( ( GX_JID == 3 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z814ContagemResultadoChckLstLog_DataHora = T002L3_A814ContagemResultadoChckLstLog_DataHora[0];
               Z824ContagemResultadoChckLstLog_Etapa = T002L3_A824ContagemResultadoChckLstLog_Etapa[0];
               Z426NaoConformidade_Codigo = T002L3_A426NaoConformidade_Codigo[0];
               Z1853ContagemResultadoChckLstLog_OSCodigo = T002L3_A1853ContagemResultadoChckLstLog_OSCodigo[0];
               Z811ContagemResultadoChckLstLog_ChckLstCod = T002L3_A811ContagemResultadoChckLstLog_ChckLstCod[0];
               Z822ContagemResultadoChckLstLog_UsuarioCod = T002L3_A822ContagemResultadoChckLstLog_UsuarioCod[0];
            }
            else
            {
               Z814ContagemResultadoChckLstLog_DataHora = A814ContagemResultadoChckLstLog_DataHora;
               Z824ContagemResultadoChckLstLog_Etapa = A824ContagemResultadoChckLstLog_Etapa;
               Z426NaoConformidade_Codigo = A426NaoConformidade_Codigo;
               Z1853ContagemResultadoChckLstLog_OSCodigo = A1853ContagemResultadoChckLstLog_OSCodigo;
               Z811ContagemResultadoChckLstLog_ChckLstCod = A811ContagemResultadoChckLstLog_ChckLstCod;
               Z822ContagemResultadoChckLstLog_UsuarioCod = A822ContagemResultadoChckLstLog_UsuarioCod;
            }
         }
         if ( GX_JID == -3 )
         {
            Z820ContagemResultadoChckLstLog_Codigo = A820ContagemResultadoChckLstLog_Codigo;
            Z814ContagemResultadoChckLstLog_DataHora = A814ContagemResultadoChckLstLog_DataHora;
            Z824ContagemResultadoChckLstLog_Etapa = A824ContagemResultadoChckLstLog_Etapa;
            Z426NaoConformidade_Codigo = A426NaoConformidade_Codigo;
            Z1853ContagemResultadoChckLstLog_OSCodigo = A1853ContagemResultadoChckLstLog_OSCodigo;
            Z811ContagemResultadoChckLstLog_ChckLstCod = A811ContagemResultadoChckLstLog_ChckLstCod;
            Z822ContagemResultadoChckLstLog_UsuarioCod = A822ContagemResultadoChckLstLog_UsuarioCod;
            Z812ContagemResultadoChckLstLog_ChckLstDes = A812ContagemResultadoChckLstLog_ChckLstDes;
            Z823ContagemResultadoChckLstLog_PessoaCod = A823ContagemResultadoChckLstLog_PessoaCod;
            Z817ContagemResultadoChckLstLog_UsuarioNome = A817ContagemResultadoChckLstLog_UsuarioNome;
         }
      }

      protected void standaloneNotModal( )
      {
         GXANAOCONFORMIDADE_CODIGO_html2L207( ) ;
      }

      protected void standaloneModal( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            imgBtn_delete2_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Enabled), 5, 0)));
         }
         else
         {
            imgBtn_delete2_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Enabled), 5, 0)));
         }
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            imgBtn_enter2_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_Enabled), 5, 0)));
         }
         else
         {
            imgBtn_enter2_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_Enabled), 5, 0)));
         }
      }

      protected void Load2L207( )
      {
         /* Using cursor T002L9 */
         pr_default.execute(7, new Object[] {A820ContagemResultadoChckLstLog_Codigo});
         if ( (pr_default.getStatus(7) != 101) )
         {
            RcdFound207 = 1;
            A814ContagemResultadoChckLstLog_DataHora = T002L9_A814ContagemResultadoChckLstLog_DataHora[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A814ContagemResultadoChckLstLog_DataHora", context.localUtil.TToC( A814ContagemResultadoChckLstLog_DataHora, 8, 5, 0, 3, "/", ":", " "));
            A812ContagemResultadoChckLstLog_ChckLstDes = T002L9_A812ContagemResultadoChckLstLog_ChckLstDes[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A812ContagemResultadoChckLstLog_ChckLstDes", A812ContagemResultadoChckLstLog_ChckLstDes);
            n812ContagemResultadoChckLstLog_ChckLstDes = T002L9_n812ContagemResultadoChckLstLog_ChckLstDes[0];
            A817ContagemResultadoChckLstLog_UsuarioNome = T002L9_A817ContagemResultadoChckLstLog_UsuarioNome[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A817ContagemResultadoChckLstLog_UsuarioNome", A817ContagemResultadoChckLstLog_UsuarioNome);
            n817ContagemResultadoChckLstLog_UsuarioNome = T002L9_n817ContagemResultadoChckLstLog_UsuarioNome[0];
            A824ContagemResultadoChckLstLog_Etapa = T002L9_A824ContagemResultadoChckLstLog_Etapa[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A824ContagemResultadoChckLstLog_Etapa", StringUtil.Str( (decimal)(A824ContagemResultadoChckLstLog_Etapa), 1, 0));
            n824ContagemResultadoChckLstLog_Etapa = T002L9_n824ContagemResultadoChckLstLog_Etapa[0];
            A426NaoConformidade_Codigo = T002L9_A426NaoConformidade_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A426NaoConformidade_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A426NaoConformidade_Codigo), 6, 0)));
            n426NaoConformidade_Codigo = T002L9_n426NaoConformidade_Codigo[0];
            A1853ContagemResultadoChckLstLog_OSCodigo = T002L9_A1853ContagemResultadoChckLstLog_OSCodigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1853ContagemResultadoChckLstLog_OSCodigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1853ContagemResultadoChckLstLog_OSCodigo), 6, 0)));
            A811ContagemResultadoChckLstLog_ChckLstCod = T002L9_A811ContagemResultadoChckLstLog_ChckLstCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A811ContagemResultadoChckLstLog_ChckLstCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A811ContagemResultadoChckLstLog_ChckLstCod), 6, 0)));
            n811ContagemResultadoChckLstLog_ChckLstCod = T002L9_n811ContagemResultadoChckLstLog_ChckLstCod[0];
            A822ContagemResultadoChckLstLog_UsuarioCod = T002L9_A822ContagemResultadoChckLstLog_UsuarioCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A822ContagemResultadoChckLstLog_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A822ContagemResultadoChckLstLog_UsuarioCod), 6, 0)));
            A823ContagemResultadoChckLstLog_PessoaCod = T002L9_A823ContagemResultadoChckLstLog_PessoaCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A823ContagemResultadoChckLstLog_PessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A823ContagemResultadoChckLstLog_PessoaCod), 6, 0)));
            n823ContagemResultadoChckLstLog_PessoaCod = T002L9_n823ContagemResultadoChckLstLog_PessoaCod[0];
            ZM2L207( -3) ;
         }
         pr_default.close(7);
         OnLoadActions2L207( ) ;
      }

      protected void OnLoadActions2L207( )
      {
      }

      protected void CheckExtendedTable2L207( )
      {
         Gx_BScreen = 1;
         standaloneModal( ) ;
         /* Using cursor T002L5 */
         pr_default.execute(3, new Object[] {A1853ContagemResultadoChckLstLog_OSCodigo});
         if ( (pr_default.getStatus(3) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contagem Resultado Chck Lst Log_Contagem Resutlado'.", "ForeignKeyNotFound", 1, "CONTAGEMRESULTADOCHCKLSTLOG_OSCODIGO");
            AnyError = 1;
            GX_FocusControl = edtContagemResultadoChckLstLog_OSCodigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         pr_default.close(3);
         if ( ! ( (DateTime.MinValue==A814ContagemResultadoChckLstLog_DataHora) || ( A814ContagemResultadoChckLstLog_DataHora >= context.localUtil.YMDHMSToT( 1753, 1, 1, 0, 0, 0) ) ) )
         {
            GX_msglist.addItem("Campo Data fora do intervalo", "OutOfRange", 1, "CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA");
            AnyError = 1;
            GX_FocusControl = edtContagemResultadoChckLstLog_DataHora_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         /* Using cursor T002L6 */
         pr_default.execute(4, new Object[] {n811ContagemResultadoChckLstLog_ChckLstCod, A811ContagemResultadoChckLstLog_ChckLstCod});
         if ( (pr_default.getStatus(4) == 101) )
         {
            if ( ! ( (0==A811ContagemResultadoChckLstLog_ChckLstCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Contagem Resultado Chck LstLog_Check List'.", "ForeignKeyNotFound", 1, "CONTAGEMRESULTADOCHCKLSTLOG_CHCKLSTCOD");
               AnyError = 1;
               GX_FocusControl = edtContagemResultadoChckLstLog_ChckLstCod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         A812ContagemResultadoChckLstLog_ChckLstDes = T002L6_A812ContagemResultadoChckLstLog_ChckLstDes[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A812ContagemResultadoChckLstLog_ChckLstDes", A812ContagemResultadoChckLstLog_ChckLstDes);
         n812ContagemResultadoChckLstLog_ChckLstDes = T002L6_n812ContagemResultadoChckLstLog_ChckLstDes[0];
         pr_default.close(4);
         /* Using cursor T002L4 */
         pr_default.execute(2, new Object[] {n426NaoConformidade_Codigo, A426NaoConformidade_Codigo});
         if ( (pr_default.getStatus(2) == 101) )
         {
            if ( ! ( (0==A426NaoConformidade_Codigo) ) )
            {
               GX_msglist.addItem("N�o existe 'N�o Conformidade'.", "ForeignKeyNotFound", 1, "NAOCONFORMIDADE_CODIGO");
               AnyError = 1;
               GX_FocusControl = dynNaoConformidade_Codigo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         pr_default.close(2);
         /* Using cursor T002L7 */
         pr_default.execute(5, new Object[] {A822ContagemResultadoChckLstLog_UsuarioCod});
         if ( (pr_default.getStatus(5) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contagem Resultado Chck Lst Log_Usuario'.", "ForeignKeyNotFound", 1, "CONTAGEMRESULTADOCHCKLSTLOG_USUARIOCOD");
            AnyError = 1;
            GX_FocusControl = edtContagemResultadoChckLstLog_UsuarioCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A823ContagemResultadoChckLstLog_PessoaCod = T002L7_A823ContagemResultadoChckLstLog_PessoaCod[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A823ContagemResultadoChckLstLog_PessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A823ContagemResultadoChckLstLog_PessoaCod), 6, 0)));
         n823ContagemResultadoChckLstLog_PessoaCod = T002L7_n823ContagemResultadoChckLstLog_PessoaCod[0];
         pr_default.close(5);
         /* Using cursor T002L8 */
         pr_default.execute(6, new Object[] {n823ContagemResultadoChckLstLog_PessoaCod, A823ContagemResultadoChckLstLog_PessoaCod});
         if ( (pr_default.getStatus(6) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Pessoa'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A817ContagemResultadoChckLstLog_UsuarioNome = T002L8_A817ContagemResultadoChckLstLog_UsuarioNome[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A817ContagemResultadoChckLstLog_UsuarioNome", A817ContagemResultadoChckLstLog_UsuarioNome);
         n817ContagemResultadoChckLstLog_UsuarioNome = T002L8_n817ContagemResultadoChckLstLog_UsuarioNome[0];
         pr_default.close(6);
      }

      protected void CloseExtendedTableCursors2L207( )
      {
         pr_default.close(3);
         pr_default.close(4);
         pr_default.close(2);
         pr_default.close(5);
         pr_default.close(6);
      }

      protected void enableDisable( )
      {
      }

      protected void gxLoad_5( int A1853ContagemResultadoChckLstLog_OSCodigo )
      {
         /* Using cursor T002L10 */
         pr_default.execute(8, new Object[] {A1853ContagemResultadoChckLstLog_OSCodigo});
         if ( (pr_default.getStatus(8) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contagem Resultado Chck Lst Log_Contagem Resutlado'.", "ForeignKeyNotFound", 1, "CONTAGEMRESULTADOCHCKLSTLOG_OSCODIGO");
            AnyError = 1;
            GX_FocusControl = edtContagemResultadoChckLstLog_OSCodigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(8) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(8);
      }

      protected void gxLoad_6( int A811ContagemResultadoChckLstLog_ChckLstCod )
      {
         /* Using cursor T002L11 */
         pr_default.execute(9, new Object[] {n811ContagemResultadoChckLstLog_ChckLstCod, A811ContagemResultadoChckLstLog_ChckLstCod});
         if ( (pr_default.getStatus(9) == 101) )
         {
            if ( ! ( (0==A811ContagemResultadoChckLstLog_ChckLstCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Contagem Resultado Chck LstLog_Check List'.", "ForeignKeyNotFound", 1, "CONTAGEMRESULTADOCHCKLSTLOG_CHCKLSTCOD");
               AnyError = 1;
               GX_FocusControl = edtContagemResultadoChckLstLog_ChckLstCod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         A812ContagemResultadoChckLstLog_ChckLstDes = T002L11_A812ContagemResultadoChckLstLog_ChckLstDes[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A812ContagemResultadoChckLstLog_ChckLstDes", A812ContagemResultadoChckLstLog_ChckLstDes);
         n812ContagemResultadoChckLstLog_ChckLstDes = T002L11_n812ContagemResultadoChckLstLog_ChckLstDes[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( A812ContagemResultadoChckLstLog_ChckLstDes)+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(9) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(9);
      }

      protected void gxLoad_4( int A426NaoConformidade_Codigo )
      {
         /* Using cursor T002L12 */
         pr_default.execute(10, new Object[] {n426NaoConformidade_Codigo, A426NaoConformidade_Codigo});
         if ( (pr_default.getStatus(10) == 101) )
         {
            if ( ! ( (0==A426NaoConformidade_Codigo) ) )
            {
               GX_msglist.addItem("N�o existe 'N�o Conformidade'.", "ForeignKeyNotFound", 1, "NAOCONFORMIDADE_CODIGO");
               AnyError = 1;
               GX_FocusControl = dynNaoConformidade_Codigo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(10) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(10);
      }

      protected void gxLoad_7( int A822ContagemResultadoChckLstLog_UsuarioCod )
      {
         /* Using cursor T002L13 */
         pr_default.execute(11, new Object[] {A822ContagemResultadoChckLstLog_UsuarioCod});
         if ( (pr_default.getStatus(11) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contagem Resultado Chck Lst Log_Usuario'.", "ForeignKeyNotFound", 1, "CONTAGEMRESULTADOCHCKLSTLOG_USUARIOCOD");
            AnyError = 1;
            GX_FocusControl = edtContagemResultadoChckLstLog_UsuarioCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A823ContagemResultadoChckLstLog_PessoaCod = T002L13_A823ContagemResultadoChckLstLog_PessoaCod[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A823ContagemResultadoChckLstLog_PessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A823ContagemResultadoChckLstLog_PessoaCod), 6, 0)));
         n823ContagemResultadoChckLstLog_PessoaCod = T002L13_n823ContagemResultadoChckLstLog_PessoaCod[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A823ContagemResultadoChckLstLog_PessoaCod), 6, 0, ".", "")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(11) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(11);
      }

      protected void gxLoad_8( int A823ContagemResultadoChckLstLog_PessoaCod )
      {
         /* Using cursor T002L14 */
         pr_default.execute(12, new Object[] {n823ContagemResultadoChckLstLog_PessoaCod, A823ContagemResultadoChckLstLog_PessoaCod});
         if ( (pr_default.getStatus(12) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Pessoa'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A817ContagemResultadoChckLstLog_UsuarioNome = T002L14_A817ContagemResultadoChckLstLog_UsuarioNome[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A817ContagemResultadoChckLstLog_UsuarioNome", A817ContagemResultadoChckLstLog_UsuarioNome);
         n817ContagemResultadoChckLstLog_UsuarioNome = T002L14_n817ContagemResultadoChckLstLog_UsuarioNome[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A817ContagemResultadoChckLstLog_UsuarioNome))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(12) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(12);
      }

      protected void GetKey2L207( )
      {
         /* Using cursor T002L15 */
         pr_default.execute(13, new Object[] {A820ContagemResultadoChckLstLog_Codigo});
         if ( (pr_default.getStatus(13) != 101) )
         {
            RcdFound207 = 1;
         }
         else
         {
            RcdFound207 = 0;
         }
         pr_default.close(13);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T002L3 */
         pr_default.execute(1, new Object[] {A820ContagemResultadoChckLstLog_Codigo});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM2L207( 3) ;
            RcdFound207 = 1;
            A820ContagemResultadoChckLstLog_Codigo = T002L3_A820ContagemResultadoChckLstLog_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A820ContagemResultadoChckLstLog_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A820ContagemResultadoChckLstLog_Codigo), 6, 0)));
            A814ContagemResultadoChckLstLog_DataHora = T002L3_A814ContagemResultadoChckLstLog_DataHora[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A814ContagemResultadoChckLstLog_DataHora", context.localUtil.TToC( A814ContagemResultadoChckLstLog_DataHora, 8, 5, 0, 3, "/", ":", " "));
            A824ContagemResultadoChckLstLog_Etapa = T002L3_A824ContagemResultadoChckLstLog_Etapa[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A824ContagemResultadoChckLstLog_Etapa", StringUtil.Str( (decimal)(A824ContagemResultadoChckLstLog_Etapa), 1, 0));
            n824ContagemResultadoChckLstLog_Etapa = T002L3_n824ContagemResultadoChckLstLog_Etapa[0];
            A426NaoConformidade_Codigo = T002L3_A426NaoConformidade_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A426NaoConformidade_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A426NaoConformidade_Codigo), 6, 0)));
            n426NaoConformidade_Codigo = T002L3_n426NaoConformidade_Codigo[0];
            A1853ContagemResultadoChckLstLog_OSCodigo = T002L3_A1853ContagemResultadoChckLstLog_OSCodigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1853ContagemResultadoChckLstLog_OSCodigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1853ContagemResultadoChckLstLog_OSCodigo), 6, 0)));
            A811ContagemResultadoChckLstLog_ChckLstCod = T002L3_A811ContagemResultadoChckLstLog_ChckLstCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A811ContagemResultadoChckLstLog_ChckLstCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A811ContagemResultadoChckLstLog_ChckLstCod), 6, 0)));
            n811ContagemResultadoChckLstLog_ChckLstCod = T002L3_n811ContagemResultadoChckLstLog_ChckLstCod[0];
            A822ContagemResultadoChckLstLog_UsuarioCod = T002L3_A822ContagemResultadoChckLstLog_UsuarioCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A822ContagemResultadoChckLstLog_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A822ContagemResultadoChckLstLog_UsuarioCod), 6, 0)));
            Z820ContagemResultadoChckLstLog_Codigo = A820ContagemResultadoChckLstLog_Codigo;
            sMode207 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            standaloneModal( ) ;
            Load2L207( ) ;
            if ( AnyError == 1 )
            {
               RcdFound207 = 0;
               InitializeNonKey2L207( ) ;
            }
            Gx_mode = sMode207;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            RcdFound207 = 0;
            InitializeNonKey2L207( ) ;
            sMode207 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            standaloneModal( ) ;
            Gx_mode = sMode207;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey2L207( ) ;
         if ( RcdFound207 == 0 )
         {
            Gx_mode = "INS";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound207 = 0;
         /* Using cursor T002L16 */
         pr_default.execute(14, new Object[] {A820ContagemResultadoChckLstLog_Codigo});
         if ( (pr_default.getStatus(14) != 101) )
         {
            while ( (pr_default.getStatus(14) != 101) && ( ( T002L16_A820ContagemResultadoChckLstLog_Codigo[0] < A820ContagemResultadoChckLstLog_Codigo ) ) )
            {
               pr_default.readNext(14);
            }
            if ( (pr_default.getStatus(14) != 101) && ( ( T002L16_A820ContagemResultadoChckLstLog_Codigo[0] > A820ContagemResultadoChckLstLog_Codigo ) ) )
            {
               A820ContagemResultadoChckLstLog_Codigo = T002L16_A820ContagemResultadoChckLstLog_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A820ContagemResultadoChckLstLog_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A820ContagemResultadoChckLstLog_Codigo), 6, 0)));
               RcdFound207 = 1;
            }
         }
         pr_default.close(14);
      }

      protected void move_previous( )
      {
         RcdFound207 = 0;
         /* Using cursor T002L17 */
         pr_default.execute(15, new Object[] {A820ContagemResultadoChckLstLog_Codigo});
         if ( (pr_default.getStatus(15) != 101) )
         {
            while ( (pr_default.getStatus(15) != 101) && ( ( T002L17_A820ContagemResultadoChckLstLog_Codigo[0] > A820ContagemResultadoChckLstLog_Codigo ) ) )
            {
               pr_default.readNext(15);
            }
            if ( (pr_default.getStatus(15) != 101) && ( ( T002L17_A820ContagemResultadoChckLstLog_Codigo[0] < A820ContagemResultadoChckLstLog_Codigo ) ) )
            {
               A820ContagemResultadoChckLstLog_Codigo = T002L17_A820ContagemResultadoChckLstLog_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A820ContagemResultadoChckLstLog_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A820ContagemResultadoChckLstLog_Codigo), 6, 0)));
               RcdFound207 = 1;
            }
         }
         pr_default.close(15);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey2L207( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = edtContagemResultadoChckLstLog_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert2L207( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound207 == 1 )
            {
               if ( A820ContagemResultadoChckLstLog_Codigo != Z820ContagemResultadoChckLstLog_Codigo )
               {
                  A820ContagemResultadoChckLstLog_Codigo = Z820ContagemResultadoChckLstLog_Codigo;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A820ContagemResultadoChckLstLog_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A820ContagemResultadoChckLstLog_Codigo), 6, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "CONTAGEMRESULTADOCHCKLSTLOG_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtContagemResultadoChckLstLog_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = edtContagemResultadoChckLstLog_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  Gx_mode = "UPD";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  /* Update record */
                  Update2L207( ) ;
                  GX_FocusControl = edtContagemResultadoChckLstLog_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( A820ContagemResultadoChckLstLog_Codigo != Z820ContagemResultadoChckLstLog_Codigo )
               {
                  Gx_mode = "INS";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  /* Insert record */
                  GX_FocusControl = edtContagemResultadoChckLstLog_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert2L207( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "CONTAGEMRESULTADOCHCKLSTLOG_CODIGO");
                     AnyError = 1;
                     GX_FocusControl = edtContagemResultadoChckLstLog_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     Gx_mode = "INS";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     /* Insert record */
                     GX_FocusControl = edtContagemResultadoChckLstLog_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert2L207( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
      }

      protected void btn_delete( )
      {
         if ( A820ContagemResultadoChckLstLog_Codigo != Z820ContagemResultadoChckLstLog_Codigo )
         {
            A820ContagemResultadoChckLstLog_Codigo = Z820ContagemResultadoChckLstLog_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A820ContagemResultadoChckLstLog_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A820ContagemResultadoChckLstLog_Codigo), 6, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "CONTAGEMRESULTADOCHCKLSTLOG_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtContagemResultadoChckLstLog_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = edtContagemResultadoChckLstLog_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            getByPrimaryKey( ) ;
         }
         CloseOpenCursors();
      }

      protected void btn_get( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         getEqualNoModal( ) ;
         if ( RcdFound207 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "CONTAGEMRESULTADOCHCKLSTLOG_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtContagemResultadoChckLstLog_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         GX_FocusControl = edtContagemResultadoChckLstLog_OSCodigo_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_first( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         ScanStart2L207( ) ;
         if ( RcdFound207 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtContagemResultadoChckLstLog_OSCodigo_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         ScanEnd2L207( ) ;
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_previous( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         move_previous( ) ;
         if ( RcdFound207 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtContagemResultadoChckLstLog_OSCodigo_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_next( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         move_next( ) ;
         if ( RcdFound207 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtContagemResultadoChckLstLog_OSCodigo_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_last( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         ScanStart2L207( ) ;
         if ( RcdFound207 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            while ( RcdFound207 != 0 )
            {
               ScanNext2L207( ) ;
            }
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtContagemResultadoChckLstLog_OSCodigo_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         ScanEnd2L207( ) ;
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_select( )
      {
         getEqualNoModal( ) ;
      }

      protected void CheckOptimisticConcurrency2L207( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T002L2 */
            pr_default.execute(0, new Object[] {A820ContagemResultadoChckLstLog_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ContagemResultadoChckLstLog"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            Gx_longc = false;
            if ( (pr_default.getStatus(0) == 101) || ( Z814ContagemResultadoChckLstLog_DataHora != T002L2_A814ContagemResultadoChckLstLog_DataHora[0] ) || ( Z824ContagemResultadoChckLstLog_Etapa != T002L2_A824ContagemResultadoChckLstLog_Etapa[0] ) || ( Z426NaoConformidade_Codigo != T002L2_A426NaoConformidade_Codigo[0] ) || ( Z1853ContagemResultadoChckLstLog_OSCodigo != T002L2_A1853ContagemResultadoChckLstLog_OSCodigo[0] ) || ( Z811ContagemResultadoChckLstLog_ChckLstCod != T002L2_A811ContagemResultadoChckLstLog_ChckLstCod[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z822ContagemResultadoChckLstLog_UsuarioCod != T002L2_A822ContagemResultadoChckLstLog_UsuarioCod[0] ) )
            {
               if ( Z814ContagemResultadoChckLstLog_DataHora != T002L2_A814ContagemResultadoChckLstLog_DataHora[0] )
               {
                  GXUtil.WriteLog("contagemresultadochcklstlog:[seudo value changed for attri]"+"ContagemResultadoChckLstLog_DataHora");
                  GXUtil.WriteLogRaw("Old: ",Z814ContagemResultadoChckLstLog_DataHora);
                  GXUtil.WriteLogRaw("Current: ",T002L2_A814ContagemResultadoChckLstLog_DataHora[0]);
               }
               if ( Z824ContagemResultadoChckLstLog_Etapa != T002L2_A824ContagemResultadoChckLstLog_Etapa[0] )
               {
                  GXUtil.WriteLog("contagemresultadochcklstlog:[seudo value changed for attri]"+"ContagemResultadoChckLstLog_Etapa");
                  GXUtil.WriteLogRaw("Old: ",Z824ContagemResultadoChckLstLog_Etapa);
                  GXUtil.WriteLogRaw("Current: ",T002L2_A824ContagemResultadoChckLstLog_Etapa[0]);
               }
               if ( Z426NaoConformidade_Codigo != T002L2_A426NaoConformidade_Codigo[0] )
               {
                  GXUtil.WriteLog("contagemresultadochcklstlog:[seudo value changed for attri]"+"NaoConformidade_Codigo");
                  GXUtil.WriteLogRaw("Old: ",Z426NaoConformidade_Codigo);
                  GXUtil.WriteLogRaw("Current: ",T002L2_A426NaoConformidade_Codigo[0]);
               }
               if ( Z1853ContagemResultadoChckLstLog_OSCodigo != T002L2_A1853ContagemResultadoChckLstLog_OSCodigo[0] )
               {
                  GXUtil.WriteLog("contagemresultadochcklstlog:[seudo value changed for attri]"+"ContagemResultadoChckLstLog_OSCodigo");
                  GXUtil.WriteLogRaw("Old: ",Z1853ContagemResultadoChckLstLog_OSCodigo);
                  GXUtil.WriteLogRaw("Current: ",T002L2_A1853ContagemResultadoChckLstLog_OSCodigo[0]);
               }
               if ( Z811ContagemResultadoChckLstLog_ChckLstCod != T002L2_A811ContagemResultadoChckLstLog_ChckLstCod[0] )
               {
                  GXUtil.WriteLog("contagemresultadochcklstlog:[seudo value changed for attri]"+"ContagemResultadoChckLstLog_ChckLstCod");
                  GXUtil.WriteLogRaw("Old: ",Z811ContagemResultadoChckLstLog_ChckLstCod);
                  GXUtil.WriteLogRaw("Current: ",T002L2_A811ContagemResultadoChckLstLog_ChckLstCod[0]);
               }
               if ( Z822ContagemResultadoChckLstLog_UsuarioCod != T002L2_A822ContagemResultadoChckLstLog_UsuarioCod[0] )
               {
                  GXUtil.WriteLog("contagemresultadochcklstlog:[seudo value changed for attri]"+"ContagemResultadoChckLstLog_UsuarioCod");
                  GXUtil.WriteLogRaw("Old: ",Z822ContagemResultadoChckLstLog_UsuarioCod);
                  GXUtil.WriteLogRaw("Current: ",T002L2_A822ContagemResultadoChckLstLog_UsuarioCod[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"ContagemResultadoChckLstLog"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert2L207( )
      {
         BeforeValidate2L207( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable2L207( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM2L207( 0) ;
            CheckOptimisticConcurrency2L207( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm2L207( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert2L207( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T002L18 */
                     pr_default.execute(16, new Object[] {A814ContagemResultadoChckLstLog_DataHora, n824ContagemResultadoChckLstLog_Etapa, A824ContagemResultadoChckLstLog_Etapa, n426NaoConformidade_Codigo, A426NaoConformidade_Codigo, A1853ContagemResultadoChckLstLog_OSCodigo, n811ContagemResultadoChckLstLog_ChckLstCod, A811ContagemResultadoChckLstLog_ChckLstCod, A822ContagemResultadoChckLstLog_UsuarioCod});
                     A820ContagemResultadoChckLstLog_Codigo = T002L18_A820ContagemResultadoChckLstLog_Codigo[0];
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A820ContagemResultadoChckLstLog_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A820ContagemResultadoChckLstLog_Codigo), 6, 0)));
                     pr_default.close(16);
                     dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoChckLstLog") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                           ResetCaption2L0( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load2L207( ) ;
            }
            EndLevel2L207( ) ;
         }
         CloseExtendedTableCursors2L207( ) ;
      }

      protected void Update2L207( )
      {
         BeforeValidate2L207( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable2L207( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency2L207( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm2L207( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate2L207( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T002L19 */
                     pr_default.execute(17, new Object[] {A814ContagemResultadoChckLstLog_DataHora, n824ContagemResultadoChckLstLog_Etapa, A824ContagemResultadoChckLstLog_Etapa, n426NaoConformidade_Codigo, A426NaoConformidade_Codigo, A1853ContagemResultadoChckLstLog_OSCodigo, n811ContagemResultadoChckLstLog_ChckLstCod, A811ContagemResultadoChckLstLog_ChckLstCod, A822ContagemResultadoChckLstLog_UsuarioCod, A820ContagemResultadoChckLstLog_Codigo});
                     pr_default.close(17);
                     dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoChckLstLog") ;
                     if ( (pr_default.getStatus(17) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ContagemResultadoChckLstLog"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate2L207( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           getByPrimaryKey( ) ;
                           GX_msglist.addItem(context.GetMessage( "GXM_sucupdated", ""), 0, "", true);
                           ResetCaption2L0( ) ;
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel2L207( ) ;
         }
         CloseExtendedTableCursors2L207( ) ;
      }

      protected void DeferredUpdate2L207( )
      {
      }

      protected void delete( )
      {
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         BeforeValidate2L207( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency2L207( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls2L207( ) ;
            AfterConfirm2L207( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete2L207( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T002L20 */
                  pr_default.execute(18, new Object[] {A820ContagemResultadoChckLstLog_Codigo});
                  pr_default.close(18);
                  dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoChckLstLog") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        move_next( ) ;
                        if ( RcdFound207 == 0 )
                        {
                           InitAll2L207( ) ;
                           Gx_mode = "INS";
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                        }
                        else
                        {
                           getByPrimaryKey( ) ;
                           Gx_mode = "UPD";
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                        }
                        GX_msglist.addItem(context.GetMessage( "GXM_sucdeleted", ""), 0, "", true);
                        ResetCaption2L0( ) ;
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode207 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         EndLevel2L207( ) ;
         Gx_mode = sMode207;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
      }

      protected void OnDeleteControls2L207( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            /* Using cursor T002L21 */
            pr_default.execute(19, new Object[] {n811ContagemResultadoChckLstLog_ChckLstCod, A811ContagemResultadoChckLstLog_ChckLstCod});
            A812ContagemResultadoChckLstLog_ChckLstDes = T002L21_A812ContagemResultadoChckLstLog_ChckLstDes[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A812ContagemResultadoChckLstLog_ChckLstDes", A812ContagemResultadoChckLstLog_ChckLstDes);
            n812ContagemResultadoChckLstLog_ChckLstDes = T002L21_n812ContagemResultadoChckLstLog_ChckLstDes[0];
            pr_default.close(19);
            /* Using cursor T002L22 */
            pr_default.execute(20, new Object[] {A822ContagemResultadoChckLstLog_UsuarioCod});
            A823ContagemResultadoChckLstLog_PessoaCod = T002L22_A823ContagemResultadoChckLstLog_PessoaCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A823ContagemResultadoChckLstLog_PessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A823ContagemResultadoChckLstLog_PessoaCod), 6, 0)));
            n823ContagemResultadoChckLstLog_PessoaCod = T002L22_n823ContagemResultadoChckLstLog_PessoaCod[0];
            pr_default.close(20);
            /* Using cursor T002L23 */
            pr_default.execute(21, new Object[] {n823ContagemResultadoChckLstLog_PessoaCod, A823ContagemResultadoChckLstLog_PessoaCod});
            A817ContagemResultadoChckLstLog_UsuarioNome = T002L23_A817ContagemResultadoChckLstLog_UsuarioNome[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A817ContagemResultadoChckLstLog_UsuarioNome", A817ContagemResultadoChckLstLog_UsuarioNome);
            n817ContagemResultadoChckLstLog_UsuarioNome = T002L23_n817ContagemResultadoChckLstLog_UsuarioNome[0];
            pr_default.close(21);
         }
      }

      protected void EndLevel2L207( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete2L207( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(1);
            pr_default.close(19);
            pr_default.close(20);
            pr_default.close(21);
            context.CommitDataStores( "ContagemResultadoChckLstLog");
            if ( AnyError == 0 )
            {
               ConfirmValues2L0( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(1);
            pr_default.close(19);
            pr_default.close(20);
            pr_default.close(21);
            context.RollbackDataStores( "ContagemResultadoChckLstLog");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart2L207( )
      {
         /* Using cursor T002L24 */
         pr_default.execute(22);
         RcdFound207 = 0;
         if ( (pr_default.getStatus(22) != 101) )
         {
            RcdFound207 = 1;
            A820ContagemResultadoChckLstLog_Codigo = T002L24_A820ContagemResultadoChckLstLog_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A820ContagemResultadoChckLstLog_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A820ContagemResultadoChckLstLog_Codigo), 6, 0)));
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext2L207( )
      {
         /* Scan next routine */
         pr_default.readNext(22);
         RcdFound207 = 0;
         if ( (pr_default.getStatus(22) != 101) )
         {
            RcdFound207 = 1;
            A820ContagemResultadoChckLstLog_Codigo = T002L24_A820ContagemResultadoChckLstLog_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A820ContagemResultadoChckLstLog_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A820ContagemResultadoChckLstLog_Codigo), 6, 0)));
         }
      }

      protected void ScanEnd2L207( )
      {
         pr_default.close(22);
      }

      protected void AfterConfirm2L207( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert2L207( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate2L207( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete2L207( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete2L207( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate2L207( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes2L207( )
      {
         edtContagemResultadoChckLstLog_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoChckLstLog_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultadoChckLstLog_Codigo_Enabled), 5, 0)));
         edtContagemResultadoChckLstLog_OSCodigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoChckLstLog_OSCodigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultadoChckLstLog_OSCodigo_Enabled), 5, 0)));
         edtContagemResultadoChckLstLog_DataHora_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoChckLstLog_DataHora_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultadoChckLstLog_DataHora_Enabled), 5, 0)));
         edtContagemResultadoChckLstLog_ChckLstCod_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoChckLstLog_ChckLstCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultadoChckLstLog_ChckLstCod_Enabled), 5, 0)));
         edtContagemResultadoChckLstLog_ChckLstDes_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoChckLstLog_ChckLstDes_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultadoChckLstLog_ChckLstDes_Enabled), 5, 0)));
         dynNaoConformidade_Codigo.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynNaoConformidade_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynNaoConformidade_Codigo.Enabled), 5, 0)));
         edtContagemResultadoChckLstLog_UsuarioCod_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoChckLstLog_UsuarioCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultadoChckLstLog_UsuarioCod_Enabled), 5, 0)));
         edtContagemResultadoChckLstLog_PessoaCod_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoChckLstLog_PessoaCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultadoChckLstLog_PessoaCod_Enabled), 5, 0)));
         edtContagemResultadoChckLstLog_UsuarioNome_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoChckLstLog_UsuarioNome_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultadoChckLstLog_UsuarioNome_Enabled), 5, 0)));
         edtContagemResultadoChckLstLog_Etapa_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoChckLstLog_Etapa_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultadoChckLstLog_Etapa_Enabled), 5, 0)));
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues2L0( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20206216171885");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("contagemresultadochcklstlog.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "Z820ContagemResultadoChckLstLog_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z820ContagemResultadoChckLstLog_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z814ContagemResultadoChckLstLog_DataHora", context.localUtil.TToC( Z814ContagemResultadoChckLstLog_DataHora, 10, 8, 0, 0, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "Z824ContagemResultadoChckLstLog_Etapa", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z824ContagemResultadoChckLstLog_Etapa), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z426NaoConformidade_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z426NaoConformidade_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1853ContagemResultadoChckLstLog_OSCodigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1853ContagemResultadoChckLstLog_OSCodigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z811ContagemResultadoChckLstLog_ChckLstCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z811ContagemResultadoChckLstLog_ChckLstCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z822ContagemResultadoChckLstLog_UsuarioCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z822ContagemResultadoChckLstLog_UsuarioCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("contagemresultadochcklstlog.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "ContagemResultadoChckLstLog" ;
      }

      public override String GetPgmdesc( )
      {
         return "Contagem Resultado Chck Lst Log" ;
      }

      protected void InitializeNonKey2L207( )
      {
         A1853ContagemResultadoChckLstLog_OSCodigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1853ContagemResultadoChckLstLog_OSCodigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1853ContagemResultadoChckLstLog_OSCodigo), 6, 0)));
         A814ContagemResultadoChckLstLog_DataHora = (DateTime)(DateTime.MinValue);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A814ContagemResultadoChckLstLog_DataHora", context.localUtil.TToC( A814ContagemResultadoChckLstLog_DataHora, 8, 5, 0, 3, "/", ":", " "));
         A811ContagemResultadoChckLstLog_ChckLstCod = 0;
         n811ContagemResultadoChckLstLog_ChckLstCod = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A811ContagemResultadoChckLstLog_ChckLstCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A811ContagemResultadoChckLstLog_ChckLstCod), 6, 0)));
         n811ContagemResultadoChckLstLog_ChckLstCod = ((0==A811ContagemResultadoChckLstLog_ChckLstCod) ? true : false);
         A812ContagemResultadoChckLstLog_ChckLstDes = "";
         n812ContagemResultadoChckLstLog_ChckLstDes = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A812ContagemResultadoChckLstLog_ChckLstDes", A812ContagemResultadoChckLstLog_ChckLstDes);
         A426NaoConformidade_Codigo = 0;
         n426NaoConformidade_Codigo = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A426NaoConformidade_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A426NaoConformidade_Codigo), 6, 0)));
         n426NaoConformidade_Codigo = ((0==A426NaoConformidade_Codigo) ? true : false);
         A822ContagemResultadoChckLstLog_UsuarioCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A822ContagemResultadoChckLstLog_UsuarioCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A822ContagemResultadoChckLstLog_UsuarioCod), 6, 0)));
         A823ContagemResultadoChckLstLog_PessoaCod = 0;
         n823ContagemResultadoChckLstLog_PessoaCod = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A823ContagemResultadoChckLstLog_PessoaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A823ContagemResultadoChckLstLog_PessoaCod), 6, 0)));
         A817ContagemResultadoChckLstLog_UsuarioNome = "";
         n817ContagemResultadoChckLstLog_UsuarioNome = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A817ContagemResultadoChckLstLog_UsuarioNome", A817ContagemResultadoChckLstLog_UsuarioNome);
         A824ContagemResultadoChckLstLog_Etapa = 0;
         n824ContagemResultadoChckLstLog_Etapa = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A824ContagemResultadoChckLstLog_Etapa", StringUtil.Str( (decimal)(A824ContagemResultadoChckLstLog_Etapa), 1, 0));
         n824ContagemResultadoChckLstLog_Etapa = ((0==A824ContagemResultadoChckLstLog_Etapa) ? true : false);
         Z814ContagemResultadoChckLstLog_DataHora = (DateTime)(DateTime.MinValue);
         Z824ContagemResultadoChckLstLog_Etapa = 0;
         Z426NaoConformidade_Codigo = 0;
         Z1853ContagemResultadoChckLstLog_OSCodigo = 0;
         Z811ContagemResultadoChckLstLog_ChckLstCod = 0;
         Z822ContagemResultadoChckLstLog_UsuarioCod = 0;
      }

      protected void InitAll2L207( )
      {
         A820ContagemResultadoChckLstLog_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A820ContagemResultadoChckLstLog_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A820ContagemResultadoChckLstLog_Codigo), 6, 0)));
         InitializeNonKey2L207( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20206216171892");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("contagemresultadochcklstlog.js", "?20206216171893");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         imgBtn_first_Internalname = "BTN_FIRST";
         imgBtn_first_separator_Internalname = "BTN_FIRST_SEPARATOR";
         imgBtn_previous_Internalname = "BTN_PREVIOUS";
         imgBtn_previous_separator_Internalname = "BTN_PREVIOUS_SEPARATOR";
         imgBtn_next_Internalname = "BTN_NEXT";
         imgBtn_next_separator_Internalname = "BTN_NEXT_SEPARATOR";
         imgBtn_last_Internalname = "BTN_LAST";
         imgBtn_last_separator_Internalname = "BTN_LAST_SEPARATOR";
         imgBtn_select_Internalname = "BTN_SELECT";
         imgBtn_select_separator_Internalname = "BTN_SELECT_SEPARATOR";
         imgBtn_enter2_Internalname = "BTN_ENTER2";
         imgBtn_enter2_separator_Internalname = "BTN_ENTER2_SEPARATOR";
         imgBtn_cancel2_Internalname = "BTN_CANCEL2";
         imgBtn_cancel2_separator_Internalname = "BTN_CANCEL2_SEPARATOR";
         imgBtn_delete2_Internalname = "BTN_DELETE2";
         imgBtn_delete2_separator_Internalname = "BTN_DELETE2_SEPARATOR";
         divSectiontoolbar_Internalname = "SECTIONTOOLBAR";
         tblTabletoolbar_Internalname = "TABLETOOLBAR";
         lblTextblockcontagemresultadochcklstlog_codigo_Internalname = "TEXTBLOCKCONTAGEMRESULTADOCHCKLSTLOG_CODIGO";
         edtContagemResultadoChckLstLog_Codigo_Internalname = "CONTAGEMRESULTADOCHCKLSTLOG_CODIGO";
         lblTextblockcontagemresultadochcklstlog_oscodigo_Internalname = "TEXTBLOCKCONTAGEMRESULTADOCHCKLSTLOG_OSCODIGO";
         edtContagemResultadoChckLstLog_OSCodigo_Internalname = "CONTAGEMRESULTADOCHCKLSTLOG_OSCODIGO";
         lblTextblockcontagemresultadochcklstlog_datahora_Internalname = "TEXTBLOCKCONTAGEMRESULTADOCHCKLSTLOG_DATAHORA";
         edtContagemResultadoChckLstLog_DataHora_Internalname = "CONTAGEMRESULTADOCHCKLSTLOG_DATAHORA";
         lblTextblockcontagemresultadochcklstlog_chcklstcod_Internalname = "TEXTBLOCKCONTAGEMRESULTADOCHCKLSTLOG_CHCKLSTCOD";
         edtContagemResultadoChckLstLog_ChckLstCod_Internalname = "CONTAGEMRESULTADOCHCKLSTLOG_CHCKLSTCOD";
         lblTextblockcontagemresultadochcklstlog_chcklstdes_Internalname = "TEXTBLOCKCONTAGEMRESULTADOCHCKLSTLOG_CHCKLSTDES";
         edtContagemResultadoChckLstLog_ChckLstDes_Internalname = "CONTAGEMRESULTADOCHCKLSTLOG_CHCKLSTDES";
         lblTextblocknaoconformidade_codigo_Internalname = "TEXTBLOCKNAOCONFORMIDADE_CODIGO";
         dynNaoConformidade_Codigo_Internalname = "NAOCONFORMIDADE_CODIGO";
         lblTextblockcontagemresultadochcklstlog_usuariocod_Internalname = "TEXTBLOCKCONTAGEMRESULTADOCHCKLSTLOG_USUARIOCOD";
         edtContagemResultadoChckLstLog_UsuarioCod_Internalname = "CONTAGEMRESULTADOCHCKLSTLOG_USUARIOCOD";
         lblTextblockcontagemresultadochcklstlog_pessoacod_Internalname = "TEXTBLOCKCONTAGEMRESULTADOCHCKLSTLOG_PESSOACOD";
         edtContagemResultadoChckLstLog_PessoaCod_Internalname = "CONTAGEMRESULTADOCHCKLSTLOG_PESSOACOD";
         lblTextblockcontagemresultadochcklstlog_usuarionome_Internalname = "TEXTBLOCKCONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME";
         edtContagemResultadoChckLstLog_UsuarioNome_Internalname = "CONTAGEMRESULTADOCHCKLSTLOG_USUARIONOME";
         lblTextblockcontagemresultadochcklstlog_etapa_Internalname = "TEXTBLOCKCONTAGEMRESULTADOCHCKLSTLOG_ETAPA";
         edtContagemResultadoChckLstLog_Etapa_Internalname = "CONTAGEMRESULTADOCHCKLSTLOG_ETAPA";
         tblTable2_Internalname = "TABLE2";
         bttBtn_enter_Internalname = "BTN_ENTER";
         bttBtn_cancel_Internalname = "BTN_CANCEL";
         bttBtn_delete_Internalname = "BTN_DELETE";
         tblTable1_Internalname = "TABLE1";
         grpGroupdata_Internalname = "GROUPDATA";
         tblTablemain_Internalname = "TABLEMAIN";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Contagem Resultado Chck Lst Log";
         imgBtn_delete2_separator_Visible = 1;
         imgBtn_delete2_Enabled = 1;
         imgBtn_delete2_Visible = 1;
         imgBtn_cancel2_separator_Visible = 1;
         imgBtn_cancel2_Visible = 1;
         imgBtn_enter2_separator_Visible = 1;
         imgBtn_enter2_Enabled = 1;
         imgBtn_enter2_Visible = 1;
         imgBtn_select_separator_Visible = 1;
         imgBtn_select_Visible = 1;
         imgBtn_last_separator_Visible = 1;
         imgBtn_last_Visible = 1;
         imgBtn_next_separator_Visible = 1;
         imgBtn_next_Visible = 1;
         imgBtn_previous_separator_Visible = 1;
         imgBtn_previous_Visible = 1;
         imgBtn_first_separator_Visible = 1;
         imgBtn_first_Visible = 1;
         edtContagemResultadoChckLstLog_Etapa_Jsonclick = "";
         edtContagemResultadoChckLstLog_Etapa_Enabled = 1;
         edtContagemResultadoChckLstLog_UsuarioNome_Jsonclick = "";
         edtContagemResultadoChckLstLog_UsuarioNome_Enabled = 0;
         edtContagemResultadoChckLstLog_PessoaCod_Jsonclick = "";
         edtContagemResultadoChckLstLog_PessoaCod_Enabled = 0;
         edtContagemResultadoChckLstLog_UsuarioCod_Jsonclick = "";
         edtContagemResultadoChckLstLog_UsuarioCod_Enabled = 1;
         dynNaoConformidade_Codigo_Jsonclick = "";
         dynNaoConformidade_Codigo.Enabled = 1;
         edtContagemResultadoChckLstLog_ChckLstDes_Enabled = 0;
         edtContagemResultadoChckLstLog_ChckLstCod_Jsonclick = "";
         edtContagemResultadoChckLstLog_ChckLstCod_Enabled = 1;
         edtContagemResultadoChckLstLog_DataHora_Jsonclick = "";
         edtContagemResultadoChckLstLog_DataHora_Enabled = 1;
         edtContagemResultadoChckLstLog_OSCodigo_Jsonclick = "";
         edtContagemResultadoChckLstLog_OSCodigo_Enabled = 1;
         edtContagemResultadoChckLstLog_Codigo_Jsonclick = "";
         edtContagemResultadoChckLstLog_Codigo_Enabled = 1;
         bttBtn_delete_Visible = 1;
         bttBtn_cancel_Visible = 1;
         bttBtn_enter_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void GXDLANAOCONFORMIDADE_CODIGO2L207( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLANAOCONFORMIDADE_CODIGO_data2L207( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXANAOCONFORMIDADE_CODIGO_html2L207( )
      {
         int gxdynajaxvalue ;
         GXDLANAOCONFORMIDADE_CODIGO_data2L207( ) ;
         gxdynajaxindex = 1;
         dynNaoConformidade_Codigo.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynNaoConformidade_Codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
      }

      protected void GXDLANAOCONFORMIDADE_CODIGO_data2L207( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("(Nenhuma)");
         /* Using cursor T002L25 */
         pr_default.execute(23);
         while ( (pr_default.getStatus(23) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(T002L25_A426NaoConformidade_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( T002L25_A427NaoConformidade_Nome[0]));
            pr_default.readNext(23);
         }
         pr_default.close(23);
      }

      protected void AfterKeyLoadScreen( )
      {
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         getEqualNoModal( ) ;
         GX_FocusControl = edtContagemResultadoChckLstLog_OSCodigo_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         standaloneNotModal( ) ;
         standaloneModal( ) ;
         /* End function AfterKeyLoadScreen */
      }

      public void Valid_Contagemresultadochcklstlog_codigo( int GX_Parm1 ,
                                                            DateTime GX_Parm2 ,
                                                            short GX_Parm3 ,
                                                            GXCombobox dynGX_Parm4 ,
                                                            int GX_Parm5 ,
                                                            int GX_Parm6 ,
                                                            int GX_Parm7 )
      {
         A820ContagemResultadoChckLstLog_Codigo = GX_Parm1;
         A814ContagemResultadoChckLstLog_DataHora = GX_Parm2;
         A824ContagemResultadoChckLstLog_Etapa = GX_Parm3;
         n824ContagemResultadoChckLstLog_Etapa = false;
         dynNaoConformidade_Codigo = dynGX_Parm4;
         A426NaoConformidade_Codigo = (int)(NumberUtil.Val( dynNaoConformidade_Codigo.CurrentValue, "."));
         n426NaoConformidade_Codigo = false;
         A1853ContagemResultadoChckLstLog_OSCodigo = GX_Parm5;
         A811ContagemResultadoChckLstLog_ChckLstCod = GX_Parm6;
         n811ContagemResultadoChckLstLog_ChckLstCod = false;
         A822ContagemResultadoChckLstLog_UsuarioCod = GX_Parm7;
         context.wbHandled = 1;
         AfterKeyLoadScreen( ) ;
         Draw( ) ;
         SendCloseFormHiddens( ) ;
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A812ContagemResultadoChckLstLog_ChckLstDes = "";
            n812ContagemResultadoChckLstLog_ChckLstDes = false;
            A823ContagemResultadoChckLstLog_PessoaCod = 0;
            n823ContagemResultadoChckLstLog_PessoaCod = false;
            A817ContagemResultadoChckLstLog_UsuarioNome = "";
            n817ContagemResultadoChckLstLog_UsuarioNome = false;
         }
         GXANAOCONFORMIDADE_CODIGO_html2L207( ) ;
         dynNaoConformidade_Codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A426NaoConformidade_Codigo), 6, 0));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A1853ContagemResultadoChckLstLog_OSCodigo), 6, 0, ".", "")));
         isValidOutput.Add(context.localUtil.TToC( A814ContagemResultadoChckLstLog_DataHora, 10, 8, 0, 3, "/", ":", " "));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A811ContagemResultadoChckLstLog_ChckLstCod), 6, 0, ".", "")));
         if ( dynNaoConformidade_Codigo.ItemCount > 0 )
         {
            A426NaoConformidade_Codigo = (int)(NumberUtil.Val( dynNaoConformidade_Codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A426NaoConformidade_Codigo), 6, 0))), "."));
            n426NaoConformidade_Codigo = false;
         }
         dynNaoConformidade_Codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A426NaoConformidade_Codigo), 6, 0));
         isValidOutput.Add(dynNaoConformidade_Codigo);
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A822ContagemResultadoChckLstLog_UsuarioCod), 6, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A824ContagemResultadoChckLstLog_Etapa), 1, 0, ".", "")));
         isValidOutput.Add(A812ContagemResultadoChckLstLog_ChckLstDes);
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A823ContagemResultadoChckLstLog_PessoaCod), 6, 0, ".", "")));
         isValidOutput.Add(StringUtil.RTrim( A817ContagemResultadoChckLstLog_UsuarioNome));
         isValidOutput.Add(StringUtil.RTrim( Gx_mode));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z820ContagemResultadoChckLstLog_Codigo), 6, 0, ",", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1853ContagemResultadoChckLstLog_OSCodigo), 6, 0, ",", "")));
         isValidOutput.Add(context.localUtil.TToC( Z814ContagemResultadoChckLstLog_DataHora, 10, 8, 0, 0, "/", ":", " "));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z811ContagemResultadoChckLstLog_ChckLstCod), 6, 0, ",", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z426NaoConformidade_Codigo), 6, 0, ",", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z822ContagemResultadoChckLstLog_UsuarioCod), 6, 0, ",", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z824ContagemResultadoChckLstLog_Etapa), 1, 0, ",", "")));
         isValidOutput.Add(Z812ContagemResultadoChckLstLog_ChckLstDes);
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z823ContagemResultadoChckLstLog_PessoaCod), 6, 0, ".", "")));
         isValidOutput.Add(StringUtil.RTrim( Z817ContagemResultadoChckLstLog_UsuarioNome));
         isValidOutput.Add(imgBtn_delete2_Enabled);
         isValidOutput.Add(imgBtn_enter2_Enabled);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Contagemresultadochcklstlog_oscodigo( int GX_Parm1 )
      {
         A1853ContagemResultadoChckLstLog_OSCodigo = GX_Parm1;
         /* Using cursor T002L26 */
         pr_default.execute(24, new Object[] {A1853ContagemResultadoChckLstLog_OSCodigo});
         if ( (pr_default.getStatus(24) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contagem Resultado Chck Lst Log_Contagem Resutlado'.", "ForeignKeyNotFound", 1, "CONTAGEMRESULTADOCHCKLSTLOG_OSCODIGO");
            AnyError = 1;
            GX_FocusControl = edtContagemResultadoChckLstLog_OSCodigo_Internalname;
         }
         pr_default.close(24);
         dynload_actions( ) ;
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Contagemresultadochcklstlog_chcklstcod( int GX_Parm1 ,
                                                                String GX_Parm2 )
      {
         A811ContagemResultadoChckLstLog_ChckLstCod = GX_Parm1;
         n811ContagemResultadoChckLstLog_ChckLstCod = false;
         A812ContagemResultadoChckLstLog_ChckLstDes = GX_Parm2;
         n812ContagemResultadoChckLstLog_ChckLstDes = false;
         /* Using cursor T002L27 */
         pr_default.execute(25, new Object[] {n811ContagemResultadoChckLstLog_ChckLstCod, A811ContagemResultadoChckLstLog_ChckLstCod});
         if ( (pr_default.getStatus(25) == 101) )
         {
            if ( ! ( (0==A811ContagemResultadoChckLstLog_ChckLstCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Contagem Resultado Chck LstLog_Check List'.", "ForeignKeyNotFound", 1, "CONTAGEMRESULTADOCHCKLSTLOG_CHCKLSTCOD");
               AnyError = 1;
               GX_FocusControl = edtContagemResultadoChckLstLog_ChckLstCod_Internalname;
            }
         }
         A812ContagemResultadoChckLstLog_ChckLstDes = T002L27_A812ContagemResultadoChckLstLog_ChckLstDes[0];
         n812ContagemResultadoChckLstLog_ChckLstDes = T002L27_n812ContagemResultadoChckLstLog_ChckLstDes[0];
         pr_default.close(25);
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A812ContagemResultadoChckLstLog_ChckLstDes = "";
            n812ContagemResultadoChckLstLog_ChckLstDes = false;
         }
         isValidOutput.Add(A812ContagemResultadoChckLstLog_ChckLstDes);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Naoconformidade_codigo( GXCombobox dynGX_Parm1 )
      {
         dynNaoConformidade_Codigo = dynGX_Parm1;
         A426NaoConformidade_Codigo = (int)(NumberUtil.Val( dynNaoConformidade_Codigo.CurrentValue, "."));
         n426NaoConformidade_Codigo = false;
         /* Using cursor T002L28 */
         pr_default.execute(26, new Object[] {n426NaoConformidade_Codigo, A426NaoConformidade_Codigo});
         if ( (pr_default.getStatus(26) == 101) )
         {
            if ( ! ( (0==A426NaoConformidade_Codigo) ) )
            {
               GX_msglist.addItem("N�o existe 'N�o Conformidade'.", "ForeignKeyNotFound", 1, "NAOCONFORMIDADE_CODIGO");
               AnyError = 1;
               GX_FocusControl = dynNaoConformidade_Codigo_Internalname;
            }
         }
         pr_default.close(26);
         dynload_actions( ) ;
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Contagemresultadochcklstlog_usuariocod( int GX_Parm1 ,
                                                                int GX_Parm2 ,
                                                                String GX_Parm3 )
      {
         A822ContagemResultadoChckLstLog_UsuarioCod = GX_Parm1;
         A823ContagemResultadoChckLstLog_PessoaCod = GX_Parm2;
         n823ContagemResultadoChckLstLog_PessoaCod = false;
         A817ContagemResultadoChckLstLog_UsuarioNome = GX_Parm3;
         n817ContagemResultadoChckLstLog_UsuarioNome = false;
         /* Using cursor T002L29 */
         pr_default.execute(27, new Object[] {A822ContagemResultadoChckLstLog_UsuarioCod});
         if ( (pr_default.getStatus(27) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contagem Resultado Chck Lst Log_Usuario'.", "ForeignKeyNotFound", 1, "CONTAGEMRESULTADOCHCKLSTLOG_USUARIOCOD");
            AnyError = 1;
            GX_FocusControl = edtContagemResultadoChckLstLog_UsuarioCod_Internalname;
         }
         A823ContagemResultadoChckLstLog_PessoaCod = T002L29_A823ContagemResultadoChckLstLog_PessoaCod[0];
         n823ContagemResultadoChckLstLog_PessoaCod = T002L29_n823ContagemResultadoChckLstLog_PessoaCod[0];
         pr_default.close(27);
         /* Using cursor T002L30 */
         pr_default.execute(28, new Object[] {n823ContagemResultadoChckLstLog_PessoaCod, A823ContagemResultadoChckLstLog_PessoaCod});
         if ( (pr_default.getStatus(28) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Pessoa'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A817ContagemResultadoChckLstLog_UsuarioNome = T002L30_A817ContagemResultadoChckLstLog_UsuarioNome[0];
         n817ContagemResultadoChckLstLog_UsuarioNome = T002L30_n817ContagemResultadoChckLstLog_UsuarioNome[0];
         pr_default.close(28);
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A823ContagemResultadoChckLstLog_PessoaCod = 0;
            n823ContagemResultadoChckLstLog_PessoaCod = false;
            A817ContagemResultadoChckLstLog_UsuarioNome = "";
            n817ContagemResultadoChckLstLog_UsuarioNome = false;
         }
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A823ContagemResultadoChckLstLog_PessoaCod), 6, 0, ".", "")));
         isValidOutput.Add(StringUtil.RTrim( A817ContagemResultadoChckLstLog_UsuarioNome));
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(26);
         pr_default.close(24);
         pr_default.close(25);
         pr_default.close(19);
         pr_default.close(27);
         pr_default.close(20);
         pr_default.close(28);
         pr_default.close(21);
      }

      public override void initialize( )
      {
         sPrefix = "";
         Z814ContagemResultadoChckLstLog_DataHora = (DateTime)(DateTime.MinValue);
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         bttBtn_enter_Jsonclick = "";
         bttBtn_cancel_Jsonclick = "";
         bttBtn_delete_Jsonclick = "";
         lblTextblockcontagemresultadochcklstlog_codigo_Jsonclick = "";
         lblTextblockcontagemresultadochcklstlog_oscodigo_Jsonclick = "";
         lblTextblockcontagemresultadochcklstlog_datahora_Jsonclick = "";
         A814ContagemResultadoChckLstLog_DataHora = (DateTime)(DateTime.MinValue);
         lblTextblockcontagemresultadochcklstlog_chcklstcod_Jsonclick = "";
         lblTextblockcontagemresultadochcklstlog_chcklstdes_Jsonclick = "";
         A812ContagemResultadoChckLstLog_ChckLstDes = "";
         lblTextblocknaoconformidade_codigo_Jsonclick = "";
         lblTextblockcontagemresultadochcklstlog_usuariocod_Jsonclick = "";
         lblTextblockcontagemresultadochcklstlog_pessoacod_Jsonclick = "";
         lblTextblockcontagemresultadochcklstlog_usuarionome_Jsonclick = "";
         A817ContagemResultadoChckLstLog_UsuarioNome = "";
         lblTextblockcontagemresultadochcklstlog_etapa_Jsonclick = "";
         imgBtn_first_Jsonclick = "";
         imgBtn_first_separator_Jsonclick = "";
         imgBtn_previous_Jsonclick = "";
         imgBtn_previous_separator_Jsonclick = "";
         imgBtn_next_Jsonclick = "";
         imgBtn_next_separator_Jsonclick = "";
         imgBtn_last_Jsonclick = "";
         imgBtn_last_separator_Jsonclick = "";
         imgBtn_select_Jsonclick = "";
         imgBtn_select_separator_Jsonclick = "";
         imgBtn_enter2_Jsonclick = "";
         imgBtn_enter2_separator_Jsonclick = "";
         imgBtn_cancel2_Jsonclick = "";
         imgBtn_cancel2_separator_Jsonclick = "";
         imgBtn_delete2_Jsonclick = "";
         imgBtn_delete2_separator_Jsonclick = "";
         Gx_mode = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         Z812ContagemResultadoChckLstLog_ChckLstDes = "";
         Z817ContagemResultadoChckLstLog_UsuarioNome = "";
         T002L9_A820ContagemResultadoChckLstLog_Codigo = new int[1] ;
         T002L9_A814ContagemResultadoChckLstLog_DataHora = new DateTime[] {DateTime.MinValue} ;
         T002L9_A812ContagemResultadoChckLstLog_ChckLstDes = new String[] {""} ;
         T002L9_n812ContagemResultadoChckLstLog_ChckLstDes = new bool[] {false} ;
         T002L9_A817ContagemResultadoChckLstLog_UsuarioNome = new String[] {""} ;
         T002L9_n817ContagemResultadoChckLstLog_UsuarioNome = new bool[] {false} ;
         T002L9_A824ContagemResultadoChckLstLog_Etapa = new short[1] ;
         T002L9_n824ContagemResultadoChckLstLog_Etapa = new bool[] {false} ;
         T002L9_A426NaoConformidade_Codigo = new int[1] ;
         T002L9_n426NaoConformidade_Codigo = new bool[] {false} ;
         T002L9_A1853ContagemResultadoChckLstLog_OSCodigo = new int[1] ;
         T002L9_A811ContagemResultadoChckLstLog_ChckLstCod = new int[1] ;
         T002L9_n811ContagemResultadoChckLstLog_ChckLstCod = new bool[] {false} ;
         T002L9_A822ContagemResultadoChckLstLog_UsuarioCod = new int[1] ;
         T002L9_A823ContagemResultadoChckLstLog_PessoaCod = new int[1] ;
         T002L9_n823ContagemResultadoChckLstLog_PessoaCod = new bool[] {false} ;
         T002L5_A1853ContagemResultadoChckLstLog_OSCodigo = new int[1] ;
         T002L6_A812ContagemResultadoChckLstLog_ChckLstDes = new String[] {""} ;
         T002L6_n812ContagemResultadoChckLstLog_ChckLstDes = new bool[] {false} ;
         T002L4_A426NaoConformidade_Codigo = new int[1] ;
         T002L4_n426NaoConformidade_Codigo = new bool[] {false} ;
         T002L7_A823ContagemResultadoChckLstLog_PessoaCod = new int[1] ;
         T002L7_n823ContagemResultadoChckLstLog_PessoaCod = new bool[] {false} ;
         T002L8_A817ContagemResultadoChckLstLog_UsuarioNome = new String[] {""} ;
         T002L8_n817ContagemResultadoChckLstLog_UsuarioNome = new bool[] {false} ;
         T002L10_A1853ContagemResultadoChckLstLog_OSCodigo = new int[1] ;
         T002L11_A812ContagemResultadoChckLstLog_ChckLstDes = new String[] {""} ;
         T002L11_n812ContagemResultadoChckLstLog_ChckLstDes = new bool[] {false} ;
         T002L12_A426NaoConformidade_Codigo = new int[1] ;
         T002L12_n426NaoConformidade_Codigo = new bool[] {false} ;
         T002L13_A823ContagemResultadoChckLstLog_PessoaCod = new int[1] ;
         T002L13_n823ContagemResultadoChckLstLog_PessoaCod = new bool[] {false} ;
         T002L14_A817ContagemResultadoChckLstLog_UsuarioNome = new String[] {""} ;
         T002L14_n817ContagemResultadoChckLstLog_UsuarioNome = new bool[] {false} ;
         T002L15_A820ContagemResultadoChckLstLog_Codigo = new int[1] ;
         T002L3_A820ContagemResultadoChckLstLog_Codigo = new int[1] ;
         T002L3_A814ContagemResultadoChckLstLog_DataHora = new DateTime[] {DateTime.MinValue} ;
         T002L3_A824ContagemResultadoChckLstLog_Etapa = new short[1] ;
         T002L3_n824ContagemResultadoChckLstLog_Etapa = new bool[] {false} ;
         T002L3_A426NaoConformidade_Codigo = new int[1] ;
         T002L3_n426NaoConformidade_Codigo = new bool[] {false} ;
         T002L3_A1853ContagemResultadoChckLstLog_OSCodigo = new int[1] ;
         T002L3_A811ContagemResultadoChckLstLog_ChckLstCod = new int[1] ;
         T002L3_n811ContagemResultadoChckLstLog_ChckLstCod = new bool[] {false} ;
         T002L3_A822ContagemResultadoChckLstLog_UsuarioCod = new int[1] ;
         sMode207 = "";
         T002L16_A820ContagemResultadoChckLstLog_Codigo = new int[1] ;
         T002L17_A820ContagemResultadoChckLstLog_Codigo = new int[1] ;
         T002L2_A820ContagemResultadoChckLstLog_Codigo = new int[1] ;
         T002L2_A814ContagemResultadoChckLstLog_DataHora = new DateTime[] {DateTime.MinValue} ;
         T002L2_A824ContagemResultadoChckLstLog_Etapa = new short[1] ;
         T002L2_n824ContagemResultadoChckLstLog_Etapa = new bool[] {false} ;
         T002L2_A426NaoConformidade_Codigo = new int[1] ;
         T002L2_n426NaoConformidade_Codigo = new bool[] {false} ;
         T002L2_A1853ContagemResultadoChckLstLog_OSCodigo = new int[1] ;
         T002L2_A811ContagemResultadoChckLstLog_ChckLstCod = new int[1] ;
         T002L2_n811ContagemResultadoChckLstLog_ChckLstCod = new bool[] {false} ;
         T002L2_A822ContagemResultadoChckLstLog_UsuarioCod = new int[1] ;
         T002L18_A820ContagemResultadoChckLstLog_Codigo = new int[1] ;
         T002L21_A812ContagemResultadoChckLstLog_ChckLstDes = new String[] {""} ;
         T002L21_n812ContagemResultadoChckLstLog_ChckLstDes = new bool[] {false} ;
         T002L22_A823ContagemResultadoChckLstLog_PessoaCod = new int[1] ;
         T002L22_n823ContagemResultadoChckLstLog_PessoaCod = new bool[] {false} ;
         T002L23_A817ContagemResultadoChckLstLog_UsuarioNome = new String[] {""} ;
         T002L23_n817ContagemResultadoChckLstLog_UsuarioNome = new bool[] {false} ;
         T002L24_A820ContagemResultadoChckLstLog_Codigo = new int[1] ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         T002L25_A426NaoConformidade_Codigo = new int[1] ;
         T002L25_n426NaoConformidade_Codigo = new bool[] {false} ;
         T002L25_A427NaoConformidade_Nome = new String[] {""} ;
         isValidOutput = new GxUnknownObjectCollection();
         T002L26_A1853ContagemResultadoChckLstLog_OSCodigo = new int[1] ;
         T002L27_A812ContagemResultadoChckLstLog_ChckLstDes = new String[] {""} ;
         T002L27_n812ContagemResultadoChckLstLog_ChckLstDes = new bool[] {false} ;
         T002L28_A426NaoConformidade_Codigo = new int[1] ;
         T002L28_n426NaoConformidade_Codigo = new bool[] {false} ;
         T002L29_A823ContagemResultadoChckLstLog_PessoaCod = new int[1] ;
         T002L29_n823ContagemResultadoChckLstLog_PessoaCod = new bool[] {false} ;
         T002L30_A817ContagemResultadoChckLstLog_UsuarioNome = new String[] {""} ;
         T002L30_n817ContagemResultadoChckLstLog_UsuarioNome = new bool[] {false} ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.contagemresultadochcklstlog__default(),
            new Object[][] {
                new Object[] {
               T002L2_A820ContagemResultadoChckLstLog_Codigo, T002L2_A814ContagemResultadoChckLstLog_DataHora, T002L2_A824ContagemResultadoChckLstLog_Etapa, T002L2_n824ContagemResultadoChckLstLog_Etapa, T002L2_A426NaoConformidade_Codigo, T002L2_n426NaoConformidade_Codigo, T002L2_A1853ContagemResultadoChckLstLog_OSCodigo, T002L2_A811ContagemResultadoChckLstLog_ChckLstCod, T002L2_n811ContagemResultadoChckLstLog_ChckLstCod, T002L2_A822ContagemResultadoChckLstLog_UsuarioCod
               }
               , new Object[] {
               T002L3_A820ContagemResultadoChckLstLog_Codigo, T002L3_A814ContagemResultadoChckLstLog_DataHora, T002L3_A824ContagemResultadoChckLstLog_Etapa, T002L3_n824ContagemResultadoChckLstLog_Etapa, T002L3_A426NaoConformidade_Codigo, T002L3_n426NaoConformidade_Codigo, T002L3_A1853ContagemResultadoChckLstLog_OSCodigo, T002L3_A811ContagemResultadoChckLstLog_ChckLstCod, T002L3_n811ContagemResultadoChckLstLog_ChckLstCod, T002L3_A822ContagemResultadoChckLstLog_UsuarioCod
               }
               , new Object[] {
               T002L4_A426NaoConformidade_Codigo
               }
               , new Object[] {
               T002L5_A1853ContagemResultadoChckLstLog_OSCodigo
               }
               , new Object[] {
               T002L6_A812ContagemResultadoChckLstLog_ChckLstDes, T002L6_n812ContagemResultadoChckLstLog_ChckLstDes
               }
               , new Object[] {
               T002L7_A823ContagemResultadoChckLstLog_PessoaCod, T002L7_n823ContagemResultadoChckLstLog_PessoaCod
               }
               , new Object[] {
               T002L8_A817ContagemResultadoChckLstLog_UsuarioNome, T002L8_n817ContagemResultadoChckLstLog_UsuarioNome
               }
               , new Object[] {
               T002L9_A820ContagemResultadoChckLstLog_Codigo, T002L9_A814ContagemResultadoChckLstLog_DataHora, T002L9_A812ContagemResultadoChckLstLog_ChckLstDes, T002L9_n812ContagemResultadoChckLstLog_ChckLstDes, T002L9_A817ContagemResultadoChckLstLog_UsuarioNome, T002L9_n817ContagemResultadoChckLstLog_UsuarioNome, T002L9_A824ContagemResultadoChckLstLog_Etapa, T002L9_n824ContagemResultadoChckLstLog_Etapa, T002L9_A426NaoConformidade_Codigo, T002L9_n426NaoConformidade_Codigo,
               T002L9_A1853ContagemResultadoChckLstLog_OSCodigo, T002L9_A811ContagemResultadoChckLstLog_ChckLstCod, T002L9_n811ContagemResultadoChckLstLog_ChckLstCod, T002L9_A822ContagemResultadoChckLstLog_UsuarioCod, T002L9_A823ContagemResultadoChckLstLog_PessoaCod, T002L9_n823ContagemResultadoChckLstLog_PessoaCod
               }
               , new Object[] {
               T002L10_A1853ContagemResultadoChckLstLog_OSCodigo
               }
               , new Object[] {
               T002L11_A812ContagemResultadoChckLstLog_ChckLstDes, T002L11_n812ContagemResultadoChckLstLog_ChckLstDes
               }
               , new Object[] {
               T002L12_A426NaoConformidade_Codigo
               }
               , new Object[] {
               T002L13_A823ContagemResultadoChckLstLog_PessoaCod, T002L13_n823ContagemResultadoChckLstLog_PessoaCod
               }
               , new Object[] {
               T002L14_A817ContagemResultadoChckLstLog_UsuarioNome, T002L14_n817ContagemResultadoChckLstLog_UsuarioNome
               }
               , new Object[] {
               T002L15_A820ContagemResultadoChckLstLog_Codigo
               }
               , new Object[] {
               T002L16_A820ContagemResultadoChckLstLog_Codigo
               }
               , new Object[] {
               T002L17_A820ContagemResultadoChckLstLog_Codigo
               }
               , new Object[] {
               T002L18_A820ContagemResultadoChckLstLog_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T002L21_A812ContagemResultadoChckLstLog_ChckLstDes, T002L21_n812ContagemResultadoChckLstLog_ChckLstDes
               }
               , new Object[] {
               T002L22_A823ContagemResultadoChckLstLog_PessoaCod, T002L22_n823ContagemResultadoChckLstLog_PessoaCod
               }
               , new Object[] {
               T002L23_A817ContagemResultadoChckLstLog_UsuarioNome, T002L23_n817ContagemResultadoChckLstLog_UsuarioNome
               }
               , new Object[] {
               T002L24_A820ContagemResultadoChckLstLog_Codigo
               }
               , new Object[] {
               T002L25_A426NaoConformidade_Codigo, T002L25_A427NaoConformidade_Nome
               }
               , new Object[] {
               T002L26_A1853ContagemResultadoChckLstLog_OSCodigo
               }
               , new Object[] {
               T002L27_A812ContagemResultadoChckLstLog_ChckLstDes, T002L27_n812ContagemResultadoChckLstLog_ChckLstDes
               }
               , new Object[] {
               T002L28_A426NaoConformidade_Codigo
               }
               , new Object[] {
               T002L29_A823ContagemResultadoChckLstLog_PessoaCod, T002L29_n823ContagemResultadoChckLstLog_PessoaCod
               }
               , new Object[] {
               T002L30_A817ContagemResultadoChckLstLog_UsuarioNome, T002L30_n817ContagemResultadoChckLstLog_UsuarioNome
               }
            }
         );
      }

      private short Z824ContagemResultadoChckLstLog_Etapa ;
      private short GxWebError ;
      private short gxcookieaux ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short A824ContagemResultadoChckLstLog_Etapa ;
      private short GX_JID ;
      private short RcdFound207 ;
      private short Gx_BScreen ;
      private short gxajaxcallmode ;
      private short wbTemp ;
      private int Z820ContagemResultadoChckLstLog_Codigo ;
      private int Z426NaoConformidade_Codigo ;
      private int Z1853ContagemResultadoChckLstLog_OSCodigo ;
      private int Z811ContagemResultadoChckLstLog_ChckLstCod ;
      private int Z822ContagemResultadoChckLstLog_UsuarioCod ;
      private int A1853ContagemResultadoChckLstLog_OSCodigo ;
      private int A811ContagemResultadoChckLstLog_ChckLstCod ;
      private int A426NaoConformidade_Codigo ;
      private int A822ContagemResultadoChckLstLog_UsuarioCod ;
      private int A823ContagemResultadoChckLstLog_PessoaCod ;
      private int trnEnded ;
      private int bttBtn_enter_Visible ;
      private int bttBtn_cancel_Visible ;
      private int bttBtn_delete_Visible ;
      private int A820ContagemResultadoChckLstLog_Codigo ;
      private int edtContagemResultadoChckLstLog_Codigo_Enabled ;
      private int edtContagemResultadoChckLstLog_OSCodigo_Enabled ;
      private int edtContagemResultadoChckLstLog_DataHora_Enabled ;
      private int edtContagemResultadoChckLstLog_ChckLstCod_Enabled ;
      private int edtContagemResultadoChckLstLog_ChckLstDes_Enabled ;
      private int edtContagemResultadoChckLstLog_UsuarioCod_Enabled ;
      private int edtContagemResultadoChckLstLog_PessoaCod_Enabled ;
      private int edtContagemResultadoChckLstLog_UsuarioNome_Enabled ;
      private int edtContagemResultadoChckLstLog_Etapa_Enabled ;
      private int imgBtn_first_Visible ;
      private int imgBtn_first_separator_Visible ;
      private int imgBtn_previous_Visible ;
      private int imgBtn_previous_separator_Visible ;
      private int imgBtn_next_Visible ;
      private int imgBtn_next_separator_Visible ;
      private int imgBtn_last_Visible ;
      private int imgBtn_last_separator_Visible ;
      private int imgBtn_select_Visible ;
      private int imgBtn_select_separator_Visible ;
      private int imgBtn_enter2_Visible ;
      private int imgBtn_enter2_Enabled ;
      private int imgBtn_enter2_separator_Visible ;
      private int imgBtn_cancel2_Visible ;
      private int imgBtn_cancel2_separator_Visible ;
      private int imgBtn_delete2_Visible ;
      private int imgBtn_delete2_Enabled ;
      private int imgBtn_delete2_separator_Visible ;
      private int Z823ContagemResultadoChckLstLog_PessoaCod ;
      private int idxLst ;
      private int gxdynajaxindex ;
      private String sPrefix ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String GXKey ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String edtContagemResultadoChckLstLog_Codigo_Internalname ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String grpGroupdata_Internalname ;
      private String tblTable1_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String TempTags ;
      private String bttBtn_enter_Internalname ;
      private String bttBtn_enter_Jsonclick ;
      private String bttBtn_cancel_Internalname ;
      private String bttBtn_cancel_Jsonclick ;
      private String bttBtn_delete_Internalname ;
      private String bttBtn_delete_Jsonclick ;
      private String tblTable2_Internalname ;
      private String lblTextblockcontagemresultadochcklstlog_codigo_Internalname ;
      private String lblTextblockcontagemresultadochcklstlog_codigo_Jsonclick ;
      private String edtContagemResultadoChckLstLog_Codigo_Jsonclick ;
      private String lblTextblockcontagemresultadochcklstlog_oscodigo_Internalname ;
      private String lblTextblockcontagemresultadochcklstlog_oscodigo_Jsonclick ;
      private String edtContagemResultadoChckLstLog_OSCodigo_Internalname ;
      private String edtContagemResultadoChckLstLog_OSCodigo_Jsonclick ;
      private String lblTextblockcontagemresultadochcklstlog_datahora_Internalname ;
      private String lblTextblockcontagemresultadochcklstlog_datahora_Jsonclick ;
      private String edtContagemResultadoChckLstLog_DataHora_Internalname ;
      private String edtContagemResultadoChckLstLog_DataHora_Jsonclick ;
      private String lblTextblockcontagemresultadochcklstlog_chcklstcod_Internalname ;
      private String lblTextblockcontagemresultadochcklstlog_chcklstcod_Jsonclick ;
      private String edtContagemResultadoChckLstLog_ChckLstCod_Internalname ;
      private String edtContagemResultadoChckLstLog_ChckLstCod_Jsonclick ;
      private String lblTextblockcontagemresultadochcklstlog_chcklstdes_Internalname ;
      private String lblTextblockcontagemresultadochcklstlog_chcklstdes_Jsonclick ;
      private String edtContagemResultadoChckLstLog_ChckLstDes_Internalname ;
      private String lblTextblocknaoconformidade_codigo_Internalname ;
      private String lblTextblocknaoconformidade_codigo_Jsonclick ;
      private String dynNaoConformidade_Codigo_Internalname ;
      private String dynNaoConformidade_Codigo_Jsonclick ;
      private String lblTextblockcontagemresultadochcklstlog_usuariocod_Internalname ;
      private String lblTextblockcontagemresultadochcklstlog_usuariocod_Jsonclick ;
      private String edtContagemResultadoChckLstLog_UsuarioCod_Internalname ;
      private String edtContagemResultadoChckLstLog_UsuarioCod_Jsonclick ;
      private String lblTextblockcontagemresultadochcklstlog_pessoacod_Internalname ;
      private String lblTextblockcontagemresultadochcklstlog_pessoacod_Jsonclick ;
      private String edtContagemResultadoChckLstLog_PessoaCod_Internalname ;
      private String edtContagemResultadoChckLstLog_PessoaCod_Jsonclick ;
      private String lblTextblockcontagemresultadochcklstlog_usuarionome_Internalname ;
      private String lblTextblockcontagemresultadochcklstlog_usuarionome_Jsonclick ;
      private String edtContagemResultadoChckLstLog_UsuarioNome_Internalname ;
      private String A817ContagemResultadoChckLstLog_UsuarioNome ;
      private String edtContagemResultadoChckLstLog_UsuarioNome_Jsonclick ;
      private String lblTextblockcontagemresultadochcklstlog_etapa_Internalname ;
      private String lblTextblockcontagemresultadochcklstlog_etapa_Jsonclick ;
      private String edtContagemResultadoChckLstLog_Etapa_Internalname ;
      private String edtContagemResultadoChckLstLog_Etapa_Jsonclick ;
      private String tblTabletoolbar_Internalname ;
      private String divSectiontoolbar_Internalname ;
      private String imgBtn_first_Internalname ;
      private String imgBtn_first_Jsonclick ;
      private String imgBtn_first_separator_Internalname ;
      private String imgBtn_first_separator_Jsonclick ;
      private String imgBtn_previous_Internalname ;
      private String imgBtn_previous_Jsonclick ;
      private String imgBtn_previous_separator_Internalname ;
      private String imgBtn_previous_separator_Jsonclick ;
      private String imgBtn_next_Internalname ;
      private String imgBtn_next_Jsonclick ;
      private String imgBtn_next_separator_Internalname ;
      private String imgBtn_next_separator_Jsonclick ;
      private String imgBtn_last_Internalname ;
      private String imgBtn_last_Jsonclick ;
      private String imgBtn_last_separator_Internalname ;
      private String imgBtn_last_separator_Jsonclick ;
      private String imgBtn_select_Internalname ;
      private String imgBtn_select_Jsonclick ;
      private String imgBtn_select_separator_Internalname ;
      private String imgBtn_select_separator_Jsonclick ;
      private String imgBtn_enter2_Internalname ;
      private String imgBtn_enter2_Jsonclick ;
      private String imgBtn_enter2_separator_Internalname ;
      private String imgBtn_enter2_separator_Jsonclick ;
      private String imgBtn_cancel2_Internalname ;
      private String imgBtn_cancel2_Jsonclick ;
      private String imgBtn_cancel2_separator_Internalname ;
      private String imgBtn_cancel2_separator_Jsonclick ;
      private String imgBtn_delete2_Internalname ;
      private String imgBtn_delete2_Jsonclick ;
      private String imgBtn_delete2_separator_Internalname ;
      private String imgBtn_delete2_separator_Jsonclick ;
      private String Gx_mode ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String Z817ContagemResultadoChckLstLog_UsuarioNome ;
      private String sMode207 ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String gxwrpcisep ;
      private DateTime Z814ContagemResultadoChckLstLog_DataHora ;
      private DateTime A814ContagemResultadoChckLstLog_DataHora ;
      private bool entryPointCalled ;
      private bool n811ContagemResultadoChckLstLog_ChckLstCod ;
      private bool n426NaoConformidade_Codigo ;
      private bool n823ContagemResultadoChckLstLog_PessoaCod ;
      private bool toggleJsOutput ;
      private bool wbErr ;
      private bool n812ContagemResultadoChckLstLog_ChckLstDes ;
      private bool n817ContagemResultadoChckLstLog_UsuarioNome ;
      private bool n824ContagemResultadoChckLstLog_Etapa ;
      private bool Gx_longc ;
      private String A812ContagemResultadoChckLstLog_ChckLstDes ;
      private String Z812ContagemResultadoChckLstLog_ChckLstDes ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private GxUnknownObjectCollection isValidOutput ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox dynNaoConformidade_Codigo ;
      private IDataStoreProvider pr_default ;
      private int[] T002L9_A820ContagemResultadoChckLstLog_Codigo ;
      private DateTime[] T002L9_A814ContagemResultadoChckLstLog_DataHora ;
      private String[] T002L9_A812ContagemResultadoChckLstLog_ChckLstDes ;
      private bool[] T002L9_n812ContagemResultadoChckLstLog_ChckLstDes ;
      private String[] T002L9_A817ContagemResultadoChckLstLog_UsuarioNome ;
      private bool[] T002L9_n817ContagemResultadoChckLstLog_UsuarioNome ;
      private short[] T002L9_A824ContagemResultadoChckLstLog_Etapa ;
      private bool[] T002L9_n824ContagemResultadoChckLstLog_Etapa ;
      private int[] T002L9_A426NaoConformidade_Codigo ;
      private bool[] T002L9_n426NaoConformidade_Codigo ;
      private int[] T002L9_A1853ContagemResultadoChckLstLog_OSCodigo ;
      private int[] T002L9_A811ContagemResultadoChckLstLog_ChckLstCod ;
      private bool[] T002L9_n811ContagemResultadoChckLstLog_ChckLstCod ;
      private int[] T002L9_A822ContagemResultadoChckLstLog_UsuarioCod ;
      private int[] T002L9_A823ContagemResultadoChckLstLog_PessoaCod ;
      private bool[] T002L9_n823ContagemResultadoChckLstLog_PessoaCod ;
      private int[] T002L5_A1853ContagemResultadoChckLstLog_OSCodigo ;
      private String[] T002L6_A812ContagemResultadoChckLstLog_ChckLstDes ;
      private bool[] T002L6_n812ContagemResultadoChckLstLog_ChckLstDes ;
      private int[] T002L4_A426NaoConformidade_Codigo ;
      private bool[] T002L4_n426NaoConformidade_Codigo ;
      private int[] T002L7_A823ContagemResultadoChckLstLog_PessoaCod ;
      private bool[] T002L7_n823ContagemResultadoChckLstLog_PessoaCod ;
      private String[] T002L8_A817ContagemResultadoChckLstLog_UsuarioNome ;
      private bool[] T002L8_n817ContagemResultadoChckLstLog_UsuarioNome ;
      private int[] T002L10_A1853ContagemResultadoChckLstLog_OSCodigo ;
      private String[] T002L11_A812ContagemResultadoChckLstLog_ChckLstDes ;
      private bool[] T002L11_n812ContagemResultadoChckLstLog_ChckLstDes ;
      private int[] T002L12_A426NaoConformidade_Codigo ;
      private bool[] T002L12_n426NaoConformidade_Codigo ;
      private int[] T002L13_A823ContagemResultadoChckLstLog_PessoaCod ;
      private bool[] T002L13_n823ContagemResultadoChckLstLog_PessoaCod ;
      private String[] T002L14_A817ContagemResultadoChckLstLog_UsuarioNome ;
      private bool[] T002L14_n817ContagemResultadoChckLstLog_UsuarioNome ;
      private int[] T002L15_A820ContagemResultadoChckLstLog_Codigo ;
      private int[] T002L3_A820ContagemResultadoChckLstLog_Codigo ;
      private DateTime[] T002L3_A814ContagemResultadoChckLstLog_DataHora ;
      private short[] T002L3_A824ContagemResultadoChckLstLog_Etapa ;
      private bool[] T002L3_n824ContagemResultadoChckLstLog_Etapa ;
      private int[] T002L3_A426NaoConformidade_Codigo ;
      private bool[] T002L3_n426NaoConformidade_Codigo ;
      private int[] T002L3_A1853ContagemResultadoChckLstLog_OSCodigo ;
      private int[] T002L3_A811ContagemResultadoChckLstLog_ChckLstCod ;
      private bool[] T002L3_n811ContagemResultadoChckLstLog_ChckLstCod ;
      private int[] T002L3_A822ContagemResultadoChckLstLog_UsuarioCod ;
      private int[] T002L16_A820ContagemResultadoChckLstLog_Codigo ;
      private int[] T002L17_A820ContagemResultadoChckLstLog_Codigo ;
      private int[] T002L2_A820ContagemResultadoChckLstLog_Codigo ;
      private DateTime[] T002L2_A814ContagemResultadoChckLstLog_DataHora ;
      private short[] T002L2_A824ContagemResultadoChckLstLog_Etapa ;
      private bool[] T002L2_n824ContagemResultadoChckLstLog_Etapa ;
      private int[] T002L2_A426NaoConformidade_Codigo ;
      private bool[] T002L2_n426NaoConformidade_Codigo ;
      private int[] T002L2_A1853ContagemResultadoChckLstLog_OSCodigo ;
      private int[] T002L2_A811ContagemResultadoChckLstLog_ChckLstCod ;
      private bool[] T002L2_n811ContagemResultadoChckLstLog_ChckLstCod ;
      private int[] T002L2_A822ContagemResultadoChckLstLog_UsuarioCod ;
      private int[] T002L18_A820ContagemResultadoChckLstLog_Codigo ;
      private String[] T002L21_A812ContagemResultadoChckLstLog_ChckLstDes ;
      private bool[] T002L21_n812ContagemResultadoChckLstLog_ChckLstDes ;
      private int[] T002L22_A823ContagemResultadoChckLstLog_PessoaCod ;
      private bool[] T002L22_n823ContagemResultadoChckLstLog_PessoaCod ;
      private String[] T002L23_A817ContagemResultadoChckLstLog_UsuarioNome ;
      private bool[] T002L23_n817ContagemResultadoChckLstLog_UsuarioNome ;
      private int[] T002L24_A820ContagemResultadoChckLstLog_Codigo ;
      private int[] T002L25_A426NaoConformidade_Codigo ;
      private bool[] T002L25_n426NaoConformidade_Codigo ;
      private String[] T002L25_A427NaoConformidade_Nome ;
      private int[] T002L26_A1853ContagemResultadoChckLstLog_OSCodigo ;
      private String[] T002L27_A812ContagemResultadoChckLstLog_ChckLstDes ;
      private bool[] T002L27_n812ContagemResultadoChckLstLog_ChckLstDes ;
      private int[] T002L28_A426NaoConformidade_Codigo ;
      private bool[] T002L28_n426NaoConformidade_Codigo ;
      private int[] T002L29_A823ContagemResultadoChckLstLog_PessoaCod ;
      private bool[] T002L29_n823ContagemResultadoChckLstLog_PessoaCod ;
      private String[] T002L30_A817ContagemResultadoChckLstLog_UsuarioNome ;
      private bool[] T002L30_n817ContagemResultadoChckLstLog_UsuarioNome ;
      private GXWebForm Form ;
   }

   public class contagemresultadochcklstlog__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new ForEachCursor(def[12])
         ,new ForEachCursor(def[13])
         ,new ForEachCursor(def[14])
         ,new ForEachCursor(def[15])
         ,new ForEachCursor(def[16])
         ,new UpdateCursor(def[17])
         ,new UpdateCursor(def[18])
         ,new ForEachCursor(def[19])
         ,new ForEachCursor(def[20])
         ,new ForEachCursor(def[21])
         ,new ForEachCursor(def[22])
         ,new ForEachCursor(def[23])
         ,new ForEachCursor(def[24])
         ,new ForEachCursor(def[25])
         ,new ForEachCursor(def[26])
         ,new ForEachCursor(def[27])
         ,new ForEachCursor(def[28])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT002L9 ;
          prmT002L9 = new Object[] {
          new Object[] {"@ContagemResultadoChckLstLog_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002L5 ;
          prmT002L5 = new Object[] {
          new Object[] {"@ContagemResultadoChckLstLog_OSCodigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002L6 ;
          prmT002L6 = new Object[] {
          new Object[] {"@ContagemResultadoChckLstLog_ChckLstCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002L4 ;
          prmT002L4 = new Object[] {
          new Object[] {"@NaoConformidade_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002L7 ;
          prmT002L7 = new Object[] {
          new Object[] {"@ContagemResultadoChckLstLog_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002L8 ;
          prmT002L8 = new Object[] {
          new Object[] {"@ContagemResultadoChckLstLog_PessoaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002L10 ;
          prmT002L10 = new Object[] {
          new Object[] {"@ContagemResultadoChckLstLog_OSCodigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002L11 ;
          prmT002L11 = new Object[] {
          new Object[] {"@ContagemResultadoChckLstLog_ChckLstCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002L12 ;
          prmT002L12 = new Object[] {
          new Object[] {"@NaoConformidade_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002L13 ;
          prmT002L13 = new Object[] {
          new Object[] {"@ContagemResultadoChckLstLog_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002L14 ;
          prmT002L14 = new Object[] {
          new Object[] {"@ContagemResultadoChckLstLog_PessoaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002L15 ;
          prmT002L15 = new Object[] {
          new Object[] {"@ContagemResultadoChckLstLog_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002L3 ;
          prmT002L3 = new Object[] {
          new Object[] {"@ContagemResultadoChckLstLog_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002L16 ;
          prmT002L16 = new Object[] {
          new Object[] {"@ContagemResultadoChckLstLog_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002L17 ;
          prmT002L17 = new Object[] {
          new Object[] {"@ContagemResultadoChckLstLog_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002L2 ;
          prmT002L2 = new Object[] {
          new Object[] {"@ContagemResultadoChckLstLog_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002L18 ;
          prmT002L18 = new Object[] {
          new Object[] {"@ContagemResultadoChckLstLog_DataHora",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultadoChckLstLog_Etapa",SqlDbType.SmallInt,1,0} ,
          new Object[] {"@NaoConformidade_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultadoChckLstLog_OSCodigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultadoChckLstLog_ChckLstCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultadoChckLstLog_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002L19 ;
          prmT002L19 = new Object[] {
          new Object[] {"@ContagemResultadoChckLstLog_DataHora",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultadoChckLstLog_Etapa",SqlDbType.SmallInt,1,0} ,
          new Object[] {"@NaoConformidade_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultadoChckLstLog_OSCodigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultadoChckLstLog_ChckLstCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultadoChckLstLog_UsuarioCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultadoChckLstLog_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002L20 ;
          prmT002L20 = new Object[] {
          new Object[] {"@ContagemResultadoChckLstLog_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002L21 ;
          prmT002L21 = new Object[] {
          new Object[] {"@ContagemResultadoChckLstLog_ChckLstCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002L22 ;
          prmT002L22 = new Object[] {
          new Object[] {"@ContagemResultadoChckLstLog_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002L23 ;
          prmT002L23 = new Object[] {
          new Object[] {"@ContagemResultadoChckLstLog_PessoaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002L24 ;
          prmT002L24 = new Object[] {
          } ;
          Object[] prmT002L25 ;
          prmT002L25 = new Object[] {
          } ;
          Object[] prmT002L26 ;
          prmT002L26 = new Object[] {
          new Object[] {"@ContagemResultadoChckLstLog_OSCodigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002L27 ;
          prmT002L27 = new Object[] {
          new Object[] {"@ContagemResultadoChckLstLog_ChckLstCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002L28 ;
          prmT002L28 = new Object[] {
          new Object[] {"@NaoConformidade_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002L29 ;
          prmT002L29 = new Object[] {
          new Object[] {"@ContagemResultadoChckLstLog_UsuarioCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT002L30 ;
          prmT002L30 = new Object[] {
          new Object[] {"@ContagemResultadoChckLstLog_PessoaCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("T002L2", "SELECT [ContagemResultadoChckLstLog_Codigo], [ContagemResultadoChckLstLog_DataHora], [ContagemResultadoChckLstLog_Etapa], [NaoConformidade_Codigo], [ContagemResultadoChckLstLog_OSCodigo] AS ContagemResultadoChckLstLog_OSCodigo, [ContagemResultadoChckLstLog_ChckLstCod] AS ContagemResultadoChckLstLog_ChckLstCod, [ContagemResultadoChckLstLog_UsuarioCod] AS ContagemResultadoChckLstLog_UsuarioCod FROM [ContagemResultadoChckLstLog] WITH (UPDLOCK) WHERE [ContagemResultadoChckLstLog_Codigo] = @ContagemResultadoChckLstLog_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT002L2,1,0,true,false )
             ,new CursorDef("T002L3", "SELECT [ContagemResultadoChckLstLog_Codigo], [ContagemResultadoChckLstLog_DataHora], [ContagemResultadoChckLstLog_Etapa], [NaoConformidade_Codigo], [ContagemResultadoChckLstLog_OSCodigo] AS ContagemResultadoChckLstLog_OSCodigo, [ContagemResultadoChckLstLog_ChckLstCod] AS ContagemResultadoChckLstLog_ChckLstCod, [ContagemResultadoChckLstLog_UsuarioCod] AS ContagemResultadoChckLstLog_UsuarioCod FROM [ContagemResultadoChckLstLog] WITH (NOLOCK) WHERE [ContagemResultadoChckLstLog_Codigo] = @ContagemResultadoChckLstLog_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT002L3,1,0,true,false )
             ,new CursorDef("T002L4", "SELECT [NaoConformidade_Codigo] FROM [NaoConformidade] WITH (NOLOCK) WHERE [NaoConformidade_Codigo] = @NaoConformidade_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT002L4,1,0,true,false )
             ,new CursorDef("T002L5", "SELECT [ContagemResultado_Codigo] AS ContagemResultadoChckLstLog_OSCodigo FROM [ContagemResultado] WITH (NOLOCK) WHERE [ContagemResultado_Codigo] = @ContagemResultadoChckLstLog_OSCodigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT002L5,1,0,true,false )
             ,new CursorDef("T002L6", "SELECT [CheckList_Descricao] AS ContagemResultadoChckLstLog_ChckLstDes FROM [CheckList] WITH (NOLOCK) WHERE [CheckList_Codigo] = @ContagemResultadoChckLstLog_ChckLstCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT002L6,1,0,true,false )
             ,new CursorDef("T002L7", "SELECT [Usuario_PessoaCod] AS ContagemResultadoChckLstLog_PessoaCod FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @ContagemResultadoChckLstLog_UsuarioCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT002L7,1,0,true,false )
             ,new CursorDef("T002L8", "SELECT [Pessoa_Nome] AS ContagemResultadoChckLstLog_UsuarioNome FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @ContagemResultadoChckLstLog_PessoaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT002L8,1,0,true,false )
             ,new CursorDef("T002L9", "SELECT TM1.[ContagemResultadoChckLstLog_Codigo], TM1.[ContagemResultadoChckLstLog_DataHora], T2.[CheckList_Descricao] AS ContagemResultadoChckLstLog_ChckLstDes, T4.[Pessoa_Nome] AS ContagemResultadoChckLstLog_UsuarioNome, TM1.[ContagemResultadoChckLstLog_Etapa], TM1.[NaoConformidade_Codigo], TM1.[ContagemResultadoChckLstLog_OSCodigo] AS ContagemResultadoChckLstLog_OSCodigo, TM1.[ContagemResultadoChckLstLog_ChckLstCod] AS ContagemResultadoChckLstLog_ChckLstCod, TM1.[ContagemResultadoChckLstLog_UsuarioCod] AS ContagemResultadoChckLstLog_UsuarioCod, T3.[Usuario_PessoaCod] AS ContagemResultadoChckLstLog_PessoaCod FROM ((([ContagemResultadoChckLstLog] TM1 WITH (NOLOCK) LEFT JOIN [CheckList] T2 WITH (NOLOCK) ON T2.[CheckList_Codigo] = TM1.[ContagemResultadoChckLstLog_ChckLstCod]) INNER JOIN [Usuario] T3 WITH (NOLOCK) ON T3.[Usuario_Codigo] = TM1.[ContagemResultadoChckLstLog_UsuarioCod]) LEFT JOIN [Pessoa] T4 WITH (NOLOCK) ON T4.[Pessoa_Codigo] = T3.[Usuario_PessoaCod]) WHERE TM1.[ContagemResultadoChckLstLog_Codigo] = @ContagemResultadoChckLstLog_Codigo ORDER BY TM1.[ContagemResultadoChckLstLog_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT002L9,100,0,true,false )
             ,new CursorDef("T002L10", "SELECT [ContagemResultado_Codigo] AS ContagemResultadoChckLstLog_OSCodigo FROM [ContagemResultado] WITH (NOLOCK) WHERE [ContagemResultado_Codigo] = @ContagemResultadoChckLstLog_OSCodigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT002L10,1,0,true,false )
             ,new CursorDef("T002L11", "SELECT [CheckList_Descricao] AS ContagemResultadoChckLstLog_ChckLstDes FROM [CheckList] WITH (NOLOCK) WHERE [CheckList_Codigo] = @ContagemResultadoChckLstLog_ChckLstCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT002L11,1,0,true,false )
             ,new CursorDef("T002L12", "SELECT [NaoConformidade_Codigo] FROM [NaoConformidade] WITH (NOLOCK) WHERE [NaoConformidade_Codigo] = @NaoConformidade_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT002L12,1,0,true,false )
             ,new CursorDef("T002L13", "SELECT [Usuario_PessoaCod] AS ContagemResultadoChckLstLog_PessoaCod FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @ContagemResultadoChckLstLog_UsuarioCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT002L13,1,0,true,false )
             ,new CursorDef("T002L14", "SELECT [Pessoa_Nome] AS ContagemResultadoChckLstLog_UsuarioNome FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @ContagemResultadoChckLstLog_PessoaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT002L14,1,0,true,false )
             ,new CursorDef("T002L15", "SELECT [ContagemResultadoChckLstLog_Codigo] FROM [ContagemResultadoChckLstLog] WITH (NOLOCK) WHERE [ContagemResultadoChckLstLog_Codigo] = @ContagemResultadoChckLstLog_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT002L15,1,0,true,false )
             ,new CursorDef("T002L16", "SELECT TOP 1 [ContagemResultadoChckLstLog_Codigo] FROM [ContagemResultadoChckLstLog] WITH (NOLOCK) WHERE ( [ContagemResultadoChckLstLog_Codigo] > @ContagemResultadoChckLstLog_Codigo) ORDER BY [ContagemResultadoChckLstLog_Codigo]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT002L16,1,0,true,true )
             ,new CursorDef("T002L17", "SELECT TOP 1 [ContagemResultadoChckLstLog_Codigo] FROM [ContagemResultadoChckLstLog] WITH (NOLOCK) WHERE ( [ContagemResultadoChckLstLog_Codigo] < @ContagemResultadoChckLstLog_Codigo) ORDER BY [ContagemResultadoChckLstLog_Codigo] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT002L17,1,0,true,true )
             ,new CursorDef("T002L18", "INSERT INTO [ContagemResultadoChckLstLog]([ContagemResultadoChckLstLog_DataHora], [ContagemResultadoChckLstLog_Etapa], [NaoConformidade_Codigo], [ContagemResultadoChckLstLog_OSCodigo], [ContagemResultadoChckLstLog_ChckLstCod], [ContagemResultadoChckLstLog_UsuarioCod]) VALUES(@ContagemResultadoChckLstLog_DataHora, @ContagemResultadoChckLstLog_Etapa, @NaoConformidade_Codigo, @ContagemResultadoChckLstLog_OSCodigo, @ContagemResultadoChckLstLog_ChckLstCod, @ContagemResultadoChckLstLog_UsuarioCod); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmT002L18)
             ,new CursorDef("T002L19", "UPDATE [ContagemResultadoChckLstLog] SET [ContagemResultadoChckLstLog_DataHora]=@ContagemResultadoChckLstLog_DataHora, [ContagemResultadoChckLstLog_Etapa]=@ContagemResultadoChckLstLog_Etapa, [NaoConformidade_Codigo]=@NaoConformidade_Codigo, [ContagemResultadoChckLstLog_OSCodigo]=@ContagemResultadoChckLstLog_OSCodigo, [ContagemResultadoChckLstLog_ChckLstCod]=@ContagemResultadoChckLstLog_ChckLstCod, [ContagemResultadoChckLstLog_UsuarioCod]=@ContagemResultadoChckLstLog_UsuarioCod  WHERE [ContagemResultadoChckLstLog_Codigo] = @ContagemResultadoChckLstLog_Codigo", GxErrorMask.GX_NOMASK,prmT002L19)
             ,new CursorDef("T002L20", "DELETE FROM [ContagemResultadoChckLstLog]  WHERE [ContagemResultadoChckLstLog_Codigo] = @ContagemResultadoChckLstLog_Codigo", GxErrorMask.GX_NOMASK,prmT002L20)
             ,new CursorDef("T002L21", "SELECT [CheckList_Descricao] AS ContagemResultadoChckLstLog_ChckLstDes FROM [CheckList] WITH (NOLOCK) WHERE [CheckList_Codigo] = @ContagemResultadoChckLstLog_ChckLstCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT002L21,1,0,true,false )
             ,new CursorDef("T002L22", "SELECT [Usuario_PessoaCod] AS ContagemResultadoChckLstLog_PessoaCod FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @ContagemResultadoChckLstLog_UsuarioCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT002L22,1,0,true,false )
             ,new CursorDef("T002L23", "SELECT [Pessoa_Nome] AS ContagemResultadoChckLstLog_UsuarioNome FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @ContagemResultadoChckLstLog_PessoaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT002L23,1,0,true,false )
             ,new CursorDef("T002L24", "SELECT [ContagemResultadoChckLstLog_Codigo] FROM [ContagemResultadoChckLstLog] WITH (NOLOCK) ORDER BY [ContagemResultadoChckLstLog_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT002L24,100,0,true,false )
             ,new CursorDef("T002L25", "SELECT [NaoConformidade_Codigo], [NaoConformidade_Nome] FROM [NaoConformidade] WITH (NOLOCK) ORDER BY [NaoConformidade_Nome] ",true, GxErrorMask.GX_NOMASK, false, this,prmT002L25,0,0,true,false )
             ,new CursorDef("T002L26", "SELECT [ContagemResultado_Codigo] AS ContagemResultadoChckLstLog_OSCodigo FROM [ContagemResultado] WITH (NOLOCK) WHERE [ContagemResultado_Codigo] = @ContagemResultadoChckLstLog_OSCodigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT002L26,1,0,true,false )
             ,new CursorDef("T002L27", "SELECT [CheckList_Descricao] AS ContagemResultadoChckLstLog_ChckLstDes FROM [CheckList] WITH (NOLOCK) WHERE [CheckList_Codigo] = @ContagemResultadoChckLstLog_ChckLstCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT002L27,1,0,true,false )
             ,new CursorDef("T002L28", "SELECT [NaoConformidade_Codigo] FROM [NaoConformidade] WITH (NOLOCK) WHERE [NaoConformidade_Codigo] = @NaoConformidade_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT002L28,1,0,true,false )
             ,new CursorDef("T002L29", "SELECT [Usuario_PessoaCod] AS ContagemResultadoChckLstLog_PessoaCod FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @ContagemResultadoChckLstLog_UsuarioCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT002L29,1,0,true,false )
             ,new CursorDef("T002L30", "SELECT [Pessoa_Nome] AS ContagemResultadoChckLstLog_UsuarioNome FROM [Pessoa] WITH (NOLOCK) WHERE [Pessoa_Codigo] = @ContagemResultadoChckLstLog_PessoaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT002L30,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDateTime(2) ;
                ((short[]) buf[2])[0] = rslt.getShort(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((int[]) buf[6])[0] = rslt.getInt(5) ;
                ((int[]) buf[7])[0] = rslt.getInt(6) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((int[]) buf[9])[0] = rslt.getInt(7) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDateTime(2) ;
                ((short[]) buf[2])[0] = rslt.getShort(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((int[]) buf[6])[0] = rslt.getInt(5) ;
                ((int[]) buf[7])[0] = rslt.getInt(6) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((int[]) buf[9])[0] = rslt.getInt(7) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 4 :
                ((String[]) buf[0])[0] = rslt.getLongVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 6 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDateTime(2) ;
                ((String[]) buf[2])[0] = rslt.getLongVarchar(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((String[]) buf[4])[0] = rslt.getString(4, 100) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((short[]) buf[6])[0] = rslt.getShort(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((int[]) buf[8])[0] = rslt.getInt(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((int[]) buf[10])[0] = rslt.getInt(7) ;
                ((int[]) buf[11])[0] = rslt.getInt(8) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(8);
                ((int[]) buf[13])[0] = rslt.getInt(9) ;
                ((int[]) buf[14])[0] = rslt.getInt(10) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(10);
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 9 :
                ((String[]) buf[0])[0] = rslt.getLongVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 10 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 11 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 12 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 13 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 14 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 15 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 16 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 19 :
                ((String[]) buf[0])[0] = rslt.getLongVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 20 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 21 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 22 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 23 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                return;
             case 24 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 25 :
                ((String[]) buf[0])[0] = rslt.getLongVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 26 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 27 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 28 :
                ((String[]) buf[0])[0] = rslt.getString(1, 100) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 9 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 10 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 11 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 12 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 13 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 14 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 15 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 16 :
                stmt.SetParameterDatetime(1, (DateTime)parms[0]);
                if ( (bool)parms[1] )
                {
                   stmt.setNull( 2 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(2, (short)parms[2]);
                }
                if ( (bool)parms[3] )
                {
                   stmt.setNull( 3 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(3, (int)parms[4]);
                }
                stmt.SetParameter(4, (int)parms[5]);
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 5 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(5, (int)parms[7]);
                }
                stmt.SetParameter(6, (int)parms[8]);
                return;
             case 17 :
                stmt.SetParameterDatetime(1, (DateTime)parms[0]);
                if ( (bool)parms[1] )
                {
                   stmt.setNull( 2 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(2, (short)parms[2]);
                }
                if ( (bool)parms[3] )
                {
                   stmt.setNull( 3 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(3, (int)parms[4]);
                }
                stmt.SetParameter(4, (int)parms[5]);
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 5 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(5, (int)parms[7]);
                }
                stmt.SetParameter(6, (int)parms[8]);
                stmt.SetParameter(7, (int)parms[9]);
                return;
             case 18 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 19 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 20 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 21 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 24 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 25 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 26 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 27 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 28 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
       }
    }

 }

}
