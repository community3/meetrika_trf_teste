/*
               File: WC_ContratoHistoricoConsumo
        Description: Historico Consumo
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/21/2020 18:2:1.36
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wc_contratohistoricoconsumo : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public wc_contratohistoricoconsumo( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public wc_contratohistoricoconsumo( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Contrato_Codigo )
      {
         this.AV7Contrato_Codigo = aP0_Contrato_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
         cmbHistoricoConsumo_HistoricoCodigo = new GXCombobox();
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  AV7Contrato_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Contrato_Codigo), 6, 0)));
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(int)AV7Contrato_Codigo});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
               {
                  nRC_GXsfl_14 = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  nGXsfl_14_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  sGXsfl_14_idx = GetNextPar( );
                  sPrefix = GetNextPar( );
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxnrGrid_newrow( ) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
               {
                  subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  AV7Contrato_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Contrato_Codigo), 6, 0)));
                  AV12OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV12OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12OrderedBy), 4, 0)));
                  AV13OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedDsc", AV13OrderedDsc);
                  AV22Pgmname = GetNextPar( );
                  A1560NotaEmpenho_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1560NotaEmpenho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1560NotaEmpenho_Codigo), 6, 0)));
                  A1581HistoricoConsumo_NotaEmpenhoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  n1581HistoricoConsumo_NotaEmpenhoCod = false;
                  A1564NotaEmpenho_Itentificador = GetNextPar( );
                  n1564NotaEmpenho_Itentificador = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1564NotaEmpenho_Itentificador", A1564NotaEmpenho_Itentificador);
                  A1565NotaEmpenho_DEmissao = context.localUtil.ParseDTimeParm( GetNextPar( ));
                  n1565NotaEmpenho_DEmissao = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1565NotaEmpenho_DEmissao", context.localUtil.TToC( A1565NotaEmpenho_DEmissao, 8, 5, 0, 3, "/", ":", " "));
                  sPrefix = GetNextPar( );
                  init_default_properties( ) ;
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxgrGrid_refresh( subGrid_Rows, AV7Contrato_Codigo, AV12OrderedBy, AV13OrderedDsc, AV22Pgmname, A1560NotaEmpenho_Codigo, A1581HistoricoConsumo_NotaEmpenhoCod, A1564NotaEmpenho_Itentificador, A1565NotaEmpenho_DEmissao, sPrefix) ;
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
                  return  ;
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PAM32( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV22Pgmname = "WC_ContratoHistoricoConsumo";
               context.Gx_err = 0;
               edtavNotaempenho_itentificador_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavNotaempenho_itentificador_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavNotaempenho_itentificador_Enabled), 5, 0)));
               edtavNotaempenho_demissao_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavNotaempenho_demissao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavNotaempenho_demissao_Enabled), 5, 0)));
               WSM32( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "Historico Consumo") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?2020521182144");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = ((nGXWrapped==0) ? " data-HasEnter=\"false\" data-Skiponenter=\"false\"" : "");
            context.WriteHtmlText( "<body") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            if ( nGXWrapped != 1 )
            {
               context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wc_contratohistoricoconsumo.aspx") + "?" + UrlEncode("" +AV7Contrato_Codigo)+"\">") ;
               GxWebStd.gx_hidden_field( context, "_EventName", "");
               GxWebStd.gx_hidden_field( context, "_EventGridId", "");
               GxWebStd.gx_hidden_field( context, "_EventRowId", "");
               context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
            }
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, sPrefix+"nRC_GXsfl_14", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_14), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOAV7Contrato_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOAV7Contrato_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vCONTRATO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7Contrato_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vPGMNAME", StringUtil.RTrim( AV22Pgmname));
         GxWebStd.gx_hidden_field( context, sPrefix+"NOTAEMPENHO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1560NotaEmpenho_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"NOTAEMPENHO_ITENTIFICADOR", StringUtil.RTrim( A1564NotaEmpenho_Itentificador));
         GxWebStd.gx_hidden_field( context, sPrefix+"NOTAEMPENHO_DEMISSAO", context.localUtil.TToC( A1565NotaEmpenho_DEmissao, 10, 8, 0, 0, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseFormM32( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("wc_contratohistoricoconsumo.js", "?2020521182148");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( nGXWrapped != 1 )
            {
               context.WriteHtmlTextNl( "</form>") ;
            }
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "WC_ContratoHistoricoConsumo" ;
      }

      public override String GetPgmdesc( )
      {
         return "Historico Consumo" ;
      }

      protected void WBM30( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "wc_contratohistoricoconsumo.aspx");
               context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
            }
            context.WriteHtmlText( "c") ;
            wb_table1_3_M32( true) ;
         }
         else
         {
            wb_table1_3_M32( false) ;
         }
         return  ;
      }

      protected void wb_table1_3_M32e( bool wbgen )
      {
         if ( wbgen )
         {
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 30,'" + sPrefix + "',false,'" + sGXsfl_14_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrderedby_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV12OrderedBy), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV12OrderedBy), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,30);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrderedby_Jsonclick, 0, "Attribute", "", "", "", edtavOrderedby_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WC_ContratoHistoricoConsumo.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 31,'" + sPrefix + "',false,'" + sGXsfl_14_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV13OrderedDsc), StringUtil.BoolToStr( AV13OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,31);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_WC_ContratoHistoricoConsumo.htm");
         }
         wbLoad = true;
      }

      protected void STARTM32( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "Historico Consumo", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUPM30( ) ;
            }
         }
      }

      protected void WSM32( )
      {
         STARTM32( ) ;
         EVTM32( ) ;
      }

      protected void EVTM32( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPM30( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPM30( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    GX_FocusControl = edtavOrderedby_Internalname;
                                    context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGING") == 0 )
                           {
                              context.wbHandled = 1;
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPM30( ) ;
                              }
                              sEvt = cgiGet( sPrefix+"GRIDPAGING");
                              if ( StringUtil.StrCmp(sEvt, "FIRST") == 0 )
                              {
                                 subgrid_firstpage( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "PREV") == 0 )
                              {
                                 subgrid_previouspage( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "NEXT") == 0 )
                              {
                                 subgrid_nextpage( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "LAST") == 0 )
                              {
                                 subgrid_lastpage( ) ;
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPM30( ) ;
                              }
                              nGXsfl_14_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_14_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_14_idx), 4, 0)), 4, "0");
                              SubsflControlProps_142( ) ;
                              A1562HistoricoConsumo_Codigo = (int)(context.localUtil.CToN( cgiGet( edtHistoricoConsumo_Codigo_Internalname), ",", "."));
                              A1577HistoricoConsumo_Data = context.localUtil.CToT( cgiGet( edtHistoricoConsumo_Data_Internalname), 0);
                              cmbHistoricoConsumo_HistoricoCodigo.Name = cmbHistoricoConsumo_HistoricoCodigo_Internalname;
                              cmbHistoricoConsumo_HistoricoCodigo.CurrentValue = cgiGet( cmbHistoricoConsumo_HistoricoCodigo_Internalname);
                              A1646HistoricoConsumo_HistoricoCodigo = cgiGet( cmbHistoricoConsumo_HistoricoCodigo_Internalname);
                              A1580HistoricoConsumo_SaldoContratoCod = (int)(context.localUtil.CToN( cgiGet( edtHistoricoConsumo_SaldoContratoCod_Internalname), ",", "."));
                              A1644HistoricoConsumo_SaldoContratoDtaVigIni = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtHistoricoConsumo_SaldoContratoDtaVigIni_Internalname), 0));
                              A1645HistoricoConsumo_SaldoContratoDtaVigFim = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtHistoricoConsumo_SaldoContratoDtaVigFim_Internalname), 0));
                              A1581HistoricoConsumo_NotaEmpenhoCod = (int)(context.localUtil.CToN( cgiGet( edtHistoricoConsumo_NotaEmpenhoCod_Internalname), ",", "."));
                              n1581HistoricoConsumo_NotaEmpenhoCod = false;
                              AV6NotaEmpenho_Itentificador = cgiGet( edtavNotaempenho_itentificador_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavNotaempenho_itentificador_Internalname, AV6NotaEmpenho_Itentificador);
                              AV5NotaEmpenho_DEmissao = context.localUtil.CToT( cgiGet( edtavNotaempenho_demissao_Internalname), 0);
                              context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavNotaempenho_demissao_Internalname, context.localUtil.TToC( AV5NotaEmpenho_DEmissao, 8, 5, 0, 3, "/", ":", " "));
                              A1582HistoricoConsumo_ContagemResultadoCod = (int)(context.localUtil.CToN( cgiGet( edtHistoricoConsumo_ContagemResultadoCod_Internalname), ",", "."));
                              n1582HistoricoConsumo_ContagemResultadoCod = false;
                              A1579HistoricoConsumo_ContratoCod = (int)(context.localUtil.CToN( cgiGet( edtHistoricoConsumo_ContratoCod_Internalname), ",", "."));
                              n1579HistoricoConsumo_ContratoCod = false;
                              A1578HistoricoConsumo_Valor = context.localUtil.CToN( cgiGet( edtHistoricoConsumo_Valor_Internalname), ",", ".");
                              A1563HistoricoConsumo_UsuarioCod = (int)(context.localUtil.CToN( cgiGet( edtHistoricoConsumo_UsuarioCod_Internalname), ",", "."));
                              n1563HistoricoConsumo_UsuarioCod = false;
                              AV18Usuario_Nome = StringUtil.Upper( cgiGet( edtavUsuario_nome_Internalname));
                              context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavUsuario_nome_Internalname, AV18Usuario_Nome);
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E11M32 */
                                          E11M32 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E12M32 */
                                          E12M32 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E13M32 */
                                          E13M32 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          if ( ! wbErr )
                                          {
                                             Rfr0gs = false;
                                             if ( ! Rfr0gs )
                                             {
                                             }
                                             dynload_actions( ) ;
                                          }
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                                    {
                                       STRUPM30( ) ;
                                    }
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavOrderedby_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEM32( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormM32( ) ;
            }
         }
      }

      protected void PAM32( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            GXCCtl = "HISTORICOCONSUMO_HISTORICOCODIGO_" + sGXsfl_14_idx;
            cmbHistoricoConsumo_HistoricoCodigo.Name = GXCCtl;
            cmbHistoricoConsumo_HistoricoCodigo.WebTags = "";
            cmbHistoricoConsumo_HistoricoCodigo.addItem("CRI", "Cria��o", 0);
            cmbHistoricoConsumo_HistoricoCodigo.addItem("ALT", "Altera��o", 0);
            cmbHistoricoConsumo_HistoricoCodigo.addItem("CRE", "Cr�dito", 0);
            cmbHistoricoConsumo_HistoricoCodigo.addItem("DEB", "D�bito", 0);
            cmbHistoricoConsumo_HistoricoCodigo.addItem("INI", "Inicializar", 0);
            cmbHistoricoConsumo_HistoricoCodigo.addItem("RES", "Reservar", 0);
            cmbHistoricoConsumo_HistoricoCodigo.addItem("EXE", "Executado", 0);
            cmbHistoricoConsumo_HistoricoCodigo.addItem("CRS", "Cancela Reservado", 0);
            if ( cmbHistoricoConsumo_HistoricoCodigo.ItemCount > 0 )
            {
               A1646HistoricoConsumo_HistoricoCodigo = cmbHistoricoConsumo_HistoricoCodigo.getValidValue(A1646HistoricoConsumo_HistoricoCodigo);
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = edtavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_142( ) ;
         while ( nGXsfl_14_idx <= nRC_GXsfl_14 )
         {
            sendrow_142( ) ;
            nGXsfl_14_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_14_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_14_idx+1));
            sGXsfl_14_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_14_idx), 4, 0)), 4, "0");
            SubsflControlProps_142( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       int AV7Contrato_Codigo ,
                                       short AV12OrderedBy ,
                                       bool AV13OrderedDsc ,
                                       String AV22Pgmname ,
                                       int A1560NotaEmpenho_Codigo ,
                                       int A1581HistoricoConsumo_NotaEmpenhoCod ,
                                       String A1564NotaEmpenho_Itentificador ,
                                       DateTime A1565NotaEmpenho_DEmissao ,
                                       String sPrefix )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RFM32( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_HISTORICOCONSUMO_CODIGO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1562HistoricoConsumo_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"HISTORICOCONSUMO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1562HistoricoConsumo_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_HISTORICOCONSUMO_DATA", GetSecureSignedToken( sPrefix, context.localUtil.Format( A1577HistoricoConsumo_Data, "99/99/99 99:99")));
         GxWebStd.gx_hidden_field( context, sPrefix+"HISTORICOCONSUMO_DATA", context.localUtil.TToC( A1577HistoricoConsumo_Data, 10, 8, 0, 3, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_HISTORICOCONSUMO_HISTORICOCODIGO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1646HistoricoConsumo_HistoricoCodigo, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"HISTORICOCONSUMO_HISTORICOCODIGO", StringUtil.RTrim( A1646HistoricoConsumo_HistoricoCodigo));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_HISTORICOCONSUMO_SALDOCONTRATOCOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1580HistoricoConsumo_SaldoContratoCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"HISTORICOCONSUMO_SALDOCONTRATOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1580HistoricoConsumo_SaldoContratoCod), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_HISTORICOCONSUMO_SALDOCONTRATODTAVIGINI", GetSecureSignedToken( sPrefix, A1644HistoricoConsumo_SaldoContratoDtaVigIni));
         GxWebStd.gx_hidden_field( context, sPrefix+"HISTORICOCONSUMO_SALDOCONTRATODTAVIGINI", context.localUtil.Format(A1644HistoricoConsumo_SaldoContratoDtaVigIni, "99/99/99"));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_HISTORICOCONSUMO_SALDOCONTRATODTAVIGFIM", GetSecureSignedToken( sPrefix, A1645HistoricoConsumo_SaldoContratoDtaVigFim));
         GxWebStd.gx_hidden_field( context, sPrefix+"HISTORICOCONSUMO_SALDOCONTRATODTAVIGFIM", context.localUtil.Format(A1645HistoricoConsumo_SaldoContratoDtaVigFim, "99/99/99"));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_HISTORICOCONSUMO_NOTAEMPENHOCOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1581HistoricoConsumo_NotaEmpenhoCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"HISTORICOCONSUMO_NOTAEMPENHOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1581HistoricoConsumo_NotaEmpenhoCod), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_HISTORICOCONSUMO_CONTAGEMRESULTADOCOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1582HistoricoConsumo_ContagemResultadoCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"HISTORICOCONSUMO_CONTAGEMRESULTADOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1582HistoricoConsumo_ContagemResultadoCod), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_HISTORICOCONSUMO_CONTRATOCOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1579HistoricoConsumo_ContratoCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"HISTORICOCONSUMO_CONTRATOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1579HistoricoConsumo_ContratoCod), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_HISTORICOCONSUMO_VALOR", GetSecureSignedToken( sPrefix, context.localUtil.Format( A1578HistoricoConsumo_Valor, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
         GxWebStd.gx_hidden_field( context, sPrefix+"HISTORICOCONSUMO_VALOR", StringUtil.LTrim( StringUtil.NToC( A1578HistoricoConsumo_Valor, 18, 5, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_HISTORICOCONSUMO_USUARIOCOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1563HistoricoConsumo_UsuarioCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"HISTORICOCONSUMO_USUARIOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1563HistoricoConsumo_UsuarioCod), 6, 0, ".", "")));
      }

      protected void fix_multi_value_controls( )
      {
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFM32( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV22Pgmname = "WC_ContratoHistoricoConsumo";
         context.Gx_err = 0;
         edtavNotaempenho_itentificador_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavNotaempenho_itentificador_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavNotaempenho_itentificador_Enabled), 5, 0)));
         edtavNotaempenho_demissao_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavNotaempenho_demissao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavNotaempenho_demissao_Enabled), 5, 0)));
      }

      protected void RFM32( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 14;
         /* Execute user event: E12M32 */
         E12M32 ();
         nGXsfl_14_idx = 1;
         sGXsfl_14_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_14_idx), 4, 0)), 4, "0");
         SubsflControlProps_142( ) ;
         nGXsfl_14_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", sPrefix);
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_142( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            /* Using cursor H00M33 */
            pr_default.execute(0, new Object[] {AV7Contrato_Codigo, GXPagingFrom2, GXPagingTo2});
            nGXsfl_14_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A1579HistoricoConsumo_ContratoCod = H00M33_A1579HistoricoConsumo_ContratoCod[0];
               n1579HistoricoConsumo_ContratoCod = H00M33_n1579HistoricoConsumo_ContratoCod[0];
               A1581HistoricoConsumo_NotaEmpenhoCod = H00M33_A1581HistoricoConsumo_NotaEmpenhoCod[0];
               n1581HistoricoConsumo_NotaEmpenhoCod = H00M33_n1581HistoricoConsumo_NotaEmpenhoCod[0];
               A1563HistoricoConsumo_UsuarioCod = H00M33_A1563HistoricoConsumo_UsuarioCod[0];
               n1563HistoricoConsumo_UsuarioCod = H00M33_n1563HistoricoConsumo_UsuarioCod[0];
               A1578HistoricoConsumo_Valor = H00M33_A1578HistoricoConsumo_Valor[0];
               A1582HistoricoConsumo_ContagemResultadoCod = H00M33_A1582HistoricoConsumo_ContagemResultadoCod[0];
               n1582HistoricoConsumo_ContagemResultadoCod = H00M33_n1582HistoricoConsumo_ContagemResultadoCod[0];
               A1645HistoricoConsumo_SaldoContratoDtaVigFim = H00M33_A1645HistoricoConsumo_SaldoContratoDtaVigFim[0];
               A1644HistoricoConsumo_SaldoContratoDtaVigIni = H00M33_A1644HistoricoConsumo_SaldoContratoDtaVigIni[0];
               A1580HistoricoConsumo_SaldoContratoCod = H00M33_A1580HistoricoConsumo_SaldoContratoCod[0];
               A1646HistoricoConsumo_HistoricoCodigo = H00M33_A1646HistoricoConsumo_HistoricoCodigo[0];
               A1577HistoricoConsumo_Data = H00M33_A1577HistoricoConsumo_Data[0];
               A1562HistoricoConsumo_Codigo = H00M33_A1562HistoricoConsumo_Codigo[0];
               A40000Usuario_Nome = H00M33_A40000Usuario_Nome[0];
               n40000Usuario_Nome = H00M33_n40000Usuario_Nome[0];
               A40000Usuario_Nome = H00M33_A40000Usuario_Nome[0];
               n40000Usuario_Nome = H00M33_n40000Usuario_Nome[0];
               /* Execute user event: E13M32 */
               E13M32 ();
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 14;
            WBM30( ) ;
         }
         nGXsfl_14_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         /* Using cursor H00M35 */
         pr_default.execute(1, new Object[] {AV7Contrato_Codigo});
         GRID_nRecordCount = H00M35_AGRID_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV7Contrato_Codigo, AV12OrderedBy, AV13OrderedDsc, AV22Pgmname, A1560NotaEmpenho_Codigo, A1581HistoricoConsumo_NotaEmpenhoCod, A1564NotaEmpenho_Itentificador, A1565NotaEmpenho_DEmissao, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV7Contrato_Codigo, AV12OrderedBy, AV13OrderedDsc, AV22Pgmname, A1560NotaEmpenho_Codigo, A1581HistoricoConsumo_NotaEmpenhoCod, A1564NotaEmpenho_Itentificador, A1565NotaEmpenho_DEmissao, sPrefix) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV7Contrato_Codigo, AV12OrderedBy, AV13OrderedDsc, AV22Pgmname, A1560NotaEmpenho_Codigo, A1581HistoricoConsumo_NotaEmpenhoCod, A1564NotaEmpenho_Itentificador, A1565NotaEmpenho_DEmissao, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV7Contrato_Codigo, AV12OrderedBy, AV13OrderedDsc, AV22Pgmname, A1560NotaEmpenho_Codigo, A1581HistoricoConsumo_NotaEmpenhoCod, A1564NotaEmpenho_Itentificador, A1565NotaEmpenho_DEmissao, sPrefix) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV7Contrato_Codigo, AV12OrderedBy, AV13OrderedDsc, AV22Pgmname, A1560NotaEmpenho_Codigo, A1581HistoricoConsumo_NotaEmpenhoCod, A1564NotaEmpenho_Itentificador, A1565NotaEmpenho_DEmissao, sPrefix) ;
         }
         return (int)(0) ;
      }

      protected void STRUPM30( )
      {
         /* Before Start, stand alone formulas. */
         AV22Pgmname = "WC_ContratoHistoricoConsumo";
         context.Gx_err = 0;
         edtavNotaempenho_itentificador_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavNotaempenho_itentificador_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavNotaempenho_itentificador_Enabled), 5, 0)));
         edtavNotaempenho_demissao_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavNotaempenho_demissao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavNotaempenho_demissao_Enabled), 5, 0)));
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11M32 */
         E11M32 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            if ( ( ( context.localUtil.CToN( cgiGet( edtavOrderedby_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavOrderedby_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vORDEREDBY");
               GX_FocusControl = edtavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV12OrderedBy = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV12OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12OrderedBy), 4, 0)));
            }
            else
            {
               AV12OrderedBy = (short)(context.localUtil.CToN( cgiGet( edtavOrderedby_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV12OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12OrderedBy), 4, 0)));
            }
            AV13OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedDsc", AV13OrderedDsc);
            /* Read saved values. */
            nRC_GXsfl_14 = (short)(context.localUtil.CToN( cgiGet( sPrefix+"nRC_GXsfl_14"), ",", "."));
            wcpOAV7Contrato_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV7Contrato_Codigo"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E11M32 */
         E11M32 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E11M32( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtavOrderedby_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavOrderedby_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrderedby_Visible), 5, 0)));
         if ( AV12OrderedBy < 1 )
         {
            AV12OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV12OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12OrderedBy), 4, 0)));
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
      }

      protected void E12M32( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV17WWPContext) ;
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtHistoricoConsumo_Codigo_Titleformat = 2;
         edtHistoricoConsumo_Codigo_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 1);' >%5</span>", ((AV12OrderedBy==1) ? (AV13OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, edtavOrderedby_Internalname, "Historico Consumo", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtHistoricoConsumo_Codigo_Internalname, "Title", edtHistoricoConsumo_Codigo_Title);
         edtHistoricoConsumo_SaldoContratoCod_Titleformat = 2;
         edtHistoricoConsumo_SaldoContratoCod_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 2);' >%5</span>", ((AV12OrderedBy==2) ? (AV13OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, edtavOrderedby_Internalname, "Saldo Contrato", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtHistoricoConsumo_SaldoContratoCod_Internalname, "Title", edtHistoricoConsumo_SaldoContratoCod_Title);
         edtHistoricoConsumo_NotaEmpenhoCod_Titleformat = 2;
         edtHistoricoConsumo_NotaEmpenhoCod_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 3);' >%5</span>", ((AV12OrderedBy==3) ? (AV13OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, edtavOrderedby_Internalname, "Nota Empenho", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtHistoricoConsumo_NotaEmpenhoCod_Internalname, "Title", edtHistoricoConsumo_NotaEmpenhoCod_Title);
         edtHistoricoConsumo_ContagemResultadoCod_Titleformat = 2;
         edtHistoricoConsumo_ContagemResultadoCod_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 4);' >%5</span>", ((AV12OrderedBy==4) ? (AV13OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, edtavOrderedby_Internalname, "Contagem", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtHistoricoConsumo_ContagemResultadoCod_Internalname, "Title", edtHistoricoConsumo_ContagemResultadoCod_Title);
         edtHistoricoConsumo_Data_Titleformat = 2;
         edtHistoricoConsumo_Data_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 5);' >%5</span>", ((AV12OrderedBy==5) ? (AV13OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, edtavOrderedby_Internalname, "Data", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtHistoricoConsumo_Data_Internalname, "Title", edtHistoricoConsumo_Data_Title);
         edtHistoricoConsumo_Valor_Titleformat = 2;
         edtHistoricoConsumo_Valor_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 6);' >%5</span>", ((AV12OrderedBy==6) ? (AV13OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, edtavOrderedby_Internalname, "Valor", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtHistoricoConsumo_Valor_Internalname, "Title", edtHistoricoConsumo_Valor_Title);
         edtHistoricoConsumo_UsuarioCod_Titleformat = 2;
         edtHistoricoConsumo_UsuarioCod_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 7);' >%5</span>", ((AV12OrderedBy==7) ? (AV13OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, edtavOrderedby_Internalname, "Usu�rio", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtHistoricoConsumo_UsuarioCod_Internalname, "Title", edtHistoricoConsumo_UsuarioCod_Title);
      }

      private void E13M32( )
      {
         /* Grid_Load Routine */
         AV18Usuario_Nome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavUsuario_nome_Internalname, AV18Usuario_Nome);
         AV18Usuario_Nome = A40000Usuario_Nome;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavUsuario_nome_Internalname, AV18Usuario_Nome);
         AV6NotaEmpenho_Itentificador = "";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavNotaempenho_itentificador_Internalname, AV6NotaEmpenho_Itentificador);
         AV5NotaEmpenho_DEmissao = (DateTime)(DateTime.MinValue);
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavNotaempenho_demissao_Internalname, context.localUtil.TToC( AV5NotaEmpenho_DEmissao, 8, 5, 0, 3, "/", ":", " "));
         /* Using cursor H00M36 */
         pr_default.execute(2, new Object[] {n1581HistoricoConsumo_NotaEmpenhoCod, A1581HistoricoConsumo_NotaEmpenhoCod});
         while ( (pr_default.getStatus(2) != 101) )
         {
            A1560NotaEmpenho_Codigo = H00M36_A1560NotaEmpenho_Codigo[0];
            A1564NotaEmpenho_Itentificador = H00M36_A1564NotaEmpenho_Itentificador[0];
            n1564NotaEmpenho_Itentificador = H00M36_n1564NotaEmpenho_Itentificador[0];
            A1565NotaEmpenho_DEmissao = H00M36_A1565NotaEmpenho_DEmissao[0];
            n1565NotaEmpenho_DEmissao = H00M36_n1565NotaEmpenho_DEmissao[0];
            AV6NotaEmpenho_Itentificador = A1564NotaEmpenho_Itentificador;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavNotaempenho_itentificador_Internalname, AV6NotaEmpenho_Itentificador);
            AV5NotaEmpenho_DEmissao = A1565NotaEmpenho_DEmissao;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavNotaempenho_demissao_Internalname, context.localUtil.TToC( AV5NotaEmpenho_DEmissao, 8, 5, 0, 3, "/", ":", " "));
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(2);
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 14;
         }
         sendrow_142( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_14_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(14, GridRow);
         }
      }

      protected void S122( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV14Session.Get(AV22Pgmname+"GridState"), "") == 0 )
         {
            AV8GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  AV22Pgmname+"GridState"), "");
         }
         else
         {
            AV8GridState.FromXml(AV14Session.Get(AV22Pgmname+"GridState"), "");
         }
         AV12OrderedBy = AV8GridState.gxTpr_Orderedby;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV12OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12OrderedBy), 4, 0)));
         AV13OrderedDsc = AV8GridState.gxTpr_Ordereddsc;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedDsc", AV13OrderedDsc);
      }

      protected void S132( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV8GridState.FromXml(AV14Session.Get(AV22Pgmname+"GridState"), "");
         AV8GridState.gxTpr_Orderedby = AV12OrderedBy;
         AV8GridState.gxTpr_Ordereddsc = AV13OrderedDsc;
         new wwpbaseobjects.savegridstate(context ).execute(  AV22Pgmname+"GridState",  AV8GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_MeetrikaVs3")) ;
      }

      protected void S112( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV15TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV15TrnContext.gxTpr_Callerobject = AV22Pgmname;
         AV15TrnContext.gxTpr_Callerondelete = true;
         AV15TrnContext.gxTpr_Callerurl = AV10HTTPRequest.ScriptName+"?"+AV10HTTPRequest.QueryString;
         AV15TrnContext.gxTpr_Transactionname = "HistoricoConsumo";
         AV16TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV16TrnContextAtt.gxTpr_Attributename = "Contrato_Codigo";
         AV16TrnContextAtt.gxTpr_Attributevalue = StringUtil.Str( (decimal)(AV7Contrato_Codigo), 6, 0);
         AV15TrnContext.gxTpr_Attributes.Add(AV16TrnContextAtt, 0);
         AV14Session.Set("TrnContext", AV15TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_MeetrikaVs3"));
      }

      protected void wb_table1_3_M32( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, sPrefix, "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='TableSearchCell'>") ;
            wb_table2_9_M32( true) ;
         }
         else
         {
            wb_table2_9_M32( false) ;
         }
         return  ;
      }

      protected void wb_table2_9_M32e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"DivS\" data-gxgridid=\"14\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtHistoricoConsumo_Codigo_Titleformat == 0 )
               {
                  context.SendWebValue( edtHistoricoConsumo_Codigo_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtHistoricoConsumo_Codigo_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtHistoricoConsumo_Data_Titleformat == 0 )
               {
                  context.SendWebValue( edtHistoricoConsumo_Data_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtHistoricoConsumo_Data_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "A��o") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtHistoricoConsumo_SaldoContratoCod_Titleformat == 0 )
               {
                  context.SendWebValue( edtHistoricoConsumo_SaldoContratoCod_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtHistoricoConsumo_SaldoContratoCod_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "In�cio Vig�ncia") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Fim Vig�ncia") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtHistoricoConsumo_NotaEmpenhoCod_Titleformat == 0 )
               {
                  context.SendWebValue( edtHistoricoConsumo_NotaEmpenhoCod_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtHistoricoConsumo_NotaEmpenhoCod_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Identificador") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Data Emiss�o") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtHistoricoConsumo_ContagemResultadoCod_Titleformat == 0 )
               {
                  context.SendWebValue( edtHistoricoConsumo_ContagemResultadoCod_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtHistoricoConsumo_ContagemResultadoCod_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Contrato C�digo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtHistoricoConsumo_Valor_Titleformat == 0 )
               {
                  context.SendWebValue( edtHistoricoConsumo_Valor_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtHistoricoConsumo_Valor_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtHistoricoConsumo_UsuarioCod_Titleformat == 0 )
               {
                  context.SendWebValue( edtHistoricoConsumo_UsuarioCod_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtHistoricoConsumo_UsuarioCod_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Nome") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", sPrefix);
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1562HistoricoConsumo_Codigo), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtHistoricoConsumo_Codigo_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtHistoricoConsumo_Codigo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.localUtil.TToC( A1577HistoricoConsumo_Data, 10, 8, 0, 3, "/", ":", " "));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtHistoricoConsumo_Data_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtHistoricoConsumo_Data_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A1646HistoricoConsumo_HistoricoCodigo));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1580HistoricoConsumo_SaldoContratoCod), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtHistoricoConsumo_SaldoContratoCod_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtHistoricoConsumo_SaldoContratoCod_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.localUtil.Format(A1644HistoricoConsumo_SaldoContratoDtaVigIni, "99/99/99"));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.localUtil.Format(A1645HistoricoConsumo_SaldoContratoDtaVigFim, "99/99/99"));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1581HistoricoConsumo_NotaEmpenhoCod), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtHistoricoConsumo_NotaEmpenhoCod_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtHistoricoConsumo_NotaEmpenhoCod_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( AV6NotaEmpenho_Itentificador));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavNotaempenho_itentificador_Enabled), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.localUtil.TToC( AV5NotaEmpenho_DEmissao, 10, 8, 0, 3, "/", ":", " "));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavNotaempenho_demissao_Enabled), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1582HistoricoConsumo_ContagemResultadoCod), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtHistoricoConsumo_ContagemResultadoCod_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtHistoricoConsumo_ContagemResultadoCod_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1579HistoricoConsumo_ContratoCod), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( A1578HistoricoConsumo_Valor, 18, 5, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtHistoricoConsumo_Valor_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtHistoricoConsumo_Valor_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1563HistoricoConsumo_UsuarioCod), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtHistoricoConsumo_UsuarioCod_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtHistoricoConsumo_UsuarioCod_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( AV18Usuario_Nome));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 14 )
         {
            wbEnd = 0;
            nRC_GXsfl_14 = (short)(nGXsfl_14_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               GridContainer.AddObjectProperty("GRID_nEOF", GRID_nEOF);
               GridContainer.AddObjectProperty("GRID_nFirstRecordOnPage", GRID_nFirstRecordOnPage);
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid(sPrefix+"_"+"Grid", GridContainer);
               if ( ! isAjaxCallMode( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+sPrefix+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_3_M32e( true) ;
         }
         else
         {
            wb_table1_3_M32e( false) ;
         }
      }

      protected void wb_table2_9_M32( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablesearch_Internalname, tblTablesearch_Internalname, "", "TableSearch", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_9_M32e( true) ;
         }
         else
         {
            wb_table2_9_M32e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV7Contrato_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Contrato_Codigo), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAM32( ) ;
         WSM32( ) ;
         WEM32( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlAV7Contrato_Codigo = (String)((String)getParm(obj,0));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PAM32( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "wc_contratohistoricoconsumo");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PAM32( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            AV7Contrato_Codigo = Convert.ToInt32(getParm(obj,2));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Contrato_Codigo), 6, 0)));
         }
         wcpOAV7Contrato_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV7Contrato_Codigo"), ",", "."));
         if ( ! GetJustCreated( ) && ( ( AV7Contrato_Codigo != wcpOAV7Contrato_Codigo ) ) )
         {
            setjustcreated();
         }
         wcpOAV7Contrato_Codigo = AV7Contrato_Codigo;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlAV7Contrato_Codigo = cgiGet( sPrefix+"AV7Contrato_Codigo_CTRL");
         if ( StringUtil.Len( sCtrlAV7Contrato_Codigo) > 0 )
         {
            AV7Contrato_Codigo = (int)(context.localUtil.CToN( cgiGet( sCtrlAV7Contrato_Codigo), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Contrato_Codigo), 6, 0)));
         }
         else
         {
            AV7Contrato_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"AV7Contrato_Codigo_PARM"), ",", "."));
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PAM32( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WSM32( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WSM32( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"AV7Contrato_Codigo_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7Contrato_Codigo), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlAV7Contrato_Codigo)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"AV7Contrato_Codigo_CTRL", StringUtil.RTrim( sCtrlAV7Contrato_Codigo));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WEM32( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?2020521182235");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         if ( nGXWrapped != 1 )
         {
            context.AddJavascriptSource("wc_contratohistoricoconsumo.js", "?2020521182235");
            context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         }
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_142( )
      {
         edtHistoricoConsumo_Codigo_Internalname = sPrefix+"HISTORICOCONSUMO_CODIGO_"+sGXsfl_14_idx;
         edtHistoricoConsumo_Data_Internalname = sPrefix+"HISTORICOCONSUMO_DATA_"+sGXsfl_14_idx;
         cmbHistoricoConsumo_HistoricoCodigo_Internalname = sPrefix+"HISTORICOCONSUMO_HISTORICOCODIGO_"+sGXsfl_14_idx;
         edtHistoricoConsumo_SaldoContratoCod_Internalname = sPrefix+"HISTORICOCONSUMO_SALDOCONTRATOCOD_"+sGXsfl_14_idx;
         edtHistoricoConsumo_SaldoContratoDtaVigIni_Internalname = sPrefix+"HISTORICOCONSUMO_SALDOCONTRATODTAVIGINI_"+sGXsfl_14_idx;
         edtHistoricoConsumo_SaldoContratoDtaVigFim_Internalname = sPrefix+"HISTORICOCONSUMO_SALDOCONTRATODTAVIGFIM_"+sGXsfl_14_idx;
         edtHistoricoConsumo_NotaEmpenhoCod_Internalname = sPrefix+"HISTORICOCONSUMO_NOTAEMPENHOCOD_"+sGXsfl_14_idx;
         edtavNotaempenho_itentificador_Internalname = sPrefix+"vNOTAEMPENHO_ITENTIFICADOR_"+sGXsfl_14_idx;
         edtavNotaempenho_demissao_Internalname = sPrefix+"vNOTAEMPENHO_DEMISSAO_"+sGXsfl_14_idx;
         edtHistoricoConsumo_ContagemResultadoCod_Internalname = sPrefix+"HISTORICOCONSUMO_CONTAGEMRESULTADOCOD_"+sGXsfl_14_idx;
         edtHistoricoConsumo_ContratoCod_Internalname = sPrefix+"HISTORICOCONSUMO_CONTRATOCOD_"+sGXsfl_14_idx;
         edtHistoricoConsumo_Valor_Internalname = sPrefix+"HISTORICOCONSUMO_VALOR_"+sGXsfl_14_idx;
         edtHistoricoConsumo_UsuarioCod_Internalname = sPrefix+"HISTORICOCONSUMO_USUARIOCOD_"+sGXsfl_14_idx;
         edtavUsuario_nome_Internalname = sPrefix+"vUSUARIO_NOME_"+sGXsfl_14_idx;
      }

      protected void SubsflControlProps_fel_142( )
      {
         edtHistoricoConsumo_Codigo_Internalname = sPrefix+"HISTORICOCONSUMO_CODIGO_"+sGXsfl_14_fel_idx;
         edtHistoricoConsumo_Data_Internalname = sPrefix+"HISTORICOCONSUMO_DATA_"+sGXsfl_14_fel_idx;
         cmbHistoricoConsumo_HistoricoCodigo_Internalname = sPrefix+"HISTORICOCONSUMO_HISTORICOCODIGO_"+sGXsfl_14_fel_idx;
         edtHistoricoConsumo_SaldoContratoCod_Internalname = sPrefix+"HISTORICOCONSUMO_SALDOCONTRATOCOD_"+sGXsfl_14_fel_idx;
         edtHistoricoConsumo_SaldoContratoDtaVigIni_Internalname = sPrefix+"HISTORICOCONSUMO_SALDOCONTRATODTAVIGINI_"+sGXsfl_14_fel_idx;
         edtHistoricoConsumo_SaldoContratoDtaVigFim_Internalname = sPrefix+"HISTORICOCONSUMO_SALDOCONTRATODTAVIGFIM_"+sGXsfl_14_fel_idx;
         edtHistoricoConsumo_NotaEmpenhoCod_Internalname = sPrefix+"HISTORICOCONSUMO_NOTAEMPENHOCOD_"+sGXsfl_14_fel_idx;
         edtavNotaempenho_itentificador_Internalname = sPrefix+"vNOTAEMPENHO_ITENTIFICADOR_"+sGXsfl_14_fel_idx;
         edtavNotaempenho_demissao_Internalname = sPrefix+"vNOTAEMPENHO_DEMISSAO_"+sGXsfl_14_fel_idx;
         edtHistoricoConsumo_ContagemResultadoCod_Internalname = sPrefix+"HISTORICOCONSUMO_CONTAGEMRESULTADOCOD_"+sGXsfl_14_fel_idx;
         edtHistoricoConsumo_ContratoCod_Internalname = sPrefix+"HISTORICOCONSUMO_CONTRATOCOD_"+sGXsfl_14_fel_idx;
         edtHistoricoConsumo_Valor_Internalname = sPrefix+"HISTORICOCONSUMO_VALOR_"+sGXsfl_14_fel_idx;
         edtHistoricoConsumo_UsuarioCod_Internalname = sPrefix+"HISTORICOCONSUMO_USUARIOCOD_"+sGXsfl_14_fel_idx;
         edtavUsuario_nome_Internalname = sPrefix+"vUSUARIO_NOME_"+sGXsfl_14_fel_idx;
      }

      protected void sendrow_142( )
      {
         SubsflControlProps_142( ) ;
         WBM30( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_14_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_14_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_14_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtHistoricoConsumo_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A1562HistoricoConsumo_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A1562HistoricoConsumo_Codigo), "ZZZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtHistoricoConsumo_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)14,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtHistoricoConsumo_Data_Internalname,context.localUtil.TToC( A1577HistoricoConsumo_Data, 10, 8, 0, 3, "/", ":", " "),context.localUtil.Format( A1577HistoricoConsumo_Data, "99/99/99 99:99"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtHistoricoConsumo_Data_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)14,(short)0,(short)0,(short)14,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            if ( ( nGXsfl_14_idx == 1 ) && isAjaxCallMode( ) )
            {
               GXCCtl = "HISTORICOCONSUMO_HISTORICOCODIGO_" + sGXsfl_14_idx;
               cmbHistoricoConsumo_HistoricoCodigo.Name = GXCCtl;
               cmbHistoricoConsumo_HistoricoCodigo.WebTags = "";
               cmbHistoricoConsumo_HistoricoCodigo.addItem("CRI", "Cria��o", 0);
               cmbHistoricoConsumo_HistoricoCodigo.addItem("ALT", "Altera��o", 0);
               cmbHistoricoConsumo_HistoricoCodigo.addItem("CRE", "Cr�dito", 0);
               cmbHistoricoConsumo_HistoricoCodigo.addItem("DEB", "D�bito", 0);
               cmbHistoricoConsumo_HistoricoCodigo.addItem("INI", "Inicializar", 0);
               cmbHistoricoConsumo_HistoricoCodigo.addItem("RES", "Reservar", 0);
               cmbHistoricoConsumo_HistoricoCodigo.addItem("EXE", "Executado", 0);
               cmbHistoricoConsumo_HistoricoCodigo.addItem("CRS", "Cancela Reservado", 0);
               if ( cmbHistoricoConsumo_HistoricoCodigo.ItemCount > 0 )
               {
                  A1646HistoricoConsumo_HistoricoCodigo = cmbHistoricoConsumo_HistoricoCodigo.getValidValue(A1646HistoricoConsumo_HistoricoCodigo);
               }
            }
            /* ComboBox */
            GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbHistoricoConsumo_HistoricoCodigo,(String)cmbHistoricoConsumo_HistoricoCodigo_Internalname,StringUtil.RTrim( A1646HistoricoConsumo_HistoricoCodigo),(short)1,(String)cmbHistoricoConsumo_HistoricoCodigo_Jsonclick,(short)0,(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"char",(String)"",(short)-1,(short)0,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"Attribute",(String)"",(String)"",(String)"",(bool)true});
            cmbHistoricoConsumo_HistoricoCodigo.CurrentValue = StringUtil.RTrim( A1646HistoricoConsumo_HistoricoCodigo);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbHistoricoConsumo_HistoricoCodigo_Internalname, "Values", (String)(cmbHistoricoConsumo_HistoricoCodigo.ToJavascriptSource()));
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtHistoricoConsumo_SaldoContratoCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A1580HistoricoConsumo_SaldoContratoCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A1580HistoricoConsumo_SaldoContratoCod), "ZZZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtHistoricoConsumo_SaldoContratoCod_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)14,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "Attribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtHistoricoConsumo_SaldoContratoDtaVigIni_Internalname,context.localUtil.Format(A1644HistoricoConsumo_SaldoContratoDtaVigIni, "99/99/99"),context.localUtil.Format( A1644HistoricoConsumo_SaldoContratoDtaVigIni, "99/99/99"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtHistoricoConsumo_SaldoContratoDtaVigIni_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)8,(short)0,(short)0,(short)14,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "Attribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtHistoricoConsumo_SaldoContratoDtaVigFim_Internalname,context.localUtil.Format(A1645HistoricoConsumo_SaldoContratoDtaVigFim, "99/99/99"),context.localUtil.Format( A1645HistoricoConsumo_SaldoContratoDtaVigFim, "99/99/99"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtHistoricoConsumo_SaldoContratoDtaVigFim_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)8,(short)0,(short)0,(short)14,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtHistoricoConsumo_NotaEmpenhoCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A1581HistoricoConsumo_NotaEmpenhoCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A1581HistoricoConsumo_NotaEmpenhoCod), "ZZZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtHistoricoConsumo_NotaEmpenhoCod_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)14,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavNotaempenho_itentificador_Internalname,StringUtil.RTrim( AV6NotaEmpenho_Itentificador),(String)"",(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavNotaempenho_itentificador_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavNotaempenho_itentificador_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)15,(short)0,(short)0,(short)14,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavNotaempenho_demissao_Internalname,context.localUtil.TToC( AV5NotaEmpenho_DEmissao, 10, 8, 0, 3, "/", ":", " "),context.localUtil.Format( AV5NotaEmpenho_DEmissao, "99/99/99 99:99"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavNotaempenho_demissao_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(int)edtavNotaempenho_demissao_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)14,(short)0,(short)0,(short)14,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtHistoricoConsumo_ContagemResultadoCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A1582HistoricoConsumo_ContagemResultadoCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A1582HistoricoConsumo_ContagemResultadoCod), "ZZZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtHistoricoConsumo_ContagemResultadoCod_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)14,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtHistoricoConsumo_ContratoCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A1579HistoricoConsumo_ContratoCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A1579HistoricoConsumo_ContratoCod), "ZZZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtHistoricoConsumo_ContratoCod_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)14,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtHistoricoConsumo_Valor_Internalname,StringUtil.LTrim( StringUtil.NToC( A1578HistoricoConsumo_Valor, 18, 5, ",", "")),context.localUtil.Format( A1578HistoricoConsumo_Valor, "ZZZ,ZZZ,ZZZ,ZZ9.99"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtHistoricoConsumo_Valor_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)18,(short)0,(short)0,(short)14,(short)1,(short)-1,(short)0,(bool)true,(String)"Valor",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtHistoricoConsumo_UsuarioCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A1563HistoricoConsumo_UsuarioCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A1563HistoricoConsumo_UsuarioCod), "ZZZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtHistoricoConsumo_UsuarioCod_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)14,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "Attribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavUsuario_nome_Internalname,StringUtil.RTrim( AV18Usuario_Nome),StringUtil.RTrim( context.localUtil.Format( AV18Usuario_Nome, "@!")),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavUsuario_nome_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)14,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_HISTORICOCONSUMO_CODIGO"+"_"+sGXsfl_14_idx, GetSecureSignedToken( sPrefix+sGXsfl_14_idx, context.localUtil.Format( (decimal)(A1562HistoricoConsumo_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_HISTORICOCONSUMO_DATA"+"_"+sGXsfl_14_idx, GetSecureSignedToken( sPrefix+sGXsfl_14_idx, context.localUtil.Format( A1577HistoricoConsumo_Data, "99/99/99 99:99")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_HISTORICOCONSUMO_HISTORICOCODIGO"+"_"+sGXsfl_14_idx, GetSecureSignedToken( sPrefix+sGXsfl_14_idx, StringUtil.RTrim( context.localUtil.Format( A1646HistoricoConsumo_HistoricoCodigo, ""))));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_HISTORICOCONSUMO_SALDOCONTRATOCOD"+"_"+sGXsfl_14_idx, GetSecureSignedToken( sPrefix+sGXsfl_14_idx, context.localUtil.Format( (decimal)(A1580HistoricoConsumo_SaldoContratoCod), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_HISTORICOCONSUMO_SALDOCONTRATODTAVIGINI"+"_"+sGXsfl_14_idx, GetSecureSignedToken( sPrefix+sGXsfl_14_idx, A1644HistoricoConsumo_SaldoContratoDtaVigIni));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_HISTORICOCONSUMO_SALDOCONTRATODTAVIGFIM"+"_"+sGXsfl_14_idx, GetSecureSignedToken( sPrefix+sGXsfl_14_idx, A1645HistoricoConsumo_SaldoContratoDtaVigFim));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_HISTORICOCONSUMO_NOTAEMPENHOCOD"+"_"+sGXsfl_14_idx, GetSecureSignedToken( sPrefix+sGXsfl_14_idx, context.localUtil.Format( (decimal)(A1581HistoricoConsumo_NotaEmpenhoCod), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_HISTORICOCONSUMO_CONTAGEMRESULTADOCOD"+"_"+sGXsfl_14_idx, GetSecureSignedToken( sPrefix+sGXsfl_14_idx, context.localUtil.Format( (decimal)(A1582HistoricoConsumo_ContagemResultadoCod), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_HISTORICOCONSUMO_CONTRATOCOD"+"_"+sGXsfl_14_idx, GetSecureSignedToken( sPrefix+sGXsfl_14_idx, context.localUtil.Format( (decimal)(A1579HistoricoConsumo_ContratoCod), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_HISTORICOCONSUMO_VALOR"+"_"+sGXsfl_14_idx, GetSecureSignedToken( sPrefix+sGXsfl_14_idx, context.localUtil.Format( A1578HistoricoConsumo_Valor, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_HISTORICOCONSUMO_USUARIOCOD"+"_"+sGXsfl_14_idx, GetSecureSignedToken( sPrefix+sGXsfl_14_idx, context.localUtil.Format( (decimal)(A1563HistoricoConsumo_UsuarioCod), "ZZZZZ9")));
            GridContainer.AddRow(GridRow);
            nGXsfl_14_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_14_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_14_idx+1));
            sGXsfl_14_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_14_idx), 4, 0)), 4, "0");
            SubsflControlProps_142( ) ;
         }
         /* End function sendrow_142 */
      }

      protected void init_default_properties( )
      {
         tblTablesearch_Internalname = sPrefix+"TABLESEARCH";
         edtHistoricoConsumo_Codigo_Internalname = sPrefix+"HISTORICOCONSUMO_CODIGO";
         edtHistoricoConsumo_Data_Internalname = sPrefix+"HISTORICOCONSUMO_DATA";
         cmbHistoricoConsumo_HistoricoCodigo_Internalname = sPrefix+"HISTORICOCONSUMO_HISTORICOCODIGO";
         edtHistoricoConsumo_SaldoContratoCod_Internalname = sPrefix+"HISTORICOCONSUMO_SALDOCONTRATOCOD";
         edtHistoricoConsumo_SaldoContratoDtaVigIni_Internalname = sPrefix+"HISTORICOCONSUMO_SALDOCONTRATODTAVIGINI";
         edtHistoricoConsumo_SaldoContratoDtaVigFim_Internalname = sPrefix+"HISTORICOCONSUMO_SALDOCONTRATODTAVIGFIM";
         edtHistoricoConsumo_NotaEmpenhoCod_Internalname = sPrefix+"HISTORICOCONSUMO_NOTAEMPENHOCOD";
         edtavNotaempenho_itentificador_Internalname = sPrefix+"vNOTAEMPENHO_ITENTIFICADOR";
         edtavNotaempenho_demissao_Internalname = sPrefix+"vNOTAEMPENHO_DEMISSAO";
         edtHistoricoConsumo_ContagemResultadoCod_Internalname = sPrefix+"HISTORICOCONSUMO_CONTAGEMRESULTADOCOD";
         edtHistoricoConsumo_ContratoCod_Internalname = sPrefix+"HISTORICOCONSUMO_CONTRATOCOD";
         edtHistoricoConsumo_Valor_Internalname = sPrefix+"HISTORICOCONSUMO_VALOR";
         edtHistoricoConsumo_UsuarioCod_Internalname = sPrefix+"HISTORICOCONSUMO_USUARIOCOD";
         edtavUsuario_nome_Internalname = sPrefix+"vUSUARIO_NOME";
         tblTablegridheader_Internalname = sPrefix+"TABLEGRIDHEADER";
         Workwithplusutilities1_Internalname = sPrefix+"WORKWITHPLUSUTILITIES1";
         edtavOrderedby_Internalname = sPrefix+"vORDEREDBY";
         edtavOrdereddsc_Internalname = sPrefix+"vORDEREDDSC";
         Form.Internalname = sPrefix+"FORM";
         subGrid_Internalname = sPrefix+"GRID";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         edtavUsuario_nome_Jsonclick = "";
         edtHistoricoConsumo_UsuarioCod_Jsonclick = "";
         edtHistoricoConsumo_Valor_Jsonclick = "";
         edtHistoricoConsumo_ContratoCod_Jsonclick = "";
         edtHistoricoConsumo_ContagemResultadoCod_Jsonclick = "";
         edtavNotaempenho_demissao_Jsonclick = "";
         edtavNotaempenho_itentificador_Jsonclick = "";
         edtHistoricoConsumo_NotaEmpenhoCod_Jsonclick = "";
         edtHistoricoConsumo_SaldoContratoDtaVigFim_Jsonclick = "";
         edtHistoricoConsumo_SaldoContratoDtaVigIni_Jsonclick = "";
         edtHistoricoConsumo_SaldoContratoCod_Jsonclick = "";
         cmbHistoricoConsumo_HistoricoCodigo_Jsonclick = "";
         edtHistoricoConsumo_Data_Jsonclick = "";
         edtHistoricoConsumo_Codigo_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtavNotaempenho_demissao_Enabled = 0;
         edtavNotaempenho_itentificador_Enabled = 0;
         edtHistoricoConsumo_UsuarioCod_Titleformat = 0;
         edtHistoricoConsumo_Valor_Titleformat = 0;
         edtHistoricoConsumo_ContagemResultadoCod_Titleformat = 0;
         edtHistoricoConsumo_NotaEmpenhoCod_Titleformat = 0;
         edtHistoricoConsumo_SaldoContratoCod_Titleformat = 0;
         edtHistoricoConsumo_Data_Titleformat = 0;
         edtHistoricoConsumo_Codigo_Titleformat = 0;
         subGrid_Class = "WorkWith";
         edtHistoricoConsumo_UsuarioCod_Title = "Usu�rio";
         edtHistoricoConsumo_Valor_Title = "Valor";
         edtHistoricoConsumo_Data_Title = "Data";
         edtHistoricoConsumo_ContagemResultadoCod_Title = "Contagem Codigo";
         edtHistoricoConsumo_NotaEmpenhoCod_Title = "Nota Empenho";
         edtHistoricoConsumo_SaldoContratoCod_Title = "Saldo Contrato";
         edtHistoricoConsumo_Codigo_Title = "Historico Consumo";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         edtavOrdereddsc_Jsonclick = "";
         edtavOrdereddsc_Visible = 1;
         edtavOrderedby_Jsonclick = "";
         edtavOrderedby_Visible = 1;
         subGrid_Rows = 0;
         context.GX_msglist.DisplayMode = 1;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV7Contrato_Codigo',fld:'vCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1560NotaEmpenho_Codigo',fld:'NOTAEMPENHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1581HistoricoConsumo_NotaEmpenhoCod',fld:'HISTORICOCONSUMO_NOTAEMPENHOCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1564NotaEmpenho_Itentificador',fld:'NOTAEMPENHO_ITENTIFICADOR',pic:'',nv:''},{av:'A1565NotaEmpenho_DEmissao',fld:'NOTAEMPENHO_DEMISSAO',pic:'99/99/99 99:99',nv:''},{av:'sPrefix',nv:''},{av:'AV12OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV22Pgmname',fld:'vPGMNAME',pic:'',nv:''}],oparms:[{av:'edtHistoricoConsumo_Codigo_Titleformat',ctrl:'HISTORICOCONSUMO_CODIGO',prop:'Titleformat'},{av:'edtHistoricoConsumo_Codigo_Title',ctrl:'HISTORICOCONSUMO_CODIGO',prop:'Title'},{av:'edtHistoricoConsumo_SaldoContratoCod_Titleformat',ctrl:'HISTORICOCONSUMO_SALDOCONTRATOCOD',prop:'Titleformat'},{av:'edtHistoricoConsumo_SaldoContratoCod_Title',ctrl:'HISTORICOCONSUMO_SALDOCONTRATOCOD',prop:'Title'},{av:'edtHistoricoConsumo_NotaEmpenhoCod_Titleformat',ctrl:'HISTORICOCONSUMO_NOTAEMPENHOCOD',prop:'Titleformat'},{av:'edtHistoricoConsumo_NotaEmpenhoCod_Title',ctrl:'HISTORICOCONSUMO_NOTAEMPENHOCOD',prop:'Title'},{av:'edtHistoricoConsumo_ContagemResultadoCod_Titleformat',ctrl:'HISTORICOCONSUMO_CONTAGEMRESULTADOCOD',prop:'Titleformat'},{av:'edtHistoricoConsumo_ContagemResultadoCod_Title',ctrl:'HISTORICOCONSUMO_CONTAGEMRESULTADOCOD',prop:'Title'},{av:'edtHistoricoConsumo_Data_Titleformat',ctrl:'HISTORICOCONSUMO_DATA',prop:'Titleformat'},{av:'edtHistoricoConsumo_Data_Title',ctrl:'HISTORICOCONSUMO_DATA',prop:'Title'},{av:'edtHistoricoConsumo_Valor_Titleformat',ctrl:'HISTORICOCONSUMO_VALOR',prop:'Titleformat'},{av:'edtHistoricoConsumo_Valor_Title',ctrl:'HISTORICOCONSUMO_VALOR',prop:'Title'},{av:'edtHistoricoConsumo_UsuarioCod_Titleformat',ctrl:'HISTORICOCONSUMO_USUARIOCOD',prop:'Titleformat'},{av:'edtHistoricoConsumo_UsuarioCod_Title',ctrl:'HISTORICOCONSUMO_USUARIOCOD',prop:'Title'}]}");
         setEventMetadata("GRID.LOAD","{handler:'E13M32',iparms:[{av:'A1560NotaEmpenho_Codigo',fld:'NOTAEMPENHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1581HistoricoConsumo_NotaEmpenhoCod',fld:'HISTORICOCONSUMO_NOTAEMPENHOCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1564NotaEmpenho_Itentificador',fld:'NOTAEMPENHO_ITENTIFICADOR',pic:'',nv:''},{av:'A1565NotaEmpenho_DEmissao',fld:'NOTAEMPENHO_DEMISSAO',pic:'99/99/99 99:99',nv:''}],oparms:[{av:'AV18Usuario_Nome',fld:'vUSUARIO_NOME',pic:'@!',nv:''},{av:'AV6NotaEmpenho_Itentificador',fld:'vNOTAEMPENHO_ITENTIFICADOR',pic:'',nv:''},{av:'AV5NotaEmpenho_DEmissao',fld:'vNOTAEMPENHO_DEMISSAO',pic:'99/99/99 99:99',nv:''}]}");
         setEventMetadata("GRID_FIRSTPAGE","{handler:'subgrid_firstpage',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV7Contrato_Codigo',fld:'vCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1560NotaEmpenho_Codigo',fld:'NOTAEMPENHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1581HistoricoConsumo_NotaEmpenhoCod',fld:'HISTORICOCONSUMO_NOTAEMPENHOCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1564NotaEmpenho_Itentificador',fld:'NOTAEMPENHO_ITENTIFICADOR',pic:'',nv:''},{av:'A1565NotaEmpenho_DEmissao',fld:'NOTAEMPENHO_DEMISSAO',pic:'99/99/99 99:99',nv:''},{av:'sPrefix',nv:''},{av:'AV12OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV22Pgmname',fld:'vPGMNAME',pic:'',nv:''}],oparms:[{av:'edtHistoricoConsumo_Codigo_Titleformat',ctrl:'HISTORICOCONSUMO_CODIGO',prop:'Titleformat'},{av:'edtHistoricoConsumo_Codigo_Title',ctrl:'HISTORICOCONSUMO_CODIGO',prop:'Title'},{av:'edtHistoricoConsumo_SaldoContratoCod_Titleformat',ctrl:'HISTORICOCONSUMO_SALDOCONTRATOCOD',prop:'Titleformat'},{av:'edtHistoricoConsumo_SaldoContratoCod_Title',ctrl:'HISTORICOCONSUMO_SALDOCONTRATOCOD',prop:'Title'},{av:'edtHistoricoConsumo_NotaEmpenhoCod_Titleformat',ctrl:'HISTORICOCONSUMO_NOTAEMPENHOCOD',prop:'Titleformat'},{av:'edtHistoricoConsumo_NotaEmpenhoCod_Title',ctrl:'HISTORICOCONSUMO_NOTAEMPENHOCOD',prop:'Title'},{av:'edtHistoricoConsumo_ContagemResultadoCod_Titleformat',ctrl:'HISTORICOCONSUMO_CONTAGEMRESULTADOCOD',prop:'Titleformat'},{av:'edtHistoricoConsumo_ContagemResultadoCod_Title',ctrl:'HISTORICOCONSUMO_CONTAGEMRESULTADOCOD',prop:'Title'},{av:'edtHistoricoConsumo_Data_Titleformat',ctrl:'HISTORICOCONSUMO_DATA',prop:'Titleformat'},{av:'edtHistoricoConsumo_Data_Title',ctrl:'HISTORICOCONSUMO_DATA',prop:'Title'},{av:'edtHistoricoConsumo_Valor_Titleformat',ctrl:'HISTORICOCONSUMO_VALOR',prop:'Titleformat'},{av:'edtHistoricoConsumo_Valor_Title',ctrl:'HISTORICOCONSUMO_VALOR',prop:'Title'},{av:'edtHistoricoConsumo_UsuarioCod_Titleformat',ctrl:'HISTORICOCONSUMO_USUARIOCOD',prop:'Titleformat'},{av:'edtHistoricoConsumo_UsuarioCod_Title',ctrl:'HISTORICOCONSUMO_USUARIOCOD',prop:'Title'}]}");
         setEventMetadata("GRID_PREVPAGE","{handler:'subgrid_previouspage',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV7Contrato_Codigo',fld:'vCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1560NotaEmpenho_Codigo',fld:'NOTAEMPENHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1581HistoricoConsumo_NotaEmpenhoCod',fld:'HISTORICOCONSUMO_NOTAEMPENHOCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1564NotaEmpenho_Itentificador',fld:'NOTAEMPENHO_ITENTIFICADOR',pic:'',nv:''},{av:'A1565NotaEmpenho_DEmissao',fld:'NOTAEMPENHO_DEMISSAO',pic:'99/99/99 99:99',nv:''},{av:'sPrefix',nv:''},{av:'AV12OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV22Pgmname',fld:'vPGMNAME',pic:'',nv:''}],oparms:[{av:'edtHistoricoConsumo_Codigo_Titleformat',ctrl:'HISTORICOCONSUMO_CODIGO',prop:'Titleformat'},{av:'edtHistoricoConsumo_Codigo_Title',ctrl:'HISTORICOCONSUMO_CODIGO',prop:'Title'},{av:'edtHistoricoConsumo_SaldoContratoCod_Titleformat',ctrl:'HISTORICOCONSUMO_SALDOCONTRATOCOD',prop:'Titleformat'},{av:'edtHistoricoConsumo_SaldoContratoCod_Title',ctrl:'HISTORICOCONSUMO_SALDOCONTRATOCOD',prop:'Title'},{av:'edtHistoricoConsumo_NotaEmpenhoCod_Titleformat',ctrl:'HISTORICOCONSUMO_NOTAEMPENHOCOD',prop:'Titleformat'},{av:'edtHistoricoConsumo_NotaEmpenhoCod_Title',ctrl:'HISTORICOCONSUMO_NOTAEMPENHOCOD',prop:'Title'},{av:'edtHistoricoConsumo_ContagemResultadoCod_Titleformat',ctrl:'HISTORICOCONSUMO_CONTAGEMRESULTADOCOD',prop:'Titleformat'},{av:'edtHistoricoConsumo_ContagemResultadoCod_Title',ctrl:'HISTORICOCONSUMO_CONTAGEMRESULTADOCOD',prop:'Title'},{av:'edtHistoricoConsumo_Data_Titleformat',ctrl:'HISTORICOCONSUMO_DATA',prop:'Titleformat'},{av:'edtHistoricoConsumo_Data_Title',ctrl:'HISTORICOCONSUMO_DATA',prop:'Title'},{av:'edtHistoricoConsumo_Valor_Titleformat',ctrl:'HISTORICOCONSUMO_VALOR',prop:'Titleformat'},{av:'edtHistoricoConsumo_Valor_Title',ctrl:'HISTORICOCONSUMO_VALOR',prop:'Title'},{av:'edtHistoricoConsumo_UsuarioCod_Titleformat',ctrl:'HISTORICOCONSUMO_USUARIOCOD',prop:'Titleformat'},{av:'edtHistoricoConsumo_UsuarioCod_Title',ctrl:'HISTORICOCONSUMO_USUARIOCOD',prop:'Title'}]}");
         setEventMetadata("GRID_NEXTPAGE","{handler:'subgrid_nextpage',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV7Contrato_Codigo',fld:'vCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1560NotaEmpenho_Codigo',fld:'NOTAEMPENHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1581HistoricoConsumo_NotaEmpenhoCod',fld:'HISTORICOCONSUMO_NOTAEMPENHOCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1564NotaEmpenho_Itentificador',fld:'NOTAEMPENHO_ITENTIFICADOR',pic:'',nv:''},{av:'A1565NotaEmpenho_DEmissao',fld:'NOTAEMPENHO_DEMISSAO',pic:'99/99/99 99:99',nv:''},{av:'sPrefix',nv:''},{av:'AV12OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV22Pgmname',fld:'vPGMNAME',pic:'',nv:''}],oparms:[{av:'edtHistoricoConsumo_Codigo_Titleformat',ctrl:'HISTORICOCONSUMO_CODIGO',prop:'Titleformat'},{av:'edtHistoricoConsumo_Codigo_Title',ctrl:'HISTORICOCONSUMO_CODIGO',prop:'Title'},{av:'edtHistoricoConsumo_SaldoContratoCod_Titleformat',ctrl:'HISTORICOCONSUMO_SALDOCONTRATOCOD',prop:'Titleformat'},{av:'edtHistoricoConsumo_SaldoContratoCod_Title',ctrl:'HISTORICOCONSUMO_SALDOCONTRATOCOD',prop:'Title'},{av:'edtHistoricoConsumo_NotaEmpenhoCod_Titleformat',ctrl:'HISTORICOCONSUMO_NOTAEMPENHOCOD',prop:'Titleformat'},{av:'edtHistoricoConsumo_NotaEmpenhoCod_Title',ctrl:'HISTORICOCONSUMO_NOTAEMPENHOCOD',prop:'Title'},{av:'edtHistoricoConsumo_ContagemResultadoCod_Titleformat',ctrl:'HISTORICOCONSUMO_CONTAGEMRESULTADOCOD',prop:'Titleformat'},{av:'edtHistoricoConsumo_ContagemResultadoCod_Title',ctrl:'HISTORICOCONSUMO_CONTAGEMRESULTADOCOD',prop:'Title'},{av:'edtHistoricoConsumo_Data_Titleformat',ctrl:'HISTORICOCONSUMO_DATA',prop:'Titleformat'},{av:'edtHistoricoConsumo_Data_Title',ctrl:'HISTORICOCONSUMO_DATA',prop:'Title'},{av:'edtHistoricoConsumo_Valor_Titleformat',ctrl:'HISTORICOCONSUMO_VALOR',prop:'Titleformat'},{av:'edtHistoricoConsumo_Valor_Title',ctrl:'HISTORICOCONSUMO_VALOR',prop:'Title'},{av:'edtHistoricoConsumo_UsuarioCod_Titleformat',ctrl:'HISTORICOCONSUMO_USUARIOCOD',prop:'Titleformat'},{av:'edtHistoricoConsumo_UsuarioCod_Title',ctrl:'HISTORICOCONSUMO_USUARIOCOD',prop:'Title'}]}");
         setEventMetadata("GRID_LASTPAGE","{handler:'subgrid_lastpage',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV7Contrato_Codigo',fld:'vCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1560NotaEmpenho_Codigo',fld:'NOTAEMPENHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1581HistoricoConsumo_NotaEmpenhoCod',fld:'HISTORICOCONSUMO_NOTAEMPENHOCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1564NotaEmpenho_Itentificador',fld:'NOTAEMPENHO_ITENTIFICADOR',pic:'',nv:''},{av:'A1565NotaEmpenho_DEmissao',fld:'NOTAEMPENHO_DEMISSAO',pic:'99/99/99 99:99',nv:''},{av:'sPrefix',nv:''},{av:'AV12OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV13OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV22Pgmname',fld:'vPGMNAME',pic:'',nv:''}],oparms:[{av:'edtHistoricoConsumo_Codigo_Titleformat',ctrl:'HISTORICOCONSUMO_CODIGO',prop:'Titleformat'},{av:'edtHistoricoConsumo_Codigo_Title',ctrl:'HISTORICOCONSUMO_CODIGO',prop:'Title'},{av:'edtHistoricoConsumo_SaldoContratoCod_Titleformat',ctrl:'HISTORICOCONSUMO_SALDOCONTRATOCOD',prop:'Titleformat'},{av:'edtHistoricoConsumo_SaldoContratoCod_Title',ctrl:'HISTORICOCONSUMO_SALDOCONTRATOCOD',prop:'Title'},{av:'edtHistoricoConsumo_NotaEmpenhoCod_Titleformat',ctrl:'HISTORICOCONSUMO_NOTAEMPENHOCOD',prop:'Titleformat'},{av:'edtHistoricoConsumo_NotaEmpenhoCod_Title',ctrl:'HISTORICOCONSUMO_NOTAEMPENHOCOD',prop:'Title'},{av:'edtHistoricoConsumo_ContagemResultadoCod_Titleformat',ctrl:'HISTORICOCONSUMO_CONTAGEMRESULTADOCOD',prop:'Titleformat'},{av:'edtHistoricoConsumo_ContagemResultadoCod_Title',ctrl:'HISTORICOCONSUMO_CONTAGEMRESULTADOCOD',prop:'Title'},{av:'edtHistoricoConsumo_Data_Titleformat',ctrl:'HISTORICOCONSUMO_DATA',prop:'Titleformat'},{av:'edtHistoricoConsumo_Data_Title',ctrl:'HISTORICOCONSUMO_DATA',prop:'Title'},{av:'edtHistoricoConsumo_Valor_Titleformat',ctrl:'HISTORICOCONSUMO_VALOR',prop:'Titleformat'},{av:'edtHistoricoConsumo_Valor_Title',ctrl:'HISTORICOCONSUMO_VALOR',prop:'Title'},{av:'edtHistoricoConsumo_UsuarioCod_Titleformat',ctrl:'HISTORICOCONSUMO_USUARIOCOD',prop:'Titleformat'},{av:'edtHistoricoConsumo_UsuarioCod_Title',ctrl:'HISTORICOCONSUMO_USUARIOCOD',prop:'Title'}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         AV22Pgmname = "";
         A1564NotaEmpenho_Itentificador = "";
         A1565NotaEmpenho_DEmissao = (DateTime)(DateTime.MinValue);
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         GX_FocusControl = "";
         TempTags = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         A1577HistoricoConsumo_Data = (DateTime)(DateTime.MinValue);
         A1646HistoricoConsumo_HistoricoCodigo = "";
         A1644HistoricoConsumo_SaldoContratoDtaVigIni = DateTime.MinValue;
         A1645HistoricoConsumo_SaldoContratoDtaVigFim = DateTime.MinValue;
         AV6NotaEmpenho_Itentificador = "";
         AV5NotaEmpenho_DEmissao = (DateTime)(DateTime.MinValue);
         AV18Usuario_Nome = "";
         GXCCtl = "";
         GridContainer = new GXWebGrid( context);
         scmdbuf = "";
         H00M33_A1579HistoricoConsumo_ContratoCod = new int[1] ;
         H00M33_n1579HistoricoConsumo_ContratoCod = new bool[] {false} ;
         H00M33_A1581HistoricoConsumo_NotaEmpenhoCod = new int[1] ;
         H00M33_n1581HistoricoConsumo_NotaEmpenhoCod = new bool[] {false} ;
         H00M33_A1563HistoricoConsumo_UsuarioCod = new int[1] ;
         H00M33_n1563HistoricoConsumo_UsuarioCod = new bool[] {false} ;
         H00M33_A1578HistoricoConsumo_Valor = new decimal[1] ;
         H00M33_A1582HistoricoConsumo_ContagemResultadoCod = new int[1] ;
         H00M33_n1582HistoricoConsumo_ContagemResultadoCod = new bool[] {false} ;
         H00M33_A1645HistoricoConsumo_SaldoContratoDtaVigFim = new DateTime[] {DateTime.MinValue} ;
         H00M33_A1644HistoricoConsumo_SaldoContratoDtaVigIni = new DateTime[] {DateTime.MinValue} ;
         H00M33_A1580HistoricoConsumo_SaldoContratoCod = new int[1] ;
         H00M33_A1646HistoricoConsumo_HistoricoCodigo = new String[] {""} ;
         H00M33_A1577HistoricoConsumo_Data = new DateTime[] {DateTime.MinValue} ;
         H00M33_A1562HistoricoConsumo_Codigo = new int[1] ;
         H00M33_A40000Usuario_Nome = new String[] {""} ;
         H00M33_n40000Usuario_Nome = new bool[] {false} ;
         A40000Usuario_Nome = "";
         H00M35_AGRID_nRecordCount = new long[1] ;
         AV17WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         H00M36_A1560NotaEmpenho_Codigo = new int[1] ;
         H00M36_A1564NotaEmpenho_Itentificador = new String[] {""} ;
         H00M36_n1564NotaEmpenho_Itentificador = new bool[] {false} ;
         H00M36_A1565NotaEmpenho_DEmissao = new DateTime[] {DateTime.MinValue} ;
         H00M36_n1565NotaEmpenho_DEmissao = new bool[] {false} ;
         GridRow = new GXWebRow();
         AV14Session = context.GetSession();
         AV8GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV15TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV10HTTPRequest = new GxHttpRequest( context);
         AV16TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlAV7Contrato_Codigo = "";
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wc_contratohistoricoconsumo__default(),
            new Object[][] {
                new Object[] {
               H00M33_A1579HistoricoConsumo_ContratoCod, H00M33_n1579HistoricoConsumo_ContratoCod, H00M33_A1581HistoricoConsumo_NotaEmpenhoCod, H00M33_n1581HistoricoConsumo_NotaEmpenhoCod, H00M33_A1563HistoricoConsumo_UsuarioCod, H00M33_n1563HistoricoConsumo_UsuarioCod, H00M33_A1578HistoricoConsumo_Valor, H00M33_A1582HistoricoConsumo_ContagemResultadoCod, H00M33_n1582HistoricoConsumo_ContagemResultadoCod, H00M33_A1645HistoricoConsumo_SaldoContratoDtaVigFim,
               H00M33_A1644HistoricoConsumo_SaldoContratoDtaVigIni, H00M33_A1580HistoricoConsumo_SaldoContratoCod, H00M33_A1646HistoricoConsumo_HistoricoCodigo, H00M33_A1577HistoricoConsumo_Data, H00M33_A1562HistoricoConsumo_Codigo, H00M33_A40000Usuario_Nome, H00M33_n40000Usuario_Nome
               }
               , new Object[] {
               H00M35_AGRID_nRecordCount
               }
               , new Object[] {
               H00M36_A1560NotaEmpenho_Codigo, H00M36_A1564NotaEmpenho_Itentificador, H00M36_n1564NotaEmpenho_Itentificador, H00M36_A1565NotaEmpenho_DEmissao, H00M36_n1565NotaEmpenho_DEmissao
               }
            }
         );
         AV22Pgmname = "WC_ContratoHistoricoConsumo";
         /* GeneXus formulas. */
         AV22Pgmname = "WC_ContratoHistoricoConsumo";
         context.Gx_err = 0;
         edtavNotaempenho_itentificador_Enabled = 0;
         edtavNotaempenho_demissao_Enabled = 0;
      }

      private short nRcdExists_3 ;
      private short nIsMod_3 ;
      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short nRC_GXsfl_14 ;
      private short nGXsfl_14_idx=1 ;
      private short AV12OrderedBy ;
      private short initialized ;
      private short nGXWrapped ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDraw ;
      private short nDoneStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_14_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short edtHistoricoConsumo_Codigo_Titleformat ;
      private short edtHistoricoConsumo_SaldoContratoCod_Titleformat ;
      private short edtHistoricoConsumo_NotaEmpenhoCod_Titleformat ;
      private short edtHistoricoConsumo_ContagemResultadoCod_Titleformat ;
      private short edtHistoricoConsumo_Data_Titleformat ;
      private short edtHistoricoConsumo_Valor_Titleformat ;
      private short edtHistoricoConsumo_UsuarioCod_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short subGrid_Backstyle ;
      private int AV7Contrato_Codigo ;
      private int wcpOAV7Contrato_Codigo ;
      private int subGrid_Rows ;
      private int A1560NotaEmpenho_Codigo ;
      private int A1581HistoricoConsumo_NotaEmpenhoCod ;
      private int edtavNotaempenho_itentificador_Enabled ;
      private int edtavNotaempenho_demissao_Enabled ;
      private int edtavOrderedby_Visible ;
      private int edtavOrdereddsc_Visible ;
      private int A1562HistoricoConsumo_Codigo ;
      private int A1580HistoricoConsumo_SaldoContratoCod ;
      private int A1582HistoricoConsumo_ContagemResultadoCod ;
      private int A1579HistoricoConsumo_ContratoCod ;
      private int A1563HistoricoConsumo_UsuarioCod ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private long GRID_nFirstRecordOnPage ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private decimal A1578HistoricoConsumo_Valor ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String sGXsfl_14_idx="0001" ;
      private String AV22Pgmname ;
      private String A1564NotaEmpenho_Itentificador ;
      private String GXKey ;
      private String edtavNotaempenho_itentificador_Internalname ;
      private String edtavNotaempenho_demissao_Internalname ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String GX_FocusControl ;
      private String TempTags ;
      private String edtavOrderedby_Internalname ;
      private String edtavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Internalname ;
      private String edtavOrdereddsc_Jsonclick ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtHistoricoConsumo_Codigo_Internalname ;
      private String edtHistoricoConsumo_Data_Internalname ;
      private String cmbHistoricoConsumo_HistoricoCodigo_Internalname ;
      private String A1646HistoricoConsumo_HistoricoCodigo ;
      private String edtHistoricoConsumo_SaldoContratoCod_Internalname ;
      private String edtHistoricoConsumo_SaldoContratoDtaVigIni_Internalname ;
      private String edtHistoricoConsumo_SaldoContratoDtaVigFim_Internalname ;
      private String edtHistoricoConsumo_NotaEmpenhoCod_Internalname ;
      private String AV6NotaEmpenho_Itentificador ;
      private String edtHistoricoConsumo_ContagemResultadoCod_Internalname ;
      private String edtHistoricoConsumo_ContratoCod_Internalname ;
      private String edtHistoricoConsumo_Valor_Internalname ;
      private String edtHistoricoConsumo_UsuarioCod_Internalname ;
      private String AV18Usuario_Nome ;
      private String edtavUsuario_nome_Internalname ;
      private String GXCCtl ;
      private String scmdbuf ;
      private String A40000Usuario_Nome ;
      private String edtHistoricoConsumo_Codigo_Title ;
      private String edtHistoricoConsumo_SaldoContratoCod_Title ;
      private String edtHistoricoConsumo_NotaEmpenhoCod_Title ;
      private String edtHistoricoConsumo_ContagemResultadoCod_Title ;
      private String edtHistoricoConsumo_Data_Title ;
      private String edtHistoricoConsumo_Valor_Title ;
      private String edtHistoricoConsumo_UsuarioCod_Title ;
      private String sStyleString ;
      private String tblTablegridheader_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String subGrid_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTablesearch_Internalname ;
      private String sCtrlAV7Contrato_Codigo ;
      private String sGXsfl_14_fel_idx="0001" ;
      private String ROClassString ;
      private String edtHistoricoConsumo_Codigo_Jsonclick ;
      private String edtHistoricoConsumo_Data_Jsonclick ;
      private String cmbHistoricoConsumo_HistoricoCodigo_Jsonclick ;
      private String edtHistoricoConsumo_SaldoContratoCod_Jsonclick ;
      private String edtHistoricoConsumo_SaldoContratoDtaVigIni_Jsonclick ;
      private String edtHistoricoConsumo_SaldoContratoDtaVigFim_Jsonclick ;
      private String edtHistoricoConsumo_NotaEmpenhoCod_Jsonclick ;
      private String edtavNotaempenho_itentificador_Jsonclick ;
      private String edtavNotaempenho_demissao_Jsonclick ;
      private String edtHistoricoConsumo_ContagemResultadoCod_Jsonclick ;
      private String edtHistoricoConsumo_ContratoCod_Jsonclick ;
      private String edtHistoricoConsumo_Valor_Jsonclick ;
      private String edtHistoricoConsumo_UsuarioCod_Jsonclick ;
      private String edtavUsuario_nome_Jsonclick ;
      private String Workwithplusutilities1_Internalname ;
      private DateTime A1565NotaEmpenho_DEmissao ;
      private DateTime A1577HistoricoConsumo_Data ;
      private DateTime AV5NotaEmpenho_DEmissao ;
      private DateTime A1644HistoricoConsumo_SaldoContratoDtaVigIni ;
      private DateTime A1645HistoricoConsumo_SaldoContratoDtaVigFim ;
      private bool entryPointCalled ;
      private bool AV13OrderedDsc ;
      private bool n1581HistoricoConsumo_NotaEmpenhoCod ;
      private bool n1564NotaEmpenho_Itentificador ;
      private bool n1565NotaEmpenho_DEmissao ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n1582HistoricoConsumo_ContagemResultadoCod ;
      private bool n1579HistoricoConsumo_ContratoCod ;
      private bool n1563HistoricoConsumo_UsuarioCod ;
      private bool n40000Usuario_Nome ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private IGxSession AV14Session ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbHistoricoConsumo_HistoricoCodigo ;
      private IDataStoreProvider pr_default ;
      private int[] H00M33_A1579HistoricoConsumo_ContratoCod ;
      private bool[] H00M33_n1579HistoricoConsumo_ContratoCod ;
      private int[] H00M33_A1581HistoricoConsumo_NotaEmpenhoCod ;
      private bool[] H00M33_n1581HistoricoConsumo_NotaEmpenhoCod ;
      private int[] H00M33_A1563HistoricoConsumo_UsuarioCod ;
      private bool[] H00M33_n1563HistoricoConsumo_UsuarioCod ;
      private decimal[] H00M33_A1578HistoricoConsumo_Valor ;
      private int[] H00M33_A1582HistoricoConsumo_ContagemResultadoCod ;
      private bool[] H00M33_n1582HistoricoConsumo_ContagemResultadoCod ;
      private DateTime[] H00M33_A1645HistoricoConsumo_SaldoContratoDtaVigFim ;
      private DateTime[] H00M33_A1644HistoricoConsumo_SaldoContratoDtaVigIni ;
      private int[] H00M33_A1580HistoricoConsumo_SaldoContratoCod ;
      private String[] H00M33_A1646HistoricoConsumo_HistoricoCodigo ;
      private DateTime[] H00M33_A1577HistoricoConsumo_Data ;
      private int[] H00M33_A1562HistoricoConsumo_Codigo ;
      private String[] H00M33_A40000Usuario_Nome ;
      private bool[] H00M33_n40000Usuario_Nome ;
      private long[] H00M35_AGRID_nRecordCount ;
      private int[] H00M36_A1560NotaEmpenho_Codigo ;
      private String[] H00M36_A1564NotaEmpenho_Itentificador ;
      private bool[] H00M36_n1564NotaEmpenho_Itentificador ;
      private DateTime[] H00M36_A1565NotaEmpenho_DEmissao ;
      private bool[] H00M36_n1565NotaEmpenho_DEmissao ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV10HTTPRequest ;
      private wwpbaseobjects.SdtWWPGridState AV8GridState ;
      private wwpbaseobjects.SdtWWPTransactionContext AV15TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV16TrnContextAtt ;
      private wwpbaseobjects.SdtWWPContext AV17WWPContext ;
   }

   public class wc_contratohistoricoconsumo__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00M33 ;
          prmH00M33 = new Object[] {
          new Object[] {"@AV7Contrato_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH00M35 ;
          prmH00M35 = new Object[] {
          new Object[] {"@AV7Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00M36 ;
          prmH00M36 = new Object[] {
          new Object[] {"@HistoricoConsumo_NotaEmpenhoCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00M33", "SELECT * FROM (SELECT  T1.[HistoricoConsumo_ContratoCod], T1.[HistoricoConsumo_NotaEmpenhoCod], T1.[HistoricoConsumo_UsuarioCod], T1.[HistoricoConsumo_Valor], T1.[HistoricoConsumo_ContagemResultadoCod], T1.[HistoricoConsumo_SaldoContratoDtaVigFim], T1.[HistoricoConsumo_SaldoContratoDtaVigIni], T1.[HistoricoConsumo_SaldoContratoCod], T1.[HistoricoConsumo_HistoricoCodigo], T1.[HistoricoConsumo_Data], T1.[HistoricoConsumo_Codigo], COALESCE( T2.[Usuario_Nome], 'Usu�rio n�o informado') AS Usuario_Nome, ROW_NUMBER() OVER ( ORDER BY T1.[HistoricoConsumo_SaldoContratoDtaVigFim] DESC, T1.[HistoricoConsumo_SaldoContratoCod] ) AS GX_ROW_NUMBER FROM ([HistoricoConsumo] T1 WITH (NOLOCK) LEFT JOIN (SELECT [Usuario_Nome] AS Usuario_Nome, [Usuario_Codigo] FROM [Usuario] WITH (NOLOCK) ) T2 ON T2.[Usuario_Codigo] = T1.[HistoricoConsumo_UsuarioCod]) WHERE T1.[HistoricoConsumo_ContratoCod] = @AV7Contrato_Codigo) AS GX_CTE WHERE GX_ROW_NUMBER BETWEEN @GXPagingFrom2 AND @GXPagingTo2 OR @GXPagingTo2 < @GXPagingFrom2 AND GX_ROW_NUMBER >= @GXPagingFrom2",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00M33,11,0,true,false )
             ,new CursorDef("H00M35", "SELECT COUNT(*) FROM ([HistoricoConsumo] T1 WITH (NOLOCK) LEFT JOIN (SELECT [Usuario_Nome] AS Usuario_Nome, [Usuario_Codigo] FROM [Usuario] WITH (NOLOCK) ) T2 ON T2.[Usuario_Codigo] = T1.[HistoricoConsumo_UsuarioCod]) WHERE T1.[HistoricoConsumo_ContratoCod] = @AV7Contrato_Codigo ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00M35,1,0,true,false )
             ,new CursorDef("H00M36", "SELECT [NotaEmpenho_Codigo], [NotaEmpenho_Itentificador], [NotaEmpenho_DEmissao] FROM [NotaEmpenho] WITH (NOLOCK) WHERE [NotaEmpenho_Codigo] = @HistoricoConsumo_NotaEmpenhoCod ORDER BY [NotaEmpenho_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00M36,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((decimal[]) buf[6])[0] = rslt.getDecimal(4) ;
                ((int[]) buf[7])[0] = rslt.getInt(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((DateTime[]) buf[9])[0] = rslt.getGXDate(6) ;
                ((DateTime[]) buf[10])[0] = rslt.getGXDate(7) ;
                ((int[]) buf[11])[0] = rslt.getInt(8) ;
                ((String[]) buf[12])[0] = rslt.getString(9, 3) ;
                ((DateTime[]) buf[13])[0] = rslt.getGXDateTime(10) ;
                ((int[]) buf[14])[0] = rslt.getInt(11) ;
                ((String[]) buf[15])[0] = rslt.getString(12, 50) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(12);
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 15) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((DateTime[]) buf[3])[0] = rslt.getGXDateTime(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
       }
    }

 }

}
