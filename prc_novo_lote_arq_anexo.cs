/*
               File: Prc_novo_lote_arq_anexo
        Description: Prc_novo_lote_arq_anexo
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:51:36.30
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_novo_lote_arq_anexo : GXProcedure
   {
      public prc_novo_lote_arq_anexo( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_novo_lote_arq_anexo( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Lote_Codigo ,
                           String aP1_NomeArqPdf ,
                           String aP2_SignatureResult ,
                           out int aP3_LoteArquivoAnexo_LoteCod ,
                           ref String aP4_Verificador )
      {
         this.AV8Lote_Codigo = aP0_Lote_Codigo;
         this.AV10NomeArqPdf = aP1_NomeArqPdf;
         this.AV11SignatureResult = aP2_SignatureResult;
         this.AV9LoteArquivoAnexo_LoteCod = 0 ;
         this.AV14Verificador = aP4_Verificador;
         initialize();
         executePrivate();
         aP3_LoteArquivoAnexo_LoteCod=this.AV9LoteArquivoAnexo_LoteCod;
         aP4_Verificador=this.AV14Verificador;
      }

      public String executeUdp( int aP0_Lote_Codigo ,
                                String aP1_NomeArqPdf ,
                                String aP2_SignatureResult ,
                                out int aP3_LoteArquivoAnexo_LoteCod )
      {
         this.AV8Lote_Codigo = aP0_Lote_Codigo;
         this.AV10NomeArqPdf = aP1_NomeArqPdf;
         this.AV11SignatureResult = aP2_SignatureResult;
         this.AV9LoteArquivoAnexo_LoteCod = 0 ;
         this.AV14Verificador = aP4_Verificador;
         initialize();
         executePrivate();
         aP3_LoteArquivoAnexo_LoteCod=this.AV9LoteArquivoAnexo_LoteCod;
         aP4_Verificador=this.AV14Verificador;
         return AV14Verificador ;
      }

      public void executeSubmit( int aP0_Lote_Codigo ,
                                 String aP1_NomeArqPdf ,
                                 String aP2_SignatureResult ,
                                 out int aP3_LoteArquivoAnexo_LoteCod ,
                                 ref String aP4_Verificador )
      {
         prc_novo_lote_arq_anexo objprc_novo_lote_arq_anexo;
         objprc_novo_lote_arq_anexo = new prc_novo_lote_arq_anexo();
         objprc_novo_lote_arq_anexo.AV8Lote_Codigo = aP0_Lote_Codigo;
         objprc_novo_lote_arq_anexo.AV10NomeArqPdf = aP1_NomeArqPdf;
         objprc_novo_lote_arq_anexo.AV11SignatureResult = aP2_SignatureResult;
         objprc_novo_lote_arq_anexo.AV9LoteArquivoAnexo_LoteCod = 0 ;
         objprc_novo_lote_arq_anexo.AV14Verificador = aP4_Verificador;
         objprc_novo_lote_arq_anexo.context.SetSubmitInitialConfig(context);
         objprc_novo_lote_arq_anexo.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_novo_lote_arq_anexo);
         aP3_LoteArquivoAnexo_LoteCod=this.AV9LoteArquivoAnexo_LoteCod;
         aP4_Verificador=this.AV14Verificador;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_novo_lote_arq_anexo)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV12UploadPDF = AV11SignatureResult;
         /*
            INSERT RECORD ON TABLE LoteArquivoAnexo

         */
         A841LoteArquivoAnexo_LoteCod = AV8Lote_Codigo;
         A836LoteArquivoAnexo_Data = DateTimeUtil.ServerNow( context, "DEFAULT");
         A838LoteArquivoAnexo_Arquivo = AV12UploadPDF;
         n838LoteArquivoAnexo_Arquivo = false;
         A837LoteArquivoAnexo_Descricao = AV10NomeArqPdf;
         n837LoteArquivoAnexo_Descricao = false;
         A645TipoDocumento_Codigo = 1;
         n645TipoDocumento_Codigo = false;
         A839LoteArquivoAnexo_NomeArq = AV10NomeArqPdf;
         n839LoteArquivoAnexo_NomeArq = false;
         A840LoteArquivoAnexo_TipoArq = "pdf";
         n840LoteArquivoAnexo_TipoArq = false;
         A1027LoteArquivoAnexo_Verificador = AV14Verificador;
         n1027LoteArquivoAnexo_Verificador = false;
         /* Using cursor P006R2 */
         A839LoteArquivoAnexo_NomeArq = FileUtil.GetFileName( A838LoteArquivoAnexo_Arquivo);
         n839LoteArquivoAnexo_NomeArq = false;
         A840LoteArquivoAnexo_TipoArq = FileUtil.GetFileType( A838LoteArquivoAnexo_Arquivo);
         n840LoteArquivoAnexo_TipoArq = false;
         pr_default.execute(0, new Object[] {A841LoteArquivoAnexo_LoteCod, A836LoteArquivoAnexo_Data, n837LoteArquivoAnexo_Descricao, A837LoteArquivoAnexo_Descricao, n838LoteArquivoAnexo_Arquivo, A838LoteArquivoAnexo_Arquivo, n839LoteArquivoAnexo_NomeArq, A839LoteArquivoAnexo_NomeArq, n840LoteArquivoAnexo_TipoArq, A840LoteArquivoAnexo_TipoArq, n645TipoDocumento_Codigo, A645TipoDocumento_Codigo, n1027LoteArquivoAnexo_Verificador, A1027LoteArquivoAnexo_Verificador});
         pr_default.close(0);
         dsDefault.SmartCacheProvider.SetUpdated("LoteArquivoAnexo") ;
         if ( (pr_default.getStatus(0) == 1) )
         {
            context.Gx_err = 1;
            Gx_emsg = (String)(context.GetMessage( "GXM_noupdate", ""));
         }
         else
         {
            context.Gx_err = 0;
            Gx_emsg = "";
         }
         /* End Insert */
         AV9LoteArquivoAnexo_LoteCod = A841LoteArquivoAnexo_LoteCod;
         this.cleanup();
      }

      public override void cleanup( )
      {
         context.CommitDataStores( "Prc_novo_lote_arq_anexo");
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV12UploadPDF = "";
         A836LoteArquivoAnexo_Data = (DateTime)(DateTime.MinValue);
         A838LoteArquivoAnexo_Arquivo = "";
         A837LoteArquivoAnexo_Descricao = "";
         A839LoteArquivoAnexo_NomeArq = "";
         A840LoteArquivoAnexo_TipoArq = "";
         A1027LoteArquivoAnexo_Verificador = "";
         Gx_emsg = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_novo_lote_arq_anexo__default(),
            new Object[][] {
                new Object[] {
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV8Lote_Codigo ;
      private int GX_INS103 ;
      private int A841LoteArquivoAnexo_LoteCod ;
      private int A645TipoDocumento_Codigo ;
      private int AV9LoteArquivoAnexo_LoteCod ;
      private String AV10NomeArqPdf ;
      private String A839LoteArquivoAnexo_NomeArq ;
      private String A840LoteArquivoAnexo_TipoArq ;
      private String Gx_emsg ;
      private DateTime A836LoteArquivoAnexo_Data ;
      private bool n838LoteArquivoAnexo_Arquivo ;
      private bool n837LoteArquivoAnexo_Descricao ;
      private bool n645TipoDocumento_Codigo ;
      private bool n839LoteArquivoAnexo_NomeArq ;
      private bool n840LoteArquivoAnexo_TipoArq ;
      private bool n1027LoteArquivoAnexo_Verificador ;
      private String A837LoteArquivoAnexo_Descricao ;
      private String AV11SignatureResult ;
      private String AV14Verificador ;
      private String A1027LoteArquivoAnexo_Verificador ;
      private String AV12UploadPDF ;
      private String A838LoteArquivoAnexo_Arquivo ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private String aP4_Verificador ;
      private IDataStoreProvider pr_default ;
      private int aP3_LoteArquivoAnexo_LoteCod ;
   }

   public class prc_novo_lote_arq_anexo__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new UpdateCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP006R2 ;
          prmP006R2 = new Object[] {
          new Object[] {"@LoteArquivoAnexo_LoteCod",SqlDbType.Int,6,0} ,
          new Object[] {"@LoteArquivoAnexo_Data",SqlDbType.DateTime,8,5} ,
          new Object[] {"@LoteArquivoAnexo_Descricao",SqlDbType.VarChar,500,0} ,
          new Object[] {"@LoteArquivoAnexo_Arquivo",SqlDbType.VarBinary,1024,0} ,
          new Object[] {"@LoteArquivoAnexo_NomeArq",SqlDbType.Char,50,0} ,
          new Object[] {"@LoteArquivoAnexo_TipoArq",SqlDbType.Char,10,0} ,
          new Object[] {"@TipoDocumento_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@LoteArquivoAnexo_Verificador",SqlDbType.VarChar,40,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P006R2", "INSERT INTO [LoteArquivoAnexo]([LoteArquivoAnexo_LoteCod], [LoteArquivoAnexo_Data], [LoteArquivoAnexo_Descricao], [LoteArquivoAnexo_Arquivo], [LoteArquivoAnexo_NomeArq], [LoteArquivoAnexo_TipoArq], [TipoDocumento_Codigo], [LoteArquivoAnexo_Verificador]) VALUES(@LoteArquivoAnexo_LoteCod, @LoteArquivoAnexo_Data, @LoteArquivoAnexo_Descricao, @LoteArquivoAnexo_Arquivo, @LoteArquivoAnexo_NomeArq, @LoteArquivoAnexo_TipoArq, @TipoDocumento_Codigo, @LoteArquivoAnexo_Verificador)", GxErrorMask.GX_NOMASK,prmP006R2)
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameterDatetime(2, (DateTime)parms[1]);
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 3 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(3, (String)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 4 , SqlDbType.VarBinary );
                }
                else
                {
                   stmt.SetParameter(4, (String)parms[5]);
                }
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 5 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(5, (String)parms[7]);
                }
                if ( (bool)parms[8] )
                {
                   stmt.setNull( 6 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(6, (String)parms[9]);
                }
                if ( (bool)parms[10] )
                {
                   stmt.setNull( 7 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(7, (int)parms[11]);
                }
                if ( (bool)parms[12] )
                {
                   stmt.setNull( 8 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(8, (String)parms[13]);
                }
                return;
       }
    }

 }

}
