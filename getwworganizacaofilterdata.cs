/*
               File: GetWWOrganizacaoFilterData
        Description: Get WWOrganizacao Filter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:54:23.96
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getwworganizacaofilterdata : GXProcedure
   {
      public getwworganizacaofilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getwworganizacaofilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV15DDOName = aP0_DDOName;
         this.AV13SearchTxt = aP1_SearchTxt;
         this.AV14SearchTxtTo = aP2_SearchTxtTo;
         this.AV19OptionsJson = "" ;
         this.AV22OptionsDescJson = "" ;
         this.AV24OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV19OptionsJson;
         aP4_OptionsDescJson=this.AV22OptionsDescJson;
         aP5_OptionIndexesJson=this.AV24OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV15DDOName = aP0_DDOName;
         this.AV13SearchTxt = aP1_SearchTxt;
         this.AV14SearchTxtTo = aP2_SearchTxtTo;
         this.AV19OptionsJson = "" ;
         this.AV22OptionsDescJson = "" ;
         this.AV24OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV19OptionsJson;
         aP4_OptionsDescJson=this.AV22OptionsDescJson;
         aP5_OptionIndexesJson=this.AV24OptionIndexesJson;
         return AV24OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getwworganizacaofilterdata objgetwworganizacaofilterdata;
         objgetwworganizacaofilterdata = new getwworganizacaofilterdata();
         objgetwworganizacaofilterdata.AV15DDOName = aP0_DDOName;
         objgetwworganizacaofilterdata.AV13SearchTxt = aP1_SearchTxt;
         objgetwworganizacaofilterdata.AV14SearchTxtTo = aP2_SearchTxtTo;
         objgetwworganizacaofilterdata.AV19OptionsJson = "" ;
         objgetwworganizacaofilterdata.AV22OptionsDescJson = "" ;
         objgetwworganizacaofilterdata.AV24OptionIndexesJson = "" ;
         objgetwworganizacaofilterdata.context.SetSubmitInitialConfig(context);
         objgetwworganizacaofilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetwworganizacaofilterdata);
         aP3_OptionsJson=this.AV19OptionsJson;
         aP4_OptionsDescJson=this.AV22OptionsDescJson;
         aP5_OptionIndexesJson=this.AV24OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getwworganizacaofilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV18Options = (IGxCollection)(new GxSimpleCollection());
         AV21OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV23OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV15DDOName), "DDO_ORGANIZACAO_NOME") == 0 )
         {
            /* Execute user subroutine: 'LOADORGANIZACAO_NOMEOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV19OptionsJson = AV18Options.ToJSonString(false);
         AV22OptionsDescJson = AV21OptionsDesc.ToJSonString(false);
         AV24OptionIndexesJson = AV23OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV26Session.Get("WWOrganizacaoGridState"), "") == 0 )
         {
            AV28GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "WWOrganizacaoGridState"), "");
         }
         else
         {
            AV28GridState.FromXml(AV26Session.Get("WWOrganizacaoGridState"), "");
         }
         AV41GXV1 = 1;
         while ( AV41GXV1 <= AV28GridState.gxTpr_Filtervalues.Count )
         {
            AV29GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV28GridState.gxTpr_Filtervalues.Item(AV41GXV1));
            if ( StringUtil.StrCmp(AV29GridStateFilterValue.gxTpr_Name, "TFORGANIZACAO_NOME") == 0 )
            {
               AV10TFOrganizacao_Nome = AV29GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV29GridStateFilterValue.gxTpr_Name, "TFORGANIZACAO_NOME_SEL") == 0 )
            {
               AV11TFOrganizacao_Nome_Sel = AV29GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV29GridStateFilterValue.gxTpr_Name, "TFORGANIZACAO_ATIVO_SEL") == 0 )
            {
               AV12TFOrganizacao_Ativo_Sel = (short)(NumberUtil.Val( AV29GridStateFilterValue.gxTpr_Value, "."));
            }
            AV41GXV1 = (int)(AV41GXV1+1);
         }
         if ( AV28GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV30GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV28GridState.gxTpr_Dynamicfilters.Item(1));
            AV31DynamicFiltersSelector1 = AV30GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV31DynamicFiltersSelector1, "ORGANIZACAO_NOME") == 0 )
            {
               AV32Organizacao_Nome1 = AV30GridStateDynamicFilter.gxTpr_Value;
            }
            if ( AV28GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV33DynamicFiltersEnabled2 = true;
               AV30GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV28GridState.gxTpr_Dynamicfilters.Item(2));
               AV34DynamicFiltersSelector2 = AV30GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV34DynamicFiltersSelector2, "ORGANIZACAO_NOME") == 0 )
               {
                  AV35Organizacao_Nome2 = AV30GridStateDynamicFilter.gxTpr_Value;
               }
               if ( AV28GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  AV36DynamicFiltersEnabled3 = true;
                  AV30GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV28GridState.gxTpr_Dynamicfilters.Item(3));
                  AV37DynamicFiltersSelector3 = AV30GridStateDynamicFilter.gxTpr_Selected;
                  if ( StringUtil.StrCmp(AV37DynamicFiltersSelector3, "ORGANIZACAO_NOME") == 0 )
                  {
                     AV38Organizacao_Nome3 = AV30GridStateDynamicFilter.gxTpr_Value;
                  }
               }
            }
         }
      }

      protected void S121( )
      {
         /* 'LOADORGANIZACAO_NOMEOPTIONS' Routine */
         AV10TFOrganizacao_Nome = AV13SearchTxt;
         AV11TFOrganizacao_Nome_Sel = "";
         AV43WWOrganizacaoDS_1_Dynamicfiltersselector1 = AV31DynamicFiltersSelector1;
         AV44WWOrganizacaoDS_2_Organizacao_nome1 = AV32Organizacao_Nome1;
         AV45WWOrganizacaoDS_3_Dynamicfiltersenabled2 = AV33DynamicFiltersEnabled2;
         AV46WWOrganizacaoDS_4_Dynamicfiltersselector2 = AV34DynamicFiltersSelector2;
         AV47WWOrganizacaoDS_5_Organizacao_nome2 = AV35Organizacao_Nome2;
         AV48WWOrganizacaoDS_6_Dynamicfiltersenabled3 = AV36DynamicFiltersEnabled3;
         AV49WWOrganizacaoDS_7_Dynamicfiltersselector3 = AV37DynamicFiltersSelector3;
         AV50WWOrganizacaoDS_8_Organizacao_nome3 = AV38Organizacao_Nome3;
         AV51WWOrganizacaoDS_9_Tforganizacao_nome = AV10TFOrganizacao_Nome;
         AV52WWOrganizacaoDS_10_Tforganizacao_nome_sel = AV11TFOrganizacao_Nome_Sel;
         AV53WWOrganizacaoDS_11_Tforganizacao_ativo_sel = AV12TFOrganizacao_Ativo_Sel;
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              AV43WWOrganizacaoDS_1_Dynamicfiltersselector1 ,
                                              AV44WWOrganizacaoDS_2_Organizacao_nome1 ,
                                              AV45WWOrganizacaoDS_3_Dynamicfiltersenabled2 ,
                                              AV46WWOrganizacaoDS_4_Dynamicfiltersselector2 ,
                                              AV47WWOrganizacaoDS_5_Organizacao_nome2 ,
                                              AV48WWOrganizacaoDS_6_Dynamicfiltersenabled3 ,
                                              AV49WWOrganizacaoDS_7_Dynamicfiltersselector3 ,
                                              AV50WWOrganizacaoDS_8_Organizacao_nome3 ,
                                              AV52WWOrganizacaoDS_10_Tforganizacao_nome_sel ,
                                              AV51WWOrganizacaoDS_9_Tforganizacao_nome ,
                                              AV53WWOrganizacaoDS_11_Tforganizacao_ativo_sel ,
                                              A1214Organizacao_Nome ,
                                              A1215Organizacao_Ativo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN
                                              }
         });
         lV44WWOrganizacaoDS_2_Organizacao_nome1 = StringUtil.PadR( StringUtil.RTrim( AV44WWOrganizacaoDS_2_Organizacao_nome1), 50, "%");
         lV47WWOrganizacaoDS_5_Organizacao_nome2 = StringUtil.PadR( StringUtil.RTrim( AV47WWOrganizacaoDS_5_Organizacao_nome2), 50, "%");
         lV50WWOrganizacaoDS_8_Organizacao_nome3 = StringUtil.PadR( StringUtil.RTrim( AV50WWOrganizacaoDS_8_Organizacao_nome3), 50, "%");
         lV51WWOrganizacaoDS_9_Tforganizacao_nome = StringUtil.PadR( StringUtil.RTrim( AV51WWOrganizacaoDS_9_Tforganizacao_nome), 50, "%");
         /* Using cursor P00QB2 */
         pr_default.execute(0, new Object[] {lV44WWOrganizacaoDS_2_Organizacao_nome1, lV47WWOrganizacaoDS_5_Organizacao_nome2, lV50WWOrganizacaoDS_8_Organizacao_nome3, lV51WWOrganizacaoDS_9_Tforganizacao_nome, AV52WWOrganizacaoDS_10_Tforganizacao_nome_sel});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKQB2 = false;
            A1214Organizacao_Nome = P00QB2_A1214Organizacao_Nome[0];
            A1215Organizacao_Ativo = P00QB2_A1215Organizacao_Ativo[0];
            A1213Organizacao_Codigo = P00QB2_A1213Organizacao_Codigo[0];
            AV25count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( StringUtil.StrCmp(P00QB2_A1214Organizacao_Nome[0], A1214Organizacao_Nome) == 0 ) )
            {
               BRKQB2 = false;
               A1213Organizacao_Codigo = P00QB2_A1213Organizacao_Codigo[0];
               AV25count = (long)(AV25count+1);
               BRKQB2 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A1214Organizacao_Nome)) )
            {
               AV17Option = A1214Organizacao_Nome;
               AV18Options.Add(AV17Option, 0);
               AV23OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV25count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV18Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKQB2 )
            {
               BRKQB2 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV18Options = new GxSimpleCollection();
         AV21OptionsDesc = new GxSimpleCollection();
         AV23OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV26Session = context.GetSession();
         AV28GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV29GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV10TFOrganizacao_Nome = "";
         AV11TFOrganizacao_Nome_Sel = "";
         AV30GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV31DynamicFiltersSelector1 = "";
         AV32Organizacao_Nome1 = "";
         AV34DynamicFiltersSelector2 = "";
         AV35Organizacao_Nome2 = "";
         AV37DynamicFiltersSelector3 = "";
         AV38Organizacao_Nome3 = "";
         AV43WWOrganizacaoDS_1_Dynamicfiltersselector1 = "";
         AV44WWOrganizacaoDS_2_Organizacao_nome1 = "";
         AV46WWOrganizacaoDS_4_Dynamicfiltersselector2 = "";
         AV47WWOrganizacaoDS_5_Organizacao_nome2 = "";
         AV49WWOrganizacaoDS_7_Dynamicfiltersselector3 = "";
         AV50WWOrganizacaoDS_8_Organizacao_nome3 = "";
         AV51WWOrganizacaoDS_9_Tforganizacao_nome = "";
         AV52WWOrganizacaoDS_10_Tforganizacao_nome_sel = "";
         scmdbuf = "";
         lV44WWOrganizacaoDS_2_Organizacao_nome1 = "";
         lV47WWOrganizacaoDS_5_Organizacao_nome2 = "";
         lV50WWOrganizacaoDS_8_Organizacao_nome3 = "";
         lV51WWOrganizacaoDS_9_Tforganizacao_nome = "";
         A1214Organizacao_Nome = "";
         P00QB2_A1214Organizacao_Nome = new String[] {""} ;
         P00QB2_A1215Organizacao_Ativo = new bool[] {false} ;
         P00QB2_A1213Organizacao_Codigo = new int[1] ;
         AV17Option = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getwworganizacaofilterdata__default(),
            new Object[][] {
                new Object[] {
               P00QB2_A1214Organizacao_Nome, P00QB2_A1215Organizacao_Ativo, P00QB2_A1213Organizacao_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV12TFOrganizacao_Ativo_Sel ;
      private short AV53WWOrganizacaoDS_11_Tforganizacao_ativo_sel ;
      private int AV41GXV1 ;
      private int A1213Organizacao_Codigo ;
      private long AV25count ;
      private String AV10TFOrganizacao_Nome ;
      private String AV11TFOrganizacao_Nome_Sel ;
      private String AV32Organizacao_Nome1 ;
      private String AV35Organizacao_Nome2 ;
      private String AV38Organizacao_Nome3 ;
      private String AV44WWOrganizacaoDS_2_Organizacao_nome1 ;
      private String AV47WWOrganizacaoDS_5_Organizacao_nome2 ;
      private String AV50WWOrganizacaoDS_8_Organizacao_nome3 ;
      private String AV51WWOrganizacaoDS_9_Tforganizacao_nome ;
      private String AV52WWOrganizacaoDS_10_Tforganizacao_nome_sel ;
      private String scmdbuf ;
      private String lV44WWOrganizacaoDS_2_Organizacao_nome1 ;
      private String lV47WWOrganizacaoDS_5_Organizacao_nome2 ;
      private String lV50WWOrganizacaoDS_8_Organizacao_nome3 ;
      private String lV51WWOrganizacaoDS_9_Tforganizacao_nome ;
      private String A1214Organizacao_Nome ;
      private bool returnInSub ;
      private bool AV33DynamicFiltersEnabled2 ;
      private bool AV36DynamicFiltersEnabled3 ;
      private bool AV45WWOrganizacaoDS_3_Dynamicfiltersenabled2 ;
      private bool AV48WWOrganizacaoDS_6_Dynamicfiltersenabled3 ;
      private bool A1215Organizacao_Ativo ;
      private bool BRKQB2 ;
      private String AV24OptionIndexesJson ;
      private String AV19OptionsJson ;
      private String AV22OptionsDescJson ;
      private String AV15DDOName ;
      private String AV13SearchTxt ;
      private String AV14SearchTxtTo ;
      private String AV31DynamicFiltersSelector1 ;
      private String AV34DynamicFiltersSelector2 ;
      private String AV37DynamicFiltersSelector3 ;
      private String AV43WWOrganizacaoDS_1_Dynamicfiltersselector1 ;
      private String AV46WWOrganizacaoDS_4_Dynamicfiltersselector2 ;
      private String AV49WWOrganizacaoDS_7_Dynamicfiltersselector3 ;
      private String AV17Option ;
      private IGxSession AV26Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private String[] P00QB2_A1214Organizacao_Nome ;
      private bool[] P00QB2_A1215Organizacao_Ativo ;
      private int[] P00QB2_A1213Organizacao_Codigo ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV18Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV21OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV23OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV28GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV29GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV30GridStateDynamicFilter ;
   }

   public class getwworganizacaofilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00QB2( IGxContext context ,
                                             String AV43WWOrganizacaoDS_1_Dynamicfiltersselector1 ,
                                             String AV44WWOrganizacaoDS_2_Organizacao_nome1 ,
                                             bool AV45WWOrganizacaoDS_3_Dynamicfiltersenabled2 ,
                                             String AV46WWOrganizacaoDS_4_Dynamicfiltersselector2 ,
                                             String AV47WWOrganizacaoDS_5_Organizacao_nome2 ,
                                             bool AV48WWOrganizacaoDS_6_Dynamicfiltersenabled3 ,
                                             String AV49WWOrganizacaoDS_7_Dynamicfiltersselector3 ,
                                             String AV50WWOrganizacaoDS_8_Organizacao_nome3 ,
                                             String AV52WWOrganizacaoDS_10_Tforganizacao_nome_sel ,
                                             String AV51WWOrganizacaoDS_9_Tforganizacao_nome ,
                                             short AV53WWOrganizacaoDS_11_Tforganizacao_ativo_sel ,
                                             String A1214Organizacao_Nome ,
                                             bool A1215Organizacao_Ativo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [5] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT [Organizacao_Nome], [Organizacao_Ativo], [Organizacao_Codigo] FROM [Organizacao] WITH (NOLOCK)";
         if ( ( StringUtil.StrCmp(AV43WWOrganizacaoDS_1_Dynamicfiltersselector1, "ORGANIZACAO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV44WWOrganizacaoDS_2_Organizacao_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Organizacao_Nome] like '%' + @lV44WWOrganizacaoDS_2_Organizacao_nome1 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " ([Organizacao_Nome] like '%' + @lV44WWOrganizacaoDS_2_Organizacao_nome1 + '%')";
            }
         }
         else
         {
            GXv_int1[0] = 1;
         }
         if ( AV45WWOrganizacaoDS_3_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV46WWOrganizacaoDS_4_Dynamicfiltersselector2, "ORGANIZACAO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV47WWOrganizacaoDS_5_Organizacao_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Organizacao_Nome] like '%' + @lV47WWOrganizacaoDS_5_Organizacao_nome2 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " ([Organizacao_Nome] like '%' + @lV47WWOrganizacaoDS_5_Organizacao_nome2 + '%')";
            }
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( AV48WWOrganizacaoDS_6_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV49WWOrganizacaoDS_7_Dynamicfiltersselector3, "ORGANIZACAO_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV50WWOrganizacaoDS_8_Organizacao_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Organizacao_Nome] like '%' + @lV50WWOrganizacaoDS_8_Organizacao_nome3 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " ([Organizacao_Nome] like '%' + @lV50WWOrganizacaoDS_8_Organizacao_nome3 + '%')";
            }
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV52WWOrganizacaoDS_10_Tforganizacao_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV51WWOrganizacaoDS_9_Tforganizacao_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Organizacao_Nome] like @lV51WWOrganizacaoDS_9_Tforganizacao_nome)";
            }
            else
            {
               sWhereString = sWhereString + " ([Organizacao_Nome] like @lV51WWOrganizacaoDS_9_Tforganizacao_nome)";
            }
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV52WWOrganizacaoDS_10_Tforganizacao_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Organizacao_Nome] = @AV52WWOrganizacaoDS_10_Tforganizacao_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([Organizacao_Nome] = @AV52WWOrganizacaoDS_10_Tforganizacao_nome_sel)";
            }
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( AV53WWOrganizacaoDS_11_Tforganizacao_ativo_sel == 1 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Organizacao_Ativo] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " ([Organizacao_Ativo] = 1)";
            }
         }
         if ( AV53WWOrganizacaoDS_11_Tforganizacao_ativo_sel == 2 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([Organizacao_Ativo] = 0)";
            }
            else
            {
               sWhereString = sWhereString + " ([Organizacao_Ativo] = 0)";
            }
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY [Organizacao_Nome]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00QB2(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (bool)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (short)dynConstraints[10] , (String)dynConstraints[11] , (bool)dynConstraints[12] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00QB2 ;
          prmP00QB2 = new Object[] {
          new Object[] {"@lV44WWOrganizacaoDS_2_Organizacao_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV47WWOrganizacaoDS_5_Organizacao_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV50WWOrganizacaoDS_8_Organizacao_nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV51WWOrganizacaoDS_9_Tforganizacao_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV52WWOrganizacaoDS_10_Tforganizacao_nome_sel",SqlDbType.Char,50,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00QB2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00QB2,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.getBool(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[5]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[6]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[7]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[8]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[9]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getwworganizacaofilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getwworganizacaofilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getwworganizacaofilterdata") )
          {
             return  ;
          }
          getwworganizacaofilterdata worker = new getwworganizacaofilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
