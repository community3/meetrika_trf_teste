/*
               File: PromptContagemResultadoIncidentes
        Description: Selecione Contagem Resultado Incidentes
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/2/2020 16:23:54.0
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class promptcontagemresultadoincidentes : GXHttpHandler, System.Web.SessionState.IRequiresSessionState
   {
      public promptcontagemresultadoincidentes( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public promptcontagemresultadoincidentes( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_InOutIncidentes_Codigo ,
                           ref DateTime aP1_InOutIncidentes_Data )
      {
         this.AV7InOutIncidentes_Codigo = aP0_InOutIncidentes_Codigo;
         this.AV8InOutIncidentes_Data = aP1_InOutIncidentes_Data;
         executePrivate();
         aP0_InOutIncidentes_Codigo=this.AV7InOutIncidentes_Codigo;
         aP1_InOutIncidentes_Data=this.AV8InOutIncidentes_Data;
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavOrderedby = new GXCombobox();
         cmbavDynamicfiltersselector1 = new GXCombobox();
         cmbavDynamicfiltersselector2 = new GXCombobox();
         cmbavDynamicfiltersselector3 = new GXCombobox();
         chkavDynamicfiltersenabled2 = new GXCheckbox();
         chkavDynamicfiltersenabled3 = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
            {
               nRC_GXsfl_86 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_86_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_86_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
            {
               subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV13OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
               AV14OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
               AV15DynamicFiltersSelector1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
               AV16Incidentes_Data1 = context.localUtil.ParseDTimeParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16Incidentes_Data1", context.localUtil.TToC( AV16Incidentes_Data1, 8, 5, 0, 3, "/", ":", " "));
               AV17Incidentes_Data_To1 = context.localUtil.ParseDTimeParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Incidentes_Data_To1", context.localUtil.TToC( AV17Incidentes_Data_To1, 8, 5, 0, 3, "/", ":", " "));
               AV19DynamicFiltersSelector2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
               AV20Incidentes_Data2 = context.localUtil.ParseDTimeParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20Incidentes_Data2", context.localUtil.TToC( AV20Incidentes_Data2, 8, 5, 0, 3, "/", ":", " "));
               AV21Incidentes_Data_To2 = context.localUtil.ParseDTimeParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Incidentes_Data_To2", context.localUtil.TToC( AV21Incidentes_Data_To2, 8, 5, 0, 3, "/", ":", " "));
               AV23DynamicFiltersSelector3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
               AV24Incidentes_Data3 = context.localUtil.ParseDTimeParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24Incidentes_Data3", context.localUtil.TToC( AV24Incidentes_Data3, 8, 5, 0, 3, "/", ":", " "));
               AV25Incidentes_Data_To3 = context.localUtil.ParseDTimeParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25Incidentes_Data_To3", context.localUtil.TToC( AV25Incidentes_Data_To3, 8, 5, 0, 3, "/", ":", " "));
               AV18DynamicFiltersEnabled2 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
               AV22DynamicFiltersEnabled3 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
               AV31TFIncidentes_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31TFIncidentes_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV31TFIncidentes_Codigo), 6, 0)));
               AV32TFIncidentes_Codigo_To = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32TFIncidentes_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV32TFIncidentes_Codigo_To), 6, 0)));
               AV35TFIncidentes_Data = context.localUtil.ParseDTimeParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TFIncidentes_Data", context.localUtil.TToC( AV35TFIncidentes_Data, 8, 5, 0, 3, "/", ":", " "));
               AV36TFIncidentes_Data_To = context.localUtil.ParseDTimeParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36TFIncidentes_Data_To", context.localUtil.TToC( AV36TFIncidentes_Data_To, 8, 5, 0, 3, "/", ":", " "));
               AV41TFIncidentes_Descricao = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41TFIncidentes_Descricao", AV41TFIncidentes_Descricao);
               AV42TFIncidentes_Descricao_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42TFIncidentes_Descricao_Sel", AV42TFIncidentes_Descricao_Sel);
               AV45TFIncidentes_DemandaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45TFIncidentes_DemandaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV45TFIncidentes_DemandaCod), 6, 0)));
               AV46TFIncidentes_DemandaCod_To = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46TFIncidentes_DemandaCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV46TFIncidentes_DemandaCod_To), 6, 0)));
               AV49TFIncidentes_DemandaNum = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49TFIncidentes_DemandaNum", AV49TFIncidentes_DemandaNum);
               AV50TFIncidentes_DemandaNum_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50TFIncidentes_DemandaNum_Sel", AV50TFIncidentes_DemandaNum_Sel);
               AV33ddo_Incidentes_CodigoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33ddo_Incidentes_CodigoTitleControlIdToReplace", AV33ddo_Incidentes_CodigoTitleControlIdToReplace);
               AV39ddo_Incidentes_DataTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39ddo_Incidentes_DataTitleControlIdToReplace", AV39ddo_Incidentes_DataTitleControlIdToReplace);
               AV43ddo_Incidentes_DescricaoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43ddo_Incidentes_DescricaoTitleControlIdToReplace", AV43ddo_Incidentes_DescricaoTitleControlIdToReplace);
               AV47ddo_Incidentes_DemandaCodTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47ddo_Incidentes_DemandaCodTitleControlIdToReplace", AV47ddo_Incidentes_DemandaCodTitleControlIdToReplace);
               AV51ddo_Incidentes_DemandaNumTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51ddo_Incidentes_DemandaNumTitleControlIdToReplace", AV51ddo_Incidentes_DemandaNumTitleControlIdToReplace);
               AV59Pgmname = GetNextPar( );
               ajax_req_read_hidden_sdt(GetNextPar( ), AV10GridState);
               AV27DynamicFiltersIgnoreFirst = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersIgnoreFirst", AV27DynamicFiltersIgnoreFirst);
               AV26DynamicFiltersRemoving = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16Incidentes_Data1, AV17Incidentes_Data_To1, AV19DynamicFiltersSelector2, AV20Incidentes_Data2, AV21Incidentes_Data_To2, AV23DynamicFiltersSelector3, AV24Incidentes_Data3, AV25Incidentes_Data_To3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV31TFIncidentes_Codigo, AV32TFIncidentes_Codigo_To, AV35TFIncidentes_Data, AV36TFIncidentes_Data_To, AV41TFIncidentes_Descricao, AV42TFIncidentes_Descricao_Sel, AV45TFIncidentes_DemandaCod, AV46TFIncidentes_DemandaCod_To, AV49TFIncidentes_DemandaNum, AV50TFIncidentes_DemandaNum_Sel, AV33ddo_Incidentes_CodigoTitleControlIdToReplace, AV39ddo_Incidentes_DataTitleControlIdToReplace, AV43ddo_Incidentes_DescricaoTitleControlIdToReplace, AV47ddo_Incidentes_DemandaCodTitleControlIdToReplace, AV51ddo_Incidentes_DemandaNumTitleControlIdToReplace, AV59Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               AV7InOutIncidentes_Codigo = (int)(NumberUtil.Val( gxfirstwebparm, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InOutIncidentes_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7InOutIncidentes_Codigo), 6, 0)));
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
               {
                  AV8InOutIncidentes_Data = context.localUtil.ParseDTimeParm( GetNextPar( ));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8InOutIncidentes_Data", context.localUtil.TToC( AV8InOutIncidentes_Data, 8, 5, 0, 3, "/", ":", " "));
               }
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            ValidateSpaRequest();
            PAJ52( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV59Pgmname = "PromptContagemResultadoIncidentes";
               context.Gx_err = 0;
               WSJ52( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  WEJ52( ) ;
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20206216235424");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("promptcontagemresultadoincidentes.aspx") + "?" + UrlEncode("" +AV7InOutIncidentes_Codigo) + "," + UrlEncode(DateTimeUtil.FormatDateTimeParm(AV8InOutIncidentes_Data))+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDDSC", StringUtil.BoolToStr( AV14OrderedDsc));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR1", AV15DynamicFiltersSelector1);
         GxWebStd.gx_hidden_field( context, "GXH_vINCIDENTES_DATA1", context.localUtil.TToC( AV16Incidentes_Data1, 10, 8, 0, 3, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "GXH_vINCIDENTES_DATA_TO1", context.localUtil.TToC( AV17Incidentes_Data_To1, 10, 8, 0, 3, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR2", AV19DynamicFiltersSelector2);
         GxWebStd.gx_hidden_field( context, "GXH_vINCIDENTES_DATA2", context.localUtil.TToC( AV20Incidentes_Data2, 10, 8, 0, 3, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "GXH_vINCIDENTES_DATA_TO2", context.localUtil.TToC( AV21Incidentes_Data_To2, 10, 8, 0, 3, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR3", AV23DynamicFiltersSelector3);
         GxWebStd.gx_hidden_field( context, "GXH_vINCIDENTES_DATA3", context.localUtil.TToC( AV24Incidentes_Data3, 10, 8, 0, 3, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "GXH_vINCIDENTES_DATA_TO3", context.localUtil.TToC( AV25Incidentes_Data_To3, 10, 8, 0, 3, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED2", StringUtil.BoolToStr( AV18DynamicFiltersEnabled2));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED3", StringUtil.BoolToStr( AV22DynamicFiltersEnabled3));
         GxWebStd.gx_hidden_field( context, "GXH_vTFINCIDENTES_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV31TFIncidentes_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFINCIDENTES_CODIGO_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV32TFIncidentes_Codigo_To), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFINCIDENTES_DATA", context.localUtil.TToC( AV35TFIncidentes_Data, 10, 8, 0, 3, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "GXH_vTFINCIDENTES_DATA_TO", context.localUtil.TToC( AV36TFIncidentes_Data_To, 10, 8, 0, 3, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "GXH_vTFINCIDENTES_DESCRICAO", AV41TFIncidentes_Descricao);
         GxWebStd.gx_hidden_field( context, "GXH_vTFINCIDENTES_DESCRICAO_SEL", AV42TFIncidentes_Descricao_Sel);
         GxWebStd.gx_hidden_field( context, "GXH_vTFINCIDENTES_DEMANDACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV45TFIncidentes_DemandaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFINCIDENTES_DEMANDACOD_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV46TFIncidentes_DemandaCod_To), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFINCIDENTES_DEMANDANUM", AV49TFIncidentes_DemandaNum);
         GxWebStd.gx_hidden_field( context, "GXH_vTFINCIDENTES_DEMANDANUM_SEL", AV50TFIncidentes_DemandaNum_Sel);
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_86", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_86), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV54GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV55GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vDDO_TITLESETTINGSICONS", AV52DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vDDO_TITLESETTINGSICONS", AV52DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vINCIDENTES_CODIGOTITLEFILTERDATA", AV30Incidentes_CodigoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vINCIDENTES_CODIGOTITLEFILTERDATA", AV30Incidentes_CodigoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vINCIDENTES_DATATITLEFILTERDATA", AV34Incidentes_DataTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vINCIDENTES_DATATITLEFILTERDATA", AV34Incidentes_DataTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vINCIDENTES_DESCRICAOTITLEFILTERDATA", AV40Incidentes_DescricaoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vINCIDENTES_DESCRICAOTITLEFILTERDATA", AV40Incidentes_DescricaoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vINCIDENTES_DEMANDACODTITLEFILTERDATA", AV44Incidentes_DemandaCodTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vINCIDENTES_DEMANDACODTITLEFILTERDATA", AV44Incidentes_DemandaCodTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vINCIDENTES_DEMANDANUMTITLEFILTERDATA", AV48Incidentes_DemandaNumTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vINCIDENTES_DEMANDANUMTITLEFILTERDATA", AV48Incidentes_DemandaNumTitleFilterData);
         }
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV59Pgmname));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vGRIDSTATE", AV10GridState);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vGRIDSTATE", AV10GridState);
         }
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSIGNOREFIRST", AV27DynamicFiltersIgnoreFirst);
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSREMOVING", AV26DynamicFiltersRemoving);
         GxWebStd.gx_hidden_field( context, "vINOUTINCIDENTES_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7InOutIncidentes_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINOUTINCIDENTES_DATA", context.localUtil.TToC( AV8InOutIncidentes_Data, 10, 8, 0, 0, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_CODIGO_Caption", StringUtil.RTrim( Ddo_incidentes_codigo_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_CODIGO_Tooltip", StringUtil.RTrim( Ddo_incidentes_codigo_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_CODIGO_Cls", StringUtil.RTrim( Ddo_incidentes_codigo_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_CODIGO_Filteredtext_set", StringUtil.RTrim( Ddo_incidentes_codigo_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_CODIGO_Filteredtextto_set", StringUtil.RTrim( Ddo_incidentes_codigo_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_CODIGO_Dropdownoptionstype", StringUtil.RTrim( Ddo_incidentes_codigo_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_CODIGO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_incidentes_codigo_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_CODIGO_Includesortasc", StringUtil.BoolToStr( Ddo_incidentes_codigo_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_CODIGO_Includesortdsc", StringUtil.BoolToStr( Ddo_incidentes_codigo_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_CODIGO_Sortedstatus", StringUtil.RTrim( Ddo_incidentes_codigo_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_CODIGO_Includefilter", StringUtil.BoolToStr( Ddo_incidentes_codigo_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_CODIGO_Filtertype", StringUtil.RTrim( Ddo_incidentes_codigo_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_CODIGO_Filterisrange", StringUtil.BoolToStr( Ddo_incidentes_codigo_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_CODIGO_Includedatalist", StringUtil.BoolToStr( Ddo_incidentes_codigo_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_CODIGO_Sortasc", StringUtil.RTrim( Ddo_incidentes_codigo_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_CODIGO_Sortdsc", StringUtil.RTrim( Ddo_incidentes_codigo_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_CODIGO_Cleanfilter", StringUtil.RTrim( Ddo_incidentes_codigo_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_CODIGO_Rangefilterfrom", StringUtil.RTrim( Ddo_incidentes_codigo_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_CODIGO_Rangefilterto", StringUtil.RTrim( Ddo_incidentes_codigo_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_CODIGO_Searchbuttontext", StringUtil.RTrim( Ddo_incidentes_codigo_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_DATA_Caption", StringUtil.RTrim( Ddo_incidentes_data_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_DATA_Tooltip", StringUtil.RTrim( Ddo_incidentes_data_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_DATA_Cls", StringUtil.RTrim( Ddo_incidentes_data_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_DATA_Filteredtext_set", StringUtil.RTrim( Ddo_incidentes_data_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_DATA_Filteredtextto_set", StringUtil.RTrim( Ddo_incidentes_data_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_DATA_Dropdownoptionstype", StringUtil.RTrim( Ddo_incidentes_data_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_DATA_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_incidentes_data_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_DATA_Includesortasc", StringUtil.BoolToStr( Ddo_incidentes_data_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_DATA_Includesortdsc", StringUtil.BoolToStr( Ddo_incidentes_data_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_DATA_Sortedstatus", StringUtil.RTrim( Ddo_incidentes_data_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_DATA_Includefilter", StringUtil.BoolToStr( Ddo_incidentes_data_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_DATA_Filtertype", StringUtil.RTrim( Ddo_incidentes_data_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_DATA_Filterisrange", StringUtil.BoolToStr( Ddo_incidentes_data_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_DATA_Includedatalist", StringUtil.BoolToStr( Ddo_incidentes_data_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_DATA_Sortasc", StringUtil.RTrim( Ddo_incidentes_data_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_DATA_Sortdsc", StringUtil.RTrim( Ddo_incidentes_data_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_DATA_Cleanfilter", StringUtil.RTrim( Ddo_incidentes_data_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_DATA_Rangefilterfrom", StringUtil.RTrim( Ddo_incidentes_data_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_DATA_Rangefilterto", StringUtil.RTrim( Ddo_incidentes_data_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_DATA_Searchbuttontext", StringUtil.RTrim( Ddo_incidentes_data_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_DESCRICAO_Caption", StringUtil.RTrim( Ddo_incidentes_descricao_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_DESCRICAO_Tooltip", StringUtil.RTrim( Ddo_incidentes_descricao_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_DESCRICAO_Cls", StringUtil.RTrim( Ddo_incidentes_descricao_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_DESCRICAO_Filteredtext_set", StringUtil.RTrim( Ddo_incidentes_descricao_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_DESCRICAO_Selectedvalue_set", StringUtil.RTrim( Ddo_incidentes_descricao_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_DESCRICAO_Dropdownoptionstype", StringUtil.RTrim( Ddo_incidentes_descricao_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_DESCRICAO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_incidentes_descricao_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_DESCRICAO_Includesortasc", StringUtil.BoolToStr( Ddo_incidentes_descricao_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_DESCRICAO_Includesortdsc", StringUtil.BoolToStr( Ddo_incidentes_descricao_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_DESCRICAO_Sortedstatus", StringUtil.RTrim( Ddo_incidentes_descricao_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_DESCRICAO_Includefilter", StringUtil.BoolToStr( Ddo_incidentes_descricao_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_DESCRICAO_Filtertype", StringUtil.RTrim( Ddo_incidentes_descricao_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_DESCRICAO_Filterisrange", StringUtil.BoolToStr( Ddo_incidentes_descricao_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_DESCRICAO_Includedatalist", StringUtil.BoolToStr( Ddo_incidentes_descricao_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_DESCRICAO_Datalisttype", StringUtil.RTrim( Ddo_incidentes_descricao_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_DESCRICAO_Datalistproc", StringUtil.RTrim( Ddo_incidentes_descricao_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_DESCRICAO_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_incidentes_descricao_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_DESCRICAO_Sortasc", StringUtil.RTrim( Ddo_incidentes_descricao_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_DESCRICAO_Sortdsc", StringUtil.RTrim( Ddo_incidentes_descricao_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_DESCRICAO_Loadingdata", StringUtil.RTrim( Ddo_incidentes_descricao_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_DESCRICAO_Cleanfilter", StringUtil.RTrim( Ddo_incidentes_descricao_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_DESCRICAO_Noresultsfound", StringUtil.RTrim( Ddo_incidentes_descricao_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_DESCRICAO_Searchbuttontext", StringUtil.RTrim( Ddo_incidentes_descricao_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_DEMANDACOD_Caption", StringUtil.RTrim( Ddo_incidentes_demandacod_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_DEMANDACOD_Tooltip", StringUtil.RTrim( Ddo_incidentes_demandacod_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_DEMANDACOD_Cls", StringUtil.RTrim( Ddo_incidentes_demandacod_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_DEMANDACOD_Filteredtext_set", StringUtil.RTrim( Ddo_incidentes_demandacod_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_DEMANDACOD_Filteredtextto_set", StringUtil.RTrim( Ddo_incidentes_demandacod_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_DEMANDACOD_Dropdownoptionstype", StringUtil.RTrim( Ddo_incidentes_demandacod_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_DEMANDACOD_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_incidentes_demandacod_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_DEMANDACOD_Includesortasc", StringUtil.BoolToStr( Ddo_incidentes_demandacod_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_DEMANDACOD_Includesortdsc", StringUtil.BoolToStr( Ddo_incidentes_demandacod_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_DEMANDACOD_Sortedstatus", StringUtil.RTrim( Ddo_incidentes_demandacod_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_DEMANDACOD_Includefilter", StringUtil.BoolToStr( Ddo_incidentes_demandacod_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_DEMANDACOD_Filtertype", StringUtil.RTrim( Ddo_incidentes_demandacod_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_DEMANDACOD_Filterisrange", StringUtil.BoolToStr( Ddo_incidentes_demandacod_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_DEMANDACOD_Includedatalist", StringUtil.BoolToStr( Ddo_incidentes_demandacod_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_DEMANDACOD_Sortasc", StringUtil.RTrim( Ddo_incidentes_demandacod_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_DEMANDACOD_Sortdsc", StringUtil.RTrim( Ddo_incidentes_demandacod_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_DEMANDACOD_Cleanfilter", StringUtil.RTrim( Ddo_incidentes_demandacod_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_DEMANDACOD_Rangefilterfrom", StringUtil.RTrim( Ddo_incidentes_demandacod_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_DEMANDACOD_Rangefilterto", StringUtil.RTrim( Ddo_incidentes_demandacod_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_DEMANDACOD_Searchbuttontext", StringUtil.RTrim( Ddo_incidentes_demandacod_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_DEMANDANUM_Caption", StringUtil.RTrim( Ddo_incidentes_demandanum_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_DEMANDANUM_Tooltip", StringUtil.RTrim( Ddo_incidentes_demandanum_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_DEMANDANUM_Cls", StringUtil.RTrim( Ddo_incidentes_demandanum_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_DEMANDANUM_Filteredtext_set", StringUtil.RTrim( Ddo_incidentes_demandanum_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_DEMANDANUM_Selectedvalue_set", StringUtil.RTrim( Ddo_incidentes_demandanum_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_DEMANDANUM_Dropdownoptionstype", StringUtil.RTrim( Ddo_incidentes_demandanum_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_DEMANDANUM_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_incidentes_demandanum_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_DEMANDANUM_Includesortasc", StringUtil.BoolToStr( Ddo_incidentes_demandanum_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_DEMANDANUM_Includesortdsc", StringUtil.BoolToStr( Ddo_incidentes_demandanum_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_DEMANDANUM_Sortedstatus", StringUtil.RTrim( Ddo_incidentes_demandanum_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_DEMANDANUM_Includefilter", StringUtil.BoolToStr( Ddo_incidentes_demandanum_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_DEMANDANUM_Filtertype", StringUtil.RTrim( Ddo_incidentes_demandanum_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_DEMANDANUM_Filterisrange", StringUtil.BoolToStr( Ddo_incidentes_demandanum_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_DEMANDANUM_Includedatalist", StringUtil.BoolToStr( Ddo_incidentes_demandanum_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_DEMANDANUM_Datalisttype", StringUtil.RTrim( Ddo_incidentes_demandanum_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_DEMANDANUM_Datalistproc", StringUtil.RTrim( Ddo_incidentes_demandanum_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_DEMANDANUM_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_incidentes_demandanum_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_DEMANDANUM_Sortasc", StringUtil.RTrim( Ddo_incidentes_demandanum_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_DEMANDANUM_Sortdsc", StringUtil.RTrim( Ddo_incidentes_demandanum_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_DEMANDANUM_Loadingdata", StringUtil.RTrim( Ddo_incidentes_demandanum_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_DEMANDANUM_Cleanfilter", StringUtil.RTrim( Ddo_incidentes_demandanum_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_DEMANDANUM_Noresultsfound", StringUtil.RTrim( Ddo_incidentes_demandanum_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_DEMANDANUM_Searchbuttontext", StringUtil.RTrim( Ddo_incidentes_demandanum_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_CODIGO_Activeeventkey", StringUtil.RTrim( Ddo_incidentes_codigo_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_CODIGO_Filteredtext_get", StringUtil.RTrim( Ddo_incidentes_codigo_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_CODIGO_Filteredtextto_get", StringUtil.RTrim( Ddo_incidentes_codigo_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_DATA_Activeeventkey", StringUtil.RTrim( Ddo_incidentes_data_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_DATA_Filteredtext_get", StringUtil.RTrim( Ddo_incidentes_data_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_DATA_Filteredtextto_get", StringUtil.RTrim( Ddo_incidentes_data_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_DESCRICAO_Activeeventkey", StringUtil.RTrim( Ddo_incidentes_descricao_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_DESCRICAO_Filteredtext_get", StringUtil.RTrim( Ddo_incidentes_descricao_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_DESCRICAO_Selectedvalue_get", StringUtil.RTrim( Ddo_incidentes_descricao_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_DEMANDACOD_Activeeventkey", StringUtil.RTrim( Ddo_incidentes_demandacod_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_DEMANDACOD_Filteredtext_get", StringUtil.RTrim( Ddo_incidentes_demandacod_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_DEMANDACOD_Filteredtextto_get", StringUtil.RTrim( Ddo_incidentes_demandacod_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_DEMANDANUM_Activeeventkey", StringUtil.RTrim( Ddo_incidentes_demandanum_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_DEMANDANUM_Filteredtext_get", StringUtil.RTrim( Ddo_incidentes_demandanum_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_INCIDENTES_DEMANDANUM_Selectedvalue_get", StringUtil.RTrim( Ddo_incidentes_demandanum_Selectedvalue_get));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseFormJ52( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
         context.WriteHtmlTextNl( "</body>") ;
         context.WriteHtmlTextNl( "</html>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
      }

      public override String GetPgmname( )
      {
         return "PromptContagemResultadoIncidentes" ;
      }

      public override String GetPgmdesc( )
      {
         return "Selecione Contagem Resultado Incidentes" ;
      }

      protected void WBJ50( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            RenderHtmlHeaders( ) ;
            RenderHtmlOpenForm( ) ;
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, "", "", "", "false");
            wb_table1_2_J52( true) ;
         }
         else
         {
            wb_table1_2_J52( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_J52e( bool wbgen )
      {
         if ( wbgen )
         {
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 97,'',false,'" + sGXsfl_86_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled2_Internalname, StringUtil.BoolToStr( AV18DynamicFiltersEnabled2), "", "", chkavDynamicfiltersenabled2.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(97, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,97);\"");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 98,'',false,'" + sGXsfl_86_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled3_Internalname, StringUtil.BoolToStr( AV22DynamicFiltersEnabled3), "", "", chkavDynamicfiltersenabled3.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(98, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,98);\"");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 99,'',false,'" + sGXsfl_86_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfincidentes_codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV31TFIncidentes_Codigo), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV31TFIncidentes_Codigo), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,99);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfincidentes_codigo_Jsonclick, 0, "Attribute", "", "", "", edtavTfincidentes_codigo_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContagemResultadoIncidentes.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 100,'',false,'" + sGXsfl_86_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfincidentes_codigo_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV32TFIncidentes_Codigo_To), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV32TFIncidentes_Codigo_To), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,100);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfincidentes_codigo_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfincidentes_codigo_to_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContagemResultadoIncidentes.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 101,'',false,'" + sGXsfl_86_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTfincidentes_data_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTfincidentes_data_Internalname, context.localUtil.TToC( AV35TFIncidentes_Data, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( AV35TFIncidentes_Data, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,101);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfincidentes_data_Jsonclick, 0, "Attribute", "", "", "", edtavTfincidentes_data_Visible, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContagemResultadoIncidentes.htm");
            GxWebStd.gx_bitmap( context, edtavTfincidentes_data_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTfincidentes_data_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptContagemResultadoIncidentes.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 102,'',false,'" + sGXsfl_86_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTfincidentes_data_to_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTfincidentes_data_to_Internalname, context.localUtil.TToC( AV36TFIncidentes_Data_To, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( AV36TFIncidentes_Data_To, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,102);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfincidentes_data_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfincidentes_data_to_Visible, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContagemResultadoIncidentes.htm");
            GxWebStd.gx_bitmap( context, edtavTfincidentes_data_to_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTfincidentes_data_to_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptContagemResultadoIncidentes.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divDdo_incidentes_dataauxdates_Internalname, 1, 0, "px", 0, "px", "Invisible", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 104,'',false,'" + sGXsfl_86_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_incidentes_dataauxdate_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_incidentes_dataauxdate_Internalname, context.localUtil.Format(AV37DDO_Incidentes_DataAuxDate, "99/99/99"), context.localUtil.Format( AV37DDO_Incidentes_DataAuxDate, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,104);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_incidentes_dataauxdate_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContagemResultadoIncidentes.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_incidentes_dataauxdate_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptContagemResultadoIncidentes.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 105,'',false,'" + sGXsfl_86_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_incidentes_dataauxdateto_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_incidentes_dataauxdateto_Internalname, context.localUtil.Format(AV38DDO_Incidentes_DataAuxDateTo, "99/99/99"), context.localUtil.Format( AV38DDO_Incidentes_DataAuxDateTo, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,105);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_incidentes_dataauxdateto_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContagemResultadoIncidentes.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_incidentes_dataauxdateto_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptContagemResultadoIncidentes.htm");
            context.WriteHtmlTextNl( "</div>") ;
            GxWebStd.gx_div_end( context, "left", "top");
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 106,'',false,'" + sGXsfl_86_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavTfincidentes_descricao_Internalname, AV41TFIncidentes_Descricao, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,106);\"", 0, edtavTfincidentes_descricao_Visible, 1, 0, 80, "chr", 3, "row", StyleString, ClassString, "", "200", -1, "", "", -1, true, "", "HLP_PromptContagemResultadoIncidentes.htm");
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 107,'',false,'" + sGXsfl_86_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavTfincidentes_descricao_sel_Internalname, AV42TFIncidentes_Descricao_Sel, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,107);\"", 0, edtavTfincidentes_descricao_sel_Visible, 1, 0, 80, "chr", 3, "row", StyleString, ClassString, "", "200", -1, "", "", -1, true, "", "HLP_PromptContagemResultadoIncidentes.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 108,'',false,'" + sGXsfl_86_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfincidentes_demandacod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV45TFIncidentes_DemandaCod), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV45TFIncidentes_DemandaCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,108);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfincidentes_demandacod_Jsonclick, 0, "Attribute", "", "", "", edtavTfincidentes_demandacod_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContagemResultadoIncidentes.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 109,'',false,'" + sGXsfl_86_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfincidentes_demandacod_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV46TFIncidentes_DemandaCod_To), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV46TFIncidentes_DemandaCod_To), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,109);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfincidentes_demandacod_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfincidentes_demandacod_to_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContagemResultadoIncidentes.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 110,'',false,'" + sGXsfl_86_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfincidentes_demandanum_Internalname, AV49TFIncidentes_DemandaNum, StringUtil.RTrim( context.localUtil.Format( AV49TFIncidentes_DemandaNum, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,110);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfincidentes_demandanum_Jsonclick, 0, "Attribute", "", "", "", edtavTfincidentes_demandanum_Visible, 1, 0, "text", "", 30, "chr", 1, "row", 30, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptContagemResultadoIncidentes.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 111,'',false,'" + sGXsfl_86_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfincidentes_demandanum_sel_Internalname, AV50TFIncidentes_DemandaNum_Sel, StringUtil.RTrim( context.localUtil.Format( AV50TFIncidentes_DemandaNum_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,111);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfincidentes_demandanum_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfincidentes_demandanum_sel_Visible, 1, 0, "text", "", 30, "chr", 1, "row", 30, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptContagemResultadoIncidentes.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_INCIDENTES_CODIGOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 113,'',false,'" + sGXsfl_86_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_incidentes_codigotitlecontrolidtoreplace_Internalname, AV33ddo_Incidentes_CodigoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,113);\"", 0, edtavDdo_incidentes_codigotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptContagemResultadoIncidentes.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_INCIDENTES_DATAContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 115,'',false,'" + sGXsfl_86_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_incidentes_datatitlecontrolidtoreplace_Internalname, AV39ddo_Incidentes_DataTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,115);\"", 0, edtavDdo_incidentes_datatitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptContagemResultadoIncidentes.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_INCIDENTES_DESCRICAOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 117,'',false,'" + sGXsfl_86_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_incidentes_descricaotitlecontrolidtoreplace_Internalname, AV43ddo_Incidentes_DescricaoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,117);\"", 0, edtavDdo_incidentes_descricaotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptContagemResultadoIncidentes.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_INCIDENTES_DEMANDACODContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 119,'',false,'" + sGXsfl_86_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_incidentes_demandacodtitlecontrolidtoreplace_Internalname, AV47ddo_Incidentes_DemandaCodTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,119);\"", 0, edtavDdo_incidentes_demandacodtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptContagemResultadoIncidentes.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_INCIDENTES_DEMANDANUMContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 121,'',false,'" + sGXsfl_86_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_incidentes_demandanumtitlecontrolidtoreplace_Internalname, AV51ddo_Incidentes_DemandaNumTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,121);\"", 0, edtavDdo_incidentes_demandanumtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptContagemResultadoIncidentes.htm");
         }
         wbLoad = true;
      }

      protected void STARTJ52( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Selecione Contagem Resultado Incidentes", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPJ50( ) ;
      }

      protected void WSJ52( )
      {
         STARTJ52( ) ;
         EVTJ52( ) ;
      }

      protected void EVTJ52( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E11J52 */
                           E11J52 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_INCIDENTES_CODIGO.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E12J52 */
                           E12J52 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_INCIDENTES_DATA.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E13J52 */
                           E13J52 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_INCIDENTES_DESCRICAO.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E14J52 */
                           E14J52 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_INCIDENTES_DEMANDACOD.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E15J52 */
                           E15J52 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_INCIDENTES_DEMANDANUM.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E16J52 */
                           E16J52 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VORDEREDBY.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E17J52 */
                           E17J52 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS1'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E18J52 */
                           E18J52 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS2'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E19J52 */
                           E19J52 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS3'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E20J52 */
                           E20J52 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'DOCLEANFILTERS'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E21J52 */
                           E21J52 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS1'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E22J52 */
                           E22J52 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR1.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E23J52 */
                           E23J52 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS2'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E24J52 */
                           E24J52 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR2.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E25J52 */
                           E25J52 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR3.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E26J52 */
                           E26J52 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           dynload_actions( ) ;
                        }
                     }
                     else
                     {
                        sEvtType = StringUtil.Right( sEvt, 4);
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                        if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) )
                        {
                           nGXsfl_86_idx = (short)(NumberUtil.Val( sEvtType, "."));
                           sGXsfl_86_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_86_idx), 4, 0)), 4, "0");
                           SubsflControlProps_862( ) ;
                           AV28Select = cgiGet( edtavSelect_Internalname);
                           context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavSelect_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV28Select)) ? AV58Select_GXI : context.convertURL( context.PathToRelativeUrl( AV28Select))));
                           A1241Incidentes_Codigo = (int)(context.localUtil.CToN( cgiGet( edtIncidentes_Codigo_Internalname), ",", "."));
                           A1242Incidentes_Data = context.localUtil.CToT( cgiGet( edtIncidentes_Data_Internalname), 0);
                           A1243Incidentes_Descricao = cgiGet( edtIncidentes_Descricao_Internalname);
                           A1239Incidentes_DemandaCod = (int)(context.localUtil.CToN( cgiGet( edtIncidentes_DemandaCod_Internalname), ",", "."));
                           A1240Incidentes_DemandaNum = StringUtil.Upper( cgiGet( edtIncidentes_DemandaNum_Internalname));
                           n1240Incidentes_DemandaNum = false;
                           sEvtType = StringUtil.Right( sEvt, 1);
                           if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                           {
                              sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                              if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E27J52 */
                                 E27J52 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E28J52 */
                                 E28J52 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E29J52 */
                                 E29J52 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    Rfr0gs = false;
                                    /* Set Refresh If Orderedby Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Ordereddsc Changed */
                                    if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersselector1 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Incidentes_data1 Changed */
                                    if ( context.localUtil.CToT( cgiGet( "GXH_vINCIDENTES_DATA1"), 0) != AV16Incidentes_Data1 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Incidentes_data_to1 Changed */
                                    if ( context.localUtil.CToT( cgiGet( "GXH_vINCIDENTES_DATA_TO1"), 0) != AV17Incidentes_Data_To1 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersselector2 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV19DynamicFiltersSelector2) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Incidentes_data2 Changed */
                                    if ( context.localUtil.CToT( cgiGet( "GXH_vINCIDENTES_DATA2"), 0) != AV20Incidentes_Data2 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Incidentes_data_to2 Changed */
                                    if ( context.localUtil.CToT( cgiGet( "GXH_vINCIDENTES_DATA_TO2"), 0) != AV21Incidentes_Data_To2 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersselector3 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV23DynamicFiltersSelector3) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Incidentes_data3 Changed */
                                    if ( context.localUtil.CToT( cgiGet( "GXH_vINCIDENTES_DATA3"), 0) != AV24Incidentes_Data3 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Incidentes_data_to3 Changed */
                                    if ( context.localUtil.CToT( cgiGet( "GXH_vINCIDENTES_DATA_TO3"), 0) != AV25Incidentes_Data_To3 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersenabled2 Changed */
                                    if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV18DynamicFiltersEnabled2 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersenabled3 Changed */
                                    if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV22DynamicFiltersEnabled3 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfincidentes_codigo Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFINCIDENTES_CODIGO"), ",", ".") != Convert.ToDecimal( AV31TFIncidentes_Codigo )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfincidentes_codigo_to Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFINCIDENTES_CODIGO_TO"), ",", ".") != Convert.ToDecimal( AV32TFIncidentes_Codigo_To )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfincidentes_data Changed */
                                    if ( context.localUtil.CToT( cgiGet( "GXH_vTFINCIDENTES_DATA"), 0) != AV35TFIncidentes_Data )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfincidentes_data_to Changed */
                                    if ( context.localUtil.CToT( cgiGet( "GXH_vTFINCIDENTES_DATA_TO"), 0) != AV36TFIncidentes_Data_To )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfincidentes_descricao Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFINCIDENTES_DESCRICAO"), AV41TFIncidentes_Descricao) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfincidentes_descricao_sel Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFINCIDENTES_DESCRICAO_SEL"), AV42TFIncidentes_Descricao_Sel) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfincidentes_demandacod Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFINCIDENTES_DEMANDACOD"), ",", ".") != Convert.ToDecimal( AV45TFIncidentes_DemandaCod )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfincidentes_demandacod_to Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFINCIDENTES_DEMANDACOD_TO"), ",", ".") != Convert.ToDecimal( AV46TFIncidentes_DemandaCod_To )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfincidentes_demandanum Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFINCIDENTES_DEMANDANUM"), AV49TFIncidentes_DemandaNum) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tfincidentes_demandanum_sel Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFINCIDENTES_DEMANDANUM_SEL"), AV50TFIncidentes_DemandaNum_Sel) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    if ( ! Rfr0gs )
                                    {
                                       /* Execute user event: E30J52 */
                                       E30J52 ();
                                    }
                                    dynload_actions( ) ;
                                 }
                              }
                              else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                              }
                           }
                           else
                           {
                           }
                        }
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void WEJ52( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormJ52( ) ;
            }
         }
      }

      protected void PAJ52( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbavOrderedby.Name = "vORDEREDBY";
            cmbavOrderedby.WebTags = "";
            if ( cmbavOrderedby.ItemCount > 0 )
            {
               AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            }
            cmbavDynamicfiltersselector1.Name = "vDYNAMICFILTERSSELECTOR1";
            cmbavDynamicfiltersselector1.WebTags = "";
            cmbavDynamicfiltersselector1.addItem("INCIDENTES_DATA", "Data", 0);
            if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
            {
               AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            }
            cmbavDynamicfiltersselector2.Name = "vDYNAMICFILTERSSELECTOR2";
            cmbavDynamicfiltersselector2.WebTags = "";
            cmbavDynamicfiltersselector2.addItem("INCIDENTES_DATA", "Data", 0);
            if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
            {
               AV19DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV19DynamicFiltersSelector2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
            }
            cmbavDynamicfiltersselector3.Name = "vDYNAMICFILTERSSELECTOR3";
            cmbavDynamicfiltersselector3.WebTags = "";
            cmbavDynamicfiltersselector3.addItem("INCIDENTES_DATA", "Data", 0);
            if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
            {
               AV23DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV23DynamicFiltersSelector3);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
            }
            chkavDynamicfiltersenabled2.Name = "vDYNAMICFILTERSENABLED2";
            chkavDynamicfiltersenabled2.WebTags = "";
            chkavDynamicfiltersenabled2.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "TitleCaption", chkavDynamicfiltersenabled2.Caption);
            chkavDynamicfiltersenabled2.CheckedValue = "false";
            chkavDynamicfiltersenabled3.Name = "vDYNAMICFILTERSENABLED3";
            chkavDynamicfiltersenabled3.WebTags = "";
            chkavDynamicfiltersenabled3.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "TitleCaption", chkavDynamicfiltersenabled3.Caption);
            chkavDynamicfiltersenabled3.CheckedValue = "false";
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_862( ) ;
         while ( nGXsfl_86_idx <= nRC_GXsfl_86 )
         {
            sendrow_862( ) ;
            nGXsfl_86_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_86_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_86_idx+1));
            sGXsfl_86_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_86_idx), 4, 0)), 4, "0");
            SubsflControlProps_862( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV13OrderedBy ,
                                       bool AV14OrderedDsc ,
                                       String AV15DynamicFiltersSelector1 ,
                                       DateTime AV16Incidentes_Data1 ,
                                       DateTime AV17Incidentes_Data_To1 ,
                                       String AV19DynamicFiltersSelector2 ,
                                       DateTime AV20Incidentes_Data2 ,
                                       DateTime AV21Incidentes_Data_To2 ,
                                       String AV23DynamicFiltersSelector3 ,
                                       DateTime AV24Incidentes_Data3 ,
                                       DateTime AV25Incidentes_Data_To3 ,
                                       bool AV18DynamicFiltersEnabled2 ,
                                       bool AV22DynamicFiltersEnabled3 ,
                                       int AV31TFIncidentes_Codigo ,
                                       int AV32TFIncidentes_Codigo_To ,
                                       DateTime AV35TFIncidentes_Data ,
                                       DateTime AV36TFIncidentes_Data_To ,
                                       String AV41TFIncidentes_Descricao ,
                                       String AV42TFIncidentes_Descricao_Sel ,
                                       int AV45TFIncidentes_DemandaCod ,
                                       int AV46TFIncidentes_DemandaCod_To ,
                                       String AV49TFIncidentes_DemandaNum ,
                                       String AV50TFIncidentes_DemandaNum_Sel ,
                                       String AV33ddo_Incidentes_CodigoTitleControlIdToReplace ,
                                       String AV39ddo_Incidentes_DataTitleControlIdToReplace ,
                                       String AV43ddo_Incidentes_DescricaoTitleControlIdToReplace ,
                                       String AV47ddo_Incidentes_DemandaCodTitleControlIdToReplace ,
                                       String AV51ddo_Incidentes_DemandaNumTitleControlIdToReplace ,
                                       String AV59Pgmname ,
                                       wwpbaseobjects.SdtWWPGridState AV10GridState ,
                                       bool AV27DynamicFiltersIgnoreFirst ,
                                       bool AV26DynamicFiltersRemoving )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RFJ52( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_INCIDENTES_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A1241Incidentes_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "INCIDENTES_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1241Incidentes_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_INCIDENTES_DATA", GetSecureSignedToken( "", context.localUtil.Format( A1242Incidentes_Data, "99/99/99 99:99")));
         GxWebStd.gx_hidden_field( context, "INCIDENTES_DATA", context.localUtil.TToC( A1242Incidentes_Data, 10, 8, 0, 3, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "gxhash_INCIDENTES_DESCRICAO", GetSecureSignedToken( "", A1243Incidentes_Descricao));
         GxWebStd.gx_hidden_field( context, "INCIDENTES_DESCRICAO", A1243Incidentes_Descricao);
         GxWebStd.gx_hidden_field( context, "gxhash_INCIDENTES_DEMANDACOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A1239Incidentes_DemandaCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "INCIDENTES_DEMANDACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1239Incidentes_DemandaCod), 6, 0, ".", "")));
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavOrderedby.ItemCount > 0 )
         {
            AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
         {
            AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         }
         if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
         {
            AV19DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV19DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         }
         if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
         {
            AV23DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV23DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFJ52( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV59Pgmname = "PromptContagemResultadoIncidentes";
         context.Gx_err = 0;
      }

      protected void RFJ52( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 86;
         /* Execute user event: E28J52 */
         E28J52 ();
         nGXsfl_86_idx = 1;
         sGXsfl_86_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_86_idx), 4, 0)), 4, "0");
         SubsflControlProps_862( ) ;
         nGXsfl_86_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", "");
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_862( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 AV15DynamicFiltersSelector1 ,
                                                 AV16Incidentes_Data1 ,
                                                 AV17Incidentes_Data_To1 ,
                                                 AV18DynamicFiltersEnabled2 ,
                                                 AV19DynamicFiltersSelector2 ,
                                                 AV20Incidentes_Data2 ,
                                                 AV21Incidentes_Data_To2 ,
                                                 AV22DynamicFiltersEnabled3 ,
                                                 AV23DynamicFiltersSelector3 ,
                                                 AV24Incidentes_Data3 ,
                                                 AV25Incidentes_Data_To3 ,
                                                 AV31TFIncidentes_Codigo ,
                                                 AV32TFIncidentes_Codigo_To ,
                                                 AV35TFIncidentes_Data ,
                                                 AV36TFIncidentes_Data_To ,
                                                 AV42TFIncidentes_Descricao_Sel ,
                                                 AV41TFIncidentes_Descricao ,
                                                 AV45TFIncidentes_DemandaCod ,
                                                 AV46TFIncidentes_DemandaCod_To ,
                                                 AV50TFIncidentes_DemandaNum_Sel ,
                                                 AV49TFIncidentes_DemandaNum ,
                                                 A1242Incidentes_Data ,
                                                 A1241Incidentes_Codigo ,
                                                 A1243Incidentes_Descricao ,
                                                 A1239Incidentes_DemandaCod ,
                                                 A1240Incidentes_DemandaNum ,
                                                 AV13OrderedBy ,
                                                 AV14OrderedDsc },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.DATE,
                                                 TypeConstants.DATE, TypeConstants.INT, TypeConstants.INT, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING,
                                                 TypeConstants.STRING, TypeConstants.DATE, TypeConstants.INT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                                 }
            });
            lV41TFIncidentes_Descricao = StringUtil.Concat( StringUtil.RTrim( AV41TFIncidentes_Descricao), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41TFIncidentes_Descricao", AV41TFIncidentes_Descricao);
            lV49TFIncidentes_DemandaNum = StringUtil.Concat( StringUtil.RTrim( AV49TFIncidentes_DemandaNum), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49TFIncidentes_DemandaNum", AV49TFIncidentes_DemandaNum);
            /* Using cursor H00J52 */
            pr_default.execute(0, new Object[] {AV16Incidentes_Data1, AV17Incidentes_Data_To1, AV20Incidentes_Data2, AV21Incidentes_Data_To2, AV24Incidentes_Data3, AV25Incidentes_Data_To3, AV31TFIncidentes_Codigo, AV32TFIncidentes_Codigo_To, AV35TFIncidentes_Data, AV36TFIncidentes_Data_To, lV41TFIncidentes_Descricao, AV42TFIncidentes_Descricao_Sel, AV45TFIncidentes_DemandaCod, AV46TFIncidentes_DemandaCod_To, lV49TFIncidentes_DemandaNum, AV50TFIncidentes_DemandaNum_Sel, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_86_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A1240Incidentes_DemandaNum = H00J52_A1240Incidentes_DemandaNum[0];
               n1240Incidentes_DemandaNum = H00J52_n1240Incidentes_DemandaNum[0];
               A1239Incidentes_DemandaCod = H00J52_A1239Incidentes_DemandaCod[0];
               A1243Incidentes_Descricao = H00J52_A1243Incidentes_Descricao[0];
               A1242Incidentes_Data = H00J52_A1242Incidentes_Data[0];
               A1241Incidentes_Codigo = H00J52_A1241Incidentes_Codigo[0];
               A1240Incidentes_DemandaNum = H00J52_A1240Incidentes_DemandaNum[0];
               n1240Incidentes_DemandaNum = H00J52_n1240Incidentes_DemandaNum[0];
               /* Execute user event: E29J52 */
               E29J52 ();
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 86;
            WBJ50( ) ;
         }
         nGXsfl_86_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV15DynamicFiltersSelector1 ,
                                              AV16Incidentes_Data1 ,
                                              AV17Incidentes_Data_To1 ,
                                              AV18DynamicFiltersEnabled2 ,
                                              AV19DynamicFiltersSelector2 ,
                                              AV20Incidentes_Data2 ,
                                              AV21Incidentes_Data_To2 ,
                                              AV22DynamicFiltersEnabled3 ,
                                              AV23DynamicFiltersSelector3 ,
                                              AV24Incidentes_Data3 ,
                                              AV25Incidentes_Data_To3 ,
                                              AV31TFIncidentes_Codigo ,
                                              AV32TFIncidentes_Codigo_To ,
                                              AV35TFIncidentes_Data ,
                                              AV36TFIncidentes_Data_To ,
                                              AV42TFIncidentes_Descricao_Sel ,
                                              AV41TFIncidentes_Descricao ,
                                              AV45TFIncidentes_DemandaCod ,
                                              AV46TFIncidentes_DemandaCod_To ,
                                              AV50TFIncidentes_DemandaNum_Sel ,
                                              AV49TFIncidentes_DemandaNum ,
                                              A1242Incidentes_Data ,
                                              A1241Incidentes_Codigo ,
                                              A1243Incidentes_Descricao ,
                                              A1239Incidentes_DemandaCod ,
                                              A1240Incidentes_DemandaNum ,
                                              AV13OrderedBy ,
                                              AV14OrderedDsc },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.DATE,
                                              TypeConstants.DATE, TypeConstants.INT, TypeConstants.INT, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.DATE, TypeConstants.INT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                              }
         });
         lV41TFIncidentes_Descricao = StringUtil.Concat( StringUtil.RTrim( AV41TFIncidentes_Descricao), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41TFIncidentes_Descricao", AV41TFIncidentes_Descricao);
         lV49TFIncidentes_DemandaNum = StringUtil.Concat( StringUtil.RTrim( AV49TFIncidentes_DemandaNum), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49TFIncidentes_DemandaNum", AV49TFIncidentes_DemandaNum);
         /* Using cursor H00J53 */
         pr_default.execute(1, new Object[] {AV16Incidentes_Data1, AV17Incidentes_Data_To1, AV20Incidentes_Data2, AV21Incidentes_Data_To2, AV24Incidentes_Data3, AV25Incidentes_Data_To3, AV31TFIncidentes_Codigo, AV32TFIncidentes_Codigo_To, AV35TFIncidentes_Data, AV36TFIncidentes_Data_To, lV41TFIncidentes_Descricao, AV42TFIncidentes_Descricao_Sel, AV45TFIncidentes_DemandaCod, AV46TFIncidentes_DemandaCod_To, lV49TFIncidentes_DemandaNum, AV50TFIncidentes_DemandaNum_Sel});
         GRID_nRecordCount = H00J53_AGRID_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16Incidentes_Data1, AV17Incidentes_Data_To1, AV19DynamicFiltersSelector2, AV20Incidentes_Data2, AV21Incidentes_Data_To2, AV23DynamicFiltersSelector3, AV24Incidentes_Data3, AV25Incidentes_Data_To3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV31TFIncidentes_Codigo, AV32TFIncidentes_Codigo_To, AV35TFIncidentes_Data, AV36TFIncidentes_Data_To, AV41TFIncidentes_Descricao, AV42TFIncidentes_Descricao_Sel, AV45TFIncidentes_DemandaCod, AV46TFIncidentes_DemandaCod_To, AV49TFIncidentes_DemandaNum, AV50TFIncidentes_DemandaNum_Sel, AV33ddo_Incidentes_CodigoTitleControlIdToReplace, AV39ddo_Incidentes_DataTitleControlIdToReplace, AV43ddo_Incidentes_DescricaoTitleControlIdToReplace, AV47ddo_Incidentes_DemandaCodTitleControlIdToReplace, AV51ddo_Incidentes_DemandaNumTitleControlIdToReplace, AV59Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16Incidentes_Data1, AV17Incidentes_Data_To1, AV19DynamicFiltersSelector2, AV20Incidentes_Data2, AV21Incidentes_Data_To2, AV23DynamicFiltersSelector3, AV24Incidentes_Data3, AV25Incidentes_Data_To3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV31TFIncidentes_Codigo, AV32TFIncidentes_Codigo_To, AV35TFIncidentes_Data, AV36TFIncidentes_Data_To, AV41TFIncidentes_Descricao, AV42TFIncidentes_Descricao_Sel, AV45TFIncidentes_DemandaCod, AV46TFIncidentes_DemandaCod_To, AV49TFIncidentes_DemandaNum, AV50TFIncidentes_DemandaNum_Sel, AV33ddo_Incidentes_CodigoTitleControlIdToReplace, AV39ddo_Incidentes_DataTitleControlIdToReplace, AV43ddo_Incidentes_DescricaoTitleControlIdToReplace, AV47ddo_Incidentes_DemandaCodTitleControlIdToReplace, AV51ddo_Incidentes_DemandaNumTitleControlIdToReplace, AV59Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16Incidentes_Data1, AV17Incidentes_Data_To1, AV19DynamicFiltersSelector2, AV20Incidentes_Data2, AV21Incidentes_Data_To2, AV23DynamicFiltersSelector3, AV24Incidentes_Data3, AV25Incidentes_Data_To3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV31TFIncidentes_Codigo, AV32TFIncidentes_Codigo_To, AV35TFIncidentes_Data, AV36TFIncidentes_Data_To, AV41TFIncidentes_Descricao, AV42TFIncidentes_Descricao_Sel, AV45TFIncidentes_DemandaCod, AV46TFIncidentes_DemandaCod_To, AV49TFIncidentes_DemandaNum, AV50TFIncidentes_DemandaNum_Sel, AV33ddo_Incidentes_CodigoTitleControlIdToReplace, AV39ddo_Incidentes_DataTitleControlIdToReplace, AV43ddo_Incidentes_DescricaoTitleControlIdToReplace, AV47ddo_Incidentes_DemandaCodTitleControlIdToReplace, AV51ddo_Incidentes_DemandaNumTitleControlIdToReplace, AV59Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16Incidentes_Data1, AV17Incidentes_Data_To1, AV19DynamicFiltersSelector2, AV20Incidentes_Data2, AV21Incidentes_Data_To2, AV23DynamicFiltersSelector3, AV24Incidentes_Data3, AV25Incidentes_Data_To3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV31TFIncidentes_Codigo, AV32TFIncidentes_Codigo_To, AV35TFIncidentes_Data, AV36TFIncidentes_Data_To, AV41TFIncidentes_Descricao, AV42TFIncidentes_Descricao_Sel, AV45TFIncidentes_DemandaCod, AV46TFIncidentes_DemandaCod_To, AV49TFIncidentes_DemandaNum, AV50TFIncidentes_DemandaNum_Sel, AV33ddo_Incidentes_CodigoTitleControlIdToReplace, AV39ddo_Incidentes_DataTitleControlIdToReplace, AV43ddo_Incidentes_DescricaoTitleControlIdToReplace, AV47ddo_Incidentes_DemandaCodTitleControlIdToReplace, AV51ddo_Incidentes_DemandaNumTitleControlIdToReplace, AV59Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16Incidentes_Data1, AV17Incidentes_Data_To1, AV19DynamicFiltersSelector2, AV20Incidentes_Data2, AV21Incidentes_Data_To2, AV23DynamicFiltersSelector3, AV24Incidentes_Data3, AV25Incidentes_Data_To3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV31TFIncidentes_Codigo, AV32TFIncidentes_Codigo_To, AV35TFIncidentes_Data, AV36TFIncidentes_Data_To, AV41TFIncidentes_Descricao, AV42TFIncidentes_Descricao_Sel, AV45TFIncidentes_DemandaCod, AV46TFIncidentes_DemandaCod_To, AV49TFIncidentes_DemandaNum, AV50TFIncidentes_DemandaNum_Sel, AV33ddo_Incidentes_CodigoTitleControlIdToReplace, AV39ddo_Incidentes_DataTitleControlIdToReplace, AV43ddo_Incidentes_DescricaoTitleControlIdToReplace, AV47ddo_Incidentes_DemandaCodTitleControlIdToReplace, AV51ddo_Incidentes_DemandaNumTitleControlIdToReplace, AV59Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving) ;
         }
         return (int)(0) ;
      }

      protected void STRUPJ50( )
      {
         /* Before Start, stand alone formulas. */
         AV59Pgmname = "PromptContagemResultadoIncidentes";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E27J52 */
         E27J52 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "vDDO_TITLESETTINGSICONS"), AV52DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( "vINCIDENTES_CODIGOTITLEFILTERDATA"), AV30Incidentes_CodigoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vINCIDENTES_DATATITLEFILTERDATA"), AV34Incidentes_DataTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vINCIDENTES_DESCRICAOTITLEFILTERDATA"), AV40Incidentes_DescricaoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vINCIDENTES_DEMANDACODTITLEFILTERDATA"), AV44Incidentes_DemandaCodTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vINCIDENTES_DEMANDANUMTITLEFILTERDATA"), AV48Incidentes_DemandaNumTitleFilterData);
            /* Read variables values. */
            cmbavOrderedby.Name = cmbavOrderedby_Internalname;
            cmbavOrderedby.CurrentValue = cgiGet( cmbavOrderedby_Internalname);
            AV13OrderedBy = (short)(NumberUtil.Val( cgiGet( cmbavOrderedby_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            cmbavDynamicfiltersselector1.Name = cmbavDynamicfiltersselector1_Internalname;
            cmbavDynamicfiltersselector1.CurrentValue = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            AV15DynamicFiltersSelector1 = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            if ( context.localUtil.VCDateTime( cgiGet( edtavIncidentes_data1_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"Incidentes_Data1"}), 1, "vINCIDENTES_DATA1");
               GX_FocusControl = edtavIncidentes_data1_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV16Incidentes_Data1 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16Incidentes_Data1", context.localUtil.TToC( AV16Incidentes_Data1, 8, 5, 0, 3, "/", ":", " "));
            }
            else
            {
               AV16Incidentes_Data1 = context.localUtil.CToT( cgiGet( edtavIncidentes_data1_Internalname), 0);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16Incidentes_Data1", context.localUtil.TToC( AV16Incidentes_Data1, 8, 5, 0, 3, "/", ":", " "));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavIncidentes_data_to1_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"Incidentes_Data_To1"}), 1, "vINCIDENTES_DATA_TO1");
               GX_FocusControl = edtavIncidentes_data_to1_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV17Incidentes_Data_To1 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Incidentes_Data_To1", context.localUtil.TToC( AV17Incidentes_Data_To1, 8, 5, 0, 3, "/", ":", " "));
            }
            else
            {
               AV17Incidentes_Data_To1 = context.localUtil.CToT( cgiGet( edtavIncidentes_data_to1_Internalname), 0);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Incidentes_Data_To1", context.localUtil.TToC( AV17Incidentes_Data_To1, 8, 5, 0, 3, "/", ":", " "));
            }
            cmbavDynamicfiltersselector2.Name = cmbavDynamicfiltersselector2_Internalname;
            cmbavDynamicfiltersselector2.CurrentValue = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            AV19DynamicFiltersSelector2 = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
            if ( context.localUtil.VCDateTime( cgiGet( edtavIncidentes_data2_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"Incidentes_Data2"}), 1, "vINCIDENTES_DATA2");
               GX_FocusControl = edtavIncidentes_data2_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV20Incidentes_Data2 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20Incidentes_Data2", context.localUtil.TToC( AV20Incidentes_Data2, 8, 5, 0, 3, "/", ":", " "));
            }
            else
            {
               AV20Incidentes_Data2 = context.localUtil.CToT( cgiGet( edtavIncidentes_data2_Internalname), 0);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20Incidentes_Data2", context.localUtil.TToC( AV20Incidentes_Data2, 8, 5, 0, 3, "/", ":", " "));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavIncidentes_data_to2_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"Incidentes_Data_To2"}), 1, "vINCIDENTES_DATA_TO2");
               GX_FocusControl = edtavIncidentes_data_to2_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV21Incidentes_Data_To2 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Incidentes_Data_To2", context.localUtil.TToC( AV21Incidentes_Data_To2, 8, 5, 0, 3, "/", ":", " "));
            }
            else
            {
               AV21Incidentes_Data_To2 = context.localUtil.CToT( cgiGet( edtavIncidentes_data_to2_Internalname), 0);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Incidentes_Data_To2", context.localUtil.TToC( AV21Incidentes_Data_To2, 8, 5, 0, 3, "/", ":", " "));
            }
            cmbavDynamicfiltersselector3.Name = cmbavDynamicfiltersselector3_Internalname;
            cmbavDynamicfiltersselector3.CurrentValue = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            AV23DynamicFiltersSelector3 = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
            if ( context.localUtil.VCDateTime( cgiGet( edtavIncidentes_data3_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"Incidentes_Data3"}), 1, "vINCIDENTES_DATA3");
               GX_FocusControl = edtavIncidentes_data3_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV24Incidentes_Data3 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24Incidentes_Data3", context.localUtil.TToC( AV24Incidentes_Data3, 8, 5, 0, 3, "/", ":", " "));
            }
            else
            {
               AV24Incidentes_Data3 = context.localUtil.CToT( cgiGet( edtavIncidentes_data3_Internalname), 0);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24Incidentes_Data3", context.localUtil.TToC( AV24Incidentes_Data3, 8, 5, 0, 3, "/", ":", " "));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavIncidentes_data_to3_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"Incidentes_Data_To3"}), 1, "vINCIDENTES_DATA_TO3");
               GX_FocusControl = edtavIncidentes_data_to3_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV25Incidentes_Data_To3 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25Incidentes_Data_To3", context.localUtil.TToC( AV25Incidentes_Data_To3, 8, 5, 0, 3, "/", ":", " "));
            }
            else
            {
               AV25Incidentes_Data_To3 = context.localUtil.CToT( cgiGet( edtavIncidentes_data_to3_Internalname), 0);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25Incidentes_Data_To3", context.localUtil.TToC( AV25Incidentes_Data_To3, 8, 5, 0, 3, "/", ":", " "));
            }
            AV18DynamicFiltersEnabled2 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
            AV22DynamicFiltersEnabled3 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfincidentes_codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfincidentes_codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFINCIDENTES_CODIGO");
               GX_FocusControl = edtavTfincidentes_codigo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV31TFIncidentes_Codigo = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31TFIncidentes_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV31TFIncidentes_Codigo), 6, 0)));
            }
            else
            {
               AV31TFIncidentes_Codigo = (int)(context.localUtil.CToN( cgiGet( edtavTfincidentes_codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31TFIncidentes_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV31TFIncidentes_Codigo), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfincidentes_codigo_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfincidentes_codigo_to_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFINCIDENTES_CODIGO_TO");
               GX_FocusControl = edtavTfincidentes_codigo_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV32TFIncidentes_Codigo_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32TFIncidentes_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV32TFIncidentes_Codigo_To), 6, 0)));
            }
            else
            {
               AV32TFIncidentes_Codigo_To = (int)(context.localUtil.CToN( cgiGet( edtavTfincidentes_codigo_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32TFIncidentes_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV32TFIncidentes_Codigo_To), 6, 0)));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavTfincidentes_data_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"TFIncidentes_Data"}), 1, "vTFINCIDENTES_DATA");
               GX_FocusControl = edtavTfincidentes_data_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV35TFIncidentes_Data = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TFIncidentes_Data", context.localUtil.TToC( AV35TFIncidentes_Data, 8, 5, 0, 3, "/", ":", " "));
            }
            else
            {
               AV35TFIncidentes_Data = context.localUtil.CToT( cgiGet( edtavTfincidentes_data_Internalname), 0);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TFIncidentes_Data", context.localUtil.TToC( AV35TFIncidentes_Data, 8, 5, 0, 3, "/", ":", " "));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavTfincidentes_data_to_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"TFIncidentes_Data_To"}), 1, "vTFINCIDENTES_DATA_TO");
               GX_FocusControl = edtavTfincidentes_data_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV36TFIncidentes_Data_To = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36TFIncidentes_Data_To", context.localUtil.TToC( AV36TFIncidentes_Data_To, 8, 5, 0, 3, "/", ":", " "));
            }
            else
            {
               AV36TFIncidentes_Data_To = context.localUtil.CToT( cgiGet( edtavTfincidentes_data_to_Internalname), 0);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36TFIncidentes_Data_To", context.localUtil.TToC( AV36TFIncidentes_Data_To, 8, 5, 0, 3, "/", ":", " "));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_incidentes_dataauxdate_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Incidentes_Data Aux Date"}), 1, "vDDO_INCIDENTES_DATAAUXDATE");
               GX_FocusControl = edtavDdo_incidentes_dataauxdate_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV37DDO_Incidentes_DataAuxDate = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37DDO_Incidentes_DataAuxDate", context.localUtil.Format(AV37DDO_Incidentes_DataAuxDate, "99/99/99"));
            }
            else
            {
               AV37DDO_Incidentes_DataAuxDate = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_incidentes_dataauxdate_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37DDO_Incidentes_DataAuxDate", context.localUtil.Format(AV37DDO_Incidentes_DataAuxDate, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_incidentes_dataauxdateto_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Incidentes_Data Aux Date To"}), 1, "vDDO_INCIDENTES_DATAAUXDATETO");
               GX_FocusControl = edtavDdo_incidentes_dataauxdateto_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV38DDO_Incidentes_DataAuxDateTo = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38DDO_Incidentes_DataAuxDateTo", context.localUtil.Format(AV38DDO_Incidentes_DataAuxDateTo, "99/99/99"));
            }
            else
            {
               AV38DDO_Incidentes_DataAuxDateTo = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_incidentes_dataauxdateto_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38DDO_Incidentes_DataAuxDateTo", context.localUtil.Format(AV38DDO_Incidentes_DataAuxDateTo, "99/99/99"));
            }
            AV41TFIncidentes_Descricao = cgiGet( edtavTfincidentes_descricao_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41TFIncidentes_Descricao", AV41TFIncidentes_Descricao);
            AV42TFIncidentes_Descricao_Sel = cgiGet( edtavTfincidentes_descricao_sel_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42TFIncidentes_Descricao_Sel", AV42TFIncidentes_Descricao_Sel);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfincidentes_demandacod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfincidentes_demandacod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFINCIDENTES_DEMANDACOD");
               GX_FocusControl = edtavTfincidentes_demandacod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV45TFIncidentes_DemandaCod = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45TFIncidentes_DemandaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV45TFIncidentes_DemandaCod), 6, 0)));
            }
            else
            {
               AV45TFIncidentes_DemandaCod = (int)(context.localUtil.CToN( cgiGet( edtavTfincidentes_demandacod_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45TFIncidentes_DemandaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV45TFIncidentes_DemandaCod), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfincidentes_demandacod_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfincidentes_demandacod_to_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFINCIDENTES_DEMANDACOD_TO");
               GX_FocusControl = edtavTfincidentes_demandacod_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV46TFIncidentes_DemandaCod_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46TFIncidentes_DemandaCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV46TFIncidentes_DemandaCod_To), 6, 0)));
            }
            else
            {
               AV46TFIncidentes_DemandaCod_To = (int)(context.localUtil.CToN( cgiGet( edtavTfincidentes_demandacod_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46TFIncidentes_DemandaCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV46TFIncidentes_DemandaCod_To), 6, 0)));
            }
            AV49TFIncidentes_DemandaNum = StringUtil.Upper( cgiGet( edtavTfincidentes_demandanum_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49TFIncidentes_DemandaNum", AV49TFIncidentes_DemandaNum);
            AV50TFIncidentes_DemandaNum_Sel = StringUtil.Upper( cgiGet( edtavTfincidentes_demandanum_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50TFIncidentes_DemandaNum_Sel", AV50TFIncidentes_DemandaNum_Sel);
            AV33ddo_Incidentes_CodigoTitleControlIdToReplace = cgiGet( edtavDdo_incidentes_codigotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33ddo_Incidentes_CodigoTitleControlIdToReplace", AV33ddo_Incidentes_CodigoTitleControlIdToReplace);
            AV39ddo_Incidentes_DataTitleControlIdToReplace = cgiGet( edtavDdo_incidentes_datatitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39ddo_Incidentes_DataTitleControlIdToReplace", AV39ddo_Incidentes_DataTitleControlIdToReplace);
            AV43ddo_Incidentes_DescricaoTitleControlIdToReplace = cgiGet( edtavDdo_incidentes_descricaotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43ddo_Incidentes_DescricaoTitleControlIdToReplace", AV43ddo_Incidentes_DescricaoTitleControlIdToReplace);
            AV47ddo_Incidentes_DemandaCodTitleControlIdToReplace = cgiGet( edtavDdo_incidentes_demandacodtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47ddo_Incidentes_DemandaCodTitleControlIdToReplace", AV47ddo_Incidentes_DemandaCodTitleControlIdToReplace);
            AV51ddo_Incidentes_DemandaNumTitleControlIdToReplace = cgiGet( edtavDdo_incidentes_demandanumtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51ddo_Incidentes_DemandaNumTitleControlIdToReplace", AV51ddo_Incidentes_DemandaNumTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_86 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_86"), ",", "."));
            AV54GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( "vGRIDCURRENTPAGE"), ",", "."));
            AV55GridPageCount = (long)(context.localUtil.CToN( cgiGet( "vGRIDPAGECOUNT"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( "GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( "GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( "GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( "GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( "GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( "GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( "GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( "GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( "GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( "GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( "GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( "GRIDPAGINATIONBAR_Emptygridcaption");
            Ddo_incidentes_codigo_Caption = cgiGet( "DDO_INCIDENTES_CODIGO_Caption");
            Ddo_incidentes_codigo_Tooltip = cgiGet( "DDO_INCIDENTES_CODIGO_Tooltip");
            Ddo_incidentes_codigo_Cls = cgiGet( "DDO_INCIDENTES_CODIGO_Cls");
            Ddo_incidentes_codigo_Filteredtext_set = cgiGet( "DDO_INCIDENTES_CODIGO_Filteredtext_set");
            Ddo_incidentes_codigo_Filteredtextto_set = cgiGet( "DDO_INCIDENTES_CODIGO_Filteredtextto_set");
            Ddo_incidentes_codigo_Dropdownoptionstype = cgiGet( "DDO_INCIDENTES_CODIGO_Dropdownoptionstype");
            Ddo_incidentes_codigo_Titlecontrolidtoreplace = cgiGet( "DDO_INCIDENTES_CODIGO_Titlecontrolidtoreplace");
            Ddo_incidentes_codigo_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_INCIDENTES_CODIGO_Includesortasc"));
            Ddo_incidentes_codigo_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_INCIDENTES_CODIGO_Includesortdsc"));
            Ddo_incidentes_codigo_Sortedstatus = cgiGet( "DDO_INCIDENTES_CODIGO_Sortedstatus");
            Ddo_incidentes_codigo_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_INCIDENTES_CODIGO_Includefilter"));
            Ddo_incidentes_codigo_Filtertype = cgiGet( "DDO_INCIDENTES_CODIGO_Filtertype");
            Ddo_incidentes_codigo_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_INCIDENTES_CODIGO_Filterisrange"));
            Ddo_incidentes_codigo_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_INCIDENTES_CODIGO_Includedatalist"));
            Ddo_incidentes_codigo_Sortasc = cgiGet( "DDO_INCIDENTES_CODIGO_Sortasc");
            Ddo_incidentes_codigo_Sortdsc = cgiGet( "DDO_INCIDENTES_CODIGO_Sortdsc");
            Ddo_incidentes_codigo_Cleanfilter = cgiGet( "DDO_INCIDENTES_CODIGO_Cleanfilter");
            Ddo_incidentes_codigo_Rangefilterfrom = cgiGet( "DDO_INCIDENTES_CODIGO_Rangefilterfrom");
            Ddo_incidentes_codigo_Rangefilterto = cgiGet( "DDO_INCIDENTES_CODIGO_Rangefilterto");
            Ddo_incidentes_codigo_Searchbuttontext = cgiGet( "DDO_INCIDENTES_CODIGO_Searchbuttontext");
            Ddo_incidentes_data_Caption = cgiGet( "DDO_INCIDENTES_DATA_Caption");
            Ddo_incidentes_data_Tooltip = cgiGet( "DDO_INCIDENTES_DATA_Tooltip");
            Ddo_incidentes_data_Cls = cgiGet( "DDO_INCIDENTES_DATA_Cls");
            Ddo_incidentes_data_Filteredtext_set = cgiGet( "DDO_INCIDENTES_DATA_Filteredtext_set");
            Ddo_incidentes_data_Filteredtextto_set = cgiGet( "DDO_INCIDENTES_DATA_Filteredtextto_set");
            Ddo_incidentes_data_Dropdownoptionstype = cgiGet( "DDO_INCIDENTES_DATA_Dropdownoptionstype");
            Ddo_incidentes_data_Titlecontrolidtoreplace = cgiGet( "DDO_INCIDENTES_DATA_Titlecontrolidtoreplace");
            Ddo_incidentes_data_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_INCIDENTES_DATA_Includesortasc"));
            Ddo_incidentes_data_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_INCIDENTES_DATA_Includesortdsc"));
            Ddo_incidentes_data_Sortedstatus = cgiGet( "DDO_INCIDENTES_DATA_Sortedstatus");
            Ddo_incidentes_data_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_INCIDENTES_DATA_Includefilter"));
            Ddo_incidentes_data_Filtertype = cgiGet( "DDO_INCIDENTES_DATA_Filtertype");
            Ddo_incidentes_data_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_INCIDENTES_DATA_Filterisrange"));
            Ddo_incidentes_data_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_INCIDENTES_DATA_Includedatalist"));
            Ddo_incidentes_data_Sortasc = cgiGet( "DDO_INCIDENTES_DATA_Sortasc");
            Ddo_incidentes_data_Sortdsc = cgiGet( "DDO_INCIDENTES_DATA_Sortdsc");
            Ddo_incidentes_data_Cleanfilter = cgiGet( "DDO_INCIDENTES_DATA_Cleanfilter");
            Ddo_incidentes_data_Rangefilterfrom = cgiGet( "DDO_INCIDENTES_DATA_Rangefilterfrom");
            Ddo_incidentes_data_Rangefilterto = cgiGet( "DDO_INCIDENTES_DATA_Rangefilterto");
            Ddo_incidentes_data_Searchbuttontext = cgiGet( "DDO_INCIDENTES_DATA_Searchbuttontext");
            Ddo_incidentes_descricao_Caption = cgiGet( "DDO_INCIDENTES_DESCRICAO_Caption");
            Ddo_incidentes_descricao_Tooltip = cgiGet( "DDO_INCIDENTES_DESCRICAO_Tooltip");
            Ddo_incidentes_descricao_Cls = cgiGet( "DDO_INCIDENTES_DESCRICAO_Cls");
            Ddo_incidentes_descricao_Filteredtext_set = cgiGet( "DDO_INCIDENTES_DESCRICAO_Filteredtext_set");
            Ddo_incidentes_descricao_Selectedvalue_set = cgiGet( "DDO_INCIDENTES_DESCRICAO_Selectedvalue_set");
            Ddo_incidentes_descricao_Dropdownoptionstype = cgiGet( "DDO_INCIDENTES_DESCRICAO_Dropdownoptionstype");
            Ddo_incidentes_descricao_Titlecontrolidtoreplace = cgiGet( "DDO_INCIDENTES_DESCRICAO_Titlecontrolidtoreplace");
            Ddo_incidentes_descricao_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_INCIDENTES_DESCRICAO_Includesortasc"));
            Ddo_incidentes_descricao_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_INCIDENTES_DESCRICAO_Includesortdsc"));
            Ddo_incidentes_descricao_Sortedstatus = cgiGet( "DDO_INCIDENTES_DESCRICAO_Sortedstatus");
            Ddo_incidentes_descricao_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_INCIDENTES_DESCRICAO_Includefilter"));
            Ddo_incidentes_descricao_Filtertype = cgiGet( "DDO_INCIDENTES_DESCRICAO_Filtertype");
            Ddo_incidentes_descricao_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_INCIDENTES_DESCRICAO_Filterisrange"));
            Ddo_incidentes_descricao_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_INCIDENTES_DESCRICAO_Includedatalist"));
            Ddo_incidentes_descricao_Datalisttype = cgiGet( "DDO_INCIDENTES_DESCRICAO_Datalisttype");
            Ddo_incidentes_descricao_Datalistproc = cgiGet( "DDO_INCIDENTES_DESCRICAO_Datalistproc");
            Ddo_incidentes_descricao_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_INCIDENTES_DESCRICAO_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_incidentes_descricao_Sortasc = cgiGet( "DDO_INCIDENTES_DESCRICAO_Sortasc");
            Ddo_incidentes_descricao_Sortdsc = cgiGet( "DDO_INCIDENTES_DESCRICAO_Sortdsc");
            Ddo_incidentes_descricao_Loadingdata = cgiGet( "DDO_INCIDENTES_DESCRICAO_Loadingdata");
            Ddo_incidentes_descricao_Cleanfilter = cgiGet( "DDO_INCIDENTES_DESCRICAO_Cleanfilter");
            Ddo_incidentes_descricao_Noresultsfound = cgiGet( "DDO_INCIDENTES_DESCRICAO_Noresultsfound");
            Ddo_incidentes_descricao_Searchbuttontext = cgiGet( "DDO_INCIDENTES_DESCRICAO_Searchbuttontext");
            Ddo_incidentes_demandacod_Caption = cgiGet( "DDO_INCIDENTES_DEMANDACOD_Caption");
            Ddo_incidentes_demandacod_Tooltip = cgiGet( "DDO_INCIDENTES_DEMANDACOD_Tooltip");
            Ddo_incidentes_demandacod_Cls = cgiGet( "DDO_INCIDENTES_DEMANDACOD_Cls");
            Ddo_incidentes_demandacod_Filteredtext_set = cgiGet( "DDO_INCIDENTES_DEMANDACOD_Filteredtext_set");
            Ddo_incidentes_demandacod_Filteredtextto_set = cgiGet( "DDO_INCIDENTES_DEMANDACOD_Filteredtextto_set");
            Ddo_incidentes_demandacod_Dropdownoptionstype = cgiGet( "DDO_INCIDENTES_DEMANDACOD_Dropdownoptionstype");
            Ddo_incidentes_demandacod_Titlecontrolidtoreplace = cgiGet( "DDO_INCIDENTES_DEMANDACOD_Titlecontrolidtoreplace");
            Ddo_incidentes_demandacod_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_INCIDENTES_DEMANDACOD_Includesortasc"));
            Ddo_incidentes_demandacod_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_INCIDENTES_DEMANDACOD_Includesortdsc"));
            Ddo_incidentes_demandacod_Sortedstatus = cgiGet( "DDO_INCIDENTES_DEMANDACOD_Sortedstatus");
            Ddo_incidentes_demandacod_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_INCIDENTES_DEMANDACOD_Includefilter"));
            Ddo_incidentes_demandacod_Filtertype = cgiGet( "DDO_INCIDENTES_DEMANDACOD_Filtertype");
            Ddo_incidentes_demandacod_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_INCIDENTES_DEMANDACOD_Filterisrange"));
            Ddo_incidentes_demandacod_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_INCIDENTES_DEMANDACOD_Includedatalist"));
            Ddo_incidentes_demandacod_Sortasc = cgiGet( "DDO_INCIDENTES_DEMANDACOD_Sortasc");
            Ddo_incidentes_demandacod_Sortdsc = cgiGet( "DDO_INCIDENTES_DEMANDACOD_Sortdsc");
            Ddo_incidentes_demandacod_Cleanfilter = cgiGet( "DDO_INCIDENTES_DEMANDACOD_Cleanfilter");
            Ddo_incidentes_demandacod_Rangefilterfrom = cgiGet( "DDO_INCIDENTES_DEMANDACOD_Rangefilterfrom");
            Ddo_incidentes_demandacod_Rangefilterto = cgiGet( "DDO_INCIDENTES_DEMANDACOD_Rangefilterto");
            Ddo_incidentes_demandacod_Searchbuttontext = cgiGet( "DDO_INCIDENTES_DEMANDACOD_Searchbuttontext");
            Ddo_incidentes_demandanum_Caption = cgiGet( "DDO_INCIDENTES_DEMANDANUM_Caption");
            Ddo_incidentes_demandanum_Tooltip = cgiGet( "DDO_INCIDENTES_DEMANDANUM_Tooltip");
            Ddo_incidentes_demandanum_Cls = cgiGet( "DDO_INCIDENTES_DEMANDANUM_Cls");
            Ddo_incidentes_demandanum_Filteredtext_set = cgiGet( "DDO_INCIDENTES_DEMANDANUM_Filteredtext_set");
            Ddo_incidentes_demandanum_Selectedvalue_set = cgiGet( "DDO_INCIDENTES_DEMANDANUM_Selectedvalue_set");
            Ddo_incidentes_demandanum_Dropdownoptionstype = cgiGet( "DDO_INCIDENTES_DEMANDANUM_Dropdownoptionstype");
            Ddo_incidentes_demandanum_Titlecontrolidtoreplace = cgiGet( "DDO_INCIDENTES_DEMANDANUM_Titlecontrolidtoreplace");
            Ddo_incidentes_demandanum_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_INCIDENTES_DEMANDANUM_Includesortasc"));
            Ddo_incidentes_demandanum_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_INCIDENTES_DEMANDANUM_Includesortdsc"));
            Ddo_incidentes_demandanum_Sortedstatus = cgiGet( "DDO_INCIDENTES_DEMANDANUM_Sortedstatus");
            Ddo_incidentes_demandanum_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_INCIDENTES_DEMANDANUM_Includefilter"));
            Ddo_incidentes_demandanum_Filtertype = cgiGet( "DDO_INCIDENTES_DEMANDANUM_Filtertype");
            Ddo_incidentes_demandanum_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_INCIDENTES_DEMANDANUM_Filterisrange"));
            Ddo_incidentes_demandanum_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_INCIDENTES_DEMANDANUM_Includedatalist"));
            Ddo_incidentes_demandanum_Datalisttype = cgiGet( "DDO_INCIDENTES_DEMANDANUM_Datalisttype");
            Ddo_incidentes_demandanum_Datalistproc = cgiGet( "DDO_INCIDENTES_DEMANDANUM_Datalistproc");
            Ddo_incidentes_demandanum_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_INCIDENTES_DEMANDANUM_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_incidentes_demandanum_Sortasc = cgiGet( "DDO_INCIDENTES_DEMANDANUM_Sortasc");
            Ddo_incidentes_demandanum_Sortdsc = cgiGet( "DDO_INCIDENTES_DEMANDANUM_Sortdsc");
            Ddo_incidentes_demandanum_Loadingdata = cgiGet( "DDO_INCIDENTES_DEMANDANUM_Loadingdata");
            Ddo_incidentes_demandanum_Cleanfilter = cgiGet( "DDO_INCIDENTES_DEMANDANUM_Cleanfilter");
            Ddo_incidentes_demandanum_Noresultsfound = cgiGet( "DDO_INCIDENTES_DEMANDANUM_Noresultsfound");
            Ddo_incidentes_demandanum_Searchbuttontext = cgiGet( "DDO_INCIDENTES_DEMANDANUM_Searchbuttontext");
            Gridpaginationbar_Selectedpage = cgiGet( "GRIDPAGINATIONBAR_Selectedpage");
            Ddo_incidentes_codigo_Activeeventkey = cgiGet( "DDO_INCIDENTES_CODIGO_Activeeventkey");
            Ddo_incidentes_codigo_Filteredtext_get = cgiGet( "DDO_INCIDENTES_CODIGO_Filteredtext_get");
            Ddo_incidentes_codigo_Filteredtextto_get = cgiGet( "DDO_INCIDENTES_CODIGO_Filteredtextto_get");
            Ddo_incidentes_data_Activeeventkey = cgiGet( "DDO_INCIDENTES_DATA_Activeeventkey");
            Ddo_incidentes_data_Filteredtext_get = cgiGet( "DDO_INCIDENTES_DATA_Filteredtext_get");
            Ddo_incidentes_data_Filteredtextto_get = cgiGet( "DDO_INCIDENTES_DATA_Filteredtextto_get");
            Ddo_incidentes_descricao_Activeeventkey = cgiGet( "DDO_INCIDENTES_DESCRICAO_Activeeventkey");
            Ddo_incidentes_descricao_Filteredtext_get = cgiGet( "DDO_INCIDENTES_DESCRICAO_Filteredtext_get");
            Ddo_incidentes_descricao_Selectedvalue_get = cgiGet( "DDO_INCIDENTES_DESCRICAO_Selectedvalue_get");
            Ddo_incidentes_demandacod_Activeeventkey = cgiGet( "DDO_INCIDENTES_DEMANDACOD_Activeeventkey");
            Ddo_incidentes_demandacod_Filteredtext_get = cgiGet( "DDO_INCIDENTES_DEMANDACOD_Filteredtext_get");
            Ddo_incidentes_demandacod_Filteredtextto_get = cgiGet( "DDO_INCIDENTES_DEMANDACOD_Filteredtextto_get");
            Ddo_incidentes_demandanum_Activeeventkey = cgiGet( "DDO_INCIDENTES_DEMANDANUM_Activeeventkey");
            Ddo_incidentes_demandanum_Filteredtext_get = cgiGet( "DDO_INCIDENTES_DEMANDANUM_Filteredtext_get");
            Ddo_incidentes_demandanum_Selectedvalue_get = cgiGet( "DDO_INCIDENTES_DEMANDANUM_Selectedvalue_get");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vINCIDENTES_DATA1"), 0) != AV16Incidentes_Data1 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vINCIDENTES_DATA_TO1"), 0) != AV17Incidentes_Data_To1 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV19DynamicFiltersSelector2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vINCIDENTES_DATA2"), 0) != AV20Incidentes_Data2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vINCIDENTES_DATA_TO2"), 0) != AV21Incidentes_Data_To2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV23DynamicFiltersSelector3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vINCIDENTES_DATA3"), 0) != AV24Incidentes_Data3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vINCIDENTES_DATA_TO3"), 0) != AV25Incidentes_Data_To3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV18DynamicFiltersEnabled2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV22DynamicFiltersEnabled3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFINCIDENTES_CODIGO"), ",", ".") != Convert.ToDecimal( AV31TFIncidentes_Codigo )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFINCIDENTES_CODIGO_TO"), ",", ".") != Convert.ToDecimal( AV32TFIncidentes_Codigo_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vTFINCIDENTES_DATA"), 0) != AV35TFIncidentes_Data )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vTFINCIDENTES_DATA_TO"), 0) != AV36TFIncidentes_Data_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFINCIDENTES_DESCRICAO"), AV41TFIncidentes_Descricao) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFINCIDENTES_DESCRICAO_SEL"), AV42TFIncidentes_Descricao_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFINCIDENTES_DEMANDACOD"), ",", ".") != Convert.ToDecimal( AV45TFIncidentes_DemandaCod )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFINCIDENTES_DEMANDACOD_TO"), ",", ".") != Convert.ToDecimal( AV46TFIncidentes_DemandaCod_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFINCIDENTES_DEMANDANUM"), AV49TFIncidentes_DemandaNum) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFINCIDENTES_DEMANDANUM_SEL"), AV50TFIncidentes_DemandaNum_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E27J52 */
         E27J52 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E27J52( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         lblJsdynamicfilters_Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         chkavDynamicfiltersenabled2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled2.Visible), 5, 0)));
         chkavDynamicfiltersenabled3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled3.Visible), 5, 0)));
         AV15DynamicFiltersSelector1 = "INCIDENTES_DATA";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV19DynamicFiltersSelector2 = "INCIDENTES_DATA";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV23DynamicFiltersSelector3 = "INCIDENTES_DATA";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         imgAdddynamicfilters1_Jsonclick = "WWPDynFilterShow(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Jsonclick", imgAdddynamicfilters1_Jsonclick);
         imgRemovedynamicfilters1_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Jsonclick", imgRemovedynamicfilters1_Jsonclick);
         imgAdddynamicfilters2_Jsonclick = "WWPDynFilterShow(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Jsonclick", imgAdddynamicfilters2_Jsonclick);
         imgRemovedynamicfilters2_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Jsonclick", imgRemovedynamicfilters2_Jsonclick);
         imgRemovedynamicfilters3_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters3_Internalname, "Jsonclick", imgRemovedynamicfilters3_Jsonclick);
         edtavTfincidentes_codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfincidentes_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfincidentes_codigo_Visible), 5, 0)));
         edtavTfincidentes_codigo_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfincidentes_codigo_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfincidentes_codigo_to_Visible), 5, 0)));
         edtavTfincidentes_data_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfincidentes_data_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfincidentes_data_Visible), 5, 0)));
         edtavTfincidentes_data_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfincidentes_data_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfincidentes_data_to_Visible), 5, 0)));
         edtavTfincidentes_descricao_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfincidentes_descricao_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfincidentes_descricao_Visible), 5, 0)));
         edtavTfincidentes_descricao_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfincidentes_descricao_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfincidentes_descricao_sel_Visible), 5, 0)));
         edtavTfincidentes_demandacod_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfincidentes_demandacod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfincidentes_demandacod_Visible), 5, 0)));
         edtavTfincidentes_demandacod_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfincidentes_demandacod_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfincidentes_demandacod_to_Visible), 5, 0)));
         edtavTfincidentes_demandanum_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfincidentes_demandanum_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfincidentes_demandanum_Visible), 5, 0)));
         edtavTfincidentes_demandanum_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfincidentes_demandanum_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfincidentes_demandanum_sel_Visible), 5, 0)));
         Ddo_incidentes_codigo_Titlecontrolidtoreplace = subGrid_Internalname+"_Incidentes_Codigo";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_incidentes_codigo_Internalname, "TitleControlIdToReplace", Ddo_incidentes_codigo_Titlecontrolidtoreplace);
         AV33ddo_Incidentes_CodigoTitleControlIdToReplace = Ddo_incidentes_codigo_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33ddo_Incidentes_CodigoTitleControlIdToReplace", AV33ddo_Incidentes_CodigoTitleControlIdToReplace);
         edtavDdo_incidentes_codigotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_incidentes_codigotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_incidentes_codigotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_incidentes_data_Titlecontrolidtoreplace = subGrid_Internalname+"_Incidentes_Data";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_incidentes_data_Internalname, "TitleControlIdToReplace", Ddo_incidentes_data_Titlecontrolidtoreplace);
         AV39ddo_Incidentes_DataTitleControlIdToReplace = Ddo_incidentes_data_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39ddo_Incidentes_DataTitleControlIdToReplace", AV39ddo_Incidentes_DataTitleControlIdToReplace);
         edtavDdo_incidentes_datatitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_incidentes_datatitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_incidentes_datatitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_incidentes_descricao_Titlecontrolidtoreplace = subGrid_Internalname+"_Incidentes_Descricao";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_incidentes_descricao_Internalname, "TitleControlIdToReplace", Ddo_incidentes_descricao_Titlecontrolidtoreplace);
         AV43ddo_Incidentes_DescricaoTitleControlIdToReplace = Ddo_incidentes_descricao_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43ddo_Incidentes_DescricaoTitleControlIdToReplace", AV43ddo_Incidentes_DescricaoTitleControlIdToReplace);
         edtavDdo_incidentes_descricaotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_incidentes_descricaotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_incidentes_descricaotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_incidentes_demandacod_Titlecontrolidtoreplace = subGrid_Internalname+"_Incidentes_DemandaCod";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_incidentes_demandacod_Internalname, "TitleControlIdToReplace", Ddo_incidentes_demandacod_Titlecontrolidtoreplace);
         AV47ddo_Incidentes_DemandaCodTitleControlIdToReplace = Ddo_incidentes_demandacod_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47ddo_Incidentes_DemandaCodTitleControlIdToReplace", AV47ddo_Incidentes_DemandaCodTitleControlIdToReplace);
         edtavDdo_incidentes_demandacodtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_incidentes_demandacodtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_incidentes_demandacodtitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_incidentes_demandanum_Titlecontrolidtoreplace = subGrid_Internalname+"_Incidentes_DemandaNum";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_incidentes_demandanum_Internalname, "TitleControlIdToReplace", Ddo_incidentes_demandanum_Titlecontrolidtoreplace);
         AV51ddo_Incidentes_DemandaNumTitleControlIdToReplace = Ddo_incidentes_demandanum_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51ddo_Incidentes_DemandaNumTitleControlIdToReplace", AV51ddo_Incidentes_DemandaNumTitleControlIdToReplace);
         edtavDdo_incidentes_demandanumtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_incidentes_demandanumtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_incidentes_demandanumtitlecontrolidtoreplace_Visible), 5, 0)));
         Form.Caption = "Selecione Contagem Resultado Incidentes";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavOrderedby.removeAllItems();
         cmbavOrderedby.addItem("1", "Data", 0);
         cmbavOrderedby.addItem("2", "Codigo", 0);
         cmbavOrderedby.addItem("3", "Descricao", 0);
         cmbavOrderedby.addItem("4", "Cod", 0);
         cmbavOrderedby.addItem("5", "Demanda", 0);
         if ( AV13OrderedBy < 1 )
         {
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         imgCleanfilters_Jsonclick = "WWPDynFilterHideAll(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgCleanfilters_Internalname, "Jsonclick", imgCleanfilters_Jsonclick);
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = AV52DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1) ;
         AV52DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1;
      }

      protected void E28J52( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV30Incidentes_CodigoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV34Incidentes_DataTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV40Incidentes_DescricaoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV44Incidentes_DemandaCodTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV48Incidentes_DemandaNumTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtIncidentes_Codigo_Titleformat = 2;
         edtIncidentes_Codigo_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Codigo", AV33ddo_Incidentes_CodigoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtIncidentes_Codigo_Internalname, "Title", edtIncidentes_Codigo_Title);
         edtIncidentes_Data_Titleformat = 2;
         edtIncidentes_Data_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Data", AV39ddo_Incidentes_DataTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtIncidentes_Data_Internalname, "Title", edtIncidentes_Data_Title);
         edtIncidentes_Descricao_Titleformat = 2;
         edtIncidentes_Descricao_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Descricao", AV43ddo_Incidentes_DescricaoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtIncidentes_Descricao_Internalname, "Title", edtIncidentes_Descricao_Title);
         edtIncidentes_DemandaCod_Titleformat = 2;
         edtIncidentes_DemandaCod_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Cod", AV47ddo_Incidentes_DemandaCodTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtIncidentes_DemandaCod_Internalname, "Title", edtIncidentes_DemandaCod_Title);
         edtIncidentes_DemandaNum_Titleformat = 2;
         edtIncidentes_DemandaNum_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Demanda", AV51ddo_Incidentes_DemandaNumTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtIncidentes_DemandaNum_Internalname, "Title", edtIncidentes_DemandaNum_Title);
         AV54GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV54GridCurrentPage), 10, 0)));
         AV55GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV55GridPageCount), 10, 0)));
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV30Incidentes_CodigoTitleFilterData", AV30Incidentes_CodigoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV34Incidentes_DataTitleFilterData", AV34Incidentes_DataTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV40Incidentes_DescricaoTitleFilterData", AV40Incidentes_DescricaoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV44Incidentes_DemandaCodTitleFilterData", AV44Incidentes_DemandaCodTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV48Incidentes_DemandaNumTitleFilterData", AV48Incidentes_DemandaNumTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
      }

      protected void E11J52( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV53PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV53PageToGo) ;
         }
      }

      protected void E12J52( )
      {
         /* Ddo_incidentes_codigo_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_incidentes_codigo_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_incidentes_codigo_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_incidentes_codigo_Internalname, "SortedStatus", Ddo_incidentes_codigo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_incidentes_codigo_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_incidentes_codigo_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_incidentes_codigo_Internalname, "SortedStatus", Ddo_incidentes_codigo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_incidentes_codigo_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV31TFIncidentes_Codigo = (int)(NumberUtil.Val( Ddo_incidentes_codigo_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31TFIncidentes_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV31TFIncidentes_Codigo), 6, 0)));
            AV32TFIncidentes_Codigo_To = (int)(NumberUtil.Val( Ddo_incidentes_codigo_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32TFIncidentes_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV32TFIncidentes_Codigo_To), 6, 0)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E13J52( )
      {
         /* Ddo_incidentes_data_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_incidentes_data_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_incidentes_data_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_incidentes_data_Internalname, "SortedStatus", Ddo_incidentes_data_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_incidentes_data_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_incidentes_data_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_incidentes_data_Internalname, "SortedStatus", Ddo_incidentes_data_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_incidentes_data_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV35TFIncidentes_Data = context.localUtil.CToT( Ddo_incidentes_data_Filteredtext_get, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TFIncidentes_Data", context.localUtil.TToC( AV35TFIncidentes_Data, 8, 5, 0, 3, "/", ":", " "));
            AV36TFIncidentes_Data_To = context.localUtil.CToT( Ddo_incidentes_data_Filteredtextto_get, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36TFIncidentes_Data_To", context.localUtil.TToC( AV36TFIncidentes_Data_To, 8, 5, 0, 3, "/", ":", " "));
            if ( ! (DateTime.MinValue==AV36TFIncidentes_Data_To) )
            {
               AV36TFIncidentes_Data_To = context.localUtil.YMDHMSToT( (short)(DateTimeUtil.Year( AV36TFIncidentes_Data_To)), (short)(DateTimeUtil.Month( AV36TFIncidentes_Data_To)), (short)(DateTimeUtil.Day( AV36TFIncidentes_Data_To)), 23, 59, 59);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36TFIncidentes_Data_To", context.localUtil.TToC( AV36TFIncidentes_Data_To, 8, 5, 0, 3, "/", ":", " "));
            }
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E14J52( )
      {
         /* Ddo_incidentes_descricao_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_incidentes_descricao_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_incidentes_descricao_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_incidentes_descricao_Internalname, "SortedStatus", Ddo_incidentes_descricao_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_incidentes_descricao_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_incidentes_descricao_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_incidentes_descricao_Internalname, "SortedStatus", Ddo_incidentes_descricao_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_incidentes_descricao_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV41TFIncidentes_Descricao = Ddo_incidentes_descricao_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41TFIncidentes_Descricao", AV41TFIncidentes_Descricao);
            AV42TFIncidentes_Descricao_Sel = Ddo_incidentes_descricao_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42TFIncidentes_Descricao_Sel", AV42TFIncidentes_Descricao_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E15J52( )
      {
         /* Ddo_incidentes_demandacod_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_incidentes_demandacod_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_incidentes_demandacod_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_incidentes_demandacod_Internalname, "SortedStatus", Ddo_incidentes_demandacod_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_incidentes_demandacod_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_incidentes_demandacod_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_incidentes_demandacod_Internalname, "SortedStatus", Ddo_incidentes_demandacod_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_incidentes_demandacod_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV45TFIncidentes_DemandaCod = (int)(NumberUtil.Val( Ddo_incidentes_demandacod_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45TFIncidentes_DemandaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV45TFIncidentes_DemandaCod), 6, 0)));
            AV46TFIncidentes_DemandaCod_To = (int)(NumberUtil.Val( Ddo_incidentes_demandacod_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46TFIncidentes_DemandaCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV46TFIncidentes_DemandaCod_To), 6, 0)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E16J52( )
      {
         /* Ddo_incidentes_demandanum_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_incidentes_demandanum_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 5;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_incidentes_demandanum_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_incidentes_demandanum_Internalname, "SortedStatus", Ddo_incidentes_demandanum_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_incidentes_demandanum_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 5;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_incidentes_demandanum_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_incidentes_demandanum_Internalname, "SortedStatus", Ddo_incidentes_demandanum_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_incidentes_demandanum_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV49TFIncidentes_DemandaNum = Ddo_incidentes_demandanum_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49TFIncidentes_DemandaNum", AV49TFIncidentes_DemandaNum);
            AV50TFIncidentes_DemandaNum_Sel = Ddo_incidentes_demandanum_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50TFIncidentes_DemandaNum_Sel", AV50TFIncidentes_DemandaNum_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      private void E29J52( )
      {
         /* Grid_Load Routine */
         AV28Select = context.GetImagePath( "3914535b-0c03-44c5-9538-906a99cdd2bc", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavSelect_Internalname, AV28Select);
         AV58Select_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "3914535b-0c03-44c5-9538-906a99cdd2bc", "", context.GetTheme( )));
         edtavSelect_Tooltiptext = "Selecionar";
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 86;
         }
         sendrow_862( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_86_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(86, GridRow);
         }
      }

      public void GXEnter( )
      {
         /* Execute user event: E30J52 */
         E30J52 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E30J52( )
      {
         /* Enter Routine */
         AV7InOutIncidentes_Codigo = A1241Incidentes_Codigo;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InOutIncidentes_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7InOutIncidentes_Codigo), 6, 0)));
         AV8InOutIncidentes_Data = A1242Incidentes_Data;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8InOutIncidentes_Data", context.localUtil.TToC( AV8InOutIncidentes_Data, 8, 5, 0, 3, "/", ":", " "));
         context.setWebReturnParms(new Object[] {(int)AV7InOutIncidentes_Codigo,context.localUtil.Format( AV8InOutIncidentes_Data, "99/99/99 99:99")});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void E17J52( )
      {
         /* Orderedby_Click Routine */
         context.DoAjaxRefresh();
      }

      protected void E22J52( )
      {
         /* 'AddDynamicFilters1' Routine */
         AV18DynamicFiltersEnabled2 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         imgAdddynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
      }

      protected void E18J52( )
      {
         /* 'RemoveDynamicFilters1' Routine */
         AV26DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV27DynamicFiltersIgnoreFirst = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersIgnoreFirst", AV27DynamicFiltersIgnoreFirst);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV27DynamicFiltersIgnoreFirst = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersIgnoreFirst", AV27DynamicFiltersIgnoreFirst);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16Incidentes_Data1, AV17Incidentes_Data_To1, AV19DynamicFiltersSelector2, AV20Incidentes_Data2, AV21Incidentes_Data_To2, AV23DynamicFiltersSelector3, AV24Incidentes_Data3, AV25Incidentes_Data_To3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV31TFIncidentes_Codigo, AV32TFIncidentes_Codigo_To, AV35TFIncidentes_Data, AV36TFIncidentes_Data_To, AV41TFIncidentes_Descricao, AV42TFIncidentes_Descricao_Sel, AV45TFIncidentes_DemandaCod, AV46TFIncidentes_DemandaCod_To, AV49TFIncidentes_DemandaNum, AV50TFIncidentes_DemandaNum_Sel, AV33ddo_Incidentes_CodigoTitleControlIdToReplace, AV39ddo_Incidentes_DataTitleControlIdToReplace, AV43ddo_Incidentes_DescricaoTitleControlIdToReplace, AV47ddo_Incidentes_DemandaCodTitleControlIdToReplace, AV51ddo_Incidentes_DemandaNumTitleControlIdToReplace, AV59Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
      }

      protected void E23J52( )
      {
         /* Dynamicfiltersselector1_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E24J52( )
      {
         /* 'AddDynamicFilters2' Routine */
         AV22DynamicFiltersEnabled3 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
         imgAdddynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
      }

      protected void E19J52( )
      {
         /* 'RemoveDynamicFilters2' Routine */
         AV26DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV18DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16Incidentes_Data1, AV17Incidentes_Data_To1, AV19DynamicFiltersSelector2, AV20Incidentes_Data2, AV21Incidentes_Data_To2, AV23DynamicFiltersSelector3, AV24Incidentes_Data3, AV25Incidentes_Data_To3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV31TFIncidentes_Codigo, AV32TFIncidentes_Codigo_To, AV35TFIncidentes_Data, AV36TFIncidentes_Data_To, AV41TFIncidentes_Descricao, AV42TFIncidentes_Descricao_Sel, AV45TFIncidentes_DemandaCod, AV46TFIncidentes_DemandaCod_To, AV49TFIncidentes_DemandaNum, AV50TFIncidentes_DemandaNum_Sel, AV33ddo_Incidentes_CodigoTitleControlIdToReplace, AV39ddo_Incidentes_DataTitleControlIdToReplace, AV43ddo_Incidentes_DescricaoTitleControlIdToReplace, AV47ddo_Incidentes_DemandaCodTitleControlIdToReplace, AV51ddo_Incidentes_DemandaNumTitleControlIdToReplace, AV59Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
      }

      protected void E25J52( )
      {
         /* Dynamicfiltersselector2_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E20J52( )
      {
         /* 'RemoveDynamicFilters3' Routine */
         AV26DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV22DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV16Incidentes_Data1, AV17Incidentes_Data_To1, AV19DynamicFiltersSelector2, AV20Incidentes_Data2, AV21Incidentes_Data_To2, AV23DynamicFiltersSelector3, AV24Incidentes_Data3, AV25Incidentes_Data_To3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV31TFIncidentes_Codigo, AV32TFIncidentes_Codigo_To, AV35TFIncidentes_Data, AV36TFIncidentes_Data_To, AV41TFIncidentes_Descricao, AV42TFIncidentes_Descricao_Sel, AV45TFIncidentes_DemandaCod, AV46TFIncidentes_DemandaCod_To, AV49TFIncidentes_DemandaNum, AV50TFIncidentes_DemandaNum_Sel, AV33ddo_Incidentes_CodigoTitleControlIdToReplace, AV39ddo_Incidentes_DataTitleControlIdToReplace, AV43ddo_Incidentes_DescricaoTitleControlIdToReplace, AV47ddo_Incidentes_DemandaCodTitleControlIdToReplace, AV51ddo_Incidentes_DemandaNumTitleControlIdToReplace, AV59Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
      }

      protected void E26J52( )
      {
         /* Dynamicfiltersselector3_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E21J52( )
      {
         /* 'DoCleanFilters' Routine */
         /* Execute user subroutine: 'CLEANFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_firstpage( ) ;
         context.DoAjaxRefresh();
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
      }

      protected void S172( )
      {
         /* 'RESETDDOSORTEDSTATUS' Routine */
         Ddo_incidentes_codigo_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_incidentes_codigo_Internalname, "SortedStatus", Ddo_incidentes_codigo_Sortedstatus);
         Ddo_incidentes_data_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_incidentes_data_Internalname, "SortedStatus", Ddo_incidentes_data_Sortedstatus);
         Ddo_incidentes_descricao_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_incidentes_descricao_Internalname, "SortedStatus", Ddo_incidentes_descricao_Sortedstatus);
         Ddo_incidentes_demandacod_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_incidentes_demandacod_Internalname, "SortedStatus", Ddo_incidentes_demandacod_Sortedstatus);
         Ddo_incidentes_demandanum_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_incidentes_demandanum_Internalname, "SortedStatus", Ddo_incidentes_demandanum_Sortedstatus);
      }

      protected void S152( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV13OrderedBy == 2 )
         {
            Ddo_incidentes_codigo_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_incidentes_codigo_Internalname, "SortedStatus", Ddo_incidentes_codigo_Sortedstatus);
         }
         else if ( AV13OrderedBy == 1 )
         {
            Ddo_incidentes_data_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_incidentes_data_Internalname, "SortedStatus", Ddo_incidentes_data_Sortedstatus);
         }
         else if ( AV13OrderedBy == 3 )
         {
            Ddo_incidentes_descricao_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_incidentes_descricao_Internalname, "SortedStatus", Ddo_incidentes_descricao_Sortedstatus);
         }
         else if ( AV13OrderedBy == 4 )
         {
            Ddo_incidentes_demandacod_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_incidentes_demandacod_Internalname, "SortedStatus", Ddo_incidentes_demandacod_Sortedstatus);
         }
         else if ( AV13OrderedBy == 5 )
         {
            Ddo_incidentes_demandanum_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_incidentes_demandanum_Internalname, "SortedStatus", Ddo_incidentes_demandanum_Sortedstatus);
         }
      }

      protected void S112( )
      {
         /* 'ENABLEDYNAMICFILTERS1' Routine */
         tblTablemergeddynamicfiltersincidentes_data1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfiltersincidentes_data1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfiltersincidentes_data1_Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "INCIDENTES_DATA") == 0 )
         {
            tblTablemergeddynamicfiltersincidentes_data1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfiltersincidentes_data1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfiltersincidentes_data1_Visible), 5, 0)));
         }
      }

      protected void S122( )
      {
         /* 'ENABLEDYNAMICFILTERS2' Routine */
         tblTablemergeddynamicfiltersincidentes_data2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfiltersincidentes_data2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfiltersincidentes_data2_Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "INCIDENTES_DATA") == 0 )
         {
            tblTablemergeddynamicfiltersincidentes_data2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfiltersincidentes_data2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfiltersincidentes_data2_Visible), 5, 0)));
         }
      }

      protected void S132( )
      {
         /* 'ENABLEDYNAMICFILTERS3' Routine */
         tblTablemergeddynamicfiltersincidentes_data3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfiltersincidentes_data3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfiltersincidentes_data3_Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "INCIDENTES_DATA") == 0 )
         {
            tblTablemergeddynamicfiltersincidentes_data3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfiltersincidentes_data3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfiltersincidentes_data3_Visible), 5, 0)));
         }
      }

      protected void S192( )
      {
         /* 'RESETDYNFILTERS' Routine */
         AV18DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         AV19DynamicFiltersSelector2 = "INCIDENTES_DATA";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         AV20Incidentes_Data2 = (DateTime)(DateTime.MinValue);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20Incidentes_Data2", context.localUtil.TToC( AV20Incidentes_Data2, 8, 5, 0, 3, "/", ":", " "));
         AV21Incidentes_Data_To2 = (DateTime)(DateTime.MinValue);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Incidentes_Data_To2", context.localUtil.TToC( AV21Incidentes_Data_To2, 8, 5, 0, 3, "/", ":", " "));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV22DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
         AV23DynamicFiltersSelector3 = "INCIDENTES_DATA";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
         AV24Incidentes_Data3 = (DateTime)(DateTime.MinValue);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24Incidentes_Data3", context.localUtil.TToC( AV24Incidentes_Data3, 8, 5, 0, 3, "/", ":", " "));
         AV25Incidentes_Data_To3 = (DateTime)(DateTime.MinValue);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25Incidentes_Data_To3", context.localUtil.TToC( AV25Incidentes_Data_To3, 8, 5, 0, 3, "/", ":", " "));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S202( )
      {
         /* 'CLEANFILTERS' Routine */
         AV31TFIncidentes_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31TFIncidentes_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV31TFIncidentes_Codigo), 6, 0)));
         Ddo_incidentes_codigo_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_incidentes_codigo_Internalname, "FilteredText_set", Ddo_incidentes_codigo_Filteredtext_set);
         AV32TFIncidentes_Codigo_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32TFIncidentes_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV32TFIncidentes_Codigo_To), 6, 0)));
         Ddo_incidentes_codigo_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_incidentes_codigo_Internalname, "FilteredTextTo_set", Ddo_incidentes_codigo_Filteredtextto_set);
         AV35TFIncidentes_Data = (DateTime)(DateTime.MinValue);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TFIncidentes_Data", context.localUtil.TToC( AV35TFIncidentes_Data, 8, 5, 0, 3, "/", ":", " "));
         Ddo_incidentes_data_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_incidentes_data_Internalname, "FilteredText_set", Ddo_incidentes_data_Filteredtext_set);
         AV36TFIncidentes_Data_To = (DateTime)(DateTime.MinValue);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36TFIncidentes_Data_To", context.localUtil.TToC( AV36TFIncidentes_Data_To, 8, 5, 0, 3, "/", ":", " "));
         Ddo_incidentes_data_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_incidentes_data_Internalname, "FilteredTextTo_set", Ddo_incidentes_data_Filteredtextto_set);
         AV41TFIncidentes_Descricao = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41TFIncidentes_Descricao", AV41TFIncidentes_Descricao);
         Ddo_incidentes_descricao_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_incidentes_descricao_Internalname, "FilteredText_set", Ddo_incidentes_descricao_Filteredtext_set);
         AV42TFIncidentes_Descricao_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42TFIncidentes_Descricao_Sel", AV42TFIncidentes_Descricao_Sel);
         Ddo_incidentes_descricao_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_incidentes_descricao_Internalname, "SelectedValue_set", Ddo_incidentes_descricao_Selectedvalue_set);
         AV45TFIncidentes_DemandaCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45TFIncidentes_DemandaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV45TFIncidentes_DemandaCod), 6, 0)));
         Ddo_incidentes_demandacod_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_incidentes_demandacod_Internalname, "FilteredText_set", Ddo_incidentes_demandacod_Filteredtext_set);
         AV46TFIncidentes_DemandaCod_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46TFIncidentes_DemandaCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV46TFIncidentes_DemandaCod_To), 6, 0)));
         Ddo_incidentes_demandacod_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_incidentes_demandacod_Internalname, "FilteredTextTo_set", Ddo_incidentes_demandacod_Filteredtextto_set);
         AV49TFIncidentes_DemandaNum = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49TFIncidentes_DemandaNum", AV49TFIncidentes_DemandaNum);
         Ddo_incidentes_demandanum_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_incidentes_demandanum_Internalname, "FilteredText_set", Ddo_incidentes_demandanum_Filteredtext_set);
         AV50TFIncidentes_DemandaNum_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50TFIncidentes_DemandaNum_Sel", AV50TFIncidentes_DemandaNum_Sel);
         Ddo_incidentes_demandanum_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_incidentes_demandanum_Internalname, "SelectedValue_set", Ddo_incidentes_demandanum_Selectedvalue_set);
         AV15DynamicFiltersSelector1 = "INCIDENTES_DATA";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         AV16Incidentes_Data1 = (DateTime)(DateTime.MinValue);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16Incidentes_Data1", context.localUtil.TToC( AV16Incidentes_Data1, 8, 5, 0, 3, "/", ":", " "));
         AV17Incidentes_Data_To1 = (DateTime)(DateTime.MinValue);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Incidentes_Data_To1", context.localUtil.TToC( AV17Incidentes_Data_To1, 8, 5, 0, 3, "/", ":", " "));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S142( )
      {
         /* 'LOADDYNFILTERSSTATE' Routine */
         imgAdddynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         imgAdddynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
         if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(1));
            AV15DynamicFiltersSelector1 = AV12GridStateDynamicFilter.gxTpr_Selected;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "INCIDENTES_DATA") == 0 )
            {
               AV16Incidentes_Data1 = context.localUtil.CToT( AV12GridStateDynamicFilter.gxTpr_Value, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16Incidentes_Data1", context.localUtil.TToC( AV16Incidentes_Data1, 8, 5, 0, 3, "/", ":", " "));
               AV17Incidentes_Data_To1 = context.localUtil.CToT( AV12GridStateDynamicFilter.gxTpr_Valueto, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Incidentes_Data_To1", context.localUtil.TToC( AV17Incidentes_Data_To1, 8, 5, 0, 3, "/", ":", " "));
            }
            /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               lblJsdynamicfilters_Caption = "<script type=\"text/javascript\">";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(2);";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               imgAdddynamicfilters1_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
               imgRemovedynamicfilters1_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
               AV18DynamicFiltersEnabled2 = true;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
               AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(2));
               AV19DynamicFiltersSelector2 = AV12GridStateDynamicFilter.gxTpr_Selected;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
               if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "INCIDENTES_DATA") == 0 )
               {
                  AV20Incidentes_Data2 = context.localUtil.CToT( AV12GridStateDynamicFilter.gxTpr_Value, 2);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20Incidentes_Data2", context.localUtil.TToC( AV20Incidentes_Data2, 8, 5, 0, 3, "/", ":", " "));
                  AV21Incidentes_Data_To2 = context.localUtil.CToT( AV12GridStateDynamicFilter.gxTpr_Valueto, 2);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Incidentes_Data_To2", context.localUtil.TToC( AV21Incidentes_Data_To2, 8, 5, 0, 3, "/", ":", " "));
               }
               /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
               S122 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(3);";
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
                  imgAdddynamicfilters2_Visible = 0;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
                  imgRemovedynamicfilters2_Visible = 1;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
                  AV22DynamicFiltersEnabled3 = true;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
                  AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(3));
                  AV23DynamicFiltersSelector3 = AV12GridStateDynamicFilter.gxTpr_Selected;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
                  if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "INCIDENTES_DATA") == 0 )
                  {
                     AV24Incidentes_Data3 = context.localUtil.CToT( AV12GridStateDynamicFilter.gxTpr_Value, 2);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24Incidentes_Data3", context.localUtil.TToC( AV24Incidentes_Data3, 8, 5, 0, 3, "/", ":", " "));
                     AV25Incidentes_Data_To3 = context.localUtil.CToT( AV12GridStateDynamicFilter.gxTpr_Valueto, 2);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25Incidentes_Data_To3", context.localUtil.TToC( AV25Incidentes_Data_To3, 8, 5, 0, 3, "/", ":", " "));
                  }
                  /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
                  S132 ();
                  if ( returnInSub )
                  {
                     returnInSub = true;
                     if (true) return;
                  }
               }
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"</script>";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
            }
         }
         if ( AV26DynamicFiltersRemoving )
         {
            lblJsdynamicfilters_Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         }
      }

      protected void S162( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV10GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV10GridState.gxTpr_Orderedby = AV13OrderedBy;
         AV10GridState.gxTpr_Ordereddsc = AV14OrderedDsc;
         AV10GridState.gxTpr_Filtervalues.Clear();
         if ( ! ( (0==AV31TFIncidentes_Codigo) && (0==AV32TFIncidentes_Codigo_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFINCIDENTES_CODIGO";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV31TFIncidentes_Codigo), 6, 0);
            AV11GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( (decimal)(AV32TFIncidentes_Codigo_To), 6, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (DateTime.MinValue==AV35TFIncidentes_Data) && (DateTime.MinValue==AV36TFIncidentes_Data_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFINCIDENTES_DATA";
            AV11GridStateFilterValue.gxTpr_Value = context.localUtil.TToC( AV35TFIncidentes_Data, 8, 5, 0, 3, "/", ":", " ");
            AV11GridStateFilterValue.gxTpr_Valueto = context.localUtil.TToC( AV36TFIncidentes_Data_To, 8, 5, 0, 3, "/", ":", " ");
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV41TFIncidentes_Descricao)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFINCIDENTES_DESCRICAO";
            AV11GridStateFilterValue.gxTpr_Value = AV41TFIncidentes_Descricao;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV42TFIncidentes_Descricao_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFINCIDENTES_DESCRICAO_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV42TFIncidentes_Descricao_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (0==AV45TFIncidentes_DemandaCod) && (0==AV46TFIncidentes_DemandaCod_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFINCIDENTES_DEMANDACOD";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV45TFIncidentes_DemandaCod), 6, 0);
            AV11GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( (decimal)(AV46TFIncidentes_DemandaCod_To), 6, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV49TFIncidentes_DemandaNum)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFINCIDENTES_DEMANDANUM";
            AV11GridStateFilterValue.gxTpr_Value = AV49TFIncidentes_DemandaNum;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV50TFIncidentes_DemandaNum_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFINCIDENTES_DEMANDANUM_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV50TFIncidentes_DemandaNum_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         new wwpbaseobjects.savegridstate(context ).execute(  AV59Pgmname+"GridState",  AV10GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_MeetrikaVs3")) ;
      }

      protected void S182( )
      {
         /* 'SAVEDYNFILTERSSTATE' Routine */
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         if ( ! AV27DynamicFiltersIgnoreFirst )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV15DynamicFiltersSelector1;
            if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "INCIDENTES_DATA") == 0 ) && ! ( (DateTime.MinValue==AV16Incidentes_Data1) && (DateTime.MinValue==AV17Incidentes_Data_To1) ) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = context.localUtil.TToC( AV16Incidentes_Data1, 8, 5, 0, 3, "/", ":", " ");
               AV12GridStateDynamicFilter.gxTpr_Valueto = context.localUtil.TToC( AV17Incidentes_Data_To1, 8, 5, 0, 3, "/", ":", " ");
            }
            if ( AV26DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Valueto)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV18DynamicFiltersEnabled2 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV19DynamicFiltersSelector2;
            if ( ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "INCIDENTES_DATA") == 0 ) && ! ( (DateTime.MinValue==AV20Incidentes_Data2) && (DateTime.MinValue==AV21Incidentes_Data_To2) ) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = context.localUtil.TToC( AV20Incidentes_Data2, 8, 5, 0, 3, "/", ":", " ");
               AV12GridStateDynamicFilter.gxTpr_Valueto = context.localUtil.TToC( AV21Incidentes_Data_To2, 8, 5, 0, 3, "/", ":", " ");
            }
            if ( AV26DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Valueto)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV22DynamicFiltersEnabled3 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV23DynamicFiltersSelector3;
            if ( ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "INCIDENTES_DATA") == 0 ) && ! ( (DateTime.MinValue==AV24Incidentes_Data3) && (DateTime.MinValue==AV25Incidentes_Data_To3) ) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = context.localUtil.TToC( AV24Incidentes_Data3, 8, 5, 0, 3, "/", ":", " ");
               AV12GridStateDynamicFilter.gxTpr_Valueto = context.localUtil.TToC( AV25Incidentes_Data_To3, 8, 5, 0, 3, "/", ":", " ");
            }
            if ( AV26DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Valueto)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
      }

      protected void wb_table1_2_J52( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableSearchCell'>") ;
            wb_table2_5_J52( true) ;
         }
         else
         {
            wb_table2_5_J52( false) ;
         }
         return  ;
      }

      protected void wb_table2_5_J52e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_80_J52( true) ;
         }
         else
         {
            wb_table3_80_J52( false) ;
         }
         return  ;
      }

      protected void wb_table3_80_J52e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_J52e( true) ;
         }
         else
         {
            wb_table1_2_J52e( false) ;
         }
      }

      protected void wb_table3_80_J52( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "Table100x100", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_83_J52( true) ;
         }
         else
         {
            wb_table4_83_J52( false) ;
         }
         return  ;
      }

      protected void wb_table4_83_J52e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_80_J52e( true) ;
         }
         else
         {
            wb_table3_80_J52e( false) ;
         }
      }

      protected void wb_table4_83_J52( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"DivS\" data-gxgridid=\"86\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtIncidentes_Codigo_Titleformat == 0 )
               {
                  context.SendWebValue( edtIncidentes_Codigo_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtIncidentes_Codigo_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtIncidentes_Data_Titleformat == 0 )
               {
                  context.SendWebValue( edtIncidentes_Data_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtIncidentes_Data_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtIncidentes_Descricao_Titleformat == 0 )
               {
                  context.SendWebValue( edtIncidentes_Descricao_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtIncidentes_Descricao_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtIncidentes_DemandaCod_Titleformat == 0 )
               {
                  context.SendWebValue( edtIncidentes_DemandaCod_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtIncidentes_DemandaCod_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtIncidentes_DemandaNum_Titleformat == 0 )
               {
                  context.SendWebValue( edtIncidentes_DemandaNum_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtIncidentes_DemandaNum_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", "");
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV28Select));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavSelect_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1241Incidentes_Codigo), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtIncidentes_Codigo_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtIncidentes_Codigo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.localUtil.TToC( A1242Incidentes_Data, 10, 8, 0, 3, "/", ":", " "));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtIncidentes_Data_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtIncidentes_Data_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A1243Incidentes_Descricao);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtIncidentes_Descricao_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtIncidentes_Descricao_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1239Incidentes_DemandaCod), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtIncidentes_DemandaCod_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtIncidentes_DemandaCod_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A1240Incidentes_DemandaNum);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtIncidentes_DemandaNum_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtIncidentes_DemandaNum_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 86 )
         {
            wbEnd = 0;
            nRC_GXsfl_86 = (short)(nGXsfl_86_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_83_J52e( true) ;
         }
         else
         {
            wb_table4_83_J52e( false) ;
         }
      }

      protected void wb_table2_5_J52( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablesearch_Internalname, tblTablesearch_Internalname, "", "TableSearch", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='OrderedByDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblOrderedtext_Internalname, "Ordenado por", "", "", lblOrderedtext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "OrderedByDescription", 0, "", 1, 1, 0, "HLP_PromptContagemResultadoIncidentes.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 10,'',false,'" + sGXsfl_86_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavOrderedby, cmbavOrderedby_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)), 1, cmbavOrderedby_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVORDEREDBY.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,10);\"", "", true, "HLP_PromptContagemResultadoIncidentes.htm");
            cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", (String)(cmbavOrderedby.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 11,'',false,'" + sGXsfl_86_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV14OrderedDsc), StringUtil.BoolToStr( AV14OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,11);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_PromptContagemResultadoIncidentes.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableFiltersCell'>") ;
            wb_table5_14_J52( true) ;
         }
         else
         {
            wb_table5_14_J52( false) ;
         }
         return  ;
      }

      protected void wb_table5_14_J52e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_J52e( true) ;
         }
         else
         {
            wb_table2_5_J52e( false) ;
         }
      }

      protected void wb_table5_14_J52( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='GridHeaderCellCleanFilters'>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 17,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgCleanfilters_Internalname, context.GetImagePath( "effedd9a-25cf-4497-952f-5a5ac032b8be", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Limpar filtros", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgCleanfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOCLEANFILTERS\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptContagemResultadoIncidentes.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataFilterContentCell'>") ;
            wb_table6_19_J52( true) ;
         }
         else
         {
            wb_table6_19_J52( false) ;
         }
         return  ;
      }

      protected void wb_table6_19_J52e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblJsdynamicfilters_Internalname, lblJsdynamicfilters_Caption, "", "", lblJsdynamicfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "", 0, "", 1, 1, 1, "HLP_PromptContagemResultadoIncidentes.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_14_J52e( true) ;
         }
         else
         {
            wb_table5_14_J52e( false) ;
         }
      }

      protected void wb_table6_19_J52( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabledynamicfilters_Internalname, tblTabledynamicfilters_Internalname, "", "TableDynamicFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix1_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContagemResultadoIncidentes.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 24,'',false,'" + sGXsfl_86_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector1, cmbavDynamicfiltersselector1_Internalname, StringUtil.RTrim( AV15DynamicFiltersSelector1), 1, cmbavDynamicfiltersselector1_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR1.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,24);\"", "", true, "HLP_PromptContagemResultadoIncidentes.htm");
            cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", (String)(cmbavDynamicfiltersselector1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle1_Internalname, "valor", "", "", lblDynamicfiltersmiddle1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContagemResultadoIncidentes.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table7_28_J52( true) ;
         }
         else
         {
            wb_table7_28_J52( false) ;
         }
         return  ;
      }

      protected void wb_table7_28_J52e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 37,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters1_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters1_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptContagemResultadoIncidentes.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 38,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters1_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters1_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptContagemResultadoIncidentes.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow2\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix2_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContagemResultadoIncidentes.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 43,'',false,'" + sGXsfl_86_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector2, cmbavDynamicfiltersselector2_Internalname, StringUtil.RTrim( AV19DynamicFiltersSelector2), 1, cmbavDynamicfiltersselector2_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR2.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,43);\"", "", true, "HLP_PromptContagemResultadoIncidentes.htm");
            cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", (String)(cmbavDynamicfiltersselector2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle2_Internalname, "valor", "", "", lblDynamicfiltersmiddle2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContagemResultadoIncidentes.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table8_47_J52( true) ;
         }
         else
         {
            wb_table8_47_J52( false) ;
         }
         return  ;
      }

      protected void wb_table8_47_J52e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 56,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters2_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters2_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptContagemResultadoIncidentes.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 57,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters2_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters2_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptContagemResultadoIncidentes.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow3\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix3_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContagemResultadoIncidentes.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 62,'',false,'" + sGXsfl_86_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector3, cmbavDynamicfiltersselector3_Internalname, StringUtil.RTrim( AV23DynamicFiltersSelector3), 1, cmbavDynamicfiltersselector3_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR3.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,62);\"", "", true, "HLP_PromptContagemResultadoIncidentes.htm");
            cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", (String)(cmbavDynamicfiltersselector3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle3_Internalname, "valor", "", "", lblDynamicfiltersmiddle3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContagemResultadoIncidentes.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table9_66_J52( true) ;
         }
         else
         {
            wb_table9_66_J52( false) ;
         }
         return  ;
      }

      protected void wb_table9_66_J52e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 75,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters3_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters3_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS3\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptContagemResultadoIncidentes.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_19_J52e( true) ;
         }
         else
         {
            wb_table6_19_J52e( false) ;
         }
      }

      protected void wb_table9_66_J52( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTablemergeddynamicfiltersincidentes_data3_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfiltersincidentes_data3_Internalname, tblTablemergeddynamicfiltersincidentes_data3_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 69,'',false,'" + sGXsfl_86_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavIncidentes_data3_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavIncidentes_data3_Internalname, context.localUtil.TToC( AV24Incidentes_Data3, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( AV24Incidentes_Data3, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,69);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavIncidentes_data3_Jsonclick, 0, "BootstrapAttributeDateTime", "", "", "", 1, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContagemResultadoIncidentes.htm");
            GxWebStd.gx_bitmap( context, edtavIncidentes_data3_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptContagemResultadoIncidentes.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersincidentes_data_rangemiddletext3_Internalname, "at�", "", "", lblDynamicfiltersincidentes_data_rangemiddletext3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContagemResultadoIncidentes.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 73,'',false,'" + sGXsfl_86_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavIncidentes_data_to3_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavIncidentes_data_to3_Internalname, context.localUtil.TToC( AV25Incidentes_Data_To3, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( AV25Incidentes_Data_To3, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,73);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavIncidentes_data_to3_Jsonclick, 0, "BootstrapAttributeDateTime", "", "", "", 1, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContagemResultadoIncidentes.htm");
            GxWebStd.gx_bitmap( context, edtavIncidentes_data_to3_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptContagemResultadoIncidentes.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table9_66_J52e( true) ;
         }
         else
         {
            wb_table9_66_J52e( false) ;
         }
      }

      protected void wb_table8_47_J52( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTablemergeddynamicfiltersincidentes_data2_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfiltersincidentes_data2_Internalname, tblTablemergeddynamicfiltersincidentes_data2_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 50,'',false,'" + sGXsfl_86_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavIncidentes_data2_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavIncidentes_data2_Internalname, context.localUtil.TToC( AV20Incidentes_Data2, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( AV20Incidentes_Data2, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,50);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavIncidentes_data2_Jsonclick, 0, "BootstrapAttributeDateTime", "", "", "", 1, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContagemResultadoIncidentes.htm");
            GxWebStd.gx_bitmap( context, edtavIncidentes_data2_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptContagemResultadoIncidentes.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersincidentes_data_rangemiddletext2_Internalname, "at�", "", "", lblDynamicfiltersincidentes_data_rangemiddletext2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContagemResultadoIncidentes.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 54,'',false,'" + sGXsfl_86_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavIncidentes_data_to2_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavIncidentes_data_to2_Internalname, context.localUtil.TToC( AV21Incidentes_Data_To2, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( AV21Incidentes_Data_To2, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,54);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavIncidentes_data_to2_Jsonclick, 0, "BootstrapAttributeDateTime", "", "", "", 1, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContagemResultadoIncidentes.htm");
            GxWebStd.gx_bitmap( context, edtavIncidentes_data_to2_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptContagemResultadoIncidentes.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table8_47_J52e( true) ;
         }
         else
         {
            wb_table8_47_J52e( false) ;
         }
      }

      protected void wb_table7_28_J52( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTablemergeddynamicfiltersincidentes_data1_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfiltersincidentes_data1_Internalname, tblTablemergeddynamicfiltersincidentes_data1_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 31,'',false,'" + sGXsfl_86_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavIncidentes_data1_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavIncidentes_data1_Internalname, context.localUtil.TToC( AV16Incidentes_Data1, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( AV16Incidentes_Data1, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,31);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavIncidentes_data1_Jsonclick, 0, "BootstrapAttributeDateTime", "", "", "", 1, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContagemResultadoIncidentes.htm");
            GxWebStd.gx_bitmap( context, edtavIncidentes_data1_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptContagemResultadoIncidentes.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersincidentes_data_rangemiddletext1_Internalname, "at�", "", "", lblDynamicfiltersincidentes_data_rangemiddletext1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptContagemResultadoIncidentes.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 35,'',false,'" + sGXsfl_86_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavIncidentes_data_to1_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavIncidentes_data_to1_Internalname, context.localUtil.TToC( AV17Incidentes_Data_To1, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( AV17Incidentes_Data_To1, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,35);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavIncidentes_data_to1_Jsonclick, 0, "BootstrapAttributeDateTime", "", "", "", 1, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptContagemResultadoIncidentes.htm");
            GxWebStd.gx_bitmap( context, edtavIncidentes_data_to1_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptContagemResultadoIncidentes.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_28_J52e( true) ;
         }
         else
         {
            wb_table7_28_J52e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV7InOutIncidentes_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InOutIncidentes_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7InOutIncidentes_Codigo), 6, 0)));
         AV8InOutIncidentes_Data = (DateTime)getParm(obj,1);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8InOutIncidentes_Data", context.localUtil.TToC( AV8InOutIncidentes_Data, 8, 5, 0, 3, "/", ":", " "));
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAJ52( ) ;
         WSJ52( ) ;
         WEJ52( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20206216235998");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("promptcontagemresultadoincidentes.js", "?20206216235998");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_862( )
      {
         edtavSelect_Internalname = "vSELECT_"+sGXsfl_86_idx;
         edtIncidentes_Codigo_Internalname = "INCIDENTES_CODIGO_"+sGXsfl_86_idx;
         edtIncidentes_Data_Internalname = "INCIDENTES_DATA_"+sGXsfl_86_idx;
         edtIncidentes_Descricao_Internalname = "INCIDENTES_DESCRICAO_"+sGXsfl_86_idx;
         edtIncidentes_DemandaCod_Internalname = "INCIDENTES_DEMANDACOD_"+sGXsfl_86_idx;
         edtIncidentes_DemandaNum_Internalname = "INCIDENTES_DEMANDANUM_"+sGXsfl_86_idx;
      }

      protected void SubsflControlProps_fel_862( )
      {
         edtavSelect_Internalname = "vSELECT_"+sGXsfl_86_fel_idx;
         edtIncidentes_Codigo_Internalname = "INCIDENTES_CODIGO_"+sGXsfl_86_fel_idx;
         edtIncidentes_Data_Internalname = "INCIDENTES_DATA_"+sGXsfl_86_fel_idx;
         edtIncidentes_Descricao_Internalname = "INCIDENTES_DESCRICAO_"+sGXsfl_86_fel_idx;
         edtIncidentes_DemandaCod_Internalname = "INCIDENTES_DEMANDACOD_"+sGXsfl_86_fel_idx;
         edtIncidentes_DemandaNum_Internalname = "INCIDENTES_DEMANDANUM_"+sGXsfl_86_fel_idx;
      }

      protected void sendrow_862( )
      {
         SubsflControlProps_862( ) ;
         WBJ50( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_86_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_86_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_86_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Active Bitmap Variable */
            TempTags = " " + ((edtavSelect_Enabled!=0)&&(edtavSelect_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 87,'',false,'',86)\"" : " ");
            ClassString = "Image";
            StyleString = "";
            AV28Select_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV28Select))&&String.IsNullOrEmpty(StringUtil.RTrim( AV58Select_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV28Select)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavSelect_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV28Select)) ? AV58Select_GXI : context.PathToRelativeUrl( AV28Select)),(String)"",(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavSelect_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)5,(String)edtavSelect_Jsonclick,"'"+""+"'"+",false,"+"'"+"EENTER."+sGXsfl_86_idx+"'",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)""+TempTags,(String)"",(String)"",(short)1,(bool)AV28Select_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtIncidentes_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A1241Incidentes_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A1241Incidentes_Codigo), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtIncidentes_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)86,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtIncidentes_Data_Internalname,context.localUtil.TToC( A1242Incidentes_Data, 10, 8, 0, 3, "/", ":", " "),context.localUtil.Format( A1242Incidentes_Data, "99/99/99 99:99"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtIncidentes_Data_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)14,(short)0,(short)0,(short)86,(short)1,(short)-1,(short)0,(bool)true,(String)"DataHora",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtIncidentes_Descricao_Internalname,(String)A1243Incidentes_Descricao,(String)A1243Incidentes_Descricao,(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtIncidentes_Descricao_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)500,(short)0,(short)0,(short)86,(short)1,(short)0,(short)-1,(bool)true,(String)"DescricaoLonga",(String)"left",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtIncidentes_DemandaCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A1239Incidentes_DemandaCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A1239Incidentes_DemandaCod), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtIncidentes_DemandaCod_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)86,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtIncidentes_DemandaNum_Internalname,(String)A1240Incidentes_DemandaNum,StringUtil.RTrim( context.localUtil.Format( A1240Incidentes_DemandaNum, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtIncidentes_DemandaNum_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)30,(short)0,(short)0,(short)86,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            GxWebStd.gx_hidden_field( context, "gxhash_INCIDENTES_CODIGO"+"_"+sGXsfl_86_idx, GetSecureSignedToken( sGXsfl_86_idx, context.localUtil.Format( (decimal)(A1241Incidentes_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_INCIDENTES_DATA"+"_"+sGXsfl_86_idx, GetSecureSignedToken( sGXsfl_86_idx, context.localUtil.Format( A1242Incidentes_Data, "99/99/99 99:99")));
            GxWebStd.gx_hidden_field( context, "gxhash_INCIDENTES_DESCRICAO"+"_"+sGXsfl_86_idx, GetSecureSignedToken( sGXsfl_86_idx, A1243Incidentes_Descricao));
            GxWebStd.gx_hidden_field( context, "gxhash_INCIDENTES_DEMANDACOD"+"_"+sGXsfl_86_idx, GetSecureSignedToken( sGXsfl_86_idx, context.localUtil.Format( (decimal)(A1239Incidentes_DemandaCod), "ZZZZZ9")));
            GridContainer.AddRow(GridRow);
            nGXsfl_86_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_86_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_86_idx+1));
            sGXsfl_86_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_86_idx), 4, 0)), 4, "0");
            SubsflControlProps_862( ) ;
         }
         /* End function sendrow_862 */
      }

      protected void init_default_properties( )
      {
         lblOrderedtext_Internalname = "ORDEREDTEXT";
         cmbavOrderedby_Internalname = "vORDEREDBY";
         edtavOrdereddsc_Internalname = "vORDEREDDSC";
         imgCleanfilters_Internalname = "CLEANFILTERS";
         lblDynamicfiltersprefix1_Internalname = "DYNAMICFILTERSPREFIX1";
         cmbavDynamicfiltersselector1_Internalname = "vDYNAMICFILTERSSELECTOR1";
         lblDynamicfiltersmiddle1_Internalname = "DYNAMICFILTERSMIDDLE1";
         edtavIncidentes_data1_Internalname = "vINCIDENTES_DATA1";
         lblDynamicfiltersincidentes_data_rangemiddletext1_Internalname = "DYNAMICFILTERSINCIDENTES_DATA_RANGEMIDDLETEXT1";
         edtavIncidentes_data_to1_Internalname = "vINCIDENTES_DATA_TO1";
         tblTablemergeddynamicfiltersincidentes_data1_Internalname = "TABLEMERGEDDYNAMICFILTERSINCIDENTES_DATA1";
         imgAdddynamicfilters1_Internalname = "ADDDYNAMICFILTERS1";
         imgRemovedynamicfilters1_Internalname = "REMOVEDYNAMICFILTERS1";
         lblDynamicfiltersprefix2_Internalname = "DYNAMICFILTERSPREFIX2";
         cmbavDynamicfiltersselector2_Internalname = "vDYNAMICFILTERSSELECTOR2";
         lblDynamicfiltersmiddle2_Internalname = "DYNAMICFILTERSMIDDLE2";
         edtavIncidentes_data2_Internalname = "vINCIDENTES_DATA2";
         lblDynamicfiltersincidentes_data_rangemiddletext2_Internalname = "DYNAMICFILTERSINCIDENTES_DATA_RANGEMIDDLETEXT2";
         edtavIncidentes_data_to2_Internalname = "vINCIDENTES_DATA_TO2";
         tblTablemergeddynamicfiltersincidentes_data2_Internalname = "TABLEMERGEDDYNAMICFILTERSINCIDENTES_DATA2";
         imgAdddynamicfilters2_Internalname = "ADDDYNAMICFILTERS2";
         imgRemovedynamicfilters2_Internalname = "REMOVEDYNAMICFILTERS2";
         lblDynamicfiltersprefix3_Internalname = "DYNAMICFILTERSPREFIX3";
         cmbavDynamicfiltersselector3_Internalname = "vDYNAMICFILTERSSELECTOR3";
         lblDynamicfiltersmiddle3_Internalname = "DYNAMICFILTERSMIDDLE3";
         edtavIncidentes_data3_Internalname = "vINCIDENTES_DATA3";
         lblDynamicfiltersincidentes_data_rangemiddletext3_Internalname = "DYNAMICFILTERSINCIDENTES_DATA_RANGEMIDDLETEXT3";
         edtavIncidentes_data_to3_Internalname = "vINCIDENTES_DATA_TO3";
         tblTablemergeddynamicfiltersincidentes_data3_Internalname = "TABLEMERGEDDYNAMICFILTERSINCIDENTES_DATA3";
         imgRemovedynamicfilters3_Internalname = "REMOVEDYNAMICFILTERS3";
         tblTabledynamicfilters_Internalname = "TABLEDYNAMICFILTERS";
         lblJsdynamicfilters_Internalname = "JSDYNAMICFILTERS";
         tblTablefilters_Internalname = "TABLEFILTERS";
         tblTablesearch_Internalname = "TABLESEARCH";
         edtavSelect_Internalname = "vSELECT";
         edtIncidentes_Codigo_Internalname = "INCIDENTES_CODIGO";
         edtIncidentes_Data_Internalname = "INCIDENTES_DATA";
         edtIncidentes_Descricao_Internalname = "INCIDENTES_DESCRICAO";
         edtIncidentes_DemandaCod_Internalname = "INCIDENTES_DEMANDACOD";
         edtIncidentes_DemandaNum_Internalname = "INCIDENTES_DEMANDANUM";
         Gridpaginationbar_Internalname = "GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = "GRIDTABLEWITHPAGINATIONBAR";
         tblTablegridheader_Internalname = "TABLEGRIDHEADER";
         tblTablemain_Internalname = "TABLEMAIN";
         Workwithplusutilities1_Internalname = "WORKWITHPLUSUTILITIES1";
         chkavDynamicfiltersenabled2_Internalname = "vDYNAMICFILTERSENABLED2";
         chkavDynamicfiltersenabled3_Internalname = "vDYNAMICFILTERSENABLED3";
         edtavTfincidentes_codigo_Internalname = "vTFINCIDENTES_CODIGO";
         edtavTfincidentes_codigo_to_Internalname = "vTFINCIDENTES_CODIGO_TO";
         edtavTfincidentes_data_Internalname = "vTFINCIDENTES_DATA";
         edtavTfincidentes_data_to_Internalname = "vTFINCIDENTES_DATA_TO";
         edtavDdo_incidentes_dataauxdate_Internalname = "vDDO_INCIDENTES_DATAAUXDATE";
         edtavDdo_incidentes_dataauxdateto_Internalname = "vDDO_INCIDENTES_DATAAUXDATETO";
         divDdo_incidentes_dataauxdates_Internalname = "DDO_INCIDENTES_DATAAUXDATES";
         edtavTfincidentes_descricao_Internalname = "vTFINCIDENTES_DESCRICAO";
         edtavTfincidentes_descricao_sel_Internalname = "vTFINCIDENTES_DESCRICAO_SEL";
         edtavTfincidentes_demandacod_Internalname = "vTFINCIDENTES_DEMANDACOD";
         edtavTfincidentes_demandacod_to_Internalname = "vTFINCIDENTES_DEMANDACOD_TO";
         edtavTfincidentes_demandanum_Internalname = "vTFINCIDENTES_DEMANDANUM";
         edtavTfincidentes_demandanum_sel_Internalname = "vTFINCIDENTES_DEMANDANUM_SEL";
         Ddo_incidentes_codigo_Internalname = "DDO_INCIDENTES_CODIGO";
         edtavDdo_incidentes_codigotitlecontrolidtoreplace_Internalname = "vDDO_INCIDENTES_CODIGOTITLECONTROLIDTOREPLACE";
         Ddo_incidentes_data_Internalname = "DDO_INCIDENTES_DATA";
         edtavDdo_incidentes_datatitlecontrolidtoreplace_Internalname = "vDDO_INCIDENTES_DATATITLECONTROLIDTOREPLACE";
         Ddo_incidentes_descricao_Internalname = "DDO_INCIDENTES_DESCRICAO";
         edtavDdo_incidentes_descricaotitlecontrolidtoreplace_Internalname = "vDDO_INCIDENTES_DESCRICAOTITLECONTROLIDTOREPLACE";
         Ddo_incidentes_demandacod_Internalname = "DDO_INCIDENTES_DEMANDACOD";
         edtavDdo_incidentes_demandacodtitlecontrolidtoreplace_Internalname = "vDDO_INCIDENTES_DEMANDACODTITLECONTROLIDTOREPLACE";
         Ddo_incidentes_demandanum_Internalname = "DDO_INCIDENTES_DEMANDANUM";
         edtavDdo_incidentes_demandanumtitlecontrolidtoreplace_Internalname = "vDDO_INCIDENTES_DEMANDANUMTITLECONTROLIDTOREPLACE";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtIncidentes_DemandaNum_Jsonclick = "";
         edtIncidentes_DemandaCod_Jsonclick = "";
         edtIncidentes_Descricao_Jsonclick = "";
         edtIncidentes_Data_Jsonclick = "";
         edtIncidentes_Codigo_Jsonclick = "";
         edtavSelect_Jsonclick = "";
         edtavSelect_Visible = -1;
         edtavSelect_Enabled = 1;
         edtavIncidentes_data_to1_Jsonclick = "";
         edtavIncidentes_data1_Jsonclick = "";
         edtavIncidentes_data_to2_Jsonclick = "";
         edtavIncidentes_data2_Jsonclick = "";
         edtavIncidentes_data_to3_Jsonclick = "";
         edtavIncidentes_data3_Jsonclick = "";
         cmbavDynamicfiltersselector3_Jsonclick = "";
         imgRemovedynamicfilters2_Visible = 1;
         imgAdddynamicfilters2_Visible = 1;
         cmbavDynamicfiltersselector2_Jsonclick = "";
         imgRemovedynamicfilters1_Visible = 1;
         imgAdddynamicfilters1_Visible = 1;
         cmbavDynamicfiltersselector1_Jsonclick = "";
         edtavOrdereddsc_Jsonclick = "";
         cmbavOrderedby_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtavSelect_Tooltiptext = "Selecionar";
         edtIncidentes_DemandaNum_Titleformat = 0;
         edtIncidentes_DemandaCod_Titleformat = 0;
         edtIncidentes_Descricao_Titleformat = 0;
         edtIncidentes_Data_Titleformat = 0;
         edtIncidentes_Codigo_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         tblTablemergeddynamicfiltersincidentes_data3_Visible = 1;
         tblTablemergeddynamicfiltersincidentes_data2_Visible = 1;
         tblTablemergeddynamicfiltersincidentes_data1_Visible = 1;
         edtIncidentes_DemandaNum_Title = "Demanda";
         edtIncidentes_DemandaCod_Title = "Cod";
         edtIncidentes_Descricao_Title = "Descricao";
         edtIncidentes_Data_Title = "Data";
         edtIncidentes_Codigo_Title = "Codigo";
         edtavOrdereddsc_Visible = 1;
         lblJsdynamicfilters_Caption = "JSDynamicFilters";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         chkavDynamicfiltersenabled3.Caption = "";
         chkavDynamicfiltersenabled2.Caption = "";
         edtavDdo_incidentes_demandanumtitlecontrolidtoreplace_Visible = 1;
         edtavDdo_incidentes_demandacodtitlecontrolidtoreplace_Visible = 1;
         edtavDdo_incidentes_descricaotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_incidentes_datatitlecontrolidtoreplace_Visible = 1;
         edtavDdo_incidentes_codigotitlecontrolidtoreplace_Visible = 1;
         edtavTfincidentes_demandanum_sel_Jsonclick = "";
         edtavTfincidentes_demandanum_sel_Visible = 1;
         edtavTfincidentes_demandanum_Jsonclick = "";
         edtavTfincidentes_demandanum_Visible = 1;
         edtavTfincidentes_demandacod_to_Jsonclick = "";
         edtavTfincidentes_demandacod_to_Visible = 1;
         edtavTfincidentes_demandacod_Jsonclick = "";
         edtavTfincidentes_demandacod_Visible = 1;
         edtavTfincidentes_descricao_sel_Visible = 1;
         edtavTfincidentes_descricao_Visible = 1;
         edtavDdo_incidentes_dataauxdateto_Jsonclick = "";
         edtavDdo_incidentes_dataauxdate_Jsonclick = "";
         edtavTfincidentes_data_to_Jsonclick = "";
         edtavTfincidentes_data_to_Visible = 1;
         edtavTfincidentes_data_Jsonclick = "";
         edtavTfincidentes_data_Visible = 1;
         edtavTfincidentes_codigo_to_Jsonclick = "";
         edtavTfincidentes_codigo_to_Visible = 1;
         edtavTfincidentes_codigo_Jsonclick = "";
         edtavTfincidentes_codigo_Visible = 1;
         chkavDynamicfiltersenabled3.Visible = 1;
         chkavDynamicfiltersenabled2.Visible = 1;
         Ddo_incidentes_demandanum_Searchbuttontext = "Pesquisar";
         Ddo_incidentes_demandanum_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_incidentes_demandanum_Cleanfilter = "Limpar pesquisa";
         Ddo_incidentes_demandanum_Loadingdata = "Carregando dados...";
         Ddo_incidentes_demandanum_Sortdsc = "Ordenar de Z � A";
         Ddo_incidentes_demandanum_Sortasc = "Ordenar de A � Z";
         Ddo_incidentes_demandanum_Datalistupdateminimumcharacters = 0;
         Ddo_incidentes_demandanum_Datalistproc = "GetPromptContagemResultadoIncidentesFilterData";
         Ddo_incidentes_demandanum_Datalisttype = "Dynamic";
         Ddo_incidentes_demandanum_Includedatalist = Convert.ToBoolean( -1);
         Ddo_incidentes_demandanum_Filterisrange = Convert.ToBoolean( 0);
         Ddo_incidentes_demandanum_Filtertype = "Character";
         Ddo_incidentes_demandanum_Includefilter = Convert.ToBoolean( -1);
         Ddo_incidentes_demandanum_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_incidentes_demandanum_Includesortasc = Convert.ToBoolean( -1);
         Ddo_incidentes_demandanum_Titlecontrolidtoreplace = "";
         Ddo_incidentes_demandanum_Dropdownoptionstype = "GridTitleSettings";
         Ddo_incidentes_demandanum_Cls = "ColumnSettings";
         Ddo_incidentes_demandanum_Tooltip = "Op��es";
         Ddo_incidentes_demandanum_Caption = "";
         Ddo_incidentes_demandacod_Searchbuttontext = "Pesquisar";
         Ddo_incidentes_demandacod_Rangefilterto = "At�";
         Ddo_incidentes_demandacod_Rangefilterfrom = "Desde";
         Ddo_incidentes_demandacod_Cleanfilter = "Limpar pesquisa";
         Ddo_incidentes_demandacod_Sortdsc = "Ordenar de Z � A";
         Ddo_incidentes_demandacod_Sortasc = "Ordenar de A � Z";
         Ddo_incidentes_demandacod_Includedatalist = Convert.ToBoolean( 0);
         Ddo_incidentes_demandacod_Filterisrange = Convert.ToBoolean( -1);
         Ddo_incidentes_demandacod_Filtertype = "Numeric";
         Ddo_incidentes_demandacod_Includefilter = Convert.ToBoolean( -1);
         Ddo_incidentes_demandacod_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_incidentes_demandacod_Includesortasc = Convert.ToBoolean( -1);
         Ddo_incidentes_demandacod_Titlecontrolidtoreplace = "";
         Ddo_incidentes_demandacod_Dropdownoptionstype = "GridTitleSettings";
         Ddo_incidentes_demandacod_Cls = "ColumnSettings";
         Ddo_incidentes_demandacod_Tooltip = "Op��es";
         Ddo_incidentes_demandacod_Caption = "";
         Ddo_incidentes_descricao_Searchbuttontext = "Pesquisar";
         Ddo_incidentes_descricao_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_incidentes_descricao_Cleanfilter = "Limpar pesquisa";
         Ddo_incidentes_descricao_Loadingdata = "Carregando dados...";
         Ddo_incidentes_descricao_Sortdsc = "Ordenar de Z � A";
         Ddo_incidentes_descricao_Sortasc = "Ordenar de A � Z";
         Ddo_incidentes_descricao_Datalistupdateminimumcharacters = 0;
         Ddo_incidentes_descricao_Datalistproc = "GetPromptContagemResultadoIncidentesFilterData";
         Ddo_incidentes_descricao_Datalisttype = "Dynamic";
         Ddo_incidentes_descricao_Includedatalist = Convert.ToBoolean( -1);
         Ddo_incidentes_descricao_Filterisrange = Convert.ToBoolean( 0);
         Ddo_incidentes_descricao_Filtertype = "Character";
         Ddo_incidentes_descricao_Includefilter = Convert.ToBoolean( -1);
         Ddo_incidentes_descricao_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_incidentes_descricao_Includesortasc = Convert.ToBoolean( -1);
         Ddo_incidentes_descricao_Titlecontrolidtoreplace = "";
         Ddo_incidentes_descricao_Dropdownoptionstype = "GridTitleSettings";
         Ddo_incidentes_descricao_Cls = "ColumnSettings";
         Ddo_incidentes_descricao_Tooltip = "Op��es";
         Ddo_incidentes_descricao_Caption = "";
         Ddo_incidentes_data_Searchbuttontext = "Pesquisar";
         Ddo_incidentes_data_Rangefilterto = "At�";
         Ddo_incidentes_data_Rangefilterfrom = "Desde";
         Ddo_incidentes_data_Cleanfilter = "Limpar pesquisa";
         Ddo_incidentes_data_Sortdsc = "Ordenar de Z � A";
         Ddo_incidentes_data_Sortasc = "Ordenar de A � Z";
         Ddo_incidentes_data_Includedatalist = Convert.ToBoolean( 0);
         Ddo_incidentes_data_Filterisrange = Convert.ToBoolean( -1);
         Ddo_incidentes_data_Filtertype = "Date";
         Ddo_incidentes_data_Includefilter = Convert.ToBoolean( -1);
         Ddo_incidentes_data_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_incidentes_data_Includesortasc = Convert.ToBoolean( -1);
         Ddo_incidentes_data_Titlecontrolidtoreplace = "";
         Ddo_incidentes_data_Dropdownoptionstype = "GridTitleSettings";
         Ddo_incidentes_data_Cls = "ColumnSettings";
         Ddo_incidentes_data_Tooltip = "Op��es";
         Ddo_incidentes_data_Caption = "";
         Ddo_incidentes_codigo_Searchbuttontext = "Pesquisar";
         Ddo_incidentes_codigo_Rangefilterto = "At�";
         Ddo_incidentes_codigo_Rangefilterfrom = "Desde";
         Ddo_incidentes_codigo_Cleanfilter = "Limpar pesquisa";
         Ddo_incidentes_codigo_Sortdsc = "Ordenar de Z � A";
         Ddo_incidentes_codigo_Sortasc = "Ordenar de A � Z";
         Ddo_incidentes_codigo_Includedatalist = Convert.ToBoolean( 0);
         Ddo_incidentes_codigo_Filterisrange = Convert.ToBoolean( -1);
         Ddo_incidentes_codigo_Filtertype = "Numeric";
         Ddo_incidentes_codigo_Includefilter = Convert.ToBoolean( -1);
         Ddo_incidentes_codigo_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_incidentes_codigo_Includesortasc = Convert.ToBoolean( -1);
         Ddo_incidentes_codigo_Titlecontrolidtoreplace = "";
         Ddo_incidentes_codigo_Dropdownoptionstype = "GridTitleSettings";
         Ddo_incidentes_codigo_Cls = "ColumnSettings";
         Ddo_incidentes_codigo_Tooltip = "Op��es";
         Ddo_incidentes_codigo_Caption = "";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         Form.Caption = "Selecione Contagem Resultado Incidentes";
         subGrid_Rows = 0;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV33ddo_Incidentes_CodigoTitleControlIdToReplace',fld:'vDDO_INCIDENTES_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV39ddo_Incidentes_DataTitleControlIdToReplace',fld:'vDDO_INCIDENTES_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV43ddo_Incidentes_DescricaoTitleControlIdToReplace',fld:'vDDO_INCIDENTES_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_Incidentes_DemandaCodTitleControlIdToReplace',fld:'vDDO_INCIDENTES_DEMANDACODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV51ddo_Incidentes_DemandaNumTitleControlIdToReplace',fld:'vDDO_INCIDENTES_DEMANDANUMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV31TFIncidentes_Codigo',fld:'vTFINCIDENTES_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32TFIncidentes_Codigo_To',fld:'vTFINCIDENTES_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV35TFIncidentes_Data',fld:'vTFINCIDENTES_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV36TFIncidentes_Data_To',fld:'vTFINCIDENTES_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV41TFIncidentes_Descricao',fld:'vTFINCIDENTES_DESCRICAO',pic:'',nv:''},{av:'AV42TFIncidentes_Descricao_Sel',fld:'vTFINCIDENTES_DESCRICAO_SEL',pic:'',nv:''},{av:'AV45TFIncidentes_DemandaCod',fld:'vTFINCIDENTES_DEMANDACOD',pic:'ZZZZZ9',nv:0},{av:'AV46TFIncidentes_DemandaCod_To',fld:'vTFINCIDENTES_DEMANDACOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV49TFIncidentes_DemandaNum',fld:'vTFINCIDENTES_DEMANDANUM',pic:'@!',nv:''},{av:'AV50TFIncidentes_DemandaNum_Sel',fld:'vTFINCIDENTES_DEMANDANUM_SEL',pic:'@!',nv:''},{av:'AV59Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16Incidentes_Data1',fld:'vINCIDENTES_DATA1',pic:'99/99/99 99:99',nv:''},{av:'AV17Incidentes_Data_To1',fld:'vINCIDENTES_DATA_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20Incidentes_Data2',fld:'vINCIDENTES_DATA2',pic:'99/99/99 99:99',nv:''},{av:'AV21Incidentes_Data_To2',fld:'vINCIDENTES_DATA_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24Incidentes_Data3',fld:'vINCIDENTES_DATA3',pic:'99/99/99 99:99',nv:''},{av:'AV25Incidentes_Data_To3',fld:'vINCIDENTES_DATA_TO3',pic:'99/99/99 99:99',nv:''}],oparms:[{av:'AV30Incidentes_CodigoTitleFilterData',fld:'vINCIDENTES_CODIGOTITLEFILTERDATA',pic:'',nv:null},{av:'AV34Incidentes_DataTitleFilterData',fld:'vINCIDENTES_DATATITLEFILTERDATA',pic:'',nv:null},{av:'AV40Incidentes_DescricaoTitleFilterData',fld:'vINCIDENTES_DESCRICAOTITLEFILTERDATA',pic:'',nv:null},{av:'AV44Incidentes_DemandaCodTitleFilterData',fld:'vINCIDENTES_DEMANDACODTITLEFILTERDATA',pic:'',nv:null},{av:'AV48Incidentes_DemandaNumTitleFilterData',fld:'vINCIDENTES_DEMANDANUMTITLEFILTERDATA',pic:'',nv:null},{av:'edtIncidentes_Codigo_Titleformat',ctrl:'INCIDENTES_CODIGO',prop:'Titleformat'},{av:'edtIncidentes_Codigo_Title',ctrl:'INCIDENTES_CODIGO',prop:'Title'},{av:'edtIncidentes_Data_Titleformat',ctrl:'INCIDENTES_DATA',prop:'Titleformat'},{av:'edtIncidentes_Data_Title',ctrl:'INCIDENTES_DATA',prop:'Title'},{av:'edtIncidentes_Descricao_Titleformat',ctrl:'INCIDENTES_DESCRICAO',prop:'Titleformat'},{av:'edtIncidentes_Descricao_Title',ctrl:'INCIDENTES_DESCRICAO',prop:'Title'},{av:'edtIncidentes_DemandaCod_Titleformat',ctrl:'INCIDENTES_DEMANDACOD',prop:'Titleformat'},{av:'edtIncidentes_DemandaCod_Title',ctrl:'INCIDENTES_DEMANDACOD',prop:'Title'},{av:'edtIncidentes_DemandaNum_Titleformat',ctrl:'INCIDENTES_DEMANDANUM',prop:'Titleformat'},{av:'edtIncidentes_DemandaNum_Title',ctrl:'INCIDENTES_DEMANDANUM',prop:'Title'},{av:'AV54GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV55GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E11J52',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16Incidentes_Data1',fld:'vINCIDENTES_DATA1',pic:'99/99/99 99:99',nv:''},{av:'AV17Incidentes_Data_To1',fld:'vINCIDENTES_DATA_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20Incidentes_Data2',fld:'vINCIDENTES_DATA2',pic:'99/99/99 99:99',nv:''},{av:'AV21Incidentes_Data_To2',fld:'vINCIDENTES_DATA_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24Incidentes_Data3',fld:'vINCIDENTES_DATA3',pic:'99/99/99 99:99',nv:''},{av:'AV25Incidentes_Data_To3',fld:'vINCIDENTES_DATA_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31TFIncidentes_Codigo',fld:'vTFINCIDENTES_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32TFIncidentes_Codigo_To',fld:'vTFINCIDENTES_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV35TFIncidentes_Data',fld:'vTFINCIDENTES_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV36TFIncidentes_Data_To',fld:'vTFINCIDENTES_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV41TFIncidentes_Descricao',fld:'vTFINCIDENTES_DESCRICAO',pic:'',nv:''},{av:'AV42TFIncidentes_Descricao_Sel',fld:'vTFINCIDENTES_DESCRICAO_SEL',pic:'',nv:''},{av:'AV45TFIncidentes_DemandaCod',fld:'vTFINCIDENTES_DEMANDACOD',pic:'ZZZZZ9',nv:0},{av:'AV46TFIncidentes_DemandaCod_To',fld:'vTFINCIDENTES_DEMANDACOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV49TFIncidentes_DemandaNum',fld:'vTFINCIDENTES_DEMANDANUM',pic:'@!',nv:''},{av:'AV50TFIncidentes_DemandaNum_Sel',fld:'vTFINCIDENTES_DEMANDANUM_SEL',pic:'@!',nv:''},{av:'AV33ddo_Incidentes_CodigoTitleControlIdToReplace',fld:'vDDO_INCIDENTES_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV39ddo_Incidentes_DataTitleControlIdToReplace',fld:'vDDO_INCIDENTES_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV43ddo_Incidentes_DescricaoTitleControlIdToReplace',fld:'vDDO_INCIDENTES_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_Incidentes_DemandaCodTitleControlIdToReplace',fld:'vDDO_INCIDENTES_DEMANDACODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV51ddo_Incidentes_DemandaNumTitleControlIdToReplace',fld:'vDDO_INCIDENTES_DEMANDANUMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV59Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("DDO_INCIDENTES_CODIGO.ONOPTIONCLICKED","{handler:'E12J52',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16Incidentes_Data1',fld:'vINCIDENTES_DATA1',pic:'99/99/99 99:99',nv:''},{av:'AV17Incidentes_Data_To1',fld:'vINCIDENTES_DATA_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20Incidentes_Data2',fld:'vINCIDENTES_DATA2',pic:'99/99/99 99:99',nv:''},{av:'AV21Incidentes_Data_To2',fld:'vINCIDENTES_DATA_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24Incidentes_Data3',fld:'vINCIDENTES_DATA3',pic:'99/99/99 99:99',nv:''},{av:'AV25Incidentes_Data_To3',fld:'vINCIDENTES_DATA_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31TFIncidentes_Codigo',fld:'vTFINCIDENTES_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32TFIncidentes_Codigo_To',fld:'vTFINCIDENTES_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV35TFIncidentes_Data',fld:'vTFINCIDENTES_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV36TFIncidentes_Data_To',fld:'vTFINCIDENTES_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV41TFIncidentes_Descricao',fld:'vTFINCIDENTES_DESCRICAO',pic:'',nv:''},{av:'AV42TFIncidentes_Descricao_Sel',fld:'vTFINCIDENTES_DESCRICAO_SEL',pic:'',nv:''},{av:'AV45TFIncidentes_DemandaCod',fld:'vTFINCIDENTES_DEMANDACOD',pic:'ZZZZZ9',nv:0},{av:'AV46TFIncidentes_DemandaCod_To',fld:'vTFINCIDENTES_DEMANDACOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV49TFIncidentes_DemandaNum',fld:'vTFINCIDENTES_DEMANDANUM',pic:'@!',nv:''},{av:'AV50TFIncidentes_DemandaNum_Sel',fld:'vTFINCIDENTES_DEMANDANUM_SEL',pic:'@!',nv:''},{av:'AV33ddo_Incidentes_CodigoTitleControlIdToReplace',fld:'vDDO_INCIDENTES_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV39ddo_Incidentes_DataTitleControlIdToReplace',fld:'vDDO_INCIDENTES_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV43ddo_Incidentes_DescricaoTitleControlIdToReplace',fld:'vDDO_INCIDENTES_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_Incidentes_DemandaCodTitleControlIdToReplace',fld:'vDDO_INCIDENTES_DEMANDACODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV51ddo_Incidentes_DemandaNumTitleControlIdToReplace',fld:'vDDO_INCIDENTES_DEMANDANUMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV59Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_incidentes_codigo_Activeeventkey',ctrl:'DDO_INCIDENTES_CODIGO',prop:'ActiveEventKey'},{av:'Ddo_incidentes_codigo_Filteredtext_get',ctrl:'DDO_INCIDENTES_CODIGO',prop:'FilteredText_get'},{av:'Ddo_incidentes_codigo_Filteredtextto_get',ctrl:'DDO_INCIDENTES_CODIGO',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_incidentes_codigo_Sortedstatus',ctrl:'DDO_INCIDENTES_CODIGO',prop:'SortedStatus'},{av:'AV31TFIncidentes_Codigo',fld:'vTFINCIDENTES_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32TFIncidentes_Codigo_To',fld:'vTFINCIDENTES_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_incidentes_data_Sortedstatus',ctrl:'DDO_INCIDENTES_DATA',prop:'SortedStatus'},{av:'Ddo_incidentes_descricao_Sortedstatus',ctrl:'DDO_INCIDENTES_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_incidentes_demandacod_Sortedstatus',ctrl:'DDO_INCIDENTES_DEMANDACOD',prop:'SortedStatus'},{av:'Ddo_incidentes_demandanum_Sortedstatus',ctrl:'DDO_INCIDENTES_DEMANDANUM',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_INCIDENTES_DATA.ONOPTIONCLICKED","{handler:'E13J52',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16Incidentes_Data1',fld:'vINCIDENTES_DATA1',pic:'99/99/99 99:99',nv:''},{av:'AV17Incidentes_Data_To1',fld:'vINCIDENTES_DATA_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20Incidentes_Data2',fld:'vINCIDENTES_DATA2',pic:'99/99/99 99:99',nv:''},{av:'AV21Incidentes_Data_To2',fld:'vINCIDENTES_DATA_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24Incidentes_Data3',fld:'vINCIDENTES_DATA3',pic:'99/99/99 99:99',nv:''},{av:'AV25Incidentes_Data_To3',fld:'vINCIDENTES_DATA_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31TFIncidentes_Codigo',fld:'vTFINCIDENTES_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32TFIncidentes_Codigo_To',fld:'vTFINCIDENTES_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV35TFIncidentes_Data',fld:'vTFINCIDENTES_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV36TFIncidentes_Data_To',fld:'vTFINCIDENTES_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV41TFIncidentes_Descricao',fld:'vTFINCIDENTES_DESCRICAO',pic:'',nv:''},{av:'AV42TFIncidentes_Descricao_Sel',fld:'vTFINCIDENTES_DESCRICAO_SEL',pic:'',nv:''},{av:'AV45TFIncidentes_DemandaCod',fld:'vTFINCIDENTES_DEMANDACOD',pic:'ZZZZZ9',nv:0},{av:'AV46TFIncidentes_DemandaCod_To',fld:'vTFINCIDENTES_DEMANDACOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV49TFIncidentes_DemandaNum',fld:'vTFINCIDENTES_DEMANDANUM',pic:'@!',nv:''},{av:'AV50TFIncidentes_DemandaNum_Sel',fld:'vTFINCIDENTES_DEMANDANUM_SEL',pic:'@!',nv:''},{av:'AV33ddo_Incidentes_CodigoTitleControlIdToReplace',fld:'vDDO_INCIDENTES_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV39ddo_Incidentes_DataTitleControlIdToReplace',fld:'vDDO_INCIDENTES_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV43ddo_Incidentes_DescricaoTitleControlIdToReplace',fld:'vDDO_INCIDENTES_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_Incidentes_DemandaCodTitleControlIdToReplace',fld:'vDDO_INCIDENTES_DEMANDACODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV51ddo_Incidentes_DemandaNumTitleControlIdToReplace',fld:'vDDO_INCIDENTES_DEMANDANUMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV59Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_incidentes_data_Activeeventkey',ctrl:'DDO_INCIDENTES_DATA',prop:'ActiveEventKey'},{av:'Ddo_incidentes_data_Filteredtext_get',ctrl:'DDO_INCIDENTES_DATA',prop:'FilteredText_get'},{av:'Ddo_incidentes_data_Filteredtextto_get',ctrl:'DDO_INCIDENTES_DATA',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_incidentes_data_Sortedstatus',ctrl:'DDO_INCIDENTES_DATA',prop:'SortedStatus'},{av:'AV35TFIncidentes_Data',fld:'vTFINCIDENTES_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV36TFIncidentes_Data_To',fld:'vTFINCIDENTES_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'Ddo_incidentes_codigo_Sortedstatus',ctrl:'DDO_INCIDENTES_CODIGO',prop:'SortedStatus'},{av:'Ddo_incidentes_descricao_Sortedstatus',ctrl:'DDO_INCIDENTES_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_incidentes_demandacod_Sortedstatus',ctrl:'DDO_INCIDENTES_DEMANDACOD',prop:'SortedStatus'},{av:'Ddo_incidentes_demandanum_Sortedstatus',ctrl:'DDO_INCIDENTES_DEMANDANUM',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_INCIDENTES_DESCRICAO.ONOPTIONCLICKED","{handler:'E14J52',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16Incidentes_Data1',fld:'vINCIDENTES_DATA1',pic:'99/99/99 99:99',nv:''},{av:'AV17Incidentes_Data_To1',fld:'vINCIDENTES_DATA_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20Incidentes_Data2',fld:'vINCIDENTES_DATA2',pic:'99/99/99 99:99',nv:''},{av:'AV21Incidentes_Data_To2',fld:'vINCIDENTES_DATA_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24Incidentes_Data3',fld:'vINCIDENTES_DATA3',pic:'99/99/99 99:99',nv:''},{av:'AV25Incidentes_Data_To3',fld:'vINCIDENTES_DATA_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31TFIncidentes_Codigo',fld:'vTFINCIDENTES_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32TFIncidentes_Codigo_To',fld:'vTFINCIDENTES_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV35TFIncidentes_Data',fld:'vTFINCIDENTES_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV36TFIncidentes_Data_To',fld:'vTFINCIDENTES_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV41TFIncidentes_Descricao',fld:'vTFINCIDENTES_DESCRICAO',pic:'',nv:''},{av:'AV42TFIncidentes_Descricao_Sel',fld:'vTFINCIDENTES_DESCRICAO_SEL',pic:'',nv:''},{av:'AV45TFIncidentes_DemandaCod',fld:'vTFINCIDENTES_DEMANDACOD',pic:'ZZZZZ9',nv:0},{av:'AV46TFIncidentes_DemandaCod_To',fld:'vTFINCIDENTES_DEMANDACOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV49TFIncidentes_DemandaNum',fld:'vTFINCIDENTES_DEMANDANUM',pic:'@!',nv:''},{av:'AV50TFIncidentes_DemandaNum_Sel',fld:'vTFINCIDENTES_DEMANDANUM_SEL',pic:'@!',nv:''},{av:'AV33ddo_Incidentes_CodigoTitleControlIdToReplace',fld:'vDDO_INCIDENTES_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV39ddo_Incidentes_DataTitleControlIdToReplace',fld:'vDDO_INCIDENTES_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV43ddo_Incidentes_DescricaoTitleControlIdToReplace',fld:'vDDO_INCIDENTES_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_Incidentes_DemandaCodTitleControlIdToReplace',fld:'vDDO_INCIDENTES_DEMANDACODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV51ddo_Incidentes_DemandaNumTitleControlIdToReplace',fld:'vDDO_INCIDENTES_DEMANDANUMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV59Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_incidentes_descricao_Activeeventkey',ctrl:'DDO_INCIDENTES_DESCRICAO',prop:'ActiveEventKey'},{av:'Ddo_incidentes_descricao_Filteredtext_get',ctrl:'DDO_INCIDENTES_DESCRICAO',prop:'FilteredText_get'},{av:'Ddo_incidentes_descricao_Selectedvalue_get',ctrl:'DDO_INCIDENTES_DESCRICAO',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_incidentes_descricao_Sortedstatus',ctrl:'DDO_INCIDENTES_DESCRICAO',prop:'SortedStatus'},{av:'AV41TFIncidentes_Descricao',fld:'vTFINCIDENTES_DESCRICAO',pic:'',nv:''},{av:'AV42TFIncidentes_Descricao_Sel',fld:'vTFINCIDENTES_DESCRICAO_SEL',pic:'',nv:''},{av:'Ddo_incidentes_codigo_Sortedstatus',ctrl:'DDO_INCIDENTES_CODIGO',prop:'SortedStatus'},{av:'Ddo_incidentes_data_Sortedstatus',ctrl:'DDO_INCIDENTES_DATA',prop:'SortedStatus'},{av:'Ddo_incidentes_demandacod_Sortedstatus',ctrl:'DDO_INCIDENTES_DEMANDACOD',prop:'SortedStatus'},{av:'Ddo_incidentes_demandanum_Sortedstatus',ctrl:'DDO_INCIDENTES_DEMANDANUM',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_INCIDENTES_DEMANDACOD.ONOPTIONCLICKED","{handler:'E15J52',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16Incidentes_Data1',fld:'vINCIDENTES_DATA1',pic:'99/99/99 99:99',nv:''},{av:'AV17Incidentes_Data_To1',fld:'vINCIDENTES_DATA_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20Incidentes_Data2',fld:'vINCIDENTES_DATA2',pic:'99/99/99 99:99',nv:''},{av:'AV21Incidentes_Data_To2',fld:'vINCIDENTES_DATA_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24Incidentes_Data3',fld:'vINCIDENTES_DATA3',pic:'99/99/99 99:99',nv:''},{av:'AV25Incidentes_Data_To3',fld:'vINCIDENTES_DATA_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31TFIncidentes_Codigo',fld:'vTFINCIDENTES_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32TFIncidentes_Codigo_To',fld:'vTFINCIDENTES_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV35TFIncidentes_Data',fld:'vTFINCIDENTES_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV36TFIncidentes_Data_To',fld:'vTFINCIDENTES_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV41TFIncidentes_Descricao',fld:'vTFINCIDENTES_DESCRICAO',pic:'',nv:''},{av:'AV42TFIncidentes_Descricao_Sel',fld:'vTFINCIDENTES_DESCRICAO_SEL',pic:'',nv:''},{av:'AV45TFIncidentes_DemandaCod',fld:'vTFINCIDENTES_DEMANDACOD',pic:'ZZZZZ9',nv:0},{av:'AV46TFIncidentes_DemandaCod_To',fld:'vTFINCIDENTES_DEMANDACOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV49TFIncidentes_DemandaNum',fld:'vTFINCIDENTES_DEMANDANUM',pic:'@!',nv:''},{av:'AV50TFIncidentes_DemandaNum_Sel',fld:'vTFINCIDENTES_DEMANDANUM_SEL',pic:'@!',nv:''},{av:'AV33ddo_Incidentes_CodigoTitleControlIdToReplace',fld:'vDDO_INCIDENTES_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV39ddo_Incidentes_DataTitleControlIdToReplace',fld:'vDDO_INCIDENTES_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV43ddo_Incidentes_DescricaoTitleControlIdToReplace',fld:'vDDO_INCIDENTES_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_Incidentes_DemandaCodTitleControlIdToReplace',fld:'vDDO_INCIDENTES_DEMANDACODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV51ddo_Incidentes_DemandaNumTitleControlIdToReplace',fld:'vDDO_INCIDENTES_DEMANDANUMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV59Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_incidentes_demandacod_Activeeventkey',ctrl:'DDO_INCIDENTES_DEMANDACOD',prop:'ActiveEventKey'},{av:'Ddo_incidentes_demandacod_Filteredtext_get',ctrl:'DDO_INCIDENTES_DEMANDACOD',prop:'FilteredText_get'},{av:'Ddo_incidentes_demandacod_Filteredtextto_get',ctrl:'DDO_INCIDENTES_DEMANDACOD',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_incidentes_demandacod_Sortedstatus',ctrl:'DDO_INCIDENTES_DEMANDACOD',prop:'SortedStatus'},{av:'AV45TFIncidentes_DemandaCod',fld:'vTFINCIDENTES_DEMANDACOD',pic:'ZZZZZ9',nv:0},{av:'AV46TFIncidentes_DemandaCod_To',fld:'vTFINCIDENTES_DEMANDACOD_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_incidentes_codigo_Sortedstatus',ctrl:'DDO_INCIDENTES_CODIGO',prop:'SortedStatus'},{av:'Ddo_incidentes_data_Sortedstatus',ctrl:'DDO_INCIDENTES_DATA',prop:'SortedStatus'},{av:'Ddo_incidentes_descricao_Sortedstatus',ctrl:'DDO_INCIDENTES_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_incidentes_demandanum_Sortedstatus',ctrl:'DDO_INCIDENTES_DEMANDANUM',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_INCIDENTES_DEMANDANUM.ONOPTIONCLICKED","{handler:'E16J52',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16Incidentes_Data1',fld:'vINCIDENTES_DATA1',pic:'99/99/99 99:99',nv:''},{av:'AV17Incidentes_Data_To1',fld:'vINCIDENTES_DATA_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20Incidentes_Data2',fld:'vINCIDENTES_DATA2',pic:'99/99/99 99:99',nv:''},{av:'AV21Incidentes_Data_To2',fld:'vINCIDENTES_DATA_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24Incidentes_Data3',fld:'vINCIDENTES_DATA3',pic:'99/99/99 99:99',nv:''},{av:'AV25Incidentes_Data_To3',fld:'vINCIDENTES_DATA_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31TFIncidentes_Codigo',fld:'vTFINCIDENTES_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32TFIncidentes_Codigo_To',fld:'vTFINCIDENTES_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV35TFIncidentes_Data',fld:'vTFINCIDENTES_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV36TFIncidentes_Data_To',fld:'vTFINCIDENTES_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV41TFIncidentes_Descricao',fld:'vTFINCIDENTES_DESCRICAO',pic:'',nv:''},{av:'AV42TFIncidentes_Descricao_Sel',fld:'vTFINCIDENTES_DESCRICAO_SEL',pic:'',nv:''},{av:'AV45TFIncidentes_DemandaCod',fld:'vTFINCIDENTES_DEMANDACOD',pic:'ZZZZZ9',nv:0},{av:'AV46TFIncidentes_DemandaCod_To',fld:'vTFINCIDENTES_DEMANDACOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV49TFIncidentes_DemandaNum',fld:'vTFINCIDENTES_DEMANDANUM',pic:'@!',nv:''},{av:'AV50TFIncidentes_DemandaNum_Sel',fld:'vTFINCIDENTES_DEMANDANUM_SEL',pic:'@!',nv:''},{av:'AV33ddo_Incidentes_CodigoTitleControlIdToReplace',fld:'vDDO_INCIDENTES_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV39ddo_Incidentes_DataTitleControlIdToReplace',fld:'vDDO_INCIDENTES_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV43ddo_Incidentes_DescricaoTitleControlIdToReplace',fld:'vDDO_INCIDENTES_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_Incidentes_DemandaCodTitleControlIdToReplace',fld:'vDDO_INCIDENTES_DEMANDACODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV51ddo_Incidentes_DemandaNumTitleControlIdToReplace',fld:'vDDO_INCIDENTES_DEMANDANUMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV59Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_incidentes_demandanum_Activeeventkey',ctrl:'DDO_INCIDENTES_DEMANDANUM',prop:'ActiveEventKey'},{av:'Ddo_incidentes_demandanum_Filteredtext_get',ctrl:'DDO_INCIDENTES_DEMANDANUM',prop:'FilteredText_get'},{av:'Ddo_incidentes_demandanum_Selectedvalue_get',ctrl:'DDO_INCIDENTES_DEMANDANUM',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_incidentes_demandanum_Sortedstatus',ctrl:'DDO_INCIDENTES_DEMANDANUM',prop:'SortedStatus'},{av:'AV49TFIncidentes_DemandaNum',fld:'vTFINCIDENTES_DEMANDANUM',pic:'@!',nv:''},{av:'AV50TFIncidentes_DemandaNum_Sel',fld:'vTFINCIDENTES_DEMANDANUM_SEL',pic:'@!',nv:''},{av:'Ddo_incidentes_codigo_Sortedstatus',ctrl:'DDO_INCIDENTES_CODIGO',prop:'SortedStatus'},{av:'Ddo_incidentes_data_Sortedstatus',ctrl:'DDO_INCIDENTES_DATA',prop:'SortedStatus'},{av:'Ddo_incidentes_descricao_Sortedstatus',ctrl:'DDO_INCIDENTES_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_incidentes_demandacod_Sortedstatus',ctrl:'DDO_INCIDENTES_DEMANDACOD',prop:'SortedStatus'}]}");
         setEventMetadata("GRID.LOAD","{handler:'E29J52',iparms:[],oparms:[{av:'AV28Select',fld:'vSELECT',pic:'',nv:''},{av:'edtavSelect_Tooltiptext',ctrl:'vSELECT',prop:'Tooltiptext'}]}");
         setEventMetadata("ENTER","{handler:'E30J52',iparms:[{av:'A1241Incidentes_Codigo',fld:'INCIDENTES_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A1242Incidentes_Data',fld:'INCIDENTES_DATA',pic:'99/99/99 99:99',hsh:true,nv:''}],oparms:[{av:'AV7InOutIncidentes_Codigo',fld:'vINOUTINCIDENTES_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV8InOutIncidentes_Data',fld:'vINOUTINCIDENTES_DATA',pic:'99/99/99 99:99',nv:''}]}");
         setEventMetadata("VORDEREDBY.CLICK","{handler:'E17J52',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16Incidentes_Data1',fld:'vINCIDENTES_DATA1',pic:'99/99/99 99:99',nv:''},{av:'AV17Incidentes_Data_To1',fld:'vINCIDENTES_DATA_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20Incidentes_Data2',fld:'vINCIDENTES_DATA2',pic:'99/99/99 99:99',nv:''},{av:'AV21Incidentes_Data_To2',fld:'vINCIDENTES_DATA_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24Incidentes_Data3',fld:'vINCIDENTES_DATA3',pic:'99/99/99 99:99',nv:''},{av:'AV25Incidentes_Data_To3',fld:'vINCIDENTES_DATA_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31TFIncidentes_Codigo',fld:'vTFINCIDENTES_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32TFIncidentes_Codigo_To',fld:'vTFINCIDENTES_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV35TFIncidentes_Data',fld:'vTFINCIDENTES_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV36TFIncidentes_Data_To',fld:'vTFINCIDENTES_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV41TFIncidentes_Descricao',fld:'vTFINCIDENTES_DESCRICAO',pic:'',nv:''},{av:'AV42TFIncidentes_Descricao_Sel',fld:'vTFINCIDENTES_DESCRICAO_SEL',pic:'',nv:''},{av:'AV45TFIncidentes_DemandaCod',fld:'vTFINCIDENTES_DEMANDACOD',pic:'ZZZZZ9',nv:0},{av:'AV46TFIncidentes_DemandaCod_To',fld:'vTFINCIDENTES_DEMANDACOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV49TFIncidentes_DemandaNum',fld:'vTFINCIDENTES_DEMANDANUM',pic:'@!',nv:''},{av:'AV50TFIncidentes_DemandaNum_Sel',fld:'vTFINCIDENTES_DEMANDANUM_SEL',pic:'@!',nv:''},{av:'AV33ddo_Incidentes_CodigoTitleControlIdToReplace',fld:'vDDO_INCIDENTES_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV39ddo_Incidentes_DataTitleControlIdToReplace',fld:'vDDO_INCIDENTES_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV43ddo_Incidentes_DescricaoTitleControlIdToReplace',fld:'vDDO_INCIDENTES_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_Incidentes_DemandaCodTitleControlIdToReplace',fld:'vDDO_INCIDENTES_DEMANDACODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV51ddo_Incidentes_DemandaNumTitleControlIdToReplace',fld:'vDDO_INCIDENTES_DEMANDANUMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV59Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[]}");
         setEventMetadata("'ADDDYNAMICFILTERS1'","{handler:'E22J52',iparms:[],oparms:[{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS1'","{handler:'E18J52',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16Incidentes_Data1',fld:'vINCIDENTES_DATA1',pic:'99/99/99 99:99',nv:''},{av:'AV17Incidentes_Data_To1',fld:'vINCIDENTES_DATA_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20Incidentes_Data2',fld:'vINCIDENTES_DATA2',pic:'99/99/99 99:99',nv:''},{av:'AV21Incidentes_Data_To2',fld:'vINCIDENTES_DATA_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24Incidentes_Data3',fld:'vINCIDENTES_DATA3',pic:'99/99/99 99:99',nv:''},{av:'AV25Incidentes_Data_To3',fld:'vINCIDENTES_DATA_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31TFIncidentes_Codigo',fld:'vTFINCIDENTES_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32TFIncidentes_Codigo_To',fld:'vTFINCIDENTES_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV35TFIncidentes_Data',fld:'vTFINCIDENTES_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV36TFIncidentes_Data_To',fld:'vTFINCIDENTES_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV41TFIncidentes_Descricao',fld:'vTFINCIDENTES_DESCRICAO',pic:'',nv:''},{av:'AV42TFIncidentes_Descricao_Sel',fld:'vTFINCIDENTES_DESCRICAO_SEL',pic:'',nv:''},{av:'AV45TFIncidentes_DemandaCod',fld:'vTFINCIDENTES_DEMANDACOD',pic:'ZZZZZ9',nv:0},{av:'AV46TFIncidentes_DemandaCod_To',fld:'vTFINCIDENTES_DEMANDACOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV49TFIncidentes_DemandaNum',fld:'vTFINCIDENTES_DEMANDANUM',pic:'@!',nv:''},{av:'AV50TFIncidentes_DemandaNum_Sel',fld:'vTFINCIDENTES_DEMANDANUM_SEL',pic:'@!',nv:''},{av:'AV33ddo_Incidentes_CodigoTitleControlIdToReplace',fld:'vDDO_INCIDENTES_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV39ddo_Incidentes_DataTitleControlIdToReplace',fld:'vDDO_INCIDENTES_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV43ddo_Incidentes_DescricaoTitleControlIdToReplace',fld:'vDDO_INCIDENTES_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_Incidentes_DemandaCodTitleControlIdToReplace',fld:'vDDO_INCIDENTES_DEMANDACODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV51ddo_Incidentes_DemandaNumTitleControlIdToReplace',fld:'vDDO_INCIDENTES_DEMANDANUMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV59Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20Incidentes_Data2',fld:'vINCIDENTES_DATA2',pic:'99/99/99 99:99',nv:''},{av:'AV21Incidentes_Data_To2',fld:'vINCIDENTES_DATA_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24Incidentes_Data3',fld:'vINCIDENTES_DATA3',pic:'99/99/99 99:99',nv:''},{av:'AV25Incidentes_Data_To3',fld:'vINCIDENTES_DATA_TO3',pic:'99/99/99 99:99',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16Incidentes_Data1',fld:'vINCIDENTES_DATA1',pic:'99/99/99 99:99',nv:''},{av:'AV17Incidentes_Data_To1',fld:'vINCIDENTES_DATA_TO1',pic:'99/99/99 99:99',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'tblTablemergeddynamicfiltersincidentes_data2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSINCIDENTES_DATA2',prop:'Visible'},{av:'tblTablemergeddynamicfiltersincidentes_data3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSINCIDENTES_DATA3',prop:'Visible'},{av:'tblTablemergeddynamicfiltersincidentes_data1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSINCIDENTES_DATA1',prop:'Visible'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR1.CLICK","{handler:'E23J52',iparms:[{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''}],oparms:[{av:'tblTablemergeddynamicfiltersincidentes_data1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSINCIDENTES_DATA1',prop:'Visible'}]}");
         setEventMetadata("'ADDDYNAMICFILTERS2'","{handler:'E24J52',iparms:[],oparms:[{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS2'","{handler:'E19J52',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16Incidentes_Data1',fld:'vINCIDENTES_DATA1',pic:'99/99/99 99:99',nv:''},{av:'AV17Incidentes_Data_To1',fld:'vINCIDENTES_DATA_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20Incidentes_Data2',fld:'vINCIDENTES_DATA2',pic:'99/99/99 99:99',nv:''},{av:'AV21Incidentes_Data_To2',fld:'vINCIDENTES_DATA_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24Incidentes_Data3',fld:'vINCIDENTES_DATA3',pic:'99/99/99 99:99',nv:''},{av:'AV25Incidentes_Data_To3',fld:'vINCIDENTES_DATA_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31TFIncidentes_Codigo',fld:'vTFINCIDENTES_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32TFIncidentes_Codigo_To',fld:'vTFINCIDENTES_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV35TFIncidentes_Data',fld:'vTFINCIDENTES_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV36TFIncidentes_Data_To',fld:'vTFINCIDENTES_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV41TFIncidentes_Descricao',fld:'vTFINCIDENTES_DESCRICAO',pic:'',nv:''},{av:'AV42TFIncidentes_Descricao_Sel',fld:'vTFINCIDENTES_DESCRICAO_SEL',pic:'',nv:''},{av:'AV45TFIncidentes_DemandaCod',fld:'vTFINCIDENTES_DEMANDACOD',pic:'ZZZZZ9',nv:0},{av:'AV46TFIncidentes_DemandaCod_To',fld:'vTFINCIDENTES_DEMANDACOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV49TFIncidentes_DemandaNum',fld:'vTFINCIDENTES_DEMANDANUM',pic:'@!',nv:''},{av:'AV50TFIncidentes_DemandaNum_Sel',fld:'vTFINCIDENTES_DEMANDANUM_SEL',pic:'@!',nv:''},{av:'AV33ddo_Incidentes_CodigoTitleControlIdToReplace',fld:'vDDO_INCIDENTES_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV39ddo_Incidentes_DataTitleControlIdToReplace',fld:'vDDO_INCIDENTES_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV43ddo_Incidentes_DescricaoTitleControlIdToReplace',fld:'vDDO_INCIDENTES_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_Incidentes_DemandaCodTitleControlIdToReplace',fld:'vDDO_INCIDENTES_DEMANDACODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV51ddo_Incidentes_DemandaNumTitleControlIdToReplace',fld:'vDDO_INCIDENTES_DEMANDANUMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV59Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20Incidentes_Data2',fld:'vINCIDENTES_DATA2',pic:'99/99/99 99:99',nv:''},{av:'AV21Incidentes_Data_To2',fld:'vINCIDENTES_DATA_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24Incidentes_Data3',fld:'vINCIDENTES_DATA3',pic:'99/99/99 99:99',nv:''},{av:'AV25Incidentes_Data_To3',fld:'vINCIDENTES_DATA_TO3',pic:'99/99/99 99:99',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16Incidentes_Data1',fld:'vINCIDENTES_DATA1',pic:'99/99/99 99:99',nv:''},{av:'AV17Incidentes_Data_To1',fld:'vINCIDENTES_DATA_TO1',pic:'99/99/99 99:99',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'tblTablemergeddynamicfiltersincidentes_data2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSINCIDENTES_DATA2',prop:'Visible'},{av:'tblTablemergeddynamicfiltersincidentes_data3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSINCIDENTES_DATA3',prop:'Visible'},{av:'tblTablemergeddynamicfiltersincidentes_data1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSINCIDENTES_DATA1',prop:'Visible'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR2.CLICK","{handler:'E25J52',iparms:[{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''}],oparms:[{av:'tblTablemergeddynamicfiltersincidentes_data2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSINCIDENTES_DATA2',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS3'","{handler:'E20J52',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16Incidentes_Data1',fld:'vINCIDENTES_DATA1',pic:'99/99/99 99:99',nv:''},{av:'AV17Incidentes_Data_To1',fld:'vINCIDENTES_DATA_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20Incidentes_Data2',fld:'vINCIDENTES_DATA2',pic:'99/99/99 99:99',nv:''},{av:'AV21Incidentes_Data_To2',fld:'vINCIDENTES_DATA_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24Incidentes_Data3',fld:'vINCIDENTES_DATA3',pic:'99/99/99 99:99',nv:''},{av:'AV25Incidentes_Data_To3',fld:'vINCIDENTES_DATA_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31TFIncidentes_Codigo',fld:'vTFINCIDENTES_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32TFIncidentes_Codigo_To',fld:'vTFINCIDENTES_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV35TFIncidentes_Data',fld:'vTFINCIDENTES_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV36TFIncidentes_Data_To',fld:'vTFINCIDENTES_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV41TFIncidentes_Descricao',fld:'vTFINCIDENTES_DESCRICAO',pic:'',nv:''},{av:'AV42TFIncidentes_Descricao_Sel',fld:'vTFINCIDENTES_DESCRICAO_SEL',pic:'',nv:''},{av:'AV45TFIncidentes_DemandaCod',fld:'vTFINCIDENTES_DEMANDACOD',pic:'ZZZZZ9',nv:0},{av:'AV46TFIncidentes_DemandaCod_To',fld:'vTFINCIDENTES_DEMANDACOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV49TFIncidentes_DemandaNum',fld:'vTFINCIDENTES_DEMANDANUM',pic:'@!',nv:''},{av:'AV50TFIncidentes_DemandaNum_Sel',fld:'vTFINCIDENTES_DEMANDANUM_SEL',pic:'@!',nv:''},{av:'AV33ddo_Incidentes_CodigoTitleControlIdToReplace',fld:'vDDO_INCIDENTES_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV39ddo_Incidentes_DataTitleControlIdToReplace',fld:'vDDO_INCIDENTES_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV43ddo_Incidentes_DescricaoTitleControlIdToReplace',fld:'vDDO_INCIDENTES_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_Incidentes_DemandaCodTitleControlIdToReplace',fld:'vDDO_INCIDENTES_DEMANDACODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV51ddo_Incidentes_DemandaNumTitleControlIdToReplace',fld:'vDDO_INCIDENTES_DEMANDANUMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV59Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20Incidentes_Data2',fld:'vINCIDENTES_DATA2',pic:'99/99/99 99:99',nv:''},{av:'AV21Incidentes_Data_To2',fld:'vINCIDENTES_DATA_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24Incidentes_Data3',fld:'vINCIDENTES_DATA3',pic:'99/99/99 99:99',nv:''},{av:'AV25Incidentes_Data_To3',fld:'vINCIDENTES_DATA_TO3',pic:'99/99/99 99:99',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16Incidentes_Data1',fld:'vINCIDENTES_DATA1',pic:'99/99/99 99:99',nv:''},{av:'AV17Incidentes_Data_To1',fld:'vINCIDENTES_DATA_TO1',pic:'99/99/99 99:99',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'tblTablemergeddynamicfiltersincidentes_data2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSINCIDENTES_DATA2',prop:'Visible'},{av:'tblTablemergeddynamicfiltersincidentes_data3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSINCIDENTES_DATA3',prop:'Visible'},{av:'tblTablemergeddynamicfiltersincidentes_data1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSINCIDENTES_DATA1',prop:'Visible'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR3.CLICK","{handler:'E26J52',iparms:[{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''}],oparms:[{av:'tblTablemergeddynamicfiltersincidentes_data3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSINCIDENTES_DATA3',prop:'Visible'}]}");
         setEventMetadata("'DOCLEANFILTERS'","{handler:'E21J52',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16Incidentes_Data1',fld:'vINCIDENTES_DATA1',pic:'99/99/99 99:99',nv:''},{av:'AV17Incidentes_Data_To1',fld:'vINCIDENTES_DATA_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20Incidentes_Data2',fld:'vINCIDENTES_DATA2',pic:'99/99/99 99:99',nv:''},{av:'AV21Incidentes_Data_To2',fld:'vINCIDENTES_DATA_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24Incidentes_Data3',fld:'vINCIDENTES_DATA3',pic:'99/99/99 99:99',nv:''},{av:'AV25Incidentes_Data_To3',fld:'vINCIDENTES_DATA_TO3',pic:'99/99/99 99:99',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV31TFIncidentes_Codigo',fld:'vTFINCIDENTES_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV32TFIncidentes_Codigo_To',fld:'vTFINCIDENTES_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV35TFIncidentes_Data',fld:'vTFINCIDENTES_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV36TFIncidentes_Data_To',fld:'vTFINCIDENTES_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV41TFIncidentes_Descricao',fld:'vTFINCIDENTES_DESCRICAO',pic:'',nv:''},{av:'AV42TFIncidentes_Descricao_Sel',fld:'vTFINCIDENTES_DESCRICAO_SEL',pic:'',nv:''},{av:'AV45TFIncidentes_DemandaCod',fld:'vTFINCIDENTES_DEMANDACOD',pic:'ZZZZZ9',nv:0},{av:'AV46TFIncidentes_DemandaCod_To',fld:'vTFINCIDENTES_DEMANDACOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV49TFIncidentes_DemandaNum',fld:'vTFINCIDENTES_DEMANDANUM',pic:'@!',nv:''},{av:'AV50TFIncidentes_DemandaNum_Sel',fld:'vTFINCIDENTES_DEMANDANUM_SEL',pic:'@!',nv:''},{av:'AV33ddo_Incidentes_CodigoTitleControlIdToReplace',fld:'vDDO_INCIDENTES_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV39ddo_Incidentes_DataTitleControlIdToReplace',fld:'vDDO_INCIDENTES_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV43ddo_Incidentes_DescricaoTitleControlIdToReplace',fld:'vDDO_INCIDENTES_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_Incidentes_DemandaCodTitleControlIdToReplace',fld:'vDDO_INCIDENTES_DEMANDACODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV51ddo_Incidentes_DemandaNumTitleControlIdToReplace',fld:'vDDO_INCIDENTES_DEMANDANUMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV59Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV31TFIncidentes_Codigo',fld:'vTFINCIDENTES_CODIGO',pic:'ZZZZZ9',nv:0},{av:'Ddo_incidentes_codigo_Filteredtext_set',ctrl:'DDO_INCIDENTES_CODIGO',prop:'FilteredText_set'},{av:'AV32TFIncidentes_Codigo_To',fld:'vTFINCIDENTES_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_incidentes_codigo_Filteredtextto_set',ctrl:'DDO_INCIDENTES_CODIGO',prop:'FilteredTextTo_set'},{av:'AV35TFIncidentes_Data',fld:'vTFINCIDENTES_DATA',pic:'99/99/99 99:99',nv:''},{av:'Ddo_incidentes_data_Filteredtext_set',ctrl:'DDO_INCIDENTES_DATA',prop:'FilteredText_set'},{av:'AV36TFIncidentes_Data_To',fld:'vTFINCIDENTES_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'Ddo_incidentes_data_Filteredtextto_set',ctrl:'DDO_INCIDENTES_DATA',prop:'FilteredTextTo_set'},{av:'AV41TFIncidentes_Descricao',fld:'vTFINCIDENTES_DESCRICAO',pic:'',nv:''},{av:'Ddo_incidentes_descricao_Filteredtext_set',ctrl:'DDO_INCIDENTES_DESCRICAO',prop:'FilteredText_set'},{av:'AV42TFIncidentes_Descricao_Sel',fld:'vTFINCIDENTES_DESCRICAO_SEL',pic:'',nv:''},{av:'Ddo_incidentes_descricao_Selectedvalue_set',ctrl:'DDO_INCIDENTES_DESCRICAO',prop:'SelectedValue_set'},{av:'AV45TFIncidentes_DemandaCod',fld:'vTFINCIDENTES_DEMANDACOD',pic:'ZZZZZ9',nv:0},{av:'Ddo_incidentes_demandacod_Filteredtext_set',ctrl:'DDO_INCIDENTES_DEMANDACOD',prop:'FilteredText_set'},{av:'AV46TFIncidentes_DemandaCod_To',fld:'vTFINCIDENTES_DEMANDACOD_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_incidentes_demandacod_Filteredtextto_set',ctrl:'DDO_INCIDENTES_DEMANDACOD',prop:'FilteredTextTo_set'},{av:'AV49TFIncidentes_DemandaNum',fld:'vTFINCIDENTES_DEMANDANUM',pic:'@!',nv:''},{av:'Ddo_incidentes_demandanum_Filteredtext_set',ctrl:'DDO_INCIDENTES_DEMANDANUM',prop:'FilteredText_set'},{av:'AV50TFIncidentes_DemandaNum_Sel',fld:'vTFINCIDENTES_DEMANDANUM_SEL',pic:'@!',nv:''},{av:'Ddo_incidentes_demandanum_Selectedvalue_set',ctrl:'DDO_INCIDENTES_DEMANDANUM',prop:'SelectedValue_set'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV16Incidentes_Data1',fld:'vINCIDENTES_DATA1',pic:'99/99/99 99:99',nv:''},{av:'AV17Incidentes_Data_To1',fld:'vINCIDENTES_DATA_TO1',pic:'99/99/99 99:99',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'tblTablemergeddynamicfiltersincidentes_data1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSINCIDENTES_DATA1',prop:'Visible'},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV20Incidentes_Data2',fld:'vINCIDENTES_DATA2',pic:'99/99/99 99:99',nv:''},{av:'AV21Incidentes_Data_To2',fld:'vINCIDENTES_DATA_TO2',pic:'99/99/99 99:99',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV24Incidentes_Data3',fld:'vINCIDENTES_DATA3',pic:'99/99/99 99:99',nv:''},{av:'AV25Incidentes_Data_To3',fld:'vINCIDENTES_DATA_TO3',pic:'99/99/99 99:99',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'tblTablemergeddynamicfiltersincidentes_data2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSINCIDENTES_DATA2',prop:'Visible'},{av:'tblTablemergeddynamicfiltersincidentes_data3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSINCIDENTES_DATA3',prop:'Visible'}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         wcpOAV8InOutIncidentes_Data = (DateTime)(DateTime.MinValue);
         Gridpaginationbar_Selectedpage = "";
         Ddo_incidentes_codigo_Activeeventkey = "";
         Ddo_incidentes_codigo_Filteredtext_get = "";
         Ddo_incidentes_codigo_Filteredtextto_get = "";
         Ddo_incidentes_data_Activeeventkey = "";
         Ddo_incidentes_data_Filteredtext_get = "";
         Ddo_incidentes_data_Filteredtextto_get = "";
         Ddo_incidentes_descricao_Activeeventkey = "";
         Ddo_incidentes_descricao_Filteredtext_get = "";
         Ddo_incidentes_descricao_Selectedvalue_get = "";
         Ddo_incidentes_demandacod_Activeeventkey = "";
         Ddo_incidentes_demandacod_Filteredtext_get = "";
         Ddo_incidentes_demandacod_Filteredtextto_get = "";
         Ddo_incidentes_demandanum_Activeeventkey = "";
         Ddo_incidentes_demandanum_Filteredtext_get = "";
         Ddo_incidentes_demandanum_Selectedvalue_get = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV15DynamicFiltersSelector1 = "";
         AV16Incidentes_Data1 = (DateTime)(DateTime.MinValue);
         AV17Incidentes_Data_To1 = (DateTime)(DateTime.MinValue);
         AV19DynamicFiltersSelector2 = "";
         AV20Incidentes_Data2 = (DateTime)(DateTime.MinValue);
         AV21Incidentes_Data_To2 = (DateTime)(DateTime.MinValue);
         AV23DynamicFiltersSelector3 = "";
         AV24Incidentes_Data3 = (DateTime)(DateTime.MinValue);
         AV25Incidentes_Data_To3 = (DateTime)(DateTime.MinValue);
         AV35TFIncidentes_Data = (DateTime)(DateTime.MinValue);
         AV36TFIncidentes_Data_To = (DateTime)(DateTime.MinValue);
         AV41TFIncidentes_Descricao = "";
         AV42TFIncidentes_Descricao_Sel = "";
         AV49TFIncidentes_DemandaNum = "";
         AV50TFIncidentes_DemandaNum_Sel = "";
         AV33ddo_Incidentes_CodigoTitleControlIdToReplace = "";
         AV39ddo_Incidentes_DataTitleControlIdToReplace = "";
         AV43ddo_Incidentes_DescricaoTitleControlIdToReplace = "";
         AV47ddo_Incidentes_DemandaCodTitleControlIdToReplace = "";
         AV51ddo_Incidentes_DemandaNumTitleControlIdToReplace = "";
         AV59Pgmname = "";
         AV10GridState = new wwpbaseobjects.SdtWWPGridState(context);
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV52DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV30Incidentes_CodigoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV34Incidentes_DataTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV40Incidentes_DescricaoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV44Incidentes_DemandaCodTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV48Incidentes_DemandaNumTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         Ddo_incidentes_codigo_Filteredtext_set = "";
         Ddo_incidentes_codigo_Filteredtextto_set = "";
         Ddo_incidentes_codigo_Sortedstatus = "";
         Ddo_incidentes_data_Filteredtext_set = "";
         Ddo_incidentes_data_Filteredtextto_set = "";
         Ddo_incidentes_data_Sortedstatus = "";
         Ddo_incidentes_descricao_Filteredtext_set = "";
         Ddo_incidentes_descricao_Selectedvalue_set = "";
         Ddo_incidentes_descricao_Sortedstatus = "";
         Ddo_incidentes_demandacod_Filteredtext_set = "";
         Ddo_incidentes_demandacod_Filteredtextto_set = "";
         Ddo_incidentes_demandacod_Sortedstatus = "";
         Ddo_incidentes_demandanum_Filteredtext_set = "";
         Ddo_incidentes_demandanum_Selectedvalue_set = "";
         Ddo_incidentes_demandanum_Sortedstatus = "";
         GX_FocusControl = "";
         sPrefix = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         AV37DDO_Incidentes_DataAuxDate = DateTime.MinValue;
         AV38DDO_Incidentes_DataAuxDateTo = DateTime.MinValue;
         Form = new GXWebForm();
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV28Select = "";
         AV58Select_GXI = "";
         A1242Incidentes_Data = (DateTime)(DateTime.MinValue);
         A1243Incidentes_Descricao = "";
         A1240Incidentes_DemandaNum = "";
         GridContainer = new GXWebGrid( context);
         scmdbuf = "";
         lV41TFIncidentes_Descricao = "";
         lV49TFIncidentes_DemandaNum = "";
         H00J52_A1240Incidentes_DemandaNum = new String[] {""} ;
         H00J52_n1240Incidentes_DemandaNum = new bool[] {false} ;
         H00J52_A1239Incidentes_DemandaCod = new int[1] ;
         H00J52_A1243Incidentes_Descricao = new String[] {""} ;
         H00J52_A1242Incidentes_Data = new DateTime[] {DateTime.MinValue} ;
         H00J52_A1241Incidentes_Codigo = new int[1] ;
         H00J53_AGRID_nRecordCount = new long[1] ;
         imgAdddynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters1_Jsonclick = "";
         imgAdddynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters3_Jsonclick = "";
         imgCleanfilters_Jsonclick = "";
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         GridRow = new GXWebRow();
         AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblOrderedtext_Jsonclick = "";
         lblJsdynamicfilters_Jsonclick = "";
         lblDynamicfiltersprefix1_Jsonclick = "";
         lblDynamicfiltersmiddle1_Jsonclick = "";
         lblDynamicfiltersprefix2_Jsonclick = "";
         lblDynamicfiltersmiddle2_Jsonclick = "";
         lblDynamicfiltersprefix3_Jsonclick = "";
         lblDynamicfiltersmiddle3_Jsonclick = "";
         lblDynamicfiltersincidentes_data_rangemiddletext3_Jsonclick = "";
         lblDynamicfiltersincidentes_data_rangemiddletext2_Jsonclick = "";
         lblDynamicfiltersincidentes_data_rangemiddletext1_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.promptcontagemresultadoincidentes__default(),
            new Object[][] {
                new Object[] {
               H00J52_A1240Incidentes_DemandaNum, H00J52_n1240Incidentes_DemandaNum, H00J52_A1239Incidentes_DemandaCod, H00J52_A1243Incidentes_Descricao, H00J52_A1242Incidentes_Data, H00J52_A1241Incidentes_Codigo
               }
               , new Object[] {
               H00J53_AGRID_nRecordCount
               }
            }
         );
         AV59Pgmname = "PromptContagemResultadoIncidentes";
         /* GeneXus formulas. */
         AV59Pgmname = "PromptContagemResultadoIncidentes";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_86 ;
      private short nGXsfl_86_idx=1 ;
      private short AV13OrderedBy ;
      private short initialized ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_86_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short edtIncidentes_Codigo_Titleformat ;
      private short edtIncidentes_Data_Titleformat ;
      private short edtIncidentes_Descricao_Titleformat ;
      private short edtIncidentes_DemandaCod_Titleformat ;
      private short edtIncidentes_DemandaNum_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int AV7InOutIncidentes_Codigo ;
      private int wcpOAV7InOutIncidentes_Codigo ;
      private int subGrid_Rows ;
      private int AV31TFIncidentes_Codigo ;
      private int AV32TFIncidentes_Codigo_To ;
      private int AV45TFIncidentes_DemandaCod ;
      private int AV46TFIncidentes_DemandaCod_To ;
      private int Gridpaginationbar_Pagestoshow ;
      private int Ddo_incidentes_descricao_Datalistupdateminimumcharacters ;
      private int Ddo_incidentes_demandanum_Datalistupdateminimumcharacters ;
      private int edtavTfincidentes_codigo_Visible ;
      private int edtavTfincidentes_codigo_to_Visible ;
      private int edtavTfincidentes_data_Visible ;
      private int edtavTfincidentes_data_to_Visible ;
      private int edtavTfincidentes_descricao_Visible ;
      private int edtavTfincidentes_descricao_sel_Visible ;
      private int edtavTfincidentes_demandacod_Visible ;
      private int edtavTfincidentes_demandacod_to_Visible ;
      private int edtavTfincidentes_demandanum_Visible ;
      private int edtavTfincidentes_demandanum_sel_Visible ;
      private int edtavDdo_incidentes_codigotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_incidentes_datatitlecontrolidtoreplace_Visible ;
      private int edtavDdo_incidentes_descricaotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_incidentes_demandacodtitlecontrolidtoreplace_Visible ;
      private int edtavDdo_incidentes_demandanumtitlecontrolidtoreplace_Visible ;
      private int A1241Incidentes_Codigo ;
      private int A1239Incidentes_DemandaCod ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int edtavOrdereddsc_Visible ;
      private int AV53PageToGo ;
      private int imgAdddynamicfilters1_Visible ;
      private int imgRemovedynamicfilters1_Visible ;
      private int imgAdddynamicfilters2_Visible ;
      private int imgRemovedynamicfilters2_Visible ;
      private int tblTablemergeddynamicfiltersincidentes_data1_Visible ;
      private int tblTablemergeddynamicfiltersincidentes_data2_Visible ;
      private int tblTablemergeddynamicfiltersincidentes_data3_Visible ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private int edtavSelect_Enabled ;
      private int edtavSelect_Visible ;
      private long GRID_nFirstRecordOnPage ;
      private long AV54GridCurrentPage ;
      private long AV55GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private String Gridpaginationbar_Selectedpage ;
      private String Ddo_incidentes_codigo_Activeeventkey ;
      private String Ddo_incidentes_codigo_Filteredtext_get ;
      private String Ddo_incidentes_codigo_Filteredtextto_get ;
      private String Ddo_incidentes_data_Activeeventkey ;
      private String Ddo_incidentes_data_Filteredtext_get ;
      private String Ddo_incidentes_data_Filteredtextto_get ;
      private String Ddo_incidentes_descricao_Activeeventkey ;
      private String Ddo_incidentes_descricao_Filteredtext_get ;
      private String Ddo_incidentes_descricao_Selectedvalue_get ;
      private String Ddo_incidentes_demandacod_Activeeventkey ;
      private String Ddo_incidentes_demandacod_Filteredtext_get ;
      private String Ddo_incidentes_demandacod_Filteredtextto_get ;
      private String Ddo_incidentes_demandanum_Activeeventkey ;
      private String Ddo_incidentes_demandanum_Filteredtext_get ;
      private String Ddo_incidentes_demandanum_Selectedvalue_get ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_86_idx="0001" ;
      private String AV59Pgmname ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Ddo_incidentes_codigo_Caption ;
      private String Ddo_incidentes_codigo_Tooltip ;
      private String Ddo_incidentes_codigo_Cls ;
      private String Ddo_incidentes_codigo_Filteredtext_set ;
      private String Ddo_incidentes_codigo_Filteredtextto_set ;
      private String Ddo_incidentes_codigo_Dropdownoptionstype ;
      private String Ddo_incidentes_codigo_Titlecontrolidtoreplace ;
      private String Ddo_incidentes_codigo_Sortedstatus ;
      private String Ddo_incidentes_codigo_Filtertype ;
      private String Ddo_incidentes_codigo_Sortasc ;
      private String Ddo_incidentes_codigo_Sortdsc ;
      private String Ddo_incidentes_codigo_Cleanfilter ;
      private String Ddo_incidentes_codigo_Rangefilterfrom ;
      private String Ddo_incidentes_codigo_Rangefilterto ;
      private String Ddo_incidentes_codigo_Searchbuttontext ;
      private String Ddo_incidentes_data_Caption ;
      private String Ddo_incidentes_data_Tooltip ;
      private String Ddo_incidentes_data_Cls ;
      private String Ddo_incidentes_data_Filteredtext_set ;
      private String Ddo_incidentes_data_Filteredtextto_set ;
      private String Ddo_incidentes_data_Dropdownoptionstype ;
      private String Ddo_incidentes_data_Titlecontrolidtoreplace ;
      private String Ddo_incidentes_data_Sortedstatus ;
      private String Ddo_incidentes_data_Filtertype ;
      private String Ddo_incidentes_data_Sortasc ;
      private String Ddo_incidentes_data_Sortdsc ;
      private String Ddo_incidentes_data_Cleanfilter ;
      private String Ddo_incidentes_data_Rangefilterfrom ;
      private String Ddo_incidentes_data_Rangefilterto ;
      private String Ddo_incidentes_data_Searchbuttontext ;
      private String Ddo_incidentes_descricao_Caption ;
      private String Ddo_incidentes_descricao_Tooltip ;
      private String Ddo_incidentes_descricao_Cls ;
      private String Ddo_incidentes_descricao_Filteredtext_set ;
      private String Ddo_incidentes_descricao_Selectedvalue_set ;
      private String Ddo_incidentes_descricao_Dropdownoptionstype ;
      private String Ddo_incidentes_descricao_Titlecontrolidtoreplace ;
      private String Ddo_incidentes_descricao_Sortedstatus ;
      private String Ddo_incidentes_descricao_Filtertype ;
      private String Ddo_incidentes_descricao_Datalisttype ;
      private String Ddo_incidentes_descricao_Datalistproc ;
      private String Ddo_incidentes_descricao_Sortasc ;
      private String Ddo_incidentes_descricao_Sortdsc ;
      private String Ddo_incidentes_descricao_Loadingdata ;
      private String Ddo_incidentes_descricao_Cleanfilter ;
      private String Ddo_incidentes_descricao_Noresultsfound ;
      private String Ddo_incidentes_descricao_Searchbuttontext ;
      private String Ddo_incidentes_demandacod_Caption ;
      private String Ddo_incidentes_demandacod_Tooltip ;
      private String Ddo_incidentes_demandacod_Cls ;
      private String Ddo_incidentes_demandacod_Filteredtext_set ;
      private String Ddo_incidentes_demandacod_Filteredtextto_set ;
      private String Ddo_incidentes_demandacod_Dropdownoptionstype ;
      private String Ddo_incidentes_demandacod_Titlecontrolidtoreplace ;
      private String Ddo_incidentes_demandacod_Sortedstatus ;
      private String Ddo_incidentes_demandacod_Filtertype ;
      private String Ddo_incidentes_demandacod_Sortasc ;
      private String Ddo_incidentes_demandacod_Sortdsc ;
      private String Ddo_incidentes_demandacod_Cleanfilter ;
      private String Ddo_incidentes_demandacod_Rangefilterfrom ;
      private String Ddo_incidentes_demandacod_Rangefilterto ;
      private String Ddo_incidentes_demandacod_Searchbuttontext ;
      private String Ddo_incidentes_demandanum_Caption ;
      private String Ddo_incidentes_demandanum_Tooltip ;
      private String Ddo_incidentes_demandanum_Cls ;
      private String Ddo_incidentes_demandanum_Filteredtext_set ;
      private String Ddo_incidentes_demandanum_Selectedvalue_set ;
      private String Ddo_incidentes_demandanum_Dropdownoptionstype ;
      private String Ddo_incidentes_demandanum_Titlecontrolidtoreplace ;
      private String Ddo_incidentes_demandanum_Sortedstatus ;
      private String Ddo_incidentes_demandanum_Filtertype ;
      private String Ddo_incidentes_demandanum_Datalisttype ;
      private String Ddo_incidentes_demandanum_Datalistproc ;
      private String Ddo_incidentes_demandanum_Sortasc ;
      private String Ddo_incidentes_demandanum_Sortdsc ;
      private String Ddo_incidentes_demandanum_Loadingdata ;
      private String Ddo_incidentes_demandanum_Cleanfilter ;
      private String Ddo_incidentes_demandanum_Noresultsfound ;
      private String Ddo_incidentes_demandanum_Searchbuttontext ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String chkavDynamicfiltersenabled2_Internalname ;
      private String chkavDynamicfiltersenabled3_Internalname ;
      private String edtavTfincidentes_codigo_Internalname ;
      private String edtavTfincidentes_codigo_Jsonclick ;
      private String edtavTfincidentes_codigo_to_Internalname ;
      private String edtavTfincidentes_codigo_to_Jsonclick ;
      private String edtavTfincidentes_data_Internalname ;
      private String edtavTfincidentes_data_Jsonclick ;
      private String edtavTfincidentes_data_to_Internalname ;
      private String edtavTfincidentes_data_to_Jsonclick ;
      private String divDdo_incidentes_dataauxdates_Internalname ;
      private String edtavDdo_incidentes_dataauxdate_Internalname ;
      private String edtavDdo_incidentes_dataauxdate_Jsonclick ;
      private String edtavDdo_incidentes_dataauxdateto_Internalname ;
      private String edtavDdo_incidentes_dataauxdateto_Jsonclick ;
      private String edtavTfincidentes_descricao_Internalname ;
      private String edtavTfincidentes_descricao_sel_Internalname ;
      private String edtavTfincidentes_demandacod_Internalname ;
      private String edtavTfincidentes_demandacod_Jsonclick ;
      private String edtavTfincidentes_demandacod_to_Internalname ;
      private String edtavTfincidentes_demandacod_to_Jsonclick ;
      private String edtavTfincidentes_demandanum_Internalname ;
      private String edtavTfincidentes_demandanum_Jsonclick ;
      private String edtavTfincidentes_demandanum_sel_Internalname ;
      private String edtavTfincidentes_demandanum_sel_Jsonclick ;
      private String edtavDdo_incidentes_codigotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_incidentes_datatitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_incidentes_descricaotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_incidentes_demandacodtitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_incidentes_demandanumtitlecontrolidtoreplace_Internalname ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavSelect_Internalname ;
      private String edtIncidentes_Codigo_Internalname ;
      private String edtIncidentes_Data_Internalname ;
      private String edtIncidentes_Descricao_Internalname ;
      private String edtIncidentes_DemandaCod_Internalname ;
      private String edtIncidentes_DemandaNum_Internalname ;
      private String cmbavOrderedby_Internalname ;
      private String scmdbuf ;
      private String edtavOrdereddsc_Internalname ;
      private String cmbavDynamicfiltersselector1_Internalname ;
      private String edtavIncidentes_data1_Internalname ;
      private String edtavIncidentes_data_to1_Internalname ;
      private String cmbavDynamicfiltersselector2_Internalname ;
      private String edtavIncidentes_data2_Internalname ;
      private String edtavIncidentes_data_to2_Internalname ;
      private String cmbavDynamicfiltersselector3_Internalname ;
      private String edtavIncidentes_data3_Internalname ;
      private String edtavIncidentes_data_to3_Internalname ;
      private String lblJsdynamicfilters_Caption ;
      private String lblJsdynamicfilters_Internalname ;
      private String imgAdddynamicfilters1_Jsonclick ;
      private String imgAdddynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters1_Jsonclick ;
      private String imgRemovedynamicfilters1_Internalname ;
      private String imgAdddynamicfilters2_Jsonclick ;
      private String imgAdddynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters2_Jsonclick ;
      private String imgRemovedynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters3_Jsonclick ;
      private String imgRemovedynamicfilters3_Internalname ;
      private String subGrid_Internalname ;
      private String Ddo_incidentes_codigo_Internalname ;
      private String Ddo_incidentes_data_Internalname ;
      private String Ddo_incidentes_descricao_Internalname ;
      private String Ddo_incidentes_demandacod_Internalname ;
      private String Ddo_incidentes_demandanum_Internalname ;
      private String imgCleanfilters_Jsonclick ;
      private String imgCleanfilters_Internalname ;
      private String edtIncidentes_Codigo_Title ;
      private String edtIncidentes_Data_Title ;
      private String edtIncidentes_Descricao_Title ;
      private String edtIncidentes_DemandaCod_Title ;
      private String edtIncidentes_DemandaNum_Title ;
      private String edtavSelect_Tooltiptext ;
      private String tblTablemergeddynamicfiltersincidentes_data1_Internalname ;
      private String tblTablemergeddynamicfiltersincidentes_data2_Internalname ;
      private String tblTablemergeddynamicfiltersincidentes_data3_Internalname ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTablegridheader_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTablesearch_Internalname ;
      private String lblOrderedtext_Internalname ;
      private String lblOrderedtext_Jsonclick ;
      private String cmbavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Jsonclick ;
      private String tblTablefilters_Internalname ;
      private String lblJsdynamicfilters_Jsonclick ;
      private String tblTabledynamicfilters_Internalname ;
      private String lblDynamicfiltersprefix1_Internalname ;
      private String lblDynamicfiltersprefix1_Jsonclick ;
      private String cmbavDynamicfiltersselector1_Jsonclick ;
      private String lblDynamicfiltersmiddle1_Internalname ;
      private String lblDynamicfiltersmiddle1_Jsonclick ;
      private String lblDynamicfiltersprefix2_Internalname ;
      private String lblDynamicfiltersprefix2_Jsonclick ;
      private String cmbavDynamicfiltersselector2_Jsonclick ;
      private String lblDynamicfiltersmiddle2_Internalname ;
      private String lblDynamicfiltersmiddle2_Jsonclick ;
      private String lblDynamicfiltersprefix3_Internalname ;
      private String lblDynamicfiltersprefix3_Jsonclick ;
      private String cmbavDynamicfiltersselector3_Jsonclick ;
      private String lblDynamicfiltersmiddle3_Internalname ;
      private String lblDynamicfiltersmiddle3_Jsonclick ;
      private String edtavIncidentes_data3_Jsonclick ;
      private String lblDynamicfiltersincidentes_data_rangemiddletext3_Internalname ;
      private String lblDynamicfiltersincidentes_data_rangemiddletext3_Jsonclick ;
      private String edtavIncidentes_data_to3_Jsonclick ;
      private String edtavIncidentes_data2_Jsonclick ;
      private String lblDynamicfiltersincidentes_data_rangemiddletext2_Internalname ;
      private String lblDynamicfiltersincidentes_data_rangemiddletext2_Jsonclick ;
      private String edtavIncidentes_data_to2_Jsonclick ;
      private String edtavIncidentes_data1_Jsonclick ;
      private String lblDynamicfiltersincidentes_data_rangemiddletext1_Internalname ;
      private String lblDynamicfiltersincidentes_data_rangemiddletext1_Jsonclick ;
      private String edtavIncidentes_data_to1_Jsonclick ;
      private String sGXsfl_86_fel_idx="0001" ;
      private String edtavSelect_Jsonclick ;
      private String ROClassString ;
      private String edtIncidentes_Codigo_Jsonclick ;
      private String edtIncidentes_Data_Jsonclick ;
      private String edtIncidentes_Descricao_Jsonclick ;
      private String edtIncidentes_DemandaCod_Jsonclick ;
      private String edtIncidentes_DemandaNum_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private DateTime AV8InOutIncidentes_Data ;
      private DateTime wcpOAV8InOutIncidentes_Data ;
      private DateTime AV16Incidentes_Data1 ;
      private DateTime AV17Incidentes_Data_To1 ;
      private DateTime AV20Incidentes_Data2 ;
      private DateTime AV21Incidentes_Data_To2 ;
      private DateTime AV24Incidentes_Data3 ;
      private DateTime AV25Incidentes_Data_To3 ;
      private DateTime AV35TFIncidentes_Data ;
      private DateTime AV36TFIncidentes_Data_To ;
      private DateTime A1242Incidentes_Data ;
      private DateTime AV37DDO_Incidentes_DataAuxDate ;
      private DateTime AV38DDO_Incidentes_DataAuxDateTo ;
      private bool entryPointCalled ;
      private bool AV14OrderedDsc ;
      private bool AV18DynamicFiltersEnabled2 ;
      private bool AV22DynamicFiltersEnabled3 ;
      private bool AV27DynamicFiltersIgnoreFirst ;
      private bool AV26DynamicFiltersRemoving ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool Ddo_incidentes_codigo_Includesortasc ;
      private bool Ddo_incidentes_codigo_Includesortdsc ;
      private bool Ddo_incidentes_codigo_Includefilter ;
      private bool Ddo_incidentes_codigo_Filterisrange ;
      private bool Ddo_incidentes_codigo_Includedatalist ;
      private bool Ddo_incidentes_data_Includesortasc ;
      private bool Ddo_incidentes_data_Includesortdsc ;
      private bool Ddo_incidentes_data_Includefilter ;
      private bool Ddo_incidentes_data_Filterisrange ;
      private bool Ddo_incidentes_data_Includedatalist ;
      private bool Ddo_incidentes_descricao_Includesortasc ;
      private bool Ddo_incidentes_descricao_Includesortdsc ;
      private bool Ddo_incidentes_descricao_Includefilter ;
      private bool Ddo_incidentes_descricao_Filterisrange ;
      private bool Ddo_incidentes_descricao_Includedatalist ;
      private bool Ddo_incidentes_demandacod_Includesortasc ;
      private bool Ddo_incidentes_demandacod_Includesortdsc ;
      private bool Ddo_incidentes_demandacod_Includefilter ;
      private bool Ddo_incidentes_demandacod_Filterisrange ;
      private bool Ddo_incidentes_demandacod_Includedatalist ;
      private bool Ddo_incidentes_demandanum_Includesortasc ;
      private bool Ddo_incidentes_demandanum_Includesortdsc ;
      private bool Ddo_incidentes_demandanum_Includefilter ;
      private bool Ddo_incidentes_demandanum_Filterisrange ;
      private bool Ddo_incidentes_demandanum_Includedatalist ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n1240Incidentes_DemandaNum ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV28Select_IsBlob ;
      private String A1243Incidentes_Descricao ;
      private String AV15DynamicFiltersSelector1 ;
      private String AV19DynamicFiltersSelector2 ;
      private String AV23DynamicFiltersSelector3 ;
      private String AV41TFIncidentes_Descricao ;
      private String AV42TFIncidentes_Descricao_Sel ;
      private String AV49TFIncidentes_DemandaNum ;
      private String AV50TFIncidentes_DemandaNum_Sel ;
      private String AV33ddo_Incidentes_CodigoTitleControlIdToReplace ;
      private String AV39ddo_Incidentes_DataTitleControlIdToReplace ;
      private String AV43ddo_Incidentes_DescricaoTitleControlIdToReplace ;
      private String AV47ddo_Incidentes_DemandaCodTitleControlIdToReplace ;
      private String AV51ddo_Incidentes_DemandaNumTitleControlIdToReplace ;
      private String AV58Select_GXI ;
      private String A1240Incidentes_DemandaNum ;
      private String lV41TFIncidentes_Descricao ;
      private String lV49TFIncidentes_DemandaNum ;
      private String AV28Select ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_InOutIncidentes_Codigo ;
      private DateTime aP1_InOutIncidentes_Data ;
      private GXCombobox cmbavOrderedby ;
      private GXCombobox cmbavDynamicfiltersselector1 ;
      private GXCombobox cmbavDynamicfiltersselector2 ;
      private GXCombobox cmbavDynamicfiltersselector3 ;
      private GXCheckbox chkavDynamicfiltersenabled2 ;
      private GXCheckbox chkavDynamicfiltersenabled3 ;
      private IDataStoreProvider pr_default ;
      private String[] H00J52_A1240Incidentes_DemandaNum ;
      private bool[] H00J52_n1240Incidentes_DemandaNum ;
      private int[] H00J52_A1239Incidentes_DemandaCod ;
      private String[] H00J52_A1243Incidentes_Descricao ;
      private DateTime[] H00J52_A1242Incidentes_Data ;
      private int[] H00J52_A1241Incidentes_Codigo ;
      private long[] H00J53_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV30Incidentes_CodigoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV34Incidentes_DataTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV40Incidentes_DescricaoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV44Incidentes_DemandaCodTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV48Incidentes_DemandaNumTitleFilterData ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV10GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV11GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV12GridStateDynamicFilter ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV52DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 ;
   }

   public class promptcontagemresultadoincidentes__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00J52( IGxContext context ,
                                             String AV15DynamicFiltersSelector1 ,
                                             DateTime AV16Incidentes_Data1 ,
                                             DateTime AV17Incidentes_Data_To1 ,
                                             bool AV18DynamicFiltersEnabled2 ,
                                             String AV19DynamicFiltersSelector2 ,
                                             DateTime AV20Incidentes_Data2 ,
                                             DateTime AV21Incidentes_Data_To2 ,
                                             bool AV22DynamicFiltersEnabled3 ,
                                             String AV23DynamicFiltersSelector3 ,
                                             DateTime AV24Incidentes_Data3 ,
                                             DateTime AV25Incidentes_Data_To3 ,
                                             int AV31TFIncidentes_Codigo ,
                                             int AV32TFIncidentes_Codigo_To ,
                                             DateTime AV35TFIncidentes_Data ,
                                             DateTime AV36TFIncidentes_Data_To ,
                                             String AV42TFIncidentes_Descricao_Sel ,
                                             String AV41TFIncidentes_Descricao ,
                                             int AV45TFIncidentes_DemandaCod ,
                                             int AV46TFIncidentes_DemandaCod_To ,
                                             String AV50TFIncidentes_DemandaNum_Sel ,
                                             String AV49TFIncidentes_DemandaNum ,
                                             DateTime A1242Incidentes_Data ,
                                             int A1241Incidentes_Codigo ,
                                             String A1243Incidentes_Descricao ,
                                             int A1239Incidentes_DemandaCod ,
                                             String A1240Incidentes_DemandaNum ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [21] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " T2.[ContagemResultado_Demanda] AS Incidentes_DemandaNum, T1.[Incidentes_DemandaCod] AS Incidentes_DemandaCod, T1.[Incidentes_Descricao], T1.[Incidentes_Data], T1.[Incidentes_Codigo]";
         sFromString = " FROM ([ContagemResultadoIncidentes] T1 WITH (NOLOCK) INNER JOIN [ContagemResultado] T2 WITH (NOLOCK) ON T2.[ContagemResultado_Codigo] = T1.[Incidentes_DemandaCod])";
         sOrderString = "";
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "INCIDENTES_DATA") == 0 ) && ( ! (DateTime.MinValue==AV16Incidentes_Data1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Incidentes_Data] >= @AV16Incidentes_Data1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Incidentes_Data] >= @AV16Incidentes_Data1)";
            }
         }
         else
         {
            GXv_int2[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "INCIDENTES_DATA") == 0 ) && ( ! (DateTime.MinValue==AV17Incidentes_Data_To1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Incidentes_Data] <= @AV17Incidentes_Data_To1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Incidentes_Data] <= @AV17Incidentes_Data_To1)";
            }
         }
         else
         {
            GXv_int2[1] = 1;
         }
         if ( AV18DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "INCIDENTES_DATA") == 0 ) && ( ! (DateTime.MinValue==AV20Incidentes_Data2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Incidentes_Data] >= @AV20Incidentes_Data2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Incidentes_Data] >= @AV20Incidentes_Data2)";
            }
         }
         else
         {
            GXv_int2[2] = 1;
         }
         if ( AV18DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "INCIDENTES_DATA") == 0 ) && ( ! (DateTime.MinValue==AV21Incidentes_Data_To2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Incidentes_Data] <= @AV21Incidentes_Data_To2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Incidentes_Data] <= @AV21Incidentes_Data_To2)";
            }
         }
         else
         {
            GXv_int2[3] = 1;
         }
         if ( AV22DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "INCIDENTES_DATA") == 0 ) && ( ! (DateTime.MinValue==AV24Incidentes_Data3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Incidentes_Data] >= @AV24Incidentes_Data3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Incidentes_Data] >= @AV24Incidentes_Data3)";
            }
         }
         else
         {
            GXv_int2[4] = 1;
         }
         if ( AV22DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "INCIDENTES_DATA") == 0 ) && ( ! (DateTime.MinValue==AV25Incidentes_Data_To3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Incidentes_Data] <= @AV25Incidentes_Data_To3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Incidentes_Data] <= @AV25Incidentes_Data_To3)";
            }
         }
         else
         {
            GXv_int2[5] = 1;
         }
         if ( ! (0==AV31TFIncidentes_Codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Incidentes_Codigo] >= @AV31TFIncidentes_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Incidentes_Codigo] >= @AV31TFIncidentes_Codigo)";
            }
         }
         else
         {
            GXv_int2[6] = 1;
         }
         if ( ! (0==AV32TFIncidentes_Codigo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Incidentes_Codigo] <= @AV32TFIncidentes_Codigo_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Incidentes_Codigo] <= @AV32TFIncidentes_Codigo_To)";
            }
         }
         else
         {
            GXv_int2[7] = 1;
         }
         if ( ! (DateTime.MinValue==AV35TFIncidentes_Data) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Incidentes_Data] >= @AV35TFIncidentes_Data)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Incidentes_Data] >= @AV35TFIncidentes_Data)";
            }
         }
         else
         {
            GXv_int2[8] = 1;
         }
         if ( ! (DateTime.MinValue==AV36TFIncidentes_Data_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Incidentes_Data] <= @AV36TFIncidentes_Data_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Incidentes_Data] <= @AV36TFIncidentes_Data_To)";
            }
         }
         else
         {
            GXv_int2[9] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV42TFIncidentes_Descricao_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV41TFIncidentes_Descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Incidentes_Descricao] like @lV41TFIncidentes_Descricao)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Incidentes_Descricao] like @lV41TFIncidentes_Descricao)";
            }
         }
         else
         {
            GXv_int2[10] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV42TFIncidentes_Descricao_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Incidentes_Descricao] = @AV42TFIncidentes_Descricao_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Incidentes_Descricao] = @AV42TFIncidentes_Descricao_Sel)";
            }
         }
         else
         {
            GXv_int2[11] = 1;
         }
         if ( ! (0==AV45TFIncidentes_DemandaCod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Incidentes_DemandaCod] >= @AV45TFIncidentes_DemandaCod)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Incidentes_DemandaCod] >= @AV45TFIncidentes_DemandaCod)";
            }
         }
         else
         {
            GXv_int2[12] = 1;
         }
         if ( ! (0==AV46TFIncidentes_DemandaCod_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Incidentes_DemandaCod] <= @AV46TFIncidentes_DemandaCod_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Incidentes_DemandaCod] <= @AV46TFIncidentes_DemandaCod_To)";
            }
         }
         else
         {
            GXv_int2[13] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV50TFIncidentes_DemandaNum_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV49TFIncidentes_DemandaNum)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] like @lV49TFIncidentes_DemandaNum)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[ContagemResultado_Demanda] like @lV49TFIncidentes_DemandaNum)";
            }
         }
         else
         {
            GXv_int2[14] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV50TFIncidentes_DemandaNum_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] = @AV50TFIncidentes_DemandaNum_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[ContagemResultado_Demanda] = @AV50TFIncidentes_DemandaNum_Sel)";
            }
         }
         else
         {
            GXv_int2[15] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            sWhereString = " WHERE" + sWhereString;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Incidentes_Data]";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Incidentes_Data] DESC";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Incidentes_Codigo]";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Incidentes_Codigo] DESC";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Incidentes_Descricao]";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Incidentes_Descricao] DESC";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Incidentes_DemandaCod]";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Incidentes_DemandaCod] DESC";
         }
         else if ( ( AV13OrderedBy == 5 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T2.[ContagemResultado_Demanda]";
         }
         else if ( ( AV13OrderedBy == 5 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T2.[ContagemResultado_Demanda] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Incidentes_Codigo]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      protected Object[] conditional_H00J53( IGxContext context ,
                                             String AV15DynamicFiltersSelector1 ,
                                             DateTime AV16Incidentes_Data1 ,
                                             DateTime AV17Incidentes_Data_To1 ,
                                             bool AV18DynamicFiltersEnabled2 ,
                                             String AV19DynamicFiltersSelector2 ,
                                             DateTime AV20Incidentes_Data2 ,
                                             DateTime AV21Incidentes_Data_To2 ,
                                             bool AV22DynamicFiltersEnabled3 ,
                                             String AV23DynamicFiltersSelector3 ,
                                             DateTime AV24Incidentes_Data3 ,
                                             DateTime AV25Incidentes_Data_To3 ,
                                             int AV31TFIncidentes_Codigo ,
                                             int AV32TFIncidentes_Codigo_To ,
                                             DateTime AV35TFIncidentes_Data ,
                                             DateTime AV36TFIncidentes_Data_To ,
                                             String AV42TFIncidentes_Descricao_Sel ,
                                             String AV41TFIncidentes_Descricao ,
                                             int AV45TFIncidentes_DemandaCod ,
                                             int AV46TFIncidentes_DemandaCod_To ,
                                             String AV50TFIncidentes_DemandaNum_Sel ,
                                             String AV49TFIncidentes_DemandaNum ,
                                             DateTime A1242Incidentes_Data ,
                                             int A1241Incidentes_Codigo ,
                                             String A1243Incidentes_Descricao ,
                                             int A1239Incidentes_DemandaCod ,
                                             String A1240Incidentes_DemandaNum ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [16] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM ([ContagemResultadoIncidentes] T1 WITH (NOLOCK) INNER JOIN [ContagemResultado] T2 WITH (NOLOCK) ON T2.[ContagemResultado_Codigo] = T1.[Incidentes_DemandaCod])";
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "INCIDENTES_DATA") == 0 ) && ( ! (DateTime.MinValue==AV16Incidentes_Data1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Incidentes_Data] >= @AV16Incidentes_Data1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Incidentes_Data] >= @AV16Incidentes_Data1)";
            }
         }
         else
         {
            GXv_int4[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "INCIDENTES_DATA") == 0 ) && ( ! (DateTime.MinValue==AV17Incidentes_Data_To1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Incidentes_Data] <= @AV17Incidentes_Data_To1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Incidentes_Data] <= @AV17Incidentes_Data_To1)";
            }
         }
         else
         {
            GXv_int4[1] = 1;
         }
         if ( AV18DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "INCIDENTES_DATA") == 0 ) && ( ! (DateTime.MinValue==AV20Incidentes_Data2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Incidentes_Data] >= @AV20Incidentes_Data2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Incidentes_Data] >= @AV20Incidentes_Data2)";
            }
         }
         else
         {
            GXv_int4[2] = 1;
         }
         if ( AV18DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "INCIDENTES_DATA") == 0 ) && ( ! (DateTime.MinValue==AV21Incidentes_Data_To2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Incidentes_Data] <= @AV21Incidentes_Data_To2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Incidentes_Data] <= @AV21Incidentes_Data_To2)";
            }
         }
         else
         {
            GXv_int4[3] = 1;
         }
         if ( AV22DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "INCIDENTES_DATA") == 0 ) && ( ! (DateTime.MinValue==AV24Incidentes_Data3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Incidentes_Data] >= @AV24Incidentes_Data3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Incidentes_Data] >= @AV24Incidentes_Data3)";
            }
         }
         else
         {
            GXv_int4[4] = 1;
         }
         if ( AV22DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "INCIDENTES_DATA") == 0 ) && ( ! (DateTime.MinValue==AV25Incidentes_Data_To3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Incidentes_Data] <= @AV25Incidentes_Data_To3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Incidentes_Data] <= @AV25Incidentes_Data_To3)";
            }
         }
         else
         {
            GXv_int4[5] = 1;
         }
         if ( ! (0==AV31TFIncidentes_Codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Incidentes_Codigo] >= @AV31TFIncidentes_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Incidentes_Codigo] >= @AV31TFIncidentes_Codigo)";
            }
         }
         else
         {
            GXv_int4[6] = 1;
         }
         if ( ! (0==AV32TFIncidentes_Codigo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Incidentes_Codigo] <= @AV32TFIncidentes_Codigo_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Incidentes_Codigo] <= @AV32TFIncidentes_Codigo_To)";
            }
         }
         else
         {
            GXv_int4[7] = 1;
         }
         if ( ! (DateTime.MinValue==AV35TFIncidentes_Data) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Incidentes_Data] >= @AV35TFIncidentes_Data)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Incidentes_Data] >= @AV35TFIncidentes_Data)";
            }
         }
         else
         {
            GXv_int4[8] = 1;
         }
         if ( ! (DateTime.MinValue==AV36TFIncidentes_Data_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Incidentes_Data] <= @AV36TFIncidentes_Data_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Incidentes_Data] <= @AV36TFIncidentes_Data_To)";
            }
         }
         else
         {
            GXv_int4[9] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV42TFIncidentes_Descricao_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV41TFIncidentes_Descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Incidentes_Descricao] like @lV41TFIncidentes_Descricao)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Incidentes_Descricao] like @lV41TFIncidentes_Descricao)";
            }
         }
         else
         {
            GXv_int4[10] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV42TFIncidentes_Descricao_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Incidentes_Descricao] = @AV42TFIncidentes_Descricao_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Incidentes_Descricao] = @AV42TFIncidentes_Descricao_Sel)";
            }
         }
         else
         {
            GXv_int4[11] = 1;
         }
         if ( ! (0==AV45TFIncidentes_DemandaCod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Incidentes_DemandaCod] >= @AV45TFIncidentes_DemandaCod)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Incidentes_DemandaCod] >= @AV45TFIncidentes_DemandaCod)";
            }
         }
         else
         {
            GXv_int4[12] = 1;
         }
         if ( ! (0==AV46TFIncidentes_DemandaCod_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[Incidentes_DemandaCod] <= @AV46TFIncidentes_DemandaCod_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[Incidentes_DemandaCod] <= @AV46TFIncidentes_DemandaCod_To)";
            }
         }
         else
         {
            GXv_int4[13] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV50TFIncidentes_DemandaNum_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV49TFIncidentes_DemandaNum)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] like @lV49TFIncidentes_DemandaNum)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[ContagemResultado_Demanda] like @lV49TFIncidentes_DemandaNum)";
            }
         }
         else
         {
            GXv_int4[14] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV50TFIncidentes_DemandaNum_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[ContagemResultado_Demanda] = @AV50TFIncidentes_DemandaNum_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[ContagemResultado_Demanda] = @AV50TFIncidentes_DemandaNum_Sel)";
            }
         }
         else
         {
            GXv_int4[15] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 5 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 5 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H00J52(context, (String)dynConstraints[0] , (DateTime)dynConstraints[1] , (DateTime)dynConstraints[2] , (bool)dynConstraints[3] , (String)dynConstraints[4] , (DateTime)dynConstraints[5] , (DateTime)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (DateTime)dynConstraints[9] , (DateTime)dynConstraints[10] , (int)dynConstraints[11] , (int)dynConstraints[12] , (DateTime)dynConstraints[13] , (DateTime)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (int)dynConstraints[17] , (int)dynConstraints[18] , (String)dynConstraints[19] , (String)dynConstraints[20] , (DateTime)dynConstraints[21] , (int)dynConstraints[22] , (String)dynConstraints[23] , (int)dynConstraints[24] , (String)dynConstraints[25] , (short)dynConstraints[26] , (bool)dynConstraints[27] );
               case 1 :
                     return conditional_H00J53(context, (String)dynConstraints[0] , (DateTime)dynConstraints[1] , (DateTime)dynConstraints[2] , (bool)dynConstraints[3] , (String)dynConstraints[4] , (DateTime)dynConstraints[5] , (DateTime)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (DateTime)dynConstraints[9] , (DateTime)dynConstraints[10] , (int)dynConstraints[11] , (int)dynConstraints[12] , (DateTime)dynConstraints[13] , (DateTime)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (int)dynConstraints[17] , (int)dynConstraints[18] , (String)dynConstraints[19] , (String)dynConstraints[20] , (DateTime)dynConstraints[21] , (int)dynConstraints[22] , (String)dynConstraints[23] , (int)dynConstraints[24] , (String)dynConstraints[25] , (short)dynConstraints[26] , (bool)dynConstraints[27] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00J52 ;
          prmH00J52 = new Object[] {
          new Object[] {"@AV16Incidentes_Data1",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV17Incidentes_Data_To1",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV20Incidentes_Data2",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV21Incidentes_Data_To2",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV24Incidentes_Data3",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV25Incidentes_Data_To3",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV31TFIncidentes_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV32TFIncidentes_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV35TFIncidentes_Data",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV36TFIncidentes_Data_To",SqlDbType.DateTime,8,5} ,
          new Object[] {"@lV41TFIncidentes_Descricao",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV42TFIncidentes_Descricao_Sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV45TFIncidentes_DemandaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV46TFIncidentes_DemandaCod_To",SqlDbType.Int,6,0} ,
          new Object[] {"@lV49TFIncidentes_DemandaNum",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV50TFIncidentes_DemandaNum_Sel",SqlDbType.VarChar,30,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH00J53 ;
          prmH00J53 = new Object[] {
          new Object[] {"@AV16Incidentes_Data1",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV17Incidentes_Data_To1",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV20Incidentes_Data2",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV21Incidentes_Data_To2",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV24Incidentes_Data3",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV25Incidentes_Data_To3",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV31TFIncidentes_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV32TFIncidentes_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV35TFIncidentes_Data",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV36TFIncidentes_Data_To",SqlDbType.DateTime,8,5} ,
          new Object[] {"@lV41TFIncidentes_Descricao",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV42TFIncidentes_Descricao_Sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV45TFIncidentes_DemandaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV46TFIncidentes_DemandaCod_To",SqlDbType.Int,6,0} ,
          new Object[] {"@lV49TFIncidentes_DemandaNum",SqlDbType.VarChar,30,0} ,
          new Object[] {"@AV50TFIncidentes_DemandaNum_Sel",SqlDbType.VarChar,30,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00J52", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00J52,11,0,true,false )
             ,new CursorDef("H00J53", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00J53,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((String[]) buf[3])[0] = rslt.getLongVarchar(3) ;
                ((DateTime[]) buf[4])[0] = rslt.getGXDateTime(4) ;
                ((int[]) buf[5])[0] = rslt.getInt(5) ;
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[21]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[22]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[23]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[24]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[25]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[26]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[27]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[28]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[29]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[30]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[33]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[34]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[37]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[38]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[39]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[40]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[41]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[16]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[17]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[18]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[19]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[20]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[21]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[22]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[23]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[24]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[25]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[28]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[29]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                return;
       }
    }

 }

}
