/*
               File: PromptLoteArquivoAnexo
        Description: Selecione Arquivos Anexos
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 23:28:9.80
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class promptlotearquivoanexo : GXHttpHandler, System.Web.SessionState.IRequiresSessionState
   {
      public promptlotearquivoanexo( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public promptlotearquivoanexo( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_InOutLoteArquivoAnexo_LoteCod ,
                           ref DateTime aP1_InOutLoteArquivoAnexo_Data ,
                           ref String aP2_InOutLoteArquivoAnexo_NomeArq )
      {
         this.AV7InOutLoteArquivoAnexo_LoteCod = aP0_InOutLoteArquivoAnexo_LoteCod;
         this.AV8InOutLoteArquivoAnexo_Data = aP1_InOutLoteArquivoAnexo_Data;
         this.AV9InOutLoteArquivoAnexo_NomeArq = aP2_InOutLoteArquivoAnexo_NomeArq;
         executePrivate();
         aP0_InOutLoteArquivoAnexo_LoteCod=this.AV7InOutLoteArquivoAnexo_LoteCod;
         aP1_InOutLoteArquivoAnexo_Data=this.AV8InOutLoteArquivoAnexo_Data;
         aP2_InOutLoteArquivoAnexo_NomeArq=this.AV9InOutLoteArquivoAnexo_NomeArq;
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavOrderedby = new GXCombobox();
         cmbavDynamicfiltersselector1 = new GXCombobox();
         cmbavDynamicfiltersoperator1 = new GXCombobox();
         cmbavDynamicfiltersselector2 = new GXCombobox();
         cmbavDynamicfiltersoperator2 = new GXCombobox();
         cmbavDynamicfiltersselector3 = new GXCombobox();
         cmbavDynamicfiltersoperator3 = new GXCombobox();
         chkavDynamicfiltersenabled2 = new GXCheckbox();
         chkavDynamicfiltersenabled3 = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
            {
               nRC_GXsfl_83 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_83_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_83_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
            {
               subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV14OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
               AV15OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15OrderedDsc", AV15OrderedDsc);
               AV16DynamicFiltersSelector1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
               AV17DynamicFiltersOperator1 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)));
               AV18LoteArquivoAnexo_NomeArq1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18LoteArquivoAnexo_NomeArq1", AV18LoteArquivoAnexo_NomeArq1);
               AV19TipoDocumento_Nome1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19TipoDocumento_Nome1", AV19TipoDocumento_Nome1);
               AV21DynamicFiltersSelector2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
               AV22DynamicFiltersOperator2 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)));
               AV23LoteArquivoAnexo_NomeArq2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23LoteArquivoAnexo_NomeArq2", AV23LoteArquivoAnexo_NomeArq2);
               AV24TipoDocumento_Nome2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24TipoDocumento_Nome2", AV24TipoDocumento_Nome2);
               AV26DynamicFiltersSelector3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersSelector3", AV26DynamicFiltersSelector3);
               AV27DynamicFiltersOperator3 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV27DynamicFiltersOperator3), 4, 0)));
               AV28LoteArquivoAnexo_NomeArq3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28LoteArquivoAnexo_NomeArq3", AV28LoteArquivoAnexo_NomeArq3);
               AV29TipoDocumento_Nome3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29TipoDocumento_Nome3", AV29TipoDocumento_Nome3);
               AV20DynamicFiltersEnabled2 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled2", AV20DynamicFiltersEnabled2);
               AV25DynamicFiltersEnabled3 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersEnabled3", AV25DynamicFiltersEnabled3);
               AV35TFLoteArquivoAnexo_LoteCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TFLoteArquivoAnexo_LoteCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35TFLoteArquivoAnexo_LoteCod), 6, 0)));
               AV36TFLoteArquivoAnexo_LoteCod_To = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36TFLoteArquivoAnexo_LoteCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36TFLoteArquivoAnexo_LoteCod_To), 6, 0)));
               AV39TFLoteArquivoAnexo_Data = context.localUtil.ParseDTimeParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFLoteArquivoAnexo_Data", context.localUtil.TToC( AV39TFLoteArquivoAnexo_Data, 8, 5, 0, 3, "/", ":", " "));
               AV40TFLoteArquivoAnexo_Data_To = context.localUtil.ParseDTimeParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40TFLoteArquivoAnexo_Data_To", context.localUtil.TToC( AV40TFLoteArquivoAnexo_Data_To, 8, 5, 0, 3, "/", ":", " "));
               AV45TFLoteArquivoAnexo_NomeArq = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45TFLoteArquivoAnexo_NomeArq", AV45TFLoteArquivoAnexo_NomeArq);
               AV46TFLoteArquivoAnexo_NomeArq_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46TFLoteArquivoAnexo_NomeArq_Sel", AV46TFLoteArquivoAnexo_NomeArq_Sel);
               AV49TFLoteArquivoAnexo_TipoArq = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49TFLoteArquivoAnexo_TipoArq", AV49TFLoteArquivoAnexo_TipoArq);
               AV50TFLoteArquivoAnexo_TipoArq_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50TFLoteArquivoAnexo_TipoArq_Sel", AV50TFLoteArquivoAnexo_TipoArq_Sel);
               AV53TFLoteArquivoAnexo_Descricao = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53TFLoteArquivoAnexo_Descricao", AV53TFLoteArquivoAnexo_Descricao);
               AV54TFLoteArquivoAnexo_Descricao_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54TFLoteArquivoAnexo_Descricao_Sel", AV54TFLoteArquivoAnexo_Descricao_Sel);
               AV57TFTipoDocumento_Nome = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57TFTipoDocumento_Nome", AV57TFTipoDocumento_Nome);
               AV58TFTipoDocumento_Nome_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58TFTipoDocumento_Nome_Sel", AV58TFTipoDocumento_Nome_Sel);
               AV37ddo_LoteArquivoAnexo_LoteCodTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37ddo_LoteArquivoAnexo_LoteCodTitleControlIdToReplace", AV37ddo_LoteArquivoAnexo_LoteCodTitleControlIdToReplace);
               AV43ddo_LoteArquivoAnexo_DataTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43ddo_LoteArquivoAnexo_DataTitleControlIdToReplace", AV43ddo_LoteArquivoAnexo_DataTitleControlIdToReplace);
               AV47ddo_LoteArquivoAnexo_NomeArqTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47ddo_LoteArquivoAnexo_NomeArqTitleControlIdToReplace", AV47ddo_LoteArquivoAnexo_NomeArqTitleControlIdToReplace);
               AV51ddo_LoteArquivoAnexo_TipoArqTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51ddo_LoteArquivoAnexo_TipoArqTitleControlIdToReplace", AV51ddo_LoteArquivoAnexo_TipoArqTitleControlIdToReplace);
               AV55ddo_LoteArquivoAnexo_DescricaoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55ddo_LoteArquivoAnexo_DescricaoTitleControlIdToReplace", AV55ddo_LoteArquivoAnexo_DescricaoTitleControlIdToReplace);
               AV59ddo_TipoDocumento_NomeTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59ddo_TipoDocumento_NomeTitleControlIdToReplace", AV59ddo_TipoDocumento_NomeTitleControlIdToReplace);
               AV67Pgmname = GetNextPar( );
               ajax_req_read_hidden_sdt(GetNextPar( ), AV11GridState);
               AV31DynamicFiltersIgnoreFirst = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31DynamicFiltersIgnoreFirst", AV31DynamicFiltersIgnoreFirst);
               AV30DynamicFiltersRemoving = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30DynamicFiltersRemoving", AV30DynamicFiltersRemoving);
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18LoteArquivoAnexo_NomeArq1, AV19TipoDocumento_Nome1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23LoteArquivoAnexo_NomeArq2, AV24TipoDocumento_Nome2, AV26DynamicFiltersSelector3, AV27DynamicFiltersOperator3, AV28LoteArquivoAnexo_NomeArq3, AV29TipoDocumento_Nome3, AV20DynamicFiltersEnabled2, AV25DynamicFiltersEnabled3, AV35TFLoteArquivoAnexo_LoteCod, AV36TFLoteArquivoAnexo_LoteCod_To, AV39TFLoteArquivoAnexo_Data, AV40TFLoteArquivoAnexo_Data_To, AV45TFLoteArquivoAnexo_NomeArq, AV46TFLoteArquivoAnexo_NomeArq_Sel, AV49TFLoteArquivoAnexo_TipoArq, AV50TFLoteArquivoAnexo_TipoArq_Sel, AV53TFLoteArquivoAnexo_Descricao, AV54TFLoteArquivoAnexo_Descricao_Sel, AV57TFTipoDocumento_Nome, AV58TFTipoDocumento_Nome_Sel, AV37ddo_LoteArquivoAnexo_LoteCodTitleControlIdToReplace, AV43ddo_LoteArquivoAnexo_DataTitleControlIdToReplace, AV47ddo_LoteArquivoAnexo_NomeArqTitleControlIdToReplace, AV51ddo_LoteArquivoAnexo_TipoArqTitleControlIdToReplace, AV55ddo_LoteArquivoAnexo_DescricaoTitleControlIdToReplace, AV59ddo_TipoDocumento_NomeTitleControlIdToReplace, AV67Pgmname, AV11GridState, AV31DynamicFiltersIgnoreFirst, AV30DynamicFiltersRemoving) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               AV7InOutLoteArquivoAnexo_LoteCod = (int)(NumberUtil.Val( gxfirstwebparm, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InOutLoteArquivoAnexo_LoteCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7InOutLoteArquivoAnexo_LoteCod), 6, 0)));
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
               {
                  AV8InOutLoteArquivoAnexo_Data = context.localUtil.ParseDTimeParm( GetNextPar( ));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8InOutLoteArquivoAnexo_Data", context.localUtil.TToC( AV8InOutLoteArquivoAnexo_Data, 8, 5, 0, 3, "/", ":", " "));
                  AV9InOutLoteArquivoAnexo_NomeArq = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9InOutLoteArquivoAnexo_NomeArq", AV9InOutLoteArquivoAnexo_NomeArq);
               }
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            ValidateSpaRequest();
            PAEX2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV67Pgmname = "PromptLoteArquivoAnexo";
               context.Gx_err = 0;
               WSEX2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  WEEX2( ) ;
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?202042823281021");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("promptlotearquivoanexo.aspx") + "?" + UrlEncode("" +AV7InOutLoteArquivoAnexo_LoteCod) + "," + UrlEncode(DateTimeUtil.FormatDateTimeParm(AV8InOutLoteArquivoAnexo_Data)) + "," + UrlEncode(StringUtil.RTrim(AV9InOutLoteArquivoAnexo_NomeArq))+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV14OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDDSC", StringUtil.BoolToStr( AV15OrderedDsc));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR1", AV16DynamicFiltersSelector1);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR1", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV17DynamicFiltersOperator1), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vLOTEARQUIVOANEXO_NOMEARQ1", StringUtil.RTrim( AV18LoteArquivoAnexo_NomeArq1));
         GxWebStd.gx_hidden_field( context, "GXH_vTIPODOCUMENTO_NOME1", StringUtil.RTrim( AV19TipoDocumento_Nome1));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR2", AV21DynamicFiltersSelector2);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR2", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV22DynamicFiltersOperator2), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vLOTEARQUIVOANEXO_NOMEARQ2", StringUtil.RTrim( AV23LoteArquivoAnexo_NomeArq2));
         GxWebStd.gx_hidden_field( context, "GXH_vTIPODOCUMENTO_NOME2", StringUtil.RTrim( AV24TipoDocumento_Nome2));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR3", AV26DynamicFiltersSelector3);
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSOPERATOR3", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV27DynamicFiltersOperator3), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vLOTEARQUIVOANEXO_NOMEARQ3", StringUtil.RTrim( AV28LoteArquivoAnexo_NomeArq3));
         GxWebStd.gx_hidden_field( context, "GXH_vTIPODOCUMENTO_NOME3", StringUtil.RTrim( AV29TipoDocumento_Nome3));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED2", StringUtil.BoolToStr( AV20DynamicFiltersEnabled2));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED3", StringUtil.BoolToStr( AV25DynamicFiltersEnabled3));
         GxWebStd.gx_hidden_field( context, "GXH_vTFLOTEARQUIVOANEXO_LOTECOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV35TFLoteArquivoAnexo_LoteCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFLOTEARQUIVOANEXO_LOTECOD_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV36TFLoteArquivoAnexo_LoteCod_To), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFLOTEARQUIVOANEXO_DATA", context.localUtil.TToC( AV39TFLoteArquivoAnexo_Data, 10, 8, 0, 3, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "GXH_vTFLOTEARQUIVOANEXO_DATA_TO", context.localUtil.TToC( AV40TFLoteArquivoAnexo_Data_To, 10, 8, 0, 3, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "GXH_vTFLOTEARQUIVOANEXO_NOMEARQ", StringUtil.RTrim( AV45TFLoteArquivoAnexo_NomeArq));
         GxWebStd.gx_hidden_field( context, "GXH_vTFLOTEARQUIVOANEXO_NOMEARQ_SEL", StringUtil.RTrim( AV46TFLoteArquivoAnexo_NomeArq_Sel));
         GxWebStd.gx_hidden_field( context, "GXH_vTFLOTEARQUIVOANEXO_TIPOARQ", StringUtil.RTrim( AV49TFLoteArquivoAnexo_TipoArq));
         GxWebStd.gx_hidden_field( context, "GXH_vTFLOTEARQUIVOANEXO_TIPOARQ_SEL", StringUtil.RTrim( AV50TFLoteArquivoAnexo_TipoArq_Sel));
         GxWebStd.gx_hidden_field( context, "GXH_vTFLOTEARQUIVOANEXO_DESCRICAO", AV53TFLoteArquivoAnexo_Descricao);
         GxWebStd.gx_hidden_field( context, "GXH_vTFLOTEARQUIVOANEXO_DESCRICAO_SEL", AV54TFLoteArquivoAnexo_Descricao_Sel);
         GxWebStd.gx_hidden_field( context, "GXH_vTFTIPODOCUMENTO_NOME", StringUtil.RTrim( AV57TFTipoDocumento_Nome));
         GxWebStd.gx_hidden_field( context, "GXH_vTFTIPODOCUMENTO_NOME_SEL", StringUtil.RTrim( AV58TFTipoDocumento_Nome_Sel));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_83", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_83), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV62GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV63GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vDDO_TITLESETTINGSICONS", AV60DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vDDO_TITLESETTINGSICONS", AV60DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vLOTEARQUIVOANEXO_LOTECODTITLEFILTERDATA", AV34LoteArquivoAnexo_LoteCodTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vLOTEARQUIVOANEXO_LOTECODTITLEFILTERDATA", AV34LoteArquivoAnexo_LoteCodTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vLOTEARQUIVOANEXO_DATATITLEFILTERDATA", AV38LoteArquivoAnexo_DataTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vLOTEARQUIVOANEXO_DATATITLEFILTERDATA", AV38LoteArquivoAnexo_DataTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vLOTEARQUIVOANEXO_NOMEARQTITLEFILTERDATA", AV44LoteArquivoAnexo_NomeArqTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vLOTEARQUIVOANEXO_NOMEARQTITLEFILTERDATA", AV44LoteArquivoAnexo_NomeArqTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vLOTEARQUIVOANEXO_TIPOARQTITLEFILTERDATA", AV48LoteArquivoAnexo_TipoArqTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vLOTEARQUIVOANEXO_TIPOARQTITLEFILTERDATA", AV48LoteArquivoAnexo_TipoArqTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vLOTEARQUIVOANEXO_DESCRICAOTITLEFILTERDATA", AV52LoteArquivoAnexo_DescricaoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vLOTEARQUIVOANEXO_DESCRICAOTITLEFILTERDATA", AV52LoteArquivoAnexo_DescricaoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTIPODOCUMENTO_NOMETITLEFILTERDATA", AV56TipoDocumento_NomeTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTIPODOCUMENTO_NOMETITLEFILTERDATA", AV56TipoDocumento_NomeTitleFilterData);
         }
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV67Pgmname));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vGRIDSTATE", AV11GridState);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vGRIDSTATE", AV11GridState);
         }
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSIGNOREFIRST", AV31DynamicFiltersIgnoreFirst);
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSREMOVING", AV30DynamicFiltersRemoving);
         GxWebStd.gx_hidden_field( context, "vINOUTLOTEARQUIVOANEXO_LOTECOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7InOutLoteArquivoAnexo_LoteCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINOUTLOTEARQUIVOANEXO_DATA", context.localUtil.TToC( AV8InOutLoteArquivoAnexo_Data, 10, 8, 0, 0, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "vINOUTLOTEARQUIVOANEXO_NOMEARQ", StringUtil.RTrim( AV9InOutLoteArquivoAnexo_NomeArq));
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_LOTECOD_Caption", StringUtil.RTrim( Ddo_lotearquivoanexo_lotecod_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_LOTECOD_Tooltip", StringUtil.RTrim( Ddo_lotearquivoanexo_lotecod_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_LOTECOD_Cls", StringUtil.RTrim( Ddo_lotearquivoanexo_lotecod_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_LOTECOD_Filteredtext_set", StringUtil.RTrim( Ddo_lotearquivoanexo_lotecod_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_LOTECOD_Filteredtextto_set", StringUtil.RTrim( Ddo_lotearquivoanexo_lotecod_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_LOTECOD_Dropdownoptionstype", StringUtil.RTrim( Ddo_lotearquivoanexo_lotecod_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_LOTECOD_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_lotearquivoanexo_lotecod_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_LOTECOD_Includesortasc", StringUtil.BoolToStr( Ddo_lotearquivoanexo_lotecod_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_LOTECOD_Includesortdsc", StringUtil.BoolToStr( Ddo_lotearquivoanexo_lotecod_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_LOTECOD_Sortedstatus", StringUtil.RTrim( Ddo_lotearquivoanexo_lotecod_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_LOTECOD_Includefilter", StringUtil.BoolToStr( Ddo_lotearquivoanexo_lotecod_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_LOTECOD_Filtertype", StringUtil.RTrim( Ddo_lotearquivoanexo_lotecod_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_LOTECOD_Filterisrange", StringUtil.BoolToStr( Ddo_lotearquivoanexo_lotecod_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_LOTECOD_Includedatalist", StringUtil.BoolToStr( Ddo_lotearquivoanexo_lotecod_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_LOTECOD_Datalistfixedvalues", StringUtil.RTrim( Ddo_lotearquivoanexo_lotecod_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_LOTECOD_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_lotearquivoanexo_lotecod_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_LOTECOD_Sortasc", StringUtil.RTrim( Ddo_lotearquivoanexo_lotecod_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_LOTECOD_Sortdsc", StringUtil.RTrim( Ddo_lotearquivoanexo_lotecod_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_LOTECOD_Loadingdata", StringUtil.RTrim( Ddo_lotearquivoanexo_lotecod_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_LOTECOD_Cleanfilter", StringUtil.RTrim( Ddo_lotearquivoanexo_lotecod_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_LOTECOD_Rangefilterfrom", StringUtil.RTrim( Ddo_lotearquivoanexo_lotecod_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_LOTECOD_Rangefilterto", StringUtil.RTrim( Ddo_lotearquivoanexo_lotecod_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_LOTECOD_Noresultsfound", StringUtil.RTrim( Ddo_lotearquivoanexo_lotecod_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_LOTECOD_Searchbuttontext", StringUtil.RTrim( Ddo_lotearquivoanexo_lotecod_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_DATA_Caption", StringUtil.RTrim( Ddo_lotearquivoanexo_data_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_DATA_Tooltip", StringUtil.RTrim( Ddo_lotearquivoanexo_data_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_DATA_Cls", StringUtil.RTrim( Ddo_lotearquivoanexo_data_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_DATA_Filteredtext_set", StringUtil.RTrim( Ddo_lotearquivoanexo_data_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_DATA_Filteredtextto_set", StringUtil.RTrim( Ddo_lotearquivoanexo_data_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_DATA_Dropdownoptionstype", StringUtil.RTrim( Ddo_lotearquivoanexo_data_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_DATA_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_lotearquivoanexo_data_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_DATA_Includesortasc", StringUtil.BoolToStr( Ddo_lotearquivoanexo_data_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_DATA_Includesortdsc", StringUtil.BoolToStr( Ddo_lotearquivoanexo_data_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_DATA_Sortedstatus", StringUtil.RTrim( Ddo_lotearquivoanexo_data_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_DATA_Includefilter", StringUtil.BoolToStr( Ddo_lotearquivoanexo_data_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_DATA_Filtertype", StringUtil.RTrim( Ddo_lotearquivoanexo_data_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_DATA_Filterisrange", StringUtil.BoolToStr( Ddo_lotearquivoanexo_data_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_DATA_Includedatalist", StringUtil.BoolToStr( Ddo_lotearquivoanexo_data_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_DATA_Datalistfixedvalues", StringUtil.RTrim( Ddo_lotearquivoanexo_data_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_DATA_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_lotearquivoanexo_data_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_DATA_Sortasc", StringUtil.RTrim( Ddo_lotearquivoanexo_data_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_DATA_Sortdsc", StringUtil.RTrim( Ddo_lotearquivoanexo_data_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_DATA_Loadingdata", StringUtil.RTrim( Ddo_lotearquivoanexo_data_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_DATA_Cleanfilter", StringUtil.RTrim( Ddo_lotearquivoanexo_data_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_DATA_Rangefilterfrom", StringUtil.RTrim( Ddo_lotearquivoanexo_data_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_DATA_Rangefilterto", StringUtil.RTrim( Ddo_lotearquivoanexo_data_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_DATA_Noresultsfound", StringUtil.RTrim( Ddo_lotearquivoanexo_data_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_DATA_Searchbuttontext", StringUtil.RTrim( Ddo_lotearquivoanexo_data_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_NOMEARQ_Caption", StringUtil.RTrim( Ddo_lotearquivoanexo_nomearq_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_NOMEARQ_Tooltip", StringUtil.RTrim( Ddo_lotearquivoanexo_nomearq_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_NOMEARQ_Cls", StringUtil.RTrim( Ddo_lotearquivoanexo_nomearq_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_NOMEARQ_Filteredtext_set", StringUtil.RTrim( Ddo_lotearquivoanexo_nomearq_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_NOMEARQ_Selectedvalue_set", StringUtil.RTrim( Ddo_lotearquivoanexo_nomearq_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_NOMEARQ_Dropdownoptionstype", StringUtil.RTrim( Ddo_lotearquivoanexo_nomearq_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_NOMEARQ_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_lotearquivoanexo_nomearq_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_NOMEARQ_Includesortasc", StringUtil.BoolToStr( Ddo_lotearquivoanexo_nomearq_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_NOMEARQ_Includesortdsc", StringUtil.BoolToStr( Ddo_lotearquivoanexo_nomearq_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_NOMEARQ_Sortedstatus", StringUtil.RTrim( Ddo_lotearquivoanexo_nomearq_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_NOMEARQ_Includefilter", StringUtil.BoolToStr( Ddo_lotearquivoanexo_nomearq_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_NOMEARQ_Filtertype", StringUtil.RTrim( Ddo_lotearquivoanexo_nomearq_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_NOMEARQ_Filterisrange", StringUtil.BoolToStr( Ddo_lotearquivoanexo_nomearq_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_NOMEARQ_Includedatalist", StringUtil.BoolToStr( Ddo_lotearquivoanexo_nomearq_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_NOMEARQ_Datalisttype", StringUtil.RTrim( Ddo_lotearquivoanexo_nomearq_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_NOMEARQ_Datalistfixedvalues", StringUtil.RTrim( Ddo_lotearquivoanexo_nomearq_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_NOMEARQ_Datalistproc", StringUtil.RTrim( Ddo_lotearquivoanexo_nomearq_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_NOMEARQ_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_lotearquivoanexo_nomearq_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_NOMEARQ_Sortasc", StringUtil.RTrim( Ddo_lotearquivoanexo_nomearq_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_NOMEARQ_Sortdsc", StringUtil.RTrim( Ddo_lotearquivoanexo_nomearq_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_NOMEARQ_Loadingdata", StringUtil.RTrim( Ddo_lotearquivoanexo_nomearq_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_NOMEARQ_Cleanfilter", StringUtil.RTrim( Ddo_lotearquivoanexo_nomearq_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_NOMEARQ_Rangefilterfrom", StringUtil.RTrim( Ddo_lotearquivoanexo_nomearq_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_NOMEARQ_Rangefilterto", StringUtil.RTrim( Ddo_lotearquivoanexo_nomearq_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_NOMEARQ_Noresultsfound", StringUtil.RTrim( Ddo_lotearquivoanexo_nomearq_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_NOMEARQ_Searchbuttontext", StringUtil.RTrim( Ddo_lotearquivoanexo_nomearq_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_TIPOARQ_Caption", StringUtil.RTrim( Ddo_lotearquivoanexo_tipoarq_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_TIPOARQ_Tooltip", StringUtil.RTrim( Ddo_lotearquivoanexo_tipoarq_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_TIPOARQ_Cls", StringUtil.RTrim( Ddo_lotearquivoanexo_tipoarq_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_TIPOARQ_Filteredtext_set", StringUtil.RTrim( Ddo_lotearquivoanexo_tipoarq_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_TIPOARQ_Selectedvalue_set", StringUtil.RTrim( Ddo_lotearquivoanexo_tipoarq_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_TIPOARQ_Dropdownoptionstype", StringUtil.RTrim( Ddo_lotearquivoanexo_tipoarq_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_TIPOARQ_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_lotearquivoanexo_tipoarq_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_TIPOARQ_Includesortasc", StringUtil.BoolToStr( Ddo_lotearquivoanexo_tipoarq_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_TIPOARQ_Includesortdsc", StringUtil.BoolToStr( Ddo_lotearquivoanexo_tipoarq_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_TIPOARQ_Sortedstatus", StringUtil.RTrim( Ddo_lotearquivoanexo_tipoarq_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_TIPOARQ_Includefilter", StringUtil.BoolToStr( Ddo_lotearquivoanexo_tipoarq_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_TIPOARQ_Filtertype", StringUtil.RTrim( Ddo_lotearquivoanexo_tipoarq_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_TIPOARQ_Filterisrange", StringUtil.BoolToStr( Ddo_lotearquivoanexo_tipoarq_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_TIPOARQ_Includedatalist", StringUtil.BoolToStr( Ddo_lotearquivoanexo_tipoarq_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_TIPOARQ_Datalisttype", StringUtil.RTrim( Ddo_lotearquivoanexo_tipoarq_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_TIPOARQ_Datalistfixedvalues", StringUtil.RTrim( Ddo_lotearquivoanexo_tipoarq_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_TIPOARQ_Datalistproc", StringUtil.RTrim( Ddo_lotearquivoanexo_tipoarq_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_TIPOARQ_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_lotearquivoanexo_tipoarq_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_TIPOARQ_Sortasc", StringUtil.RTrim( Ddo_lotearquivoanexo_tipoarq_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_TIPOARQ_Sortdsc", StringUtil.RTrim( Ddo_lotearquivoanexo_tipoarq_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_TIPOARQ_Loadingdata", StringUtil.RTrim( Ddo_lotearquivoanexo_tipoarq_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_TIPOARQ_Cleanfilter", StringUtil.RTrim( Ddo_lotearquivoanexo_tipoarq_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_TIPOARQ_Rangefilterfrom", StringUtil.RTrim( Ddo_lotearquivoanexo_tipoarq_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_TIPOARQ_Rangefilterto", StringUtil.RTrim( Ddo_lotearquivoanexo_tipoarq_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_TIPOARQ_Noresultsfound", StringUtil.RTrim( Ddo_lotearquivoanexo_tipoarq_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_TIPOARQ_Searchbuttontext", StringUtil.RTrim( Ddo_lotearquivoanexo_tipoarq_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_DESCRICAO_Caption", StringUtil.RTrim( Ddo_lotearquivoanexo_descricao_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_DESCRICAO_Tooltip", StringUtil.RTrim( Ddo_lotearquivoanexo_descricao_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_DESCRICAO_Cls", StringUtil.RTrim( Ddo_lotearquivoanexo_descricao_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_DESCRICAO_Filteredtext_set", StringUtil.RTrim( Ddo_lotearquivoanexo_descricao_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_DESCRICAO_Selectedvalue_set", StringUtil.RTrim( Ddo_lotearquivoanexo_descricao_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_DESCRICAO_Dropdownoptionstype", StringUtil.RTrim( Ddo_lotearquivoanexo_descricao_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_DESCRICAO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_lotearquivoanexo_descricao_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_DESCRICAO_Includesortasc", StringUtil.BoolToStr( Ddo_lotearquivoanexo_descricao_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_DESCRICAO_Includesortdsc", StringUtil.BoolToStr( Ddo_lotearquivoanexo_descricao_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_DESCRICAO_Sortedstatus", StringUtil.RTrim( Ddo_lotearquivoanexo_descricao_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_DESCRICAO_Includefilter", StringUtil.BoolToStr( Ddo_lotearquivoanexo_descricao_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_DESCRICAO_Filtertype", StringUtil.RTrim( Ddo_lotearquivoanexo_descricao_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_DESCRICAO_Filterisrange", StringUtil.BoolToStr( Ddo_lotearquivoanexo_descricao_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_DESCRICAO_Includedatalist", StringUtil.BoolToStr( Ddo_lotearquivoanexo_descricao_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_DESCRICAO_Datalisttype", StringUtil.RTrim( Ddo_lotearquivoanexo_descricao_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_DESCRICAO_Datalistfixedvalues", StringUtil.RTrim( Ddo_lotearquivoanexo_descricao_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_DESCRICAO_Datalistproc", StringUtil.RTrim( Ddo_lotearquivoanexo_descricao_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_DESCRICAO_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_lotearquivoanexo_descricao_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_DESCRICAO_Sortasc", StringUtil.RTrim( Ddo_lotearquivoanexo_descricao_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_DESCRICAO_Sortdsc", StringUtil.RTrim( Ddo_lotearquivoanexo_descricao_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_DESCRICAO_Loadingdata", StringUtil.RTrim( Ddo_lotearquivoanexo_descricao_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_DESCRICAO_Cleanfilter", StringUtil.RTrim( Ddo_lotearquivoanexo_descricao_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_DESCRICAO_Rangefilterfrom", StringUtil.RTrim( Ddo_lotearquivoanexo_descricao_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_DESCRICAO_Rangefilterto", StringUtil.RTrim( Ddo_lotearquivoanexo_descricao_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_DESCRICAO_Noresultsfound", StringUtil.RTrim( Ddo_lotearquivoanexo_descricao_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_DESCRICAO_Searchbuttontext", StringUtil.RTrim( Ddo_lotearquivoanexo_descricao_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODOCUMENTO_NOME_Caption", StringUtil.RTrim( Ddo_tipodocumento_nome_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODOCUMENTO_NOME_Tooltip", StringUtil.RTrim( Ddo_tipodocumento_nome_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODOCUMENTO_NOME_Cls", StringUtil.RTrim( Ddo_tipodocumento_nome_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODOCUMENTO_NOME_Filteredtext_set", StringUtil.RTrim( Ddo_tipodocumento_nome_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODOCUMENTO_NOME_Selectedvalue_set", StringUtil.RTrim( Ddo_tipodocumento_nome_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODOCUMENTO_NOME_Dropdownoptionstype", StringUtil.RTrim( Ddo_tipodocumento_nome_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODOCUMENTO_NOME_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_tipodocumento_nome_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODOCUMENTO_NOME_Includesortasc", StringUtil.BoolToStr( Ddo_tipodocumento_nome_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODOCUMENTO_NOME_Includesortdsc", StringUtil.BoolToStr( Ddo_tipodocumento_nome_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODOCUMENTO_NOME_Sortedstatus", StringUtil.RTrim( Ddo_tipodocumento_nome_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODOCUMENTO_NOME_Includefilter", StringUtil.BoolToStr( Ddo_tipodocumento_nome_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODOCUMENTO_NOME_Filtertype", StringUtil.RTrim( Ddo_tipodocumento_nome_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODOCUMENTO_NOME_Filterisrange", StringUtil.BoolToStr( Ddo_tipodocumento_nome_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODOCUMENTO_NOME_Includedatalist", StringUtil.BoolToStr( Ddo_tipodocumento_nome_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODOCUMENTO_NOME_Datalisttype", StringUtil.RTrim( Ddo_tipodocumento_nome_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODOCUMENTO_NOME_Datalistfixedvalues", StringUtil.RTrim( Ddo_tipodocumento_nome_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODOCUMENTO_NOME_Datalistproc", StringUtil.RTrim( Ddo_tipodocumento_nome_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODOCUMENTO_NOME_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_tipodocumento_nome_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODOCUMENTO_NOME_Sortasc", StringUtil.RTrim( Ddo_tipodocumento_nome_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODOCUMENTO_NOME_Sortdsc", StringUtil.RTrim( Ddo_tipodocumento_nome_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODOCUMENTO_NOME_Loadingdata", StringUtil.RTrim( Ddo_tipodocumento_nome_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODOCUMENTO_NOME_Cleanfilter", StringUtil.RTrim( Ddo_tipodocumento_nome_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODOCUMENTO_NOME_Rangefilterfrom", StringUtil.RTrim( Ddo_tipodocumento_nome_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODOCUMENTO_NOME_Rangefilterto", StringUtil.RTrim( Ddo_tipodocumento_nome_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODOCUMENTO_NOME_Noresultsfound", StringUtil.RTrim( Ddo_tipodocumento_nome_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODOCUMENTO_NOME_Searchbuttontext", StringUtil.RTrim( Ddo_tipodocumento_nome_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_LOTECOD_Activeeventkey", StringUtil.RTrim( Ddo_lotearquivoanexo_lotecod_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_LOTECOD_Filteredtext_get", StringUtil.RTrim( Ddo_lotearquivoanexo_lotecod_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_LOTECOD_Filteredtextto_get", StringUtil.RTrim( Ddo_lotearquivoanexo_lotecod_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_DATA_Activeeventkey", StringUtil.RTrim( Ddo_lotearquivoanexo_data_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_DATA_Filteredtext_get", StringUtil.RTrim( Ddo_lotearquivoanexo_data_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_DATA_Filteredtextto_get", StringUtil.RTrim( Ddo_lotearquivoanexo_data_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_NOMEARQ_Activeeventkey", StringUtil.RTrim( Ddo_lotearquivoanexo_nomearq_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_NOMEARQ_Filteredtext_get", StringUtil.RTrim( Ddo_lotearquivoanexo_nomearq_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_NOMEARQ_Selectedvalue_get", StringUtil.RTrim( Ddo_lotearquivoanexo_nomearq_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_TIPOARQ_Activeeventkey", StringUtil.RTrim( Ddo_lotearquivoanexo_tipoarq_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_TIPOARQ_Filteredtext_get", StringUtil.RTrim( Ddo_lotearquivoanexo_tipoarq_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_TIPOARQ_Selectedvalue_get", StringUtil.RTrim( Ddo_lotearquivoanexo_tipoarq_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_DESCRICAO_Activeeventkey", StringUtil.RTrim( Ddo_lotearquivoanexo_descricao_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_DESCRICAO_Filteredtext_get", StringUtil.RTrim( Ddo_lotearquivoanexo_descricao_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_LOTEARQUIVOANEXO_DESCRICAO_Selectedvalue_get", StringUtil.RTrim( Ddo_lotearquivoanexo_descricao_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODOCUMENTO_NOME_Activeeventkey", StringUtil.RTrim( Ddo_tipodocumento_nome_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODOCUMENTO_NOME_Filteredtext_get", StringUtil.RTrim( Ddo_tipodocumento_nome_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_TIPODOCUMENTO_NOME_Selectedvalue_get", StringUtil.RTrim( Ddo_tipodocumento_nome_Selectedvalue_get));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseFormEX2( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
         context.WriteHtmlTextNl( "</body>") ;
         context.WriteHtmlTextNl( "</html>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
      }

      public override String GetPgmname( )
      {
         return "PromptLoteArquivoAnexo" ;
      }

      public override String GetPgmdesc( )
      {
         return "Selecione Arquivos Anexos" ;
      }

      protected void WBEX0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            RenderHtmlHeaders( ) ;
            RenderHtmlOpenForm( ) ;
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, "", "", "", "false");
            wb_table1_2_EX2( true) ;
         }
         else
         {
            wb_table1_2_EX2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_EX2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 97,'',false,'" + sGXsfl_83_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled2_Internalname, StringUtil.BoolToStr( AV20DynamicFiltersEnabled2), "", "", chkavDynamicfiltersenabled2.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(97, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,97);\"");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 98,'',false,'" + sGXsfl_83_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled3_Internalname, StringUtil.BoolToStr( AV25DynamicFiltersEnabled3), "", "", chkavDynamicfiltersenabled3.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(98, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,98);\"");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 99,'',false,'" + sGXsfl_83_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTflotearquivoanexo_lotecod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV35TFLoteArquivoAnexo_LoteCod), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV35TFLoteArquivoAnexo_LoteCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,99);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTflotearquivoanexo_lotecod_Jsonclick, 0, "Attribute", "", "", "", edtavTflotearquivoanexo_lotecod_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptLoteArquivoAnexo.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 100,'',false,'" + sGXsfl_83_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTflotearquivoanexo_lotecod_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV36TFLoteArquivoAnexo_LoteCod_To), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV36TFLoteArquivoAnexo_LoteCod_To), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,100);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTflotearquivoanexo_lotecod_to_Jsonclick, 0, "Attribute", "", "", "", edtavTflotearquivoanexo_lotecod_to_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptLoteArquivoAnexo.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 101,'',false,'" + sGXsfl_83_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTflotearquivoanexo_data_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTflotearquivoanexo_data_Internalname, context.localUtil.TToC( AV39TFLoteArquivoAnexo_Data, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( AV39TFLoteArquivoAnexo_Data, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,101);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTflotearquivoanexo_data_Jsonclick, 0, "Attribute", "", "", "", edtavTflotearquivoanexo_data_Visible, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptLoteArquivoAnexo.htm");
            GxWebStd.gx_bitmap( context, edtavTflotearquivoanexo_data_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTflotearquivoanexo_data_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptLoteArquivoAnexo.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 102,'',false,'" + sGXsfl_83_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTflotearquivoanexo_data_to_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTflotearquivoanexo_data_to_Internalname, context.localUtil.TToC( AV40TFLoteArquivoAnexo_Data_To, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( AV40TFLoteArquivoAnexo_Data_To, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,102);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTflotearquivoanexo_data_to_Jsonclick, 0, "Attribute", "", "", "", edtavTflotearquivoanexo_data_to_Visible, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptLoteArquivoAnexo.htm");
            GxWebStd.gx_bitmap( context, edtavTflotearquivoanexo_data_to_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTflotearquivoanexo_data_to_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptLoteArquivoAnexo.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divDdo_lotearquivoanexo_dataauxdates_Internalname, 1, 0, "px", 0, "px", "Invisible", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 104,'',false,'" + sGXsfl_83_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_lotearquivoanexo_dataauxdate_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_lotearquivoanexo_dataauxdate_Internalname, context.localUtil.Format(AV41DDO_LoteArquivoAnexo_DataAuxDate, "99/99/99"), context.localUtil.Format( AV41DDO_LoteArquivoAnexo_DataAuxDate, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,104);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_lotearquivoanexo_dataauxdate_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptLoteArquivoAnexo.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_lotearquivoanexo_dataauxdate_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptLoteArquivoAnexo.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 105,'',false,'" + sGXsfl_83_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_lotearquivoanexo_dataauxdateto_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_lotearquivoanexo_dataauxdateto_Internalname, context.localUtil.Format(AV42DDO_LoteArquivoAnexo_DataAuxDateTo, "99/99/99"), context.localUtil.Format( AV42DDO_LoteArquivoAnexo_DataAuxDateTo, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,105);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_lotearquivoanexo_dataauxdateto_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_PromptLoteArquivoAnexo.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_lotearquivoanexo_dataauxdateto_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_PromptLoteArquivoAnexo.htm");
            context.WriteHtmlTextNl( "</div>") ;
            GxWebStd.gx_div_end( context, "left", "top");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 106,'',false,'" + sGXsfl_83_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTflotearquivoanexo_nomearq_Internalname, StringUtil.RTrim( AV45TFLoteArquivoAnexo_NomeArq), StringUtil.RTrim( context.localUtil.Format( AV45TFLoteArquivoAnexo_NomeArq, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,106);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTflotearquivoanexo_nomearq_Jsonclick, 0, "Attribute", "", "", "", edtavTflotearquivoanexo_nomearq_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptLoteArquivoAnexo.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 107,'',false,'" + sGXsfl_83_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTflotearquivoanexo_nomearq_sel_Internalname, StringUtil.RTrim( AV46TFLoteArquivoAnexo_NomeArq_Sel), StringUtil.RTrim( context.localUtil.Format( AV46TFLoteArquivoAnexo_NomeArq_Sel, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,107);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTflotearquivoanexo_nomearq_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTflotearquivoanexo_nomearq_sel_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptLoteArquivoAnexo.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 108,'',false,'" + sGXsfl_83_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTflotearquivoanexo_tipoarq_Internalname, StringUtil.RTrim( AV49TFLoteArquivoAnexo_TipoArq), StringUtil.RTrim( context.localUtil.Format( AV49TFLoteArquivoAnexo_TipoArq, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,108);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTflotearquivoanexo_tipoarq_Jsonclick, 0, "Attribute", "", "", "", edtavTflotearquivoanexo_tipoarq_Visible, 1, 0, "text", "", 10, "chr", 1, "row", 10, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptLoteArquivoAnexo.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 109,'',false,'" + sGXsfl_83_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTflotearquivoanexo_tipoarq_sel_Internalname, StringUtil.RTrim( AV50TFLoteArquivoAnexo_TipoArq_Sel), StringUtil.RTrim( context.localUtil.Format( AV50TFLoteArquivoAnexo_TipoArq_Sel, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,109);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTflotearquivoanexo_tipoarq_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTflotearquivoanexo_tipoarq_sel_Visible, 1, 0, "text", "", 10, "chr", 1, "row", 10, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptLoteArquivoAnexo.htm");
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 110,'',false,'" + sGXsfl_83_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavTflotearquivoanexo_descricao_Internalname, AV53TFLoteArquivoAnexo_Descricao, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,110);\"", 0, edtavTflotearquivoanexo_descricao_Visible, 1, 0, 80, "chr", 3, "row", StyleString, ClassString, "", "200", -1, "", "", -1, true, "", "HLP_PromptLoteArquivoAnexo.htm");
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 111,'',false,'" + sGXsfl_83_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavTflotearquivoanexo_descricao_sel_Internalname, AV54TFLoteArquivoAnexo_Descricao_Sel, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,111);\"", 0, edtavTflotearquivoanexo_descricao_sel_Visible, 1, 0, 80, "chr", 3, "row", StyleString, ClassString, "", "200", -1, "", "", -1, true, "", "HLP_PromptLoteArquivoAnexo.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 112,'',false,'" + sGXsfl_83_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTftipodocumento_nome_Internalname, StringUtil.RTrim( AV57TFTipoDocumento_Nome), StringUtil.RTrim( context.localUtil.Format( AV57TFTipoDocumento_Nome, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,112);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTftipodocumento_nome_Jsonclick, 0, "Attribute", "", "", "", edtavTftipodocumento_nome_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptLoteArquivoAnexo.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 113,'',false,'" + sGXsfl_83_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTftipodocumento_nome_sel_Internalname, StringUtil.RTrim( AV58TFTipoDocumento_Nome_Sel), StringUtil.RTrim( context.localUtil.Format( AV58TFTipoDocumento_Nome_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,113);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTftipodocumento_nome_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTftipodocumento_nome_sel_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptLoteArquivoAnexo.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_LOTEARQUIVOANEXO_LOTECODContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 115,'',false,'" + sGXsfl_83_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_lotearquivoanexo_lotecodtitlecontrolidtoreplace_Internalname, AV37ddo_LoteArquivoAnexo_LoteCodTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,115);\"", 0, edtavDdo_lotearquivoanexo_lotecodtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptLoteArquivoAnexo.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_LOTEARQUIVOANEXO_DATAContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 117,'',false,'" + sGXsfl_83_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_lotearquivoanexo_datatitlecontrolidtoreplace_Internalname, AV43ddo_LoteArquivoAnexo_DataTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,117);\"", 0, edtavDdo_lotearquivoanexo_datatitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptLoteArquivoAnexo.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_LOTEARQUIVOANEXO_NOMEARQContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 119,'',false,'" + sGXsfl_83_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_lotearquivoanexo_nomearqtitlecontrolidtoreplace_Internalname, AV47ddo_LoteArquivoAnexo_NomeArqTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,119);\"", 0, edtavDdo_lotearquivoanexo_nomearqtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptLoteArquivoAnexo.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_LOTEARQUIVOANEXO_TIPOARQContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 121,'',false,'" + sGXsfl_83_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_lotearquivoanexo_tipoarqtitlecontrolidtoreplace_Internalname, AV51ddo_LoteArquivoAnexo_TipoArqTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,121);\"", 0, edtavDdo_lotearquivoanexo_tipoarqtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptLoteArquivoAnexo.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_LOTEARQUIVOANEXO_DESCRICAOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 123,'',false,'" + sGXsfl_83_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_lotearquivoanexo_descricaotitlecontrolidtoreplace_Internalname, AV55ddo_LoteArquivoAnexo_DescricaoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,123);\"", 0, edtavDdo_lotearquivoanexo_descricaotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptLoteArquivoAnexo.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_TIPODOCUMENTO_NOMEContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 125,'',false,'" + sGXsfl_83_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_tipodocumento_nometitlecontrolidtoreplace_Internalname, AV59ddo_TipoDocumento_NomeTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,125);\"", 0, edtavDdo_tipodocumento_nometitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_PromptLoteArquivoAnexo.htm");
         }
         wbLoad = true;
      }

      protected void STARTEX2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Selecione Arquivos Anexos", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPEX0( ) ;
      }

      protected void WSEX2( )
      {
         STARTEX2( ) ;
         EVTEX2( ) ;
      }

      protected void EVTEX2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E11EX2 */
                           E11EX2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_LOTEARQUIVOANEXO_LOTECOD.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E12EX2 */
                           E12EX2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_LOTEARQUIVOANEXO_DATA.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E13EX2 */
                           E13EX2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_LOTEARQUIVOANEXO_NOMEARQ.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E14EX2 */
                           E14EX2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_LOTEARQUIVOANEXO_TIPOARQ.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E15EX2 */
                           E15EX2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_LOTEARQUIVOANEXO_DESCRICAO.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E16EX2 */
                           E16EX2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DDO_TIPODOCUMENTO_NOME.ONOPTIONCLICKED") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E17EX2 */
                           E17EX2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VORDEREDBY.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E18EX2 */
                           E18EX2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS1'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E19EX2 */
                           E19EX2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS2'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E20EX2 */
                           E20EX2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS3'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E21EX2 */
                           E21EX2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'DOCLEANFILTERS'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E22EX2 */
                           E22EX2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS1'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E23EX2 */
                           E23EX2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR1.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E24EX2 */
                           E24EX2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS2'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E25EX2 */
                           E25EX2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR2.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E26EX2 */
                           E26EX2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR3.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E27EX2 */
                           E27EX2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           dynload_actions( ) ;
                        }
                     }
                     else
                     {
                        sEvtType = StringUtil.Right( sEvt, 4);
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                        if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) )
                        {
                           nGXsfl_83_idx = (short)(NumberUtil.Val( sEvtType, "."));
                           sGXsfl_83_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_83_idx), 4, 0)), 4, "0");
                           SubsflControlProps_832( ) ;
                           AV32Select = cgiGet( edtavSelect_Internalname);
                           context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavSelect_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV32Select)) ? AV66Select_GXI : context.convertURL( context.PathToRelativeUrl( AV32Select))));
                           A841LoteArquivoAnexo_LoteCod = (int)(context.localUtil.CToN( cgiGet( edtLoteArquivoAnexo_LoteCod_Internalname), ",", "."));
                           A836LoteArquivoAnexo_Data = context.localUtil.CToT( cgiGet( edtLoteArquivoAnexo_Data_Internalname), 0);
                           A838LoteArquivoAnexo_Arquivo = cgiGet( edtLoteArquivoAnexo_Arquivo_Internalname);
                           n838LoteArquivoAnexo_Arquivo = false;
                           context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLoteArquivoAnexo_Arquivo_Internalname, "URL", context.PathToRelativeUrl( A838LoteArquivoAnexo_Arquivo));
                           A837LoteArquivoAnexo_Descricao = cgiGet( edtLoteArquivoAnexo_Descricao_Internalname);
                           n837LoteArquivoAnexo_Descricao = false;
                           A645TipoDocumento_Codigo = (int)(context.localUtil.CToN( cgiGet( edtTipoDocumento_Codigo_Internalname), ",", "."));
                           n645TipoDocumento_Codigo = false;
                           A646TipoDocumento_Nome = StringUtil.Upper( cgiGet( edtTipoDocumento_Nome_Internalname));
                           sEvtType = StringUtil.Right( sEvt, 1);
                           if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                           {
                              sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                              if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E28EX2 */
                                 E28EX2 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E29EX2 */
                                 E29EX2 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                                 /* Execute user event: E30EX2 */
                                 E30EX2 ();
                              }
                              else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    Rfr0gs = false;
                                    /* Set Refresh If Orderedby Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV14OrderedBy )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Ordereddsc Changed */
                                    if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV15OrderedDsc )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersselector1 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV16DynamicFiltersSelector1) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersoperator1 Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR1"), ",", ".") != Convert.ToDecimal( AV17DynamicFiltersOperator1 )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Lotearquivoanexo_nomearq1 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vLOTEARQUIVOANEXO_NOMEARQ1"), AV18LoteArquivoAnexo_NomeArq1) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tipodocumento_nome1 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTIPODOCUMENTO_NOME1"), AV19TipoDocumento_Nome1) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersselector2 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV21DynamicFiltersSelector2) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersoperator2 Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR2"), ",", ".") != Convert.ToDecimal( AV22DynamicFiltersOperator2 )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Lotearquivoanexo_nomearq2 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vLOTEARQUIVOANEXO_NOMEARQ2"), AV23LoteArquivoAnexo_NomeArq2) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tipodocumento_nome2 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTIPODOCUMENTO_NOME2"), AV24TipoDocumento_Nome2) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersselector3 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV26DynamicFiltersSelector3) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersoperator3 Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR3"), ",", ".") != Convert.ToDecimal( AV27DynamicFiltersOperator3 )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Lotearquivoanexo_nomearq3 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vLOTEARQUIVOANEXO_NOMEARQ3"), AV28LoteArquivoAnexo_NomeArq3) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tipodocumento_nome3 Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTIPODOCUMENTO_NOME3"), AV29TipoDocumento_Nome3) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersenabled2 Changed */
                                    if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV20DynamicFiltersEnabled2 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Dynamicfiltersenabled3 Changed */
                                    if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV25DynamicFiltersEnabled3 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tflotearquivoanexo_lotecod Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFLOTEARQUIVOANEXO_LOTECOD"), ",", ".") != Convert.ToDecimal( AV35TFLoteArquivoAnexo_LoteCod )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tflotearquivoanexo_lotecod_to Changed */
                                    if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFLOTEARQUIVOANEXO_LOTECOD_TO"), ",", ".") != Convert.ToDecimal( AV36TFLoteArquivoAnexo_LoteCod_To )) )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tflotearquivoanexo_data Changed */
                                    if ( context.localUtil.CToT( cgiGet( "GXH_vTFLOTEARQUIVOANEXO_DATA"), 0) != AV39TFLoteArquivoAnexo_Data )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tflotearquivoanexo_data_to Changed */
                                    if ( context.localUtil.CToT( cgiGet( "GXH_vTFLOTEARQUIVOANEXO_DATA_TO"), 0) != AV40TFLoteArquivoAnexo_Data_To )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tflotearquivoanexo_nomearq Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFLOTEARQUIVOANEXO_NOMEARQ"), AV45TFLoteArquivoAnexo_NomeArq) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tflotearquivoanexo_nomearq_sel Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFLOTEARQUIVOANEXO_NOMEARQ_SEL"), AV46TFLoteArquivoAnexo_NomeArq_Sel) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tflotearquivoanexo_tipoarq Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFLOTEARQUIVOANEXO_TIPOARQ"), AV49TFLoteArquivoAnexo_TipoArq) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tflotearquivoanexo_tipoarq_sel Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFLOTEARQUIVOANEXO_TIPOARQ_SEL"), AV50TFLoteArquivoAnexo_TipoArq_Sel) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tflotearquivoanexo_descricao Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFLOTEARQUIVOANEXO_DESCRICAO"), AV53TFLoteArquivoAnexo_Descricao) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tflotearquivoanexo_descricao_sel Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFLOTEARQUIVOANEXO_DESCRICAO_SEL"), AV54TFLoteArquivoAnexo_Descricao_Sel) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tftipodocumento_nome Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFTIPODOCUMENTO_NOME"), AV57TFTipoDocumento_Nome) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    /* Set Refresh If Tftipodocumento_nome_sel Changed */
                                    if ( StringUtil.StrCmp(cgiGet( "GXH_vTFTIPODOCUMENTO_NOME_SEL"), AV58TFTipoDocumento_Nome_Sel) != 0 )
                                    {
                                       Rfr0gs = true;
                                    }
                                    if ( ! Rfr0gs )
                                    {
                                       /* Execute user event: E31EX2 */
                                       E31EX2 ();
                                    }
                                    dynload_actions( ) ;
                                 }
                              }
                              else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                              {
                                 context.wbHandled = 1;
                                 dynload_actions( ) ;
                              }
                           }
                           else
                           {
                           }
                        }
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void WEEX2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormEX2( ) ;
            }
         }
      }

      protected void PAEX2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbavOrderedby.Name = "vORDEREDBY";
            cmbavOrderedby.WebTags = "";
            if ( cmbavOrderedby.ItemCount > 0 )
            {
               AV14OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            }
            cmbavDynamicfiltersselector1.Name = "vDYNAMICFILTERSSELECTOR1";
            cmbavDynamicfiltersselector1.WebTags = "";
            cmbavDynamicfiltersselector1.addItem("LOTEARQUIVOANEXO_NOMEARQ", "do Arquivo", 0);
            cmbavDynamicfiltersselector1.addItem("TIPODOCUMENTO_NOME", "Nome", 0);
            if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
            {
               AV16DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV16DynamicFiltersSelector1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
            }
            cmbavDynamicfiltersoperator1.Name = "vDYNAMICFILTERSOPERATOR1";
            cmbavDynamicfiltersoperator1.WebTags = "";
            cmbavDynamicfiltersoperator1.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
            {
               AV17DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)));
            }
            cmbavDynamicfiltersselector2.Name = "vDYNAMICFILTERSSELECTOR2";
            cmbavDynamicfiltersselector2.WebTags = "";
            cmbavDynamicfiltersselector2.addItem("LOTEARQUIVOANEXO_NOMEARQ", "do Arquivo", 0);
            cmbavDynamicfiltersselector2.addItem("TIPODOCUMENTO_NOME", "Nome", 0);
            if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
            {
               AV21DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV21DynamicFiltersSelector2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
            }
            cmbavDynamicfiltersoperator2.Name = "vDYNAMICFILTERSOPERATOR2";
            cmbavDynamicfiltersoperator2.WebTags = "";
            cmbavDynamicfiltersoperator2.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator2.addItem("1", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator2.ItemCount > 0 )
            {
               AV22DynamicFiltersOperator2 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)));
            }
            cmbavDynamicfiltersselector3.Name = "vDYNAMICFILTERSSELECTOR3";
            cmbavDynamicfiltersselector3.WebTags = "";
            cmbavDynamicfiltersselector3.addItem("LOTEARQUIVOANEXO_NOMEARQ", "do Arquivo", 0);
            cmbavDynamicfiltersselector3.addItem("TIPODOCUMENTO_NOME", "Nome", 0);
            if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
            {
               AV26DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV26DynamicFiltersSelector3);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersSelector3", AV26DynamicFiltersSelector3);
            }
            cmbavDynamicfiltersoperator3.Name = "vDYNAMICFILTERSOPERATOR3";
            cmbavDynamicfiltersoperator3.WebTags = "";
            cmbavDynamicfiltersoperator3.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator3.addItem("1", "Cont�m", 0);
            if ( cmbavDynamicfiltersoperator3.ItemCount > 0 )
            {
               AV27DynamicFiltersOperator3 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV27DynamicFiltersOperator3), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV27DynamicFiltersOperator3), 4, 0)));
            }
            chkavDynamicfiltersenabled2.Name = "vDYNAMICFILTERSENABLED2";
            chkavDynamicfiltersenabled2.WebTags = "";
            chkavDynamicfiltersenabled2.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "TitleCaption", chkavDynamicfiltersenabled2.Caption);
            chkavDynamicfiltersenabled2.CheckedValue = "false";
            chkavDynamicfiltersenabled3.Name = "vDYNAMICFILTERSENABLED3";
            chkavDynamicfiltersenabled3.WebTags = "";
            chkavDynamicfiltersenabled3.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "TitleCaption", chkavDynamicfiltersenabled3.Caption);
            chkavDynamicfiltersenabled3.CheckedValue = "false";
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_832( ) ;
         while ( nGXsfl_83_idx <= nRC_GXsfl_83 )
         {
            sendrow_832( ) ;
            nGXsfl_83_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_83_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_83_idx+1));
            sGXsfl_83_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_83_idx), 4, 0)), 4, "0");
            SubsflControlProps_832( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV14OrderedBy ,
                                       bool AV15OrderedDsc ,
                                       String AV16DynamicFiltersSelector1 ,
                                       short AV17DynamicFiltersOperator1 ,
                                       String AV18LoteArquivoAnexo_NomeArq1 ,
                                       String AV19TipoDocumento_Nome1 ,
                                       String AV21DynamicFiltersSelector2 ,
                                       short AV22DynamicFiltersOperator2 ,
                                       String AV23LoteArquivoAnexo_NomeArq2 ,
                                       String AV24TipoDocumento_Nome2 ,
                                       String AV26DynamicFiltersSelector3 ,
                                       short AV27DynamicFiltersOperator3 ,
                                       String AV28LoteArquivoAnexo_NomeArq3 ,
                                       String AV29TipoDocumento_Nome3 ,
                                       bool AV20DynamicFiltersEnabled2 ,
                                       bool AV25DynamicFiltersEnabled3 ,
                                       int AV35TFLoteArquivoAnexo_LoteCod ,
                                       int AV36TFLoteArquivoAnexo_LoteCod_To ,
                                       DateTime AV39TFLoteArquivoAnexo_Data ,
                                       DateTime AV40TFLoteArquivoAnexo_Data_To ,
                                       String AV45TFLoteArquivoAnexo_NomeArq ,
                                       String AV46TFLoteArquivoAnexo_NomeArq_Sel ,
                                       String AV49TFLoteArquivoAnexo_TipoArq ,
                                       String AV50TFLoteArquivoAnexo_TipoArq_Sel ,
                                       String AV53TFLoteArquivoAnexo_Descricao ,
                                       String AV54TFLoteArquivoAnexo_Descricao_Sel ,
                                       String AV57TFTipoDocumento_Nome ,
                                       String AV58TFTipoDocumento_Nome_Sel ,
                                       String AV37ddo_LoteArquivoAnexo_LoteCodTitleControlIdToReplace ,
                                       String AV43ddo_LoteArquivoAnexo_DataTitleControlIdToReplace ,
                                       String AV47ddo_LoteArquivoAnexo_NomeArqTitleControlIdToReplace ,
                                       String AV51ddo_LoteArquivoAnexo_TipoArqTitleControlIdToReplace ,
                                       String AV55ddo_LoteArquivoAnexo_DescricaoTitleControlIdToReplace ,
                                       String AV59ddo_TipoDocumento_NomeTitleControlIdToReplace ,
                                       String AV67Pgmname ,
                                       wwpbaseobjects.SdtWWPGridState AV11GridState ,
                                       bool AV31DynamicFiltersIgnoreFirst ,
                                       bool AV30DynamicFiltersRemoving )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RFEX2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_LOTEARQUIVOANEXO_LOTECOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A841LoteArquivoAnexo_LoteCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "LOTEARQUIVOANEXO_LOTECOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A841LoteArquivoAnexo_LoteCod), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_LOTEARQUIVOANEXO_DATA", GetSecureSignedToken( "", context.localUtil.Format( A836LoteArquivoAnexo_Data, "99/99/99 99:99")));
         GxWebStd.gx_hidden_field( context, "LOTEARQUIVOANEXO_DATA", context.localUtil.TToC( A836LoteArquivoAnexo_Data, 10, 8, 0, 3, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "gxhash_LOTEARQUIVOANEXO_DESCRICAO", GetSecureSignedToken( "", A837LoteArquivoAnexo_Descricao));
         GxWebStd.gx_hidden_field( context, "LOTEARQUIVOANEXO_DESCRICAO", A837LoteArquivoAnexo_Descricao);
         GxWebStd.gx_hidden_field( context, "gxhash_TIPODOCUMENTO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A645TipoDocumento_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "TIPODOCUMENTO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A645TipoDocumento_Codigo), 6, 0, ".", "")));
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavOrderedby.ItemCount > 0 )
         {
            AV14OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
         {
            AV16DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV16DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
         }
         if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
         {
            AV17DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
         {
            AV21DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV21DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
         }
         if ( cmbavDynamicfiltersoperator2.ItemCount > 0 )
         {
            AV22DynamicFiltersOperator2 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
         {
            AV26DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV26DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersSelector3", AV26DynamicFiltersSelector3);
         }
         if ( cmbavDynamicfiltersoperator3.ItemCount > 0 )
         {
            AV27DynamicFiltersOperator3 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator3.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV27DynamicFiltersOperator3), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV27DynamicFiltersOperator3), 4, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFEX2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV67Pgmname = "PromptLoteArquivoAnexo";
         context.Gx_err = 0;
      }

      protected void RFEX2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 83;
         /* Execute user event: E29EX2 */
         E29EX2 ();
         nGXsfl_83_idx = 1;
         sGXsfl_83_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_83_idx), 4, 0)), 4, "0");
         SubsflControlProps_832( ) ;
         nGXsfl_83_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", "");
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_832( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 AV16DynamicFiltersSelector1 ,
                                                 AV17DynamicFiltersOperator1 ,
                                                 AV18LoteArquivoAnexo_NomeArq1 ,
                                                 AV19TipoDocumento_Nome1 ,
                                                 AV20DynamicFiltersEnabled2 ,
                                                 AV21DynamicFiltersSelector2 ,
                                                 AV22DynamicFiltersOperator2 ,
                                                 AV23LoteArquivoAnexo_NomeArq2 ,
                                                 AV24TipoDocumento_Nome2 ,
                                                 AV25DynamicFiltersEnabled3 ,
                                                 AV26DynamicFiltersSelector3 ,
                                                 AV27DynamicFiltersOperator3 ,
                                                 AV28LoteArquivoAnexo_NomeArq3 ,
                                                 AV29TipoDocumento_Nome3 ,
                                                 AV35TFLoteArquivoAnexo_LoteCod ,
                                                 AV36TFLoteArquivoAnexo_LoteCod_To ,
                                                 AV39TFLoteArquivoAnexo_Data ,
                                                 AV40TFLoteArquivoAnexo_Data_To ,
                                                 AV46TFLoteArquivoAnexo_NomeArq_Sel ,
                                                 AV45TFLoteArquivoAnexo_NomeArq ,
                                                 AV50TFLoteArquivoAnexo_TipoArq_Sel ,
                                                 AV49TFLoteArquivoAnexo_TipoArq ,
                                                 AV54TFLoteArquivoAnexo_Descricao_Sel ,
                                                 AV53TFLoteArquivoAnexo_Descricao ,
                                                 AV58TFTipoDocumento_Nome_Sel ,
                                                 AV57TFTipoDocumento_Nome ,
                                                 A839LoteArquivoAnexo_NomeArq ,
                                                 A646TipoDocumento_Nome ,
                                                 A841LoteArquivoAnexo_LoteCod ,
                                                 A836LoteArquivoAnexo_Data ,
                                                 A840LoteArquivoAnexo_TipoArq ,
                                                 A837LoteArquivoAnexo_Descricao ,
                                                 AV14OrderedBy ,
                                                 AV15OrderedDsc },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                                 TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING,
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.INT,
                                                 TypeConstants.DATE, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                                 }
            });
            lV18LoteArquivoAnexo_NomeArq1 = StringUtil.PadR( StringUtil.RTrim( AV18LoteArquivoAnexo_NomeArq1), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18LoteArquivoAnexo_NomeArq1", AV18LoteArquivoAnexo_NomeArq1);
            lV18LoteArquivoAnexo_NomeArq1 = StringUtil.PadR( StringUtil.RTrim( AV18LoteArquivoAnexo_NomeArq1), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18LoteArquivoAnexo_NomeArq1", AV18LoteArquivoAnexo_NomeArq1);
            lV19TipoDocumento_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV19TipoDocumento_Nome1), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19TipoDocumento_Nome1", AV19TipoDocumento_Nome1);
            lV19TipoDocumento_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV19TipoDocumento_Nome1), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19TipoDocumento_Nome1", AV19TipoDocumento_Nome1);
            lV23LoteArquivoAnexo_NomeArq2 = StringUtil.PadR( StringUtil.RTrim( AV23LoteArquivoAnexo_NomeArq2), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23LoteArquivoAnexo_NomeArq2", AV23LoteArquivoAnexo_NomeArq2);
            lV23LoteArquivoAnexo_NomeArq2 = StringUtil.PadR( StringUtil.RTrim( AV23LoteArquivoAnexo_NomeArq2), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23LoteArquivoAnexo_NomeArq2", AV23LoteArquivoAnexo_NomeArq2);
            lV24TipoDocumento_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV24TipoDocumento_Nome2), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24TipoDocumento_Nome2", AV24TipoDocumento_Nome2);
            lV24TipoDocumento_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV24TipoDocumento_Nome2), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24TipoDocumento_Nome2", AV24TipoDocumento_Nome2);
            lV28LoteArquivoAnexo_NomeArq3 = StringUtil.PadR( StringUtil.RTrim( AV28LoteArquivoAnexo_NomeArq3), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28LoteArquivoAnexo_NomeArq3", AV28LoteArquivoAnexo_NomeArq3);
            lV28LoteArquivoAnexo_NomeArq3 = StringUtil.PadR( StringUtil.RTrim( AV28LoteArquivoAnexo_NomeArq3), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28LoteArquivoAnexo_NomeArq3", AV28LoteArquivoAnexo_NomeArq3);
            lV29TipoDocumento_Nome3 = StringUtil.PadR( StringUtil.RTrim( AV29TipoDocumento_Nome3), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29TipoDocumento_Nome3", AV29TipoDocumento_Nome3);
            lV29TipoDocumento_Nome3 = StringUtil.PadR( StringUtil.RTrim( AV29TipoDocumento_Nome3), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29TipoDocumento_Nome3", AV29TipoDocumento_Nome3);
            lV45TFLoteArquivoAnexo_NomeArq = StringUtil.PadR( StringUtil.RTrim( AV45TFLoteArquivoAnexo_NomeArq), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45TFLoteArquivoAnexo_NomeArq", AV45TFLoteArquivoAnexo_NomeArq);
            lV49TFLoteArquivoAnexo_TipoArq = StringUtil.PadR( StringUtil.RTrim( AV49TFLoteArquivoAnexo_TipoArq), 10, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49TFLoteArquivoAnexo_TipoArq", AV49TFLoteArquivoAnexo_TipoArq);
            lV53TFLoteArquivoAnexo_Descricao = StringUtil.Concat( StringUtil.RTrim( AV53TFLoteArquivoAnexo_Descricao), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53TFLoteArquivoAnexo_Descricao", AV53TFLoteArquivoAnexo_Descricao);
            lV57TFTipoDocumento_Nome = StringUtil.PadR( StringUtil.RTrim( AV57TFTipoDocumento_Nome), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57TFTipoDocumento_Nome", AV57TFTipoDocumento_Nome);
            /* Using cursor H00EX2 */
            pr_default.execute(0, new Object[] {lV18LoteArquivoAnexo_NomeArq1, lV18LoteArquivoAnexo_NomeArq1, lV19TipoDocumento_Nome1, lV19TipoDocumento_Nome1, lV23LoteArquivoAnexo_NomeArq2, lV23LoteArquivoAnexo_NomeArq2, lV24TipoDocumento_Nome2, lV24TipoDocumento_Nome2, lV28LoteArquivoAnexo_NomeArq3, lV28LoteArquivoAnexo_NomeArq3, lV29TipoDocumento_Nome3, lV29TipoDocumento_Nome3, AV35TFLoteArquivoAnexo_LoteCod, AV36TFLoteArquivoAnexo_LoteCod_To, AV39TFLoteArquivoAnexo_Data, AV40TFLoteArquivoAnexo_Data_To, lV45TFLoteArquivoAnexo_NomeArq, AV46TFLoteArquivoAnexo_NomeArq_Sel, lV49TFLoteArquivoAnexo_TipoArq, AV50TFLoteArquivoAnexo_TipoArq_Sel, lV53TFLoteArquivoAnexo_Descricao, AV54TFLoteArquivoAnexo_Descricao_Sel, lV57TFTipoDocumento_Nome, AV58TFTipoDocumento_Nome_Sel, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_83_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A839LoteArquivoAnexo_NomeArq = H00EX2_A839LoteArquivoAnexo_NomeArq[0];
               n839LoteArquivoAnexo_NomeArq = H00EX2_n839LoteArquivoAnexo_NomeArq[0];
               edtLoteArquivoAnexo_Arquivo_Filename = A839LoteArquivoAnexo_NomeArq;
               A840LoteArquivoAnexo_TipoArq = H00EX2_A840LoteArquivoAnexo_TipoArq[0];
               n840LoteArquivoAnexo_TipoArq = H00EX2_n840LoteArquivoAnexo_TipoArq[0];
               edtLoteArquivoAnexo_Arquivo_Filetype = A840LoteArquivoAnexo_TipoArq;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLoteArquivoAnexo_Arquivo_Internalname, "Filetype", edtLoteArquivoAnexo_Arquivo_Filetype);
               A646TipoDocumento_Nome = H00EX2_A646TipoDocumento_Nome[0];
               A645TipoDocumento_Codigo = H00EX2_A645TipoDocumento_Codigo[0];
               n645TipoDocumento_Codigo = H00EX2_n645TipoDocumento_Codigo[0];
               A837LoteArquivoAnexo_Descricao = H00EX2_A837LoteArquivoAnexo_Descricao[0];
               n837LoteArquivoAnexo_Descricao = H00EX2_n837LoteArquivoAnexo_Descricao[0];
               A836LoteArquivoAnexo_Data = H00EX2_A836LoteArquivoAnexo_Data[0];
               A841LoteArquivoAnexo_LoteCod = H00EX2_A841LoteArquivoAnexo_LoteCod[0];
               A838LoteArquivoAnexo_Arquivo = H00EX2_A838LoteArquivoAnexo_Arquivo[0];
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLoteArquivoAnexo_Arquivo_Internalname, "URL", context.PathToRelativeUrl( A838LoteArquivoAnexo_Arquivo));
               n838LoteArquivoAnexo_Arquivo = H00EX2_n838LoteArquivoAnexo_Arquivo[0];
               A646TipoDocumento_Nome = H00EX2_A646TipoDocumento_Nome[0];
               /* Execute user event: E30EX2 */
               E30EX2 ();
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 83;
            WBEX0( ) ;
         }
         nGXsfl_83_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV16DynamicFiltersSelector1 ,
                                              AV17DynamicFiltersOperator1 ,
                                              AV18LoteArquivoAnexo_NomeArq1 ,
                                              AV19TipoDocumento_Nome1 ,
                                              AV20DynamicFiltersEnabled2 ,
                                              AV21DynamicFiltersSelector2 ,
                                              AV22DynamicFiltersOperator2 ,
                                              AV23LoteArquivoAnexo_NomeArq2 ,
                                              AV24TipoDocumento_Nome2 ,
                                              AV25DynamicFiltersEnabled3 ,
                                              AV26DynamicFiltersSelector3 ,
                                              AV27DynamicFiltersOperator3 ,
                                              AV28LoteArquivoAnexo_NomeArq3 ,
                                              AV29TipoDocumento_Nome3 ,
                                              AV35TFLoteArquivoAnexo_LoteCod ,
                                              AV36TFLoteArquivoAnexo_LoteCod_To ,
                                              AV39TFLoteArquivoAnexo_Data ,
                                              AV40TFLoteArquivoAnexo_Data_To ,
                                              AV46TFLoteArquivoAnexo_NomeArq_Sel ,
                                              AV45TFLoteArquivoAnexo_NomeArq ,
                                              AV50TFLoteArquivoAnexo_TipoArq_Sel ,
                                              AV49TFLoteArquivoAnexo_TipoArq ,
                                              AV54TFLoteArquivoAnexo_Descricao_Sel ,
                                              AV53TFLoteArquivoAnexo_Descricao ,
                                              AV58TFTipoDocumento_Nome_Sel ,
                                              AV57TFTipoDocumento_Nome ,
                                              A839LoteArquivoAnexo_NomeArq ,
                                              A646TipoDocumento_Nome ,
                                              A841LoteArquivoAnexo_LoteCod ,
                                              A836LoteArquivoAnexo_Data ,
                                              A840LoteArquivoAnexo_TipoArq ,
                                              A837LoteArquivoAnexo_Descricao ,
                                              AV14OrderedBy ,
                                              AV15OrderedDsc },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.INT,
                                              TypeConstants.DATE, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                              }
         });
         lV18LoteArquivoAnexo_NomeArq1 = StringUtil.PadR( StringUtil.RTrim( AV18LoteArquivoAnexo_NomeArq1), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18LoteArquivoAnexo_NomeArq1", AV18LoteArquivoAnexo_NomeArq1);
         lV18LoteArquivoAnexo_NomeArq1 = StringUtil.PadR( StringUtil.RTrim( AV18LoteArquivoAnexo_NomeArq1), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18LoteArquivoAnexo_NomeArq1", AV18LoteArquivoAnexo_NomeArq1);
         lV19TipoDocumento_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV19TipoDocumento_Nome1), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19TipoDocumento_Nome1", AV19TipoDocumento_Nome1);
         lV19TipoDocumento_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV19TipoDocumento_Nome1), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19TipoDocumento_Nome1", AV19TipoDocumento_Nome1);
         lV23LoteArquivoAnexo_NomeArq2 = StringUtil.PadR( StringUtil.RTrim( AV23LoteArquivoAnexo_NomeArq2), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23LoteArquivoAnexo_NomeArq2", AV23LoteArquivoAnexo_NomeArq2);
         lV23LoteArquivoAnexo_NomeArq2 = StringUtil.PadR( StringUtil.RTrim( AV23LoteArquivoAnexo_NomeArq2), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23LoteArquivoAnexo_NomeArq2", AV23LoteArquivoAnexo_NomeArq2);
         lV24TipoDocumento_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV24TipoDocumento_Nome2), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24TipoDocumento_Nome2", AV24TipoDocumento_Nome2);
         lV24TipoDocumento_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV24TipoDocumento_Nome2), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24TipoDocumento_Nome2", AV24TipoDocumento_Nome2);
         lV28LoteArquivoAnexo_NomeArq3 = StringUtil.PadR( StringUtil.RTrim( AV28LoteArquivoAnexo_NomeArq3), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28LoteArquivoAnexo_NomeArq3", AV28LoteArquivoAnexo_NomeArq3);
         lV28LoteArquivoAnexo_NomeArq3 = StringUtil.PadR( StringUtil.RTrim( AV28LoteArquivoAnexo_NomeArq3), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28LoteArquivoAnexo_NomeArq3", AV28LoteArquivoAnexo_NomeArq3);
         lV29TipoDocumento_Nome3 = StringUtil.PadR( StringUtil.RTrim( AV29TipoDocumento_Nome3), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29TipoDocumento_Nome3", AV29TipoDocumento_Nome3);
         lV29TipoDocumento_Nome3 = StringUtil.PadR( StringUtil.RTrim( AV29TipoDocumento_Nome3), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29TipoDocumento_Nome3", AV29TipoDocumento_Nome3);
         lV45TFLoteArquivoAnexo_NomeArq = StringUtil.PadR( StringUtil.RTrim( AV45TFLoteArquivoAnexo_NomeArq), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45TFLoteArquivoAnexo_NomeArq", AV45TFLoteArquivoAnexo_NomeArq);
         lV49TFLoteArquivoAnexo_TipoArq = StringUtil.PadR( StringUtil.RTrim( AV49TFLoteArquivoAnexo_TipoArq), 10, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49TFLoteArquivoAnexo_TipoArq", AV49TFLoteArquivoAnexo_TipoArq);
         lV53TFLoteArquivoAnexo_Descricao = StringUtil.Concat( StringUtil.RTrim( AV53TFLoteArquivoAnexo_Descricao), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53TFLoteArquivoAnexo_Descricao", AV53TFLoteArquivoAnexo_Descricao);
         lV57TFTipoDocumento_Nome = StringUtil.PadR( StringUtil.RTrim( AV57TFTipoDocumento_Nome), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57TFTipoDocumento_Nome", AV57TFTipoDocumento_Nome);
         /* Using cursor H00EX3 */
         pr_default.execute(1, new Object[] {lV18LoteArquivoAnexo_NomeArq1, lV18LoteArquivoAnexo_NomeArq1, lV19TipoDocumento_Nome1, lV19TipoDocumento_Nome1, lV23LoteArquivoAnexo_NomeArq2, lV23LoteArquivoAnexo_NomeArq2, lV24TipoDocumento_Nome2, lV24TipoDocumento_Nome2, lV28LoteArquivoAnexo_NomeArq3, lV28LoteArquivoAnexo_NomeArq3, lV29TipoDocumento_Nome3, lV29TipoDocumento_Nome3, AV35TFLoteArquivoAnexo_LoteCod, AV36TFLoteArquivoAnexo_LoteCod_To, AV39TFLoteArquivoAnexo_Data, AV40TFLoteArquivoAnexo_Data_To, lV45TFLoteArquivoAnexo_NomeArq, AV46TFLoteArquivoAnexo_NomeArq_Sel, lV49TFLoteArquivoAnexo_TipoArq, AV50TFLoteArquivoAnexo_TipoArq_Sel, lV53TFLoteArquivoAnexo_Descricao, AV54TFLoteArquivoAnexo_Descricao_Sel, lV57TFTipoDocumento_Nome, AV58TFTipoDocumento_Nome_Sel});
         GRID_nRecordCount = H00EX3_AGRID_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18LoteArquivoAnexo_NomeArq1, AV19TipoDocumento_Nome1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23LoteArquivoAnexo_NomeArq2, AV24TipoDocumento_Nome2, AV26DynamicFiltersSelector3, AV27DynamicFiltersOperator3, AV28LoteArquivoAnexo_NomeArq3, AV29TipoDocumento_Nome3, AV20DynamicFiltersEnabled2, AV25DynamicFiltersEnabled3, AV35TFLoteArquivoAnexo_LoteCod, AV36TFLoteArquivoAnexo_LoteCod_To, AV39TFLoteArquivoAnexo_Data, AV40TFLoteArquivoAnexo_Data_To, AV45TFLoteArquivoAnexo_NomeArq, AV46TFLoteArquivoAnexo_NomeArq_Sel, AV49TFLoteArquivoAnexo_TipoArq, AV50TFLoteArquivoAnexo_TipoArq_Sel, AV53TFLoteArquivoAnexo_Descricao, AV54TFLoteArquivoAnexo_Descricao_Sel, AV57TFTipoDocumento_Nome, AV58TFTipoDocumento_Nome_Sel, AV37ddo_LoteArquivoAnexo_LoteCodTitleControlIdToReplace, AV43ddo_LoteArquivoAnexo_DataTitleControlIdToReplace, AV47ddo_LoteArquivoAnexo_NomeArqTitleControlIdToReplace, AV51ddo_LoteArquivoAnexo_TipoArqTitleControlIdToReplace, AV55ddo_LoteArquivoAnexo_DescricaoTitleControlIdToReplace, AV59ddo_TipoDocumento_NomeTitleControlIdToReplace, AV67Pgmname, AV11GridState, AV31DynamicFiltersIgnoreFirst, AV30DynamicFiltersRemoving) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18LoteArquivoAnexo_NomeArq1, AV19TipoDocumento_Nome1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23LoteArquivoAnexo_NomeArq2, AV24TipoDocumento_Nome2, AV26DynamicFiltersSelector3, AV27DynamicFiltersOperator3, AV28LoteArquivoAnexo_NomeArq3, AV29TipoDocumento_Nome3, AV20DynamicFiltersEnabled2, AV25DynamicFiltersEnabled3, AV35TFLoteArquivoAnexo_LoteCod, AV36TFLoteArquivoAnexo_LoteCod_To, AV39TFLoteArquivoAnexo_Data, AV40TFLoteArquivoAnexo_Data_To, AV45TFLoteArquivoAnexo_NomeArq, AV46TFLoteArquivoAnexo_NomeArq_Sel, AV49TFLoteArquivoAnexo_TipoArq, AV50TFLoteArquivoAnexo_TipoArq_Sel, AV53TFLoteArquivoAnexo_Descricao, AV54TFLoteArquivoAnexo_Descricao_Sel, AV57TFTipoDocumento_Nome, AV58TFTipoDocumento_Nome_Sel, AV37ddo_LoteArquivoAnexo_LoteCodTitleControlIdToReplace, AV43ddo_LoteArquivoAnexo_DataTitleControlIdToReplace, AV47ddo_LoteArquivoAnexo_NomeArqTitleControlIdToReplace, AV51ddo_LoteArquivoAnexo_TipoArqTitleControlIdToReplace, AV55ddo_LoteArquivoAnexo_DescricaoTitleControlIdToReplace, AV59ddo_TipoDocumento_NomeTitleControlIdToReplace, AV67Pgmname, AV11GridState, AV31DynamicFiltersIgnoreFirst, AV30DynamicFiltersRemoving) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18LoteArquivoAnexo_NomeArq1, AV19TipoDocumento_Nome1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23LoteArquivoAnexo_NomeArq2, AV24TipoDocumento_Nome2, AV26DynamicFiltersSelector3, AV27DynamicFiltersOperator3, AV28LoteArquivoAnexo_NomeArq3, AV29TipoDocumento_Nome3, AV20DynamicFiltersEnabled2, AV25DynamicFiltersEnabled3, AV35TFLoteArquivoAnexo_LoteCod, AV36TFLoteArquivoAnexo_LoteCod_To, AV39TFLoteArquivoAnexo_Data, AV40TFLoteArquivoAnexo_Data_To, AV45TFLoteArquivoAnexo_NomeArq, AV46TFLoteArquivoAnexo_NomeArq_Sel, AV49TFLoteArquivoAnexo_TipoArq, AV50TFLoteArquivoAnexo_TipoArq_Sel, AV53TFLoteArquivoAnexo_Descricao, AV54TFLoteArquivoAnexo_Descricao_Sel, AV57TFTipoDocumento_Nome, AV58TFTipoDocumento_Nome_Sel, AV37ddo_LoteArquivoAnexo_LoteCodTitleControlIdToReplace, AV43ddo_LoteArquivoAnexo_DataTitleControlIdToReplace, AV47ddo_LoteArquivoAnexo_NomeArqTitleControlIdToReplace, AV51ddo_LoteArquivoAnexo_TipoArqTitleControlIdToReplace, AV55ddo_LoteArquivoAnexo_DescricaoTitleControlIdToReplace, AV59ddo_TipoDocumento_NomeTitleControlIdToReplace, AV67Pgmname, AV11GridState, AV31DynamicFiltersIgnoreFirst, AV30DynamicFiltersRemoving) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18LoteArquivoAnexo_NomeArq1, AV19TipoDocumento_Nome1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23LoteArquivoAnexo_NomeArq2, AV24TipoDocumento_Nome2, AV26DynamicFiltersSelector3, AV27DynamicFiltersOperator3, AV28LoteArquivoAnexo_NomeArq3, AV29TipoDocumento_Nome3, AV20DynamicFiltersEnabled2, AV25DynamicFiltersEnabled3, AV35TFLoteArquivoAnexo_LoteCod, AV36TFLoteArquivoAnexo_LoteCod_To, AV39TFLoteArquivoAnexo_Data, AV40TFLoteArquivoAnexo_Data_To, AV45TFLoteArquivoAnexo_NomeArq, AV46TFLoteArquivoAnexo_NomeArq_Sel, AV49TFLoteArquivoAnexo_TipoArq, AV50TFLoteArquivoAnexo_TipoArq_Sel, AV53TFLoteArquivoAnexo_Descricao, AV54TFLoteArquivoAnexo_Descricao_Sel, AV57TFTipoDocumento_Nome, AV58TFTipoDocumento_Nome_Sel, AV37ddo_LoteArquivoAnexo_LoteCodTitleControlIdToReplace, AV43ddo_LoteArquivoAnexo_DataTitleControlIdToReplace, AV47ddo_LoteArquivoAnexo_NomeArqTitleControlIdToReplace, AV51ddo_LoteArquivoAnexo_TipoArqTitleControlIdToReplace, AV55ddo_LoteArquivoAnexo_DescricaoTitleControlIdToReplace, AV59ddo_TipoDocumento_NomeTitleControlIdToReplace, AV67Pgmname, AV11GridState, AV31DynamicFiltersIgnoreFirst, AV30DynamicFiltersRemoving) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18LoteArquivoAnexo_NomeArq1, AV19TipoDocumento_Nome1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23LoteArquivoAnexo_NomeArq2, AV24TipoDocumento_Nome2, AV26DynamicFiltersSelector3, AV27DynamicFiltersOperator3, AV28LoteArquivoAnexo_NomeArq3, AV29TipoDocumento_Nome3, AV20DynamicFiltersEnabled2, AV25DynamicFiltersEnabled3, AV35TFLoteArquivoAnexo_LoteCod, AV36TFLoteArquivoAnexo_LoteCod_To, AV39TFLoteArquivoAnexo_Data, AV40TFLoteArquivoAnexo_Data_To, AV45TFLoteArquivoAnexo_NomeArq, AV46TFLoteArquivoAnexo_NomeArq_Sel, AV49TFLoteArquivoAnexo_TipoArq, AV50TFLoteArquivoAnexo_TipoArq_Sel, AV53TFLoteArquivoAnexo_Descricao, AV54TFLoteArquivoAnexo_Descricao_Sel, AV57TFTipoDocumento_Nome, AV58TFTipoDocumento_Nome_Sel, AV37ddo_LoteArquivoAnexo_LoteCodTitleControlIdToReplace, AV43ddo_LoteArquivoAnexo_DataTitleControlIdToReplace, AV47ddo_LoteArquivoAnexo_NomeArqTitleControlIdToReplace, AV51ddo_LoteArquivoAnexo_TipoArqTitleControlIdToReplace, AV55ddo_LoteArquivoAnexo_DescricaoTitleControlIdToReplace, AV59ddo_TipoDocumento_NomeTitleControlIdToReplace, AV67Pgmname, AV11GridState, AV31DynamicFiltersIgnoreFirst, AV30DynamicFiltersRemoving) ;
         }
         return (int)(0) ;
      }

      protected void STRUPEX0( )
      {
         /* Before Start, stand alone formulas. */
         AV67Pgmname = "PromptLoteArquivoAnexo";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E28EX2 */
         E28EX2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "vDDO_TITLESETTINGSICONS"), AV60DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( "vLOTEARQUIVOANEXO_LOTECODTITLEFILTERDATA"), AV34LoteArquivoAnexo_LoteCodTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vLOTEARQUIVOANEXO_DATATITLEFILTERDATA"), AV38LoteArquivoAnexo_DataTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vLOTEARQUIVOANEXO_NOMEARQTITLEFILTERDATA"), AV44LoteArquivoAnexo_NomeArqTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vLOTEARQUIVOANEXO_TIPOARQTITLEFILTERDATA"), AV48LoteArquivoAnexo_TipoArqTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vLOTEARQUIVOANEXO_DESCRICAOTITLEFILTERDATA"), AV52LoteArquivoAnexo_DescricaoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vTIPODOCUMENTO_NOMETITLEFILTERDATA"), AV56TipoDocumento_NomeTitleFilterData);
            /* Read variables values. */
            cmbavOrderedby.Name = cmbavOrderedby_Internalname;
            cmbavOrderedby.CurrentValue = cgiGet( cmbavOrderedby_Internalname);
            AV14OrderedBy = (short)(NumberUtil.Val( cgiGet( cmbavOrderedby_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15OrderedDsc", AV15OrderedDsc);
            cmbavDynamicfiltersselector1.Name = cmbavDynamicfiltersselector1_Internalname;
            cmbavDynamicfiltersselector1.CurrentValue = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            AV16DynamicFiltersSelector1 = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
            cmbavDynamicfiltersoperator1.Name = cmbavDynamicfiltersoperator1_Internalname;
            cmbavDynamicfiltersoperator1.CurrentValue = cgiGet( cmbavDynamicfiltersoperator1_Internalname);
            AV17DynamicFiltersOperator1 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator1_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)));
            AV18LoteArquivoAnexo_NomeArq1 = cgiGet( edtavLotearquivoanexo_nomearq1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18LoteArquivoAnexo_NomeArq1", AV18LoteArquivoAnexo_NomeArq1);
            AV19TipoDocumento_Nome1 = StringUtil.Upper( cgiGet( edtavTipodocumento_nome1_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19TipoDocumento_Nome1", AV19TipoDocumento_Nome1);
            cmbavDynamicfiltersselector2.Name = cmbavDynamicfiltersselector2_Internalname;
            cmbavDynamicfiltersselector2.CurrentValue = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            AV21DynamicFiltersSelector2 = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
            cmbavDynamicfiltersoperator2.Name = cmbavDynamicfiltersoperator2_Internalname;
            cmbavDynamicfiltersoperator2.CurrentValue = cgiGet( cmbavDynamicfiltersoperator2_Internalname);
            AV22DynamicFiltersOperator2 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator2_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)));
            AV23LoteArquivoAnexo_NomeArq2 = cgiGet( edtavLotearquivoanexo_nomearq2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23LoteArquivoAnexo_NomeArq2", AV23LoteArquivoAnexo_NomeArq2);
            AV24TipoDocumento_Nome2 = StringUtil.Upper( cgiGet( edtavTipodocumento_nome2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24TipoDocumento_Nome2", AV24TipoDocumento_Nome2);
            cmbavDynamicfiltersselector3.Name = cmbavDynamicfiltersselector3_Internalname;
            cmbavDynamicfiltersselector3.CurrentValue = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            AV26DynamicFiltersSelector3 = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersSelector3", AV26DynamicFiltersSelector3);
            cmbavDynamicfiltersoperator3.Name = cmbavDynamicfiltersoperator3_Internalname;
            cmbavDynamicfiltersoperator3.CurrentValue = cgiGet( cmbavDynamicfiltersoperator3_Internalname);
            AV27DynamicFiltersOperator3 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator3_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV27DynamicFiltersOperator3), 4, 0)));
            AV28LoteArquivoAnexo_NomeArq3 = cgiGet( edtavLotearquivoanexo_nomearq3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28LoteArquivoAnexo_NomeArq3", AV28LoteArquivoAnexo_NomeArq3);
            AV29TipoDocumento_Nome3 = StringUtil.Upper( cgiGet( edtavTipodocumento_nome3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29TipoDocumento_Nome3", AV29TipoDocumento_Nome3);
            AV20DynamicFiltersEnabled2 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled2", AV20DynamicFiltersEnabled2);
            AV25DynamicFiltersEnabled3 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersEnabled3", AV25DynamicFiltersEnabled3);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTflotearquivoanexo_lotecod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTflotearquivoanexo_lotecod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFLOTEARQUIVOANEXO_LOTECOD");
               GX_FocusControl = edtavTflotearquivoanexo_lotecod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV35TFLoteArquivoAnexo_LoteCod = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TFLoteArquivoAnexo_LoteCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35TFLoteArquivoAnexo_LoteCod), 6, 0)));
            }
            else
            {
               AV35TFLoteArquivoAnexo_LoteCod = (int)(context.localUtil.CToN( cgiGet( edtavTflotearquivoanexo_lotecod_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TFLoteArquivoAnexo_LoteCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35TFLoteArquivoAnexo_LoteCod), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTflotearquivoanexo_lotecod_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTflotearquivoanexo_lotecod_to_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFLOTEARQUIVOANEXO_LOTECOD_TO");
               GX_FocusControl = edtavTflotearquivoanexo_lotecod_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV36TFLoteArquivoAnexo_LoteCod_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36TFLoteArquivoAnexo_LoteCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36TFLoteArquivoAnexo_LoteCod_To), 6, 0)));
            }
            else
            {
               AV36TFLoteArquivoAnexo_LoteCod_To = (int)(context.localUtil.CToN( cgiGet( edtavTflotearquivoanexo_lotecod_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36TFLoteArquivoAnexo_LoteCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36TFLoteArquivoAnexo_LoteCod_To), 6, 0)));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavTflotearquivoanexo_data_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"TFLote Arquivo Anexo_Data"}), 1, "vTFLOTEARQUIVOANEXO_DATA");
               GX_FocusControl = edtavTflotearquivoanexo_data_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV39TFLoteArquivoAnexo_Data = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFLoteArquivoAnexo_Data", context.localUtil.TToC( AV39TFLoteArquivoAnexo_Data, 8, 5, 0, 3, "/", ":", " "));
            }
            else
            {
               AV39TFLoteArquivoAnexo_Data = context.localUtil.CToT( cgiGet( edtavTflotearquivoanexo_data_Internalname), 0);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFLoteArquivoAnexo_Data", context.localUtil.TToC( AV39TFLoteArquivoAnexo_Data, 8, 5, 0, 3, "/", ":", " "));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavTflotearquivoanexo_data_to_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"TFLote Arquivo Anexo_Data_To"}), 1, "vTFLOTEARQUIVOANEXO_DATA_TO");
               GX_FocusControl = edtavTflotearquivoanexo_data_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV40TFLoteArquivoAnexo_Data_To = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40TFLoteArquivoAnexo_Data_To", context.localUtil.TToC( AV40TFLoteArquivoAnexo_Data_To, 8, 5, 0, 3, "/", ":", " "));
            }
            else
            {
               AV40TFLoteArquivoAnexo_Data_To = context.localUtil.CToT( cgiGet( edtavTflotearquivoanexo_data_to_Internalname), 0);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40TFLoteArquivoAnexo_Data_To", context.localUtil.TToC( AV40TFLoteArquivoAnexo_Data_To, 8, 5, 0, 3, "/", ":", " "));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_lotearquivoanexo_dataauxdate_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Lote Arquivo Anexo_Data Aux Date"}), 1, "vDDO_LOTEARQUIVOANEXO_DATAAUXDATE");
               GX_FocusControl = edtavDdo_lotearquivoanexo_dataauxdate_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV41DDO_LoteArquivoAnexo_DataAuxDate = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41DDO_LoteArquivoAnexo_DataAuxDate", context.localUtil.Format(AV41DDO_LoteArquivoAnexo_DataAuxDate, "99/99/99"));
            }
            else
            {
               AV41DDO_LoteArquivoAnexo_DataAuxDate = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_lotearquivoanexo_dataauxdate_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41DDO_LoteArquivoAnexo_DataAuxDate", context.localUtil.Format(AV41DDO_LoteArquivoAnexo_DataAuxDate, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_lotearquivoanexo_dataauxdateto_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Lote Arquivo Anexo_Data Aux Date To"}), 1, "vDDO_LOTEARQUIVOANEXO_DATAAUXDATETO");
               GX_FocusControl = edtavDdo_lotearquivoanexo_dataauxdateto_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV42DDO_LoteArquivoAnexo_DataAuxDateTo = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42DDO_LoteArquivoAnexo_DataAuxDateTo", context.localUtil.Format(AV42DDO_LoteArquivoAnexo_DataAuxDateTo, "99/99/99"));
            }
            else
            {
               AV42DDO_LoteArquivoAnexo_DataAuxDateTo = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_lotearquivoanexo_dataauxdateto_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42DDO_LoteArquivoAnexo_DataAuxDateTo", context.localUtil.Format(AV42DDO_LoteArquivoAnexo_DataAuxDateTo, "99/99/99"));
            }
            AV45TFLoteArquivoAnexo_NomeArq = cgiGet( edtavTflotearquivoanexo_nomearq_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45TFLoteArquivoAnexo_NomeArq", AV45TFLoteArquivoAnexo_NomeArq);
            AV46TFLoteArquivoAnexo_NomeArq_Sel = cgiGet( edtavTflotearquivoanexo_nomearq_sel_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46TFLoteArquivoAnexo_NomeArq_Sel", AV46TFLoteArquivoAnexo_NomeArq_Sel);
            AV49TFLoteArquivoAnexo_TipoArq = cgiGet( edtavTflotearquivoanexo_tipoarq_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49TFLoteArquivoAnexo_TipoArq", AV49TFLoteArquivoAnexo_TipoArq);
            AV50TFLoteArquivoAnexo_TipoArq_Sel = cgiGet( edtavTflotearquivoanexo_tipoarq_sel_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50TFLoteArquivoAnexo_TipoArq_Sel", AV50TFLoteArquivoAnexo_TipoArq_Sel);
            AV53TFLoteArquivoAnexo_Descricao = cgiGet( edtavTflotearquivoanexo_descricao_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53TFLoteArquivoAnexo_Descricao", AV53TFLoteArquivoAnexo_Descricao);
            AV54TFLoteArquivoAnexo_Descricao_Sel = cgiGet( edtavTflotearquivoanexo_descricao_sel_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54TFLoteArquivoAnexo_Descricao_Sel", AV54TFLoteArquivoAnexo_Descricao_Sel);
            AV57TFTipoDocumento_Nome = StringUtil.Upper( cgiGet( edtavTftipodocumento_nome_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57TFTipoDocumento_Nome", AV57TFTipoDocumento_Nome);
            AV58TFTipoDocumento_Nome_Sel = StringUtil.Upper( cgiGet( edtavTftipodocumento_nome_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58TFTipoDocumento_Nome_Sel", AV58TFTipoDocumento_Nome_Sel);
            AV37ddo_LoteArquivoAnexo_LoteCodTitleControlIdToReplace = cgiGet( edtavDdo_lotearquivoanexo_lotecodtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37ddo_LoteArquivoAnexo_LoteCodTitleControlIdToReplace", AV37ddo_LoteArquivoAnexo_LoteCodTitleControlIdToReplace);
            AV43ddo_LoteArquivoAnexo_DataTitleControlIdToReplace = cgiGet( edtavDdo_lotearquivoanexo_datatitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43ddo_LoteArquivoAnexo_DataTitleControlIdToReplace", AV43ddo_LoteArquivoAnexo_DataTitleControlIdToReplace);
            AV47ddo_LoteArquivoAnexo_NomeArqTitleControlIdToReplace = cgiGet( edtavDdo_lotearquivoanexo_nomearqtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47ddo_LoteArquivoAnexo_NomeArqTitleControlIdToReplace", AV47ddo_LoteArquivoAnexo_NomeArqTitleControlIdToReplace);
            AV51ddo_LoteArquivoAnexo_TipoArqTitleControlIdToReplace = cgiGet( edtavDdo_lotearquivoanexo_tipoarqtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51ddo_LoteArquivoAnexo_TipoArqTitleControlIdToReplace", AV51ddo_LoteArquivoAnexo_TipoArqTitleControlIdToReplace);
            AV55ddo_LoteArquivoAnexo_DescricaoTitleControlIdToReplace = cgiGet( edtavDdo_lotearquivoanexo_descricaotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55ddo_LoteArquivoAnexo_DescricaoTitleControlIdToReplace", AV55ddo_LoteArquivoAnexo_DescricaoTitleControlIdToReplace);
            AV59ddo_TipoDocumento_NomeTitleControlIdToReplace = cgiGet( edtavDdo_tipodocumento_nometitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59ddo_TipoDocumento_NomeTitleControlIdToReplace", AV59ddo_TipoDocumento_NomeTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_83 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_83"), ",", "."));
            AV62GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( "vGRIDCURRENTPAGE"), ",", "."));
            AV63GridPageCount = (long)(context.localUtil.CToN( cgiGet( "vGRIDPAGECOUNT"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( "GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( "GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( "GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( "GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( "GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( "GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( "GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( "GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( "GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( "GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( "GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( "GRIDPAGINATIONBAR_Emptygridcaption");
            Ddo_lotearquivoanexo_lotecod_Caption = cgiGet( "DDO_LOTEARQUIVOANEXO_LOTECOD_Caption");
            Ddo_lotearquivoanexo_lotecod_Tooltip = cgiGet( "DDO_LOTEARQUIVOANEXO_LOTECOD_Tooltip");
            Ddo_lotearquivoanexo_lotecod_Cls = cgiGet( "DDO_LOTEARQUIVOANEXO_LOTECOD_Cls");
            Ddo_lotearquivoanexo_lotecod_Filteredtext_set = cgiGet( "DDO_LOTEARQUIVOANEXO_LOTECOD_Filteredtext_set");
            Ddo_lotearquivoanexo_lotecod_Filteredtextto_set = cgiGet( "DDO_LOTEARQUIVOANEXO_LOTECOD_Filteredtextto_set");
            Ddo_lotearquivoanexo_lotecod_Dropdownoptionstype = cgiGet( "DDO_LOTEARQUIVOANEXO_LOTECOD_Dropdownoptionstype");
            Ddo_lotearquivoanexo_lotecod_Titlecontrolidtoreplace = cgiGet( "DDO_LOTEARQUIVOANEXO_LOTECOD_Titlecontrolidtoreplace");
            Ddo_lotearquivoanexo_lotecod_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_LOTEARQUIVOANEXO_LOTECOD_Includesortasc"));
            Ddo_lotearquivoanexo_lotecod_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_LOTEARQUIVOANEXO_LOTECOD_Includesortdsc"));
            Ddo_lotearquivoanexo_lotecod_Sortedstatus = cgiGet( "DDO_LOTEARQUIVOANEXO_LOTECOD_Sortedstatus");
            Ddo_lotearquivoanexo_lotecod_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_LOTEARQUIVOANEXO_LOTECOD_Includefilter"));
            Ddo_lotearquivoanexo_lotecod_Filtertype = cgiGet( "DDO_LOTEARQUIVOANEXO_LOTECOD_Filtertype");
            Ddo_lotearquivoanexo_lotecod_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_LOTEARQUIVOANEXO_LOTECOD_Filterisrange"));
            Ddo_lotearquivoanexo_lotecod_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_LOTEARQUIVOANEXO_LOTECOD_Includedatalist"));
            Ddo_lotearquivoanexo_lotecod_Datalistfixedvalues = cgiGet( "DDO_LOTEARQUIVOANEXO_LOTECOD_Datalistfixedvalues");
            Ddo_lotearquivoanexo_lotecod_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_LOTEARQUIVOANEXO_LOTECOD_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_lotearquivoanexo_lotecod_Sortasc = cgiGet( "DDO_LOTEARQUIVOANEXO_LOTECOD_Sortasc");
            Ddo_lotearquivoanexo_lotecod_Sortdsc = cgiGet( "DDO_LOTEARQUIVOANEXO_LOTECOD_Sortdsc");
            Ddo_lotearquivoanexo_lotecod_Loadingdata = cgiGet( "DDO_LOTEARQUIVOANEXO_LOTECOD_Loadingdata");
            Ddo_lotearquivoanexo_lotecod_Cleanfilter = cgiGet( "DDO_LOTEARQUIVOANEXO_LOTECOD_Cleanfilter");
            Ddo_lotearquivoanexo_lotecod_Rangefilterfrom = cgiGet( "DDO_LOTEARQUIVOANEXO_LOTECOD_Rangefilterfrom");
            Ddo_lotearquivoanexo_lotecod_Rangefilterto = cgiGet( "DDO_LOTEARQUIVOANEXO_LOTECOD_Rangefilterto");
            Ddo_lotearquivoanexo_lotecod_Noresultsfound = cgiGet( "DDO_LOTEARQUIVOANEXO_LOTECOD_Noresultsfound");
            Ddo_lotearquivoanexo_lotecod_Searchbuttontext = cgiGet( "DDO_LOTEARQUIVOANEXO_LOTECOD_Searchbuttontext");
            Ddo_lotearquivoanexo_data_Caption = cgiGet( "DDO_LOTEARQUIVOANEXO_DATA_Caption");
            Ddo_lotearquivoanexo_data_Tooltip = cgiGet( "DDO_LOTEARQUIVOANEXO_DATA_Tooltip");
            Ddo_lotearquivoanexo_data_Cls = cgiGet( "DDO_LOTEARQUIVOANEXO_DATA_Cls");
            Ddo_lotearquivoanexo_data_Filteredtext_set = cgiGet( "DDO_LOTEARQUIVOANEXO_DATA_Filteredtext_set");
            Ddo_lotearquivoanexo_data_Filteredtextto_set = cgiGet( "DDO_LOTEARQUIVOANEXO_DATA_Filteredtextto_set");
            Ddo_lotearquivoanexo_data_Dropdownoptionstype = cgiGet( "DDO_LOTEARQUIVOANEXO_DATA_Dropdownoptionstype");
            Ddo_lotearquivoanexo_data_Titlecontrolidtoreplace = cgiGet( "DDO_LOTEARQUIVOANEXO_DATA_Titlecontrolidtoreplace");
            Ddo_lotearquivoanexo_data_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_LOTEARQUIVOANEXO_DATA_Includesortasc"));
            Ddo_lotearquivoanexo_data_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_LOTEARQUIVOANEXO_DATA_Includesortdsc"));
            Ddo_lotearquivoanexo_data_Sortedstatus = cgiGet( "DDO_LOTEARQUIVOANEXO_DATA_Sortedstatus");
            Ddo_lotearquivoanexo_data_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_LOTEARQUIVOANEXO_DATA_Includefilter"));
            Ddo_lotearquivoanexo_data_Filtertype = cgiGet( "DDO_LOTEARQUIVOANEXO_DATA_Filtertype");
            Ddo_lotearquivoanexo_data_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_LOTEARQUIVOANEXO_DATA_Filterisrange"));
            Ddo_lotearquivoanexo_data_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_LOTEARQUIVOANEXO_DATA_Includedatalist"));
            Ddo_lotearquivoanexo_data_Datalistfixedvalues = cgiGet( "DDO_LOTEARQUIVOANEXO_DATA_Datalistfixedvalues");
            Ddo_lotearquivoanexo_data_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_LOTEARQUIVOANEXO_DATA_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_lotearquivoanexo_data_Sortasc = cgiGet( "DDO_LOTEARQUIVOANEXO_DATA_Sortasc");
            Ddo_lotearquivoanexo_data_Sortdsc = cgiGet( "DDO_LOTEARQUIVOANEXO_DATA_Sortdsc");
            Ddo_lotearquivoanexo_data_Loadingdata = cgiGet( "DDO_LOTEARQUIVOANEXO_DATA_Loadingdata");
            Ddo_lotearquivoanexo_data_Cleanfilter = cgiGet( "DDO_LOTEARQUIVOANEXO_DATA_Cleanfilter");
            Ddo_lotearquivoanexo_data_Rangefilterfrom = cgiGet( "DDO_LOTEARQUIVOANEXO_DATA_Rangefilterfrom");
            Ddo_lotearquivoanexo_data_Rangefilterto = cgiGet( "DDO_LOTEARQUIVOANEXO_DATA_Rangefilterto");
            Ddo_lotearquivoanexo_data_Noresultsfound = cgiGet( "DDO_LOTEARQUIVOANEXO_DATA_Noresultsfound");
            Ddo_lotearquivoanexo_data_Searchbuttontext = cgiGet( "DDO_LOTEARQUIVOANEXO_DATA_Searchbuttontext");
            Ddo_lotearquivoanexo_nomearq_Caption = cgiGet( "DDO_LOTEARQUIVOANEXO_NOMEARQ_Caption");
            Ddo_lotearquivoanexo_nomearq_Tooltip = cgiGet( "DDO_LOTEARQUIVOANEXO_NOMEARQ_Tooltip");
            Ddo_lotearquivoanexo_nomearq_Cls = cgiGet( "DDO_LOTEARQUIVOANEXO_NOMEARQ_Cls");
            Ddo_lotearquivoanexo_nomearq_Filteredtext_set = cgiGet( "DDO_LOTEARQUIVOANEXO_NOMEARQ_Filteredtext_set");
            Ddo_lotearquivoanexo_nomearq_Selectedvalue_set = cgiGet( "DDO_LOTEARQUIVOANEXO_NOMEARQ_Selectedvalue_set");
            Ddo_lotearquivoanexo_nomearq_Dropdownoptionstype = cgiGet( "DDO_LOTEARQUIVOANEXO_NOMEARQ_Dropdownoptionstype");
            Ddo_lotearquivoanexo_nomearq_Titlecontrolidtoreplace = cgiGet( "DDO_LOTEARQUIVOANEXO_NOMEARQ_Titlecontrolidtoreplace");
            Ddo_lotearquivoanexo_nomearq_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_LOTEARQUIVOANEXO_NOMEARQ_Includesortasc"));
            Ddo_lotearquivoanexo_nomearq_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_LOTEARQUIVOANEXO_NOMEARQ_Includesortdsc"));
            Ddo_lotearquivoanexo_nomearq_Sortedstatus = cgiGet( "DDO_LOTEARQUIVOANEXO_NOMEARQ_Sortedstatus");
            Ddo_lotearquivoanexo_nomearq_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_LOTEARQUIVOANEXO_NOMEARQ_Includefilter"));
            Ddo_lotearquivoanexo_nomearq_Filtertype = cgiGet( "DDO_LOTEARQUIVOANEXO_NOMEARQ_Filtertype");
            Ddo_lotearquivoanexo_nomearq_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_LOTEARQUIVOANEXO_NOMEARQ_Filterisrange"));
            Ddo_lotearquivoanexo_nomearq_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_LOTEARQUIVOANEXO_NOMEARQ_Includedatalist"));
            Ddo_lotearquivoanexo_nomearq_Datalisttype = cgiGet( "DDO_LOTEARQUIVOANEXO_NOMEARQ_Datalisttype");
            Ddo_lotearquivoanexo_nomearq_Datalistfixedvalues = cgiGet( "DDO_LOTEARQUIVOANEXO_NOMEARQ_Datalistfixedvalues");
            Ddo_lotearquivoanexo_nomearq_Datalistproc = cgiGet( "DDO_LOTEARQUIVOANEXO_NOMEARQ_Datalistproc");
            Ddo_lotearquivoanexo_nomearq_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_LOTEARQUIVOANEXO_NOMEARQ_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_lotearquivoanexo_nomearq_Sortasc = cgiGet( "DDO_LOTEARQUIVOANEXO_NOMEARQ_Sortasc");
            Ddo_lotearquivoanexo_nomearq_Sortdsc = cgiGet( "DDO_LOTEARQUIVOANEXO_NOMEARQ_Sortdsc");
            Ddo_lotearquivoanexo_nomearq_Loadingdata = cgiGet( "DDO_LOTEARQUIVOANEXO_NOMEARQ_Loadingdata");
            Ddo_lotearquivoanexo_nomearq_Cleanfilter = cgiGet( "DDO_LOTEARQUIVOANEXO_NOMEARQ_Cleanfilter");
            Ddo_lotearquivoanexo_nomearq_Rangefilterfrom = cgiGet( "DDO_LOTEARQUIVOANEXO_NOMEARQ_Rangefilterfrom");
            Ddo_lotearquivoanexo_nomearq_Rangefilterto = cgiGet( "DDO_LOTEARQUIVOANEXO_NOMEARQ_Rangefilterto");
            Ddo_lotearquivoanexo_nomearq_Noresultsfound = cgiGet( "DDO_LOTEARQUIVOANEXO_NOMEARQ_Noresultsfound");
            Ddo_lotearquivoanexo_nomearq_Searchbuttontext = cgiGet( "DDO_LOTEARQUIVOANEXO_NOMEARQ_Searchbuttontext");
            Ddo_lotearquivoanexo_tipoarq_Caption = cgiGet( "DDO_LOTEARQUIVOANEXO_TIPOARQ_Caption");
            Ddo_lotearquivoanexo_tipoarq_Tooltip = cgiGet( "DDO_LOTEARQUIVOANEXO_TIPOARQ_Tooltip");
            Ddo_lotearquivoanexo_tipoarq_Cls = cgiGet( "DDO_LOTEARQUIVOANEXO_TIPOARQ_Cls");
            Ddo_lotearquivoanexo_tipoarq_Filteredtext_set = cgiGet( "DDO_LOTEARQUIVOANEXO_TIPOARQ_Filteredtext_set");
            Ddo_lotearquivoanexo_tipoarq_Selectedvalue_set = cgiGet( "DDO_LOTEARQUIVOANEXO_TIPOARQ_Selectedvalue_set");
            Ddo_lotearquivoanexo_tipoarq_Dropdownoptionstype = cgiGet( "DDO_LOTEARQUIVOANEXO_TIPOARQ_Dropdownoptionstype");
            Ddo_lotearquivoanexo_tipoarq_Titlecontrolidtoreplace = cgiGet( "DDO_LOTEARQUIVOANEXO_TIPOARQ_Titlecontrolidtoreplace");
            Ddo_lotearquivoanexo_tipoarq_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_LOTEARQUIVOANEXO_TIPOARQ_Includesortasc"));
            Ddo_lotearquivoanexo_tipoarq_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_LOTEARQUIVOANEXO_TIPOARQ_Includesortdsc"));
            Ddo_lotearquivoanexo_tipoarq_Sortedstatus = cgiGet( "DDO_LOTEARQUIVOANEXO_TIPOARQ_Sortedstatus");
            Ddo_lotearquivoanexo_tipoarq_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_LOTEARQUIVOANEXO_TIPOARQ_Includefilter"));
            Ddo_lotearquivoanexo_tipoarq_Filtertype = cgiGet( "DDO_LOTEARQUIVOANEXO_TIPOARQ_Filtertype");
            Ddo_lotearquivoanexo_tipoarq_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_LOTEARQUIVOANEXO_TIPOARQ_Filterisrange"));
            Ddo_lotearquivoanexo_tipoarq_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_LOTEARQUIVOANEXO_TIPOARQ_Includedatalist"));
            Ddo_lotearquivoanexo_tipoarq_Datalisttype = cgiGet( "DDO_LOTEARQUIVOANEXO_TIPOARQ_Datalisttype");
            Ddo_lotearquivoanexo_tipoarq_Datalistfixedvalues = cgiGet( "DDO_LOTEARQUIVOANEXO_TIPOARQ_Datalistfixedvalues");
            Ddo_lotearquivoanexo_tipoarq_Datalistproc = cgiGet( "DDO_LOTEARQUIVOANEXO_TIPOARQ_Datalistproc");
            Ddo_lotearquivoanexo_tipoarq_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_LOTEARQUIVOANEXO_TIPOARQ_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_lotearquivoanexo_tipoarq_Sortasc = cgiGet( "DDO_LOTEARQUIVOANEXO_TIPOARQ_Sortasc");
            Ddo_lotearquivoanexo_tipoarq_Sortdsc = cgiGet( "DDO_LOTEARQUIVOANEXO_TIPOARQ_Sortdsc");
            Ddo_lotearquivoanexo_tipoarq_Loadingdata = cgiGet( "DDO_LOTEARQUIVOANEXO_TIPOARQ_Loadingdata");
            Ddo_lotearquivoanexo_tipoarq_Cleanfilter = cgiGet( "DDO_LOTEARQUIVOANEXO_TIPOARQ_Cleanfilter");
            Ddo_lotearquivoanexo_tipoarq_Rangefilterfrom = cgiGet( "DDO_LOTEARQUIVOANEXO_TIPOARQ_Rangefilterfrom");
            Ddo_lotearquivoanexo_tipoarq_Rangefilterto = cgiGet( "DDO_LOTEARQUIVOANEXO_TIPOARQ_Rangefilterto");
            Ddo_lotearquivoanexo_tipoarq_Noresultsfound = cgiGet( "DDO_LOTEARQUIVOANEXO_TIPOARQ_Noresultsfound");
            Ddo_lotearquivoanexo_tipoarq_Searchbuttontext = cgiGet( "DDO_LOTEARQUIVOANEXO_TIPOARQ_Searchbuttontext");
            Ddo_lotearquivoanexo_descricao_Caption = cgiGet( "DDO_LOTEARQUIVOANEXO_DESCRICAO_Caption");
            Ddo_lotearquivoanexo_descricao_Tooltip = cgiGet( "DDO_LOTEARQUIVOANEXO_DESCRICAO_Tooltip");
            Ddo_lotearquivoanexo_descricao_Cls = cgiGet( "DDO_LOTEARQUIVOANEXO_DESCRICAO_Cls");
            Ddo_lotearquivoanexo_descricao_Filteredtext_set = cgiGet( "DDO_LOTEARQUIVOANEXO_DESCRICAO_Filteredtext_set");
            Ddo_lotearquivoanexo_descricao_Selectedvalue_set = cgiGet( "DDO_LOTEARQUIVOANEXO_DESCRICAO_Selectedvalue_set");
            Ddo_lotearquivoanexo_descricao_Dropdownoptionstype = cgiGet( "DDO_LOTEARQUIVOANEXO_DESCRICAO_Dropdownoptionstype");
            Ddo_lotearquivoanexo_descricao_Titlecontrolidtoreplace = cgiGet( "DDO_LOTEARQUIVOANEXO_DESCRICAO_Titlecontrolidtoreplace");
            Ddo_lotearquivoanexo_descricao_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_LOTEARQUIVOANEXO_DESCRICAO_Includesortasc"));
            Ddo_lotearquivoanexo_descricao_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_LOTEARQUIVOANEXO_DESCRICAO_Includesortdsc"));
            Ddo_lotearquivoanexo_descricao_Sortedstatus = cgiGet( "DDO_LOTEARQUIVOANEXO_DESCRICAO_Sortedstatus");
            Ddo_lotearquivoanexo_descricao_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_LOTEARQUIVOANEXO_DESCRICAO_Includefilter"));
            Ddo_lotearquivoanexo_descricao_Filtertype = cgiGet( "DDO_LOTEARQUIVOANEXO_DESCRICAO_Filtertype");
            Ddo_lotearquivoanexo_descricao_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_LOTEARQUIVOANEXO_DESCRICAO_Filterisrange"));
            Ddo_lotearquivoanexo_descricao_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_LOTEARQUIVOANEXO_DESCRICAO_Includedatalist"));
            Ddo_lotearquivoanexo_descricao_Datalisttype = cgiGet( "DDO_LOTEARQUIVOANEXO_DESCRICAO_Datalisttype");
            Ddo_lotearquivoanexo_descricao_Datalistfixedvalues = cgiGet( "DDO_LOTEARQUIVOANEXO_DESCRICAO_Datalistfixedvalues");
            Ddo_lotearquivoanexo_descricao_Datalistproc = cgiGet( "DDO_LOTEARQUIVOANEXO_DESCRICAO_Datalistproc");
            Ddo_lotearquivoanexo_descricao_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_LOTEARQUIVOANEXO_DESCRICAO_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_lotearquivoanexo_descricao_Sortasc = cgiGet( "DDO_LOTEARQUIVOANEXO_DESCRICAO_Sortasc");
            Ddo_lotearquivoanexo_descricao_Sortdsc = cgiGet( "DDO_LOTEARQUIVOANEXO_DESCRICAO_Sortdsc");
            Ddo_lotearquivoanexo_descricao_Loadingdata = cgiGet( "DDO_LOTEARQUIVOANEXO_DESCRICAO_Loadingdata");
            Ddo_lotearquivoanexo_descricao_Cleanfilter = cgiGet( "DDO_LOTEARQUIVOANEXO_DESCRICAO_Cleanfilter");
            Ddo_lotearquivoanexo_descricao_Rangefilterfrom = cgiGet( "DDO_LOTEARQUIVOANEXO_DESCRICAO_Rangefilterfrom");
            Ddo_lotearquivoanexo_descricao_Rangefilterto = cgiGet( "DDO_LOTEARQUIVOANEXO_DESCRICAO_Rangefilterto");
            Ddo_lotearquivoanexo_descricao_Noresultsfound = cgiGet( "DDO_LOTEARQUIVOANEXO_DESCRICAO_Noresultsfound");
            Ddo_lotearquivoanexo_descricao_Searchbuttontext = cgiGet( "DDO_LOTEARQUIVOANEXO_DESCRICAO_Searchbuttontext");
            Ddo_tipodocumento_nome_Caption = cgiGet( "DDO_TIPODOCUMENTO_NOME_Caption");
            Ddo_tipodocumento_nome_Tooltip = cgiGet( "DDO_TIPODOCUMENTO_NOME_Tooltip");
            Ddo_tipodocumento_nome_Cls = cgiGet( "DDO_TIPODOCUMENTO_NOME_Cls");
            Ddo_tipodocumento_nome_Filteredtext_set = cgiGet( "DDO_TIPODOCUMENTO_NOME_Filteredtext_set");
            Ddo_tipodocumento_nome_Selectedvalue_set = cgiGet( "DDO_TIPODOCUMENTO_NOME_Selectedvalue_set");
            Ddo_tipodocumento_nome_Dropdownoptionstype = cgiGet( "DDO_TIPODOCUMENTO_NOME_Dropdownoptionstype");
            Ddo_tipodocumento_nome_Titlecontrolidtoreplace = cgiGet( "DDO_TIPODOCUMENTO_NOME_Titlecontrolidtoreplace");
            Ddo_tipodocumento_nome_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_TIPODOCUMENTO_NOME_Includesortasc"));
            Ddo_tipodocumento_nome_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_TIPODOCUMENTO_NOME_Includesortdsc"));
            Ddo_tipodocumento_nome_Sortedstatus = cgiGet( "DDO_TIPODOCUMENTO_NOME_Sortedstatus");
            Ddo_tipodocumento_nome_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_TIPODOCUMENTO_NOME_Includefilter"));
            Ddo_tipodocumento_nome_Filtertype = cgiGet( "DDO_TIPODOCUMENTO_NOME_Filtertype");
            Ddo_tipodocumento_nome_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_TIPODOCUMENTO_NOME_Filterisrange"));
            Ddo_tipodocumento_nome_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_TIPODOCUMENTO_NOME_Includedatalist"));
            Ddo_tipodocumento_nome_Datalisttype = cgiGet( "DDO_TIPODOCUMENTO_NOME_Datalisttype");
            Ddo_tipodocumento_nome_Datalistfixedvalues = cgiGet( "DDO_TIPODOCUMENTO_NOME_Datalistfixedvalues");
            Ddo_tipodocumento_nome_Datalistproc = cgiGet( "DDO_TIPODOCUMENTO_NOME_Datalistproc");
            Ddo_tipodocumento_nome_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_TIPODOCUMENTO_NOME_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_tipodocumento_nome_Sortasc = cgiGet( "DDO_TIPODOCUMENTO_NOME_Sortasc");
            Ddo_tipodocumento_nome_Sortdsc = cgiGet( "DDO_TIPODOCUMENTO_NOME_Sortdsc");
            Ddo_tipodocumento_nome_Loadingdata = cgiGet( "DDO_TIPODOCUMENTO_NOME_Loadingdata");
            Ddo_tipodocumento_nome_Cleanfilter = cgiGet( "DDO_TIPODOCUMENTO_NOME_Cleanfilter");
            Ddo_tipodocumento_nome_Rangefilterfrom = cgiGet( "DDO_TIPODOCUMENTO_NOME_Rangefilterfrom");
            Ddo_tipodocumento_nome_Rangefilterto = cgiGet( "DDO_TIPODOCUMENTO_NOME_Rangefilterto");
            Ddo_tipodocumento_nome_Noresultsfound = cgiGet( "DDO_TIPODOCUMENTO_NOME_Noresultsfound");
            Ddo_tipodocumento_nome_Searchbuttontext = cgiGet( "DDO_TIPODOCUMENTO_NOME_Searchbuttontext");
            Gridpaginationbar_Selectedpage = cgiGet( "GRIDPAGINATIONBAR_Selectedpage");
            Ddo_lotearquivoanexo_lotecod_Activeeventkey = cgiGet( "DDO_LOTEARQUIVOANEXO_LOTECOD_Activeeventkey");
            Ddo_lotearquivoanexo_lotecod_Filteredtext_get = cgiGet( "DDO_LOTEARQUIVOANEXO_LOTECOD_Filteredtext_get");
            Ddo_lotearquivoanexo_lotecod_Filteredtextto_get = cgiGet( "DDO_LOTEARQUIVOANEXO_LOTECOD_Filteredtextto_get");
            Ddo_lotearquivoanexo_data_Activeeventkey = cgiGet( "DDO_LOTEARQUIVOANEXO_DATA_Activeeventkey");
            Ddo_lotearquivoanexo_data_Filteredtext_get = cgiGet( "DDO_LOTEARQUIVOANEXO_DATA_Filteredtext_get");
            Ddo_lotearquivoanexo_data_Filteredtextto_get = cgiGet( "DDO_LOTEARQUIVOANEXO_DATA_Filteredtextto_get");
            Ddo_lotearquivoanexo_nomearq_Activeeventkey = cgiGet( "DDO_LOTEARQUIVOANEXO_NOMEARQ_Activeeventkey");
            Ddo_lotearquivoanexo_nomearq_Filteredtext_get = cgiGet( "DDO_LOTEARQUIVOANEXO_NOMEARQ_Filteredtext_get");
            Ddo_lotearquivoanexo_nomearq_Selectedvalue_get = cgiGet( "DDO_LOTEARQUIVOANEXO_NOMEARQ_Selectedvalue_get");
            Ddo_lotearquivoanexo_tipoarq_Activeeventkey = cgiGet( "DDO_LOTEARQUIVOANEXO_TIPOARQ_Activeeventkey");
            Ddo_lotearquivoanexo_tipoarq_Filteredtext_get = cgiGet( "DDO_LOTEARQUIVOANEXO_TIPOARQ_Filteredtext_get");
            Ddo_lotearquivoanexo_tipoarq_Selectedvalue_get = cgiGet( "DDO_LOTEARQUIVOANEXO_TIPOARQ_Selectedvalue_get");
            Ddo_lotearquivoanexo_descricao_Activeeventkey = cgiGet( "DDO_LOTEARQUIVOANEXO_DESCRICAO_Activeeventkey");
            Ddo_lotearquivoanexo_descricao_Filteredtext_get = cgiGet( "DDO_LOTEARQUIVOANEXO_DESCRICAO_Filteredtext_get");
            Ddo_lotearquivoanexo_descricao_Selectedvalue_get = cgiGet( "DDO_LOTEARQUIVOANEXO_DESCRICAO_Selectedvalue_get");
            Ddo_tipodocumento_nome_Activeeventkey = cgiGet( "DDO_TIPODOCUMENTO_NOME_Activeeventkey");
            Ddo_tipodocumento_nome_Filteredtext_get = cgiGet( "DDO_TIPODOCUMENTO_NOME_Filteredtext_get");
            Ddo_tipodocumento_nome_Selectedvalue_get = cgiGet( "DDO_TIPODOCUMENTO_NOME_Selectedvalue_get");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV14OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV15OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV16DynamicFiltersSelector1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR1"), ",", ".") != Convert.ToDecimal( AV17DynamicFiltersOperator1 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vLOTEARQUIVOANEXO_NOMEARQ1"), AV18LoteArquivoAnexo_NomeArq1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTIPODOCUMENTO_NOME1"), AV19TipoDocumento_Nome1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV21DynamicFiltersSelector2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR2"), ",", ".") != Convert.ToDecimal( AV22DynamicFiltersOperator2 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vLOTEARQUIVOANEXO_NOMEARQ2"), AV23LoteArquivoAnexo_NomeArq2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTIPODOCUMENTO_NOME2"), AV24TipoDocumento_Nome2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV26DynamicFiltersSelector3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vDYNAMICFILTERSOPERATOR3"), ",", ".") != Convert.ToDecimal( AV27DynamicFiltersOperator3 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vLOTEARQUIVOANEXO_NOMEARQ3"), AV28LoteArquivoAnexo_NomeArq3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTIPODOCUMENTO_NOME3"), AV29TipoDocumento_Nome3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV20DynamicFiltersEnabled2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV25DynamicFiltersEnabled3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFLOTEARQUIVOANEXO_LOTECOD"), ",", ".") != Convert.ToDecimal( AV35TFLoteArquivoAnexo_LoteCod )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFLOTEARQUIVOANEXO_LOTECOD_TO"), ",", ".") != Convert.ToDecimal( AV36TFLoteArquivoAnexo_LoteCod_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vTFLOTEARQUIVOANEXO_DATA"), 0) != AV39TFLoteArquivoAnexo_Data )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vTFLOTEARQUIVOANEXO_DATA_TO"), 0) != AV40TFLoteArquivoAnexo_Data_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFLOTEARQUIVOANEXO_NOMEARQ"), AV45TFLoteArquivoAnexo_NomeArq) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFLOTEARQUIVOANEXO_NOMEARQ_SEL"), AV46TFLoteArquivoAnexo_NomeArq_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFLOTEARQUIVOANEXO_TIPOARQ"), AV49TFLoteArquivoAnexo_TipoArq) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFLOTEARQUIVOANEXO_TIPOARQ_SEL"), AV50TFLoteArquivoAnexo_TipoArq_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFLOTEARQUIVOANEXO_DESCRICAO"), AV53TFLoteArquivoAnexo_Descricao) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFLOTEARQUIVOANEXO_DESCRICAO_SEL"), AV54TFLoteArquivoAnexo_Descricao_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFTIPODOCUMENTO_NOME"), AV57TFTipoDocumento_Nome) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFTIPODOCUMENTO_NOME_SEL"), AV58TFTipoDocumento_Nome_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E28EX2 */
         E28EX2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E28EX2( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         lblJsdynamicfilters_Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         chkavDynamicfiltersenabled2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled2.Visible), 5, 0)));
         chkavDynamicfiltersenabled3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled3.Visible), 5, 0)));
         AV16DynamicFiltersSelector1 = "LOTEARQUIVOANEXO_NOMEARQ";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV21DynamicFiltersSelector2 = "LOTEARQUIVOANEXO_NOMEARQ";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersSelector3 = "LOTEARQUIVOANEXO_NOMEARQ";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersSelector3", AV26DynamicFiltersSelector3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         imgAdddynamicfilters1_Jsonclick = "WWPDynFilterShow(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Jsonclick", imgAdddynamicfilters1_Jsonclick);
         imgRemovedynamicfilters1_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Jsonclick", imgRemovedynamicfilters1_Jsonclick);
         imgAdddynamicfilters2_Jsonclick = "WWPDynFilterShow(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Jsonclick", imgAdddynamicfilters2_Jsonclick);
         imgRemovedynamicfilters2_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Jsonclick", imgRemovedynamicfilters2_Jsonclick);
         imgRemovedynamicfilters3_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters3_Internalname, "Jsonclick", imgRemovedynamicfilters3_Jsonclick);
         edtavTflotearquivoanexo_lotecod_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTflotearquivoanexo_lotecod_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTflotearquivoanexo_lotecod_Visible), 5, 0)));
         edtavTflotearquivoanexo_lotecod_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTflotearquivoanexo_lotecod_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTflotearquivoanexo_lotecod_to_Visible), 5, 0)));
         edtavTflotearquivoanexo_data_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTflotearquivoanexo_data_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTflotearquivoanexo_data_Visible), 5, 0)));
         edtavTflotearquivoanexo_data_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTflotearquivoanexo_data_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTflotearquivoanexo_data_to_Visible), 5, 0)));
         edtavTflotearquivoanexo_nomearq_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTflotearquivoanexo_nomearq_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTflotearquivoanexo_nomearq_Visible), 5, 0)));
         edtavTflotearquivoanexo_nomearq_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTflotearquivoanexo_nomearq_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTflotearquivoanexo_nomearq_sel_Visible), 5, 0)));
         edtavTflotearquivoanexo_tipoarq_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTflotearquivoanexo_tipoarq_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTflotearquivoanexo_tipoarq_Visible), 5, 0)));
         edtavTflotearquivoanexo_tipoarq_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTflotearquivoanexo_tipoarq_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTflotearquivoanexo_tipoarq_sel_Visible), 5, 0)));
         edtavTflotearquivoanexo_descricao_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTflotearquivoanexo_descricao_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTflotearquivoanexo_descricao_Visible), 5, 0)));
         edtavTflotearquivoanexo_descricao_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTflotearquivoanexo_descricao_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTflotearquivoanexo_descricao_sel_Visible), 5, 0)));
         edtavTftipodocumento_nome_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTftipodocumento_nome_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTftipodocumento_nome_Visible), 5, 0)));
         edtavTftipodocumento_nome_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTftipodocumento_nome_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTftipodocumento_nome_sel_Visible), 5, 0)));
         Ddo_lotearquivoanexo_lotecod_Titlecontrolidtoreplace = subGrid_Internalname+"_LoteArquivoAnexo_LoteCod";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_lotearquivoanexo_lotecod_Internalname, "TitleControlIdToReplace", Ddo_lotearquivoanexo_lotecod_Titlecontrolidtoreplace);
         AV37ddo_LoteArquivoAnexo_LoteCodTitleControlIdToReplace = Ddo_lotearquivoanexo_lotecod_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37ddo_LoteArquivoAnexo_LoteCodTitleControlIdToReplace", AV37ddo_LoteArquivoAnexo_LoteCodTitleControlIdToReplace);
         edtavDdo_lotearquivoanexo_lotecodtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_lotearquivoanexo_lotecodtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_lotearquivoanexo_lotecodtitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_lotearquivoanexo_data_Titlecontrolidtoreplace = subGrid_Internalname+"_LoteArquivoAnexo_Data";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_lotearquivoanexo_data_Internalname, "TitleControlIdToReplace", Ddo_lotearquivoanexo_data_Titlecontrolidtoreplace);
         AV43ddo_LoteArquivoAnexo_DataTitleControlIdToReplace = Ddo_lotearquivoanexo_data_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43ddo_LoteArquivoAnexo_DataTitleControlIdToReplace", AV43ddo_LoteArquivoAnexo_DataTitleControlIdToReplace);
         edtavDdo_lotearquivoanexo_datatitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_lotearquivoanexo_datatitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_lotearquivoanexo_datatitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_lotearquivoanexo_nomearq_Titlecontrolidtoreplace = subGrid_Internalname+"_LoteArquivoAnexo_NomeArq";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_lotearquivoanexo_nomearq_Internalname, "TitleControlIdToReplace", Ddo_lotearquivoanexo_nomearq_Titlecontrolidtoreplace);
         AV47ddo_LoteArquivoAnexo_NomeArqTitleControlIdToReplace = Ddo_lotearquivoanexo_nomearq_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47ddo_LoteArquivoAnexo_NomeArqTitleControlIdToReplace", AV47ddo_LoteArquivoAnexo_NomeArqTitleControlIdToReplace);
         edtavDdo_lotearquivoanexo_nomearqtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_lotearquivoanexo_nomearqtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_lotearquivoanexo_nomearqtitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_lotearquivoanexo_tipoarq_Titlecontrolidtoreplace = subGrid_Internalname+"_LoteArquivoAnexo_TipoArq";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_lotearquivoanexo_tipoarq_Internalname, "TitleControlIdToReplace", Ddo_lotearquivoanexo_tipoarq_Titlecontrolidtoreplace);
         AV51ddo_LoteArquivoAnexo_TipoArqTitleControlIdToReplace = Ddo_lotearquivoanexo_tipoarq_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51ddo_LoteArquivoAnexo_TipoArqTitleControlIdToReplace", AV51ddo_LoteArquivoAnexo_TipoArqTitleControlIdToReplace);
         edtavDdo_lotearquivoanexo_tipoarqtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_lotearquivoanexo_tipoarqtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_lotearquivoanexo_tipoarqtitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_lotearquivoanexo_descricao_Titlecontrolidtoreplace = subGrid_Internalname+"_LoteArquivoAnexo_Descricao";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_lotearquivoanexo_descricao_Internalname, "TitleControlIdToReplace", Ddo_lotearquivoanexo_descricao_Titlecontrolidtoreplace);
         AV55ddo_LoteArquivoAnexo_DescricaoTitleControlIdToReplace = Ddo_lotearquivoanexo_descricao_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55ddo_LoteArquivoAnexo_DescricaoTitleControlIdToReplace", AV55ddo_LoteArquivoAnexo_DescricaoTitleControlIdToReplace);
         edtavDdo_lotearquivoanexo_descricaotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_lotearquivoanexo_descricaotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_lotearquivoanexo_descricaotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_tipodocumento_nome_Titlecontrolidtoreplace = subGrid_Internalname+"_TipoDocumento_Nome";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tipodocumento_nome_Internalname, "TitleControlIdToReplace", Ddo_tipodocumento_nome_Titlecontrolidtoreplace);
         AV59ddo_TipoDocumento_NomeTitleControlIdToReplace = Ddo_tipodocumento_nome_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59ddo_TipoDocumento_NomeTitleControlIdToReplace", AV59ddo_TipoDocumento_NomeTitleControlIdToReplace);
         edtavDdo_tipodocumento_nometitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_tipodocumento_nometitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_tipodocumento_nometitlecontrolidtoreplace_Visible), 5, 0)));
         Form.Caption = "Selecione Arquivos Anexos";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavOrderedby.removeAllItems();
         cmbavOrderedby.addItem("1", "do Arquivo", 0);
         cmbavOrderedby.addItem("2", "Anexo_Lote Cod", 0);
         cmbavOrderedby.addItem("3", "Hora Upload", 0);
         cmbavOrderedby.addItem("4", "de Arquivo", 0);
         cmbavOrderedby.addItem("5", "Descri��o", 0);
         cmbavOrderedby.addItem("6", "Nome", 0);
         if ( AV14OrderedBy < 1 )
         {
            AV14OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         imgCleanfilters_Jsonclick = "WWPDynFilterHideAll(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgCleanfilters_Internalname, "Jsonclick", imgCleanfilters_Jsonclick);
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = AV60DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1) ;
         AV60DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1;
      }

      protected void E29EX2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV34LoteArquivoAnexo_LoteCodTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV38LoteArquivoAnexo_DataTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV44LoteArquivoAnexo_NomeArqTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV48LoteArquivoAnexo_TipoArqTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV52LoteArquivoAnexo_DescricaoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV56TipoDocumento_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         cmbavDynamicfiltersoperator1.removeAllItems();
         if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "LOTEARQUIVOANEXO_NOMEARQ") == 0 )
         {
            cmbavDynamicfiltersoperator1.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "Cont�m", 0);
         }
         else if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "TIPODOCUMENTO_NOME") == 0 )
         {
            cmbavDynamicfiltersoperator1.addItem("0", "Come�a com", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "Cont�m", 0);
         }
         if ( AV20DynamicFiltersEnabled2 )
         {
            cmbavDynamicfiltersoperator2.removeAllItems();
            if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "LOTEARQUIVOANEXO_NOMEARQ") == 0 )
            {
               cmbavDynamicfiltersoperator2.addItem("0", "Come�a com", 0);
               cmbavDynamicfiltersoperator2.addItem("1", "Cont�m", 0);
            }
            else if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "TIPODOCUMENTO_NOME") == 0 )
            {
               cmbavDynamicfiltersoperator2.addItem("0", "Come�a com", 0);
               cmbavDynamicfiltersoperator2.addItem("1", "Cont�m", 0);
            }
            if ( AV25DynamicFiltersEnabled3 )
            {
               cmbavDynamicfiltersoperator3.removeAllItems();
               if ( StringUtil.StrCmp(AV26DynamicFiltersSelector3, "LOTEARQUIVOANEXO_NOMEARQ") == 0 )
               {
                  cmbavDynamicfiltersoperator3.addItem("0", "Come�a com", 0);
                  cmbavDynamicfiltersoperator3.addItem("1", "Cont�m", 0);
               }
               else if ( StringUtil.StrCmp(AV26DynamicFiltersSelector3, "TIPODOCUMENTO_NOME") == 0 )
               {
                  cmbavDynamicfiltersoperator3.addItem("0", "Come�a com", 0);
                  cmbavDynamicfiltersoperator3.addItem("1", "Cont�m", 0);
               }
            }
         }
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtLoteArquivoAnexo_LoteCod_Titleformat = 2;
         edtLoteArquivoAnexo_LoteCod_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Anexo_Lote Cod", AV37ddo_LoteArquivoAnexo_LoteCodTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLoteArquivoAnexo_LoteCod_Internalname, "Title", edtLoteArquivoAnexo_LoteCod_Title);
         edtLoteArquivoAnexo_Data_Titleformat = 2;
         edtLoteArquivoAnexo_Data_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Hora Upload", AV43ddo_LoteArquivoAnexo_DataTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLoteArquivoAnexo_Data_Internalname, "Title", edtLoteArquivoAnexo_Data_Title);
         edtLoteArquivoAnexo_NomeArq_Titleformat = 2;
         edtLoteArquivoAnexo_NomeArq_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "do Arquivo", AV47ddo_LoteArquivoAnexo_NomeArqTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLoteArquivoAnexo_NomeArq_Internalname, "Title", edtLoteArquivoAnexo_NomeArq_Title);
         edtLoteArquivoAnexo_TipoArq_Titleformat = 2;
         edtLoteArquivoAnexo_TipoArq_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "de Arquivo", AV51ddo_LoteArquivoAnexo_TipoArqTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLoteArquivoAnexo_TipoArq_Internalname, "Title", edtLoteArquivoAnexo_TipoArq_Title);
         edtLoteArquivoAnexo_Descricao_Titleformat = 2;
         edtLoteArquivoAnexo_Descricao_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Descri��o", AV55ddo_LoteArquivoAnexo_DescricaoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLoteArquivoAnexo_Descricao_Internalname, "Title", edtLoteArquivoAnexo_Descricao_Title);
         edtTipoDocumento_Nome_Titleformat = 2;
         edtTipoDocumento_Nome_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Nome", AV59ddo_TipoDocumento_NomeTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtTipoDocumento_Nome_Internalname, "Title", edtTipoDocumento_Nome_Title);
         AV62GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV62GridCurrentPage), 10, 0)));
         AV63GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV63GridPageCount), 10, 0)));
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV34LoteArquivoAnexo_LoteCodTitleFilterData", AV34LoteArquivoAnexo_LoteCodTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV38LoteArquivoAnexo_DataTitleFilterData", AV38LoteArquivoAnexo_DataTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV44LoteArquivoAnexo_NomeArqTitleFilterData", AV44LoteArquivoAnexo_NomeArqTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV48LoteArquivoAnexo_TipoArqTitleFilterData", AV48LoteArquivoAnexo_TipoArqTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV52LoteArquivoAnexo_DescricaoTitleFilterData", AV52LoteArquivoAnexo_DescricaoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV56TipoDocumento_NomeTitleFilterData", AV56TipoDocumento_NomeTitleFilterData);
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV27DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV11GridState", AV11GridState);
      }

      protected void E11EX2( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV61PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV61PageToGo) ;
         }
      }

      protected void E12EX2( )
      {
         /* Ddo_lotearquivoanexo_lotecod_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_lotearquivoanexo_lotecod_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_lotearquivoanexo_lotecod_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_lotearquivoanexo_lotecod_Internalname, "SortedStatus", Ddo_lotearquivoanexo_lotecod_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_lotearquivoanexo_lotecod_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_lotearquivoanexo_lotecod_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_lotearquivoanexo_lotecod_Internalname, "SortedStatus", Ddo_lotearquivoanexo_lotecod_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_lotearquivoanexo_lotecod_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV35TFLoteArquivoAnexo_LoteCod = (int)(NumberUtil.Val( Ddo_lotearquivoanexo_lotecod_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TFLoteArquivoAnexo_LoteCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35TFLoteArquivoAnexo_LoteCod), 6, 0)));
            AV36TFLoteArquivoAnexo_LoteCod_To = (int)(NumberUtil.Val( Ddo_lotearquivoanexo_lotecod_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36TFLoteArquivoAnexo_LoteCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36TFLoteArquivoAnexo_LoteCod_To), 6, 0)));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E13EX2( )
      {
         /* Ddo_lotearquivoanexo_data_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_lotearquivoanexo_data_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_lotearquivoanexo_data_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_lotearquivoanexo_data_Internalname, "SortedStatus", Ddo_lotearquivoanexo_data_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_lotearquivoanexo_data_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_lotearquivoanexo_data_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_lotearquivoanexo_data_Internalname, "SortedStatus", Ddo_lotearquivoanexo_data_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_lotearquivoanexo_data_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV39TFLoteArquivoAnexo_Data = context.localUtil.CToT( Ddo_lotearquivoanexo_data_Filteredtext_get, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFLoteArquivoAnexo_Data", context.localUtil.TToC( AV39TFLoteArquivoAnexo_Data, 8, 5, 0, 3, "/", ":", " "));
            AV40TFLoteArquivoAnexo_Data_To = context.localUtil.CToT( Ddo_lotearquivoanexo_data_Filteredtextto_get, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40TFLoteArquivoAnexo_Data_To", context.localUtil.TToC( AV40TFLoteArquivoAnexo_Data_To, 8, 5, 0, 3, "/", ":", " "));
            if ( ! (DateTime.MinValue==AV40TFLoteArquivoAnexo_Data_To) )
            {
               AV40TFLoteArquivoAnexo_Data_To = context.localUtil.YMDHMSToT( (short)(DateTimeUtil.Year( AV40TFLoteArquivoAnexo_Data_To)), (short)(DateTimeUtil.Month( AV40TFLoteArquivoAnexo_Data_To)), (short)(DateTimeUtil.Day( AV40TFLoteArquivoAnexo_Data_To)), 23, 59, 59);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40TFLoteArquivoAnexo_Data_To", context.localUtil.TToC( AV40TFLoteArquivoAnexo_Data_To, 8, 5, 0, 3, "/", ":", " "));
            }
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E14EX2( )
      {
         /* Ddo_lotearquivoanexo_nomearq_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_lotearquivoanexo_nomearq_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_lotearquivoanexo_nomearq_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_lotearquivoanexo_nomearq_Internalname, "SortedStatus", Ddo_lotearquivoanexo_nomearq_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_lotearquivoanexo_nomearq_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_lotearquivoanexo_nomearq_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_lotearquivoanexo_nomearq_Internalname, "SortedStatus", Ddo_lotearquivoanexo_nomearq_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_lotearquivoanexo_nomearq_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV45TFLoteArquivoAnexo_NomeArq = Ddo_lotearquivoanexo_nomearq_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45TFLoteArquivoAnexo_NomeArq", AV45TFLoteArquivoAnexo_NomeArq);
            AV46TFLoteArquivoAnexo_NomeArq_Sel = Ddo_lotearquivoanexo_nomearq_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46TFLoteArquivoAnexo_NomeArq_Sel", AV46TFLoteArquivoAnexo_NomeArq_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E15EX2( )
      {
         /* Ddo_lotearquivoanexo_tipoarq_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_lotearquivoanexo_tipoarq_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_lotearquivoanexo_tipoarq_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_lotearquivoanexo_tipoarq_Internalname, "SortedStatus", Ddo_lotearquivoanexo_tipoarq_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_lotearquivoanexo_tipoarq_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_lotearquivoanexo_tipoarq_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_lotearquivoanexo_tipoarq_Internalname, "SortedStatus", Ddo_lotearquivoanexo_tipoarq_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_lotearquivoanexo_tipoarq_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV49TFLoteArquivoAnexo_TipoArq = Ddo_lotearquivoanexo_tipoarq_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49TFLoteArquivoAnexo_TipoArq", AV49TFLoteArquivoAnexo_TipoArq);
            AV50TFLoteArquivoAnexo_TipoArq_Sel = Ddo_lotearquivoanexo_tipoarq_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50TFLoteArquivoAnexo_TipoArq_Sel", AV50TFLoteArquivoAnexo_TipoArq_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E16EX2( )
      {
         /* Ddo_lotearquivoanexo_descricao_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_lotearquivoanexo_descricao_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 5;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_lotearquivoanexo_descricao_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_lotearquivoanexo_descricao_Internalname, "SortedStatus", Ddo_lotearquivoanexo_descricao_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_lotearquivoanexo_descricao_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 5;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_lotearquivoanexo_descricao_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_lotearquivoanexo_descricao_Internalname, "SortedStatus", Ddo_lotearquivoanexo_descricao_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_lotearquivoanexo_descricao_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV53TFLoteArquivoAnexo_Descricao = Ddo_lotearquivoanexo_descricao_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53TFLoteArquivoAnexo_Descricao", AV53TFLoteArquivoAnexo_Descricao);
            AV54TFLoteArquivoAnexo_Descricao_Sel = Ddo_lotearquivoanexo_descricao_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54TFLoteArquivoAnexo_Descricao_Sel", AV54TFLoteArquivoAnexo_Descricao_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E17EX2( )
      {
         /* Ddo_tipodocumento_nome_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_tipodocumento_nome_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 6;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_tipodocumento_nome_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tipodocumento_nome_Internalname, "SortedStatus", Ddo_tipodocumento_nome_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_tipodocumento_nome_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV14OrderedBy = 6;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)));
            AV15OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15OrderedDsc", AV15OrderedDsc);
            Ddo_tipodocumento_nome_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tipodocumento_nome_Internalname, "SortedStatus", Ddo_tipodocumento_nome_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_tipodocumento_nome_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV57TFTipoDocumento_Nome = Ddo_tipodocumento_nome_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57TFTipoDocumento_Nome", AV57TFTipoDocumento_Nome);
            AV58TFTipoDocumento_Nome_Sel = Ddo_tipodocumento_nome_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58TFTipoDocumento_Nome_Sel", AV58TFTipoDocumento_Nome_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      private void E30EX2( )
      {
         /* Grid_Load Routine */
         AV32Select = context.GetImagePath( "3914535b-0c03-44c5-9538-906a99cdd2bc", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavSelect_Internalname, AV32Select);
         AV66Select_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "3914535b-0c03-44c5-9538-906a99cdd2bc", "", context.GetTheme( )));
         edtavSelect_Tooltiptext = "Selecionar";
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 83;
         }
         sendrow_832( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_83_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(83, GridRow);
         }
      }

      public void GXEnter( )
      {
         /* Execute user event: E31EX2 */
         E31EX2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E31EX2( )
      {
         /* Enter Routine */
         AV7InOutLoteArquivoAnexo_LoteCod = A841LoteArquivoAnexo_LoteCod;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InOutLoteArquivoAnexo_LoteCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7InOutLoteArquivoAnexo_LoteCod), 6, 0)));
         AV8InOutLoteArquivoAnexo_Data = A836LoteArquivoAnexo_Data;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8InOutLoteArquivoAnexo_Data", context.localUtil.TToC( AV8InOutLoteArquivoAnexo_Data, 8, 5, 0, 3, "/", ":", " "));
         AV9InOutLoteArquivoAnexo_NomeArq = A839LoteArquivoAnexo_NomeArq;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9InOutLoteArquivoAnexo_NomeArq", AV9InOutLoteArquivoAnexo_NomeArq);
         context.setWebReturnParms(new Object[] {(int)AV7InOutLoteArquivoAnexo_LoteCod,context.localUtil.Format( AV8InOutLoteArquivoAnexo_Data, "99/99/99 99:99"),(String)AV9InOutLoteArquivoAnexo_NomeArq});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void E18EX2( )
      {
         /* Orderedby_Click Routine */
         context.DoAjaxRefresh();
      }

      protected void E23EX2( )
      {
         /* 'AddDynamicFilters1' Routine */
         AV20DynamicFiltersEnabled2 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled2", AV20DynamicFiltersEnabled2);
         imgAdddynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18LoteArquivoAnexo_NomeArq1, AV19TipoDocumento_Nome1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23LoteArquivoAnexo_NomeArq2, AV24TipoDocumento_Nome2, AV26DynamicFiltersSelector3, AV27DynamicFiltersOperator3, AV28LoteArquivoAnexo_NomeArq3, AV29TipoDocumento_Nome3, AV20DynamicFiltersEnabled2, AV25DynamicFiltersEnabled3, AV35TFLoteArquivoAnexo_LoteCod, AV36TFLoteArquivoAnexo_LoteCod_To, AV39TFLoteArquivoAnexo_Data, AV40TFLoteArquivoAnexo_Data_To, AV45TFLoteArquivoAnexo_NomeArq, AV46TFLoteArquivoAnexo_NomeArq_Sel, AV49TFLoteArquivoAnexo_TipoArq, AV50TFLoteArquivoAnexo_TipoArq_Sel, AV53TFLoteArquivoAnexo_Descricao, AV54TFLoteArquivoAnexo_Descricao_Sel, AV57TFTipoDocumento_Nome, AV58TFTipoDocumento_Nome_Sel, AV37ddo_LoteArquivoAnexo_LoteCodTitleControlIdToReplace, AV43ddo_LoteArquivoAnexo_DataTitleControlIdToReplace, AV47ddo_LoteArquivoAnexo_NomeArqTitleControlIdToReplace, AV51ddo_LoteArquivoAnexo_TipoArqTitleControlIdToReplace, AV55ddo_LoteArquivoAnexo_DescricaoTitleControlIdToReplace, AV59ddo_TipoDocumento_NomeTitleControlIdToReplace, AV67Pgmname, AV11GridState, AV31DynamicFiltersIgnoreFirst, AV30DynamicFiltersRemoving) ;
      }

      protected void E19EX2( )
      {
         /* 'RemoveDynamicFilters1' Routine */
         AV30DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30DynamicFiltersRemoving", AV30DynamicFiltersRemoving);
         AV31DynamicFiltersIgnoreFirst = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31DynamicFiltersIgnoreFirst", AV31DynamicFiltersIgnoreFirst);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV30DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30DynamicFiltersRemoving", AV30DynamicFiltersRemoving);
         AV31DynamicFiltersIgnoreFirst = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31DynamicFiltersIgnoreFirst", AV31DynamicFiltersIgnoreFirst);
         gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18LoteArquivoAnexo_NomeArq1, AV19TipoDocumento_Nome1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23LoteArquivoAnexo_NomeArq2, AV24TipoDocumento_Nome2, AV26DynamicFiltersSelector3, AV27DynamicFiltersOperator3, AV28LoteArquivoAnexo_NomeArq3, AV29TipoDocumento_Nome3, AV20DynamicFiltersEnabled2, AV25DynamicFiltersEnabled3, AV35TFLoteArquivoAnexo_LoteCod, AV36TFLoteArquivoAnexo_LoteCod_To, AV39TFLoteArquivoAnexo_Data, AV40TFLoteArquivoAnexo_Data_To, AV45TFLoteArquivoAnexo_NomeArq, AV46TFLoteArquivoAnexo_NomeArq_Sel, AV49TFLoteArquivoAnexo_TipoArq, AV50TFLoteArquivoAnexo_TipoArq_Sel, AV53TFLoteArquivoAnexo_Descricao, AV54TFLoteArquivoAnexo_Descricao_Sel, AV57TFTipoDocumento_Nome, AV58TFTipoDocumento_Nome_Sel, AV37ddo_LoteArquivoAnexo_LoteCodTitleControlIdToReplace, AV43ddo_LoteArquivoAnexo_DataTitleControlIdToReplace, AV47ddo_LoteArquivoAnexo_NomeArqTitleControlIdToReplace, AV51ddo_LoteArquivoAnexo_TipoArqTitleControlIdToReplace, AV55ddo_LoteArquivoAnexo_DescricaoTitleControlIdToReplace, AV59ddo_TipoDocumento_NomeTitleControlIdToReplace, AV67Pgmname, AV11GridState, AV31DynamicFiltersIgnoreFirst, AV30DynamicFiltersRemoving) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV11GridState", AV11GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV26DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV27DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV16DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E24EX2( )
      {
         /* Dynamicfiltersselector1_Click Routine */
         AV17DynamicFiltersOperator1 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E25EX2( )
      {
         /* 'AddDynamicFilters2' Routine */
         AV25DynamicFiltersEnabled3 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersEnabled3", AV25DynamicFiltersEnabled3);
         imgAdddynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
         gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18LoteArquivoAnexo_NomeArq1, AV19TipoDocumento_Nome1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23LoteArquivoAnexo_NomeArq2, AV24TipoDocumento_Nome2, AV26DynamicFiltersSelector3, AV27DynamicFiltersOperator3, AV28LoteArquivoAnexo_NomeArq3, AV29TipoDocumento_Nome3, AV20DynamicFiltersEnabled2, AV25DynamicFiltersEnabled3, AV35TFLoteArquivoAnexo_LoteCod, AV36TFLoteArquivoAnexo_LoteCod_To, AV39TFLoteArquivoAnexo_Data, AV40TFLoteArquivoAnexo_Data_To, AV45TFLoteArquivoAnexo_NomeArq, AV46TFLoteArquivoAnexo_NomeArq_Sel, AV49TFLoteArquivoAnexo_TipoArq, AV50TFLoteArquivoAnexo_TipoArq_Sel, AV53TFLoteArquivoAnexo_Descricao, AV54TFLoteArquivoAnexo_Descricao_Sel, AV57TFTipoDocumento_Nome, AV58TFTipoDocumento_Nome_Sel, AV37ddo_LoteArquivoAnexo_LoteCodTitleControlIdToReplace, AV43ddo_LoteArquivoAnexo_DataTitleControlIdToReplace, AV47ddo_LoteArquivoAnexo_NomeArqTitleControlIdToReplace, AV51ddo_LoteArquivoAnexo_TipoArqTitleControlIdToReplace, AV55ddo_LoteArquivoAnexo_DescricaoTitleControlIdToReplace, AV59ddo_TipoDocumento_NomeTitleControlIdToReplace, AV67Pgmname, AV11GridState, AV31DynamicFiltersIgnoreFirst, AV30DynamicFiltersRemoving) ;
      }

      protected void E20EX2( )
      {
         /* 'RemoveDynamicFilters2' Routine */
         AV30DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30DynamicFiltersRemoving", AV30DynamicFiltersRemoving);
         AV20DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled2", AV20DynamicFiltersEnabled2);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV30DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30DynamicFiltersRemoving", AV30DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18LoteArquivoAnexo_NomeArq1, AV19TipoDocumento_Nome1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23LoteArquivoAnexo_NomeArq2, AV24TipoDocumento_Nome2, AV26DynamicFiltersSelector3, AV27DynamicFiltersOperator3, AV28LoteArquivoAnexo_NomeArq3, AV29TipoDocumento_Nome3, AV20DynamicFiltersEnabled2, AV25DynamicFiltersEnabled3, AV35TFLoteArquivoAnexo_LoteCod, AV36TFLoteArquivoAnexo_LoteCod_To, AV39TFLoteArquivoAnexo_Data, AV40TFLoteArquivoAnexo_Data_To, AV45TFLoteArquivoAnexo_NomeArq, AV46TFLoteArquivoAnexo_NomeArq_Sel, AV49TFLoteArquivoAnexo_TipoArq, AV50TFLoteArquivoAnexo_TipoArq_Sel, AV53TFLoteArquivoAnexo_Descricao, AV54TFLoteArquivoAnexo_Descricao_Sel, AV57TFTipoDocumento_Nome, AV58TFTipoDocumento_Nome_Sel, AV37ddo_LoteArquivoAnexo_LoteCodTitleControlIdToReplace, AV43ddo_LoteArquivoAnexo_DataTitleControlIdToReplace, AV47ddo_LoteArquivoAnexo_NomeArqTitleControlIdToReplace, AV51ddo_LoteArquivoAnexo_TipoArqTitleControlIdToReplace, AV55ddo_LoteArquivoAnexo_DescricaoTitleControlIdToReplace, AV59ddo_TipoDocumento_NomeTitleControlIdToReplace, AV67Pgmname, AV11GridState, AV31DynamicFiltersIgnoreFirst, AV30DynamicFiltersRemoving) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV11GridState", AV11GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV26DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV27DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV16DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E26EX2( )
      {
         /* Dynamicfiltersselector2_Click Routine */
         AV22DynamicFiltersOperator2 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
      }

      protected void E21EX2( )
      {
         /* 'RemoveDynamicFilters3' Routine */
         AV30DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30DynamicFiltersRemoving", AV30DynamicFiltersRemoving);
         AV25DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersEnabled3", AV25DynamicFiltersEnabled3);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV30DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30DynamicFiltersRemoving", AV30DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV14OrderedBy, AV15OrderedDsc, AV16DynamicFiltersSelector1, AV17DynamicFiltersOperator1, AV18LoteArquivoAnexo_NomeArq1, AV19TipoDocumento_Nome1, AV21DynamicFiltersSelector2, AV22DynamicFiltersOperator2, AV23LoteArquivoAnexo_NomeArq2, AV24TipoDocumento_Nome2, AV26DynamicFiltersSelector3, AV27DynamicFiltersOperator3, AV28LoteArquivoAnexo_NomeArq3, AV29TipoDocumento_Nome3, AV20DynamicFiltersEnabled2, AV25DynamicFiltersEnabled3, AV35TFLoteArquivoAnexo_LoteCod, AV36TFLoteArquivoAnexo_LoteCod_To, AV39TFLoteArquivoAnexo_Data, AV40TFLoteArquivoAnexo_Data_To, AV45TFLoteArquivoAnexo_NomeArq, AV46TFLoteArquivoAnexo_NomeArq_Sel, AV49TFLoteArquivoAnexo_TipoArq, AV50TFLoteArquivoAnexo_TipoArq_Sel, AV53TFLoteArquivoAnexo_Descricao, AV54TFLoteArquivoAnexo_Descricao_Sel, AV57TFTipoDocumento_Nome, AV58TFTipoDocumento_Nome_Sel, AV37ddo_LoteArquivoAnexo_LoteCodTitleControlIdToReplace, AV43ddo_LoteArquivoAnexo_DataTitleControlIdToReplace, AV47ddo_LoteArquivoAnexo_NomeArqTitleControlIdToReplace, AV51ddo_LoteArquivoAnexo_TipoArqTitleControlIdToReplace, AV55ddo_LoteArquivoAnexo_DescricaoTitleControlIdToReplace, AV59ddo_TipoDocumento_NomeTitleControlIdToReplace, AV67Pgmname, AV11GridState, AV31DynamicFiltersIgnoreFirst, AV30DynamicFiltersRemoving) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV11GridState", AV11GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV26DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV27DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV16DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
      }

      protected void E27EX2( )
      {
         /* Dynamicfiltersselector3_Click Routine */
         AV27DynamicFiltersOperator3 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV27DynamicFiltersOperator3), 4, 0)));
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV27DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
      }

      protected void E22EX2( )
      {
         /* 'DoCleanFilters' Routine */
         /* Execute user subroutine: 'CLEANFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_firstpage( ) ;
         context.DoAjaxRefresh();
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV16DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV11GridState", AV11GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV26DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV27DynamicFiltersOperator3), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", cmbavDynamicfiltersoperator3.ToJavascriptSource());
      }

      protected void S172( )
      {
         /* 'RESETDDOSORTEDSTATUS' Routine */
         Ddo_lotearquivoanexo_lotecod_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_lotearquivoanexo_lotecod_Internalname, "SortedStatus", Ddo_lotearquivoanexo_lotecod_Sortedstatus);
         Ddo_lotearquivoanexo_data_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_lotearquivoanexo_data_Internalname, "SortedStatus", Ddo_lotearquivoanexo_data_Sortedstatus);
         Ddo_lotearquivoanexo_nomearq_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_lotearquivoanexo_nomearq_Internalname, "SortedStatus", Ddo_lotearquivoanexo_nomearq_Sortedstatus);
         Ddo_lotearquivoanexo_tipoarq_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_lotearquivoanexo_tipoarq_Internalname, "SortedStatus", Ddo_lotearquivoanexo_tipoarq_Sortedstatus);
         Ddo_lotearquivoanexo_descricao_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_lotearquivoanexo_descricao_Internalname, "SortedStatus", Ddo_lotearquivoanexo_descricao_Sortedstatus);
         Ddo_tipodocumento_nome_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tipodocumento_nome_Internalname, "SortedStatus", Ddo_tipodocumento_nome_Sortedstatus);
      }

      protected void S152( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV14OrderedBy == 2 )
         {
            Ddo_lotearquivoanexo_lotecod_Sortedstatus = (AV15OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_lotearquivoanexo_lotecod_Internalname, "SortedStatus", Ddo_lotearquivoanexo_lotecod_Sortedstatus);
         }
         else if ( AV14OrderedBy == 3 )
         {
            Ddo_lotearquivoanexo_data_Sortedstatus = (AV15OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_lotearquivoanexo_data_Internalname, "SortedStatus", Ddo_lotearquivoanexo_data_Sortedstatus);
         }
         else if ( AV14OrderedBy == 1 )
         {
            Ddo_lotearquivoanexo_nomearq_Sortedstatus = (AV15OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_lotearquivoanexo_nomearq_Internalname, "SortedStatus", Ddo_lotearquivoanexo_nomearq_Sortedstatus);
         }
         else if ( AV14OrderedBy == 4 )
         {
            Ddo_lotearquivoanexo_tipoarq_Sortedstatus = (AV15OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_lotearquivoanexo_tipoarq_Internalname, "SortedStatus", Ddo_lotearquivoanexo_tipoarq_Sortedstatus);
         }
         else if ( AV14OrderedBy == 5 )
         {
            Ddo_lotearquivoanexo_descricao_Sortedstatus = (AV15OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_lotearquivoanexo_descricao_Internalname, "SortedStatus", Ddo_lotearquivoanexo_descricao_Sortedstatus);
         }
         else if ( AV14OrderedBy == 6 )
         {
            Ddo_tipodocumento_nome_Sortedstatus = (AV15OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tipodocumento_nome_Internalname, "SortedStatus", Ddo_tipodocumento_nome_Sortedstatus);
         }
      }

      protected void S112( )
      {
         /* 'ENABLEDYNAMICFILTERS1' Routine */
         edtavLotearquivoanexo_nomearq1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavLotearquivoanexo_nomearq1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavLotearquivoanexo_nomearq1_Visible), 5, 0)));
         edtavTipodocumento_nome1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTipodocumento_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTipodocumento_nome1_Visible), 5, 0)));
         cmbavDynamicfiltersoperator1.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "LOTEARQUIVOANEXO_NOMEARQ") == 0 )
         {
            edtavLotearquivoanexo_nomearq1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavLotearquivoanexo_nomearq1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavLotearquivoanexo_nomearq1_Visible), 5, 0)));
            cmbavDynamicfiltersoperator1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "TIPODOCUMENTO_NOME") == 0 )
         {
            edtavTipodocumento_nome1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTipodocumento_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTipodocumento_nome1_Visible), 5, 0)));
            cmbavDynamicfiltersoperator1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         }
      }

      protected void S122( )
      {
         /* 'ENABLEDYNAMICFILTERS2' Routine */
         edtavLotearquivoanexo_nomearq2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavLotearquivoanexo_nomearq2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavLotearquivoanexo_nomearq2_Visible), 5, 0)));
         edtavTipodocumento_nome2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTipodocumento_nome2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTipodocumento_nome2_Visible), 5, 0)));
         cmbavDynamicfiltersoperator2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "LOTEARQUIVOANEXO_NOMEARQ") == 0 )
         {
            edtavLotearquivoanexo_nomearq2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavLotearquivoanexo_nomearq2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavLotearquivoanexo_nomearq2_Visible), 5, 0)));
            cmbavDynamicfiltersoperator2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "TIPODOCUMENTO_NOME") == 0 )
         {
            edtavTipodocumento_nome2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTipodocumento_nome2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTipodocumento_nome2_Visible), 5, 0)));
            cmbavDynamicfiltersoperator2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         }
      }

      protected void S132( )
      {
         /* 'ENABLEDYNAMICFILTERS3' Routine */
         edtavLotearquivoanexo_nomearq3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavLotearquivoanexo_nomearq3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavLotearquivoanexo_nomearq3_Visible), 5, 0)));
         edtavTipodocumento_nome3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTipodocumento_nome3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTipodocumento_nome3_Visible), 5, 0)));
         cmbavDynamicfiltersoperator3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV26DynamicFiltersSelector3, "LOTEARQUIVOANEXO_NOMEARQ") == 0 )
         {
            edtavLotearquivoanexo_nomearq3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavLotearquivoanexo_nomearq3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavLotearquivoanexo_nomearq3_Visible), 5, 0)));
            cmbavDynamicfiltersoperator3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV26DynamicFiltersSelector3, "TIPODOCUMENTO_NOME") == 0 )
         {
            edtavTipodocumento_nome3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTipodocumento_nome3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTipodocumento_nome3_Visible), 5, 0)));
            cmbavDynamicfiltersoperator3.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator3.Visible), 5, 0)));
         }
      }

      protected void S192( )
      {
         /* 'RESETDYNFILTERS' Routine */
         AV20DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled2", AV20DynamicFiltersEnabled2);
         AV21DynamicFiltersSelector2 = "LOTEARQUIVOANEXO_NOMEARQ";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
         AV22DynamicFiltersOperator2 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)));
         AV23LoteArquivoAnexo_NomeArq2 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23LoteArquivoAnexo_NomeArq2", AV23LoteArquivoAnexo_NomeArq2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV25DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersEnabled3", AV25DynamicFiltersEnabled3);
         AV26DynamicFiltersSelector3 = "LOTEARQUIVOANEXO_NOMEARQ";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersSelector3", AV26DynamicFiltersSelector3);
         AV27DynamicFiltersOperator3 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV27DynamicFiltersOperator3), 4, 0)));
         AV28LoteArquivoAnexo_NomeArq3 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28LoteArquivoAnexo_NomeArq3", AV28LoteArquivoAnexo_NomeArq3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S202( )
      {
         /* 'CLEANFILTERS' Routine */
         AV35TFLoteArquivoAnexo_LoteCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TFLoteArquivoAnexo_LoteCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35TFLoteArquivoAnexo_LoteCod), 6, 0)));
         Ddo_lotearquivoanexo_lotecod_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_lotearquivoanexo_lotecod_Internalname, "FilteredText_set", Ddo_lotearquivoanexo_lotecod_Filteredtext_set);
         AV36TFLoteArquivoAnexo_LoteCod_To = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36TFLoteArquivoAnexo_LoteCod_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36TFLoteArquivoAnexo_LoteCod_To), 6, 0)));
         Ddo_lotearquivoanexo_lotecod_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_lotearquivoanexo_lotecod_Internalname, "FilteredTextTo_set", Ddo_lotearquivoanexo_lotecod_Filteredtextto_set);
         AV39TFLoteArquivoAnexo_Data = (DateTime)(DateTime.MinValue);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39TFLoteArquivoAnexo_Data", context.localUtil.TToC( AV39TFLoteArquivoAnexo_Data, 8, 5, 0, 3, "/", ":", " "));
         Ddo_lotearquivoanexo_data_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_lotearquivoanexo_data_Internalname, "FilteredText_set", Ddo_lotearquivoanexo_data_Filteredtext_set);
         AV40TFLoteArquivoAnexo_Data_To = (DateTime)(DateTime.MinValue);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40TFLoteArquivoAnexo_Data_To", context.localUtil.TToC( AV40TFLoteArquivoAnexo_Data_To, 8, 5, 0, 3, "/", ":", " "));
         Ddo_lotearquivoanexo_data_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_lotearquivoanexo_data_Internalname, "FilteredTextTo_set", Ddo_lotearquivoanexo_data_Filteredtextto_set);
         AV45TFLoteArquivoAnexo_NomeArq = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45TFLoteArquivoAnexo_NomeArq", AV45TFLoteArquivoAnexo_NomeArq);
         Ddo_lotearquivoanexo_nomearq_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_lotearquivoanexo_nomearq_Internalname, "FilteredText_set", Ddo_lotearquivoanexo_nomearq_Filteredtext_set);
         AV46TFLoteArquivoAnexo_NomeArq_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46TFLoteArquivoAnexo_NomeArq_Sel", AV46TFLoteArquivoAnexo_NomeArq_Sel);
         Ddo_lotearquivoanexo_nomearq_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_lotearquivoanexo_nomearq_Internalname, "SelectedValue_set", Ddo_lotearquivoanexo_nomearq_Selectedvalue_set);
         AV49TFLoteArquivoAnexo_TipoArq = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49TFLoteArquivoAnexo_TipoArq", AV49TFLoteArquivoAnexo_TipoArq);
         Ddo_lotearquivoanexo_tipoarq_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_lotearquivoanexo_tipoarq_Internalname, "FilteredText_set", Ddo_lotearquivoanexo_tipoarq_Filteredtext_set);
         AV50TFLoteArquivoAnexo_TipoArq_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50TFLoteArquivoAnexo_TipoArq_Sel", AV50TFLoteArquivoAnexo_TipoArq_Sel);
         Ddo_lotearquivoanexo_tipoarq_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_lotearquivoanexo_tipoarq_Internalname, "SelectedValue_set", Ddo_lotearquivoanexo_tipoarq_Selectedvalue_set);
         AV53TFLoteArquivoAnexo_Descricao = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53TFLoteArquivoAnexo_Descricao", AV53TFLoteArquivoAnexo_Descricao);
         Ddo_lotearquivoanexo_descricao_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_lotearquivoanexo_descricao_Internalname, "FilteredText_set", Ddo_lotearquivoanexo_descricao_Filteredtext_set);
         AV54TFLoteArquivoAnexo_Descricao_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54TFLoteArquivoAnexo_Descricao_Sel", AV54TFLoteArquivoAnexo_Descricao_Sel);
         Ddo_lotearquivoanexo_descricao_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_lotearquivoanexo_descricao_Internalname, "SelectedValue_set", Ddo_lotearquivoanexo_descricao_Selectedvalue_set);
         AV57TFTipoDocumento_Nome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57TFTipoDocumento_Nome", AV57TFTipoDocumento_Nome);
         Ddo_tipodocumento_nome_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tipodocumento_nome_Internalname, "FilteredText_set", Ddo_tipodocumento_nome_Filteredtext_set);
         AV58TFTipoDocumento_Nome_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58TFTipoDocumento_Nome_Sel", AV58TFTipoDocumento_Nome_Sel);
         Ddo_tipodocumento_nome_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_tipodocumento_nome_Internalname, "SelectedValue_set", Ddo_tipodocumento_nome_Selectedvalue_set);
         AV16DynamicFiltersSelector1 = "LOTEARQUIVOANEXO_NOMEARQ";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
         AV17DynamicFiltersOperator1 = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)));
         AV18LoteArquivoAnexo_NomeArq1 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18LoteArquivoAnexo_NomeArq1", AV18LoteArquivoAnexo_NomeArq1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV11GridState.gxTpr_Dynamicfilters.Clear();
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S142( )
      {
         /* 'LOADDYNFILTERSSTATE' Routine */
         imgAdddynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         imgAdddynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
         if ( AV11GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV13GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV11GridState.gxTpr_Dynamicfilters.Item(1));
            AV16DynamicFiltersSelector1 = AV13GridStateDynamicFilter.gxTpr_Selected;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
            if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "LOTEARQUIVOANEXO_NOMEARQ") == 0 )
            {
               AV17DynamicFiltersOperator1 = AV13GridStateDynamicFilter.gxTpr_Operator;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)));
               AV18LoteArquivoAnexo_NomeArq1 = AV13GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18LoteArquivoAnexo_NomeArq1", AV18LoteArquivoAnexo_NomeArq1);
            }
            else if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "TIPODOCUMENTO_NOME") == 0 )
            {
               AV17DynamicFiltersOperator1 = AV13GridStateDynamicFilter.gxTpr_Operator;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)));
               AV19TipoDocumento_Nome1 = AV13GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19TipoDocumento_Nome1", AV19TipoDocumento_Nome1);
            }
            /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            if ( AV11GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               lblJsdynamicfilters_Caption = "<script type=\"text/javascript\">";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(2);";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               imgAdddynamicfilters1_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
               imgRemovedynamicfilters1_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
               AV20DynamicFiltersEnabled2 = true;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled2", AV20DynamicFiltersEnabled2);
               AV13GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV11GridState.gxTpr_Dynamicfilters.Item(2));
               AV21DynamicFiltersSelector2 = AV13GridStateDynamicFilter.gxTpr_Selected;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
               if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "LOTEARQUIVOANEXO_NOMEARQ") == 0 )
               {
                  AV22DynamicFiltersOperator2 = AV13GridStateDynamicFilter.gxTpr_Operator;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)));
                  AV23LoteArquivoAnexo_NomeArq2 = AV13GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23LoteArquivoAnexo_NomeArq2", AV23LoteArquivoAnexo_NomeArq2);
               }
               else if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "TIPODOCUMENTO_NOME") == 0 )
               {
                  AV22DynamicFiltersOperator2 = AV13GridStateDynamicFilter.gxTpr_Operator;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)));
                  AV24TipoDocumento_Nome2 = AV13GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24TipoDocumento_Nome2", AV24TipoDocumento_Nome2);
               }
               /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
               S122 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               if ( AV11GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(3);";
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
                  imgAdddynamicfilters2_Visible = 0;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
                  imgRemovedynamicfilters2_Visible = 1;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
                  AV25DynamicFiltersEnabled3 = true;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersEnabled3", AV25DynamicFiltersEnabled3);
                  AV13GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV11GridState.gxTpr_Dynamicfilters.Item(3));
                  AV26DynamicFiltersSelector3 = AV13GridStateDynamicFilter.gxTpr_Selected;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersSelector3", AV26DynamicFiltersSelector3);
                  if ( StringUtil.StrCmp(AV26DynamicFiltersSelector3, "LOTEARQUIVOANEXO_NOMEARQ") == 0 )
                  {
                     AV27DynamicFiltersOperator3 = AV13GridStateDynamicFilter.gxTpr_Operator;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV27DynamicFiltersOperator3), 4, 0)));
                     AV28LoteArquivoAnexo_NomeArq3 = AV13GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28LoteArquivoAnexo_NomeArq3", AV28LoteArquivoAnexo_NomeArq3);
                  }
                  else if ( StringUtil.StrCmp(AV26DynamicFiltersSelector3, "TIPODOCUMENTO_NOME") == 0 )
                  {
                     AV27DynamicFiltersOperator3 = AV13GridStateDynamicFilter.gxTpr_Operator;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersOperator3", StringUtil.LTrim( StringUtil.Str( (decimal)(AV27DynamicFiltersOperator3), 4, 0)));
                     AV29TipoDocumento_Nome3 = AV13GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29TipoDocumento_Nome3", AV29TipoDocumento_Nome3);
                  }
                  /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
                  S132 ();
                  if ( returnInSub )
                  {
                     returnInSub = true;
                     if (true) return;
                  }
               }
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"</script>";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
            }
         }
         if ( AV30DynamicFiltersRemoving )
         {
            lblJsdynamicfilters_Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         }
      }

      protected void S162( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV11GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV11GridState.gxTpr_Orderedby = AV14OrderedBy;
         AV11GridState.gxTpr_Ordereddsc = AV15OrderedDsc;
         AV11GridState.gxTpr_Filtervalues.Clear();
         if ( ! ( (0==AV35TFLoteArquivoAnexo_LoteCod) && (0==AV36TFLoteArquivoAnexo_LoteCod_To) ) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFLOTEARQUIVOANEXO_LOTECOD";
            AV12GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV35TFLoteArquivoAnexo_LoteCod), 6, 0);
            AV12GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( (decimal)(AV36TFLoteArquivoAnexo_LoteCod_To), 6, 0);
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! ( (DateTime.MinValue==AV39TFLoteArquivoAnexo_Data) && (DateTime.MinValue==AV40TFLoteArquivoAnexo_Data_To) ) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFLOTEARQUIVOANEXO_DATA";
            AV12GridStateFilterValue.gxTpr_Value = context.localUtil.TToC( AV39TFLoteArquivoAnexo_Data, 8, 5, 0, 3, "/", ":", " ");
            AV12GridStateFilterValue.gxTpr_Valueto = context.localUtil.TToC( AV40TFLoteArquivoAnexo_Data_To, 8, 5, 0, 3, "/", ":", " ");
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV45TFLoteArquivoAnexo_NomeArq)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFLOTEARQUIVOANEXO_NOMEARQ";
            AV12GridStateFilterValue.gxTpr_Value = AV45TFLoteArquivoAnexo_NomeArq;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV46TFLoteArquivoAnexo_NomeArq_Sel)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFLOTEARQUIVOANEXO_NOMEARQ_SEL";
            AV12GridStateFilterValue.gxTpr_Value = AV46TFLoteArquivoAnexo_NomeArq_Sel;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV49TFLoteArquivoAnexo_TipoArq)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFLOTEARQUIVOANEXO_TIPOARQ";
            AV12GridStateFilterValue.gxTpr_Value = AV49TFLoteArquivoAnexo_TipoArq;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV50TFLoteArquivoAnexo_TipoArq_Sel)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFLOTEARQUIVOANEXO_TIPOARQ_SEL";
            AV12GridStateFilterValue.gxTpr_Value = AV50TFLoteArquivoAnexo_TipoArq_Sel;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV53TFLoteArquivoAnexo_Descricao)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFLOTEARQUIVOANEXO_DESCRICAO";
            AV12GridStateFilterValue.gxTpr_Value = AV53TFLoteArquivoAnexo_Descricao;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV54TFLoteArquivoAnexo_Descricao_Sel)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFLOTEARQUIVOANEXO_DESCRICAO_SEL";
            AV12GridStateFilterValue.gxTpr_Value = AV54TFLoteArquivoAnexo_Descricao_Sel;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV57TFTipoDocumento_Nome)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFTIPODOCUMENTO_NOME";
            AV12GridStateFilterValue.gxTpr_Value = AV57TFTipoDocumento_Nome;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV58TFTipoDocumento_Nome_Sel)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFTIPODOCUMENTO_NOME_SEL";
            AV12GridStateFilterValue.gxTpr_Value = AV58TFTipoDocumento_Nome_Sel;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         new wwpbaseobjects.savegridstate(context ).execute(  AV67Pgmname+"GridState",  AV11GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_MeetrikaVs3")) ;
      }

      protected void S182( )
      {
         /* 'SAVEDYNFILTERSSTATE' Routine */
         AV11GridState.gxTpr_Dynamicfilters.Clear();
         if ( ! AV31DynamicFiltersIgnoreFirst )
         {
            AV13GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV13GridStateDynamicFilter.gxTpr_Selected = AV16DynamicFiltersSelector1;
            if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "LOTEARQUIVOANEXO_NOMEARQ") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV18LoteArquivoAnexo_NomeArq1)) )
            {
               AV13GridStateDynamicFilter.gxTpr_Value = AV18LoteArquivoAnexo_NomeArq1;
               AV13GridStateDynamicFilter.gxTpr_Operator = AV17DynamicFiltersOperator1;
            }
            else if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "TIPODOCUMENTO_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV19TipoDocumento_Nome1)) )
            {
               AV13GridStateDynamicFilter.gxTpr_Value = AV19TipoDocumento_Nome1;
               AV13GridStateDynamicFilter.gxTpr_Operator = AV17DynamicFiltersOperator1;
            }
            if ( AV30DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV13GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV11GridState.gxTpr_Dynamicfilters.Add(AV13GridStateDynamicFilter, 0);
            }
         }
         if ( AV20DynamicFiltersEnabled2 )
         {
            AV13GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV13GridStateDynamicFilter.gxTpr_Selected = AV21DynamicFiltersSelector2;
            if ( ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "LOTEARQUIVOANEXO_NOMEARQ") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV23LoteArquivoAnexo_NomeArq2)) )
            {
               AV13GridStateDynamicFilter.gxTpr_Value = AV23LoteArquivoAnexo_NomeArq2;
               AV13GridStateDynamicFilter.gxTpr_Operator = AV22DynamicFiltersOperator2;
            }
            else if ( ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "TIPODOCUMENTO_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV24TipoDocumento_Nome2)) )
            {
               AV13GridStateDynamicFilter.gxTpr_Value = AV24TipoDocumento_Nome2;
               AV13GridStateDynamicFilter.gxTpr_Operator = AV22DynamicFiltersOperator2;
            }
            if ( AV30DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV13GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV11GridState.gxTpr_Dynamicfilters.Add(AV13GridStateDynamicFilter, 0);
            }
         }
         if ( AV25DynamicFiltersEnabled3 )
         {
            AV13GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV13GridStateDynamicFilter.gxTpr_Selected = AV26DynamicFiltersSelector3;
            if ( ( StringUtil.StrCmp(AV26DynamicFiltersSelector3, "LOTEARQUIVOANEXO_NOMEARQ") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV28LoteArquivoAnexo_NomeArq3)) )
            {
               AV13GridStateDynamicFilter.gxTpr_Value = AV28LoteArquivoAnexo_NomeArq3;
               AV13GridStateDynamicFilter.gxTpr_Operator = AV27DynamicFiltersOperator3;
            }
            else if ( ( StringUtil.StrCmp(AV26DynamicFiltersSelector3, "TIPODOCUMENTO_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV29TipoDocumento_Nome3)) )
            {
               AV13GridStateDynamicFilter.gxTpr_Value = AV29TipoDocumento_Nome3;
               AV13GridStateDynamicFilter.gxTpr_Operator = AV27DynamicFiltersOperator3;
            }
            if ( AV30DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV13GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV11GridState.gxTpr_Dynamicfilters.Add(AV13GridStateDynamicFilter, 0);
            }
         }
      }

      protected void wb_table1_2_EX2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableSearchCell'>") ;
            wb_table2_5_EX2( true) ;
         }
         else
         {
            wb_table2_5_EX2( false) ;
         }
         return  ;
      }

      protected void wb_table2_5_EX2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_77_EX2( true) ;
         }
         else
         {
            wb_table3_77_EX2( false) ;
         }
         return  ;
      }

      protected void wb_table3_77_EX2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_EX2e( true) ;
         }
         else
         {
            wb_table1_2_EX2e( false) ;
         }
      }

      protected void wb_table3_77_EX2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "Table100x100", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_80_EX2( true) ;
         }
         else
         {
            wb_table4_80_EX2( false) ;
         }
         return  ;
      }

      protected void wb_table4_80_EX2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_77_EX2e( true) ;
         }
         else
         {
            wb_table3_77_EX2e( false) ;
         }
      }

      protected void wb_table4_80_EX2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"DivS\" data-gxgridid=\"83\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtLoteArquivoAnexo_LoteCod_Titleformat == 0 )
               {
                  context.SendWebValue( edtLoteArquivoAnexo_LoteCod_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtLoteArquivoAnexo_LoteCod_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtLoteArquivoAnexo_Data_Titleformat == 0 )
               {
                  context.SendWebValue( edtLoteArquivoAnexo_Data_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtLoteArquivoAnexo_Data_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Arquivo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtLoteArquivoAnexo_NomeArq_Titleformat == 0 )
               {
                  context.SendWebValue( edtLoteArquivoAnexo_NomeArq_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtLoteArquivoAnexo_NomeArq_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtLoteArquivoAnexo_TipoArq_Titleformat == 0 )
               {
                  context.SendWebValue( edtLoteArquivoAnexo_TipoArq_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtLoteArquivoAnexo_TipoArq_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtLoteArquivoAnexo_Descricao_Titleformat == 0 )
               {
                  context.SendWebValue( edtLoteArquivoAnexo_Descricao_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtLoteArquivoAnexo_Descricao_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Tipo Documento") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtTipoDocumento_Nome_Titleformat == 0 )
               {
                  context.SendWebValue( edtTipoDocumento_Nome_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtTipoDocumento_Nome_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", "");
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV32Select));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavSelect_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A841LoteArquivoAnexo_LoteCod), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtLoteArquivoAnexo_LoteCod_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtLoteArquivoAnexo_LoteCod_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.localUtil.TToC( A836LoteArquivoAnexo_Data, 10, 8, 0, 3, "/", ":", " "));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtLoteArquivoAnexo_Data_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtLoteArquivoAnexo_Data_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A838LoteArquivoAnexo_Arquivo);
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A839LoteArquivoAnexo_NomeArq));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtLoteArquivoAnexo_NomeArq_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtLoteArquivoAnexo_NomeArq_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A840LoteArquivoAnexo_TipoArq));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtLoteArquivoAnexo_TipoArq_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtLoteArquivoAnexo_TipoArq_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A837LoteArquivoAnexo_Descricao);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtLoteArquivoAnexo_Descricao_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtLoteArquivoAnexo_Descricao_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A645TipoDocumento_Codigo), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A646TipoDocumento_Nome));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtTipoDocumento_Nome_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtTipoDocumento_Nome_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 83 )
         {
            wbEnd = 0;
            nRC_GXsfl_83 = (short)(nGXsfl_83_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_80_EX2e( true) ;
         }
         else
         {
            wb_table4_80_EX2e( false) ;
         }
      }

      protected void wb_table2_5_EX2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablesearch_Internalname, tblTablesearch_Internalname, "", "TableSearch", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='OrderedByDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblOrderedtext_Internalname, "Ordenado por", "", "", lblOrderedtext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "OrderedByDescription", 0, "", 1, 1, 0, "HLP_PromptLoteArquivoAnexo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 10,'',false,'" + sGXsfl_83_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavOrderedby, cmbavOrderedby_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0)), 1, cmbavOrderedby_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVORDEREDBY.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,10);\"", "", true, "HLP_PromptLoteArquivoAnexo.htm");
            cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV14OrderedBy), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", (String)(cmbavOrderedby.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 11,'',false,'" + sGXsfl_83_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV15OrderedDsc), StringUtil.BoolToStr( AV15OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,11);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_PromptLoteArquivoAnexo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableFiltersCell'>") ;
            wb_table5_14_EX2( true) ;
         }
         else
         {
            wb_table5_14_EX2( false) ;
         }
         return  ;
      }

      protected void wb_table5_14_EX2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_EX2e( true) ;
         }
         else
         {
            wb_table2_5_EX2e( false) ;
         }
      }

      protected void wb_table5_14_EX2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='GridHeaderCellCleanFilters'>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 17,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgCleanfilters_Internalname, context.GetImagePath( "effedd9a-25cf-4497-952f-5a5ac032b8be", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Limpar filtros", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgCleanfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOCLEANFILTERS\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptLoteArquivoAnexo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataFilterContentCell'>") ;
            wb_table6_19_EX2( true) ;
         }
         else
         {
            wb_table6_19_EX2( false) ;
         }
         return  ;
      }

      protected void wb_table6_19_EX2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblJsdynamicfilters_Internalname, lblJsdynamicfilters_Caption, "", "", lblJsdynamicfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "", 0, "", 1, 1, 1, "HLP_PromptLoteArquivoAnexo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_14_EX2e( true) ;
         }
         else
         {
            wb_table5_14_EX2e( false) ;
         }
      }

      protected void wb_table6_19_EX2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabledynamicfilters_Internalname, tblTabledynamicfilters_Internalname, "", "TableDynamicFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix1_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptLoteArquivoAnexo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 24,'',false,'" + sGXsfl_83_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector1, cmbavDynamicfiltersselector1_Internalname, StringUtil.RTrim( AV16DynamicFiltersSelector1), 1, cmbavDynamicfiltersselector1_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR1.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,24);\"", "", true, "HLP_PromptLoteArquivoAnexo.htm");
            cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV16DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", (String)(cmbavDynamicfiltersselector1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle1_Internalname, "valor", "", "", lblDynamicfiltersmiddle1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptLoteArquivoAnexo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table7_28_EX2( true) ;
         }
         else
         {
            wb_table7_28_EX2( false) ;
         }
         return  ;
      }

      protected void wb_table7_28_EX2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 36,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters1_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters1_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptLoteArquivoAnexo.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 37,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters1_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters1_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptLoteArquivoAnexo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow2\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix2_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptLoteArquivoAnexo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 42,'',false,'" + sGXsfl_83_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector2, cmbavDynamicfiltersselector2_Internalname, StringUtil.RTrim( AV21DynamicFiltersSelector2), 1, cmbavDynamicfiltersselector2_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR2.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,42);\"", "", true, "HLP_PromptLoteArquivoAnexo.htm");
            cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", (String)(cmbavDynamicfiltersselector2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle2_Internalname, "valor", "", "", lblDynamicfiltersmiddle2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptLoteArquivoAnexo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table8_46_EX2( true) ;
         }
         else
         {
            wb_table8_46_EX2( false) ;
         }
         return  ;
      }

      protected void wb_table8_46_EX2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 54,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters2_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters2_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptLoteArquivoAnexo.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 55,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters2_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters2_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptLoteArquivoAnexo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow3\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix3_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptLoteArquivoAnexo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 60,'',false,'" + sGXsfl_83_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector3, cmbavDynamicfiltersselector3_Internalname, StringUtil.RTrim( AV26DynamicFiltersSelector3), 1, cmbavDynamicfiltersselector3_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR3.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,60);\"", "", true, "HLP_PromptLoteArquivoAnexo.htm");
            cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV26DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", (String)(cmbavDynamicfiltersselector3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle3_Internalname, "valor", "", "", lblDynamicfiltersmiddle3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_PromptLoteArquivoAnexo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table9_64_EX2( true) ;
         }
         else
         {
            wb_table9_64_EX2( false) ;
         }
         return  ;
      }

      protected void wb_table9_64_EX2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 72,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters3_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters3_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS3\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_PromptLoteArquivoAnexo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_19_EX2e( true) ;
         }
         else
         {
            wb_table6_19_EX2e( false) ;
         }
      }

      protected void wb_table9_64_EX2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters3_Internalname, tblTablemergeddynamicfilters3_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 67,'',false,'" + sGXsfl_83_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator3, cmbavDynamicfiltersoperator3_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV27DynamicFiltersOperator3), 4, 0)), 1, cmbavDynamicfiltersoperator3_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator3.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,67);\"", "", true, "HLP_PromptLoteArquivoAnexo.htm");
            cmbavDynamicfiltersoperator3.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV27DynamicFiltersOperator3), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator3_Internalname, "Values", (String)(cmbavDynamicfiltersoperator3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 69,'',false,'" + sGXsfl_83_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavLotearquivoanexo_nomearq3_Internalname, StringUtil.RTrim( AV28LoteArquivoAnexo_NomeArq3), StringUtil.RTrim( context.localUtil.Format( AV28LoteArquivoAnexo_NomeArq3, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,69);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavLotearquivoanexo_nomearq3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavLotearquivoanexo_nomearq3_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptLoteArquivoAnexo.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 70,'',false,'" + sGXsfl_83_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTipodocumento_nome3_Internalname, StringUtil.RTrim( AV29TipoDocumento_Nome3), StringUtil.RTrim( context.localUtil.Format( AV29TipoDocumento_Nome3, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,70);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTipodocumento_nome3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavTipodocumento_nome3_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptLoteArquivoAnexo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table9_64_EX2e( true) ;
         }
         else
         {
            wb_table9_64_EX2e( false) ;
         }
      }

      protected void wb_table8_46_EX2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters2_Internalname, tblTablemergeddynamicfilters2_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 49,'',false,'" + sGXsfl_83_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator2, cmbavDynamicfiltersoperator2_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0)), 1, cmbavDynamicfiltersoperator2_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator2.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,49);\"", "", true, "HLP_PromptLoteArquivoAnexo.htm");
            cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV22DynamicFiltersOperator2), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator2_Internalname, "Values", (String)(cmbavDynamicfiltersoperator2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 51,'',false,'" + sGXsfl_83_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavLotearquivoanexo_nomearq2_Internalname, StringUtil.RTrim( AV23LoteArquivoAnexo_NomeArq2), StringUtil.RTrim( context.localUtil.Format( AV23LoteArquivoAnexo_NomeArq2, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,51);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavLotearquivoanexo_nomearq2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavLotearquivoanexo_nomearq2_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptLoteArquivoAnexo.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 52,'',false,'" + sGXsfl_83_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTipodocumento_nome2_Internalname, StringUtil.RTrim( AV24TipoDocumento_Nome2), StringUtil.RTrim( context.localUtil.Format( AV24TipoDocumento_Nome2, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,52);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTipodocumento_nome2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavTipodocumento_nome2_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptLoteArquivoAnexo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table8_46_EX2e( true) ;
         }
         else
         {
            wb_table8_46_EX2e( false) ;
         }
      }

      protected void wb_table7_28_EX2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters1_Internalname, tblTablemergeddynamicfilters1_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 31,'',false,'" + sGXsfl_83_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator1, cmbavDynamicfiltersoperator1_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0)), 1, cmbavDynamicfiltersoperator1_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator1.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,31);\"", "", true, "HLP_PromptLoteArquivoAnexo.htm");
            cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV17DynamicFiltersOperator1), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersoperator1_Internalname, "Values", (String)(cmbavDynamicfiltersoperator1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 33,'',false,'" + sGXsfl_83_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavLotearquivoanexo_nomearq1_Internalname, StringUtil.RTrim( AV18LoteArquivoAnexo_NomeArq1), StringUtil.RTrim( context.localUtil.Format( AV18LoteArquivoAnexo_NomeArq1, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,33);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavLotearquivoanexo_nomearq1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavLotearquivoanexo_nomearq1_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptLoteArquivoAnexo.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 34,'',false,'" + sGXsfl_83_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTipodocumento_nome1_Internalname, StringUtil.RTrim( AV19TipoDocumento_Nome1), StringUtil.RTrim( context.localUtil.Format( AV19TipoDocumento_Nome1, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,34);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTipodocumento_nome1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavTipodocumento_nome1_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_PromptLoteArquivoAnexo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_28_EX2e( true) ;
         }
         else
         {
            wb_table7_28_EX2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV7InOutLoteArquivoAnexo_LoteCod = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7InOutLoteArquivoAnexo_LoteCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7InOutLoteArquivoAnexo_LoteCod), 6, 0)));
         AV8InOutLoteArquivoAnexo_Data = (DateTime)getParm(obj,1);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8InOutLoteArquivoAnexo_Data", context.localUtil.TToC( AV8InOutLoteArquivoAnexo_Data, 8, 5, 0, 3, "/", ":", " "));
         AV9InOutLoteArquivoAnexo_NomeArq = (String)getParm(obj,2);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9InOutLoteArquivoAnexo_NomeArq", AV9InOutLoteArquivoAnexo_NomeArq);
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAEX2( ) ;
         WSEX2( ) ;
         WEEX2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?2249787");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?202042823282068");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("promptlotearquivoanexo.js", "?202042823282068");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_832( )
      {
         edtavSelect_Internalname = "vSELECT_"+sGXsfl_83_idx;
         edtLoteArquivoAnexo_LoteCod_Internalname = "LOTEARQUIVOANEXO_LOTECOD_"+sGXsfl_83_idx;
         edtLoteArquivoAnexo_Data_Internalname = "LOTEARQUIVOANEXO_DATA_"+sGXsfl_83_idx;
         edtLoteArquivoAnexo_Arquivo_Internalname = "LOTEARQUIVOANEXO_ARQUIVO_"+sGXsfl_83_idx;
         edtLoteArquivoAnexo_NomeArq_Internalname = "LOTEARQUIVOANEXO_NOMEARQ_"+sGXsfl_83_idx;
         edtLoteArquivoAnexo_TipoArq_Internalname = "LOTEARQUIVOANEXO_TIPOARQ_"+sGXsfl_83_idx;
         edtLoteArquivoAnexo_Descricao_Internalname = "LOTEARQUIVOANEXO_DESCRICAO_"+sGXsfl_83_idx;
         edtTipoDocumento_Codigo_Internalname = "TIPODOCUMENTO_CODIGO_"+sGXsfl_83_idx;
         edtTipoDocumento_Nome_Internalname = "TIPODOCUMENTO_NOME_"+sGXsfl_83_idx;
      }

      protected void SubsflControlProps_fel_832( )
      {
         edtavSelect_Internalname = "vSELECT_"+sGXsfl_83_fel_idx;
         edtLoteArquivoAnexo_LoteCod_Internalname = "LOTEARQUIVOANEXO_LOTECOD_"+sGXsfl_83_fel_idx;
         edtLoteArquivoAnexo_Data_Internalname = "LOTEARQUIVOANEXO_DATA_"+sGXsfl_83_fel_idx;
         edtLoteArquivoAnexo_Arquivo_Internalname = "LOTEARQUIVOANEXO_ARQUIVO_"+sGXsfl_83_fel_idx;
         edtLoteArquivoAnexo_NomeArq_Internalname = "LOTEARQUIVOANEXO_NOMEARQ_"+sGXsfl_83_fel_idx;
         edtLoteArquivoAnexo_TipoArq_Internalname = "LOTEARQUIVOANEXO_TIPOARQ_"+sGXsfl_83_fel_idx;
         edtLoteArquivoAnexo_Descricao_Internalname = "LOTEARQUIVOANEXO_DESCRICAO_"+sGXsfl_83_fel_idx;
         edtTipoDocumento_Codigo_Internalname = "TIPODOCUMENTO_CODIGO_"+sGXsfl_83_fel_idx;
         edtTipoDocumento_Nome_Internalname = "TIPODOCUMENTO_NOME_"+sGXsfl_83_fel_idx;
      }

      protected void sendrow_832( )
      {
         SubsflControlProps_832( ) ;
         WBEX0( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_83_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_83_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_83_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Active Bitmap Variable */
            TempTags = " " + ((edtavSelect_Enabled!=0)&&(edtavSelect_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 84,'',false,'',83)\"" : " ");
            ClassString = "Image";
            StyleString = "";
            AV32Select_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV32Select))&&String.IsNullOrEmpty(StringUtil.RTrim( AV66Select_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV32Select)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavSelect_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV32Select)) ? AV66Select_GXI : context.PathToRelativeUrl( AV32Select)),(String)"",(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavSelect_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)5,(String)edtavSelect_Jsonclick,"'"+""+"'"+",false,"+"'"+"EENTER."+sGXsfl_83_idx+"'",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)""+TempTags,(String)"",(String)"",(short)1,(bool)AV32Select_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtLoteArquivoAnexo_LoteCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A841LoteArquivoAnexo_LoteCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A841LoteArquivoAnexo_LoteCod), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtLoteArquivoAnexo_LoteCod_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)83,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtLoteArquivoAnexo_Data_Internalname,context.localUtil.TToC( A836LoteArquivoAnexo_Data, 10, 8, 0, 3, "/", ":", " "),context.localUtil.Format( A836LoteArquivoAnexo_Data, "99/99/99 99:99"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtLoteArquivoAnexo_Data_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)14,(short)0,(short)0,(short)83,(short)1,(short)-1,(short)0,(bool)true,(String)"DataHora",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            ClassString = "Image";
            StyleString = "";
            edtLoteArquivoAnexo_Arquivo_Filename = A839LoteArquivoAnexo_NomeArq;
            edtLoteArquivoAnexo_Arquivo_Filetype = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLoteArquivoAnexo_Arquivo_Internalname, "Filetype", edtLoteArquivoAnexo_Arquivo_Filetype);
            edtLoteArquivoAnexo_Arquivo_Filetype = A840LoteArquivoAnexo_TipoArq;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLoteArquivoAnexo_Arquivo_Internalname, "Filetype", edtLoteArquivoAnexo_Arquivo_Filetype);
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A838LoteArquivoAnexo_Arquivo)) )
            {
               gxblobfileaux.Source = A838LoteArquivoAnexo_Arquivo;
               if ( ! gxblobfileaux.HasExtension() || ( StringUtil.StrCmp(edtLoteArquivoAnexo_Arquivo_Filetype, "tmp") != 0 ) )
               {
                  gxblobfileaux.SetExtension(StringUtil.Trim( edtLoteArquivoAnexo_Arquivo_Filetype));
               }
               if ( gxblobfileaux.ErrCode == 0 )
               {
                  A838LoteArquivoAnexo_Arquivo = gxblobfileaux.GetAbsoluteName();
                  n838LoteArquivoAnexo_Arquivo = false;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLoteArquivoAnexo_Arquivo_Internalname, "URL", context.PathToRelativeUrl( A838LoteArquivoAnexo_Arquivo));
                  edtLoteArquivoAnexo_Arquivo_Filetype = gxblobfileaux.GetExtension();
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLoteArquivoAnexo_Arquivo_Internalname, "Filetype", edtLoteArquivoAnexo_Arquivo_Filetype);
               }
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLoteArquivoAnexo_Arquivo_Internalname, "URL", context.PathToRelativeUrl( A838LoteArquivoAnexo_Arquivo));
            }
            GridRow.AddColumnProperties("blob", 2, isAjaxCallMode( ), new Object[] {(String)edtLoteArquivoAnexo_Arquivo_Internalname,StringUtil.RTrim( A838LoteArquivoAnexo_Arquivo),context.PathToRelativeUrl( A838LoteArquivoAnexo_Arquivo),(String.IsNullOrEmpty(StringUtil.RTrim( edtLoteArquivoAnexo_Arquivo_Contenttype)) ? context.GetContentType( (String.IsNullOrEmpty(StringUtil.RTrim( edtLoteArquivoAnexo_Arquivo_Filetype)) ? A838LoteArquivoAnexo_Arquivo : edtLoteArquivoAnexo_Arquivo_Filetype)) : edtLoteArquivoAnexo_Arquivo_Contenttype),(bool)true,(String)"",(String)edtLoteArquivoAnexo_Arquivo_Parameters,(short)0,(short)0,(short)-1,(String)"",(String)"",(short)0,(short)-1,(short)0,(String)"px",(short)60,(String)"px",(short)0,(short)0,(short)0,(String)edtLoteArquivoAnexo_Arquivo_Jsonclick,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)StyleString,(String)ClassString,(String)"",(String)""+"",(String)"",(String)""});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtLoteArquivoAnexo_NomeArq_Internalname,StringUtil.RTrim( A839LoteArquivoAnexo_NomeArq),(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtLoteArquivoAnexo_NomeArq_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)83,(short)1,(short)-1,(short)-1,(bool)true,(String)"NomeArq",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtLoteArquivoAnexo_TipoArq_Internalname,StringUtil.RTrim( A840LoteArquivoAnexo_TipoArq),(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtLoteArquivoAnexo_TipoArq_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)10,(short)0,(short)0,(short)83,(short)1,(short)-1,(short)-1,(bool)true,(String)"TipoArq",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtLoteArquivoAnexo_Descricao_Internalname,(String)A837LoteArquivoAnexo_Descricao,(String)A837LoteArquivoAnexo_Descricao,(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtLoteArquivoAnexo_Descricao_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)500,(short)0,(short)0,(short)83,(short)1,(short)0,(short)-1,(bool)true,(String)"DescricaoLonga",(String)"left",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtTipoDocumento_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A645TipoDocumento_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A645TipoDocumento_Codigo), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtTipoDocumento_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)83,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtTipoDocumento_Nome_Internalname,StringUtil.RTrim( A646TipoDocumento_Nome),StringUtil.RTrim( context.localUtil.Format( A646TipoDocumento_Nome, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtTipoDocumento_Nome_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)83,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome",(String)"left",(bool)true});
            GxWebStd.gx_hidden_field( context, "gxhash_LOTEARQUIVOANEXO_LOTECOD"+"_"+sGXsfl_83_idx, GetSecureSignedToken( sGXsfl_83_idx, context.localUtil.Format( (decimal)(A841LoteArquivoAnexo_LoteCod), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_LOTEARQUIVOANEXO_DATA"+"_"+sGXsfl_83_idx, GetSecureSignedToken( sGXsfl_83_idx, context.localUtil.Format( A836LoteArquivoAnexo_Data, "99/99/99 99:99")));
            GxWebStd.gx_hidden_field( context, "gxhash_LOTEARQUIVOANEXO_DESCRICAO"+"_"+sGXsfl_83_idx, GetSecureSignedToken( sGXsfl_83_idx, A837LoteArquivoAnexo_Descricao));
            GxWebStd.gx_hidden_field( context, "gxhash_TIPODOCUMENTO_CODIGO"+"_"+sGXsfl_83_idx, GetSecureSignedToken( sGXsfl_83_idx, context.localUtil.Format( (decimal)(A645TipoDocumento_Codigo), "ZZZZZ9")));
            GridContainer.AddRow(GridRow);
            nGXsfl_83_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_83_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_83_idx+1));
            sGXsfl_83_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_83_idx), 4, 0)), 4, "0");
            SubsflControlProps_832( ) ;
         }
         /* End function sendrow_832 */
      }

      protected void init_default_properties( )
      {
         lblOrderedtext_Internalname = "ORDEREDTEXT";
         cmbavOrderedby_Internalname = "vORDEREDBY";
         edtavOrdereddsc_Internalname = "vORDEREDDSC";
         imgCleanfilters_Internalname = "CLEANFILTERS";
         lblDynamicfiltersprefix1_Internalname = "DYNAMICFILTERSPREFIX1";
         cmbavDynamicfiltersselector1_Internalname = "vDYNAMICFILTERSSELECTOR1";
         lblDynamicfiltersmiddle1_Internalname = "DYNAMICFILTERSMIDDLE1";
         cmbavDynamicfiltersoperator1_Internalname = "vDYNAMICFILTERSOPERATOR1";
         edtavLotearquivoanexo_nomearq1_Internalname = "vLOTEARQUIVOANEXO_NOMEARQ1";
         edtavTipodocumento_nome1_Internalname = "vTIPODOCUMENTO_NOME1";
         tblTablemergeddynamicfilters1_Internalname = "TABLEMERGEDDYNAMICFILTERS1";
         imgAdddynamicfilters1_Internalname = "ADDDYNAMICFILTERS1";
         imgRemovedynamicfilters1_Internalname = "REMOVEDYNAMICFILTERS1";
         lblDynamicfiltersprefix2_Internalname = "DYNAMICFILTERSPREFIX2";
         cmbavDynamicfiltersselector2_Internalname = "vDYNAMICFILTERSSELECTOR2";
         lblDynamicfiltersmiddle2_Internalname = "DYNAMICFILTERSMIDDLE2";
         cmbavDynamicfiltersoperator2_Internalname = "vDYNAMICFILTERSOPERATOR2";
         edtavLotearquivoanexo_nomearq2_Internalname = "vLOTEARQUIVOANEXO_NOMEARQ2";
         edtavTipodocumento_nome2_Internalname = "vTIPODOCUMENTO_NOME2";
         tblTablemergeddynamicfilters2_Internalname = "TABLEMERGEDDYNAMICFILTERS2";
         imgAdddynamicfilters2_Internalname = "ADDDYNAMICFILTERS2";
         imgRemovedynamicfilters2_Internalname = "REMOVEDYNAMICFILTERS2";
         lblDynamicfiltersprefix3_Internalname = "DYNAMICFILTERSPREFIX3";
         cmbavDynamicfiltersselector3_Internalname = "vDYNAMICFILTERSSELECTOR3";
         lblDynamicfiltersmiddle3_Internalname = "DYNAMICFILTERSMIDDLE3";
         cmbavDynamicfiltersoperator3_Internalname = "vDYNAMICFILTERSOPERATOR3";
         edtavLotearquivoanexo_nomearq3_Internalname = "vLOTEARQUIVOANEXO_NOMEARQ3";
         edtavTipodocumento_nome3_Internalname = "vTIPODOCUMENTO_NOME3";
         tblTablemergeddynamicfilters3_Internalname = "TABLEMERGEDDYNAMICFILTERS3";
         imgRemovedynamicfilters3_Internalname = "REMOVEDYNAMICFILTERS3";
         tblTabledynamicfilters_Internalname = "TABLEDYNAMICFILTERS";
         lblJsdynamicfilters_Internalname = "JSDYNAMICFILTERS";
         tblTablefilters_Internalname = "TABLEFILTERS";
         tblTablesearch_Internalname = "TABLESEARCH";
         edtavSelect_Internalname = "vSELECT";
         edtLoteArquivoAnexo_LoteCod_Internalname = "LOTEARQUIVOANEXO_LOTECOD";
         edtLoteArquivoAnexo_Data_Internalname = "LOTEARQUIVOANEXO_DATA";
         edtLoteArquivoAnexo_Arquivo_Internalname = "LOTEARQUIVOANEXO_ARQUIVO";
         edtLoteArquivoAnexo_NomeArq_Internalname = "LOTEARQUIVOANEXO_NOMEARQ";
         edtLoteArquivoAnexo_TipoArq_Internalname = "LOTEARQUIVOANEXO_TIPOARQ";
         edtLoteArquivoAnexo_Descricao_Internalname = "LOTEARQUIVOANEXO_DESCRICAO";
         edtTipoDocumento_Codigo_Internalname = "TIPODOCUMENTO_CODIGO";
         edtTipoDocumento_Nome_Internalname = "TIPODOCUMENTO_NOME";
         Gridpaginationbar_Internalname = "GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = "GRIDTABLEWITHPAGINATIONBAR";
         tblTablegridheader_Internalname = "TABLEGRIDHEADER";
         tblTablemain_Internalname = "TABLEMAIN";
         Workwithplusutilities1_Internalname = "WORKWITHPLUSUTILITIES1";
         chkavDynamicfiltersenabled2_Internalname = "vDYNAMICFILTERSENABLED2";
         chkavDynamicfiltersenabled3_Internalname = "vDYNAMICFILTERSENABLED3";
         edtavTflotearquivoanexo_lotecod_Internalname = "vTFLOTEARQUIVOANEXO_LOTECOD";
         edtavTflotearquivoanexo_lotecod_to_Internalname = "vTFLOTEARQUIVOANEXO_LOTECOD_TO";
         edtavTflotearquivoanexo_data_Internalname = "vTFLOTEARQUIVOANEXO_DATA";
         edtavTflotearquivoanexo_data_to_Internalname = "vTFLOTEARQUIVOANEXO_DATA_TO";
         edtavDdo_lotearquivoanexo_dataauxdate_Internalname = "vDDO_LOTEARQUIVOANEXO_DATAAUXDATE";
         edtavDdo_lotearquivoanexo_dataauxdateto_Internalname = "vDDO_LOTEARQUIVOANEXO_DATAAUXDATETO";
         divDdo_lotearquivoanexo_dataauxdates_Internalname = "DDO_LOTEARQUIVOANEXO_DATAAUXDATES";
         edtavTflotearquivoanexo_nomearq_Internalname = "vTFLOTEARQUIVOANEXO_NOMEARQ";
         edtavTflotearquivoanexo_nomearq_sel_Internalname = "vTFLOTEARQUIVOANEXO_NOMEARQ_SEL";
         edtavTflotearquivoanexo_tipoarq_Internalname = "vTFLOTEARQUIVOANEXO_TIPOARQ";
         edtavTflotearquivoanexo_tipoarq_sel_Internalname = "vTFLOTEARQUIVOANEXO_TIPOARQ_SEL";
         edtavTflotearquivoanexo_descricao_Internalname = "vTFLOTEARQUIVOANEXO_DESCRICAO";
         edtavTflotearquivoanexo_descricao_sel_Internalname = "vTFLOTEARQUIVOANEXO_DESCRICAO_SEL";
         edtavTftipodocumento_nome_Internalname = "vTFTIPODOCUMENTO_NOME";
         edtavTftipodocumento_nome_sel_Internalname = "vTFTIPODOCUMENTO_NOME_SEL";
         Ddo_lotearquivoanexo_lotecod_Internalname = "DDO_LOTEARQUIVOANEXO_LOTECOD";
         edtavDdo_lotearquivoanexo_lotecodtitlecontrolidtoreplace_Internalname = "vDDO_LOTEARQUIVOANEXO_LOTECODTITLECONTROLIDTOREPLACE";
         Ddo_lotearquivoanexo_data_Internalname = "DDO_LOTEARQUIVOANEXO_DATA";
         edtavDdo_lotearquivoanexo_datatitlecontrolidtoreplace_Internalname = "vDDO_LOTEARQUIVOANEXO_DATATITLECONTROLIDTOREPLACE";
         Ddo_lotearquivoanexo_nomearq_Internalname = "DDO_LOTEARQUIVOANEXO_NOMEARQ";
         edtavDdo_lotearquivoanexo_nomearqtitlecontrolidtoreplace_Internalname = "vDDO_LOTEARQUIVOANEXO_NOMEARQTITLECONTROLIDTOREPLACE";
         Ddo_lotearquivoanexo_tipoarq_Internalname = "DDO_LOTEARQUIVOANEXO_TIPOARQ";
         edtavDdo_lotearquivoanexo_tipoarqtitlecontrolidtoreplace_Internalname = "vDDO_LOTEARQUIVOANEXO_TIPOARQTITLECONTROLIDTOREPLACE";
         Ddo_lotearquivoanexo_descricao_Internalname = "DDO_LOTEARQUIVOANEXO_DESCRICAO";
         edtavDdo_lotearquivoanexo_descricaotitlecontrolidtoreplace_Internalname = "vDDO_LOTEARQUIVOANEXO_DESCRICAOTITLECONTROLIDTOREPLACE";
         Ddo_tipodocumento_nome_Internalname = "DDO_TIPODOCUMENTO_NOME";
         edtavDdo_tipodocumento_nometitlecontrolidtoreplace_Internalname = "vDDO_TIPODOCUMENTO_NOMETITLECONTROLIDTOREPLACE";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtTipoDocumento_Nome_Jsonclick = "";
         edtTipoDocumento_Codigo_Jsonclick = "";
         edtLoteArquivoAnexo_Descricao_Jsonclick = "";
         edtLoteArquivoAnexo_TipoArq_Jsonclick = "";
         edtLoteArquivoAnexo_NomeArq_Jsonclick = "";
         edtLoteArquivoAnexo_Arquivo_Jsonclick = "";
         edtLoteArquivoAnexo_Arquivo_Parameters = "";
         edtLoteArquivoAnexo_Arquivo_Contenttype = "";
         edtLoteArquivoAnexo_Data_Jsonclick = "";
         edtLoteArquivoAnexo_LoteCod_Jsonclick = "";
         edtavSelect_Jsonclick = "";
         edtavSelect_Visible = -1;
         edtavSelect_Enabled = 1;
         edtavTipodocumento_nome1_Jsonclick = "";
         edtavLotearquivoanexo_nomearq1_Jsonclick = "";
         cmbavDynamicfiltersoperator1_Jsonclick = "";
         edtavTipodocumento_nome2_Jsonclick = "";
         edtavLotearquivoanexo_nomearq2_Jsonclick = "";
         cmbavDynamicfiltersoperator2_Jsonclick = "";
         edtavTipodocumento_nome3_Jsonclick = "";
         edtavLotearquivoanexo_nomearq3_Jsonclick = "";
         cmbavDynamicfiltersoperator3_Jsonclick = "";
         cmbavDynamicfiltersselector3_Jsonclick = "";
         imgRemovedynamicfilters2_Visible = 1;
         imgAdddynamicfilters2_Visible = 1;
         cmbavDynamicfiltersselector2_Jsonclick = "";
         imgRemovedynamicfilters1_Visible = 1;
         imgAdddynamicfilters1_Visible = 1;
         cmbavDynamicfiltersselector1_Jsonclick = "";
         edtavOrdereddsc_Jsonclick = "";
         cmbavOrderedby_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtavSelect_Tooltiptext = "Selecionar";
         edtTipoDocumento_Nome_Titleformat = 0;
         edtLoteArquivoAnexo_Descricao_Titleformat = 0;
         edtLoteArquivoAnexo_TipoArq_Titleformat = 0;
         edtLoteArquivoAnexo_NomeArq_Titleformat = 0;
         edtLoteArquivoAnexo_Data_Titleformat = 0;
         edtLoteArquivoAnexo_LoteCod_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         cmbavDynamicfiltersoperator3.Visible = 1;
         edtavTipodocumento_nome3_Visible = 1;
         edtavLotearquivoanexo_nomearq3_Visible = 1;
         cmbavDynamicfiltersoperator2.Visible = 1;
         edtavTipodocumento_nome2_Visible = 1;
         edtavLotearquivoanexo_nomearq2_Visible = 1;
         cmbavDynamicfiltersoperator1.Visible = 1;
         edtavTipodocumento_nome1_Visible = 1;
         edtavLotearquivoanexo_nomearq1_Visible = 1;
         edtTipoDocumento_Nome_Title = "Nome";
         edtLoteArquivoAnexo_Descricao_Title = "Descri��o";
         edtLoteArquivoAnexo_TipoArq_Title = "de Arquivo";
         edtLoteArquivoAnexo_NomeArq_Title = "do Arquivo";
         edtLoteArquivoAnexo_Data_Title = "Hora Upload";
         edtLoteArquivoAnexo_LoteCod_Title = "Anexo_Lote Cod";
         edtavOrdereddsc_Visible = 1;
         lblJsdynamicfilters_Caption = "JSDynamicFilters";
         edtLoteArquivoAnexo_Arquivo_Filetype = "";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         chkavDynamicfiltersenabled3.Caption = "";
         chkavDynamicfiltersenabled2.Caption = "";
         edtavDdo_tipodocumento_nometitlecontrolidtoreplace_Visible = 1;
         edtavDdo_lotearquivoanexo_descricaotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_lotearquivoanexo_tipoarqtitlecontrolidtoreplace_Visible = 1;
         edtavDdo_lotearquivoanexo_nomearqtitlecontrolidtoreplace_Visible = 1;
         edtavDdo_lotearquivoanexo_datatitlecontrolidtoreplace_Visible = 1;
         edtavDdo_lotearquivoanexo_lotecodtitlecontrolidtoreplace_Visible = 1;
         edtavTftipodocumento_nome_sel_Jsonclick = "";
         edtavTftipodocumento_nome_sel_Visible = 1;
         edtavTftipodocumento_nome_Jsonclick = "";
         edtavTftipodocumento_nome_Visible = 1;
         edtavTflotearquivoanexo_descricao_sel_Visible = 1;
         edtavTflotearquivoanexo_descricao_Visible = 1;
         edtavTflotearquivoanexo_tipoarq_sel_Jsonclick = "";
         edtavTflotearquivoanexo_tipoarq_sel_Visible = 1;
         edtavTflotearquivoanexo_tipoarq_Jsonclick = "";
         edtavTflotearquivoanexo_tipoarq_Visible = 1;
         edtavTflotearquivoanexo_nomearq_sel_Jsonclick = "";
         edtavTflotearquivoanexo_nomearq_sel_Visible = 1;
         edtavTflotearquivoanexo_nomearq_Jsonclick = "";
         edtavTflotearquivoanexo_nomearq_Visible = 1;
         edtavDdo_lotearquivoanexo_dataauxdateto_Jsonclick = "";
         edtavDdo_lotearquivoanexo_dataauxdate_Jsonclick = "";
         edtavTflotearquivoanexo_data_to_Jsonclick = "";
         edtavTflotearquivoanexo_data_to_Visible = 1;
         edtavTflotearquivoanexo_data_Jsonclick = "";
         edtavTflotearquivoanexo_data_Visible = 1;
         edtavTflotearquivoanexo_lotecod_to_Jsonclick = "";
         edtavTflotearquivoanexo_lotecod_to_Visible = 1;
         edtavTflotearquivoanexo_lotecod_Jsonclick = "";
         edtavTflotearquivoanexo_lotecod_Visible = 1;
         chkavDynamicfiltersenabled3.Visible = 1;
         chkavDynamicfiltersenabled2.Visible = 1;
         Ddo_tipodocumento_nome_Searchbuttontext = "Pesquisar";
         Ddo_tipodocumento_nome_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_tipodocumento_nome_Rangefilterto = "At�";
         Ddo_tipodocumento_nome_Rangefilterfrom = "Desde";
         Ddo_tipodocumento_nome_Cleanfilter = "Limpar pesquisa";
         Ddo_tipodocumento_nome_Loadingdata = "Carregando dados...";
         Ddo_tipodocumento_nome_Sortdsc = "Ordenar de Z � A";
         Ddo_tipodocumento_nome_Sortasc = "Ordenar de A � Z";
         Ddo_tipodocumento_nome_Datalistupdateminimumcharacters = 0;
         Ddo_tipodocumento_nome_Datalistproc = "GetPromptLoteArquivoAnexoFilterData";
         Ddo_tipodocumento_nome_Datalistfixedvalues = "";
         Ddo_tipodocumento_nome_Datalisttype = "Dynamic";
         Ddo_tipodocumento_nome_Includedatalist = Convert.ToBoolean( -1);
         Ddo_tipodocumento_nome_Filterisrange = Convert.ToBoolean( 0);
         Ddo_tipodocumento_nome_Filtertype = "Character";
         Ddo_tipodocumento_nome_Includefilter = Convert.ToBoolean( -1);
         Ddo_tipodocumento_nome_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_tipodocumento_nome_Includesortasc = Convert.ToBoolean( -1);
         Ddo_tipodocumento_nome_Titlecontrolidtoreplace = "";
         Ddo_tipodocumento_nome_Dropdownoptionstype = "GridTitleSettings";
         Ddo_tipodocumento_nome_Cls = "ColumnSettings";
         Ddo_tipodocumento_nome_Tooltip = "Op��es";
         Ddo_tipodocumento_nome_Caption = "";
         Ddo_lotearquivoanexo_descricao_Searchbuttontext = "Pesquisar";
         Ddo_lotearquivoanexo_descricao_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_lotearquivoanexo_descricao_Rangefilterto = "At�";
         Ddo_lotearquivoanexo_descricao_Rangefilterfrom = "Desde";
         Ddo_lotearquivoanexo_descricao_Cleanfilter = "Limpar pesquisa";
         Ddo_lotearquivoanexo_descricao_Loadingdata = "Carregando dados...";
         Ddo_lotearquivoanexo_descricao_Sortdsc = "Ordenar de Z � A";
         Ddo_lotearquivoanexo_descricao_Sortasc = "Ordenar de A � Z";
         Ddo_lotearquivoanexo_descricao_Datalistupdateminimumcharacters = 0;
         Ddo_lotearquivoanexo_descricao_Datalistproc = "GetPromptLoteArquivoAnexoFilterData";
         Ddo_lotearquivoanexo_descricao_Datalistfixedvalues = "";
         Ddo_lotearquivoanexo_descricao_Datalisttype = "Dynamic";
         Ddo_lotearquivoanexo_descricao_Includedatalist = Convert.ToBoolean( -1);
         Ddo_lotearquivoanexo_descricao_Filterisrange = Convert.ToBoolean( 0);
         Ddo_lotearquivoanexo_descricao_Filtertype = "Character";
         Ddo_lotearquivoanexo_descricao_Includefilter = Convert.ToBoolean( -1);
         Ddo_lotearquivoanexo_descricao_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_lotearquivoanexo_descricao_Includesortasc = Convert.ToBoolean( -1);
         Ddo_lotearquivoanexo_descricao_Titlecontrolidtoreplace = "";
         Ddo_lotearquivoanexo_descricao_Dropdownoptionstype = "GridTitleSettings";
         Ddo_lotearquivoanexo_descricao_Cls = "ColumnSettings";
         Ddo_lotearquivoanexo_descricao_Tooltip = "Op��es";
         Ddo_lotearquivoanexo_descricao_Caption = "";
         Ddo_lotearquivoanexo_tipoarq_Searchbuttontext = "Pesquisar";
         Ddo_lotearquivoanexo_tipoarq_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_lotearquivoanexo_tipoarq_Rangefilterto = "At�";
         Ddo_lotearquivoanexo_tipoarq_Rangefilterfrom = "Desde";
         Ddo_lotearquivoanexo_tipoarq_Cleanfilter = "Limpar pesquisa";
         Ddo_lotearquivoanexo_tipoarq_Loadingdata = "Carregando dados...";
         Ddo_lotearquivoanexo_tipoarq_Sortdsc = "Ordenar de Z � A";
         Ddo_lotearquivoanexo_tipoarq_Sortasc = "Ordenar de A � Z";
         Ddo_lotearquivoanexo_tipoarq_Datalistupdateminimumcharacters = 0;
         Ddo_lotearquivoanexo_tipoarq_Datalistproc = "GetPromptLoteArquivoAnexoFilterData";
         Ddo_lotearquivoanexo_tipoarq_Datalistfixedvalues = "";
         Ddo_lotearquivoanexo_tipoarq_Datalisttype = "Dynamic";
         Ddo_lotearquivoanexo_tipoarq_Includedatalist = Convert.ToBoolean( -1);
         Ddo_lotearquivoanexo_tipoarq_Filterisrange = Convert.ToBoolean( 0);
         Ddo_lotearquivoanexo_tipoarq_Filtertype = "Character";
         Ddo_lotearquivoanexo_tipoarq_Includefilter = Convert.ToBoolean( -1);
         Ddo_lotearquivoanexo_tipoarq_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_lotearquivoanexo_tipoarq_Includesortasc = Convert.ToBoolean( -1);
         Ddo_lotearquivoanexo_tipoarq_Titlecontrolidtoreplace = "";
         Ddo_lotearquivoanexo_tipoarq_Dropdownoptionstype = "GridTitleSettings";
         Ddo_lotearquivoanexo_tipoarq_Cls = "ColumnSettings";
         Ddo_lotearquivoanexo_tipoarq_Tooltip = "Op��es";
         Ddo_lotearquivoanexo_tipoarq_Caption = "";
         Ddo_lotearquivoanexo_nomearq_Searchbuttontext = "Pesquisar";
         Ddo_lotearquivoanexo_nomearq_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_lotearquivoanexo_nomearq_Rangefilterto = "At�";
         Ddo_lotearquivoanexo_nomearq_Rangefilterfrom = "Desde";
         Ddo_lotearquivoanexo_nomearq_Cleanfilter = "Limpar pesquisa";
         Ddo_lotearquivoanexo_nomearq_Loadingdata = "Carregando dados...";
         Ddo_lotearquivoanexo_nomearq_Sortdsc = "Ordenar de Z � A";
         Ddo_lotearquivoanexo_nomearq_Sortasc = "Ordenar de A � Z";
         Ddo_lotearquivoanexo_nomearq_Datalistupdateminimumcharacters = 0;
         Ddo_lotearquivoanexo_nomearq_Datalistproc = "GetPromptLoteArquivoAnexoFilterData";
         Ddo_lotearquivoanexo_nomearq_Datalistfixedvalues = "";
         Ddo_lotearquivoanexo_nomearq_Datalisttype = "Dynamic";
         Ddo_lotearquivoanexo_nomearq_Includedatalist = Convert.ToBoolean( -1);
         Ddo_lotearquivoanexo_nomearq_Filterisrange = Convert.ToBoolean( 0);
         Ddo_lotearquivoanexo_nomearq_Filtertype = "Character";
         Ddo_lotearquivoanexo_nomearq_Includefilter = Convert.ToBoolean( -1);
         Ddo_lotearquivoanexo_nomearq_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_lotearquivoanexo_nomearq_Includesortasc = Convert.ToBoolean( -1);
         Ddo_lotearquivoanexo_nomearq_Titlecontrolidtoreplace = "";
         Ddo_lotearquivoanexo_nomearq_Dropdownoptionstype = "GridTitleSettings";
         Ddo_lotearquivoanexo_nomearq_Cls = "ColumnSettings";
         Ddo_lotearquivoanexo_nomearq_Tooltip = "Op��es";
         Ddo_lotearquivoanexo_nomearq_Caption = "";
         Ddo_lotearquivoanexo_data_Searchbuttontext = "Pesquisar";
         Ddo_lotearquivoanexo_data_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_lotearquivoanexo_data_Rangefilterto = "At�";
         Ddo_lotearquivoanexo_data_Rangefilterfrom = "Desde";
         Ddo_lotearquivoanexo_data_Cleanfilter = "Limpar pesquisa";
         Ddo_lotearquivoanexo_data_Loadingdata = "Carregando dados...";
         Ddo_lotearquivoanexo_data_Sortdsc = "Ordenar de Z � A";
         Ddo_lotearquivoanexo_data_Sortasc = "Ordenar de A � Z";
         Ddo_lotearquivoanexo_data_Datalistupdateminimumcharacters = 0;
         Ddo_lotearquivoanexo_data_Datalistfixedvalues = "";
         Ddo_lotearquivoanexo_data_Includedatalist = Convert.ToBoolean( 0);
         Ddo_lotearquivoanexo_data_Filterisrange = Convert.ToBoolean( -1);
         Ddo_lotearquivoanexo_data_Filtertype = "Date";
         Ddo_lotearquivoanexo_data_Includefilter = Convert.ToBoolean( -1);
         Ddo_lotearquivoanexo_data_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_lotearquivoanexo_data_Includesortasc = Convert.ToBoolean( -1);
         Ddo_lotearquivoanexo_data_Titlecontrolidtoreplace = "";
         Ddo_lotearquivoanexo_data_Dropdownoptionstype = "GridTitleSettings";
         Ddo_lotearquivoanexo_data_Cls = "ColumnSettings";
         Ddo_lotearquivoanexo_data_Tooltip = "Op��es";
         Ddo_lotearquivoanexo_data_Caption = "";
         Ddo_lotearquivoanexo_lotecod_Searchbuttontext = "Pesquisar";
         Ddo_lotearquivoanexo_lotecod_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_lotearquivoanexo_lotecod_Rangefilterto = "At�";
         Ddo_lotearquivoanexo_lotecod_Rangefilterfrom = "Desde";
         Ddo_lotearquivoanexo_lotecod_Cleanfilter = "Limpar pesquisa";
         Ddo_lotearquivoanexo_lotecod_Loadingdata = "Carregando dados...";
         Ddo_lotearquivoanexo_lotecod_Sortdsc = "Ordenar de Z � A";
         Ddo_lotearquivoanexo_lotecod_Sortasc = "Ordenar de A � Z";
         Ddo_lotearquivoanexo_lotecod_Datalistupdateminimumcharacters = 0;
         Ddo_lotearquivoanexo_lotecod_Datalistfixedvalues = "";
         Ddo_lotearquivoanexo_lotecod_Includedatalist = Convert.ToBoolean( 0);
         Ddo_lotearquivoanexo_lotecod_Filterisrange = Convert.ToBoolean( -1);
         Ddo_lotearquivoanexo_lotecod_Filtertype = "Numeric";
         Ddo_lotearquivoanexo_lotecod_Includefilter = Convert.ToBoolean( -1);
         Ddo_lotearquivoanexo_lotecod_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_lotearquivoanexo_lotecod_Includesortasc = Convert.ToBoolean( -1);
         Ddo_lotearquivoanexo_lotecod_Titlecontrolidtoreplace = "";
         Ddo_lotearquivoanexo_lotecod_Dropdownoptionstype = "GridTitleSettings";
         Ddo_lotearquivoanexo_lotecod_Cls = "ColumnSettings";
         Ddo_lotearquivoanexo_lotecod_Tooltip = "Op��es";
         Ddo_lotearquivoanexo_lotecod_Caption = "";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         Form.Caption = "Selecione Arquivos Anexos";
         subGrid_Rows = 0;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV25DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV37ddo_LoteArquivoAnexo_LoteCodTitleControlIdToReplace',fld:'vDDO_LOTEARQUIVOANEXO_LOTECODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV43ddo_LoteArquivoAnexo_DataTitleControlIdToReplace',fld:'vDDO_LOTEARQUIVOANEXO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_LoteArquivoAnexo_NomeArqTitleControlIdToReplace',fld:'vDDO_LOTEARQUIVOANEXO_NOMEARQTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV51ddo_LoteArquivoAnexo_TipoArqTitleControlIdToReplace',fld:'vDDO_LOTEARQUIVOANEXO_TIPOARQTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV55ddo_LoteArquivoAnexo_DescricaoTitleControlIdToReplace',fld:'vDDO_LOTEARQUIVOANEXO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV59ddo_TipoDocumento_NomeTitleControlIdToReplace',fld:'vDDO_TIPODOCUMENTO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV35TFLoteArquivoAnexo_LoteCod',fld:'vTFLOTEARQUIVOANEXO_LOTECOD',pic:'ZZZZZ9',nv:0},{av:'AV36TFLoteArquivoAnexo_LoteCod_To',fld:'vTFLOTEARQUIVOANEXO_LOTECOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFLoteArquivoAnexo_Data',fld:'vTFLOTEARQUIVOANEXO_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV40TFLoteArquivoAnexo_Data_To',fld:'vTFLOTEARQUIVOANEXO_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV45TFLoteArquivoAnexo_NomeArq',fld:'vTFLOTEARQUIVOANEXO_NOMEARQ',pic:'',nv:''},{av:'AV46TFLoteArquivoAnexo_NomeArq_Sel',fld:'vTFLOTEARQUIVOANEXO_NOMEARQ_SEL',pic:'',nv:''},{av:'AV49TFLoteArquivoAnexo_TipoArq',fld:'vTFLOTEARQUIVOANEXO_TIPOARQ',pic:'',nv:''},{av:'AV50TFLoteArquivoAnexo_TipoArq_Sel',fld:'vTFLOTEARQUIVOANEXO_TIPOARQ_SEL',pic:'',nv:''},{av:'AV53TFLoteArquivoAnexo_Descricao',fld:'vTFLOTEARQUIVOANEXO_DESCRICAO',pic:'',nv:''},{av:'AV54TFLoteArquivoAnexo_Descricao_Sel',fld:'vTFLOTEARQUIVOANEXO_DESCRICAO_SEL',pic:'',nv:''},{av:'AV57TFTipoDocumento_Nome',fld:'vTFTIPODOCUMENTO_NOME',pic:'@!',nv:''},{av:'AV58TFTipoDocumento_Nome_Sel',fld:'vTFTIPODOCUMENTO_NOME_SEL',pic:'@!',nv:''},{av:'AV67Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV31DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV18LoteArquivoAnexo_NomeArq1',fld:'vLOTEARQUIVOANEXO_NOMEARQ1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV19TipoDocumento_Nome1',fld:'vTIPODOCUMENTO_NOME1',pic:'@!',nv:''},{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV23LoteArquivoAnexo_NomeArq2',fld:'vLOTEARQUIVOANEXO_NOMEARQ2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV24TipoDocumento_Nome2',fld:'vTIPODOCUMENTO_NOME2',pic:'@!',nv:''},{av:'AV28LoteArquivoAnexo_NomeArq3',fld:'vLOTEARQUIVOANEXO_NOMEARQ3',pic:'',nv:''},{av:'AV27DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV29TipoDocumento_Nome3',fld:'vTIPODOCUMENTO_NOME3',pic:'@!',nv:''}],oparms:[{av:'AV34LoteArquivoAnexo_LoteCodTitleFilterData',fld:'vLOTEARQUIVOANEXO_LOTECODTITLEFILTERDATA',pic:'',nv:null},{av:'AV38LoteArquivoAnexo_DataTitleFilterData',fld:'vLOTEARQUIVOANEXO_DATATITLEFILTERDATA',pic:'',nv:null},{av:'AV44LoteArquivoAnexo_NomeArqTitleFilterData',fld:'vLOTEARQUIVOANEXO_NOMEARQTITLEFILTERDATA',pic:'',nv:null},{av:'AV48LoteArquivoAnexo_TipoArqTitleFilterData',fld:'vLOTEARQUIVOANEXO_TIPOARQTITLEFILTERDATA',pic:'',nv:null},{av:'AV52LoteArquivoAnexo_DescricaoTitleFilterData',fld:'vLOTEARQUIVOANEXO_DESCRICAOTITLEFILTERDATA',pic:'',nv:null},{av:'AV56TipoDocumento_NomeTitleFilterData',fld:'vTIPODOCUMENTO_NOMETITLEFILTERDATA',pic:'',nv:null},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV27DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'edtLoteArquivoAnexo_LoteCod_Titleformat',ctrl:'LOTEARQUIVOANEXO_LOTECOD',prop:'Titleformat'},{av:'edtLoteArquivoAnexo_LoteCod_Title',ctrl:'LOTEARQUIVOANEXO_LOTECOD',prop:'Title'},{av:'edtLoteArquivoAnexo_Data_Titleformat',ctrl:'LOTEARQUIVOANEXO_DATA',prop:'Titleformat'},{av:'edtLoteArquivoAnexo_Data_Title',ctrl:'LOTEARQUIVOANEXO_DATA',prop:'Title'},{av:'edtLoteArquivoAnexo_NomeArq_Titleformat',ctrl:'LOTEARQUIVOANEXO_NOMEARQ',prop:'Titleformat'},{av:'edtLoteArquivoAnexo_NomeArq_Title',ctrl:'LOTEARQUIVOANEXO_NOMEARQ',prop:'Title'},{av:'edtLoteArquivoAnexo_TipoArq_Titleformat',ctrl:'LOTEARQUIVOANEXO_TIPOARQ',prop:'Titleformat'},{av:'edtLoteArquivoAnexo_TipoArq_Title',ctrl:'LOTEARQUIVOANEXO_TIPOARQ',prop:'Title'},{av:'edtLoteArquivoAnexo_Descricao_Titleformat',ctrl:'LOTEARQUIVOANEXO_DESCRICAO',prop:'Titleformat'},{av:'edtLoteArquivoAnexo_Descricao_Title',ctrl:'LOTEARQUIVOANEXO_DESCRICAO',prop:'Title'},{av:'edtTipoDocumento_Nome_Titleformat',ctrl:'TIPODOCUMENTO_NOME',prop:'Titleformat'},{av:'edtTipoDocumento_Nome_Title',ctrl:'TIPODOCUMENTO_NOME',prop:'Title'},{av:'AV62GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV63GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E11EX2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18LoteArquivoAnexo_NomeArq1',fld:'vLOTEARQUIVOANEXO_NOMEARQ1',pic:'',nv:''},{av:'AV19TipoDocumento_Nome1',fld:'vTIPODOCUMENTO_NOME1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23LoteArquivoAnexo_NomeArq2',fld:'vLOTEARQUIVOANEXO_NOMEARQ2',pic:'',nv:''},{av:'AV24TipoDocumento_Nome2',fld:'vTIPODOCUMENTO_NOME2',pic:'@!',nv:''},{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV27DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV28LoteArquivoAnexo_NomeArq3',fld:'vLOTEARQUIVOANEXO_NOMEARQ3',pic:'',nv:''},{av:'AV29TipoDocumento_Nome3',fld:'vTIPODOCUMENTO_NOME3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV25DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV35TFLoteArquivoAnexo_LoteCod',fld:'vTFLOTEARQUIVOANEXO_LOTECOD',pic:'ZZZZZ9',nv:0},{av:'AV36TFLoteArquivoAnexo_LoteCod_To',fld:'vTFLOTEARQUIVOANEXO_LOTECOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFLoteArquivoAnexo_Data',fld:'vTFLOTEARQUIVOANEXO_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV40TFLoteArquivoAnexo_Data_To',fld:'vTFLOTEARQUIVOANEXO_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV45TFLoteArquivoAnexo_NomeArq',fld:'vTFLOTEARQUIVOANEXO_NOMEARQ',pic:'',nv:''},{av:'AV46TFLoteArquivoAnexo_NomeArq_Sel',fld:'vTFLOTEARQUIVOANEXO_NOMEARQ_SEL',pic:'',nv:''},{av:'AV49TFLoteArquivoAnexo_TipoArq',fld:'vTFLOTEARQUIVOANEXO_TIPOARQ',pic:'',nv:''},{av:'AV50TFLoteArquivoAnexo_TipoArq_Sel',fld:'vTFLOTEARQUIVOANEXO_TIPOARQ_SEL',pic:'',nv:''},{av:'AV53TFLoteArquivoAnexo_Descricao',fld:'vTFLOTEARQUIVOANEXO_DESCRICAO',pic:'',nv:''},{av:'AV54TFLoteArquivoAnexo_Descricao_Sel',fld:'vTFLOTEARQUIVOANEXO_DESCRICAO_SEL',pic:'',nv:''},{av:'AV57TFTipoDocumento_Nome',fld:'vTFTIPODOCUMENTO_NOME',pic:'@!',nv:''},{av:'AV58TFTipoDocumento_Nome_Sel',fld:'vTFTIPODOCUMENTO_NOME_SEL',pic:'@!',nv:''},{av:'AV37ddo_LoteArquivoAnexo_LoteCodTitleControlIdToReplace',fld:'vDDO_LOTEARQUIVOANEXO_LOTECODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV43ddo_LoteArquivoAnexo_DataTitleControlIdToReplace',fld:'vDDO_LOTEARQUIVOANEXO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_LoteArquivoAnexo_NomeArqTitleControlIdToReplace',fld:'vDDO_LOTEARQUIVOANEXO_NOMEARQTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV51ddo_LoteArquivoAnexo_TipoArqTitleControlIdToReplace',fld:'vDDO_LOTEARQUIVOANEXO_TIPOARQTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV55ddo_LoteArquivoAnexo_DescricaoTitleControlIdToReplace',fld:'vDDO_LOTEARQUIVOANEXO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV59ddo_TipoDocumento_NomeTitleControlIdToReplace',fld:'vDDO_TIPODOCUMENTO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV31DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("DDO_LOTEARQUIVOANEXO_LOTECOD.ONOPTIONCLICKED","{handler:'E12EX2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18LoteArquivoAnexo_NomeArq1',fld:'vLOTEARQUIVOANEXO_NOMEARQ1',pic:'',nv:''},{av:'AV19TipoDocumento_Nome1',fld:'vTIPODOCUMENTO_NOME1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23LoteArquivoAnexo_NomeArq2',fld:'vLOTEARQUIVOANEXO_NOMEARQ2',pic:'',nv:''},{av:'AV24TipoDocumento_Nome2',fld:'vTIPODOCUMENTO_NOME2',pic:'@!',nv:''},{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV27DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV28LoteArquivoAnexo_NomeArq3',fld:'vLOTEARQUIVOANEXO_NOMEARQ3',pic:'',nv:''},{av:'AV29TipoDocumento_Nome3',fld:'vTIPODOCUMENTO_NOME3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV25DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV35TFLoteArquivoAnexo_LoteCod',fld:'vTFLOTEARQUIVOANEXO_LOTECOD',pic:'ZZZZZ9',nv:0},{av:'AV36TFLoteArquivoAnexo_LoteCod_To',fld:'vTFLOTEARQUIVOANEXO_LOTECOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFLoteArquivoAnexo_Data',fld:'vTFLOTEARQUIVOANEXO_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV40TFLoteArquivoAnexo_Data_To',fld:'vTFLOTEARQUIVOANEXO_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV45TFLoteArquivoAnexo_NomeArq',fld:'vTFLOTEARQUIVOANEXO_NOMEARQ',pic:'',nv:''},{av:'AV46TFLoteArquivoAnexo_NomeArq_Sel',fld:'vTFLOTEARQUIVOANEXO_NOMEARQ_SEL',pic:'',nv:''},{av:'AV49TFLoteArquivoAnexo_TipoArq',fld:'vTFLOTEARQUIVOANEXO_TIPOARQ',pic:'',nv:''},{av:'AV50TFLoteArquivoAnexo_TipoArq_Sel',fld:'vTFLOTEARQUIVOANEXO_TIPOARQ_SEL',pic:'',nv:''},{av:'AV53TFLoteArquivoAnexo_Descricao',fld:'vTFLOTEARQUIVOANEXO_DESCRICAO',pic:'',nv:''},{av:'AV54TFLoteArquivoAnexo_Descricao_Sel',fld:'vTFLOTEARQUIVOANEXO_DESCRICAO_SEL',pic:'',nv:''},{av:'AV57TFTipoDocumento_Nome',fld:'vTFTIPODOCUMENTO_NOME',pic:'@!',nv:''},{av:'AV58TFTipoDocumento_Nome_Sel',fld:'vTFTIPODOCUMENTO_NOME_SEL',pic:'@!',nv:''},{av:'AV37ddo_LoteArquivoAnexo_LoteCodTitleControlIdToReplace',fld:'vDDO_LOTEARQUIVOANEXO_LOTECODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV43ddo_LoteArquivoAnexo_DataTitleControlIdToReplace',fld:'vDDO_LOTEARQUIVOANEXO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_LoteArquivoAnexo_NomeArqTitleControlIdToReplace',fld:'vDDO_LOTEARQUIVOANEXO_NOMEARQTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV51ddo_LoteArquivoAnexo_TipoArqTitleControlIdToReplace',fld:'vDDO_LOTEARQUIVOANEXO_TIPOARQTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV55ddo_LoteArquivoAnexo_DescricaoTitleControlIdToReplace',fld:'vDDO_LOTEARQUIVOANEXO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV59ddo_TipoDocumento_NomeTitleControlIdToReplace',fld:'vDDO_TIPODOCUMENTO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV31DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_lotearquivoanexo_lotecod_Activeeventkey',ctrl:'DDO_LOTEARQUIVOANEXO_LOTECOD',prop:'ActiveEventKey'},{av:'Ddo_lotearquivoanexo_lotecod_Filteredtext_get',ctrl:'DDO_LOTEARQUIVOANEXO_LOTECOD',prop:'FilteredText_get'},{av:'Ddo_lotearquivoanexo_lotecod_Filteredtextto_get',ctrl:'DDO_LOTEARQUIVOANEXO_LOTECOD',prop:'FilteredTextTo_get'}],oparms:[{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_lotearquivoanexo_lotecod_Sortedstatus',ctrl:'DDO_LOTEARQUIVOANEXO_LOTECOD',prop:'SortedStatus'},{av:'AV35TFLoteArquivoAnexo_LoteCod',fld:'vTFLOTEARQUIVOANEXO_LOTECOD',pic:'ZZZZZ9',nv:0},{av:'AV36TFLoteArquivoAnexo_LoteCod_To',fld:'vTFLOTEARQUIVOANEXO_LOTECOD_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_lotearquivoanexo_data_Sortedstatus',ctrl:'DDO_LOTEARQUIVOANEXO_DATA',prop:'SortedStatus'},{av:'Ddo_lotearquivoanexo_nomearq_Sortedstatus',ctrl:'DDO_LOTEARQUIVOANEXO_NOMEARQ',prop:'SortedStatus'},{av:'Ddo_lotearquivoanexo_tipoarq_Sortedstatus',ctrl:'DDO_LOTEARQUIVOANEXO_TIPOARQ',prop:'SortedStatus'},{av:'Ddo_lotearquivoanexo_descricao_Sortedstatus',ctrl:'DDO_LOTEARQUIVOANEXO_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_tipodocumento_nome_Sortedstatus',ctrl:'DDO_TIPODOCUMENTO_NOME',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_LOTEARQUIVOANEXO_DATA.ONOPTIONCLICKED","{handler:'E13EX2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18LoteArquivoAnexo_NomeArq1',fld:'vLOTEARQUIVOANEXO_NOMEARQ1',pic:'',nv:''},{av:'AV19TipoDocumento_Nome1',fld:'vTIPODOCUMENTO_NOME1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23LoteArquivoAnexo_NomeArq2',fld:'vLOTEARQUIVOANEXO_NOMEARQ2',pic:'',nv:''},{av:'AV24TipoDocumento_Nome2',fld:'vTIPODOCUMENTO_NOME2',pic:'@!',nv:''},{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV27DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV28LoteArquivoAnexo_NomeArq3',fld:'vLOTEARQUIVOANEXO_NOMEARQ3',pic:'',nv:''},{av:'AV29TipoDocumento_Nome3',fld:'vTIPODOCUMENTO_NOME3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV25DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV35TFLoteArquivoAnexo_LoteCod',fld:'vTFLOTEARQUIVOANEXO_LOTECOD',pic:'ZZZZZ9',nv:0},{av:'AV36TFLoteArquivoAnexo_LoteCod_To',fld:'vTFLOTEARQUIVOANEXO_LOTECOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFLoteArquivoAnexo_Data',fld:'vTFLOTEARQUIVOANEXO_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV40TFLoteArquivoAnexo_Data_To',fld:'vTFLOTEARQUIVOANEXO_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV45TFLoteArquivoAnexo_NomeArq',fld:'vTFLOTEARQUIVOANEXO_NOMEARQ',pic:'',nv:''},{av:'AV46TFLoteArquivoAnexo_NomeArq_Sel',fld:'vTFLOTEARQUIVOANEXO_NOMEARQ_SEL',pic:'',nv:''},{av:'AV49TFLoteArquivoAnexo_TipoArq',fld:'vTFLOTEARQUIVOANEXO_TIPOARQ',pic:'',nv:''},{av:'AV50TFLoteArquivoAnexo_TipoArq_Sel',fld:'vTFLOTEARQUIVOANEXO_TIPOARQ_SEL',pic:'',nv:''},{av:'AV53TFLoteArquivoAnexo_Descricao',fld:'vTFLOTEARQUIVOANEXO_DESCRICAO',pic:'',nv:''},{av:'AV54TFLoteArquivoAnexo_Descricao_Sel',fld:'vTFLOTEARQUIVOANEXO_DESCRICAO_SEL',pic:'',nv:''},{av:'AV57TFTipoDocumento_Nome',fld:'vTFTIPODOCUMENTO_NOME',pic:'@!',nv:''},{av:'AV58TFTipoDocumento_Nome_Sel',fld:'vTFTIPODOCUMENTO_NOME_SEL',pic:'@!',nv:''},{av:'AV37ddo_LoteArquivoAnexo_LoteCodTitleControlIdToReplace',fld:'vDDO_LOTEARQUIVOANEXO_LOTECODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV43ddo_LoteArquivoAnexo_DataTitleControlIdToReplace',fld:'vDDO_LOTEARQUIVOANEXO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_LoteArquivoAnexo_NomeArqTitleControlIdToReplace',fld:'vDDO_LOTEARQUIVOANEXO_NOMEARQTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV51ddo_LoteArquivoAnexo_TipoArqTitleControlIdToReplace',fld:'vDDO_LOTEARQUIVOANEXO_TIPOARQTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV55ddo_LoteArquivoAnexo_DescricaoTitleControlIdToReplace',fld:'vDDO_LOTEARQUIVOANEXO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV59ddo_TipoDocumento_NomeTitleControlIdToReplace',fld:'vDDO_TIPODOCUMENTO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV31DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_lotearquivoanexo_data_Activeeventkey',ctrl:'DDO_LOTEARQUIVOANEXO_DATA',prop:'ActiveEventKey'},{av:'Ddo_lotearquivoanexo_data_Filteredtext_get',ctrl:'DDO_LOTEARQUIVOANEXO_DATA',prop:'FilteredText_get'},{av:'Ddo_lotearquivoanexo_data_Filteredtextto_get',ctrl:'DDO_LOTEARQUIVOANEXO_DATA',prop:'FilteredTextTo_get'}],oparms:[{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_lotearquivoanexo_data_Sortedstatus',ctrl:'DDO_LOTEARQUIVOANEXO_DATA',prop:'SortedStatus'},{av:'AV39TFLoteArquivoAnexo_Data',fld:'vTFLOTEARQUIVOANEXO_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV40TFLoteArquivoAnexo_Data_To',fld:'vTFLOTEARQUIVOANEXO_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'Ddo_lotearquivoanexo_lotecod_Sortedstatus',ctrl:'DDO_LOTEARQUIVOANEXO_LOTECOD',prop:'SortedStatus'},{av:'Ddo_lotearquivoanexo_nomearq_Sortedstatus',ctrl:'DDO_LOTEARQUIVOANEXO_NOMEARQ',prop:'SortedStatus'},{av:'Ddo_lotearquivoanexo_tipoarq_Sortedstatus',ctrl:'DDO_LOTEARQUIVOANEXO_TIPOARQ',prop:'SortedStatus'},{av:'Ddo_lotearquivoanexo_descricao_Sortedstatus',ctrl:'DDO_LOTEARQUIVOANEXO_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_tipodocumento_nome_Sortedstatus',ctrl:'DDO_TIPODOCUMENTO_NOME',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_LOTEARQUIVOANEXO_NOMEARQ.ONOPTIONCLICKED","{handler:'E14EX2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18LoteArquivoAnexo_NomeArq1',fld:'vLOTEARQUIVOANEXO_NOMEARQ1',pic:'',nv:''},{av:'AV19TipoDocumento_Nome1',fld:'vTIPODOCUMENTO_NOME1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23LoteArquivoAnexo_NomeArq2',fld:'vLOTEARQUIVOANEXO_NOMEARQ2',pic:'',nv:''},{av:'AV24TipoDocumento_Nome2',fld:'vTIPODOCUMENTO_NOME2',pic:'@!',nv:''},{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV27DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV28LoteArquivoAnexo_NomeArq3',fld:'vLOTEARQUIVOANEXO_NOMEARQ3',pic:'',nv:''},{av:'AV29TipoDocumento_Nome3',fld:'vTIPODOCUMENTO_NOME3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV25DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV35TFLoteArquivoAnexo_LoteCod',fld:'vTFLOTEARQUIVOANEXO_LOTECOD',pic:'ZZZZZ9',nv:0},{av:'AV36TFLoteArquivoAnexo_LoteCod_To',fld:'vTFLOTEARQUIVOANEXO_LOTECOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFLoteArquivoAnexo_Data',fld:'vTFLOTEARQUIVOANEXO_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV40TFLoteArquivoAnexo_Data_To',fld:'vTFLOTEARQUIVOANEXO_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV45TFLoteArquivoAnexo_NomeArq',fld:'vTFLOTEARQUIVOANEXO_NOMEARQ',pic:'',nv:''},{av:'AV46TFLoteArquivoAnexo_NomeArq_Sel',fld:'vTFLOTEARQUIVOANEXO_NOMEARQ_SEL',pic:'',nv:''},{av:'AV49TFLoteArquivoAnexo_TipoArq',fld:'vTFLOTEARQUIVOANEXO_TIPOARQ',pic:'',nv:''},{av:'AV50TFLoteArquivoAnexo_TipoArq_Sel',fld:'vTFLOTEARQUIVOANEXO_TIPOARQ_SEL',pic:'',nv:''},{av:'AV53TFLoteArquivoAnexo_Descricao',fld:'vTFLOTEARQUIVOANEXO_DESCRICAO',pic:'',nv:''},{av:'AV54TFLoteArquivoAnexo_Descricao_Sel',fld:'vTFLOTEARQUIVOANEXO_DESCRICAO_SEL',pic:'',nv:''},{av:'AV57TFTipoDocumento_Nome',fld:'vTFTIPODOCUMENTO_NOME',pic:'@!',nv:''},{av:'AV58TFTipoDocumento_Nome_Sel',fld:'vTFTIPODOCUMENTO_NOME_SEL',pic:'@!',nv:''},{av:'AV37ddo_LoteArquivoAnexo_LoteCodTitleControlIdToReplace',fld:'vDDO_LOTEARQUIVOANEXO_LOTECODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV43ddo_LoteArquivoAnexo_DataTitleControlIdToReplace',fld:'vDDO_LOTEARQUIVOANEXO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_LoteArquivoAnexo_NomeArqTitleControlIdToReplace',fld:'vDDO_LOTEARQUIVOANEXO_NOMEARQTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV51ddo_LoteArquivoAnexo_TipoArqTitleControlIdToReplace',fld:'vDDO_LOTEARQUIVOANEXO_TIPOARQTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV55ddo_LoteArquivoAnexo_DescricaoTitleControlIdToReplace',fld:'vDDO_LOTEARQUIVOANEXO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV59ddo_TipoDocumento_NomeTitleControlIdToReplace',fld:'vDDO_TIPODOCUMENTO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV31DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_lotearquivoanexo_nomearq_Activeeventkey',ctrl:'DDO_LOTEARQUIVOANEXO_NOMEARQ',prop:'ActiveEventKey'},{av:'Ddo_lotearquivoanexo_nomearq_Filteredtext_get',ctrl:'DDO_LOTEARQUIVOANEXO_NOMEARQ',prop:'FilteredText_get'},{av:'Ddo_lotearquivoanexo_nomearq_Selectedvalue_get',ctrl:'DDO_LOTEARQUIVOANEXO_NOMEARQ',prop:'SelectedValue_get'}],oparms:[{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_lotearquivoanexo_nomearq_Sortedstatus',ctrl:'DDO_LOTEARQUIVOANEXO_NOMEARQ',prop:'SortedStatus'},{av:'AV45TFLoteArquivoAnexo_NomeArq',fld:'vTFLOTEARQUIVOANEXO_NOMEARQ',pic:'',nv:''},{av:'AV46TFLoteArquivoAnexo_NomeArq_Sel',fld:'vTFLOTEARQUIVOANEXO_NOMEARQ_SEL',pic:'',nv:''},{av:'Ddo_lotearquivoanexo_lotecod_Sortedstatus',ctrl:'DDO_LOTEARQUIVOANEXO_LOTECOD',prop:'SortedStatus'},{av:'Ddo_lotearquivoanexo_data_Sortedstatus',ctrl:'DDO_LOTEARQUIVOANEXO_DATA',prop:'SortedStatus'},{av:'Ddo_lotearquivoanexo_tipoarq_Sortedstatus',ctrl:'DDO_LOTEARQUIVOANEXO_TIPOARQ',prop:'SortedStatus'},{av:'Ddo_lotearquivoanexo_descricao_Sortedstatus',ctrl:'DDO_LOTEARQUIVOANEXO_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_tipodocumento_nome_Sortedstatus',ctrl:'DDO_TIPODOCUMENTO_NOME',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_LOTEARQUIVOANEXO_TIPOARQ.ONOPTIONCLICKED","{handler:'E15EX2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18LoteArquivoAnexo_NomeArq1',fld:'vLOTEARQUIVOANEXO_NOMEARQ1',pic:'',nv:''},{av:'AV19TipoDocumento_Nome1',fld:'vTIPODOCUMENTO_NOME1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23LoteArquivoAnexo_NomeArq2',fld:'vLOTEARQUIVOANEXO_NOMEARQ2',pic:'',nv:''},{av:'AV24TipoDocumento_Nome2',fld:'vTIPODOCUMENTO_NOME2',pic:'@!',nv:''},{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV27DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV28LoteArquivoAnexo_NomeArq3',fld:'vLOTEARQUIVOANEXO_NOMEARQ3',pic:'',nv:''},{av:'AV29TipoDocumento_Nome3',fld:'vTIPODOCUMENTO_NOME3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV25DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV35TFLoteArquivoAnexo_LoteCod',fld:'vTFLOTEARQUIVOANEXO_LOTECOD',pic:'ZZZZZ9',nv:0},{av:'AV36TFLoteArquivoAnexo_LoteCod_To',fld:'vTFLOTEARQUIVOANEXO_LOTECOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFLoteArquivoAnexo_Data',fld:'vTFLOTEARQUIVOANEXO_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV40TFLoteArquivoAnexo_Data_To',fld:'vTFLOTEARQUIVOANEXO_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV45TFLoteArquivoAnexo_NomeArq',fld:'vTFLOTEARQUIVOANEXO_NOMEARQ',pic:'',nv:''},{av:'AV46TFLoteArquivoAnexo_NomeArq_Sel',fld:'vTFLOTEARQUIVOANEXO_NOMEARQ_SEL',pic:'',nv:''},{av:'AV49TFLoteArquivoAnexo_TipoArq',fld:'vTFLOTEARQUIVOANEXO_TIPOARQ',pic:'',nv:''},{av:'AV50TFLoteArquivoAnexo_TipoArq_Sel',fld:'vTFLOTEARQUIVOANEXO_TIPOARQ_SEL',pic:'',nv:''},{av:'AV53TFLoteArquivoAnexo_Descricao',fld:'vTFLOTEARQUIVOANEXO_DESCRICAO',pic:'',nv:''},{av:'AV54TFLoteArquivoAnexo_Descricao_Sel',fld:'vTFLOTEARQUIVOANEXO_DESCRICAO_SEL',pic:'',nv:''},{av:'AV57TFTipoDocumento_Nome',fld:'vTFTIPODOCUMENTO_NOME',pic:'@!',nv:''},{av:'AV58TFTipoDocumento_Nome_Sel',fld:'vTFTIPODOCUMENTO_NOME_SEL',pic:'@!',nv:''},{av:'AV37ddo_LoteArquivoAnexo_LoteCodTitleControlIdToReplace',fld:'vDDO_LOTEARQUIVOANEXO_LOTECODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV43ddo_LoteArquivoAnexo_DataTitleControlIdToReplace',fld:'vDDO_LOTEARQUIVOANEXO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_LoteArquivoAnexo_NomeArqTitleControlIdToReplace',fld:'vDDO_LOTEARQUIVOANEXO_NOMEARQTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV51ddo_LoteArquivoAnexo_TipoArqTitleControlIdToReplace',fld:'vDDO_LOTEARQUIVOANEXO_TIPOARQTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV55ddo_LoteArquivoAnexo_DescricaoTitleControlIdToReplace',fld:'vDDO_LOTEARQUIVOANEXO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV59ddo_TipoDocumento_NomeTitleControlIdToReplace',fld:'vDDO_TIPODOCUMENTO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV31DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_lotearquivoanexo_tipoarq_Activeeventkey',ctrl:'DDO_LOTEARQUIVOANEXO_TIPOARQ',prop:'ActiveEventKey'},{av:'Ddo_lotearquivoanexo_tipoarq_Filteredtext_get',ctrl:'DDO_LOTEARQUIVOANEXO_TIPOARQ',prop:'FilteredText_get'},{av:'Ddo_lotearquivoanexo_tipoarq_Selectedvalue_get',ctrl:'DDO_LOTEARQUIVOANEXO_TIPOARQ',prop:'SelectedValue_get'}],oparms:[{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_lotearquivoanexo_tipoarq_Sortedstatus',ctrl:'DDO_LOTEARQUIVOANEXO_TIPOARQ',prop:'SortedStatus'},{av:'AV49TFLoteArquivoAnexo_TipoArq',fld:'vTFLOTEARQUIVOANEXO_TIPOARQ',pic:'',nv:''},{av:'AV50TFLoteArquivoAnexo_TipoArq_Sel',fld:'vTFLOTEARQUIVOANEXO_TIPOARQ_SEL',pic:'',nv:''},{av:'Ddo_lotearquivoanexo_lotecod_Sortedstatus',ctrl:'DDO_LOTEARQUIVOANEXO_LOTECOD',prop:'SortedStatus'},{av:'Ddo_lotearquivoanexo_data_Sortedstatus',ctrl:'DDO_LOTEARQUIVOANEXO_DATA',prop:'SortedStatus'},{av:'Ddo_lotearquivoanexo_nomearq_Sortedstatus',ctrl:'DDO_LOTEARQUIVOANEXO_NOMEARQ',prop:'SortedStatus'},{av:'Ddo_lotearquivoanexo_descricao_Sortedstatus',ctrl:'DDO_LOTEARQUIVOANEXO_DESCRICAO',prop:'SortedStatus'},{av:'Ddo_tipodocumento_nome_Sortedstatus',ctrl:'DDO_TIPODOCUMENTO_NOME',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_LOTEARQUIVOANEXO_DESCRICAO.ONOPTIONCLICKED","{handler:'E16EX2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18LoteArquivoAnexo_NomeArq1',fld:'vLOTEARQUIVOANEXO_NOMEARQ1',pic:'',nv:''},{av:'AV19TipoDocumento_Nome1',fld:'vTIPODOCUMENTO_NOME1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23LoteArquivoAnexo_NomeArq2',fld:'vLOTEARQUIVOANEXO_NOMEARQ2',pic:'',nv:''},{av:'AV24TipoDocumento_Nome2',fld:'vTIPODOCUMENTO_NOME2',pic:'@!',nv:''},{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV27DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV28LoteArquivoAnexo_NomeArq3',fld:'vLOTEARQUIVOANEXO_NOMEARQ3',pic:'',nv:''},{av:'AV29TipoDocumento_Nome3',fld:'vTIPODOCUMENTO_NOME3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV25DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV35TFLoteArquivoAnexo_LoteCod',fld:'vTFLOTEARQUIVOANEXO_LOTECOD',pic:'ZZZZZ9',nv:0},{av:'AV36TFLoteArquivoAnexo_LoteCod_To',fld:'vTFLOTEARQUIVOANEXO_LOTECOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFLoteArquivoAnexo_Data',fld:'vTFLOTEARQUIVOANEXO_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV40TFLoteArquivoAnexo_Data_To',fld:'vTFLOTEARQUIVOANEXO_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV45TFLoteArquivoAnexo_NomeArq',fld:'vTFLOTEARQUIVOANEXO_NOMEARQ',pic:'',nv:''},{av:'AV46TFLoteArquivoAnexo_NomeArq_Sel',fld:'vTFLOTEARQUIVOANEXO_NOMEARQ_SEL',pic:'',nv:''},{av:'AV49TFLoteArquivoAnexo_TipoArq',fld:'vTFLOTEARQUIVOANEXO_TIPOARQ',pic:'',nv:''},{av:'AV50TFLoteArquivoAnexo_TipoArq_Sel',fld:'vTFLOTEARQUIVOANEXO_TIPOARQ_SEL',pic:'',nv:''},{av:'AV53TFLoteArquivoAnexo_Descricao',fld:'vTFLOTEARQUIVOANEXO_DESCRICAO',pic:'',nv:''},{av:'AV54TFLoteArquivoAnexo_Descricao_Sel',fld:'vTFLOTEARQUIVOANEXO_DESCRICAO_SEL',pic:'',nv:''},{av:'AV57TFTipoDocumento_Nome',fld:'vTFTIPODOCUMENTO_NOME',pic:'@!',nv:''},{av:'AV58TFTipoDocumento_Nome_Sel',fld:'vTFTIPODOCUMENTO_NOME_SEL',pic:'@!',nv:''},{av:'AV37ddo_LoteArquivoAnexo_LoteCodTitleControlIdToReplace',fld:'vDDO_LOTEARQUIVOANEXO_LOTECODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV43ddo_LoteArquivoAnexo_DataTitleControlIdToReplace',fld:'vDDO_LOTEARQUIVOANEXO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_LoteArquivoAnexo_NomeArqTitleControlIdToReplace',fld:'vDDO_LOTEARQUIVOANEXO_NOMEARQTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV51ddo_LoteArquivoAnexo_TipoArqTitleControlIdToReplace',fld:'vDDO_LOTEARQUIVOANEXO_TIPOARQTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV55ddo_LoteArquivoAnexo_DescricaoTitleControlIdToReplace',fld:'vDDO_LOTEARQUIVOANEXO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV59ddo_TipoDocumento_NomeTitleControlIdToReplace',fld:'vDDO_TIPODOCUMENTO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV31DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_lotearquivoanexo_descricao_Activeeventkey',ctrl:'DDO_LOTEARQUIVOANEXO_DESCRICAO',prop:'ActiveEventKey'},{av:'Ddo_lotearquivoanexo_descricao_Filteredtext_get',ctrl:'DDO_LOTEARQUIVOANEXO_DESCRICAO',prop:'FilteredText_get'},{av:'Ddo_lotearquivoanexo_descricao_Selectedvalue_get',ctrl:'DDO_LOTEARQUIVOANEXO_DESCRICAO',prop:'SelectedValue_get'}],oparms:[{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_lotearquivoanexo_descricao_Sortedstatus',ctrl:'DDO_LOTEARQUIVOANEXO_DESCRICAO',prop:'SortedStatus'},{av:'AV53TFLoteArquivoAnexo_Descricao',fld:'vTFLOTEARQUIVOANEXO_DESCRICAO',pic:'',nv:''},{av:'AV54TFLoteArquivoAnexo_Descricao_Sel',fld:'vTFLOTEARQUIVOANEXO_DESCRICAO_SEL',pic:'',nv:''},{av:'Ddo_lotearquivoanexo_lotecod_Sortedstatus',ctrl:'DDO_LOTEARQUIVOANEXO_LOTECOD',prop:'SortedStatus'},{av:'Ddo_lotearquivoanexo_data_Sortedstatus',ctrl:'DDO_LOTEARQUIVOANEXO_DATA',prop:'SortedStatus'},{av:'Ddo_lotearquivoanexo_nomearq_Sortedstatus',ctrl:'DDO_LOTEARQUIVOANEXO_NOMEARQ',prop:'SortedStatus'},{av:'Ddo_lotearquivoanexo_tipoarq_Sortedstatus',ctrl:'DDO_LOTEARQUIVOANEXO_TIPOARQ',prop:'SortedStatus'},{av:'Ddo_tipodocumento_nome_Sortedstatus',ctrl:'DDO_TIPODOCUMENTO_NOME',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_TIPODOCUMENTO_NOME.ONOPTIONCLICKED","{handler:'E17EX2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18LoteArquivoAnexo_NomeArq1',fld:'vLOTEARQUIVOANEXO_NOMEARQ1',pic:'',nv:''},{av:'AV19TipoDocumento_Nome1',fld:'vTIPODOCUMENTO_NOME1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23LoteArquivoAnexo_NomeArq2',fld:'vLOTEARQUIVOANEXO_NOMEARQ2',pic:'',nv:''},{av:'AV24TipoDocumento_Nome2',fld:'vTIPODOCUMENTO_NOME2',pic:'@!',nv:''},{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV27DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV28LoteArquivoAnexo_NomeArq3',fld:'vLOTEARQUIVOANEXO_NOMEARQ3',pic:'',nv:''},{av:'AV29TipoDocumento_Nome3',fld:'vTIPODOCUMENTO_NOME3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV25DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV35TFLoteArquivoAnexo_LoteCod',fld:'vTFLOTEARQUIVOANEXO_LOTECOD',pic:'ZZZZZ9',nv:0},{av:'AV36TFLoteArquivoAnexo_LoteCod_To',fld:'vTFLOTEARQUIVOANEXO_LOTECOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFLoteArquivoAnexo_Data',fld:'vTFLOTEARQUIVOANEXO_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV40TFLoteArquivoAnexo_Data_To',fld:'vTFLOTEARQUIVOANEXO_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV45TFLoteArquivoAnexo_NomeArq',fld:'vTFLOTEARQUIVOANEXO_NOMEARQ',pic:'',nv:''},{av:'AV46TFLoteArquivoAnexo_NomeArq_Sel',fld:'vTFLOTEARQUIVOANEXO_NOMEARQ_SEL',pic:'',nv:''},{av:'AV49TFLoteArquivoAnexo_TipoArq',fld:'vTFLOTEARQUIVOANEXO_TIPOARQ',pic:'',nv:''},{av:'AV50TFLoteArquivoAnexo_TipoArq_Sel',fld:'vTFLOTEARQUIVOANEXO_TIPOARQ_SEL',pic:'',nv:''},{av:'AV53TFLoteArquivoAnexo_Descricao',fld:'vTFLOTEARQUIVOANEXO_DESCRICAO',pic:'',nv:''},{av:'AV54TFLoteArquivoAnexo_Descricao_Sel',fld:'vTFLOTEARQUIVOANEXO_DESCRICAO_SEL',pic:'',nv:''},{av:'AV57TFTipoDocumento_Nome',fld:'vTFTIPODOCUMENTO_NOME',pic:'@!',nv:''},{av:'AV58TFTipoDocumento_Nome_Sel',fld:'vTFTIPODOCUMENTO_NOME_SEL',pic:'@!',nv:''},{av:'AV37ddo_LoteArquivoAnexo_LoteCodTitleControlIdToReplace',fld:'vDDO_LOTEARQUIVOANEXO_LOTECODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV43ddo_LoteArquivoAnexo_DataTitleControlIdToReplace',fld:'vDDO_LOTEARQUIVOANEXO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_LoteArquivoAnexo_NomeArqTitleControlIdToReplace',fld:'vDDO_LOTEARQUIVOANEXO_NOMEARQTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV51ddo_LoteArquivoAnexo_TipoArqTitleControlIdToReplace',fld:'vDDO_LOTEARQUIVOANEXO_TIPOARQTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV55ddo_LoteArquivoAnexo_DescricaoTitleControlIdToReplace',fld:'vDDO_LOTEARQUIVOANEXO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV59ddo_TipoDocumento_NomeTitleControlIdToReplace',fld:'vDDO_TIPODOCUMENTO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV31DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'Ddo_tipodocumento_nome_Activeeventkey',ctrl:'DDO_TIPODOCUMENTO_NOME',prop:'ActiveEventKey'},{av:'Ddo_tipodocumento_nome_Filteredtext_get',ctrl:'DDO_TIPODOCUMENTO_NOME',prop:'FilteredText_get'},{av:'Ddo_tipodocumento_nome_Selectedvalue_get',ctrl:'DDO_TIPODOCUMENTO_NOME',prop:'SelectedValue_get'}],oparms:[{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_tipodocumento_nome_Sortedstatus',ctrl:'DDO_TIPODOCUMENTO_NOME',prop:'SortedStatus'},{av:'AV57TFTipoDocumento_Nome',fld:'vTFTIPODOCUMENTO_NOME',pic:'@!',nv:''},{av:'AV58TFTipoDocumento_Nome_Sel',fld:'vTFTIPODOCUMENTO_NOME_SEL',pic:'@!',nv:''},{av:'Ddo_lotearquivoanexo_lotecod_Sortedstatus',ctrl:'DDO_LOTEARQUIVOANEXO_LOTECOD',prop:'SortedStatus'},{av:'Ddo_lotearquivoanexo_data_Sortedstatus',ctrl:'DDO_LOTEARQUIVOANEXO_DATA',prop:'SortedStatus'},{av:'Ddo_lotearquivoanexo_nomearq_Sortedstatus',ctrl:'DDO_LOTEARQUIVOANEXO_NOMEARQ',prop:'SortedStatus'},{av:'Ddo_lotearquivoanexo_tipoarq_Sortedstatus',ctrl:'DDO_LOTEARQUIVOANEXO_TIPOARQ',prop:'SortedStatus'},{av:'Ddo_lotearquivoanexo_descricao_Sortedstatus',ctrl:'DDO_LOTEARQUIVOANEXO_DESCRICAO',prop:'SortedStatus'}]}");
         setEventMetadata("GRID.LOAD","{handler:'E30EX2',iparms:[],oparms:[{av:'AV32Select',fld:'vSELECT',pic:'',nv:''},{av:'edtavSelect_Tooltiptext',ctrl:'vSELECT',prop:'Tooltiptext'}]}");
         setEventMetadata("ENTER","{handler:'E31EX2',iparms:[{av:'A841LoteArquivoAnexo_LoteCod',fld:'LOTEARQUIVOANEXO_LOTECOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A836LoteArquivoAnexo_Data',fld:'LOTEARQUIVOANEXO_DATA',pic:'99/99/99 99:99',hsh:true,nv:''},{av:'A839LoteArquivoAnexo_NomeArq',fld:'LOTEARQUIVOANEXO_NOMEARQ',pic:'',nv:''}],oparms:[{av:'AV7InOutLoteArquivoAnexo_LoteCod',fld:'vINOUTLOTEARQUIVOANEXO_LOTECOD',pic:'ZZZZZ9',nv:0},{av:'AV8InOutLoteArquivoAnexo_Data',fld:'vINOUTLOTEARQUIVOANEXO_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV9InOutLoteArquivoAnexo_NomeArq',fld:'vINOUTLOTEARQUIVOANEXO_NOMEARQ',pic:'',nv:''}]}");
         setEventMetadata("VORDEREDBY.CLICK","{handler:'E18EX2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18LoteArquivoAnexo_NomeArq1',fld:'vLOTEARQUIVOANEXO_NOMEARQ1',pic:'',nv:''},{av:'AV19TipoDocumento_Nome1',fld:'vTIPODOCUMENTO_NOME1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23LoteArquivoAnexo_NomeArq2',fld:'vLOTEARQUIVOANEXO_NOMEARQ2',pic:'',nv:''},{av:'AV24TipoDocumento_Nome2',fld:'vTIPODOCUMENTO_NOME2',pic:'@!',nv:''},{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV27DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV28LoteArquivoAnexo_NomeArq3',fld:'vLOTEARQUIVOANEXO_NOMEARQ3',pic:'',nv:''},{av:'AV29TipoDocumento_Nome3',fld:'vTIPODOCUMENTO_NOME3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV25DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV35TFLoteArquivoAnexo_LoteCod',fld:'vTFLOTEARQUIVOANEXO_LOTECOD',pic:'ZZZZZ9',nv:0},{av:'AV36TFLoteArquivoAnexo_LoteCod_To',fld:'vTFLOTEARQUIVOANEXO_LOTECOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFLoteArquivoAnexo_Data',fld:'vTFLOTEARQUIVOANEXO_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV40TFLoteArquivoAnexo_Data_To',fld:'vTFLOTEARQUIVOANEXO_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV45TFLoteArquivoAnexo_NomeArq',fld:'vTFLOTEARQUIVOANEXO_NOMEARQ',pic:'',nv:''},{av:'AV46TFLoteArquivoAnexo_NomeArq_Sel',fld:'vTFLOTEARQUIVOANEXO_NOMEARQ_SEL',pic:'',nv:''},{av:'AV49TFLoteArquivoAnexo_TipoArq',fld:'vTFLOTEARQUIVOANEXO_TIPOARQ',pic:'',nv:''},{av:'AV50TFLoteArquivoAnexo_TipoArq_Sel',fld:'vTFLOTEARQUIVOANEXO_TIPOARQ_SEL',pic:'',nv:''},{av:'AV53TFLoteArquivoAnexo_Descricao',fld:'vTFLOTEARQUIVOANEXO_DESCRICAO',pic:'',nv:''},{av:'AV54TFLoteArquivoAnexo_Descricao_Sel',fld:'vTFLOTEARQUIVOANEXO_DESCRICAO_SEL',pic:'',nv:''},{av:'AV57TFTipoDocumento_Nome',fld:'vTFTIPODOCUMENTO_NOME',pic:'@!',nv:''},{av:'AV58TFTipoDocumento_Nome_Sel',fld:'vTFTIPODOCUMENTO_NOME_SEL',pic:'@!',nv:''},{av:'AV37ddo_LoteArquivoAnexo_LoteCodTitleControlIdToReplace',fld:'vDDO_LOTEARQUIVOANEXO_LOTECODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV43ddo_LoteArquivoAnexo_DataTitleControlIdToReplace',fld:'vDDO_LOTEARQUIVOANEXO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_LoteArquivoAnexo_NomeArqTitleControlIdToReplace',fld:'vDDO_LOTEARQUIVOANEXO_NOMEARQTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV51ddo_LoteArquivoAnexo_TipoArqTitleControlIdToReplace',fld:'vDDO_LOTEARQUIVOANEXO_TIPOARQTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV55ddo_LoteArquivoAnexo_DescricaoTitleControlIdToReplace',fld:'vDDO_LOTEARQUIVOANEXO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV59ddo_TipoDocumento_NomeTitleControlIdToReplace',fld:'vDDO_TIPODOCUMENTO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV31DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[]}");
         setEventMetadata("'ADDDYNAMICFILTERS1'","{handler:'E23EX2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18LoteArquivoAnexo_NomeArq1',fld:'vLOTEARQUIVOANEXO_NOMEARQ1',pic:'',nv:''},{av:'AV19TipoDocumento_Nome1',fld:'vTIPODOCUMENTO_NOME1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23LoteArquivoAnexo_NomeArq2',fld:'vLOTEARQUIVOANEXO_NOMEARQ2',pic:'',nv:''},{av:'AV24TipoDocumento_Nome2',fld:'vTIPODOCUMENTO_NOME2',pic:'@!',nv:''},{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV27DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV28LoteArquivoAnexo_NomeArq3',fld:'vLOTEARQUIVOANEXO_NOMEARQ3',pic:'',nv:''},{av:'AV29TipoDocumento_Nome3',fld:'vTIPODOCUMENTO_NOME3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV25DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV35TFLoteArquivoAnexo_LoteCod',fld:'vTFLOTEARQUIVOANEXO_LOTECOD',pic:'ZZZZZ9',nv:0},{av:'AV36TFLoteArquivoAnexo_LoteCod_To',fld:'vTFLOTEARQUIVOANEXO_LOTECOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFLoteArquivoAnexo_Data',fld:'vTFLOTEARQUIVOANEXO_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV40TFLoteArquivoAnexo_Data_To',fld:'vTFLOTEARQUIVOANEXO_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV45TFLoteArquivoAnexo_NomeArq',fld:'vTFLOTEARQUIVOANEXO_NOMEARQ',pic:'',nv:''},{av:'AV46TFLoteArquivoAnexo_NomeArq_Sel',fld:'vTFLOTEARQUIVOANEXO_NOMEARQ_SEL',pic:'',nv:''},{av:'AV49TFLoteArquivoAnexo_TipoArq',fld:'vTFLOTEARQUIVOANEXO_TIPOARQ',pic:'',nv:''},{av:'AV50TFLoteArquivoAnexo_TipoArq_Sel',fld:'vTFLOTEARQUIVOANEXO_TIPOARQ_SEL',pic:'',nv:''},{av:'AV53TFLoteArquivoAnexo_Descricao',fld:'vTFLOTEARQUIVOANEXO_DESCRICAO',pic:'',nv:''},{av:'AV54TFLoteArquivoAnexo_Descricao_Sel',fld:'vTFLOTEARQUIVOANEXO_DESCRICAO_SEL',pic:'',nv:''},{av:'AV57TFTipoDocumento_Nome',fld:'vTFTIPODOCUMENTO_NOME',pic:'@!',nv:''},{av:'AV58TFTipoDocumento_Nome_Sel',fld:'vTFTIPODOCUMENTO_NOME_SEL',pic:'@!',nv:''},{av:'AV37ddo_LoteArquivoAnexo_LoteCodTitleControlIdToReplace',fld:'vDDO_LOTEARQUIVOANEXO_LOTECODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV43ddo_LoteArquivoAnexo_DataTitleControlIdToReplace',fld:'vDDO_LOTEARQUIVOANEXO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_LoteArquivoAnexo_NomeArqTitleControlIdToReplace',fld:'vDDO_LOTEARQUIVOANEXO_NOMEARQTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV51ddo_LoteArquivoAnexo_TipoArqTitleControlIdToReplace',fld:'vDDO_LOTEARQUIVOANEXO_TIPOARQTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV55ddo_LoteArquivoAnexo_DescricaoTitleControlIdToReplace',fld:'vDDO_LOTEARQUIVOANEXO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV59ddo_TipoDocumento_NomeTitleControlIdToReplace',fld:'vDDO_TIPODOCUMENTO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV31DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS1'","{handler:'E19EX2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18LoteArquivoAnexo_NomeArq1',fld:'vLOTEARQUIVOANEXO_NOMEARQ1',pic:'',nv:''},{av:'AV19TipoDocumento_Nome1',fld:'vTIPODOCUMENTO_NOME1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23LoteArquivoAnexo_NomeArq2',fld:'vLOTEARQUIVOANEXO_NOMEARQ2',pic:'',nv:''},{av:'AV24TipoDocumento_Nome2',fld:'vTIPODOCUMENTO_NOME2',pic:'@!',nv:''},{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV27DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV28LoteArquivoAnexo_NomeArq3',fld:'vLOTEARQUIVOANEXO_NOMEARQ3',pic:'',nv:''},{av:'AV29TipoDocumento_Nome3',fld:'vTIPODOCUMENTO_NOME3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV25DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV35TFLoteArquivoAnexo_LoteCod',fld:'vTFLOTEARQUIVOANEXO_LOTECOD',pic:'ZZZZZ9',nv:0},{av:'AV36TFLoteArquivoAnexo_LoteCod_To',fld:'vTFLOTEARQUIVOANEXO_LOTECOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFLoteArquivoAnexo_Data',fld:'vTFLOTEARQUIVOANEXO_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV40TFLoteArquivoAnexo_Data_To',fld:'vTFLOTEARQUIVOANEXO_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV45TFLoteArquivoAnexo_NomeArq',fld:'vTFLOTEARQUIVOANEXO_NOMEARQ',pic:'',nv:''},{av:'AV46TFLoteArquivoAnexo_NomeArq_Sel',fld:'vTFLOTEARQUIVOANEXO_NOMEARQ_SEL',pic:'',nv:''},{av:'AV49TFLoteArquivoAnexo_TipoArq',fld:'vTFLOTEARQUIVOANEXO_TIPOARQ',pic:'',nv:''},{av:'AV50TFLoteArquivoAnexo_TipoArq_Sel',fld:'vTFLOTEARQUIVOANEXO_TIPOARQ_SEL',pic:'',nv:''},{av:'AV53TFLoteArquivoAnexo_Descricao',fld:'vTFLOTEARQUIVOANEXO_DESCRICAO',pic:'',nv:''},{av:'AV54TFLoteArquivoAnexo_Descricao_Sel',fld:'vTFLOTEARQUIVOANEXO_DESCRICAO_SEL',pic:'',nv:''},{av:'AV57TFTipoDocumento_Nome',fld:'vTFTIPODOCUMENTO_NOME',pic:'@!',nv:''},{av:'AV58TFTipoDocumento_Nome_Sel',fld:'vTFTIPODOCUMENTO_NOME_SEL',pic:'@!',nv:''},{av:'AV37ddo_LoteArquivoAnexo_LoteCodTitleControlIdToReplace',fld:'vDDO_LOTEARQUIVOANEXO_LOTECODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV43ddo_LoteArquivoAnexo_DataTitleControlIdToReplace',fld:'vDDO_LOTEARQUIVOANEXO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_LoteArquivoAnexo_NomeArqTitleControlIdToReplace',fld:'vDDO_LOTEARQUIVOANEXO_NOMEARQTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV51ddo_LoteArquivoAnexo_TipoArqTitleControlIdToReplace',fld:'vDDO_LOTEARQUIVOANEXO_TIPOARQTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV55ddo_LoteArquivoAnexo_DescricaoTitleControlIdToReplace',fld:'vDDO_LOTEARQUIVOANEXO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV59ddo_TipoDocumento_NomeTitleControlIdToReplace',fld:'vDDO_TIPODOCUMENTO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV31DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV31DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23LoteArquivoAnexo_NomeArq2',fld:'vLOTEARQUIVOANEXO_NOMEARQ2',pic:'',nv:''},{av:'AV25DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV27DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV28LoteArquivoAnexo_NomeArq3',fld:'vLOTEARQUIVOANEXO_NOMEARQ3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18LoteArquivoAnexo_NomeArq1',fld:'vLOTEARQUIVOANEXO_NOMEARQ1',pic:'',nv:''},{av:'AV19TipoDocumento_Nome1',fld:'vTIPODOCUMENTO_NOME1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV24TipoDocumento_Nome2',fld:'vTIPODOCUMENTO_NOME2',pic:'@!',nv:''},{av:'AV29TipoDocumento_Nome3',fld:'vTIPODOCUMENTO_NOME3',pic:'@!',nv:''},{av:'edtavLotearquivoanexo_nomearq2_Visible',ctrl:'vLOTEARQUIVOANEXO_NOMEARQ2',prop:'Visible'},{av:'edtavTipodocumento_nome2_Visible',ctrl:'vTIPODOCUMENTO_NOME2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavLotearquivoanexo_nomearq3_Visible',ctrl:'vLOTEARQUIVOANEXO_NOMEARQ3',prop:'Visible'},{av:'edtavTipodocumento_nome3_Visible',ctrl:'vTIPODOCUMENTO_NOME3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'},{av:'edtavLotearquivoanexo_nomearq1_Visible',ctrl:'vLOTEARQUIVOANEXO_NOMEARQ1',prop:'Visible'},{av:'edtavTipodocumento_nome1_Visible',ctrl:'vTIPODOCUMENTO_NOME1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR1.CLICK","{handler:'E24EX2',iparms:[{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''}],oparms:[{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'edtavLotearquivoanexo_nomearq1_Visible',ctrl:'vLOTEARQUIVOANEXO_NOMEARQ1',prop:'Visible'},{av:'edtavTipodocumento_nome1_Visible',ctrl:'vTIPODOCUMENTO_NOME1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("'ADDDYNAMICFILTERS2'","{handler:'E25EX2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18LoteArquivoAnexo_NomeArq1',fld:'vLOTEARQUIVOANEXO_NOMEARQ1',pic:'',nv:''},{av:'AV19TipoDocumento_Nome1',fld:'vTIPODOCUMENTO_NOME1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23LoteArquivoAnexo_NomeArq2',fld:'vLOTEARQUIVOANEXO_NOMEARQ2',pic:'',nv:''},{av:'AV24TipoDocumento_Nome2',fld:'vTIPODOCUMENTO_NOME2',pic:'@!',nv:''},{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV27DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV28LoteArquivoAnexo_NomeArq3',fld:'vLOTEARQUIVOANEXO_NOMEARQ3',pic:'',nv:''},{av:'AV29TipoDocumento_Nome3',fld:'vTIPODOCUMENTO_NOME3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV25DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV35TFLoteArquivoAnexo_LoteCod',fld:'vTFLOTEARQUIVOANEXO_LOTECOD',pic:'ZZZZZ9',nv:0},{av:'AV36TFLoteArquivoAnexo_LoteCod_To',fld:'vTFLOTEARQUIVOANEXO_LOTECOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFLoteArquivoAnexo_Data',fld:'vTFLOTEARQUIVOANEXO_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV40TFLoteArquivoAnexo_Data_To',fld:'vTFLOTEARQUIVOANEXO_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV45TFLoteArquivoAnexo_NomeArq',fld:'vTFLOTEARQUIVOANEXO_NOMEARQ',pic:'',nv:''},{av:'AV46TFLoteArquivoAnexo_NomeArq_Sel',fld:'vTFLOTEARQUIVOANEXO_NOMEARQ_SEL',pic:'',nv:''},{av:'AV49TFLoteArquivoAnexo_TipoArq',fld:'vTFLOTEARQUIVOANEXO_TIPOARQ',pic:'',nv:''},{av:'AV50TFLoteArquivoAnexo_TipoArq_Sel',fld:'vTFLOTEARQUIVOANEXO_TIPOARQ_SEL',pic:'',nv:''},{av:'AV53TFLoteArquivoAnexo_Descricao',fld:'vTFLOTEARQUIVOANEXO_DESCRICAO',pic:'',nv:''},{av:'AV54TFLoteArquivoAnexo_Descricao_Sel',fld:'vTFLOTEARQUIVOANEXO_DESCRICAO_SEL',pic:'',nv:''},{av:'AV57TFTipoDocumento_Nome',fld:'vTFTIPODOCUMENTO_NOME',pic:'@!',nv:''},{av:'AV58TFTipoDocumento_Nome_Sel',fld:'vTFTIPODOCUMENTO_NOME_SEL',pic:'@!',nv:''},{av:'AV37ddo_LoteArquivoAnexo_LoteCodTitleControlIdToReplace',fld:'vDDO_LOTEARQUIVOANEXO_LOTECODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV43ddo_LoteArquivoAnexo_DataTitleControlIdToReplace',fld:'vDDO_LOTEARQUIVOANEXO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_LoteArquivoAnexo_NomeArqTitleControlIdToReplace',fld:'vDDO_LOTEARQUIVOANEXO_NOMEARQTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV51ddo_LoteArquivoAnexo_TipoArqTitleControlIdToReplace',fld:'vDDO_LOTEARQUIVOANEXO_TIPOARQTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV55ddo_LoteArquivoAnexo_DescricaoTitleControlIdToReplace',fld:'vDDO_LOTEARQUIVOANEXO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV59ddo_TipoDocumento_NomeTitleControlIdToReplace',fld:'vDDO_TIPODOCUMENTO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV31DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV25DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS2'","{handler:'E20EX2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18LoteArquivoAnexo_NomeArq1',fld:'vLOTEARQUIVOANEXO_NOMEARQ1',pic:'',nv:''},{av:'AV19TipoDocumento_Nome1',fld:'vTIPODOCUMENTO_NOME1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23LoteArquivoAnexo_NomeArq2',fld:'vLOTEARQUIVOANEXO_NOMEARQ2',pic:'',nv:''},{av:'AV24TipoDocumento_Nome2',fld:'vTIPODOCUMENTO_NOME2',pic:'@!',nv:''},{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV27DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV28LoteArquivoAnexo_NomeArq3',fld:'vLOTEARQUIVOANEXO_NOMEARQ3',pic:'',nv:''},{av:'AV29TipoDocumento_Nome3',fld:'vTIPODOCUMENTO_NOME3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV25DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV35TFLoteArquivoAnexo_LoteCod',fld:'vTFLOTEARQUIVOANEXO_LOTECOD',pic:'ZZZZZ9',nv:0},{av:'AV36TFLoteArquivoAnexo_LoteCod_To',fld:'vTFLOTEARQUIVOANEXO_LOTECOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFLoteArquivoAnexo_Data',fld:'vTFLOTEARQUIVOANEXO_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV40TFLoteArquivoAnexo_Data_To',fld:'vTFLOTEARQUIVOANEXO_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV45TFLoteArquivoAnexo_NomeArq',fld:'vTFLOTEARQUIVOANEXO_NOMEARQ',pic:'',nv:''},{av:'AV46TFLoteArquivoAnexo_NomeArq_Sel',fld:'vTFLOTEARQUIVOANEXO_NOMEARQ_SEL',pic:'',nv:''},{av:'AV49TFLoteArquivoAnexo_TipoArq',fld:'vTFLOTEARQUIVOANEXO_TIPOARQ',pic:'',nv:''},{av:'AV50TFLoteArquivoAnexo_TipoArq_Sel',fld:'vTFLOTEARQUIVOANEXO_TIPOARQ_SEL',pic:'',nv:''},{av:'AV53TFLoteArquivoAnexo_Descricao',fld:'vTFLOTEARQUIVOANEXO_DESCRICAO',pic:'',nv:''},{av:'AV54TFLoteArquivoAnexo_Descricao_Sel',fld:'vTFLOTEARQUIVOANEXO_DESCRICAO_SEL',pic:'',nv:''},{av:'AV57TFTipoDocumento_Nome',fld:'vTFTIPODOCUMENTO_NOME',pic:'@!',nv:''},{av:'AV58TFTipoDocumento_Nome_Sel',fld:'vTFTIPODOCUMENTO_NOME_SEL',pic:'@!',nv:''},{av:'AV37ddo_LoteArquivoAnexo_LoteCodTitleControlIdToReplace',fld:'vDDO_LOTEARQUIVOANEXO_LOTECODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV43ddo_LoteArquivoAnexo_DataTitleControlIdToReplace',fld:'vDDO_LOTEARQUIVOANEXO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_LoteArquivoAnexo_NomeArqTitleControlIdToReplace',fld:'vDDO_LOTEARQUIVOANEXO_NOMEARQTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV51ddo_LoteArquivoAnexo_TipoArqTitleControlIdToReplace',fld:'vDDO_LOTEARQUIVOANEXO_TIPOARQTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV55ddo_LoteArquivoAnexo_DescricaoTitleControlIdToReplace',fld:'vDDO_LOTEARQUIVOANEXO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV59ddo_TipoDocumento_NomeTitleControlIdToReplace',fld:'vDDO_TIPODOCUMENTO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV31DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23LoteArquivoAnexo_NomeArq2',fld:'vLOTEARQUIVOANEXO_NOMEARQ2',pic:'',nv:''},{av:'AV25DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV27DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV28LoteArquivoAnexo_NomeArq3',fld:'vLOTEARQUIVOANEXO_NOMEARQ3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18LoteArquivoAnexo_NomeArq1',fld:'vLOTEARQUIVOANEXO_NOMEARQ1',pic:'',nv:''},{av:'AV19TipoDocumento_Nome1',fld:'vTIPODOCUMENTO_NOME1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV24TipoDocumento_Nome2',fld:'vTIPODOCUMENTO_NOME2',pic:'@!',nv:''},{av:'AV29TipoDocumento_Nome3',fld:'vTIPODOCUMENTO_NOME3',pic:'@!',nv:''},{av:'edtavLotearquivoanexo_nomearq2_Visible',ctrl:'vLOTEARQUIVOANEXO_NOMEARQ2',prop:'Visible'},{av:'edtavTipodocumento_nome2_Visible',ctrl:'vTIPODOCUMENTO_NOME2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavLotearquivoanexo_nomearq3_Visible',ctrl:'vLOTEARQUIVOANEXO_NOMEARQ3',prop:'Visible'},{av:'edtavTipodocumento_nome3_Visible',ctrl:'vTIPODOCUMENTO_NOME3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'},{av:'edtavLotearquivoanexo_nomearq1_Visible',ctrl:'vLOTEARQUIVOANEXO_NOMEARQ1',prop:'Visible'},{av:'edtavTipodocumento_nome1_Visible',ctrl:'vTIPODOCUMENTO_NOME1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR2.CLICK","{handler:'E26EX2',iparms:[{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''}],oparms:[{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'edtavLotearquivoanexo_nomearq2_Visible',ctrl:'vLOTEARQUIVOANEXO_NOMEARQ2',prop:'Visible'},{av:'edtavTipodocumento_nome2_Visible',ctrl:'vTIPODOCUMENTO_NOME2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS3'","{handler:'E21EX2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18LoteArquivoAnexo_NomeArq1',fld:'vLOTEARQUIVOANEXO_NOMEARQ1',pic:'',nv:''},{av:'AV19TipoDocumento_Nome1',fld:'vTIPODOCUMENTO_NOME1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23LoteArquivoAnexo_NomeArq2',fld:'vLOTEARQUIVOANEXO_NOMEARQ2',pic:'',nv:''},{av:'AV24TipoDocumento_Nome2',fld:'vTIPODOCUMENTO_NOME2',pic:'@!',nv:''},{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV27DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV28LoteArquivoAnexo_NomeArq3',fld:'vLOTEARQUIVOANEXO_NOMEARQ3',pic:'',nv:''},{av:'AV29TipoDocumento_Nome3',fld:'vTIPODOCUMENTO_NOME3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV25DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV35TFLoteArquivoAnexo_LoteCod',fld:'vTFLOTEARQUIVOANEXO_LOTECOD',pic:'ZZZZZ9',nv:0},{av:'AV36TFLoteArquivoAnexo_LoteCod_To',fld:'vTFLOTEARQUIVOANEXO_LOTECOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFLoteArquivoAnexo_Data',fld:'vTFLOTEARQUIVOANEXO_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV40TFLoteArquivoAnexo_Data_To',fld:'vTFLOTEARQUIVOANEXO_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV45TFLoteArquivoAnexo_NomeArq',fld:'vTFLOTEARQUIVOANEXO_NOMEARQ',pic:'',nv:''},{av:'AV46TFLoteArquivoAnexo_NomeArq_Sel',fld:'vTFLOTEARQUIVOANEXO_NOMEARQ_SEL',pic:'',nv:''},{av:'AV49TFLoteArquivoAnexo_TipoArq',fld:'vTFLOTEARQUIVOANEXO_TIPOARQ',pic:'',nv:''},{av:'AV50TFLoteArquivoAnexo_TipoArq_Sel',fld:'vTFLOTEARQUIVOANEXO_TIPOARQ_SEL',pic:'',nv:''},{av:'AV53TFLoteArquivoAnexo_Descricao',fld:'vTFLOTEARQUIVOANEXO_DESCRICAO',pic:'',nv:''},{av:'AV54TFLoteArquivoAnexo_Descricao_Sel',fld:'vTFLOTEARQUIVOANEXO_DESCRICAO_SEL',pic:'',nv:''},{av:'AV57TFTipoDocumento_Nome',fld:'vTFTIPODOCUMENTO_NOME',pic:'@!',nv:''},{av:'AV58TFTipoDocumento_Nome_Sel',fld:'vTFTIPODOCUMENTO_NOME_SEL',pic:'@!',nv:''},{av:'AV37ddo_LoteArquivoAnexo_LoteCodTitleControlIdToReplace',fld:'vDDO_LOTEARQUIVOANEXO_LOTECODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV43ddo_LoteArquivoAnexo_DataTitleControlIdToReplace',fld:'vDDO_LOTEARQUIVOANEXO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_LoteArquivoAnexo_NomeArqTitleControlIdToReplace',fld:'vDDO_LOTEARQUIVOANEXO_NOMEARQTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV51ddo_LoteArquivoAnexo_TipoArqTitleControlIdToReplace',fld:'vDDO_LOTEARQUIVOANEXO_TIPOARQTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV55ddo_LoteArquivoAnexo_DescricaoTitleControlIdToReplace',fld:'vDDO_LOTEARQUIVOANEXO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV59ddo_TipoDocumento_NomeTitleControlIdToReplace',fld:'vDDO_TIPODOCUMENTO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV31DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV25DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23LoteArquivoAnexo_NomeArq2',fld:'vLOTEARQUIVOANEXO_NOMEARQ2',pic:'',nv:''},{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV27DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV28LoteArquivoAnexo_NomeArq3',fld:'vLOTEARQUIVOANEXO_NOMEARQ3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18LoteArquivoAnexo_NomeArq1',fld:'vLOTEARQUIVOANEXO_NOMEARQ1',pic:'',nv:''},{av:'AV19TipoDocumento_Nome1',fld:'vTIPODOCUMENTO_NOME1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV24TipoDocumento_Nome2',fld:'vTIPODOCUMENTO_NOME2',pic:'@!',nv:''},{av:'AV29TipoDocumento_Nome3',fld:'vTIPODOCUMENTO_NOME3',pic:'@!',nv:''},{av:'edtavLotearquivoanexo_nomearq2_Visible',ctrl:'vLOTEARQUIVOANEXO_NOMEARQ2',prop:'Visible'},{av:'edtavTipodocumento_nome2_Visible',ctrl:'vTIPODOCUMENTO_NOME2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavLotearquivoanexo_nomearq3_Visible',ctrl:'vLOTEARQUIVOANEXO_NOMEARQ3',prop:'Visible'},{av:'edtavTipodocumento_nome3_Visible',ctrl:'vTIPODOCUMENTO_NOME3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'},{av:'edtavLotearquivoanexo_nomearq1_Visible',ctrl:'vLOTEARQUIVOANEXO_NOMEARQ1',prop:'Visible'},{av:'edtavTipodocumento_nome1_Visible',ctrl:'vTIPODOCUMENTO_NOME1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR3.CLICK","{handler:'E27EX2',iparms:[{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''}],oparms:[{av:'AV27DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'edtavLotearquivoanexo_nomearq3_Visible',ctrl:'vLOTEARQUIVOANEXO_NOMEARQ3',prop:'Visible'},{av:'edtavTipodocumento_nome3_Visible',ctrl:'vTIPODOCUMENTO_NOME3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'}]}");
         setEventMetadata("'DOCLEANFILTERS'","{handler:'E22EX2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV15OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18LoteArquivoAnexo_NomeArq1',fld:'vLOTEARQUIVOANEXO_NOMEARQ1',pic:'',nv:''},{av:'AV19TipoDocumento_Nome1',fld:'vTIPODOCUMENTO_NOME1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23LoteArquivoAnexo_NomeArq2',fld:'vLOTEARQUIVOANEXO_NOMEARQ2',pic:'',nv:''},{av:'AV24TipoDocumento_Nome2',fld:'vTIPODOCUMENTO_NOME2',pic:'@!',nv:''},{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV27DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV28LoteArquivoAnexo_NomeArq3',fld:'vLOTEARQUIVOANEXO_NOMEARQ3',pic:'',nv:''},{av:'AV29TipoDocumento_Nome3',fld:'vTIPODOCUMENTO_NOME3',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV25DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV35TFLoteArquivoAnexo_LoteCod',fld:'vTFLOTEARQUIVOANEXO_LOTECOD',pic:'ZZZZZ9',nv:0},{av:'AV36TFLoteArquivoAnexo_LoteCod_To',fld:'vTFLOTEARQUIVOANEXO_LOTECOD_TO',pic:'ZZZZZ9',nv:0},{av:'AV39TFLoteArquivoAnexo_Data',fld:'vTFLOTEARQUIVOANEXO_DATA',pic:'99/99/99 99:99',nv:''},{av:'AV40TFLoteArquivoAnexo_Data_To',fld:'vTFLOTEARQUIVOANEXO_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'AV45TFLoteArquivoAnexo_NomeArq',fld:'vTFLOTEARQUIVOANEXO_NOMEARQ',pic:'',nv:''},{av:'AV46TFLoteArquivoAnexo_NomeArq_Sel',fld:'vTFLOTEARQUIVOANEXO_NOMEARQ_SEL',pic:'',nv:''},{av:'AV49TFLoteArquivoAnexo_TipoArq',fld:'vTFLOTEARQUIVOANEXO_TIPOARQ',pic:'',nv:''},{av:'AV50TFLoteArquivoAnexo_TipoArq_Sel',fld:'vTFLOTEARQUIVOANEXO_TIPOARQ_SEL',pic:'',nv:''},{av:'AV53TFLoteArquivoAnexo_Descricao',fld:'vTFLOTEARQUIVOANEXO_DESCRICAO',pic:'',nv:''},{av:'AV54TFLoteArquivoAnexo_Descricao_Sel',fld:'vTFLOTEARQUIVOANEXO_DESCRICAO_SEL',pic:'',nv:''},{av:'AV57TFTipoDocumento_Nome',fld:'vTFTIPODOCUMENTO_NOME',pic:'@!',nv:''},{av:'AV58TFTipoDocumento_Nome_Sel',fld:'vTFTIPODOCUMENTO_NOME_SEL',pic:'@!',nv:''},{av:'AV37ddo_LoteArquivoAnexo_LoteCodTitleControlIdToReplace',fld:'vDDO_LOTEARQUIVOANEXO_LOTECODTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV43ddo_LoteArquivoAnexo_DataTitleControlIdToReplace',fld:'vDDO_LOTEARQUIVOANEXO_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV47ddo_LoteArquivoAnexo_NomeArqTitleControlIdToReplace',fld:'vDDO_LOTEARQUIVOANEXO_NOMEARQTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV51ddo_LoteArquivoAnexo_TipoArqTitleControlIdToReplace',fld:'vDDO_LOTEARQUIVOANEXO_TIPOARQTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV55ddo_LoteArquivoAnexo_DescricaoTitleControlIdToReplace',fld:'vDDO_LOTEARQUIVOANEXO_DESCRICAOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV59ddo_TipoDocumento_NomeTitleControlIdToReplace',fld:'vDDO_TIPODOCUMENTO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV67Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV31DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV30DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV35TFLoteArquivoAnexo_LoteCod',fld:'vTFLOTEARQUIVOANEXO_LOTECOD',pic:'ZZZZZ9',nv:0},{av:'Ddo_lotearquivoanexo_lotecod_Filteredtext_set',ctrl:'DDO_LOTEARQUIVOANEXO_LOTECOD',prop:'FilteredText_set'},{av:'AV36TFLoteArquivoAnexo_LoteCod_To',fld:'vTFLOTEARQUIVOANEXO_LOTECOD_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_lotearquivoanexo_lotecod_Filteredtextto_set',ctrl:'DDO_LOTEARQUIVOANEXO_LOTECOD',prop:'FilteredTextTo_set'},{av:'AV39TFLoteArquivoAnexo_Data',fld:'vTFLOTEARQUIVOANEXO_DATA',pic:'99/99/99 99:99',nv:''},{av:'Ddo_lotearquivoanexo_data_Filteredtext_set',ctrl:'DDO_LOTEARQUIVOANEXO_DATA',prop:'FilteredText_set'},{av:'AV40TFLoteArquivoAnexo_Data_To',fld:'vTFLOTEARQUIVOANEXO_DATA_TO',pic:'99/99/99 99:99',nv:''},{av:'Ddo_lotearquivoanexo_data_Filteredtextto_set',ctrl:'DDO_LOTEARQUIVOANEXO_DATA',prop:'FilteredTextTo_set'},{av:'AV45TFLoteArquivoAnexo_NomeArq',fld:'vTFLOTEARQUIVOANEXO_NOMEARQ',pic:'',nv:''},{av:'Ddo_lotearquivoanexo_nomearq_Filteredtext_set',ctrl:'DDO_LOTEARQUIVOANEXO_NOMEARQ',prop:'FilteredText_set'},{av:'AV46TFLoteArquivoAnexo_NomeArq_Sel',fld:'vTFLOTEARQUIVOANEXO_NOMEARQ_SEL',pic:'',nv:''},{av:'Ddo_lotearquivoanexo_nomearq_Selectedvalue_set',ctrl:'DDO_LOTEARQUIVOANEXO_NOMEARQ',prop:'SelectedValue_set'},{av:'AV49TFLoteArquivoAnexo_TipoArq',fld:'vTFLOTEARQUIVOANEXO_TIPOARQ',pic:'',nv:''},{av:'Ddo_lotearquivoanexo_tipoarq_Filteredtext_set',ctrl:'DDO_LOTEARQUIVOANEXO_TIPOARQ',prop:'FilteredText_set'},{av:'AV50TFLoteArquivoAnexo_TipoArq_Sel',fld:'vTFLOTEARQUIVOANEXO_TIPOARQ_SEL',pic:'',nv:''},{av:'Ddo_lotearquivoanexo_tipoarq_Selectedvalue_set',ctrl:'DDO_LOTEARQUIVOANEXO_TIPOARQ',prop:'SelectedValue_set'},{av:'AV53TFLoteArquivoAnexo_Descricao',fld:'vTFLOTEARQUIVOANEXO_DESCRICAO',pic:'',nv:''},{av:'Ddo_lotearquivoanexo_descricao_Filteredtext_set',ctrl:'DDO_LOTEARQUIVOANEXO_DESCRICAO',prop:'FilteredText_set'},{av:'AV54TFLoteArquivoAnexo_Descricao_Sel',fld:'vTFLOTEARQUIVOANEXO_DESCRICAO_SEL',pic:'',nv:''},{av:'Ddo_lotearquivoanexo_descricao_Selectedvalue_set',ctrl:'DDO_LOTEARQUIVOANEXO_DESCRICAO',prop:'SelectedValue_set'},{av:'AV57TFTipoDocumento_Nome',fld:'vTFTIPODOCUMENTO_NOME',pic:'@!',nv:''},{av:'Ddo_tipodocumento_nome_Filteredtext_set',ctrl:'DDO_TIPODOCUMENTO_NOME',prop:'FilteredText_set'},{av:'AV58TFTipoDocumento_Nome_Sel',fld:'vTFTIPODOCUMENTO_NOME_SEL',pic:'@!',nv:''},{av:'Ddo_tipodocumento_nome_Selectedvalue_set',ctrl:'DDO_TIPODOCUMENTO_NOME',prop:'SelectedValue_set'},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV18LoteArquivoAnexo_NomeArq1',fld:'vLOTEARQUIVOANEXO_NOMEARQ1',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'edtavLotearquivoanexo_nomearq1_Visible',ctrl:'vLOTEARQUIVOANEXO_NOMEARQ1',prop:'Visible'},{av:'edtavTipodocumento_nome1_Visible',ctrl:'vTIPODOCUMENTO_NOME1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV23LoteArquivoAnexo_NomeArq2',fld:'vLOTEARQUIVOANEXO_NOMEARQ2',pic:'',nv:''},{av:'AV25DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV26DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV27DynamicFiltersOperator3',fld:'vDYNAMICFILTERSOPERATOR3',pic:'ZZZ9',nv:0},{av:'AV28LoteArquivoAnexo_NomeArq3',fld:'vLOTEARQUIVOANEXO_NOMEARQ3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV19TipoDocumento_Nome1',fld:'vTIPODOCUMENTO_NOME1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV24TipoDocumento_Nome2',fld:'vTIPODOCUMENTO_NOME2',pic:'@!',nv:''},{av:'AV29TipoDocumento_Nome3',fld:'vTIPODOCUMENTO_NOME3',pic:'@!',nv:''},{av:'edtavLotearquivoanexo_nomearq2_Visible',ctrl:'vLOTEARQUIVOANEXO_NOMEARQ2',prop:'Visible'},{av:'edtavTipodocumento_nome2_Visible',ctrl:'vTIPODOCUMENTO_NOME2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'edtavLotearquivoanexo_nomearq3_Visible',ctrl:'vLOTEARQUIVOANEXO_NOMEARQ3',prop:'Visible'},{av:'edtavTipodocumento_nome3_Visible',ctrl:'vTIPODOCUMENTO_NOME3',prop:'Visible'},{av:'cmbavDynamicfiltersoperator3'}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         wcpOAV8InOutLoteArquivoAnexo_Data = (DateTime)(DateTime.MinValue);
         wcpOAV9InOutLoteArquivoAnexo_NomeArq = "";
         Gridpaginationbar_Selectedpage = "";
         Ddo_lotearquivoanexo_lotecod_Activeeventkey = "";
         Ddo_lotearquivoanexo_lotecod_Filteredtext_get = "";
         Ddo_lotearquivoanexo_lotecod_Filteredtextto_get = "";
         Ddo_lotearquivoanexo_data_Activeeventkey = "";
         Ddo_lotearquivoanexo_data_Filteredtext_get = "";
         Ddo_lotearquivoanexo_data_Filteredtextto_get = "";
         Ddo_lotearquivoanexo_nomearq_Activeeventkey = "";
         Ddo_lotearquivoanexo_nomearq_Filteredtext_get = "";
         Ddo_lotearquivoanexo_nomearq_Selectedvalue_get = "";
         Ddo_lotearquivoanexo_tipoarq_Activeeventkey = "";
         Ddo_lotearquivoanexo_tipoarq_Filteredtext_get = "";
         Ddo_lotearquivoanexo_tipoarq_Selectedvalue_get = "";
         Ddo_lotearquivoanexo_descricao_Activeeventkey = "";
         Ddo_lotearquivoanexo_descricao_Filteredtext_get = "";
         Ddo_lotearquivoanexo_descricao_Selectedvalue_get = "";
         Ddo_tipodocumento_nome_Activeeventkey = "";
         Ddo_tipodocumento_nome_Filteredtext_get = "";
         Ddo_tipodocumento_nome_Selectedvalue_get = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV16DynamicFiltersSelector1 = "";
         AV18LoteArquivoAnexo_NomeArq1 = "";
         AV19TipoDocumento_Nome1 = "";
         AV21DynamicFiltersSelector2 = "";
         AV23LoteArquivoAnexo_NomeArq2 = "";
         AV24TipoDocumento_Nome2 = "";
         AV26DynamicFiltersSelector3 = "";
         AV28LoteArquivoAnexo_NomeArq3 = "";
         AV29TipoDocumento_Nome3 = "";
         AV39TFLoteArquivoAnexo_Data = DateTimeUtil.ServerNow( context, "DEFAULT");
         AV40TFLoteArquivoAnexo_Data_To = DateTimeUtil.ServerNow( context, "DEFAULT");
         AV45TFLoteArquivoAnexo_NomeArq = "";
         AV46TFLoteArquivoAnexo_NomeArq_Sel = "";
         AV49TFLoteArquivoAnexo_TipoArq = "";
         AV50TFLoteArquivoAnexo_TipoArq_Sel = "";
         AV53TFLoteArquivoAnexo_Descricao = "";
         AV54TFLoteArquivoAnexo_Descricao_Sel = "";
         AV57TFTipoDocumento_Nome = "";
         AV58TFTipoDocumento_Nome_Sel = "";
         AV37ddo_LoteArquivoAnexo_LoteCodTitleControlIdToReplace = "";
         AV43ddo_LoteArquivoAnexo_DataTitleControlIdToReplace = "";
         AV47ddo_LoteArquivoAnexo_NomeArqTitleControlIdToReplace = "";
         AV51ddo_LoteArquivoAnexo_TipoArqTitleControlIdToReplace = "";
         AV55ddo_LoteArquivoAnexo_DescricaoTitleControlIdToReplace = "";
         AV59ddo_TipoDocumento_NomeTitleControlIdToReplace = "";
         AV67Pgmname = "";
         AV11GridState = new wwpbaseobjects.SdtWWPGridState(context);
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV60DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV34LoteArquivoAnexo_LoteCodTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV38LoteArquivoAnexo_DataTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV44LoteArquivoAnexo_NomeArqTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV48LoteArquivoAnexo_TipoArqTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV52LoteArquivoAnexo_DescricaoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV56TipoDocumento_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         Ddo_lotearquivoanexo_lotecod_Filteredtext_set = "";
         Ddo_lotearquivoanexo_lotecod_Filteredtextto_set = "";
         Ddo_lotearquivoanexo_lotecod_Sortedstatus = "";
         Ddo_lotearquivoanexo_data_Filteredtext_set = "";
         Ddo_lotearquivoanexo_data_Filteredtextto_set = "";
         Ddo_lotearquivoanexo_data_Sortedstatus = "";
         Ddo_lotearquivoanexo_nomearq_Filteredtext_set = "";
         Ddo_lotearquivoanexo_nomearq_Selectedvalue_set = "";
         Ddo_lotearquivoanexo_nomearq_Sortedstatus = "";
         Ddo_lotearquivoanexo_tipoarq_Filteredtext_set = "";
         Ddo_lotearquivoanexo_tipoarq_Selectedvalue_set = "";
         Ddo_lotearquivoanexo_tipoarq_Sortedstatus = "";
         Ddo_lotearquivoanexo_descricao_Filteredtext_set = "";
         Ddo_lotearquivoanexo_descricao_Selectedvalue_set = "";
         Ddo_lotearquivoanexo_descricao_Sortedstatus = "";
         Ddo_tipodocumento_nome_Filteredtext_set = "";
         Ddo_tipodocumento_nome_Selectedvalue_set = "";
         Ddo_tipodocumento_nome_Sortedstatus = "";
         GX_FocusControl = "";
         sPrefix = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         AV41DDO_LoteArquivoAnexo_DataAuxDate = DateTime.MinValue;
         AV42DDO_LoteArquivoAnexo_DataAuxDateTo = DateTime.MinValue;
         Form = new GXWebForm();
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV32Select = "";
         AV66Select_GXI = "";
         A836LoteArquivoAnexo_Data = (DateTime)(DateTime.MinValue);
         A838LoteArquivoAnexo_Arquivo = "";
         A837LoteArquivoAnexo_Descricao = "";
         A646TipoDocumento_Nome = "";
         GridContainer = new GXWebGrid( context);
         scmdbuf = "";
         lV18LoteArquivoAnexo_NomeArq1 = "";
         lV19TipoDocumento_Nome1 = "";
         lV23LoteArquivoAnexo_NomeArq2 = "";
         lV24TipoDocumento_Nome2 = "";
         lV28LoteArquivoAnexo_NomeArq3 = "";
         lV29TipoDocumento_Nome3 = "";
         lV45TFLoteArquivoAnexo_NomeArq = "";
         lV49TFLoteArquivoAnexo_TipoArq = "";
         lV53TFLoteArquivoAnexo_Descricao = "";
         lV57TFTipoDocumento_Nome = "";
         A839LoteArquivoAnexo_NomeArq = "";
         A840LoteArquivoAnexo_TipoArq = "";
         H00EX2_A839LoteArquivoAnexo_NomeArq = new String[] {""} ;
         H00EX2_n839LoteArquivoAnexo_NomeArq = new bool[] {false} ;
         H00EX2_A840LoteArquivoAnexo_TipoArq = new String[] {""} ;
         H00EX2_n840LoteArquivoAnexo_TipoArq = new bool[] {false} ;
         H00EX2_A646TipoDocumento_Nome = new String[] {""} ;
         H00EX2_A645TipoDocumento_Codigo = new int[1] ;
         H00EX2_n645TipoDocumento_Codigo = new bool[] {false} ;
         H00EX2_A837LoteArquivoAnexo_Descricao = new String[] {""} ;
         H00EX2_n837LoteArquivoAnexo_Descricao = new bool[] {false} ;
         H00EX2_A836LoteArquivoAnexo_Data = new DateTime[] {DateTime.MinValue} ;
         H00EX2_A841LoteArquivoAnexo_LoteCod = new int[1] ;
         H00EX2_A838LoteArquivoAnexo_Arquivo = new String[] {""} ;
         H00EX2_n838LoteArquivoAnexo_Arquivo = new bool[] {false} ;
         edtLoteArquivoAnexo_Arquivo_Filename = "";
         H00EX3_AGRID_nRecordCount = new long[1] ;
         imgAdddynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters1_Jsonclick = "";
         imgAdddynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters3_Jsonclick = "";
         imgCleanfilters_Jsonclick = "";
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         GridRow = new GXWebRow();
         AV13GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblOrderedtext_Jsonclick = "";
         lblJsdynamicfilters_Jsonclick = "";
         lblDynamicfiltersprefix1_Jsonclick = "";
         lblDynamicfiltersmiddle1_Jsonclick = "";
         lblDynamicfiltersprefix2_Jsonclick = "";
         lblDynamicfiltersmiddle2_Jsonclick = "";
         lblDynamicfiltersprefix3_Jsonclick = "";
         lblDynamicfiltersmiddle3_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         gxblobfileaux = new GxFile(context.GetPhysicalPath());
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.promptlotearquivoanexo__default(),
            new Object[][] {
                new Object[] {
               H00EX2_A839LoteArquivoAnexo_NomeArq, H00EX2_n839LoteArquivoAnexo_NomeArq, H00EX2_A840LoteArquivoAnexo_TipoArq, H00EX2_n840LoteArquivoAnexo_TipoArq, H00EX2_A646TipoDocumento_Nome, H00EX2_A645TipoDocumento_Codigo, H00EX2_n645TipoDocumento_Codigo, H00EX2_A837LoteArquivoAnexo_Descricao, H00EX2_n837LoteArquivoAnexo_Descricao, H00EX2_A836LoteArquivoAnexo_Data,
               H00EX2_A841LoteArquivoAnexo_LoteCod, H00EX2_A838LoteArquivoAnexo_Arquivo, H00EX2_n838LoteArquivoAnexo_Arquivo
               }
               , new Object[] {
               H00EX3_AGRID_nRecordCount
               }
            }
         );
         AV67Pgmname = "PromptLoteArquivoAnexo";
         /* GeneXus formulas. */
         AV67Pgmname = "PromptLoteArquivoAnexo";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_83 ;
      private short nGXsfl_83_idx=1 ;
      private short AV14OrderedBy ;
      private short AV17DynamicFiltersOperator1 ;
      private short AV22DynamicFiltersOperator2 ;
      private short AV27DynamicFiltersOperator3 ;
      private short initialized ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_83_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short edtLoteArquivoAnexo_LoteCod_Titleformat ;
      private short edtLoteArquivoAnexo_Data_Titleformat ;
      private short edtLoteArquivoAnexo_NomeArq_Titleformat ;
      private short edtLoteArquivoAnexo_TipoArq_Titleformat ;
      private short edtLoteArquivoAnexo_Descricao_Titleformat ;
      private short edtTipoDocumento_Nome_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int AV7InOutLoteArquivoAnexo_LoteCod ;
      private int wcpOAV7InOutLoteArquivoAnexo_LoteCod ;
      private int subGrid_Rows ;
      private int AV35TFLoteArquivoAnexo_LoteCod ;
      private int AV36TFLoteArquivoAnexo_LoteCod_To ;
      private int Gridpaginationbar_Pagestoshow ;
      private int Ddo_lotearquivoanexo_lotecod_Datalistupdateminimumcharacters ;
      private int Ddo_lotearquivoanexo_data_Datalistupdateminimumcharacters ;
      private int Ddo_lotearquivoanexo_nomearq_Datalistupdateminimumcharacters ;
      private int Ddo_lotearquivoanexo_tipoarq_Datalistupdateminimumcharacters ;
      private int Ddo_lotearquivoanexo_descricao_Datalistupdateminimumcharacters ;
      private int Ddo_tipodocumento_nome_Datalistupdateminimumcharacters ;
      private int edtavTflotearquivoanexo_lotecod_Visible ;
      private int edtavTflotearquivoanexo_lotecod_to_Visible ;
      private int edtavTflotearquivoanexo_data_Visible ;
      private int edtavTflotearquivoanexo_data_to_Visible ;
      private int edtavTflotearquivoanexo_nomearq_Visible ;
      private int edtavTflotearquivoanexo_nomearq_sel_Visible ;
      private int edtavTflotearquivoanexo_tipoarq_Visible ;
      private int edtavTflotearquivoanexo_tipoarq_sel_Visible ;
      private int edtavTflotearquivoanexo_descricao_Visible ;
      private int edtavTflotearquivoanexo_descricao_sel_Visible ;
      private int edtavTftipodocumento_nome_Visible ;
      private int edtavTftipodocumento_nome_sel_Visible ;
      private int edtavDdo_lotearquivoanexo_lotecodtitlecontrolidtoreplace_Visible ;
      private int edtavDdo_lotearquivoanexo_datatitlecontrolidtoreplace_Visible ;
      private int edtavDdo_lotearquivoanexo_nomearqtitlecontrolidtoreplace_Visible ;
      private int edtavDdo_lotearquivoanexo_tipoarqtitlecontrolidtoreplace_Visible ;
      private int edtavDdo_lotearquivoanexo_descricaotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_tipodocumento_nometitlecontrolidtoreplace_Visible ;
      private int A841LoteArquivoAnexo_LoteCod ;
      private int A645TipoDocumento_Codigo ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int edtavOrdereddsc_Visible ;
      private int AV61PageToGo ;
      private int imgAdddynamicfilters1_Visible ;
      private int imgRemovedynamicfilters1_Visible ;
      private int imgAdddynamicfilters2_Visible ;
      private int imgRemovedynamicfilters2_Visible ;
      private int edtavLotearquivoanexo_nomearq1_Visible ;
      private int edtavTipodocumento_nome1_Visible ;
      private int edtavLotearquivoanexo_nomearq2_Visible ;
      private int edtavTipodocumento_nome2_Visible ;
      private int edtavLotearquivoanexo_nomearq3_Visible ;
      private int edtavTipodocumento_nome3_Visible ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private int edtavSelect_Enabled ;
      private int edtavSelect_Visible ;
      private long GRID_nFirstRecordOnPage ;
      private long AV62GridCurrentPage ;
      private long AV63GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private String AV9InOutLoteArquivoAnexo_NomeArq ;
      private String wcpOAV9InOutLoteArquivoAnexo_NomeArq ;
      private String Gridpaginationbar_Selectedpage ;
      private String Ddo_lotearquivoanexo_lotecod_Activeeventkey ;
      private String Ddo_lotearquivoanexo_lotecod_Filteredtext_get ;
      private String Ddo_lotearquivoanexo_lotecod_Filteredtextto_get ;
      private String Ddo_lotearquivoanexo_data_Activeeventkey ;
      private String Ddo_lotearquivoanexo_data_Filteredtext_get ;
      private String Ddo_lotearquivoanexo_data_Filteredtextto_get ;
      private String Ddo_lotearquivoanexo_nomearq_Activeeventkey ;
      private String Ddo_lotearquivoanexo_nomearq_Filteredtext_get ;
      private String Ddo_lotearquivoanexo_nomearq_Selectedvalue_get ;
      private String Ddo_lotearquivoanexo_tipoarq_Activeeventkey ;
      private String Ddo_lotearquivoanexo_tipoarq_Filteredtext_get ;
      private String Ddo_lotearquivoanexo_tipoarq_Selectedvalue_get ;
      private String Ddo_lotearquivoanexo_descricao_Activeeventkey ;
      private String Ddo_lotearquivoanexo_descricao_Filteredtext_get ;
      private String Ddo_lotearquivoanexo_descricao_Selectedvalue_get ;
      private String Ddo_tipodocumento_nome_Activeeventkey ;
      private String Ddo_tipodocumento_nome_Filteredtext_get ;
      private String Ddo_tipodocumento_nome_Selectedvalue_get ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_83_idx="0001" ;
      private String AV18LoteArquivoAnexo_NomeArq1 ;
      private String AV19TipoDocumento_Nome1 ;
      private String AV23LoteArquivoAnexo_NomeArq2 ;
      private String AV24TipoDocumento_Nome2 ;
      private String AV28LoteArquivoAnexo_NomeArq3 ;
      private String AV29TipoDocumento_Nome3 ;
      private String AV45TFLoteArquivoAnexo_NomeArq ;
      private String AV46TFLoteArquivoAnexo_NomeArq_Sel ;
      private String AV49TFLoteArquivoAnexo_TipoArq ;
      private String AV50TFLoteArquivoAnexo_TipoArq_Sel ;
      private String AV57TFTipoDocumento_Nome ;
      private String AV58TFTipoDocumento_Nome_Sel ;
      private String AV67Pgmname ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Ddo_lotearquivoanexo_lotecod_Caption ;
      private String Ddo_lotearquivoanexo_lotecod_Tooltip ;
      private String Ddo_lotearquivoanexo_lotecod_Cls ;
      private String Ddo_lotearquivoanexo_lotecod_Filteredtext_set ;
      private String Ddo_lotearquivoanexo_lotecod_Filteredtextto_set ;
      private String Ddo_lotearquivoanexo_lotecod_Dropdownoptionstype ;
      private String Ddo_lotearquivoanexo_lotecod_Titlecontrolidtoreplace ;
      private String Ddo_lotearquivoanexo_lotecod_Sortedstatus ;
      private String Ddo_lotearquivoanexo_lotecod_Filtertype ;
      private String Ddo_lotearquivoanexo_lotecod_Datalistfixedvalues ;
      private String Ddo_lotearquivoanexo_lotecod_Sortasc ;
      private String Ddo_lotearquivoanexo_lotecod_Sortdsc ;
      private String Ddo_lotearquivoanexo_lotecod_Loadingdata ;
      private String Ddo_lotearquivoanexo_lotecod_Cleanfilter ;
      private String Ddo_lotearquivoanexo_lotecod_Rangefilterfrom ;
      private String Ddo_lotearquivoanexo_lotecod_Rangefilterto ;
      private String Ddo_lotearquivoanexo_lotecod_Noresultsfound ;
      private String Ddo_lotearquivoanexo_lotecod_Searchbuttontext ;
      private String Ddo_lotearquivoanexo_data_Caption ;
      private String Ddo_lotearquivoanexo_data_Tooltip ;
      private String Ddo_lotearquivoanexo_data_Cls ;
      private String Ddo_lotearquivoanexo_data_Filteredtext_set ;
      private String Ddo_lotearquivoanexo_data_Filteredtextto_set ;
      private String Ddo_lotearquivoanexo_data_Dropdownoptionstype ;
      private String Ddo_lotearquivoanexo_data_Titlecontrolidtoreplace ;
      private String Ddo_lotearquivoanexo_data_Sortedstatus ;
      private String Ddo_lotearquivoanexo_data_Filtertype ;
      private String Ddo_lotearquivoanexo_data_Datalistfixedvalues ;
      private String Ddo_lotearquivoanexo_data_Sortasc ;
      private String Ddo_lotearquivoanexo_data_Sortdsc ;
      private String Ddo_lotearquivoanexo_data_Loadingdata ;
      private String Ddo_lotearquivoanexo_data_Cleanfilter ;
      private String Ddo_lotearquivoanexo_data_Rangefilterfrom ;
      private String Ddo_lotearquivoanexo_data_Rangefilterto ;
      private String Ddo_lotearquivoanexo_data_Noresultsfound ;
      private String Ddo_lotearquivoanexo_data_Searchbuttontext ;
      private String Ddo_lotearquivoanexo_nomearq_Caption ;
      private String Ddo_lotearquivoanexo_nomearq_Tooltip ;
      private String Ddo_lotearquivoanexo_nomearq_Cls ;
      private String Ddo_lotearquivoanexo_nomearq_Filteredtext_set ;
      private String Ddo_lotearquivoanexo_nomearq_Selectedvalue_set ;
      private String Ddo_lotearquivoanexo_nomearq_Dropdownoptionstype ;
      private String Ddo_lotearquivoanexo_nomearq_Titlecontrolidtoreplace ;
      private String Ddo_lotearquivoanexo_nomearq_Sortedstatus ;
      private String Ddo_lotearquivoanexo_nomearq_Filtertype ;
      private String Ddo_lotearquivoanexo_nomearq_Datalisttype ;
      private String Ddo_lotearquivoanexo_nomearq_Datalistfixedvalues ;
      private String Ddo_lotearquivoanexo_nomearq_Datalistproc ;
      private String Ddo_lotearquivoanexo_nomearq_Sortasc ;
      private String Ddo_lotearquivoanexo_nomearq_Sortdsc ;
      private String Ddo_lotearquivoanexo_nomearq_Loadingdata ;
      private String Ddo_lotearquivoanexo_nomearq_Cleanfilter ;
      private String Ddo_lotearquivoanexo_nomearq_Rangefilterfrom ;
      private String Ddo_lotearquivoanexo_nomearq_Rangefilterto ;
      private String Ddo_lotearquivoanexo_nomearq_Noresultsfound ;
      private String Ddo_lotearquivoanexo_nomearq_Searchbuttontext ;
      private String Ddo_lotearquivoanexo_tipoarq_Caption ;
      private String Ddo_lotearquivoanexo_tipoarq_Tooltip ;
      private String Ddo_lotearquivoanexo_tipoarq_Cls ;
      private String Ddo_lotearquivoanexo_tipoarq_Filteredtext_set ;
      private String Ddo_lotearquivoanexo_tipoarq_Selectedvalue_set ;
      private String Ddo_lotearquivoanexo_tipoarq_Dropdownoptionstype ;
      private String Ddo_lotearquivoanexo_tipoarq_Titlecontrolidtoreplace ;
      private String Ddo_lotearquivoanexo_tipoarq_Sortedstatus ;
      private String Ddo_lotearquivoanexo_tipoarq_Filtertype ;
      private String Ddo_lotearquivoanexo_tipoarq_Datalisttype ;
      private String Ddo_lotearquivoanexo_tipoarq_Datalistfixedvalues ;
      private String Ddo_lotearquivoanexo_tipoarq_Datalistproc ;
      private String Ddo_lotearquivoanexo_tipoarq_Sortasc ;
      private String Ddo_lotearquivoanexo_tipoarq_Sortdsc ;
      private String Ddo_lotearquivoanexo_tipoarq_Loadingdata ;
      private String Ddo_lotearquivoanexo_tipoarq_Cleanfilter ;
      private String Ddo_lotearquivoanexo_tipoarq_Rangefilterfrom ;
      private String Ddo_lotearquivoanexo_tipoarq_Rangefilterto ;
      private String Ddo_lotearquivoanexo_tipoarq_Noresultsfound ;
      private String Ddo_lotearquivoanexo_tipoarq_Searchbuttontext ;
      private String Ddo_lotearquivoanexo_descricao_Caption ;
      private String Ddo_lotearquivoanexo_descricao_Tooltip ;
      private String Ddo_lotearquivoanexo_descricao_Cls ;
      private String Ddo_lotearquivoanexo_descricao_Filteredtext_set ;
      private String Ddo_lotearquivoanexo_descricao_Selectedvalue_set ;
      private String Ddo_lotearquivoanexo_descricao_Dropdownoptionstype ;
      private String Ddo_lotearquivoanexo_descricao_Titlecontrolidtoreplace ;
      private String Ddo_lotearquivoanexo_descricao_Sortedstatus ;
      private String Ddo_lotearquivoanexo_descricao_Filtertype ;
      private String Ddo_lotearquivoanexo_descricao_Datalisttype ;
      private String Ddo_lotearquivoanexo_descricao_Datalistfixedvalues ;
      private String Ddo_lotearquivoanexo_descricao_Datalistproc ;
      private String Ddo_lotearquivoanexo_descricao_Sortasc ;
      private String Ddo_lotearquivoanexo_descricao_Sortdsc ;
      private String Ddo_lotearquivoanexo_descricao_Loadingdata ;
      private String Ddo_lotearquivoanexo_descricao_Cleanfilter ;
      private String Ddo_lotearquivoanexo_descricao_Rangefilterfrom ;
      private String Ddo_lotearquivoanexo_descricao_Rangefilterto ;
      private String Ddo_lotearquivoanexo_descricao_Noresultsfound ;
      private String Ddo_lotearquivoanexo_descricao_Searchbuttontext ;
      private String Ddo_tipodocumento_nome_Caption ;
      private String Ddo_tipodocumento_nome_Tooltip ;
      private String Ddo_tipodocumento_nome_Cls ;
      private String Ddo_tipodocumento_nome_Filteredtext_set ;
      private String Ddo_tipodocumento_nome_Selectedvalue_set ;
      private String Ddo_tipodocumento_nome_Dropdownoptionstype ;
      private String Ddo_tipodocumento_nome_Titlecontrolidtoreplace ;
      private String Ddo_tipodocumento_nome_Sortedstatus ;
      private String Ddo_tipodocumento_nome_Filtertype ;
      private String Ddo_tipodocumento_nome_Datalisttype ;
      private String Ddo_tipodocumento_nome_Datalistfixedvalues ;
      private String Ddo_tipodocumento_nome_Datalistproc ;
      private String Ddo_tipodocumento_nome_Sortasc ;
      private String Ddo_tipodocumento_nome_Sortdsc ;
      private String Ddo_tipodocumento_nome_Loadingdata ;
      private String Ddo_tipodocumento_nome_Cleanfilter ;
      private String Ddo_tipodocumento_nome_Rangefilterfrom ;
      private String Ddo_tipodocumento_nome_Rangefilterto ;
      private String Ddo_tipodocumento_nome_Noresultsfound ;
      private String Ddo_tipodocumento_nome_Searchbuttontext ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String chkavDynamicfiltersenabled2_Internalname ;
      private String chkavDynamicfiltersenabled3_Internalname ;
      private String edtavTflotearquivoanexo_lotecod_Internalname ;
      private String edtavTflotearquivoanexo_lotecod_Jsonclick ;
      private String edtavTflotearquivoanexo_lotecod_to_Internalname ;
      private String edtavTflotearquivoanexo_lotecod_to_Jsonclick ;
      private String edtavTflotearquivoanexo_data_Internalname ;
      private String edtavTflotearquivoanexo_data_Jsonclick ;
      private String edtavTflotearquivoanexo_data_to_Internalname ;
      private String edtavTflotearquivoanexo_data_to_Jsonclick ;
      private String divDdo_lotearquivoanexo_dataauxdates_Internalname ;
      private String edtavDdo_lotearquivoanexo_dataauxdate_Internalname ;
      private String edtavDdo_lotearquivoanexo_dataauxdate_Jsonclick ;
      private String edtavDdo_lotearquivoanexo_dataauxdateto_Internalname ;
      private String edtavDdo_lotearquivoanexo_dataauxdateto_Jsonclick ;
      private String edtavTflotearquivoanexo_nomearq_Internalname ;
      private String edtavTflotearquivoanexo_nomearq_Jsonclick ;
      private String edtavTflotearquivoanexo_nomearq_sel_Internalname ;
      private String edtavTflotearquivoanexo_nomearq_sel_Jsonclick ;
      private String edtavTflotearquivoanexo_tipoarq_Internalname ;
      private String edtavTflotearquivoanexo_tipoarq_Jsonclick ;
      private String edtavTflotearquivoanexo_tipoarq_sel_Internalname ;
      private String edtavTflotearquivoanexo_tipoarq_sel_Jsonclick ;
      private String edtavTflotearquivoanexo_descricao_Internalname ;
      private String edtavTflotearquivoanexo_descricao_sel_Internalname ;
      private String edtavTftipodocumento_nome_Internalname ;
      private String edtavTftipodocumento_nome_Jsonclick ;
      private String edtavTftipodocumento_nome_sel_Internalname ;
      private String edtavTftipodocumento_nome_sel_Jsonclick ;
      private String edtavDdo_lotearquivoanexo_lotecodtitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_lotearquivoanexo_datatitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_lotearquivoanexo_nomearqtitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_lotearquivoanexo_tipoarqtitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_lotearquivoanexo_descricaotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_tipodocumento_nometitlecontrolidtoreplace_Internalname ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavSelect_Internalname ;
      private String edtLoteArquivoAnexo_LoteCod_Internalname ;
      private String edtLoteArquivoAnexo_Data_Internalname ;
      private String edtLoteArquivoAnexo_Arquivo_Internalname ;
      private String edtLoteArquivoAnexo_Descricao_Internalname ;
      private String edtTipoDocumento_Codigo_Internalname ;
      private String A646TipoDocumento_Nome ;
      private String edtTipoDocumento_Nome_Internalname ;
      private String cmbavOrderedby_Internalname ;
      private String scmdbuf ;
      private String lV18LoteArquivoAnexo_NomeArq1 ;
      private String lV19TipoDocumento_Nome1 ;
      private String lV23LoteArquivoAnexo_NomeArq2 ;
      private String lV24TipoDocumento_Nome2 ;
      private String lV28LoteArquivoAnexo_NomeArq3 ;
      private String lV29TipoDocumento_Nome3 ;
      private String lV45TFLoteArquivoAnexo_NomeArq ;
      private String lV49TFLoteArquivoAnexo_TipoArq ;
      private String lV57TFTipoDocumento_Nome ;
      private String A839LoteArquivoAnexo_NomeArq ;
      private String A840LoteArquivoAnexo_TipoArq ;
      private String edtLoteArquivoAnexo_Arquivo_Filename ;
      private String edtLoteArquivoAnexo_Arquivo_Filetype ;
      private String edtavOrdereddsc_Internalname ;
      private String cmbavDynamicfiltersselector1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Internalname ;
      private String edtavLotearquivoanexo_nomearq1_Internalname ;
      private String edtavTipodocumento_nome1_Internalname ;
      private String cmbavDynamicfiltersselector2_Internalname ;
      private String cmbavDynamicfiltersoperator2_Internalname ;
      private String edtavLotearquivoanexo_nomearq2_Internalname ;
      private String edtavTipodocumento_nome2_Internalname ;
      private String cmbavDynamicfiltersselector3_Internalname ;
      private String cmbavDynamicfiltersoperator3_Internalname ;
      private String edtavLotearquivoanexo_nomearq3_Internalname ;
      private String edtavTipodocumento_nome3_Internalname ;
      private String lblJsdynamicfilters_Caption ;
      private String lblJsdynamicfilters_Internalname ;
      private String imgAdddynamicfilters1_Jsonclick ;
      private String imgAdddynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters1_Jsonclick ;
      private String imgRemovedynamicfilters1_Internalname ;
      private String imgAdddynamicfilters2_Jsonclick ;
      private String imgAdddynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters2_Jsonclick ;
      private String imgRemovedynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters3_Jsonclick ;
      private String imgRemovedynamicfilters3_Internalname ;
      private String subGrid_Internalname ;
      private String Ddo_lotearquivoanexo_lotecod_Internalname ;
      private String Ddo_lotearquivoanexo_data_Internalname ;
      private String Ddo_lotearquivoanexo_nomearq_Internalname ;
      private String Ddo_lotearquivoanexo_tipoarq_Internalname ;
      private String Ddo_lotearquivoanexo_descricao_Internalname ;
      private String Ddo_tipodocumento_nome_Internalname ;
      private String imgCleanfilters_Jsonclick ;
      private String imgCleanfilters_Internalname ;
      private String edtLoteArquivoAnexo_LoteCod_Title ;
      private String edtLoteArquivoAnexo_Data_Title ;
      private String edtLoteArquivoAnexo_NomeArq_Title ;
      private String edtLoteArquivoAnexo_NomeArq_Internalname ;
      private String edtLoteArquivoAnexo_TipoArq_Title ;
      private String edtLoteArquivoAnexo_TipoArq_Internalname ;
      private String edtLoteArquivoAnexo_Descricao_Title ;
      private String edtTipoDocumento_Nome_Title ;
      private String edtavSelect_Tooltiptext ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTablegridheader_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTablesearch_Internalname ;
      private String lblOrderedtext_Internalname ;
      private String lblOrderedtext_Jsonclick ;
      private String cmbavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Jsonclick ;
      private String tblTablefilters_Internalname ;
      private String lblJsdynamicfilters_Jsonclick ;
      private String tblTabledynamicfilters_Internalname ;
      private String lblDynamicfiltersprefix1_Internalname ;
      private String lblDynamicfiltersprefix1_Jsonclick ;
      private String cmbavDynamicfiltersselector1_Jsonclick ;
      private String lblDynamicfiltersmiddle1_Internalname ;
      private String lblDynamicfiltersmiddle1_Jsonclick ;
      private String lblDynamicfiltersprefix2_Internalname ;
      private String lblDynamicfiltersprefix2_Jsonclick ;
      private String cmbavDynamicfiltersselector2_Jsonclick ;
      private String lblDynamicfiltersmiddle2_Internalname ;
      private String lblDynamicfiltersmiddle2_Jsonclick ;
      private String lblDynamicfiltersprefix3_Internalname ;
      private String lblDynamicfiltersprefix3_Jsonclick ;
      private String cmbavDynamicfiltersselector3_Jsonclick ;
      private String lblDynamicfiltersmiddle3_Internalname ;
      private String lblDynamicfiltersmiddle3_Jsonclick ;
      private String tblTablemergeddynamicfilters3_Internalname ;
      private String cmbavDynamicfiltersoperator3_Jsonclick ;
      private String edtavLotearquivoanexo_nomearq3_Jsonclick ;
      private String edtavTipodocumento_nome3_Jsonclick ;
      private String tblTablemergeddynamicfilters2_Internalname ;
      private String cmbavDynamicfiltersoperator2_Jsonclick ;
      private String edtavLotearquivoanexo_nomearq2_Jsonclick ;
      private String edtavTipodocumento_nome2_Jsonclick ;
      private String tblTablemergeddynamicfilters1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Jsonclick ;
      private String edtavLotearquivoanexo_nomearq1_Jsonclick ;
      private String edtavTipodocumento_nome1_Jsonclick ;
      private String sGXsfl_83_fel_idx="0001" ;
      private String edtavSelect_Jsonclick ;
      private String ROClassString ;
      private String edtLoteArquivoAnexo_LoteCod_Jsonclick ;
      private String edtLoteArquivoAnexo_Data_Jsonclick ;
      private String edtLoteArquivoAnexo_Arquivo_Contenttype ;
      private String edtLoteArquivoAnexo_Arquivo_Parameters ;
      private String edtLoteArquivoAnexo_Arquivo_Jsonclick ;
      private String edtLoteArquivoAnexo_NomeArq_Jsonclick ;
      private String edtLoteArquivoAnexo_TipoArq_Jsonclick ;
      private String edtLoteArquivoAnexo_Descricao_Jsonclick ;
      private String edtTipoDocumento_Codigo_Jsonclick ;
      private String edtTipoDocumento_Nome_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private DateTime AV8InOutLoteArquivoAnexo_Data ;
      private DateTime wcpOAV8InOutLoteArquivoAnexo_Data ;
      private DateTime AV39TFLoteArquivoAnexo_Data ;
      private DateTime AV40TFLoteArquivoAnexo_Data_To ;
      private DateTime A836LoteArquivoAnexo_Data ;
      private DateTime AV41DDO_LoteArquivoAnexo_DataAuxDate ;
      private DateTime AV42DDO_LoteArquivoAnexo_DataAuxDateTo ;
      private bool entryPointCalled ;
      private bool AV15OrderedDsc ;
      private bool AV20DynamicFiltersEnabled2 ;
      private bool AV25DynamicFiltersEnabled3 ;
      private bool AV31DynamicFiltersIgnoreFirst ;
      private bool AV30DynamicFiltersRemoving ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool Ddo_lotearquivoanexo_lotecod_Includesortasc ;
      private bool Ddo_lotearquivoanexo_lotecod_Includesortdsc ;
      private bool Ddo_lotearquivoanexo_lotecod_Includefilter ;
      private bool Ddo_lotearquivoanexo_lotecod_Filterisrange ;
      private bool Ddo_lotearquivoanexo_lotecod_Includedatalist ;
      private bool Ddo_lotearquivoanexo_data_Includesortasc ;
      private bool Ddo_lotearquivoanexo_data_Includesortdsc ;
      private bool Ddo_lotearquivoanexo_data_Includefilter ;
      private bool Ddo_lotearquivoanexo_data_Filterisrange ;
      private bool Ddo_lotearquivoanexo_data_Includedatalist ;
      private bool Ddo_lotearquivoanexo_nomearq_Includesortasc ;
      private bool Ddo_lotearquivoanexo_nomearq_Includesortdsc ;
      private bool Ddo_lotearquivoanexo_nomearq_Includefilter ;
      private bool Ddo_lotearquivoanexo_nomearq_Filterisrange ;
      private bool Ddo_lotearquivoanexo_nomearq_Includedatalist ;
      private bool Ddo_lotearquivoanexo_tipoarq_Includesortasc ;
      private bool Ddo_lotearquivoanexo_tipoarq_Includesortdsc ;
      private bool Ddo_lotearquivoanexo_tipoarq_Includefilter ;
      private bool Ddo_lotearquivoanexo_tipoarq_Filterisrange ;
      private bool Ddo_lotearquivoanexo_tipoarq_Includedatalist ;
      private bool Ddo_lotearquivoanexo_descricao_Includesortasc ;
      private bool Ddo_lotearquivoanexo_descricao_Includesortdsc ;
      private bool Ddo_lotearquivoanexo_descricao_Includefilter ;
      private bool Ddo_lotearquivoanexo_descricao_Filterisrange ;
      private bool Ddo_lotearquivoanexo_descricao_Includedatalist ;
      private bool Ddo_tipodocumento_nome_Includesortasc ;
      private bool Ddo_tipodocumento_nome_Includesortdsc ;
      private bool Ddo_tipodocumento_nome_Includefilter ;
      private bool Ddo_tipodocumento_nome_Filterisrange ;
      private bool Ddo_tipodocumento_nome_Includedatalist ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n838LoteArquivoAnexo_Arquivo ;
      private bool n837LoteArquivoAnexo_Descricao ;
      private bool n645TipoDocumento_Codigo ;
      private bool n839LoteArquivoAnexo_NomeArq ;
      private bool n840LoteArquivoAnexo_TipoArq ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV32Select_IsBlob ;
      private String A837LoteArquivoAnexo_Descricao ;
      private String AV16DynamicFiltersSelector1 ;
      private String AV21DynamicFiltersSelector2 ;
      private String AV26DynamicFiltersSelector3 ;
      private String AV53TFLoteArquivoAnexo_Descricao ;
      private String AV54TFLoteArquivoAnexo_Descricao_Sel ;
      private String AV37ddo_LoteArquivoAnexo_LoteCodTitleControlIdToReplace ;
      private String AV43ddo_LoteArquivoAnexo_DataTitleControlIdToReplace ;
      private String AV47ddo_LoteArquivoAnexo_NomeArqTitleControlIdToReplace ;
      private String AV51ddo_LoteArquivoAnexo_TipoArqTitleControlIdToReplace ;
      private String AV55ddo_LoteArquivoAnexo_DescricaoTitleControlIdToReplace ;
      private String AV59ddo_TipoDocumento_NomeTitleControlIdToReplace ;
      private String AV66Select_GXI ;
      private String lV53TFLoteArquivoAnexo_Descricao ;
      private String AV32Select ;
      private String A838LoteArquivoAnexo_Arquivo ;
      private GxFile gxblobfileaux ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_InOutLoteArquivoAnexo_LoteCod ;
      private DateTime aP1_InOutLoteArquivoAnexo_Data ;
      private String aP2_InOutLoteArquivoAnexo_NomeArq ;
      private GXCombobox cmbavOrderedby ;
      private GXCombobox cmbavDynamicfiltersselector1 ;
      private GXCombobox cmbavDynamicfiltersoperator1 ;
      private GXCombobox cmbavDynamicfiltersselector2 ;
      private GXCombobox cmbavDynamicfiltersoperator2 ;
      private GXCombobox cmbavDynamicfiltersselector3 ;
      private GXCombobox cmbavDynamicfiltersoperator3 ;
      private GXCheckbox chkavDynamicfiltersenabled2 ;
      private GXCheckbox chkavDynamicfiltersenabled3 ;
      private IDataStoreProvider pr_default ;
      private String[] H00EX2_A839LoteArquivoAnexo_NomeArq ;
      private bool[] H00EX2_n839LoteArquivoAnexo_NomeArq ;
      private String[] H00EX2_A840LoteArquivoAnexo_TipoArq ;
      private bool[] H00EX2_n840LoteArquivoAnexo_TipoArq ;
      private String[] H00EX2_A646TipoDocumento_Nome ;
      private int[] H00EX2_A645TipoDocumento_Codigo ;
      private bool[] H00EX2_n645TipoDocumento_Codigo ;
      private String[] H00EX2_A837LoteArquivoAnexo_Descricao ;
      private bool[] H00EX2_n837LoteArquivoAnexo_Descricao ;
      private DateTime[] H00EX2_A836LoteArquivoAnexo_Data ;
      private int[] H00EX2_A841LoteArquivoAnexo_LoteCod ;
      private String[] H00EX2_A838LoteArquivoAnexo_Arquivo ;
      private bool[] H00EX2_n838LoteArquivoAnexo_Arquivo ;
      private long[] H00EX3_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV34LoteArquivoAnexo_LoteCodTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV38LoteArquivoAnexo_DataTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV44LoteArquivoAnexo_NomeArqTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV48LoteArquivoAnexo_TipoArqTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV52LoteArquivoAnexo_DescricaoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV56TipoDocumento_NomeTitleFilterData ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV11GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV12GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV13GridStateDynamicFilter ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV60DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 ;
   }

   public class promptlotearquivoanexo__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00EX2( IGxContext context ,
                                             String AV16DynamicFiltersSelector1 ,
                                             short AV17DynamicFiltersOperator1 ,
                                             String AV18LoteArquivoAnexo_NomeArq1 ,
                                             String AV19TipoDocumento_Nome1 ,
                                             bool AV20DynamicFiltersEnabled2 ,
                                             String AV21DynamicFiltersSelector2 ,
                                             short AV22DynamicFiltersOperator2 ,
                                             String AV23LoteArquivoAnexo_NomeArq2 ,
                                             String AV24TipoDocumento_Nome2 ,
                                             bool AV25DynamicFiltersEnabled3 ,
                                             String AV26DynamicFiltersSelector3 ,
                                             short AV27DynamicFiltersOperator3 ,
                                             String AV28LoteArquivoAnexo_NomeArq3 ,
                                             String AV29TipoDocumento_Nome3 ,
                                             int AV35TFLoteArquivoAnexo_LoteCod ,
                                             int AV36TFLoteArquivoAnexo_LoteCod_To ,
                                             DateTime AV39TFLoteArquivoAnexo_Data ,
                                             DateTime AV40TFLoteArquivoAnexo_Data_To ,
                                             String AV46TFLoteArquivoAnexo_NomeArq_Sel ,
                                             String AV45TFLoteArquivoAnexo_NomeArq ,
                                             String AV50TFLoteArquivoAnexo_TipoArq_Sel ,
                                             String AV49TFLoteArquivoAnexo_TipoArq ,
                                             String AV54TFLoteArquivoAnexo_Descricao_Sel ,
                                             String AV53TFLoteArquivoAnexo_Descricao ,
                                             String AV58TFTipoDocumento_Nome_Sel ,
                                             String AV57TFTipoDocumento_Nome ,
                                             String A839LoteArquivoAnexo_NomeArq ,
                                             String A646TipoDocumento_Nome ,
                                             int A841LoteArquivoAnexo_LoteCod ,
                                             DateTime A836LoteArquivoAnexo_Data ,
                                             String A840LoteArquivoAnexo_TipoArq ,
                                             String A837LoteArquivoAnexo_Descricao ,
                                             short AV14OrderedBy ,
                                             bool AV15OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [29] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " T1.[LoteArquivoAnexo_NomeArq], T1.[LoteArquivoAnexo_TipoArq], T2.[TipoDocumento_Nome], T1.[TipoDocumento_Codigo], T1.[LoteArquivoAnexo_Descricao], T1.[LoteArquivoAnexo_Data], T1.[LoteArquivoAnexo_LoteCod], T1.[LoteArquivoAnexo_Arquivo]";
         sFromString = " FROM ([LoteArquivoAnexo] T1 WITH (NOLOCK) LEFT JOIN [TipoDocumento] T2 WITH (NOLOCK) ON T2.[TipoDocumento_Codigo] = T1.[TipoDocumento_Codigo])";
         sOrderString = "";
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "LOTEARQUIVOANEXO_NOMEARQ") == 0 ) && ( AV17DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV18LoteArquivoAnexo_NomeArq1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[LoteArquivoAnexo_NomeArq] like @lV18LoteArquivoAnexo_NomeArq1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[LoteArquivoAnexo_NomeArq] like @lV18LoteArquivoAnexo_NomeArq1)";
            }
         }
         else
         {
            GXv_int2[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "LOTEARQUIVOANEXO_NOMEARQ") == 0 ) && ( AV17DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV18LoteArquivoAnexo_NomeArq1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[LoteArquivoAnexo_NomeArq] like '%' + @lV18LoteArquivoAnexo_NomeArq1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[LoteArquivoAnexo_NomeArq] like '%' + @lV18LoteArquivoAnexo_NomeArq1)";
            }
         }
         else
         {
            GXv_int2[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "TIPODOCUMENTO_NOME") == 0 ) && ( AV17DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV19TipoDocumento_Nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[TipoDocumento_Nome] like @lV19TipoDocumento_Nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[TipoDocumento_Nome] like @lV19TipoDocumento_Nome1)";
            }
         }
         else
         {
            GXv_int2[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "TIPODOCUMENTO_NOME") == 0 ) && ( AV17DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV19TipoDocumento_Nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[TipoDocumento_Nome] like '%' + @lV19TipoDocumento_Nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[TipoDocumento_Nome] like '%' + @lV19TipoDocumento_Nome1)";
            }
         }
         else
         {
            GXv_int2[3] = 1;
         }
         if ( AV20DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "LOTEARQUIVOANEXO_NOMEARQ") == 0 ) && ( AV22DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV23LoteArquivoAnexo_NomeArq2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[LoteArquivoAnexo_NomeArq] like @lV23LoteArquivoAnexo_NomeArq2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[LoteArquivoAnexo_NomeArq] like @lV23LoteArquivoAnexo_NomeArq2)";
            }
         }
         else
         {
            GXv_int2[4] = 1;
         }
         if ( AV20DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "LOTEARQUIVOANEXO_NOMEARQ") == 0 ) && ( AV22DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV23LoteArquivoAnexo_NomeArq2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[LoteArquivoAnexo_NomeArq] like '%' + @lV23LoteArquivoAnexo_NomeArq2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[LoteArquivoAnexo_NomeArq] like '%' + @lV23LoteArquivoAnexo_NomeArq2)";
            }
         }
         else
         {
            GXv_int2[5] = 1;
         }
         if ( AV20DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "TIPODOCUMENTO_NOME") == 0 ) && ( AV22DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV24TipoDocumento_Nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[TipoDocumento_Nome] like @lV24TipoDocumento_Nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[TipoDocumento_Nome] like @lV24TipoDocumento_Nome2)";
            }
         }
         else
         {
            GXv_int2[6] = 1;
         }
         if ( AV20DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "TIPODOCUMENTO_NOME") == 0 ) && ( AV22DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV24TipoDocumento_Nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[TipoDocumento_Nome] like '%' + @lV24TipoDocumento_Nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[TipoDocumento_Nome] like '%' + @lV24TipoDocumento_Nome2)";
            }
         }
         else
         {
            GXv_int2[7] = 1;
         }
         if ( AV25DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV26DynamicFiltersSelector3, "LOTEARQUIVOANEXO_NOMEARQ") == 0 ) && ( AV27DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV28LoteArquivoAnexo_NomeArq3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[LoteArquivoAnexo_NomeArq] like @lV28LoteArquivoAnexo_NomeArq3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[LoteArquivoAnexo_NomeArq] like @lV28LoteArquivoAnexo_NomeArq3)";
            }
         }
         else
         {
            GXv_int2[8] = 1;
         }
         if ( AV25DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV26DynamicFiltersSelector3, "LOTEARQUIVOANEXO_NOMEARQ") == 0 ) && ( AV27DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV28LoteArquivoAnexo_NomeArq3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[LoteArquivoAnexo_NomeArq] like '%' + @lV28LoteArquivoAnexo_NomeArq3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[LoteArquivoAnexo_NomeArq] like '%' + @lV28LoteArquivoAnexo_NomeArq3)";
            }
         }
         else
         {
            GXv_int2[9] = 1;
         }
         if ( AV25DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV26DynamicFiltersSelector3, "TIPODOCUMENTO_NOME") == 0 ) && ( AV27DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV29TipoDocumento_Nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[TipoDocumento_Nome] like @lV29TipoDocumento_Nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[TipoDocumento_Nome] like @lV29TipoDocumento_Nome3)";
            }
         }
         else
         {
            GXv_int2[10] = 1;
         }
         if ( AV25DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV26DynamicFiltersSelector3, "TIPODOCUMENTO_NOME") == 0 ) && ( AV27DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV29TipoDocumento_Nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[TipoDocumento_Nome] like '%' + @lV29TipoDocumento_Nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[TipoDocumento_Nome] like '%' + @lV29TipoDocumento_Nome3)";
            }
         }
         else
         {
            GXv_int2[11] = 1;
         }
         if ( ! (0==AV35TFLoteArquivoAnexo_LoteCod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[LoteArquivoAnexo_LoteCod] >= @AV35TFLoteArquivoAnexo_LoteCod)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[LoteArquivoAnexo_LoteCod] >= @AV35TFLoteArquivoAnexo_LoteCod)";
            }
         }
         else
         {
            GXv_int2[12] = 1;
         }
         if ( ! (0==AV36TFLoteArquivoAnexo_LoteCod_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[LoteArquivoAnexo_LoteCod] <= @AV36TFLoteArquivoAnexo_LoteCod_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[LoteArquivoAnexo_LoteCod] <= @AV36TFLoteArquivoAnexo_LoteCod_To)";
            }
         }
         else
         {
            GXv_int2[13] = 1;
         }
         if ( ! (DateTime.MinValue==AV39TFLoteArquivoAnexo_Data) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[LoteArquivoAnexo_Data] >= @AV39TFLoteArquivoAnexo_Data)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[LoteArquivoAnexo_Data] >= @AV39TFLoteArquivoAnexo_Data)";
            }
         }
         else
         {
            GXv_int2[14] = 1;
         }
         if ( ! (DateTime.MinValue==AV40TFLoteArquivoAnexo_Data_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[LoteArquivoAnexo_Data] <= @AV40TFLoteArquivoAnexo_Data_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[LoteArquivoAnexo_Data] <= @AV40TFLoteArquivoAnexo_Data_To)";
            }
         }
         else
         {
            GXv_int2[15] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV46TFLoteArquivoAnexo_NomeArq_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV45TFLoteArquivoAnexo_NomeArq)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[LoteArquivoAnexo_NomeArq] like @lV45TFLoteArquivoAnexo_NomeArq)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[LoteArquivoAnexo_NomeArq] like @lV45TFLoteArquivoAnexo_NomeArq)";
            }
         }
         else
         {
            GXv_int2[16] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV46TFLoteArquivoAnexo_NomeArq_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[LoteArquivoAnexo_NomeArq] = @AV46TFLoteArquivoAnexo_NomeArq_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[LoteArquivoAnexo_NomeArq] = @AV46TFLoteArquivoAnexo_NomeArq_Sel)";
            }
         }
         else
         {
            GXv_int2[17] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV50TFLoteArquivoAnexo_TipoArq_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV49TFLoteArquivoAnexo_TipoArq)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[LoteArquivoAnexo_TipoArq] like @lV49TFLoteArquivoAnexo_TipoArq)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[LoteArquivoAnexo_TipoArq] like @lV49TFLoteArquivoAnexo_TipoArq)";
            }
         }
         else
         {
            GXv_int2[18] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV50TFLoteArquivoAnexo_TipoArq_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[LoteArquivoAnexo_TipoArq] = @AV50TFLoteArquivoAnexo_TipoArq_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[LoteArquivoAnexo_TipoArq] = @AV50TFLoteArquivoAnexo_TipoArq_Sel)";
            }
         }
         else
         {
            GXv_int2[19] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV54TFLoteArquivoAnexo_Descricao_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV53TFLoteArquivoAnexo_Descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[LoteArquivoAnexo_Descricao] like @lV53TFLoteArquivoAnexo_Descricao)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[LoteArquivoAnexo_Descricao] like @lV53TFLoteArquivoAnexo_Descricao)";
            }
         }
         else
         {
            GXv_int2[20] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV54TFLoteArquivoAnexo_Descricao_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[LoteArquivoAnexo_Descricao] = @AV54TFLoteArquivoAnexo_Descricao_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[LoteArquivoAnexo_Descricao] = @AV54TFLoteArquivoAnexo_Descricao_Sel)";
            }
         }
         else
         {
            GXv_int2[21] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV58TFTipoDocumento_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV57TFTipoDocumento_Nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[TipoDocumento_Nome] like @lV57TFTipoDocumento_Nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[TipoDocumento_Nome] like @lV57TFTipoDocumento_Nome)";
            }
         }
         else
         {
            GXv_int2[22] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV58TFTipoDocumento_Nome_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[TipoDocumento_Nome] = @AV58TFTipoDocumento_Nome_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[TipoDocumento_Nome] = @AV58TFTipoDocumento_Nome_Sel)";
            }
         }
         else
         {
            GXv_int2[23] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            sWhereString = " WHERE" + sWhereString;
         }
         if ( ( AV14OrderedBy == 1 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[LoteArquivoAnexo_NomeArq]";
         }
         else if ( ( AV14OrderedBy == 1 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[LoteArquivoAnexo_NomeArq] DESC";
         }
         else if ( ( AV14OrderedBy == 2 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[LoteArquivoAnexo_LoteCod]";
         }
         else if ( ( AV14OrderedBy == 2 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[LoteArquivoAnexo_LoteCod] DESC";
         }
         else if ( ( AV14OrderedBy == 3 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[LoteArquivoAnexo_Data]";
         }
         else if ( ( AV14OrderedBy == 3 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[LoteArquivoAnexo_Data] DESC";
         }
         else if ( ( AV14OrderedBy == 4 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[LoteArquivoAnexo_TipoArq]";
         }
         else if ( ( AV14OrderedBy == 4 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[LoteArquivoAnexo_TipoArq] DESC";
         }
         else if ( ( AV14OrderedBy == 5 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[LoteArquivoAnexo_Descricao]";
         }
         else if ( ( AV14OrderedBy == 5 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[LoteArquivoAnexo_Descricao] DESC";
         }
         else if ( ( AV14OrderedBy == 6 ) && ! AV15OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T2.[TipoDocumento_Nome]";
         }
         else if ( ( AV14OrderedBy == 6 ) && ( AV15OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T2.[TipoDocumento_Nome] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY T1.[LoteArquivoAnexo_LoteCod], T1.[LoteArquivoAnexo_Data]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      protected Object[] conditional_H00EX3( IGxContext context ,
                                             String AV16DynamicFiltersSelector1 ,
                                             short AV17DynamicFiltersOperator1 ,
                                             String AV18LoteArquivoAnexo_NomeArq1 ,
                                             String AV19TipoDocumento_Nome1 ,
                                             bool AV20DynamicFiltersEnabled2 ,
                                             String AV21DynamicFiltersSelector2 ,
                                             short AV22DynamicFiltersOperator2 ,
                                             String AV23LoteArquivoAnexo_NomeArq2 ,
                                             String AV24TipoDocumento_Nome2 ,
                                             bool AV25DynamicFiltersEnabled3 ,
                                             String AV26DynamicFiltersSelector3 ,
                                             short AV27DynamicFiltersOperator3 ,
                                             String AV28LoteArquivoAnexo_NomeArq3 ,
                                             String AV29TipoDocumento_Nome3 ,
                                             int AV35TFLoteArquivoAnexo_LoteCod ,
                                             int AV36TFLoteArquivoAnexo_LoteCod_To ,
                                             DateTime AV39TFLoteArquivoAnexo_Data ,
                                             DateTime AV40TFLoteArquivoAnexo_Data_To ,
                                             String AV46TFLoteArquivoAnexo_NomeArq_Sel ,
                                             String AV45TFLoteArquivoAnexo_NomeArq ,
                                             String AV50TFLoteArquivoAnexo_TipoArq_Sel ,
                                             String AV49TFLoteArquivoAnexo_TipoArq ,
                                             String AV54TFLoteArquivoAnexo_Descricao_Sel ,
                                             String AV53TFLoteArquivoAnexo_Descricao ,
                                             String AV58TFTipoDocumento_Nome_Sel ,
                                             String AV57TFTipoDocumento_Nome ,
                                             String A839LoteArquivoAnexo_NomeArq ,
                                             String A646TipoDocumento_Nome ,
                                             int A841LoteArquivoAnexo_LoteCod ,
                                             DateTime A836LoteArquivoAnexo_Data ,
                                             String A840LoteArquivoAnexo_TipoArq ,
                                             String A837LoteArquivoAnexo_Descricao ,
                                             short AV14OrderedBy ,
                                             bool AV15OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [24] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM ([LoteArquivoAnexo] T1 WITH (NOLOCK) LEFT JOIN [TipoDocumento] T2 WITH (NOLOCK) ON T2.[TipoDocumento_Codigo] = T1.[TipoDocumento_Codigo])";
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "LOTEARQUIVOANEXO_NOMEARQ") == 0 ) && ( AV17DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV18LoteArquivoAnexo_NomeArq1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[LoteArquivoAnexo_NomeArq] like @lV18LoteArquivoAnexo_NomeArq1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[LoteArquivoAnexo_NomeArq] like @lV18LoteArquivoAnexo_NomeArq1)";
            }
         }
         else
         {
            GXv_int4[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "LOTEARQUIVOANEXO_NOMEARQ") == 0 ) && ( AV17DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV18LoteArquivoAnexo_NomeArq1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[LoteArquivoAnexo_NomeArq] like '%' + @lV18LoteArquivoAnexo_NomeArq1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[LoteArquivoAnexo_NomeArq] like '%' + @lV18LoteArquivoAnexo_NomeArq1)";
            }
         }
         else
         {
            GXv_int4[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "TIPODOCUMENTO_NOME") == 0 ) && ( AV17DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV19TipoDocumento_Nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[TipoDocumento_Nome] like @lV19TipoDocumento_Nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[TipoDocumento_Nome] like @lV19TipoDocumento_Nome1)";
            }
         }
         else
         {
            GXv_int4[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "TIPODOCUMENTO_NOME") == 0 ) && ( AV17DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV19TipoDocumento_Nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[TipoDocumento_Nome] like '%' + @lV19TipoDocumento_Nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[TipoDocumento_Nome] like '%' + @lV19TipoDocumento_Nome1)";
            }
         }
         else
         {
            GXv_int4[3] = 1;
         }
         if ( AV20DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "LOTEARQUIVOANEXO_NOMEARQ") == 0 ) && ( AV22DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV23LoteArquivoAnexo_NomeArq2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[LoteArquivoAnexo_NomeArq] like @lV23LoteArquivoAnexo_NomeArq2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[LoteArquivoAnexo_NomeArq] like @lV23LoteArquivoAnexo_NomeArq2)";
            }
         }
         else
         {
            GXv_int4[4] = 1;
         }
         if ( AV20DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "LOTEARQUIVOANEXO_NOMEARQ") == 0 ) && ( AV22DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV23LoteArquivoAnexo_NomeArq2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[LoteArquivoAnexo_NomeArq] like '%' + @lV23LoteArquivoAnexo_NomeArq2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[LoteArquivoAnexo_NomeArq] like '%' + @lV23LoteArquivoAnexo_NomeArq2)";
            }
         }
         else
         {
            GXv_int4[5] = 1;
         }
         if ( AV20DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "TIPODOCUMENTO_NOME") == 0 ) && ( AV22DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV24TipoDocumento_Nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[TipoDocumento_Nome] like @lV24TipoDocumento_Nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[TipoDocumento_Nome] like @lV24TipoDocumento_Nome2)";
            }
         }
         else
         {
            GXv_int4[6] = 1;
         }
         if ( AV20DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "TIPODOCUMENTO_NOME") == 0 ) && ( AV22DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV24TipoDocumento_Nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[TipoDocumento_Nome] like '%' + @lV24TipoDocumento_Nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[TipoDocumento_Nome] like '%' + @lV24TipoDocumento_Nome2)";
            }
         }
         else
         {
            GXv_int4[7] = 1;
         }
         if ( AV25DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV26DynamicFiltersSelector3, "LOTEARQUIVOANEXO_NOMEARQ") == 0 ) && ( AV27DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV28LoteArquivoAnexo_NomeArq3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[LoteArquivoAnexo_NomeArq] like @lV28LoteArquivoAnexo_NomeArq3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[LoteArquivoAnexo_NomeArq] like @lV28LoteArquivoAnexo_NomeArq3)";
            }
         }
         else
         {
            GXv_int4[8] = 1;
         }
         if ( AV25DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV26DynamicFiltersSelector3, "LOTEARQUIVOANEXO_NOMEARQ") == 0 ) && ( AV27DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV28LoteArquivoAnexo_NomeArq3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[LoteArquivoAnexo_NomeArq] like '%' + @lV28LoteArquivoAnexo_NomeArq3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[LoteArquivoAnexo_NomeArq] like '%' + @lV28LoteArquivoAnexo_NomeArq3)";
            }
         }
         else
         {
            GXv_int4[9] = 1;
         }
         if ( AV25DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV26DynamicFiltersSelector3, "TIPODOCUMENTO_NOME") == 0 ) && ( AV27DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV29TipoDocumento_Nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[TipoDocumento_Nome] like @lV29TipoDocumento_Nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[TipoDocumento_Nome] like @lV29TipoDocumento_Nome3)";
            }
         }
         else
         {
            GXv_int4[10] = 1;
         }
         if ( AV25DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV26DynamicFiltersSelector3, "TIPODOCUMENTO_NOME") == 0 ) && ( AV27DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV29TipoDocumento_Nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[TipoDocumento_Nome] like '%' + @lV29TipoDocumento_Nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[TipoDocumento_Nome] like '%' + @lV29TipoDocumento_Nome3)";
            }
         }
         else
         {
            GXv_int4[11] = 1;
         }
         if ( ! (0==AV35TFLoteArquivoAnexo_LoteCod) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[LoteArquivoAnexo_LoteCod] >= @AV35TFLoteArquivoAnexo_LoteCod)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[LoteArquivoAnexo_LoteCod] >= @AV35TFLoteArquivoAnexo_LoteCod)";
            }
         }
         else
         {
            GXv_int4[12] = 1;
         }
         if ( ! (0==AV36TFLoteArquivoAnexo_LoteCod_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[LoteArquivoAnexo_LoteCod] <= @AV36TFLoteArquivoAnexo_LoteCod_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[LoteArquivoAnexo_LoteCod] <= @AV36TFLoteArquivoAnexo_LoteCod_To)";
            }
         }
         else
         {
            GXv_int4[13] = 1;
         }
         if ( ! (DateTime.MinValue==AV39TFLoteArquivoAnexo_Data) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[LoteArquivoAnexo_Data] >= @AV39TFLoteArquivoAnexo_Data)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[LoteArquivoAnexo_Data] >= @AV39TFLoteArquivoAnexo_Data)";
            }
         }
         else
         {
            GXv_int4[14] = 1;
         }
         if ( ! (DateTime.MinValue==AV40TFLoteArquivoAnexo_Data_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[LoteArquivoAnexo_Data] <= @AV40TFLoteArquivoAnexo_Data_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[LoteArquivoAnexo_Data] <= @AV40TFLoteArquivoAnexo_Data_To)";
            }
         }
         else
         {
            GXv_int4[15] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV46TFLoteArquivoAnexo_NomeArq_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV45TFLoteArquivoAnexo_NomeArq)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[LoteArquivoAnexo_NomeArq] like @lV45TFLoteArquivoAnexo_NomeArq)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[LoteArquivoAnexo_NomeArq] like @lV45TFLoteArquivoAnexo_NomeArq)";
            }
         }
         else
         {
            GXv_int4[16] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV46TFLoteArquivoAnexo_NomeArq_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[LoteArquivoAnexo_NomeArq] = @AV46TFLoteArquivoAnexo_NomeArq_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[LoteArquivoAnexo_NomeArq] = @AV46TFLoteArquivoAnexo_NomeArq_Sel)";
            }
         }
         else
         {
            GXv_int4[17] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV50TFLoteArquivoAnexo_TipoArq_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV49TFLoteArquivoAnexo_TipoArq)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[LoteArquivoAnexo_TipoArq] like @lV49TFLoteArquivoAnexo_TipoArq)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[LoteArquivoAnexo_TipoArq] like @lV49TFLoteArquivoAnexo_TipoArq)";
            }
         }
         else
         {
            GXv_int4[18] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV50TFLoteArquivoAnexo_TipoArq_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[LoteArquivoAnexo_TipoArq] = @AV50TFLoteArquivoAnexo_TipoArq_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[LoteArquivoAnexo_TipoArq] = @AV50TFLoteArquivoAnexo_TipoArq_Sel)";
            }
         }
         else
         {
            GXv_int4[19] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV54TFLoteArquivoAnexo_Descricao_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV53TFLoteArquivoAnexo_Descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[LoteArquivoAnexo_Descricao] like @lV53TFLoteArquivoAnexo_Descricao)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[LoteArquivoAnexo_Descricao] like @lV53TFLoteArquivoAnexo_Descricao)";
            }
         }
         else
         {
            GXv_int4[20] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV54TFLoteArquivoAnexo_Descricao_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[LoteArquivoAnexo_Descricao] = @AV54TFLoteArquivoAnexo_Descricao_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[LoteArquivoAnexo_Descricao] = @AV54TFLoteArquivoAnexo_Descricao_Sel)";
            }
         }
         else
         {
            GXv_int4[21] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV58TFTipoDocumento_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV57TFTipoDocumento_Nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[TipoDocumento_Nome] like @lV57TFTipoDocumento_Nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[TipoDocumento_Nome] like @lV57TFTipoDocumento_Nome)";
            }
         }
         else
         {
            GXv_int4[22] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV58TFTipoDocumento_Nome_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[TipoDocumento_Nome] = @AV58TFTipoDocumento_Nome_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[TipoDocumento_Nome] = @AV58TFTipoDocumento_Nome_Sel)";
            }
         }
         else
         {
            GXv_int4[23] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         if ( ( AV14OrderedBy == 1 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 1 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 2 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 2 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 3 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 3 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 4 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 4 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 5 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 5 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 6 ) && ! AV15OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV14OrderedBy == 6 ) && ( AV15OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H00EX2(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (bool)dynConstraints[4] , (String)dynConstraints[5] , (short)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (bool)dynConstraints[9] , (String)dynConstraints[10] , (short)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (int)dynConstraints[14] , (int)dynConstraints[15] , (DateTime)dynConstraints[16] , (DateTime)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] , (String)dynConstraints[26] , (String)dynConstraints[27] , (int)dynConstraints[28] , (DateTime)dynConstraints[29] , (String)dynConstraints[30] , (String)dynConstraints[31] , (short)dynConstraints[32] , (bool)dynConstraints[33] );
               case 1 :
                     return conditional_H00EX3(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (bool)dynConstraints[4] , (String)dynConstraints[5] , (short)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (bool)dynConstraints[9] , (String)dynConstraints[10] , (short)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (int)dynConstraints[14] , (int)dynConstraints[15] , (DateTime)dynConstraints[16] , (DateTime)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] , (String)dynConstraints[26] , (String)dynConstraints[27] , (int)dynConstraints[28] , (DateTime)dynConstraints[29] , (String)dynConstraints[30] , (String)dynConstraints[31] , (short)dynConstraints[32] , (bool)dynConstraints[33] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00EX2 ;
          prmH00EX2 = new Object[] {
          new Object[] {"@lV18LoteArquivoAnexo_NomeArq1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV18LoteArquivoAnexo_NomeArq1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV19TipoDocumento_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV19TipoDocumento_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV23LoteArquivoAnexo_NomeArq2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV23LoteArquivoAnexo_NomeArq2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV24TipoDocumento_Nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV24TipoDocumento_Nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV28LoteArquivoAnexo_NomeArq3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV28LoteArquivoAnexo_NomeArq3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV29TipoDocumento_Nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV29TipoDocumento_Nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@AV35TFLoteArquivoAnexo_LoteCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV36TFLoteArquivoAnexo_LoteCod_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV39TFLoteArquivoAnexo_Data",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV40TFLoteArquivoAnexo_Data_To",SqlDbType.DateTime,8,5} ,
          new Object[] {"@lV45TFLoteArquivoAnexo_NomeArq",SqlDbType.Char,50,0} ,
          new Object[] {"@AV46TFLoteArquivoAnexo_NomeArq_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV49TFLoteArquivoAnexo_TipoArq",SqlDbType.Char,10,0} ,
          new Object[] {"@AV50TFLoteArquivoAnexo_TipoArq_Sel",SqlDbType.Char,10,0} ,
          new Object[] {"@lV53TFLoteArquivoAnexo_Descricao",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV54TFLoteArquivoAnexo_Descricao_Sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV57TFTipoDocumento_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV58TFTipoDocumento_Nome_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH00EX3 ;
          prmH00EX3 = new Object[] {
          new Object[] {"@lV18LoteArquivoAnexo_NomeArq1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV18LoteArquivoAnexo_NomeArq1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV19TipoDocumento_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV19TipoDocumento_Nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV23LoteArquivoAnexo_NomeArq2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV23LoteArquivoAnexo_NomeArq2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV24TipoDocumento_Nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV24TipoDocumento_Nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV28LoteArquivoAnexo_NomeArq3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV28LoteArquivoAnexo_NomeArq3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV29TipoDocumento_Nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV29TipoDocumento_Nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@AV35TFLoteArquivoAnexo_LoteCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV36TFLoteArquivoAnexo_LoteCod_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV39TFLoteArquivoAnexo_Data",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV40TFLoteArquivoAnexo_Data_To",SqlDbType.DateTime,8,5} ,
          new Object[] {"@lV45TFLoteArquivoAnexo_NomeArq",SqlDbType.Char,50,0} ,
          new Object[] {"@AV46TFLoteArquivoAnexo_NomeArq_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV49TFLoteArquivoAnexo_TipoArq",SqlDbType.Char,10,0} ,
          new Object[] {"@AV50TFLoteArquivoAnexo_TipoArq_Sel",SqlDbType.Char,10,0} ,
          new Object[] {"@lV53TFLoteArquivoAnexo_Descricao",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV54TFLoteArquivoAnexo_Descricao_Sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@lV57TFTipoDocumento_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV58TFTipoDocumento_Nome_Sel",SqlDbType.Char,50,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00EX2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00EX2,11,0,true,false )
             ,new CursorDef("H00EX3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00EX3,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 10) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getString(3, 50) ;
                ((int[]) buf[5])[0] = rslt.getInt(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((String[]) buf[7])[0] = rslt.getLongVarchar(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((DateTime[]) buf[9])[0] = rslt.getGXDateTime(6) ;
                ((int[]) buf[10])[0] = rslt.getInt(7) ;
                ((String[]) buf[11])[0] = rslt.getBLOBFile(8, rslt.getString(2, 10), rslt.getString(1, 50)) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(8);
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[36]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[37]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[38]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[39]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[40]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[41]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[42]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[43]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[44]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[45]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[46]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[47]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[48]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[49]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[50]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[51]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[52]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[53]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[54]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[55]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[56]);
                }
                if ( (short)parms[28] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[57]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[32]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[36]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[37]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[38]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[39]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[40]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[41]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[42]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[43]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[44]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[45]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[46]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[47]);
                }
                return;
       }
    }

 }

}
