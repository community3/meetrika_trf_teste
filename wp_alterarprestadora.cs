/*
               File: WP_AlterarPrestadora
        Description: Alterar Prestadora
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/2/2020 16:26:33.96
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wp_alterarprestadora : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wp_alterarprestadora( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wp_alterarprestadora( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Codigo )
      {
         this.AV8Codigo = aP0_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavNovaprestadora = new GXCombobox();
         cmbavContrato = new GXCombobox();
         cmbavTipodias = new GXCombobox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               AV8Codigo = (int)(NumberUtil.Val( gxfirstwebparm, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8Codigo), 6, 0)));
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PAOH2( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            STARTOH2( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20206216263539");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wp_alterarprestadora.aspx") + "?" + UrlEncode("" +AV8Codigo)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "CONTRATADA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A39Contratada_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "SERVICO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A155Servico_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vSERVICO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV28Servico_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_boolean_hidden_field( context, "CONTRATO_ATIVO", A92Contrato_Ativo);
         GxWebStd.gx_hidden_field( context, "CONTRATO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A74Contrato_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATO_NUMERO", StringUtil.RTrim( A77Contrato_Numero));
         GxWebStd.gx_hidden_field( context, "CONTRATOSERVICOS_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A160ContratoServicos_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATOSERVICOS_PRAZOTPDIAS", StringUtil.RTrim( A1454ContratoServicos_PrazoTpDias));
         GxWebStd.gx_hidden_field( context, "CONTRATO_PREPOSTOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1013Contrato_PrepostoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vCODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV8Codigo), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vWWPCONTEXT", AV26WWPContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vWWPCONTEXT", AV26WWPContext);
         }
         GxWebStd.gx_hidden_field( context, "vSTATUSANTERIOR", StringUtil.RTrim( AV29StatusAnterior));
         GxWebStd.gx_boolean_hidden_field( context, "vCONTRATANTESEMEMAILSDA", AV40ContratanteSemEmailSda);
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vUSUARIOS", AV38Usuarios);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vUSUARIOS", AV38Usuarios);
         }
         GxWebStd.gx_hidden_field( context, "vPREPOSTO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV35Preposto), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vPREPOSTONOVAPRESTADORA", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV36PrepostoNovaPrestadora), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vOWNER", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV39Owner), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vDEMANDA", AV12Demanda);
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCODIGOS", AV48Codigos);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCODIGOS", AV48Codigos);
         }
         GxWebStd.gx_hidden_field( context, "vEMAILTEXT", StringUtil.RTrim( AV45EmailText));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vATTACHMENTS", AV46Attachments);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vATTACHMENTS", AV46Attachments);
         }
         GxWebStd.gx_hidden_field( context, "vRESULTADO", StringUtil.RTrim( AV47Resultado));
         GxWebStd.gx_hidden_field( context, "CONTRATO_DATATERMINO", context.localUtil.DToC( A1869Contrato_DataTermino, 0, "/"));
         GxWebStd.gx_hidden_field( context, "gxhash_vPRESTADORA", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV25Prestadora, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vPRESTADORA", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV25Prestadora, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vPRESTADORA", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV25Prestadora, "@!"))));
         GxWebStd.gx_hidden_field( context, "vNOVAPRESTADORA_Text", StringUtil.RTrim( cmbavNovaprestadora.Description));
         GxWebStd.gx_hidden_field( context, "vCONTRATO_Text", StringUtil.RTrim( cmbavContrato.Description));
         GxWebStd.gx_hidden_field( context, "vWWPCONTEXT_Userid", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV26WWPContext.gxTpr_Userid), 4, 0, ".", "")));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = "hsh" + "WP_AlterarPrestadora";
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( AV25Prestadora, "@!"));
         GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("wp_alterarprestadora:[SendSecurityCheck value for]"+"Prestadora:"+StringUtil.RTrim( context.localUtil.Format( AV25Prestadora, "@!")));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WEOH2( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVTOH2( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wp_alterarprestadora.aspx") + "?" + UrlEncode("" +AV8Codigo) ;
      }

      public override String GetPgmname( )
      {
         return "WP_AlterarPrestadora" ;
      }

      public override String GetPgmdesc( )
      {
         return "Alterar Prestadora" ;
      }

      protected void WBOH0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            wb_table1_2_OH2( true) ;
         }
         else
         {
            wb_table1_2_OH2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_OH2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbjava_Internalname, lblTbjava_Caption, "", "", lblTbjava_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", lblTbjava_Visible, 1, 1, "HLP_WP_AlterarPrestadora.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 42,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContratoservicos_codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV11ContratoServicos_Codigo), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV11ContratoServicos_Codigo), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,42);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContratoservicos_codigo_Jsonclick, 0, "Attribute", "", "", "", edtavContratoservicos_codigo_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WP_AlterarPrestadora.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 43,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavTipodias, cmbavTipodias_Internalname, StringUtil.RTrim( AV34TipoDias), 1, cmbavTipodias_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", cmbavTipodias.Visible, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,43);\"", "", true, "HLP_WP_AlterarPrestadora.htm");
            cmbavTipodias.CurrentValue = StringUtil.RTrim( AV34TipoDias);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavTipodias_Internalname, "Values", (String)(cmbavTipodias.ToJavascriptSource()));
         }
         wbLoad = true;
      }

      protected void STARTOH2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Alterar Prestadora", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPOH0( ) ;
      }

      protected void WSOH2( )
      {
         STARTOH2( ) ;
         EVTOH2( ) ;
      }

      protected void EVTOH2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E11OH2 */
                              E11OH2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VNOVAPRESTADORA.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E12OH2 */
                              E12OH2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VCONTRATO.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E13OH2 */
                              E13OH2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              context.wbHandled = 1;
                              if ( ! wbErr )
                              {
                                 Rfr0gs = false;
                                 if ( ! Rfr0gs )
                                 {
                                    /* Execute user event: E14OH2 */
                                    E14OH2 ();
                                 }
                                 dynload_actions( ) ;
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOFECHAR'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E15OH2 */
                              E15OH2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E16OH2 */
                              E16OH2 ();
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEOH2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PAOH2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbavNovaprestadora.Name = "vNOVAPRESTADORA";
            cmbavNovaprestadora.WebTags = "";
            cmbavNovaprestadora.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 6, 0)), "(Nenhuma)", 0);
            if ( cmbavNovaprestadora.ItemCount > 0 )
            {
               AV18NovaPrestadora = (int)(NumberUtil.Val( cmbavNovaprestadora.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV18NovaPrestadora), 6, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18NovaPrestadora", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18NovaPrestadora), 6, 0)));
            }
            cmbavContrato.Name = "vCONTRATO";
            cmbavContrato.WebTags = "";
            cmbavContrato.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 6, 0)), "(Nenhum)�������", 0);
            if ( cmbavContrato.ItemCount > 0 )
            {
               AV10Contrato = (int)(NumberUtil.Val( cmbavContrato.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV10Contrato), 6, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10Contrato", StringUtil.LTrim( StringUtil.Str( (decimal)(AV10Contrato), 6, 0)));
            }
            cmbavTipodias.Name = "vTIPODIAS";
            cmbavTipodias.WebTags = "";
            cmbavTipodias.addItem("", "(Nenhum)", 0);
            cmbavTipodias.addItem("U", "�teis", 0);
            cmbavTipodias.addItem("C", "Corridos", 0);
            if ( cmbavTipodias.ItemCount > 0 )
            {
               AV34TipoDias = cmbavTipodias.getValidValue(AV34TipoDias);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34TipoDias", AV34TipoDias);
            }
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = edtavPrestadora_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavNovaprestadora.ItemCount > 0 )
         {
            AV18NovaPrestadora = (int)(NumberUtil.Val( cmbavNovaprestadora.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV18NovaPrestadora), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18NovaPrestadora", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18NovaPrestadora), 6, 0)));
         }
         if ( cmbavContrato.ItemCount > 0 )
         {
            AV10Contrato = (int)(NumberUtil.Val( cmbavContrato.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV10Contrato), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10Contrato", StringUtil.LTrim( StringUtil.Str( (decimal)(AV10Contrato), 6, 0)));
         }
         if ( cmbavTipodias.ItemCount > 0 )
         {
            AV34TipoDias = cmbavTipodias.getValidValue(AV34TipoDias);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34TipoDias", AV34TipoDias);
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFOH2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
         edtavPrestadora_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPrestadora_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPrestadora_Enabled), 5, 0)));
      }

      protected void RFOH2( )
      {
         initialize_formulas( ) ;
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Execute user event: E16OH2 */
            E16OH2 ();
            WBOH0( ) ;
         }
      }

      protected void STRUPOH0( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         edtavPrestadora_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPrestadora_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPrestadora_Enabled), 5, 0)));
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11OH2 */
         E11OH2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            AV25Prestadora = StringUtil.Upper( cgiGet( edtavPrestadora_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25Prestadora", AV25Prestadora);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vPRESTADORA", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV25Prestadora, "@!"))));
            cmbavNovaprestadora.CurrentValue = cgiGet( cmbavNovaprestadora_Internalname);
            AV18NovaPrestadora = (int)(NumberUtil.Val( cgiGet( cmbavNovaprestadora_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18NovaPrestadora", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18NovaPrestadora), 6, 0)));
            cmbavContrato.CurrentValue = cgiGet( cmbavContrato_Internalname);
            AV10Contrato = (int)(NumberUtil.Val( cgiGet( cmbavContrato_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10Contrato", StringUtil.LTrim( StringUtil.Str( (decimal)(AV10Contrato), 6, 0)));
            AV19Observacao = cgiGet( edtavObservacao_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19Observacao", AV19Observacao);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavContratoservicos_codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavContratoservicos_codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vCONTRATOSERVICOS_CODIGO");
               GX_FocusControl = edtavContratoservicos_codigo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV11ContratoServicos_Codigo = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11ContratoServicos_Codigo), 6, 0)));
            }
            else
            {
               AV11ContratoServicos_Codigo = (int)(context.localUtil.CToN( cgiGet( edtavContratoservicos_codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11ContratoServicos_Codigo), 6, 0)));
            }
            cmbavTipodias.CurrentValue = cgiGet( cmbavTipodias_Internalname);
            AV34TipoDias = cgiGet( cmbavTipodias_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34TipoDias", AV34TipoDias);
            /* Read saved values. */
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            forbiddenHiddens = "hsh" + "WP_AlterarPrestadora";
            AV25Prestadora = cgiGet( edtavPrestadora_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25Prestadora", AV25Prestadora);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vPRESTADORA", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV25Prestadora, "@!"))));
            forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( AV25Prestadora, "@!"));
            hsh = cgiGet( "hsh");
            if ( ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
            {
               GXUtil.WriteLog("wp_alterarprestadora:[SecurityCheckFailed value for]"+"Prestadora:"+StringUtil.RTrim( context.localUtil.Format( AV25Prestadora, "@!")));
               GxWebError = 1;
               context.HttpContext.Response.StatusDescription = 403.ToString();
               context.HttpContext.Response.StatusCode = 403;
               context.WriteHtmlText( "<title>403 Forbidden</title>") ;
               context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
               context.WriteHtmlText( "<p /><hr />") ;
               GXUtil.WriteLog("send_http_error_code " + 403.ToString());
               return  ;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E11OH2 */
         E11OH2 ();
         if (returnInSub) return;
      }

      protected void E11OH2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV26WWPContext) ;
         edtavContratoservicos_codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavContratoservicos_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContratoservicos_codigo_Visible), 5, 0)));
         cmbavTipodias.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavTipodias_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavTipodias.Visible), 5, 0)));
         lblTbjava_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbjava_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTbjava_Visible), 5, 0)));
         bttBtnenter_Visible = (AV26WWPContext.gxTpr_Update ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnenter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnenter_Visible), 5, 0)));
         /* Using cursor H00OH2 */
         pr_default.execute(0, new Object[] {AV8Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A146Modulo_Codigo = H00OH2_A146Modulo_Codigo[0];
            n146Modulo_Codigo = H00OH2_n146Modulo_Codigo[0];
            A127Sistema_Codigo = H00OH2_A127Sistema_Codigo[0];
            A135Sistema_AreaTrabalhoCod = H00OH2_A135Sistema_AreaTrabalhoCod[0];
            A499ContagemResultado_ContratadaPessoaCod = H00OH2_A499ContagemResultado_ContratadaPessoaCod[0];
            n499ContagemResultado_ContratadaPessoaCod = H00OH2_n499ContagemResultado_ContratadaPessoaCod[0];
            A1553ContagemResultado_CntSrvCod = H00OH2_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = H00OH2_n1553ContagemResultado_CntSrvCod[0];
            A1603ContagemResultado_CntCod = H00OH2_A1603ContagemResultado_CntCod[0];
            n1603ContagemResultado_CntCod = H00OH2_n1603ContagemResultado_CntCod[0];
            A29Contratante_Codigo = H00OH2_A29Contratante_Codigo[0];
            n29Contratante_Codigo = H00OH2_n29Contratante_Codigo[0];
            A456ContagemResultado_Codigo = H00OH2_A456ContagemResultado_Codigo[0];
            A490ContagemResultado_ContratadaCod = H00OH2_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = H00OH2_n490ContagemResultado_ContratadaCod[0];
            A601ContagemResultado_Servico = H00OH2_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = H00OH2_n601ContagemResultado_Servico[0];
            A484ContagemResultado_StatusDmn = H00OH2_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = H00OH2_n484ContagemResultado_StatusDmn[0];
            A500ContagemResultado_ContratadaPessoaNom = H00OH2_A500ContagemResultado_ContratadaPessoaNom[0];
            n500ContagemResultado_ContratadaPessoaNom = H00OH2_n500ContagemResultado_ContratadaPessoaNom[0];
            A493ContagemResultado_DemandaFM = H00OH2_A493ContagemResultado_DemandaFM[0];
            n493ContagemResultado_DemandaFM = H00OH2_n493ContagemResultado_DemandaFM[0];
            A1604ContagemResultado_CntPrpCod = H00OH2_A1604ContagemResultado_CntPrpCod[0];
            n1604ContagemResultado_CntPrpCod = H00OH2_n1604ContagemResultado_CntPrpCod[0];
            A508ContagemResultado_Owner = H00OH2_A508ContagemResultado_Owner[0];
            A548Contratante_EmailSdaUser = H00OH2_A548Contratante_EmailSdaUser[0];
            n548Contratante_EmailSdaUser = H00OH2_n548Contratante_EmailSdaUser[0];
            A552Contratante_EmailSdaPort = H00OH2_A552Contratante_EmailSdaPort[0];
            n552Contratante_EmailSdaPort = H00OH2_n552Contratante_EmailSdaPort[0];
            A547Contratante_EmailSdaHost = H00OH2_A547Contratante_EmailSdaHost[0];
            n547Contratante_EmailSdaHost = H00OH2_n547Contratante_EmailSdaHost[0];
            A127Sistema_Codigo = H00OH2_A127Sistema_Codigo[0];
            A135Sistema_AreaTrabalhoCod = H00OH2_A135Sistema_AreaTrabalhoCod[0];
            A29Contratante_Codigo = H00OH2_A29Contratante_Codigo[0];
            n29Contratante_Codigo = H00OH2_n29Contratante_Codigo[0];
            A548Contratante_EmailSdaUser = H00OH2_A548Contratante_EmailSdaUser[0];
            n548Contratante_EmailSdaUser = H00OH2_n548Contratante_EmailSdaUser[0];
            A552Contratante_EmailSdaPort = H00OH2_A552Contratante_EmailSdaPort[0];
            n552Contratante_EmailSdaPort = H00OH2_n552Contratante_EmailSdaPort[0];
            A547Contratante_EmailSdaHost = H00OH2_A547Contratante_EmailSdaHost[0];
            n547Contratante_EmailSdaHost = H00OH2_n547Contratante_EmailSdaHost[0];
            A1603ContagemResultado_CntCod = H00OH2_A1603ContagemResultado_CntCod[0];
            n1603ContagemResultado_CntCod = H00OH2_n1603ContagemResultado_CntCod[0];
            A601ContagemResultado_Servico = H00OH2_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = H00OH2_n601ContagemResultado_Servico[0];
            A1604ContagemResultado_CntPrpCod = H00OH2_A1604ContagemResultado_CntPrpCod[0];
            n1604ContagemResultado_CntPrpCod = H00OH2_n1604ContagemResultado_CntPrpCod[0];
            A499ContagemResultado_ContratadaPessoaCod = H00OH2_A499ContagemResultado_ContratadaPessoaCod[0];
            n499ContagemResultado_ContratadaPessoaCod = H00OH2_n499ContagemResultado_ContratadaPessoaCod[0];
            A500ContagemResultado_ContratadaPessoaNom = H00OH2_A500ContagemResultado_ContratadaPessoaNom[0];
            n500ContagemResultado_ContratadaPessoaNom = H00OH2_n500ContagemResultado_ContratadaPessoaNom[0];
            AV9Contratada_Codigo = A490ContagemResultado_ContratadaCod;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9Contratada_Codigo), 6, 0)));
            AV28Servico_Codigo = A601ContagemResultado_Servico;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28Servico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV28Servico_Codigo), 6, 0)));
            AV29StatusAnterior = A484ContagemResultado_StatusDmn;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29StatusAnterior", AV29StatusAnterior);
            AV25Prestadora = A500ContagemResultado_ContratadaPessoaNom;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25Prestadora", AV25Prestadora);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vPRESTADORA", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV25Prestadora, "@!"))));
            AV12Demanda = A493ContagemResultado_DemandaFM;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12Demanda", AV12Demanda);
            AV35Preposto = (short)(A1604ContagemResultado_CntPrpCod);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35Preposto", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35Preposto), 4, 0)));
            AV39Owner = A508ContagemResultado_Owner;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39Owner", StringUtil.LTrim( StringUtil.Str( (decimal)(AV39Owner), 6, 0)));
            if ( H00OH2_n547Contratante_EmailSdaHost[0] || H00OH2_n552Contratante_EmailSdaPort[0] || H00OH2_n548Contratante_EmailSdaUser[0] )
            {
               GX_msglist.addItem("Cadastro da Contratante sem dados configurados para envio de e-mails!");
               AV40ContratanteSemEmailSda = true;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40ContratanteSemEmailSda", AV40ContratanteSemEmailSda);
            }
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(0);
         Form.Caption = "Alterar Servi�o da OS "+StringUtil.Trim( AV12Demanda);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
         /* Using cursor H00OH6 */
         pr_default.execute(1, new Object[] {AV9Contratada_Codigo, AV26WWPContext.gxTpr_Areatrabalho_codigo, AV28Servico_Codigo});
         while ( (pr_default.getStatus(1) != 101) )
         {
            A40Contratada_PessoaCod = H00OH6_A40Contratada_PessoaCod[0];
            A75Contrato_AreaTrabalhoCod = H00OH6_A75Contrato_AreaTrabalhoCod[0];
            A155Servico_Codigo = H00OH6_A155Servico_Codigo[0];
            A74Contrato_Codigo = H00OH6_A74Contrato_Codigo[0];
            n74Contrato_Codigo = H00OH6_n74Contrato_Codigo[0];
            A41Contratada_PessoaNom = H00OH6_A41Contratada_PessoaNom[0];
            n41Contratada_PessoaNom = H00OH6_n41Contratada_PessoaNom[0];
            A39Contratada_Codigo = H00OH6_A39Contratada_Codigo[0];
            A92Contrato_Ativo = H00OH6_A92Contrato_Ativo[0];
            A1869Contrato_DataTermino = H00OH6_A1869Contrato_DataTermino[0];
            n1869Contrato_DataTermino = H00OH6_n1869Contrato_DataTermino[0];
            A75Contrato_AreaTrabalhoCod = H00OH6_A75Contrato_AreaTrabalhoCod[0];
            A39Contratada_Codigo = H00OH6_A39Contratada_Codigo[0];
            A92Contrato_Ativo = H00OH6_A92Contrato_Ativo[0];
            A40Contratada_PessoaCod = H00OH6_A40Contratada_PessoaCod[0];
            A41Contratada_PessoaNom = H00OH6_A41Contratada_PessoaNom[0];
            n41Contratada_PessoaNom = H00OH6_n41Contratada_PessoaNom[0];
            A1869Contrato_DataTermino = H00OH6_A1869Contrato_DataTermino[0];
            n1869Contrato_DataTermino = H00OH6_n1869Contrato_DataTermino[0];
            if ( A1869Contrato_DataTermino <= DateTimeUtil.ServerDate( context, "DEFAULT") )
            {
               /* Using cursor H00OH7 */
               pr_default.execute(2, new Object[] {n74Contrato_Codigo, A74Contrato_Codigo, AV26WWPContext.gxTpr_Userid});
               while ( (pr_default.getStatus(2) != 101) )
               {
                  A1078ContratoGestor_ContratoCod = H00OH7_A1078ContratoGestor_ContratoCod[0];
                  A1079ContratoGestor_UsuarioCod = H00OH7_A1079ContratoGestor_UsuarioCod[0];
                  cmbavNovaprestadora.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(A39Contratada_Codigo), 6, 0)), A41Contratada_PessoaNom, 0);
                  /* Exit For each command. Update data (if necessary), close cursors & exit. */
                  if (true) break;
                  /* Exiting from a For First loop. */
                  if (true) break;
               }
               pr_default.close(2);
            }
            pr_default.readNext(1);
         }
         pr_default.close(1);
      }

      protected void E12OH2( )
      {
         /* Novaprestadora_Click Routine */
         cmbavContrato.removeAllItems();
         if ( (0==AV18NovaPrestadora) )
         {
            cmbavContrato.addItem("0", "(Nenhum)�������", 0);
            AV10Contrato = 0;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10Contrato", StringUtil.LTrim( StringUtil.Str( (decimal)(AV10Contrato), 6, 0)));
            AV11ContratoServicos_Codigo = 0;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11ContratoServicos_Codigo), 6, 0)));
            AV34TipoDias = "";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34TipoDias", AV34TipoDias);
            AV36PrepostoNovaPrestadora = 0;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36PrepostoNovaPrestadora", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36PrepostoNovaPrestadora), 6, 0)));
         }
         else
         {
            /* Using cursor H00OH11 */
            pr_default.execute(3, new Object[] {AV28Servico_Codigo, AV18NovaPrestadora});
            while ( (pr_default.getStatus(3) != 101) )
            {
               A92Contrato_Ativo = H00OH11_A92Contrato_Ativo[0];
               A155Servico_Codigo = H00OH11_A155Servico_Codigo[0];
               A39Contratada_Codigo = H00OH11_A39Contratada_Codigo[0];
               A77Contrato_Numero = H00OH11_A77Contrato_Numero[0];
               A74Contrato_Codigo = H00OH11_A74Contrato_Codigo[0];
               n74Contrato_Codigo = H00OH11_n74Contrato_Codigo[0];
               A160ContratoServicos_Codigo = H00OH11_A160ContratoServicos_Codigo[0];
               A1454ContratoServicos_PrazoTpDias = H00OH11_A1454ContratoServicos_PrazoTpDias[0];
               n1454ContratoServicos_PrazoTpDias = H00OH11_n1454ContratoServicos_PrazoTpDias[0];
               A1013Contrato_PrepostoCod = H00OH11_A1013Contrato_PrepostoCod[0];
               n1013Contrato_PrepostoCod = H00OH11_n1013Contrato_PrepostoCod[0];
               A1869Contrato_DataTermino = H00OH11_A1869Contrato_DataTermino[0];
               n1869Contrato_DataTermino = H00OH11_n1869Contrato_DataTermino[0];
               A92Contrato_Ativo = H00OH11_A92Contrato_Ativo[0];
               A39Contratada_Codigo = H00OH11_A39Contratada_Codigo[0];
               A77Contrato_Numero = H00OH11_A77Contrato_Numero[0];
               A1013Contrato_PrepostoCod = H00OH11_A1013Contrato_PrepostoCod[0];
               n1013Contrato_PrepostoCod = H00OH11_n1013Contrato_PrepostoCod[0];
               A1869Contrato_DataTermino = H00OH11_A1869Contrato_DataTermino[0];
               n1869Contrato_DataTermino = H00OH11_n1869Contrato_DataTermino[0];
               if ( A1869Contrato_DataTermino <= DateTimeUtil.ServerDate( context, "DEFAULT") )
               {
                  cmbavContrato.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(A74Contrato_Codigo), 6, 0)), A77Contrato_Numero, 0);
                  AV11ContratoServicos_Codigo = A160ContratoServicos_Codigo;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11ContratoServicos_Codigo), 6, 0)));
                  AV34TipoDias = A1454ContratoServicos_PrazoTpDias;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34TipoDias", AV34TipoDias);
                  AV36PrepostoNovaPrestadora = A1013Contrato_PrepostoCod;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36PrepostoNovaPrestadora", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36PrepostoNovaPrestadora), 6, 0)));
               }
               pr_default.readNext(3);
            }
            pr_default.close(3);
            if ( cmbavContrato.ItemCount > 1 )
            {
               cmbavContrato.addItem("0", "(Nenhum)�������", 1);
               AV11ContratoServicos_Codigo = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11ContratoServicos_Codigo), 6, 0)));
               AV34TipoDias = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34TipoDias", AV34TipoDias);
               AV36PrepostoNovaPrestadora = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36PrepostoNovaPrestadora", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36PrepostoNovaPrestadora), 6, 0)));
            }
         }
         cmbavContrato.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV10Contrato), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContrato_Internalname, "Values", cmbavContrato.ToJavascriptSource());
         cmbavTipodias.CurrentValue = StringUtil.RTrim( AV34TipoDias);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavTipodias_Internalname, "Values", cmbavTipodias.ToJavascriptSource());
      }

      protected void E13OH2( )
      {
         /* Contrato_Click Routine */
         if ( AV10Contrato > 0 )
         {
            /* Using cursor H00OH12 */
            pr_default.execute(4, new Object[] {AV10Contrato, AV28Servico_Codigo});
            while ( (pr_default.getStatus(4) != 101) )
            {
               A155Servico_Codigo = H00OH12_A155Servico_Codigo[0];
               A74Contrato_Codigo = H00OH12_A74Contrato_Codigo[0];
               n74Contrato_Codigo = H00OH12_n74Contrato_Codigo[0];
               A160ContratoServicos_Codigo = H00OH12_A160ContratoServicos_Codigo[0];
               A1454ContratoServicos_PrazoTpDias = H00OH12_A1454ContratoServicos_PrazoTpDias[0];
               n1454ContratoServicos_PrazoTpDias = H00OH12_n1454ContratoServicos_PrazoTpDias[0];
               A1013Contrato_PrepostoCod = H00OH12_A1013Contrato_PrepostoCod[0];
               n1013Contrato_PrepostoCod = H00OH12_n1013Contrato_PrepostoCod[0];
               A1013Contrato_PrepostoCod = H00OH12_A1013Contrato_PrepostoCod[0];
               n1013Contrato_PrepostoCod = H00OH12_n1013Contrato_PrepostoCod[0];
               AV11ContratoServicos_Codigo = A160ContratoServicos_Codigo;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11ContratoServicos_Codigo), 6, 0)));
               AV34TipoDias = A1454ContratoServicos_PrazoTpDias;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34TipoDias", AV34TipoDias);
               AV36PrepostoNovaPrestadora = A1013Contrato_PrepostoCod;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36PrepostoNovaPrestadora", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36PrepostoNovaPrestadora), 6, 0)));
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               pr_default.readNext(4);
            }
            pr_default.close(4);
         }
         cmbavTipodias.CurrentValue = StringUtil.RTrim( AV34TipoDias);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavTipodias_Internalname, "Values", cmbavTipodias.ToJavascriptSource());
      }

      public void GXEnter( )
      {
         /* Execute user event: E14OH2 */
         E14OH2 ();
         if (returnInSub) return;
      }

      protected void E14OH2( )
      {
         /* Enter Routine */
         if ( (0==AV18NovaPrestadora) )
         {
            GX_msglist.addItem("Selecione a nova Presatadora!");
            GX_FocusControl = cmbavNovaprestadora_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            context.DoAjaxSetFocus(GX_FocusControl);
         }
         else if ( (0==AV10Contrato) )
         {
            GX_msglist.addItem("Contrato � obrigat�rio!");
            GX_FocusControl = cmbavContrato_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            context.DoAjaxSetFocus(GX_FocusControl);
         }
         else if ( String.IsNullOrEmpty(StringUtil.RTrim( AV19Observacao)) )
         {
            GX_msglist.addItem("Observa��o � obrigat�rio!");
            GX_FocusControl = edtavObservacao_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            context.DoAjaxSetFocus(GX_FocusControl);
         }
         else
         {
            /* Execute user subroutine: 'ALTERAPRESTADORA' */
            S112 ();
            if (returnInSub) return;
         }
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV38Usuarios", AV38Usuarios);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV48Codigos", AV48Codigos);
      }

      protected void E15OH2( )
      {
         /* 'DoFechar' Routine */
         context.setWebReturnParms(new Object[] {});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void S112( )
      {
         /* 'ALTERAPRESTADORA' Routine */
         AV20OS.Load(AV8Codigo);
         GXt_int1 = AV27Dias;
         new prc_diasparaentrega(context ).execute( ref  AV11ContratoServicos_Codigo,  AV20OS.gxTpr_Contagemresultado_pfbfsimp,  0, out  GXt_int1) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11ContratoServicos_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11ContratoServicos_Codigo), 6, 0)));
         AV27Dias = GXt_int1;
         GXt_dtime2 = AV22PrazoEntrega;
         GXt_dtime3 = DateTimeUtil.ResetTime( DateTimeUtil.ServerDate( context, "DEFAULT") ) ;
         new prc_adddiasuteis(context ).execute(  GXt_dtime3,  AV27Dias,  AV34TipoDias, out  GXt_dtime2) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34TipoDias", AV34TipoDias);
         AV22PrazoEntrega = GXt_dtime2;
         AV22PrazoEntrega = DateTimeUtil.TAdd( AV22PrazoEntrega, 3600*(DateTimeUtil.Hour( AV20OS.gxTpr_Contagemresultado_horaentrega)));
         AV22PrazoEntrega = DateTimeUtil.TAdd( AV22PrazoEntrega, 60*(DateTimeUtil.Minute( AV20OS.gxTpr_Contagemresultado_horaentrega)));
         AV20OS.gxTpr_Contagemresultado_statusdmn = "S";
         AV20OS.gxTpr_Contagemresultado_cntsrvcod = AV11ContratoServicos_Codigo;
         AV20OS.gxTpr_Contagemresultado_contratadacod = AV18NovaPrestadora;
         AV20OS.gxTv_SdtContagemResultado_Contagemresultado_responsavel_SetNull();
         AV20OS.gxTpr_Contagemresultado_dataentrega = AV22PrazoEntrega;
         GXt_dtime3 = DateTimeUtil.ResetDate(AV22PrazoEntrega);
         AV20OS.gxTpr_Contagemresultado_horaentrega = GXt_dtime3;
         AV20OS.gxTpr_Contagemresultado_prazoinicialdias = AV27Dias;
         AV20OS.Save();
         if ( AV20OS.Success() )
         {
            if ( cmbavContrato.ItemCount > 1 )
            {
               AV19Observacao = "Prestadora " + StringUtil.Trim( AV25Prestadora) + " para " + cmbavNovaprestadora.Description + ", contrato " + cmbavContrato.Description + ". " + StringUtil.Trim( AV19Observacao);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19Observacao", AV19Observacao);
            }
            else
            {
               AV19Observacao = "Prestadora " + StringUtil.Trim( AV25Prestadora) + " para " + cmbavNovaprestadora.Description + ". " + AV19Observacao;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19Observacao", AV19Observacao);
            }
            new prc_inslogresponsavel(context ).execute( ref  AV8Codigo,  0,  "U",  "D",  AV26WWPContext.gxTpr_Userid,  0,  AV29StatusAnterior,  "S",  AV19Observacao,  AV22PrazoEntrega,  false) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8Codigo), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29StatusAnterior", AV29StatusAnterior);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19Observacao", AV19Observacao);
            context.CommitDataStores( "WP_AlterarPrestadora");
            if ( ! AV40ContratanteSemEmailSda )
            {
               /* Execute user subroutine: 'ENVIONOTIFICACAO' */
               S122 ();
               if (returnInSub) return;
            }
            /* Execute user subroutine: 'CLOSEREFRESH' */
            S132 ();
            if (returnInSub) return;
         }
         else
         {
            context.RollbackDataStores( "WP_AlterarPrestadora");
            AV30Messages = AV20OS.GetMessages();
            AV56GXV1 = 1;
            while ( AV56GXV1 <= AV30Messages.Count )
            {
               AV31oneMessage = ((SdtMessages_Message)AV30Messages.Item(AV56GXV1));
               GX_msglist.addItem(AV31oneMessage.gxTpr_Description);
               AV56GXV1 = (int)(AV56GXV1+1);
            }
         }
      }

      protected void S122( )
      {
         /* 'ENVIONOTIFICACAO' Routine */
         AV38Usuarios.Add(AV26WWPContext.gxTpr_Userid, 0);
         AV38Usuarios.Add(AV35Preposto, 0);
         AV38Usuarios.Add(AV36PrepostoNovaPrestadora, 0);
         if ( AV39Owner != AV26WWPContext.gxTpr_Userid )
         {
            AV38Usuarios.Add(AV39Owner, 0);
         }
         AV44Subject = "[" + AV26WWPContext.gxTpr_Parametrossistema_nomesistema + "] OS " + StringUtil.Trim( AV12Demanda) + ": Prestadora alterada" + " (No reply)";
         AV48Codigos.Add(AV8Codigo, 0);
         AV41WebSession.Set("DemandaCodigo", AV48Codigos.ToXml(false, true, "Collection", ""));
         new prc_enviaremail(context ).execute(  AV26WWPContext.gxTpr_Areatrabalho_codigo,  AV38Usuarios,  AV44Subject,  AV45EmailText,  AV46Attachments, ref  AV47Resultado) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47Resultado", AV47Resultado);
         AV41WebSession.Remove("DemandaCodigo");
      }

      protected void S132( )
      {
         /* 'CLOSEREFRESH' Routine */
         lblTbjava_Caption = "<script language=\"javascript\" type=\"text/javascript\">parent.location.replace(parent.location.href); </script>";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbjava_Internalname, "Caption", lblTbjava_Caption);
      }

      protected void nextLoad( )
      {
      }

      protected void E16OH2( )
      {
         /* Load Routine */
      }

      protected void wb_table1_2_OH2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_OH2( true) ;
         }
         else
         {
            wb_table2_8_OH2( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_OH2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\">") ;
            wb_table3_35_OH2( true) ;
         }
         else
         {
            wb_table3_35_OH2( false) ;
         }
         return  ;
      }

      protected void wb_table3_35_OH2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_OH2e( true) ;
         }
         else
         {
            wb_table1_2_OH2e( false) ;
         }
      }

      protected void wb_table3_35_OH2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable1_Internalname, tblUnnamedtable1_Internalname, "", "Table", 0, "", "", 1, 15, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 38,'',false,'',0)\"";
            ClassString = "BtnDefault";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnenter_Internalname, "", "Confirmar", bttBtnenter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtnenter_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_AlterarPrestadora.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 40,'',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnfechar_Internalname, "", "Fechar", bttBtnfechar_Jsonclick, 5, "Fechar", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"E\\'DOFECHAR\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_AlterarPrestadora.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_35_OH2e( true) ;
         }
         else
         {
            wb_table3_35_OH2e( false) ;
         }
      }

      protected void wb_table2_8_OH2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "Table", 0, "", "", 1, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbservicoatual_Internalname, "Da prestadora", "", "", lblTbservicoatual_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_AlterarPrestadora.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"4\"  class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 13,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavPrestadora_Internalname, StringUtil.RTrim( AV25Prestadora), StringUtil.RTrim( context.localUtil.Format( AV25Prestadora, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,13);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavPrestadora_Jsonclick, 0, "Attribute", "", "", "", 1, edtavPrestadora_Enabled, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Nome", "left", true, "HLP_WP_AlterarPrestadora.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbnovoservico_Internalname, "Para", "", "", lblTbnovoservico_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_AlterarPrestadora.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"2\"  class='RequiredDataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavNovaprestadora, cmbavNovaprestadora_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV18NovaPrestadora), 6, 0)), 1, cmbavNovaprestadora_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVNOVAPRESTADORA.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,18);\"", "", true, "HLP_WP_AlterarPrestadora.htm");
            cmbavNovaprestadora.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV18NovaPrestadora), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavNovaprestadora_Internalname, "Values", (String)(cmbavNovaprestadora.ToJavascriptSource()));
            context.WriteHtmlText( "&nbsp;&nbsp; ") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock1_Internalname, "(Os prazos ser�o atualizados)", "", "", lblTextblock1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WP_AlterarPrestadora.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbnovoservico2_Internalname, "Contrato", "", "", lblTbnovoservico2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_AlterarPrestadora.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavContrato, cmbavContrato_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV10Contrato), 6, 0)), 1, cmbavContrato_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVCONTRATO.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,23);\"", "", true, "HLP_WP_AlterarPrestadora.htm");
            cmbavContrato.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV10Contrato), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavContrato_Internalname, "Values", (String)(cmbavContrato.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbobservacao_Internalname, "Observa��o", "", "", lblTbobservacao_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_WP_AlterarPrestadora.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"4\"  class='RequiredDataContentCell'>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 28,'',false,'',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavObservacao_Internalname, AV19Observacao, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,28);\"", 0, 1, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "2097152", 1, "", "", -1, true, "", "HLP_WP_AlterarPrestadora.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"3\"  class='DataContentCell'>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\">") ;
            context.WriteHtmlText( "<td colspan=\"4\" >") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_OH2e( true) ;
         }
         else
         {
            wb_table2_8_OH2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV8Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8Codigo), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAOH2( ) ;
         WSOH2( ) ;
         WEOH2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20206216263617");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wp_alterarprestadora.js", "?20206216263617");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTbservicoatual_Internalname = "TBSERVICOATUAL";
         edtavPrestadora_Internalname = "vPRESTADORA";
         lblTbnovoservico_Internalname = "TBNOVOSERVICO";
         cmbavNovaprestadora_Internalname = "vNOVAPRESTADORA";
         lblTextblock1_Internalname = "TEXTBLOCK1";
         lblTbnovoservico2_Internalname = "TBNOVOSERVICO2";
         cmbavContrato_Internalname = "vCONTRATO";
         lblTbobservacao_Internalname = "TBOBSERVACAO";
         edtavObservacao_Internalname = "vOBSERVACAO";
         tblTablecontent_Internalname = "TABLECONTENT";
         bttBtnenter_Internalname = "BTNENTER";
         bttBtnfechar_Internalname = "BTNFECHAR";
         tblUnnamedtable1_Internalname = "UNNAMEDTABLE1";
         tblTablemain_Internalname = "TABLEMAIN";
         lblTbjava_Internalname = "TBJAVA";
         edtavContratoservicos_codigo_Internalname = "vCONTRATOSERVICOS_CODIGO";
         cmbavTipodias_Internalname = "vTIPODIAS";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         cmbavContrato_Jsonclick = "";
         cmbavNovaprestadora_Jsonclick = "";
         edtavPrestadora_Jsonclick = "";
         edtavPrestadora_Enabled = 1;
         bttBtnenter_Visible = 1;
         cmbavTipodias_Jsonclick = "";
         cmbavTipodias.Visible = 1;
         edtavContratoservicos_codigo_Jsonclick = "";
         edtavContratoservicos_codigo_Visible = 1;
         lblTbjava_Caption = "Script";
         lblTbjava_Visible = 1;
         cmbavContrato.Description = "";
         cmbavNovaprestadora.Description = "";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Alterar Prestadora";
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("VNOVAPRESTADORA.CLICK","{handler:'E12OH2',iparms:[{av:'AV18NovaPrestadora',fld:'vNOVAPRESTADORA',pic:'ZZZZZ9',nv:0},{av:'A39Contratada_Codigo',fld:'CONTRATADA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A155Servico_Codigo',fld:'SERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV28Servico_Codigo',fld:'vSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1869Contrato_DataTermino',fld:'CONTRATO_DATATERMINO',pic:'',nv:''},{av:'A92Contrato_Ativo',fld:'CONTRATO_ATIVO',pic:'',nv:false},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A77Contrato_Numero',fld:'CONTRATO_NUMERO',pic:'',nv:''},{av:'A160ContratoServicos_Codigo',fld:'CONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1454ContratoServicos_PrazoTpDias',fld:'CONTRATOSERVICOS_PRAZOTPDIAS',pic:'',nv:''},{av:'A1013Contrato_PrepostoCod',fld:'CONTRATO_PREPOSTOCOD',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV10Contrato',fld:'vCONTRATO',pic:'ZZZZZ9',nv:0},{av:'AV11ContratoServicos_Codigo',fld:'vCONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV34TipoDias',fld:'vTIPODIAS',pic:'',nv:''},{av:'AV36PrepostoNovaPrestadora',fld:'vPREPOSTONOVAPRESTADORA',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("VCONTRATO.CLICK","{handler:'E13OH2',iparms:[{av:'AV10Contrato',fld:'vCONTRATO',pic:'ZZZZZ9',nv:0},{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A155Servico_Codigo',fld:'SERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV28Servico_Codigo',fld:'vSERVICO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A160ContratoServicos_Codigo',fld:'CONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A1454ContratoServicos_PrazoTpDias',fld:'CONTRATOSERVICOS_PRAZOTPDIAS',pic:'',nv:''},{av:'A1013Contrato_PrepostoCod',fld:'CONTRATO_PREPOSTOCOD',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV11ContratoServicos_Codigo',fld:'vCONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV34TipoDias',fld:'vTIPODIAS',pic:'',nv:''},{av:'AV36PrepostoNovaPrestadora',fld:'vPREPOSTONOVAPRESTADORA',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("ENTER","{handler:'E14OH2',iparms:[{av:'AV18NovaPrestadora',fld:'vNOVAPRESTADORA',pic:'ZZZZZ9',nv:0},{av:'AV10Contrato',fld:'vCONTRATO',pic:'ZZZZZ9',nv:0},{av:'AV19Observacao',fld:'vOBSERVACAO',pic:'',nv:''},{av:'AV8Codigo',fld:'vCODIGO',pic:'ZZZZZ9',nv:0},{av:'AV11ContratoServicos_Codigo',fld:'vCONTRATOSERVICOS_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV34TipoDias',fld:'vTIPODIAS',pic:'',nv:''},{av:'AV25Prestadora',fld:'vPRESTADORA',pic:'@!',hsh:true,nv:''},{av:'cmbavNovaprestadora'},{av:'cmbavContrato'},{av:'AV26WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV29StatusAnterior',fld:'vSTATUSANTERIOR',pic:'',nv:''},{av:'AV40ContratanteSemEmailSda',fld:'vCONTRATANTESEMEMAILSDA',pic:'',nv:false},{av:'AV38Usuarios',fld:'vUSUARIOS',pic:'',nv:null},{av:'AV35Preposto',fld:'vPREPOSTO',pic:'ZZZ9',nv:0},{av:'AV36PrepostoNovaPrestadora',fld:'vPREPOSTONOVAPRESTADORA',pic:'ZZZZZ9',nv:0},{av:'AV39Owner',fld:'vOWNER',pic:'ZZZZZ9',nv:0},{av:'AV12Demanda',fld:'vDEMANDA',pic:'@!',nv:''},{av:'AV48Codigos',fld:'vCODIGOS',pic:'',nv:null},{av:'AV45EmailText',fld:'vEMAILTEXT',pic:'',nv:''},{av:'AV46Attachments',fld:'vATTACHMENTS',pic:'',nv:null},{av:'AV47Resultado',fld:'vRESULTADO',pic:'',nv:''}],oparms:[{av:'AV19Observacao',fld:'vOBSERVACAO',pic:'',nv:''},{av:'AV8Codigo',fld:'vCODIGO',pic:'ZZZZZ9',nv:0},{av:'AV38Usuarios',fld:'vUSUARIOS',pic:'',nv:null},{av:'AV48Codigos',fld:'vCODIGOS',pic:'',nv:null},{av:'AV47Resultado',fld:'vRESULTADO',pic:'',nv:''},{av:'lblTbjava_Caption',ctrl:'TBJAVA',prop:'Caption'}]}");
         setEventMetadata("'DOFECHAR'","{handler:'E15OH2',iparms:[],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV26WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         A77Contrato_Numero = "";
         A1454ContratoServicos_PrazoTpDias = "";
         AV29StatusAnterior = "";
         AV38Usuarios = new GxSimpleCollection();
         AV12Demanda = "";
         AV48Codigos = new GxSimpleCollection();
         AV45EmailText = "";
         AV46Attachments = new GxSimpleCollection();
         AV47Resultado = "";
         A1869Contrato_DataTermino = DateTime.MinValue;
         AV25Prestadora = "";
         GXKey = "";
         forbiddenHiddens = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         lblTbjava_Jsonclick = "";
         TempTags = "";
         AV34TipoDias = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV19Observacao = "";
         hsh = "";
         scmdbuf = "";
         H00OH2_A146Modulo_Codigo = new int[1] ;
         H00OH2_n146Modulo_Codigo = new bool[] {false} ;
         H00OH2_A127Sistema_Codigo = new int[1] ;
         H00OH2_A135Sistema_AreaTrabalhoCod = new int[1] ;
         H00OH2_A499ContagemResultado_ContratadaPessoaCod = new int[1] ;
         H00OH2_n499ContagemResultado_ContratadaPessoaCod = new bool[] {false} ;
         H00OH2_A1553ContagemResultado_CntSrvCod = new int[1] ;
         H00OH2_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         H00OH2_A1603ContagemResultado_CntCod = new int[1] ;
         H00OH2_n1603ContagemResultado_CntCod = new bool[] {false} ;
         H00OH2_A29Contratante_Codigo = new int[1] ;
         H00OH2_n29Contratante_Codigo = new bool[] {false} ;
         H00OH2_A456ContagemResultado_Codigo = new int[1] ;
         H00OH2_A490ContagemResultado_ContratadaCod = new int[1] ;
         H00OH2_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         H00OH2_A601ContagemResultado_Servico = new int[1] ;
         H00OH2_n601ContagemResultado_Servico = new bool[] {false} ;
         H00OH2_A484ContagemResultado_StatusDmn = new String[] {""} ;
         H00OH2_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         H00OH2_A500ContagemResultado_ContratadaPessoaNom = new String[] {""} ;
         H00OH2_n500ContagemResultado_ContratadaPessoaNom = new bool[] {false} ;
         H00OH2_A493ContagemResultado_DemandaFM = new String[] {""} ;
         H00OH2_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         H00OH2_A1604ContagemResultado_CntPrpCod = new int[1] ;
         H00OH2_n1604ContagemResultado_CntPrpCod = new bool[] {false} ;
         H00OH2_A508ContagemResultado_Owner = new int[1] ;
         H00OH2_A548Contratante_EmailSdaUser = new String[] {""} ;
         H00OH2_n548Contratante_EmailSdaUser = new bool[] {false} ;
         H00OH2_A552Contratante_EmailSdaPort = new short[1] ;
         H00OH2_n552Contratante_EmailSdaPort = new bool[] {false} ;
         H00OH2_A547Contratante_EmailSdaHost = new String[] {""} ;
         H00OH2_n547Contratante_EmailSdaHost = new bool[] {false} ;
         A484ContagemResultado_StatusDmn = "";
         A500ContagemResultado_ContratadaPessoaNom = "";
         A493ContagemResultado_DemandaFM = "";
         A548Contratante_EmailSdaUser = "";
         A547Contratante_EmailSdaHost = "";
         H00OH6_A40Contratada_PessoaCod = new int[1] ;
         H00OH6_A75Contrato_AreaTrabalhoCod = new int[1] ;
         H00OH6_A155Servico_Codigo = new int[1] ;
         H00OH6_A74Contrato_Codigo = new int[1] ;
         H00OH6_n74Contrato_Codigo = new bool[] {false} ;
         H00OH6_A41Contratada_PessoaNom = new String[] {""} ;
         H00OH6_n41Contratada_PessoaNom = new bool[] {false} ;
         H00OH6_A39Contratada_Codigo = new int[1] ;
         H00OH6_A92Contrato_Ativo = new bool[] {false} ;
         H00OH6_A1869Contrato_DataTermino = new DateTime[] {DateTime.MinValue} ;
         H00OH6_n1869Contrato_DataTermino = new bool[] {false} ;
         A41Contratada_PessoaNom = "";
         H00OH7_A1078ContratoGestor_ContratoCod = new int[1] ;
         H00OH7_A1079ContratoGestor_UsuarioCod = new int[1] ;
         H00OH11_A92Contrato_Ativo = new bool[] {false} ;
         H00OH11_A155Servico_Codigo = new int[1] ;
         H00OH11_A39Contratada_Codigo = new int[1] ;
         H00OH11_A77Contrato_Numero = new String[] {""} ;
         H00OH11_A74Contrato_Codigo = new int[1] ;
         H00OH11_n74Contrato_Codigo = new bool[] {false} ;
         H00OH11_A160ContratoServicos_Codigo = new int[1] ;
         H00OH11_A1454ContratoServicos_PrazoTpDias = new String[] {""} ;
         H00OH11_n1454ContratoServicos_PrazoTpDias = new bool[] {false} ;
         H00OH11_A1013Contrato_PrepostoCod = new int[1] ;
         H00OH11_n1013Contrato_PrepostoCod = new bool[] {false} ;
         H00OH11_A1869Contrato_DataTermino = new DateTime[] {DateTime.MinValue} ;
         H00OH11_n1869Contrato_DataTermino = new bool[] {false} ;
         H00OH12_A155Servico_Codigo = new int[1] ;
         H00OH12_A74Contrato_Codigo = new int[1] ;
         H00OH12_n74Contrato_Codigo = new bool[] {false} ;
         H00OH12_A160ContratoServicos_Codigo = new int[1] ;
         H00OH12_A1454ContratoServicos_PrazoTpDias = new String[] {""} ;
         H00OH12_n1454ContratoServicos_PrazoTpDias = new bool[] {false} ;
         H00OH12_A1013Contrato_PrepostoCod = new int[1] ;
         H00OH12_n1013Contrato_PrepostoCod = new bool[] {false} ;
         AV20OS = new SdtContagemResultado(context);
         AV22PrazoEntrega = (DateTime)(DateTime.MinValue);
         GXt_dtime2 = (DateTime)(DateTime.MinValue);
         GXt_dtime3 = (DateTime)(DateTime.MinValue);
         AV30Messages = new GxObjectCollection( context, "Messages.Message", "Genexus", "SdtMessages_Message", "GeneXus.Programs");
         AV31oneMessage = new SdtMessages_Message(context);
         AV44Subject = "";
         AV41WebSession = context.GetSession();
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         bttBtnenter_Jsonclick = "";
         bttBtnfechar_Jsonclick = "";
         lblTbservicoatual_Jsonclick = "";
         lblTbnovoservico_Jsonclick = "";
         lblTextblock1_Jsonclick = "";
         lblTbnovoservico2_Jsonclick = "";
         lblTbobservacao_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wp_alterarprestadora__default(),
            new Object[][] {
                new Object[] {
               H00OH2_A146Modulo_Codigo, H00OH2_n146Modulo_Codigo, H00OH2_A127Sistema_Codigo, H00OH2_A135Sistema_AreaTrabalhoCod, H00OH2_A499ContagemResultado_ContratadaPessoaCod, H00OH2_n499ContagemResultado_ContratadaPessoaCod, H00OH2_A1553ContagemResultado_CntSrvCod, H00OH2_n1553ContagemResultado_CntSrvCod, H00OH2_A1603ContagemResultado_CntCod, H00OH2_n1603ContagemResultado_CntCod,
               H00OH2_A29Contratante_Codigo, H00OH2_n29Contratante_Codigo, H00OH2_A456ContagemResultado_Codigo, H00OH2_A490ContagemResultado_ContratadaCod, H00OH2_n490ContagemResultado_ContratadaCod, H00OH2_A601ContagemResultado_Servico, H00OH2_n601ContagemResultado_Servico, H00OH2_A484ContagemResultado_StatusDmn, H00OH2_n484ContagemResultado_StatusDmn, H00OH2_A500ContagemResultado_ContratadaPessoaNom,
               H00OH2_n500ContagemResultado_ContratadaPessoaNom, H00OH2_A493ContagemResultado_DemandaFM, H00OH2_n493ContagemResultado_DemandaFM, H00OH2_A1604ContagemResultado_CntPrpCod, H00OH2_n1604ContagemResultado_CntPrpCod, H00OH2_A508ContagemResultado_Owner, H00OH2_A548Contratante_EmailSdaUser, H00OH2_n548Contratante_EmailSdaUser, H00OH2_A552Contratante_EmailSdaPort, H00OH2_n552Contratante_EmailSdaPort,
               H00OH2_A547Contratante_EmailSdaHost, H00OH2_n547Contratante_EmailSdaHost
               }
               , new Object[] {
               H00OH6_A40Contratada_PessoaCod, H00OH6_A75Contrato_AreaTrabalhoCod, H00OH6_A155Servico_Codigo, H00OH6_A74Contrato_Codigo, H00OH6_A41Contratada_PessoaNom, H00OH6_n41Contratada_PessoaNom, H00OH6_A39Contratada_Codigo, H00OH6_A92Contrato_Ativo, H00OH6_A1869Contrato_DataTermino, H00OH6_n1869Contrato_DataTermino
               }
               , new Object[] {
               H00OH7_A1078ContratoGestor_ContratoCod, H00OH7_A1079ContratoGestor_UsuarioCod
               }
               , new Object[] {
               H00OH11_A92Contrato_Ativo, H00OH11_A155Servico_Codigo, H00OH11_A39Contratada_Codigo, H00OH11_A77Contrato_Numero, H00OH11_A74Contrato_Codigo, H00OH11_A160ContratoServicos_Codigo, H00OH11_A1454ContratoServicos_PrazoTpDias, H00OH11_n1454ContratoServicos_PrazoTpDias, H00OH11_A1013Contrato_PrepostoCod, H00OH11_n1013Contrato_PrepostoCod,
               H00OH11_A1869Contrato_DataTermino, H00OH11_n1869Contrato_DataTermino
               }
               , new Object[] {
               H00OH12_A155Servico_Codigo, H00OH12_A74Contrato_Codigo, H00OH12_A160ContratoServicos_Codigo, H00OH12_A1454ContratoServicos_PrazoTpDias, H00OH12_n1454ContratoServicos_PrazoTpDias, H00OH12_A1013Contrato_PrepostoCod, H00OH12_n1013Contrato_PrepostoCod
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
         edtavPrestadora_Enabled = 0;
      }

      private short nRcdExists_7 ;
      private short nIsMod_7 ;
      private short nRcdExists_6 ;
      private short nIsMod_6 ;
      private short nGotPars ;
      private short GxWebError ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short AV35Preposto ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short A552Contratante_EmailSdaPort ;
      private short AV27Dias ;
      private short GXt_int1 ;
      private short nGXWrapped ;
      private int AV8Codigo ;
      private int wcpOAV8Codigo ;
      private int A39Contratada_Codigo ;
      private int A155Servico_Codigo ;
      private int AV28Servico_Codigo ;
      private int A74Contrato_Codigo ;
      private int A160ContratoServicos_Codigo ;
      private int A1013Contrato_PrepostoCod ;
      private int AV36PrepostoNovaPrestadora ;
      private int AV39Owner ;
      private int lblTbjava_Visible ;
      private int AV11ContratoServicos_Codigo ;
      private int edtavContratoservicos_codigo_Visible ;
      private int AV18NovaPrestadora ;
      private int AV10Contrato ;
      private int edtavPrestadora_Enabled ;
      private int bttBtnenter_Visible ;
      private int A146Modulo_Codigo ;
      private int A127Sistema_Codigo ;
      private int A135Sistema_AreaTrabalhoCod ;
      private int A499ContagemResultado_ContratadaPessoaCod ;
      private int A1553ContagemResultado_CntSrvCod ;
      private int A1603ContagemResultado_CntCod ;
      private int A29Contratante_Codigo ;
      private int A456ContagemResultado_Codigo ;
      private int A490ContagemResultado_ContratadaCod ;
      private int A601ContagemResultado_Servico ;
      private int A1604ContagemResultado_CntPrpCod ;
      private int A508ContagemResultado_Owner ;
      private int AV9Contratada_Codigo ;
      private int A40Contratada_PessoaCod ;
      private int A75Contrato_AreaTrabalhoCod ;
      private int A1078ContratoGestor_ContratoCod ;
      private int A1079ContratoGestor_UsuarioCod ;
      private int AV56GXV1 ;
      private int idxLst ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String A77Contrato_Numero ;
      private String A1454ContratoServicos_PrazoTpDias ;
      private String AV29StatusAnterior ;
      private String AV45EmailText ;
      private String AV47Resultado ;
      private String AV25Prestadora ;
      private String GXKey ;
      private String forbiddenHiddens ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String lblTbjava_Internalname ;
      private String lblTbjava_Caption ;
      private String lblTbjava_Jsonclick ;
      private String TempTags ;
      private String edtavContratoservicos_codigo_Internalname ;
      private String edtavContratoservicos_codigo_Jsonclick ;
      private String cmbavTipodias_Internalname ;
      private String AV34TipoDias ;
      private String cmbavTipodias_Jsonclick ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavPrestadora_Internalname ;
      private String cmbavNovaprestadora_Internalname ;
      private String cmbavContrato_Internalname ;
      private String edtavObservacao_Internalname ;
      private String hsh ;
      private String bttBtnenter_Internalname ;
      private String scmdbuf ;
      private String A484ContagemResultado_StatusDmn ;
      private String A500ContagemResultado_ContratadaPessoaNom ;
      private String A41Contratada_PessoaNom ;
      private String AV44Subject ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String tblUnnamedtable1_Internalname ;
      private String bttBtnenter_Jsonclick ;
      private String bttBtnfechar_Internalname ;
      private String bttBtnfechar_Jsonclick ;
      private String tblTablecontent_Internalname ;
      private String lblTbservicoatual_Internalname ;
      private String lblTbservicoatual_Jsonclick ;
      private String edtavPrestadora_Jsonclick ;
      private String lblTbnovoservico_Internalname ;
      private String lblTbnovoservico_Jsonclick ;
      private String cmbavNovaprestadora_Jsonclick ;
      private String lblTextblock1_Internalname ;
      private String lblTextblock1_Jsonclick ;
      private String lblTbnovoservico2_Internalname ;
      private String lblTbnovoservico2_Jsonclick ;
      private String cmbavContrato_Jsonclick ;
      private String lblTbobservacao_Internalname ;
      private String lblTbobservacao_Jsonclick ;
      private DateTime AV22PrazoEntrega ;
      private DateTime GXt_dtime2 ;
      private DateTime GXt_dtime3 ;
      private DateTime A1869Contrato_DataTermino ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool A92Contrato_Ativo ;
      private bool AV40ContratanteSemEmailSda ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool returnInSub ;
      private bool n146Modulo_Codigo ;
      private bool n499ContagemResultado_ContratadaPessoaCod ;
      private bool n1553ContagemResultado_CntSrvCod ;
      private bool n1603ContagemResultado_CntCod ;
      private bool n29Contratante_Codigo ;
      private bool n490ContagemResultado_ContratadaCod ;
      private bool n601ContagemResultado_Servico ;
      private bool n484ContagemResultado_StatusDmn ;
      private bool n500ContagemResultado_ContratadaPessoaNom ;
      private bool n493ContagemResultado_DemandaFM ;
      private bool n1604ContagemResultado_CntPrpCod ;
      private bool n548Contratante_EmailSdaUser ;
      private bool n552Contratante_EmailSdaPort ;
      private bool n547Contratante_EmailSdaHost ;
      private bool n74Contrato_Codigo ;
      private bool n41Contratada_PessoaNom ;
      private bool n1869Contrato_DataTermino ;
      private bool n1454ContratoServicos_PrazoTpDias ;
      private bool n1013Contrato_PrepostoCod ;
      private String AV19Observacao ;
      private String AV12Demanda ;
      private String A493ContagemResultado_DemandaFM ;
      private String A548Contratante_EmailSdaUser ;
      private String A547Contratante_EmailSdaHost ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbavNovaprestadora ;
      private GXCombobox cmbavContrato ;
      private GXCombobox cmbavTipodias ;
      private IDataStoreProvider pr_default ;
      private int[] H00OH2_A146Modulo_Codigo ;
      private bool[] H00OH2_n146Modulo_Codigo ;
      private int[] H00OH2_A127Sistema_Codigo ;
      private int[] H00OH2_A135Sistema_AreaTrabalhoCod ;
      private int[] H00OH2_A499ContagemResultado_ContratadaPessoaCod ;
      private bool[] H00OH2_n499ContagemResultado_ContratadaPessoaCod ;
      private int[] H00OH2_A1553ContagemResultado_CntSrvCod ;
      private bool[] H00OH2_n1553ContagemResultado_CntSrvCod ;
      private int[] H00OH2_A1603ContagemResultado_CntCod ;
      private bool[] H00OH2_n1603ContagemResultado_CntCod ;
      private int[] H00OH2_A29Contratante_Codigo ;
      private bool[] H00OH2_n29Contratante_Codigo ;
      private int[] H00OH2_A456ContagemResultado_Codigo ;
      private int[] H00OH2_A490ContagemResultado_ContratadaCod ;
      private bool[] H00OH2_n490ContagemResultado_ContratadaCod ;
      private int[] H00OH2_A601ContagemResultado_Servico ;
      private bool[] H00OH2_n601ContagemResultado_Servico ;
      private String[] H00OH2_A484ContagemResultado_StatusDmn ;
      private bool[] H00OH2_n484ContagemResultado_StatusDmn ;
      private String[] H00OH2_A500ContagemResultado_ContratadaPessoaNom ;
      private bool[] H00OH2_n500ContagemResultado_ContratadaPessoaNom ;
      private String[] H00OH2_A493ContagemResultado_DemandaFM ;
      private bool[] H00OH2_n493ContagemResultado_DemandaFM ;
      private int[] H00OH2_A1604ContagemResultado_CntPrpCod ;
      private bool[] H00OH2_n1604ContagemResultado_CntPrpCod ;
      private int[] H00OH2_A508ContagemResultado_Owner ;
      private String[] H00OH2_A548Contratante_EmailSdaUser ;
      private bool[] H00OH2_n548Contratante_EmailSdaUser ;
      private short[] H00OH2_A552Contratante_EmailSdaPort ;
      private bool[] H00OH2_n552Contratante_EmailSdaPort ;
      private String[] H00OH2_A547Contratante_EmailSdaHost ;
      private bool[] H00OH2_n547Contratante_EmailSdaHost ;
      private int[] H00OH6_A40Contratada_PessoaCod ;
      private int[] H00OH6_A75Contrato_AreaTrabalhoCod ;
      private int[] H00OH6_A155Servico_Codigo ;
      private int[] H00OH6_A74Contrato_Codigo ;
      private bool[] H00OH6_n74Contrato_Codigo ;
      private String[] H00OH6_A41Contratada_PessoaNom ;
      private bool[] H00OH6_n41Contratada_PessoaNom ;
      private int[] H00OH6_A39Contratada_Codigo ;
      private bool[] H00OH6_A92Contrato_Ativo ;
      private DateTime[] H00OH6_A1869Contrato_DataTermino ;
      private bool[] H00OH6_n1869Contrato_DataTermino ;
      private int[] H00OH7_A1078ContratoGestor_ContratoCod ;
      private int[] H00OH7_A1079ContratoGestor_UsuarioCod ;
      private bool[] H00OH11_A92Contrato_Ativo ;
      private int[] H00OH11_A155Servico_Codigo ;
      private int[] H00OH11_A39Contratada_Codigo ;
      private String[] H00OH11_A77Contrato_Numero ;
      private int[] H00OH11_A74Contrato_Codigo ;
      private bool[] H00OH11_n74Contrato_Codigo ;
      private int[] H00OH11_A160ContratoServicos_Codigo ;
      private String[] H00OH11_A1454ContratoServicos_PrazoTpDias ;
      private bool[] H00OH11_n1454ContratoServicos_PrazoTpDias ;
      private int[] H00OH11_A1013Contrato_PrepostoCod ;
      private bool[] H00OH11_n1013Contrato_PrepostoCod ;
      private DateTime[] H00OH11_A1869Contrato_DataTermino ;
      private bool[] H00OH11_n1869Contrato_DataTermino ;
      private int[] H00OH12_A155Servico_Codigo ;
      private int[] H00OH12_A74Contrato_Codigo ;
      private bool[] H00OH12_n74Contrato_Codigo ;
      private int[] H00OH12_A160ContratoServicos_Codigo ;
      private String[] H00OH12_A1454ContratoServicos_PrazoTpDias ;
      private bool[] H00OH12_n1454ContratoServicos_PrazoTpDias ;
      private int[] H00OH12_A1013Contrato_PrepostoCod ;
      private bool[] H00OH12_n1013Contrato_PrepostoCod ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private IGxSession AV41WebSession ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV38Usuarios ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV48Codigos ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV46Attachments ;
      [ObjectCollection(ItemType=typeof( SdtMessages_Message ))]
      private IGxCollection AV30Messages ;
      private GXWebForm Form ;
      private SdtMessages_Message AV31oneMessage ;
      private SdtContagemResultado AV20OS ;
      private wwpbaseobjects.SdtWWPContext AV26WWPContext ;
   }

   public class wp_alterarprestadora__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00OH2 ;
          prmH00OH2 = new Object[] {
          new Object[] {"@AV8Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00OH6 ;
          prmH00OH6 = new Object[] {
          new Object[] {"@AV9Contratada_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV26WWPC_1Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV28Servico_Codigo",SqlDbType.Int,6,0}
          } ;
          String cmdBufferH00OH6 ;
          cmdBufferH00OH6=" SELECT DISTINCT NULL AS [Contratada_PessoaCod], NULL AS [Contrato_AreaTrabalhoCod], NULL AS [Servico_Codigo], [Contrato_Codigo], [Contratada_PessoaNom], [Contratada_Codigo], NULL AS [Contrato_Ativo], NULL AS [Contrato_DataTermino] FROM ( SELECT TOP(100) PERCENT T3.[Contratada_PessoaCod] AS Contratada_PessoaCod, T2.[Contrato_AreaTrabalhoCod], T1.[Servico_Codigo], T1.[Contrato_Codigo], T4.[Pessoa_Nome] AS Contratada_PessoaNom, T2.[Contratada_Codigo], T2.[Contrato_Ativo], COALESCE( T5.[Contrato_DataTermino], convert( DATETIME, '17530101', 112 )) AS Contrato_DataTermino FROM (((([ContratoServicos] T1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = T1.[Contrato_Codigo]) INNER JOIN [Contratada] T3 WITH (NOLOCK) ON T3.[Contratada_Codigo] = T2.[Contratada_Codigo]) INNER JOIN [Pessoa] T4 WITH (NOLOCK) ON T4.[Pessoa_Codigo] = T3.[Contratada_PessoaCod]) LEFT JOIN (SELECT CASE  WHEN T6.[Contrato_DataVigenciaTermino] > COALESCE( T7.[ContratoTermoAditivo_DataFim], convert( DATETIME, '17530101', 112 )) THEN T6.[Contrato_DataVigenciaTermino] ELSE COALESCE( T7.[ContratoTermoAditivo_DataFim], convert( DATETIME, '17530101', 112 )) END AS Contrato_DataTermino, T6.[Contrato_Codigo] FROM ([Contrato] T6 WITH (NOLOCK) LEFT JOIN (SELECT T8.[ContratoTermoAditivo_DataFim], T8.[Contrato_Codigo], T8.[ContratoTermoAditivo_Codigo], T9.[GXC5] AS GXC5 FROM ([ContratoTermoAditivo] T8 WITH (NOLOCK) INNER JOIN (SELECT MAX([ContratoTermoAditivo_Codigo]) AS GXC5, [Contrato_Codigo] FROM [ContratoTermoAditivo] WITH (NOLOCK) GROUP BY [Contrato_Codigo] ) T9 ON T9.[Contrato_Codigo] = T8.[Contrato_Codigo]) WHERE T8.[ContratoTermoAditivo_Codigo] = T9.[GXC5] ) T7 ON T7.[Contrato_Codigo] = T6.[Contrato_Codigo]) ) T5 ON T5.[Contrato_Codigo] = T1.[Contrato_Codigo]) WHERE (T2.[Contratada_Codigo] "
          + " <> @AV9Contratada_Codigo) AND (T2.[Contrato_Ativo] = 1) AND (T2.[Contrato_AreaTrabalhoCod] = @AV26WWPC_1Areatrabalho_codigo) AND (T1.[Servico_Codigo] = @AV28Servico_Codigo) ORDER BY T4.[Pessoa_Nome]) DistinctT ORDER BY [Contratada_PessoaNom]" ;
          Object[] prmH00OH7 ;
          prmH00OH7 = new Object[] {
          new Object[] {"@Contrato_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV26WWPContext__Userid",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmH00OH11 ;
          prmH00OH11 = new Object[] {
          new Object[] {"@AV28Servico_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV18NovaPrestadora",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00OH12 ;
          prmH00OH12 = new Object[] {
          new Object[] {"@AV10Contrato",SqlDbType.Int,6,0} ,
          new Object[] {"@AV28Servico_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00OH2", "SELECT TOP 1 T1.[Modulo_Codigo], T2.[Sistema_Codigo], T3.[Sistema_AreaTrabalhoCod] AS Sistema_AreaTrabalhoCod, T8.[Contratada_PessoaCod] AS ContagemResultado_ContratadaPessoaCod, T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T6.[Contrato_Codigo] AS ContagemResultado_CntCod, T4.[Contratante_Codigo], T1.[ContagemResultado_Codigo], T1.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, T6.[Servico_Codigo] AS ContagemResultado_Servico, T1.[ContagemResultado_StatusDmn], T9.[Pessoa_Nome] AS ContagemResultado_ContratadaPessoaNom, T1.[ContagemResultado_DemandaFM], T7.[Contrato_PrepostoCod] AS ContagemResultado_CntPrpCod, T1.[ContagemResultado_Owner], T5.[Contratante_EmailSdaUser], T5.[Contratante_EmailSdaPort], T5.[Contratante_EmailSdaHost] FROM (((((((([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [Modulo] T2 WITH (NOLOCK) ON T2.[Modulo_Codigo] = T1.[Modulo_Codigo]) LEFT JOIN [Sistema] T3 WITH (NOLOCK) ON T3.[Sistema_Codigo] = T2.[Sistema_Codigo]) LEFT JOIN [AreaTrabalho] T4 WITH (NOLOCK) ON T4.[AreaTrabalho_Codigo] = T3.[Sistema_AreaTrabalhoCod]) LEFT JOIN [Contratante] T5 WITH (NOLOCK) ON T5.[Contratante_Codigo] = T4.[Contratante_Codigo]) LEFT JOIN [ContratoServicos] T6 WITH (NOLOCK) ON T6.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod]) LEFT JOIN [Contrato] T7 WITH (NOLOCK) ON T7.[Contrato_Codigo] = T6.[Contrato_Codigo]) LEFT JOIN [Contratada] T8 WITH (NOLOCK) ON T8.[Contratada_Codigo] = T1.[ContagemResultado_ContratadaCod]) LEFT JOIN [Pessoa] T9 WITH (NOLOCK) ON T9.[Pessoa_Codigo] = T8.[Contratada_PessoaCod]) WHERE T1.[ContagemResultado_Codigo] = @AV8Codigo ORDER BY T1.[ContagemResultado_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00OH2,1,0,false,true )
             ,new CursorDef("H00OH6", cmdBufferH00OH6,false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00OH6,100,0,true,false )
             ,new CursorDef("H00OH7", "SELECT TOP 1 [ContratoGestor_ContratoCod], [ContratoGestor_UsuarioCod] FROM [ContratoGestor] WITH (NOLOCK) WHERE [ContratoGestor_ContratoCod] = @Contrato_Codigo and [ContratoGestor_UsuarioCod] = @AV26WWPContext__Userid ORDER BY [ContratoGestor_ContratoCod], [ContratoGestor_UsuarioCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00OH7,1,0,false,true )
             ,new CursorDef("H00OH11", "SELECT T2.[Contrato_Ativo], T1.[Servico_Codigo], T2.[Contratada_Codigo], T2.[Contrato_Numero], T1.[Contrato_Codigo], T1.[ContratoServicos_Codigo], T1.[ContratoServicos_PrazoTpDias], T2.[Contrato_PrepostoCod], COALESCE( T3.[Contrato_DataTermino], convert( DATETIME, '17530101', 112 )) AS Contrato_DataTermino FROM (([ContratoServicos] T1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = T1.[Contrato_Codigo]) LEFT JOIN (SELECT CASE  WHEN T4.[Contrato_DataVigenciaTermino] > COALESCE( T5.[ContratoTermoAditivo_DataFim], convert( DATETIME, '17530101', 112 )) THEN T4.[Contrato_DataVigenciaTermino] ELSE COALESCE( T5.[ContratoTermoAditivo_DataFim], convert( DATETIME, '17530101', 112 )) END AS Contrato_DataTermino, T4.[Contrato_Codigo] FROM ([Contrato] T4 WITH (NOLOCK) LEFT JOIN (SELECT T6.[ContratoTermoAditivo_DataFim], T6.[Contrato_Codigo], T6.[ContratoTermoAditivo_Codigo], T7.[GXC5] AS GXC5 FROM ([ContratoTermoAditivo] T6 WITH (NOLOCK) INNER JOIN (SELECT MAX([ContratoTermoAditivo_Codigo]) AS GXC5, [Contrato_Codigo] FROM [ContratoTermoAditivo] WITH (NOLOCK) GROUP BY [Contrato_Codigo] ) T7 ON T7.[Contrato_Codigo] = T6.[Contrato_Codigo]) WHERE T6.[ContratoTermoAditivo_Codigo] = T7.[GXC5] ) T5 ON T5.[Contrato_Codigo] = T4.[Contrato_Codigo]) ) T3 ON T3.[Contrato_Codigo] = T1.[Contrato_Codigo]) WHERE (T1.[Servico_Codigo] = @AV28Servico_Codigo) AND (T2.[Contrato_Ativo] = 1) AND (T2.[Contratada_Codigo] = @AV18NovaPrestadora) ORDER BY T1.[Servico_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00OH11,100,0,true,false )
             ,new CursorDef("H00OH12", "SELECT TOP 1 T1.[Servico_Codigo], T1.[Contrato_Codigo], T1.[ContratoServicos_Codigo], T1.[ContratoServicos_PrazoTpDias], T2.[Contrato_PrepostoCod] FROM ([ContratoServicos] T1 WITH (NOLOCK) INNER JOIN [Contrato] T2 WITH (NOLOCK) ON T2.[Contrato_Codigo] = T1.[Contrato_Codigo]) WHERE (T1.[Contrato_Codigo] = @AV10Contrato) AND (T1.[Servico_Codigo] = @AV28Servico_Codigo) ORDER BY T1.[Contrato_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00OH12,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((int[]) buf[6])[0] = rslt.getInt(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((int[]) buf[8])[0] = rslt.getInt(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((int[]) buf[10])[0] = rslt.getInt(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((int[]) buf[12])[0] = rslt.getInt(8) ;
                ((int[]) buf[13])[0] = rslt.getInt(9) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(9);
                ((int[]) buf[15])[0] = rslt.getInt(10) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(10);
                ((String[]) buf[17])[0] = rslt.getString(11, 1) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(11);
                ((String[]) buf[19])[0] = rslt.getString(12, 100) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(12);
                ((String[]) buf[21])[0] = rslt.getVarchar(13) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(13);
                ((int[]) buf[23])[0] = rslt.getInt(14) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(14);
                ((int[]) buf[25])[0] = rslt.getInt(15) ;
                ((String[]) buf[26])[0] = rslt.getVarchar(16) ;
                ((bool[]) buf[27])[0] = rslt.wasNull(16);
                ((short[]) buf[28])[0] = rslt.getShort(17) ;
                ((bool[]) buf[29])[0] = rslt.wasNull(17);
                ((String[]) buf[30])[0] = rslt.getVarchar(18) ;
                ((bool[]) buf[31])[0] = rslt.wasNull(18);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                ((String[]) buf[4])[0] = rslt.getString(5, 100) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(5);
                ((int[]) buf[6])[0] = rslt.getInt(6) ;
                ((bool[]) buf[7])[0] = rslt.getBool(7) ;
                ((DateTime[]) buf[8])[0] = rslt.getGXDate(8) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(8);
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 3 :
                ((bool[]) buf[0])[0] = rslt.getBool(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 20) ;
                ((int[]) buf[4])[0] = rslt.getInt(5) ;
                ((int[]) buf[5])[0] = rslt.getInt(6) ;
                ((String[]) buf[6])[0] = rslt.getString(7, 1) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(7);
                ((int[]) buf[8])[0] = rslt.getInt(8) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(8);
                ((DateTime[]) buf[10])[0] = rslt.getGXDate(9) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(9);
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 1) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((int[]) buf[5])[0] = rslt.getInt(5) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                return;
             case 2 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                stmt.SetParameter(2, (short)parms[2]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
       }
    }

 }

}
