/*
               File: GetContratoContratoSaldoContratoWCFilterData
        Description: Get Contrato Contrato Saldo Contrato WCFilter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/29/2020 1:57:44.25
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getcontratocontratosaldocontratowcfilterdata : GXProcedure
   {
      public getcontratocontratosaldocontratowcfilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getcontratocontratosaldocontratowcfilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV26DDOName = aP0_DDOName;
         this.AV24SearchTxt = aP1_SearchTxt;
         this.AV25SearchTxtTo = aP2_SearchTxtTo;
         this.AV30OptionsJson = "" ;
         this.AV33OptionsDescJson = "" ;
         this.AV35OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV30OptionsJson;
         aP4_OptionsDescJson=this.AV33OptionsDescJson;
         aP5_OptionIndexesJson=this.AV35OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV26DDOName = aP0_DDOName;
         this.AV24SearchTxt = aP1_SearchTxt;
         this.AV25SearchTxtTo = aP2_SearchTxtTo;
         this.AV30OptionsJson = "" ;
         this.AV33OptionsDescJson = "" ;
         this.AV35OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV30OptionsJson;
         aP4_OptionsDescJson=this.AV33OptionsDescJson;
         aP5_OptionIndexesJson=this.AV35OptionIndexesJson;
         return AV35OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getcontratocontratosaldocontratowcfilterdata objgetcontratocontratosaldocontratowcfilterdata;
         objgetcontratocontratosaldocontratowcfilterdata = new getcontratocontratosaldocontratowcfilterdata();
         objgetcontratocontratosaldocontratowcfilterdata.AV26DDOName = aP0_DDOName;
         objgetcontratocontratosaldocontratowcfilterdata.AV24SearchTxt = aP1_SearchTxt;
         objgetcontratocontratosaldocontratowcfilterdata.AV25SearchTxtTo = aP2_SearchTxtTo;
         objgetcontratocontratosaldocontratowcfilterdata.AV30OptionsJson = "" ;
         objgetcontratocontratosaldocontratowcfilterdata.AV33OptionsDescJson = "" ;
         objgetcontratocontratosaldocontratowcfilterdata.AV35OptionIndexesJson = "" ;
         objgetcontratocontratosaldocontratowcfilterdata.context.SetSubmitInitialConfig(context);
         objgetcontratocontratosaldocontratowcfilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetcontratocontratosaldocontratowcfilterdata);
         aP3_OptionsJson=this.AV30OptionsJson;
         aP4_OptionsDescJson=this.AV33OptionsDescJson;
         aP5_OptionIndexesJson=this.AV35OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getcontratocontratosaldocontratowcfilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV29Options = (IGxCollection)(new GxSimpleCollection());
         AV32OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV34OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV26DDOName), "DDO_SALDOCONTRATO_UNIDADEMEDICAO_NOME") == 0 )
         {
            /* Execute user subroutine: 'LOADSALDOCONTRATO_UNIDADEMEDICAO_NOMEOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV30OptionsJson = AV29Options.ToJSonString(false);
         AV33OptionsDescJson = AV32OptionsDesc.ToJSonString(false);
         AV35OptionIndexesJson = AV34OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV37Session.Get("ContratoContratoSaldoContratoWCGridState"), "") == 0 )
         {
            AV39GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "ContratoContratoSaldoContratoWCGridState"), "");
         }
         else
         {
            AV39GridState.FromXml(AV37Session.Get("ContratoContratoSaldoContratoWCGridState"), "");
         }
         AV45GXV1 = 1;
         while ( AV45GXV1 <= AV39GridState.gxTpr_Filtervalues.Count )
         {
            AV40GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV39GridState.gxTpr_Filtervalues.Item(AV45GXV1));
            if ( StringUtil.StrCmp(AV40GridStateFilterValue.gxTpr_Name, "TFSALDOCONTRATO_VIGENCIAINICIO") == 0 )
            {
               AV10TFSaldoContrato_VigenciaInicio = context.localUtil.CToD( AV40GridStateFilterValue.gxTpr_Value, 2);
               AV11TFSaldoContrato_VigenciaInicio_To = context.localUtil.CToD( AV40GridStateFilterValue.gxTpr_Valueto, 2);
            }
            else if ( StringUtil.StrCmp(AV40GridStateFilterValue.gxTpr_Name, "TFSALDOCONTRATO_VIGENCIAFIM") == 0 )
            {
               AV12TFSaldoContrato_VigenciaFim = context.localUtil.CToD( AV40GridStateFilterValue.gxTpr_Value, 2);
               AV13TFSaldoContrato_VigenciaFim_To = context.localUtil.CToD( AV40GridStateFilterValue.gxTpr_Valueto, 2);
            }
            else if ( StringUtil.StrCmp(AV40GridStateFilterValue.gxTpr_Name, "TFSALDOCONTRATO_UNIDADEMEDICAO_NOME") == 0 )
            {
               AV14TFSaldoContrato_UnidadeMedicao_Nome = AV40GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV40GridStateFilterValue.gxTpr_Name, "TFSALDOCONTRATO_UNIDADEMEDICAO_NOME_SEL") == 0 )
            {
               AV15TFSaldoContrato_UnidadeMedicao_Nome_Sel = AV40GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV40GridStateFilterValue.gxTpr_Name, "TFSALDOCONTRATO_CREDITO") == 0 )
            {
               AV16TFSaldoContrato_Credito = NumberUtil.Val( AV40GridStateFilterValue.gxTpr_Value, ".");
               AV17TFSaldoContrato_Credito_To = NumberUtil.Val( AV40GridStateFilterValue.gxTpr_Valueto, ".");
            }
            else if ( StringUtil.StrCmp(AV40GridStateFilterValue.gxTpr_Name, "TFSALDOCONTRATO_RESERVADO") == 0 )
            {
               AV18TFSaldoContrato_Reservado = NumberUtil.Val( AV40GridStateFilterValue.gxTpr_Value, ".");
               AV19TFSaldoContrato_Reservado_To = NumberUtil.Val( AV40GridStateFilterValue.gxTpr_Valueto, ".");
            }
            else if ( StringUtil.StrCmp(AV40GridStateFilterValue.gxTpr_Name, "TFSALDOCONTRATO_EXECUTADO") == 0 )
            {
               AV20TFSaldoContrato_Executado = NumberUtil.Val( AV40GridStateFilterValue.gxTpr_Value, ".");
               AV21TFSaldoContrato_Executado_To = NumberUtil.Val( AV40GridStateFilterValue.gxTpr_Valueto, ".");
            }
            else if ( StringUtil.StrCmp(AV40GridStateFilterValue.gxTpr_Name, "TFSALDOCONTRATO_SALDO") == 0 )
            {
               AV22TFSaldoContrato_Saldo = NumberUtil.Val( AV40GridStateFilterValue.gxTpr_Value, ".");
               AV23TFSaldoContrato_Saldo_To = NumberUtil.Val( AV40GridStateFilterValue.gxTpr_Valueto, ".");
            }
            else if ( StringUtil.StrCmp(AV40GridStateFilterValue.gxTpr_Name, "PARM_&CONTRATO_CODIGO") == 0 )
            {
               AV42Contrato_Codigo = (int)(NumberUtil.Val( AV40GridStateFilterValue.gxTpr_Value, "."));
            }
            AV45GXV1 = (int)(AV45GXV1+1);
         }
      }

      protected void S121( )
      {
         /* 'LOADSALDOCONTRATO_UNIDADEMEDICAO_NOMEOPTIONS' Routine */
         AV14TFSaldoContrato_UnidadeMedicao_Nome = AV24SearchTxt;
         AV15TFSaldoContrato_UnidadeMedicao_Nome_Sel = "";
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              AV10TFSaldoContrato_VigenciaInicio ,
                                              AV11TFSaldoContrato_VigenciaInicio_To ,
                                              AV12TFSaldoContrato_VigenciaFim ,
                                              AV13TFSaldoContrato_VigenciaFim_To ,
                                              AV15TFSaldoContrato_UnidadeMedicao_Nome_Sel ,
                                              AV14TFSaldoContrato_UnidadeMedicao_Nome ,
                                              AV16TFSaldoContrato_Credito ,
                                              AV17TFSaldoContrato_Credito_To ,
                                              AV18TFSaldoContrato_Reservado ,
                                              AV19TFSaldoContrato_Reservado_To ,
                                              AV20TFSaldoContrato_Executado ,
                                              AV21TFSaldoContrato_Executado_To ,
                                              AV22TFSaldoContrato_Saldo ,
                                              AV23TFSaldoContrato_Saldo_To ,
                                              A1571SaldoContrato_VigenciaInicio ,
                                              A1572SaldoContrato_VigenciaFim ,
                                              A1784SaldoContrato_UnidadeMedicao_Nome ,
                                              A1573SaldoContrato_Credito ,
                                              A1574SaldoContrato_Reservado ,
                                              A1575SaldoContrato_Executado ,
                                              A1576SaldoContrato_Saldo ,
                                              AV42Contrato_Codigo ,
                                              A74Contrato_Codigo },
                                              new int[] {
                                              TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL,
                                              TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.DECIMAL, TypeConstants.DECIMAL,
                                              TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.INT, TypeConstants.INT
                                              }
         });
         lV14TFSaldoContrato_UnidadeMedicao_Nome = StringUtil.PadR( StringUtil.RTrim( AV14TFSaldoContrato_UnidadeMedicao_Nome), 50, "%");
         /* Using cursor P00IZ2 */
         pr_default.execute(0, new Object[] {AV42Contrato_Codigo, AV10TFSaldoContrato_VigenciaInicio, AV11TFSaldoContrato_VigenciaInicio_To, AV12TFSaldoContrato_VigenciaFim, AV13TFSaldoContrato_VigenciaFim_To, lV14TFSaldoContrato_UnidadeMedicao_Nome, AV15TFSaldoContrato_UnidadeMedicao_Nome_Sel, AV16TFSaldoContrato_Credito, AV17TFSaldoContrato_Credito_To, AV18TFSaldoContrato_Reservado, AV19TFSaldoContrato_Reservado_To, AV20TFSaldoContrato_Executado, AV21TFSaldoContrato_Executado_To, AV22TFSaldoContrato_Saldo, AV23TFSaldoContrato_Saldo_To});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKIZ2 = false;
            A74Contrato_Codigo = P00IZ2_A74Contrato_Codigo[0];
            A1783SaldoContrato_UnidadeMedicao_Codigo = P00IZ2_A1783SaldoContrato_UnidadeMedicao_Codigo[0];
            A1576SaldoContrato_Saldo = P00IZ2_A1576SaldoContrato_Saldo[0];
            A1575SaldoContrato_Executado = P00IZ2_A1575SaldoContrato_Executado[0];
            A1574SaldoContrato_Reservado = P00IZ2_A1574SaldoContrato_Reservado[0];
            A1573SaldoContrato_Credito = P00IZ2_A1573SaldoContrato_Credito[0];
            A1784SaldoContrato_UnidadeMedicao_Nome = P00IZ2_A1784SaldoContrato_UnidadeMedicao_Nome[0];
            n1784SaldoContrato_UnidadeMedicao_Nome = P00IZ2_n1784SaldoContrato_UnidadeMedicao_Nome[0];
            A1572SaldoContrato_VigenciaFim = P00IZ2_A1572SaldoContrato_VigenciaFim[0];
            A1571SaldoContrato_VigenciaInicio = P00IZ2_A1571SaldoContrato_VigenciaInicio[0];
            A1561SaldoContrato_Codigo = P00IZ2_A1561SaldoContrato_Codigo[0];
            A1784SaldoContrato_UnidadeMedicao_Nome = P00IZ2_A1784SaldoContrato_UnidadeMedicao_Nome[0];
            n1784SaldoContrato_UnidadeMedicao_Nome = P00IZ2_n1784SaldoContrato_UnidadeMedicao_Nome[0];
            AV36count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( P00IZ2_A74Contrato_Codigo[0] == A74Contrato_Codigo ) && ( P00IZ2_A1783SaldoContrato_UnidadeMedicao_Codigo[0] == A1783SaldoContrato_UnidadeMedicao_Codigo ) )
            {
               BRKIZ2 = false;
               A1561SaldoContrato_Codigo = P00IZ2_A1561SaldoContrato_Codigo[0];
               AV36count = (long)(AV36count+1);
               BRKIZ2 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A1784SaldoContrato_UnidadeMedicao_Nome)) )
            {
               AV28Option = A1784SaldoContrato_UnidadeMedicao_Nome;
               AV27InsertIndex = 1;
               while ( ( AV27InsertIndex <= AV29Options.Count ) && ( StringUtil.StrCmp(((String)AV29Options.Item(AV27InsertIndex)), AV28Option) < 0 ) )
               {
                  AV27InsertIndex = (int)(AV27InsertIndex+1);
               }
               AV29Options.Add(AV28Option, AV27InsertIndex);
               AV34OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV36count), "Z,ZZZ,ZZZ,ZZ9")), AV27InsertIndex);
            }
            if ( AV29Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKIZ2 )
            {
               BRKIZ2 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV29Options = new GxSimpleCollection();
         AV32OptionsDesc = new GxSimpleCollection();
         AV34OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV37Session = context.GetSession();
         AV39GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV40GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV10TFSaldoContrato_VigenciaInicio = DateTime.MinValue;
         AV11TFSaldoContrato_VigenciaInicio_To = DateTime.MinValue;
         AV12TFSaldoContrato_VigenciaFim = DateTime.MinValue;
         AV13TFSaldoContrato_VigenciaFim_To = DateTime.MinValue;
         AV14TFSaldoContrato_UnidadeMedicao_Nome = "";
         AV15TFSaldoContrato_UnidadeMedicao_Nome_Sel = "";
         scmdbuf = "";
         lV14TFSaldoContrato_UnidadeMedicao_Nome = "";
         A1571SaldoContrato_VigenciaInicio = DateTime.MinValue;
         A1572SaldoContrato_VigenciaFim = DateTime.MinValue;
         A1784SaldoContrato_UnidadeMedicao_Nome = "";
         P00IZ2_A74Contrato_Codigo = new int[1] ;
         P00IZ2_A1783SaldoContrato_UnidadeMedicao_Codigo = new int[1] ;
         P00IZ2_A1576SaldoContrato_Saldo = new decimal[1] ;
         P00IZ2_A1575SaldoContrato_Executado = new decimal[1] ;
         P00IZ2_A1574SaldoContrato_Reservado = new decimal[1] ;
         P00IZ2_A1573SaldoContrato_Credito = new decimal[1] ;
         P00IZ2_A1784SaldoContrato_UnidadeMedicao_Nome = new String[] {""} ;
         P00IZ2_n1784SaldoContrato_UnidadeMedicao_Nome = new bool[] {false} ;
         P00IZ2_A1572SaldoContrato_VigenciaFim = new DateTime[] {DateTime.MinValue} ;
         P00IZ2_A1571SaldoContrato_VigenciaInicio = new DateTime[] {DateTime.MinValue} ;
         P00IZ2_A1561SaldoContrato_Codigo = new int[1] ;
         AV28Option = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getcontratocontratosaldocontratowcfilterdata__default(),
            new Object[][] {
                new Object[] {
               P00IZ2_A74Contrato_Codigo, P00IZ2_A1783SaldoContrato_UnidadeMedicao_Codigo, P00IZ2_A1576SaldoContrato_Saldo, P00IZ2_A1575SaldoContrato_Executado, P00IZ2_A1574SaldoContrato_Reservado, P00IZ2_A1573SaldoContrato_Credito, P00IZ2_A1784SaldoContrato_UnidadeMedicao_Nome, P00IZ2_n1784SaldoContrato_UnidadeMedicao_Nome, P00IZ2_A1572SaldoContrato_VigenciaFim, P00IZ2_A1571SaldoContrato_VigenciaInicio,
               P00IZ2_A1561SaldoContrato_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV45GXV1 ;
      private int AV42Contrato_Codigo ;
      private int A74Contrato_Codigo ;
      private int A1783SaldoContrato_UnidadeMedicao_Codigo ;
      private int A1561SaldoContrato_Codigo ;
      private int AV27InsertIndex ;
      private long AV36count ;
      private decimal AV16TFSaldoContrato_Credito ;
      private decimal AV17TFSaldoContrato_Credito_To ;
      private decimal AV18TFSaldoContrato_Reservado ;
      private decimal AV19TFSaldoContrato_Reservado_To ;
      private decimal AV20TFSaldoContrato_Executado ;
      private decimal AV21TFSaldoContrato_Executado_To ;
      private decimal AV22TFSaldoContrato_Saldo ;
      private decimal AV23TFSaldoContrato_Saldo_To ;
      private decimal A1573SaldoContrato_Credito ;
      private decimal A1574SaldoContrato_Reservado ;
      private decimal A1575SaldoContrato_Executado ;
      private decimal A1576SaldoContrato_Saldo ;
      private String AV14TFSaldoContrato_UnidadeMedicao_Nome ;
      private String AV15TFSaldoContrato_UnidadeMedicao_Nome_Sel ;
      private String scmdbuf ;
      private String lV14TFSaldoContrato_UnidadeMedicao_Nome ;
      private String A1784SaldoContrato_UnidadeMedicao_Nome ;
      private DateTime AV10TFSaldoContrato_VigenciaInicio ;
      private DateTime AV11TFSaldoContrato_VigenciaInicio_To ;
      private DateTime AV12TFSaldoContrato_VigenciaFim ;
      private DateTime AV13TFSaldoContrato_VigenciaFim_To ;
      private DateTime A1571SaldoContrato_VigenciaInicio ;
      private DateTime A1572SaldoContrato_VigenciaFim ;
      private bool returnInSub ;
      private bool BRKIZ2 ;
      private bool n1784SaldoContrato_UnidadeMedicao_Nome ;
      private String AV35OptionIndexesJson ;
      private String AV30OptionsJson ;
      private String AV33OptionsDescJson ;
      private String AV26DDOName ;
      private String AV24SearchTxt ;
      private String AV25SearchTxtTo ;
      private String AV28Option ;
      private IGxSession AV37Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00IZ2_A74Contrato_Codigo ;
      private int[] P00IZ2_A1783SaldoContrato_UnidadeMedicao_Codigo ;
      private decimal[] P00IZ2_A1576SaldoContrato_Saldo ;
      private decimal[] P00IZ2_A1575SaldoContrato_Executado ;
      private decimal[] P00IZ2_A1574SaldoContrato_Reservado ;
      private decimal[] P00IZ2_A1573SaldoContrato_Credito ;
      private String[] P00IZ2_A1784SaldoContrato_UnidadeMedicao_Nome ;
      private bool[] P00IZ2_n1784SaldoContrato_UnidadeMedicao_Nome ;
      private DateTime[] P00IZ2_A1572SaldoContrato_VigenciaFim ;
      private DateTime[] P00IZ2_A1571SaldoContrato_VigenciaInicio ;
      private int[] P00IZ2_A1561SaldoContrato_Codigo ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV29Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV32OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV34OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV39GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV40GridStateFilterValue ;
   }

   public class getcontratocontratosaldocontratowcfilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00IZ2( IGxContext context ,
                                             DateTime AV10TFSaldoContrato_VigenciaInicio ,
                                             DateTime AV11TFSaldoContrato_VigenciaInicio_To ,
                                             DateTime AV12TFSaldoContrato_VigenciaFim ,
                                             DateTime AV13TFSaldoContrato_VigenciaFim_To ,
                                             String AV15TFSaldoContrato_UnidadeMedicao_Nome_Sel ,
                                             String AV14TFSaldoContrato_UnidadeMedicao_Nome ,
                                             decimal AV16TFSaldoContrato_Credito ,
                                             decimal AV17TFSaldoContrato_Credito_To ,
                                             decimal AV18TFSaldoContrato_Reservado ,
                                             decimal AV19TFSaldoContrato_Reservado_To ,
                                             decimal AV20TFSaldoContrato_Executado ,
                                             decimal AV21TFSaldoContrato_Executado_To ,
                                             decimal AV22TFSaldoContrato_Saldo ,
                                             decimal AV23TFSaldoContrato_Saldo_To ,
                                             DateTime A1571SaldoContrato_VigenciaInicio ,
                                             DateTime A1572SaldoContrato_VigenciaFim ,
                                             String A1784SaldoContrato_UnidadeMedicao_Nome ,
                                             decimal A1573SaldoContrato_Credito ,
                                             decimal A1574SaldoContrato_Reservado ,
                                             decimal A1575SaldoContrato_Executado ,
                                             decimal A1576SaldoContrato_Saldo ,
                                             int AV42Contrato_Codigo ,
                                             int A74Contrato_Codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [15] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT T1.[Contrato_Codigo], T1.[SaldoContrato_UnidadeMedicao_Codigo] AS SaldoContrato_UnidadeMedicao_Codigo, T1.[SaldoContrato_Saldo], T1.[SaldoContrato_Executado], T1.[SaldoContrato_Reservado], T1.[SaldoContrato_Credito], T2.[UnidadeMedicao_Nome] AS SaldoContrato_UnidadeMedicao_Nome, T1.[SaldoContrato_VigenciaFim], T1.[SaldoContrato_VigenciaInicio], T1.[SaldoContrato_Codigo] FROM ([SaldoContrato] T1 WITH (NOLOCK) INNER JOIN [UnidadeMedicao] T2 WITH (NOLOCK) ON T2.[UnidadeMedicao_Codigo] = T1.[SaldoContrato_UnidadeMedicao_Codigo])";
         scmdbuf = scmdbuf + " WHERE (T1.[Contrato_Codigo] = @AV42Contrato_Codigo)";
         if ( ! (DateTime.MinValue==AV10TFSaldoContrato_VigenciaInicio) )
         {
            sWhereString = sWhereString + " and (T1.[SaldoContrato_VigenciaInicio] >= @AV10TFSaldoContrato_VigenciaInicio)";
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( ! (DateTime.MinValue==AV11TFSaldoContrato_VigenciaInicio_To) )
         {
            sWhereString = sWhereString + " and (T1.[SaldoContrato_VigenciaInicio] <= @AV11TFSaldoContrato_VigenciaInicio_To)";
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( ! (DateTime.MinValue==AV12TFSaldoContrato_VigenciaFim) )
         {
            sWhereString = sWhereString + " and (T1.[SaldoContrato_VigenciaFim] >= @AV12TFSaldoContrato_VigenciaFim)";
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( ! (DateTime.MinValue==AV13TFSaldoContrato_VigenciaFim_To) )
         {
            sWhereString = sWhereString + " and (T1.[SaldoContrato_VigenciaFim] <= @AV13TFSaldoContrato_VigenciaFim_To)";
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV15TFSaldoContrato_UnidadeMedicao_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV14TFSaldoContrato_UnidadeMedicao_Nome)) ) )
         {
            sWhereString = sWhereString + " and (T2.[UnidadeMedicao_Nome] like @lV14TFSaldoContrato_UnidadeMedicao_Nome)";
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV15TFSaldoContrato_UnidadeMedicao_Nome_Sel)) )
         {
            sWhereString = sWhereString + " and (T2.[UnidadeMedicao_Nome] = @AV15TFSaldoContrato_UnidadeMedicao_Nome_Sel)";
         }
         else
         {
            GXv_int1[6] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV16TFSaldoContrato_Credito) )
         {
            sWhereString = sWhereString + " and (T1.[SaldoContrato_Credito] >= @AV16TFSaldoContrato_Credito)";
         }
         else
         {
            GXv_int1[7] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV17TFSaldoContrato_Credito_To) )
         {
            sWhereString = sWhereString + " and (T1.[SaldoContrato_Credito] <= @AV17TFSaldoContrato_Credito_To)";
         }
         else
         {
            GXv_int1[8] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV18TFSaldoContrato_Reservado) )
         {
            sWhereString = sWhereString + " and (T1.[SaldoContrato_Reservado] >= @AV18TFSaldoContrato_Reservado)";
         }
         else
         {
            GXv_int1[9] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV19TFSaldoContrato_Reservado_To) )
         {
            sWhereString = sWhereString + " and (T1.[SaldoContrato_Reservado] <= @AV19TFSaldoContrato_Reservado_To)";
         }
         else
         {
            GXv_int1[10] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV20TFSaldoContrato_Executado) )
         {
            sWhereString = sWhereString + " and (T1.[SaldoContrato_Executado] >= @AV20TFSaldoContrato_Executado)";
         }
         else
         {
            GXv_int1[11] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV21TFSaldoContrato_Executado_To) )
         {
            sWhereString = sWhereString + " and (T1.[SaldoContrato_Executado] <= @AV21TFSaldoContrato_Executado_To)";
         }
         else
         {
            GXv_int1[12] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV22TFSaldoContrato_Saldo) )
         {
            sWhereString = sWhereString + " and (T1.[SaldoContrato_Saldo] >= @AV22TFSaldoContrato_Saldo)";
         }
         else
         {
            GXv_int1[13] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV23TFSaldoContrato_Saldo_To) )
         {
            sWhereString = sWhereString + " and (T1.[SaldoContrato_Saldo] <= @AV23TFSaldoContrato_Saldo_To)";
         }
         else
         {
            GXv_int1[14] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[Contrato_Codigo], T1.[SaldoContrato_UnidadeMedicao_Codigo]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00IZ2(context, (DateTime)dynConstraints[0] , (DateTime)dynConstraints[1] , (DateTime)dynConstraints[2] , (DateTime)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (decimal)dynConstraints[6] , (decimal)dynConstraints[7] , (decimal)dynConstraints[8] , (decimal)dynConstraints[9] , (decimal)dynConstraints[10] , (decimal)dynConstraints[11] , (decimal)dynConstraints[12] , (decimal)dynConstraints[13] , (DateTime)dynConstraints[14] , (DateTime)dynConstraints[15] , (String)dynConstraints[16] , (decimal)dynConstraints[17] , (decimal)dynConstraints[18] , (decimal)dynConstraints[19] , (decimal)dynConstraints[20] , (int)dynConstraints[21] , (int)dynConstraints[22] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00IZ2 ;
          prmP00IZ2 = new Object[] {
          new Object[] {"@AV42Contrato_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV10TFSaldoContrato_VigenciaInicio",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV11TFSaldoContrato_VigenciaInicio_To",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV12TFSaldoContrato_VigenciaFim",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV13TFSaldoContrato_VigenciaFim_To",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV14TFSaldoContrato_UnidadeMedicao_Nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV15TFSaldoContrato_UnidadeMedicao_Nome_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@AV16TFSaldoContrato_Credito",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV17TFSaldoContrato_Credito_To",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV18TFSaldoContrato_Reservado",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV19TFSaldoContrato_Reservado_To",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV20TFSaldoContrato_Executado",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV21TFSaldoContrato_Executado_To",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV22TFSaldoContrato_Saldo",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV23TFSaldoContrato_Saldo_To",SqlDbType.Decimal,18,5}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00IZ2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00IZ2,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((decimal[]) buf[2])[0] = rslt.getDecimal(3) ;
                ((decimal[]) buf[3])[0] = rslt.getDecimal(4) ;
                ((decimal[]) buf[4])[0] = rslt.getDecimal(5) ;
                ((decimal[]) buf[5])[0] = rslt.getDecimal(6) ;
                ((String[]) buf[6])[0] = rslt.getString(7, 50) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(7);
                ((DateTime[]) buf[8])[0] = rslt.getGXDate(8) ;
                ((DateTime[]) buf[9])[0] = rslt.getGXDate(9) ;
                ((int[]) buf[10])[0] = rslt.getInt(10) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[15]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[16]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[17]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[18]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[19]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[22]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[23]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[24]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[25]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[26]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[27]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[28]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[29]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getcontratocontratosaldocontratowcfilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getcontratocontratosaldocontratowcfilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getcontratocontratosaldocontratowcfilterdata") )
          {
             return  ;
          }
          getcontratocontratosaldocontratowcfilterdata worker = new getcontratocontratosaldocontratowcfilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
