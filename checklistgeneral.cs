/*
               File: CheckListGeneral
        Description: Check List General
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 23:3:20.49
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class checklistgeneral : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public checklistgeneral( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public checklistgeneral( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_CheckList_Codigo )
      {
         this.A758CheckList_Codigo = aP0_CheckList_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
         cmbCheckList_De = new GXCombobox();
         cmbCheckList_Obrigatorio = new GXCombobox();
         chkCheckList_Ativo = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  A758CheckList_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A758CheckList_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A758CheckList_Codigo), 6, 0)));
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(int)A758CheckList_Codigo});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PAO42( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV15Pgmname = "CheckListGeneral";
               context.Gx_err = 0;
               WSO42( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "Check List General") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20204282332054");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
            context.WriteHtmlText( "<body") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("checklistgeneral.aspx") + "?" + UrlEncode("" +A758CheckList_Codigo)+"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOA758CheckList_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOA758CheckList_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vCHECK_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV12Check_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CHECK_CODIGO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1839Check_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CHECKLIST_DESCRICAO", GetSecureSignedToken( sPrefix, A763CheckList_Descricao));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CHECKLIST_DE", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1230CheckList_De), "ZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CHECKLIST_OBRIGATORIO", GetSecureSignedToken( sPrefix, A1845CheckList_Obrigatorio));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_CHECKLIST_ATIVO", GetSecureSignedToken( sPrefix, A1151CheckList_Ativo));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseFormO42( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("checklistgeneral.js", "?20204282332056");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            context.WriteHtmlTextNl( "</form>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "CheckListGeneral" ;
      }

      public override String GetPgmdesc( )
      {
         return "Check List General" ;
      }

      protected void WBO40( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "checklistgeneral.aspx");
            }
            wb_table1_2_O42( true) ;
         }
         else
         {
            wb_table1_2_O42( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_O42e( bool wbgen )
      {
         if ( wbgen )
         {
         }
         wbLoad = true;
      }

      protected void STARTO42( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "Check List General", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUPO40( ) ;
            }
         }
      }

      protected void WSO42( )
      {
         STARTO42( ) ;
         EVTO42( ) ;
      }

      protected void EVTO42( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPO40( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPO40( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E11O42 */
                                    E11O42 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPO40( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E12O42 */
                                    E12O42 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOUPDATE'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPO40( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E13O42 */
                                    E13O42 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DODELETE'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPO40( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E14O42 */
                                    E14O42 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPO40( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                              }
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPO40( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEO42( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormO42( ) ;
            }
         }
      }

      protected void PAO42( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            cmbCheckList_De.Name = "CHECKLIST_DE";
            cmbCheckList_De.WebTags = "";
            cmbCheckList_De.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 4, 0)), "(Nenhum)", 0);
            cmbCheckList_De.addItem("1", "Libera valida��o", 0);
            cmbCheckList_De.addItem("2", "Em an�lise", 0);
            if ( cmbCheckList_De.ItemCount > 0 )
            {
               A1230CheckList_De = (short)(NumberUtil.Val( cmbCheckList_De.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1230CheckList_De), 4, 0))), "."));
               n1230CheckList_De = false;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1230CheckList_De", StringUtil.LTrim( StringUtil.Str( (decimal)(A1230CheckList_De), 4, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CHECKLIST_DE", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1230CheckList_De), "ZZZ9")));
            }
            cmbCheckList_Obrigatorio.Name = "CHECKLIST_OBRIGATORIO";
            cmbCheckList_Obrigatorio.WebTags = "";
            cmbCheckList_Obrigatorio.addItem(StringUtil.BoolToStr( false), "N�o", 0);
            cmbCheckList_Obrigatorio.addItem(StringUtil.BoolToStr( true), "Sim", 0);
            if ( cmbCheckList_Obrigatorio.ItemCount > 0 )
            {
               A1845CheckList_Obrigatorio = StringUtil.StrToBool( cmbCheckList_Obrigatorio.getValidValue(StringUtil.BoolToStr( A1845CheckList_Obrigatorio)));
               n1845CheckList_Obrigatorio = false;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1845CheckList_Obrigatorio", A1845CheckList_Obrigatorio);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CHECKLIST_OBRIGATORIO", GetSecureSignedToken( sPrefix, A1845CheckList_Obrigatorio));
            }
            chkCheckList_Ativo.Name = "CHECKLIST_ATIVO";
            chkCheckList_Ativo.WebTags = "";
            chkCheckList_Ativo.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkCheckList_Ativo_Internalname, "TitleCaption", chkCheckList_Ativo.Caption);
            chkCheckList_Ativo.CheckedValue = "false";
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbCheckList_De.ItemCount > 0 )
         {
            A1230CheckList_De = (short)(NumberUtil.Val( cmbCheckList_De.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1230CheckList_De), 4, 0))), "."));
            n1230CheckList_De = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1230CheckList_De", StringUtil.LTrim( StringUtil.Str( (decimal)(A1230CheckList_De), 4, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CHECKLIST_DE", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1230CheckList_De), "ZZZ9")));
         }
         if ( cmbCheckList_Obrigatorio.ItemCount > 0 )
         {
            A1845CheckList_Obrigatorio = StringUtil.StrToBool( cmbCheckList_Obrigatorio.getValidValue(StringUtil.BoolToStr( A1845CheckList_Obrigatorio)));
            n1845CheckList_Obrigatorio = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1845CheckList_Obrigatorio", A1845CheckList_Obrigatorio);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CHECKLIST_OBRIGATORIO", GetSecureSignedToken( sPrefix, A1845CheckList_Obrigatorio));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFO42( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV15Pgmname = "CheckListGeneral";
         context.Gx_err = 0;
      }

      protected void RFO42( )
      {
         initialize_formulas( ) ;
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Using cursor H00O42 */
            pr_default.execute(0, new Object[] {A758CheckList_Codigo});
            while ( (pr_default.getStatus(0) != 101) )
            {
               A1151CheckList_Ativo = H00O42_A1151CheckList_Ativo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1151CheckList_Ativo", A1151CheckList_Ativo);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CHECKLIST_ATIVO", GetSecureSignedToken( sPrefix, A1151CheckList_Ativo));
               A1845CheckList_Obrigatorio = H00O42_A1845CheckList_Obrigatorio[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1845CheckList_Obrigatorio", A1845CheckList_Obrigatorio);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CHECKLIST_OBRIGATORIO", GetSecureSignedToken( sPrefix, A1845CheckList_Obrigatorio));
               n1845CheckList_Obrigatorio = H00O42_n1845CheckList_Obrigatorio[0];
               A1230CheckList_De = H00O42_A1230CheckList_De[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1230CheckList_De", StringUtil.LTrim( StringUtil.Str( (decimal)(A1230CheckList_De), 4, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CHECKLIST_DE", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1230CheckList_De), "ZZZ9")));
               n1230CheckList_De = H00O42_n1230CheckList_De[0];
               A763CheckList_Descricao = H00O42_A763CheckList_Descricao[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A763CheckList_Descricao", A763CheckList_Descricao);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CHECKLIST_DESCRICAO", GetSecureSignedToken( sPrefix, A763CheckList_Descricao));
               A1839Check_Codigo = H00O42_A1839Check_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1839Check_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1839Check_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CHECK_CODIGO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1839Check_Codigo), "ZZZZZ9")));
               n1839Check_Codigo = H00O42_n1839Check_Codigo[0];
               /* Execute user event: E12O42 */
               E12O42 ();
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(0);
            WBO40( ) ;
         }
      }

      protected void STRUPO40( )
      {
         /* Before Start, stand alone formulas. */
         AV15Pgmname = "CheckListGeneral";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11O42 */
         E11O42 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            A1839Check_Codigo = (int)(context.localUtil.CToN( cgiGet( edtCheck_Codigo_Internalname), ",", "."));
            n1839Check_Codigo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1839Check_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1839Check_Codigo), 6, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CHECK_CODIGO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1839Check_Codigo), "ZZZZZ9")));
            A763CheckList_Descricao = cgiGet( edtCheckList_Descricao_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A763CheckList_Descricao", A763CheckList_Descricao);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CHECKLIST_DESCRICAO", GetSecureSignedToken( sPrefix, A763CheckList_Descricao));
            cmbCheckList_De.CurrentValue = cgiGet( cmbCheckList_De_Internalname);
            A1230CheckList_De = (short)(NumberUtil.Val( cgiGet( cmbCheckList_De_Internalname), "."));
            n1230CheckList_De = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1230CheckList_De", StringUtil.LTrim( StringUtil.Str( (decimal)(A1230CheckList_De), 4, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CHECKLIST_DE", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1230CheckList_De), "ZZZ9")));
            cmbCheckList_Obrigatorio.CurrentValue = cgiGet( cmbCheckList_Obrigatorio_Internalname);
            A1845CheckList_Obrigatorio = StringUtil.StrToBool( cgiGet( cmbCheckList_Obrigatorio_Internalname));
            n1845CheckList_Obrigatorio = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1845CheckList_Obrigatorio", A1845CheckList_Obrigatorio);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CHECKLIST_OBRIGATORIO", GetSecureSignedToken( sPrefix, A1845CheckList_Obrigatorio));
            A1151CheckList_Ativo = StringUtil.StrToBool( cgiGet( chkCheckList_Ativo_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1151CheckList_Ativo", A1151CheckList_Ativo);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "gxhash_CHECKLIST_ATIVO", GetSecureSignedToken( sPrefix, A1151CheckList_Ativo));
            /* Read saved values. */
            wcpOA758CheckList_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA758CheckList_Codigo"), ",", "."));
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E11O42 */
         E11O42 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E11O42( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void nextLoad( )
      {
      }

      protected void E12O42( )
      {
         /* Load Routine */
      }

      protected void E13O42( )
      {
         /* 'DoUpdate' Routine */
         context.wjLoc = formatLink("checklist.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A758CheckList_Codigo) + "," + UrlEncode("" +AV12Check_Codigo);
         context.wjLocDisableFrm = 1;
      }

      protected void E14O42( )
      {
         /* 'DoDelete' Routine */
         context.wjLoc = formatLink("checklist.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A758CheckList_Codigo) + "," + UrlEncode("" +AV12Check_Codigo);
         context.wjLocDisableFrm = 1;
      }

      protected void S112( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8TrnContext.gxTpr_Callerobject = AV15Pgmname;
         AV8TrnContext.gxTpr_Callerondelete = false;
         AV8TrnContext.gxTpr_Callerurl = AV11HTTPRequest.ScriptName+"?"+AV11HTTPRequest.QueryString;
         AV8TrnContext.gxTpr_Transactionname = "CheckList";
         AV9TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV9TrnContextAtt.gxTpr_Attributename = "CheckList_Codigo";
         AV9TrnContextAtt.gxTpr_Attributevalue = StringUtil.Str( (decimal)(AV7CheckList_Codigo), 6, 0);
         AV8TrnContext.gxTpr_Attributes.Add(AV9TrnContextAtt, 0);
         AV10Session.Set("TrnContext", AV8TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_MeetrikaVs3"));
      }

      protected void wb_table1_2_O42( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, sPrefix, "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_O42( true) ;
         }
         else
         {
            wb_table2_8_O42( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_O42e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_41_O42( true) ;
         }
         else
         {
            wb_table3_41_O42( false) ;
         }
         return  ;
      }

      protected void wb_table3_41_O42e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_O42e( true) ;
         }
         else
         {
            wb_table1_2_O42e( false) ;
         }
      }

      protected void wb_table3_41_O42( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 44,'" + sPrefix + "',false,'',0)\"";
            ClassString = "btn btn-success";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnupdate_Internalname, "", "Modifica", bttBtnupdate_Jsonclick, 5, "Modifica", "", StyleString, ClassString, 1, 1, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOUPDATE\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_CheckListGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 46,'" + sPrefix + "',false,'',0)\"";
            ClassString = "btn btn-default";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtndelete_Internalname, "", "Eliminar", bttBtndelete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, 1, 1, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DODELETE\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_CheckListGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_41_O42e( true) ;
         }
         else
         {
            wb_table3_41_O42e( false) ;
         }
      }

      protected void wb_table2_8_O42( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "TableViewGeneralAtts", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockchecklist_codigo_Internalname, "List Item", "", "", lblTextblockchecklist_codigo_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_CheckListGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtCheckList_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A758CheckList_Codigo), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A758CheckList_Codigo), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtCheckList_Codigo_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_CheckListGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcheck_codigo_Internalname, "Check List", "", "", lblTextblockcheck_codigo_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_CheckListGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtCheck_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1839Check_Codigo), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A1839Check_Codigo), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtCheck_Codigo_Jsonclick, 0, "BootstrapAttributeGray", "", "", "", 1, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_CheckListGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockchecklist_descricao_Internalname, "Descri��o", "", "", lblTextblockchecklist_descricao_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_CheckListGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Multiple line edit */
            ClassString = "BootstrapAttributeGray";
            StyleString = "";
            ClassString = "BootstrapAttributeGray";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtCheckList_Descricao_Internalname, A763CheckList_Descricao, "", "", 0, 1, 0, 0, 80, "chr", 3, "row", StyleString, ClassString, "", "500", 1, "", "", -1, true, "DescricaoLonga", "HLP_CheckListGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockchecklist_de_Internalname, "List de", "", "", lblTextblockchecklist_de_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_CheckListGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbCheckList_De, cmbCheckList_De_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A1230CheckList_De), 4, 0)), 1, cmbCheckList_De_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "int", "", 1, 0, 0, 0, 0, "em", 0, "", "", "BootstrapAttributeGray", "", "", "", true, "HLP_CheckListGeneral.htm");
            cmbCheckList_De.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A1230CheckList_De), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbCheckList_De_Internalname, "Values", (String)(cmbCheckList_De.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockchecklist_obrigatorio_Internalname, "Obrigat�rio", "", "", lblTextblockchecklist_obrigatorio_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_CheckListGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbCheckList_Obrigatorio, cmbCheckList_Obrigatorio_Internalname, StringUtil.BoolToStr( A1845CheckList_Obrigatorio), 1, cmbCheckList_Obrigatorio_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "boolean", "", 1, 0, 0, 0, 0, "em", 0, "", "", "BootstrapAttributeGray", "", "", "", true, "HLP_CheckListGeneral.htm");
            cmbCheckList_Obrigatorio.CurrentValue = StringUtil.BoolToStr( A1845CheckList_Obrigatorio);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbCheckList_Obrigatorio_Internalname, "Values", (String)(cmbCheckList_Obrigatorio.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockchecklist_ativo_Internalname, "Ativo?", "", "", lblTextblockchecklist_ativo_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_CheckListGeneral.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCellView'>") ;
            /* Check box */
            ClassString = "BootstrapAttributeGray";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkCheckList_Ativo_Internalname, StringUtil.BoolToStr( A1151CheckList_Ativo), "", "", 1, 0, "true", "", StyleString, ClassString, "", "");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_O42e( true) ;
         }
         else
         {
            wb_table2_8_O42e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         A758CheckList_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A758CheckList_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A758CheckList_Codigo), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAO42( ) ;
         WSO42( ) ;
         WEO42( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlA758CheckList_Codigo = (String)((String)getParm(obj,0));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PAO42( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "checklistgeneral");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PAO42( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            A758CheckList_Codigo = Convert.ToInt32(getParm(obj,2));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A758CheckList_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A758CheckList_Codigo), 6, 0)));
         }
         wcpOA758CheckList_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOA758CheckList_Codigo"), ",", "."));
         if ( ! GetJustCreated( ) && ( ( A758CheckList_Codigo != wcpOA758CheckList_Codigo ) ) )
         {
            setjustcreated();
         }
         wcpOA758CheckList_Codigo = A758CheckList_Codigo;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlA758CheckList_Codigo = cgiGet( sPrefix+"A758CheckList_Codigo_CTRL");
         if ( StringUtil.Len( sCtrlA758CheckList_Codigo) > 0 )
         {
            A758CheckList_Codigo = (int)(context.localUtil.CToN( cgiGet( sCtrlA758CheckList_Codigo), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A758CheckList_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A758CheckList_Codigo), 6, 0)));
         }
         else
         {
            A758CheckList_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"A758CheckList_Codigo_PARM"), ",", "."));
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PAO42( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WSO42( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WSO42( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"A758CheckList_Codigo_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(A758CheckList_Codigo), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlA758CheckList_Codigo)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"A758CheckList_Codigo_CTRL", StringUtil.RTrim( sCtrlA758CheckList_Codigo));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WEO42( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?2249787");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20204282332093");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("checklistgeneral.js", "?20204282332093");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblockchecklist_codigo_Internalname = sPrefix+"TEXTBLOCKCHECKLIST_CODIGO";
         edtCheckList_Codigo_Internalname = sPrefix+"CHECKLIST_CODIGO";
         lblTextblockcheck_codigo_Internalname = sPrefix+"TEXTBLOCKCHECK_CODIGO";
         edtCheck_Codigo_Internalname = sPrefix+"CHECK_CODIGO";
         lblTextblockchecklist_descricao_Internalname = sPrefix+"TEXTBLOCKCHECKLIST_DESCRICAO";
         edtCheckList_Descricao_Internalname = sPrefix+"CHECKLIST_DESCRICAO";
         lblTextblockchecklist_de_Internalname = sPrefix+"TEXTBLOCKCHECKLIST_DE";
         cmbCheckList_De_Internalname = sPrefix+"CHECKLIST_DE";
         lblTextblockchecklist_obrigatorio_Internalname = sPrefix+"TEXTBLOCKCHECKLIST_OBRIGATORIO";
         cmbCheckList_Obrigatorio_Internalname = sPrefix+"CHECKLIST_OBRIGATORIO";
         lblTextblockchecklist_ativo_Internalname = sPrefix+"TEXTBLOCKCHECKLIST_ATIVO";
         chkCheckList_Ativo_Internalname = sPrefix+"CHECKLIST_ATIVO";
         tblTableattributes_Internalname = sPrefix+"TABLEATTRIBUTES";
         bttBtnupdate_Internalname = sPrefix+"BTNUPDATE";
         bttBtndelete_Internalname = sPrefix+"BTNDELETE";
         tblTableactions_Internalname = sPrefix+"TABLEACTIONS";
         tblTablecontent_Internalname = sPrefix+"TABLECONTENT";
         Form.Internalname = sPrefix+"FORM";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         cmbCheckList_Obrigatorio_Jsonclick = "";
         cmbCheckList_De_Jsonclick = "";
         edtCheck_Codigo_Jsonclick = "";
         edtCheckList_Codigo_Jsonclick = "";
         chkCheckList_Ativo.Caption = "";
         context.GX_msglist.DisplayMode = 1;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("'DOUPDATE'","{handler:'E13O42',iparms:[{av:'A758CheckList_Codigo',fld:'CHECKLIST_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV12Check_Codigo',fld:'vCHECK_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV12Check_Codigo',fld:'vCHECK_CODIGO',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("'DODELETE'","{handler:'E14O42',iparms:[{av:'A758CheckList_Codigo',fld:'CHECKLIST_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV12Check_Codigo',fld:'vCHECK_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV12Check_Codigo',fld:'vCHECK_CODIGO',pic:'ZZZZZ9',nv:0}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         AV15Pgmname = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         A763CheckList_Descricao = "";
         GXKey = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         scmdbuf = "";
         H00O42_A758CheckList_Codigo = new int[1] ;
         H00O42_A1151CheckList_Ativo = new bool[] {false} ;
         H00O42_A1845CheckList_Obrigatorio = new bool[] {false} ;
         H00O42_n1845CheckList_Obrigatorio = new bool[] {false} ;
         H00O42_A1230CheckList_De = new short[1] ;
         H00O42_n1230CheckList_De = new bool[] {false} ;
         H00O42_A763CheckList_Descricao = new String[] {""} ;
         H00O42_A1839Check_Codigo = new int[1] ;
         H00O42_n1839Check_Codigo = new bool[] {false} ;
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV11HTTPRequest = new GxHttpRequest( context);
         AV9TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV10Session = context.GetSession();
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         bttBtnupdate_Jsonclick = "";
         bttBtndelete_Jsonclick = "";
         lblTextblockchecklist_codigo_Jsonclick = "";
         lblTextblockcheck_codigo_Jsonclick = "";
         lblTextblockchecklist_descricao_Jsonclick = "";
         lblTextblockchecklist_de_Jsonclick = "";
         lblTextblockchecklist_obrigatorio_Jsonclick = "";
         lblTextblockchecklist_ativo_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlA758CheckList_Codigo = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.checklistgeneral__default(),
            new Object[][] {
                new Object[] {
               H00O42_A758CheckList_Codigo, H00O42_A1151CheckList_Ativo, H00O42_A1845CheckList_Obrigatorio, H00O42_n1845CheckList_Obrigatorio, H00O42_A1230CheckList_De, H00O42_n1230CheckList_De, H00O42_A763CheckList_Descricao, H00O42_A1839Check_Codigo, H00O42_n1839Check_Codigo
               }
            }
         );
         AV15Pgmname = "CheckListGeneral";
         /* GeneXus formulas. */
         AV15Pgmname = "CheckListGeneral";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short initialized ;
      private short A1230CheckList_De ;
      private short wbEnd ;
      private short wbStart ;
      private short nDraw ;
      private short nDoneStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXWrapped ;
      private int A758CheckList_Codigo ;
      private int wcpOA758CheckList_Codigo ;
      private int AV12Check_Codigo ;
      private int A1839Check_Codigo ;
      private int AV7CheckList_Codigo ;
      private int idxLst ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String AV15Pgmname ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String GXKey ;
      private String GX_FocusControl ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String chkCheckList_Ativo_Internalname ;
      private String scmdbuf ;
      private String edtCheck_Codigo_Internalname ;
      private String edtCheckList_Descricao_Internalname ;
      private String cmbCheckList_De_Internalname ;
      private String cmbCheckList_Obrigatorio_Internalname ;
      private String sStyleString ;
      private String tblTablecontent_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String tblTableactions_Internalname ;
      private String TempTags ;
      private String bttBtnupdate_Internalname ;
      private String bttBtnupdate_Jsonclick ;
      private String bttBtndelete_Internalname ;
      private String bttBtndelete_Jsonclick ;
      private String tblTableattributes_Internalname ;
      private String lblTextblockchecklist_codigo_Internalname ;
      private String lblTextblockchecklist_codigo_Jsonclick ;
      private String edtCheckList_Codigo_Internalname ;
      private String edtCheckList_Codigo_Jsonclick ;
      private String lblTextblockcheck_codigo_Internalname ;
      private String lblTextblockcheck_codigo_Jsonclick ;
      private String edtCheck_Codigo_Jsonclick ;
      private String lblTextblockchecklist_descricao_Internalname ;
      private String lblTextblockchecklist_descricao_Jsonclick ;
      private String lblTextblockchecklist_de_Internalname ;
      private String lblTextblockchecklist_de_Jsonclick ;
      private String cmbCheckList_De_Jsonclick ;
      private String lblTextblockchecklist_obrigatorio_Internalname ;
      private String lblTextblockchecklist_obrigatorio_Jsonclick ;
      private String cmbCheckList_Obrigatorio_Jsonclick ;
      private String lblTextblockchecklist_ativo_Internalname ;
      private String lblTextblockchecklist_ativo_Jsonclick ;
      private String sCtrlA758CheckList_Codigo ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool A1845CheckList_Obrigatorio ;
      private bool A1151CheckList_Ativo ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n1230CheckList_De ;
      private bool n1845CheckList_Obrigatorio ;
      private bool n1839Check_Codigo ;
      private bool returnInSub ;
      private String A763CheckList_Descricao ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbCheckList_De ;
      private GXCombobox cmbCheckList_Obrigatorio ;
      private GXCheckbox chkCheckList_Ativo ;
      private IDataStoreProvider pr_default ;
      private int[] H00O42_A758CheckList_Codigo ;
      private bool[] H00O42_A1151CheckList_Ativo ;
      private bool[] H00O42_A1845CheckList_Obrigatorio ;
      private bool[] H00O42_n1845CheckList_Obrigatorio ;
      private short[] H00O42_A1230CheckList_De ;
      private bool[] H00O42_n1230CheckList_De ;
      private String[] H00O42_A763CheckList_Descricao ;
      private int[] H00O42_A1839Check_Codigo ;
      private bool[] H00O42_n1839Check_Codigo ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV11HTTPRequest ;
      private IGxSession AV10Session ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV8TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV9TrnContextAtt ;
   }

   public class checklistgeneral__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00O42 ;
          prmH00O42 = new Object[] {
          new Object[] {"@CheckList_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00O42", "SELECT [CheckList_Codigo], [CheckList_Ativo], [CheckList_Obrigatorio], [CheckList_De], [CheckList_Descricao], [Check_Codigo] FROM [CheckList] WITH (NOLOCK) WHERE [CheckList_Codigo] = @CheckList_Codigo ORDER BY [CheckList_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00O42,1,0,true,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.getBool(2) ;
                ((bool[]) buf[2])[0] = rslt.getBool(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((short[]) buf[4])[0] = rslt.getShort(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((String[]) buf[6])[0] = rslt.getLongVarchar(5) ;
                ((int[]) buf[7])[0] = rslt.getInt(6) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
