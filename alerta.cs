/*
               File: Alerta
        Description: Alertas
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 23:9:33.5
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class alerta : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"ALERTA_TOAREATRABALHO") == 0 )
         {
            AV11Contratada_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11Contratada_Codigo), 6, 0)));
            AV12UserId = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12UserId", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12UserId), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            GXDSAALERTA_TOAREATRABALHO4Q211( AV11Contratada_Codigo, AV12UserId) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"ALERTA_TOUSER") == 0 )
         {
            AV11Contratada_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11Contratada_Codigo), 6, 0)));
            AV13Contratante_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13Contratante_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13Contratante_Codigo), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            GXDSAALERTA_TOUSER4Q211( AV11Contratada_Codigo, AV13Contratante_Codigo) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
         {
            Gx_mode = gxfirstwebparm;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
            {
               AV7Alerta_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7Alerta_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Alerta_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vALERTA_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7Alerta_Codigo), "ZZZZZ9")));
            }
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         dynAlerta_ToAreaTrabalho.Name = "ALERTA_TOAREATRABALHO";
         dynAlerta_ToAreaTrabalho.WebTags = "";
         dynAlerta_ToUser.Name = "ALERTA_TOUSER";
         dynAlerta_ToUser.WebTags = "";
         cmbAlerta_ToGroup.Name = "ALERTA_TOGROUP";
         cmbAlerta_ToGroup.WebTags = "";
         cmbAlerta_ToGroup.addItem("0", "(Nenhum)", 0);
         cmbAlerta_ToGroup.addItem("1", "Todos os usu�rios", 0);
         cmbAlerta_ToGroup.addItem("2", "Todos os usu�rios da Contratante", 0);
         cmbAlerta_ToGroup.addItem("3", "Todos os usu�rios da Contratada", 0);
         cmbAlerta_ToGroup.addItem("4", "Todos os gestores", 0);
         cmbAlerta_ToGroup.addItem("5", "Todos os gestores da Contratante", 0);
         cmbAlerta_ToGroup.addItem("6", "Todos os gestores da Contratada", 0);
         if ( cmbAlerta_ToGroup.ItemCount > 0 )
         {
            A1884Alerta_ToGroup = (short)(NumberUtil.Val( cmbAlerta_ToGroup.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1884Alerta_ToGroup), 4, 0))), "."));
            n1884Alerta_ToGroup = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1884Alerta_ToGroup", StringUtil.LTrim( StringUtil.Str( (decimal)(A1884Alerta_ToGroup), 4, 0)));
         }
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Alertas", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = edtAlerta_Assunto_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public alerta( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public alerta( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_Gx_mode ,
                           int aP1_Alerta_Codigo )
      {
         this.Gx_mode = aP0_Gx_mode;
         this.AV7Alerta_Codigo = aP1_Alerta_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         dynAlerta_ToAreaTrabalho = new GXCombobox();
         dynAlerta_ToUser = new GXCombobox();
         cmbAlerta_ToGroup = new GXCombobox();
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
         if ( dynAlerta_ToAreaTrabalho.ItemCount > 0 )
         {
            A1882Alerta_ToAreaTrabalho = (int)(NumberUtil.Val( dynAlerta_ToAreaTrabalho.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1882Alerta_ToAreaTrabalho), 6, 0))), "."));
            n1882Alerta_ToAreaTrabalho = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1882Alerta_ToAreaTrabalho", StringUtil.LTrim( StringUtil.Str( (decimal)(A1882Alerta_ToAreaTrabalho), 6, 0)));
         }
         if ( dynAlerta_ToUser.ItemCount > 0 )
         {
            A1883Alerta_ToUser = (int)(NumberUtil.Val( dynAlerta_ToUser.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1883Alerta_ToUser), 6, 0))), "."));
            n1883Alerta_ToUser = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1883Alerta_ToUser", StringUtil.LTrim( StringUtil.Str( (decimal)(A1883Alerta_ToUser), 6, 0)));
         }
         if ( cmbAlerta_ToGroup.ItemCount > 0 )
         {
            A1884Alerta_ToGroup = (short)(NumberUtil.Val( cmbAlerta_ToGroup.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1884Alerta_ToGroup), 4, 0))), "."));
            n1884Alerta_ToGroup = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1884Alerta_ToGroup", StringUtil.LTrim( StringUtil.Str( (decimal)(A1884Alerta_ToGroup), 4, 0)));
         }
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            wb_table1_2_4Q211( true) ;
         }
         return  ;
      }

      protected void wb_table1_2_4Q211e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtAlerta_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1881Alerta_Codigo), 6, 0, ",", "")), ((edtAlerta_Codigo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1881Alerta_Codigo), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A1881Alerta_Codigo), "ZZZZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtAlerta_Codigo_Jsonclick, 0, "Attribute", "", "", "", edtAlerta_Codigo_Visible, edtAlerta_Codigo_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_Alerta.htm");
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void wb_table1_2_4Q211( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_5_4Q211( true) ;
         }
         return  ;
      }

      protected void wb_table2_5_4Q211e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_49_4Q211( true) ;
         }
         return  ;
      }

      protected void wb_table3_49_4Q211e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_4Q211e( true) ;
         }
         else
         {
            wb_table1_2_4Q211e( false) ;
         }
      }

      protected void wb_table3_49_4Q211( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 52,'',false,'',0)\"";
            ClassString = "btn btn-success";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_enter_Internalname, "", "Confirmar", bttBtn_trn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_trn_enter_Visible, bttBtn_trn_enter_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_Alerta.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 54,'',false,'',0)\"";
            ClassString = "btn btn-default";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_cancel_Internalname, "", "Fechar", bttBtn_trn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, bttBtn_trn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_Alerta.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 56,'',false,'',0)\"";
            ClassString = "btn btn-default";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_delete_Internalname, "", "Eliminar", bttBtn_trn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_trn_delete_Visible, bttBtn_trn_delete_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_Alerta.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_49_4Q211e( true) ;
         }
         else
         {
            wb_table3_49_4Q211e( false) ;
         }
      }

      protected void wb_table2_5_4Q211( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "TableContent", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"Body"+"\" style=\"display:none;\">") ;
            wb_table4_13_4Q211( true) ;
         }
         return  ;
      }

      protected void wb_table4_13_4Q211e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_4Q211e( true) ;
         }
         else
         {
            wb_table2_5_4Q211e( false) ;
         }
      }

      protected void wb_table4_13_4Q211( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "Table", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockalerta_assunto_Internalname, "Assunto", "", "", lblTextblockalerta_assunto_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Alerta.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"3\"  class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtAlerta_Assunto_Internalname, A1892Alerta_Assunto, StringUtil.RTrim( context.localUtil.Format( A1892Alerta_Assunto, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,18);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtAlerta_Assunto_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtAlerta_Assunto_Enabled, 0, "text", "", 80, "chr", 1, "row", 80, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_Alerta.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockalerta_descricao_Internalname, "Descri��o", "", "", lblTextblockalerta_descricao_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Alerta.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"3\"  class='DataContentCell'>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"ALERTA_DESCRICAOContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockalerta_toareatrabalho_Internalname, "�rea de Trabalho", "", "", lblTextblockalerta_toareatrabalho_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Alerta.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td colspan=\"3\"  class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 28,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynAlerta_ToAreaTrabalho, dynAlerta_ToAreaTrabalho_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A1882Alerta_ToAreaTrabalho), 6, 0)), 1, dynAlerta_ToAreaTrabalho_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, dynAlerta_ToAreaTrabalho.Enabled, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,28);\"", "", true, "HLP_Alerta.htm");
            dynAlerta_ToAreaTrabalho.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A1882Alerta_ToAreaTrabalho), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynAlerta_ToAreaTrabalho_Internalname, "Values", (String)(dynAlerta_ToAreaTrabalho.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockalerta_touser_Internalname, "Usu�rio", "", "", lblTextblockalerta_touser_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Alerta.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 33,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynAlerta_ToUser, dynAlerta_ToUser_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A1883Alerta_ToUser), 6, 0)), 1, dynAlerta_ToUser_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, dynAlerta_ToUser.Enabled, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,33);\"", "", true, "HLP_Alerta.htm");
            dynAlerta_ToUser.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A1883Alerta_ToUser), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynAlerta_ToUser_Internalname, "Values", (String)(dynAlerta_ToUser.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockalerta_togroup_Internalname, "Grupo", "", "", lblTextblockalerta_togroup_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Alerta.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 37,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbAlerta_ToGroup, cmbAlerta_ToGroup_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A1884Alerta_ToGroup), 4, 0)), 1, cmbAlerta_ToGroup_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, cmbAlerta_ToGroup.Enabled, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,37);\"", "", true, "HLP_Alerta.htm");
            cmbAlerta_ToGroup.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A1884Alerta_ToGroup), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbAlerta_ToGroup_Internalname, "Values", (String)(cmbAlerta_ToGroup.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockalerta_inicio_Internalname, "Inicio", "", "", lblTextblockalerta_inicio_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Alerta.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 42,'',false,'',0)\"";
            context.WriteHtmlText( "<div id=\""+edtAlerta_Inicio_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtAlerta_Inicio_Internalname, context.localUtil.Format(A1885Alerta_Inicio, "99/99/99"), context.localUtil.Format( A1885Alerta_Inicio, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,42);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtAlerta_Inicio_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, edtAlerta_Inicio_Enabled, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_Alerta.htm");
            GxWebStd.gx_bitmap( context, edtAlerta_Inicio_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(edtAlerta_Inicio_Enabled==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_Alerta.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockalerta_fim_Internalname, "Fim", "", "", lblTextblockalerta_fim_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_Alerta.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 46,'',false,'',0)\"";
            context.WriteHtmlText( "<div id=\""+edtAlerta_Fim_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtAlerta_Fim_Internalname, context.localUtil.Format(A1886Alerta_Fim, "99/99/99"), context.localUtil.Format( A1886Alerta_Fim, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,46);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtAlerta_Fim_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, edtAlerta_Fim_Enabled, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_Alerta.htm");
            GxWebStd.gx_bitmap( context, edtAlerta_Fim_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(edtAlerta_Fim_Enabled==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_Alerta.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_13_4Q211e( true) ;
         }
         else
         {
            wb_table4_13_4Q211e( false) ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E114Q2 */
         E114Q2 ();
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               A1892Alerta_Assunto = cgiGet( edtAlerta_Assunto_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1892Alerta_Assunto", A1892Alerta_Assunto);
               dynAlerta_ToAreaTrabalho.CurrentValue = cgiGet( dynAlerta_ToAreaTrabalho_Internalname);
               A1882Alerta_ToAreaTrabalho = (int)(NumberUtil.Val( cgiGet( dynAlerta_ToAreaTrabalho_Internalname), "."));
               n1882Alerta_ToAreaTrabalho = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1882Alerta_ToAreaTrabalho", StringUtil.LTrim( StringUtil.Str( (decimal)(A1882Alerta_ToAreaTrabalho), 6, 0)));
               n1882Alerta_ToAreaTrabalho = ((0==A1882Alerta_ToAreaTrabalho) ? true : false);
               dynAlerta_ToUser.CurrentValue = cgiGet( dynAlerta_ToUser_Internalname);
               A1883Alerta_ToUser = (int)(NumberUtil.Val( cgiGet( dynAlerta_ToUser_Internalname), "."));
               n1883Alerta_ToUser = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1883Alerta_ToUser", StringUtil.LTrim( StringUtil.Str( (decimal)(A1883Alerta_ToUser), 6, 0)));
               n1883Alerta_ToUser = ((0==A1883Alerta_ToUser) ? true : false);
               cmbAlerta_ToGroup.CurrentValue = cgiGet( cmbAlerta_ToGroup_Internalname);
               A1884Alerta_ToGroup = (short)(NumberUtil.Val( cgiGet( cmbAlerta_ToGroup_Internalname), "."));
               n1884Alerta_ToGroup = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1884Alerta_ToGroup", StringUtil.LTrim( StringUtil.Str( (decimal)(A1884Alerta_ToGroup), 4, 0)));
               n1884Alerta_ToGroup = ((0==A1884Alerta_ToGroup) ? true : false);
               if ( context.localUtil.VCDate( cgiGet( edtAlerta_Inicio_Internalname), 2) == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Inicio"}), 1, "ALERTA_INICIO");
                  AnyError = 1;
                  GX_FocusControl = edtAlerta_Inicio_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1885Alerta_Inicio = DateTime.MinValue;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1885Alerta_Inicio", context.localUtil.Format(A1885Alerta_Inicio, "99/99/99"));
               }
               else
               {
                  A1885Alerta_Inicio = context.localUtil.CToD( cgiGet( edtAlerta_Inicio_Internalname), 2);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1885Alerta_Inicio", context.localUtil.Format(A1885Alerta_Inicio, "99/99/99"));
               }
               if ( context.localUtil.VCDate( cgiGet( edtAlerta_Fim_Internalname), 2) == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Fim"}), 1, "ALERTA_FIM");
                  AnyError = 1;
                  GX_FocusControl = edtAlerta_Fim_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1886Alerta_Fim = DateTime.MinValue;
                  n1886Alerta_Fim = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1886Alerta_Fim", context.localUtil.Format(A1886Alerta_Fim, "99/99/99"));
               }
               else
               {
                  A1886Alerta_Fim = context.localUtil.CToD( cgiGet( edtAlerta_Fim_Internalname), 2);
                  n1886Alerta_Fim = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1886Alerta_Fim", context.localUtil.Format(A1886Alerta_Fim, "99/99/99"));
               }
               n1886Alerta_Fim = ((DateTime.MinValue==A1886Alerta_Fim) ? true : false);
               A1881Alerta_Codigo = (int)(context.localUtil.CToN( cgiGet( edtAlerta_Codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1881Alerta_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1881Alerta_Codigo), 6, 0)));
               /* Read saved values. */
               Z1881Alerta_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z1881Alerta_Codigo"), ",", "."));
               Z1892Alerta_Assunto = cgiGet( "Z1892Alerta_Assunto");
               Z1882Alerta_ToAreaTrabalho = (int)(context.localUtil.CToN( cgiGet( "Z1882Alerta_ToAreaTrabalho"), ",", "."));
               n1882Alerta_ToAreaTrabalho = ((0==A1882Alerta_ToAreaTrabalho) ? true : false);
               Z1883Alerta_ToUser = (int)(context.localUtil.CToN( cgiGet( "Z1883Alerta_ToUser"), ",", "."));
               n1883Alerta_ToUser = ((0==A1883Alerta_ToUser) ? true : false);
               Z1884Alerta_ToGroup = (short)(context.localUtil.CToN( cgiGet( "Z1884Alerta_ToGroup"), ",", "."));
               n1884Alerta_ToGroup = ((0==A1884Alerta_ToGroup) ? true : false);
               Z1885Alerta_Inicio = context.localUtil.CToD( cgiGet( "Z1885Alerta_Inicio"), 0);
               Z1886Alerta_Fim = context.localUtil.CToD( cgiGet( "Z1886Alerta_Fim"), 0);
               n1886Alerta_Fim = ((DateTime.MinValue==A1886Alerta_Fim) ? true : false);
               Z1887Alerta_Owner = (int)(context.localUtil.CToN( cgiGet( "Z1887Alerta_Owner"), ",", "."));
               Z1888Alerta_Cadastro = context.localUtil.CToT( cgiGet( "Z1888Alerta_Cadastro"), 0);
               A1887Alerta_Owner = (int)(context.localUtil.CToN( cgiGet( "Z1887Alerta_Owner"), ",", "."));
               A1888Alerta_Cadastro = context.localUtil.CToT( cgiGet( "Z1888Alerta_Cadastro"), 0);
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
               Gx_mode = cgiGet( "Mode");
               AV7Alerta_Codigo = (int)(context.localUtil.CToN( cgiGet( "vALERTA_CODIGO"), ",", "."));
               Gx_BScreen = (short)(context.localUtil.CToN( cgiGet( "vGXBSCREEN"), ",", "."));
               A1888Alerta_Cadastro = context.localUtil.CToT( cgiGet( "ALERTA_CADASTRO"), 0);
               A1887Alerta_Owner = (int)(context.localUtil.CToN( cgiGet( "ALERTA_OWNER"), ",", "."));
               AV11Contratada_Codigo = (int)(context.localUtil.CToN( cgiGet( "vCONTRATADA_CODIGO"), ",", "."));
               AV12UserId = (int)(context.localUtil.CToN( cgiGet( "vUSERID"), ",", "."));
               AV13Contratante_Codigo = (int)(context.localUtil.CToN( cgiGet( "vCONTRATANTE_CODIGO"), ",", "."));
               A1893Alerta_Descricao = cgiGet( "ALERTA_DESCRICAO");
               Gx_mode = cgiGet( "vMODE");
               Alerta_descricao_Width = cgiGet( "ALERTA_DESCRICAO_Width");
               Alerta_descricao_Height = cgiGet( "ALERTA_DESCRICAO_Height");
               Alerta_descricao_Skin = cgiGet( "ALERTA_DESCRICAO_Skin");
               Alerta_descricao_Toolbar = cgiGet( "ALERTA_DESCRICAO_Toolbar");
               Alerta_descricao_Color = (int)(context.localUtil.CToN( cgiGet( "ALERTA_DESCRICAO_Color"), ",", "."));
               Alerta_descricao_Enabled = StringUtil.StrToBool( cgiGet( "ALERTA_DESCRICAO_Enabled"));
               Alerta_descricao_Class = cgiGet( "ALERTA_DESCRICAO_Class");
               Alerta_descricao_Customtoolbar = cgiGet( "ALERTA_DESCRICAO_Customtoolbar");
               Alerta_descricao_Customconfiguration = cgiGet( "ALERTA_DESCRICAO_Customconfiguration");
               Alerta_descricao_Toolbarcancollapse = StringUtil.StrToBool( cgiGet( "ALERTA_DESCRICAO_Toolbarcancollapse"));
               Alerta_descricao_Toolbarexpanded = StringUtil.StrToBool( cgiGet( "ALERTA_DESCRICAO_Toolbarexpanded"));
               Alerta_descricao_Buttonpressedid = cgiGet( "ALERTA_DESCRICAO_Buttonpressedid");
               Alerta_descricao_Captionvalue = cgiGet( "ALERTA_DESCRICAO_Captionvalue");
               Alerta_descricao_Captionclass = cgiGet( "ALERTA_DESCRICAO_Captionclass");
               Alerta_descricao_Captionposition = cgiGet( "ALERTA_DESCRICAO_Captionposition");
               Alerta_descricao_Coltitle = cgiGet( "ALERTA_DESCRICAO_Coltitle");
               Alerta_descricao_Coltitlefont = cgiGet( "ALERTA_DESCRICAO_Coltitlefont");
               Alerta_descricao_Coltitlecolor = (int)(context.localUtil.CToN( cgiGet( "ALERTA_DESCRICAO_Coltitlecolor"), ",", "."));
               Alerta_descricao_Usercontroliscolumn = StringUtil.StrToBool( cgiGet( "ALERTA_DESCRICAO_Usercontroliscolumn"));
               Alerta_descricao_Visible = StringUtil.StrToBool( cgiGet( "ALERTA_DESCRICAO_Visible"));
               Dvpanel_tableattributes_Width = cgiGet( "DVPANEL_TABLEATTRIBUTES_Width");
               Dvpanel_tableattributes_Height = cgiGet( "DVPANEL_TABLEATTRIBUTES_Height");
               Dvpanel_tableattributes_Cls = cgiGet( "DVPANEL_TABLEATTRIBUTES_Cls");
               Dvpanel_tableattributes_Title = cgiGet( "DVPANEL_TABLEATTRIBUTES_Title");
               Dvpanel_tableattributes_Collapsible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsible"));
               Dvpanel_tableattributes_Collapsed = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsed"));
               Dvpanel_tableattributes_Enabled = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Enabled"));
               Dvpanel_tableattributes_Class = cgiGet( "DVPANEL_TABLEATTRIBUTES_Class");
               Dvpanel_tableattributes_Autowidth = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autowidth"));
               Dvpanel_tableattributes_Autoheight = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoheight"));
               Dvpanel_tableattributes_Showheader = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showheader"));
               Dvpanel_tableattributes_Showcollapseicon = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showcollapseicon"));
               Dvpanel_tableattributes_Iconposition = cgiGet( "DVPANEL_TABLEATTRIBUTES_Iconposition");
               Dvpanel_tableattributes_Autoscroll = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoscroll"));
               Dvpanel_tableattributes_Visible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Visible"));
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               forbiddenHiddens = "hsh" + "Alerta";
               A1881Alerta_Codigo = (int)(context.localUtil.CToN( cgiGet( edtAlerta_Codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1881Alerta_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1881Alerta_Codigo), 6, 0)));
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A1881Alerta_Codigo), "ZZZZZ9");
               forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A1887Alerta_Owner), "ZZZZZ9");
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( A1888Alerta_Cadastro, "99/99/99 99:99");
               hsh = cgiGet( "hsh");
               if ( ( ! ( ( A1881Alerta_Codigo != Z1881Alerta_Codigo ) ) || ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ) && ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
               {
                  GXUtil.WriteLog("alerta:[SecurityCheckFailed value for]"+"Alerta_Codigo:"+context.localUtil.Format( (decimal)(A1881Alerta_Codigo), "ZZZZZ9"));
                  GXUtil.WriteLog("alerta:[SecurityCheckFailed value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
                  GXUtil.WriteLog("alerta:[SecurityCheckFailed value for]"+"Alerta_Owner:"+context.localUtil.Format( (decimal)(A1887Alerta_Owner), "ZZZZZ9"));
                  GXUtil.WriteLog("alerta:[SecurityCheckFailed value for]"+"Alerta_Cadastro:"+context.localUtil.Format( A1888Alerta_Cadastro, "99/99/99 99:99"));
                  GxWebError = 1;
                  context.HttpContext.Response.StatusDescription = 403.ToString();
                  context.HttpContext.Response.StatusCode = 403;
                  context.WriteHtmlText( "<title>403 Forbidden</title>") ;
                  context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
                  context.WriteHtmlText( "<p /><hr />") ;
                  GXUtil.WriteLog("send_http_error_code " + 403.ToString());
                  AnyError = 1;
                  return  ;
               }
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  A1881Alerta_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1881Alerta_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1881Alerta_Codigo), 6, 0)));
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  disable_std_buttons( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
                  {
                     sMode211 = Gx_mode;
                     Gx_mode = "UPD";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                     Gx_mode = sMode211;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  }
                  standaloneModal( ) ;
                  if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
                  {
                     getByPrimaryKey( ) ;
                     if ( RcdFound211 == 1 )
                     {
                        if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
                        {
                           /* Confirm record */
                           CONFIRM_4Q0( ) ;
                           if ( AnyError == 0 )
                           {
                              GX_FocusControl = bttBtn_trn_enter_Internalname;
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noinsert", ""), 1, "ALERTA_CODIGO");
                        AnyError = 1;
                        GX_FocusControl = edtAlerta_Codigo_Internalname;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E114Q2 */
                           E114Q2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "AFTER TRN") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E124Q2 */
                           E124Q2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           if ( StringUtil.StrCmp(Gx_mode, "DSP") != 0 )
                           {
                              btn_enter( ) ;
                           }
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            /* Execute user event: E124Q2 */
            E124Q2 ();
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll4Q211( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         bttBtn_trn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
         if ( ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            bttBtn_trn_delete_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
            if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
            {
               bttBtn_trn_enter_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Visible), 5, 0)));
            }
            DisableAttributes4Q211( ) ;
         }
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void CONFIRM_4Q0( )
      {
         BeforeValidate4Q211( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls4Q211( ) ;
            }
            else
            {
               CheckExtendedTable4Q211( ) ;
               CloseExtendedTableCursors4Q211( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         }
      }

      protected void ResetCaption4Q0( )
      {
      }

      protected void E114Q2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV8WWPContext) ;
         AV9TrnContext.FromXml(AV10WebSession.Get("TrnContext"), "");
         edtAlerta_Codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtAlerta_Codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtAlerta_Codigo_Visible), 5, 0)));
         AV11Contratada_Codigo = AV8WWPContext.gxTpr_Contratada_codigo;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11Contratada_Codigo), 6, 0)));
         AV13Contratante_Codigo = AV8WWPContext.gxTpr_Contratante_codigo;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13Contratante_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13Contratante_Codigo), 6, 0)));
         AV12UserId = AV8WWPContext.gxTpr_Userid;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12UserId", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12UserId), 6, 0)));
      }

      protected void E124Q2( )
      {
         /* After Trn Routine */
         if ( ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) && ! AV9TrnContext.gxTpr_Callerondelete )
         {
            context.wjLoc = formatLink("wwalerta.aspx") ;
            context.wjLocDisableFrm = 1;
         }
         context.setWebReturnParms(new Object[] {});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void ZM4Q211( short GX_JID )
      {
         if ( ( GX_JID == 10 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z1892Alerta_Assunto = T004Q3_A1892Alerta_Assunto[0];
               Z1882Alerta_ToAreaTrabalho = T004Q3_A1882Alerta_ToAreaTrabalho[0];
               Z1883Alerta_ToUser = T004Q3_A1883Alerta_ToUser[0];
               Z1884Alerta_ToGroup = T004Q3_A1884Alerta_ToGroup[0];
               Z1885Alerta_Inicio = T004Q3_A1885Alerta_Inicio[0];
               Z1886Alerta_Fim = T004Q3_A1886Alerta_Fim[0];
               Z1887Alerta_Owner = T004Q3_A1887Alerta_Owner[0];
               Z1888Alerta_Cadastro = T004Q3_A1888Alerta_Cadastro[0];
            }
            else
            {
               Z1892Alerta_Assunto = A1892Alerta_Assunto;
               Z1882Alerta_ToAreaTrabalho = A1882Alerta_ToAreaTrabalho;
               Z1883Alerta_ToUser = A1883Alerta_ToUser;
               Z1884Alerta_ToGroup = A1884Alerta_ToGroup;
               Z1885Alerta_Inicio = A1885Alerta_Inicio;
               Z1886Alerta_Fim = A1886Alerta_Fim;
               Z1887Alerta_Owner = A1887Alerta_Owner;
               Z1888Alerta_Cadastro = A1888Alerta_Cadastro;
            }
         }
         if ( GX_JID == -10 )
         {
            Z1881Alerta_Codigo = A1881Alerta_Codigo;
            Z1892Alerta_Assunto = A1892Alerta_Assunto;
            Z1893Alerta_Descricao = A1893Alerta_Descricao;
            Z1882Alerta_ToAreaTrabalho = A1882Alerta_ToAreaTrabalho;
            Z1883Alerta_ToUser = A1883Alerta_ToUser;
            Z1884Alerta_ToGroup = A1884Alerta_ToGroup;
            Z1885Alerta_Inicio = A1885Alerta_Inicio;
            Z1886Alerta_Fim = A1886Alerta_Fim;
            Z1887Alerta_Owner = A1887Alerta_Owner;
            Z1888Alerta_Cadastro = A1888Alerta_Cadastro;
         }
      }

      protected void standaloneNotModal( )
      {
         edtAlerta_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtAlerta_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtAlerta_Codigo_Enabled), 5, 0)));
         Gx_BScreen = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_BScreen", StringUtil.Str( (decimal)(Gx_BScreen), 1, 0));
         edtAlerta_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtAlerta_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtAlerta_Codigo_Enabled), 5, 0)));
         bttBtn_trn_delete_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Enabled), 5, 0)));
         if ( ! (0==AV7Alerta_Codigo) )
         {
            A1881Alerta_Codigo = AV7Alerta_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1881Alerta_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1881Alerta_Codigo), 6, 0)));
         }
         GXAALERTA_TOUSER_html4Q211( AV11Contratada_Codigo, AV13Contratante_Codigo) ;
         GXAALERTA_TOAREATRABALHO_html4Q211( AV11Contratada_Codigo, AV12UserId) ;
      }

      protected void standaloneModal( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            bttBtn_trn_enter_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         else
         {
            bttBtn_trn_enter_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (DateTime.MinValue==A1888Alerta_Cadastro) && ( Gx_BScreen == 0 ) )
         {
            A1888Alerta_Cadastro = DateTimeUtil.ServerNow( context, "DEFAULT");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1888Alerta_Cadastro", context.localUtil.TToC( A1888Alerta_Cadastro, 8, 5, 0, 3, "/", ":", " "));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (0==A1887Alerta_Owner) && ( Gx_BScreen == 0 ) )
         {
            A1887Alerta_Owner = AV8WWPContext.gxTpr_Userid;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1887Alerta_Owner", StringUtil.LTrim( StringUtil.Str( (decimal)(A1887Alerta_Owner), 6, 0)));
         }
      }

      protected void Load4Q211( )
      {
         /* Using cursor T004Q4 */
         pr_default.execute(2, new Object[] {A1881Alerta_Codigo});
         if ( (pr_default.getStatus(2) != 101) )
         {
            RcdFound211 = 1;
            A1892Alerta_Assunto = T004Q4_A1892Alerta_Assunto[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1892Alerta_Assunto", A1892Alerta_Assunto);
            A1893Alerta_Descricao = T004Q4_A1893Alerta_Descricao[0];
            A1882Alerta_ToAreaTrabalho = T004Q4_A1882Alerta_ToAreaTrabalho[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1882Alerta_ToAreaTrabalho", StringUtil.LTrim( StringUtil.Str( (decimal)(A1882Alerta_ToAreaTrabalho), 6, 0)));
            n1882Alerta_ToAreaTrabalho = T004Q4_n1882Alerta_ToAreaTrabalho[0];
            A1883Alerta_ToUser = T004Q4_A1883Alerta_ToUser[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1883Alerta_ToUser", StringUtil.LTrim( StringUtil.Str( (decimal)(A1883Alerta_ToUser), 6, 0)));
            n1883Alerta_ToUser = T004Q4_n1883Alerta_ToUser[0];
            A1884Alerta_ToGroup = T004Q4_A1884Alerta_ToGroup[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1884Alerta_ToGroup", StringUtil.LTrim( StringUtil.Str( (decimal)(A1884Alerta_ToGroup), 4, 0)));
            n1884Alerta_ToGroup = T004Q4_n1884Alerta_ToGroup[0];
            A1885Alerta_Inicio = T004Q4_A1885Alerta_Inicio[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1885Alerta_Inicio", context.localUtil.Format(A1885Alerta_Inicio, "99/99/99"));
            A1886Alerta_Fim = T004Q4_A1886Alerta_Fim[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1886Alerta_Fim", context.localUtil.Format(A1886Alerta_Fim, "99/99/99"));
            n1886Alerta_Fim = T004Q4_n1886Alerta_Fim[0];
            A1887Alerta_Owner = T004Q4_A1887Alerta_Owner[0];
            A1888Alerta_Cadastro = T004Q4_A1888Alerta_Cadastro[0];
            ZM4Q211( -10) ;
         }
         pr_default.close(2);
         OnLoadActions4Q211( ) ;
      }

      protected void OnLoadActions4Q211( )
      {
      }

      protected void CheckExtendedTable4Q211( )
      {
         Gx_BScreen = 1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_BScreen", StringUtil.Str( (decimal)(Gx_BScreen), 1, 0));
         standaloneModal( ) ;
         if ( ! ( ( A1884Alerta_ToGroup == 0 ) || ( A1884Alerta_ToGroup == 1 ) || ( A1884Alerta_ToGroup == 2 ) || ( A1884Alerta_ToGroup == 3 ) || ( A1884Alerta_ToGroup == 4 ) || ( A1884Alerta_ToGroup == 5 ) || ( A1884Alerta_ToGroup == 6 ) || (0==A1884Alerta_ToGroup) ) )
         {
            GX_msglist.addItem("Campo Grupo fora do intervalo", "OutOfRange", 1, "ALERTA_TOGROUP");
            AnyError = 1;
            GX_FocusControl = cmbAlerta_ToGroup_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( ! ( (DateTime.MinValue==A1885Alerta_Inicio) || ( A1885Alerta_Inicio >= context.localUtil.YMDToD( 1753, 1, 1) ) ) )
         {
            GX_msglist.addItem("Campo Inicio fora do intervalo", "OutOfRange", 1, "ALERTA_INICIO");
            AnyError = 1;
            GX_FocusControl = edtAlerta_Inicio_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( ! ( (DateTime.MinValue==A1886Alerta_Fim) || ( A1886Alerta_Fim >= context.localUtil.YMDToD( 1753, 1, 1) ) ) )
         {
            GX_msglist.addItem("Campo Fim fora do intervalo", "OutOfRange", 1, "ALERTA_FIM");
            AnyError = 1;
            GX_FocusControl = edtAlerta_Fim_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
      }

      protected void CloseExtendedTableCursors4Q211( )
      {
      }

      protected void enableDisable( )
      {
      }

      protected void GetKey4Q211( )
      {
         /* Using cursor T004Q5 */
         pr_default.execute(3, new Object[] {A1881Alerta_Codigo});
         if ( (pr_default.getStatus(3) != 101) )
         {
            RcdFound211 = 1;
         }
         else
         {
            RcdFound211 = 0;
         }
         pr_default.close(3);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T004Q3 */
         pr_default.execute(1, new Object[] {A1881Alerta_Codigo});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM4Q211( 10) ;
            RcdFound211 = 1;
            A1881Alerta_Codigo = T004Q3_A1881Alerta_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1881Alerta_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1881Alerta_Codigo), 6, 0)));
            A1892Alerta_Assunto = T004Q3_A1892Alerta_Assunto[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1892Alerta_Assunto", A1892Alerta_Assunto);
            A1893Alerta_Descricao = T004Q3_A1893Alerta_Descricao[0];
            A1882Alerta_ToAreaTrabalho = T004Q3_A1882Alerta_ToAreaTrabalho[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1882Alerta_ToAreaTrabalho", StringUtil.LTrim( StringUtil.Str( (decimal)(A1882Alerta_ToAreaTrabalho), 6, 0)));
            n1882Alerta_ToAreaTrabalho = T004Q3_n1882Alerta_ToAreaTrabalho[0];
            A1883Alerta_ToUser = T004Q3_A1883Alerta_ToUser[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1883Alerta_ToUser", StringUtil.LTrim( StringUtil.Str( (decimal)(A1883Alerta_ToUser), 6, 0)));
            n1883Alerta_ToUser = T004Q3_n1883Alerta_ToUser[0];
            A1884Alerta_ToGroup = T004Q3_A1884Alerta_ToGroup[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1884Alerta_ToGroup", StringUtil.LTrim( StringUtil.Str( (decimal)(A1884Alerta_ToGroup), 4, 0)));
            n1884Alerta_ToGroup = T004Q3_n1884Alerta_ToGroup[0];
            A1885Alerta_Inicio = T004Q3_A1885Alerta_Inicio[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1885Alerta_Inicio", context.localUtil.Format(A1885Alerta_Inicio, "99/99/99"));
            A1886Alerta_Fim = T004Q3_A1886Alerta_Fim[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1886Alerta_Fim", context.localUtil.Format(A1886Alerta_Fim, "99/99/99"));
            n1886Alerta_Fim = T004Q3_n1886Alerta_Fim[0];
            A1887Alerta_Owner = T004Q3_A1887Alerta_Owner[0];
            A1888Alerta_Cadastro = T004Q3_A1888Alerta_Cadastro[0];
            Z1881Alerta_Codigo = A1881Alerta_Codigo;
            sMode211 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            Load4Q211( ) ;
            if ( AnyError == 1 )
            {
               RcdFound211 = 0;
               InitializeNonKey4Q211( ) ;
            }
            Gx_mode = sMode211;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         else
         {
            RcdFound211 = 0;
            InitializeNonKey4Q211( ) ;
            sMode211 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            standaloneModal( ) ;
            Gx_mode = sMode211;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey4Q211( ) ;
         if ( RcdFound211 == 0 )
         {
         }
         else
         {
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound211 = 0;
         /* Using cursor T004Q6 */
         pr_default.execute(4, new Object[] {A1881Alerta_Codigo});
         if ( (pr_default.getStatus(4) != 101) )
         {
            while ( (pr_default.getStatus(4) != 101) && ( ( T004Q6_A1881Alerta_Codigo[0] < A1881Alerta_Codigo ) ) )
            {
               pr_default.readNext(4);
            }
            if ( (pr_default.getStatus(4) != 101) && ( ( T004Q6_A1881Alerta_Codigo[0] > A1881Alerta_Codigo ) ) )
            {
               A1881Alerta_Codigo = T004Q6_A1881Alerta_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1881Alerta_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1881Alerta_Codigo), 6, 0)));
               RcdFound211 = 1;
            }
         }
         pr_default.close(4);
      }

      protected void move_previous( )
      {
         RcdFound211 = 0;
         /* Using cursor T004Q7 */
         pr_default.execute(5, new Object[] {A1881Alerta_Codigo});
         if ( (pr_default.getStatus(5) != 101) )
         {
            while ( (pr_default.getStatus(5) != 101) && ( ( T004Q7_A1881Alerta_Codigo[0] > A1881Alerta_Codigo ) ) )
            {
               pr_default.readNext(5);
            }
            if ( (pr_default.getStatus(5) != 101) && ( ( T004Q7_A1881Alerta_Codigo[0] < A1881Alerta_Codigo ) ) )
            {
               A1881Alerta_Codigo = T004Q7_A1881Alerta_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1881Alerta_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1881Alerta_Codigo), 6, 0)));
               RcdFound211 = 1;
            }
         }
         pr_default.close(5);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey4Q211( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = edtAlerta_Assunto_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert4Q211( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound211 == 1 )
            {
               if ( A1881Alerta_Codigo != Z1881Alerta_Codigo )
               {
                  A1881Alerta_Codigo = Z1881Alerta_Codigo;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1881Alerta_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1881Alerta_Codigo), 6, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "ALERTA_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtAlerta_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = edtAlerta_Assunto_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  /* Update record */
                  Update4Q211( ) ;
                  GX_FocusControl = edtAlerta_Assunto_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( A1881Alerta_Codigo != Z1881Alerta_Codigo )
               {
                  /* Insert record */
                  GX_FocusControl = edtAlerta_Assunto_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert4Q211( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "ALERTA_CODIGO");
                     AnyError = 1;
                     GX_FocusControl = edtAlerta_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     /* Insert record */
                     GX_FocusControl = edtAlerta_Assunto_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert4Q211( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            if ( AnyError == 0 )
            {
               context.nUserReturn = 1;
            }
         }
      }

      protected void btn_delete( )
      {
         if ( A1881Alerta_Codigo != Z1881Alerta_Codigo )
         {
            A1881Alerta_Codigo = Z1881Alerta_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1881Alerta_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1881Alerta_Codigo), 6, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "ALERTA_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtAlerta_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = edtAlerta_Assunto_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
         }
      }

      protected void CheckOptimisticConcurrency4Q211( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T004Q2 */
            pr_default.execute(0, new Object[] {A1881Alerta_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Alerta"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            Gx_longc = false;
            if ( (pr_default.getStatus(0) == 101) || ( StringUtil.StrCmp(Z1892Alerta_Assunto, T004Q2_A1892Alerta_Assunto[0]) != 0 ) || ( Z1882Alerta_ToAreaTrabalho != T004Q2_A1882Alerta_ToAreaTrabalho[0] ) || ( Z1883Alerta_ToUser != T004Q2_A1883Alerta_ToUser[0] ) || ( Z1884Alerta_ToGroup != T004Q2_A1884Alerta_ToGroup[0] ) || ( Z1885Alerta_Inicio != T004Q2_A1885Alerta_Inicio[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z1886Alerta_Fim != T004Q2_A1886Alerta_Fim[0] ) || ( Z1887Alerta_Owner != T004Q2_A1887Alerta_Owner[0] ) || ( Z1888Alerta_Cadastro != T004Q2_A1888Alerta_Cadastro[0] ) )
            {
               if ( StringUtil.StrCmp(Z1892Alerta_Assunto, T004Q2_A1892Alerta_Assunto[0]) != 0 )
               {
                  GXUtil.WriteLog("alerta:[seudo value changed for attri]"+"Alerta_Assunto");
                  GXUtil.WriteLogRaw("Old: ",Z1892Alerta_Assunto);
                  GXUtil.WriteLogRaw("Current: ",T004Q2_A1892Alerta_Assunto[0]);
               }
               if ( Z1882Alerta_ToAreaTrabalho != T004Q2_A1882Alerta_ToAreaTrabalho[0] )
               {
                  GXUtil.WriteLog("alerta:[seudo value changed for attri]"+"Alerta_ToAreaTrabalho");
                  GXUtil.WriteLogRaw("Old: ",Z1882Alerta_ToAreaTrabalho);
                  GXUtil.WriteLogRaw("Current: ",T004Q2_A1882Alerta_ToAreaTrabalho[0]);
               }
               if ( Z1883Alerta_ToUser != T004Q2_A1883Alerta_ToUser[0] )
               {
                  GXUtil.WriteLog("alerta:[seudo value changed for attri]"+"Alerta_ToUser");
                  GXUtil.WriteLogRaw("Old: ",Z1883Alerta_ToUser);
                  GXUtil.WriteLogRaw("Current: ",T004Q2_A1883Alerta_ToUser[0]);
               }
               if ( Z1884Alerta_ToGroup != T004Q2_A1884Alerta_ToGroup[0] )
               {
                  GXUtil.WriteLog("alerta:[seudo value changed for attri]"+"Alerta_ToGroup");
                  GXUtil.WriteLogRaw("Old: ",Z1884Alerta_ToGroup);
                  GXUtil.WriteLogRaw("Current: ",T004Q2_A1884Alerta_ToGroup[0]);
               }
               if ( Z1885Alerta_Inicio != T004Q2_A1885Alerta_Inicio[0] )
               {
                  GXUtil.WriteLog("alerta:[seudo value changed for attri]"+"Alerta_Inicio");
                  GXUtil.WriteLogRaw("Old: ",Z1885Alerta_Inicio);
                  GXUtil.WriteLogRaw("Current: ",T004Q2_A1885Alerta_Inicio[0]);
               }
               if ( Z1886Alerta_Fim != T004Q2_A1886Alerta_Fim[0] )
               {
                  GXUtil.WriteLog("alerta:[seudo value changed for attri]"+"Alerta_Fim");
                  GXUtil.WriteLogRaw("Old: ",Z1886Alerta_Fim);
                  GXUtil.WriteLogRaw("Current: ",T004Q2_A1886Alerta_Fim[0]);
               }
               if ( Z1887Alerta_Owner != T004Q2_A1887Alerta_Owner[0] )
               {
                  GXUtil.WriteLog("alerta:[seudo value changed for attri]"+"Alerta_Owner");
                  GXUtil.WriteLogRaw("Old: ",Z1887Alerta_Owner);
                  GXUtil.WriteLogRaw("Current: ",T004Q2_A1887Alerta_Owner[0]);
               }
               if ( Z1888Alerta_Cadastro != T004Q2_A1888Alerta_Cadastro[0] )
               {
                  GXUtil.WriteLog("alerta:[seudo value changed for attri]"+"Alerta_Cadastro");
                  GXUtil.WriteLogRaw("Old: ",Z1888Alerta_Cadastro);
                  GXUtil.WriteLogRaw("Current: ",T004Q2_A1888Alerta_Cadastro[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"Alerta"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert4Q211( )
      {
         BeforeValidate4Q211( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable4Q211( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM4Q211( 0) ;
            CheckOptimisticConcurrency4Q211( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm4Q211( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert4Q211( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T004Q8 */
                     pr_default.execute(6, new Object[] {A1892Alerta_Assunto, A1893Alerta_Descricao, n1882Alerta_ToAreaTrabalho, A1882Alerta_ToAreaTrabalho, n1883Alerta_ToUser, A1883Alerta_ToUser, n1884Alerta_ToGroup, A1884Alerta_ToGroup, A1885Alerta_Inicio, n1886Alerta_Fim, A1886Alerta_Fim, A1887Alerta_Owner, A1888Alerta_Cadastro});
                     A1881Alerta_Codigo = T004Q8_A1881Alerta_Codigo[0];
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1881Alerta_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1881Alerta_Codigo), 6, 0)));
                     pr_default.close(6);
                     dsDefault.SmartCacheProvider.SetUpdated("Alerta") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                           ResetCaption4Q0( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load4Q211( ) ;
            }
            EndLevel4Q211( ) ;
         }
         CloseExtendedTableCursors4Q211( ) ;
      }

      protected void Update4Q211( )
      {
         BeforeValidate4Q211( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable4Q211( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency4Q211( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm4Q211( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate4Q211( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T004Q9 */
                     pr_default.execute(7, new Object[] {A1892Alerta_Assunto, A1893Alerta_Descricao, n1882Alerta_ToAreaTrabalho, A1882Alerta_ToAreaTrabalho, n1883Alerta_ToUser, A1883Alerta_ToUser, n1884Alerta_ToGroup, A1884Alerta_ToGroup, A1885Alerta_Inicio, n1886Alerta_Fim, A1886Alerta_Fim, A1887Alerta_Owner, A1888Alerta_Cadastro, A1881Alerta_Codigo});
                     pr_default.close(7);
                     dsDefault.SmartCacheProvider.SetUpdated("Alerta") ;
                     if ( (pr_default.getStatus(7) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Alerta"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate4Q211( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                           {
                              if ( AnyError == 0 )
                              {
                                 context.nUserReturn = 1;
                              }
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel4Q211( ) ;
         }
         CloseExtendedTableCursors4Q211( ) ;
      }

      protected void DeferredUpdate4Q211( )
      {
      }

      protected void delete( )
      {
         BeforeValidate4Q211( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency4Q211( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls4Q211( ) ;
            AfterConfirm4Q211( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete4Q211( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T004Q10 */
                  pr_default.execute(8, new Object[] {A1881Alerta_Codigo});
                  pr_default.close(8);
                  dsDefault.SmartCacheProvider.SetUpdated("Alerta") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                        {
                           if ( AnyError == 0 )
                           {
                              context.nUserReturn = 1;
                           }
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode211 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         EndLevel4Q211( ) ;
         Gx_mode = sMode211;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
      }

      protected void OnDeleteControls4Q211( )
      {
         standaloneModal( ) ;
         /* No delete mode formulas found. */
         if ( AnyError == 0 )
         {
            /* Using cursor T004Q11 */
            pr_default.execute(9, new Object[] {A1881Alerta_Codigo});
            if ( (pr_default.getStatus(9) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Leitura"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(9);
         }
      }

      protected void EndLevel4Q211( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete4Q211( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(1);
            context.CommitDataStores( "Alerta");
            if ( AnyError == 0 )
            {
               ConfirmValues4Q0( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(1);
            context.RollbackDataStores( "Alerta");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart4Q211( )
      {
         /* Scan By routine */
         /* Using cursor T004Q12 */
         pr_default.execute(10);
         RcdFound211 = 0;
         if ( (pr_default.getStatus(10) != 101) )
         {
            RcdFound211 = 1;
            A1881Alerta_Codigo = T004Q12_A1881Alerta_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1881Alerta_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1881Alerta_Codigo), 6, 0)));
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext4Q211( )
      {
         /* Scan next routine */
         pr_default.readNext(10);
         RcdFound211 = 0;
         if ( (pr_default.getStatus(10) != 101) )
         {
            RcdFound211 = 1;
            A1881Alerta_Codigo = T004Q12_A1881Alerta_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1881Alerta_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1881Alerta_Codigo), 6, 0)));
         }
      }

      protected void ScanEnd4Q211( )
      {
         pr_default.close(10);
      }

      protected void AfterConfirm4Q211( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert4Q211( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate4Q211( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete4Q211( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete4Q211( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate4Q211( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes4Q211( )
      {
         edtAlerta_Assunto_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtAlerta_Assunto_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtAlerta_Assunto_Enabled), 5, 0)));
         dynAlerta_ToAreaTrabalho.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynAlerta_ToAreaTrabalho_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynAlerta_ToAreaTrabalho.Enabled), 5, 0)));
         dynAlerta_ToUser.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynAlerta_ToUser_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynAlerta_ToUser.Enabled), 5, 0)));
         cmbAlerta_ToGroup.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbAlerta_ToGroup_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbAlerta_ToGroup.Enabled), 5, 0)));
         edtAlerta_Inicio_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtAlerta_Inicio_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtAlerta_Inicio_Enabled), 5, 0)));
         edtAlerta_Fim_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtAlerta_Fim_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtAlerta_Fim_Enabled), 5, 0)));
         edtAlerta_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtAlerta_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtAlerta_Codigo_Enabled), 5, 0)));
         Alerta_descricao_Enabled = Convert.ToBoolean( 0);
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Alerta_descricao_Internalname, "Enabled", StringUtil.BoolToStr( Alerta_descricao_Enabled));
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues4Q0( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?2020428239341");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.AddJavascriptSource("CKEditor/ckeditor/ckeditor.js", "");
         context.AddJavascriptSource("CKEditor/CKEditorRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("alerta.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7Alerta_Codigo)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "Z1881Alerta_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1881Alerta_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1892Alerta_Assunto", Z1892Alerta_Assunto);
         GxWebStd.gx_hidden_field( context, "Z1882Alerta_ToAreaTrabalho", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1882Alerta_ToAreaTrabalho), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1883Alerta_ToUser", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1883Alerta_ToUser), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1884Alerta_ToGroup", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1884Alerta_ToGroup), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1885Alerta_Inicio", context.localUtil.DToC( Z1885Alerta_Inicio, 0, "/"));
         GxWebStd.gx_hidden_field( context, "Z1886Alerta_Fim", context.localUtil.DToC( Z1886Alerta_Fim, 0, "/"));
         GxWebStd.gx_hidden_field( context, "Z1887Alerta_Owner", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1887Alerta_Owner), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1888Alerta_Cadastro", context.localUtil.TToC( Z1888Alerta_Cadastro, 10, 8, 0, 0, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTRNCONTEXT", AV9TrnContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTRNCONTEXT", AV9TrnContext);
         }
         GxWebStd.gx_hidden_field( context, "vALERTA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7Alerta_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGXBSCREEN", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gx_BScreen), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "ALERTA_CADASTRO", context.localUtil.TToC( A1888Alerta_Cadastro, 10, 8, 0, 0, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "ALERTA_OWNER", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1887Alerta_Owner), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vCONTRATADA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV11Contratada_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vUSERID", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV12UserId), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vCONTRATANTE_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13Contratante_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "ALERTA_DESCRICAO", A1893Alerta_Descricao);
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vALERTA_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7Alerta_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "ALERTA_DESCRICAO_Enabled", StringUtil.BoolToStr( Alerta_descricao_Enabled));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Width", StringUtil.RTrim( Dvpanel_tableattributes_Width));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Cls", StringUtil.RTrim( Dvpanel_tableattributes_Cls));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Title", StringUtil.RTrim( Dvpanel_tableattributes_Title));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsible", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsible));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsed", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsed));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Enabled", StringUtil.BoolToStr( Dvpanel_tableattributes_Enabled));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autowidth", StringUtil.BoolToStr( Dvpanel_tableattributes_Autowidth));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoheight", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoheight));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Showcollapseicon", StringUtil.BoolToStr( Dvpanel_tableattributes_Showcollapseicon));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Iconposition", StringUtil.RTrim( Dvpanel_tableattributes_Iconposition));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoscroll", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoscroll));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = "hsh" + "Alerta";
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A1881Alerta_Codigo), "ZZZZZ9");
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A1887Alerta_Owner), "ZZZZZ9");
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( A1888Alerta_Cadastro, "99/99/99 99:99");
         GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("alerta:[SendSecurityCheck value for]"+"Alerta_Codigo:"+context.localUtil.Format( (decimal)(A1881Alerta_Codigo), "ZZZZZ9"));
         GXUtil.WriteLog("alerta:[SendSecurityCheck value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
         GXUtil.WriteLog("alerta:[SendSecurityCheck value for]"+"Alerta_Owner:"+context.localUtil.Format( (decimal)(A1887Alerta_Owner), "ZZZZZ9"));
         GXUtil.WriteLog("alerta:[SendSecurityCheck value for]"+"Alerta_Cadastro:"+context.localUtil.Format( A1888Alerta_Cadastro, "99/99/99 99:99"));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("alerta.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7Alerta_Codigo) ;
      }

      public override String GetPgmname( )
      {
         return "Alerta" ;
      }

      public override String GetPgmdesc( )
      {
         return "Alertas" ;
      }

      protected void InitializeNonKey4Q211( )
      {
         A1892Alerta_Assunto = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1892Alerta_Assunto", A1892Alerta_Assunto);
         A1893Alerta_Descricao = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1893Alerta_Descricao", A1893Alerta_Descricao);
         A1882Alerta_ToAreaTrabalho = 0;
         n1882Alerta_ToAreaTrabalho = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1882Alerta_ToAreaTrabalho", StringUtil.LTrim( StringUtil.Str( (decimal)(A1882Alerta_ToAreaTrabalho), 6, 0)));
         n1882Alerta_ToAreaTrabalho = ((0==A1882Alerta_ToAreaTrabalho) ? true : false);
         A1883Alerta_ToUser = 0;
         n1883Alerta_ToUser = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1883Alerta_ToUser", StringUtil.LTrim( StringUtil.Str( (decimal)(A1883Alerta_ToUser), 6, 0)));
         n1883Alerta_ToUser = ((0==A1883Alerta_ToUser) ? true : false);
         A1884Alerta_ToGroup = 0;
         n1884Alerta_ToGroup = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1884Alerta_ToGroup", StringUtil.LTrim( StringUtil.Str( (decimal)(A1884Alerta_ToGroup), 4, 0)));
         n1884Alerta_ToGroup = ((0==A1884Alerta_ToGroup) ? true : false);
         A1885Alerta_Inicio = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1885Alerta_Inicio", context.localUtil.Format(A1885Alerta_Inicio, "99/99/99"));
         A1886Alerta_Fim = DateTime.MinValue;
         n1886Alerta_Fim = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1886Alerta_Fim", context.localUtil.Format(A1886Alerta_Fim, "99/99/99"));
         n1886Alerta_Fim = ((DateTime.MinValue==A1886Alerta_Fim) ? true : false);
         A1887Alerta_Owner = AV8WWPContext.gxTpr_Userid;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1887Alerta_Owner", StringUtil.LTrim( StringUtil.Str( (decimal)(A1887Alerta_Owner), 6, 0)));
         A1888Alerta_Cadastro = DateTimeUtil.ServerNow( context, "DEFAULT");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1888Alerta_Cadastro", context.localUtil.TToC( A1888Alerta_Cadastro, 8, 5, 0, 3, "/", ":", " "));
         Z1892Alerta_Assunto = "";
         Z1882Alerta_ToAreaTrabalho = 0;
         Z1883Alerta_ToUser = 0;
         Z1884Alerta_ToGroup = 0;
         Z1885Alerta_Inicio = DateTime.MinValue;
         Z1886Alerta_Fim = DateTime.MinValue;
         Z1887Alerta_Owner = 0;
         Z1888Alerta_Cadastro = (DateTime)(DateTime.MinValue);
      }

      protected void InitAll4Q211( )
      {
         A1881Alerta_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1881Alerta_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A1881Alerta_Codigo), 6, 0)));
         InitializeNonKey4Q211( ) ;
      }

      protected void StandaloneModalInsert( )
      {
         A1888Alerta_Cadastro = i1888Alerta_Cadastro;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1888Alerta_Cadastro", context.localUtil.TToC( A1888Alerta_Cadastro, 8, 5, 0, 3, "/", ":", " "));
         A1887Alerta_Owner = i1887Alerta_Owner;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1887Alerta_Owner", StringUtil.LTrim( StringUtil.Str( (decimal)(A1887Alerta_Owner), 6, 0)));
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?2249787");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20204282393420");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("alerta.js", "?20204282393421");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.AddJavascriptSource("CKEditor/ckeditor/ckeditor.js", "");
         context.AddJavascriptSource("CKEditor/CKEditorRender.js", "");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblockalerta_assunto_Internalname = "TEXTBLOCKALERTA_ASSUNTO";
         edtAlerta_Assunto_Internalname = "ALERTA_ASSUNTO";
         lblTextblockalerta_descricao_Internalname = "TEXTBLOCKALERTA_DESCRICAO";
         Alerta_descricao_Internalname = "ALERTA_DESCRICAO";
         lblTextblockalerta_toareatrabalho_Internalname = "TEXTBLOCKALERTA_TOAREATRABALHO";
         dynAlerta_ToAreaTrabalho_Internalname = "ALERTA_TOAREATRABALHO";
         lblTextblockalerta_touser_Internalname = "TEXTBLOCKALERTA_TOUSER";
         dynAlerta_ToUser_Internalname = "ALERTA_TOUSER";
         lblTextblockalerta_togroup_Internalname = "TEXTBLOCKALERTA_TOGROUP";
         cmbAlerta_ToGroup_Internalname = "ALERTA_TOGROUP";
         lblTextblockalerta_inicio_Internalname = "TEXTBLOCKALERTA_INICIO";
         edtAlerta_Inicio_Internalname = "ALERTA_INICIO";
         lblTextblockalerta_fim_Internalname = "TEXTBLOCKALERTA_FIM";
         edtAlerta_Fim_Internalname = "ALERTA_FIM";
         tblTableattributes_Internalname = "TABLEATTRIBUTES";
         Dvpanel_tableattributes_Internalname = "DVPANEL_TABLEATTRIBUTES";
         tblTablecontent_Internalname = "TABLECONTENT";
         bttBtn_trn_enter_Internalname = "BTN_TRN_ENTER";
         bttBtn_trn_cancel_Internalname = "BTN_TRN_CANCEL";
         bttBtn_trn_delete_Internalname = "BTN_TRN_DELETE";
         tblTableactions_Internalname = "TABLEACTIONS";
         tblTablemain_Internalname = "TABLEMAIN";
         edtAlerta_Codigo_Internalname = "ALERTA_CODIGO";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Dvpanel_tableattributes_Autoscroll = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Iconposition = "left";
         Dvpanel_tableattributes_Showcollapseicon = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Autoheight = Convert.ToBoolean( -1);
         Dvpanel_tableattributes_Autowidth = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsed = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsible = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Title = "Alerta";
         Dvpanel_tableattributes_Cls = "GXUI-DVelop-Panel";
         Dvpanel_tableattributes_Width = "100%";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Alertas";
         edtAlerta_Fim_Jsonclick = "";
         edtAlerta_Fim_Enabled = 1;
         edtAlerta_Inicio_Jsonclick = "";
         edtAlerta_Inicio_Enabled = 1;
         cmbAlerta_ToGroup_Jsonclick = "";
         cmbAlerta_ToGroup.Enabled = 1;
         dynAlerta_ToUser_Jsonclick = "";
         dynAlerta_ToUser.Enabled = 1;
         dynAlerta_ToAreaTrabalho_Jsonclick = "";
         dynAlerta_ToAreaTrabalho.Enabled = 1;
         Alerta_descricao_Enabled = Convert.ToBoolean( 1);
         edtAlerta_Assunto_Jsonclick = "";
         edtAlerta_Assunto_Enabled = 1;
         bttBtn_trn_delete_Enabled = 0;
         bttBtn_trn_delete_Visible = 1;
         bttBtn_trn_cancel_Visible = 1;
         bttBtn_trn_enter_Enabled = 1;
         bttBtn_trn_enter_Visible = 1;
         edtAlerta_Codigo_Jsonclick = "";
         edtAlerta_Codigo_Enabled = 0;
         edtAlerta_Codigo_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void GXDSAALERTA_TOAREATRABALHO4Q211( int AV11Contratada_Codigo ,
                                                      int AV12UserId )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDSAALERTA_TOAREATRABALHO_data4Q211( AV11Contratada_Codigo, AV12UserId) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXAALERTA_TOAREATRABALHO_html4Q211( int AV11Contratada_Codigo ,
                                                         int AV12UserId )
      {
         int gxdynajaxvalue ;
         GXDSAALERTA_TOAREATRABALHO_data4Q211( AV11Contratada_Codigo, AV12UserId) ;
         gxdynajaxindex = 1;
         dynAlerta_ToAreaTrabalho.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynAlerta_ToAreaTrabalho.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
      }

      protected void GXDSAALERTA_TOAREATRABALHO_data4Q211( int AV11Contratada_Codigo ,
                                                           int AV12UserId )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("(Indiferente)");
         IGxCollection gxcolALERTA_TOAREATRABALHO ;
         SdtSDT_Codigos gxcolitemALERTA_TOAREATRABALHO ;
         new dp_areatrabalho(context ).execute(  AV11Contratada_Codigo,  AV12UserId, out  gxcolALERTA_TOAREATRABALHO) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11Contratada_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12UserId", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12UserId), 6, 0)));
         gxcolALERTA_TOAREATRABALHO.Sort("Descricao");
         int gxindex = 1 ;
         while ( gxindex <= gxcolALERTA_TOAREATRABALHO.Count )
         {
            gxcolitemALERTA_TOAREATRABALHO = ((SdtSDT_Codigos)gxcolALERTA_TOAREATRABALHO.Item(gxindex));
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.Str( (decimal)(gxcolitemALERTA_TOAREATRABALHO.gxTpr_Codigo), 6, 0)));
            gxdynajaxctrldescr.Add(gxcolitemALERTA_TOAREATRABALHO.gxTpr_Descricao);
            gxindex = (int)(gxindex+1);
         }
      }

      protected void GXDSAALERTA_TOUSER4Q211( int AV11Contratada_Codigo ,
                                              int AV13Contratante_Codigo )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDSAALERTA_TOUSER_data4Q211( AV11Contratada_Codigo, AV13Contratante_Codigo) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXAALERTA_TOUSER_html4Q211( int AV11Contratada_Codigo ,
                                                 int AV13Contratante_Codigo )
      {
         int gxdynajaxvalue ;
         GXDSAALERTA_TOUSER_data4Q211( AV11Contratada_Codigo, AV13Contratante_Codigo) ;
         gxdynajaxindex = 1;
         dynAlerta_ToUser.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynAlerta_ToUser.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
      }

      protected void GXDSAALERTA_TOUSER_data4Q211( int AV11Contratada_Codigo ,
                                                   int AV13Contratante_Codigo )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("(Indiferente)");
         IGxCollection gxcolALERTA_TOUSER ;
         SdtSDT_Codigos gxcolitemALERTA_TOUSER ;
         new dp_usuariosdaentidade(context ).execute(  AV11Contratada_Codigo,  AV13Contratante_Codigo, out  gxcolALERTA_TOUSER) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11Contratada_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11Contratada_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13Contratante_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13Contratante_Codigo), 6, 0)));
         gxcolALERTA_TOUSER.Sort("Descricao");
         int gxindex = 1 ;
         while ( gxindex <= gxcolALERTA_TOUSER.Count )
         {
            gxcolitemALERTA_TOUSER = ((SdtSDT_Codigos)gxcolALERTA_TOUSER.Item(gxindex));
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.Str( (decimal)(gxcolitemALERTA_TOUSER.gxTpr_Codigo), 6, 0)));
            gxdynajaxctrldescr.Add(gxcolitemALERTA_TOUSER.gxTpr_Descricao);
            gxindex = (int)(gxindex+1);
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true},{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV7Alerta_Codigo',fld:'vALERTA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("AFTER TRN","{handler:'E124Q2',iparms:[{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV9TrnContext',fld:'vTRNCONTEXT',pic:'',nv:null}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
      }

      public override void initialize( )
      {
         sPrefix = "";
         wcpOGx_mode = "";
         Z1892Alerta_Assunto = "";
         Z1885Alerta_Inicio = DateTime.MinValue;
         Z1886Alerta_Fim = DateTime.MinValue;
         Z1888Alerta_Cadastro = (DateTime)(DateTime.MinValue);
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         sStyleString = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         bttBtn_trn_enter_Jsonclick = "";
         bttBtn_trn_cancel_Jsonclick = "";
         bttBtn_trn_delete_Jsonclick = "";
         lblTextblockalerta_assunto_Jsonclick = "";
         A1892Alerta_Assunto = "";
         lblTextblockalerta_descricao_Jsonclick = "";
         lblTextblockalerta_toareatrabalho_Jsonclick = "";
         lblTextblockalerta_touser_Jsonclick = "";
         lblTextblockalerta_togroup_Jsonclick = "";
         lblTextblockalerta_inicio_Jsonclick = "";
         A1885Alerta_Inicio = DateTime.MinValue;
         lblTextblockalerta_fim_Jsonclick = "";
         A1886Alerta_Fim = DateTime.MinValue;
         A1888Alerta_Cadastro = (DateTime)(DateTime.MinValue);
         A1893Alerta_Descricao = "";
         Alerta_descricao_Width = "";
         Alerta_descricao_Height = "";
         Alerta_descricao_Skin = "";
         Alerta_descricao_Toolbar = "";
         Alerta_descricao_Class = "";
         Alerta_descricao_Customtoolbar = "";
         Alerta_descricao_Customconfiguration = "";
         Alerta_descricao_Buttonpressedid = "";
         Alerta_descricao_Captionvalue = "";
         Alerta_descricao_Captionclass = "";
         Alerta_descricao_Captionposition = "";
         Alerta_descricao_Coltitle = "";
         Alerta_descricao_Coltitlefont = "";
         Dvpanel_tableattributes_Height = "";
         Dvpanel_tableattributes_Class = "";
         forbiddenHiddens = "";
         hsh = "";
         sMode211 = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV8WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV10WebSession = context.GetSession();
         Z1893Alerta_Descricao = "";
         T004Q4_A1881Alerta_Codigo = new int[1] ;
         T004Q4_A1892Alerta_Assunto = new String[] {""} ;
         T004Q4_A1893Alerta_Descricao = new String[] {""} ;
         T004Q4_A1882Alerta_ToAreaTrabalho = new int[1] ;
         T004Q4_n1882Alerta_ToAreaTrabalho = new bool[] {false} ;
         T004Q4_A1883Alerta_ToUser = new int[1] ;
         T004Q4_n1883Alerta_ToUser = new bool[] {false} ;
         T004Q4_A1884Alerta_ToGroup = new short[1] ;
         T004Q4_n1884Alerta_ToGroup = new bool[] {false} ;
         T004Q4_A1885Alerta_Inicio = new DateTime[] {DateTime.MinValue} ;
         T004Q4_A1886Alerta_Fim = new DateTime[] {DateTime.MinValue} ;
         T004Q4_n1886Alerta_Fim = new bool[] {false} ;
         T004Q4_A1887Alerta_Owner = new int[1] ;
         T004Q4_A1888Alerta_Cadastro = new DateTime[] {DateTime.MinValue} ;
         T004Q5_A1881Alerta_Codigo = new int[1] ;
         T004Q3_A1881Alerta_Codigo = new int[1] ;
         T004Q3_A1892Alerta_Assunto = new String[] {""} ;
         T004Q3_A1893Alerta_Descricao = new String[] {""} ;
         T004Q3_A1882Alerta_ToAreaTrabalho = new int[1] ;
         T004Q3_n1882Alerta_ToAreaTrabalho = new bool[] {false} ;
         T004Q3_A1883Alerta_ToUser = new int[1] ;
         T004Q3_n1883Alerta_ToUser = new bool[] {false} ;
         T004Q3_A1884Alerta_ToGroup = new short[1] ;
         T004Q3_n1884Alerta_ToGroup = new bool[] {false} ;
         T004Q3_A1885Alerta_Inicio = new DateTime[] {DateTime.MinValue} ;
         T004Q3_A1886Alerta_Fim = new DateTime[] {DateTime.MinValue} ;
         T004Q3_n1886Alerta_Fim = new bool[] {false} ;
         T004Q3_A1887Alerta_Owner = new int[1] ;
         T004Q3_A1888Alerta_Cadastro = new DateTime[] {DateTime.MinValue} ;
         T004Q6_A1881Alerta_Codigo = new int[1] ;
         T004Q7_A1881Alerta_Codigo = new int[1] ;
         T004Q2_A1881Alerta_Codigo = new int[1] ;
         T004Q2_A1892Alerta_Assunto = new String[] {""} ;
         T004Q2_A1893Alerta_Descricao = new String[] {""} ;
         T004Q2_A1882Alerta_ToAreaTrabalho = new int[1] ;
         T004Q2_n1882Alerta_ToAreaTrabalho = new bool[] {false} ;
         T004Q2_A1883Alerta_ToUser = new int[1] ;
         T004Q2_n1883Alerta_ToUser = new bool[] {false} ;
         T004Q2_A1884Alerta_ToGroup = new short[1] ;
         T004Q2_n1884Alerta_ToGroup = new bool[] {false} ;
         T004Q2_A1885Alerta_Inicio = new DateTime[] {DateTime.MinValue} ;
         T004Q2_A1886Alerta_Fim = new DateTime[] {DateTime.MinValue} ;
         T004Q2_n1886Alerta_Fim = new bool[] {false} ;
         T004Q2_A1887Alerta_Owner = new int[1] ;
         T004Q2_A1888Alerta_Cadastro = new DateTime[] {DateTime.MinValue} ;
         T004Q8_A1881Alerta_Codigo = new int[1] ;
         T004Q11_A1881Alerta_Codigo = new int[1] ;
         T004Q11_A1889AlertaRead_Codigo = new int[1] ;
         T004Q12_A1881Alerta_Codigo = new int[1] ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         i1888Alerta_Cadastro = (DateTime)(DateTime.MinValue);
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.alerta__default(),
            new Object[][] {
                new Object[] {
               T004Q2_A1881Alerta_Codigo, T004Q2_A1892Alerta_Assunto, T004Q2_A1893Alerta_Descricao, T004Q2_A1882Alerta_ToAreaTrabalho, T004Q2_n1882Alerta_ToAreaTrabalho, T004Q2_A1883Alerta_ToUser, T004Q2_n1883Alerta_ToUser, T004Q2_A1884Alerta_ToGroup, T004Q2_n1884Alerta_ToGroup, T004Q2_A1885Alerta_Inicio,
               T004Q2_A1886Alerta_Fim, T004Q2_n1886Alerta_Fim, T004Q2_A1887Alerta_Owner, T004Q2_A1888Alerta_Cadastro
               }
               , new Object[] {
               T004Q3_A1881Alerta_Codigo, T004Q3_A1892Alerta_Assunto, T004Q3_A1893Alerta_Descricao, T004Q3_A1882Alerta_ToAreaTrabalho, T004Q3_n1882Alerta_ToAreaTrabalho, T004Q3_A1883Alerta_ToUser, T004Q3_n1883Alerta_ToUser, T004Q3_A1884Alerta_ToGroup, T004Q3_n1884Alerta_ToGroup, T004Q3_A1885Alerta_Inicio,
               T004Q3_A1886Alerta_Fim, T004Q3_n1886Alerta_Fim, T004Q3_A1887Alerta_Owner, T004Q3_A1888Alerta_Cadastro
               }
               , new Object[] {
               T004Q4_A1881Alerta_Codigo, T004Q4_A1892Alerta_Assunto, T004Q4_A1893Alerta_Descricao, T004Q4_A1882Alerta_ToAreaTrabalho, T004Q4_n1882Alerta_ToAreaTrabalho, T004Q4_A1883Alerta_ToUser, T004Q4_n1883Alerta_ToUser, T004Q4_A1884Alerta_ToGroup, T004Q4_n1884Alerta_ToGroup, T004Q4_A1885Alerta_Inicio,
               T004Q4_A1886Alerta_Fim, T004Q4_n1886Alerta_Fim, T004Q4_A1887Alerta_Owner, T004Q4_A1888Alerta_Cadastro
               }
               , new Object[] {
               T004Q5_A1881Alerta_Codigo
               }
               , new Object[] {
               T004Q6_A1881Alerta_Codigo
               }
               , new Object[] {
               T004Q7_A1881Alerta_Codigo
               }
               , new Object[] {
               T004Q8_A1881Alerta_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T004Q11_A1881Alerta_Codigo, T004Q11_A1889AlertaRead_Codigo
               }
               , new Object[] {
               T004Q12_A1881Alerta_Codigo
               }
            }
         );
         Z1888Alerta_Cadastro = DateTimeUtil.ServerNow( context, "DEFAULT");
         A1888Alerta_Cadastro = DateTimeUtil.ServerNow( context, "DEFAULT");
         i1888Alerta_Cadastro = DateTimeUtil.ServerNow( context, "DEFAULT");
         Z1887Alerta_Owner = AV8WWPContext.gxTpr_Userid;
         A1887Alerta_Owner = AV8WWPContext.gxTpr_Userid;
         i1887Alerta_Owner = AV8WWPContext.gxTpr_Userid;
      }

      private short Z1884Alerta_ToGroup ;
      private short GxWebError ;
      private short gxcookieaux ;
      private short A1884Alerta_ToGroup ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short Gx_BScreen ;
      private short RcdFound211 ;
      private short GX_JID ;
      private short gxajaxcallmode ;
      private int wcpOAV7Alerta_Codigo ;
      private int Z1881Alerta_Codigo ;
      private int Z1882Alerta_ToAreaTrabalho ;
      private int Z1883Alerta_ToUser ;
      private int Z1887Alerta_Owner ;
      private int AV11Contratada_Codigo ;
      private int AV12UserId ;
      private int AV13Contratante_Codigo ;
      private int AV7Alerta_Codigo ;
      private int trnEnded ;
      private int A1882Alerta_ToAreaTrabalho ;
      private int A1883Alerta_ToUser ;
      private int A1881Alerta_Codigo ;
      private int edtAlerta_Codigo_Enabled ;
      private int edtAlerta_Codigo_Visible ;
      private int bttBtn_trn_enter_Visible ;
      private int bttBtn_trn_enter_Enabled ;
      private int bttBtn_trn_cancel_Visible ;
      private int bttBtn_trn_delete_Visible ;
      private int bttBtn_trn_delete_Enabled ;
      private int edtAlerta_Assunto_Enabled ;
      private int edtAlerta_Inicio_Enabled ;
      private int edtAlerta_Fim_Enabled ;
      private int A1887Alerta_Owner ;
      private int Alerta_descricao_Color ;
      private int Alerta_descricao_Coltitlecolor ;
      private int i1887Alerta_Owner ;
      private int idxLst ;
      private int gxdynajaxindex ;
      private String sPrefix ;
      private String wcpOGx_mode ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String Gx_mode ;
      private String GXKey ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String edtAlerta_Assunto_Internalname ;
      private String edtAlerta_Codigo_Internalname ;
      private String edtAlerta_Codigo_Jsonclick ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTableactions_Internalname ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String bttBtn_trn_enter_Internalname ;
      private String bttBtn_trn_enter_Jsonclick ;
      private String bttBtn_trn_cancel_Internalname ;
      private String bttBtn_trn_cancel_Jsonclick ;
      private String bttBtn_trn_delete_Internalname ;
      private String bttBtn_trn_delete_Jsonclick ;
      private String tblTablecontent_Internalname ;
      private String tblTableattributes_Internalname ;
      private String lblTextblockalerta_assunto_Internalname ;
      private String lblTextblockalerta_assunto_Jsonclick ;
      private String edtAlerta_Assunto_Jsonclick ;
      private String lblTextblockalerta_descricao_Internalname ;
      private String lblTextblockalerta_descricao_Jsonclick ;
      private String lblTextblockalerta_toareatrabalho_Internalname ;
      private String lblTextblockalerta_toareatrabalho_Jsonclick ;
      private String dynAlerta_ToAreaTrabalho_Internalname ;
      private String dynAlerta_ToAreaTrabalho_Jsonclick ;
      private String lblTextblockalerta_touser_Internalname ;
      private String lblTextblockalerta_touser_Jsonclick ;
      private String dynAlerta_ToUser_Internalname ;
      private String dynAlerta_ToUser_Jsonclick ;
      private String lblTextblockalerta_togroup_Internalname ;
      private String lblTextblockalerta_togroup_Jsonclick ;
      private String cmbAlerta_ToGroup_Internalname ;
      private String cmbAlerta_ToGroup_Jsonclick ;
      private String lblTextblockalerta_inicio_Internalname ;
      private String lblTextblockalerta_inicio_Jsonclick ;
      private String edtAlerta_Inicio_Internalname ;
      private String edtAlerta_Inicio_Jsonclick ;
      private String lblTextblockalerta_fim_Internalname ;
      private String lblTextblockalerta_fim_Jsonclick ;
      private String edtAlerta_Fim_Internalname ;
      private String edtAlerta_Fim_Jsonclick ;
      private String Alerta_descricao_Width ;
      private String Alerta_descricao_Height ;
      private String Alerta_descricao_Skin ;
      private String Alerta_descricao_Toolbar ;
      private String Alerta_descricao_Class ;
      private String Alerta_descricao_Customtoolbar ;
      private String Alerta_descricao_Customconfiguration ;
      private String Alerta_descricao_Buttonpressedid ;
      private String Alerta_descricao_Captionvalue ;
      private String Alerta_descricao_Captionclass ;
      private String Alerta_descricao_Captionposition ;
      private String Alerta_descricao_Coltitle ;
      private String Alerta_descricao_Coltitlefont ;
      private String Dvpanel_tableattributes_Width ;
      private String Dvpanel_tableattributes_Height ;
      private String Dvpanel_tableattributes_Cls ;
      private String Dvpanel_tableattributes_Title ;
      private String Dvpanel_tableattributes_Class ;
      private String Dvpanel_tableattributes_Iconposition ;
      private String forbiddenHiddens ;
      private String hsh ;
      private String sMode211 ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String Alerta_descricao_Internalname ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Dvpanel_tableattributes_Internalname ;
      private String gxwrpcisep ;
      private DateTime Z1888Alerta_Cadastro ;
      private DateTime A1888Alerta_Cadastro ;
      private DateTime i1888Alerta_Cadastro ;
      private DateTime Z1885Alerta_Inicio ;
      private DateTime Z1886Alerta_Fim ;
      private DateTime A1885Alerta_Inicio ;
      private DateTime A1886Alerta_Fim ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool n1884Alerta_ToGroup ;
      private bool wbErr ;
      private bool n1882Alerta_ToAreaTrabalho ;
      private bool n1883Alerta_ToUser ;
      private bool n1886Alerta_Fim ;
      private bool Alerta_descricao_Enabled ;
      private bool Alerta_descricao_Toolbarcancollapse ;
      private bool Alerta_descricao_Toolbarexpanded ;
      private bool Alerta_descricao_Usercontroliscolumn ;
      private bool Alerta_descricao_Visible ;
      private bool Dvpanel_tableattributes_Collapsible ;
      private bool Dvpanel_tableattributes_Collapsed ;
      private bool Dvpanel_tableattributes_Enabled ;
      private bool Dvpanel_tableattributes_Autowidth ;
      private bool Dvpanel_tableattributes_Autoheight ;
      private bool Dvpanel_tableattributes_Showheader ;
      private bool Dvpanel_tableattributes_Showcollapseicon ;
      private bool Dvpanel_tableattributes_Autoscroll ;
      private bool Dvpanel_tableattributes_Visible ;
      private bool returnInSub ;
      private bool Gx_longc ;
      private String A1893Alerta_Descricao ;
      private String Z1893Alerta_Descricao ;
      private String Z1892Alerta_Assunto ;
      private String A1892Alerta_Assunto ;
      private IGxSession AV10WebSession ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox dynAlerta_ToAreaTrabalho ;
      private GXCombobox dynAlerta_ToUser ;
      private GXCombobox cmbAlerta_ToGroup ;
      private IDataStoreProvider pr_default ;
      private int[] T004Q4_A1881Alerta_Codigo ;
      private String[] T004Q4_A1892Alerta_Assunto ;
      private String[] T004Q4_A1893Alerta_Descricao ;
      private int[] T004Q4_A1882Alerta_ToAreaTrabalho ;
      private bool[] T004Q4_n1882Alerta_ToAreaTrabalho ;
      private int[] T004Q4_A1883Alerta_ToUser ;
      private bool[] T004Q4_n1883Alerta_ToUser ;
      private short[] T004Q4_A1884Alerta_ToGroup ;
      private bool[] T004Q4_n1884Alerta_ToGroup ;
      private DateTime[] T004Q4_A1885Alerta_Inicio ;
      private DateTime[] T004Q4_A1886Alerta_Fim ;
      private bool[] T004Q4_n1886Alerta_Fim ;
      private int[] T004Q4_A1887Alerta_Owner ;
      private DateTime[] T004Q4_A1888Alerta_Cadastro ;
      private int[] T004Q5_A1881Alerta_Codigo ;
      private int[] T004Q3_A1881Alerta_Codigo ;
      private String[] T004Q3_A1892Alerta_Assunto ;
      private String[] T004Q3_A1893Alerta_Descricao ;
      private int[] T004Q3_A1882Alerta_ToAreaTrabalho ;
      private bool[] T004Q3_n1882Alerta_ToAreaTrabalho ;
      private int[] T004Q3_A1883Alerta_ToUser ;
      private bool[] T004Q3_n1883Alerta_ToUser ;
      private short[] T004Q3_A1884Alerta_ToGroup ;
      private bool[] T004Q3_n1884Alerta_ToGroup ;
      private DateTime[] T004Q3_A1885Alerta_Inicio ;
      private DateTime[] T004Q3_A1886Alerta_Fim ;
      private bool[] T004Q3_n1886Alerta_Fim ;
      private int[] T004Q3_A1887Alerta_Owner ;
      private DateTime[] T004Q3_A1888Alerta_Cadastro ;
      private int[] T004Q6_A1881Alerta_Codigo ;
      private int[] T004Q7_A1881Alerta_Codigo ;
      private int[] T004Q2_A1881Alerta_Codigo ;
      private String[] T004Q2_A1892Alerta_Assunto ;
      private String[] T004Q2_A1893Alerta_Descricao ;
      private int[] T004Q2_A1882Alerta_ToAreaTrabalho ;
      private bool[] T004Q2_n1882Alerta_ToAreaTrabalho ;
      private int[] T004Q2_A1883Alerta_ToUser ;
      private bool[] T004Q2_n1883Alerta_ToUser ;
      private short[] T004Q2_A1884Alerta_ToGroup ;
      private bool[] T004Q2_n1884Alerta_ToGroup ;
      private DateTime[] T004Q2_A1885Alerta_Inicio ;
      private DateTime[] T004Q2_A1886Alerta_Fim ;
      private bool[] T004Q2_n1886Alerta_Fim ;
      private int[] T004Q2_A1887Alerta_Owner ;
      private DateTime[] T004Q2_A1888Alerta_Cadastro ;
      private int[] T004Q8_A1881Alerta_Codigo ;
      private int[] T004Q11_A1881Alerta_Codigo ;
      private int[] T004Q11_A1889AlertaRead_Codigo ;
      private int[] T004Q12_A1881Alerta_Codigo ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV8WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
   }

   public class alerta__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new UpdateCursor(def[7])
         ,new UpdateCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT004Q4 ;
          prmT004Q4 = new Object[] {
          new Object[] {"@Alerta_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004Q5 ;
          prmT004Q5 = new Object[] {
          new Object[] {"@Alerta_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004Q3 ;
          prmT004Q3 = new Object[] {
          new Object[] {"@Alerta_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004Q6 ;
          prmT004Q6 = new Object[] {
          new Object[] {"@Alerta_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004Q7 ;
          prmT004Q7 = new Object[] {
          new Object[] {"@Alerta_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004Q2 ;
          prmT004Q2 = new Object[] {
          new Object[] {"@Alerta_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004Q8 ;
          prmT004Q8 = new Object[] {
          new Object[] {"@Alerta_Assunto",SqlDbType.VarChar,80,0} ,
          new Object[] {"@Alerta_Descricao",SqlDbType.VarChar,500,0} ,
          new Object[] {"@Alerta_ToAreaTrabalho",SqlDbType.Int,6,0} ,
          new Object[] {"@Alerta_ToUser",SqlDbType.Int,6,0} ,
          new Object[] {"@Alerta_ToGroup",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@Alerta_Inicio",SqlDbType.DateTime,8,0} ,
          new Object[] {"@Alerta_Fim",SqlDbType.DateTime,8,0} ,
          new Object[] {"@Alerta_Owner",SqlDbType.Int,6,0} ,
          new Object[] {"@Alerta_Cadastro",SqlDbType.DateTime,8,5}
          } ;
          Object[] prmT004Q9 ;
          prmT004Q9 = new Object[] {
          new Object[] {"@Alerta_Assunto",SqlDbType.VarChar,80,0} ,
          new Object[] {"@Alerta_Descricao",SqlDbType.VarChar,500,0} ,
          new Object[] {"@Alerta_ToAreaTrabalho",SqlDbType.Int,6,0} ,
          new Object[] {"@Alerta_ToUser",SqlDbType.Int,6,0} ,
          new Object[] {"@Alerta_ToGroup",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@Alerta_Inicio",SqlDbType.DateTime,8,0} ,
          new Object[] {"@Alerta_Fim",SqlDbType.DateTime,8,0} ,
          new Object[] {"@Alerta_Owner",SqlDbType.Int,6,0} ,
          new Object[] {"@Alerta_Cadastro",SqlDbType.DateTime,8,5} ,
          new Object[] {"@Alerta_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004Q10 ;
          prmT004Q10 = new Object[] {
          new Object[] {"@Alerta_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004Q11 ;
          prmT004Q11 = new Object[] {
          new Object[] {"@Alerta_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004Q12 ;
          prmT004Q12 = new Object[] {
          } ;
          def= new CursorDef[] {
              new CursorDef("T004Q2", "SELECT [Alerta_Codigo], [Alerta_Assunto], [Alerta_Descricao], [Alerta_ToAreaTrabalho], [Alerta_ToUser], [Alerta_ToGroup], [Alerta_Inicio], [Alerta_Fim], [Alerta_Owner], [Alerta_Cadastro] FROM [Alerta] WITH (UPDLOCK) WHERE [Alerta_Codigo] = @Alerta_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT004Q2,1,0,true,false )
             ,new CursorDef("T004Q3", "SELECT [Alerta_Codigo], [Alerta_Assunto], [Alerta_Descricao], [Alerta_ToAreaTrabalho], [Alerta_ToUser], [Alerta_ToGroup], [Alerta_Inicio], [Alerta_Fim], [Alerta_Owner], [Alerta_Cadastro] FROM [Alerta] WITH (NOLOCK) WHERE [Alerta_Codigo] = @Alerta_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT004Q3,1,0,true,false )
             ,new CursorDef("T004Q4", "SELECT TM1.[Alerta_Codigo], TM1.[Alerta_Assunto], TM1.[Alerta_Descricao], TM1.[Alerta_ToAreaTrabalho], TM1.[Alerta_ToUser], TM1.[Alerta_ToGroup], TM1.[Alerta_Inicio], TM1.[Alerta_Fim], TM1.[Alerta_Owner], TM1.[Alerta_Cadastro] FROM [Alerta] TM1 WITH (NOLOCK) WHERE TM1.[Alerta_Codigo] = @Alerta_Codigo ORDER BY TM1.[Alerta_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT004Q4,100,0,true,false )
             ,new CursorDef("T004Q5", "SELECT [Alerta_Codigo] FROM [Alerta] WITH (NOLOCK) WHERE [Alerta_Codigo] = @Alerta_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT004Q5,1,0,true,false )
             ,new CursorDef("T004Q6", "SELECT TOP 1 [Alerta_Codigo] FROM [Alerta] WITH (NOLOCK) WHERE ( [Alerta_Codigo] > @Alerta_Codigo) ORDER BY [Alerta_Codigo]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT004Q6,1,0,true,true )
             ,new CursorDef("T004Q7", "SELECT TOP 1 [Alerta_Codigo] FROM [Alerta] WITH (NOLOCK) WHERE ( [Alerta_Codigo] < @Alerta_Codigo) ORDER BY [Alerta_Codigo] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT004Q7,1,0,true,true )
             ,new CursorDef("T004Q8", "INSERT INTO [Alerta]([Alerta_Assunto], [Alerta_Descricao], [Alerta_ToAreaTrabalho], [Alerta_ToUser], [Alerta_ToGroup], [Alerta_Inicio], [Alerta_Fim], [Alerta_Owner], [Alerta_Cadastro]) VALUES(@Alerta_Assunto, @Alerta_Descricao, @Alerta_ToAreaTrabalho, @Alerta_ToUser, @Alerta_ToGroup, @Alerta_Inicio, @Alerta_Fim, @Alerta_Owner, @Alerta_Cadastro); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmT004Q8)
             ,new CursorDef("T004Q9", "UPDATE [Alerta] SET [Alerta_Assunto]=@Alerta_Assunto, [Alerta_Descricao]=@Alerta_Descricao, [Alerta_ToAreaTrabalho]=@Alerta_ToAreaTrabalho, [Alerta_ToUser]=@Alerta_ToUser, [Alerta_ToGroup]=@Alerta_ToGroup, [Alerta_Inicio]=@Alerta_Inicio, [Alerta_Fim]=@Alerta_Fim, [Alerta_Owner]=@Alerta_Owner, [Alerta_Cadastro]=@Alerta_Cadastro  WHERE [Alerta_Codigo] = @Alerta_Codigo", GxErrorMask.GX_NOMASK,prmT004Q9)
             ,new CursorDef("T004Q10", "DELETE FROM [Alerta]  WHERE [Alerta_Codigo] = @Alerta_Codigo", GxErrorMask.GX_NOMASK,prmT004Q10)
             ,new CursorDef("T004Q11", "SELECT TOP 1 [Alerta_Codigo], [AlertaRead_Codigo] FROM [AlertaRead] WITH (NOLOCK) WHERE [Alerta_Codigo] = @Alerta_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT004Q11,1,0,true,true )
             ,new CursorDef("T004Q12", "SELECT [Alerta_Codigo] FROM [Alerta] WITH (NOLOCK) ORDER BY [Alerta_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT004Q12,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((String[]) buf[2])[0] = rslt.getLongVarchar(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((int[]) buf[5])[0] = rslt.getInt(5) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((short[]) buf[7])[0] = rslt.getShort(6) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((DateTime[]) buf[9])[0] = rslt.getGXDate(7) ;
                ((DateTime[]) buf[10])[0] = rslt.getGXDate(8) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(8);
                ((int[]) buf[12])[0] = rslt.getInt(9) ;
                ((DateTime[]) buf[13])[0] = rslt.getGXDateTime(10) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((String[]) buf[2])[0] = rslt.getLongVarchar(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((int[]) buf[5])[0] = rslt.getInt(5) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((short[]) buf[7])[0] = rslt.getShort(6) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((DateTime[]) buf[9])[0] = rslt.getGXDate(7) ;
                ((DateTime[]) buf[10])[0] = rslt.getGXDate(8) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(8);
                ((int[]) buf[12])[0] = rslt.getInt(9) ;
                ((DateTime[]) buf[13])[0] = rslt.getGXDateTime(10) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((String[]) buf[2])[0] = rslt.getLongVarchar(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((int[]) buf[5])[0] = rslt.getInt(5) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((short[]) buf[7])[0] = rslt.getShort(6) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((DateTime[]) buf[9])[0] = rslt.getGXDate(7) ;
                ((DateTime[]) buf[10])[0] = rslt.getGXDate(8) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(8);
                ((int[]) buf[12])[0] = rslt.getInt(9) ;
                ((DateTime[]) buf[13])[0] = rslt.getGXDateTime(10) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 10 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 3 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(3, (int)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 4 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(4, (int)parms[5]);
                }
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 5 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(5, (short)parms[7]);
                }
                stmt.SetParameter(6, (DateTime)parms[8]);
                if ( (bool)parms[9] )
                {
                   stmt.setNull( 7 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameter(7, (DateTime)parms[10]);
                }
                stmt.SetParameter(8, (int)parms[11]);
                stmt.SetParameterDatetime(9, (DateTime)parms[12]);
                return;
             case 7 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                if ( (bool)parms[2] )
                {
                   stmt.setNull( 3 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(3, (int)parms[3]);
                }
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 4 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(4, (int)parms[5]);
                }
                if ( (bool)parms[6] )
                {
                   stmt.setNull( 5 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(5, (short)parms[7]);
                }
                stmt.SetParameter(6, (DateTime)parms[8]);
                if ( (bool)parms[9] )
                {
                   stmt.setNull( 7 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameter(7, (DateTime)parms[10]);
                }
                stmt.SetParameter(8, (int)parms[11]);
                stmt.SetParameterDatetime(9, (DateTime)parms[12]);
                stmt.SetParameter(10, (int)parms[13]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
