/*
               File: WWFatoresImpacto
        Description:  Configura��o de Fatores de Impacto na Contagem
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/18/2020 13:45:57.18
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wwfatoresimpacto : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wwfatoresimpacto( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wwfatoresimpacto( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
            {
               nRC_GXsfl_32 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_32_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_32_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
            {
               subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV14AreaTrabalho_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14AreaTrabalho_Codigo), 6, 0)));
               A5AreaTrabalho_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               ajax_req_read_hidden_sdt(GetNextPar( ), AV6WWPContext);
               AV29Pgmname = GetNextPar( );
               A2127FatoresImpacto_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid_refresh( subGrid_Rows, AV14AreaTrabalho_Codigo, A5AreaTrabalho_Codigo, AV6WWPContext, AV29Pgmname, A2127FatoresImpacto_Codigo) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PASX2( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            STARTSX2( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?202051813455726");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wwfatoresimpacto.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_32", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_32), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV17GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV18GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vWWPCONTEXT", AV6WWPContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vWWPCONTEXT", AV6WWPContext);
         }
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV29Pgmname));
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WESX2( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVTSX2( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return false ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wwfatoresimpacto.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "WWFatoresImpacto" ;
      }

      public override String GetPgmdesc( )
      {
         return " Configura��o de Fatores de Impacto na Contagem" ;
      }

      protected void WBSX0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            wb_table1_2_SX2( true) ;
         }
         else
         {
            wb_table1_2_SX2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_SX2e( bool wbgen )
      {
         if ( wbgen )
         {
         }
         wbLoad = true;
      }

      protected void STARTSX2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", " Configura��o de Fatores de Impacto na Contagem", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPSX0( ) ;
      }

      protected void WSSX2( )
      {
         STARTSX2( ) ;
         EVTSX2( ) ;
      }

      protected void EVTSX2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E11SX2 */
                              E11SX2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOINSERT'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E12SX2 */
                              E12SX2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              nGXsfl_32_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_32_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_32_idx), 4, 0)), 4, "0");
                              SubsflControlProps_322( ) ;
                              AV19Update = cgiGet( edtavUpdate_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUpdate_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV19Update)) ? AV26Update_GXI : context.convertURL( context.PathToRelativeUrl( AV19Update))));
                              AV20Delete = cgiGet( edtavDelete_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDelete_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV20Delete)) ? AV27Delete_GXI : context.convertURL( context.PathToRelativeUrl( AV20Delete))));
                              A5AreaTrabalho_Codigo = (int)(context.localUtil.CToN( cgiGet( edtAreaTrabalho_Codigo_Internalname), ",", "."));
                              A2127FatoresImpacto_Codigo = (int)(context.localUtil.CToN( cgiGet( edtFatoresImpacto_Codigo_Internalname), ",", "."));
                              A2119FatoresImpacto_ValorEMCSD = context.localUtil.CToN( cgiGet( edtFatoresImpacto_ValorEMCSD_Internalname), ",", ".");
                              A2120FatoresImpacto_ValorEMCCD = context.localUtil.CToN( cgiGet( edtFatoresImpacto_ValorEMCCD_Internalname), ",", ".");
                              A2121FatoresImpacto_ValorENCSD = context.localUtil.CToN( cgiGet( edtFatoresImpacto_ValorENCSD_Internalname), ",", ".");
                              A2122FatoresImpacto_ValorENCCD = context.localUtil.CToN( cgiGet( edtFatoresImpacto_ValorENCCD_Internalname), ",", ".");
                              A2123FatoresImpacto_ValorCMCSD = context.localUtil.CToN( cgiGet( edtFatoresImpacto_ValorCMCSD_Internalname), ",", ".");
                              A2124FatoresImpacto_ValorCMCCD = context.localUtil.CToN( cgiGet( edtFatoresImpacto_ValorCMCCD_Internalname), ",", ".");
                              A2125FatoresImpacto_ValorCNCSD = context.localUtil.CToN( cgiGet( edtFatoresImpacto_ValorCNCSD_Internalname), ",", ".");
                              A2126FatoresImpacto_ValorCNCCD = context.localUtil.CToN( cgiGet( edtFatoresImpacto_ValorCNCCD_Internalname), ",", ".");
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E13SX2 */
                                    E13SX2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E14SX2 */
                                    E14SX2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E15SX2 */
                                    E15SX2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WESX2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PASX2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = edtavAreatrabalho_codigo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_322( ) ;
         while ( nGXsfl_32_idx <= nRC_GXsfl_32 )
         {
            sendrow_322( ) ;
            nGXsfl_32_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_32_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_32_idx+1));
            sGXsfl_32_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_32_idx), 4, 0)), 4, "0");
            SubsflControlProps_322( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       int AV14AreaTrabalho_Codigo ,
                                       int A5AreaTrabalho_Codigo ,
                                       wwpbaseobjects.SdtWWPContext AV6WWPContext ,
                                       String AV29Pgmname ,
                                       int A2127FatoresImpacto_Codigo )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RFSX2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_AREATRABALHO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A5AreaTrabalho_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "AREATRABALHO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A5AreaTrabalho_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_FATORESIMPACTO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A2127FatoresImpacto_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "FATORESIMPACTO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A2127FatoresImpacto_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_FATORESIMPACTO_VALOREMCSD", GetSecureSignedToken( "", context.localUtil.Format( A2119FatoresImpacto_ValorEMCSD, "Z9.99")));
         GxWebStd.gx_hidden_field( context, "FATORESIMPACTO_VALOREMCSD", StringUtil.LTrim( StringUtil.NToC( A2119FatoresImpacto_ValorEMCSD, 5, 2, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_FATORESIMPACTO_VALOREMCCD", GetSecureSignedToken( "", context.localUtil.Format( A2120FatoresImpacto_ValorEMCCD, "Z9.99")));
         GxWebStd.gx_hidden_field( context, "FATORESIMPACTO_VALOREMCCD", StringUtil.LTrim( StringUtil.NToC( A2120FatoresImpacto_ValorEMCCD, 5, 2, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_FATORESIMPACTO_VALORENCSD", GetSecureSignedToken( "", context.localUtil.Format( A2121FatoresImpacto_ValorENCSD, "Z9.99")));
         GxWebStd.gx_hidden_field( context, "FATORESIMPACTO_VALORENCSD", StringUtil.LTrim( StringUtil.NToC( A2121FatoresImpacto_ValorENCSD, 5, 2, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_FATORESIMPACTO_VALORENCCD", GetSecureSignedToken( "", context.localUtil.Format( A2122FatoresImpacto_ValorENCCD, "Z9.99")));
         GxWebStd.gx_hidden_field( context, "FATORESIMPACTO_VALORENCCD", StringUtil.LTrim( StringUtil.NToC( A2122FatoresImpacto_ValorENCCD, 5, 2, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_FATORESIMPACTO_VALORCMCSD", GetSecureSignedToken( "", context.localUtil.Format( A2123FatoresImpacto_ValorCMCSD, "Z9.99")));
         GxWebStd.gx_hidden_field( context, "FATORESIMPACTO_VALORCMCSD", StringUtil.LTrim( StringUtil.NToC( A2123FatoresImpacto_ValorCMCSD, 5, 2, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_FATORESIMPACTO_VALORCMCCD", GetSecureSignedToken( "", context.localUtil.Format( A2124FatoresImpacto_ValorCMCCD, "Z9.99")));
         GxWebStd.gx_hidden_field( context, "FATORESIMPACTO_VALORCMCCD", StringUtil.LTrim( StringUtil.NToC( A2124FatoresImpacto_ValorCMCCD, 5, 2, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_FATORESIMPACTO_VALORCNCSD", GetSecureSignedToken( "", context.localUtil.Format( A2125FatoresImpacto_ValorCNCSD, "Z9.99")));
         GxWebStd.gx_hidden_field( context, "FATORESIMPACTO_VALORCNCSD", StringUtil.LTrim( StringUtil.NToC( A2125FatoresImpacto_ValorCNCSD, 5, 2, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_FATORESIMPACTO_VALORCNCCD", GetSecureSignedToken( "", context.localUtil.Format( A2126FatoresImpacto_ValorCNCCD, "Z9.99")));
         GxWebStd.gx_hidden_field( context, "FATORESIMPACTO_VALORCNCCD", StringUtil.LTrim( StringUtil.NToC( A2126FatoresImpacto_ValorCNCCD, 5, 2, ".", "")));
      }

      protected void fix_multi_value_controls( )
      {
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFSX2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV29Pgmname = "WWFatoresImpacto";
         context.Gx_err = 0;
      }

      protected void RFSX2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 32;
         /* Execute user event: E14SX2 */
         E14SX2 ();
         nGXsfl_32_idx = 1;
         sGXsfl_32_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_32_idx), 4, 0)), 4, "0");
         SubsflControlProps_322( ) ;
         nGXsfl_32_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", "");
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_322( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            /* Using cursor H00SX2 */
            pr_default.execute(0, new Object[] {AV6WWPContext.gxTpr_Areatrabalho_codigo, GXPagingFrom2, GXPagingTo2});
            nGXsfl_32_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A2126FatoresImpacto_ValorCNCCD = H00SX2_A2126FatoresImpacto_ValorCNCCD[0];
               A2125FatoresImpacto_ValorCNCSD = H00SX2_A2125FatoresImpacto_ValorCNCSD[0];
               A2124FatoresImpacto_ValorCMCCD = H00SX2_A2124FatoresImpacto_ValorCMCCD[0];
               A2123FatoresImpacto_ValorCMCSD = H00SX2_A2123FatoresImpacto_ValorCMCSD[0];
               A2122FatoresImpacto_ValorENCCD = H00SX2_A2122FatoresImpacto_ValorENCCD[0];
               A2121FatoresImpacto_ValorENCSD = H00SX2_A2121FatoresImpacto_ValorENCSD[0];
               A2120FatoresImpacto_ValorEMCCD = H00SX2_A2120FatoresImpacto_ValorEMCCD[0];
               A2119FatoresImpacto_ValorEMCSD = H00SX2_A2119FatoresImpacto_ValorEMCSD[0];
               A2127FatoresImpacto_Codigo = H00SX2_A2127FatoresImpacto_Codigo[0];
               A5AreaTrabalho_Codigo = H00SX2_A5AreaTrabalho_Codigo[0];
               /* Execute user event: E15SX2 */
               E15SX2 ();
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 32;
            WBSX0( ) ;
         }
         nGXsfl_32_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         AV25WWFatoresImpactoDS_1_Areatrabalho_codigo = AV14AreaTrabalho_Codigo;
         /* Using cursor H00SX3 */
         pr_default.execute(1, new Object[] {AV6WWPContext.gxTpr_Areatrabalho_codigo});
         GRID_nRecordCount = H00SX3_AGRID_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         AV25WWFatoresImpactoDS_1_Areatrabalho_codigo = AV14AreaTrabalho_Codigo;
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14AreaTrabalho_Codigo, A5AreaTrabalho_Codigo, AV6WWPContext, AV29Pgmname, A2127FatoresImpacto_Codigo) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         AV25WWFatoresImpactoDS_1_Areatrabalho_codigo = AV14AreaTrabalho_Codigo;
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14AreaTrabalho_Codigo, A5AreaTrabalho_Codigo, AV6WWPContext, AV29Pgmname, A2127FatoresImpacto_Codigo) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         AV25WWFatoresImpactoDS_1_Areatrabalho_codigo = AV14AreaTrabalho_Codigo;
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14AreaTrabalho_Codigo, A5AreaTrabalho_Codigo, AV6WWPContext, AV29Pgmname, A2127FatoresImpacto_Codigo) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         AV25WWFatoresImpactoDS_1_Areatrabalho_codigo = AV14AreaTrabalho_Codigo;
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14AreaTrabalho_Codigo, A5AreaTrabalho_Codigo, AV6WWPContext, AV29Pgmname, A2127FatoresImpacto_Codigo) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         AV25WWFatoresImpactoDS_1_Areatrabalho_codigo = AV14AreaTrabalho_Codigo;
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV14AreaTrabalho_Codigo, A5AreaTrabalho_Codigo, AV6WWPContext, AV29Pgmname, A2127FatoresImpacto_Codigo) ;
         }
         return (int)(0) ;
      }

      protected void STRUPSX0( )
      {
         /* Before Start, stand alone formulas. */
         AV29Pgmname = "WWFatoresImpacto";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E13SX2 */
         E13SX2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            if ( ( ( context.localUtil.CToN( cgiGet( edtavAreatrabalho_codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavAreatrabalho_codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vAREATRABALHO_CODIGO");
               GX_FocusControl = edtavAreatrabalho_codigo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV14AreaTrabalho_Codigo = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14AreaTrabalho_Codigo), 6, 0)));
            }
            else
            {
               AV14AreaTrabalho_Codigo = (int)(context.localUtil.CToN( cgiGet( edtavAreatrabalho_codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14AreaTrabalho_Codigo), 6, 0)));
            }
            /* Read saved values. */
            nRC_GXsfl_32 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_32"), ",", "."));
            AV17GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( "vGRIDCURRENTPAGE"), ",", "."));
            AV18GridPageCount = (long)(context.localUtil.CToN( cgiGet( "vGRIDPAGECOUNT"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( "GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( "GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( "GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( "GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( "GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( "GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( "GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( "GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( "GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( "GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( "GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( "GRIDPAGINATIONBAR_Emptygridcaption");
            Gridpaginationbar_Selectedpage = cgiGet( "GRIDPAGINATIONBAR_Selectedpage");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E13SX2 */
         E13SX2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E13SX2( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         Form.Caption = " Configura��o de Fatores de Impacto na Contagem";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E14SX2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'CHECKSECURITYFORACTIONS' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV17GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17GridCurrentPage), 10, 0)));
         AV18GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18GridPageCount), 10, 0)));
         AV25WWFatoresImpactoDS_1_Areatrabalho_codigo = AV14AreaTrabalho_Codigo;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV6WWPContext", AV6WWPContext);
      }

      protected void E11SX2( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV16PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV16PageToGo) ;
         }
      }

      private void E15SX2( )
      {
         /* Grid_Load Routine */
         edtavUpdate_Tooltiptext = "Modifica";
         if ( AV6WWPContext.gxTpr_Update )
         {
            edtavUpdate_Link = formatLink("fatoresimpacto.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A5AreaTrabalho_Codigo) + "," + UrlEncode("" +A2127FatoresImpacto_Codigo);
            AV19Update = context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavUpdate_Internalname, AV19Update);
            AV26Update_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( )));
            edtavUpdate_Enabled = 1;
         }
         else
         {
            edtavUpdate_Link = "";
            AV19Update = context.GetImagePath( "0211baea-a1e2-4bb5-849c-f3afb69b09ee", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavUpdate_Internalname, AV19Update);
            AV26Update_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "0211baea-a1e2-4bb5-849c-f3afb69b09ee", "", context.GetTheme( )));
            edtavUpdate_Enabled = 0;
         }
         edtavDelete_Tooltiptext = "Eliminar";
         if ( AV6WWPContext.gxTpr_Delete )
         {
            edtavDelete_Link = formatLink("fatoresimpacto.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A5AreaTrabalho_Codigo) + "," + UrlEncode("" +A2127FatoresImpacto_Codigo);
            AV20Delete = context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDelete_Internalname, AV20Delete);
            AV27Delete_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )));
            edtavDelete_Enabled = 1;
         }
         else
         {
            edtavDelete_Link = "";
            AV20Delete = context.GetImagePath( "138fdfa8-ae9b-4664-8007-e04d47468418", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDelete_Internalname, AV20Delete);
            AV27Delete_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "138fdfa8-ae9b-4664-8007-e04d47468418", "", context.GetTheme( )));
            edtavDelete_Enabled = 0;
         }
         edtFatoresImpacto_ValorEMCSD_Link = formatLink("viewfatoresimpacto.aspx") + "?" + UrlEncode("" +A5AreaTrabalho_Codigo) + "," + UrlEncode("" +A2127FatoresImpacto_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 32;
         }
         sendrow_322( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_32_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(32, GridRow);
         }
      }

      protected void E12SX2( )
      {
         /* 'DoInsert' Routine */
         context.wjLoc = formatLink("fatoresimpacto.aspx") + "?" + UrlEncode(StringUtil.RTrim("INS")) + "," + UrlEncode("" +0) + "," + UrlEncode("" +0);
         context.wjLocDisableFrm = 1;
      }

      protected void S132( )
      {
         /* 'CHECKSECURITYFORACTIONS' Routine */
         AV22IsFatoresImpacto = true;
         /* Using cursor H00SX4 */
         pr_default.execute(2, new Object[] {AV6WWPContext.gxTpr_Areatrabalho_codigo});
         while ( (pr_default.getStatus(2) != 101) )
         {
            A5AreaTrabalho_Codigo = H00SX4_A5AreaTrabalho_Codigo[0];
            AV22IsFatoresImpacto = false;
            pr_default.readNext(2);
         }
         pr_default.close(2);
         if ( ! ( AV6WWPContext.gxTpr_Insert && AV22IsFatoresImpacto ) )
         {
            imgInsert_Link = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgInsert_Internalname, "Link", imgInsert_Link);
            imgInsert_Bitmap = context.GetImagePath( "13d28f37-c579-4dd9-a404-ba51ab71d1e3", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgInsert_Internalname, "Bitmap", context.convertURL( context.PathToRelativeUrl( imgInsert_Bitmap)));
            imgInsert_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgInsert_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgInsert_Enabled), 5, 0)));
         }
      }

      protected void S122( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV15Session.Get(AV29Pgmname+"GridState"), "") == 0 )
         {
            AV10GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  AV29Pgmname+"GridState"), "");
         }
         else
         {
            AV10GridState.FromXml(AV15Session.Get(AV29Pgmname+"GridState"), "");
         }
         AV30GXV1 = 1;
         while ( AV30GXV1 <= AV10GridState.gxTpr_Filtervalues.Count )
         {
            AV11GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV10GridState.gxTpr_Filtervalues.Item(AV30GXV1));
            if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "AREATRABALHO_CODIGO") == 0 )
            {
               AV14AreaTrabalho_Codigo = (int)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14AreaTrabalho_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14AreaTrabalho_Codigo), 6, 0)));
            }
            AV30GXV1 = (int)(AV30GXV1+1);
         }
         subgrid_gotopage( AV10GridState.gxTpr_Currentpage) ;
      }

      protected void S142( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV10GridState.FromXml(AV15Session.Get(AV29Pgmname+"GridState"), "");
         AV10GridState.gxTpr_Filtervalues.Clear();
         if ( ! (0==AV14AreaTrabalho_Codigo) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "AREATRABALHO_CODIGO";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV14AreaTrabalho_Codigo), 6, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         AV10GridState.gxTpr_Currentpage = (short)(subGrid_Currentpage( ));
         new wwpbaseobjects.savegridstate(context ).execute(  AV29Pgmname+"GridState",  AV10GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_MeetrikaVs3")) ;
      }

      protected void S112( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8TrnContext.gxTpr_Callerobject = AV29Pgmname;
         AV8TrnContext.gxTpr_Callerondelete = true;
         AV8TrnContext.gxTpr_Callerurl = AV7HTTPRequest.ScriptName+"?"+AV7HTTPRequest.QueryString;
         AV8TrnContext.gxTpr_Transactionname = "FatoresImpacto";
         AV15Session.Set("TrnContext", AV8TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_MeetrikaVs3"));
      }

      protected void wb_table1_2_SX2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_SX2( true) ;
         }
         else
         {
            wb_table2_8_SX2( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_SX2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_26_SX2( true) ;
         }
         else
         {
            wb_table3_26_SX2( false) ;
         }
         return  ;
      }

      protected void wb_table3_26_SX2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_SX2e( true) ;
         }
         else
         {
            wb_table1_2_SX2e( false) ;
         }
      }

      protected void wb_table3_26_SX2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "TableGridHeader", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_29_SX2( true) ;
         }
         else
         {
            wb_table4_29_SX2( false) ;
         }
         return  ;
      }

      protected void wb_table4_29_SX2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_26_SX2e( true) ;
         }
         else
         {
            wb_table3_26_SX2e( false) ;
         }
      }

      protected void wb_table4_29_SX2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"DivS\" data-gxgridid=\"32\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "C�digo Area de Trabalho") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "C�digo do Fatores de Impacto") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "EMCSD") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "EMCCD") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "ENCSD") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "ENCCD") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "CMCSD") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "CMCCD") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "CNCSD") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "CNCCD") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", "");
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV19Update));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavUpdate_Enabled), 5, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavUpdate_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavUpdate_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV20Delete));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavDelete_Enabled), 5, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavDelete_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDelete_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A5AreaTrabalho_Codigo), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A2127FatoresImpacto_Codigo), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( A2119FatoresImpacto_ValorEMCSD, 5, 2, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtFatoresImpacto_ValorEMCSD_Link));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( A2120FatoresImpacto_ValorEMCCD, 5, 2, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( A2121FatoresImpacto_ValorENCSD, 5, 2, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( A2122FatoresImpacto_ValorENCCD, 5, 2, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( A2123FatoresImpacto_ValorCMCSD, 5, 2, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( A2124FatoresImpacto_ValorCMCCD, 5, 2, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( A2125FatoresImpacto_ValorCNCSD, 5, 2, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( A2126FatoresImpacto_ValorCNCCD, 5, 2, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 32 )
         {
            wbEnd = 0;
            nRC_GXsfl_32 = (short)(nGXsfl_32_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_29_SX2e( true) ;
         }
         else
         {
            wb_table4_29_SX2e( false) ;
         }
      }

      protected void wb_table2_8_SX2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableheader_Internalname, tblTableheader_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            wb_table5_11_SX2( true) ;
         }
         else
         {
            wb_table5_11_SX2( false) ;
         }
         return  ;
      }

      protected void wb_table5_11_SX2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table6_18_SX2( true) ;
         }
         else
         {
            wb_table6_18_SX2( false) ;
         }
         return  ;
      }

      protected void wb_table6_18_SX2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_SX2e( true) ;
         }
         else
         {
            wb_table2_8_SX2e( false) ;
         }
      }

      protected void wb_table6_18_SX2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "TableFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='Invisible'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblFiltertextareatrabalho_codigo_Internalname, "de Trabalho", "", "", lblFiltertextareatrabalho_codigo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWFatoresImpacto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Invisible'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'',false,'" + sGXsfl_32_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavAreatrabalho_codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV14AreaTrabalho_Codigo), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV14AreaTrabalho_Codigo), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,23);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavAreatrabalho_codigo_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWFatoresImpacto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_18_SX2e( true) ;
         }
         else
         {
            wb_table6_18_SX2e( false) ;
         }
      }

      protected void wb_table5_11_SX2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "Table", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='GridHeaderCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblFatoresimpactotitle_Internalname, "Fatores de Impacto", "", "", lblFatoresimpactotitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_WWFatoresImpacto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 16,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgInsert_Internalname, imgInsert_Bitmap, "", "", "", context.GetTheme( ), 1, imgInsert_Enabled, "", "Inserir", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgInsert_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOINSERT\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWFatoresImpacto.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_11_SX2e( true) ;
         }
         else
         {
            wb_table5_11_SX2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PASX2( ) ;
         WSSX2( ) ;
         WESX2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?202051813455841");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wwfatoresimpacto.js", "?202051813455841");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_322( )
      {
         edtavUpdate_Internalname = "vUPDATE_"+sGXsfl_32_idx;
         edtavDelete_Internalname = "vDELETE_"+sGXsfl_32_idx;
         edtAreaTrabalho_Codigo_Internalname = "AREATRABALHO_CODIGO_"+sGXsfl_32_idx;
         edtFatoresImpacto_Codigo_Internalname = "FATORESIMPACTO_CODIGO_"+sGXsfl_32_idx;
         edtFatoresImpacto_ValorEMCSD_Internalname = "FATORESIMPACTO_VALOREMCSD_"+sGXsfl_32_idx;
         edtFatoresImpacto_ValorEMCCD_Internalname = "FATORESIMPACTO_VALOREMCCD_"+sGXsfl_32_idx;
         edtFatoresImpacto_ValorENCSD_Internalname = "FATORESIMPACTO_VALORENCSD_"+sGXsfl_32_idx;
         edtFatoresImpacto_ValorENCCD_Internalname = "FATORESIMPACTO_VALORENCCD_"+sGXsfl_32_idx;
         edtFatoresImpacto_ValorCMCSD_Internalname = "FATORESIMPACTO_VALORCMCSD_"+sGXsfl_32_idx;
         edtFatoresImpacto_ValorCMCCD_Internalname = "FATORESIMPACTO_VALORCMCCD_"+sGXsfl_32_idx;
         edtFatoresImpacto_ValorCNCSD_Internalname = "FATORESIMPACTO_VALORCNCSD_"+sGXsfl_32_idx;
         edtFatoresImpacto_ValorCNCCD_Internalname = "FATORESIMPACTO_VALORCNCCD_"+sGXsfl_32_idx;
      }

      protected void SubsflControlProps_fel_322( )
      {
         edtavUpdate_Internalname = "vUPDATE_"+sGXsfl_32_fel_idx;
         edtavDelete_Internalname = "vDELETE_"+sGXsfl_32_fel_idx;
         edtAreaTrabalho_Codigo_Internalname = "AREATRABALHO_CODIGO_"+sGXsfl_32_fel_idx;
         edtFatoresImpacto_Codigo_Internalname = "FATORESIMPACTO_CODIGO_"+sGXsfl_32_fel_idx;
         edtFatoresImpacto_ValorEMCSD_Internalname = "FATORESIMPACTO_VALOREMCSD_"+sGXsfl_32_fel_idx;
         edtFatoresImpacto_ValorEMCCD_Internalname = "FATORESIMPACTO_VALOREMCCD_"+sGXsfl_32_fel_idx;
         edtFatoresImpacto_ValorENCSD_Internalname = "FATORESIMPACTO_VALORENCSD_"+sGXsfl_32_fel_idx;
         edtFatoresImpacto_ValorENCCD_Internalname = "FATORESIMPACTO_VALORENCCD_"+sGXsfl_32_fel_idx;
         edtFatoresImpacto_ValorCMCSD_Internalname = "FATORESIMPACTO_VALORCMCSD_"+sGXsfl_32_fel_idx;
         edtFatoresImpacto_ValorCMCCD_Internalname = "FATORESIMPACTO_VALORCMCCD_"+sGXsfl_32_fel_idx;
         edtFatoresImpacto_ValorCNCSD_Internalname = "FATORESIMPACTO_VALORCNCSD_"+sGXsfl_32_fel_idx;
         edtFatoresImpacto_ValorCNCCD_Internalname = "FATORESIMPACTO_VALORCNCCD_"+sGXsfl_32_fel_idx;
      }

      protected void sendrow_322( )
      {
         SubsflControlProps_322( ) ;
         WBSX0( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_32_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_32_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_32_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV19Update_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV19Update))&&String.IsNullOrEmpty(StringUtil.RTrim( AV26Update_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV19Update)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavUpdate_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV19Update)) ? AV26Update_GXI : context.PathToRelativeUrl( AV19Update)),(String)edtavUpdate_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(int)edtavUpdate_Enabled,(String)"",(String)edtavUpdate_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV19Update_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV20Delete_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV20Delete))&&String.IsNullOrEmpty(StringUtil.RTrim( AV27Delete_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV20Delete)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDelete_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV20Delete)) ? AV27Delete_GXI : context.PathToRelativeUrl( AV20Delete)),(String)edtavDelete_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(int)edtavDelete_Enabled,(String)"",(String)edtavDelete_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV20Delete_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtAreaTrabalho_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A5AreaTrabalho_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A5AreaTrabalho_Codigo), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtAreaTrabalho_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)32,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtFatoresImpacto_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A2127FatoresImpacto_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A2127FatoresImpacto_Codigo), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtFatoresImpacto_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)32,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtFatoresImpacto_ValorEMCSD_Internalname,StringUtil.LTrim( StringUtil.NToC( A2119FatoresImpacto_ValorEMCSD, 5, 2, ",", "")),context.localUtil.Format( A2119FatoresImpacto_ValorEMCSD, "Z9.99"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)edtFatoresImpacto_ValorEMCSD_Link,(String)"",(String)"",(String)"",(String)edtFatoresImpacto_ValorEMCSD_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)5,(short)0,(short)0,(short)32,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtFatoresImpacto_ValorEMCCD_Internalname,StringUtil.LTrim( StringUtil.NToC( A2120FatoresImpacto_ValorEMCCD, 5, 2, ",", "")),context.localUtil.Format( A2120FatoresImpacto_ValorEMCCD, "Z9.99"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtFatoresImpacto_ValorEMCCD_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)5,(short)0,(short)0,(short)32,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtFatoresImpacto_ValorENCSD_Internalname,StringUtil.LTrim( StringUtil.NToC( A2121FatoresImpacto_ValorENCSD, 5, 2, ",", "")),context.localUtil.Format( A2121FatoresImpacto_ValorENCSD, "Z9.99"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtFatoresImpacto_ValorENCSD_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)5,(short)0,(short)0,(short)32,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtFatoresImpacto_ValorENCCD_Internalname,StringUtil.LTrim( StringUtil.NToC( A2122FatoresImpacto_ValorENCCD, 5, 2, ",", "")),context.localUtil.Format( A2122FatoresImpacto_ValorENCCD, "Z9.99"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtFatoresImpacto_ValorENCCD_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)5,(short)0,(short)0,(short)32,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtFatoresImpacto_ValorCMCSD_Internalname,StringUtil.LTrim( StringUtil.NToC( A2123FatoresImpacto_ValorCMCSD, 5, 2, ",", "")),context.localUtil.Format( A2123FatoresImpacto_ValorCMCSD, "Z9.99"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtFatoresImpacto_ValorCMCSD_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)5,(short)0,(short)0,(short)32,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtFatoresImpacto_ValorCMCCD_Internalname,StringUtil.LTrim( StringUtil.NToC( A2124FatoresImpacto_ValorCMCCD, 5, 2, ",", "")),context.localUtil.Format( A2124FatoresImpacto_ValorCMCCD, "Z9.99"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtFatoresImpacto_ValorCMCCD_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)5,(short)0,(short)0,(short)32,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtFatoresImpacto_ValorCNCSD_Internalname,StringUtil.LTrim( StringUtil.NToC( A2125FatoresImpacto_ValorCNCSD, 5, 2, ",", "")),context.localUtil.Format( A2125FatoresImpacto_ValorCNCSD, "Z9.99"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtFatoresImpacto_ValorCNCSD_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)5,(short)0,(short)0,(short)32,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtFatoresImpacto_ValorCNCCD_Internalname,StringUtil.LTrim( StringUtil.NToC( A2126FatoresImpacto_ValorCNCCD, 5, 2, ",", "")),context.localUtil.Format( A2126FatoresImpacto_ValorCNCCD, "Z9.99"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtFatoresImpacto_ValorCNCCD_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)5,(short)0,(short)0,(short)32,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            GxWebStd.gx_hidden_field( context, "gxhash_AREATRABALHO_CODIGO"+"_"+sGXsfl_32_idx, GetSecureSignedToken( sGXsfl_32_idx, context.localUtil.Format( (decimal)(A5AreaTrabalho_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_FATORESIMPACTO_CODIGO"+"_"+sGXsfl_32_idx, GetSecureSignedToken( sGXsfl_32_idx, context.localUtil.Format( (decimal)(A2127FatoresImpacto_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_FATORESIMPACTO_VALOREMCSD"+"_"+sGXsfl_32_idx, GetSecureSignedToken( sGXsfl_32_idx, context.localUtil.Format( A2119FatoresImpacto_ValorEMCSD, "Z9.99")));
            GxWebStd.gx_hidden_field( context, "gxhash_FATORESIMPACTO_VALOREMCCD"+"_"+sGXsfl_32_idx, GetSecureSignedToken( sGXsfl_32_idx, context.localUtil.Format( A2120FatoresImpacto_ValorEMCCD, "Z9.99")));
            GxWebStd.gx_hidden_field( context, "gxhash_FATORESIMPACTO_VALORENCSD"+"_"+sGXsfl_32_idx, GetSecureSignedToken( sGXsfl_32_idx, context.localUtil.Format( A2121FatoresImpacto_ValorENCSD, "Z9.99")));
            GxWebStd.gx_hidden_field( context, "gxhash_FATORESIMPACTO_VALORENCCD"+"_"+sGXsfl_32_idx, GetSecureSignedToken( sGXsfl_32_idx, context.localUtil.Format( A2122FatoresImpacto_ValorENCCD, "Z9.99")));
            GxWebStd.gx_hidden_field( context, "gxhash_FATORESIMPACTO_VALORCMCSD"+"_"+sGXsfl_32_idx, GetSecureSignedToken( sGXsfl_32_idx, context.localUtil.Format( A2123FatoresImpacto_ValorCMCSD, "Z9.99")));
            GxWebStd.gx_hidden_field( context, "gxhash_FATORESIMPACTO_VALORCMCCD"+"_"+sGXsfl_32_idx, GetSecureSignedToken( sGXsfl_32_idx, context.localUtil.Format( A2124FatoresImpacto_ValorCMCCD, "Z9.99")));
            GxWebStd.gx_hidden_field( context, "gxhash_FATORESIMPACTO_VALORCNCSD"+"_"+sGXsfl_32_idx, GetSecureSignedToken( sGXsfl_32_idx, context.localUtil.Format( A2125FatoresImpacto_ValorCNCSD, "Z9.99")));
            GxWebStd.gx_hidden_field( context, "gxhash_FATORESIMPACTO_VALORCNCCD"+"_"+sGXsfl_32_idx, GetSecureSignedToken( sGXsfl_32_idx, context.localUtil.Format( A2126FatoresImpacto_ValorCNCCD, "Z9.99")));
            GridContainer.AddRow(GridRow);
            nGXsfl_32_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_32_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_32_idx+1));
            sGXsfl_32_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_32_idx), 4, 0)), 4, "0");
            SubsflControlProps_322( ) ;
         }
         /* End function sendrow_322 */
      }

      protected void init_default_properties( )
      {
         lblFatoresimpactotitle_Internalname = "FATORESIMPACTOTITLE";
         imgInsert_Internalname = "INSERT";
         tblTableactions_Internalname = "TABLEACTIONS";
         lblFiltertextareatrabalho_codigo_Internalname = "FILTERTEXTAREATRABALHO_CODIGO";
         edtavAreatrabalho_codigo_Internalname = "vAREATRABALHO_CODIGO";
         tblTablefilters_Internalname = "TABLEFILTERS";
         tblTableheader_Internalname = "TABLEHEADER";
         edtavUpdate_Internalname = "vUPDATE";
         edtavDelete_Internalname = "vDELETE";
         edtAreaTrabalho_Codigo_Internalname = "AREATRABALHO_CODIGO";
         edtFatoresImpacto_Codigo_Internalname = "FATORESIMPACTO_CODIGO";
         edtFatoresImpacto_ValorEMCSD_Internalname = "FATORESIMPACTO_VALOREMCSD";
         edtFatoresImpacto_ValorEMCCD_Internalname = "FATORESIMPACTO_VALOREMCCD";
         edtFatoresImpacto_ValorENCSD_Internalname = "FATORESIMPACTO_VALORENCSD";
         edtFatoresImpacto_ValorENCCD_Internalname = "FATORESIMPACTO_VALORENCCD";
         edtFatoresImpacto_ValorCMCSD_Internalname = "FATORESIMPACTO_VALORCMCSD";
         edtFatoresImpacto_ValorCMCCD_Internalname = "FATORESIMPACTO_VALORCMCCD";
         edtFatoresImpacto_ValorCNCSD_Internalname = "FATORESIMPACTO_VALORCNCSD";
         edtFatoresImpacto_ValorCNCCD_Internalname = "FATORESIMPACTO_VALORCNCCD";
         Gridpaginationbar_Internalname = "GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = "GRIDTABLEWITHPAGINATIONBAR";
         tblTablegridheader_Internalname = "TABLEGRIDHEADER";
         tblTablemain_Internalname = "TABLEMAIN";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtFatoresImpacto_ValorCNCCD_Jsonclick = "";
         edtFatoresImpacto_ValorCNCSD_Jsonclick = "";
         edtFatoresImpacto_ValorCMCCD_Jsonclick = "";
         edtFatoresImpacto_ValorCMCSD_Jsonclick = "";
         edtFatoresImpacto_ValorENCCD_Jsonclick = "";
         edtFatoresImpacto_ValorENCSD_Jsonclick = "";
         edtFatoresImpacto_ValorEMCCD_Jsonclick = "";
         edtFatoresImpacto_ValorEMCSD_Jsonclick = "";
         edtFatoresImpacto_Codigo_Jsonclick = "";
         edtAreaTrabalho_Codigo_Jsonclick = "";
         imgInsert_Enabled = 1;
         imgInsert_Bitmap = (String)(context.GetImagePath( "5649fbb8-8ce0-4810-a5ce-bd649ea83c3a", "", context.GetTheme( )));
         edtavAreatrabalho_codigo_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtFatoresImpacto_ValorEMCSD_Link = "";
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = "";
         edtavDelete_Enabled = 1;
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = "";
         edtavUpdate_Enabled = 1;
         subGrid_Class = "WorkWithBorder WorkWith";
         subGrid_Backcolorstyle = 3;
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = " Configura��o de Fatores de Impacto na Contagem";
         subGrid_Rows = 0;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A2127FatoresImpacto_Codigo',fld:'FATORESIMPACTO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV14AreaTrabalho_Codigo',fld:'vAREATRABALHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A5AreaTrabalho_Codigo',fld:'AREATRABALHO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV29Pgmname',fld:'vPGMNAME',pic:'',nv:''}],oparms:[{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV17GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV18GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'imgInsert_Link',ctrl:'INSERT',prop:'Link'},{av:'imgInsert_Enabled',ctrl:'INSERT',prop:'Enabled'}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E11SX2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV14AreaTrabalho_Codigo',fld:'vAREATRABALHO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A5AreaTrabalho_Codigo',fld:'AREATRABALHO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV29Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'A2127FatoresImpacto_Codigo',fld:'FATORESIMPACTO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("GRID.LOAD","{handler:'E15SX2',iparms:[{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A5AreaTrabalho_Codigo',fld:'AREATRABALHO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A2127FatoresImpacto_Codigo',fld:'FATORESIMPACTO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'edtavUpdate_Tooltiptext',ctrl:'vUPDATE',prop:'Tooltiptext'},{av:'edtavUpdate_Link',ctrl:'vUPDATE',prop:'Link'},{av:'AV19Update',fld:'vUPDATE',pic:'',nv:''},{av:'edtavUpdate_Enabled',ctrl:'vUPDATE',prop:'Enabled'},{av:'edtavDelete_Tooltiptext',ctrl:'vDELETE',prop:'Tooltiptext'},{av:'edtavDelete_Link',ctrl:'vDELETE',prop:'Link'},{av:'AV20Delete',fld:'vDELETE',pic:'',nv:''},{av:'edtavDelete_Enabled',ctrl:'vDELETE',prop:'Enabled'},{av:'edtFatoresImpacto_ValorEMCSD_Link',ctrl:'FATORESIMPACTO_VALOREMCSD',prop:'Link'}]}");
         setEventMetadata("'DOINSERT'","{handler:'E12SX2',iparms:[{av:'A5AreaTrabalho_Codigo',fld:'AREATRABALHO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A2127FatoresImpacto_Codigo',fld:'FATORESIMPACTO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Gridpaginationbar_Selectedpage = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV29Pgmname = "";
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV19Update = "";
         AV26Update_GXI = "";
         AV20Delete = "";
         AV27Delete_GXI = "";
         GridContainer = new GXWebGrid( context);
         scmdbuf = "";
         H00SX2_A2126FatoresImpacto_ValorCNCCD = new decimal[1] ;
         H00SX2_A2125FatoresImpacto_ValorCNCSD = new decimal[1] ;
         H00SX2_A2124FatoresImpacto_ValorCMCCD = new decimal[1] ;
         H00SX2_A2123FatoresImpacto_ValorCMCSD = new decimal[1] ;
         H00SX2_A2122FatoresImpacto_ValorENCCD = new decimal[1] ;
         H00SX2_A2121FatoresImpacto_ValorENCSD = new decimal[1] ;
         H00SX2_A2120FatoresImpacto_ValorEMCCD = new decimal[1] ;
         H00SX2_A2119FatoresImpacto_ValorEMCSD = new decimal[1] ;
         H00SX2_A2127FatoresImpacto_Codigo = new int[1] ;
         H00SX2_A5AreaTrabalho_Codigo = new int[1] ;
         H00SX3_AGRID_nRecordCount = new long[1] ;
         GridRow = new GXWebRow();
         H00SX4_A2127FatoresImpacto_Codigo = new int[1] ;
         H00SX4_A5AreaTrabalho_Codigo = new int[1] ;
         imgInsert_Link = "";
         AV15Session = context.GetSession();
         AV10GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV7HTTPRequest = new GxHttpRequest( context);
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblFiltertextareatrabalho_codigo_Jsonclick = "";
         TempTags = "";
         lblFatoresimpactotitle_Jsonclick = "";
         imgInsert_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wwfatoresimpacto__default(),
            new Object[][] {
                new Object[] {
               H00SX2_A2126FatoresImpacto_ValorCNCCD, H00SX2_A2125FatoresImpacto_ValorCNCSD, H00SX2_A2124FatoresImpacto_ValorCMCCD, H00SX2_A2123FatoresImpacto_ValorCMCSD, H00SX2_A2122FatoresImpacto_ValorENCCD, H00SX2_A2121FatoresImpacto_ValorENCSD, H00SX2_A2120FatoresImpacto_ValorEMCCD, H00SX2_A2119FatoresImpacto_ValorEMCSD, H00SX2_A2127FatoresImpacto_Codigo, H00SX2_A5AreaTrabalho_Codigo
               }
               , new Object[] {
               H00SX3_AGRID_nRecordCount
               }
               , new Object[] {
               H00SX4_A2127FatoresImpacto_Codigo, H00SX4_A5AreaTrabalho_Codigo
               }
            }
         );
         AV29Pgmname = "WWFatoresImpacto";
         /* GeneXus formulas. */
         AV29Pgmname = "WWFatoresImpacto";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_32 ;
      private short nGXsfl_32_idx=1 ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_32_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int subGrid_Rows ;
      private int AV14AreaTrabalho_Codigo ;
      private int A5AreaTrabalho_Codigo ;
      private int A2127FatoresImpacto_Codigo ;
      private int Gridpaginationbar_Pagestoshow ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int AV25WWFatoresImpactoDS_1_Areatrabalho_codigo ;
      private int AV16PageToGo ;
      private int edtavUpdate_Enabled ;
      private int edtavDelete_Enabled ;
      private int imgInsert_Enabled ;
      private int AV30GXV1 ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private long GRID_nFirstRecordOnPage ;
      private long AV17GridCurrentPage ;
      private long AV18GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private decimal A2119FatoresImpacto_ValorEMCSD ;
      private decimal A2120FatoresImpacto_ValorEMCCD ;
      private decimal A2121FatoresImpacto_ValorENCSD ;
      private decimal A2122FatoresImpacto_ValorENCCD ;
      private decimal A2123FatoresImpacto_ValorCMCSD ;
      private decimal A2124FatoresImpacto_ValorCMCCD ;
      private decimal A2125FatoresImpacto_ValorCNCSD ;
      private decimal A2126FatoresImpacto_ValorCNCCD ;
      private String Gridpaginationbar_Selectedpage ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_32_idx="0001" ;
      private String AV29Pgmname ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavUpdate_Internalname ;
      private String edtavDelete_Internalname ;
      private String edtAreaTrabalho_Codigo_Internalname ;
      private String edtFatoresImpacto_Codigo_Internalname ;
      private String edtFatoresImpacto_ValorEMCSD_Internalname ;
      private String edtFatoresImpacto_ValorEMCCD_Internalname ;
      private String edtFatoresImpacto_ValorENCSD_Internalname ;
      private String edtFatoresImpacto_ValorENCCD_Internalname ;
      private String edtFatoresImpacto_ValorCMCSD_Internalname ;
      private String edtFatoresImpacto_ValorCMCCD_Internalname ;
      private String edtFatoresImpacto_ValorCNCSD_Internalname ;
      private String edtFatoresImpacto_ValorCNCCD_Internalname ;
      private String edtavAreatrabalho_codigo_Internalname ;
      private String scmdbuf ;
      private String edtavUpdate_Tooltiptext ;
      private String edtavUpdate_Link ;
      private String edtavDelete_Tooltiptext ;
      private String edtavDelete_Link ;
      private String edtFatoresImpacto_ValorEMCSD_Link ;
      private String imgInsert_Link ;
      private String imgInsert_Internalname ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String tblTablegridheader_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTableheader_Internalname ;
      private String tblTablefilters_Internalname ;
      private String lblFiltertextareatrabalho_codigo_Internalname ;
      private String lblFiltertextareatrabalho_codigo_Jsonclick ;
      private String TempTags ;
      private String edtavAreatrabalho_codigo_Jsonclick ;
      private String tblTableactions_Internalname ;
      private String lblFatoresimpactotitle_Internalname ;
      private String lblFatoresimpactotitle_Jsonclick ;
      private String imgInsert_Jsonclick ;
      private String sGXsfl_32_fel_idx="0001" ;
      private String ROClassString ;
      private String edtAreaTrabalho_Codigo_Jsonclick ;
      private String edtFatoresImpacto_Codigo_Jsonclick ;
      private String edtFatoresImpacto_ValorEMCSD_Jsonclick ;
      private String edtFatoresImpacto_ValorEMCCD_Jsonclick ;
      private String edtFatoresImpacto_ValorENCSD_Jsonclick ;
      private String edtFatoresImpacto_ValorENCCD_Jsonclick ;
      private String edtFatoresImpacto_ValorCMCSD_Jsonclick ;
      private String edtFatoresImpacto_ValorCMCCD_Jsonclick ;
      private String edtFatoresImpacto_ValorCNCSD_Jsonclick ;
      private String edtFatoresImpacto_ValorCNCCD_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV22IsFatoresImpacto ;
      private bool AV19Update_IsBlob ;
      private bool AV20Delete_IsBlob ;
      private String AV26Update_GXI ;
      private String AV27Delete_GXI ;
      private String AV19Update ;
      private String AV20Delete ;
      private String imgInsert_Bitmap ;
      private IGxSession AV15Session ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private decimal[] H00SX2_A2126FatoresImpacto_ValorCNCCD ;
      private decimal[] H00SX2_A2125FatoresImpacto_ValorCNCSD ;
      private decimal[] H00SX2_A2124FatoresImpacto_ValorCMCCD ;
      private decimal[] H00SX2_A2123FatoresImpacto_ValorCMCSD ;
      private decimal[] H00SX2_A2122FatoresImpacto_ValorENCCD ;
      private decimal[] H00SX2_A2121FatoresImpacto_ValorENCSD ;
      private decimal[] H00SX2_A2120FatoresImpacto_ValorEMCCD ;
      private decimal[] H00SX2_A2119FatoresImpacto_ValorEMCSD ;
      private int[] H00SX2_A2127FatoresImpacto_Codigo ;
      private int[] H00SX2_A5AreaTrabalho_Codigo ;
      private long[] H00SX3_AGRID_nRecordCount ;
      private int[] H00SX4_A2127FatoresImpacto_Codigo ;
      private int[] H00SX4_A5AreaTrabalho_Codigo ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV7HTTPRequest ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV8TrnContext ;
      private wwpbaseobjects.SdtWWPGridState AV10GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV11GridStateFilterValue ;
   }

   public class wwfatoresimpacto__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00SX2 ;
          prmH00SX2 = new Object[] {
          new Object[] {"@AV6WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH00SX3 ;
          prmH00SX3 = new Object[] {
          new Object[] {"@AV6WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00SX4 ;
          prmH00SX4 = new Object[] {
          new Object[] {"@AV6WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00SX2", "SELECT * FROM (SELECT  [FatoresImpacto_ValorCNCCD], [FatoresImpacto_ValorCNCSD], [FatoresImpacto_ValorCMCCD], [FatoresImpacto_ValorCMCSD], [FatoresImpacto_ValorENCCD], [FatoresImpacto_ValorENCSD], [FatoresImpacto_ValorEMCCD], [FatoresImpacto_ValorEMCSD], [FatoresImpacto_Codigo], [AreaTrabalho_Codigo], ROW_NUMBER() OVER ( ORDER BY [AreaTrabalho_Codigo], [FatoresImpacto_Codigo] ) AS GX_ROW_NUMBER FROM [FatoresImpacto] WITH (NOLOCK) WHERE [AreaTrabalho_Codigo] = @AV6WWPCo_1Areatrabalho_codigo) AS GX_CTE WHERE GX_ROW_NUMBER BETWEEN @GXPagingFrom2 AND @GXPagingTo2 OR @GXPagingTo2 < @GXPagingFrom2 AND GX_ROW_NUMBER >= @GXPagingFrom2",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00SX2,11,0,true,false )
             ,new CursorDef("H00SX3", "SELECT COUNT(*) FROM [FatoresImpacto] WITH (NOLOCK) WHERE [AreaTrabalho_Codigo] = @AV6WWPCo_1Areatrabalho_codigo ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00SX3,1,0,true,false )
             ,new CursorDef("H00SX4", "SELECT [FatoresImpacto_Codigo], [AreaTrabalho_Codigo] FROM [FatoresImpacto] WITH (NOLOCK) WHERE [AreaTrabalho_Codigo] = @AV6WWPCo_1Areatrabalho_codigo ORDER BY [AreaTrabalho_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00SX4,11,0,false,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((decimal[]) buf[0])[0] = rslt.getDecimal(1) ;
                ((decimal[]) buf[1])[0] = rslt.getDecimal(2) ;
                ((decimal[]) buf[2])[0] = rslt.getDecimal(3) ;
                ((decimal[]) buf[3])[0] = rslt.getDecimal(4) ;
                ((decimal[]) buf[4])[0] = rslt.getDecimal(5) ;
                ((decimal[]) buf[5])[0] = rslt.getDecimal(6) ;
                ((decimal[]) buf[6])[0] = rslt.getDecimal(7) ;
                ((decimal[]) buf[7])[0] = rslt.getDecimal(8) ;
                ((int[]) buf[8])[0] = rslt.getInt(9) ;
                ((int[]) buf[9])[0] = rslt.getInt(10) ;
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
