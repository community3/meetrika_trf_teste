/*
               File: PRC_RetornaCodigoVerificador
        Description: PRC_Retorna Codigo Verificador
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:51:36.34
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_retornacodigoverificador : GXProcedure
   {
      public prc_retornacodigoverificador( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_retornacodigoverificador( IGxContext context )
      {
         this.context = context;
         IsMain = false;
      }

      public void release( )
      {
      }

      public void execute( int aP0_Lote_Codigo ,
                           out String aP1_Verificador )
      {
         this.AV9Lote_Codigo = aP0_Lote_Codigo;
         this.AV10Verificador = "" ;
         initialize();
         executePrivate();
         aP1_Verificador=this.AV10Verificador;
      }

      public String executeUdp( int aP0_Lote_Codigo )
      {
         this.AV9Lote_Codigo = aP0_Lote_Codigo;
         this.AV10Verificador = "" ;
         initialize();
         executePrivate();
         aP1_Verificador=this.AV10Verificador;
         return AV10Verificador ;
      }

      public void executeSubmit( int aP0_Lote_Codigo ,
                                 out String aP1_Verificador )
      {
         prc_retornacodigoverificador objprc_retornacodigoverificador;
         objprc_retornacodigoverificador = new prc_retornacodigoverificador();
         objprc_retornacodigoverificador.AV9Lote_Codigo = aP0_Lote_Codigo;
         objprc_retornacodigoverificador.AV10Verificador = "" ;
         objprc_retornacodigoverificador.context.SetSubmitInitialConfig(context);
         objprc_retornacodigoverificador.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_retornacodigoverificador);
         aP1_Verificador=this.AV10Verificador;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_retornacodigoverificador)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV11vrHora = context.localUtil.ServerTime( context, "DEFAULT");
         AV10Verificador = StringUtil.Substring( AV11vrHora, 4, 2) + StringUtil.Trim( StringUtil.Str( (decimal)(AV9Lote_Codigo), 6, 0)) + StringUtil.Substring( AV11vrHora, 7, 2) + StringUtil.Substring( AV11vrHora, 1, 2);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV11vrHora = "";
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV9Lote_Codigo ;
      private String AV10Verificador ;
      private String AV11vrHora ;
      private String aP1_Verificador ;
   }

}
