/*
               File: GetPromptContratoOcorrenciaNotificacaoFilterData
        Description: Get Prompt Contrato Ocorrencia Notificacao Filter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:53:37.17
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getpromptcontratoocorrencianotificacaofilterdata : GXProcedure
   {
      public getpromptcontratoocorrencianotificacaofilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getpromptcontratoocorrencianotificacaofilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV36DDOName = aP0_DDOName;
         this.AV34SearchTxt = aP1_SearchTxt;
         this.AV35SearchTxtTo = aP2_SearchTxtTo;
         this.AV40OptionsJson = "" ;
         this.AV43OptionsDescJson = "" ;
         this.AV45OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV40OptionsJson;
         aP4_OptionsDescJson=this.AV43OptionsDescJson;
         aP5_OptionIndexesJson=this.AV45OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV36DDOName = aP0_DDOName;
         this.AV34SearchTxt = aP1_SearchTxt;
         this.AV35SearchTxtTo = aP2_SearchTxtTo;
         this.AV40OptionsJson = "" ;
         this.AV43OptionsDescJson = "" ;
         this.AV45OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV40OptionsJson;
         aP4_OptionsDescJson=this.AV43OptionsDescJson;
         aP5_OptionIndexesJson=this.AV45OptionIndexesJson;
         return AV45OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getpromptcontratoocorrencianotificacaofilterdata objgetpromptcontratoocorrencianotificacaofilterdata;
         objgetpromptcontratoocorrencianotificacaofilterdata = new getpromptcontratoocorrencianotificacaofilterdata();
         objgetpromptcontratoocorrencianotificacaofilterdata.AV36DDOName = aP0_DDOName;
         objgetpromptcontratoocorrencianotificacaofilterdata.AV34SearchTxt = aP1_SearchTxt;
         objgetpromptcontratoocorrencianotificacaofilterdata.AV35SearchTxtTo = aP2_SearchTxtTo;
         objgetpromptcontratoocorrencianotificacaofilterdata.AV40OptionsJson = "" ;
         objgetpromptcontratoocorrencianotificacaofilterdata.AV43OptionsDescJson = "" ;
         objgetpromptcontratoocorrencianotificacaofilterdata.AV45OptionIndexesJson = "" ;
         objgetpromptcontratoocorrencianotificacaofilterdata.context.SetSubmitInitialConfig(context);
         objgetpromptcontratoocorrencianotificacaofilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetpromptcontratoocorrencianotificacaofilterdata);
         aP3_OptionsJson=this.AV40OptionsJson;
         aP4_OptionsDescJson=this.AV43OptionsDescJson;
         aP5_OptionIndexesJson=this.AV45OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getpromptcontratoocorrencianotificacaofilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV39Options = (IGxCollection)(new GxSimpleCollection());
         AV42OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV44OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV36DDOName), "DDO_CONTRATO_NUMERO") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTRATO_NUMEROOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV36DDOName), "DDO_CONTRATOOCORRENCIANOTIFICACAO_DESCRICAO") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTRATOOCORRENCIANOTIFICACAO_DESCRICAOOPTIONS' */
            S131 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV36DDOName), "DDO_CONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTRATOOCORRENCIANOTIFICACAO_PROTOCOLOOPTIONS' */
            S141 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV36DDOName), "DDO_CONTRATOOCORRENCIANOTIFICACAO_NOME") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTRATOOCORRENCIANOTIFICACAO_NOMEOPTIONS' */
            S151 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV36DDOName), "DDO_CONTRATOOCORRENCIANOTIFICACAO_DOCTO") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTRATOOCORRENCIANOTIFICACAO_DOCTOOPTIONS' */
            S161 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV40OptionsJson = AV39Options.ToJSonString(false);
         AV43OptionsDescJson = AV42OptionsDesc.ToJSonString(false);
         AV45OptionIndexesJson = AV44OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV47Session.Get("PromptContratoOcorrenciaNotificacaoGridState"), "") == 0 )
         {
            AV49GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "PromptContratoOcorrenciaNotificacaoGridState"), "");
         }
         else
         {
            AV49GridState.FromXml(AV47Session.Get("PromptContratoOcorrenciaNotificacaoGridState"), "");
         }
         AV71GXV1 = 1;
         while ( AV71GXV1 <= AV49GridState.gxTpr_Filtervalues.Count )
         {
            AV50GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV49GridState.gxTpr_Filtervalues.Item(AV71GXV1));
            if ( StringUtil.StrCmp(AV50GridStateFilterValue.gxTpr_Name, "TFCONTRATOOCORRENCIANOTIFICACAO_CODIGO") == 0 )
            {
               AV10TFContratoOcorrenciaNotificacao_Codigo = (int)(NumberUtil.Val( AV50GridStateFilterValue.gxTpr_Value, "."));
               AV11TFContratoOcorrenciaNotificacao_Codigo_To = (int)(NumberUtil.Val( AV50GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV50GridStateFilterValue.gxTpr_Name, "TFCONTRATOOCORRENCIA_CODIGO") == 0 )
            {
               AV12TFContratoOcorrencia_Codigo = (int)(NumberUtil.Val( AV50GridStateFilterValue.gxTpr_Value, "."));
               AV13TFContratoOcorrencia_Codigo_To = (int)(NumberUtil.Val( AV50GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV50GridStateFilterValue.gxTpr_Name, "TFCONTRATO_CODIGO") == 0 )
            {
               AV14TFContrato_Codigo = (int)(NumberUtil.Val( AV50GridStateFilterValue.gxTpr_Value, "."));
               AV15TFContrato_Codigo_To = (int)(NumberUtil.Val( AV50GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV50GridStateFilterValue.gxTpr_Name, "TFCONTRATO_NUMERO") == 0 )
            {
               AV16TFContrato_Numero = AV50GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV50GridStateFilterValue.gxTpr_Name, "TFCONTRATO_NUMERO_SEL") == 0 )
            {
               AV17TFContrato_Numero_Sel = AV50GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV50GridStateFilterValue.gxTpr_Name, "TFCONTRATOOCORRENCIANOTIFICACAO_DATA") == 0 )
            {
               AV18TFContratoOcorrenciaNotificacao_Data = context.localUtil.CToD( AV50GridStateFilterValue.gxTpr_Value, 2);
               AV19TFContratoOcorrenciaNotificacao_Data_To = context.localUtil.CToD( AV50GridStateFilterValue.gxTpr_Valueto, 2);
            }
            else if ( StringUtil.StrCmp(AV50GridStateFilterValue.gxTpr_Name, "TFCONTRATOOCORRENCIANOTIFICACAO_PRAZO") == 0 )
            {
               AV20TFContratoOcorrenciaNotificacao_Prazo = (short)(NumberUtil.Val( AV50GridStateFilterValue.gxTpr_Value, "."));
               AV21TFContratoOcorrenciaNotificacao_Prazo_To = (short)(NumberUtil.Val( AV50GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV50GridStateFilterValue.gxTpr_Name, "TFCONTRATOOCORRENCIANOTIFICACAO_DESCRICAO") == 0 )
            {
               AV22TFContratoOcorrenciaNotificacao_Descricao = AV50GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV50GridStateFilterValue.gxTpr_Name, "TFCONTRATOOCORRENCIANOTIFICACAO_DESCRICAO_SEL") == 0 )
            {
               AV23TFContratoOcorrenciaNotificacao_Descricao_Sel = AV50GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV50GridStateFilterValue.gxTpr_Name, "TFCONTRATOOCORRENCIANOTIFICACAO_CUMPRIDO") == 0 )
            {
               AV24TFContratoOcorrenciaNotificacao_Cumprido = context.localUtil.CToD( AV50GridStateFilterValue.gxTpr_Value, 2);
               AV25TFContratoOcorrenciaNotificacao_Cumprido_To = context.localUtil.CToD( AV50GridStateFilterValue.gxTpr_Valueto, 2);
            }
            else if ( StringUtil.StrCmp(AV50GridStateFilterValue.gxTpr_Name, "TFCONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO") == 0 )
            {
               AV26TFContratoOcorrenciaNotificacao_Protocolo = AV50GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV50GridStateFilterValue.gxTpr_Name, "TFCONTRATOOCORRENCIANOTIFICACAO_PROTOCOLO_SEL") == 0 )
            {
               AV27TFContratoOcorrenciaNotificacao_Protocolo_Sel = AV50GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV50GridStateFilterValue.gxTpr_Name, "TFCONTRATOOCORRENCIANOTIFICACAO_RESPONSAVEL") == 0 )
            {
               AV28TFContratoOcorrenciaNotificacao_Responsavel = (int)(NumberUtil.Val( AV50GridStateFilterValue.gxTpr_Value, "."));
               AV29TFContratoOcorrenciaNotificacao_Responsavel_To = (int)(NumberUtil.Val( AV50GridStateFilterValue.gxTpr_Valueto, "."));
            }
            else if ( StringUtil.StrCmp(AV50GridStateFilterValue.gxTpr_Name, "TFCONTRATOOCORRENCIANOTIFICACAO_NOME") == 0 )
            {
               AV30TFContratoOcorrenciaNotificacao_Nome = AV50GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV50GridStateFilterValue.gxTpr_Name, "TFCONTRATOOCORRENCIANOTIFICACAO_NOME_SEL") == 0 )
            {
               AV31TFContratoOcorrenciaNotificacao_Nome_Sel = AV50GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV50GridStateFilterValue.gxTpr_Name, "TFCONTRATOOCORRENCIANOTIFICACAO_DOCTO") == 0 )
            {
               AV32TFContratoOcorrenciaNotificacao_Docto = AV50GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV50GridStateFilterValue.gxTpr_Name, "TFCONTRATOOCORRENCIANOTIFICACAO_DOCTO_SEL") == 0 )
            {
               AV33TFContratoOcorrenciaNotificacao_Docto_Sel = AV50GridStateFilterValue.gxTpr_Value;
            }
            AV71GXV1 = (int)(AV71GXV1+1);
         }
         if ( AV49GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV51GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV49GridState.gxTpr_Dynamicfilters.Item(1));
            AV52DynamicFiltersSelector1 = AV51GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV52DynamicFiltersSelector1, "CONTRATOOCORRENCIANOTIFICACAO_DATA") == 0 )
            {
               AV54ContratoOcorrenciaNotificacao_Data1 = context.localUtil.CToD( AV51GridStateDynamicFilter.gxTpr_Value, 2);
               AV55ContratoOcorrenciaNotificacao_Data_To1 = context.localUtil.CToD( AV51GridStateDynamicFilter.gxTpr_Valueto, 2);
            }
            else if ( StringUtil.StrCmp(AV52DynamicFiltersSelector1, "CONTRATOOCORRENCIANOTIFICACAO_NOME") == 0 )
            {
               AV53DynamicFiltersOperator1 = AV51GridStateDynamicFilter.gxTpr_Operator;
               AV56ContratoOcorrenciaNotificacao_Nome1 = AV51GridStateDynamicFilter.gxTpr_Value;
            }
            if ( AV49GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV57DynamicFiltersEnabled2 = true;
               AV51GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV49GridState.gxTpr_Dynamicfilters.Item(2));
               AV58DynamicFiltersSelector2 = AV51GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV58DynamicFiltersSelector2, "CONTRATOOCORRENCIANOTIFICACAO_DATA") == 0 )
               {
                  AV60ContratoOcorrenciaNotificacao_Data2 = context.localUtil.CToD( AV51GridStateDynamicFilter.gxTpr_Value, 2);
                  AV61ContratoOcorrenciaNotificacao_Data_To2 = context.localUtil.CToD( AV51GridStateDynamicFilter.gxTpr_Valueto, 2);
               }
               else if ( StringUtil.StrCmp(AV58DynamicFiltersSelector2, "CONTRATOOCORRENCIANOTIFICACAO_NOME") == 0 )
               {
                  AV59DynamicFiltersOperator2 = AV51GridStateDynamicFilter.gxTpr_Operator;
                  AV62ContratoOcorrenciaNotificacao_Nome2 = AV51GridStateDynamicFilter.gxTpr_Value;
               }
               if ( AV49GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  AV63DynamicFiltersEnabled3 = true;
                  AV51GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV49GridState.gxTpr_Dynamicfilters.Item(3));
                  AV64DynamicFiltersSelector3 = AV51GridStateDynamicFilter.gxTpr_Selected;
                  if ( StringUtil.StrCmp(AV64DynamicFiltersSelector3, "CONTRATOOCORRENCIANOTIFICACAO_DATA") == 0 )
                  {
                     AV66ContratoOcorrenciaNotificacao_Data3 = context.localUtil.CToD( AV51GridStateDynamicFilter.gxTpr_Value, 2);
                     AV67ContratoOcorrenciaNotificacao_Data_To3 = context.localUtil.CToD( AV51GridStateDynamicFilter.gxTpr_Valueto, 2);
                  }
                  else if ( StringUtil.StrCmp(AV64DynamicFiltersSelector3, "CONTRATOOCORRENCIANOTIFICACAO_NOME") == 0 )
                  {
                     AV65DynamicFiltersOperator3 = AV51GridStateDynamicFilter.gxTpr_Operator;
                     AV68ContratoOcorrenciaNotificacao_Nome3 = AV51GridStateDynamicFilter.gxTpr_Value;
                  }
               }
            }
         }
      }

      protected void S121( )
      {
         /* 'LOADCONTRATO_NUMEROOPTIONS' Routine */
         AV16TFContrato_Numero = AV34SearchTxt;
         AV17TFContrato_Numero_Sel = "";
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              AV52DynamicFiltersSelector1 ,
                                              AV54ContratoOcorrenciaNotificacao_Data1 ,
                                              AV55ContratoOcorrenciaNotificacao_Data_To1 ,
                                              AV53DynamicFiltersOperator1 ,
                                              AV56ContratoOcorrenciaNotificacao_Nome1 ,
                                              AV57DynamicFiltersEnabled2 ,
                                              AV58DynamicFiltersSelector2 ,
                                              AV60ContratoOcorrenciaNotificacao_Data2 ,
                                              AV61ContratoOcorrenciaNotificacao_Data_To2 ,
                                              AV59DynamicFiltersOperator2 ,
                                              AV62ContratoOcorrenciaNotificacao_Nome2 ,
                                              AV63DynamicFiltersEnabled3 ,
                                              AV64DynamicFiltersSelector3 ,
                                              AV66ContratoOcorrenciaNotificacao_Data3 ,
                                              AV67ContratoOcorrenciaNotificacao_Data_To3 ,
                                              AV65DynamicFiltersOperator3 ,
                                              AV68ContratoOcorrenciaNotificacao_Nome3 ,
                                              AV10TFContratoOcorrenciaNotificacao_Codigo ,
                                              AV11TFContratoOcorrenciaNotificacao_Codigo_To ,
                                              AV12TFContratoOcorrencia_Codigo ,
                                              AV13TFContratoOcorrencia_Codigo_To ,
                                              AV14TFContrato_Codigo ,
                                              AV15TFContrato_Codigo_To ,
                                              AV17TFContrato_Numero_Sel ,
                                              AV16TFContrato_Numero ,
                                              AV18TFContratoOcorrenciaNotificacao_Data ,
                                              AV19TFContratoOcorrenciaNotificacao_Data_To ,
                                              AV20TFContratoOcorrenciaNotificacao_Prazo ,
                                              AV21TFContratoOcorrenciaNotificacao_Prazo_To ,
                                              AV23TFContratoOcorrenciaNotificacao_Descricao_Sel ,
                                              AV22TFContratoOcorrenciaNotificacao_Descricao ,
                                              AV24TFContratoOcorrenciaNotificacao_Cumprido ,
                                              AV25TFContratoOcorrenciaNotificacao_Cumprido_To ,
                                              AV27TFContratoOcorrenciaNotificacao_Protocolo_Sel ,
                                              AV26TFContratoOcorrenciaNotificacao_Protocolo ,
                                              AV28TFContratoOcorrenciaNotificacao_Responsavel ,
                                              AV29TFContratoOcorrenciaNotificacao_Responsavel_To ,
                                              AV31TFContratoOcorrenciaNotificacao_Nome_Sel ,
                                              AV30TFContratoOcorrenciaNotificacao_Nome ,
                                              AV33TFContratoOcorrenciaNotificacao_Docto_Sel ,
                                              AV32TFContratoOcorrenciaNotificacao_Docto ,
                                              A298ContratoOcorrenciaNotificacao_Data ,
                                              A304ContratoOcorrenciaNotificacao_Nome ,
                                              A297ContratoOcorrenciaNotificacao_Codigo ,
                                              A294ContratoOcorrencia_Codigo ,
                                              A74Contrato_Codigo ,
                                              A77Contrato_Numero ,
                                              A299ContratoOcorrenciaNotificacao_Prazo ,
                                              A300ContratoOcorrenciaNotificacao_Descricao ,
                                              A301ContratoOcorrenciaNotificacao_Cumprido ,
                                              A302ContratoOcorrenciaNotificacao_Protocolo ,
                                              A303ContratoOcorrenciaNotificacao_Responsavel ,
                                              A306ContratoOcorrenciaNotificacao_Docto },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.SHORT,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING,
                                              TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.STRING, TypeConstants.BOOLEAN
                                              }
         });
         lV56ContratoOcorrenciaNotificacao_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV56ContratoOcorrenciaNotificacao_Nome1), 100, "%");
         lV56ContratoOcorrenciaNotificacao_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV56ContratoOcorrenciaNotificacao_Nome1), 100, "%");
         lV62ContratoOcorrenciaNotificacao_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV62ContratoOcorrenciaNotificacao_Nome2), 100, "%");
         lV62ContratoOcorrenciaNotificacao_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV62ContratoOcorrenciaNotificacao_Nome2), 100, "%");
         lV68ContratoOcorrenciaNotificacao_Nome3 = StringUtil.PadR( StringUtil.RTrim( AV68ContratoOcorrenciaNotificacao_Nome3), 100, "%");
         lV68ContratoOcorrenciaNotificacao_Nome3 = StringUtil.PadR( StringUtil.RTrim( AV68ContratoOcorrenciaNotificacao_Nome3), 100, "%");
         lV16TFContrato_Numero = StringUtil.PadR( StringUtil.RTrim( AV16TFContrato_Numero), 20, "%");
         lV22TFContratoOcorrenciaNotificacao_Descricao = StringUtil.Concat( StringUtil.RTrim( AV22TFContratoOcorrenciaNotificacao_Descricao), "%", "");
         lV26TFContratoOcorrenciaNotificacao_Protocolo = StringUtil.Concat( StringUtil.RTrim( AV26TFContratoOcorrenciaNotificacao_Protocolo), "%", "");
         lV30TFContratoOcorrenciaNotificacao_Nome = StringUtil.PadR( StringUtil.RTrim( AV30TFContratoOcorrenciaNotificacao_Nome), 100, "%");
         lV32TFContratoOcorrenciaNotificacao_Docto = StringUtil.Concat( StringUtil.RTrim( AV32TFContratoOcorrenciaNotificacao_Docto), "%", "");
         /* Using cursor P00KA2 */
         pr_default.execute(0, new Object[] {AV54ContratoOcorrenciaNotificacao_Data1, AV55ContratoOcorrenciaNotificacao_Data_To1, lV56ContratoOcorrenciaNotificacao_Nome1, lV56ContratoOcorrenciaNotificacao_Nome1, AV60ContratoOcorrenciaNotificacao_Data2, AV61ContratoOcorrenciaNotificacao_Data_To2, lV62ContratoOcorrenciaNotificacao_Nome2, lV62ContratoOcorrenciaNotificacao_Nome2, AV66ContratoOcorrenciaNotificacao_Data3, AV67ContratoOcorrenciaNotificacao_Data_To3, lV68ContratoOcorrenciaNotificacao_Nome3, lV68ContratoOcorrenciaNotificacao_Nome3, AV10TFContratoOcorrenciaNotificacao_Codigo, AV11TFContratoOcorrenciaNotificacao_Codigo_To, AV12TFContratoOcorrencia_Codigo, AV13TFContratoOcorrencia_Codigo_To, AV14TFContrato_Codigo, AV15TFContrato_Codigo_To, lV16TFContrato_Numero, AV17TFContrato_Numero_Sel, AV18TFContratoOcorrenciaNotificacao_Data, AV19TFContratoOcorrenciaNotificacao_Data_To, AV20TFContratoOcorrenciaNotificacao_Prazo, AV21TFContratoOcorrenciaNotificacao_Prazo_To, lV22TFContratoOcorrenciaNotificacao_Descricao, AV23TFContratoOcorrenciaNotificacao_Descricao_Sel, AV24TFContratoOcorrenciaNotificacao_Cumprido, AV25TFContratoOcorrenciaNotificacao_Cumprido_To, lV26TFContratoOcorrenciaNotificacao_Protocolo, AV27TFContratoOcorrenciaNotificacao_Protocolo_Sel, AV28TFContratoOcorrenciaNotificacao_Responsavel, AV29TFContratoOcorrenciaNotificacao_Responsavel_To, lV30TFContratoOcorrenciaNotificacao_Nome, AV31TFContratoOcorrenciaNotificacao_Nome_Sel, lV32TFContratoOcorrenciaNotificacao_Docto, AV33TFContratoOcorrenciaNotificacao_Docto_Sel});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKKA2 = false;
            A77Contrato_Numero = P00KA2_A77Contrato_Numero[0];
            A306ContratoOcorrenciaNotificacao_Docto = P00KA2_A306ContratoOcorrenciaNotificacao_Docto[0];
            n306ContratoOcorrenciaNotificacao_Docto = P00KA2_n306ContratoOcorrenciaNotificacao_Docto[0];
            A303ContratoOcorrenciaNotificacao_Responsavel = P00KA2_A303ContratoOcorrenciaNotificacao_Responsavel[0];
            A302ContratoOcorrenciaNotificacao_Protocolo = P00KA2_A302ContratoOcorrenciaNotificacao_Protocolo[0];
            n302ContratoOcorrenciaNotificacao_Protocolo = P00KA2_n302ContratoOcorrenciaNotificacao_Protocolo[0];
            A301ContratoOcorrenciaNotificacao_Cumprido = P00KA2_A301ContratoOcorrenciaNotificacao_Cumprido[0];
            n301ContratoOcorrenciaNotificacao_Cumprido = P00KA2_n301ContratoOcorrenciaNotificacao_Cumprido[0];
            A300ContratoOcorrenciaNotificacao_Descricao = P00KA2_A300ContratoOcorrenciaNotificacao_Descricao[0];
            A299ContratoOcorrenciaNotificacao_Prazo = P00KA2_A299ContratoOcorrenciaNotificacao_Prazo[0];
            A74Contrato_Codigo = P00KA2_A74Contrato_Codigo[0];
            A294ContratoOcorrencia_Codigo = P00KA2_A294ContratoOcorrencia_Codigo[0];
            A297ContratoOcorrenciaNotificacao_Codigo = P00KA2_A297ContratoOcorrenciaNotificacao_Codigo[0];
            A304ContratoOcorrenciaNotificacao_Nome = P00KA2_A304ContratoOcorrenciaNotificacao_Nome[0];
            n304ContratoOcorrenciaNotificacao_Nome = P00KA2_n304ContratoOcorrenciaNotificacao_Nome[0];
            A298ContratoOcorrenciaNotificacao_Data = P00KA2_A298ContratoOcorrenciaNotificacao_Data[0];
            A306ContratoOcorrenciaNotificacao_Docto = P00KA2_A306ContratoOcorrenciaNotificacao_Docto[0];
            n306ContratoOcorrenciaNotificacao_Docto = P00KA2_n306ContratoOcorrenciaNotificacao_Docto[0];
            A304ContratoOcorrenciaNotificacao_Nome = P00KA2_A304ContratoOcorrenciaNotificacao_Nome[0];
            n304ContratoOcorrenciaNotificacao_Nome = P00KA2_n304ContratoOcorrenciaNotificacao_Nome[0];
            A74Contrato_Codigo = P00KA2_A74Contrato_Codigo[0];
            A77Contrato_Numero = P00KA2_A77Contrato_Numero[0];
            AV46count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( StringUtil.StrCmp(P00KA2_A77Contrato_Numero[0], A77Contrato_Numero) == 0 ) )
            {
               BRKKA2 = false;
               A74Contrato_Codigo = P00KA2_A74Contrato_Codigo[0];
               A294ContratoOcorrencia_Codigo = P00KA2_A294ContratoOcorrencia_Codigo[0];
               A297ContratoOcorrenciaNotificacao_Codigo = P00KA2_A297ContratoOcorrenciaNotificacao_Codigo[0];
               A74Contrato_Codigo = P00KA2_A74Contrato_Codigo[0];
               AV46count = (long)(AV46count+1);
               BRKKA2 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A77Contrato_Numero)) )
            {
               AV38Option = A77Contrato_Numero;
               AV39Options.Add(AV38Option, 0);
               AV44OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV46count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV39Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKKA2 )
            {
               BRKKA2 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      protected void S131( )
      {
         /* 'LOADCONTRATOOCORRENCIANOTIFICACAO_DESCRICAOOPTIONS' Routine */
         AV22TFContratoOcorrenciaNotificacao_Descricao = AV34SearchTxt;
         AV23TFContratoOcorrenciaNotificacao_Descricao_Sel = "";
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV52DynamicFiltersSelector1 ,
                                              AV54ContratoOcorrenciaNotificacao_Data1 ,
                                              AV55ContratoOcorrenciaNotificacao_Data_To1 ,
                                              AV53DynamicFiltersOperator1 ,
                                              AV56ContratoOcorrenciaNotificacao_Nome1 ,
                                              AV57DynamicFiltersEnabled2 ,
                                              AV58DynamicFiltersSelector2 ,
                                              AV60ContratoOcorrenciaNotificacao_Data2 ,
                                              AV61ContratoOcorrenciaNotificacao_Data_To2 ,
                                              AV59DynamicFiltersOperator2 ,
                                              AV62ContratoOcorrenciaNotificacao_Nome2 ,
                                              AV63DynamicFiltersEnabled3 ,
                                              AV64DynamicFiltersSelector3 ,
                                              AV66ContratoOcorrenciaNotificacao_Data3 ,
                                              AV67ContratoOcorrenciaNotificacao_Data_To3 ,
                                              AV65DynamicFiltersOperator3 ,
                                              AV68ContratoOcorrenciaNotificacao_Nome3 ,
                                              AV10TFContratoOcorrenciaNotificacao_Codigo ,
                                              AV11TFContratoOcorrenciaNotificacao_Codigo_To ,
                                              AV12TFContratoOcorrencia_Codigo ,
                                              AV13TFContratoOcorrencia_Codigo_To ,
                                              AV14TFContrato_Codigo ,
                                              AV15TFContrato_Codigo_To ,
                                              AV17TFContrato_Numero_Sel ,
                                              AV16TFContrato_Numero ,
                                              AV18TFContratoOcorrenciaNotificacao_Data ,
                                              AV19TFContratoOcorrenciaNotificacao_Data_To ,
                                              AV20TFContratoOcorrenciaNotificacao_Prazo ,
                                              AV21TFContratoOcorrenciaNotificacao_Prazo_To ,
                                              AV23TFContratoOcorrenciaNotificacao_Descricao_Sel ,
                                              AV22TFContratoOcorrenciaNotificacao_Descricao ,
                                              AV24TFContratoOcorrenciaNotificacao_Cumprido ,
                                              AV25TFContratoOcorrenciaNotificacao_Cumprido_To ,
                                              AV27TFContratoOcorrenciaNotificacao_Protocolo_Sel ,
                                              AV26TFContratoOcorrenciaNotificacao_Protocolo ,
                                              AV28TFContratoOcorrenciaNotificacao_Responsavel ,
                                              AV29TFContratoOcorrenciaNotificacao_Responsavel_To ,
                                              AV31TFContratoOcorrenciaNotificacao_Nome_Sel ,
                                              AV30TFContratoOcorrenciaNotificacao_Nome ,
                                              AV33TFContratoOcorrenciaNotificacao_Docto_Sel ,
                                              AV32TFContratoOcorrenciaNotificacao_Docto ,
                                              A298ContratoOcorrenciaNotificacao_Data ,
                                              A304ContratoOcorrenciaNotificacao_Nome ,
                                              A297ContratoOcorrenciaNotificacao_Codigo ,
                                              A294ContratoOcorrencia_Codigo ,
                                              A74Contrato_Codigo ,
                                              A77Contrato_Numero ,
                                              A299ContratoOcorrenciaNotificacao_Prazo ,
                                              A300ContratoOcorrenciaNotificacao_Descricao ,
                                              A301ContratoOcorrenciaNotificacao_Cumprido ,
                                              A302ContratoOcorrenciaNotificacao_Protocolo ,
                                              A303ContratoOcorrenciaNotificacao_Responsavel ,
                                              A306ContratoOcorrenciaNotificacao_Docto },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.SHORT,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING,
                                              TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.STRING, TypeConstants.BOOLEAN
                                              }
         });
         lV56ContratoOcorrenciaNotificacao_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV56ContratoOcorrenciaNotificacao_Nome1), 100, "%");
         lV56ContratoOcorrenciaNotificacao_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV56ContratoOcorrenciaNotificacao_Nome1), 100, "%");
         lV62ContratoOcorrenciaNotificacao_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV62ContratoOcorrenciaNotificacao_Nome2), 100, "%");
         lV62ContratoOcorrenciaNotificacao_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV62ContratoOcorrenciaNotificacao_Nome2), 100, "%");
         lV68ContratoOcorrenciaNotificacao_Nome3 = StringUtil.PadR( StringUtil.RTrim( AV68ContratoOcorrenciaNotificacao_Nome3), 100, "%");
         lV68ContratoOcorrenciaNotificacao_Nome3 = StringUtil.PadR( StringUtil.RTrim( AV68ContratoOcorrenciaNotificacao_Nome3), 100, "%");
         lV16TFContrato_Numero = StringUtil.PadR( StringUtil.RTrim( AV16TFContrato_Numero), 20, "%");
         lV22TFContratoOcorrenciaNotificacao_Descricao = StringUtil.Concat( StringUtil.RTrim( AV22TFContratoOcorrenciaNotificacao_Descricao), "%", "");
         lV26TFContratoOcorrenciaNotificacao_Protocolo = StringUtil.Concat( StringUtil.RTrim( AV26TFContratoOcorrenciaNotificacao_Protocolo), "%", "");
         lV30TFContratoOcorrenciaNotificacao_Nome = StringUtil.PadR( StringUtil.RTrim( AV30TFContratoOcorrenciaNotificacao_Nome), 100, "%");
         lV32TFContratoOcorrenciaNotificacao_Docto = StringUtil.Concat( StringUtil.RTrim( AV32TFContratoOcorrenciaNotificacao_Docto), "%", "");
         /* Using cursor P00KA3 */
         pr_default.execute(1, new Object[] {AV54ContratoOcorrenciaNotificacao_Data1, AV55ContratoOcorrenciaNotificacao_Data_To1, lV56ContratoOcorrenciaNotificacao_Nome1, lV56ContratoOcorrenciaNotificacao_Nome1, AV60ContratoOcorrenciaNotificacao_Data2, AV61ContratoOcorrenciaNotificacao_Data_To2, lV62ContratoOcorrenciaNotificacao_Nome2, lV62ContratoOcorrenciaNotificacao_Nome2, AV66ContratoOcorrenciaNotificacao_Data3, AV67ContratoOcorrenciaNotificacao_Data_To3, lV68ContratoOcorrenciaNotificacao_Nome3, lV68ContratoOcorrenciaNotificacao_Nome3, AV10TFContratoOcorrenciaNotificacao_Codigo, AV11TFContratoOcorrenciaNotificacao_Codigo_To, AV12TFContratoOcorrencia_Codigo, AV13TFContratoOcorrencia_Codigo_To, AV14TFContrato_Codigo, AV15TFContrato_Codigo_To, lV16TFContrato_Numero, AV17TFContrato_Numero_Sel, AV18TFContratoOcorrenciaNotificacao_Data, AV19TFContratoOcorrenciaNotificacao_Data_To, AV20TFContratoOcorrenciaNotificacao_Prazo, AV21TFContratoOcorrenciaNotificacao_Prazo_To, lV22TFContratoOcorrenciaNotificacao_Descricao, AV23TFContratoOcorrenciaNotificacao_Descricao_Sel, AV24TFContratoOcorrenciaNotificacao_Cumprido, AV25TFContratoOcorrenciaNotificacao_Cumprido_To, lV26TFContratoOcorrenciaNotificacao_Protocolo, AV27TFContratoOcorrenciaNotificacao_Protocolo_Sel, AV28TFContratoOcorrenciaNotificacao_Responsavel, AV29TFContratoOcorrenciaNotificacao_Responsavel_To, lV30TFContratoOcorrenciaNotificacao_Nome, AV31TFContratoOcorrenciaNotificacao_Nome_Sel, lV32TFContratoOcorrenciaNotificacao_Docto, AV33TFContratoOcorrenciaNotificacao_Docto_Sel});
         while ( (pr_default.getStatus(1) != 101) )
         {
            BRKKA4 = false;
            A300ContratoOcorrenciaNotificacao_Descricao = P00KA3_A300ContratoOcorrenciaNotificacao_Descricao[0];
            A306ContratoOcorrenciaNotificacao_Docto = P00KA3_A306ContratoOcorrenciaNotificacao_Docto[0];
            n306ContratoOcorrenciaNotificacao_Docto = P00KA3_n306ContratoOcorrenciaNotificacao_Docto[0];
            A303ContratoOcorrenciaNotificacao_Responsavel = P00KA3_A303ContratoOcorrenciaNotificacao_Responsavel[0];
            A302ContratoOcorrenciaNotificacao_Protocolo = P00KA3_A302ContratoOcorrenciaNotificacao_Protocolo[0];
            n302ContratoOcorrenciaNotificacao_Protocolo = P00KA3_n302ContratoOcorrenciaNotificacao_Protocolo[0];
            A301ContratoOcorrenciaNotificacao_Cumprido = P00KA3_A301ContratoOcorrenciaNotificacao_Cumprido[0];
            n301ContratoOcorrenciaNotificacao_Cumprido = P00KA3_n301ContratoOcorrenciaNotificacao_Cumprido[0];
            A299ContratoOcorrenciaNotificacao_Prazo = P00KA3_A299ContratoOcorrenciaNotificacao_Prazo[0];
            A77Contrato_Numero = P00KA3_A77Contrato_Numero[0];
            A74Contrato_Codigo = P00KA3_A74Contrato_Codigo[0];
            A294ContratoOcorrencia_Codigo = P00KA3_A294ContratoOcorrencia_Codigo[0];
            A297ContratoOcorrenciaNotificacao_Codigo = P00KA3_A297ContratoOcorrenciaNotificacao_Codigo[0];
            A304ContratoOcorrenciaNotificacao_Nome = P00KA3_A304ContratoOcorrenciaNotificacao_Nome[0];
            n304ContratoOcorrenciaNotificacao_Nome = P00KA3_n304ContratoOcorrenciaNotificacao_Nome[0];
            A298ContratoOcorrenciaNotificacao_Data = P00KA3_A298ContratoOcorrenciaNotificacao_Data[0];
            A306ContratoOcorrenciaNotificacao_Docto = P00KA3_A306ContratoOcorrenciaNotificacao_Docto[0];
            n306ContratoOcorrenciaNotificacao_Docto = P00KA3_n306ContratoOcorrenciaNotificacao_Docto[0];
            A304ContratoOcorrenciaNotificacao_Nome = P00KA3_A304ContratoOcorrenciaNotificacao_Nome[0];
            n304ContratoOcorrenciaNotificacao_Nome = P00KA3_n304ContratoOcorrenciaNotificacao_Nome[0];
            A74Contrato_Codigo = P00KA3_A74Contrato_Codigo[0];
            A77Contrato_Numero = P00KA3_A77Contrato_Numero[0];
            AV46count = 0;
            while ( (pr_default.getStatus(1) != 101) && ( StringUtil.StrCmp(P00KA3_A300ContratoOcorrenciaNotificacao_Descricao[0], A300ContratoOcorrenciaNotificacao_Descricao) == 0 ) )
            {
               BRKKA4 = false;
               A297ContratoOcorrenciaNotificacao_Codigo = P00KA3_A297ContratoOcorrenciaNotificacao_Codigo[0];
               AV46count = (long)(AV46count+1);
               BRKKA4 = true;
               pr_default.readNext(1);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A300ContratoOcorrenciaNotificacao_Descricao)) )
            {
               AV38Option = A300ContratoOcorrenciaNotificacao_Descricao;
               AV39Options.Add(AV38Option, 0);
               AV44OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV46count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV39Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKKA4 )
            {
               BRKKA4 = true;
               pr_default.readNext(1);
            }
         }
         pr_default.close(1);
      }

      protected void S141( )
      {
         /* 'LOADCONTRATOOCORRENCIANOTIFICACAO_PROTOCOLOOPTIONS' Routine */
         AV26TFContratoOcorrenciaNotificacao_Protocolo = AV34SearchTxt;
         AV27TFContratoOcorrenciaNotificacao_Protocolo_Sel = "";
         pr_default.dynParam(2, new Object[]{ new Object[]{
                                              AV52DynamicFiltersSelector1 ,
                                              AV54ContratoOcorrenciaNotificacao_Data1 ,
                                              AV55ContratoOcorrenciaNotificacao_Data_To1 ,
                                              AV53DynamicFiltersOperator1 ,
                                              AV56ContratoOcorrenciaNotificacao_Nome1 ,
                                              AV57DynamicFiltersEnabled2 ,
                                              AV58DynamicFiltersSelector2 ,
                                              AV60ContratoOcorrenciaNotificacao_Data2 ,
                                              AV61ContratoOcorrenciaNotificacao_Data_To2 ,
                                              AV59DynamicFiltersOperator2 ,
                                              AV62ContratoOcorrenciaNotificacao_Nome2 ,
                                              AV63DynamicFiltersEnabled3 ,
                                              AV64DynamicFiltersSelector3 ,
                                              AV66ContratoOcorrenciaNotificacao_Data3 ,
                                              AV67ContratoOcorrenciaNotificacao_Data_To3 ,
                                              AV65DynamicFiltersOperator3 ,
                                              AV68ContratoOcorrenciaNotificacao_Nome3 ,
                                              AV10TFContratoOcorrenciaNotificacao_Codigo ,
                                              AV11TFContratoOcorrenciaNotificacao_Codigo_To ,
                                              AV12TFContratoOcorrencia_Codigo ,
                                              AV13TFContratoOcorrencia_Codigo_To ,
                                              AV14TFContrato_Codigo ,
                                              AV15TFContrato_Codigo_To ,
                                              AV17TFContrato_Numero_Sel ,
                                              AV16TFContrato_Numero ,
                                              AV18TFContratoOcorrenciaNotificacao_Data ,
                                              AV19TFContratoOcorrenciaNotificacao_Data_To ,
                                              AV20TFContratoOcorrenciaNotificacao_Prazo ,
                                              AV21TFContratoOcorrenciaNotificacao_Prazo_To ,
                                              AV23TFContratoOcorrenciaNotificacao_Descricao_Sel ,
                                              AV22TFContratoOcorrenciaNotificacao_Descricao ,
                                              AV24TFContratoOcorrenciaNotificacao_Cumprido ,
                                              AV25TFContratoOcorrenciaNotificacao_Cumprido_To ,
                                              AV27TFContratoOcorrenciaNotificacao_Protocolo_Sel ,
                                              AV26TFContratoOcorrenciaNotificacao_Protocolo ,
                                              AV28TFContratoOcorrenciaNotificacao_Responsavel ,
                                              AV29TFContratoOcorrenciaNotificacao_Responsavel_To ,
                                              AV31TFContratoOcorrenciaNotificacao_Nome_Sel ,
                                              AV30TFContratoOcorrenciaNotificacao_Nome ,
                                              AV33TFContratoOcorrenciaNotificacao_Docto_Sel ,
                                              AV32TFContratoOcorrenciaNotificacao_Docto ,
                                              A298ContratoOcorrenciaNotificacao_Data ,
                                              A304ContratoOcorrenciaNotificacao_Nome ,
                                              A297ContratoOcorrenciaNotificacao_Codigo ,
                                              A294ContratoOcorrencia_Codigo ,
                                              A74Contrato_Codigo ,
                                              A77Contrato_Numero ,
                                              A299ContratoOcorrenciaNotificacao_Prazo ,
                                              A300ContratoOcorrenciaNotificacao_Descricao ,
                                              A301ContratoOcorrenciaNotificacao_Cumprido ,
                                              A302ContratoOcorrenciaNotificacao_Protocolo ,
                                              A303ContratoOcorrenciaNotificacao_Responsavel ,
                                              A306ContratoOcorrenciaNotificacao_Docto },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.SHORT,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING,
                                              TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.STRING, TypeConstants.BOOLEAN
                                              }
         });
         lV56ContratoOcorrenciaNotificacao_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV56ContratoOcorrenciaNotificacao_Nome1), 100, "%");
         lV56ContratoOcorrenciaNotificacao_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV56ContratoOcorrenciaNotificacao_Nome1), 100, "%");
         lV62ContratoOcorrenciaNotificacao_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV62ContratoOcorrenciaNotificacao_Nome2), 100, "%");
         lV62ContratoOcorrenciaNotificacao_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV62ContratoOcorrenciaNotificacao_Nome2), 100, "%");
         lV68ContratoOcorrenciaNotificacao_Nome3 = StringUtil.PadR( StringUtil.RTrim( AV68ContratoOcorrenciaNotificacao_Nome3), 100, "%");
         lV68ContratoOcorrenciaNotificacao_Nome3 = StringUtil.PadR( StringUtil.RTrim( AV68ContratoOcorrenciaNotificacao_Nome3), 100, "%");
         lV16TFContrato_Numero = StringUtil.PadR( StringUtil.RTrim( AV16TFContrato_Numero), 20, "%");
         lV22TFContratoOcorrenciaNotificacao_Descricao = StringUtil.Concat( StringUtil.RTrim( AV22TFContratoOcorrenciaNotificacao_Descricao), "%", "");
         lV26TFContratoOcorrenciaNotificacao_Protocolo = StringUtil.Concat( StringUtil.RTrim( AV26TFContratoOcorrenciaNotificacao_Protocolo), "%", "");
         lV30TFContratoOcorrenciaNotificacao_Nome = StringUtil.PadR( StringUtil.RTrim( AV30TFContratoOcorrenciaNotificacao_Nome), 100, "%");
         lV32TFContratoOcorrenciaNotificacao_Docto = StringUtil.Concat( StringUtil.RTrim( AV32TFContratoOcorrenciaNotificacao_Docto), "%", "");
         /* Using cursor P00KA4 */
         pr_default.execute(2, new Object[] {AV54ContratoOcorrenciaNotificacao_Data1, AV55ContratoOcorrenciaNotificacao_Data_To1, lV56ContratoOcorrenciaNotificacao_Nome1, lV56ContratoOcorrenciaNotificacao_Nome1, AV60ContratoOcorrenciaNotificacao_Data2, AV61ContratoOcorrenciaNotificacao_Data_To2, lV62ContratoOcorrenciaNotificacao_Nome2, lV62ContratoOcorrenciaNotificacao_Nome2, AV66ContratoOcorrenciaNotificacao_Data3, AV67ContratoOcorrenciaNotificacao_Data_To3, lV68ContratoOcorrenciaNotificacao_Nome3, lV68ContratoOcorrenciaNotificacao_Nome3, AV10TFContratoOcorrenciaNotificacao_Codigo, AV11TFContratoOcorrenciaNotificacao_Codigo_To, AV12TFContratoOcorrencia_Codigo, AV13TFContratoOcorrencia_Codigo_To, AV14TFContrato_Codigo, AV15TFContrato_Codigo_To, lV16TFContrato_Numero, AV17TFContrato_Numero_Sel, AV18TFContratoOcorrenciaNotificacao_Data, AV19TFContratoOcorrenciaNotificacao_Data_To, AV20TFContratoOcorrenciaNotificacao_Prazo, AV21TFContratoOcorrenciaNotificacao_Prazo_To, lV22TFContratoOcorrenciaNotificacao_Descricao, AV23TFContratoOcorrenciaNotificacao_Descricao_Sel, AV24TFContratoOcorrenciaNotificacao_Cumprido, AV25TFContratoOcorrenciaNotificacao_Cumprido_To, lV26TFContratoOcorrenciaNotificacao_Protocolo, AV27TFContratoOcorrenciaNotificacao_Protocolo_Sel, AV28TFContratoOcorrenciaNotificacao_Responsavel, AV29TFContratoOcorrenciaNotificacao_Responsavel_To, lV30TFContratoOcorrenciaNotificacao_Nome, AV31TFContratoOcorrenciaNotificacao_Nome_Sel, lV32TFContratoOcorrenciaNotificacao_Docto, AV33TFContratoOcorrenciaNotificacao_Docto_Sel});
         while ( (pr_default.getStatus(2) != 101) )
         {
            BRKKA6 = false;
            A302ContratoOcorrenciaNotificacao_Protocolo = P00KA4_A302ContratoOcorrenciaNotificacao_Protocolo[0];
            n302ContratoOcorrenciaNotificacao_Protocolo = P00KA4_n302ContratoOcorrenciaNotificacao_Protocolo[0];
            A306ContratoOcorrenciaNotificacao_Docto = P00KA4_A306ContratoOcorrenciaNotificacao_Docto[0];
            n306ContratoOcorrenciaNotificacao_Docto = P00KA4_n306ContratoOcorrenciaNotificacao_Docto[0];
            A303ContratoOcorrenciaNotificacao_Responsavel = P00KA4_A303ContratoOcorrenciaNotificacao_Responsavel[0];
            A301ContratoOcorrenciaNotificacao_Cumprido = P00KA4_A301ContratoOcorrenciaNotificacao_Cumprido[0];
            n301ContratoOcorrenciaNotificacao_Cumprido = P00KA4_n301ContratoOcorrenciaNotificacao_Cumprido[0];
            A300ContratoOcorrenciaNotificacao_Descricao = P00KA4_A300ContratoOcorrenciaNotificacao_Descricao[0];
            A299ContratoOcorrenciaNotificacao_Prazo = P00KA4_A299ContratoOcorrenciaNotificacao_Prazo[0];
            A77Contrato_Numero = P00KA4_A77Contrato_Numero[0];
            A74Contrato_Codigo = P00KA4_A74Contrato_Codigo[0];
            A294ContratoOcorrencia_Codigo = P00KA4_A294ContratoOcorrencia_Codigo[0];
            A297ContratoOcorrenciaNotificacao_Codigo = P00KA4_A297ContratoOcorrenciaNotificacao_Codigo[0];
            A304ContratoOcorrenciaNotificacao_Nome = P00KA4_A304ContratoOcorrenciaNotificacao_Nome[0];
            n304ContratoOcorrenciaNotificacao_Nome = P00KA4_n304ContratoOcorrenciaNotificacao_Nome[0];
            A298ContratoOcorrenciaNotificacao_Data = P00KA4_A298ContratoOcorrenciaNotificacao_Data[0];
            A306ContratoOcorrenciaNotificacao_Docto = P00KA4_A306ContratoOcorrenciaNotificacao_Docto[0];
            n306ContratoOcorrenciaNotificacao_Docto = P00KA4_n306ContratoOcorrenciaNotificacao_Docto[0];
            A304ContratoOcorrenciaNotificacao_Nome = P00KA4_A304ContratoOcorrenciaNotificacao_Nome[0];
            n304ContratoOcorrenciaNotificacao_Nome = P00KA4_n304ContratoOcorrenciaNotificacao_Nome[0];
            A74Contrato_Codigo = P00KA4_A74Contrato_Codigo[0];
            A77Contrato_Numero = P00KA4_A77Contrato_Numero[0];
            AV46count = 0;
            while ( (pr_default.getStatus(2) != 101) && ( StringUtil.StrCmp(P00KA4_A302ContratoOcorrenciaNotificacao_Protocolo[0], A302ContratoOcorrenciaNotificacao_Protocolo) == 0 ) )
            {
               BRKKA6 = false;
               A297ContratoOcorrenciaNotificacao_Codigo = P00KA4_A297ContratoOcorrenciaNotificacao_Codigo[0];
               AV46count = (long)(AV46count+1);
               BRKKA6 = true;
               pr_default.readNext(2);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A302ContratoOcorrenciaNotificacao_Protocolo)) )
            {
               AV38Option = A302ContratoOcorrenciaNotificacao_Protocolo;
               AV39Options.Add(AV38Option, 0);
               AV44OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV46count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV39Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKKA6 )
            {
               BRKKA6 = true;
               pr_default.readNext(2);
            }
         }
         pr_default.close(2);
      }

      protected void S151( )
      {
         /* 'LOADCONTRATOOCORRENCIANOTIFICACAO_NOMEOPTIONS' Routine */
         AV30TFContratoOcorrenciaNotificacao_Nome = AV34SearchTxt;
         AV31TFContratoOcorrenciaNotificacao_Nome_Sel = "";
         pr_default.dynParam(3, new Object[]{ new Object[]{
                                              AV52DynamicFiltersSelector1 ,
                                              AV54ContratoOcorrenciaNotificacao_Data1 ,
                                              AV55ContratoOcorrenciaNotificacao_Data_To1 ,
                                              AV53DynamicFiltersOperator1 ,
                                              AV56ContratoOcorrenciaNotificacao_Nome1 ,
                                              AV57DynamicFiltersEnabled2 ,
                                              AV58DynamicFiltersSelector2 ,
                                              AV60ContratoOcorrenciaNotificacao_Data2 ,
                                              AV61ContratoOcorrenciaNotificacao_Data_To2 ,
                                              AV59DynamicFiltersOperator2 ,
                                              AV62ContratoOcorrenciaNotificacao_Nome2 ,
                                              AV63DynamicFiltersEnabled3 ,
                                              AV64DynamicFiltersSelector3 ,
                                              AV66ContratoOcorrenciaNotificacao_Data3 ,
                                              AV67ContratoOcorrenciaNotificacao_Data_To3 ,
                                              AV65DynamicFiltersOperator3 ,
                                              AV68ContratoOcorrenciaNotificacao_Nome3 ,
                                              AV10TFContratoOcorrenciaNotificacao_Codigo ,
                                              AV11TFContratoOcorrenciaNotificacao_Codigo_To ,
                                              AV12TFContratoOcorrencia_Codigo ,
                                              AV13TFContratoOcorrencia_Codigo_To ,
                                              AV14TFContrato_Codigo ,
                                              AV15TFContrato_Codigo_To ,
                                              AV17TFContrato_Numero_Sel ,
                                              AV16TFContrato_Numero ,
                                              AV18TFContratoOcorrenciaNotificacao_Data ,
                                              AV19TFContratoOcorrenciaNotificacao_Data_To ,
                                              AV20TFContratoOcorrenciaNotificacao_Prazo ,
                                              AV21TFContratoOcorrenciaNotificacao_Prazo_To ,
                                              AV23TFContratoOcorrenciaNotificacao_Descricao_Sel ,
                                              AV22TFContratoOcorrenciaNotificacao_Descricao ,
                                              AV24TFContratoOcorrenciaNotificacao_Cumprido ,
                                              AV25TFContratoOcorrenciaNotificacao_Cumprido_To ,
                                              AV27TFContratoOcorrenciaNotificacao_Protocolo_Sel ,
                                              AV26TFContratoOcorrenciaNotificacao_Protocolo ,
                                              AV28TFContratoOcorrenciaNotificacao_Responsavel ,
                                              AV29TFContratoOcorrenciaNotificacao_Responsavel_To ,
                                              AV31TFContratoOcorrenciaNotificacao_Nome_Sel ,
                                              AV30TFContratoOcorrenciaNotificacao_Nome ,
                                              AV33TFContratoOcorrenciaNotificacao_Docto_Sel ,
                                              AV32TFContratoOcorrenciaNotificacao_Docto ,
                                              A298ContratoOcorrenciaNotificacao_Data ,
                                              A304ContratoOcorrenciaNotificacao_Nome ,
                                              A297ContratoOcorrenciaNotificacao_Codigo ,
                                              A294ContratoOcorrencia_Codigo ,
                                              A74Contrato_Codigo ,
                                              A77Contrato_Numero ,
                                              A299ContratoOcorrenciaNotificacao_Prazo ,
                                              A300ContratoOcorrenciaNotificacao_Descricao ,
                                              A301ContratoOcorrenciaNotificacao_Cumprido ,
                                              A302ContratoOcorrenciaNotificacao_Protocolo ,
                                              A303ContratoOcorrenciaNotificacao_Responsavel ,
                                              A306ContratoOcorrenciaNotificacao_Docto },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.SHORT,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING,
                                              TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.STRING, TypeConstants.BOOLEAN
                                              }
         });
         lV56ContratoOcorrenciaNotificacao_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV56ContratoOcorrenciaNotificacao_Nome1), 100, "%");
         lV56ContratoOcorrenciaNotificacao_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV56ContratoOcorrenciaNotificacao_Nome1), 100, "%");
         lV62ContratoOcorrenciaNotificacao_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV62ContratoOcorrenciaNotificacao_Nome2), 100, "%");
         lV62ContratoOcorrenciaNotificacao_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV62ContratoOcorrenciaNotificacao_Nome2), 100, "%");
         lV68ContratoOcorrenciaNotificacao_Nome3 = StringUtil.PadR( StringUtil.RTrim( AV68ContratoOcorrenciaNotificacao_Nome3), 100, "%");
         lV68ContratoOcorrenciaNotificacao_Nome3 = StringUtil.PadR( StringUtil.RTrim( AV68ContratoOcorrenciaNotificacao_Nome3), 100, "%");
         lV16TFContrato_Numero = StringUtil.PadR( StringUtil.RTrim( AV16TFContrato_Numero), 20, "%");
         lV22TFContratoOcorrenciaNotificacao_Descricao = StringUtil.Concat( StringUtil.RTrim( AV22TFContratoOcorrenciaNotificacao_Descricao), "%", "");
         lV26TFContratoOcorrenciaNotificacao_Protocolo = StringUtil.Concat( StringUtil.RTrim( AV26TFContratoOcorrenciaNotificacao_Protocolo), "%", "");
         lV30TFContratoOcorrenciaNotificacao_Nome = StringUtil.PadR( StringUtil.RTrim( AV30TFContratoOcorrenciaNotificacao_Nome), 100, "%");
         lV32TFContratoOcorrenciaNotificacao_Docto = StringUtil.Concat( StringUtil.RTrim( AV32TFContratoOcorrenciaNotificacao_Docto), "%", "");
         /* Using cursor P00KA5 */
         pr_default.execute(3, new Object[] {AV54ContratoOcorrenciaNotificacao_Data1, AV55ContratoOcorrenciaNotificacao_Data_To1, lV56ContratoOcorrenciaNotificacao_Nome1, lV56ContratoOcorrenciaNotificacao_Nome1, AV60ContratoOcorrenciaNotificacao_Data2, AV61ContratoOcorrenciaNotificacao_Data_To2, lV62ContratoOcorrenciaNotificacao_Nome2, lV62ContratoOcorrenciaNotificacao_Nome2, AV66ContratoOcorrenciaNotificacao_Data3, AV67ContratoOcorrenciaNotificacao_Data_To3, lV68ContratoOcorrenciaNotificacao_Nome3, lV68ContratoOcorrenciaNotificacao_Nome3, AV10TFContratoOcorrenciaNotificacao_Codigo, AV11TFContratoOcorrenciaNotificacao_Codigo_To, AV12TFContratoOcorrencia_Codigo, AV13TFContratoOcorrencia_Codigo_To, AV14TFContrato_Codigo, AV15TFContrato_Codigo_To, lV16TFContrato_Numero, AV17TFContrato_Numero_Sel, AV18TFContratoOcorrenciaNotificacao_Data, AV19TFContratoOcorrenciaNotificacao_Data_To, AV20TFContratoOcorrenciaNotificacao_Prazo, AV21TFContratoOcorrenciaNotificacao_Prazo_To, lV22TFContratoOcorrenciaNotificacao_Descricao, AV23TFContratoOcorrenciaNotificacao_Descricao_Sel, AV24TFContratoOcorrenciaNotificacao_Cumprido, AV25TFContratoOcorrenciaNotificacao_Cumprido_To, lV26TFContratoOcorrenciaNotificacao_Protocolo, AV27TFContratoOcorrenciaNotificacao_Protocolo_Sel, AV28TFContratoOcorrenciaNotificacao_Responsavel, AV29TFContratoOcorrenciaNotificacao_Responsavel_To, lV30TFContratoOcorrenciaNotificacao_Nome, AV31TFContratoOcorrenciaNotificacao_Nome_Sel, lV32TFContratoOcorrenciaNotificacao_Docto, AV33TFContratoOcorrenciaNotificacao_Docto_Sel});
         while ( (pr_default.getStatus(3) != 101) )
         {
            BRKKA8 = false;
            A303ContratoOcorrenciaNotificacao_Responsavel = P00KA5_A303ContratoOcorrenciaNotificacao_Responsavel[0];
            A306ContratoOcorrenciaNotificacao_Docto = P00KA5_A306ContratoOcorrenciaNotificacao_Docto[0];
            n306ContratoOcorrenciaNotificacao_Docto = P00KA5_n306ContratoOcorrenciaNotificacao_Docto[0];
            A302ContratoOcorrenciaNotificacao_Protocolo = P00KA5_A302ContratoOcorrenciaNotificacao_Protocolo[0];
            n302ContratoOcorrenciaNotificacao_Protocolo = P00KA5_n302ContratoOcorrenciaNotificacao_Protocolo[0];
            A301ContratoOcorrenciaNotificacao_Cumprido = P00KA5_A301ContratoOcorrenciaNotificacao_Cumprido[0];
            n301ContratoOcorrenciaNotificacao_Cumprido = P00KA5_n301ContratoOcorrenciaNotificacao_Cumprido[0];
            A300ContratoOcorrenciaNotificacao_Descricao = P00KA5_A300ContratoOcorrenciaNotificacao_Descricao[0];
            A299ContratoOcorrenciaNotificacao_Prazo = P00KA5_A299ContratoOcorrenciaNotificacao_Prazo[0];
            A77Contrato_Numero = P00KA5_A77Contrato_Numero[0];
            A74Contrato_Codigo = P00KA5_A74Contrato_Codigo[0];
            A294ContratoOcorrencia_Codigo = P00KA5_A294ContratoOcorrencia_Codigo[0];
            A297ContratoOcorrenciaNotificacao_Codigo = P00KA5_A297ContratoOcorrenciaNotificacao_Codigo[0];
            A304ContratoOcorrenciaNotificacao_Nome = P00KA5_A304ContratoOcorrenciaNotificacao_Nome[0];
            n304ContratoOcorrenciaNotificacao_Nome = P00KA5_n304ContratoOcorrenciaNotificacao_Nome[0];
            A298ContratoOcorrenciaNotificacao_Data = P00KA5_A298ContratoOcorrenciaNotificacao_Data[0];
            A306ContratoOcorrenciaNotificacao_Docto = P00KA5_A306ContratoOcorrenciaNotificacao_Docto[0];
            n306ContratoOcorrenciaNotificacao_Docto = P00KA5_n306ContratoOcorrenciaNotificacao_Docto[0];
            A304ContratoOcorrenciaNotificacao_Nome = P00KA5_A304ContratoOcorrenciaNotificacao_Nome[0];
            n304ContratoOcorrenciaNotificacao_Nome = P00KA5_n304ContratoOcorrenciaNotificacao_Nome[0];
            A74Contrato_Codigo = P00KA5_A74Contrato_Codigo[0];
            A77Contrato_Numero = P00KA5_A77Contrato_Numero[0];
            AV46count = 0;
            while ( (pr_default.getStatus(3) != 101) && ( P00KA5_A303ContratoOcorrenciaNotificacao_Responsavel[0] == A303ContratoOcorrenciaNotificacao_Responsavel ) )
            {
               BRKKA8 = false;
               A297ContratoOcorrenciaNotificacao_Codigo = P00KA5_A297ContratoOcorrenciaNotificacao_Codigo[0];
               AV46count = (long)(AV46count+1);
               BRKKA8 = true;
               pr_default.readNext(3);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A304ContratoOcorrenciaNotificacao_Nome)) )
            {
               AV38Option = A304ContratoOcorrenciaNotificacao_Nome;
               AV37InsertIndex = 1;
               while ( ( AV37InsertIndex <= AV39Options.Count ) && ( StringUtil.StrCmp(((String)AV39Options.Item(AV37InsertIndex)), AV38Option) < 0 ) )
               {
                  AV37InsertIndex = (int)(AV37InsertIndex+1);
               }
               AV39Options.Add(AV38Option, AV37InsertIndex);
               AV44OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV46count), "Z,ZZZ,ZZZ,ZZ9")), AV37InsertIndex);
            }
            if ( AV39Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKKA8 )
            {
               BRKKA8 = true;
               pr_default.readNext(3);
            }
         }
         pr_default.close(3);
      }

      protected void S161( )
      {
         /* 'LOADCONTRATOOCORRENCIANOTIFICACAO_DOCTOOPTIONS' Routine */
         AV32TFContratoOcorrenciaNotificacao_Docto = AV34SearchTxt;
         AV33TFContratoOcorrenciaNotificacao_Docto_Sel = "";
         pr_default.dynParam(4, new Object[]{ new Object[]{
                                              AV52DynamicFiltersSelector1 ,
                                              AV54ContratoOcorrenciaNotificacao_Data1 ,
                                              AV55ContratoOcorrenciaNotificacao_Data_To1 ,
                                              AV53DynamicFiltersOperator1 ,
                                              AV56ContratoOcorrenciaNotificacao_Nome1 ,
                                              AV57DynamicFiltersEnabled2 ,
                                              AV58DynamicFiltersSelector2 ,
                                              AV60ContratoOcorrenciaNotificacao_Data2 ,
                                              AV61ContratoOcorrenciaNotificacao_Data_To2 ,
                                              AV59DynamicFiltersOperator2 ,
                                              AV62ContratoOcorrenciaNotificacao_Nome2 ,
                                              AV63DynamicFiltersEnabled3 ,
                                              AV64DynamicFiltersSelector3 ,
                                              AV66ContratoOcorrenciaNotificacao_Data3 ,
                                              AV67ContratoOcorrenciaNotificacao_Data_To3 ,
                                              AV65DynamicFiltersOperator3 ,
                                              AV68ContratoOcorrenciaNotificacao_Nome3 ,
                                              AV10TFContratoOcorrenciaNotificacao_Codigo ,
                                              AV11TFContratoOcorrenciaNotificacao_Codigo_To ,
                                              AV12TFContratoOcorrencia_Codigo ,
                                              AV13TFContratoOcorrencia_Codigo_To ,
                                              AV14TFContrato_Codigo ,
                                              AV15TFContrato_Codigo_To ,
                                              AV17TFContrato_Numero_Sel ,
                                              AV16TFContrato_Numero ,
                                              AV18TFContratoOcorrenciaNotificacao_Data ,
                                              AV19TFContratoOcorrenciaNotificacao_Data_To ,
                                              AV20TFContratoOcorrenciaNotificacao_Prazo ,
                                              AV21TFContratoOcorrenciaNotificacao_Prazo_To ,
                                              AV23TFContratoOcorrenciaNotificacao_Descricao_Sel ,
                                              AV22TFContratoOcorrenciaNotificacao_Descricao ,
                                              AV24TFContratoOcorrenciaNotificacao_Cumprido ,
                                              AV25TFContratoOcorrenciaNotificacao_Cumprido_To ,
                                              AV27TFContratoOcorrenciaNotificacao_Protocolo_Sel ,
                                              AV26TFContratoOcorrenciaNotificacao_Protocolo ,
                                              AV28TFContratoOcorrenciaNotificacao_Responsavel ,
                                              AV29TFContratoOcorrenciaNotificacao_Responsavel_To ,
                                              AV31TFContratoOcorrenciaNotificacao_Nome_Sel ,
                                              AV30TFContratoOcorrenciaNotificacao_Nome ,
                                              AV33TFContratoOcorrenciaNotificacao_Docto_Sel ,
                                              AV32TFContratoOcorrenciaNotificacao_Docto ,
                                              A298ContratoOcorrenciaNotificacao_Data ,
                                              A304ContratoOcorrenciaNotificacao_Nome ,
                                              A297ContratoOcorrenciaNotificacao_Codigo ,
                                              A294ContratoOcorrencia_Codigo ,
                                              A74Contrato_Codigo ,
                                              A77Contrato_Numero ,
                                              A299ContratoOcorrenciaNotificacao_Prazo ,
                                              A300ContratoOcorrenciaNotificacao_Descricao ,
                                              A301ContratoOcorrenciaNotificacao_Cumprido ,
                                              A302ContratoOcorrenciaNotificacao_Protocolo ,
                                              A303ContratoOcorrenciaNotificacao_Responsavel ,
                                              A306ContratoOcorrenciaNotificacao_Docto },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.SHORT,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT,
                                              TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT, TypeConstants.INT, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING,
                                              TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.STRING, TypeConstants.BOOLEAN
                                              }
         });
         lV56ContratoOcorrenciaNotificacao_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV56ContratoOcorrenciaNotificacao_Nome1), 100, "%");
         lV56ContratoOcorrenciaNotificacao_Nome1 = StringUtil.PadR( StringUtil.RTrim( AV56ContratoOcorrenciaNotificacao_Nome1), 100, "%");
         lV62ContratoOcorrenciaNotificacao_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV62ContratoOcorrenciaNotificacao_Nome2), 100, "%");
         lV62ContratoOcorrenciaNotificacao_Nome2 = StringUtil.PadR( StringUtil.RTrim( AV62ContratoOcorrenciaNotificacao_Nome2), 100, "%");
         lV68ContratoOcorrenciaNotificacao_Nome3 = StringUtil.PadR( StringUtil.RTrim( AV68ContratoOcorrenciaNotificacao_Nome3), 100, "%");
         lV68ContratoOcorrenciaNotificacao_Nome3 = StringUtil.PadR( StringUtil.RTrim( AV68ContratoOcorrenciaNotificacao_Nome3), 100, "%");
         lV16TFContrato_Numero = StringUtil.PadR( StringUtil.RTrim( AV16TFContrato_Numero), 20, "%");
         lV22TFContratoOcorrenciaNotificacao_Descricao = StringUtil.Concat( StringUtil.RTrim( AV22TFContratoOcorrenciaNotificacao_Descricao), "%", "");
         lV26TFContratoOcorrenciaNotificacao_Protocolo = StringUtil.Concat( StringUtil.RTrim( AV26TFContratoOcorrenciaNotificacao_Protocolo), "%", "");
         lV30TFContratoOcorrenciaNotificacao_Nome = StringUtil.PadR( StringUtil.RTrim( AV30TFContratoOcorrenciaNotificacao_Nome), 100, "%");
         lV32TFContratoOcorrenciaNotificacao_Docto = StringUtil.Concat( StringUtil.RTrim( AV32TFContratoOcorrenciaNotificacao_Docto), "%", "");
         /* Using cursor P00KA6 */
         pr_default.execute(4, new Object[] {AV54ContratoOcorrenciaNotificacao_Data1, AV55ContratoOcorrenciaNotificacao_Data_To1, lV56ContratoOcorrenciaNotificacao_Nome1, lV56ContratoOcorrenciaNotificacao_Nome1, AV60ContratoOcorrenciaNotificacao_Data2, AV61ContratoOcorrenciaNotificacao_Data_To2, lV62ContratoOcorrenciaNotificacao_Nome2, lV62ContratoOcorrenciaNotificacao_Nome2, AV66ContratoOcorrenciaNotificacao_Data3, AV67ContratoOcorrenciaNotificacao_Data_To3, lV68ContratoOcorrenciaNotificacao_Nome3, lV68ContratoOcorrenciaNotificacao_Nome3, AV10TFContratoOcorrenciaNotificacao_Codigo, AV11TFContratoOcorrenciaNotificacao_Codigo_To, AV12TFContratoOcorrencia_Codigo, AV13TFContratoOcorrencia_Codigo_To, AV14TFContrato_Codigo, AV15TFContrato_Codigo_To, lV16TFContrato_Numero, AV17TFContrato_Numero_Sel, AV18TFContratoOcorrenciaNotificacao_Data, AV19TFContratoOcorrenciaNotificacao_Data_To, AV20TFContratoOcorrenciaNotificacao_Prazo, AV21TFContratoOcorrenciaNotificacao_Prazo_To, lV22TFContratoOcorrenciaNotificacao_Descricao, AV23TFContratoOcorrenciaNotificacao_Descricao_Sel, AV24TFContratoOcorrenciaNotificacao_Cumprido, AV25TFContratoOcorrenciaNotificacao_Cumprido_To, lV26TFContratoOcorrenciaNotificacao_Protocolo, AV27TFContratoOcorrenciaNotificacao_Protocolo_Sel, AV28TFContratoOcorrenciaNotificacao_Responsavel, AV29TFContratoOcorrenciaNotificacao_Responsavel_To, lV30TFContratoOcorrenciaNotificacao_Nome, AV31TFContratoOcorrenciaNotificacao_Nome_Sel, lV32TFContratoOcorrenciaNotificacao_Docto, AV33TFContratoOcorrenciaNotificacao_Docto_Sel});
         while ( (pr_default.getStatus(4) != 101) )
         {
            BRKKA10 = false;
            A306ContratoOcorrenciaNotificacao_Docto = P00KA6_A306ContratoOcorrenciaNotificacao_Docto[0];
            n306ContratoOcorrenciaNotificacao_Docto = P00KA6_n306ContratoOcorrenciaNotificacao_Docto[0];
            A303ContratoOcorrenciaNotificacao_Responsavel = P00KA6_A303ContratoOcorrenciaNotificacao_Responsavel[0];
            A302ContratoOcorrenciaNotificacao_Protocolo = P00KA6_A302ContratoOcorrenciaNotificacao_Protocolo[0];
            n302ContratoOcorrenciaNotificacao_Protocolo = P00KA6_n302ContratoOcorrenciaNotificacao_Protocolo[0];
            A301ContratoOcorrenciaNotificacao_Cumprido = P00KA6_A301ContratoOcorrenciaNotificacao_Cumprido[0];
            n301ContratoOcorrenciaNotificacao_Cumprido = P00KA6_n301ContratoOcorrenciaNotificacao_Cumprido[0];
            A300ContratoOcorrenciaNotificacao_Descricao = P00KA6_A300ContratoOcorrenciaNotificacao_Descricao[0];
            A299ContratoOcorrenciaNotificacao_Prazo = P00KA6_A299ContratoOcorrenciaNotificacao_Prazo[0];
            A77Contrato_Numero = P00KA6_A77Contrato_Numero[0];
            A74Contrato_Codigo = P00KA6_A74Contrato_Codigo[0];
            A294ContratoOcorrencia_Codigo = P00KA6_A294ContratoOcorrencia_Codigo[0];
            A297ContratoOcorrenciaNotificacao_Codigo = P00KA6_A297ContratoOcorrenciaNotificacao_Codigo[0];
            A304ContratoOcorrenciaNotificacao_Nome = P00KA6_A304ContratoOcorrenciaNotificacao_Nome[0];
            n304ContratoOcorrenciaNotificacao_Nome = P00KA6_n304ContratoOcorrenciaNotificacao_Nome[0];
            A298ContratoOcorrenciaNotificacao_Data = P00KA6_A298ContratoOcorrenciaNotificacao_Data[0];
            A306ContratoOcorrenciaNotificacao_Docto = P00KA6_A306ContratoOcorrenciaNotificacao_Docto[0];
            n306ContratoOcorrenciaNotificacao_Docto = P00KA6_n306ContratoOcorrenciaNotificacao_Docto[0];
            A304ContratoOcorrenciaNotificacao_Nome = P00KA6_A304ContratoOcorrenciaNotificacao_Nome[0];
            n304ContratoOcorrenciaNotificacao_Nome = P00KA6_n304ContratoOcorrenciaNotificacao_Nome[0];
            A74Contrato_Codigo = P00KA6_A74Contrato_Codigo[0];
            A77Contrato_Numero = P00KA6_A77Contrato_Numero[0];
            AV46count = 0;
            while ( (pr_default.getStatus(4) != 101) && ( StringUtil.StrCmp(P00KA6_A306ContratoOcorrenciaNotificacao_Docto[0], A306ContratoOcorrenciaNotificacao_Docto) == 0 ) )
            {
               BRKKA10 = false;
               A303ContratoOcorrenciaNotificacao_Responsavel = P00KA6_A303ContratoOcorrenciaNotificacao_Responsavel[0];
               A297ContratoOcorrenciaNotificacao_Codigo = P00KA6_A297ContratoOcorrenciaNotificacao_Codigo[0];
               AV46count = (long)(AV46count+1);
               BRKKA10 = true;
               pr_default.readNext(4);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A306ContratoOcorrenciaNotificacao_Docto)) )
            {
               AV38Option = A306ContratoOcorrenciaNotificacao_Docto;
               AV39Options.Add(AV38Option, 0);
               AV44OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV46count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV39Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKKA10 )
            {
               BRKKA10 = true;
               pr_default.readNext(4);
            }
         }
         pr_default.close(4);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV39Options = new GxSimpleCollection();
         AV42OptionsDesc = new GxSimpleCollection();
         AV44OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV47Session = context.GetSession();
         AV49GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV50GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV16TFContrato_Numero = "";
         AV17TFContrato_Numero_Sel = "";
         AV18TFContratoOcorrenciaNotificacao_Data = DateTime.MinValue;
         AV19TFContratoOcorrenciaNotificacao_Data_To = DateTime.MinValue;
         AV22TFContratoOcorrenciaNotificacao_Descricao = "";
         AV23TFContratoOcorrenciaNotificacao_Descricao_Sel = "";
         AV24TFContratoOcorrenciaNotificacao_Cumprido = DateTime.MinValue;
         AV25TFContratoOcorrenciaNotificacao_Cumprido_To = DateTime.MinValue;
         AV26TFContratoOcorrenciaNotificacao_Protocolo = "";
         AV27TFContratoOcorrenciaNotificacao_Protocolo_Sel = "";
         AV30TFContratoOcorrenciaNotificacao_Nome = "";
         AV31TFContratoOcorrenciaNotificacao_Nome_Sel = "";
         AV32TFContratoOcorrenciaNotificacao_Docto = "";
         AV33TFContratoOcorrenciaNotificacao_Docto_Sel = "";
         AV51GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV52DynamicFiltersSelector1 = "";
         AV54ContratoOcorrenciaNotificacao_Data1 = DateTime.MinValue;
         AV55ContratoOcorrenciaNotificacao_Data_To1 = DateTime.MinValue;
         AV56ContratoOcorrenciaNotificacao_Nome1 = "";
         AV58DynamicFiltersSelector2 = "";
         AV60ContratoOcorrenciaNotificacao_Data2 = DateTime.MinValue;
         AV61ContratoOcorrenciaNotificacao_Data_To2 = DateTime.MinValue;
         AV62ContratoOcorrenciaNotificacao_Nome2 = "";
         AV64DynamicFiltersSelector3 = "";
         AV66ContratoOcorrenciaNotificacao_Data3 = DateTime.MinValue;
         AV67ContratoOcorrenciaNotificacao_Data_To3 = DateTime.MinValue;
         AV68ContratoOcorrenciaNotificacao_Nome3 = "";
         scmdbuf = "";
         lV56ContratoOcorrenciaNotificacao_Nome1 = "";
         lV62ContratoOcorrenciaNotificacao_Nome2 = "";
         lV68ContratoOcorrenciaNotificacao_Nome3 = "";
         lV16TFContrato_Numero = "";
         lV22TFContratoOcorrenciaNotificacao_Descricao = "";
         lV26TFContratoOcorrenciaNotificacao_Protocolo = "";
         lV30TFContratoOcorrenciaNotificacao_Nome = "";
         lV32TFContratoOcorrenciaNotificacao_Docto = "";
         A298ContratoOcorrenciaNotificacao_Data = DateTime.MinValue;
         A304ContratoOcorrenciaNotificacao_Nome = "";
         A77Contrato_Numero = "";
         A300ContratoOcorrenciaNotificacao_Descricao = "";
         A301ContratoOcorrenciaNotificacao_Cumprido = DateTime.MinValue;
         A302ContratoOcorrenciaNotificacao_Protocolo = "";
         A306ContratoOcorrenciaNotificacao_Docto = "";
         P00KA2_A77Contrato_Numero = new String[] {""} ;
         P00KA2_A306ContratoOcorrenciaNotificacao_Docto = new String[] {""} ;
         P00KA2_n306ContratoOcorrenciaNotificacao_Docto = new bool[] {false} ;
         P00KA2_A303ContratoOcorrenciaNotificacao_Responsavel = new int[1] ;
         P00KA2_A302ContratoOcorrenciaNotificacao_Protocolo = new String[] {""} ;
         P00KA2_n302ContratoOcorrenciaNotificacao_Protocolo = new bool[] {false} ;
         P00KA2_A301ContratoOcorrenciaNotificacao_Cumprido = new DateTime[] {DateTime.MinValue} ;
         P00KA2_n301ContratoOcorrenciaNotificacao_Cumprido = new bool[] {false} ;
         P00KA2_A300ContratoOcorrenciaNotificacao_Descricao = new String[] {""} ;
         P00KA2_A299ContratoOcorrenciaNotificacao_Prazo = new short[1] ;
         P00KA2_A74Contrato_Codigo = new int[1] ;
         P00KA2_A294ContratoOcorrencia_Codigo = new int[1] ;
         P00KA2_A297ContratoOcorrenciaNotificacao_Codigo = new int[1] ;
         P00KA2_A304ContratoOcorrenciaNotificacao_Nome = new String[] {""} ;
         P00KA2_n304ContratoOcorrenciaNotificacao_Nome = new bool[] {false} ;
         P00KA2_A298ContratoOcorrenciaNotificacao_Data = new DateTime[] {DateTime.MinValue} ;
         AV38Option = "";
         P00KA3_A300ContratoOcorrenciaNotificacao_Descricao = new String[] {""} ;
         P00KA3_A306ContratoOcorrenciaNotificacao_Docto = new String[] {""} ;
         P00KA3_n306ContratoOcorrenciaNotificacao_Docto = new bool[] {false} ;
         P00KA3_A303ContratoOcorrenciaNotificacao_Responsavel = new int[1] ;
         P00KA3_A302ContratoOcorrenciaNotificacao_Protocolo = new String[] {""} ;
         P00KA3_n302ContratoOcorrenciaNotificacao_Protocolo = new bool[] {false} ;
         P00KA3_A301ContratoOcorrenciaNotificacao_Cumprido = new DateTime[] {DateTime.MinValue} ;
         P00KA3_n301ContratoOcorrenciaNotificacao_Cumprido = new bool[] {false} ;
         P00KA3_A299ContratoOcorrenciaNotificacao_Prazo = new short[1] ;
         P00KA3_A77Contrato_Numero = new String[] {""} ;
         P00KA3_A74Contrato_Codigo = new int[1] ;
         P00KA3_A294ContratoOcorrencia_Codigo = new int[1] ;
         P00KA3_A297ContratoOcorrenciaNotificacao_Codigo = new int[1] ;
         P00KA3_A304ContratoOcorrenciaNotificacao_Nome = new String[] {""} ;
         P00KA3_n304ContratoOcorrenciaNotificacao_Nome = new bool[] {false} ;
         P00KA3_A298ContratoOcorrenciaNotificacao_Data = new DateTime[] {DateTime.MinValue} ;
         P00KA4_A302ContratoOcorrenciaNotificacao_Protocolo = new String[] {""} ;
         P00KA4_n302ContratoOcorrenciaNotificacao_Protocolo = new bool[] {false} ;
         P00KA4_A306ContratoOcorrenciaNotificacao_Docto = new String[] {""} ;
         P00KA4_n306ContratoOcorrenciaNotificacao_Docto = new bool[] {false} ;
         P00KA4_A303ContratoOcorrenciaNotificacao_Responsavel = new int[1] ;
         P00KA4_A301ContratoOcorrenciaNotificacao_Cumprido = new DateTime[] {DateTime.MinValue} ;
         P00KA4_n301ContratoOcorrenciaNotificacao_Cumprido = new bool[] {false} ;
         P00KA4_A300ContratoOcorrenciaNotificacao_Descricao = new String[] {""} ;
         P00KA4_A299ContratoOcorrenciaNotificacao_Prazo = new short[1] ;
         P00KA4_A77Contrato_Numero = new String[] {""} ;
         P00KA4_A74Contrato_Codigo = new int[1] ;
         P00KA4_A294ContratoOcorrencia_Codigo = new int[1] ;
         P00KA4_A297ContratoOcorrenciaNotificacao_Codigo = new int[1] ;
         P00KA4_A304ContratoOcorrenciaNotificacao_Nome = new String[] {""} ;
         P00KA4_n304ContratoOcorrenciaNotificacao_Nome = new bool[] {false} ;
         P00KA4_A298ContratoOcorrenciaNotificacao_Data = new DateTime[] {DateTime.MinValue} ;
         P00KA5_A303ContratoOcorrenciaNotificacao_Responsavel = new int[1] ;
         P00KA5_A306ContratoOcorrenciaNotificacao_Docto = new String[] {""} ;
         P00KA5_n306ContratoOcorrenciaNotificacao_Docto = new bool[] {false} ;
         P00KA5_A302ContratoOcorrenciaNotificacao_Protocolo = new String[] {""} ;
         P00KA5_n302ContratoOcorrenciaNotificacao_Protocolo = new bool[] {false} ;
         P00KA5_A301ContratoOcorrenciaNotificacao_Cumprido = new DateTime[] {DateTime.MinValue} ;
         P00KA5_n301ContratoOcorrenciaNotificacao_Cumprido = new bool[] {false} ;
         P00KA5_A300ContratoOcorrenciaNotificacao_Descricao = new String[] {""} ;
         P00KA5_A299ContratoOcorrenciaNotificacao_Prazo = new short[1] ;
         P00KA5_A77Contrato_Numero = new String[] {""} ;
         P00KA5_A74Contrato_Codigo = new int[1] ;
         P00KA5_A294ContratoOcorrencia_Codigo = new int[1] ;
         P00KA5_A297ContratoOcorrenciaNotificacao_Codigo = new int[1] ;
         P00KA5_A304ContratoOcorrenciaNotificacao_Nome = new String[] {""} ;
         P00KA5_n304ContratoOcorrenciaNotificacao_Nome = new bool[] {false} ;
         P00KA5_A298ContratoOcorrenciaNotificacao_Data = new DateTime[] {DateTime.MinValue} ;
         P00KA6_A306ContratoOcorrenciaNotificacao_Docto = new String[] {""} ;
         P00KA6_n306ContratoOcorrenciaNotificacao_Docto = new bool[] {false} ;
         P00KA6_A303ContratoOcorrenciaNotificacao_Responsavel = new int[1] ;
         P00KA6_A302ContratoOcorrenciaNotificacao_Protocolo = new String[] {""} ;
         P00KA6_n302ContratoOcorrenciaNotificacao_Protocolo = new bool[] {false} ;
         P00KA6_A301ContratoOcorrenciaNotificacao_Cumprido = new DateTime[] {DateTime.MinValue} ;
         P00KA6_n301ContratoOcorrenciaNotificacao_Cumprido = new bool[] {false} ;
         P00KA6_A300ContratoOcorrenciaNotificacao_Descricao = new String[] {""} ;
         P00KA6_A299ContratoOcorrenciaNotificacao_Prazo = new short[1] ;
         P00KA6_A77Contrato_Numero = new String[] {""} ;
         P00KA6_A74Contrato_Codigo = new int[1] ;
         P00KA6_A294ContratoOcorrencia_Codigo = new int[1] ;
         P00KA6_A297ContratoOcorrenciaNotificacao_Codigo = new int[1] ;
         P00KA6_A304ContratoOcorrenciaNotificacao_Nome = new String[] {""} ;
         P00KA6_n304ContratoOcorrenciaNotificacao_Nome = new bool[] {false} ;
         P00KA6_A298ContratoOcorrenciaNotificacao_Data = new DateTime[] {DateTime.MinValue} ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getpromptcontratoocorrencianotificacaofilterdata__default(),
            new Object[][] {
                new Object[] {
               P00KA2_A77Contrato_Numero, P00KA2_A306ContratoOcorrenciaNotificacao_Docto, P00KA2_n306ContratoOcorrenciaNotificacao_Docto, P00KA2_A303ContratoOcorrenciaNotificacao_Responsavel, P00KA2_A302ContratoOcorrenciaNotificacao_Protocolo, P00KA2_n302ContratoOcorrenciaNotificacao_Protocolo, P00KA2_A301ContratoOcorrenciaNotificacao_Cumprido, P00KA2_n301ContratoOcorrenciaNotificacao_Cumprido, P00KA2_A300ContratoOcorrenciaNotificacao_Descricao, P00KA2_A299ContratoOcorrenciaNotificacao_Prazo,
               P00KA2_A74Contrato_Codigo, P00KA2_A294ContratoOcorrencia_Codigo, P00KA2_A297ContratoOcorrenciaNotificacao_Codigo, P00KA2_A304ContratoOcorrenciaNotificacao_Nome, P00KA2_n304ContratoOcorrenciaNotificacao_Nome, P00KA2_A298ContratoOcorrenciaNotificacao_Data
               }
               , new Object[] {
               P00KA3_A300ContratoOcorrenciaNotificacao_Descricao, P00KA3_A306ContratoOcorrenciaNotificacao_Docto, P00KA3_n306ContratoOcorrenciaNotificacao_Docto, P00KA3_A303ContratoOcorrenciaNotificacao_Responsavel, P00KA3_A302ContratoOcorrenciaNotificacao_Protocolo, P00KA3_n302ContratoOcorrenciaNotificacao_Protocolo, P00KA3_A301ContratoOcorrenciaNotificacao_Cumprido, P00KA3_n301ContratoOcorrenciaNotificacao_Cumprido, P00KA3_A299ContratoOcorrenciaNotificacao_Prazo, P00KA3_A77Contrato_Numero,
               P00KA3_A74Contrato_Codigo, P00KA3_A294ContratoOcorrencia_Codigo, P00KA3_A297ContratoOcorrenciaNotificacao_Codigo, P00KA3_A304ContratoOcorrenciaNotificacao_Nome, P00KA3_n304ContratoOcorrenciaNotificacao_Nome, P00KA3_A298ContratoOcorrenciaNotificacao_Data
               }
               , new Object[] {
               P00KA4_A302ContratoOcorrenciaNotificacao_Protocolo, P00KA4_n302ContratoOcorrenciaNotificacao_Protocolo, P00KA4_A306ContratoOcorrenciaNotificacao_Docto, P00KA4_n306ContratoOcorrenciaNotificacao_Docto, P00KA4_A303ContratoOcorrenciaNotificacao_Responsavel, P00KA4_A301ContratoOcorrenciaNotificacao_Cumprido, P00KA4_n301ContratoOcorrenciaNotificacao_Cumprido, P00KA4_A300ContratoOcorrenciaNotificacao_Descricao, P00KA4_A299ContratoOcorrenciaNotificacao_Prazo, P00KA4_A77Contrato_Numero,
               P00KA4_A74Contrato_Codigo, P00KA4_A294ContratoOcorrencia_Codigo, P00KA4_A297ContratoOcorrenciaNotificacao_Codigo, P00KA4_A304ContratoOcorrenciaNotificacao_Nome, P00KA4_n304ContratoOcorrenciaNotificacao_Nome, P00KA4_A298ContratoOcorrenciaNotificacao_Data
               }
               , new Object[] {
               P00KA5_A303ContratoOcorrenciaNotificacao_Responsavel, P00KA5_A306ContratoOcorrenciaNotificacao_Docto, P00KA5_n306ContratoOcorrenciaNotificacao_Docto, P00KA5_A302ContratoOcorrenciaNotificacao_Protocolo, P00KA5_n302ContratoOcorrenciaNotificacao_Protocolo, P00KA5_A301ContratoOcorrenciaNotificacao_Cumprido, P00KA5_n301ContratoOcorrenciaNotificacao_Cumprido, P00KA5_A300ContratoOcorrenciaNotificacao_Descricao, P00KA5_A299ContratoOcorrenciaNotificacao_Prazo, P00KA5_A77Contrato_Numero,
               P00KA5_A74Contrato_Codigo, P00KA5_A294ContratoOcorrencia_Codigo, P00KA5_A297ContratoOcorrenciaNotificacao_Codigo, P00KA5_A304ContratoOcorrenciaNotificacao_Nome, P00KA5_n304ContratoOcorrenciaNotificacao_Nome, P00KA5_A298ContratoOcorrenciaNotificacao_Data
               }
               , new Object[] {
               P00KA6_A306ContratoOcorrenciaNotificacao_Docto, P00KA6_n306ContratoOcorrenciaNotificacao_Docto, P00KA6_A303ContratoOcorrenciaNotificacao_Responsavel, P00KA6_A302ContratoOcorrenciaNotificacao_Protocolo, P00KA6_n302ContratoOcorrenciaNotificacao_Protocolo, P00KA6_A301ContratoOcorrenciaNotificacao_Cumprido, P00KA6_n301ContratoOcorrenciaNotificacao_Cumprido, P00KA6_A300ContratoOcorrenciaNotificacao_Descricao, P00KA6_A299ContratoOcorrenciaNotificacao_Prazo, P00KA6_A77Contrato_Numero,
               P00KA6_A74Contrato_Codigo, P00KA6_A294ContratoOcorrencia_Codigo, P00KA6_A297ContratoOcorrenciaNotificacao_Codigo, P00KA6_A304ContratoOcorrenciaNotificacao_Nome, P00KA6_n304ContratoOcorrenciaNotificacao_Nome, P00KA6_A298ContratoOcorrenciaNotificacao_Data
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV20TFContratoOcorrenciaNotificacao_Prazo ;
      private short AV21TFContratoOcorrenciaNotificacao_Prazo_To ;
      private short AV53DynamicFiltersOperator1 ;
      private short AV59DynamicFiltersOperator2 ;
      private short AV65DynamicFiltersOperator3 ;
      private short A299ContratoOcorrenciaNotificacao_Prazo ;
      private int AV71GXV1 ;
      private int AV10TFContratoOcorrenciaNotificacao_Codigo ;
      private int AV11TFContratoOcorrenciaNotificacao_Codigo_To ;
      private int AV12TFContratoOcorrencia_Codigo ;
      private int AV13TFContratoOcorrencia_Codigo_To ;
      private int AV14TFContrato_Codigo ;
      private int AV15TFContrato_Codigo_To ;
      private int AV28TFContratoOcorrenciaNotificacao_Responsavel ;
      private int AV29TFContratoOcorrenciaNotificacao_Responsavel_To ;
      private int A297ContratoOcorrenciaNotificacao_Codigo ;
      private int A294ContratoOcorrencia_Codigo ;
      private int A74Contrato_Codigo ;
      private int A303ContratoOcorrenciaNotificacao_Responsavel ;
      private int AV37InsertIndex ;
      private long AV46count ;
      private String AV16TFContrato_Numero ;
      private String AV17TFContrato_Numero_Sel ;
      private String AV30TFContratoOcorrenciaNotificacao_Nome ;
      private String AV31TFContratoOcorrenciaNotificacao_Nome_Sel ;
      private String AV56ContratoOcorrenciaNotificacao_Nome1 ;
      private String AV62ContratoOcorrenciaNotificacao_Nome2 ;
      private String AV68ContratoOcorrenciaNotificacao_Nome3 ;
      private String scmdbuf ;
      private String lV56ContratoOcorrenciaNotificacao_Nome1 ;
      private String lV62ContratoOcorrenciaNotificacao_Nome2 ;
      private String lV68ContratoOcorrenciaNotificacao_Nome3 ;
      private String lV16TFContrato_Numero ;
      private String lV30TFContratoOcorrenciaNotificacao_Nome ;
      private String A304ContratoOcorrenciaNotificacao_Nome ;
      private String A77Contrato_Numero ;
      private DateTime AV18TFContratoOcorrenciaNotificacao_Data ;
      private DateTime AV19TFContratoOcorrenciaNotificacao_Data_To ;
      private DateTime AV24TFContratoOcorrenciaNotificacao_Cumprido ;
      private DateTime AV25TFContratoOcorrenciaNotificacao_Cumprido_To ;
      private DateTime AV54ContratoOcorrenciaNotificacao_Data1 ;
      private DateTime AV55ContratoOcorrenciaNotificacao_Data_To1 ;
      private DateTime AV60ContratoOcorrenciaNotificacao_Data2 ;
      private DateTime AV61ContratoOcorrenciaNotificacao_Data_To2 ;
      private DateTime AV66ContratoOcorrenciaNotificacao_Data3 ;
      private DateTime AV67ContratoOcorrenciaNotificacao_Data_To3 ;
      private DateTime A298ContratoOcorrenciaNotificacao_Data ;
      private DateTime A301ContratoOcorrenciaNotificacao_Cumprido ;
      private bool returnInSub ;
      private bool AV57DynamicFiltersEnabled2 ;
      private bool AV63DynamicFiltersEnabled3 ;
      private bool BRKKA2 ;
      private bool n306ContratoOcorrenciaNotificacao_Docto ;
      private bool n302ContratoOcorrenciaNotificacao_Protocolo ;
      private bool n301ContratoOcorrenciaNotificacao_Cumprido ;
      private bool n304ContratoOcorrenciaNotificacao_Nome ;
      private bool BRKKA4 ;
      private bool BRKKA6 ;
      private bool BRKKA8 ;
      private bool BRKKA10 ;
      private String AV45OptionIndexesJson ;
      private String AV40OptionsJson ;
      private String AV43OptionsDescJson ;
      private String AV36DDOName ;
      private String AV34SearchTxt ;
      private String AV35SearchTxtTo ;
      private String AV22TFContratoOcorrenciaNotificacao_Descricao ;
      private String AV23TFContratoOcorrenciaNotificacao_Descricao_Sel ;
      private String AV26TFContratoOcorrenciaNotificacao_Protocolo ;
      private String AV27TFContratoOcorrenciaNotificacao_Protocolo_Sel ;
      private String AV32TFContratoOcorrenciaNotificacao_Docto ;
      private String AV33TFContratoOcorrenciaNotificacao_Docto_Sel ;
      private String AV52DynamicFiltersSelector1 ;
      private String AV58DynamicFiltersSelector2 ;
      private String AV64DynamicFiltersSelector3 ;
      private String lV22TFContratoOcorrenciaNotificacao_Descricao ;
      private String lV26TFContratoOcorrenciaNotificacao_Protocolo ;
      private String lV32TFContratoOcorrenciaNotificacao_Docto ;
      private String A300ContratoOcorrenciaNotificacao_Descricao ;
      private String A302ContratoOcorrenciaNotificacao_Protocolo ;
      private String A306ContratoOcorrenciaNotificacao_Docto ;
      private String AV38Option ;
      private IGxSession AV47Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private String[] P00KA2_A77Contrato_Numero ;
      private String[] P00KA2_A306ContratoOcorrenciaNotificacao_Docto ;
      private bool[] P00KA2_n306ContratoOcorrenciaNotificacao_Docto ;
      private int[] P00KA2_A303ContratoOcorrenciaNotificacao_Responsavel ;
      private String[] P00KA2_A302ContratoOcorrenciaNotificacao_Protocolo ;
      private bool[] P00KA2_n302ContratoOcorrenciaNotificacao_Protocolo ;
      private DateTime[] P00KA2_A301ContratoOcorrenciaNotificacao_Cumprido ;
      private bool[] P00KA2_n301ContratoOcorrenciaNotificacao_Cumprido ;
      private String[] P00KA2_A300ContratoOcorrenciaNotificacao_Descricao ;
      private short[] P00KA2_A299ContratoOcorrenciaNotificacao_Prazo ;
      private int[] P00KA2_A74Contrato_Codigo ;
      private int[] P00KA2_A294ContratoOcorrencia_Codigo ;
      private int[] P00KA2_A297ContratoOcorrenciaNotificacao_Codigo ;
      private String[] P00KA2_A304ContratoOcorrenciaNotificacao_Nome ;
      private bool[] P00KA2_n304ContratoOcorrenciaNotificacao_Nome ;
      private DateTime[] P00KA2_A298ContratoOcorrenciaNotificacao_Data ;
      private String[] P00KA3_A300ContratoOcorrenciaNotificacao_Descricao ;
      private String[] P00KA3_A306ContratoOcorrenciaNotificacao_Docto ;
      private bool[] P00KA3_n306ContratoOcorrenciaNotificacao_Docto ;
      private int[] P00KA3_A303ContratoOcorrenciaNotificacao_Responsavel ;
      private String[] P00KA3_A302ContratoOcorrenciaNotificacao_Protocolo ;
      private bool[] P00KA3_n302ContratoOcorrenciaNotificacao_Protocolo ;
      private DateTime[] P00KA3_A301ContratoOcorrenciaNotificacao_Cumprido ;
      private bool[] P00KA3_n301ContratoOcorrenciaNotificacao_Cumprido ;
      private short[] P00KA3_A299ContratoOcorrenciaNotificacao_Prazo ;
      private String[] P00KA3_A77Contrato_Numero ;
      private int[] P00KA3_A74Contrato_Codigo ;
      private int[] P00KA3_A294ContratoOcorrencia_Codigo ;
      private int[] P00KA3_A297ContratoOcorrenciaNotificacao_Codigo ;
      private String[] P00KA3_A304ContratoOcorrenciaNotificacao_Nome ;
      private bool[] P00KA3_n304ContratoOcorrenciaNotificacao_Nome ;
      private DateTime[] P00KA3_A298ContratoOcorrenciaNotificacao_Data ;
      private String[] P00KA4_A302ContratoOcorrenciaNotificacao_Protocolo ;
      private bool[] P00KA4_n302ContratoOcorrenciaNotificacao_Protocolo ;
      private String[] P00KA4_A306ContratoOcorrenciaNotificacao_Docto ;
      private bool[] P00KA4_n306ContratoOcorrenciaNotificacao_Docto ;
      private int[] P00KA4_A303ContratoOcorrenciaNotificacao_Responsavel ;
      private DateTime[] P00KA4_A301ContratoOcorrenciaNotificacao_Cumprido ;
      private bool[] P00KA4_n301ContratoOcorrenciaNotificacao_Cumprido ;
      private String[] P00KA4_A300ContratoOcorrenciaNotificacao_Descricao ;
      private short[] P00KA4_A299ContratoOcorrenciaNotificacao_Prazo ;
      private String[] P00KA4_A77Contrato_Numero ;
      private int[] P00KA4_A74Contrato_Codigo ;
      private int[] P00KA4_A294ContratoOcorrencia_Codigo ;
      private int[] P00KA4_A297ContratoOcorrenciaNotificacao_Codigo ;
      private String[] P00KA4_A304ContratoOcorrenciaNotificacao_Nome ;
      private bool[] P00KA4_n304ContratoOcorrenciaNotificacao_Nome ;
      private DateTime[] P00KA4_A298ContratoOcorrenciaNotificacao_Data ;
      private int[] P00KA5_A303ContratoOcorrenciaNotificacao_Responsavel ;
      private String[] P00KA5_A306ContratoOcorrenciaNotificacao_Docto ;
      private bool[] P00KA5_n306ContratoOcorrenciaNotificacao_Docto ;
      private String[] P00KA5_A302ContratoOcorrenciaNotificacao_Protocolo ;
      private bool[] P00KA5_n302ContratoOcorrenciaNotificacao_Protocolo ;
      private DateTime[] P00KA5_A301ContratoOcorrenciaNotificacao_Cumprido ;
      private bool[] P00KA5_n301ContratoOcorrenciaNotificacao_Cumprido ;
      private String[] P00KA5_A300ContratoOcorrenciaNotificacao_Descricao ;
      private short[] P00KA5_A299ContratoOcorrenciaNotificacao_Prazo ;
      private String[] P00KA5_A77Contrato_Numero ;
      private int[] P00KA5_A74Contrato_Codigo ;
      private int[] P00KA5_A294ContratoOcorrencia_Codigo ;
      private int[] P00KA5_A297ContratoOcorrenciaNotificacao_Codigo ;
      private String[] P00KA5_A304ContratoOcorrenciaNotificacao_Nome ;
      private bool[] P00KA5_n304ContratoOcorrenciaNotificacao_Nome ;
      private DateTime[] P00KA5_A298ContratoOcorrenciaNotificacao_Data ;
      private String[] P00KA6_A306ContratoOcorrenciaNotificacao_Docto ;
      private bool[] P00KA6_n306ContratoOcorrenciaNotificacao_Docto ;
      private int[] P00KA6_A303ContratoOcorrenciaNotificacao_Responsavel ;
      private String[] P00KA6_A302ContratoOcorrenciaNotificacao_Protocolo ;
      private bool[] P00KA6_n302ContratoOcorrenciaNotificacao_Protocolo ;
      private DateTime[] P00KA6_A301ContratoOcorrenciaNotificacao_Cumprido ;
      private bool[] P00KA6_n301ContratoOcorrenciaNotificacao_Cumprido ;
      private String[] P00KA6_A300ContratoOcorrenciaNotificacao_Descricao ;
      private short[] P00KA6_A299ContratoOcorrenciaNotificacao_Prazo ;
      private String[] P00KA6_A77Contrato_Numero ;
      private int[] P00KA6_A74Contrato_Codigo ;
      private int[] P00KA6_A294ContratoOcorrencia_Codigo ;
      private int[] P00KA6_A297ContratoOcorrenciaNotificacao_Codigo ;
      private String[] P00KA6_A304ContratoOcorrenciaNotificacao_Nome ;
      private bool[] P00KA6_n304ContratoOcorrenciaNotificacao_Nome ;
      private DateTime[] P00KA6_A298ContratoOcorrenciaNotificacao_Data ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV39Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV42OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV44OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV49GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV50GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV51GridStateDynamicFilter ;
   }

   public class getpromptcontratoocorrencianotificacaofilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00KA2( IGxContext context ,
                                             String AV52DynamicFiltersSelector1 ,
                                             DateTime AV54ContratoOcorrenciaNotificacao_Data1 ,
                                             DateTime AV55ContratoOcorrenciaNotificacao_Data_To1 ,
                                             short AV53DynamicFiltersOperator1 ,
                                             String AV56ContratoOcorrenciaNotificacao_Nome1 ,
                                             bool AV57DynamicFiltersEnabled2 ,
                                             String AV58DynamicFiltersSelector2 ,
                                             DateTime AV60ContratoOcorrenciaNotificacao_Data2 ,
                                             DateTime AV61ContratoOcorrenciaNotificacao_Data_To2 ,
                                             short AV59DynamicFiltersOperator2 ,
                                             String AV62ContratoOcorrenciaNotificacao_Nome2 ,
                                             bool AV63DynamicFiltersEnabled3 ,
                                             String AV64DynamicFiltersSelector3 ,
                                             DateTime AV66ContratoOcorrenciaNotificacao_Data3 ,
                                             DateTime AV67ContratoOcorrenciaNotificacao_Data_To3 ,
                                             short AV65DynamicFiltersOperator3 ,
                                             String AV68ContratoOcorrenciaNotificacao_Nome3 ,
                                             int AV10TFContratoOcorrenciaNotificacao_Codigo ,
                                             int AV11TFContratoOcorrenciaNotificacao_Codigo_To ,
                                             int AV12TFContratoOcorrencia_Codigo ,
                                             int AV13TFContratoOcorrencia_Codigo_To ,
                                             int AV14TFContrato_Codigo ,
                                             int AV15TFContrato_Codigo_To ,
                                             String AV17TFContrato_Numero_Sel ,
                                             String AV16TFContrato_Numero ,
                                             DateTime AV18TFContratoOcorrenciaNotificacao_Data ,
                                             DateTime AV19TFContratoOcorrenciaNotificacao_Data_To ,
                                             short AV20TFContratoOcorrenciaNotificacao_Prazo ,
                                             short AV21TFContratoOcorrenciaNotificacao_Prazo_To ,
                                             String AV23TFContratoOcorrenciaNotificacao_Descricao_Sel ,
                                             String AV22TFContratoOcorrenciaNotificacao_Descricao ,
                                             DateTime AV24TFContratoOcorrenciaNotificacao_Cumprido ,
                                             DateTime AV25TFContratoOcorrenciaNotificacao_Cumprido_To ,
                                             String AV27TFContratoOcorrenciaNotificacao_Protocolo_Sel ,
                                             String AV26TFContratoOcorrenciaNotificacao_Protocolo ,
                                             int AV28TFContratoOcorrenciaNotificacao_Responsavel ,
                                             int AV29TFContratoOcorrenciaNotificacao_Responsavel_To ,
                                             String AV31TFContratoOcorrenciaNotificacao_Nome_Sel ,
                                             String AV30TFContratoOcorrenciaNotificacao_Nome ,
                                             String AV33TFContratoOcorrenciaNotificacao_Docto_Sel ,
                                             String AV32TFContratoOcorrenciaNotificacao_Docto ,
                                             DateTime A298ContratoOcorrenciaNotificacao_Data ,
                                             String A304ContratoOcorrenciaNotificacao_Nome ,
                                             int A297ContratoOcorrenciaNotificacao_Codigo ,
                                             int A294ContratoOcorrencia_Codigo ,
                                             int A74Contrato_Codigo ,
                                             String A77Contrato_Numero ,
                                             short A299ContratoOcorrenciaNotificacao_Prazo ,
                                             String A300ContratoOcorrenciaNotificacao_Descricao ,
                                             DateTime A301ContratoOcorrenciaNotificacao_Cumprido ,
                                             String A302ContratoOcorrenciaNotificacao_Protocolo ,
                                             int A303ContratoOcorrenciaNotificacao_Responsavel ,
                                             String A306ContratoOcorrenciaNotificacao_Docto )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [36] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT T4.[Contrato_Numero], T2.[Pessoa_Docto] AS ContratoOcorrenciaNotificacao_Docto, T1.[ContratoOcorrenciaNotificacao_Responsavel] AS ContratoOcorrenciaNotificacao_Responsavel, T1.[ContratoOcorrenciaNotificacao_Protocolo], T1.[ContratoOcorrenciaNotificacao_Cumprido], T1.[ContratoOcorrenciaNotificacao_Descricao], T1.[ContratoOcorrenciaNotificacao_Prazo], T3.[Contrato_Codigo], T1.[ContratoOcorrencia_Codigo], T1.[ContratoOcorrenciaNotificacao_Codigo], T2.[Pessoa_Nome] AS ContratoOcorrenciaNotificacao_Nome, T1.[ContratoOcorrenciaNotificacao_Data] FROM ((([ContratoOcorrenciaNotificacao] T1 WITH (NOLOCK) INNER JOIN [Pessoa] T2 WITH (NOLOCK) ON T2.[Pessoa_Codigo] = T1.[ContratoOcorrenciaNotificacao_Responsavel]) INNER JOIN [ContratoOcorrencia] T3 WITH (NOLOCK) ON T3.[ContratoOcorrencia_Codigo] = T1.[ContratoOcorrencia_Codigo]) INNER JOIN [Contrato] T4 WITH (NOLOCK) ON T4.[Contrato_Codigo] = T3.[Contrato_Codigo])";
         if ( ( StringUtil.StrCmp(AV52DynamicFiltersSelector1, "CONTRATOOCORRENCIANOTIFICACAO_DATA") == 0 ) && ( ! (DateTime.MinValue==AV54ContratoOcorrenciaNotificacao_Data1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Data] >= @AV54ContratoOcorrenciaNotificacao_Data1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Data] >= @AV54ContratoOcorrenciaNotificacao_Data1)";
            }
         }
         else
         {
            GXv_int1[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV52DynamicFiltersSelector1, "CONTRATOOCORRENCIANOTIFICACAO_DATA") == 0 ) && ( ! (DateTime.MinValue==AV55ContratoOcorrenciaNotificacao_Data_To1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Data] <= @AV55ContratoOcorrenciaNotificacao_Data_To1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Data] <= @AV55ContratoOcorrenciaNotificacao_Data_To1)";
            }
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV52DynamicFiltersSelector1, "CONTRATOOCORRENCIANOTIFICACAO_NOME") == 0 ) && ( AV53DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV56ContratoOcorrenciaNotificacao_Nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Pessoa_Nome] like @lV56ContratoOcorrenciaNotificacao_Nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Pessoa_Nome] like @lV56ContratoOcorrenciaNotificacao_Nome1)";
            }
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV52DynamicFiltersSelector1, "CONTRATOOCORRENCIANOTIFICACAO_NOME") == 0 ) && ( AV53DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV56ContratoOcorrenciaNotificacao_Nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Pessoa_Nome] like '%' + @lV56ContratoOcorrenciaNotificacao_Nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Pessoa_Nome] like '%' + @lV56ContratoOcorrenciaNotificacao_Nome1)";
            }
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( AV57DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV58DynamicFiltersSelector2, "CONTRATOOCORRENCIANOTIFICACAO_DATA") == 0 ) && ( ! (DateTime.MinValue==AV60ContratoOcorrenciaNotificacao_Data2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Data] >= @AV60ContratoOcorrenciaNotificacao_Data2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Data] >= @AV60ContratoOcorrenciaNotificacao_Data2)";
            }
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( AV57DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV58DynamicFiltersSelector2, "CONTRATOOCORRENCIANOTIFICACAO_DATA") == 0 ) && ( ! (DateTime.MinValue==AV61ContratoOcorrenciaNotificacao_Data_To2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Data] <= @AV61ContratoOcorrenciaNotificacao_Data_To2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Data] <= @AV61ContratoOcorrenciaNotificacao_Data_To2)";
            }
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( AV57DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV58DynamicFiltersSelector2, "CONTRATOOCORRENCIANOTIFICACAO_NOME") == 0 ) && ( AV59DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV62ContratoOcorrenciaNotificacao_Nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Pessoa_Nome] like @lV62ContratoOcorrenciaNotificacao_Nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Pessoa_Nome] like @lV62ContratoOcorrenciaNotificacao_Nome2)";
            }
         }
         else
         {
            GXv_int1[6] = 1;
         }
         if ( AV57DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV58DynamicFiltersSelector2, "CONTRATOOCORRENCIANOTIFICACAO_NOME") == 0 ) && ( AV59DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV62ContratoOcorrenciaNotificacao_Nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Pessoa_Nome] like '%' + @lV62ContratoOcorrenciaNotificacao_Nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Pessoa_Nome] like '%' + @lV62ContratoOcorrenciaNotificacao_Nome2)";
            }
         }
         else
         {
            GXv_int1[7] = 1;
         }
         if ( AV63DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV64DynamicFiltersSelector3, "CONTRATOOCORRENCIANOTIFICACAO_DATA") == 0 ) && ( ! (DateTime.MinValue==AV66ContratoOcorrenciaNotificacao_Data3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Data] >= @AV66ContratoOcorrenciaNotificacao_Data3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Data] >= @AV66ContratoOcorrenciaNotificacao_Data3)";
            }
         }
         else
         {
            GXv_int1[8] = 1;
         }
         if ( AV63DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV64DynamicFiltersSelector3, "CONTRATOOCORRENCIANOTIFICACAO_DATA") == 0 ) && ( ! (DateTime.MinValue==AV67ContratoOcorrenciaNotificacao_Data_To3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Data] <= @AV67ContratoOcorrenciaNotificacao_Data_To3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Data] <= @AV67ContratoOcorrenciaNotificacao_Data_To3)";
            }
         }
         else
         {
            GXv_int1[9] = 1;
         }
         if ( AV63DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV64DynamicFiltersSelector3, "CONTRATOOCORRENCIANOTIFICACAO_NOME") == 0 ) && ( AV65DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV68ContratoOcorrenciaNotificacao_Nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Pessoa_Nome] like @lV68ContratoOcorrenciaNotificacao_Nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Pessoa_Nome] like @lV68ContratoOcorrenciaNotificacao_Nome3)";
            }
         }
         else
         {
            GXv_int1[10] = 1;
         }
         if ( AV63DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV64DynamicFiltersSelector3, "CONTRATOOCORRENCIANOTIFICACAO_NOME") == 0 ) && ( AV65DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV68ContratoOcorrenciaNotificacao_Nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Pessoa_Nome] like '%' + @lV68ContratoOcorrenciaNotificacao_Nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Pessoa_Nome] like '%' + @lV68ContratoOcorrenciaNotificacao_Nome3)";
            }
         }
         else
         {
            GXv_int1[11] = 1;
         }
         if ( ! (0==AV10TFContratoOcorrenciaNotificacao_Codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Codigo] >= @AV10TFContratoOcorrenciaNotificacao_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Codigo] >= @AV10TFContratoOcorrenciaNotificacao_Codigo)";
            }
         }
         else
         {
            GXv_int1[12] = 1;
         }
         if ( ! (0==AV11TFContratoOcorrenciaNotificacao_Codigo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Codigo] <= @AV11TFContratoOcorrenciaNotificacao_Codigo_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Codigo] <= @AV11TFContratoOcorrenciaNotificacao_Codigo_To)";
            }
         }
         else
         {
            GXv_int1[13] = 1;
         }
         if ( ! (0==AV12TFContratoOcorrencia_Codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrencia_Codigo] >= @AV12TFContratoOcorrencia_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrencia_Codigo] >= @AV12TFContratoOcorrencia_Codigo)";
            }
         }
         else
         {
            GXv_int1[14] = 1;
         }
         if ( ! (0==AV13TFContratoOcorrencia_Codigo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrencia_Codigo] <= @AV13TFContratoOcorrencia_Codigo_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrencia_Codigo] <= @AV13TFContratoOcorrencia_Codigo_To)";
            }
         }
         else
         {
            GXv_int1[15] = 1;
         }
         if ( ! (0==AV14TFContrato_Codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Contrato_Codigo] >= @AV14TFContrato_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Contrato_Codigo] >= @AV14TFContrato_Codigo)";
            }
         }
         else
         {
            GXv_int1[16] = 1;
         }
         if ( ! (0==AV15TFContrato_Codigo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Contrato_Codigo] <= @AV15TFContrato_Codigo_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Contrato_Codigo] <= @AV15TFContrato_Codigo_To)";
            }
         }
         else
         {
            GXv_int1[17] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV17TFContrato_Numero_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV16TFContrato_Numero)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Contrato_Numero] like @lV16TFContrato_Numero)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Contrato_Numero] like @lV16TFContrato_Numero)";
            }
         }
         else
         {
            GXv_int1[18] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV17TFContrato_Numero_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Contrato_Numero] = @AV17TFContrato_Numero_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Contrato_Numero] = @AV17TFContrato_Numero_Sel)";
            }
         }
         else
         {
            GXv_int1[19] = 1;
         }
         if ( ! (DateTime.MinValue==AV18TFContratoOcorrenciaNotificacao_Data) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Data] >= @AV18TFContratoOcorrenciaNotificacao_Data)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Data] >= @AV18TFContratoOcorrenciaNotificacao_Data)";
            }
         }
         else
         {
            GXv_int1[20] = 1;
         }
         if ( ! (DateTime.MinValue==AV19TFContratoOcorrenciaNotificacao_Data_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Data] <= @AV19TFContratoOcorrenciaNotificacao_Data_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Data] <= @AV19TFContratoOcorrenciaNotificacao_Data_To)";
            }
         }
         else
         {
            GXv_int1[21] = 1;
         }
         if ( ! (0==AV20TFContratoOcorrenciaNotificacao_Prazo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Prazo] >= @AV20TFContratoOcorrenciaNotificacao_Prazo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Prazo] >= @AV20TFContratoOcorrenciaNotificacao_Prazo)";
            }
         }
         else
         {
            GXv_int1[22] = 1;
         }
         if ( ! (0==AV21TFContratoOcorrenciaNotificacao_Prazo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Prazo] <= @AV21TFContratoOcorrenciaNotificacao_Prazo_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Prazo] <= @AV21TFContratoOcorrenciaNotificacao_Prazo_To)";
            }
         }
         else
         {
            GXv_int1[23] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV23TFContratoOcorrenciaNotificacao_Descricao_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV22TFContratoOcorrenciaNotificacao_Descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Descricao] like @lV22TFContratoOcorrenciaNotificacao_Descricao)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Descricao] like @lV22TFContratoOcorrenciaNotificacao_Descricao)";
            }
         }
         else
         {
            GXv_int1[24] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV23TFContratoOcorrenciaNotificacao_Descricao_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Descricao] = @AV23TFContratoOcorrenciaNotificacao_Descricao_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Descricao] = @AV23TFContratoOcorrenciaNotificacao_Descricao_Sel)";
            }
         }
         else
         {
            GXv_int1[25] = 1;
         }
         if ( ! (DateTime.MinValue==AV24TFContratoOcorrenciaNotificacao_Cumprido) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Cumprido] >= @AV24TFContratoOcorrenciaNotificacao_Cumprido)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Cumprido] >= @AV24TFContratoOcorrenciaNotificacao_Cumprido)";
            }
         }
         else
         {
            GXv_int1[26] = 1;
         }
         if ( ! (DateTime.MinValue==AV25TFContratoOcorrenciaNotificacao_Cumprido_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Cumprido] <= @AV25TFContratoOcorrenciaNotificacao_Cumprido_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Cumprido] <= @AV25TFContratoOcorrenciaNotificacao_Cumprido_To)";
            }
         }
         else
         {
            GXv_int1[27] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV27TFContratoOcorrenciaNotificacao_Protocolo_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV26TFContratoOcorrenciaNotificacao_Protocolo)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Protocolo] like @lV26TFContratoOcorrenciaNotificacao_Protocolo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Protocolo] like @lV26TFContratoOcorrenciaNotificacao_Protocolo)";
            }
         }
         else
         {
            GXv_int1[28] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV27TFContratoOcorrenciaNotificacao_Protocolo_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Protocolo] = @AV27TFContratoOcorrenciaNotificacao_Protocolo_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Protocolo] = @AV27TFContratoOcorrenciaNotificacao_Protocolo_Sel)";
            }
         }
         else
         {
            GXv_int1[29] = 1;
         }
         if ( ! (0==AV28TFContratoOcorrenciaNotificacao_Responsavel) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Responsavel] >= @AV28TFContratoOcorrenciaNotificacao_Responsavel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Responsavel] >= @AV28TFContratoOcorrenciaNotificacao_Responsavel)";
            }
         }
         else
         {
            GXv_int1[30] = 1;
         }
         if ( ! (0==AV29TFContratoOcorrenciaNotificacao_Responsavel_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Responsavel] <= @AV29TFContratoOcorrenciaNotificacao_Responsavel_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Responsavel] <= @AV29TFContratoOcorrenciaNotificacao_Responsavel_To)";
            }
         }
         else
         {
            GXv_int1[31] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV31TFContratoOcorrenciaNotificacao_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV30TFContratoOcorrenciaNotificacao_Nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Pessoa_Nome] like @lV30TFContratoOcorrenciaNotificacao_Nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Pessoa_Nome] like @lV30TFContratoOcorrenciaNotificacao_Nome)";
            }
         }
         else
         {
            GXv_int1[32] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV31TFContratoOcorrenciaNotificacao_Nome_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Pessoa_Nome] = @AV31TFContratoOcorrenciaNotificacao_Nome_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Pessoa_Nome] = @AV31TFContratoOcorrenciaNotificacao_Nome_Sel)";
            }
         }
         else
         {
            GXv_int1[33] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV33TFContratoOcorrenciaNotificacao_Docto_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV32TFContratoOcorrenciaNotificacao_Docto)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Pessoa_Docto] like @lV32TFContratoOcorrenciaNotificacao_Docto)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Pessoa_Docto] like @lV32TFContratoOcorrenciaNotificacao_Docto)";
            }
         }
         else
         {
            GXv_int1[34] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV33TFContratoOcorrenciaNotificacao_Docto_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Pessoa_Docto] = @AV33TFContratoOcorrenciaNotificacao_Docto_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Pessoa_Docto] = @AV33TFContratoOcorrenciaNotificacao_Docto_Sel)";
            }
         }
         else
         {
            GXv_int1[35] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T4.[Contrato_Numero]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      protected Object[] conditional_P00KA3( IGxContext context ,
                                             String AV52DynamicFiltersSelector1 ,
                                             DateTime AV54ContratoOcorrenciaNotificacao_Data1 ,
                                             DateTime AV55ContratoOcorrenciaNotificacao_Data_To1 ,
                                             short AV53DynamicFiltersOperator1 ,
                                             String AV56ContratoOcorrenciaNotificacao_Nome1 ,
                                             bool AV57DynamicFiltersEnabled2 ,
                                             String AV58DynamicFiltersSelector2 ,
                                             DateTime AV60ContratoOcorrenciaNotificacao_Data2 ,
                                             DateTime AV61ContratoOcorrenciaNotificacao_Data_To2 ,
                                             short AV59DynamicFiltersOperator2 ,
                                             String AV62ContratoOcorrenciaNotificacao_Nome2 ,
                                             bool AV63DynamicFiltersEnabled3 ,
                                             String AV64DynamicFiltersSelector3 ,
                                             DateTime AV66ContratoOcorrenciaNotificacao_Data3 ,
                                             DateTime AV67ContratoOcorrenciaNotificacao_Data_To3 ,
                                             short AV65DynamicFiltersOperator3 ,
                                             String AV68ContratoOcorrenciaNotificacao_Nome3 ,
                                             int AV10TFContratoOcorrenciaNotificacao_Codigo ,
                                             int AV11TFContratoOcorrenciaNotificacao_Codigo_To ,
                                             int AV12TFContratoOcorrencia_Codigo ,
                                             int AV13TFContratoOcorrencia_Codigo_To ,
                                             int AV14TFContrato_Codigo ,
                                             int AV15TFContrato_Codigo_To ,
                                             String AV17TFContrato_Numero_Sel ,
                                             String AV16TFContrato_Numero ,
                                             DateTime AV18TFContratoOcorrenciaNotificacao_Data ,
                                             DateTime AV19TFContratoOcorrenciaNotificacao_Data_To ,
                                             short AV20TFContratoOcorrenciaNotificacao_Prazo ,
                                             short AV21TFContratoOcorrenciaNotificacao_Prazo_To ,
                                             String AV23TFContratoOcorrenciaNotificacao_Descricao_Sel ,
                                             String AV22TFContratoOcorrenciaNotificacao_Descricao ,
                                             DateTime AV24TFContratoOcorrenciaNotificacao_Cumprido ,
                                             DateTime AV25TFContratoOcorrenciaNotificacao_Cumprido_To ,
                                             String AV27TFContratoOcorrenciaNotificacao_Protocolo_Sel ,
                                             String AV26TFContratoOcorrenciaNotificacao_Protocolo ,
                                             int AV28TFContratoOcorrenciaNotificacao_Responsavel ,
                                             int AV29TFContratoOcorrenciaNotificacao_Responsavel_To ,
                                             String AV31TFContratoOcorrenciaNotificacao_Nome_Sel ,
                                             String AV30TFContratoOcorrenciaNotificacao_Nome ,
                                             String AV33TFContratoOcorrenciaNotificacao_Docto_Sel ,
                                             String AV32TFContratoOcorrenciaNotificacao_Docto ,
                                             DateTime A298ContratoOcorrenciaNotificacao_Data ,
                                             String A304ContratoOcorrenciaNotificacao_Nome ,
                                             int A297ContratoOcorrenciaNotificacao_Codigo ,
                                             int A294ContratoOcorrencia_Codigo ,
                                             int A74Contrato_Codigo ,
                                             String A77Contrato_Numero ,
                                             short A299ContratoOcorrenciaNotificacao_Prazo ,
                                             String A300ContratoOcorrenciaNotificacao_Descricao ,
                                             DateTime A301ContratoOcorrenciaNotificacao_Cumprido ,
                                             String A302ContratoOcorrenciaNotificacao_Protocolo ,
                                             int A303ContratoOcorrenciaNotificacao_Responsavel ,
                                             String A306ContratoOcorrenciaNotificacao_Docto )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [36] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT T1.[ContratoOcorrenciaNotificacao_Descricao], T2.[Pessoa_Docto] AS ContratoOcorrenciaNotificacao_Docto, T1.[ContratoOcorrenciaNotificacao_Responsavel] AS ContratoOcorrenciaNotificacao_Responsavel, T1.[ContratoOcorrenciaNotificacao_Protocolo], T1.[ContratoOcorrenciaNotificacao_Cumprido], T1.[ContratoOcorrenciaNotificacao_Prazo], T4.[Contrato_Numero], T3.[Contrato_Codigo], T1.[ContratoOcorrencia_Codigo], T1.[ContratoOcorrenciaNotificacao_Codigo], T2.[Pessoa_Nome] AS ContratoOcorrenciaNotificacao_Nome, T1.[ContratoOcorrenciaNotificacao_Data] FROM ((([ContratoOcorrenciaNotificacao] T1 WITH (NOLOCK) INNER JOIN [Pessoa] T2 WITH (NOLOCK) ON T2.[Pessoa_Codigo] = T1.[ContratoOcorrenciaNotificacao_Responsavel]) INNER JOIN [ContratoOcorrencia] T3 WITH (NOLOCK) ON T3.[ContratoOcorrencia_Codigo] = T1.[ContratoOcorrencia_Codigo]) INNER JOIN [Contrato] T4 WITH (NOLOCK) ON T4.[Contrato_Codigo] = T3.[Contrato_Codigo])";
         if ( ( StringUtil.StrCmp(AV52DynamicFiltersSelector1, "CONTRATOOCORRENCIANOTIFICACAO_DATA") == 0 ) && ( ! (DateTime.MinValue==AV54ContratoOcorrenciaNotificacao_Data1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Data] >= @AV54ContratoOcorrenciaNotificacao_Data1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Data] >= @AV54ContratoOcorrenciaNotificacao_Data1)";
            }
         }
         else
         {
            GXv_int3[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV52DynamicFiltersSelector1, "CONTRATOOCORRENCIANOTIFICACAO_DATA") == 0 ) && ( ! (DateTime.MinValue==AV55ContratoOcorrenciaNotificacao_Data_To1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Data] <= @AV55ContratoOcorrenciaNotificacao_Data_To1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Data] <= @AV55ContratoOcorrenciaNotificacao_Data_To1)";
            }
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV52DynamicFiltersSelector1, "CONTRATOOCORRENCIANOTIFICACAO_NOME") == 0 ) && ( AV53DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV56ContratoOcorrenciaNotificacao_Nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Pessoa_Nome] like @lV56ContratoOcorrenciaNotificacao_Nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Pessoa_Nome] like @lV56ContratoOcorrenciaNotificacao_Nome1)";
            }
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV52DynamicFiltersSelector1, "CONTRATOOCORRENCIANOTIFICACAO_NOME") == 0 ) && ( AV53DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV56ContratoOcorrenciaNotificacao_Nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Pessoa_Nome] like '%' + @lV56ContratoOcorrenciaNotificacao_Nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Pessoa_Nome] like '%' + @lV56ContratoOcorrenciaNotificacao_Nome1)";
            }
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( AV57DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV58DynamicFiltersSelector2, "CONTRATOOCORRENCIANOTIFICACAO_DATA") == 0 ) && ( ! (DateTime.MinValue==AV60ContratoOcorrenciaNotificacao_Data2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Data] >= @AV60ContratoOcorrenciaNotificacao_Data2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Data] >= @AV60ContratoOcorrenciaNotificacao_Data2)";
            }
         }
         else
         {
            GXv_int3[4] = 1;
         }
         if ( AV57DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV58DynamicFiltersSelector2, "CONTRATOOCORRENCIANOTIFICACAO_DATA") == 0 ) && ( ! (DateTime.MinValue==AV61ContratoOcorrenciaNotificacao_Data_To2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Data] <= @AV61ContratoOcorrenciaNotificacao_Data_To2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Data] <= @AV61ContratoOcorrenciaNotificacao_Data_To2)";
            }
         }
         else
         {
            GXv_int3[5] = 1;
         }
         if ( AV57DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV58DynamicFiltersSelector2, "CONTRATOOCORRENCIANOTIFICACAO_NOME") == 0 ) && ( AV59DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV62ContratoOcorrenciaNotificacao_Nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Pessoa_Nome] like @lV62ContratoOcorrenciaNotificacao_Nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Pessoa_Nome] like @lV62ContratoOcorrenciaNotificacao_Nome2)";
            }
         }
         else
         {
            GXv_int3[6] = 1;
         }
         if ( AV57DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV58DynamicFiltersSelector2, "CONTRATOOCORRENCIANOTIFICACAO_NOME") == 0 ) && ( AV59DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV62ContratoOcorrenciaNotificacao_Nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Pessoa_Nome] like '%' + @lV62ContratoOcorrenciaNotificacao_Nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Pessoa_Nome] like '%' + @lV62ContratoOcorrenciaNotificacao_Nome2)";
            }
         }
         else
         {
            GXv_int3[7] = 1;
         }
         if ( AV63DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV64DynamicFiltersSelector3, "CONTRATOOCORRENCIANOTIFICACAO_DATA") == 0 ) && ( ! (DateTime.MinValue==AV66ContratoOcorrenciaNotificacao_Data3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Data] >= @AV66ContratoOcorrenciaNotificacao_Data3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Data] >= @AV66ContratoOcorrenciaNotificacao_Data3)";
            }
         }
         else
         {
            GXv_int3[8] = 1;
         }
         if ( AV63DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV64DynamicFiltersSelector3, "CONTRATOOCORRENCIANOTIFICACAO_DATA") == 0 ) && ( ! (DateTime.MinValue==AV67ContratoOcorrenciaNotificacao_Data_To3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Data] <= @AV67ContratoOcorrenciaNotificacao_Data_To3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Data] <= @AV67ContratoOcorrenciaNotificacao_Data_To3)";
            }
         }
         else
         {
            GXv_int3[9] = 1;
         }
         if ( AV63DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV64DynamicFiltersSelector3, "CONTRATOOCORRENCIANOTIFICACAO_NOME") == 0 ) && ( AV65DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV68ContratoOcorrenciaNotificacao_Nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Pessoa_Nome] like @lV68ContratoOcorrenciaNotificacao_Nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Pessoa_Nome] like @lV68ContratoOcorrenciaNotificacao_Nome3)";
            }
         }
         else
         {
            GXv_int3[10] = 1;
         }
         if ( AV63DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV64DynamicFiltersSelector3, "CONTRATOOCORRENCIANOTIFICACAO_NOME") == 0 ) && ( AV65DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV68ContratoOcorrenciaNotificacao_Nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Pessoa_Nome] like '%' + @lV68ContratoOcorrenciaNotificacao_Nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Pessoa_Nome] like '%' + @lV68ContratoOcorrenciaNotificacao_Nome3)";
            }
         }
         else
         {
            GXv_int3[11] = 1;
         }
         if ( ! (0==AV10TFContratoOcorrenciaNotificacao_Codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Codigo] >= @AV10TFContratoOcorrenciaNotificacao_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Codigo] >= @AV10TFContratoOcorrenciaNotificacao_Codigo)";
            }
         }
         else
         {
            GXv_int3[12] = 1;
         }
         if ( ! (0==AV11TFContratoOcorrenciaNotificacao_Codigo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Codigo] <= @AV11TFContratoOcorrenciaNotificacao_Codigo_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Codigo] <= @AV11TFContratoOcorrenciaNotificacao_Codigo_To)";
            }
         }
         else
         {
            GXv_int3[13] = 1;
         }
         if ( ! (0==AV12TFContratoOcorrencia_Codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrencia_Codigo] >= @AV12TFContratoOcorrencia_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrencia_Codigo] >= @AV12TFContratoOcorrencia_Codigo)";
            }
         }
         else
         {
            GXv_int3[14] = 1;
         }
         if ( ! (0==AV13TFContratoOcorrencia_Codigo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrencia_Codigo] <= @AV13TFContratoOcorrencia_Codigo_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrencia_Codigo] <= @AV13TFContratoOcorrencia_Codigo_To)";
            }
         }
         else
         {
            GXv_int3[15] = 1;
         }
         if ( ! (0==AV14TFContrato_Codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Contrato_Codigo] >= @AV14TFContrato_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Contrato_Codigo] >= @AV14TFContrato_Codigo)";
            }
         }
         else
         {
            GXv_int3[16] = 1;
         }
         if ( ! (0==AV15TFContrato_Codigo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Contrato_Codigo] <= @AV15TFContrato_Codigo_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Contrato_Codigo] <= @AV15TFContrato_Codigo_To)";
            }
         }
         else
         {
            GXv_int3[17] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV17TFContrato_Numero_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV16TFContrato_Numero)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Contrato_Numero] like @lV16TFContrato_Numero)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Contrato_Numero] like @lV16TFContrato_Numero)";
            }
         }
         else
         {
            GXv_int3[18] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV17TFContrato_Numero_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Contrato_Numero] = @AV17TFContrato_Numero_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Contrato_Numero] = @AV17TFContrato_Numero_Sel)";
            }
         }
         else
         {
            GXv_int3[19] = 1;
         }
         if ( ! (DateTime.MinValue==AV18TFContratoOcorrenciaNotificacao_Data) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Data] >= @AV18TFContratoOcorrenciaNotificacao_Data)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Data] >= @AV18TFContratoOcorrenciaNotificacao_Data)";
            }
         }
         else
         {
            GXv_int3[20] = 1;
         }
         if ( ! (DateTime.MinValue==AV19TFContratoOcorrenciaNotificacao_Data_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Data] <= @AV19TFContratoOcorrenciaNotificacao_Data_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Data] <= @AV19TFContratoOcorrenciaNotificacao_Data_To)";
            }
         }
         else
         {
            GXv_int3[21] = 1;
         }
         if ( ! (0==AV20TFContratoOcorrenciaNotificacao_Prazo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Prazo] >= @AV20TFContratoOcorrenciaNotificacao_Prazo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Prazo] >= @AV20TFContratoOcorrenciaNotificacao_Prazo)";
            }
         }
         else
         {
            GXv_int3[22] = 1;
         }
         if ( ! (0==AV21TFContratoOcorrenciaNotificacao_Prazo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Prazo] <= @AV21TFContratoOcorrenciaNotificacao_Prazo_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Prazo] <= @AV21TFContratoOcorrenciaNotificacao_Prazo_To)";
            }
         }
         else
         {
            GXv_int3[23] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV23TFContratoOcorrenciaNotificacao_Descricao_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV22TFContratoOcorrenciaNotificacao_Descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Descricao] like @lV22TFContratoOcorrenciaNotificacao_Descricao)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Descricao] like @lV22TFContratoOcorrenciaNotificacao_Descricao)";
            }
         }
         else
         {
            GXv_int3[24] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV23TFContratoOcorrenciaNotificacao_Descricao_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Descricao] = @AV23TFContratoOcorrenciaNotificacao_Descricao_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Descricao] = @AV23TFContratoOcorrenciaNotificacao_Descricao_Sel)";
            }
         }
         else
         {
            GXv_int3[25] = 1;
         }
         if ( ! (DateTime.MinValue==AV24TFContratoOcorrenciaNotificacao_Cumprido) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Cumprido] >= @AV24TFContratoOcorrenciaNotificacao_Cumprido)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Cumprido] >= @AV24TFContratoOcorrenciaNotificacao_Cumprido)";
            }
         }
         else
         {
            GXv_int3[26] = 1;
         }
         if ( ! (DateTime.MinValue==AV25TFContratoOcorrenciaNotificacao_Cumprido_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Cumprido] <= @AV25TFContratoOcorrenciaNotificacao_Cumprido_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Cumprido] <= @AV25TFContratoOcorrenciaNotificacao_Cumprido_To)";
            }
         }
         else
         {
            GXv_int3[27] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV27TFContratoOcorrenciaNotificacao_Protocolo_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV26TFContratoOcorrenciaNotificacao_Protocolo)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Protocolo] like @lV26TFContratoOcorrenciaNotificacao_Protocolo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Protocolo] like @lV26TFContratoOcorrenciaNotificacao_Protocolo)";
            }
         }
         else
         {
            GXv_int3[28] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV27TFContratoOcorrenciaNotificacao_Protocolo_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Protocolo] = @AV27TFContratoOcorrenciaNotificacao_Protocolo_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Protocolo] = @AV27TFContratoOcorrenciaNotificacao_Protocolo_Sel)";
            }
         }
         else
         {
            GXv_int3[29] = 1;
         }
         if ( ! (0==AV28TFContratoOcorrenciaNotificacao_Responsavel) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Responsavel] >= @AV28TFContratoOcorrenciaNotificacao_Responsavel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Responsavel] >= @AV28TFContratoOcorrenciaNotificacao_Responsavel)";
            }
         }
         else
         {
            GXv_int3[30] = 1;
         }
         if ( ! (0==AV29TFContratoOcorrenciaNotificacao_Responsavel_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Responsavel] <= @AV29TFContratoOcorrenciaNotificacao_Responsavel_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Responsavel] <= @AV29TFContratoOcorrenciaNotificacao_Responsavel_To)";
            }
         }
         else
         {
            GXv_int3[31] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV31TFContratoOcorrenciaNotificacao_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV30TFContratoOcorrenciaNotificacao_Nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Pessoa_Nome] like @lV30TFContratoOcorrenciaNotificacao_Nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Pessoa_Nome] like @lV30TFContratoOcorrenciaNotificacao_Nome)";
            }
         }
         else
         {
            GXv_int3[32] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV31TFContratoOcorrenciaNotificacao_Nome_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Pessoa_Nome] = @AV31TFContratoOcorrenciaNotificacao_Nome_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Pessoa_Nome] = @AV31TFContratoOcorrenciaNotificacao_Nome_Sel)";
            }
         }
         else
         {
            GXv_int3[33] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV33TFContratoOcorrenciaNotificacao_Docto_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV32TFContratoOcorrenciaNotificacao_Docto)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Pessoa_Docto] like @lV32TFContratoOcorrenciaNotificacao_Docto)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Pessoa_Docto] like @lV32TFContratoOcorrenciaNotificacao_Docto)";
            }
         }
         else
         {
            GXv_int3[34] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV33TFContratoOcorrenciaNotificacao_Docto_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Pessoa_Docto] = @AV33TFContratoOcorrenciaNotificacao_Docto_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Pessoa_Docto] = @AV33TFContratoOcorrenciaNotificacao_Docto_Sel)";
            }
         }
         else
         {
            GXv_int3[35] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T1.[ContratoOcorrenciaNotificacao_Descricao]";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      protected Object[] conditional_P00KA4( IGxContext context ,
                                             String AV52DynamicFiltersSelector1 ,
                                             DateTime AV54ContratoOcorrenciaNotificacao_Data1 ,
                                             DateTime AV55ContratoOcorrenciaNotificacao_Data_To1 ,
                                             short AV53DynamicFiltersOperator1 ,
                                             String AV56ContratoOcorrenciaNotificacao_Nome1 ,
                                             bool AV57DynamicFiltersEnabled2 ,
                                             String AV58DynamicFiltersSelector2 ,
                                             DateTime AV60ContratoOcorrenciaNotificacao_Data2 ,
                                             DateTime AV61ContratoOcorrenciaNotificacao_Data_To2 ,
                                             short AV59DynamicFiltersOperator2 ,
                                             String AV62ContratoOcorrenciaNotificacao_Nome2 ,
                                             bool AV63DynamicFiltersEnabled3 ,
                                             String AV64DynamicFiltersSelector3 ,
                                             DateTime AV66ContratoOcorrenciaNotificacao_Data3 ,
                                             DateTime AV67ContratoOcorrenciaNotificacao_Data_To3 ,
                                             short AV65DynamicFiltersOperator3 ,
                                             String AV68ContratoOcorrenciaNotificacao_Nome3 ,
                                             int AV10TFContratoOcorrenciaNotificacao_Codigo ,
                                             int AV11TFContratoOcorrenciaNotificacao_Codigo_To ,
                                             int AV12TFContratoOcorrencia_Codigo ,
                                             int AV13TFContratoOcorrencia_Codigo_To ,
                                             int AV14TFContrato_Codigo ,
                                             int AV15TFContrato_Codigo_To ,
                                             String AV17TFContrato_Numero_Sel ,
                                             String AV16TFContrato_Numero ,
                                             DateTime AV18TFContratoOcorrenciaNotificacao_Data ,
                                             DateTime AV19TFContratoOcorrenciaNotificacao_Data_To ,
                                             short AV20TFContratoOcorrenciaNotificacao_Prazo ,
                                             short AV21TFContratoOcorrenciaNotificacao_Prazo_To ,
                                             String AV23TFContratoOcorrenciaNotificacao_Descricao_Sel ,
                                             String AV22TFContratoOcorrenciaNotificacao_Descricao ,
                                             DateTime AV24TFContratoOcorrenciaNotificacao_Cumprido ,
                                             DateTime AV25TFContratoOcorrenciaNotificacao_Cumprido_To ,
                                             String AV27TFContratoOcorrenciaNotificacao_Protocolo_Sel ,
                                             String AV26TFContratoOcorrenciaNotificacao_Protocolo ,
                                             int AV28TFContratoOcorrenciaNotificacao_Responsavel ,
                                             int AV29TFContratoOcorrenciaNotificacao_Responsavel_To ,
                                             String AV31TFContratoOcorrenciaNotificacao_Nome_Sel ,
                                             String AV30TFContratoOcorrenciaNotificacao_Nome ,
                                             String AV33TFContratoOcorrenciaNotificacao_Docto_Sel ,
                                             String AV32TFContratoOcorrenciaNotificacao_Docto ,
                                             DateTime A298ContratoOcorrenciaNotificacao_Data ,
                                             String A304ContratoOcorrenciaNotificacao_Nome ,
                                             int A297ContratoOcorrenciaNotificacao_Codigo ,
                                             int A294ContratoOcorrencia_Codigo ,
                                             int A74Contrato_Codigo ,
                                             String A77Contrato_Numero ,
                                             short A299ContratoOcorrenciaNotificacao_Prazo ,
                                             String A300ContratoOcorrenciaNotificacao_Descricao ,
                                             DateTime A301ContratoOcorrenciaNotificacao_Cumprido ,
                                             String A302ContratoOcorrenciaNotificacao_Protocolo ,
                                             int A303ContratoOcorrenciaNotificacao_Responsavel ,
                                             String A306ContratoOcorrenciaNotificacao_Docto )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int5 ;
         GXv_int5 = new short [36] ;
         Object[] GXv_Object6 ;
         GXv_Object6 = new Object [2] ;
         scmdbuf = "SELECT T1.[ContratoOcorrenciaNotificacao_Protocolo], T2.[Pessoa_Docto] AS ContratoOcorrenciaNotificacao_Docto, T1.[ContratoOcorrenciaNotificacao_Responsavel] AS ContratoOcorrenciaNotificacao_Responsavel, T1.[ContratoOcorrenciaNotificacao_Cumprido], T1.[ContratoOcorrenciaNotificacao_Descricao], T1.[ContratoOcorrenciaNotificacao_Prazo], T4.[Contrato_Numero], T3.[Contrato_Codigo], T1.[ContratoOcorrencia_Codigo], T1.[ContratoOcorrenciaNotificacao_Codigo], T2.[Pessoa_Nome] AS ContratoOcorrenciaNotificacao_Nome, T1.[ContratoOcorrenciaNotificacao_Data] FROM ((([ContratoOcorrenciaNotificacao] T1 WITH (NOLOCK) INNER JOIN [Pessoa] T2 WITH (NOLOCK) ON T2.[Pessoa_Codigo] = T1.[ContratoOcorrenciaNotificacao_Responsavel]) INNER JOIN [ContratoOcorrencia] T3 WITH (NOLOCK) ON T3.[ContratoOcorrencia_Codigo] = T1.[ContratoOcorrencia_Codigo]) INNER JOIN [Contrato] T4 WITH (NOLOCK) ON T4.[Contrato_Codigo] = T3.[Contrato_Codigo])";
         if ( ( StringUtil.StrCmp(AV52DynamicFiltersSelector1, "CONTRATOOCORRENCIANOTIFICACAO_DATA") == 0 ) && ( ! (DateTime.MinValue==AV54ContratoOcorrenciaNotificacao_Data1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Data] >= @AV54ContratoOcorrenciaNotificacao_Data1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Data] >= @AV54ContratoOcorrenciaNotificacao_Data1)";
            }
         }
         else
         {
            GXv_int5[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV52DynamicFiltersSelector1, "CONTRATOOCORRENCIANOTIFICACAO_DATA") == 0 ) && ( ! (DateTime.MinValue==AV55ContratoOcorrenciaNotificacao_Data_To1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Data] <= @AV55ContratoOcorrenciaNotificacao_Data_To1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Data] <= @AV55ContratoOcorrenciaNotificacao_Data_To1)";
            }
         }
         else
         {
            GXv_int5[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV52DynamicFiltersSelector1, "CONTRATOOCORRENCIANOTIFICACAO_NOME") == 0 ) && ( AV53DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV56ContratoOcorrenciaNotificacao_Nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Pessoa_Nome] like @lV56ContratoOcorrenciaNotificacao_Nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Pessoa_Nome] like @lV56ContratoOcorrenciaNotificacao_Nome1)";
            }
         }
         else
         {
            GXv_int5[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV52DynamicFiltersSelector1, "CONTRATOOCORRENCIANOTIFICACAO_NOME") == 0 ) && ( AV53DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV56ContratoOcorrenciaNotificacao_Nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Pessoa_Nome] like '%' + @lV56ContratoOcorrenciaNotificacao_Nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Pessoa_Nome] like '%' + @lV56ContratoOcorrenciaNotificacao_Nome1)";
            }
         }
         else
         {
            GXv_int5[3] = 1;
         }
         if ( AV57DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV58DynamicFiltersSelector2, "CONTRATOOCORRENCIANOTIFICACAO_DATA") == 0 ) && ( ! (DateTime.MinValue==AV60ContratoOcorrenciaNotificacao_Data2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Data] >= @AV60ContratoOcorrenciaNotificacao_Data2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Data] >= @AV60ContratoOcorrenciaNotificacao_Data2)";
            }
         }
         else
         {
            GXv_int5[4] = 1;
         }
         if ( AV57DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV58DynamicFiltersSelector2, "CONTRATOOCORRENCIANOTIFICACAO_DATA") == 0 ) && ( ! (DateTime.MinValue==AV61ContratoOcorrenciaNotificacao_Data_To2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Data] <= @AV61ContratoOcorrenciaNotificacao_Data_To2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Data] <= @AV61ContratoOcorrenciaNotificacao_Data_To2)";
            }
         }
         else
         {
            GXv_int5[5] = 1;
         }
         if ( AV57DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV58DynamicFiltersSelector2, "CONTRATOOCORRENCIANOTIFICACAO_NOME") == 0 ) && ( AV59DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV62ContratoOcorrenciaNotificacao_Nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Pessoa_Nome] like @lV62ContratoOcorrenciaNotificacao_Nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Pessoa_Nome] like @lV62ContratoOcorrenciaNotificacao_Nome2)";
            }
         }
         else
         {
            GXv_int5[6] = 1;
         }
         if ( AV57DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV58DynamicFiltersSelector2, "CONTRATOOCORRENCIANOTIFICACAO_NOME") == 0 ) && ( AV59DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV62ContratoOcorrenciaNotificacao_Nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Pessoa_Nome] like '%' + @lV62ContratoOcorrenciaNotificacao_Nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Pessoa_Nome] like '%' + @lV62ContratoOcorrenciaNotificacao_Nome2)";
            }
         }
         else
         {
            GXv_int5[7] = 1;
         }
         if ( AV63DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV64DynamicFiltersSelector3, "CONTRATOOCORRENCIANOTIFICACAO_DATA") == 0 ) && ( ! (DateTime.MinValue==AV66ContratoOcorrenciaNotificacao_Data3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Data] >= @AV66ContratoOcorrenciaNotificacao_Data3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Data] >= @AV66ContratoOcorrenciaNotificacao_Data3)";
            }
         }
         else
         {
            GXv_int5[8] = 1;
         }
         if ( AV63DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV64DynamicFiltersSelector3, "CONTRATOOCORRENCIANOTIFICACAO_DATA") == 0 ) && ( ! (DateTime.MinValue==AV67ContratoOcorrenciaNotificacao_Data_To3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Data] <= @AV67ContratoOcorrenciaNotificacao_Data_To3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Data] <= @AV67ContratoOcorrenciaNotificacao_Data_To3)";
            }
         }
         else
         {
            GXv_int5[9] = 1;
         }
         if ( AV63DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV64DynamicFiltersSelector3, "CONTRATOOCORRENCIANOTIFICACAO_NOME") == 0 ) && ( AV65DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV68ContratoOcorrenciaNotificacao_Nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Pessoa_Nome] like @lV68ContratoOcorrenciaNotificacao_Nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Pessoa_Nome] like @lV68ContratoOcorrenciaNotificacao_Nome3)";
            }
         }
         else
         {
            GXv_int5[10] = 1;
         }
         if ( AV63DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV64DynamicFiltersSelector3, "CONTRATOOCORRENCIANOTIFICACAO_NOME") == 0 ) && ( AV65DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV68ContratoOcorrenciaNotificacao_Nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Pessoa_Nome] like '%' + @lV68ContratoOcorrenciaNotificacao_Nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Pessoa_Nome] like '%' + @lV68ContratoOcorrenciaNotificacao_Nome3)";
            }
         }
         else
         {
            GXv_int5[11] = 1;
         }
         if ( ! (0==AV10TFContratoOcorrenciaNotificacao_Codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Codigo] >= @AV10TFContratoOcorrenciaNotificacao_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Codigo] >= @AV10TFContratoOcorrenciaNotificacao_Codigo)";
            }
         }
         else
         {
            GXv_int5[12] = 1;
         }
         if ( ! (0==AV11TFContratoOcorrenciaNotificacao_Codigo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Codigo] <= @AV11TFContratoOcorrenciaNotificacao_Codigo_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Codigo] <= @AV11TFContratoOcorrenciaNotificacao_Codigo_To)";
            }
         }
         else
         {
            GXv_int5[13] = 1;
         }
         if ( ! (0==AV12TFContratoOcorrencia_Codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrencia_Codigo] >= @AV12TFContratoOcorrencia_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrencia_Codigo] >= @AV12TFContratoOcorrencia_Codigo)";
            }
         }
         else
         {
            GXv_int5[14] = 1;
         }
         if ( ! (0==AV13TFContratoOcorrencia_Codigo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrencia_Codigo] <= @AV13TFContratoOcorrencia_Codigo_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrencia_Codigo] <= @AV13TFContratoOcorrencia_Codigo_To)";
            }
         }
         else
         {
            GXv_int5[15] = 1;
         }
         if ( ! (0==AV14TFContrato_Codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Contrato_Codigo] >= @AV14TFContrato_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Contrato_Codigo] >= @AV14TFContrato_Codigo)";
            }
         }
         else
         {
            GXv_int5[16] = 1;
         }
         if ( ! (0==AV15TFContrato_Codigo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Contrato_Codigo] <= @AV15TFContrato_Codigo_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Contrato_Codigo] <= @AV15TFContrato_Codigo_To)";
            }
         }
         else
         {
            GXv_int5[17] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV17TFContrato_Numero_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV16TFContrato_Numero)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Contrato_Numero] like @lV16TFContrato_Numero)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Contrato_Numero] like @lV16TFContrato_Numero)";
            }
         }
         else
         {
            GXv_int5[18] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV17TFContrato_Numero_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Contrato_Numero] = @AV17TFContrato_Numero_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Contrato_Numero] = @AV17TFContrato_Numero_Sel)";
            }
         }
         else
         {
            GXv_int5[19] = 1;
         }
         if ( ! (DateTime.MinValue==AV18TFContratoOcorrenciaNotificacao_Data) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Data] >= @AV18TFContratoOcorrenciaNotificacao_Data)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Data] >= @AV18TFContratoOcorrenciaNotificacao_Data)";
            }
         }
         else
         {
            GXv_int5[20] = 1;
         }
         if ( ! (DateTime.MinValue==AV19TFContratoOcorrenciaNotificacao_Data_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Data] <= @AV19TFContratoOcorrenciaNotificacao_Data_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Data] <= @AV19TFContratoOcorrenciaNotificacao_Data_To)";
            }
         }
         else
         {
            GXv_int5[21] = 1;
         }
         if ( ! (0==AV20TFContratoOcorrenciaNotificacao_Prazo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Prazo] >= @AV20TFContratoOcorrenciaNotificacao_Prazo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Prazo] >= @AV20TFContratoOcorrenciaNotificacao_Prazo)";
            }
         }
         else
         {
            GXv_int5[22] = 1;
         }
         if ( ! (0==AV21TFContratoOcorrenciaNotificacao_Prazo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Prazo] <= @AV21TFContratoOcorrenciaNotificacao_Prazo_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Prazo] <= @AV21TFContratoOcorrenciaNotificacao_Prazo_To)";
            }
         }
         else
         {
            GXv_int5[23] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV23TFContratoOcorrenciaNotificacao_Descricao_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV22TFContratoOcorrenciaNotificacao_Descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Descricao] like @lV22TFContratoOcorrenciaNotificacao_Descricao)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Descricao] like @lV22TFContratoOcorrenciaNotificacao_Descricao)";
            }
         }
         else
         {
            GXv_int5[24] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV23TFContratoOcorrenciaNotificacao_Descricao_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Descricao] = @AV23TFContratoOcorrenciaNotificacao_Descricao_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Descricao] = @AV23TFContratoOcorrenciaNotificacao_Descricao_Sel)";
            }
         }
         else
         {
            GXv_int5[25] = 1;
         }
         if ( ! (DateTime.MinValue==AV24TFContratoOcorrenciaNotificacao_Cumprido) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Cumprido] >= @AV24TFContratoOcorrenciaNotificacao_Cumprido)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Cumprido] >= @AV24TFContratoOcorrenciaNotificacao_Cumprido)";
            }
         }
         else
         {
            GXv_int5[26] = 1;
         }
         if ( ! (DateTime.MinValue==AV25TFContratoOcorrenciaNotificacao_Cumprido_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Cumprido] <= @AV25TFContratoOcorrenciaNotificacao_Cumprido_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Cumprido] <= @AV25TFContratoOcorrenciaNotificacao_Cumprido_To)";
            }
         }
         else
         {
            GXv_int5[27] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV27TFContratoOcorrenciaNotificacao_Protocolo_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV26TFContratoOcorrenciaNotificacao_Protocolo)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Protocolo] like @lV26TFContratoOcorrenciaNotificacao_Protocolo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Protocolo] like @lV26TFContratoOcorrenciaNotificacao_Protocolo)";
            }
         }
         else
         {
            GXv_int5[28] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV27TFContratoOcorrenciaNotificacao_Protocolo_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Protocolo] = @AV27TFContratoOcorrenciaNotificacao_Protocolo_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Protocolo] = @AV27TFContratoOcorrenciaNotificacao_Protocolo_Sel)";
            }
         }
         else
         {
            GXv_int5[29] = 1;
         }
         if ( ! (0==AV28TFContratoOcorrenciaNotificacao_Responsavel) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Responsavel] >= @AV28TFContratoOcorrenciaNotificacao_Responsavel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Responsavel] >= @AV28TFContratoOcorrenciaNotificacao_Responsavel)";
            }
         }
         else
         {
            GXv_int5[30] = 1;
         }
         if ( ! (0==AV29TFContratoOcorrenciaNotificacao_Responsavel_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Responsavel] <= @AV29TFContratoOcorrenciaNotificacao_Responsavel_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Responsavel] <= @AV29TFContratoOcorrenciaNotificacao_Responsavel_To)";
            }
         }
         else
         {
            GXv_int5[31] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV31TFContratoOcorrenciaNotificacao_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV30TFContratoOcorrenciaNotificacao_Nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Pessoa_Nome] like @lV30TFContratoOcorrenciaNotificacao_Nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Pessoa_Nome] like @lV30TFContratoOcorrenciaNotificacao_Nome)";
            }
         }
         else
         {
            GXv_int5[32] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV31TFContratoOcorrenciaNotificacao_Nome_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Pessoa_Nome] = @AV31TFContratoOcorrenciaNotificacao_Nome_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Pessoa_Nome] = @AV31TFContratoOcorrenciaNotificacao_Nome_Sel)";
            }
         }
         else
         {
            GXv_int5[33] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV33TFContratoOcorrenciaNotificacao_Docto_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV32TFContratoOcorrenciaNotificacao_Docto)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Pessoa_Docto] like @lV32TFContratoOcorrenciaNotificacao_Docto)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Pessoa_Docto] like @lV32TFContratoOcorrenciaNotificacao_Docto)";
            }
         }
         else
         {
            GXv_int5[34] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV33TFContratoOcorrenciaNotificacao_Docto_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Pessoa_Docto] = @AV33TFContratoOcorrenciaNotificacao_Docto_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Pessoa_Docto] = @AV33TFContratoOcorrenciaNotificacao_Docto_Sel)";
            }
         }
         else
         {
            GXv_int5[35] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T1.[ContratoOcorrenciaNotificacao_Protocolo]";
         GXv_Object6[0] = scmdbuf;
         GXv_Object6[1] = GXv_int5;
         return GXv_Object6 ;
      }

      protected Object[] conditional_P00KA5( IGxContext context ,
                                             String AV52DynamicFiltersSelector1 ,
                                             DateTime AV54ContratoOcorrenciaNotificacao_Data1 ,
                                             DateTime AV55ContratoOcorrenciaNotificacao_Data_To1 ,
                                             short AV53DynamicFiltersOperator1 ,
                                             String AV56ContratoOcorrenciaNotificacao_Nome1 ,
                                             bool AV57DynamicFiltersEnabled2 ,
                                             String AV58DynamicFiltersSelector2 ,
                                             DateTime AV60ContratoOcorrenciaNotificacao_Data2 ,
                                             DateTime AV61ContratoOcorrenciaNotificacao_Data_To2 ,
                                             short AV59DynamicFiltersOperator2 ,
                                             String AV62ContratoOcorrenciaNotificacao_Nome2 ,
                                             bool AV63DynamicFiltersEnabled3 ,
                                             String AV64DynamicFiltersSelector3 ,
                                             DateTime AV66ContratoOcorrenciaNotificacao_Data3 ,
                                             DateTime AV67ContratoOcorrenciaNotificacao_Data_To3 ,
                                             short AV65DynamicFiltersOperator3 ,
                                             String AV68ContratoOcorrenciaNotificacao_Nome3 ,
                                             int AV10TFContratoOcorrenciaNotificacao_Codigo ,
                                             int AV11TFContratoOcorrenciaNotificacao_Codigo_To ,
                                             int AV12TFContratoOcorrencia_Codigo ,
                                             int AV13TFContratoOcorrencia_Codigo_To ,
                                             int AV14TFContrato_Codigo ,
                                             int AV15TFContrato_Codigo_To ,
                                             String AV17TFContrato_Numero_Sel ,
                                             String AV16TFContrato_Numero ,
                                             DateTime AV18TFContratoOcorrenciaNotificacao_Data ,
                                             DateTime AV19TFContratoOcorrenciaNotificacao_Data_To ,
                                             short AV20TFContratoOcorrenciaNotificacao_Prazo ,
                                             short AV21TFContratoOcorrenciaNotificacao_Prazo_To ,
                                             String AV23TFContratoOcorrenciaNotificacao_Descricao_Sel ,
                                             String AV22TFContratoOcorrenciaNotificacao_Descricao ,
                                             DateTime AV24TFContratoOcorrenciaNotificacao_Cumprido ,
                                             DateTime AV25TFContratoOcorrenciaNotificacao_Cumprido_To ,
                                             String AV27TFContratoOcorrenciaNotificacao_Protocolo_Sel ,
                                             String AV26TFContratoOcorrenciaNotificacao_Protocolo ,
                                             int AV28TFContratoOcorrenciaNotificacao_Responsavel ,
                                             int AV29TFContratoOcorrenciaNotificacao_Responsavel_To ,
                                             String AV31TFContratoOcorrenciaNotificacao_Nome_Sel ,
                                             String AV30TFContratoOcorrenciaNotificacao_Nome ,
                                             String AV33TFContratoOcorrenciaNotificacao_Docto_Sel ,
                                             String AV32TFContratoOcorrenciaNotificacao_Docto ,
                                             DateTime A298ContratoOcorrenciaNotificacao_Data ,
                                             String A304ContratoOcorrenciaNotificacao_Nome ,
                                             int A297ContratoOcorrenciaNotificacao_Codigo ,
                                             int A294ContratoOcorrencia_Codigo ,
                                             int A74Contrato_Codigo ,
                                             String A77Contrato_Numero ,
                                             short A299ContratoOcorrenciaNotificacao_Prazo ,
                                             String A300ContratoOcorrenciaNotificacao_Descricao ,
                                             DateTime A301ContratoOcorrenciaNotificacao_Cumprido ,
                                             String A302ContratoOcorrenciaNotificacao_Protocolo ,
                                             int A303ContratoOcorrenciaNotificacao_Responsavel ,
                                             String A306ContratoOcorrenciaNotificacao_Docto )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int7 ;
         GXv_int7 = new short [36] ;
         Object[] GXv_Object8 ;
         GXv_Object8 = new Object [2] ;
         scmdbuf = "SELECT T1.[ContratoOcorrenciaNotificacao_Responsavel] AS ContratoOcorrenciaNotificacao_Responsavel, T2.[Pessoa_Docto] AS ContratoOcorrenciaNotificacao_Docto, T1.[ContratoOcorrenciaNotificacao_Protocolo], T1.[ContratoOcorrenciaNotificacao_Cumprido], T1.[ContratoOcorrenciaNotificacao_Descricao], T1.[ContratoOcorrenciaNotificacao_Prazo], T4.[Contrato_Numero], T3.[Contrato_Codigo], T1.[ContratoOcorrencia_Codigo], T1.[ContratoOcorrenciaNotificacao_Codigo], T2.[Pessoa_Nome] AS ContratoOcorrenciaNotificacao_Nome, T1.[ContratoOcorrenciaNotificacao_Data] FROM ((([ContratoOcorrenciaNotificacao] T1 WITH (NOLOCK) INNER JOIN [Pessoa] T2 WITH (NOLOCK) ON T2.[Pessoa_Codigo] = T1.[ContratoOcorrenciaNotificacao_Responsavel]) INNER JOIN [ContratoOcorrencia] T3 WITH (NOLOCK) ON T3.[ContratoOcorrencia_Codigo] = T1.[ContratoOcorrencia_Codigo]) INNER JOIN [Contrato] T4 WITH (NOLOCK) ON T4.[Contrato_Codigo] = T3.[Contrato_Codigo])";
         if ( ( StringUtil.StrCmp(AV52DynamicFiltersSelector1, "CONTRATOOCORRENCIANOTIFICACAO_DATA") == 0 ) && ( ! (DateTime.MinValue==AV54ContratoOcorrenciaNotificacao_Data1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Data] >= @AV54ContratoOcorrenciaNotificacao_Data1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Data] >= @AV54ContratoOcorrenciaNotificacao_Data1)";
            }
         }
         else
         {
            GXv_int7[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV52DynamicFiltersSelector1, "CONTRATOOCORRENCIANOTIFICACAO_DATA") == 0 ) && ( ! (DateTime.MinValue==AV55ContratoOcorrenciaNotificacao_Data_To1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Data] <= @AV55ContratoOcorrenciaNotificacao_Data_To1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Data] <= @AV55ContratoOcorrenciaNotificacao_Data_To1)";
            }
         }
         else
         {
            GXv_int7[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV52DynamicFiltersSelector1, "CONTRATOOCORRENCIANOTIFICACAO_NOME") == 0 ) && ( AV53DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV56ContratoOcorrenciaNotificacao_Nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Pessoa_Nome] like @lV56ContratoOcorrenciaNotificacao_Nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Pessoa_Nome] like @lV56ContratoOcorrenciaNotificacao_Nome1)";
            }
         }
         else
         {
            GXv_int7[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV52DynamicFiltersSelector1, "CONTRATOOCORRENCIANOTIFICACAO_NOME") == 0 ) && ( AV53DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV56ContratoOcorrenciaNotificacao_Nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Pessoa_Nome] like '%' + @lV56ContratoOcorrenciaNotificacao_Nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Pessoa_Nome] like '%' + @lV56ContratoOcorrenciaNotificacao_Nome1)";
            }
         }
         else
         {
            GXv_int7[3] = 1;
         }
         if ( AV57DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV58DynamicFiltersSelector2, "CONTRATOOCORRENCIANOTIFICACAO_DATA") == 0 ) && ( ! (DateTime.MinValue==AV60ContratoOcorrenciaNotificacao_Data2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Data] >= @AV60ContratoOcorrenciaNotificacao_Data2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Data] >= @AV60ContratoOcorrenciaNotificacao_Data2)";
            }
         }
         else
         {
            GXv_int7[4] = 1;
         }
         if ( AV57DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV58DynamicFiltersSelector2, "CONTRATOOCORRENCIANOTIFICACAO_DATA") == 0 ) && ( ! (DateTime.MinValue==AV61ContratoOcorrenciaNotificacao_Data_To2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Data] <= @AV61ContratoOcorrenciaNotificacao_Data_To2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Data] <= @AV61ContratoOcorrenciaNotificacao_Data_To2)";
            }
         }
         else
         {
            GXv_int7[5] = 1;
         }
         if ( AV57DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV58DynamicFiltersSelector2, "CONTRATOOCORRENCIANOTIFICACAO_NOME") == 0 ) && ( AV59DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV62ContratoOcorrenciaNotificacao_Nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Pessoa_Nome] like @lV62ContratoOcorrenciaNotificacao_Nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Pessoa_Nome] like @lV62ContratoOcorrenciaNotificacao_Nome2)";
            }
         }
         else
         {
            GXv_int7[6] = 1;
         }
         if ( AV57DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV58DynamicFiltersSelector2, "CONTRATOOCORRENCIANOTIFICACAO_NOME") == 0 ) && ( AV59DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV62ContratoOcorrenciaNotificacao_Nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Pessoa_Nome] like '%' + @lV62ContratoOcorrenciaNotificacao_Nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Pessoa_Nome] like '%' + @lV62ContratoOcorrenciaNotificacao_Nome2)";
            }
         }
         else
         {
            GXv_int7[7] = 1;
         }
         if ( AV63DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV64DynamicFiltersSelector3, "CONTRATOOCORRENCIANOTIFICACAO_DATA") == 0 ) && ( ! (DateTime.MinValue==AV66ContratoOcorrenciaNotificacao_Data3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Data] >= @AV66ContratoOcorrenciaNotificacao_Data3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Data] >= @AV66ContratoOcorrenciaNotificacao_Data3)";
            }
         }
         else
         {
            GXv_int7[8] = 1;
         }
         if ( AV63DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV64DynamicFiltersSelector3, "CONTRATOOCORRENCIANOTIFICACAO_DATA") == 0 ) && ( ! (DateTime.MinValue==AV67ContratoOcorrenciaNotificacao_Data_To3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Data] <= @AV67ContratoOcorrenciaNotificacao_Data_To3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Data] <= @AV67ContratoOcorrenciaNotificacao_Data_To3)";
            }
         }
         else
         {
            GXv_int7[9] = 1;
         }
         if ( AV63DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV64DynamicFiltersSelector3, "CONTRATOOCORRENCIANOTIFICACAO_NOME") == 0 ) && ( AV65DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV68ContratoOcorrenciaNotificacao_Nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Pessoa_Nome] like @lV68ContratoOcorrenciaNotificacao_Nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Pessoa_Nome] like @lV68ContratoOcorrenciaNotificacao_Nome3)";
            }
         }
         else
         {
            GXv_int7[10] = 1;
         }
         if ( AV63DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV64DynamicFiltersSelector3, "CONTRATOOCORRENCIANOTIFICACAO_NOME") == 0 ) && ( AV65DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV68ContratoOcorrenciaNotificacao_Nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Pessoa_Nome] like '%' + @lV68ContratoOcorrenciaNotificacao_Nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Pessoa_Nome] like '%' + @lV68ContratoOcorrenciaNotificacao_Nome3)";
            }
         }
         else
         {
            GXv_int7[11] = 1;
         }
         if ( ! (0==AV10TFContratoOcorrenciaNotificacao_Codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Codigo] >= @AV10TFContratoOcorrenciaNotificacao_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Codigo] >= @AV10TFContratoOcorrenciaNotificacao_Codigo)";
            }
         }
         else
         {
            GXv_int7[12] = 1;
         }
         if ( ! (0==AV11TFContratoOcorrenciaNotificacao_Codigo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Codigo] <= @AV11TFContratoOcorrenciaNotificacao_Codigo_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Codigo] <= @AV11TFContratoOcorrenciaNotificacao_Codigo_To)";
            }
         }
         else
         {
            GXv_int7[13] = 1;
         }
         if ( ! (0==AV12TFContratoOcorrencia_Codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrencia_Codigo] >= @AV12TFContratoOcorrencia_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrencia_Codigo] >= @AV12TFContratoOcorrencia_Codigo)";
            }
         }
         else
         {
            GXv_int7[14] = 1;
         }
         if ( ! (0==AV13TFContratoOcorrencia_Codigo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrencia_Codigo] <= @AV13TFContratoOcorrencia_Codigo_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrencia_Codigo] <= @AV13TFContratoOcorrencia_Codigo_To)";
            }
         }
         else
         {
            GXv_int7[15] = 1;
         }
         if ( ! (0==AV14TFContrato_Codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Contrato_Codigo] >= @AV14TFContrato_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Contrato_Codigo] >= @AV14TFContrato_Codigo)";
            }
         }
         else
         {
            GXv_int7[16] = 1;
         }
         if ( ! (0==AV15TFContrato_Codigo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Contrato_Codigo] <= @AV15TFContrato_Codigo_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Contrato_Codigo] <= @AV15TFContrato_Codigo_To)";
            }
         }
         else
         {
            GXv_int7[17] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV17TFContrato_Numero_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV16TFContrato_Numero)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Contrato_Numero] like @lV16TFContrato_Numero)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Contrato_Numero] like @lV16TFContrato_Numero)";
            }
         }
         else
         {
            GXv_int7[18] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV17TFContrato_Numero_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Contrato_Numero] = @AV17TFContrato_Numero_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Contrato_Numero] = @AV17TFContrato_Numero_Sel)";
            }
         }
         else
         {
            GXv_int7[19] = 1;
         }
         if ( ! (DateTime.MinValue==AV18TFContratoOcorrenciaNotificacao_Data) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Data] >= @AV18TFContratoOcorrenciaNotificacao_Data)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Data] >= @AV18TFContratoOcorrenciaNotificacao_Data)";
            }
         }
         else
         {
            GXv_int7[20] = 1;
         }
         if ( ! (DateTime.MinValue==AV19TFContratoOcorrenciaNotificacao_Data_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Data] <= @AV19TFContratoOcorrenciaNotificacao_Data_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Data] <= @AV19TFContratoOcorrenciaNotificacao_Data_To)";
            }
         }
         else
         {
            GXv_int7[21] = 1;
         }
         if ( ! (0==AV20TFContratoOcorrenciaNotificacao_Prazo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Prazo] >= @AV20TFContratoOcorrenciaNotificacao_Prazo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Prazo] >= @AV20TFContratoOcorrenciaNotificacao_Prazo)";
            }
         }
         else
         {
            GXv_int7[22] = 1;
         }
         if ( ! (0==AV21TFContratoOcorrenciaNotificacao_Prazo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Prazo] <= @AV21TFContratoOcorrenciaNotificacao_Prazo_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Prazo] <= @AV21TFContratoOcorrenciaNotificacao_Prazo_To)";
            }
         }
         else
         {
            GXv_int7[23] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV23TFContratoOcorrenciaNotificacao_Descricao_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV22TFContratoOcorrenciaNotificacao_Descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Descricao] like @lV22TFContratoOcorrenciaNotificacao_Descricao)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Descricao] like @lV22TFContratoOcorrenciaNotificacao_Descricao)";
            }
         }
         else
         {
            GXv_int7[24] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV23TFContratoOcorrenciaNotificacao_Descricao_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Descricao] = @AV23TFContratoOcorrenciaNotificacao_Descricao_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Descricao] = @AV23TFContratoOcorrenciaNotificacao_Descricao_Sel)";
            }
         }
         else
         {
            GXv_int7[25] = 1;
         }
         if ( ! (DateTime.MinValue==AV24TFContratoOcorrenciaNotificacao_Cumprido) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Cumprido] >= @AV24TFContratoOcorrenciaNotificacao_Cumprido)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Cumprido] >= @AV24TFContratoOcorrenciaNotificacao_Cumprido)";
            }
         }
         else
         {
            GXv_int7[26] = 1;
         }
         if ( ! (DateTime.MinValue==AV25TFContratoOcorrenciaNotificacao_Cumprido_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Cumprido] <= @AV25TFContratoOcorrenciaNotificacao_Cumprido_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Cumprido] <= @AV25TFContratoOcorrenciaNotificacao_Cumprido_To)";
            }
         }
         else
         {
            GXv_int7[27] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV27TFContratoOcorrenciaNotificacao_Protocolo_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV26TFContratoOcorrenciaNotificacao_Protocolo)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Protocolo] like @lV26TFContratoOcorrenciaNotificacao_Protocolo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Protocolo] like @lV26TFContratoOcorrenciaNotificacao_Protocolo)";
            }
         }
         else
         {
            GXv_int7[28] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV27TFContratoOcorrenciaNotificacao_Protocolo_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Protocolo] = @AV27TFContratoOcorrenciaNotificacao_Protocolo_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Protocolo] = @AV27TFContratoOcorrenciaNotificacao_Protocolo_Sel)";
            }
         }
         else
         {
            GXv_int7[29] = 1;
         }
         if ( ! (0==AV28TFContratoOcorrenciaNotificacao_Responsavel) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Responsavel] >= @AV28TFContratoOcorrenciaNotificacao_Responsavel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Responsavel] >= @AV28TFContratoOcorrenciaNotificacao_Responsavel)";
            }
         }
         else
         {
            GXv_int7[30] = 1;
         }
         if ( ! (0==AV29TFContratoOcorrenciaNotificacao_Responsavel_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Responsavel] <= @AV29TFContratoOcorrenciaNotificacao_Responsavel_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Responsavel] <= @AV29TFContratoOcorrenciaNotificacao_Responsavel_To)";
            }
         }
         else
         {
            GXv_int7[31] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV31TFContratoOcorrenciaNotificacao_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV30TFContratoOcorrenciaNotificacao_Nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Pessoa_Nome] like @lV30TFContratoOcorrenciaNotificacao_Nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Pessoa_Nome] like @lV30TFContratoOcorrenciaNotificacao_Nome)";
            }
         }
         else
         {
            GXv_int7[32] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV31TFContratoOcorrenciaNotificacao_Nome_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Pessoa_Nome] = @AV31TFContratoOcorrenciaNotificacao_Nome_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Pessoa_Nome] = @AV31TFContratoOcorrenciaNotificacao_Nome_Sel)";
            }
         }
         else
         {
            GXv_int7[33] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV33TFContratoOcorrenciaNotificacao_Docto_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV32TFContratoOcorrenciaNotificacao_Docto)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Pessoa_Docto] like @lV32TFContratoOcorrenciaNotificacao_Docto)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Pessoa_Docto] like @lV32TFContratoOcorrenciaNotificacao_Docto)";
            }
         }
         else
         {
            GXv_int7[34] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV33TFContratoOcorrenciaNotificacao_Docto_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Pessoa_Docto] = @AV33TFContratoOcorrenciaNotificacao_Docto_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Pessoa_Docto] = @AV33TFContratoOcorrenciaNotificacao_Docto_Sel)";
            }
         }
         else
         {
            GXv_int7[35] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T1.[ContratoOcorrenciaNotificacao_Responsavel]";
         GXv_Object8[0] = scmdbuf;
         GXv_Object8[1] = GXv_int7;
         return GXv_Object8 ;
      }

      protected Object[] conditional_P00KA6( IGxContext context ,
                                             String AV52DynamicFiltersSelector1 ,
                                             DateTime AV54ContratoOcorrenciaNotificacao_Data1 ,
                                             DateTime AV55ContratoOcorrenciaNotificacao_Data_To1 ,
                                             short AV53DynamicFiltersOperator1 ,
                                             String AV56ContratoOcorrenciaNotificacao_Nome1 ,
                                             bool AV57DynamicFiltersEnabled2 ,
                                             String AV58DynamicFiltersSelector2 ,
                                             DateTime AV60ContratoOcorrenciaNotificacao_Data2 ,
                                             DateTime AV61ContratoOcorrenciaNotificacao_Data_To2 ,
                                             short AV59DynamicFiltersOperator2 ,
                                             String AV62ContratoOcorrenciaNotificacao_Nome2 ,
                                             bool AV63DynamicFiltersEnabled3 ,
                                             String AV64DynamicFiltersSelector3 ,
                                             DateTime AV66ContratoOcorrenciaNotificacao_Data3 ,
                                             DateTime AV67ContratoOcorrenciaNotificacao_Data_To3 ,
                                             short AV65DynamicFiltersOperator3 ,
                                             String AV68ContratoOcorrenciaNotificacao_Nome3 ,
                                             int AV10TFContratoOcorrenciaNotificacao_Codigo ,
                                             int AV11TFContratoOcorrenciaNotificacao_Codigo_To ,
                                             int AV12TFContratoOcorrencia_Codigo ,
                                             int AV13TFContratoOcorrencia_Codigo_To ,
                                             int AV14TFContrato_Codigo ,
                                             int AV15TFContrato_Codigo_To ,
                                             String AV17TFContrato_Numero_Sel ,
                                             String AV16TFContrato_Numero ,
                                             DateTime AV18TFContratoOcorrenciaNotificacao_Data ,
                                             DateTime AV19TFContratoOcorrenciaNotificacao_Data_To ,
                                             short AV20TFContratoOcorrenciaNotificacao_Prazo ,
                                             short AV21TFContratoOcorrenciaNotificacao_Prazo_To ,
                                             String AV23TFContratoOcorrenciaNotificacao_Descricao_Sel ,
                                             String AV22TFContratoOcorrenciaNotificacao_Descricao ,
                                             DateTime AV24TFContratoOcorrenciaNotificacao_Cumprido ,
                                             DateTime AV25TFContratoOcorrenciaNotificacao_Cumprido_To ,
                                             String AV27TFContratoOcorrenciaNotificacao_Protocolo_Sel ,
                                             String AV26TFContratoOcorrenciaNotificacao_Protocolo ,
                                             int AV28TFContratoOcorrenciaNotificacao_Responsavel ,
                                             int AV29TFContratoOcorrenciaNotificacao_Responsavel_To ,
                                             String AV31TFContratoOcorrenciaNotificacao_Nome_Sel ,
                                             String AV30TFContratoOcorrenciaNotificacao_Nome ,
                                             String AV33TFContratoOcorrenciaNotificacao_Docto_Sel ,
                                             String AV32TFContratoOcorrenciaNotificacao_Docto ,
                                             DateTime A298ContratoOcorrenciaNotificacao_Data ,
                                             String A304ContratoOcorrenciaNotificacao_Nome ,
                                             int A297ContratoOcorrenciaNotificacao_Codigo ,
                                             int A294ContratoOcorrencia_Codigo ,
                                             int A74Contrato_Codigo ,
                                             String A77Contrato_Numero ,
                                             short A299ContratoOcorrenciaNotificacao_Prazo ,
                                             String A300ContratoOcorrenciaNotificacao_Descricao ,
                                             DateTime A301ContratoOcorrenciaNotificacao_Cumprido ,
                                             String A302ContratoOcorrenciaNotificacao_Protocolo ,
                                             int A303ContratoOcorrenciaNotificacao_Responsavel ,
                                             String A306ContratoOcorrenciaNotificacao_Docto )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int9 ;
         GXv_int9 = new short [36] ;
         Object[] GXv_Object10 ;
         GXv_Object10 = new Object [2] ;
         scmdbuf = "SELECT T2.[Pessoa_Docto] AS ContratoOcorrenciaNotificacao_Docto, T1.[ContratoOcorrenciaNotificacao_Responsavel] AS ContratoOcorrenciaNotificacao_Responsavel, T1.[ContratoOcorrenciaNotificacao_Protocolo], T1.[ContratoOcorrenciaNotificacao_Cumprido], T1.[ContratoOcorrenciaNotificacao_Descricao], T1.[ContratoOcorrenciaNotificacao_Prazo], T4.[Contrato_Numero], T3.[Contrato_Codigo], T1.[ContratoOcorrencia_Codigo], T1.[ContratoOcorrenciaNotificacao_Codigo], T2.[Pessoa_Nome] AS ContratoOcorrenciaNotificacao_Nome, T1.[ContratoOcorrenciaNotificacao_Data] FROM ((([ContratoOcorrenciaNotificacao] T1 WITH (NOLOCK) INNER JOIN [Pessoa] T2 WITH (NOLOCK) ON T2.[Pessoa_Codigo] = T1.[ContratoOcorrenciaNotificacao_Responsavel]) INNER JOIN [ContratoOcorrencia] T3 WITH (NOLOCK) ON T3.[ContratoOcorrencia_Codigo] = T1.[ContratoOcorrencia_Codigo]) INNER JOIN [Contrato] T4 WITH (NOLOCK) ON T4.[Contrato_Codigo] = T3.[Contrato_Codigo])";
         if ( ( StringUtil.StrCmp(AV52DynamicFiltersSelector1, "CONTRATOOCORRENCIANOTIFICACAO_DATA") == 0 ) && ( ! (DateTime.MinValue==AV54ContratoOcorrenciaNotificacao_Data1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Data] >= @AV54ContratoOcorrenciaNotificacao_Data1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Data] >= @AV54ContratoOcorrenciaNotificacao_Data1)";
            }
         }
         else
         {
            GXv_int9[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV52DynamicFiltersSelector1, "CONTRATOOCORRENCIANOTIFICACAO_DATA") == 0 ) && ( ! (DateTime.MinValue==AV55ContratoOcorrenciaNotificacao_Data_To1) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Data] <= @AV55ContratoOcorrenciaNotificacao_Data_To1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Data] <= @AV55ContratoOcorrenciaNotificacao_Data_To1)";
            }
         }
         else
         {
            GXv_int9[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV52DynamicFiltersSelector1, "CONTRATOOCORRENCIANOTIFICACAO_NOME") == 0 ) && ( AV53DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV56ContratoOcorrenciaNotificacao_Nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Pessoa_Nome] like @lV56ContratoOcorrenciaNotificacao_Nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Pessoa_Nome] like @lV56ContratoOcorrenciaNotificacao_Nome1)";
            }
         }
         else
         {
            GXv_int9[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV52DynamicFiltersSelector1, "CONTRATOOCORRENCIANOTIFICACAO_NOME") == 0 ) && ( AV53DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV56ContratoOcorrenciaNotificacao_Nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Pessoa_Nome] like '%' + @lV56ContratoOcorrenciaNotificacao_Nome1)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Pessoa_Nome] like '%' + @lV56ContratoOcorrenciaNotificacao_Nome1)";
            }
         }
         else
         {
            GXv_int9[3] = 1;
         }
         if ( AV57DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV58DynamicFiltersSelector2, "CONTRATOOCORRENCIANOTIFICACAO_DATA") == 0 ) && ( ! (DateTime.MinValue==AV60ContratoOcorrenciaNotificacao_Data2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Data] >= @AV60ContratoOcorrenciaNotificacao_Data2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Data] >= @AV60ContratoOcorrenciaNotificacao_Data2)";
            }
         }
         else
         {
            GXv_int9[4] = 1;
         }
         if ( AV57DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV58DynamicFiltersSelector2, "CONTRATOOCORRENCIANOTIFICACAO_DATA") == 0 ) && ( ! (DateTime.MinValue==AV61ContratoOcorrenciaNotificacao_Data_To2) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Data] <= @AV61ContratoOcorrenciaNotificacao_Data_To2)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Data] <= @AV61ContratoOcorrenciaNotificacao_Data_To2)";
            }
         }
         else
         {
            GXv_int9[5] = 1;
         }
         if ( AV57DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV58DynamicFiltersSelector2, "CONTRATOOCORRENCIANOTIFICACAO_NOME") == 0 ) && ( AV59DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV62ContratoOcorrenciaNotificacao_Nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Pessoa_Nome] like @lV62ContratoOcorrenciaNotificacao_Nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Pessoa_Nome] like @lV62ContratoOcorrenciaNotificacao_Nome2)";
            }
         }
         else
         {
            GXv_int9[6] = 1;
         }
         if ( AV57DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV58DynamicFiltersSelector2, "CONTRATOOCORRENCIANOTIFICACAO_NOME") == 0 ) && ( AV59DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV62ContratoOcorrenciaNotificacao_Nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Pessoa_Nome] like '%' + @lV62ContratoOcorrenciaNotificacao_Nome2)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Pessoa_Nome] like '%' + @lV62ContratoOcorrenciaNotificacao_Nome2)";
            }
         }
         else
         {
            GXv_int9[7] = 1;
         }
         if ( AV63DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV64DynamicFiltersSelector3, "CONTRATOOCORRENCIANOTIFICACAO_DATA") == 0 ) && ( ! (DateTime.MinValue==AV66ContratoOcorrenciaNotificacao_Data3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Data] >= @AV66ContratoOcorrenciaNotificacao_Data3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Data] >= @AV66ContratoOcorrenciaNotificacao_Data3)";
            }
         }
         else
         {
            GXv_int9[8] = 1;
         }
         if ( AV63DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV64DynamicFiltersSelector3, "CONTRATOOCORRENCIANOTIFICACAO_DATA") == 0 ) && ( ! (DateTime.MinValue==AV67ContratoOcorrenciaNotificacao_Data_To3) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Data] <= @AV67ContratoOcorrenciaNotificacao_Data_To3)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Data] <= @AV67ContratoOcorrenciaNotificacao_Data_To3)";
            }
         }
         else
         {
            GXv_int9[9] = 1;
         }
         if ( AV63DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV64DynamicFiltersSelector3, "CONTRATOOCORRENCIANOTIFICACAO_NOME") == 0 ) && ( AV65DynamicFiltersOperator3 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV68ContratoOcorrenciaNotificacao_Nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Pessoa_Nome] like @lV68ContratoOcorrenciaNotificacao_Nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Pessoa_Nome] like @lV68ContratoOcorrenciaNotificacao_Nome3)";
            }
         }
         else
         {
            GXv_int9[10] = 1;
         }
         if ( AV63DynamicFiltersEnabled3 && ( StringUtil.StrCmp(AV64DynamicFiltersSelector3, "CONTRATOOCORRENCIANOTIFICACAO_NOME") == 0 ) && ( AV65DynamicFiltersOperator3 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV68ContratoOcorrenciaNotificacao_Nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Pessoa_Nome] like '%' + @lV68ContratoOcorrenciaNotificacao_Nome3)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Pessoa_Nome] like '%' + @lV68ContratoOcorrenciaNotificacao_Nome3)";
            }
         }
         else
         {
            GXv_int9[11] = 1;
         }
         if ( ! (0==AV10TFContratoOcorrenciaNotificacao_Codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Codigo] >= @AV10TFContratoOcorrenciaNotificacao_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Codigo] >= @AV10TFContratoOcorrenciaNotificacao_Codigo)";
            }
         }
         else
         {
            GXv_int9[12] = 1;
         }
         if ( ! (0==AV11TFContratoOcorrenciaNotificacao_Codigo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Codigo] <= @AV11TFContratoOcorrenciaNotificacao_Codigo_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Codigo] <= @AV11TFContratoOcorrenciaNotificacao_Codigo_To)";
            }
         }
         else
         {
            GXv_int9[13] = 1;
         }
         if ( ! (0==AV12TFContratoOcorrencia_Codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrencia_Codigo] >= @AV12TFContratoOcorrencia_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrencia_Codigo] >= @AV12TFContratoOcorrencia_Codigo)";
            }
         }
         else
         {
            GXv_int9[14] = 1;
         }
         if ( ! (0==AV13TFContratoOcorrencia_Codigo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrencia_Codigo] <= @AV13TFContratoOcorrencia_Codigo_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrencia_Codigo] <= @AV13TFContratoOcorrencia_Codigo_To)";
            }
         }
         else
         {
            GXv_int9[15] = 1;
         }
         if ( ! (0==AV14TFContrato_Codigo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Contrato_Codigo] >= @AV14TFContrato_Codigo)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Contrato_Codigo] >= @AV14TFContrato_Codigo)";
            }
         }
         else
         {
            GXv_int9[16] = 1;
         }
         if ( ! (0==AV15TFContrato_Codigo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Contrato_Codigo] <= @AV15TFContrato_Codigo_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Contrato_Codigo] <= @AV15TFContrato_Codigo_To)";
            }
         }
         else
         {
            GXv_int9[17] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV17TFContrato_Numero_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV16TFContrato_Numero)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Contrato_Numero] like @lV16TFContrato_Numero)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Contrato_Numero] like @lV16TFContrato_Numero)";
            }
         }
         else
         {
            GXv_int9[18] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV17TFContrato_Numero_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Contrato_Numero] = @AV17TFContrato_Numero_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Contrato_Numero] = @AV17TFContrato_Numero_Sel)";
            }
         }
         else
         {
            GXv_int9[19] = 1;
         }
         if ( ! (DateTime.MinValue==AV18TFContratoOcorrenciaNotificacao_Data) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Data] >= @AV18TFContratoOcorrenciaNotificacao_Data)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Data] >= @AV18TFContratoOcorrenciaNotificacao_Data)";
            }
         }
         else
         {
            GXv_int9[20] = 1;
         }
         if ( ! (DateTime.MinValue==AV19TFContratoOcorrenciaNotificacao_Data_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Data] <= @AV19TFContratoOcorrenciaNotificacao_Data_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Data] <= @AV19TFContratoOcorrenciaNotificacao_Data_To)";
            }
         }
         else
         {
            GXv_int9[21] = 1;
         }
         if ( ! (0==AV20TFContratoOcorrenciaNotificacao_Prazo) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Prazo] >= @AV20TFContratoOcorrenciaNotificacao_Prazo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Prazo] >= @AV20TFContratoOcorrenciaNotificacao_Prazo)";
            }
         }
         else
         {
            GXv_int9[22] = 1;
         }
         if ( ! (0==AV21TFContratoOcorrenciaNotificacao_Prazo_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Prazo] <= @AV21TFContratoOcorrenciaNotificacao_Prazo_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Prazo] <= @AV21TFContratoOcorrenciaNotificacao_Prazo_To)";
            }
         }
         else
         {
            GXv_int9[23] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV23TFContratoOcorrenciaNotificacao_Descricao_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV22TFContratoOcorrenciaNotificacao_Descricao)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Descricao] like @lV22TFContratoOcorrenciaNotificacao_Descricao)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Descricao] like @lV22TFContratoOcorrenciaNotificacao_Descricao)";
            }
         }
         else
         {
            GXv_int9[24] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV23TFContratoOcorrenciaNotificacao_Descricao_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Descricao] = @AV23TFContratoOcorrenciaNotificacao_Descricao_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Descricao] = @AV23TFContratoOcorrenciaNotificacao_Descricao_Sel)";
            }
         }
         else
         {
            GXv_int9[25] = 1;
         }
         if ( ! (DateTime.MinValue==AV24TFContratoOcorrenciaNotificacao_Cumprido) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Cumprido] >= @AV24TFContratoOcorrenciaNotificacao_Cumprido)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Cumprido] >= @AV24TFContratoOcorrenciaNotificacao_Cumprido)";
            }
         }
         else
         {
            GXv_int9[26] = 1;
         }
         if ( ! (DateTime.MinValue==AV25TFContratoOcorrenciaNotificacao_Cumprido_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Cumprido] <= @AV25TFContratoOcorrenciaNotificacao_Cumprido_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Cumprido] <= @AV25TFContratoOcorrenciaNotificacao_Cumprido_To)";
            }
         }
         else
         {
            GXv_int9[27] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV27TFContratoOcorrenciaNotificacao_Protocolo_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV26TFContratoOcorrenciaNotificacao_Protocolo)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Protocolo] like @lV26TFContratoOcorrenciaNotificacao_Protocolo)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Protocolo] like @lV26TFContratoOcorrenciaNotificacao_Protocolo)";
            }
         }
         else
         {
            GXv_int9[28] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV27TFContratoOcorrenciaNotificacao_Protocolo_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Protocolo] = @AV27TFContratoOcorrenciaNotificacao_Protocolo_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Protocolo] = @AV27TFContratoOcorrenciaNotificacao_Protocolo_Sel)";
            }
         }
         else
         {
            GXv_int9[29] = 1;
         }
         if ( ! (0==AV28TFContratoOcorrenciaNotificacao_Responsavel) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Responsavel] >= @AV28TFContratoOcorrenciaNotificacao_Responsavel)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Responsavel] >= @AV28TFContratoOcorrenciaNotificacao_Responsavel)";
            }
         }
         else
         {
            GXv_int9[30] = 1;
         }
         if ( ! (0==AV29TFContratoOcorrenciaNotificacao_Responsavel_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[ContratoOcorrenciaNotificacao_Responsavel] <= @AV29TFContratoOcorrenciaNotificacao_Responsavel_To)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[ContratoOcorrenciaNotificacao_Responsavel] <= @AV29TFContratoOcorrenciaNotificacao_Responsavel_To)";
            }
         }
         else
         {
            GXv_int9[31] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV31TFContratoOcorrenciaNotificacao_Nome_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV30TFContratoOcorrenciaNotificacao_Nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Pessoa_Nome] like @lV30TFContratoOcorrenciaNotificacao_Nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Pessoa_Nome] like @lV30TFContratoOcorrenciaNotificacao_Nome)";
            }
         }
         else
         {
            GXv_int9[32] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV31TFContratoOcorrenciaNotificacao_Nome_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Pessoa_Nome] = @AV31TFContratoOcorrenciaNotificacao_Nome_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Pessoa_Nome] = @AV31TFContratoOcorrenciaNotificacao_Nome_Sel)";
            }
         }
         else
         {
            GXv_int9[33] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV33TFContratoOcorrenciaNotificacao_Docto_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV32TFContratoOcorrenciaNotificacao_Docto)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Pessoa_Docto] like @lV32TFContratoOcorrenciaNotificacao_Docto)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Pessoa_Docto] like @lV32TFContratoOcorrenciaNotificacao_Docto)";
            }
         }
         else
         {
            GXv_int9[34] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV33TFContratoOcorrenciaNotificacao_Docto_Sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Pessoa_Docto] = @AV33TFContratoOcorrenciaNotificacao_Docto_Sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Pessoa_Docto] = @AV33TFContratoOcorrenciaNotificacao_Docto_Sel)";
            }
         }
         else
         {
            GXv_int9[35] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY T2.[Pessoa_Docto]";
         GXv_Object10[0] = scmdbuf;
         GXv_Object10[1] = GXv_int9;
         return GXv_Object10 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00KA2(context, (String)dynConstraints[0] , (DateTime)dynConstraints[1] , (DateTime)dynConstraints[2] , (short)dynConstraints[3] , (String)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (DateTime)dynConstraints[7] , (DateTime)dynConstraints[8] , (short)dynConstraints[9] , (String)dynConstraints[10] , (bool)dynConstraints[11] , (String)dynConstraints[12] , (DateTime)dynConstraints[13] , (DateTime)dynConstraints[14] , (short)dynConstraints[15] , (String)dynConstraints[16] , (int)dynConstraints[17] , (int)dynConstraints[18] , (int)dynConstraints[19] , (int)dynConstraints[20] , (int)dynConstraints[21] , (int)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (DateTime)dynConstraints[25] , (DateTime)dynConstraints[26] , (short)dynConstraints[27] , (short)dynConstraints[28] , (String)dynConstraints[29] , (String)dynConstraints[30] , (DateTime)dynConstraints[31] , (DateTime)dynConstraints[32] , (String)dynConstraints[33] , (String)dynConstraints[34] , (int)dynConstraints[35] , (int)dynConstraints[36] , (String)dynConstraints[37] , (String)dynConstraints[38] , (String)dynConstraints[39] , (String)dynConstraints[40] , (DateTime)dynConstraints[41] , (String)dynConstraints[42] , (int)dynConstraints[43] , (int)dynConstraints[44] , (int)dynConstraints[45] , (String)dynConstraints[46] , (short)dynConstraints[47] , (String)dynConstraints[48] , (DateTime)dynConstraints[49] , (String)dynConstraints[50] , (int)dynConstraints[51] , (String)dynConstraints[52] );
               case 1 :
                     return conditional_P00KA3(context, (String)dynConstraints[0] , (DateTime)dynConstraints[1] , (DateTime)dynConstraints[2] , (short)dynConstraints[3] , (String)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (DateTime)dynConstraints[7] , (DateTime)dynConstraints[8] , (short)dynConstraints[9] , (String)dynConstraints[10] , (bool)dynConstraints[11] , (String)dynConstraints[12] , (DateTime)dynConstraints[13] , (DateTime)dynConstraints[14] , (short)dynConstraints[15] , (String)dynConstraints[16] , (int)dynConstraints[17] , (int)dynConstraints[18] , (int)dynConstraints[19] , (int)dynConstraints[20] , (int)dynConstraints[21] , (int)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (DateTime)dynConstraints[25] , (DateTime)dynConstraints[26] , (short)dynConstraints[27] , (short)dynConstraints[28] , (String)dynConstraints[29] , (String)dynConstraints[30] , (DateTime)dynConstraints[31] , (DateTime)dynConstraints[32] , (String)dynConstraints[33] , (String)dynConstraints[34] , (int)dynConstraints[35] , (int)dynConstraints[36] , (String)dynConstraints[37] , (String)dynConstraints[38] , (String)dynConstraints[39] , (String)dynConstraints[40] , (DateTime)dynConstraints[41] , (String)dynConstraints[42] , (int)dynConstraints[43] , (int)dynConstraints[44] , (int)dynConstraints[45] , (String)dynConstraints[46] , (short)dynConstraints[47] , (String)dynConstraints[48] , (DateTime)dynConstraints[49] , (String)dynConstraints[50] , (int)dynConstraints[51] , (String)dynConstraints[52] );
               case 2 :
                     return conditional_P00KA4(context, (String)dynConstraints[0] , (DateTime)dynConstraints[1] , (DateTime)dynConstraints[2] , (short)dynConstraints[3] , (String)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (DateTime)dynConstraints[7] , (DateTime)dynConstraints[8] , (short)dynConstraints[9] , (String)dynConstraints[10] , (bool)dynConstraints[11] , (String)dynConstraints[12] , (DateTime)dynConstraints[13] , (DateTime)dynConstraints[14] , (short)dynConstraints[15] , (String)dynConstraints[16] , (int)dynConstraints[17] , (int)dynConstraints[18] , (int)dynConstraints[19] , (int)dynConstraints[20] , (int)dynConstraints[21] , (int)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (DateTime)dynConstraints[25] , (DateTime)dynConstraints[26] , (short)dynConstraints[27] , (short)dynConstraints[28] , (String)dynConstraints[29] , (String)dynConstraints[30] , (DateTime)dynConstraints[31] , (DateTime)dynConstraints[32] , (String)dynConstraints[33] , (String)dynConstraints[34] , (int)dynConstraints[35] , (int)dynConstraints[36] , (String)dynConstraints[37] , (String)dynConstraints[38] , (String)dynConstraints[39] , (String)dynConstraints[40] , (DateTime)dynConstraints[41] , (String)dynConstraints[42] , (int)dynConstraints[43] , (int)dynConstraints[44] , (int)dynConstraints[45] , (String)dynConstraints[46] , (short)dynConstraints[47] , (String)dynConstraints[48] , (DateTime)dynConstraints[49] , (String)dynConstraints[50] , (int)dynConstraints[51] , (String)dynConstraints[52] );
               case 3 :
                     return conditional_P00KA5(context, (String)dynConstraints[0] , (DateTime)dynConstraints[1] , (DateTime)dynConstraints[2] , (short)dynConstraints[3] , (String)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (DateTime)dynConstraints[7] , (DateTime)dynConstraints[8] , (short)dynConstraints[9] , (String)dynConstraints[10] , (bool)dynConstraints[11] , (String)dynConstraints[12] , (DateTime)dynConstraints[13] , (DateTime)dynConstraints[14] , (short)dynConstraints[15] , (String)dynConstraints[16] , (int)dynConstraints[17] , (int)dynConstraints[18] , (int)dynConstraints[19] , (int)dynConstraints[20] , (int)dynConstraints[21] , (int)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (DateTime)dynConstraints[25] , (DateTime)dynConstraints[26] , (short)dynConstraints[27] , (short)dynConstraints[28] , (String)dynConstraints[29] , (String)dynConstraints[30] , (DateTime)dynConstraints[31] , (DateTime)dynConstraints[32] , (String)dynConstraints[33] , (String)dynConstraints[34] , (int)dynConstraints[35] , (int)dynConstraints[36] , (String)dynConstraints[37] , (String)dynConstraints[38] , (String)dynConstraints[39] , (String)dynConstraints[40] , (DateTime)dynConstraints[41] , (String)dynConstraints[42] , (int)dynConstraints[43] , (int)dynConstraints[44] , (int)dynConstraints[45] , (String)dynConstraints[46] , (short)dynConstraints[47] , (String)dynConstraints[48] , (DateTime)dynConstraints[49] , (String)dynConstraints[50] , (int)dynConstraints[51] , (String)dynConstraints[52] );
               case 4 :
                     return conditional_P00KA6(context, (String)dynConstraints[0] , (DateTime)dynConstraints[1] , (DateTime)dynConstraints[2] , (short)dynConstraints[3] , (String)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (DateTime)dynConstraints[7] , (DateTime)dynConstraints[8] , (short)dynConstraints[9] , (String)dynConstraints[10] , (bool)dynConstraints[11] , (String)dynConstraints[12] , (DateTime)dynConstraints[13] , (DateTime)dynConstraints[14] , (short)dynConstraints[15] , (String)dynConstraints[16] , (int)dynConstraints[17] , (int)dynConstraints[18] , (int)dynConstraints[19] , (int)dynConstraints[20] , (int)dynConstraints[21] , (int)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (DateTime)dynConstraints[25] , (DateTime)dynConstraints[26] , (short)dynConstraints[27] , (short)dynConstraints[28] , (String)dynConstraints[29] , (String)dynConstraints[30] , (DateTime)dynConstraints[31] , (DateTime)dynConstraints[32] , (String)dynConstraints[33] , (String)dynConstraints[34] , (int)dynConstraints[35] , (int)dynConstraints[36] , (String)dynConstraints[37] , (String)dynConstraints[38] , (String)dynConstraints[39] , (String)dynConstraints[40] , (DateTime)dynConstraints[41] , (String)dynConstraints[42] , (int)dynConstraints[43] , (int)dynConstraints[44] , (int)dynConstraints[45] , (String)dynConstraints[46] , (short)dynConstraints[47] , (String)dynConstraints[48] , (DateTime)dynConstraints[49] , (String)dynConstraints[50] , (int)dynConstraints[51] , (String)dynConstraints[52] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00KA2 ;
          prmP00KA2 = new Object[] {
          new Object[] {"@AV54ContratoOcorrenciaNotificacao_Data1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV55ContratoOcorrenciaNotificacao_Data_To1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV56ContratoOcorrenciaNotificacao_Nome1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV56ContratoOcorrenciaNotificacao_Nome1",SqlDbType.Char,100,0} ,
          new Object[] {"@AV60ContratoOcorrenciaNotificacao_Data2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV61ContratoOcorrenciaNotificacao_Data_To2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV62ContratoOcorrenciaNotificacao_Nome2",SqlDbType.Char,100,0} ,
          new Object[] {"@lV62ContratoOcorrenciaNotificacao_Nome2",SqlDbType.Char,100,0} ,
          new Object[] {"@AV66ContratoOcorrenciaNotificacao_Data3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV67ContratoOcorrenciaNotificacao_Data_To3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV68ContratoOcorrenciaNotificacao_Nome3",SqlDbType.Char,100,0} ,
          new Object[] {"@lV68ContratoOcorrenciaNotificacao_Nome3",SqlDbType.Char,100,0} ,
          new Object[] {"@AV10TFContratoOcorrenciaNotificacao_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV11TFContratoOcorrenciaNotificacao_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV12TFContratoOcorrencia_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV13TFContratoOcorrencia_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV14TFContrato_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV15TFContrato_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@lV16TFContrato_Numero",SqlDbType.Char,20,0} ,
          new Object[] {"@AV17TFContrato_Numero_Sel",SqlDbType.Char,20,0} ,
          new Object[] {"@AV18TFContratoOcorrenciaNotificacao_Data",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV19TFContratoOcorrenciaNotificacao_Data_To",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV20TFContratoOcorrenciaNotificacao_Prazo",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV21TFContratoOcorrenciaNotificacao_Prazo_To",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@lV22TFContratoOcorrenciaNotificacao_Descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV23TFContratoOcorrenciaNotificacao_Descricao_Sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV24TFContratoOcorrenciaNotificacao_Cumprido",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV25TFContratoOcorrenciaNotificacao_Cumprido_To",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV26TFContratoOcorrenciaNotificacao_Protocolo",SqlDbType.VarChar,20,0} ,
          new Object[] {"@AV27TFContratoOcorrenciaNotificacao_Protocolo_Sel",SqlDbType.VarChar,20,0} ,
          new Object[] {"@AV28TFContratoOcorrenciaNotificacao_Responsavel",SqlDbType.Int,6,0} ,
          new Object[] {"@AV29TFContratoOcorrenciaNotificacao_Responsavel_To",SqlDbType.Int,6,0} ,
          new Object[] {"@lV30TFContratoOcorrenciaNotificacao_Nome",SqlDbType.Char,100,0} ,
          new Object[] {"@AV31TFContratoOcorrenciaNotificacao_Nome_Sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV32TFContratoOcorrenciaNotificacao_Docto",SqlDbType.VarChar,15,0} ,
          new Object[] {"@AV33TFContratoOcorrenciaNotificacao_Docto_Sel",SqlDbType.VarChar,15,0}
          } ;
          Object[] prmP00KA3 ;
          prmP00KA3 = new Object[] {
          new Object[] {"@AV54ContratoOcorrenciaNotificacao_Data1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV55ContratoOcorrenciaNotificacao_Data_To1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV56ContratoOcorrenciaNotificacao_Nome1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV56ContratoOcorrenciaNotificacao_Nome1",SqlDbType.Char,100,0} ,
          new Object[] {"@AV60ContratoOcorrenciaNotificacao_Data2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV61ContratoOcorrenciaNotificacao_Data_To2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV62ContratoOcorrenciaNotificacao_Nome2",SqlDbType.Char,100,0} ,
          new Object[] {"@lV62ContratoOcorrenciaNotificacao_Nome2",SqlDbType.Char,100,0} ,
          new Object[] {"@AV66ContratoOcorrenciaNotificacao_Data3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV67ContratoOcorrenciaNotificacao_Data_To3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV68ContratoOcorrenciaNotificacao_Nome3",SqlDbType.Char,100,0} ,
          new Object[] {"@lV68ContratoOcorrenciaNotificacao_Nome3",SqlDbType.Char,100,0} ,
          new Object[] {"@AV10TFContratoOcorrenciaNotificacao_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV11TFContratoOcorrenciaNotificacao_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV12TFContratoOcorrencia_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV13TFContratoOcorrencia_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV14TFContrato_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV15TFContrato_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@lV16TFContrato_Numero",SqlDbType.Char,20,0} ,
          new Object[] {"@AV17TFContrato_Numero_Sel",SqlDbType.Char,20,0} ,
          new Object[] {"@AV18TFContratoOcorrenciaNotificacao_Data",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV19TFContratoOcorrenciaNotificacao_Data_To",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV20TFContratoOcorrenciaNotificacao_Prazo",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV21TFContratoOcorrenciaNotificacao_Prazo_To",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@lV22TFContratoOcorrenciaNotificacao_Descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV23TFContratoOcorrenciaNotificacao_Descricao_Sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV24TFContratoOcorrenciaNotificacao_Cumprido",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV25TFContratoOcorrenciaNotificacao_Cumprido_To",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV26TFContratoOcorrenciaNotificacao_Protocolo",SqlDbType.VarChar,20,0} ,
          new Object[] {"@AV27TFContratoOcorrenciaNotificacao_Protocolo_Sel",SqlDbType.VarChar,20,0} ,
          new Object[] {"@AV28TFContratoOcorrenciaNotificacao_Responsavel",SqlDbType.Int,6,0} ,
          new Object[] {"@AV29TFContratoOcorrenciaNotificacao_Responsavel_To",SqlDbType.Int,6,0} ,
          new Object[] {"@lV30TFContratoOcorrenciaNotificacao_Nome",SqlDbType.Char,100,0} ,
          new Object[] {"@AV31TFContratoOcorrenciaNotificacao_Nome_Sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV32TFContratoOcorrenciaNotificacao_Docto",SqlDbType.VarChar,15,0} ,
          new Object[] {"@AV33TFContratoOcorrenciaNotificacao_Docto_Sel",SqlDbType.VarChar,15,0}
          } ;
          Object[] prmP00KA4 ;
          prmP00KA4 = new Object[] {
          new Object[] {"@AV54ContratoOcorrenciaNotificacao_Data1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV55ContratoOcorrenciaNotificacao_Data_To1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV56ContratoOcorrenciaNotificacao_Nome1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV56ContratoOcorrenciaNotificacao_Nome1",SqlDbType.Char,100,0} ,
          new Object[] {"@AV60ContratoOcorrenciaNotificacao_Data2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV61ContratoOcorrenciaNotificacao_Data_To2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV62ContratoOcorrenciaNotificacao_Nome2",SqlDbType.Char,100,0} ,
          new Object[] {"@lV62ContratoOcorrenciaNotificacao_Nome2",SqlDbType.Char,100,0} ,
          new Object[] {"@AV66ContratoOcorrenciaNotificacao_Data3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV67ContratoOcorrenciaNotificacao_Data_To3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV68ContratoOcorrenciaNotificacao_Nome3",SqlDbType.Char,100,0} ,
          new Object[] {"@lV68ContratoOcorrenciaNotificacao_Nome3",SqlDbType.Char,100,0} ,
          new Object[] {"@AV10TFContratoOcorrenciaNotificacao_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV11TFContratoOcorrenciaNotificacao_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV12TFContratoOcorrencia_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV13TFContratoOcorrencia_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV14TFContrato_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV15TFContrato_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@lV16TFContrato_Numero",SqlDbType.Char,20,0} ,
          new Object[] {"@AV17TFContrato_Numero_Sel",SqlDbType.Char,20,0} ,
          new Object[] {"@AV18TFContratoOcorrenciaNotificacao_Data",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV19TFContratoOcorrenciaNotificacao_Data_To",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV20TFContratoOcorrenciaNotificacao_Prazo",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV21TFContratoOcorrenciaNotificacao_Prazo_To",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@lV22TFContratoOcorrenciaNotificacao_Descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV23TFContratoOcorrenciaNotificacao_Descricao_Sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV24TFContratoOcorrenciaNotificacao_Cumprido",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV25TFContratoOcorrenciaNotificacao_Cumprido_To",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV26TFContratoOcorrenciaNotificacao_Protocolo",SqlDbType.VarChar,20,0} ,
          new Object[] {"@AV27TFContratoOcorrenciaNotificacao_Protocolo_Sel",SqlDbType.VarChar,20,0} ,
          new Object[] {"@AV28TFContratoOcorrenciaNotificacao_Responsavel",SqlDbType.Int,6,0} ,
          new Object[] {"@AV29TFContratoOcorrenciaNotificacao_Responsavel_To",SqlDbType.Int,6,0} ,
          new Object[] {"@lV30TFContratoOcorrenciaNotificacao_Nome",SqlDbType.Char,100,0} ,
          new Object[] {"@AV31TFContratoOcorrenciaNotificacao_Nome_Sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV32TFContratoOcorrenciaNotificacao_Docto",SqlDbType.VarChar,15,0} ,
          new Object[] {"@AV33TFContratoOcorrenciaNotificacao_Docto_Sel",SqlDbType.VarChar,15,0}
          } ;
          Object[] prmP00KA5 ;
          prmP00KA5 = new Object[] {
          new Object[] {"@AV54ContratoOcorrenciaNotificacao_Data1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV55ContratoOcorrenciaNotificacao_Data_To1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV56ContratoOcorrenciaNotificacao_Nome1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV56ContratoOcorrenciaNotificacao_Nome1",SqlDbType.Char,100,0} ,
          new Object[] {"@AV60ContratoOcorrenciaNotificacao_Data2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV61ContratoOcorrenciaNotificacao_Data_To2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV62ContratoOcorrenciaNotificacao_Nome2",SqlDbType.Char,100,0} ,
          new Object[] {"@lV62ContratoOcorrenciaNotificacao_Nome2",SqlDbType.Char,100,0} ,
          new Object[] {"@AV66ContratoOcorrenciaNotificacao_Data3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV67ContratoOcorrenciaNotificacao_Data_To3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV68ContratoOcorrenciaNotificacao_Nome3",SqlDbType.Char,100,0} ,
          new Object[] {"@lV68ContratoOcorrenciaNotificacao_Nome3",SqlDbType.Char,100,0} ,
          new Object[] {"@AV10TFContratoOcorrenciaNotificacao_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV11TFContratoOcorrenciaNotificacao_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV12TFContratoOcorrencia_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV13TFContratoOcorrencia_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV14TFContrato_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV15TFContrato_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@lV16TFContrato_Numero",SqlDbType.Char,20,0} ,
          new Object[] {"@AV17TFContrato_Numero_Sel",SqlDbType.Char,20,0} ,
          new Object[] {"@AV18TFContratoOcorrenciaNotificacao_Data",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV19TFContratoOcorrenciaNotificacao_Data_To",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV20TFContratoOcorrenciaNotificacao_Prazo",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV21TFContratoOcorrenciaNotificacao_Prazo_To",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@lV22TFContratoOcorrenciaNotificacao_Descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV23TFContratoOcorrenciaNotificacao_Descricao_Sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV24TFContratoOcorrenciaNotificacao_Cumprido",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV25TFContratoOcorrenciaNotificacao_Cumprido_To",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV26TFContratoOcorrenciaNotificacao_Protocolo",SqlDbType.VarChar,20,0} ,
          new Object[] {"@AV27TFContratoOcorrenciaNotificacao_Protocolo_Sel",SqlDbType.VarChar,20,0} ,
          new Object[] {"@AV28TFContratoOcorrenciaNotificacao_Responsavel",SqlDbType.Int,6,0} ,
          new Object[] {"@AV29TFContratoOcorrenciaNotificacao_Responsavel_To",SqlDbType.Int,6,0} ,
          new Object[] {"@lV30TFContratoOcorrenciaNotificacao_Nome",SqlDbType.Char,100,0} ,
          new Object[] {"@AV31TFContratoOcorrenciaNotificacao_Nome_Sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV32TFContratoOcorrenciaNotificacao_Docto",SqlDbType.VarChar,15,0} ,
          new Object[] {"@AV33TFContratoOcorrenciaNotificacao_Docto_Sel",SqlDbType.VarChar,15,0}
          } ;
          Object[] prmP00KA6 ;
          prmP00KA6 = new Object[] {
          new Object[] {"@AV54ContratoOcorrenciaNotificacao_Data1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV55ContratoOcorrenciaNotificacao_Data_To1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV56ContratoOcorrenciaNotificacao_Nome1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV56ContratoOcorrenciaNotificacao_Nome1",SqlDbType.Char,100,0} ,
          new Object[] {"@AV60ContratoOcorrenciaNotificacao_Data2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV61ContratoOcorrenciaNotificacao_Data_To2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV62ContratoOcorrenciaNotificacao_Nome2",SqlDbType.Char,100,0} ,
          new Object[] {"@lV62ContratoOcorrenciaNotificacao_Nome2",SqlDbType.Char,100,0} ,
          new Object[] {"@AV66ContratoOcorrenciaNotificacao_Data3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV67ContratoOcorrenciaNotificacao_Data_To3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV68ContratoOcorrenciaNotificacao_Nome3",SqlDbType.Char,100,0} ,
          new Object[] {"@lV68ContratoOcorrenciaNotificacao_Nome3",SqlDbType.Char,100,0} ,
          new Object[] {"@AV10TFContratoOcorrenciaNotificacao_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV11TFContratoOcorrenciaNotificacao_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV12TFContratoOcorrencia_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV13TFContratoOcorrencia_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV14TFContrato_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV15TFContrato_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@lV16TFContrato_Numero",SqlDbType.Char,20,0} ,
          new Object[] {"@AV17TFContrato_Numero_Sel",SqlDbType.Char,20,0} ,
          new Object[] {"@AV18TFContratoOcorrenciaNotificacao_Data",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV19TFContratoOcorrenciaNotificacao_Data_To",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV20TFContratoOcorrenciaNotificacao_Prazo",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV21TFContratoOcorrenciaNotificacao_Prazo_To",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@lV22TFContratoOcorrenciaNotificacao_Descricao",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV23TFContratoOcorrenciaNotificacao_Descricao_Sel",SqlDbType.VarChar,50,0} ,
          new Object[] {"@AV24TFContratoOcorrenciaNotificacao_Cumprido",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV25TFContratoOcorrenciaNotificacao_Cumprido_To",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV26TFContratoOcorrenciaNotificacao_Protocolo",SqlDbType.VarChar,20,0} ,
          new Object[] {"@AV27TFContratoOcorrenciaNotificacao_Protocolo_Sel",SqlDbType.VarChar,20,0} ,
          new Object[] {"@AV28TFContratoOcorrenciaNotificacao_Responsavel",SqlDbType.Int,6,0} ,
          new Object[] {"@AV29TFContratoOcorrenciaNotificacao_Responsavel_To",SqlDbType.Int,6,0} ,
          new Object[] {"@lV30TFContratoOcorrenciaNotificacao_Nome",SqlDbType.Char,100,0} ,
          new Object[] {"@AV31TFContratoOcorrenciaNotificacao_Nome_Sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV32TFContratoOcorrenciaNotificacao_Docto",SqlDbType.VarChar,15,0} ,
          new Object[] {"@AV33TFContratoOcorrenciaNotificacao_Docto_Sel",SqlDbType.VarChar,15,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00KA2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00KA2,100,0,true,false )
             ,new CursorDef("P00KA3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00KA3,100,0,true,false )
             ,new CursorDef("P00KA4", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00KA4,100,0,true,false )
             ,new CursorDef("P00KA5", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00KA5,100,0,true,false )
             ,new CursorDef("P00KA6", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00KA6,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getString(1, 20) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((String[]) buf[4])[0] = rslt.getVarchar(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((DateTime[]) buf[6])[0] = rslt.getGXDate(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((String[]) buf[8])[0] = rslt.getVarchar(6) ;
                ((short[]) buf[9])[0] = rslt.getShort(7) ;
                ((int[]) buf[10])[0] = rslt.getInt(8) ;
                ((int[]) buf[11])[0] = rslt.getInt(9) ;
                ((int[]) buf[12])[0] = rslt.getInt(10) ;
                ((String[]) buf[13])[0] = rslt.getString(11, 100) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(11);
                ((DateTime[]) buf[15])[0] = rslt.getGXDate(12) ;
                return;
             case 1 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((String[]) buf[4])[0] = rslt.getVarchar(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((DateTime[]) buf[6])[0] = rslt.getGXDate(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((short[]) buf[8])[0] = rslt.getShort(6) ;
                ((String[]) buf[9])[0] = rslt.getString(7, 20) ;
                ((int[]) buf[10])[0] = rslt.getInt(8) ;
                ((int[]) buf[11])[0] = rslt.getInt(9) ;
                ((int[]) buf[12])[0] = rslt.getInt(10) ;
                ((String[]) buf[13])[0] = rslt.getString(11, 100) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(11);
                ((DateTime[]) buf[15])[0] = rslt.getGXDate(12) ;
                return;
             case 2 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((DateTime[]) buf[5])[0] = rslt.getGXDate(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((String[]) buf[7])[0] = rslt.getVarchar(5) ;
                ((short[]) buf[8])[0] = rslt.getShort(6) ;
                ((String[]) buf[9])[0] = rslt.getString(7, 20) ;
                ((int[]) buf[10])[0] = rslt.getInt(8) ;
                ((int[]) buf[11])[0] = rslt.getInt(9) ;
                ((int[]) buf[12])[0] = rslt.getInt(10) ;
                ((String[]) buf[13])[0] = rslt.getString(11, 100) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(11);
                ((DateTime[]) buf[15])[0] = rslt.getGXDate(12) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((DateTime[]) buf[5])[0] = rslt.getGXDate(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((String[]) buf[7])[0] = rslt.getVarchar(5) ;
                ((short[]) buf[8])[0] = rslt.getShort(6) ;
                ((String[]) buf[9])[0] = rslt.getString(7, 20) ;
                ((int[]) buf[10])[0] = rslt.getInt(8) ;
                ((int[]) buf[11])[0] = rslt.getInt(9) ;
                ((int[]) buf[12])[0] = rslt.getInt(10) ;
                ((String[]) buf[13])[0] = rslt.getString(11, 100) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(11);
                ((DateTime[]) buf[15])[0] = rslt.getGXDate(12) ;
                return;
             case 4 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((String[]) buf[3])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((DateTime[]) buf[5])[0] = rslt.getGXDate(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((String[]) buf[7])[0] = rslt.getVarchar(5) ;
                ((short[]) buf[8])[0] = rslt.getShort(6) ;
                ((String[]) buf[9])[0] = rslt.getString(7, 20) ;
                ((int[]) buf[10])[0] = rslt.getInt(8) ;
                ((int[]) buf[11])[0] = rslt.getInt(9) ;
                ((int[]) buf[12])[0] = rslt.getInt(10) ;
                ((String[]) buf[13])[0] = rslt.getString(11, 100) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(11);
                ((DateTime[]) buf[15])[0] = rslt.getGXDate(12) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[36]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[37]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[38]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[39]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[40]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[41]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[42]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[43]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[44]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[45]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[46]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[47]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[48]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[49]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[50]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[51]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[52]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[53]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[54]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[55]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[56]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[57]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[58]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[59]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[60]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[61]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[62]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[63]);
                }
                if ( (short)parms[28] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[64]);
                }
                if ( (short)parms[29] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[65]);
                }
                if ( (short)parms[30] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[66]);
                }
                if ( (short)parms[31] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[67]);
                }
                if ( (short)parms[32] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[68]);
                }
                if ( (short)parms[33] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[69]);
                }
                if ( (short)parms[34] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[70]);
                }
                if ( (short)parms[35] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[71]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[36]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[37]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[38]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[39]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[40]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[41]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[42]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[43]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[44]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[45]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[46]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[47]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[48]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[49]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[50]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[51]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[52]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[53]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[54]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[55]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[56]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[57]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[58]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[59]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[60]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[61]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[62]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[63]);
                }
                if ( (short)parms[28] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[64]);
                }
                if ( (short)parms[29] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[65]);
                }
                if ( (short)parms[30] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[66]);
                }
                if ( (short)parms[31] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[67]);
                }
                if ( (short)parms[32] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[68]);
                }
                if ( (short)parms[33] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[69]);
                }
                if ( (short)parms[34] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[70]);
                }
                if ( (short)parms[35] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[71]);
                }
                return;
             case 2 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[36]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[37]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[38]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[39]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[40]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[41]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[42]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[43]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[44]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[45]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[46]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[47]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[48]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[49]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[50]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[51]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[52]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[53]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[54]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[55]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[56]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[57]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[58]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[59]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[60]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[61]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[62]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[63]);
                }
                if ( (short)parms[28] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[64]);
                }
                if ( (short)parms[29] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[65]);
                }
                if ( (short)parms[30] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[66]);
                }
                if ( (short)parms[31] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[67]);
                }
                if ( (short)parms[32] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[68]);
                }
                if ( (short)parms[33] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[69]);
                }
                if ( (short)parms[34] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[70]);
                }
                if ( (short)parms[35] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[71]);
                }
                return;
             case 3 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[36]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[37]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[38]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[39]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[40]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[41]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[42]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[43]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[44]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[45]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[46]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[47]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[48]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[49]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[50]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[51]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[52]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[53]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[54]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[55]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[56]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[57]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[58]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[59]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[60]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[61]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[62]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[63]);
                }
                if ( (short)parms[28] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[64]);
                }
                if ( (short)parms[29] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[65]);
                }
                if ( (short)parms[30] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[66]);
                }
                if ( (short)parms[31] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[67]);
                }
                if ( (short)parms[32] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[68]);
                }
                if ( (short)parms[33] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[69]);
                }
                if ( (short)parms[34] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[70]);
                }
                if ( (short)parms[35] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[71]);
                }
                return;
             case 4 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[36]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[37]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[38]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[39]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[40]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[41]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[42]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[43]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[44]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[45]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[46]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[47]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[48]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[49]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[50]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[51]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[52]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[53]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[54]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[55]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[56]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[57]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[58]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[59]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[60]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[61]);
                }
                if ( (short)parms[26] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[62]);
                }
                if ( (short)parms[27] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[63]);
                }
                if ( (short)parms[28] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[64]);
                }
                if ( (short)parms[29] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[65]);
                }
                if ( (short)parms[30] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[66]);
                }
                if ( (short)parms[31] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[67]);
                }
                if ( (short)parms[32] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[68]);
                }
                if ( (short)parms[33] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[69]);
                }
                if ( (short)parms[34] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[70]);
                }
                if ( (short)parms[35] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[71]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getpromptcontratoocorrencianotificacaofilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getpromptcontratoocorrencianotificacaofilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getpromptcontratoocorrencianotificacaofilterdata") )
          {
             return  ;
          }
          getpromptcontratoocorrencianotificacaofilterdata worker = new getpromptcontratoocorrencianotificacaofilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
