/*
               File: type_SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho
        Description: SDT_WS_ConsultarAutorizacaoUsuario
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/29/2020 0:30:8.22
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "SDT_WS_ConsultarAutorizacaoUsuario.AreaTrabalho" )]
   [XmlType(TypeName =  "SDT_WS_ConsultarAutorizacaoUsuario.AreaTrabalho" , Namespace = "AutorizacaoUsuario" )]
   [System.Xml.Serialization.XmlInclude( typeof( SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho_Contrato ))]
   [Serializable]
   public class SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho : GxUserType
   {
      public SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho( )
      {
         /* Constructor for serialization */
         gxTv_SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho_Areatrabalho_descricao = "";
         gxTv_SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho_Perfil_nome = "";
         gxTv_SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho_Contratante_razaosocial = "";
         gxTv_SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho_Contratada_pessoanom = "";
      }

      public SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho( IGxContext context )
      {
         this.context = context;
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         xmls = new XmlSerializer(this.GetType(), sNameSpace);
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "AutorizacaoUsuario" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho obj ;
         obj = this;
         obj.gxTpr_Areatrabalho_codigo = deserialized.gxTpr_Areatrabalho_codigo;
         obj.gxTpr_Areatrabalho_descricao = deserialized.gxTpr_Areatrabalho_descricao;
         obj.gxTpr_Usuario_codigo = deserialized.gxTpr_Usuario_codigo;
         obj.gxTpr_Perfil_codigo = deserialized.gxTpr_Perfil_codigo;
         obj.gxTpr_Perfil_nome = deserialized.gxTpr_Perfil_nome;
         obj.gxTpr_Contratante_codigo = deserialized.gxTpr_Contratante_codigo;
         obj.gxTpr_Contratante_razaosocial = deserialized.gxTpr_Contratante_razaosocial;
         obj.gxTpr_Contratada_codigo = deserialized.gxTpr_Contratada_codigo;
         obj.gxTpr_Contratada_pessoanom = deserialized.gxTpr_Contratada_pessoanom;
         obj.gxTpr_Contratos = deserialized.gxTpr_Contratos;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "AreaTrabalho_Codigo") )
               {
                  gxTv_SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho_Areatrabalho_codigo = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "AreaTrabalho_Descricao") )
               {
                  gxTv_SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho_Areatrabalho_descricao = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Usuario_Codigo") )
               {
                  gxTv_SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho_Usuario_codigo = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Perfil_Codigo") )
               {
                  gxTv_SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho_Perfil_codigo = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Perfil_Nome") )
               {
                  gxTv_SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho_Perfil_nome = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_Codigo") )
               {
                  gxTv_SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho_Contratante_codigo = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratante_RazaoSocial") )
               {
                  gxTv_SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho_Contratante_razaosocial = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratada_Codigo") )
               {
                  gxTv_SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho_Contratada_codigo = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratada_PessoaNom") )
               {
                  gxTv_SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho_Contratada_pessoanom = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contratos") )
               {
                  if ( gxTv_SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho_Contratos == null )
                  {
                     gxTv_SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho_Contratos = new GxObjectCollection( context, "SDT_WS_ConsultarAutorizacaoUsuario.AreaTrabalho.Contrato", "", "SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho_Contrato", "GeneXus.Programs");
                  }
                  if ( oReader.IsSimple == 0 )
                  {
                     GXSoapError = gxTv_SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho_Contratos.readxmlcollection(oReader, "Contratos", "Contrato");
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "SDT_WS_ConsultarAutorizacaoUsuario.AreaTrabalho";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("AreaTrabalho_Codigo", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho_Areatrabalho_codigo), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "AutorizacaoUsuario") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "AutorizacaoUsuario");
         }
         oWriter.WriteElement("AreaTrabalho_Descricao", StringUtil.RTrim( gxTv_SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho_Areatrabalho_descricao));
         if ( StringUtil.StrCmp(sNameSpace, "AutorizacaoUsuario") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "AutorizacaoUsuario");
         }
         oWriter.WriteElement("Usuario_Codigo", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho_Usuario_codigo), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "AutorizacaoUsuario") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "AutorizacaoUsuario");
         }
         oWriter.WriteElement("Perfil_Codigo", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho_Perfil_codigo), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "AutorizacaoUsuario") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "AutorizacaoUsuario");
         }
         oWriter.WriteElement("Perfil_Nome", StringUtil.RTrim( gxTv_SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho_Perfil_nome));
         if ( StringUtil.StrCmp(sNameSpace, "AutorizacaoUsuario") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "AutorizacaoUsuario");
         }
         oWriter.WriteElement("Contratante_Codigo", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho_Contratante_codigo), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "AutorizacaoUsuario") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "AutorizacaoUsuario");
         }
         oWriter.WriteElement("Contratante_RazaoSocial", StringUtil.RTrim( gxTv_SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho_Contratante_razaosocial));
         if ( StringUtil.StrCmp(sNameSpace, "AutorizacaoUsuario") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "AutorizacaoUsuario");
         }
         oWriter.WriteElement("Contratada_Codigo", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho_Contratada_codigo), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "AutorizacaoUsuario") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "AutorizacaoUsuario");
         }
         oWriter.WriteElement("Contratada_PessoaNom", StringUtil.RTrim( gxTv_SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho_Contratada_pessoanom));
         if ( StringUtil.StrCmp(sNameSpace, "AutorizacaoUsuario") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "AutorizacaoUsuario");
         }
         if ( gxTv_SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho_Contratos != null )
         {
            String sNameSpace1 ;
            if ( StringUtil.StrCmp(sNameSpace, "AutorizacaoUsuario") == 0 )
            {
               sNameSpace1 = "[*:nosend]" + "AutorizacaoUsuario";
            }
            else
            {
               sNameSpace1 = "AutorizacaoUsuario";
            }
            gxTv_SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho_Contratos.writexmlcollection(oWriter, "Contratos", sNameSpace1, "Contrato", sNameSpace1);
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("AreaTrabalho_Codigo", gxTv_SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho_Areatrabalho_codigo, false);
         AddObjectProperty("AreaTrabalho_Descricao", gxTv_SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho_Areatrabalho_descricao, false);
         AddObjectProperty("Usuario_Codigo", gxTv_SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho_Usuario_codigo, false);
         AddObjectProperty("Perfil_Codigo", gxTv_SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho_Perfil_codigo, false);
         AddObjectProperty("Perfil_Nome", gxTv_SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho_Perfil_nome, false);
         AddObjectProperty("Contratante_Codigo", gxTv_SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho_Contratante_codigo, false);
         AddObjectProperty("Contratante_RazaoSocial", gxTv_SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho_Contratante_razaosocial, false);
         AddObjectProperty("Contratada_Codigo", gxTv_SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho_Contratada_codigo, false);
         AddObjectProperty("Contratada_PessoaNom", gxTv_SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho_Contratada_pessoanom, false);
         if ( gxTv_SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho_Contratos != null )
         {
            AddObjectProperty("Contratos", gxTv_SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho_Contratos, false);
         }
         return  ;
      }

      [  SoapElement( ElementName = "AreaTrabalho_Codigo" )]
      [  XmlElement( ElementName = "AreaTrabalho_Codigo"   )]
      public int gxTpr_Areatrabalho_codigo
      {
         get {
            return gxTv_SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho_Areatrabalho_codigo ;
         }

         set {
            gxTv_SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho_Areatrabalho_codigo = (int)(value);
         }

      }

      [  SoapElement( ElementName = "AreaTrabalho_Descricao" )]
      [  XmlElement( ElementName = "AreaTrabalho_Descricao"   )]
      public String gxTpr_Areatrabalho_descricao
      {
         get {
            return gxTv_SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho_Areatrabalho_descricao ;
         }

         set {
            gxTv_SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho_Areatrabalho_descricao = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Usuario_Codigo" )]
      [  XmlElement( ElementName = "Usuario_Codigo"   )]
      public int gxTpr_Usuario_codigo
      {
         get {
            return gxTv_SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho_Usuario_codigo ;
         }

         set {
            gxTv_SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho_Usuario_codigo = (int)(value);
         }

      }

      [  SoapElement( ElementName = "Perfil_Codigo" )]
      [  XmlElement( ElementName = "Perfil_Codigo"   )]
      public int gxTpr_Perfil_codigo
      {
         get {
            return gxTv_SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho_Perfil_codigo ;
         }

         set {
            gxTv_SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho_Perfil_codigo = (int)(value);
         }

      }

      [  SoapElement( ElementName = "Perfil_Nome" )]
      [  XmlElement( ElementName = "Perfil_Nome"   )]
      public String gxTpr_Perfil_nome
      {
         get {
            return gxTv_SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho_Perfil_nome ;
         }

         set {
            gxTv_SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho_Perfil_nome = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Contratante_Codigo" )]
      [  XmlElement( ElementName = "Contratante_Codigo"   )]
      public int gxTpr_Contratante_codigo
      {
         get {
            return gxTv_SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho_Contratante_codigo ;
         }

         set {
            gxTv_SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho_Contratante_codigo = (int)(value);
         }

      }

      [  SoapElement( ElementName = "Contratante_RazaoSocial" )]
      [  XmlElement( ElementName = "Contratante_RazaoSocial"   )]
      public String gxTpr_Contratante_razaosocial
      {
         get {
            return gxTv_SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho_Contratante_razaosocial ;
         }

         set {
            gxTv_SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho_Contratante_razaosocial = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Contratada_Codigo" )]
      [  XmlElement( ElementName = "Contratada_Codigo"   )]
      public int gxTpr_Contratada_codigo
      {
         get {
            return gxTv_SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho_Contratada_codigo ;
         }

         set {
            gxTv_SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho_Contratada_codigo = (int)(value);
         }

      }

      [  SoapElement( ElementName = "Contratada_PessoaNom" )]
      [  XmlElement( ElementName = "Contratada_PessoaNom"   )]
      public String gxTpr_Contratada_pessoanom
      {
         get {
            return gxTv_SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho_Contratada_pessoanom ;
         }

         set {
            gxTv_SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho_Contratada_pessoanom = (String)(value);
         }

      }

      public class gxTv_SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho_Contratos_SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho_Contrato_80compatibility:SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho_Contrato {}
      [  SoapElement( ElementName = "Contratos" )]
      [  XmlArray( ElementName = "Contratos"  )]
      [  XmlArrayItemAttribute( Type= typeof( SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho_Contrato ), ElementName= "Contrato"  , IsNullable=false)]
      [  XmlArrayItemAttribute( Type= typeof( gxTv_SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho_Contratos_SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho_Contrato_80compatibility ), ElementName= "SDT_WS_ConsultarAutorizacaoUsuario.AreaTrabalho.Contrato"  , IsNullable=false)]
      public GxObjectCollection gxTpr_Contratos_GxObjectCollection
      {
         get {
            if ( gxTv_SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho_Contratos == null )
            {
               gxTv_SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho_Contratos = new GxObjectCollection( context, "SDT_WS_ConsultarAutorizacaoUsuario.AreaTrabalho.Contrato", "", "SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho_Contrato", "GeneXus.Programs");
            }
            return (GxObjectCollection)gxTv_SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho_Contratos ;
         }

         set {
            if ( gxTv_SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho_Contratos == null )
            {
               gxTv_SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho_Contratos = new GxObjectCollection( context, "SDT_WS_ConsultarAutorizacaoUsuario.AreaTrabalho.Contrato", "", "SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho_Contrato", "GeneXus.Programs");
            }
            gxTv_SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho_Contratos = (GxObjectCollection) value;
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public IGxCollection gxTpr_Contratos
      {
         get {
            if ( gxTv_SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho_Contratos == null )
            {
               gxTv_SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho_Contratos = new GxObjectCollection( context, "SDT_WS_ConsultarAutorizacaoUsuario.AreaTrabalho.Contrato", "", "SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho_Contrato", "GeneXus.Programs");
            }
            return gxTv_SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho_Contratos ;
         }

         set {
            gxTv_SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho_Contratos = value;
         }

      }

      public void gxTv_SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho_Contratos_SetNull( )
      {
         gxTv_SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho_Contratos = null;
         return  ;
      }

      public bool gxTv_SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho_Contratos_IsNull( )
      {
         if ( gxTv_SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho_Contratos == null )
         {
            return true ;
         }
         return false ;
      }

      public void initialize( )
      {
         gxTv_SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho_Areatrabalho_descricao = "";
         gxTv_SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho_Perfil_nome = "";
         gxTv_SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho_Contratante_razaosocial = "";
         gxTv_SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho_Contratada_pessoanom = "";
         sTagName = "";
         return  ;
      }

      protected short readOk ;
      protected short nOutParmCount ;
      protected int gxTv_SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho_Areatrabalho_codigo ;
      protected int gxTv_SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho_Usuario_codigo ;
      protected int gxTv_SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho_Perfil_codigo ;
      protected int gxTv_SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho_Contratante_codigo ;
      protected int gxTv_SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho_Contratada_codigo ;
      protected String gxTv_SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho_Perfil_nome ;
      protected String gxTv_SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho_Contratante_razaosocial ;
      protected String gxTv_SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho_Contratada_pessoanom ;
      protected String sTagName ;
      protected String gxTv_SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho_Areatrabalho_descricao ;
      [ObjectCollection(ItemType=typeof( SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho_Contrato ))]
      protected IGxCollection gxTv_SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho_Contratos=null ;
   }

   [DataContract(Name = @"SDT_WS_ConsultarAutorizacaoUsuario.AreaTrabalho", Namespace = "AutorizacaoUsuario")]
   public class SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho_RESTInterface : GxGenericCollectionItem<SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho_RESTInterface( ) : base()
      {
      }

      public SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho_RESTInterface( SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "AreaTrabalho_Codigo" , Order = 0 )]
      public Nullable<int> gxTpr_Areatrabalho_codigo
      {
         get {
            return sdt.gxTpr_Areatrabalho_codigo ;
         }

         set {
            sdt.gxTpr_Areatrabalho_codigo = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "AreaTrabalho_Descricao" , Order = 1 )]
      public String gxTpr_Areatrabalho_descricao
      {
         get {
            return sdt.gxTpr_Areatrabalho_descricao ;
         }

         set {
            sdt.gxTpr_Areatrabalho_descricao = (String)(value);
         }

      }

      [DataMember( Name = "Usuario_Codigo" , Order = 2 )]
      public Nullable<int> gxTpr_Usuario_codigo
      {
         get {
            return sdt.gxTpr_Usuario_codigo ;
         }

         set {
            sdt.gxTpr_Usuario_codigo = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Perfil_Codigo" , Order = 3 )]
      public Nullable<int> gxTpr_Perfil_codigo
      {
         get {
            return sdt.gxTpr_Perfil_codigo ;
         }

         set {
            sdt.gxTpr_Perfil_codigo = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Perfil_Nome" , Order = 4 )]
      public String gxTpr_Perfil_nome
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Perfil_nome) ;
         }

         set {
            sdt.gxTpr_Perfil_nome = (String)(value);
         }

      }

      [DataMember( Name = "Contratante_Codigo" , Order = 5 )]
      public Nullable<int> gxTpr_Contratante_codigo
      {
         get {
            return sdt.gxTpr_Contratante_codigo ;
         }

         set {
            sdt.gxTpr_Contratante_codigo = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Contratante_RazaoSocial" , Order = 6 )]
      public String gxTpr_Contratante_razaosocial
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Contratante_razaosocial) ;
         }

         set {
            sdt.gxTpr_Contratante_razaosocial = (String)(value);
         }

      }

      [DataMember( Name = "Contratada_Codigo" , Order = 7 )]
      public Nullable<int> gxTpr_Contratada_codigo
      {
         get {
            return sdt.gxTpr_Contratada_codigo ;
         }

         set {
            sdt.gxTpr_Contratada_codigo = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Contratada_PessoaNom" , Order = 8 )]
      public String gxTpr_Contratada_pessoanom
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Contratada_pessoanom) ;
         }

         set {
            sdt.gxTpr_Contratada_pessoanom = (String)(value);
         }

      }

      [DataMember( Name = "Contratos" , Order = 9 )]
      public GxGenericCollection<SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho_Contrato_RESTInterface> gxTpr_Contratos
      {
         get {
            return new GxGenericCollection<SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho_Contrato_RESTInterface>(sdt.gxTpr_Contratos) ;
         }

         set {
            value.LoadCollection(sdt.gxTpr_Contratos);
         }

      }

      public SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho sdt
      {
         get {
            return (SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtSDT_WS_ConsultarAutorizacaoUsuario_AreaTrabalho() ;
         }
      }

   }

}
