/*
               File: LinhaNegocio
        Description: Linhas de Neg�cio
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/29/2020 9:32:41.50
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class linhanegocio : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"LINHANEGOCIO_GPOOBJCTRLCOD") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            GXDLALINHANEGOCIO_GPOOBJCTRLCOD53225( ) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_10") == 0 )
         {
            A2035LinhaNegocio_GpoObjCtrlCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2035LinhaNegocio_GpoObjCtrlCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A2035LinhaNegocio_GpoObjCtrlCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_10( A2035LinhaNegocio_GpoObjCtrlCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
         {
            Gx_mode = gxfirstwebparm;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
            {
               AV7LinhadeNegocio_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7LinhadeNegocio_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7LinhadeNegocio_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vLINHADENEGOCIO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7LinhadeNegocio_Codigo), "ZZZZZ9")));
            }
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         dynLinhaNegocio_GpoObjCtrlCod.Name = "LINHANEGOCIO_GPOOBJCTRLCOD";
         dynLinhaNegocio_GpoObjCtrlCod.WebTags = "";
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Linhas de Neg�cio", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = edtLinhaNegocio_Identificador_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public linhanegocio( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public linhanegocio( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_Gx_mode ,
                           int aP1_LinhadeNegocio_Codigo )
      {
         this.Gx_mode = aP0_Gx_mode;
         this.AV7LinhadeNegocio_Codigo = aP1_LinhadeNegocio_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         dynLinhaNegocio_GpoObjCtrlCod = new GXCombobox();
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
         if ( dynLinhaNegocio_GpoObjCtrlCod.ItemCount > 0 )
         {
            A2035LinhaNegocio_GpoObjCtrlCod = (int)(NumberUtil.Val( dynLinhaNegocio_GpoObjCtrlCod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A2035LinhaNegocio_GpoObjCtrlCod), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2035LinhaNegocio_GpoObjCtrlCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A2035LinhaNegocio_GpoObjCtrlCod), 6, 0)));
         }
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            wb_table1_2_53225( true) ;
         }
         return  ;
      }

      protected void wb_table1_2_53225e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtLinhadeNegocio_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A2036LinhadeNegocio_Codigo), 6, 0, ",", "")), ((edtLinhadeNegocio_Codigo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A2036LinhadeNegocio_Codigo), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A2036LinhadeNegocio_Codigo), "ZZZZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtLinhadeNegocio_Codigo_Jsonclick, 0, "Attribute", "", "", "", edtLinhadeNegocio_Codigo_Visible, edtLinhadeNegocio_Codigo_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_LinhaNegocio.htm");
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void wb_table1_2_53225( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_5_53225( true) ;
         }
         return  ;
      }

      protected void wb_table2_5_53225e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_31_53225( true) ;
         }
         return  ;
      }

      protected void wb_table3_31_53225e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_53225e( true) ;
         }
         else
         {
            wb_table1_2_53225e( false) ;
         }
      }

      protected void wb_table3_31_53225( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 34,'',false,'',0)\"";
            ClassString = "BtnEnter";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_enter_Internalname, "", "Confirmar", bttBtn_trn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_trn_enter_Visible, bttBtn_trn_enter_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_LinhaNegocio.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 36,'',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_cancel_Internalname, "", "Fechar", bttBtn_trn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, bttBtn_trn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_LinhaNegocio.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 38,'',false,'',0)\"";
            ClassString = "btn btn-danger";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_delete_Internalname, "", "Eliminar", bttBtn_trn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_trn_delete_Visible, bttBtn_trn_delete_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_LinhaNegocio.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_31_53225e( true) ;
         }
         else
         {
            wb_table3_31_53225e( false) ;
         }
      }

      protected void wb_table2_5_53225( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"Body"+"\" style=\"display:none;\">") ;
            wb_table4_13_53225( true) ;
         }
         return  ;
      }

      protected void wb_table4_13_53225e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_53225e( true) ;
         }
         else
         {
            wb_table2_5_53225e( false) ;
         }
      }

      protected void wb_table4_13_53225( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "TableData", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocklinhanegocio_identificador_Internalname, "Identificador", "", "", lblTextblocklinhanegocio_identificador_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_LinhaNegocio.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtLinhaNegocio_Identificador_Internalname, A2037LinhaNegocio_Identificador, StringUtil.RTrim( context.localUtil.Format( A2037LinhaNegocio_Identificador, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,18);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtLinhaNegocio_Identificador_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtLinhaNegocio_Identificador_Enabled, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_LinhaNegocio.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocklinhanegocio_descricao_Internalname, "Descri��o", "", "", lblTextblocklinhanegocio_descricao_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_LinhaNegocio.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'',false,'',0)\"";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtLinhaNegocio_Descricao_Internalname, A2038LinhaNegocio_Descricao, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,23);\"", 0, 1, edtLinhaNegocio_Descricao_Enabled, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "250", -1, "", "", -1, true, "", "HLP_LinhaNegocio.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblocklinhanegocio_gpoobjctrlcod_Internalname, "Objeto de Controle", "", "", lblTextblocklinhanegocio_gpoobjctrlcod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_LinhaNegocio.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 28,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynLinhaNegocio_GpoObjCtrlCod, dynLinhaNegocio_GpoObjCtrlCod_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A2035LinhaNegocio_GpoObjCtrlCod), 6, 0)), 1, dynLinhaNegocio_GpoObjCtrlCod_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, dynLinhaNegocio_GpoObjCtrlCod.Enabled, 1, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,28);\"", "", true, "HLP_LinhaNegocio.htm");
            dynLinhaNegocio_GpoObjCtrlCod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A2035LinhaNegocio_GpoObjCtrlCod), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynLinhaNegocio_GpoObjCtrlCod_Internalname, "Values", (String)(dynLinhaNegocio_GpoObjCtrlCod.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_13_53225e( true) ;
         }
         else
         {
            wb_table4_13_53225e( false) ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11532 */
         E11532 ();
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               A2037LinhaNegocio_Identificador = cgiGet( edtLinhaNegocio_Identificador_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2037LinhaNegocio_Identificador", A2037LinhaNegocio_Identificador);
               A2038LinhaNegocio_Descricao = cgiGet( edtLinhaNegocio_Descricao_Internalname);
               n2038LinhaNegocio_Descricao = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2038LinhaNegocio_Descricao", A2038LinhaNegocio_Descricao);
               n2038LinhaNegocio_Descricao = (String.IsNullOrEmpty(StringUtil.RTrim( A2038LinhaNegocio_Descricao)) ? true : false);
               dynLinhaNegocio_GpoObjCtrlCod.CurrentValue = cgiGet( dynLinhaNegocio_GpoObjCtrlCod_Internalname);
               A2035LinhaNegocio_GpoObjCtrlCod = (int)(NumberUtil.Val( cgiGet( dynLinhaNegocio_GpoObjCtrlCod_Internalname), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2035LinhaNegocio_GpoObjCtrlCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A2035LinhaNegocio_GpoObjCtrlCod), 6, 0)));
               A2036LinhadeNegocio_Codigo = (int)(context.localUtil.CToN( cgiGet( edtLinhadeNegocio_Codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2036LinhadeNegocio_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2036LinhadeNegocio_Codigo), 6, 0)));
               /* Read saved values. */
               Z2036LinhadeNegocio_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z2036LinhadeNegocio_Codigo"), ",", "."));
               Z2037LinhaNegocio_Identificador = cgiGet( "Z2037LinhaNegocio_Identificador");
               Z2038LinhaNegocio_Descricao = cgiGet( "Z2038LinhaNegocio_Descricao");
               n2038LinhaNegocio_Descricao = (String.IsNullOrEmpty(StringUtil.RTrim( A2038LinhaNegocio_Descricao)) ? true : false);
               Z2035LinhaNegocio_GpoObjCtrlCod = (int)(context.localUtil.CToN( cgiGet( "Z2035LinhaNegocio_GpoObjCtrlCod"), ",", "."));
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
               Gx_mode = cgiGet( "Mode");
               N2035LinhaNegocio_GpoObjCtrlCod = (int)(context.localUtil.CToN( cgiGet( "N2035LinhaNegocio_GpoObjCtrlCod"), ",", "."));
               AV7LinhadeNegocio_Codigo = (int)(context.localUtil.CToN( cgiGet( "vLINHADENEGOCIO_CODIGO"), ",", "."));
               AV11Insert_LinhaNegocio_GpoObjCtrlCod = (int)(context.localUtil.CToN( cgiGet( "vINSERT_LINHANEGOCIO_GPOOBJCTRLCOD"), ",", "."));
               AV13Pgmname = cgiGet( "vPGMNAME");
               Gx_mode = cgiGet( "vMODE");
               Dvpanel_tableattributes_Width = cgiGet( "DVPANEL_TABLEATTRIBUTES_Width");
               Dvpanel_tableattributes_Height = cgiGet( "DVPANEL_TABLEATTRIBUTES_Height");
               Dvpanel_tableattributes_Cls = cgiGet( "DVPANEL_TABLEATTRIBUTES_Cls");
               Dvpanel_tableattributes_Title = cgiGet( "DVPANEL_TABLEATTRIBUTES_Title");
               Dvpanel_tableattributes_Collapsible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsible"));
               Dvpanel_tableattributes_Collapsed = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsed"));
               Dvpanel_tableattributes_Enabled = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Enabled"));
               Dvpanel_tableattributes_Class = cgiGet( "DVPANEL_TABLEATTRIBUTES_Class");
               Dvpanel_tableattributes_Autowidth = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autowidth"));
               Dvpanel_tableattributes_Autoheight = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoheight"));
               Dvpanel_tableattributes_Showheader = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showheader"));
               Dvpanel_tableattributes_Showcollapseicon = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showcollapseicon"));
               Dvpanel_tableattributes_Iconposition = cgiGet( "DVPANEL_TABLEATTRIBUTES_Iconposition");
               Dvpanel_tableattributes_Autoscroll = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoscroll"));
               Dvpanel_tableattributes_Visible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Visible"));
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               forbiddenHiddens = "hsh" + "LinhaNegocio";
               A2036LinhadeNegocio_Codigo = (int)(context.localUtil.CToN( cgiGet( edtLinhadeNegocio_Codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2036LinhadeNegocio_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2036LinhadeNegocio_Codigo), 6, 0)));
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A2036LinhadeNegocio_Codigo), "ZZZZZ9");
               forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
               hsh = cgiGet( "hsh");
               if ( ( ! ( ( A2036LinhadeNegocio_Codigo != Z2036LinhadeNegocio_Codigo ) ) || ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ) && ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
               {
                  GXUtil.WriteLog("linhanegocio:[SecurityCheckFailed value for]"+"LinhadeNegocio_Codigo:"+context.localUtil.Format( (decimal)(A2036LinhadeNegocio_Codigo), "ZZZZZ9"));
                  GXUtil.WriteLog("linhanegocio:[SecurityCheckFailed value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
                  GxWebError = 1;
                  context.HttpContext.Response.StatusDescription = 403.ToString();
                  context.HttpContext.Response.StatusCode = 403;
                  context.WriteHtmlText( "<title>403 Forbidden</title>") ;
                  context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
                  context.WriteHtmlText( "<p /><hr />") ;
                  GXUtil.WriteLog("send_http_error_code " + 403.ToString());
                  AnyError = 1;
                  return  ;
               }
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  A2036LinhadeNegocio_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2036LinhadeNegocio_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2036LinhadeNegocio_Codigo), 6, 0)));
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  disable_std_buttons( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
                  {
                     sMode225 = Gx_mode;
                     Gx_mode = "UPD";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                     Gx_mode = sMode225;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  }
                  standaloneModal( ) ;
                  if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
                  {
                     getByPrimaryKey( ) ;
                     if ( RcdFound225 == 1 )
                     {
                        if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
                        {
                           /* Confirm record */
                           CONFIRM_530( ) ;
                           if ( AnyError == 0 )
                           {
                              GX_FocusControl = bttBtn_trn_enter_Internalname;
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noinsert", ""), 1, "LINHADENEGOCIO_CODIGO");
                        AnyError = 1;
                        GX_FocusControl = edtLinhadeNegocio_Codigo_Internalname;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E11532 */
                           E11532 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "AFTER TRN") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E12532 */
                           E12532 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           if ( StringUtil.StrCmp(Gx_mode, "DSP") != 0 )
                           {
                              btn_enter( ) ;
                           }
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            /* Execute user event: E12532 */
            E12532 ();
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll53225( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         bttBtn_trn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
         if ( ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            bttBtn_trn_delete_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
            if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
            {
               bttBtn_trn_enter_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Visible), 5, 0)));
            }
            DisableAttributes53225( ) ;
         }
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void CONFIRM_530( )
      {
         BeforeValidate53225( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls53225( ) ;
            }
            else
            {
               CheckExtendedTable53225( ) ;
               CloseExtendedTableCursors53225( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         }
      }

      protected void ResetCaption530( )
      {
      }

      protected void E11532( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV8WWPContext) ;
         AV9TrnContext.FromXml(AV10WebSession.Get("TrnContext"), "");
         if ( ( StringUtil.StrCmp(AV9TrnContext.gxTpr_Transactionname, AV13Pgmname) == 0 ) && ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) )
         {
            AV14GXV1 = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14GXV1), 8, 0)));
            while ( AV14GXV1 <= AV9TrnContext.gxTpr_Attributes.Count )
            {
               AV12TrnContextAtt = ((wwpbaseobjects.SdtWWPTransactionContext_Attribute)AV9TrnContext.gxTpr_Attributes.Item(AV14GXV1));
               if ( StringUtil.StrCmp(AV12TrnContextAtt.gxTpr_Attributename, "LinhaNegocio_GpoObjCtrlCod") == 0 )
               {
                  AV11Insert_LinhaNegocio_GpoObjCtrlCod = (int)(NumberUtil.Val( AV12TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11Insert_LinhaNegocio_GpoObjCtrlCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11Insert_LinhaNegocio_GpoObjCtrlCod), 6, 0)));
               }
               AV14GXV1 = (int)(AV14GXV1+1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14GXV1), 8, 0)));
            }
         }
         edtLinhadeNegocio_Codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLinhadeNegocio_Codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtLinhadeNegocio_Codigo_Visible), 5, 0)));
      }

      protected void E12532( )
      {
         /* After Trn Routine */
         if ( ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) && ! AV9TrnContext.gxTpr_Callerondelete )
         {
            context.wjLoc = formatLink("wwlinhanegocio.aspx") ;
            context.wjLocDisableFrm = 1;
         }
         context.setWebReturnParms(new Object[] {});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void ZM53225( short GX_JID )
      {
         if ( ( GX_JID == 9 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z2037LinhaNegocio_Identificador = T00533_A2037LinhaNegocio_Identificador[0];
               Z2038LinhaNegocio_Descricao = T00533_A2038LinhaNegocio_Descricao[0];
               Z2035LinhaNegocio_GpoObjCtrlCod = T00533_A2035LinhaNegocio_GpoObjCtrlCod[0];
            }
            else
            {
               Z2037LinhaNegocio_Identificador = A2037LinhaNegocio_Identificador;
               Z2038LinhaNegocio_Descricao = A2038LinhaNegocio_Descricao;
               Z2035LinhaNegocio_GpoObjCtrlCod = A2035LinhaNegocio_GpoObjCtrlCod;
            }
         }
         if ( GX_JID == -9 )
         {
            Z2036LinhadeNegocio_Codigo = A2036LinhadeNegocio_Codigo;
            Z2037LinhaNegocio_Identificador = A2037LinhaNegocio_Identificador;
            Z2038LinhaNegocio_Descricao = A2038LinhaNegocio_Descricao;
            Z2035LinhaNegocio_GpoObjCtrlCod = A2035LinhaNegocio_GpoObjCtrlCod;
         }
      }

      protected void standaloneNotModal( )
      {
         GXALINHANEGOCIO_GPOOBJCTRLCOD_html53225( ) ;
         edtLinhadeNegocio_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLinhadeNegocio_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtLinhadeNegocio_Codigo_Enabled), 5, 0)));
         AV13Pgmname = "LinhaNegocio";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13Pgmname", AV13Pgmname);
         edtLinhadeNegocio_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLinhadeNegocio_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtLinhadeNegocio_Codigo_Enabled), 5, 0)));
         bttBtn_trn_delete_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Enabled), 5, 0)));
         if ( ! (0==AV7LinhadeNegocio_Codigo) )
         {
            A2036LinhadeNegocio_Codigo = AV7LinhadeNegocio_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2036LinhadeNegocio_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2036LinhadeNegocio_Codigo), 6, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV11Insert_LinhaNegocio_GpoObjCtrlCod) )
         {
            dynLinhaNegocio_GpoObjCtrlCod.Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynLinhaNegocio_GpoObjCtrlCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynLinhaNegocio_GpoObjCtrlCod.Enabled), 5, 0)));
         }
         else
         {
            dynLinhaNegocio_GpoObjCtrlCod.Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynLinhaNegocio_GpoObjCtrlCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynLinhaNegocio_GpoObjCtrlCod.Enabled), 5, 0)));
         }
      }

      protected void standaloneModal( )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ! (0==AV11Insert_LinhaNegocio_GpoObjCtrlCod) )
         {
            A2035LinhaNegocio_GpoObjCtrlCod = AV11Insert_LinhaNegocio_GpoObjCtrlCod;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2035LinhaNegocio_GpoObjCtrlCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A2035LinhaNegocio_GpoObjCtrlCod), 6, 0)));
         }
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            bttBtn_trn_enter_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         else
         {
            bttBtn_trn_enter_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
      }

      protected void Load53225( )
      {
         /* Using cursor T00535 */
         pr_default.execute(3, new Object[] {A2036LinhadeNegocio_Codigo});
         if ( (pr_default.getStatus(3) != 101) )
         {
            RcdFound225 = 1;
            A2037LinhaNegocio_Identificador = T00535_A2037LinhaNegocio_Identificador[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2037LinhaNegocio_Identificador", A2037LinhaNegocio_Identificador);
            A2038LinhaNegocio_Descricao = T00535_A2038LinhaNegocio_Descricao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2038LinhaNegocio_Descricao", A2038LinhaNegocio_Descricao);
            n2038LinhaNegocio_Descricao = T00535_n2038LinhaNegocio_Descricao[0];
            A2035LinhaNegocio_GpoObjCtrlCod = T00535_A2035LinhaNegocio_GpoObjCtrlCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2035LinhaNegocio_GpoObjCtrlCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A2035LinhaNegocio_GpoObjCtrlCod), 6, 0)));
            ZM53225( -9) ;
         }
         pr_default.close(3);
         OnLoadActions53225( ) ;
      }

      protected void OnLoadActions53225( )
      {
      }

      protected void CheckExtendedTable53225( )
      {
         Gx_BScreen = 1;
         standaloneModal( ) ;
         /* Using cursor T00534 */
         pr_default.execute(2, new Object[] {A2035LinhaNegocio_GpoObjCtrlCod});
         if ( (pr_default.getStatus(2) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Linha Negocio_Gpo Obj Ctrl'.", "ForeignKeyNotFound", 1, "LINHANEGOCIO_GPOOBJCTRLCOD");
            AnyError = 1;
            GX_FocusControl = dynLinhaNegocio_GpoObjCtrlCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         pr_default.close(2);
      }

      protected void CloseExtendedTableCursors53225( )
      {
         pr_default.close(2);
      }

      protected void enableDisable( )
      {
      }

      protected void gxLoad_10( int A2035LinhaNegocio_GpoObjCtrlCod )
      {
         /* Using cursor T00536 */
         pr_default.execute(4, new Object[] {A2035LinhaNegocio_GpoObjCtrlCod});
         if ( (pr_default.getStatus(4) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Linha Negocio_Gpo Obj Ctrl'.", "ForeignKeyNotFound", 1, "LINHANEGOCIO_GPOOBJCTRLCOD");
            AnyError = 1;
            GX_FocusControl = dynLinhaNegocio_GpoObjCtrlCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(4) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(4);
      }

      protected void GetKey53225( )
      {
         /* Using cursor T00537 */
         pr_default.execute(5, new Object[] {A2036LinhadeNegocio_Codigo});
         if ( (pr_default.getStatus(5) != 101) )
         {
            RcdFound225 = 1;
         }
         else
         {
            RcdFound225 = 0;
         }
         pr_default.close(5);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T00533 */
         pr_default.execute(1, new Object[] {A2036LinhadeNegocio_Codigo});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM53225( 9) ;
            RcdFound225 = 1;
            A2036LinhadeNegocio_Codigo = T00533_A2036LinhadeNegocio_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2036LinhadeNegocio_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2036LinhadeNegocio_Codigo), 6, 0)));
            A2037LinhaNegocio_Identificador = T00533_A2037LinhaNegocio_Identificador[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2037LinhaNegocio_Identificador", A2037LinhaNegocio_Identificador);
            A2038LinhaNegocio_Descricao = T00533_A2038LinhaNegocio_Descricao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2038LinhaNegocio_Descricao", A2038LinhaNegocio_Descricao);
            n2038LinhaNegocio_Descricao = T00533_n2038LinhaNegocio_Descricao[0];
            A2035LinhaNegocio_GpoObjCtrlCod = T00533_A2035LinhaNegocio_GpoObjCtrlCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2035LinhaNegocio_GpoObjCtrlCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A2035LinhaNegocio_GpoObjCtrlCod), 6, 0)));
            Z2036LinhadeNegocio_Codigo = A2036LinhadeNegocio_Codigo;
            sMode225 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            Load53225( ) ;
            if ( AnyError == 1 )
            {
               RcdFound225 = 0;
               InitializeNonKey53225( ) ;
            }
            Gx_mode = sMode225;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         else
         {
            RcdFound225 = 0;
            InitializeNonKey53225( ) ;
            sMode225 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            standaloneModal( ) ;
            Gx_mode = sMode225;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey53225( ) ;
         if ( RcdFound225 == 0 )
         {
         }
         else
         {
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound225 = 0;
         /* Using cursor T00538 */
         pr_default.execute(6, new Object[] {A2036LinhadeNegocio_Codigo});
         if ( (pr_default.getStatus(6) != 101) )
         {
            while ( (pr_default.getStatus(6) != 101) && ( ( T00538_A2036LinhadeNegocio_Codigo[0] < A2036LinhadeNegocio_Codigo ) ) )
            {
               pr_default.readNext(6);
            }
            if ( (pr_default.getStatus(6) != 101) && ( ( T00538_A2036LinhadeNegocio_Codigo[0] > A2036LinhadeNegocio_Codigo ) ) )
            {
               A2036LinhadeNegocio_Codigo = T00538_A2036LinhadeNegocio_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2036LinhadeNegocio_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2036LinhadeNegocio_Codigo), 6, 0)));
               RcdFound225 = 1;
            }
         }
         pr_default.close(6);
      }

      protected void move_previous( )
      {
         RcdFound225 = 0;
         /* Using cursor T00539 */
         pr_default.execute(7, new Object[] {A2036LinhadeNegocio_Codigo});
         if ( (pr_default.getStatus(7) != 101) )
         {
            while ( (pr_default.getStatus(7) != 101) && ( ( T00539_A2036LinhadeNegocio_Codigo[0] > A2036LinhadeNegocio_Codigo ) ) )
            {
               pr_default.readNext(7);
            }
            if ( (pr_default.getStatus(7) != 101) && ( ( T00539_A2036LinhadeNegocio_Codigo[0] < A2036LinhadeNegocio_Codigo ) ) )
            {
               A2036LinhadeNegocio_Codigo = T00539_A2036LinhadeNegocio_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2036LinhadeNegocio_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2036LinhadeNegocio_Codigo), 6, 0)));
               RcdFound225 = 1;
            }
         }
         pr_default.close(7);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey53225( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = edtLinhaNegocio_Identificador_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert53225( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound225 == 1 )
            {
               if ( A2036LinhadeNegocio_Codigo != Z2036LinhadeNegocio_Codigo )
               {
                  A2036LinhadeNegocio_Codigo = Z2036LinhadeNegocio_Codigo;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2036LinhadeNegocio_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2036LinhadeNegocio_Codigo), 6, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "LINHADENEGOCIO_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtLinhadeNegocio_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = edtLinhaNegocio_Identificador_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  /* Update record */
                  Update53225( ) ;
                  GX_FocusControl = edtLinhaNegocio_Identificador_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( A2036LinhadeNegocio_Codigo != Z2036LinhadeNegocio_Codigo )
               {
                  /* Insert record */
                  GX_FocusControl = edtLinhaNegocio_Identificador_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert53225( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "LINHADENEGOCIO_CODIGO");
                     AnyError = 1;
                     GX_FocusControl = edtLinhadeNegocio_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     /* Insert record */
                     GX_FocusControl = edtLinhaNegocio_Identificador_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert53225( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            if ( AnyError == 0 )
            {
               context.nUserReturn = 1;
            }
         }
      }

      protected void btn_delete( )
      {
         if ( A2036LinhadeNegocio_Codigo != Z2036LinhadeNegocio_Codigo )
         {
            A2036LinhadeNegocio_Codigo = Z2036LinhadeNegocio_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2036LinhadeNegocio_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2036LinhadeNegocio_Codigo), 6, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "LINHADENEGOCIO_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtLinhadeNegocio_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = edtLinhaNegocio_Identificador_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
         }
      }

      protected void CheckOptimisticConcurrency53225( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T00532 */
            pr_default.execute(0, new Object[] {A2036LinhadeNegocio_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"LinhaNegocio"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) || ( StringUtil.StrCmp(Z2037LinhaNegocio_Identificador, T00532_A2037LinhaNegocio_Identificador[0]) != 0 ) || ( StringUtil.StrCmp(Z2038LinhaNegocio_Descricao, T00532_A2038LinhaNegocio_Descricao[0]) != 0 ) || ( Z2035LinhaNegocio_GpoObjCtrlCod != T00532_A2035LinhaNegocio_GpoObjCtrlCod[0] ) )
            {
               if ( StringUtil.StrCmp(Z2037LinhaNegocio_Identificador, T00532_A2037LinhaNegocio_Identificador[0]) != 0 )
               {
                  GXUtil.WriteLog("linhanegocio:[seudo value changed for attri]"+"LinhaNegocio_Identificador");
                  GXUtil.WriteLogRaw("Old: ",Z2037LinhaNegocio_Identificador);
                  GXUtil.WriteLogRaw("Current: ",T00532_A2037LinhaNegocio_Identificador[0]);
               }
               if ( StringUtil.StrCmp(Z2038LinhaNegocio_Descricao, T00532_A2038LinhaNegocio_Descricao[0]) != 0 )
               {
                  GXUtil.WriteLog("linhanegocio:[seudo value changed for attri]"+"LinhaNegocio_Descricao");
                  GXUtil.WriteLogRaw("Old: ",Z2038LinhaNegocio_Descricao);
                  GXUtil.WriteLogRaw("Current: ",T00532_A2038LinhaNegocio_Descricao[0]);
               }
               if ( Z2035LinhaNegocio_GpoObjCtrlCod != T00532_A2035LinhaNegocio_GpoObjCtrlCod[0] )
               {
                  GXUtil.WriteLog("linhanegocio:[seudo value changed for attri]"+"LinhaNegocio_GpoObjCtrlCod");
                  GXUtil.WriteLogRaw("Old: ",Z2035LinhaNegocio_GpoObjCtrlCod);
                  GXUtil.WriteLogRaw("Current: ",T00532_A2035LinhaNegocio_GpoObjCtrlCod[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"LinhaNegocio"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert53225( )
      {
         BeforeValidate53225( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable53225( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM53225( 0) ;
            CheckOptimisticConcurrency53225( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm53225( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert53225( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T005310 */
                     pr_default.execute(8, new Object[] {A2037LinhaNegocio_Identificador, n2038LinhaNegocio_Descricao, A2038LinhaNegocio_Descricao, A2035LinhaNegocio_GpoObjCtrlCod});
                     A2036LinhadeNegocio_Codigo = T005310_A2036LinhadeNegocio_Codigo[0];
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2036LinhadeNegocio_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2036LinhadeNegocio_Codigo), 6, 0)));
                     pr_default.close(8);
                     dsDefault.SmartCacheProvider.SetUpdated("LinhaNegocio") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                           ResetCaption530( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load53225( ) ;
            }
            EndLevel53225( ) ;
         }
         CloseExtendedTableCursors53225( ) ;
      }

      protected void Update53225( )
      {
         BeforeValidate53225( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable53225( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency53225( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm53225( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate53225( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T005311 */
                     pr_default.execute(9, new Object[] {A2037LinhaNegocio_Identificador, n2038LinhaNegocio_Descricao, A2038LinhaNegocio_Descricao, A2035LinhaNegocio_GpoObjCtrlCod, A2036LinhadeNegocio_Codigo});
                     pr_default.close(9);
                     dsDefault.SmartCacheProvider.SetUpdated("LinhaNegocio") ;
                     if ( (pr_default.getStatus(9) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"LinhaNegocio"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate53225( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                           {
                              if ( AnyError == 0 )
                              {
                                 context.nUserReturn = 1;
                              }
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel53225( ) ;
         }
         CloseExtendedTableCursors53225( ) ;
      }

      protected void DeferredUpdate53225( )
      {
      }

      protected void delete( )
      {
         BeforeValidate53225( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency53225( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls53225( ) ;
            AfterConfirm53225( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete53225( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T005312 */
                  pr_default.execute(10, new Object[] {A2036LinhadeNegocio_Codigo});
                  pr_default.close(10);
                  dsDefault.SmartCacheProvider.SetUpdated("LinhaNegocio") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                        {
                           if ( AnyError == 0 )
                           {
                              context.nUserReturn = 1;
                           }
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode225 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         EndLevel53225( ) ;
         Gx_mode = sMode225;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
      }

      protected void OnDeleteControls53225( )
      {
         standaloneModal( ) ;
         /* No delete mode formulas found. */
         if ( AnyError == 0 )
         {
            /* Using cursor T005313 */
            pr_default.execute(11, new Object[] {A2036LinhadeNegocio_Codigo});
            if ( (pr_default.getStatus(11) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Servico"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(11);
            /* Using cursor T005314 */
            pr_default.execute(12, new Object[] {A2036LinhadeNegocio_Codigo});
            if ( (pr_default.getStatus(12) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Tipos de Requisitos"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(12);
         }
      }

      protected void EndLevel53225( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete53225( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(1);
            context.CommitDataStores( "LinhaNegocio");
            if ( AnyError == 0 )
            {
               ConfirmValues530( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(1);
            context.RollbackDataStores( "LinhaNegocio");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart53225( )
      {
         /* Scan By routine */
         /* Using cursor T005315 */
         pr_default.execute(13);
         RcdFound225 = 0;
         if ( (pr_default.getStatus(13) != 101) )
         {
            RcdFound225 = 1;
            A2036LinhadeNegocio_Codigo = T005315_A2036LinhadeNegocio_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2036LinhadeNegocio_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2036LinhadeNegocio_Codigo), 6, 0)));
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext53225( )
      {
         /* Scan next routine */
         pr_default.readNext(13);
         RcdFound225 = 0;
         if ( (pr_default.getStatus(13) != 101) )
         {
            RcdFound225 = 1;
            A2036LinhadeNegocio_Codigo = T005315_A2036LinhadeNegocio_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2036LinhadeNegocio_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2036LinhadeNegocio_Codigo), 6, 0)));
         }
      }

      protected void ScanEnd53225( )
      {
         pr_default.close(13);
      }

      protected void AfterConfirm53225( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert53225( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate53225( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete53225( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete53225( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate53225( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes53225( )
      {
         edtLinhaNegocio_Identificador_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLinhaNegocio_Identificador_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtLinhaNegocio_Identificador_Enabled), 5, 0)));
         edtLinhaNegocio_Descricao_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLinhaNegocio_Descricao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtLinhaNegocio_Descricao_Enabled), 5, 0)));
         dynLinhaNegocio_GpoObjCtrlCod.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynLinhaNegocio_GpoObjCtrlCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynLinhaNegocio_GpoObjCtrlCod.Enabled), 5, 0)));
         edtLinhadeNegocio_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLinhadeNegocio_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtLinhadeNegocio_Codigo_Enabled), 5, 0)));
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues530( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20205299324256");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("linhanegocio.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7LinhadeNegocio_Codigo)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "Z2036LinhadeNegocio_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z2036LinhadeNegocio_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z2037LinhaNegocio_Identificador", Z2037LinhaNegocio_Identificador);
         GxWebStd.gx_hidden_field( context, "Z2038LinhaNegocio_Descricao", Z2038LinhaNegocio_Descricao);
         GxWebStd.gx_hidden_field( context, "Z2035LinhaNegocio_GpoObjCtrlCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z2035LinhaNegocio_GpoObjCtrlCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "N2035LinhaNegocio_GpoObjCtrlCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(A2035LinhaNegocio_GpoObjCtrlCod), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTRNCONTEXT", AV9TrnContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTRNCONTEXT", AV9TrnContext);
         }
         GxWebStd.gx_hidden_field( context, "vLINHADENEGOCIO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7LinhadeNegocio_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vINSERT_LINHANEGOCIO_GPOOBJCTRLCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV11Insert_LinhaNegocio_GpoObjCtrlCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV13Pgmname));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vLINHADENEGOCIO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7LinhadeNegocio_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Width", StringUtil.RTrim( Dvpanel_tableattributes_Width));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Cls", StringUtil.RTrim( Dvpanel_tableattributes_Cls));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Title", StringUtil.RTrim( Dvpanel_tableattributes_Title));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsible", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsible));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsed", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsed));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Enabled", StringUtil.BoolToStr( Dvpanel_tableattributes_Enabled));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autowidth", StringUtil.BoolToStr( Dvpanel_tableattributes_Autowidth));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoheight", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoheight));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Showcollapseicon", StringUtil.BoolToStr( Dvpanel_tableattributes_Showcollapseicon));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Iconposition", StringUtil.RTrim( Dvpanel_tableattributes_Iconposition));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoscroll", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoscroll));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = "hsh" + "LinhaNegocio";
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A2036LinhadeNegocio_Codigo), "ZZZZZ9");
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
         GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("linhanegocio:[SendSecurityCheck value for]"+"LinhadeNegocio_Codigo:"+context.localUtil.Format( (decimal)(A2036LinhadeNegocio_Codigo), "ZZZZZ9"));
         GXUtil.WriteLog("linhanegocio:[SendSecurityCheck value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("linhanegocio.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7LinhadeNegocio_Codigo) ;
      }

      public override String GetPgmname( )
      {
         return "LinhaNegocio" ;
      }

      public override String GetPgmdesc( )
      {
         return "Linhas de Neg�cio" ;
      }

      protected void InitializeNonKey53225( )
      {
         A2035LinhaNegocio_GpoObjCtrlCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2035LinhaNegocio_GpoObjCtrlCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A2035LinhaNegocio_GpoObjCtrlCod), 6, 0)));
         A2037LinhaNegocio_Identificador = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2037LinhaNegocio_Identificador", A2037LinhaNegocio_Identificador);
         A2038LinhaNegocio_Descricao = "";
         n2038LinhaNegocio_Descricao = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2038LinhaNegocio_Descricao", A2038LinhaNegocio_Descricao);
         n2038LinhaNegocio_Descricao = (String.IsNullOrEmpty(StringUtil.RTrim( A2038LinhaNegocio_Descricao)) ? true : false);
         Z2037LinhaNegocio_Identificador = "";
         Z2038LinhaNegocio_Descricao = "";
         Z2035LinhaNegocio_GpoObjCtrlCod = 0;
      }

      protected void InitAll53225( )
      {
         A2036LinhadeNegocio_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2036LinhadeNegocio_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A2036LinhadeNegocio_Codigo), 6, 0)));
         InitializeNonKey53225( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20205299324277");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("linhanegocio.js", "?20205299324277");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblocklinhanegocio_identificador_Internalname = "TEXTBLOCKLINHANEGOCIO_IDENTIFICADOR";
         edtLinhaNegocio_Identificador_Internalname = "LINHANEGOCIO_IDENTIFICADOR";
         lblTextblocklinhanegocio_descricao_Internalname = "TEXTBLOCKLINHANEGOCIO_DESCRICAO";
         edtLinhaNegocio_Descricao_Internalname = "LINHANEGOCIO_DESCRICAO";
         lblTextblocklinhanegocio_gpoobjctrlcod_Internalname = "TEXTBLOCKLINHANEGOCIO_GPOOBJCTRLCOD";
         dynLinhaNegocio_GpoObjCtrlCod_Internalname = "LINHANEGOCIO_GPOOBJCTRLCOD";
         tblTableattributes_Internalname = "TABLEATTRIBUTES";
         Dvpanel_tableattributes_Internalname = "DVPANEL_TABLEATTRIBUTES";
         tblTablecontent_Internalname = "TABLECONTENT";
         bttBtn_trn_enter_Internalname = "BTN_TRN_ENTER";
         bttBtn_trn_cancel_Internalname = "BTN_TRN_CANCEL";
         bttBtn_trn_delete_Internalname = "BTN_TRN_DELETE";
         tblTableactions_Internalname = "TABLEACTIONS";
         tblTablemain_Internalname = "TABLEMAIN";
         edtLinhadeNegocio_Codigo_Internalname = "LINHADENEGOCIO_CODIGO";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Dvpanel_tableattributes_Autoscroll = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Iconposition = "left";
         Dvpanel_tableattributes_Showcollapseicon = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Autoheight = Convert.ToBoolean( -1);
         Dvpanel_tableattributes_Autowidth = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsed = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsible = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Title = "Linha de Neg�cio";
         Dvpanel_tableattributes_Cls = "GXUI-DVelop-Panel";
         Dvpanel_tableattributes_Width = "100%";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Linhas de Neg�cio";
         dynLinhaNegocio_GpoObjCtrlCod_Jsonclick = "";
         dynLinhaNegocio_GpoObjCtrlCod.Enabled = 1;
         edtLinhaNegocio_Descricao_Enabled = 1;
         edtLinhaNegocio_Identificador_Jsonclick = "";
         edtLinhaNegocio_Identificador_Enabled = 1;
         bttBtn_trn_delete_Enabled = 0;
         bttBtn_trn_delete_Visible = 1;
         bttBtn_trn_cancel_Visible = 1;
         bttBtn_trn_enter_Enabled = 1;
         bttBtn_trn_enter_Visible = 1;
         edtLinhadeNegocio_Codigo_Jsonclick = "";
         edtLinhadeNegocio_Codigo_Enabled = 0;
         edtLinhadeNegocio_Codigo_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void GXDLALINHANEGOCIO_GPOOBJCTRLCOD53225( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLALINHANEGOCIO_GPOOBJCTRLCOD_data53225( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXALINHANEGOCIO_GPOOBJCTRLCOD_html53225( )
      {
         int gxdynajaxvalue ;
         GXDLALINHANEGOCIO_GPOOBJCTRLCOD_data53225( ) ;
         gxdynajaxindex = 1;
         dynLinhaNegocio_GpoObjCtrlCod.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynLinhaNegocio_GpoObjCtrlCod.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
      }

      protected void GXDLALINHANEGOCIO_GPOOBJCTRLCOD_data53225( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("(Nenhum)");
         /* Using cursor T005316 */
         pr_default.execute(14);
         while ( (pr_default.getStatus(14) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(T005316_A2035LinhaNegocio_GpoObjCtrlCod[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( T005316_A1827GpoObjCtrl_Nome[0]));
            pr_default.readNext(14);
         }
         pr_default.close(14);
      }

      public void Valid_Linhanegocio_gpoobjctrlcod( GXCombobox dynGX_Parm1 )
      {
         dynLinhaNegocio_GpoObjCtrlCod = dynGX_Parm1;
         A2035LinhaNegocio_GpoObjCtrlCod = (int)(NumberUtil.Val( dynLinhaNegocio_GpoObjCtrlCod.CurrentValue, "."));
         /* Using cursor T005317 */
         pr_default.execute(15, new Object[] {A2035LinhaNegocio_GpoObjCtrlCod});
         if ( (pr_default.getStatus(15) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Linha Negocio_Gpo Obj Ctrl'.", "ForeignKeyNotFound", 1, "LINHANEGOCIO_GPOOBJCTRLCOD");
            AnyError = 1;
            GX_FocusControl = dynLinhaNegocio_GpoObjCtrlCod_Internalname;
         }
         pr_default.close(15);
         dynload_actions( ) ;
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true},{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV7LinhadeNegocio_Codigo',fld:'vLINHADENEGOCIO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("AFTER TRN","{handler:'E12532',iparms:[{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV9TrnContext',fld:'vTRNCONTEXT',pic:'',nv:null}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(15);
      }

      public override void initialize( )
      {
         sPrefix = "";
         wcpOGx_mode = "";
         Z2037LinhaNegocio_Identificador = "";
         Z2038LinhaNegocio_Descricao = "";
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         sStyleString = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         bttBtn_trn_enter_Jsonclick = "";
         bttBtn_trn_cancel_Jsonclick = "";
         bttBtn_trn_delete_Jsonclick = "";
         lblTextblocklinhanegocio_identificador_Jsonclick = "";
         A2037LinhaNegocio_Identificador = "";
         lblTextblocklinhanegocio_descricao_Jsonclick = "";
         A2038LinhaNegocio_Descricao = "";
         lblTextblocklinhanegocio_gpoobjctrlcod_Jsonclick = "";
         AV13Pgmname = "";
         Dvpanel_tableattributes_Height = "";
         Dvpanel_tableattributes_Class = "";
         forbiddenHiddens = "";
         hsh = "";
         sMode225 = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV8WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV10WebSession = context.GetSession();
         AV12TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         T00535_A2036LinhadeNegocio_Codigo = new int[1] ;
         T00535_A2037LinhaNegocio_Identificador = new String[] {""} ;
         T00535_A2038LinhaNegocio_Descricao = new String[] {""} ;
         T00535_n2038LinhaNegocio_Descricao = new bool[] {false} ;
         T00535_A2035LinhaNegocio_GpoObjCtrlCod = new int[1] ;
         T00534_A2035LinhaNegocio_GpoObjCtrlCod = new int[1] ;
         T00536_A2035LinhaNegocio_GpoObjCtrlCod = new int[1] ;
         T00537_A2036LinhadeNegocio_Codigo = new int[1] ;
         T00533_A2036LinhadeNegocio_Codigo = new int[1] ;
         T00533_A2037LinhaNegocio_Identificador = new String[] {""} ;
         T00533_A2038LinhaNegocio_Descricao = new String[] {""} ;
         T00533_n2038LinhaNegocio_Descricao = new bool[] {false} ;
         T00533_A2035LinhaNegocio_GpoObjCtrlCod = new int[1] ;
         T00538_A2036LinhadeNegocio_Codigo = new int[1] ;
         T00539_A2036LinhadeNegocio_Codigo = new int[1] ;
         T00532_A2036LinhadeNegocio_Codigo = new int[1] ;
         T00532_A2037LinhaNegocio_Identificador = new String[] {""} ;
         T00532_A2038LinhaNegocio_Descricao = new String[] {""} ;
         T00532_n2038LinhaNegocio_Descricao = new bool[] {false} ;
         T00532_A2035LinhaNegocio_GpoObjCtrlCod = new int[1] ;
         T005310_A2036LinhadeNegocio_Codigo = new int[1] ;
         T005313_A155Servico_Codigo = new int[1] ;
         T005314_A2041TipoRequisito_Codigo = new int[1] ;
         T005315_A2036LinhadeNegocio_Codigo = new int[1] ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         T005316_A2035LinhaNegocio_GpoObjCtrlCod = new int[1] ;
         T005316_A1827GpoObjCtrl_Nome = new String[] {""} ;
         T005316_n1827GpoObjCtrl_Nome = new bool[] {false} ;
         T005317_A2035LinhaNegocio_GpoObjCtrlCod = new int[1] ;
         isValidOutput = new GxUnknownObjectCollection();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.linhanegocio__default(),
            new Object[][] {
                new Object[] {
               T00532_A2036LinhadeNegocio_Codigo, T00532_A2037LinhaNegocio_Identificador, T00532_A2038LinhaNegocio_Descricao, T00532_n2038LinhaNegocio_Descricao, T00532_A2035LinhaNegocio_GpoObjCtrlCod
               }
               , new Object[] {
               T00533_A2036LinhadeNegocio_Codigo, T00533_A2037LinhaNegocio_Identificador, T00533_A2038LinhaNegocio_Descricao, T00533_n2038LinhaNegocio_Descricao, T00533_A2035LinhaNegocio_GpoObjCtrlCod
               }
               , new Object[] {
               T00534_A2035LinhaNegocio_GpoObjCtrlCod
               }
               , new Object[] {
               T00535_A2036LinhadeNegocio_Codigo, T00535_A2037LinhaNegocio_Identificador, T00535_A2038LinhaNegocio_Descricao, T00535_n2038LinhaNegocio_Descricao, T00535_A2035LinhaNegocio_GpoObjCtrlCod
               }
               , new Object[] {
               T00536_A2035LinhaNegocio_GpoObjCtrlCod
               }
               , new Object[] {
               T00537_A2036LinhadeNegocio_Codigo
               }
               , new Object[] {
               T00538_A2036LinhadeNegocio_Codigo
               }
               , new Object[] {
               T00539_A2036LinhadeNegocio_Codigo
               }
               , new Object[] {
               T005310_A2036LinhadeNegocio_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T005313_A155Servico_Codigo
               }
               , new Object[] {
               T005314_A2041TipoRequisito_Codigo
               }
               , new Object[] {
               T005315_A2036LinhadeNegocio_Codigo
               }
               , new Object[] {
               T005316_A2035LinhaNegocio_GpoObjCtrlCod, T005316_A1827GpoObjCtrl_Nome, T005316_n1827GpoObjCtrl_Nome
               }
               , new Object[] {
               T005317_A2035LinhaNegocio_GpoObjCtrlCod
               }
            }
         );
         AV13Pgmname = "LinhaNegocio";
      }

      private short GxWebError ;
      private short gxcookieaux ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short RcdFound225 ;
      private short GX_JID ;
      private short Gx_BScreen ;
      private short gxajaxcallmode ;
      private short wbTemp ;
      private int wcpOAV7LinhadeNegocio_Codigo ;
      private int Z2036LinhadeNegocio_Codigo ;
      private int Z2035LinhaNegocio_GpoObjCtrlCod ;
      private int N2035LinhaNegocio_GpoObjCtrlCod ;
      private int A2035LinhaNegocio_GpoObjCtrlCod ;
      private int AV7LinhadeNegocio_Codigo ;
      private int trnEnded ;
      private int A2036LinhadeNegocio_Codigo ;
      private int edtLinhadeNegocio_Codigo_Enabled ;
      private int edtLinhadeNegocio_Codigo_Visible ;
      private int bttBtn_trn_enter_Visible ;
      private int bttBtn_trn_enter_Enabled ;
      private int bttBtn_trn_cancel_Visible ;
      private int bttBtn_trn_delete_Visible ;
      private int bttBtn_trn_delete_Enabled ;
      private int edtLinhaNegocio_Identificador_Enabled ;
      private int edtLinhaNegocio_Descricao_Enabled ;
      private int AV11Insert_LinhaNegocio_GpoObjCtrlCod ;
      private int AV14GXV1 ;
      private int idxLst ;
      private int gxdynajaxindex ;
      private String sPrefix ;
      private String wcpOGx_mode ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String Gx_mode ;
      private String GXKey ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String edtLinhaNegocio_Identificador_Internalname ;
      private String edtLinhadeNegocio_Codigo_Internalname ;
      private String edtLinhadeNegocio_Codigo_Jsonclick ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTableactions_Internalname ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String bttBtn_trn_enter_Internalname ;
      private String bttBtn_trn_enter_Jsonclick ;
      private String bttBtn_trn_cancel_Internalname ;
      private String bttBtn_trn_cancel_Jsonclick ;
      private String bttBtn_trn_delete_Internalname ;
      private String bttBtn_trn_delete_Jsonclick ;
      private String tblTablecontent_Internalname ;
      private String tblTableattributes_Internalname ;
      private String lblTextblocklinhanegocio_identificador_Internalname ;
      private String lblTextblocklinhanegocio_identificador_Jsonclick ;
      private String edtLinhaNegocio_Identificador_Jsonclick ;
      private String lblTextblocklinhanegocio_descricao_Internalname ;
      private String lblTextblocklinhanegocio_descricao_Jsonclick ;
      private String edtLinhaNegocio_Descricao_Internalname ;
      private String lblTextblocklinhanegocio_gpoobjctrlcod_Internalname ;
      private String lblTextblocklinhanegocio_gpoobjctrlcod_Jsonclick ;
      private String dynLinhaNegocio_GpoObjCtrlCod_Internalname ;
      private String dynLinhaNegocio_GpoObjCtrlCod_Jsonclick ;
      private String AV13Pgmname ;
      private String Dvpanel_tableattributes_Width ;
      private String Dvpanel_tableattributes_Height ;
      private String Dvpanel_tableattributes_Cls ;
      private String Dvpanel_tableattributes_Title ;
      private String Dvpanel_tableattributes_Class ;
      private String Dvpanel_tableattributes_Iconposition ;
      private String forbiddenHiddens ;
      private String hsh ;
      private String sMode225 ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Dvpanel_tableattributes_Internalname ;
      private String gxwrpcisep ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbErr ;
      private bool n2038LinhaNegocio_Descricao ;
      private bool Dvpanel_tableattributes_Collapsible ;
      private bool Dvpanel_tableattributes_Collapsed ;
      private bool Dvpanel_tableattributes_Enabled ;
      private bool Dvpanel_tableattributes_Autowidth ;
      private bool Dvpanel_tableattributes_Autoheight ;
      private bool Dvpanel_tableattributes_Showheader ;
      private bool Dvpanel_tableattributes_Showcollapseicon ;
      private bool Dvpanel_tableattributes_Autoscroll ;
      private bool Dvpanel_tableattributes_Visible ;
      private bool returnInSub ;
      private String Z2037LinhaNegocio_Identificador ;
      private String Z2038LinhaNegocio_Descricao ;
      private String A2037LinhaNegocio_Identificador ;
      private String A2038LinhaNegocio_Descricao ;
      private IGxSession AV10WebSession ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private GxUnknownObjectCollection isValidOutput ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox dynLinhaNegocio_GpoObjCtrlCod ;
      private IDataStoreProvider pr_default ;
      private int[] T00535_A2036LinhadeNegocio_Codigo ;
      private String[] T00535_A2037LinhaNegocio_Identificador ;
      private String[] T00535_A2038LinhaNegocio_Descricao ;
      private bool[] T00535_n2038LinhaNegocio_Descricao ;
      private int[] T00535_A2035LinhaNegocio_GpoObjCtrlCod ;
      private int[] T00534_A2035LinhaNegocio_GpoObjCtrlCod ;
      private int[] T00536_A2035LinhaNegocio_GpoObjCtrlCod ;
      private int[] T00537_A2036LinhadeNegocio_Codigo ;
      private int[] T00533_A2036LinhadeNegocio_Codigo ;
      private String[] T00533_A2037LinhaNegocio_Identificador ;
      private String[] T00533_A2038LinhaNegocio_Descricao ;
      private bool[] T00533_n2038LinhaNegocio_Descricao ;
      private int[] T00533_A2035LinhaNegocio_GpoObjCtrlCod ;
      private int[] T00538_A2036LinhadeNegocio_Codigo ;
      private int[] T00539_A2036LinhadeNegocio_Codigo ;
      private int[] T00532_A2036LinhadeNegocio_Codigo ;
      private String[] T00532_A2037LinhaNegocio_Identificador ;
      private String[] T00532_A2038LinhaNegocio_Descricao ;
      private bool[] T00532_n2038LinhaNegocio_Descricao ;
      private int[] T00532_A2035LinhaNegocio_GpoObjCtrlCod ;
      private int[] T005310_A2036LinhadeNegocio_Codigo ;
      private int[] T005313_A155Servico_Codigo ;
      private int[] T005314_A2041TipoRequisito_Codigo ;
      private int[] T005315_A2036LinhadeNegocio_Codigo ;
      private int[] T005316_A2035LinhaNegocio_GpoObjCtrlCod ;
      private String[] T005316_A1827GpoObjCtrl_Nome ;
      private bool[] T005316_n1827GpoObjCtrl_Nome ;
      private int[] T005317_A2035LinhaNegocio_GpoObjCtrlCod ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV8WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV12TrnContextAtt ;
   }

   public class linhanegocio__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new UpdateCursor(def[9])
         ,new UpdateCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new ForEachCursor(def[12])
         ,new ForEachCursor(def[13])
         ,new ForEachCursor(def[14])
         ,new ForEachCursor(def[15])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT00535 ;
          prmT00535 = new Object[] {
          new Object[] {"@LinhadeNegocio_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00534 ;
          prmT00534 = new Object[] {
          new Object[] {"@LinhaNegocio_GpoObjCtrlCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00536 ;
          prmT00536 = new Object[] {
          new Object[] {"@LinhaNegocio_GpoObjCtrlCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00537 ;
          prmT00537 = new Object[] {
          new Object[] {"@LinhadeNegocio_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00533 ;
          prmT00533 = new Object[] {
          new Object[] {"@LinhadeNegocio_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00538 ;
          prmT00538 = new Object[] {
          new Object[] {"@LinhadeNegocio_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00539 ;
          prmT00539 = new Object[] {
          new Object[] {"@LinhadeNegocio_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00532 ;
          prmT00532 = new Object[] {
          new Object[] {"@LinhadeNegocio_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT005310 ;
          prmT005310 = new Object[] {
          new Object[] {"@LinhaNegocio_Identificador",SqlDbType.VarChar,15,0} ,
          new Object[] {"@LinhaNegocio_Descricao",SqlDbType.VarChar,250,0} ,
          new Object[] {"@LinhaNegocio_GpoObjCtrlCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT005311 ;
          prmT005311 = new Object[] {
          new Object[] {"@LinhaNegocio_Identificador",SqlDbType.VarChar,15,0} ,
          new Object[] {"@LinhaNegocio_Descricao",SqlDbType.VarChar,250,0} ,
          new Object[] {"@LinhaNegocio_GpoObjCtrlCod",SqlDbType.Int,6,0} ,
          new Object[] {"@LinhadeNegocio_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT005312 ;
          prmT005312 = new Object[] {
          new Object[] {"@LinhadeNegocio_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT005313 ;
          prmT005313 = new Object[] {
          new Object[] {"@LinhadeNegocio_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT005314 ;
          prmT005314 = new Object[] {
          new Object[] {"@LinhadeNegocio_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT005315 ;
          prmT005315 = new Object[] {
          } ;
          Object[] prmT005316 ;
          prmT005316 = new Object[] {
          } ;
          Object[] prmT005317 ;
          prmT005317 = new Object[] {
          new Object[] {"@LinhaNegocio_GpoObjCtrlCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("T00532", "SELECT [LinhadeNegocio_Codigo], [LinhaNegocio_Identificador], [LinhaNegocio_Descricao], [LinhaNegocio_GpoObjCtrlCod] AS LinhaNegocio_GpoObjCtrlCod FROM [LinhaNegocio] WITH (UPDLOCK) WHERE [LinhadeNegocio_Codigo] = @LinhadeNegocio_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT00532,1,0,true,false )
             ,new CursorDef("T00533", "SELECT [LinhadeNegocio_Codigo], [LinhaNegocio_Identificador], [LinhaNegocio_Descricao], [LinhaNegocio_GpoObjCtrlCod] AS LinhaNegocio_GpoObjCtrlCod FROM [LinhaNegocio] WITH (NOLOCK) WHERE [LinhadeNegocio_Codigo] = @LinhadeNegocio_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT00533,1,0,true,false )
             ,new CursorDef("T00534", "SELECT [GpoObjCtrl_Codigo] AS LinhaNegocio_GpoObjCtrlCod FROM [GrupoObjetoControle] WITH (NOLOCK) WHERE [GpoObjCtrl_Codigo] = @LinhaNegocio_GpoObjCtrlCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT00534,1,0,true,false )
             ,new CursorDef("T00535", "SELECT TM1.[LinhadeNegocio_Codigo], TM1.[LinhaNegocio_Identificador], TM1.[LinhaNegocio_Descricao], TM1.[LinhaNegocio_GpoObjCtrlCod] AS LinhaNegocio_GpoObjCtrlCod FROM [LinhaNegocio] TM1 WITH (NOLOCK) WHERE TM1.[LinhadeNegocio_Codigo] = @LinhadeNegocio_Codigo ORDER BY TM1.[LinhadeNegocio_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT00535,100,0,true,false )
             ,new CursorDef("T00536", "SELECT [GpoObjCtrl_Codigo] AS LinhaNegocio_GpoObjCtrlCod FROM [GrupoObjetoControle] WITH (NOLOCK) WHERE [GpoObjCtrl_Codigo] = @LinhaNegocio_GpoObjCtrlCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT00536,1,0,true,false )
             ,new CursorDef("T00537", "SELECT [LinhadeNegocio_Codigo] FROM [LinhaNegocio] WITH (NOLOCK) WHERE [LinhadeNegocio_Codigo] = @LinhadeNegocio_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT00537,1,0,true,false )
             ,new CursorDef("T00538", "SELECT TOP 1 [LinhadeNegocio_Codigo] FROM [LinhaNegocio] WITH (NOLOCK) WHERE ( [LinhadeNegocio_Codigo] > @LinhadeNegocio_Codigo) ORDER BY [LinhadeNegocio_Codigo]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT00538,1,0,true,true )
             ,new CursorDef("T00539", "SELECT TOP 1 [LinhadeNegocio_Codigo] FROM [LinhaNegocio] WITH (NOLOCK) WHERE ( [LinhadeNegocio_Codigo] < @LinhadeNegocio_Codigo) ORDER BY [LinhadeNegocio_Codigo] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT00539,1,0,true,true )
             ,new CursorDef("T005310", "INSERT INTO [LinhaNegocio]([LinhaNegocio_Identificador], [LinhaNegocio_Descricao], [LinhaNegocio_GpoObjCtrlCod]) VALUES(@LinhaNegocio_Identificador, @LinhaNegocio_Descricao, @LinhaNegocio_GpoObjCtrlCod); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmT005310)
             ,new CursorDef("T005311", "UPDATE [LinhaNegocio] SET [LinhaNegocio_Identificador]=@LinhaNegocio_Identificador, [LinhaNegocio_Descricao]=@LinhaNegocio_Descricao, [LinhaNegocio_GpoObjCtrlCod]=@LinhaNegocio_GpoObjCtrlCod  WHERE [LinhadeNegocio_Codigo] = @LinhadeNegocio_Codigo", GxErrorMask.GX_NOMASK,prmT005311)
             ,new CursorDef("T005312", "DELETE FROM [LinhaNegocio]  WHERE [LinhadeNegocio_Codigo] = @LinhadeNegocio_Codigo", GxErrorMask.GX_NOMASK,prmT005312)
             ,new CursorDef("T005313", "SELECT TOP 1 [Servico_Codigo] FROM [Servico] WITH (NOLOCK) WHERE [Servico_LinNegCod] = @LinhadeNegocio_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT005313,1,0,true,true )
             ,new CursorDef("T005314", "SELECT TOP 1 [TipoRequisito_Codigo] FROM [TipoRequisito] WITH (NOLOCK) WHERE [TipoRequisito_LinNegCod] = @LinhadeNegocio_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT005314,1,0,true,true )
             ,new CursorDef("T005315", "SELECT [LinhadeNegocio_Codigo] FROM [LinhaNegocio] WITH (NOLOCK) ORDER BY [LinhadeNegocio_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT005315,100,0,true,false )
             ,new CursorDef("T005316", "SELECT [GpoObjCtrl_Codigo] AS LinhaNegocio_GpoObjCtrlCod, [GpoObjCtrl_Nome] FROM [GrupoObjetoControle] WITH (NOLOCK) ORDER BY [GpoObjCtrl_Nome] ",true, GxErrorMask.GX_NOMASK, false, this,prmT005316,0,0,true,false )
             ,new CursorDef("T005317", "SELECT [GpoObjCtrl_Codigo] AS LinhaNegocio_GpoObjCtrlCod FROM [GrupoObjetoControle] WITH (NOLOCK) WHERE [GpoObjCtrl_Codigo] = @LinhaNegocio_GpoObjCtrlCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT005317,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 11 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 12 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 13 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 14 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                return;
             case 15 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 8 :
                stmt.SetParameter(1, (String)parms[0]);
                if ( (bool)parms[1] )
                {
                   stmt.setNull( 2 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(2, (String)parms[2]);
                }
                stmt.SetParameter(3, (int)parms[3]);
                return;
             case 9 :
                stmt.SetParameter(1, (String)parms[0]);
                if ( (bool)parms[1] )
                {
                   stmt.setNull( 2 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(2, (String)parms[2]);
                }
                stmt.SetParameter(3, (int)parms[3]);
                stmt.SetParameter(4, (int)parms[4]);
                return;
             case 10 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 11 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 12 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 15 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
