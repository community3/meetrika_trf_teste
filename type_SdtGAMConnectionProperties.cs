/*
               File: type_SdtGAMConnectionProperties
        Description: GAMConnectionProperties
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/29/2020 0:29:43.80
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [Serializable]
   public class SdtGAMConnectionProperties : GxUserType, IGxExternalObject
   {
      public SdtGAMConnectionProperties( )
      {
         initialize();
      }

      public SdtGAMConnectionProperties( IGxContext context )
      {
         this.context = context;
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public String tostring( )
      {
         String returntostring ;
         if ( GAMConnectionProperties_externalReference == null )
         {
            GAMConnectionProperties_externalReference = new Artech.Security.GAMConnectionProperties(context);
         }
         returntostring = "";
         returntostring = (String)(GAMConnectionProperties_externalReference.ToString());
         return returntostring ;
      }

      public String gxTpr_Id
      {
         get {
            if ( GAMConnectionProperties_externalReference == null )
            {
               GAMConnectionProperties_externalReference = new Artech.Security.GAMConnectionProperties(context);
            }
            return GAMConnectionProperties_externalReference.Id ;
         }

         set {
            if ( GAMConnectionProperties_externalReference == null )
            {
               GAMConnectionProperties_externalReference = new Artech.Security.GAMConnectionProperties(context);
            }
            GAMConnectionProperties_externalReference.Id = value;
         }

      }

      public String gxTpr_Value
      {
         get {
            if ( GAMConnectionProperties_externalReference == null )
            {
               GAMConnectionProperties_externalReference = new Artech.Security.GAMConnectionProperties(context);
            }
            return GAMConnectionProperties_externalReference.Value ;
         }

         set {
            if ( GAMConnectionProperties_externalReference == null )
            {
               GAMConnectionProperties_externalReference = new Artech.Security.GAMConnectionProperties(context);
            }
            GAMConnectionProperties_externalReference.Value = value;
         }

      }

      public Object ExternalInstance
      {
         get {
            if ( GAMConnectionProperties_externalReference == null )
            {
               GAMConnectionProperties_externalReference = new Artech.Security.GAMConnectionProperties(context);
            }
            return GAMConnectionProperties_externalReference ;
         }

         set {
            GAMConnectionProperties_externalReference = (Artech.Security.GAMConnectionProperties)(value);
         }

      }

      public void initialize( )
      {
         return  ;
      }

      protected Artech.Security.GAMConnectionProperties GAMConnectionProperties_externalReference=null ;
   }

}
