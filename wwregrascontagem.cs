/*
               File: WWRegrasContagem
        Description:  Regras de Contagem
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/18/2020 13:25:57.98
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wwregrascontagem : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wwregrascontagem( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wwregrascontagem( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavOrderedby = new GXCombobox();
         cmbavDynamicfiltersselector1 = new GXCombobox();
         cmbavDynamicfiltersselector2 = new GXCombobox();
         cmbavDynamicfiltersselector3 = new GXCombobox();
         chkavDynamicfiltersenabled2 = new GXCheckbox();
         chkavDynamicfiltersenabled3 = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
            {
               nRC_GXsfl_106 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_106_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_106_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
            {
               subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV13OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
               AV14OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
               AV15DynamicFiltersSelector1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
               AV31RegrasContagem_Regra1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31RegrasContagem_Regra1", AV31RegrasContagem_Regra1);
               AV16RegrasContagem_Data1 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16RegrasContagem_Data1", context.localUtil.Format(AV16RegrasContagem_Data1, "99/99/99"));
               AV17RegrasContagem_Data_To1 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17RegrasContagem_Data_To1", context.localUtil.Format(AV17RegrasContagem_Data_To1, "99/99/99"));
               AV32RegrasContagem_Responsavel1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32RegrasContagem_Responsavel1", AV32RegrasContagem_Responsavel1);
               AV19DynamicFiltersSelector2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
               AV33RegrasContagem_Regra2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33RegrasContagem_Regra2", AV33RegrasContagem_Regra2);
               AV20RegrasContagem_Data2 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20RegrasContagem_Data2", context.localUtil.Format(AV20RegrasContagem_Data2, "99/99/99"));
               AV21RegrasContagem_Data_To2 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21RegrasContagem_Data_To2", context.localUtil.Format(AV21RegrasContagem_Data_To2, "99/99/99"));
               AV34RegrasContagem_Responsavel2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34RegrasContagem_Responsavel2", AV34RegrasContagem_Responsavel2);
               AV23DynamicFiltersSelector3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
               AV35RegrasContagem_Regra3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35RegrasContagem_Regra3", AV35RegrasContagem_Regra3);
               AV24RegrasContagem_Data3 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24RegrasContagem_Data3", context.localUtil.Format(AV24RegrasContagem_Data3, "99/99/99"));
               AV25RegrasContagem_Data_To3 = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25RegrasContagem_Data_To3", context.localUtil.Format(AV25RegrasContagem_Data_To3, "99/99/99"));
               AV36RegrasContagem_Responsavel3 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36RegrasContagem_Responsavel3", AV36RegrasContagem_Responsavel3);
               AV18DynamicFiltersEnabled2 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
               AV22DynamicFiltersEnabled3 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
               AV41TFRegrasContagem_Regra = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41TFRegrasContagem_Regra", AV41TFRegrasContagem_Regra);
               AV42TFRegrasContagem_Regra_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42TFRegrasContagem_Regra_Sel", AV42TFRegrasContagem_Regra_Sel);
               AV45TFRegrasContagem_Data = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45TFRegrasContagem_Data", context.localUtil.Format(AV45TFRegrasContagem_Data, "99/99/99"));
               AV46TFRegrasContagem_Data_To = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46TFRegrasContagem_Data_To", context.localUtil.Format(AV46TFRegrasContagem_Data_To, "99/99/99"));
               AV51TFRegrasContagem_Validade = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51TFRegrasContagem_Validade", context.localUtil.Format(AV51TFRegrasContagem_Validade, "99/99/99"));
               AV52TFRegrasContagem_Validade_To = context.localUtil.ParseDateParm( GetNextPar( ));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52TFRegrasContagem_Validade_To", context.localUtil.Format(AV52TFRegrasContagem_Validade_To, "99/99/99"));
               AV57TFRegrasContagem_Responsavel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57TFRegrasContagem_Responsavel", AV57TFRegrasContagem_Responsavel);
               AV58TFRegrasContagem_Responsavel_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58TFRegrasContagem_Responsavel_Sel", AV58TFRegrasContagem_Responsavel_Sel);
               AV43ddo_RegrasContagem_RegraTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43ddo_RegrasContagem_RegraTitleControlIdToReplace", AV43ddo_RegrasContagem_RegraTitleControlIdToReplace);
               AV49ddo_RegrasContagem_DataTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49ddo_RegrasContagem_DataTitleControlIdToReplace", AV49ddo_RegrasContagem_DataTitleControlIdToReplace);
               AV55ddo_RegrasContagem_ValidadeTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55ddo_RegrasContagem_ValidadeTitleControlIdToReplace", AV55ddo_RegrasContagem_ValidadeTitleControlIdToReplace);
               AV59ddo_RegrasContagem_ResponsavelTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59ddo_RegrasContagem_ResponsavelTitleControlIdToReplace", AV59ddo_RegrasContagem_ResponsavelTitleControlIdToReplace);
               AV37RegrasContagem_AreaTrabalhoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37RegrasContagem_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV37RegrasContagem_AreaTrabalhoCod), 6, 0)));
               ajax_req_read_hidden_sdt(GetNextPar( ), AV6WWPContext);
               AV94Pgmname = GetNextPar( );
               ajax_req_read_hidden_sdt(GetNextPar( ), AV10GridState);
               AV27DynamicFiltersIgnoreFirst = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersIgnoreFirst", AV27DynamicFiltersIgnoreFirst);
               AV26DynamicFiltersRemoving = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
               A860RegrasContagem_Regra = GetNextPar( );
               A864RegrasContagem_Descricao = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A864RegrasContagem_Descricao", A864RegrasContagem_Descricao);
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV31RegrasContagem_Regra1, AV16RegrasContagem_Data1, AV17RegrasContagem_Data_To1, AV32RegrasContagem_Responsavel1, AV19DynamicFiltersSelector2, AV33RegrasContagem_Regra2, AV20RegrasContagem_Data2, AV21RegrasContagem_Data_To2, AV34RegrasContagem_Responsavel2, AV23DynamicFiltersSelector3, AV35RegrasContagem_Regra3, AV24RegrasContagem_Data3, AV25RegrasContagem_Data_To3, AV36RegrasContagem_Responsavel3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV41TFRegrasContagem_Regra, AV42TFRegrasContagem_Regra_Sel, AV45TFRegrasContagem_Data, AV46TFRegrasContagem_Data_To, AV51TFRegrasContagem_Validade, AV52TFRegrasContagem_Validade_To, AV57TFRegrasContagem_Responsavel, AV58TFRegrasContagem_Responsavel_Sel, AV43ddo_RegrasContagem_RegraTitleControlIdToReplace, AV49ddo_RegrasContagem_DataTitleControlIdToReplace, AV55ddo_RegrasContagem_ValidadeTitleControlIdToReplace, AV59ddo_RegrasContagem_ResponsavelTitleControlIdToReplace, AV37RegrasContagem_AreaTrabalhoCod, AV6WWPContext, AV94Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A860RegrasContagem_Regra, A864RegrasContagem_Descricao) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PAFC2( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            STARTFC2( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?202051813255844");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wwregrascontagem.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDDSC", StringUtil.BoolToStr( AV14OrderedDsc));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR1", AV15DynamicFiltersSelector1);
         GxWebStd.gx_hidden_field( context, "GXH_vREGRASCONTAGEM_REGRA1", AV31RegrasContagem_Regra1);
         GxWebStd.gx_hidden_field( context, "GXH_vREGRASCONTAGEM_DATA1", context.localUtil.Format(AV16RegrasContagem_Data1, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vREGRASCONTAGEM_DATA_TO1", context.localUtil.Format(AV17RegrasContagem_Data_To1, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vREGRASCONTAGEM_RESPONSAVEL1", StringUtil.RTrim( AV32RegrasContagem_Responsavel1));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR2", AV19DynamicFiltersSelector2);
         GxWebStd.gx_hidden_field( context, "GXH_vREGRASCONTAGEM_REGRA2", AV33RegrasContagem_Regra2);
         GxWebStd.gx_hidden_field( context, "GXH_vREGRASCONTAGEM_DATA2", context.localUtil.Format(AV20RegrasContagem_Data2, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vREGRASCONTAGEM_DATA_TO2", context.localUtil.Format(AV21RegrasContagem_Data_To2, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vREGRASCONTAGEM_RESPONSAVEL2", StringUtil.RTrim( AV34RegrasContagem_Responsavel2));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR3", AV23DynamicFiltersSelector3);
         GxWebStd.gx_hidden_field( context, "GXH_vREGRASCONTAGEM_REGRA3", AV35RegrasContagem_Regra3);
         GxWebStd.gx_hidden_field( context, "GXH_vREGRASCONTAGEM_DATA3", context.localUtil.Format(AV24RegrasContagem_Data3, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vREGRASCONTAGEM_DATA_TO3", context.localUtil.Format(AV25RegrasContagem_Data_To3, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vREGRASCONTAGEM_RESPONSAVEL3", StringUtil.RTrim( AV36RegrasContagem_Responsavel3));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED2", StringUtil.BoolToStr( AV18DynamicFiltersEnabled2));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED3", StringUtil.BoolToStr( AV22DynamicFiltersEnabled3));
         GxWebStd.gx_hidden_field( context, "GXH_vTFREGRASCONTAGEM_REGRA", AV41TFRegrasContagem_Regra);
         GxWebStd.gx_hidden_field( context, "GXH_vTFREGRASCONTAGEM_REGRA_SEL", AV42TFRegrasContagem_Regra_Sel);
         GxWebStd.gx_hidden_field( context, "GXH_vTFREGRASCONTAGEM_DATA", context.localUtil.Format(AV45TFRegrasContagem_Data, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vTFREGRASCONTAGEM_DATA_TO", context.localUtil.Format(AV46TFRegrasContagem_Data_To, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vTFREGRASCONTAGEM_VALIDADE", context.localUtil.Format(AV51TFRegrasContagem_Validade, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vTFREGRASCONTAGEM_VALIDADE_TO", context.localUtil.Format(AV52TFRegrasContagem_Validade_To, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vTFREGRASCONTAGEM_RESPONSAVEL", StringUtil.RTrim( AV57TFRegrasContagem_Responsavel));
         GxWebStd.gx_hidden_field( context, "GXH_vTFREGRASCONTAGEM_RESPONSAVEL_SEL", StringUtil.RTrim( AV58TFRegrasContagem_Responsavel_Sel));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_106", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_106), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV62GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV63GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vDDO_TITLESETTINGSICONS", AV60DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vDDO_TITLESETTINGSICONS", AV60DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vREGRASCONTAGEM_REGRATITLEFILTERDATA", AV40RegrasContagem_RegraTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vREGRASCONTAGEM_REGRATITLEFILTERDATA", AV40RegrasContagem_RegraTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vREGRASCONTAGEM_DATATITLEFILTERDATA", AV44RegrasContagem_DataTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vREGRASCONTAGEM_DATATITLEFILTERDATA", AV44RegrasContagem_DataTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vREGRASCONTAGEM_VALIDADETITLEFILTERDATA", AV50RegrasContagem_ValidadeTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vREGRASCONTAGEM_VALIDADETITLEFILTERDATA", AV50RegrasContagem_ValidadeTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vREGRASCONTAGEM_RESPONSAVELTITLEFILTERDATA", AV56RegrasContagem_ResponsavelTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vREGRASCONTAGEM_RESPONSAVELTITLEFILTERDATA", AV56RegrasContagem_ResponsavelTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vWWPCONTEXT", AV6WWPContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vWWPCONTEXT", AV6WWPContext);
         }
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV94Pgmname));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vGRIDSTATE", AV10GridState);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vGRIDSTATE", AV10GridState);
         }
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSIGNOREFIRST", AV27DynamicFiltersIgnoreFirst);
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSREMOVING", AV26DynamicFiltersRemoving);
         GxWebStd.gx_hidden_field( context, "REGRASCONTAGEM_DESCRICAO", A864RegrasContagem_Descricao);
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, "DDO_REGRASCONTAGEM_REGRA_Caption", StringUtil.RTrim( Ddo_regrascontagem_regra_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_REGRASCONTAGEM_REGRA_Tooltip", StringUtil.RTrim( Ddo_regrascontagem_regra_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_REGRASCONTAGEM_REGRA_Cls", StringUtil.RTrim( Ddo_regrascontagem_regra_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_REGRASCONTAGEM_REGRA_Filteredtext_set", StringUtil.RTrim( Ddo_regrascontagem_regra_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_REGRASCONTAGEM_REGRA_Selectedvalue_set", StringUtil.RTrim( Ddo_regrascontagem_regra_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_REGRASCONTAGEM_REGRA_Dropdownoptionstype", StringUtil.RTrim( Ddo_regrascontagem_regra_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_REGRASCONTAGEM_REGRA_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_regrascontagem_regra_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_REGRASCONTAGEM_REGRA_Includesortasc", StringUtil.BoolToStr( Ddo_regrascontagem_regra_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_REGRASCONTAGEM_REGRA_Includesortdsc", StringUtil.BoolToStr( Ddo_regrascontagem_regra_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_REGRASCONTAGEM_REGRA_Sortedstatus", StringUtil.RTrim( Ddo_regrascontagem_regra_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_REGRASCONTAGEM_REGRA_Includefilter", StringUtil.BoolToStr( Ddo_regrascontagem_regra_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_REGRASCONTAGEM_REGRA_Filtertype", StringUtil.RTrim( Ddo_regrascontagem_regra_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_REGRASCONTAGEM_REGRA_Filterisrange", StringUtil.BoolToStr( Ddo_regrascontagem_regra_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_REGRASCONTAGEM_REGRA_Includedatalist", StringUtil.BoolToStr( Ddo_regrascontagem_regra_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_REGRASCONTAGEM_REGRA_Datalisttype", StringUtil.RTrim( Ddo_regrascontagem_regra_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_REGRASCONTAGEM_REGRA_Datalistproc", StringUtil.RTrim( Ddo_regrascontagem_regra_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_REGRASCONTAGEM_REGRA_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_regrascontagem_regra_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_REGRASCONTAGEM_REGRA_Sortasc", StringUtil.RTrim( Ddo_regrascontagem_regra_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_REGRASCONTAGEM_REGRA_Sortdsc", StringUtil.RTrim( Ddo_regrascontagem_regra_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_REGRASCONTAGEM_REGRA_Loadingdata", StringUtil.RTrim( Ddo_regrascontagem_regra_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_REGRASCONTAGEM_REGRA_Cleanfilter", StringUtil.RTrim( Ddo_regrascontagem_regra_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_REGRASCONTAGEM_REGRA_Noresultsfound", StringUtil.RTrim( Ddo_regrascontagem_regra_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_REGRASCONTAGEM_REGRA_Searchbuttontext", StringUtil.RTrim( Ddo_regrascontagem_regra_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_REGRASCONTAGEM_DATA_Caption", StringUtil.RTrim( Ddo_regrascontagem_data_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_REGRASCONTAGEM_DATA_Tooltip", StringUtil.RTrim( Ddo_regrascontagem_data_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_REGRASCONTAGEM_DATA_Cls", StringUtil.RTrim( Ddo_regrascontagem_data_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_REGRASCONTAGEM_DATA_Filteredtext_set", StringUtil.RTrim( Ddo_regrascontagem_data_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_REGRASCONTAGEM_DATA_Filteredtextto_set", StringUtil.RTrim( Ddo_regrascontagem_data_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_REGRASCONTAGEM_DATA_Dropdownoptionstype", StringUtil.RTrim( Ddo_regrascontagem_data_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_REGRASCONTAGEM_DATA_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_regrascontagem_data_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_REGRASCONTAGEM_DATA_Includesortasc", StringUtil.BoolToStr( Ddo_regrascontagem_data_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_REGRASCONTAGEM_DATA_Includesortdsc", StringUtil.BoolToStr( Ddo_regrascontagem_data_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_REGRASCONTAGEM_DATA_Sortedstatus", StringUtil.RTrim( Ddo_regrascontagem_data_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_REGRASCONTAGEM_DATA_Includefilter", StringUtil.BoolToStr( Ddo_regrascontagem_data_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_REGRASCONTAGEM_DATA_Filtertype", StringUtil.RTrim( Ddo_regrascontagem_data_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_REGRASCONTAGEM_DATA_Filterisrange", StringUtil.BoolToStr( Ddo_regrascontagem_data_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_REGRASCONTAGEM_DATA_Includedatalist", StringUtil.BoolToStr( Ddo_regrascontagem_data_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_REGRASCONTAGEM_DATA_Sortasc", StringUtil.RTrim( Ddo_regrascontagem_data_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_REGRASCONTAGEM_DATA_Sortdsc", StringUtil.RTrim( Ddo_regrascontagem_data_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_REGRASCONTAGEM_DATA_Cleanfilter", StringUtil.RTrim( Ddo_regrascontagem_data_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_REGRASCONTAGEM_DATA_Rangefilterfrom", StringUtil.RTrim( Ddo_regrascontagem_data_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_REGRASCONTAGEM_DATA_Rangefilterto", StringUtil.RTrim( Ddo_regrascontagem_data_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_REGRASCONTAGEM_DATA_Searchbuttontext", StringUtil.RTrim( Ddo_regrascontagem_data_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_REGRASCONTAGEM_VALIDADE_Caption", StringUtil.RTrim( Ddo_regrascontagem_validade_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_REGRASCONTAGEM_VALIDADE_Tooltip", StringUtil.RTrim( Ddo_regrascontagem_validade_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_REGRASCONTAGEM_VALIDADE_Cls", StringUtil.RTrim( Ddo_regrascontagem_validade_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_REGRASCONTAGEM_VALIDADE_Filteredtext_set", StringUtil.RTrim( Ddo_regrascontagem_validade_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_REGRASCONTAGEM_VALIDADE_Filteredtextto_set", StringUtil.RTrim( Ddo_regrascontagem_validade_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, "DDO_REGRASCONTAGEM_VALIDADE_Dropdownoptionstype", StringUtil.RTrim( Ddo_regrascontagem_validade_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_REGRASCONTAGEM_VALIDADE_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_regrascontagem_validade_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_REGRASCONTAGEM_VALIDADE_Includesortasc", StringUtil.BoolToStr( Ddo_regrascontagem_validade_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_REGRASCONTAGEM_VALIDADE_Includesortdsc", StringUtil.BoolToStr( Ddo_regrascontagem_validade_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_REGRASCONTAGEM_VALIDADE_Sortedstatus", StringUtil.RTrim( Ddo_regrascontagem_validade_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_REGRASCONTAGEM_VALIDADE_Includefilter", StringUtil.BoolToStr( Ddo_regrascontagem_validade_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_REGRASCONTAGEM_VALIDADE_Filtertype", StringUtil.RTrim( Ddo_regrascontagem_validade_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_REGRASCONTAGEM_VALIDADE_Filterisrange", StringUtil.BoolToStr( Ddo_regrascontagem_validade_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_REGRASCONTAGEM_VALIDADE_Includedatalist", StringUtil.BoolToStr( Ddo_regrascontagem_validade_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_REGRASCONTAGEM_VALIDADE_Sortasc", StringUtil.RTrim( Ddo_regrascontagem_validade_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_REGRASCONTAGEM_VALIDADE_Sortdsc", StringUtil.RTrim( Ddo_regrascontagem_validade_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_REGRASCONTAGEM_VALIDADE_Cleanfilter", StringUtil.RTrim( Ddo_regrascontagem_validade_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_REGRASCONTAGEM_VALIDADE_Rangefilterfrom", StringUtil.RTrim( Ddo_regrascontagem_validade_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, "DDO_REGRASCONTAGEM_VALIDADE_Rangefilterto", StringUtil.RTrim( Ddo_regrascontagem_validade_Rangefilterto));
         GxWebStd.gx_hidden_field( context, "DDO_REGRASCONTAGEM_VALIDADE_Searchbuttontext", StringUtil.RTrim( Ddo_regrascontagem_validade_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_REGRASCONTAGEM_RESPONSAVEL_Caption", StringUtil.RTrim( Ddo_regrascontagem_responsavel_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_REGRASCONTAGEM_RESPONSAVEL_Tooltip", StringUtil.RTrim( Ddo_regrascontagem_responsavel_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_REGRASCONTAGEM_RESPONSAVEL_Cls", StringUtil.RTrim( Ddo_regrascontagem_responsavel_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_REGRASCONTAGEM_RESPONSAVEL_Filteredtext_set", StringUtil.RTrim( Ddo_regrascontagem_responsavel_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_REGRASCONTAGEM_RESPONSAVEL_Selectedvalue_set", StringUtil.RTrim( Ddo_regrascontagem_responsavel_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_REGRASCONTAGEM_RESPONSAVEL_Dropdownoptionstype", StringUtil.RTrim( Ddo_regrascontagem_responsavel_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_REGRASCONTAGEM_RESPONSAVEL_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_regrascontagem_responsavel_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_REGRASCONTAGEM_RESPONSAVEL_Includesortasc", StringUtil.BoolToStr( Ddo_regrascontagem_responsavel_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_REGRASCONTAGEM_RESPONSAVEL_Includesortdsc", StringUtil.BoolToStr( Ddo_regrascontagem_responsavel_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_REGRASCONTAGEM_RESPONSAVEL_Sortedstatus", StringUtil.RTrim( Ddo_regrascontagem_responsavel_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_REGRASCONTAGEM_RESPONSAVEL_Includefilter", StringUtil.BoolToStr( Ddo_regrascontagem_responsavel_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_REGRASCONTAGEM_RESPONSAVEL_Filtertype", StringUtil.RTrim( Ddo_regrascontagem_responsavel_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_REGRASCONTAGEM_RESPONSAVEL_Filterisrange", StringUtil.BoolToStr( Ddo_regrascontagem_responsavel_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_REGRASCONTAGEM_RESPONSAVEL_Includedatalist", StringUtil.BoolToStr( Ddo_regrascontagem_responsavel_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_REGRASCONTAGEM_RESPONSAVEL_Datalisttype", StringUtil.RTrim( Ddo_regrascontagem_responsavel_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_REGRASCONTAGEM_RESPONSAVEL_Datalistproc", StringUtil.RTrim( Ddo_regrascontagem_responsavel_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_REGRASCONTAGEM_RESPONSAVEL_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_regrascontagem_responsavel_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_REGRASCONTAGEM_RESPONSAVEL_Sortasc", StringUtil.RTrim( Ddo_regrascontagem_responsavel_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_REGRASCONTAGEM_RESPONSAVEL_Sortdsc", StringUtil.RTrim( Ddo_regrascontagem_responsavel_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_REGRASCONTAGEM_RESPONSAVEL_Loadingdata", StringUtil.RTrim( Ddo_regrascontagem_responsavel_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_REGRASCONTAGEM_RESPONSAVEL_Cleanfilter", StringUtil.RTrim( Ddo_regrascontagem_responsavel_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_REGRASCONTAGEM_RESPONSAVEL_Noresultsfound", StringUtil.RTrim( Ddo_regrascontagem_responsavel_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_REGRASCONTAGEM_RESPONSAVEL_Searchbuttontext", StringUtil.RTrim( Ddo_regrascontagem_responsavel_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, "DDO_REGRASCONTAGEM_REGRA_Activeeventkey", StringUtil.RTrim( Ddo_regrascontagem_regra_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_REGRASCONTAGEM_REGRA_Filteredtext_get", StringUtil.RTrim( Ddo_regrascontagem_regra_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_REGRASCONTAGEM_REGRA_Selectedvalue_get", StringUtil.RTrim( Ddo_regrascontagem_regra_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_REGRASCONTAGEM_DATA_Activeeventkey", StringUtil.RTrim( Ddo_regrascontagem_data_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_REGRASCONTAGEM_DATA_Filteredtext_get", StringUtil.RTrim( Ddo_regrascontagem_data_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_REGRASCONTAGEM_DATA_Filteredtextto_get", StringUtil.RTrim( Ddo_regrascontagem_data_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_REGRASCONTAGEM_VALIDADE_Activeeventkey", StringUtil.RTrim( Ddo_regrascontagem_validade_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_REGRASCONTAGEM_VALIDADE_Filteredtext_get", StringUtil.RTrim( Ddo_regrascontagem_validade_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_REGRASCONTAGEM_VALIDADE_Filteredtextto_get", StringUtil.RTrim( Ddo_regrascontagem_validade_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, "DDO_REGRASCONTAGEM_RESPONSAVEL_Activeeventkey", StringUtil.RTrim( Ddo_regrascontagem_responsavel_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_REGRASCONTAGEM_RESPONSAVEL_Filteredtext_get", StringUtil.RTrim( Ddo_regrascontagem_responsavel_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_REGRASCONTAGEM_RESPONSAVEL_Selectedvalue_get", StringUtil.RTrim( Ddo_regrascontagem_responsavel_Selectedvalue_get));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WEFC2( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVTFC2( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return false ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wwregrascontagem.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "WWRegrasContagem" ;
      }

      public override String GetPgmdesc( )
      {
         return " Regras de Contagem" ;
      }

      protected void WBFC0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            wb_table1_2_FC2( true) ;
         }
         else
         {
            wb_table1_2_FC2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_FC2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 117,'',false,'" + sGXsfl_106_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled2_Internalname, StringUtil.BoolToStr( AV18DynamicFiltersEnabled2), "", "", chkavDynamicfiltersenabled2.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(117, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,117);\"");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 118,'',false,'" + sGXsfl_106_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled3_Internalname, StringUtil.BoolToStr( AV22DynamicFiltersEnabled3), "", "", chkavDynamicfiltersenabled3.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(118, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,118);\"");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 119,'',false,'" + sGXsfl_106_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfregrascontagem_regra_Internalname, AV41TFRegrasContagem_Regra, StringUtil.RTrim( context.localUtil.Format( AV41TFRegrasContagem_Regra, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,119);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfregrascontagem_regra_Jsonclick, 0, "Attribute", "", "", "", edtavTfregrascontagem_regra_Visible, 1, 0, "text", "", 570, "px", 1, "row", 255, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWRegrasContagem.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 120,'',false,'" + sGXsfl_106_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfregrascontagem_regra_sel_Internalname, AV42TFRegrasContagem_Regra_Sel, StringUtil.RTrim( context.localUtil.Format( AV42TFRegrasContagem_Regra_Sel, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,120);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfregrascontagem_regra_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfregrascontagem_regra_sel_Visible, 1, 0, "text", "", 570, "px", 1, "row", 255, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWRegrasContagem.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 121,'',false,'" + sGXsfl_106_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTfregrascontagem_data_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTfregrascontagem_data_Internalname, context.localUtil.Format(AV45TFRegrasContagem_Data, "99/99/99"), context.localUtil.Format( AV45TFRegrasContagem_Data, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,121);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfregrascontagem_data_Jsonclick, 0, "Attribute", "", "", "", edtavTfregrascontagem_data_Visible, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWRegrasContagem.htm");
            GxWebStd.gx_bitmap( context, edtavTfregrascontagem_data_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTfregrascontagem_data_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWRegrasContagem.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 122,'',false,'" + sGXsfl_106_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTfregrascontagem_data_to_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTfregrascontagem_data_to_Internalname, context.localUtil.Format(AV46TFRegrasContagem_Data_To, "99/99/99"), context.localUtil.Format( AV46TFRegrasContagem_Data_To, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,122);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfregrascontagem_data_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfregrascontagem_data_to_Visible, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWRegrasContagem.htm");
            GxWebStd.gx_bitmap( context, edtavTfregrascontagem_data_to_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTfregrascontagem_data_to_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWRegrasContagem.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divDdo_regrascontagem_dataauxdates_Internalname, 1, 0, "px", 0, "px", "Invisible", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 124,'',false,'" + sGXsfl_106_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_regrascontagem_dataauxdate_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_regrascontagem_dataauxdate_Internalname, context.localUtil.Format(AV47DDO_RegrasContagem_DataAuxDate, "99/99/99"), context.localUtil.Format( AV47DDO_RegrasContagem_DataAuxDate, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,124);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_regrascontagem_dataauxdate_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWRegrasContagem.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_regrascontagem_dataauxdate_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWRegrasContagem.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 125,'',false,'" + sGXsfl_106_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_regrascontagem_dataauxdateto_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_regrascontagem_dataauxdateto_Internalname, context.localUtil.Format(AV48DDO_RegrasContagem_DataAuxDateTo, "99/99/99"), context.localUtil.Format( AV48DDO_RegrasContagem_DataAuxDateTo, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,125);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_regrascontagem_dataauxdateto_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWRegrasContagem.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_regrascontagem_dataauxdateto_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWRegrasContagem.htm");
            context.WriteHtmlTextNl( "</div>") ;
            GxWebStd.gx_div_end( context, "left", "top");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 126,'',false,'" + sGXsfl_106_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTfregrascontagem_validade_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTfregrascontagem_validade_Internalname, context.localUtil.Format(AV51TFRegrasContagem_Validade, "99/99/99"), context.localUtil.Format( AV51TFRegrasContagem_Validade, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,126);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfregrascontagem_validade_Jsonclick, 0, "Attribute", "", "", "", edtavTfregrascontagem_validade_Visible, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWRegrasContagem.htm");
            GxWebStd.gx_bitmap( context, edtavTfregrascontagem_validade_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTfregrascontagem_validade_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWRegrasContagem.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 127,'',false,'" + sGXsfl_106_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTfregrascontagem_validade_to_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTfregrascontagem_validade_to_Internalname, context.localUtil.Format(AV52TFRegrasContagem_Validade_To, "99/99/99"), context.localUtil.Format( AV52TFRegrasContagem_Validade_To, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,127);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfregrascontagem_validade_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfregrascontagem_validade_to_Visible, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWRegrasContagem.htm");
            GxWebStd.gx_bitmap( context, edtavTfregrascontagem_validade_to_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTfregrascontagem_validade_to_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWRegrasContagem.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divDdo_regrascontagem_validadeauxdates_Internalname, 1, 0, "px", 0, "px", "Invisible", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 129,'',false,'" + sGXsfl_106_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_regrascontagem_validadeauxdate_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_regrascontagem_validadeauxdate_Internalname, context.localUtil.Format(AV53DDO_RegrasContagem_ValidadeAuxDate, "99/99/99"), context.localUtil.Format( AV53DDO_RegrasContagem_ValidadeAuxDate, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,129);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_regrascontagem_validadeauxdate_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWRegrasContagem.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_regrascontagem_validadeauxdate_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWRegrasContagem.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 130,'',false,'" + sGXsfl_106_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_regrascontagem_validadeauxdateto_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_regrascontagem_validadeauxdateto_Internalname, context.localUtil.Format(AV54DDO_RegrasContagem_ValidadeAuxDateTo, "99/99/99"), context.localUtil.Format( AV54DDO_RegrasContagem_ValidadeAuxDateTo, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,130);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_regrascontagem_validadeauxdateto_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWRegrasContagem.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_regrascontagem_validadeauxdateto_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWRegrasContagem.htm");
            context.WriteHtmlTextNl( "</div>") ;
            GxWebStd.gx_div_end( context, "left", "top");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 131,'',false,'" + sGXsfl_106_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfregrascontagem_responsavel_Internalname, StringUtil.RTrim( AV57TFRegrasContagem_Responsavel), StringUtil.RTrim( context.localUtil.Format( AV57TFRegrasContagem_Responsavel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,131);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfregrascontagem_responsavel_Jsonclick, 0, "Attribute", "", "", "", edtavTfregrascontagem_responsavel_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWRegrasContagem.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 132,'',false,'" + sGXsfl_106_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfregrascontagem_responsavel_sel_Internalname, StringUtil.RTrim( AV58TFRegrasContagem_Responsavel_Sel), StringUtil.RTrim( context.localUtil.Format( AV58TFRegrasContagem_Responsavel_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,132);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfregrascontagem_responsavel_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfregrascontagem_responsavel_sel_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWRegrasContagem.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_REGRASCONTAGEM_REGRAContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 134,'',false,'" + sGXsfl_106_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_regrascontagem_regratitlecontrolidtoreplace_Internalname, AV43ddo_RegrasContagem_RegraTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,134);\"", 0, edtavDdo_regrascontagem_regratitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWRegrasContagem.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_REGRASCONTAGEM_DATAContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 136,'',false,'" + sGXsfl_106_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_regrascontagem_datatitlecontrolidtoreplace_Internalname, AV49ddo_RegrasContagem_DataTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,136);\"", 0, edtavDdo_regrascontagem_datatitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWRegrasContagem.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_REGRASCONTAGEM_VALIDADEContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 138,'',false,'" + sGXsfl_106_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_regrascontagem_validadetitlecontrolidtoreplace_Internalname, AV55ddo_RegrasContagem_ValidadeTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,138);\"", 0, edtavDdo_regrascontagem_validadetitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWRegrasContagem.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_REGRASCONTAGEM_RESPONSAVELContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 140,'',false,'" + sGXsfl_106_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_regrascontagem_responsaveltitlecontrolidtoreplace_Internalname, AV59ddo_RegrasContagem_ResponsavelTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,140);\"", 0, edtavDdo_regrascontagem_responsaveltitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWRegrasContagem.htm");
         }
         wbLoad = true;
      }

      protected void STARTFC2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", " Regras de Contagem", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPFC0( ) ;
      }

      protected void WSFC2( )
      {
         STARTFC2( ) ;
         EVTFC2( ) ;
      }

      protected void EVTFC2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E11FC2 */
                              E11FC2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_REGRASCONTAGEM_REGRA.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E12FC2 */
                              E12FC2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_REGRASCONTAGEM_DATA.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E13FC2 */
                              E13FC2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_REGRASCONTAGEM_VALIDADE.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E14FC2 */
                              E14FC2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_REGRASCONTAGEM_RESPONSAVEL.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E15FC2 */
                              E15FC2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E16FC2 */
                              E16FC2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VORDEREDBY.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E17FC2 */
                              E17FC2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS1'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E18FC2 */
                              E18FC2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS2'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E19FC2 */
                              E19FC2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS3'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E20FC2 */
                              E20FC2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOCLEANFILTERS'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E21FC2 */
                              E21FC2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOINSERT'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E22FC2 */
                              E22FC2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS1'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E23FC2 */
                              E23FC2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR1.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E24FC2 */
                              E24FC2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS2'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E25FC2 */
                              E25FC2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR2.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E26FC2 */
                              E26FC2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR3.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E27FC2 */
                              E27FC2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              nGXsfl_106_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_106_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_106_idx), 4, 0)), 4, "0");
                              SubsflControlProps_1062( ) ;
                              AV28Update = cgiGet( edtavUpdate_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUpdate_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV28Update)) ? AV92Update_GXI : context.convertURL( context.PathToRelativeUrl( AV28Update))));
                              AV29Delete = cgiGet( edtavDelete_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDelete_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV29Delete)) ? AV93Delete_GXI : context.convertURL( context.PathToRelativeUrl( AV29Delete))));
                              A860RegrasContagem_Regra = cgiGet( edtRegrasContagem_Regra_Internalname);
                              A861RegrasContagem_Data = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtRegrasContagem_Data_Internalname), 0));
                              A862RegrasContagem_Validade = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtRegrasContagem_Validade_Internalname), 0));
                              n862RegrasContagem_Validade = false;
                              A863RegrasContagem_Responsavel = StringUtil.Upper( cgiGet( edtRegrasContagem_Responsavel_Internalname));
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E28FC2 */
                                    E28FC2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E29FC2 */
                                    E29FC2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       /* Set Refresh If Orderedby Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Ordereddsc Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Regrascontagem_regra1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vREGRASCONTAGEM_REGRA1"), AV31RegrasContagem_Regra1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Regrascontagem_data1 Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vREGRASCONTAGEM_DATA1"), 0) != AV16RegrasContagem_Data1 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Regrascontagem_data_to1 Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vREGRASCONTAGEM_DATA_TO1"), 0) != AV17RegrasContagem_Data_To1 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Regrascontagem_responsavel1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vREGRASCONTAGEM_RESPONSAVEL1"), AV32RegrasContagem_Responsavel1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV19DynamicFiltersSelector2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Regrascontagem_regra2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vREGRASCONTAGEM_REGRA2"), AV33RegrasContagem_Regra2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Regrascontagem_data2 Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vREGRASCONTAGEM_DATA2"), 0) != AV20RegrasContagem_Data2 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Regrascontagem_data_to2 Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vREGRASCONTAGEM_DATA_TO2"), 0) != AV21RegrasContagem_Data_To2 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Regrascontagem_responsavel2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vREGRASCONTAGEM_RESPONSAVEL2"), AV34RegrasContagem_Responsavel2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector3 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV23DynamicFiltersSelector3) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Regrascontagem_regra3 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vREGRASCONTAGEM_REGRA3"), AV35RegrasContagem_Regra3) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Regrascontagem_data3 Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vREGRASCONTAGEM_DATA3"), 0) != AV24RegrasContagem_Data3 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Regrascontagem_data_to3 Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vREGRASCONTAGEM_DATA_TO3"), 0) != AV25RegrasContagem_Data_To3 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Regrascontagem_responsavel3 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vREGRASCONTAGEM_RESPONSAVEL3"), AV36RegrasContagem_Responsavel3) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersenabled2 Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV18DynamicFiltersEnabled2 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersenabled3 Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV22DynamicFiltersEnabled3 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfregrascontagem_regra Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFREGRASCONTAGEM_REGRA"), AV41TFRegrasContagem_Regra) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfregrascontagem_regra_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFREGRASCONTAGEM_REGRA_SEL"), AV42TFRegrasContagem_Regra_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfregrascontagem_data Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vTFREGRASCONTAGEM_DATA"), 0) != AV45TFRegrasContagem_Data )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfregrascontagem_data_to Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vTFREGRASCONTAGEM_DATA_TO"), 0) != AV46TFRegrasContagem_Data_To )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfregrascontagem_validade Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vTFREGRASCONTAGEM_VALIDADE"), 0) != AV51TFRegrasContagem_Validade )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfregrascontagem_validade_to Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vTFREGRASCONTAGEM_VALIDADE_TO"), 0) != AV52TFRegrasContagem_Validade_To )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfregrascontagem_responsavel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFREGRASCONTAGEM_RESPONSAVEL"), AV57TFRegrasContagem_Responsavel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfregrascontagem_responsavel_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFREGRASCONTAGEM_RESPONSAVEL_SEL"), AV58TFRegrasContagem_Responsavel_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEFC2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PAFC2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbavOrderedby.Name = "vORDEREDBY";
            cmbavOrderedby.WebTags = "";
            if ( cmbavOrderedby.ItemCount > 0 )
            {
               AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            }
            cmbavDynamicfiltersselector1.Name = "vDYNAMICFILTERSSELECTOR1";
            cmbavDynamicfiltersselector1.WebTags = "";
            cmbavDynamicfiltersselector1.addItem("REGRASCONTAGEM_REGRA", "Regra", 0);
            cmbavDynamicfiltersselector1.addItem("REGRASCONTAGEM_DATA", "Data", 0);
            cmbavDynamicfiltersselector1.addItem("REGRASCONTAGEM_RESPONSAVEL", "Respons�vel", 0);
            if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
            {
               AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            }
            cmbavDynamicfiltersselector2.Name = "vDYNAMICFILTERSSELECTOR2";
            cmbavDynamicfiltersselector2.WebTags = "";
            cmbavDynamicfiltersselector2.addItem("REGRASCONTAGEM_REGRA", "Regra", 0);
            cmbavDynamicfiltersselector2.addItem("REGRASCONTAGEM_DATA", "Data", 0);
            cmbavDynamicfiltersselector2.addItem("REGRASCONTAGEM_RESPONSAVEL", "Respons�vel", 0);
            if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
            {
               AV19DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV19DynamicFiltersSelector2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
            }
            cmbavDynamicfiltersselector3.Name = "vDYNAMICFILTERSSELECTOR3";
            cmbavDynamicfiltersselector3.WebTags = "";
            cmbavDynamicfiltersselector3.addItem("REGRASCONTAGEM_REGRA", "Regra", 0);
            cmbavDynamicfiltersselector3.addItem("REGRASCONTAGEM_DATA", "Data", 0);
            cmbavDynamicfiltersselector3.addItem("REGRASCONTAGEM_RESPONSAVEL", "Respons�vel", 0);
            if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
            {
               AV23DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV23DynamicFiltersSelector3);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
            }
            chkavDynamicfiltersenabled2.Name = "vDYNAMICFILTERSENABLED2";
            chkavDynamicfiltersenabled2.WebTags = "";
            chkavDynamicfiltersenabled2.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "TitleCaption", chkavDynamicfiltersenabled2.Caption);
            chkavDynamicfiltersenabled2.CheckedValue = "false";
            chkavDynamicfiltersenabled3.Name = "vDYNAMICFILTERSENABLED3";
            chkavDynamicfiltersenabled3.WebTags = "";
            chkavDynamicfiltersenabled3.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "TitleCaption", chkavDynamicfiltersenabled3.Caption);
            chkavDynamicfiltersenabled3.CheckedValue = "false";
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_1062( ) ;
         while ( nGXsfl_106_idx <= nRC_GXsfl_106 )
         {
            sendrow_1062( ) ;
            nGXsfl_106_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_106_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_106_idx+1));
            sGXsfl_106_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_106_idx), 4, 0)), 4, "0");
            SubsflControlProps_1062( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV13OrderedBy ,
                                       bool AV14OrderedDsc ,
                                       String AV15DynamicFiltersSelector1 ,
                                       String AV31RegrasContagem_Regra1 ,
                                       DateTime AV16RegrasContagem_Data1 ,
                                       DateTime AV17RegrasContagem_Data_To1 ,
                                       String AV32RegrasContagem_Responsavel1 ,
                                       String AV19DynamicFiltersSelector2 ,
                                       String AV33RegrasContagem_Regra2 ,
                                       DateTime AV20RegrasContagem_Data2 ,
                                       DateTime AV21RegrasContagem_Data_To2 ,
                                       String AV34RegrasContagem_Responsavel2 ,
                                       String AV23DynamicFiltersSelector3 ,
                                       String AV35RegrasContagem_Regra3 ,
                                       DateTime AV24RegrasContagem_Data3 ,
                                       DateTime AV25RegrasContagem_Data_To3 ,
                                       String AV36RegrasContagem_Responsavel3 ,
                                       bool AV18DynamicFiltersEnabled2 ,
                                       bool AV22DynamicFiltersEnabled3 ,
                                       String AV41TFRegrasContagem_Regra ,
                                       String AV42TFRegrasContagem_Regra_Sel ,
                                       DateTime AV45TFRegrasContagem_Data ,
                                       DateTime AV46TFRegrasContagem_Data_To ,
                                       DateTime AV51TFRegrasContagem_Validade ,
                                       DateTime AV52TFRegrasContagem_Validade_To ,
                                       String AV57TFRegrasContagem_Responsavel ,
                                       String AV58TFRegrasContagem_Responsavel_Sel ,
                                       String AV43ddo_RegrasContagem_RegraTitleControlIdToReplace ,
                                       String AV49ddo_RegrasContagem_DataTitleControlIdToReplace ,
                                       String AV55ddo_RegrasContagem_ValidadeTitleControlIdToReplace ,
                                       String AV59ddo_RegrasContagem_ResponsavelTitleControlIdToReplace ,
                                       int AV37RegrasContagem_AreaTrabalhoCod ,
                                       wwpbaseobjects.SdtWWPContext AV6WWPContext ,
                                       String AV94Pgmname ,
                                       wwpbaseobjects.SdtWWPGridState AV10GridState ,
                                       bool AV27DynamicFiltersIgnoreFirst ,
                                       bool AV26DynamicFiltersRemoving ,
                                       String A860RegrasContagem_Regra ,
                                       String A864RegrasContagem_Descricao )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RFFC2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_REGRASCONTAGEM_REGRA", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A860RegrasContagem_Regra, ""))));
         GxWebStd.gx_hidden_field( context, "REGRASCONTAGEM_REGRA", A860RegrasContagem_Regra);
         GxWebStd.gx_hidden_field( context, "gxhash_REGRASCONTAGEM_DATA", GetSecureSignedToken( "", A861RegrasContagem_Data));
         GxWebStd.gx_hidden_field( context, "REGRASCONTAGEM_DATA", context.localUtil.Format(A861RegrasContagem_Data, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "gxhash_REGRASCONTAGEM_VALIDADE", GetSecureSignedToken( "", A862RegrasContagem_Validade));
         GxWebStd.gx_hidden_field( context, "REGRASCONTAGEM_VALIDADE", context.localUtil.Format(A862RegrasContagem_Validade, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "gxhash_REGRASCONTAGEM_RESPONSAVEL", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A863RegrasContagem_Responsavel, "@!"))));
         GxWebStd.gx_hidden_field( context, "REGRASCONTAGEM_RESPONSAVEL", StringUtil.RTrim( A863RegrasContagem_Responsavel));
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavOrderedby.ItemCount > 0 )
         {
            AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
         {
            AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         }
         if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
         {
            AV19DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV19DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         }
         if ( cmbavDynamicfiltersselector3.ItemCount > 0 )
         {
            AV23DynamicFiltersSelector3 = cmbavDynamicfiltersselector3.getValidValue(AV23DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFFC2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV94Pgmname = "WWRegrasContagem";
         context.Gx_err = 0;
      }

      protected void RFFC2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 106;
         /* Execute user event: E16FC2 */
         E16FC2 ();
         nGXsfl_106_idx = 1;
         sGXsfl_106_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_106_idx), 4, 0)), 4, "0");
         SubsflControlProps_1062( ) ;
         nGXsfl_106_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", "");
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_1062( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 AV67WWRegrasContagemDS_2_Dynamicfiltersselector1 ,
                                                 AV68WWRegrasContagemDS_3_Regrascontagem_regra1 ,
                                                 AV69WWRegrasContagemDS_4_Regrascontagem_data1 ,
                                                 AV70WWRegrasContagemDS_5_Regrascontagem_data_to1 ,
                                                 AV71WWRegrasContagemDS_6_Regrascontagem_responsavel1 ,
                                                 AV72WWRegrasContagemDS_7_Dynamicfiltersenabled2 ,
                                                 AV73WWRegrasContagemDS_8_Dynamicfiltersselector2 ,
                                                 AV74WWRegrasContagemDS_9_Regrascontagem_regra2 ,
                                                 AV75WWRegrasContagemDS_10_Regrascontagem_data2 ,
                                                 AV76WWRegrasContagemDS_11_Regrascontagem_data_to2 ,
                                                 AV77WWRegrasContagemDS_12_Regrascontagem_responsavel2 ,
                                                 AV78WWRegrasContagemDS_13_Dynamicfiltersenabled3 ,
                                                 AV79WWRegrasContagemDS_14_Dynamicfiltersselector3 ,
                                                 AV80WWRegrasContagemDS_15_Regrascontagem_regra3 ,
                                                 AV81WWRegrasContagemDS_16_Regrascontagem_data3 ,
                                                 AV82WWRegrasContagemDS_17_Regrascontagem_data_to3 ,
                                                 AV83WWRegrasContagemDS_18_Regrascontagem_responsavel3 ,
                                                 AV85WWRegrasContagemDS_20_Tfregrascontagem_regra_sel ,
                                                 AV84WWRegrasContagemDS_19_Tfregrascontagem_regra ,
                                                 AV86WWRegrasContagemDS_21_Tfregrascontagem_data ,
                                                 AV87WWRegrasContagemDS_22_Tfregrascontagem_data_to ,
                                                 AV88WWRegrasContagemDS_23_Tfregrascontagem_validade ,
                                                 AV89WWRegrasContagemDS_24_Tfregrascontagem_validade_to ,
                                                 AV91WWRegrasContagemDS_26_Tfregrascontagem_responsavel_sel ,
                                                 AV90WWRegrasContagemDS_25_Tfregrascontagem_responsavel ,
                                                 A860RegrasContagem_Regra ,
                                                 A861RegrasContagem_Data ,
                                                 A863RegrasContagem_Responsavel ,
                                                 A862RegrasContagem_Validade ,
                                                 AV13OrderedBy ,
                                                 AV14OrderedDsc ,
                                                 A865RegrasContagem_AreaTrabalhoCod ,
                                                 AV6WWPContext.gxTpr_Areatrabalho_codigo },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE,
                                                 TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE,
                                                 TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.BOOLEAN,
                                                 TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT
                                                 }
            });
            lV68WWRegrasContagemDS_3_Regrascontagem_regra1 = StringUtil.Concat( StringUtil.RTrim( AV68WWRegrasContagemDS_3_Regrascontagem_regra1), "%", "");
            lV71WWRegrasContagemDS_6_Regrascontagem_responsavel1 = StringUtil.PadR( StringUtil.RTrim( AV71WWRegrasContagemDS_6_Regrascontagem_responsavel1), 50, "%");
            lV74WWRegrasContagemDS_9_Regrascontagem_regra2 = StringUtil.Concat( StringUtil.RTrim( AV74WWRegrasContagemDS_9_Regrascontagem_regra2), "%", "");
            lV77WWRegrasContagemDS_12_Regrascontagem_responsavel2 = StringUtil.PadR( StringUtil.RTrim( AV77WWRegrasContagemDS_12_Regrascontagem_responsavel2), 50, "%");
            lV80WWRegrasContagemDS_15_Regrascontagem_regra3 = StringUtil.Concat( StringUtil.RTrim( AV80WWRegrasContagemDS_15_Regrascontagem_regra3), "%", "");
            lV83WWRegrasContagemDS_18_Regrascontagem_responsavel3 = StringUtil.PadR( StringUtil.RTrim( AV83WWRegrasContagemDS_18_Regrascontagem_responsavel3), 50, "%");
            lV84WWRegrasContagemDS_19_Tfregrascontagem_regra = StringUtil.Concat( StringUtil.RTrim( AV84WWRegrasContagemDS_19_Tfregrascontagem_regra), "%", "");
            lV90WWRegrasContagemDS_25_Tfregrascontagem_responsavel = StringUtil.PadR( StringUtil.RTrim( AV90WWRegrasContagemDS_25_Tfregrascontagem_responsavel), 50, "%");
            /* Using cursor H00FC2 */
            pr_default.execute(0, new Object[] {AV6WWPContext.gxTpr_Areatrabalho_codigo, lV68WWRegrasContagemDS_3_Regrascontagem_regra1, AV69WWRegrasContagemDS_4_Regrascontagem_data1, AV70WWRegrasContagemDS_5_Regrascontagem_data_to1, lV71WWRegrasContagemDS_6_Regrascontagem_responsavel1, lV74WWRegrasContagemDS_9_Regrascontagem_regra2, AV75WWRegrasContagemDS_10_Regrascontagem_data2, AV76WWRegrasContagemDS_11_Regrascontagem_data_to2, lV77WWRegrasContagemDS_12_Regrascontagem_responsavel2, lV80WWRegrasContagemDS_15_Regrascontagem_regra3, AV81WWRegrasContagemDS_16_Regrascontagem_data3, AV82WWRegrasContagemDS_17_Regrascontagem_data_to3, lV83WWRegrasContagemDS_18_Regrascontagem_responsavel3, lV84WWRegrasContagemDS_19_Tfregrascontagem_regra, AV85WWRegrasContagemDS_20_Tfregrascontagem_regra_sel, AV86WWRegrasContagemDS_21_Tfregrascontagem_data, AV87WWRegrasContagemDS_22_Tfregrascontagem_data_to, AV88WWRegrasContagemDS_23_Tfregrascontagem_validade, AV89WWRegrasContagemDS_24_Tfregrascontagem_validade_to, lV90WWRegrasContagemDS_25_Tfregrascontagem_responsavel, AV91WWRegrasContagemDS_26_Tfregrascontagem_responsavel_sel, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_106_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A865RegrasContagem_AreaTrabalhoCod = H00FC2_A865RegrasContagem_AreaTrabalhoCod[0];
               A864RegrasContagem_Descricao = H00FC2_A864RegrasContagem_Descricao[0];
               A863RegrasContagem_Responsavel = H00FC2_A863RegrasContagem_Responsavel[0];
               A862RegrasContagem_Validade = H00FC2_A862RegrasContagem_Validade[0];
               n862RegrasContagem_Validade = H00FC2_n862RegrasContagem_Validade[0];
               A861RegrasContagem_Data = H00FC2_A861RegrasContagem_Data[0];
               A860RegrasContagem_Regra = H00FC2_A860RegrasContagem_Regra[0];
               /* Execute user event: E29FC2 */
               E29FC2 ();
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 106;
            WBFC0( ) ;
         }
         nGXsfl_106_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         AV66WWRegrasContagemDS_1_Regrascontagem_areatrabalhocod = AV37RegrasContagem_AreaTrabalhoCod;
         AV67WWRegrasContagemDS_2_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV68WWRegrasContagemDS_3_Regrascontagem_regra1 = AV31RegrasContagem_Regra1;
         AV69WWRegrasContagemDS_4_Regrascontagem_data1 = AV16RegrasContagem_Data1;
         AV70WWRegrasContagemDS_5_Regrascontagem_data_to1 = AV17RegrasContagem_Data_To1;
         AV71WWRegrasContagemDS_6_Regrascontagem_responsavel1 = AV32RegrasContagem_Responsavel1;
         AV72WWRegrasContagemDS_7_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV73WWRegrasContagemDS_8_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV74WWRegrasContagemDS_9_Regrascontagem_regra2 = AV33RegrasContagem_Regra2;
         AV75WWRegrasContagemDS_10_Regrascontagem_data2 = AV20RegrasContagem_Data2;
         AV76WWRegrasContagemDS_11_Regrascontagem_data_to2 = AV21RegrasContagem_Data_To2;
         AV77WWRegrasContagemDS_12_Regrascontagem_responsavel2 = AV34RegrasContagem_Responsavel2;
         AV78WWRegrasContagemDS_13_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV79WWRegrasContagemDS_14_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV80WWRegrasContagemDS_15_Regrascontagem_regra3 = AV35RegrasContagem_Regra3;
         AV81WWRegrasContagemDS_16_Regrascontagem_data3 = AV24RegrasContagem_Data3;
         AV82WWRegrasContagemDS_17_Regrascontagem_data_to3 = AV25RegrasContagem_Data_To3;
         AV83WWRegrasContagemDS_18_Regrascontagem_responsavel3 = AV36RegrasContagem_Responsavel3;
         AV84WWRegrasContagemDS_19_Tfregrascontagem_regra = AV41TFRegrasContagem_Regra;
         AV85WWRegrasContagemDS_20_Tfregrascontagem_regra_sel = AV42TFRegrasContagem_Regra_Sel;
         AV86WWRegrasContagemDS_21_Tfregrascontagem_data = AV45TFRegrasContagem_Data;
         AV87WWRegrasContagemDS_22_Tfregrascontagem_data_to = AV46TFRegrasContagem_Data_To;
         AV88WWRegrasContagemDS_23_Tfregrascontagem_validade = AV51TFRegrasContagem_Validade;
         AV89WWRegrasContagemDS_24_Tfregrascontagem_validade_to = AV52TFRegrasContagem_Validade_To;
         AV90WWRegrasContagemDS_25_Tfregrascontagem_responsavel = AV57TFRegrasContagem_Responsavel;
         AV91WWRegrasContagemDS_26_Tfregrascontagem_responsavel_sel = AV58TFRegrasContagem_Responsavel_Sel;
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV67WWRegrasContagemDS_2_Dynamicfiltersselector1 ,
                                              AV68WWRegrasContagemDS_3_Regrascontagem_regra1 ,
                                              AV69WWRegrasContagemDS_4_Regrascontagem_data1 ,
                                              AV70WWRegrasContagemDS_5_Regrascontagem_data_to1 ,
                                              AV71WWRegrasContagemDS_6_Regrascontagem_responsavel1 ,
                                              AV72WWRegrasContagemDS_7_Dynamicfiltersenabled2 ,
                                              AV73WWRegrasContagemDS_8_Dynamicfiltersselector2 ,
                                              AV74WWRegrasContagemDS_9_Regrascontagem_regra2 ,
                                              AV75WWRegrasContagemDS_10_Regrascontagem_data2 ,
                                              AV76WWRegrasContagemDS_11_Regrascontagem_data_to2 ,
                                              AV77WWRegrasContagemDS_12_Regrascontagem_responsavel2 ,
                                              AV78WWRegrasContagemDS_13_Dynamicfiltersenabled3 ,
                                              AV79WWRegrasContagemDS_14_Dynamicfiltersselector3 ,
                                              AV80WWRegrasContagemDS_15_Regrascontagem_regra3 ,
                                              AV81WWRegrasContagemDS_16_Regrascontagem_data3 ,
                                              AV82WWRegrasContagemDS_17_Regrascontagem_data_to3 ,
                                              AV83WWRegrasContagemDS_18_Regrascontagem_responsavel3 ,
                                              AV85WWRegrasContagemDS_20_Tfregrascontagem_regra_sel ,
                                              AV84WWRegrasContagemDS_19_Tfregrascontagem_regra ,
                                              AV86WWRegrasContagemDS_21_Tfregrascontagem_data ,
                                              AV87WWRegrasContagemDS_22_Tfregrascontagem_data_to ,
                                              AV88WWRegrasContagemDS_23_Tfregrascontagem_validade ,
                                              AV89WWRegrasContagemDS_24_Tfregrascontagem_validade_to ,
                                              AV91WWRegrasContagemDS_26_Tfregrascontagem_responsavel_sel ,
                                              AV90WWRegrasContagemDS_25_Tfregrascontagem_responsavel ,
                                              A860RegrasContagem_Regra ,
                                              A861RegrasContagem_Data ,
                                              A863RegrasContagem_Responsavel ,
                                              A862RegrasContagem_Validade ,
                                              AV13OrderedBy ,
                                              AV14OrderedDsc ,
                                              A865RegrasContagem_AreaTrabalhoCod ,
                                              AV6WWPContext.gxTpr_Areatrabalho_codigo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE,
                                              TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.BOOLEAN,
                                              TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT
                                              }
         });
         lV68WWRegrasContagemDS_3_Regrascontagem_regra1 = StringUtil.Concat( StringUtil.RTrim( AV68WWRegrasContagemDS_3_Regrascontagem_regra1), "%", "");
         lV71WWRegrasContagemDS_6_Regrascontagem_responsavel1 = StringUtil.PadR( StringUtil.RTrim( AV71WWRegrasContagemDS_6_Regrascontagem_responsavel1), 50, "%");
         lV74WWRegrasContagemDS_9_Regrascontagem_regra2 = StringUtil.Concat( StringUtil.RTrim( AV74WWRegrasContagemDS_9_Regrascontagem_regra2), "%", "");
         lV77WWRegrasContagemDS_12_Regrascontagem_responsavel2 = StringUtil.PadR( StringUtil.RTrim( AV77WWRegrasContagemDS_12_Regrascontagem_responsavel2), 50, "%");
         lV80WWRegrasContagemDS_15_Regrascontagem_regra3 = StringUtil.Concat( StringUtil.RTrim( AV80WWRegrasContagemDS_15_Regrascontagem_regra3), "%", "");
         lV83WWRegrasContagemDS_18_Regrascontagem_responsavel3 = StringUtil.PadR( StringUtil.RTrim( AV83WWRegrasContagemDS_18_Regrascontagem_responsavel3), 50, "%");
         lV84WWRegrasContagemDS_19_Tfregrascontagem_regra = StringUtil.Concat( StringUtil.RTrim( AV84WWRegrasContagemDS_19_Tfregrascontagem_regra), "%", "");
         lV90WWRegrasContagemDS_25_Tfregrascontagem_responsavel = StringUtil.PadR( StringUtil.RTrim( AV90WWRegrasContagemDS_25_Tfregrascontagem_responsavel), 50, "%");
         /* Using cursor H00FC3 */
         pr_default.execute(1, new Object[] {AV6WWPContext.gxTpr_Areatrabalho_codigo, lV68WWRegrasContagemDS_3_Regrascontagem_regra1, AV69WWRegrasContagemDS_4_Regrascontagem_data1, AV70WWRegrasContagemDS_5_Regrascontagem_data_to1, lV71WWRegrasContagemDS_6_Regrascontagem_responsavel1, lV74WWRegrasContagemDS_9_Regrascontagem_regra2, AV75WWRegrasContagemDS_10_Regrascontagem_data2, AV76WWRegrasContagemDS_11_Regrascontagem_data_to2, lV77WWRegrasContagemDS_12_Regrascontagem_responsavel2, lV80WWRegrasContagemDS_15_Regrascontagem_regra3, AV81WWRegrasContagemDS_16_Regrascontagem_data3, AV82WWRegrasContagemDS_17_Regrascontagem_data_to3, lV83WWRegrasContagemDS_18_Regrascontagem_responsavel3, lV84WWRegrasContagemDS_19_Tfregrascontagem_regra, AV85WWRegrasContagemDS_20_Tfregrascontagem_regra_sel, AV86WWRegrasContagemDS_21_Tfregrascontagem_data, AV87WWRegrasContagemDS_22_Tfregrascontagem_data_to, AV88WWRegrasContagemDS_23_Tfregrascontagem_validade, AV89WWRegrasContagemDS_24_Tfregrascontagem_validade_to, lV90WWRegrasContagemDS_25_Tfregrascontagem_responsavel, AV91WWRegrasContagemDS_26_Tfregrascontagem_responsavel_sel});
         GRID_nRecordCount = H00FC3_AGRID_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         AV66WWRegrasContagemDS_1_Regrascontagem_areatrabalhocod = AV37RegrasContagem_AreaTrabalhoCod;
         AV67WWRegrasContagemDS_2_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV68WWRegrasContagemDS_3_Regrascontagem_regra1 = AV31RegrasContagem_Regra1;
         AV69WWRegrasContagemDS_4_Regrascontagem_data1 = AV16RegrasContagem_Data1;
         AV70WWRegrasContagemDS_5_Regrascontagem_data_to1 = AV17RegrasContagem_Data_To1;
         AV71WWRegrasContagemDS_6_Regrascontagem_responsavel1 = AV32RegrasContagem_Responsavel1;
         AV72WWRegrasContagemDS_7_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV73WWRegrasContagemDS_8_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV74WWRegrasContagemDS_9_Regrascontagem_regra2 = AV33RegrasContagem_Regra2;
         AV75WWRegrasContagemDS_10_Regrascontagem_data2 = AV20RegrasContagem_Data2;
         AV76WWRegrasContagemDS_11_Regrascontagem_data_to2 = AV21RegrasContagem_Data_To2;
         AV77WWRegrasContagemDS_12_Regrascontagem_responsavel2 = AV34RegrasContagem_Responsavel2;
         AV78WWRegrasContagemDS_13_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV79WWRegrasContagemDS_14_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV80WWRegrasContagemDS_15_Regrascontagem_regra3 = AV35RegrasContagem_Regra3;
         AV81WWRegrasContagemDS_16_Regrascontagem_data3 = AV24RegrasContagem_Data3;
         AV82WWRegrasContagemDS_17_Regrascontagem_data_to3 = AV25RegrasContagem_Data_To3;
         AV83WWRegrasContagemDS_18_Regrascontagem_responsavel3 = AV36RegrasContagem_Responsavel3;
         AV84WWRegrasContagemDS_19_Tfregrascontagem_regra = AV41TFRegrasContagem_Regra;
         AV85WWRegrasContagemDS_20_Tfregrascontagem_regra_sel = AV42TFRegrasContagem_Regra_Sel;
         AV86WWRegrasContagemDS_21_Tfregrascontagem_data = AV45TFRegrasContagem_Data;
         AV87WWRegrasContagemDS_22_Tfregrascontagem_data_to = AV46TFRegrasContagem_Data_To;
         AV88WWRegrasContagemDS_23_Tfregrascontagem_validade = AV51TFRegrasContagem_Validade;
         AV89WWRegrasContagemDS_24_Tfregrascontagem_validade_to = AV52TFRegrasContagem_Validade_To;
         AV90WWRegrasContagemDS_25_Tfregrascontagem_responsavel = AV57TFRegrasContagem_Responsavel;
         AV91WWRegrasContagemDS_26_Tfregrascontagem_responsavel_sel = AV58TFRegrasContagem_Responsavel_Sel;
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV31RegrasContagem_Regra1, AV16RegrasContagem_Data1, AV17RegrasContagem_Data_To1, AV32RegrasContagem_Responsavel1, AV19DynamicFiltersSelector2, AV33RegrasContagem_Regra2, AV20RegrasContagem_Data2, AV21RegrasContagem_Data_To2, AV34RegrasContagem_Responsavel2, AV23DynamicFiltersSelector3, AV35RegrasContagem_Regra3, AV24RegrasContagem_Data3, AV25RegrasContagem_Data_To3, AV36RegrasContagem_Responsavel3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV41TFRegrasContagem_Regra, AV42TFRegrasContagem_Regra_Sel, AV45TFRegrasContagem_Data, AV46TFRegrasContagem_Data_To, AV51TFRegrasContagem_Validade, AV52TFRegrasContagem_Validade_To, AV57TFRegrasContagem_Responsavel, AV58TFRegrasContagem_Responsavel_Sel, AV43ddo_RegrasContagem_RegraTitleControlIdToReplace, AV49ddo_RegrasContagem_DataTitleControlIdToReplace, AV55ddo_RegrasContagem_ValidadeTitleControlIdToReplace, AV59ddo_RegrasContagem_ResponsavelTitleControlIdToReplace, AV37RegrasContagem_AreaTrabalhoCod, AV6WWPContext, AV94Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A860RegrasContagem_Regra, A864RegrasContagem_Descricao) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         AV66WWRegrasContagemDS_1_Regrascontagem_areatrabalhocod = AV37RegrasContagem_AreaTrabalhoCod;
         AV67WWRegrasContagemDS_2_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV68WWRegrasContagemDS_3_Regrascontagem_regra1 = AV31RegrasContagem_Regra1;
         AV69WWRegrasContagemDS_4_Regrascontagem_data1 = AV16RegrasContagem_Data1;
         AV70WWRegrasContagemDS_5_Regrascontagem_data_to1 = AV17RegrasContagem_Data_To1;
         AV71WWRegrasContagemDS_6_Regrascontagem_responsavel1 = AV32RegrasContagem_Responsavel1;
         AV72WWRegrasContagemDS_7_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV73WWRegrasContagemDS_8_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV74WWRegrasContagemDS_9_Regrascontagem_regra2 = AV33RegrasContagem_Regra2;
         AV75WWRegrasContagemDS_10_Regrascontagem_data2 = AV20RegrasContagem_Data2;
         AV76WWRegrasContagemDS_11_Regrascontagem_data_to2 = AV21RegrasContagem_Data_To2;
         AV77WWRegrasContagemDS_12_Regrascontagem_responsavel2 = AV34RegrasContagem_Responsavel2;
         AV78WWRegrasContagemDS_13_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV79WWRegrasContagemDS_14_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV80WWRegrasContagemDS_15_Regrascontagem_regra3 = AV35RegrasContagem_Regra3;
         AV81WWRegrasContagemDS_16_Regrascontagem_data3 = AV24RegrasContagem_Data3;
         AV82WWRegrasContagemDS_17_Regrascontagem_data_to3 = AV25RegrasContagem_Data_To3;
         AV83WWRegrasContagemDS_18_Regrascontagem_responsavel3 = AV36RegrasContagem_Responsavel3;
         AV84WWRegrasContagemDS_19_Tfregrascontagem_regra = AV41TFRegrasContagem_Regra;
         AV85WWRegrasContagemDS_20_Tfregrascontagem_regra_sel = AV42TFRegrasContagem_Regra_Sel;
         AV86WWRegrasContagemDS_21_Tfregrascontagem_data = AV45TFRegrasContagem_Data;
         AV87WWRegrasContagemDS_22_Tfregrascontagem_data_to = AV46TFRegrasContagem_Data_To;
         AV88WWRegrasContagemDS_23_Tfregrascontagem_validade = AV51TFRegrasContagem_Validade;
         AV89WWRegrasContagemDS_24_Tfregrascontagem_validade_to = AV52TFRegrasContagem_Validade_To;
         AV90WWRegrasContagemDS_25_Tfregrascontagem_responsavel = AV57TFRegrasContagem_Responsavel;
         AV91WWRegrasContagemDS_26_Tfregrascontagem_responsavel_sel = AV58TFRegrasContagem_Responsavel_Sel;
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV31RegrasContagem_Regra1, AV16RegrasContagem_Data1, AV17RegrasContagem_Data_To1, AV32RegrasContagem_Responsavel1, AV19DynamicFiltersSelector2, AV33RegrasContagem_Regra2, AV20RegrasContagem_Data2, AV21RegrasContagem_Data_To2, AV34RegrasContagem_Responsavel2, AV23DynamicFiltersSelector3, AV35RegrasContagem_Regra3, AV24RegrasContagem_Data3, AV25RegrasContagem_Data_To3, AV36RegrasContagem_Responsavel3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV41TFRegrasContagem_Regra, AV42TFRegrasContagem_Regra_Sel, AV45TFRegrasContagem_Data, AV46TFRegrasContagem_Data_To, AV51TFRegrasContagem_Validade, AV52TFRegrasContagem_Validade_To, AV57TFRegrasContagem_Responsavel, AV58TFRegrasContagem_Responsavel_Sel, AV43ddo_RegrasContagem_RegraTitleControlIdToReplace, AV49ddo_RegrasContagem_DataTitleControlIdToReplace, AV55ddo_RegrasContagem_ValidadeTitleControlIdToReplace, AV59ddo_RegrasContagem_ResponsavelTitleControlIdToReplace, AV37RegrasContagem_AreaTrabalhoCod, AV6WWPContext, AV94Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A860RegrasContagem_Regra, A864RegrasContagem_Descricao) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         AV66WWRegrasContagemDS_1_Regrascontagem_areatrabalhocod = AV37RegrasContagem_AreaTrabalhoCod;
         AV67WWRegrasContagemDS_2_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV68WWRegrasContagemDS_3_Regrascontagem_regra1 = AV31RegrasContagem_Regra1;
         AV69WWRegrasContagemDS_4_Regrascontagem_data1 = AV16RegrasContagem_Data1;
         AV70WWRegrasContagemDS_5_Regrascontagem_data_to1 = AV17RegrasContagem_Data_To1;
         AV71WWRegrasContagemDS_6_Regrascontagem_responsavel1 = AV32RegrasContagem_Responsavel1;
         AV72WWRegrasContagemDS_7_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV73WWRegrasContagemDS_8_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV74WWRegrasContagemDS_9_Regrascontagem_regra2 = AV33RegrasContagem_Regra2;
         AV75WWRegrasContagemDS_10_Regrascontagem_data2 = AV20RegrasContagem_Data2;
         AV76WWRegrasContagemDS_11_Regrascontagem_data_to2 = AV21RegrasContagem_Data_To2;
         AV77WWRegrasContagemDS_12_Regrascontagem_responsavel2 = AV34RegrasContagem_Responsavel2;
         AV78WWRegrasContagemDS_13_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV79WWRegrasContagemDS_14_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV80WWRegrasContagemDS_15_Regrascontagem_regra3 = AV35RegrasContagem_Regra3;
         AV81WWRegrasContagemDS_16_Regrascontagem_data3 = AV24RegrasContagem_Data3;
         AV82WWRegrasContagemDS_17_Regrascontagem_data_to3 = AV25RegrasContagem_Data_To3;
         AV83WWRegrasContagemDS_18_Regrascontagem_responsavel3 = AV36RegrasContagem_Responsavel3;
         AV84WWRegrasContagemDS_19_Tfregrascontagem_regra = AV41TFRegrasContagem_Regra;
         AV85WWRegrasContagemDS_20_Tfregrascontagem_regra_sel = AV42TFRegrasContagem_Regra_Sel;
         AV86WWRegrasContagemDS_21_Tfregrascontagem_data = AV45TFRegrasContagem_Data;
         AV87WWRegrasContagemDS_22_Tfregrascontagem_data_to = AV46TFRegrasContagem_Data_To;
         AV88WWRegrasContagemDS_23_Tfregrascontagem_validade = AV51TFRegrasContagem_Validade;
         AV89WWRegrasContagemDS_24_Tfregrascontagem_validade_to = AV52TFRegrasContagem_Validade_To;
         AV90WWRegrasContagemDS_25_Tfregrascontagem_responsavel = AV57TFRegrasContagem_Responsavel;
         AV91WWRegrasContagemDS_26_Tfregrascontagem_responsavel_sel = AV58TFRegrasContagem_Responsavel_Sel;
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV31RegrasContagem_Regra1, AV16RegrasContagem_Data1, AV17RegrasContagem_Data_To1, AV32RegrasContagem_Responsavel1, AV19DynamicFiltersSelector2, AV33RegrasContagem_Regra2, AV20RegrasContagem_Data2, AV21RegrasContagem_Data_To2, AV34RegrasContagem_Responsavel2, AV23DynamicFiltersSelector3, AV35RegrasContagem_Regra3, AV24RegrasContagem_Data3, AV25RegrasContagem_Data_To3, AV36RegrasContagem_Responsavel3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV41TFRegrasContagem_Regra, AV42TFRegrasContagem_Regra_Sel, AV45TFRegrasContagem_Data, AV46TFRegrasContagem_Data_To, AV51TFRegrasContagem_Validade, AV52TFRegrasContagem_Validade_To, AV57TFRegrasContagem_Responsavel, AV58TFRegrasContagem_Responsavel_Sel, AV43ddo_RegrasContagem_RegraTitleControlIdToReplace, AV49ddo_RegrasContagem_DataTitleControlIdToReplace, AV55ddo_RegrasContagem_ValidadeTitleControlIdToReplace, AV59ddo_RegrasContagem_ResponsavelTitleControlIdToReplace, AV37RegrasContagem_AreaTrabalhoCod, AV6WWPContext, AV94Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A860RegrasContagem_Regra, A864RegrasContagem_Descricao) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         AV66WWRegrasContagemDS_1_Regrascontagem_areatrabalhocod = AV37RegrasContagem_AreaTrabalhoCod;
         AV67WWRegrasContagemDS_2_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV68WWRegrasContagemDS_3_Regrascontagem_regra1 = AV31RegrasContagem_Regra1;
         AV69WWRegrasContagemDS_4_Regrascontagem_data1 = AV16RegrasContagem_Data1;
         AV70WWRegrasContagemDS_5_Regrascontagem_data_to1 = AV17RegrasContagem_Data_To1;
         AV71WWRegrasContagemDS_6_Regrascontagem_responsavel1 = AV32RegrasContagem_Responsavel1;
         AV72WWRegrasContagemDS_7_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV73WWRegrasContagemDS_8_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV74WWRegrasContagemDS_9_Regrascontagem_regra2 = AV33RegrasContagem_Regra2;
         AV75WWRegrasContagemDS_10_Regrascontagem_data2 = AV20RegrasContagem_Data2;
         AV76WWRegrasContagemDS_11_Regrascontagem_data_to2 = AV21RegrasContagem_Data_To2;
         AV77WWRegrasContagemDS_12_Regrascontagem_responsavel2 = AV34RegrasContagem_Responsavel2;
         AV78WWRegrasContagemDS_13_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV79WWRegrasContagemDS_14_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV80WWRegrasContagemDS_15_Regrascontagem_regra3 = AV35RegrasContagem_Regra3;
         AV81WWRegrasContagemDS_16_Regrascontagem_data3 = AV24RegrasContagem_Data3;
         AV82WWRegrasContagemDS_17_Regrascontagem_data_to3 = AV25RegrasContagem_Data_To3;
         AV83WWRegrasContagemDS_18_Regrascontagem_responsavel3 = AV36RegrasContagem_Responsavel3;
         AV84WWRegrasContagemDS_19_Tfregrascontagem_regra = AV41TFRegrasContagem_Regra;
         AV85WWRegrasContagemDS_20_Tfregrascontagem_regra_sel = AV42TFRegrasContagem_Regra_Sel;
         AV86WWRegrasContagemDS_21_Tfregrascontagem_data = AV45TFRegrasContagem_Data;
         AV87WWRegrasContagemDS_22_Tfregrascontagem_data_to = AV46TFRegrasContagem_Data_To;
         AV88WWRegrasContagemDS_23_Tfregrascontagem_validade = AV51TFRegrasContagem_Validade;
         AV89WWRegrasContagemDS_24_Tfregrascontagem_validade_to = AV52TFRegrasContagem_Validade_To;
         AV90WWRegrasContagemDS_25_Tfregrascontagem_responsavel = AV57TFRegrasContagem_Responsavel;
         AV91WWRegrasContagemDS_26_Tfregrascontagem_responsavel_sel = AV58TFRegrasContagem_Responsavel_Sel;
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV31RegrasContagem_Regra1, AV16RegrasContagem_Data1, AV17RegrasContagem_Data_To1, AV32RegrasContagem_Responsavel1, AV19DynamicFiltersSelector2, AV33RegrasContagem_Regra2, AV20RegrasContagem_Data2, AV21RegrasContagem_Data_To2, AV34RegrasContagem_Responsavel2, AV23DynamicFiltersSelector3, AV35RegrasContagem_Regra3, AV24RegrasContagem_Data3, AV25RegrasContagem_Data_To3, AV36RegrasContagem_Responsavel3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV41TFRegrasContagem_Regra, AV42TFRegrasContagem_Regra_Sel, AV45TFRegrasContagem_Data, AV46TFRegrasContagem_Data_To, AV51TFRegrasContagem_Validade, AV52TFRegrasContagem_Validade_To, AV57TFRegrasContagem_Responsavel, AV58TFRegrasContagem_Responsavel_Sel, AV43ddo_RegrasContagem_RegraTitleControlIdToReplace, AV49ddo_RegrasContagem_DataTitleControlIdToReplace, AV55ddo_RegrasContagem_ValidadeTitleControlIdToReplace, AV59ddo_RegrasContagem_ResponsavelTitleControlIdToReplace, AV37RegrasContagem_AreaTrabalhoCod, AV6WWPContext, AV94Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A860RegrasContagem_Regra, A864RegrasContagem_Descricao) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         AV66WWRegrasContagemDS_1_Regrascontagem_areatrabalhocod = AV37RegrasContagem_AreaTrabalhoCod;
         AV67WWRegrasContagemDS_2_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV68WWRegrasContagemDS_3_Regrascontagem_regra1 = AV31RegrasContagem_Regra1;
         AV69WWRegrasContagemDS_4_Regrascontagem_data1 = AV16RegrasContagem_Data1;
         AV70WWRegrasContagemDS_5_Regrascontagem_data_to1 = AV17RegrasContagem_Data_To1;
         AV71WWRegrasContagemDS_6_Regrascontagem_responsavel1 = AV32RegrasContagem_Responsavel1;
         AV72WWRegrasContagemDS_7_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV73WWRegrasContagemDS_8_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV74WWRegrasContagemDS_9_Regrascontagem_regra2 = AV33RegrasContagem_Regra2;
         AV75WWRegrasContagemDS_10_Regrascontagem_data2 = AV20RegrasContagem_Data2;
         AV76WWRegrasContagemDS_11_Regrascontagem_data_to2 = AV21RegrasContagem_Data_To2;
         AV77WWRegrasContagemDS_12_Regrascontagem_responsavel2 = AV34RegrasContagem_Responsavel2;
         AV78WWRegrasContagemDS_13_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV79WWRegrasContagemDS_14_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV80WWRegrasContagemDS_15_Regrascontagem_regra3 = AV35RegrasContagem_Regra3;
         AV81WWRegrasContagemDS_16_Regrascontagem_data3 = AV24RegrasContagem_Data3;
         AV82WWRegrasContagemDS_17_Regrascontagem_data_to3 = AV25RegrasContagem_Data_To3;
         AV83WWRegrasContagemDS_18_Regrascontagem_responsavel3 = AV36RegrasContagem_Responsavel3;
         AV84WWRegrasContagemDS_19_Tfregrascontagem_regra = AV41TFRegrasContagem_Regra;
         AV85WWRegrasContagemDS_20_Tfregrascontagem_regra_sel = AV42TFRegrasContagem_Regra_Sel;
         AV86WWRegrasContagemDS_21_Tfregrascontagem_data = AV45TFRegrasContagem_Data;
         AV87WWRegrasContagemDS_22_Tfregrascontagem_data_to = AV46TFRegrasContagem_Data_To;
         AV88WWRegrasContagemDS_23_Tfregrascontagem_validade = AV51TFRegrasContagem_Validade;
         AV89WWRegrasContagemDS_24_Tfregrascontagem_validade_to = AV52TFRegrasContagem_Validade_To;
         AV90WWRegrasContagemDS_25_Tfregrascontagem_responsavel = AV57TFRegrasContagem_Responsavel;
         AV91WWRegrasContagemDS_26_Tfregrascontagem_responsavel_sel = AV58TFRegrasContagem_Responsavel_Sel;
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV31RegrasContagem_Regra1, AV16RegrasContagem_Data1, AV17RegrasContagem_Data_To1, AV32RegrasContagem_Responsavel1, AV19DynamicFiltersSelector2, AV33RegrasContagem_Regra2, AV20RegrasContagem_Data2, AV21RegrasContagem_Data_To2, AV34RegrasContagem_Responsavel2, AV23DynamicFiltersSelector3, AV35RegrasContagem_Regra3, AV24RegrasContagem_Data3, AV25RegrasContagem_Data_To3, AV36RegrasContagem_Responsavel3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV41TFRegrasContagem_Regra, AV42TFRegrasContagem_Regra_Sel, AV45TFRegrasContagem_Data, AV46TFRegrasContagem_Data_To, AV51TFRegrasContagem_Validade, AV52TFRegrasContagem_Validade_To, AV57TFRegrasContagem_Responsavel, AV58TFRegrasContagem_Responsavel_Sel, AV43ddo_RegrasContagem_RegraTitleControlIdToReplace, AV49ddo_RegrasContagem_DataTitleControlIdToReplace, AV55ddo_RegrasContagem_ValidadeTitleControlIdToReplace, AV59ddo_RegrasContagem_ResponsavelTitleControlIdToReplace, AV37RegrasContagem_AreaTrabalhoCod, AV6WWPContext, AV94Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A860RegrasContagem_Regra, A864RegrasContagem_Descricao) ;
         }
         return (int)(0) ;
      }

      protected void STRUPFC0( )
      {
         /* Before Start, stand alone formulas. */
         AV94Pgmname = "WWRegrasContagem";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E28FC2 */
         E28FC2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "vDDO_TITLESETTINGSICONS"), AV60DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( "vREGRASCONTAGEM_REGRATITLEFILTERDATA"), AV40RegrasContagem_RegraTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vREGRASCONTAGEM_DATATITLEFILTERDATA"), AV44RegrasContagem_DataTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vREGRASCONTAGEM_VALIDADETITLEFILTERDATA"), AV50RegrasContagem_ValidadeTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vREGRASCONTAGEM_RESPONSAVELTITLEFILTERDATA"), AV56RegrasContagem_ResponsavelTitleFilterData);
            /* Read variables values. */
            cmbavOrderedby.Name = cmbavOrderedby_Internalname;
            cmbavOrderedby.CurrentValue = cgiGet( cmbavOrderedby_Internalname);
            AV13OrderedBy = (short)(NumberUtil.Val( cgiGet( cmbavOrderedby_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavRegrascontagem_areatrabalhocod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavRegrascontagem_areatrabalhocod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vREGRASCONTAGEM_AREATRABALHOCOD");
               GX_FocusControl = edtavRegrascontagem_areatrabalhocod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV37RegrasContagem_AreaTrabalhoCod = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37RegrasContagem_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV37RegrasContagem_AreaTrabalhoCod), 6, 0)));
            }
            else
            {
               AV37RegrasContagem_AreaTrabalhoCod = (int)(context.localUtil.CToN( cgiGet( edtavRegrascontagem_areatrabalhocod_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37RegrasContagem_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV37RegrasContagem_AreaTrabalhoCod), 6, 0)));
            }
            cmbavDynamicfiltersselector1.Name = cmbavDynamicfiltersselector1_Internalname;
            cmbavDynamicfiltersselector1.CurrentValue = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            AV15DynamicFiltersSelector1 = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            AV31RegrasContagem_Regra1 = cgiGet( edtavRegrascontagem_regra1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31RegrasContagem_Regra1", AV31RegrasContagem_Regra1);
            if ( context.localUtil.VCDateTime( cgiGet( edtavRegrascontagem_data1_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Regras Contagem_Data1"}), 1, "vREGRASCONTAGEM_DATA1");
               GX_FocusControl = edtavRegrascontagem_data1_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV16RegrasContagem_Data1 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16RegrasContagem_Data1", context.localUtil.Format(AV16RegrasContagem_Data1, "99/99/99"));
            }
            else
            {
               AV16RegrasContagem_Data1 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavRegrascontagem_data1_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16RegrasContagem_Data1", context.localUtil.Format(AV16RegrasContagem_Data1, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavRegrascontagem_data_to1_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Regras Contagem_Data_To1"}), 1, "vREGRASCONTAGEM_DATA_TO1");
               GX_FocusControl = edtavRegrascontagem_data_to1_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV17RegrasContagem_Data_To1 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17RegrasContagem_Data_To1", context.localUtil.Format(AV17RegrasContagem_Data_To1, "99/99/99"));
            }
            else
            {
               AV17RegrasContagem_Data_To1 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavRegrascontagem_data_to1_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17RegrasContagem_Data_To1", context.localUtil.Format(AV17RegrasContagem_Data_To1, "99/99/99"));
            }
            AV32RegrasContagem_Responsavel1 = StringUtil.Upper( cgiGet( edtavRegrascontagem_responsavel1_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32RegrasContagem_Responsavel1", AV32RegrasContagem_Responsavel1);
            cmbavDynamicfiltersselector2.Name = cmbavDynamicfiltersselector2_Internalname;
            cmbavDynamicfiltersselector2.CurrentValue = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            AV19DynamicFiltersSelector2 = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
            AV33RegrasContagem_Regra2 = cgiGet( edtavRegrascontagem_regra2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33RegrasContagem_Regra2", AV33RegrasContagem_Regra2);
            if ( context.localUtil.VCDateTime( cgiGet( edtavRegrascontagem_data2_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Regras Contagem_Data2"}), 1, "vREGRASCONTAGEM_DATA2");
               GX_FocusControl = edtavRegrascontagem_data2_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV20RegrasContagem_Data2 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20RegrasContagem_Data2", context.localUtil.Format(AV20RegrasContagem_Data2, "99/99/99"));
            }
            else
            {
               AV20RegrasContagem_Data2 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavRegrascontagem_data2_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20RegrasContagem_Data2", context.localUtil.Format(AV20RegrasContagem_Data2, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavRegrascontagem_data_to2_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Regras Contagem_Data_To2"}), 1, "vREGRASCONTAGEM_DATA_TO2");
               GX_FocusControl = edtavRegrascontagem_data_to2_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV21RegrasContagem_Data_To2 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21RegrasContagem_Data_To2", context.localUtil.Format(AV21RegrasContagem_Data_To2, "99/99/99"));
            }
            else
            {
               AV21RegrasContagem_Data_To2 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavRegrascontagem_data_to2_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21RegrasContagem_Data_To2", context.localUtil.Format(AV21RegrasContagem_Data_To2, "99/99/99"));
            }
            AV34RegrasContagem_Responsavel2 = StringUtil.Upper( cgiGet( edtavRegrascontagem_responsavel2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34RegrasContagem_Responsavel2", AV34RegrasContagem_Responsavel2);
            cmbavDynamicfiltersselector3.Name = cmbavDynamicfiltersselector3_Internalname;
            cmbavDynamicfiltersselector3.CurrentValue = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            AV23DynamicFiltersSelector3 = cgiGet( cmbavDynamicfiltersselector3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
            AV35RegrasContagem_Regra3 = cgiGet( edtavRegrascontagem_regra3_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35RegrasContagem_Regra3", AV35RegrasContagem_Regra3);
            if ( context.localUtil.VCDateTime( cgiGet( edtavRegrascontagem_data3_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Regras Contagem_Data3"}), 1, "vREGRASCONTAGEM_DATA3");
               GX_FocusControl = edtavRegrascontagem_data3_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV24RegrasContagem_Data3 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24RegrasContagem_Data3", context.localUtil.Format(AV24RegrasContagem_Data3, "99/99/99"));
            }
            else
            {
               AV24RegrasContagem_Data3 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavRegrascontagem_data3_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24RegrasContagem_Data3", context.localUtil.Format(AV24RegrasContagem_Data3, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavRegrascontagem_data_to3_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Regras Contagem_Data_To3"}), 1, "vREGRASCONTAGEM_DATA_TO3");
               GX_FocusControl = edtavRegrascontagem_data_to3_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV25RegrasContagem_Data_To3 = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25RegrasContagem_Data_To3", context.localUtil.Format(AV25RegrasContagem_Data_To3, "99/99/99"));
            }
            else
            {
               AV25RegrasContagem_Data_To3 = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavRegrascontagem_data_to3_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25RegrasContagem_Data_To3", context.localUtil.Format(AV25RegrasContagem_Data_To3, "99/99/99"));
            }
            AV36RegrasContagem_Responsavel3 = StringUtil.Upper( cgiGet( edtavRegrascontagem_responsavel3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36RegrasContagem_Responsavel3", AV36RegrasContagem_Responsavel3);
            AV18DynamicFiltersEnabled2 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
            AV22DynamicFiltersEnabled3 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled3_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
            AV41TFRegrasContagem_Regra = cgiGet( edtavTfregrascontagem_regra_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41TFRegrasContagem_Regra", AV41TFRegrasContagem_Regra);
            AV42TFRegrasContagem_Regra_Sel = cgiGet( edtavTfregrascontagem_regra_sel_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42TFRegrasContagem_Regra_Sel", AV42TFRegrasContagem_Regra_Sel);
            if ( context.localUtil.VCDateTime( cgiGet( edtavTfregrascontagem_data_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"TFRegras Contagem_Data"}), 1, "vTFREGRASCONTAGEM_DATA");
               GX_FocusControl = edtavTfregrascontagem_data_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV45TFRegrasContagem_Data = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45TFRegrasContagem_Data", context.localUtil.Format(AV45TFRegrasContagem_Data, "99/99/99"));
            }
            else
            {
               AV45TFRegrasContagem_Data = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavTfregrascontagem_data_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45TFRegrasContagem_Data", context.localUtil.Format(AV45TFRegrasContagem_Data, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavTfregrascontagem_data_to_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"TFRegras Contagem_Data_To"}), 1, "vTFREGRASCONTAGEM_DATA_TO");
               GX_FocusControl = edtavTfregrascontagem_data_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV46TFRegrasContagem_Data_To = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46TFRegrasContagem_Data_To", context.localUtil.Format(AV46TFRegrasContagem_Data_To, "99/99/99"));
            }
            else
            {
               AV46TFRegrasContagem_Data_To = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavTfregrascontagem_data_to_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46TFRegrasContagem_Data_To", context.localUtil.Format(AV46TFRegrasContagem_Data_To, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_regrascontagem_dataauxdate_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Regras Contagem_Data Aux Date"}), 1, "vDDO_REGRASCONTAGEM_DATAAUXDATE");
               GX_FocusControl = edtavDdo_regrascontagem_dataauxdate_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV47DDO_RegrasContagem_DataAuxDate = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47DDO_RegrasContagem_DataAuxDate", context.localUtil.Format(AV47DDO_RegrasContagem_DataAuxDate, "99/99/99"));
            }
            else
            {
               AV47DDO_RegrasContagem_DataAuxDate = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_regrascontagem_dataauxdate_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47DDO_RegrasContagem_DataAuxDate", context.localUtil.Format(AV47DDO_RegrasContagem_DataAuxDate, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_regrascontagem_dataauxdateto_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Regras Contagem_Data Aux Date To"}), 1, "vDDO_REGRASCONTAGEM_DATAAUXDATETO");
               GX_FocusControl = edtavDdo_regrascontagem_dataauxdateto_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV48DDO_RegrasContagem_DataAuxDateTo = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48DDO_RegrasContagem_DataAuxDateTo", context.localUtil.Format(AV48DDO_RegrasContagem_DataAuxDateTo, "99/99/99"));
            }
            else
            {
               AV48DDO_RegrasContagem_DataAuxDateTo = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_regrascontagem_dataauxdateto_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48DDO_RegrasContagem_DataAuxDateTo", context.localUtil.Format(AV48DDO_RegrasContagem_DataAuxDateTo, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavTfregrascontagem_validade_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"TFRegras Contagem_Validade"}), 1, "vTFREGRASCONTAGEM_VALIDADE");
               GX_FocusControl = edtavTfregrascontagem_validade_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV51TFRegrasContagem_Validade = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51TFRegrasContagem_Validade", context.localUtil.Format(AV51TFRegrasContagem_Validade, "99/99/99"));
            }
            else
            {
               AV51TFRegrasContagem_Validade = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavTfregrascontagem_validade_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51TFRegrasContagem_Validade", context.localUtil.Format(AV51TFRegrasContagem_Validade, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavTfregrascontagem_validade_to_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"TFRegras Contagem_Validade_To"}), 1, "vTFREGRASCONTAGEM_VALIDADE_TO");
               GX_FocusControl = edtavTfregrascontagem_validade_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV52TFRegrasContagem_Validade_To = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52TFRegrasContagem_Validade_To", context.localUtil.Format(AV52TFRegrasContagem_Validade_To, "99/99/99"));
            }
            else
            {
               AV52TFRegrasContagem_Validade_To = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavTfregrascontagem_validade_to_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52TFRegrasContagem_Validade_To", context.localUtil.Format(AV52TFRegrasContagem_Validade_To, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_regrascontagem_validadeauxdate_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Regras Contagem_Validade Aux Date"}), 1, "vDDO_REGRASCONTAGEM_VALIDADEAUXDATE");
               GX_FocusControl = edtavDdo_regrascontagem_validadeauxdate_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV53DDO_RegrasContagem_ValidadeAuxDate = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53DDO_RegrasContagem_ValidadeAuxDate", context.localUtil.Format(AV53DDO_RegrasContagem_ValidadeAuxDate, "99/99/99"));
            }
            else
            {
               AV53DDO_RegrasContagem_ValidadeAuxDate = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_regrascontagem_validadeauxdate_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53DDO_RegrasContagem_ValidadeAuxDate", context.localUtil.Format(AV53DDO_RegrasContagem_ValidadeAuxDate, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_regrascontagem_validadeauxdateto_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Regras Contagem_Validade Aux Date To"}), 1, "vDDO_REGRASCONTAGEM_VALIDADEAUXDATETO");
               GX_FocusControl = edtavDdo_regrascontagem_validadeauxdateto_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV54DDO_RegrasContagem_ValidadeAuxDateTo = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54DDO_RegrasContagem_ValidadeAuxDateTo", context.localUtil.Format(AV54DDO_RegrasContagem_ValidadeAuxDateTo, "99/99/99"));
            }
            else
            {
               AV54DDO_RegrasContagem_ValidadeAuxDateTo = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_regrascontagem_validadeauxdateto_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54DDO_RegrasContagem_ValidadeAuxDateTo", context.localUtil.Format(AV54DDO_RegrasContagem_ValidadeAuxDateTo, "99/99/99"));
            }
            AV57TFRegrasContagem_Responsavel = StringUtil.Upper( cgiGet( edtavTfregrascontagem_responsavel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57TFRegrasContagem_Responsavel", AV57TFRegrasContagem_Responsavel);
            AV58TFRegrasContagem_Responsavel_Sel = StringUtil.Upper( cgiGet( edtavTfregrascontagem_responsavel_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58TFRegrasContagem_Responsavel_Sel", AV58TFRegrasContagem_Responsavel_Sel);
            AV43ddo_RegrasContagem_RegraTitleControlIdToReplace = cgiGet( edtavDdo_regrascontagem_regratitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43ddo_RegrasContagem_RegraTitleControlIdToReplace", AV43ddo_RegrasContagem_RegraTitleControlIdToReplace);
            AV49ddo_RegrasContagem_DataTitleControlIdToReplace = cgiGet( edtavDdo_regrascontagem_datatitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49ddo_RegrasContagem_DataTitleControlIdToReplace", AV49ddo_RegrasContagem_DataTitleControlIdToReplace);
            AV55ddo_RegrasContagem_ValidadeTitleControlIdToReplace = cgiGet( edtavDdo_regrascontagem_validadetitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55ddo_RegrasContagem_ValidadeTitleControlIdToReplace", AV55ddo_RegrasContagem_ValidadeTitleControlIdToReplace);
            AV59ddo_RegrasContagem_ResponsavelTitleControlIdToReplace = cgiGet( edtavDdo_regrascontagem_responsaveltitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59ddo_RegrasContagem_ResponsavelTitleControlIdToReplace", AV59ddo_RegrasContagem_ResponsavelTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_106 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_106"), ",", "."));
            AV62GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( "vGRIDCURRENTPAGE"), ",", "."));
            AV63GridPageCount = (long)(context.localUtil.CToN( cgiGet( "vGRIDPAGECOUNT"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( "GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( "GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( "GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( "GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( "GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( "GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( "GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( "GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( "GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( "GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( "GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( "GRIDPAGINATIONBAR_Emptygridcaption");
            Ddo_regrascontagem_regra_Caption = cgiGet( "DDO_REGRASCONTAGEM_REGRA_Caption");
            Ddo_regrascontagem_regra_Tooltip = cgiGet( "DDO_REGRASCONTAGEM_REGRA_Tooltip");
            Ddo_regrascontagem_regra_Cls = cgiGet( "DDO_REGRASCONTAGEM_REGRA_Cls");
            Ddo_regrascontagem_regra_Filteredtext_set = cgiGet( "DDO_REGRASCONTAGEM_REGRA_Filteredtext_set");
            Ddo_regrascontagem_regra_Selectedvalue_set = cgiGet( "DDO_REGRASCONTAGEM_REGRA_Selectedvalue_set");
            Ddo_regrascontagem_regra_Dropdownoptionstype = cgiGet( "DDO_REGRASCONTAGEM_REGRA_Dropdownoptionstype");
            Ddo_regrascontagem_regra_Titlecontrolidtoreplace = cgiGet( "DDO_REGRASCONTAGEM_REGRA_Titlecontrolidtoreplace");
            Ddo_regrascontagem_regra_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_REGRASCONTAGEM_REGRA_Includesortasc"));
            Ddo_regrascontagem_regra_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_REGRASCONTAGEM_REGRA_Includesortdsc"));
            Ddo_regrascontagem_regra_Sortedstatus = cgiGet( "DDO_REGRASCONTAGEM_REGRA_Sortedstatus");
            Ddo_regrascontagem_regra_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_REGRASCONTAGEM_REGRA_Includefilter"));
            Ddo_regrascontagem_regra_Filtertype = cgiGet( "DDO_REGRASCONTAGEM_REGRA_Filtertype");
            Ddo_regrascontagem_regra_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_REGRASCONTAGEM_REGRA_Filterisrange"));
            Ddo_regrascontagem_regra_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_REGRASCONTAGEM_REGRA_Includedatalist"));
            Ddo_regrascontagem_regra_Datalisttype = cgiGet( "DDO_REGRASCONTAGEM_REGRA_Datalisttype");
            Ddo_regrascontagem_regra_Datalistproc = cgiGet( "DDO_REGRASCONTAGEM_REGRA_Datalistproc");
            Ddo_regrascontagem_regra_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_REGRASCONTAGEM_REGRA_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_regrascontagem_regra_Sortasc = cgiGet( "DDO_REGRASCONTAGEM_REGRA_Sortasc");
            Ddo_regrascontagem_regra_Sortdsc = cgiGet( "DDO_REGRASCONTAGEM_REGRA_Sortdsc");
            Ddo_regrascontagem_regra_Loadingdata = cgiGet( "DDO_REGRASCONTAGEM_REGRA_Loadingdata");
            Ddo_regrascontagem_regra_Cleanfilter = cgiGet( "DDO_REGRASCONTAGEM_REGRA_Cleanfilter");
            Ddo_regrascontagem_regra_Noresultsfound = cgiGet( "DDO_REGRASCONTAGEM_REGRA_Noresultsfound");
            Ddo_regrascontagem_regra_Searchbuttontext = cgiGet( "DDO_REGRASCONTAGEM_REGRA_Searchbuttontext");
            Ddo_regrascontagem_data_Caption = cgiGet( "DDO_REGRASCONTAGEM_DATA_Caption");
            Ddo_regrascontagem_data_Tooltip = cgiGet( "DDO_REGRASCONTAGEM_DATA_Tooltip");
            Ddo_regrascontagem_data_Cls = cgiGet( "DDO_REGRASCONTAGEM_DATA_Cls");
            Ddo_regrascontagem_data_Filteredtext_set = cgiGet( "DDO_REGRASCONTAGEM_DATA_Filteredtext_set");
            Ddo_regrascontagem_data_Filteredtextto_set = cgiGet( "DDO_REGRASCONTAGEM_DATA_Filteredtextto_set");
            Ddo_regrascontagem_data_Dropdownoptionstype = cgiGet( "DDO_REGRASCONTAGEM_DATA_Dropdownoptionstype");
            Ddo_regrascontagem_data_Titlecontrolidtoreplace = cgiGet( "DDO_REGRASCONTAGEM_DATA_Titlecontrolidtoreplace");
            Ddo_regrascontagem_data_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_REGRASCONTAGEM_DATA_Includesortasc"));
            Ddo_regrascontagem_data_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_REGRASCONTAGEM_DATA_Includesortdsc"));
            Ddo_regrascontagem_data_Sortedstatus = cgiGet( "DDO_REGRASCONTAGEM_DATA_Sortedstatus");
            Ddo_regrascontagem_data_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_REGRASCONTAGEM_DATA_Includefilter"));
            Ddo_regrascontagem_data_Filtertype = cgiGet( "DDO_REGRASCONTAGEM_DATA_Filtertype");
            Ddo_regrascontagem_data_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_REGRASCONTAGEM_DATA_Filterisrange"));
            Ddo_regrascontagem_data_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_REGRASCONTAGEM_DATA_Includedatalist"));
            Ddo_regrascontagem_data_Sortasc = cgiGet( "DDO_REGRASCONTAGEM_DATA_Sortasc");
            Ddo_regrascontagem_data_Sortdsc = cgiGet( "DDO_REGRASCONTAGEM_DATA_Sortdsc");
            Ddo_regrascontagem_data_Cleanfilter = cgiGet( "DDO_REGRASCONTAGEM_DATA_Cleanfilter");
            Ddo_regrascontagem_data_Rangefilterfrom = cgiGet( "DDO_REGRASCONTAGEM_DATA_Rangefilterfrom");
            Ddo_regrascontagem_data_Rangefilterto = cgiGet( "DDO_REGRASCONTAGEM_DATA_Rangefilterto");
            Ddo_regrascontagem_data_Searchbuttontext = cgiGet( "DDO_REGRASCONTAGEM_DATA_Searchbuttontext");
            Ddo_regrascontagem_validade_Caption = cgiGet( "DDO_REGRASCONTAGEM_VALIDADE_Caption");
            Ddo_regrascontagem_validade_Tooltip = cgiGet( "DDO_REGRASCONTAGEM_VALIDADE_Tooltip");
            Ddo_regrascontagem_validade_Cls = cgiGet( "DDO_REGRASCONTAGEM_VALIDADE_Cls");
            Ddo_regrascontagem_validade_Filteredtext_set = cgiGet( "DDO_REGRASCONTAGEM_VALIDADE_Filteredtext_set");
            Ddo_regrascontagem_validade_Filteredtextto_set = cgiGet( "DDO_REGRASCONTAGEM_VALIDADE_Filteredtextto_set");
            Ddo_regrascontagem_validade_Dropdownoptionstype = cgiGet( "DDO_REGRASCONTAGEM_VALIDADE_Dropdownoptionstype");
            Ddo_regrascontagem_validade_Titlecontrolidtoreplace = cgiGet( "DDO_REGRASCONTAGEM_VALIDADE_Titlecontrolidtoreplace");
            Ddo_regrascontagem_validade_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_REGRASCONTAGEM_VALIDADE_Includesortasc"));
            Ddo_regrascontagem_validade_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_REGRASCONTAGEM_VALIDADE_Includesortdsc"));
            Ddo_regrascontagem_validade_Sortedstatus = cgiGet( "DDO_REGRASCONTAGEM_VALIDADE_Sortedstatus");
            Ddo_regrascontagem_validade_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_REGRASCONTAGEM_VALIDADE_Includefilter"));
            Ddo_regrascontagem_validade_Filtertype = cgiGet( "DDO_REGRASCONTAGEM_VALIDADE_Filtertype");
            Ddo_regrascontagem_validade_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_REGRASCONTAGEM_VALIDADE_Filterisrange"));
            Ddo_regrascontagem_validade_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_REGRASCONTAGEM_VALIDADE_Includedatalist"));
            Ddo_regrascontagem_validade_Sortasc = cgiGet( "DDO_REGRASCONTAGEM_VALIDADE_Sortasc");
            Ddo_regrascontagem_validade_Sortdsc = cgiGet( "DDO_REGRASCONTAGEM_VALIDADE_Sortdsc");
            Ddo_regrascontagem_validade_Cleanfilter = cgiGet( "DDO_REGRASCONTAGEM_VALIDADE_Cleanfilter");
            Ddo_regrascontagem_validade_Rangefilterfrom = cgiGet( "DDO_REGRASCONTAGEM_VALIDADE_Rangefilterfrom");
            Ddo_regrascontagem_validade_Rangefilterto = cgiGet( "DDO_REGRASCONTAGEM_VALIDADE_Rangefilterto");
            Ddo_regrascontagem_validade_Searchbuttontext = cgiGet( "DDO_REGRASCONTAGEM_VALIDADE_Searchbuttontext");
            Ddo_regrascontagem_responsavel_Caption = cgiGet( "DDO_REGRASCONTAGEM_RESPONSAVEL_Caption");
            Ddo_regrascontagem_responsavel_Tooltip = cgiGet( "DDO_REGRASCONTAGEM_RESPONSAVEL_Tooltip");
            Ddo_regrascontagem_responsavel_Cls = cgiGet( "DDO_REGRASCONTAGEM_RESPONSAVEL_Cls");
            Ddo_regrascontagem_responsavel_Filteredtext_set = cgiGet( "DDO_REGRASCONTAGEM_RESPONSAVEL_Filteredtext_set");
            Ddo_regrascontagem_responsavel_Selectedvalue_set = cgiGet( "DDO_REGRASCONTAGEM_RESPONSAVEL_Selectedvalue_set");
            Ddo_regrascontagem_responsavel_Dropdownoptionstype = cgiGet( "DDO_REGRASCONTAGEM_RESPONSAVEL_Dropdownoptionstype");
            Ddo_regrascontagem_responsavel_Titlecontrolidtoreplace = cgiGet( "DDO_REGRASCONTAGEM_RESPONSAVEL_Titlecontrolidtoreplace");
            Ddo_regrascontagem_responsavel_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_REGRASCONTAGEM_RESPONSAVEL_Includesortasc"));
            Ddo_regrascontagem_responsavel_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_REGRASCONTAGEM_RESPONSAVEL_Includesortdsc"));
            Ddo_regrascontagem_responsavel_Sortedstatus = cgiGet( "DDO_REGRASCONTAGEM_RESPONSAVEL_Sortedstatus");
            Ddo_regrascontagem_responsavel_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_REGRASCONTAGEM_RESPONSAVEL_Includefilter"));
            Ddo_regrascontagem_responsavel_Filtertype = cgiGet( "DDO_REGRASCONTAGEM_RESPONSAVEL_Filtertype");
            Ddo_regrascontagem_responsavel_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_REGRASCONTAGEM_RESPONSAVEL_Filterisrange"));
            Ddo_regrascontagem_responsavel_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_REGRASCONTAGEM_RESPONSAVEL_Includedatalist"));
            Ddo_regrascontagem_responsavel_Datalisttype = cgiGet( "DDO_REGRASCONTAGEM_RESPONSAVEL_Datalisttype");
            Ddo_regrascontagem_responsavel_Datalistproc = cgiGet( "DDO_REGRASCONTAGEM_RESPONSAVEL_Datalistproc");
            Ddo_regrascontagem_responsavel_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_REGRASCONTAGEM_RESPONSAVEL_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_regrascontagem_responsavel_Sortasc = cgiGet( "DDO_REGRASCONTAGEM_RESPONSAVEL_Sortasc");
            Ddo_regrascontagem_responsavel_Sortdsc = cgiGet( "DDO_REGRASCONTAGEM_RESPONSAVEL_Sortdsc");
            Ddo_regrascontagem_responsavel_Loadingdata = cgiGet( "DDO_REGRASCONTAGEM_RESPONSAVEL_Loadingdata");
            Ddo_regrascontagem_responsavel_Cleanfilter = cgiGet( "DDO_REGRASCONTAGEM_RESPONSAVEL_Cleanfilter");
            Ddo_regrascontagem_responsavel_Noresultsfound = cgiGet( "DDO_REGRASCONTAGEM_RESPONSAVEL_Noresultsfound");
            Ddo_regrascontagem_responsavel_Searchbuttontext = cgiGet( "DDO_REGRASCONTAGEM_RESPONSAVEL_Searchbuttontext");
            Gridpaginationbar_Selectedpage = cgiGet( "GRIDPAGINATIONBAR_Selectedpage");
            Ddo_regrascontagem_regra_Activeeventkey = cgiGet( "DDO_REGRASCONTAGEM_REGRA_Activeeventkey");
            Ddo_regrascontagem_regra_Filteredtext_get = cgiGet( "DDO_REGRASCONTAGEM_REGRA_Filteredtext_get");
            Ddo_regrascontagem_regra_Selectedvalue_get = cgiGet( "DDO_REGRASCONTAGEM_REGRA_Selectedvalue_get");
            Ddo_regrascontagem_data_Activeeventkey = cgiGet( "DDO_REGRASCONTAGEM_DATA_Activeeventkey");
            Ddo_regrascontagem_data_Filteredtext_get = cgiGet( "DDO_REGRASCONTAGEM_DATA_Filteredtext_get");
            Ddo_regrascontagem_data_Filteredtextto_get = cgiGet( "DDO_REGRASCONTAGEM_DATA_Filteredtextto_get");
            Ddo_regrascontagem_validade_Activeeventkey = cgiGet( "DDO_REGRASCONTAGEM_VALIDADE_Activeeventkey");
            Ddo_regrascontagem_validade_Filteredtext_get = cgiGet( "DDO_REGRASCONTAGEM_VALIDADE_Filteredtext_get");
            Ddo_regrascontagem_validade_Filteredtextto_get = cgiGet( "DDO_REGRASCONTAGEM_VALIDADE_Filteredtextto_get");
            Ddo_regrascontagem_responsavel_Activeeventkey = cgiGet( "DDO_REGRASCONTAGEM_RESPONSAVEL_Activeeventkey");
            Ddo_regrascontagem_responsavel_Filteredtext_get = cgiGet( "DDO_REGRASCONTAGEM_RESPONSAVEL_Filteredtext_get");
            Ddo_regrascontagem_responsavel_Selectedvalue_get = cgiGet( "DDO_REGRASCONTAGEM_RESPONSAVEL_Selectedvalue_get");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vREGRASCONTAGEM_REGRA1"), AV31RegrasContagem_Regra1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vREGRASCONTAGEM_DATA1"), 0) != AV16RegrasContagem_Data1 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vREGRASCONTAGEM_DATA_TO1"), 0) != AV17RegrasContagem_Data_To1 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vREGRASCONTAGEM_RESPONSAVEL1"), AV32RegrasContagem_Responsavel1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV19DynamicFiltersSelector2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vREGRASCONTAGEM_REGRA2"), AV33RegrasContagem_Regra2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vREGRASCONTAGEM_DATA2"), 0) != AV20RegrasContagem_Data2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vREGRASCONTAGEM_DATA_TO2"), 0) != AV21RegrasContagem_Data_To2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vREGRASCONTAGEM_RESPONSAVEL2"), AV34RegrasContagem_Responsavel2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR3"), AV23DynamicFiltersSelector3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vREGRASCONTAGEM_REGRA3"), AV35RegrasContagem_Regra3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vREGRASCONTAGEM_DATA3"), 0) != AV24RegrasContagem_Data3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vREGRASCONTAGEM_DATA_TO3"), 0) != AV25RegrasContagem_Data_To3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vREGRASCONTAGEM_RESPONSAVEL3"), AV36RegrasContagem_Responsavel3) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV18DynamicFiltersEnabled2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED3")) != AV22DynamicFiltersEnabled3 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFREGRASCONTAGEM_REGRA"), AV41TFRegrasContagem_Regra) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFREGRASCONTAGEM_REGRA_SEL"), AV42TFRegrasContagem_Regra_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vTFREGRASCONTAGEM_DATA"), 0) != AV45TFRegrasContagem_Data )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vTFREGRASCONTAGEM_DATA_TO"), 0) != AV46TFRegrasContagem_Data_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vTFREGRASCONTAGEM_VALIDADE"), 0) != AV51TFRegrasContagem_Validade )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vTFREGRASCONTAGEM_VALIDADE_TO"), 0) != AV52TFRegrasContagem_Validade_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFREGRASCONTAGEM_RESPONSAVEL"), AV57TFRegrasContagem_Responsavel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFREGRASCONTAGEM_RESPONSAVEL_SEL"), AV58TFRegrasContagem_Responsavel_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E28FC2 */
         E28FC2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E28FC2( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         lblJsdynamicfilters_Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         chkavDynamicfiltersenabled2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled2.Visible), 5, 0)));
         chkavDynamicfiltersenabled3.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled3.Visible), 5, 0)));
         AV15DynamicFiltersSelector1 = "REGRASCONTAGEM_REGRA";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV19DynamicFiltersSelector2 = "REGRASCONTAGEM_REGRA";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV23DynamicFiltersSelector3 = "REGRASCONTAGEM_REGRA";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         imgAdddynamicfilters1_Jsonclick = "WWPDynFilterShow(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Jsonclick", imgAdddynamicfilters1_Jsonclick);
         imgRemovedynamicfilters1_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Jsonclick", imgRemovedynamicfilters1_Jsonclick);
         imgAdddynamicfilters2_Jsonclick = "WWPDynFilterShow(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Jsonclick", imgAdddynamicfilters2_Jsonclick);
         imgRemovedynamicfilters2_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Jsonclick", imgRemovedynamicfilters2_Jsonclick);
         imgRemovedynamicfilters3_Jsonclick = "WWPDynFilterHideLast(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters3_Internalname, "Jsonclick", imgRemovedynamicfilters3_Jsonclick);
         edtavTfregrascontagem_regra_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfregrascontagem_regra_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfregrascontagem_regra_Visible), 5, 0)));
         edtavTfregrascontagem_regra_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfregrascontagem_regra_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfregrascontagem_regra_sel_Visible), 5, 0)));
         edtavTfregrascontagem_data_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfregrascontagem_data_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfregrascontagem_data_Visible), 5, 0)));
         edtavTfregrascontagem_data_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfregrascontagem_data_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfregrascontagem_data_to_Visible), 5, 0)));
         edtavTfregrascontagem_validade_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfregrascontagem_validade_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfregrascontagem_validade_Visible), 5, 0)));
         edtavTfregrascontagem_validade_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfregrascontagem_validade_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfregrascontagem_validade_to_Visible), 5, 0)));
         edtavTfregrascontagem_responsavel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfregrascontagem_responsavel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfregrascontagem_responsavel_Visible), 5, 0)));
         edtavTfregrascontagem_responsavel_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfregrascontagem_responsavel_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfregrascontagem_responsavel_sel_Visible), 5, 0)));
         Ddo_regrascontagem_regra_Titlecontrolidtoreplace = subGrid_Internalname+"_RegrasContagem_Regra";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_regrascontagem_regra_Internalname, "TitleControlIdToReplace", Ddo_regrascontagem_regra_Titlecontrolidtoreplace);
         AV43ddo_RegrasContagem_RegraTitleControlIdToReplace = Ddo_regrascontagem_regra_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43ddo_RegrasContagem_RegraTitleControlIdToReplace", AV43ddo_RegrasContagem_RegraTitleControlIdToReplace);
         edtavDdo_regrascontagem_regratitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_regrascontagem_regratitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_regrascontagem_regratitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_regrascontagem_data_Titlecontrolidtoreplace = subGrid_Internalname+"_RegrasContagem_Data";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_regrascontagem_data_Internalname, "TitleControlIdToReplace", Ddo_regrascontagem_data_Titlecontrolidtoreplace);
         AV49ddo_RegrasContagem_DataTitleControlIdToReplace = Ddo_regrascontagem_data_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49ddo_RegrasContagem_DataTitleControlIdToReplace", AV49ddo_RegrasContagem_DataTitleControlIdToReplace);
         edtavDdo_regrascontagem_datatitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_regrascontagem_datatitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_regrascontagem_datatitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_regrascontagem_validade_Titlecontrolidtoreplace = subGrid_Internalname+"_RegrasContagem_Validade";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_regrascontagem_validade_Internalname, "TitleControlIdToReplace", Ddo_regrascontagem_validade_Titlecontrolidtoreplace);
         AV55ddo_RegrasContagem_ValidadeTitleControlIdToReplace = Ddo_regrascontagem_validade_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55ddo_RegrasContagem_ValidadeTitleControlIdToReplace", AV55ddo_RegrasContagem_ValidadeTitleControlIdToReplace);
         edtavDdo_regrascontagem_validadetitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_regrascontagem_validadetitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_regrascontagem_validadetitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_regrascontagem_responsavel_Titlecontrolidtoreplace = subGrid_Internalname+"_RegrasContagem_Responsavel";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_regrascontagem_responsavel_Internalname, "TitleControlIdToReplace", Ddo_regrascontagem_responsavel_Titlecontrolidtoreplace);
         AV59ddo_RegrasContagem_ResponsavelTitleControlIdToReplace = Ddo_regrascontagem_responsavel_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59ddo_RegrasContagem_ResponsavelTitleControlIdToReplace", AV59ddo_RegrasContagem_ResponsavelTitleControlIdToReplace);
         edtavDdo_regrascontagem_responsaveltitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_regrascontagem_responsaveltitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_regrascontagem_responsaveltitlecontrolidtoreplace_Visible), 5, 0)));
         Form.Caption = " Regras de Contagem";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavOrderedby.removeAllItems();
         cmbavOrderedby.addItem("1", "Data", 0);
         cmbavOrderedby.addItem("2", "Regra", 0);
         cmbavOrderedby.addItem("3", "Validade", 0);
         cmbavOrderedby.addItem("4", "Respons�vel", 0);
         if ( AV13OrderedBy < 1 )
         {
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         imgCleanfilters_Jsonclick = "WWPDynFilterHideAll(3)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgCleanfilters_Internalname, "Jsonclick", imgCleanfilters_Jsonclick);
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = AV60DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1) ;
         AV60DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1;
      }

      protected void E16FC2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV40RegrasContagem_RegraTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV44RegrasContagem_DataTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV50RegrasContagem_ValidadeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV56RegrasContagem_ResponsavelTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'CHECKSECURITYFORACTIONS' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtRegrasContagem_Regra_Titleformat = 2;
         edtRegrasContagem_Regra_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Regra", AV43ddo_RegrasContagem_RegraTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtRegrasContagem_Regra_Internalname, "Title", edtRegrasContagem_Regra_Title);
         edtRegrasContagem_Data_Titleformat = 2;
         edtRegrasContagem_Data_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Data", AV49ddo_RegrasContagem_DataTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtRegrasContagem_Data_Internalname, "Title", edtRegrasContagem_Data_Title);
         edtRegrasContagem_Validade_Titleformat = 2;
         edtRegrasContagem_Validade_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Validade", AV55ddo_RegrasContagem_ValidadeTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtRegrasContagem_Validade_Internalname, "Title", edtRegrasContagem_Validade_Title);
         edtRegrasContagem_Responsavel_Titleformat = 2;
         edtRegrasContagem_Responsavel_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Respons�vel", AV59ddo_RegrasContagem_ResponsavelTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtRegrasContagem_Responsavel_Internalname, "Title", edtRegrasContagem_Responsavel_Title);
         AV62GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV62GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV62GridCurrentPage), 10, 0)));
         AV63GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV63GridPageCount), 10, 0)));
         imgInsert_Visible = (AV6WWPContext.gxTpr_Userehadministradorgam||AV6WWPContext.gxTpr_Insert ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgInsert_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgInsert_Visible), 5, 0)));
         edtavUpdate_Visible = (AV6WWPContext.gxTpr_Userehadministradorgam||AV6WWPContext.gxTpr_Update ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUpdate_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUpdate_Visible), 5, 0)));
         edtavDelete_Visible = (AV6WWPContext.gxTpr_Userehadministradorgam||AV6WWPContext.gxTpr_Delete ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDelete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDelete_Visible), 5, 0)));
         AV66WWRegrasContagemDS_1_Regrascontagem_areatrabalhocod = AV37RegrasContagem_AreaTrabalhoCod;
         AV67WWRegrasContagemDS_2_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV68WWRegrasContagemDS_3_Regrascontagem_regra1 = AV31RegrasContagem_Regra1;
         AV69WWRegrasContagemDS_4_Regrascontagem_data1 = AV16RegrasContagem_Data1;
         AV70WWRegrasContagemDS_5_Regrascontagem_data_to1 = AV17RegrasContagem_Data_To1;
         AV71WWRegrasContagemDS_6_Regrascontagem_responsavel1 = AV32RegrasContagem_Responsavel1;
         AV72WWRegrasContagemDS_7_Dynamicfiltersenabled2 = AV18DynamicFiltersEnabled2;
         AV73WWRegrasContagemDS_8_Dynamicfiltersselector2 = AV19DynamicFiltersSelector2;
         AV74WWRegrasContagemDS_9_Regrascontagem_regra2 = AV33RegrasContagem_Regra2;
         AV75WWRegrasContagemDS_10_Regrascontagem_data2 = AV20RegrasContagem_Data2;
         AV76WWRegrasContagemDS_11_Regrascontagem_data_to2 = AV21RegrasContagem_Data_To2;
         AV77WWRegrasContagemDS_12_Regrascontagem_responsavel2 = AV34RegrasContagem_Responsavel2;
         AV78WWRegrasContagemDS_13_Dynamicfiltersenabled3 = AV22DynamicFiltersEnabled3;
         AV79WWRegrasContagemDS_14_Dynamicfiltersselector3 = AV23DynamicFiltersSelector3;
         AV80WWRegrasContagemDS_15_Regrascontagem_regra3 = AV35RegrasContagem_Regra3;
         AV81WWRegrasContagemDS_16_Regrascontagem_data3 = AV24RegrasContagem_Data3;
         AV82WWRegrasContagemDS_17_Regrascontagem_data_to3 = AV25RegrasContagem_Data_To3;
         AV83WWRegrasContagemDS_18_Regrascontagem_responsavel3 = AV36RegrasContagem_Responsavel3;
         AV84WWRegrasContagemDS_19_Tfregrascontagem_regra = AV41TFRegrasContagem_Regra;
         AV85WWRegrasContagemDS_20_Tfregrascontagem_regra_sel = AV42TFRegrasContagem_Regra_Sel;
         AV86WWRegrasContagemDS_21_Tfregrascontagem_data = AV45TFRegrasContagem_Data;
         AV87WWRegrasContagemDS_22_Tfregrascontagem_data_to = AV46TFRegrasContagem_Data_To;
         AV88WWRegrasContagemDS_23_Tfregrascontagem_validade = AV51TFRegrasContagem_Validade;
         AV89WWRegrasContagemDS_24_Tfregrascontagem_validade_to = AV52TFRegrasContagem_Validade_To;
         AV90WWRegrasContagemDS_25_Tfregrascontagem_responsavel = AV57TFRegrasContagem_Responsavel;
         AV91WWRegrasContagemDS_26_Tfregrascontagem_responsavel_sel = AV58TFRegrasContagem_Responsavel_Sel;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV40RegrasContagem_RegraTitleFilterData", AV40RegrasContagem_RegraTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV44RegrasContagem_DataTitleFilterData", AV44RegrasContagem_DataTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV50RegrasContagem_ValidadeTitleFilterData", AV50RegrasContagem_ValidadeTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV56RegrasContagem_ResponsavelTitleFilterData", AV56RegrasContagem_ResponsavelTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV6WWPContext", AV6WWPContext);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
      }

      protected void E11FC2( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV61PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV61PageToGo) ;
         }
      }

      protected void E12FC2( )
      {
         /* Ddo_regrascontagem_regra_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_regrascontagem_regra_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S192 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_regrascontagem_regra_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_regrascontagem_regra_Internalname, "SortedStatus", Ddo_regrascontagem_regra_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_regrascontagem_regra_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S192 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_regrascontagem_regra_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_regrascontagem_regra_Internalname, "SortedStatus", Ddo_regrascontagem_regra_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_regrascontagem_regra_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV41TFRegrasContagem_Regra = Ddo_regrascontagem_regra_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41TFRegrasContagem_Regra", AV41TFRegrasContagem_Regra);
            AV42TFRegrasContagem_Regra_Sel = Ddo_regrascontagem_regra_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42TFRegrasContagem_Regra_Sel", AV42TFRegrasContagem_Regra_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E13FC2( )
      {
         /* Ddo_regrascontagem_data_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_regrascontagem_data_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S192 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_regrascontagem_data_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_regrascontagem_data_Internalname, "SortedStatus", Ddo_regrascontagem_data_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_regrascontagem_data_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S192 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_regrascontagem_data_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_regrascontagem_data_Internalname, "SortedStatus", Ddo_regrascontagem_data_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_regrascontagem_data_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV45TFRegrasContagem_Data = context.localUtil.CToD( Ddo_regrascontagem_data_Filteredtext_get, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45TFRegrasContagem_Data", context.localUtil.Format(AV45TFRegrasContagem_Data, "99/99/99"));
            AV46TFRegrasContagem_Data_To = context.localUtil.CToD( Ddo_regrascontagem_data_Filteredtextto_get, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46TFRegrasContagem_Data_To", context.localUtil.Format(AV46TFRegrasContagem_Data_To, "99/99/99"));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E14FC2( )
      {
         /* Ddo_regrascontagem_validade_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_regrascontagem_validade_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S192 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_regrascontagem_validade_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_regrascontagem_validade_Internalname, "SortedStatus", Ddo_regrascontagem_validade_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_regrascontagem_validade_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S192 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_regrascontagem_validade_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_regrascontagem_validade_Internalname, "SortedStatus", Ddo_regrascontagem_validade_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_regrascontagem_validade_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV51TFRegrasContagem_Validade = context.localUtil.CToD( Ddo_regrascontagem_validade_Filteredtext_get, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51TFRegrasContagem_Validade", context.localUtil.Format(AV51TFRegrasContagem_Validade, "99/99/99"));
            AV52TFRegrasContagem_Validade_To = context.localUtil.CToD( Ddo_regrascontagem_validade_Filteredtextto_get, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52TFRegrasContagem_Validade_To", context.localUtil.Format(AV52TFRegrasContagem_Validade_To, "99/99/99"));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E15FC2( )
      {
         /* Ddo_regrascontagem_responsavel_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_regrascontagem_responsavel_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S192 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_regrascontagem_responsavel_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_regrascontagem_responsavel_Internalname, "SortedStatus", Ddo_regrascontagem_responsavel_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_regrascontagem_responsavel_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S192 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_regrascontagem_responsavel_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_regrascontagem_responsavel_Internalname, "SortedStatus", Ddo_regrascontagem_responsavel_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_regrascontagem_responsavel_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV57TFRegrasContagem_Responsavel = Ddo_regrascontagem_responsavel_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57TFRegrasContagem_Responsavel", AV57TFRegrasContagem_Responsavel);
            AV58TFRegrasContagem_Responsavel_Sel = Ddo_regrascontagem_responsavel_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58TFRegrasContagem_Responsavel_Sel", AV58TFRegrasContagem_Responsavel_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      private void E29FC2( )
      {
         /* Grid_Load Routine */
         edtavUpdate_Tooltiptext = "Modifica";
         if ( ( AV6WWPContext.gxTpr_Insert && AV6WWPContext.gxTpr_Update && AV6WWPContext.gxTpr_Delete ) || AV6WWPContext.gxTpr_Userehadministradorgam )
         {
            edtavUpdate_Link = formatLink("regrascontagem.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode(StringUtil.RTrim(A860RegrasContagem_Regra));
            AV28Update = context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavUpdate_Internalname, AV28Update);
            AV92Update_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( )));
            edtavUpdate_Enabled = 1;
         }
         else
         {
            edtavUpdate_Link = "";
            AV28Update = context.GetImagePath( "0211baea-a1e2-4bb5-849c-f3afb69b09ee", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavUpdate_Internalname, AV28Update);
            AV92Update_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "0211baea-a1e2-4bb5-849c-f3afb69b09ee", "", context.GetTheme( )));
            edtavUpdate_Enabled = 0;
         }
         edtavDelete_Tooltiptext = "Eliminar";
         if ( ( AV6WWPContext.gxTpr_Insert && AV6WWPContext.gxTpr_Update && AV6WWPContext.gxTpr_Delete ) || AV6WWPContext.gxTpr_Userehadministradorgam )
         {
            edtavDelete_Link = formatLink("regrascontagem.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode(StringUtil.RTrim(A860RegrasContagem_Regra));
            AV29Delete = context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDelete_Internalname, AV29Delete);
            AV93Delete_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )));
            edtavDelete_Enabled = 1;
         }
         else
         {
            edtavDelete_Link = "";
            AV29Delete = context.GetImagePath( "138fdfa8-ae9b-4664-8007-e04d47468418", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDelete_Internalname, AV29Delete);
            AV93Delete_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "138fdfa8-ae9b-4664-8007-e04d47468418", "", context.GetTheme( )));
            edtavDelete_Enabled = 0;
         }
         edtRegrasContagem_Data_Link = formatLink("viewregrascontagem.aspx") + "?" + UrlEncode(StringUtil.RTrim(A860RegrasContagem_Regra)) + "," + UrlEncode(StringUtil.RTrim(""));
         edtRegrasContagem_Regra_Tooltiptext = A864RegrasContagem_Descricao;
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 106;
         }
         sendrow_1062( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_106_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(106, GridRow);
         }
      }

      protected void E17FC2( )
      {
         /* Orderedby_Click Routine */
         context.DoAjaxRefresh();
      }

      protected void E23FC2( )
      {
         /* 'AddDynamicFilters1' Routine */
         AV18DynamicFiltersEnabled2 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         imgAdddynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
      }

      protected void E18FC2( )
      {
         /* 'RemoveDynamicFilters1' Routine */
         AV26DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV27DynamicFiltersIgnoreFirst = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersIgnoreFirst", AV27DynamicFiltersIgnoreFirst);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S222 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV27DynamicFiltersIgnoreFirst = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27DynamicFiltersIgnoreFirst", AV27DynamicFiltersIgnoreFirst);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV31RegrasContagem_Regra1, AV16RegrasContagem_Data1, AV17RegrasContagem_Data_To1, AV32RegrasContagem_Responsavel1, AV19DynamicFiltersSelector2, AV33RegrasContagem_Regra2, AV20RegrasContagem_Data2, AV21RegrasContagem_Data_To2, AV34RegrasContagem_Responsavel2, AV23DynamicFiltersSelector3, AV35RegrasContagem_Regra3, AV24RegrasContagem_Data3, AV25RegrasContagem_Data_To3, AV36RegrasContagem_Responsavel3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV41TFRegrasContagem_Regra, AV42TFRegrasContagem_Regra_Sel, AV45TFRegrasContagem_Data, AV46TFRegrasContagem_Data_To, AV51TFRegrasContagem_Validade, AV52TFRegrasContagem_Validade_To, AV57TFRegrasContagem_Responsavel, AV58TFRegrasContagem_Responsavel_Sel, AV43ddo_RegrasContagem_RegraTitleControlIdToReplace, AV49ddo_RegrasContagem_DataTitleControlIdToReplace, AV55ddo_RegrasContagem_ValidadeTitleControlIdToReplace, AV59ddo_RegrasContagem_ResponsavelTitleControlIdToReplace, AV37RegrasContagem_AreaTrabalhoCod, AV6WWPContext, AV94Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A860RegrasContagem_Regra, A864RegrasContagem_Descricao) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
      }

      protected void E24FC2( )
      {
         /* Dynamicfiltersselector1_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E25FC2( )
      {
         /* 'AddDynamicFilters2' Routine */
         AV22DynamicFiltersEnabled3 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
         imgAdddynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
      }

      protected void E19FC2( )
      {
         /* 'RemoveDynamicFilters2' Routine */
         AV26DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV18DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S222 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV31RegrasContagem_Regra1, AV16RegrasContagem_Data1, AV17RegrasContagem_Data_To1, AV32RegrasContagem_Responsavel1, AV19DynamicFiltersSelector2, AV33RegrasContagem_Regra2, AV20RegrasContagem_Data2, AV21RegrasContagem_Data_To2, AV34RegrasContagem_Responsavel2, AV23DynamicFiltersSelector3, AV35RegrasContagem_Regra3, AV24RegrasContagem_Data3, AV25RegrasContagem_Data_To3, AV36RegrasContagem_Responsavel3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV41TFRegrasContagem_Regra, AV42TFRegrasContagem_Regra_Sel, AV45TFRegrasContagem_Data, AV46TFRegrasContagem_Data_To, AV51TFRegrasContagem_Validade, AV52TFRegrasContagem_Validade_To, AV57TFRegrasContagem_Responsavel, AV58TFRegrasContagem_Responsavel_Sel, AV43ddo_RegrasContagem_RegraTitleControlIdToReplace, AV49ddo_RegrasContagem_DataTitleControlIdToReplace, AV55ddo_RegrasContagem_ValidadeTitleControlIdToReplace, AV59ddo_RegrasContagem_ResponsavelTitleControlIdToReplace, AV37RegrasContagem_AreaTrabalhoCod, AV6WWPContext, AV94Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A860RegrasContagem_Regra, A864RegrasContagem_Descricao) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
      }

      protected void E26FC2( )
      {
         /* Dynamicfiltersselector2_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E20FC2( )
      {
         /* 'RemoveDynamicFilters3' Routine */
         AV26DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         AV22DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S222 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV26DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersRemoving", AV26DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV31RegrasContagem_Regra1, AV16RegrasContagem_Data1, AV17RegrasContagem_Data_To1, AV32RegrasContagem_Responsavel1, AV19DynamicFiltersSelector2, AV33RegrasContagem_Regra2, AV20RegrasContagem_Data2, AV21RegrasContagem_Data_To2, AV34RegrasContagem_Responsavel2, AV23DynamicFiltersSelector3, AV35RegrasContagem_Regra3, AV24RegrasContagem_Data3, AV25RegrasContagem_Data_To3, AV36RegrasContagem_Responsavel3, AV18DynamicFiltersEnabled2, AV22DynamicFiltersEnabled3, AV41TFRegrasContagem_Regra, AV42TFRegrasContagem_Regra_Sel, AV45TFRegrasContagem_Data, AV46TFRegrasContagem_Data_To, AV51TFRegrasContagem_Validade, AV52TFRegrasContagem_Validade_To, AV57TFRegrasContagem_Responsavel, AV58TFRegrasContagem_Responsavel_Sel, AV43ddo_RegrasContagem_RegraTitleControlIdToReplace, AV49ddo_RegrasContagem_DataTitleControlIdToReplace, AV55ddo_RegrasContagem_ValidadeTitleControlIdToReplace, AV59ddo_RegrasContagem_ResponsavelTitleControlIdToReplace, AV37RegrasContagem_AreaTrabalhoCod, AV6WWPContext, AV94Pgmname, AV10GridState, AV27DynamicFiltersIgnoreFirst, AV26DynamicFiltersRemoving, A860RegrasContagem_Regra, A864RegrasContagem_Descricao) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
      }

      protected void E27FC2( )
      {
         /* Dynamicfiltersselector3_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E21FC2( )
      {
         /* 'DoCleanFilters' Routine */
         /* Execute user subroutine: 'CLEANFILTERS' */
         S232 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_firstpage( ) ;
         context.DoAjaxRefresh();
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", cmbavDynamicfiltersselector3.ToJavascriptSource());
      }

      protected void E22FC2( )
      {
         /* 'DoInsert' Routine */
         context.wjLoc = formatLink("regrascontagem.aspx") + "?" + UrlEncode(StringUtil.RTrim("INS")) + "," + UrlEncode(StringUtil.RTrim(""));
         context.wjLocDisableFrm = 1;
      }

      protected void S192( )
      {
         /* 'RESETDDOSORTEDSTATUS' Routine */
         Ddo_regrascontagem_regra_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_regrascontagem_regra_Internalname, "SortedStatus", Ddo_regrascontagem_regra_Sortedstatus);
         Ddo_regrascontagem_data_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_regrascontagem_data_Internalname, "SortedStatus", Ddo_regrascontagem_data_Sortedstatus);
         Ddo_regrascontagem_validade_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_regrascontagem_validade_Internalname, "SortedStatus", Ddo_regrascontagem_validade_Sortedstatus);
         Ddo_regrascontagem_responsavel_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_regrascontagem_responsavel_Internalname, "SortedStatus", Ddo_regrascontagem_responsavel_Sortedstatus);
      }

      protected void S162( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV13OrderedBy == 2 )
         {
            Ddo_regrascontagem_regra_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_regrascontagem_regra_Internalname, "SortedStatus", Ddo_regrascontagem_regra_Sortedstatus);
         }
         else if ( AV13OrderedBy == 1 )
         {
            Ddo_regrascontagem_data_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_regrascontagem_data_Internalname, "SortedStatus", Ddo_regrascontagem_data_Sortedstatus);
         }
         else if ( AV13OrderedBy == 3 )
         {
            Ddo_regrascontagem_validade_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_regrascontagem_validade_Internalname, "SortedStatus", Ddo_regrascontagem_validade_Sortedstatus);
         }
         else if ( AV13OrderedBy == 4 )
         {
            Ddo_regrascontagem_responsavel_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_regrascontagem_responsavel_Internalname, "SortedStatus", Ddo_regrascontagem_responsavel_Sortedstatus);
         }
      }

      protected void S172( )
      {
         /* 'CHECKSECURITYFORACTIONS' Routine */
         if ( ! ( ( AV6WWPContext.gxTpr_Insert && AV6WWPContext.gxTpr_Update && AV6WWPContext.gxTpr_Delete ) || AV6WWPContext.gxTpr_Userehadministradorgam ) )
         {
            imgInsert_Link = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgInsert_Internalname, "Link", imgInsert_Link);
            imgInsert_Bitmap = context.GetImagePath( "13d28f37-c579-4dd9-a404-ba51ab71d1e3", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgInsert_Internalname, "Bitmap", context.convertURL( context.PathToRelativeUrl( imgInsert_Bitmap)));
            imgInsert_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgInsert_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgInsert_Enabled), 5, 0)));
         }
      }

      protected void S112( )
      {
         /* 'ENABLEDYNAMICFILTERS1' Routine */
         edtavRegrascontagem_regra1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavRegrascontagem_regra1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavRegrascontagem_regra1_Visible), 5, 0)));
         tblTablemergeddynamicfiltersregrascontagem_data1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfiltersregrascontagem_data1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfiltersregrascontagem_data1_Visible), 5, 0)));
         edtavRegrascontagem_responsavel1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavRegrascontagem_responsavel1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavRegrascontagem_responsavel1_Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "REGRASCONTAGEM_REGRA") == 0 )
         {
            edtavRegrascontagem_regra1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavRegrascontagem_regra1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavRegrascontagem_regra1_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "REGRASCONTAGEM_DATA") == 0 )
         {
            tblTablemergeddynamicfiltersregrascontagem_data1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfiltersregrascontagem_data1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfiltersregrascontagem_data1_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "REGRASCONTAGEM_RESPONSAVEL") == 0 )
         {
            edtavRegrascontagem_responsavel1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavRegrascontagem_responsavel1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavRegrascontagem_responsavel1_Visible), 5, 0)));
         }
      }

      protected void S122( )
      {
         /* 'ENABLEDYNAMICFILTERS2' Routine */
         edtavRegrascontagem_regra2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavRegrascontagem_regra2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavRegrascontagem_regra2_Visible), 5, 0)));
         tblTablemergeddynamicfiltersregrascontagem_data2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfiltersregrascontagem_data2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfiltersregrascontagem_data2_Visible), 5, 0)));
         edtavRegrascontagem_responsavel2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavRegrascontagem_responsavel2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavRegrascontagem_responsavel2_Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "REGRASCONTAGEM_REGRA") == 0 )
         {
            edtavRegrascontagem_regra2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavRegrascontagem_regra2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavRegrascontagem_regra2_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "REGRASCONTAGEM_DATA") == 0 )
         {
            tblTablemergeddynamicfiltersregrascontagem_data2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfiltersregrascontagem_data2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfiltersregrascontagem_data2_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "REGRASCONTAGEM_RESPONSAVEL") == 0 )
         {
            edtavRegrascontagem_responsavel2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavRegrascontagem_responsavel2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavRegrascontagem_responsavel2_Visible), 5, 0)));
         }
      }

      protected void S132( )
      {
         /* 'ENABLEDYNAMICFILTERS3' Routine */
         edtavRegrascontagem_regra3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavRegrascontagem_regra3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavRegrascontagem_regra3_Visible), 5, 0)));
         tblTablemergeddynamicfiltersregrascontagem_data3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfiltersregrascontagem_data3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfiltersregrascontagem_data3_Visible), 5, 0)));
         edtavRegrascontagem_responsavel3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavRegrascontagem_responsavel3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavRegrascontagem_responsavel3_Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "REGRASCONTAGEM_REGRA") == 0 )
         {
            edtavRegrascontagem_regra3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavRegrascontagem_regra3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavRegrascontagem_regra3_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "REGRASCONTAGEM_DATA") == 0 )
         {
            tblTablemergeddynamicfiltersregrascontagem_data3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTablemergeddynamicfiltersregrascontagem_data3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTablemergeddynamicfiltersregrascontagem_data3_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "REGRASCONTAGEM_RESPONSAVEL") == 0 )
         {
            edtavRegrascontagem_responsavel3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavRegrascontagem_responsavel3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavRegrascontagem_responsavel3_Visible), 5, 0)));
         }
      }

      protected void S212( )
      {
         /* 'RESETDYNFILTERS' Routine */
         AV18DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
         AV19DynamicFiltersSelector2 = "REGRASCONTAGEM_REGRA";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
         AV33RegrasContagem_Regra2 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33RegrasContagem_Regra2", AV33RegrasContagem_Regra2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV22DynamicFiltersEnabled3 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
         AV23DynamicFiltersSelector3 = "REGRASCONTAGEM_REGRA";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
         AV35RegrasContagem_Regra3 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35RegrasContagem_Regra3", AV35RegrasContagem_Regra3);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S232( )
      {
         /* 'CLEANFILTERS' Routine */
         AV37RegrasContagem_AreaTrabalhoCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37RegrasContagem_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV37RegrasContagem_AreaTrabalhoCod), 6, 0)));
         AV41TFRegrasContagem_Regra = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41TFRegrasContagem_Regra", AV41TFRegrasContagem_Regra);
         Ddo_regrascontagem_regra_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_regrascontagem_regra_Internalname, "FilteredText_set", Ddo_regrascontagem_regra_Filteredtext_set);
         AV42TFRegrasContagem_Regra_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42TFRegrasContagem_Regra_Sel", AV42TFRegrasContagem_Regra_Sel);
         Ddo_regrascontagem_regra_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_regrascontagem_regra_Internalname, "SelectedValue_set", Ddo_regrascontagem_regra_Selectedvalue_set);
         AV45TFRegrasContagem_Data = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45TFRegrasContagem_Data", context.localUtil.Format(AV45TFRegrasContagem_Data, "99/99/99"));
         Ddo_regrascontagem_data_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_regrascontagem_data_Internalname, "FilteredText_set", Ddo_regrascontagem_data_Filteredtext_set);
         AV46TFRegrasContagem_Data_To = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46TFRegrasContagem_Data_To", context.localUtil.Format(AV46TFRegrasContagem_Data_To, "99/99/99"));
         Ddo_regrascontagem_data_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_regrascontagem_data_Internalname, "FilteredTextTo_set", Ddo_regrascontagem_data_Filteredtextto_set);
         AV51TFRegrasContagem_Validade = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51TFRegrasContagem_Validade", context.localUtil.Format(AV51TFRegrasContagem_Validade, "99/99/99"));
         Ddo_regrascontagem_validade_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_regrascontagem_validade_Internalname, "FilteredText_set", Ddo_regrascontagem_validade_Filteredtext_set);
         AV52TFRegrasContagem_Validade_To = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52TFRegrasContagem_Validade_To", context.localUtil.Format(AV52TFRegrasContagem_Validade_To, "99/99/99"));
         Ddo_regrascontagem_validade_Filteredtextto_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_regrascontagem_validade_Internalname, "FilteredTextTo_set", Ddo_regrascontagem_validade_Filteredtextto_set);
         AV57TFRegrasContagem_Responsavel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57TFRegrasContagem_Responsavel", AV57TFRegrasContagem_Responsavel);
         Ddo_regrascontagem_responsavel_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_regrascontagem_responsavel_Internalname, "FilteredText_set", Ddo_regrascontagem_responsavel_Filteredtext_set);
         AV58TFRegrasContagem_Responsavel_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58TFRegrasContagem_Responsavel_Sel", AV58TFRegrasContagem_Responsavel_Sel);
         Ddo_regrascontagem_responsavel_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_regrascontagem_responsavel_Internalname, "SelectedValue_set", Ddo_regrascontagem_responsavel_Selectedvalue_set);
         AV15DynamicFiltersSelector1 = "REGRASCONTAGEM_REGRA";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         AV31RegrasContagem_Regra1 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31RegrasContagem_Regra1", AV31RegrasContagem_Regra1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S222 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S152( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV30Session.Get(AV94Pgmname+"GridState"), "") == 0 )
         {
            AV10GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  AV94Pgmname+"GridState"), "");
         }
         else
         {
            AV10GridState.FromXml(AV30Session.Get(AV94Pgmname+"GridState"), "");
         }
         AV13OrderedBy = AV10GridState.gxTpr_Orderedby;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         AV14OrderedDsc = AV10GridState.gxTpr_Ordereddsc;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
         /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADREGFILTERSSTATE' */
         S242 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S222 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_gotopage( AV10GridState.gxTpr_Currentpage) ;
      }

      protected void S242( )
      {
         /* 'LOADREGFILTERSSTATE' Routine */
         AV95GXV1 = 1;
         while ( AV95GXV1 <= AV10GridState.gxTpr_Filtervalues.Count )
         {
            AV11GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV10GridState.gxTpr_Filtervalues.Item(AV95GXV1));
            if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "REGRASCONTAGEM_AREATRABALHOCOD") == 0 )
            {
               AV37RegrasContagem_AreaTrabalhoCod = (int)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37RegrasContagem_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV37RegrasContagem_AreaTrabalhoCod), 6, 0)));
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFREGRASCONTAGEM_REGRA") == 0 )
            {
               AV41TFRegrasContagem_Regra = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41TFRegrasContagem_Regra", AV41TFRegrasContagem_Regra);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV41TFRegrasContagem_Regra)) )
               {
                  Ddo_regrascontagem_regra_Filteredtext_set = AV41TFRegrasContagem_Regra;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_regrascontagem_regra_Internalname, "FilteredText_set", Ddo_regrascontagem_regra_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFREGRASCONTAGEM_REGRA_SEL") == 0 )
            {
               AV42TFRegrasContagem_Regra_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42TFRegrasContagem_Regra_Sel", AV42TFRegrasContagem_Regra_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV42TFRegrasContagem_Regra_Sel)) )
               {
                  Ddo_regrascontagem_regra_Selectedvalue_set = AV42TFRegrasContagem_Regra_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_regrascontagem_regra_Internalname, "SelectedValue_set", Ddo_regrascontagem_regra_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFREGRASCONTAGEM_DATA") == 0 )
            {
               AV45TFRegrasContagem_Data = context.localUtil.CToD( AV11GridStateFilterValue.gxTpr_Value, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45TFRegrasContagem_Data", context.localUtil.Format(AV45TFRegrasContagem_Data, "99/99/99"));
               AV46TFRegrasContagem_Data_To = context.localUtil.CToD( AV11GridStateFilterValue.gxTpr_Valueto, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46TFRegrasContagem_Data_To", context.localUtil.Format(AV46TFRegrasContagem_Data_To, "99/99/99"));
               if ( ! (DateTime.MinValue==AV45TFRegrasContagem_Data) )
               {
                  Ddo_regrascontagem_data_Filteredtext_set = context.localUtil.DToC( AV45TFRegrasContagem_Data, 2, "/");
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_regrascontagem_data_Internalname, "FilteredText_set", Ddo_regrascontagem_data_Filteredtext_set);
               }
               if ( ! (DateTime.MinValue==AV46TFRegrasContagem_Data_To) )
               {
                  Ddo_regrascontagem_data_Filteredtextto_set = context.localUtil.DToC( AV46TFRegrasContagem_Data_To, 2, "/");
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_regrascontagem_data_Internalname, "FilteredTextTo_set", Ddo_regrascontagem_data_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFREGRASCONTAGEM_VALIDADE") == 0 )
            {
               AV51TFRegrasContagem_Validade = context.localUtil.CToD( AV11GridStateFilterValue.gxTpr_Value, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51TFRegrasContagem_Validade", context.localUtil.Format(AV51TFRegrasContagem_Validade, "99/99/99"));
               AV52TFRegrasContagem_Validade_To = context.localUtil.CToD( AV11GridStateFilterValue.gxTpr_Valueto, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52TFRegrasContagem_Validade_To", context.localUtil.Format(AV52TFRegrasContagem_Validade_To, "99/99/99"));
               if ( ! (DateTime.MinValue==AV51TFRegrasContagem_Validade) )
               {
                  Ddo_regrascontagem_validade_Filteredtext_set = context.localUtil.DToC( AV51TFRegrasContagem_Validade, 2, "/");
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_regrascontagem_validade_Internalname, "FilteredText_set", Ddo_regrascontagem_validade_Filteredtext_set);
               }
               if ( ! (DateTime.MinValue==AV52TFRegrasContagem_Validade_To) )
               {
                  Ddo_regrascontagem_validade_Filteredtextto_set = context.localUtil.DToC( AV52TFRegrasContagem_Validade_To, 2, "/");
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_regrascontagem_validade_Internalname, "FilteredTextTo_set", Ddo_regrascontagem_validade_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFREGRASCONTAGEM_RESPONSAVEL") == 0 )
            {
               AV57TFRegrasContagem_Responsavel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57TFRegrasContagem_Responsavel", AV57TFRegrasContagem_Responsavel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV57TFRegrasContagem_Responsavel)) )
               {
                  Ddo_regrascontagem_responsavel_Filteredtext_set = AV57TFRegrasContagem_Responsavel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_regrascontagem_responsavel_Internalname, "FilteredText_set", Ddo_regrascontagem_responsavel_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFREGRASCONTAGEM_RESPONSAVEL_SEL") == 0 )
            {
               AV58TFRegrasContagem_Responsavel_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV58TFRegrasContagem_Responsavel_Sel", AV58TFRegrasContagem_Responsavel_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV58TFRegrasContagem_Responsavel_Sel)) )
               {
                  Ddo_regrascontagem_responsavel_Selectedvalue_set = AV58TFRegrasContagem_Responsavel_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_regrascontagem_responsavel_Internalname, "SelectedValue_set", Ddo_regrascontagem_responsavel_Selectedvalue_set);
               }
            }
            AV95GXV1 = (int)(AV95GXV1+1);
         }
      }

      protected void S222( )
      {
         /* 'LOADDYNFILTERSSTATE' Routine */
         imgAdddynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         imgAdddynamicfilters2_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
         imgRemovedynamicfilters2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
         if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(1));
            AV15DynamicFiltersSelector1 = AV12GridStateDynamicFilter.gxTpr_Selected;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "REGRASCONTAGEM_REGRA") == 0 )
            {
               AV31RegrasContagem_Regra1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31RegrasContagem_Regra1", AV31RegrasContagem_Regra1);
            }
            else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "REGRASCONTAGEM_DATA") == 0 )
            {
               AV16RegrasContagem_Data1 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Value, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16RegrasContagem_Data1", context.localUtil.Format(AV16RegrasContagem_Data1, "99/99/99"));
               AV17RegrasContagem_Data_To1 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Valueto, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17RegrasContagem_Data_To1", context.localUtil.Format(AV17RegrasContagem_Data_To1, "99/99/99"));
            }
            else if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "REGRASCONTAGEM_RESPONSAVEL") == 0 )
            {
               AV32RegrasContagem_Responsavel1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32RegrasContagem_Responsavel1", AV32RegrasContagem_Responsavel1);
            }
            /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               lblJsdynamicfilters_Caption = "<script type=\"text/javascript\">";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(2);";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               imgAdddynamicfilters1_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
               imgRemovedynamicfilters1_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
               AV18DynamicFiltersEnabled2 = true;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18DynamicFiltersEnabled2", AV18DynamicFiltersEnabled2);
               AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(2));
               AV19DynamicFiltersSelector2 = AV12GridStateDynamicFilter.gxTpr_Selected;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19DynamicFiltersSelector2", AV19DynamicFiltersSelector2);
               if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "REGRASCONTAGEM_REGRA") == 0 )
               {
                  AV33RegrasContagem_Regra2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33RegrasContagem_Regra2", AV33RegrasContagem_Regra2);
               }
               else if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "REGRASCONTAGEM_DATA") == 0 )
               {
                  AV20RegrasContagem_Data2 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Value, 2);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20RegrasContagem_Data2", context.localUtil.Format(AV20RegrasContagem_Data2, "99/99/99"));
                  AV21RegrasContagem_Data_To2 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Valueto, 2);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21RegrasContagem_Data_To2", context.localUtil.Format(AV21RegrasContagem_Data_To2, "99/99/99"));
               }
               else if ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "REGRASCONTAGEM_RESPONSAVEL") == 0 )
               {
                  AV34RegrasContagem_Responsavel2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34RegrasContagem_Responsavel2", AV34RegrasContagem_Responsavel2);
               }
               /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
               S122 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(3);";
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
                  imgAdddynamicfilters2_Visible = 0;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters2_Visible), 5, 0)));
                  imgRemovedynamicfilters2_Visible = 1;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters2_Visible), 5, 0)));
                  AV22DynamicFiltersEnabled3 = true;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22DynamicFiltersEnabled3", AV22DynamicFiltersEnabled3);
                  AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(3));
                  AV23DynamicFiltersSelector3 = AV12GridStateDynamicFilter.gxTpr_Selected;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23DynamicFiltersSelector3", AV23DynamicFiltersSelector3);
                  if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "REGRASCONTAGEM_REGRA") == 0 )
                  {
                     AV35RegrasContagem_Regra3 = AV12GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35RegrasContagem_Regra3", AV35RegrasContagem_Regra3);
                  }
                  else if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "REGRASCONTAGEM_DATA") == 0 )
                  {
                     AV24RegrasContagem_Data3 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Value, 2);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24RegrasContagem_Data3", context.localUtil.Format(AV24RegrasContagem_Data3, "99/99/99"));
                     AV25RegrasContagem_Data_To3 = context.localUtil.CToD( AV12GridStateDynamicFilter.gxTpr_Valueto, 2);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25RegrasContagem_Data_To3", context.localUtil.Format(AV25RegrasContagem_Data_To3, "99/99/99"));
                  }
                  else if ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "REGRASCONTAGEM_RESPONSAVEL") == 0 )
                  {
                     AV36RegrasContagem_Responsavel3 = AV12GridStateDynamicFilter.gxTpr_Value;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36RegrasContagem_Responsavel3", AV36RegrasContagem_Responsavel3);
                  }
                  /* Execute user subroutine: 'ENABLEDYNAMICFILTERS3' */
                  S132 ();
                  if ( returnInSub )
                  {
                     returnInSub = true;
                     if (true) return;
                  }
               }
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"</script>";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
            }
         }
         if ( AV26DynamicFiltersRemoving )
         {
            lblJsdynamicfilters_Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         }
      }

      protected void S182( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV10GridState.FromXml(AV30Session.Get(AV94Pgmname+"GridState"), "");
         AV10GridState.gxTpr_Orderedby = AV13OrderedBy;
         AV10GridState.gxTpr_Ordereddsc = AV14OrderedDsc;
         AV10GridState.gxTpr_Filtervalues.Clear();
         if ( ! (0==AV37RegrasContagem_AreaTrabalhoCod) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "REGRASCONTAGEM_AREATRABALHOCOD";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV37RegrasContagem_AreaTrabalhoCod), 6, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV41TFRegrasContagem_Regra)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFREGRASCONTAGEM_REGRA";
            AV11GridStateFilterValue.gxTpr_Value = AV41TFRegrasContagem_Regra;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV42TFRegrasContagem_Regra_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFREGRASCONTAGEM_REGRA_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV42TFRegrasContagem_Regra_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (DateTime.MinValue==AV45TFRegrasContagem_Data) && (DateTime.MinValue==AV46TFRegrasContagem_Data_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFREGRASCONTAGEM_DATA";
            AV11GridStateFilterValue.gxTpr_Value = context.localUtil.DToC( AV45TFRegrasContagem_Data, 2, "/");
            AV11GridStateFilterValue.gxTpr_Valueto = context.localUtil.DToC( AV46TFRegrasContagem_Data_To, 2, "/");
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( (DateTime.MinValue==AV51TFRegrasContagem_Validade) && (DateTime.MinValue==AV52TFRegrasContagem_Validade_To) ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFREGRASCONTAGEM_VALIDADE";
            AV11GridStateFilterValue.gxTpr_Value = context.localUtil.DToC( AV51TFRegrasContagem_Validade, 2, "/");
            AV11GridStateFilterValue.gxTpr_Valueto = context.localUtil.DToC( AV52TFRegrasContagem_Validade_To, 2, "/");
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV57TFRegrasContagem_Responsavel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFREGRASCONTAGEM_RESPONSAVEL";
            AV11GridStateFilterValue.gxTpr_Value = AV57TFRegrasContagem_Responsavel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV58TFRegrasContagem_Responsavel_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFREGRASCONTAGEM_RESPONSAVEL_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV58TFRegrasContagem_Responsavel_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Currentpage = (short)(subGrid_Currentpage( ));
         new wwpbaseobjects.savegridstate(context ).execute(  AV94Pgmname+"GridState",  AV10GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_MeetrikaVs3")) ;
      }

      protected void S202( )
      {
         /* 'SAVEDYNFILTERSSTATE' Routine */
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         if ( ! AV27DynamicFiltersIgnoreFirst )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV15DynamicFiltersSelector1;
            if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "REGRASCONTAGEM_REGRA") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV31RegrasContagem_Regra1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV31RegrasContagem_Regra1;
            }
            else if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "REGRASCONTAGEM_DATA") == 0 ) && ! ( (DateTime.MinValue==AV16RegrasContagem_Data1) && (DateTime.MinValue==AV17RegrasContagem_Data_To1) ) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = context.localUtil.DToC( AV16RegrasContagem_Data1, 2, "/");
               AV12GridStateDynamicFilter.gxTpr_Valueto = context.localUtil.DToC( AV17RegrasContagem_Data_To1, 2, "/");
            }
            else if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "REGRASCONTAGEM_RESPONSAVEL") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV32RegrasContagem_Responsavel1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV32RegrasContagem_Responsavel1;
            }
            if ( AV26DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Valueto)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV18DynamicFiltersEnabled2 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV19DynamicFiltersSelector2;
            if ( ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "REGRASCONTAGEM_REGRA") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV33RegrasContagem_Regra2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV33RegrasContagem_Regra2;
            }
            else if ( ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "REGRASCONTAGEM_DATA") == 0 ) && ! ( (DateTime.MinValue==AV20RegrasContagem_Data2) && (DateTime.MinValue==AV21RegrasContagem_Data_To2) ) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = context.localUtil.DToC( AV20RegrasContagem_Data2, 2, "/");
               AV12GridStateDynamicFilter.gxTpr_Valueto = context.localUtil.DToC( AV21RegrasContagem_Data_To2, 2, "/");
            }
            else if ( ( StringUtil.StrCmp(AV19DynamicFiltersSelector2, "REGRASCONTAGEM_RESPONSAVEL") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV34RegrasContagem_Responsavel2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV34RegrasContagem_Responsavel2;
            }
            if ( AV26DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Valueto)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV22DynamicFiltersEnabled3 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV23DynamicFiltersSelector3;
            if ( ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "REGRASCONTAGEM_REGRA") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV35RegrasContagem_Regra3)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV35RegrasContagem_Regra3;
            }
            else if ( ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "REGRASCONTAGEM_DATA") == 0 ) && ! ( (DateTime.MinValue==AV24RegrasContagem_Data3) && (DateTime.MinValue==AV25RegrasContagem_Data_To3) ) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = context.localUtil.DToC( AV24RegrasContagem_Data3, 2, "/");
               AV12GridStateDynamicFilter.gxTpr_Valueto = context.localUtil.DToC( AV25RegrasContagem_Data_To3, 2, "/");
            }
            else if ( ( StringUtil.StrCmp(AV23DynamicFiltersSelector3, "REGRASCONTAGEM_RESPONSAVEL") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV36RegrasContagem_Responsavel3)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV36RegrasContagem_Responsavel3;
            }
            if ( AV26DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Valueto)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
      }

      protected void S142( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8TrnContext.gxTpr_Callerobject = AV94Pgmname;
         AV8TrnContext.gxTpr_Callerondelete = true;
         AV8TrnContext.gxTpr_Callerurl = AV7HTTPRequest.ScriptName+"?"+AV7HTTPRequest.QueryString;
         AV8TrnContext.gxTpr_Transactionname = "RegrasContagem";
         AV30Session.Set("TrnContext", AV8TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_MeetrikaVs3"));
      }

      protected void wb_table1_2_FC2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_FC2( true) ;
         }
         else
         {
            wb_table2_8_FC2( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_FC2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_100_FC2( true) ;
         }
         else
         {
            wb_table3_100_FC2( false) ;
         }
         return  ;
      }

      protected void wb_table3_100_FC2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_FC2e( true) ;
         }
         else
         {
            wb_table1_2_FC2e( false) ;
         }
      }

      protected void wb_table3_100_FC2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_103_FC2( true) ;
         }
         else
         {
            wb_table4_103_FC2( false) ;
         }
         return  ;
      }

      protected void wb_table4_103_FC2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_100_FC2e( true) ;
         }
         else
         {
            wb_table3_100_FC2e( false) ;
         }
      }

      protected void wb_table4_103_FC2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"DivS\" data-gxgridid=\"106\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+((edtavUpdate_Visible==0) ? "display:none;" : "")+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+((edtavDelete_Visible==0) ? "display:none;" : "")+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(410), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtRegrasContagem_Regra_Titleformat == 0 )
               {
                  context.SendWebValue( edtRegrasContagem_Regra_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtRegrasContagem_Regra_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtRegrasContagem_Data_Titleformat == 0 )
               {
                  context.SendWebValue( edtRegrasContagem_Data_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtRegrasContagem_Data_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtRegrasContagem_Validade_Titleformat == 0 )
               {
                  context.SendWebValue( edtRegrasContagem_Validade_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtRegrasContagem_Validade_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtRegrasContagem_Responsavel_Titleformat == 0 )
               {
                  context.SendWebValue( edtRegrasContagem_Responsavel_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtRegrasContagem_Responsavel_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", "");
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV28Update));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavUpdate_Enabled), 5, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavUpdate_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavUpdate_Tooltiptext));
               GridColumn.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavUpdate_Visible), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV29Delete));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavDelete_Enabled), 5, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavDelete_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDelete_Tooltiptext));
               GridColumn.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavDelete_Visible), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A860RegrasContagem_Regra);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtRegrasContagem_Regra_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtRegrasContagem_Regra_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtRegrasContagem_Regra_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.localUtil.Format(A861RegrasContagem_Data, "99/99/99"));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtRegrasContagem_Data_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtRegrasContagem_Data_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtRegrasContagem_Data_Link));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.localUtil.Format(A862RegrasContagem_Validade, "99/99/99"));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtRegrasContagem_Validade_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtRegrasContagem_Validade_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A863RegrasContagem_Responsavel));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtRegrasContagem_Responsavel_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtRegrasContagem_Responsavel_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 106 )
         {
            wbEnd = 0;
            nRC_GXsfl_106 = (short)(nGXsfl_106_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_103_FC2e( true) ;
         }
         else
         {
            wb_table4_103_FC2e( false) ;
         }
      }

      protected void wb_table2_8_FC2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableheader_Internalname, tblTableheader_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='GridHeaderCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblRegrascontagemtitle_Internalname, "Regras de Contagem", "", "", lblRegrascontagemtitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_WWRegrasContagem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table5_13_FC2( true) ;
         }
         else
         {
            wb_table5_13_FC2( false) ;
         }
         return  ;
      }

      protected void wb_table5_13_FC2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='OrderedByDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblOrderedtext_Internalname, "Ordenado por", "", "", lblOrderedtext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "OrderedByDescription", 0, "", 1, 1, 0, "HLP_WWRegrasContagem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 20,'',false,'" + sGXsfl_106_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavOrderedby, cmbavOrderedby_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)), 1, cmbavOrderedby_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVORDEREDBY.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,20);\"", "", true, "HLP_WWRegrasContagem.htm");
            cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", (String)(cmbavOrderedby.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'" + sGXsfl_106_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV14OrderedDsc), StringUtil.BoolToStr( AV14OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,21);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_WWRegrasContagem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableFiltersCell'>") ;
            wb_table6_23_FC2( true) ;
         }
         else
         {
            wb_table6_23_FC2( false) ;
         }
         return  ;
      }

      protected void wb_table6_23_FC2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_FC2e( true) ;
         }
         else
         {
            wb_table2_8_FC2e( false) ;
         }
      }

      protected void wb_table6_23_FC2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 26,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgCleanfilters_Internalname, context.GetImagePath( "effedd9a-25cf-4497-952f-5a5ac032b8be", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Limpar filtros", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgCleanfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOCLEANFILTERS\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWRegrasContagem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Invisible'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblFiltertextregrascontagem_areatrabalhocod_Internalname, "de Trabalho", "", "", lblFiltertextregrascontagem_areatrabalhocod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWRegrasContagem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='Invisible'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 30,'',false,'" + sGXsfl_106_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavRegrascontagem_areatrabalhocod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV37RegrasContagem_AreaTrabalhoCod), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV37RegrasContagem_AreaTrabalhoCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,30);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavRegrascontagem_areatrabalhocod_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWRegrasContagem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table7_32_FC2( true) ;
         }
         else
         {
            wb_table7_32_FC2( false) ;
         }
         return  ;
      }

      protected void wb_table7_32_FC2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblJsdynamicfilters_Internalname, lblJsdynamicfilters_Caption, "", "", lblJsdynamicfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "", 0, "", 1, 1, 1, "HLP_WWRegrasContagem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 97,'',false,'',0)\"";
            ClassString = "btn btn-success";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_search_Internalname, "gx.evt.setGridEvt("+StringUtil.Str( (decimal)(106), 3, 0)+","+"null"+");", "Procurar", bttBtn_search_Jsonclick, 5, "Procurar", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"EREFRESH."+"'", TempTags, "", context.GetButtonType( ), "HLP_WWRegrasContagem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_23_FC2e( true) ;
         }
         else
         {
            wb_table6_23_FC2e( false) ;
         }
      }

      protected void wb_table7_32_FC2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabledynamicfilters_Internalname, tblTabledynamicfilters_Internalname, "", "TableDynamicFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix1_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWRegrasContagem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 37,'',false,'" + sGXsfl_106_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector1, cmbavDynamicfiltersselector1_Internalname, StringUtil.RTrim( AV15DynamicFiltersSelector1), 1, cmbavDynamicfiltersselector1_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR1.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,37);\"", "", true, "HLP_WWRegrasContagem.htm");
            cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", (String)(cmbavDynamicfiltersselector1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle1_Internalname, "valor", "", "", lblDynamicfiltersmiddle1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWRegrasContagem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 41,'',false,'" + sGXsfl_106_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavRegrascontagem_regra1_Internalname, AV31RegrasContagem_Regra1, StringUtil.RTrim( context.localUtil.Format( AV31RegrasContagem_Regra1, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,41);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavRegrascontagem_regra1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavRegrascontagem_regra1_Visible, 1, 0, "text", "", 570, "px", 1, "row", 255, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWRegrasContagem.htm");
            wb_table8_42_FC2( true) ;
         }
         else
         {
            wb_table8_42_FC2( false) ;
         }
         return  ;
      }

      protected void wb_table8_42_FC2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 50,'',false,'" + sGXsfl_106_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavRegrascontagem_responsavel1_Internalname, StringUtil.RTrim( AV32RegrasContagem_Responsavel1), StringUtil.RTrim( context.localUtil.Format( AV32RegrasContagem_Responsavel1, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,50);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavRegrascontagem_responsavel1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavRegrascontagem_responsavel1_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWRegrasContagem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 52,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters1_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters1_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWRegrasContagem.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 53,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters1_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters1_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWRegrasContagem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow2\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix2_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWRegrasContagem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 58,'',false,'" + sGXsfl_106_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector2, cmbavDynamicfiltersselector2_Internalname, StringUtil.RTrim( AV19DynamicFiltersSelector2), 1, cmbavDynamicfiltersselector2_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR2.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,58);\"", "", true, "HLP_WWRegrasContagem.htm");
            cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV19DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", (String)(cmbavDynamicfiltersselector2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle2_Internalname, "valor", "", "", lblDynamicfiltersmiddle2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWRegrasContagem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 62,'',false,'" + sGXsfl_106_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavRegrascontagem_regra2_Internalname, AV33RegrasContagem_Regra2, StringUtil.RTrim( context.localUtil.Format( AV33RegrasContagem_Regra2, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,62);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavRegrascontagem_regra2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavRegrascontagem_regra2_Visible, 1, 0, "text", "", 570, "px", 1, "row", 255, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWRegrasContagem.htm");
            wb_table9_63_FC2( true) ;
         }
         else
         {
            wb_table9_63_FC2( false) ;
         }
         return  ;
      }

      protected void wb_table9_63_FC2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 71,'',false,'" + sGXsfl_106_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavRegrascontagem_responsavel2_Internalname, StringUtil.RTrim( AV34RegrasContagem_Responsavel2), StringUtil.RTrim( context.localUtil.Format( AV34RegrasContagem_Responsavel2, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,71);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavRegrascontagem_responsavel2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavRegrascontagem_responsavel2_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWRegrasContagem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 73,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters2_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters2_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWRegrasContagem.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 74,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters2_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters2_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWRegrasContagem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow3\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix3_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWRegrasContagem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 79,'',false,'" + sGXsfl_106_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector3, cmbavDynamicfiltersselector3_Internalname, StringUtil.RTrim( AV23DynamicFiltersSelector3), 1, cmbavDynamicfiltersselector3_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR3.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,79);\"", "", true, "HLP_WWRegrasContagem.htm");
            cmbavDynamicfiltersselector3.CurrentValue = StringUtil.RTrim( AV23DynamicFiltersSelector3);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector3_Internalname, "Values", (String)(cmbavDynamicfiltersselector3.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle3_Internalname, "valor", "", "", lblDynamicfiltersmiddle3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWRegrasContagem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 83,'',false,'" + sGXsfl_106_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavRegrascontagem_regra3_Internalname, AV35RegrasContagem_Regra3, StringUtil.RTrim( context.localUtil.Format( AV35RegrasContagem_Regra3, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,83);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavRegrascontagem_regra3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavRegrascontagem_regra3_Visible, 1, 0, "text", "", 570, "px", 1, "row", 255, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWRegrasContagem.htm");
            wb_table10_84_FC2( true) ;
         }
         else
         {
            wb_table10_84_FC2( false) ;
         }
         return  ;
      }

      protected void wb_table10_84_FC2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 92,'',false,'" + sGXsfl_106_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavRegrascontagem_responsavel3_Internalname, StringUtil.RTrim( AV36RegrasContagem_Responsavel3), StringUtil.RTrim( context.localUtil.Format( AV36RegrasContagem_Responsavel3, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,92);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavRegrascontagem_responsavel3_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavRegrascontagem_responsavel3_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWRegrasContagem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 94,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters3_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters3_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS3\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWRegrasContagem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_32_FC2e( true) ;
         }
         else
         {
            wb_table7_32_FC2e( false) ;
         }
      }

      protected void wb_table10_84_FC2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTablemergeddynamicfiltersregrascontagem_data3_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfiltersregrascontagem_data3_Internalname, tblTablemergeddynamicfiltersregrascontagem_data3_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 87,'',false,'" + sGXsfl_106_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavRegrascontagem_data3_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavRegrascontagem_data3_Internalname, context.localUtil.Format(AV24RegrasContagem_Data3, "99/99/99"), context.localUtil.Format( AV24RegrasContagem_Data3, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,87);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavRegrascontagem_data3_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWRegrasContagem.htm");
            GxWebStd.gx_bitmap( context, edtavRegrascontagem_data3_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWRegrasContagem.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersregrascontagem_data_rangemiddletext3_Internalname, "at�", "", "", lblDynamicfiltersregrascontagem_data_rangemiddletext3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWRegrasContagem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 91,'',false,'" + sGXsfl_106_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavRegrascontagem_data_to3_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavRegrascontagem_data_to3_Internalname, context.localUtil.Format(AV25RegrasContagem_Data_To3, "99/99/99"), context.localUtil.Format( AV25RegrasContagem_Data_To3, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,91);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavRegrascontagem_data_to3_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWRegrasContagem.htm");
            GxWebStd.gx_bitmap( context, edtavRegrascontagem_data_to3_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWRegrasContagem.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table10_84_FC2e( true) ;
         }
         else
         {
            wb_table10_84_FC2e( false) ;
         }
      }

      protected void wb_table9_63_FC2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTablemergeddynamicfiltersregrascontagem_data2_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfiltersregrascontagem_data2_Internalname, tblTablemergeddynamicfiltersregrascontagem_data2_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 66,'',false,'" + sGXsfl_106_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavRegrascontagem_data2_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavRegrascontagem_data2_Internalname, context.localUtil.Format(AV20RegrasContagem_Data2, "99/99/99"), context.localUtil.Format( AV20RegrasContagem_Data2, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,66);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavRegrascontagem_data2_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWRegrasContagem.htm");
            GxWebStd.gx_bitmap( context, edtavRegrascontagem_data2_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWRegrasContagem.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersregrascontagem_data_rangemiddletext2_Internalname, "at�", "", "", lblDynamicfiltersregrascontagem_data_rangemiddletext2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWRegrasContagem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 70,'',false,'" + sGXsfl_106_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavRegrascontagem_data_to2_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavRegrascontagem_data_to2_Internalname, context.localUtil.Format(AV21RegrasContagem_Data_To2, "99/99/99"), context.localUtil.Format( AV21RegrasContagem_Data_To2, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,70);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavRegrascontagem_data_to2_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWRegrasContagem.htm");
            GxWebStd.gx_bitmap( context, edtavRegrascontagem_data_to2_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWRegrasContagem.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table9_63_FC2e( true) ;
         }
         else
         {
            wb_table9_63_FC2e( false) ;
         }
      }

      protected void wb_table8_42_FC2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTablemergeddynamicfiltersregrascontagem_data1_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfiltersregrascontagem_data1_Internalname, tblTablemergeddynamicfiltersregrascontagem_data1_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 45,'',false,'" + sGXsfl_106_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavRegrascontagem_data1_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavRegrascontagem_data1_Internalname, context.localUtil.Format(AV16RegrasContagem_Data1, "99/99/99"), context.localUtil.Format( AV16RegrasContagem_Data1, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,45);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavRegrascontagem_data1_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWRegrasContagem.htm");
            GxWebStd.gx_bitmap( context, edtavRegrascontagem_data1_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWRegrasContagem.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersregrascontagem_data_rangemiddletext1_Internalname, "at�", "", "", lblDynamicfiltersregrascontagem_data_rangemiddletext1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWRegrasContagem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 49,'',false,'" + sGXsfl_106_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavRegrascontagem_data_to1_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavRegrascontagem_data_to1_Internalname, context.localUtil.Format(AV17RegrasContagem_Data_To1, "99/99/99"), context.localUtil.Format( AV17RegrasContagem_Data_To1, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,49);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavRegrascontagem_data_to1_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWRegrasContagem.htm");
            GxWebStd.gx_bitmap( context, edtavRegrascontagem_data_to1_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_WWRegrasContagem.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table8_42_FC2e( true) ;
         }
         else
         {
            wb_table8_42_FC2e( false) ;
         }
      }

      protected void wb_table5_13_FC2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "Table", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 16,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgInsert_Internalname, imgInsert_Bitmap, "", "", "", context.GetTheme( ), imgInsert_Visible, imgInsert_Enabled, "", "Inserir", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgInsert_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOINSERT\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWRegrasContagem.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_13_FC2e( true) ;
         }
         else
         {
            wb_table5_13_FC2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAFC2( ) ;
         WSFC2( ) ;
         WEFC2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20205181326726");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wwregrascontagem.js", "?20205181326726");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_1062( )
      {
         edtavUpdate_Internalname = "vUPDATE_"+sGXsfl_106_idx;
         edtavDelete_Internalname = "vDELETE_"+sGXsfl_106_idx;
         edtRegrasContagem_Regra_Internalname = "REGRASCONTAGEM_REGRA_"+sGXsfl_106_idx;
         edtRegrasContagem_Data_Internalname = "REGRASCONTAGEM_DATA_"+sGXsfl_106_idx;
         edtRegrasContagem_Validade_Internalname = "REGRASCONTAGEM_VALIDADE_"+sGXsfl_106_idx;
         edtRegrasContagem_Responsavel_Internalname = "REGRASCONTAGEM_RESPONSAVEL_"+sGXsfl_106_idx;
      }

      protected void SubsflControlProps_fel_1062( )
      {
         edtavUpdate_Internalname = "vUPDATE_"+sGXsfl_106_fel_idx;
         edtavDelete_Internalname = "vDELETE_"+sGXsfl_106_fel_idx;
         edtRegrasContagem_Regra_Internalname = "REGRASCONTAGEM_REGRA_"+sGXsfl_106_fel_idx;
         edtRegrasContagem_Data_Internalname = "REGRASCONTAGEM_DATA_"+sGXsfl_106_fel_idx;
         edtRegrasContagem_Validade_Internalname = "REGRASCONTAGEM_VALIDADE_"+sGXsfl_106_fel_idx;
         edtRegrasContagem_Responsavel_Internalname = "REGRASCONTAGEM_RESPONSAVEL_"+sGXsfl_106_fel_idx;
      }

      protected void sendrow_1062( )
      {
         SubsflControlProps_1062( ) ;
         WBFC0( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_106_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_106_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_106_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+((edtavUpdate_Visible==0) ? "display:none;" : "")+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV28Update_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV28Update))&&String.IsNullOrEmpty(StringUtil.RTrim( AV92Update_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV28Update)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavUpdate_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV28Update)) ? AV92Update_GXI : context.PathToRelativeUrl( AV28Update)),(String)edtavUpdate_Link,(String)"",(String)"",context.GetTheme( ),(int)edtavUpdate_Visible,(int)edtavUpdate_Enabled,(String)"",(String)edtavUpdate_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV28Update_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+((edtavDelete_Visible==0) ? "display:none;" : "")+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV29Delete_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV29Delete))&&String.IsNullOrEmpty(StringUtil.RTrim( AV93Delete_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV29Delete)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDelete_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV29Delete)) ? AV93Delete_GXI : context.PathToRelativeUrl( AV29Delete)),(String)edtavDelete_Link,(String)"",(String)"",context.GetTheme( ),(int)edtavDelete_Visible,(int)edtavDelete_Enabled,(String)"",(String)edtavDelete_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV29Delete_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtRegrasContagem_Regra_Internalname,(String)A860RegrasContagem_Regra,(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)edtRegrasContagem_Regra_Tooltiptext,(String)"",(String)edtRegrasContagem_Regra_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)410,(String)"px",(short)17,(String)"px",(short)255,(short)0,(short)0,(short)106,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtRegrasContagem_Data_Internalname,context.localUtil.Format(A861RegrasContagem_Data, "99/99/99"),context.localUtil.Format( A861RegrasContagem_Data, "99/99/99"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)edtRegrasContagem_Data_Link,(String)"",(String)"",(String)"",(String)edtRegrasContagem_Data_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)8,(short)0,(short)0,(short)106,(short)1,(short)-1,(short)0,(bool)true,(String)"Data",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtRegrasContagem_Validade_Internalname,context.localUtil.Format(A862RegrasContagem_Validade, "99/99/99"),context.localUtil.Format( A862RegrasContagem_Validade, "99/99/99"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtRegrasContagem_Validade_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)8,(short)0,(short)0,(short)106,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtRegrasContagem_Responsavel_Internalname,StringUtil.RTrim( A863RegrasContagem_Responsavel),StringUtil.RTrim( context.localUtil.Format( A863RegrasContagem_Responsavel, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtRegrasContagem_Responsavel_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)106,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome",(String)"left",(bool)true});
            GxWebStd.gx_hidden_field( context, "gxhash_REGRASCONTAGEM_REGRA"+"_"+sGXsfl_106_idx, GetSecureSignedToken( sGXsfl_106_idx, StringUtil.RTrim( context.localUtil.Format( A860RegrasContagem_Regra, ""))));
            GxWebStd.gx_hidden_field( context, "gxhash_REGRASCONTAGEM_DATA"+"_"+sGXsfl_106_idx, GetSecureSignedToken( sGXsfl_106_idx, A861RegrasContagem_Data));
            GxWebStd.gx_hidden_field( context, "gxhash_REGRASCONTAGEM_VALIDADE"+"_"+sGXsfl_106_idx, GetSecureSignedToken( sGXsfl_106_idx, A862RegrasContagem_Validade));
            GxWebStd.gx_hidden_field( context, "gxhash_REGRASCONTAGEM_RESPONSAVEL"+"_"+sGXsfl_106_idx, GetSecureSignedToken( sGXsfl_106_idx, StringUtil.RTrim( context.localUtil.Format( A863RegrasContagem_Responsavel, "@!"))));
            GridContainer.AddRow(GridRow);
            nGXsfl_106_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_106_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_106_idx+1));
            sGXsfl_106_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_106_idx), 4, 0)), 4, "0");
            SubsflControlProps_1062( ) ;
         }
         /* End function sendrow_1062 */
      }

      protected void init_default_properties( )
      {
         lblRegrascontagemtitle_Internalname = "REGRASCONTAGEMTITLE";
         imgInsert_Internalname = "INSERT";
         tblTableactions_Internalname = "TABLEACTIONS";
         lblOrderedtext_Internalname = "ORDEREDTEXT";
         cmbavOrderedby_Internalname = "vORDEREDBY";
         edtavOrdereddsc_Internalname = "vORDEREDDSC";
         imgCleanfilters_Internalname = "CLEANFILTERS";
         lblFiltertextregrascontagem_areatrabalhocod_Internalname = "FILTERTEXTREGRASCONTAGEM_AREATRABALHOCOD";
         edtavRegrascontagem_areatrabalhocod_Internalname = "vREGRASCONTAGEM_AREATRABALHOCOD";
         lblDynamicfiltersprefix1_Internalname = "DYNAMICFILTERSPREFIX1";
         cmbavDynamicfiltersselector1_Internalname = "vDYNAMICFILTERSSELECTOR1";
         lblDynamicfiltersmiddle1_Internalname = "DYNAMICFILTERSMIDDLE1";
         edtavRegrascontagem_regra1_Internalname = "vREGRASCONTAGEM_REGRA1";
         edtavRegrascontagem_data1_Internalname = "vREGRASCONTAGEM_DATA1";
         lblDynamicfiltersregrascontagem_data_rangemiddletext1_Internalname = "DYNAMICFILTERSREGRASCONTAGEM_DATA_RANGEMIDDLETEXT1";
         edtavRegrascontagem_data_to1_Internalname = "vREGRASCONTAGEM_DATA_TO1";
         tblTablemergeddynamicfiltersregrascontagem_data1_Internalname = "TABLEMERGEDDYNAMICFILTERSREGRASCONTAGEM_DATA1";
         edtavRegrascontagem_responsavel1_Internalname = "vREGRASCONTAGEM_RESPONSAVEL1";
         imgAdddynamicfilters1_Internalname = "ADDDYNAMICFILTERS1";
         imgRemovedynamicfilters1_Internalname = "REMOVEDYNAMICFILTERS1";
         lblDynamicfiltersprefix2_Internalname = "DYNAMICFILTERSPREFIX2";
         cmbavDynamicfiltersselector2_Internalname = "vDYNAMICFILTERSSELECTOR2";
         lblDynamicfiltersmiddle2_Internalname = "DYNAMICFILTERSMIDDLE2";
         edtavRegrascontagem_regra2_Internalname = "vREGRASCONTAGEM_REGRA2";
         edtavRegrascontagem_data2_Internalname = "vREGRASCONTAGEM_DATA2";
         lblDynamicfiltersregrascontagem_data_rangemiddletext2_Internalname = "DYNAMICFILTERSREGRASCONTAGEM_DATA_RANGEMIDDLETEXT2";
         edtavRegrascontagem_data_to2_Internalname = "vREGRASCONTAGEM_DATA_TO2";
         tblTablemergeddynamicfiltersregrascontagem_data2_Internalname = "TABLEMERGEDDYNAMICFILTERSREGRASCONTAGEM_DATA2";
         edtavRegrascontagem_responsavel2_Internalname = "vREGRASCONTAGEM_RESPONSAVEL2";
         imgAdddynamicfilters2_Internalname = "ADDDYNAMICFILTERS2";
         imgRemovedynamicfilters2_Internalname = "REMOVEDYNAMICFILTERS2";
         lblDynamicfiltersprefix3_Internalname = "DYNAMICFILTERSPREFIX3";
         cmbavDynamicfiltersselector3_Internalname = "vDYNAMICFILTERSSELECTOR3";
         lblDynamicfiltersmiddle3_Internalname = "DYNAMICFILTERSMIDDLE3";
         edtavRegrascontagem_regra3_Internalname = "vREGRASCONTAGEM_REGRA3";
         edtavRegrascontagem_data3_Internalname = "vREGRASCONTAGEM_DATA3";
         lblDynamicfiltersregrascontagem_data_rangemiddletext3_Internalname = "DYNAMICFILTERSREGRASCONTAGEM_DATA_RANGEMIDDLETEXT3";
         edtavRegrascontagem_data_to3_Internalname = "vREGRASCONTAGEM_DATA_TO3";
         tblTablemergeddynamicfiltersregrascontagem_data3_Internalname = "TABLEMERGEDDYNAMICFILTERSREGRASCONTAGEM_DATA3";
         edtavRegrascontagem_responsavel3_Internalname = "vREGRASCONTAGEM_RESPONSAVEL3";
         imgRemovedynamicfilters3_Internalname = "REMOVEDYNAMICFILTERS3";
         tblTabledynamicfilters_Internalname = "TABLEDYNAMICFILTERS";
         lblJsdynamicfilters_Internalname = "JSDYNAMICFILTERS";
         bttBtn_search_Internalname = "BTN_SEARCH";
         tblTablefilters_Internalname = "TABLEFILTERS";
         tblTableheader_Internalname = "TABLEHEADER";
         edtavUpdate_Internalname = "vUPDATE";
         edtavDelete_Internalname = "vDELETE";
         edtRegrasContagem_Regra_Internalname = "REGRASCONTAGEM_REGRA";
         edtRegrasContagem_Data_Internalname = "REGRASCONTAGEM_DATA";
         edtRegrasContagem_Validade_Internalname = "REGRASCONTAGEM_VALIDADE";
         edtRegrasContagem_Responsavel_Internalname = "REGRASCONTAGEM_RESPONSAVEL";
         Gridpaginationbar_Internalname = "GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = "GRIDTABLEWITHPAGINATIONBAR";
         tblTablegridheader_Internalname = "TABLEGRIDHEADER";
         tblTablemain_Internalname = "TABLEMAIN";
         Workwithplusutilities1_Internalname = "WORKWITHPLUSUTILITIES1";
         chkavDynamicfiltersenabled2_Internalname = "vDYNAMICFILTERSENABLED2";
         chkavDynamicfiltersenabled3_Internalname = "vDYNAMICFILTERSENABLED3";
         edtavTfregrascontagem_regra_Internalname = "vTFREGRASCONTAGEM_REGRA";
         edtavTfregrascontagem_regra_sel_Internalname = "vTFREGRASCONTAGEM_REGRA_SEL";
         edtavTfregrascontagem_data_Internalname = "vTFREGRASCONTAGEM_DATA";
         edtavTfregrascontagem_data_to_Internalname = "vTFREGRASCONTAGEM_DATA_TO";
         edtavDdo_regrascontagem_dataauxdate_Internalname = "vDDO_REGRASCONTAGEM_DATAAUXDATE";
         edtavDdo_regrascontagem_dataauxdateto_Internalname = "vDDO_REGRASCONTAGEM_DATAAUXDATETO";
         divDdo_regrascontagem_dataauxdates_Internalname = "DDO_REGRASCONTAGEM_DATAAUXDATES";
         edtavTfregrascontagem_validade_Internalname = "vTFREGRASCONTAGEM_VALIDADE";
         edtavTfregrascontagem_validade_to_Internalname = "vTFREGRASCONTAGEM_VALIDADE_TO";
         edtavDdo_regrascontagem_validadeauxdate_Internalname = "vDDO_REGRASCONTAGEM_VALIDADEAUXDATE";
         edtavDdo_regrascontagem_validadeauxdateto_Internalname = "vDDO_REGRASCONTAGEM_VALIDADEAUXDATETO";
         divDdo_regrascontagem_validadeauxdates_Internalname = "DDO_REGRASCONTAGEM_VALIDADEAUXDATES";
         edtavTfregrascontagem_responsavel_Internalname = "vTFREGRASCONTAGEM_RESPONSAVEL";
         edtavTfregrascontagem_responsavel_sel_Internalname = "vTFREGRASCONTAGEM_RESPONSAVEL_SEL";
         Ddo_regrascontagem_regra_Internalname = "DDO_REGRASCONTAGEM_REGRA";
         edtavDdo_regrascontagem_regratitlecontrolidtoreplace_Internalname = "vDDO_REGRASCONTAGEM_REGRATITLECONTROLIDTOREPLACE";
         Ddo_regrascontagem_data_Internalname = "DDO_REGRASCONTAGEM_DATA";
         edtavDdo_regrascontagem_datatitlecontrolidtoreplace_Internalname = "vDDO_REGRASCONTAGEM_DATATITLECONTROLIDTOREPLACE";
         Ddo_regrascontagem_validade_Internalname = "DDO_REGRASCONTAGEM_VALIDADE";
         edtavDdo_regrascontagem_validadetitlecontrolidtoreplace_Internalname = "vDDO_REGRASCONTAGEM_VALIDADETITLECONTROLIDTOREPLACE";
         Ddo_regrascontagem_responsavel_Internalname = "DDO_REGRASCONTAGEM_RESPONSAVEL";
         edtavDdo_regrascontagem_responsaveltitlecontrolidtoreplace_Internalname = "vDDO_REGRASCONTAGEM_RESPONSAVELTITLECONTROLIDTOREPLACE";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtRegrasContagem_Responsavel_Jsonclick = "";
         edtRegrasContagem_Validade_Jsonclick = "";
         edtRegrasContagem_Data_Jsonclick = "";
         edtRegrasContagem_Regra_Jsonclick = "";
         imgInsert_Enabled = 1;
         imgInsert_Visible = 1;
         imgInsert_Bitmap = (String)(context.GetImagePath( "5649fbb8-8ce0-4810-a5ce-bd649ea83c3a", "", context.GetTheme( )));
         edtavRegrascontagem_data_to1_Jsonclick = "";
         edtavRegrascontagem_data1_Jsonclick = "";
         edtavRegrascontagem_data_to2_Jsonclick = "";
         edtavRegrascontagem_data2_Jsonclick = "";
         edtavRegrascontagem_data_to3_Jsonclick = "";
         edtavRegrascontagem_data3_Jsonclick = "";
         edtavRegrascontagem_responsavel3_Jsonclick = "";
         edtavRegrascontagem_regra3_Jsonclick = "";
         cmbavDynamicfiltersselector3_Jsonclick = "";
         imgRemovedynamicfilters2_Visible = 1;
         imgAdddynamicfilters2_Visible = 1;
         edtavRegrascontagem_responsavel2_Jsonclick = "";
         edtavRegrascontagem_regra2_Jsonclick = "";
         cmbavDynamicfiltersselector2_Jsonclick = "";
         imgRemovedynamicfilters1_Visible = 1;
         imgAdddynamicfilters1_Visible = 1;
         edtavRegrascontagem_responsavel1_Jsonclick = "";
         edtavRegrascontagem_regra1_Jsonclick = "";
         cmbavDynamicfiltersselector1_Jsonclick = "";
         edtavRegrascontagem_areatrabalhocod_Jsonclick = "";
         edtavOrdereddsc_Jsonclick = "";
         cmbavOrderedby_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtRegrasContagem_Data_Link = "";
         edtRegrasContagem_Regra_Tooltiptext = "";
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = "";
         edtavDelete_Enabled = 1;
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = "";
         edtavUpdate_Enabled = 1;
         edtRegrasContagem_Responsavel_Titleformat = 0;
         edtRegrasContagem_Validade_Titleformat = 0;
         edtRegrasContagem_Data_Titleformat = 0;
         edtRegrasContagem_Regra_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         edtavRegrascontagem_responsavel3_Visible = 1;
         tblTablemergeddynamicfiltersregrascontagem_data3_Visible = 1;
         edtavRegrascontagem_regra3_Visible = 1;
         edtavRegrascontagem_responsavel2_Visible = 1;
         tblTablemergeddynamicfiltersregrascontagem_data2_Visible = 1;
         edtavRegrascontagem_regra2_Visible = 1;
         edtavRegrascontagem_responsavel1_Visible = 1;
         tblTablemergeddynamicfiltersregrascontagem_data1_Visible = 1;
         edtavRegrascontagem_regra1_Visible = 1;
         edtavDelete_Visible = -1;
         edtavUpdate_Visible = -1;
         edtRegrasContagem_Responsavel_Title = "Respons�vel";
         edtRegrasContagem_Validade_Title = "Validade";
         edtRegrasContagem_Data_Title = "Data";
         edtRegrasContagem_Regra_Title = "Regra";
         edtavOrdereddsc_Visible = 1;
         lblJsdynamicfilters_Caption = "JSDynamicFilters";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         chkavDynamicfiltersenabled3.Caption = "";
         chkavDynamicfiltersenabled2.Caption = "";
         edtavDdo_regrascontagem_responsaveltitlecontrolidtoreplace_Visible = 1;
         edtavDdo_regrascontagem_validadetitlecontrolidtoreplace_Visible = 1;
         edtavDdo_regrascontagem_datatitlecontrolidtoreplace_Visible = 1;
         edtavDdo_regrascontagem_regratitlecontrolidtoreplace_Visible = 1;
         edtavTfregrascontagem_responsavel_sel_Jsonclick = "";
         edtavTfregrascontagem_responsavel_sel_Visible = 1;
         edtavTfregrascontagem_responsavel_Jsonclick = "";
         edtavTfregrascontagem_responsavel_Visible = 1;
         edtavDdo_regrascontagem_validadeauxdateto_Jsonclick = "";
         edtavDdo_regrascontagem_validadeauxdate_Jsonclick = "";
         edtavTfregrascontagem_validade_to_Jsonclick = "";
         edtavTfregrascontagem_validade_to_Visible = 1;
         edtavTfregrascontagem_validade_Jsonclick = "";
         edtavTfregrascontagem_validade_Visible = 1;
         edtavDdo_regrascontagem_dataauxdateto_Jsonclick = "";
         edtavDdo_regrascontagem_dataauxdate_Jsonclick = "";
         edtavTfregrascontagem_data_to_Jsonclick = "";
         edtavTfregrascontagem_data_to_Visible = 1;
         edtavTfregrascontagem_data_Jsonclick = "";
         edtavTfregrascontagem_data_Visible = 1;
         edtavTfregrascontagem_regra_sel_Jsonclick = "";
         edtavTfregrascontagem_regra_sel_Visible = 1;
         edtavTfregrascontagem_regra_Jsonclick = "";
         edtavTfregrascontagem_regra_Visible = 1;
         chkavDynamicfiltersenabled3.Visible = 1;
         chkavDynamicfiltersenabled2.Visible = 1;
         Ddo_regrascontagem_responsavel_Searchbuttontext = "Pesquisar";
         Ddo_regrascontagem_responsavel_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_regrascontagem_responsavel_Cleanfilter = "Limpar pesquisa";
         Ddo_regrascontagem_responsavel_Loadingdata = "Carregando dados...";
         Ddo_regrascontagem_responsavel_Sortdsc = "Ordenar de Z � A";
         Ddo_regrascontagem_responsavel_Sortasc = "Ordenar de A � Z";
         Ddo_regrascontagem_responsavel_Datalistupdateminimumcharacters = 0;
         Ddo_regrascontagem_responsavel_Datalistproc = "GetWWRegrasContagemFilterData";
         Ddo_regrascontagem_responsavel_Datalisttype = "Dynamic";
         Ddo_regrascontagem_responsavel_Includedatalist = Convert.ToBoolean( -1);
         Ddo_regrascontagem_responsavel_Filterisrange = Convert.ToBoolean( 0);
         Ddo_regrascontagem_responsavel_Filtertype = "Character";
         Ddo_regrascontagem_responsavel_Includefilter = Convert.ToBoolean( -1);
         Ddo_regrascontagem_responsavel_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_regrascontagem_responsavel_Includesortasc = Convert.ToBoolean( -1);
         Ddo_regrascontagem_responsavel_Titlecontrolidtoreplace = "";
         Ddo_regrascontagem_responsavel_Dropdownoptionstype = "GridTitleSettings";
         Ddo_regrascontagem_responsavel_Cls = "ColumnSettings";
         Ddo_regrascontagem_responsavel_Tooltip = "Op��es";
         Ddo_regrascontagem_responsavel_Caption = "";
         Ddo_regrascontagem_validade_Searchbuttontext = "Pesquisar";
         Ddo_regrascontagem_validade_Rangefilterto = "At�";
         Ddo_regrascontagem_validade_Rangefilterfrom = "Desde";
         Ddo_regrascontagem_validade_Cleanfilter = "Limpar pesquisa";
         Ddo_regrascontagem_validade_Sortdsc = "Ordenar de Z � A";
         Ddo_regrascontagem_validade_Sortasc = "Ordenar de A � Z";
         Ddo_regrascontagem_validade_Includedatalist = Convert.ToBoolean( 0);
         Ddo_regrascontagem_validade_Filterisrange = Convert.ToBoolean( -1);
         Ddo_regrascontagem_validade_Filtertype = "Date";
         Ddo_regrascontagem_validade_Includefilter = Convert.ToBoolean( -1);
         Ddo_regrascontagem_validade_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_regrascontagem_validade_Includesortasc = Convert.ToBoolean( -1);
         Ddo_regrascontagem_validade_Titlecontrolidtoreplace = "";
         Ddo_regrascontagem_validade_Dropdownoptionstype = "GridTitleSettings";
         Ddo_regrascontagem_validade_Cls = "ColumnSettings";
         Ddo_regrascontagem_validade_Tooltip = "Op��es";
         Ddo_regrascontagem_validade_Caption = "";
         Ddo_regrascontagem_data_Searchbuttontext = "Pesquisar";
         Ddo_regrascontagem_data_Rangefilterto = "At�";
         Ddo_regrascontagem_data_Rangefilterfrom = "Desde";
         Ddo_regrascontagem_data_Cleanfilter = "Limpar pesquisa";
         Ddo_regrascontagem_data_Sortdsc = "Ordenar de Z � A";
         Ddo_regrascontagem_data_Sortasc = "Ordenar de A � Z";
         Ddo_regrascontagem_data_Includedatalist = Convert.ToBoolean( 0);
         Ddo_regrascontagem_data_Filterisrange = Convert.ToBoolean( -1);
         Ddo_regrascontagem_data_Filtertype = "Date";
         Ddo_regrascontagem_data_Includefilter = Convert.ToBoolean( -1);
         Ddo_regrascontagem_data_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_regrascontagem_data_Includesortasc = Convert.ToBoolean( -1);
         Ddo_regrascontagem_data_Titlecontrolidtoreplace = "";
         Ddo_regrascontagem_data_Dropdownoptionstype = "GridTitleSettings";
         Ddo_regrascontagem_data_Cls = "ColumnSettings";
         Ddo_regrascontagem_data_Tooltip = "Op��es";
         Ddo_regrascontagem_data_Caption = "";
         Ddo_regrascontagem_regra_Searchbuttontext = "Pesquisar";
         Ddo_regrascontagem_regra_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_regrascontagem_regra_Cleanfilter = "Limpar pesquisa";
         Ddo_regrascontagem_regra_Loadingdata = "Carregando dados...";
         Ddo_regrascontagem_regra_Sortdsc = "Ordenar de Z � A";
         Ddo_regrascontagem_regra_Sortasc = "Ordenar de A � Z";
         Ddo_regrascontagem_regra_Datalistupdateminimumcharacters = 0;
         Ddo_regrascontagem_regra_Datalistproc = "GetWWRegrasContagemFilterData";
         Ddo_regrascontagem_regra_Datalisttype = "Dynamic";
         Ddo_regrascontagem_regra_Includedatalist = Convert.ToBoolean( -1);
         Ddo_regrascontagem_regra_Filterisrange = Convert.ToBoolean( 0);
         Ddo_regrascontagem_regra_Filtertype = "Character";
         Ddo_regrascontagem_regra_Includefilter = Convert.ToBoolean( -1);
         Ddo_regrascontagem_regra_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_regrascontagem_regra_Includesortasc = Convert.ToBoolean( -1);
         Ddo_regrascontagem_regra_Titlecontrolidtoreplace = "";
         Ddo_regrascontagem_regra_Dropdownoptionstype = "GridTitleSettings";
         Ddo_regrascontagem_regra_Cls = "ColumnSettings";
         Ddo_regrascontagem_regra_Tooltip = "Op��es";
         Ddo_regrascontagem_regra_Caption = "";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = " Regras de Contagem";
         subGrid_Rows = 0;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A860RegrasContagem_Regra',fld:'REGRASCONTAGEM_REGRA',pic:'',hsh:true,nv:''},{av:'A864RegrasContagem_Descricao',fld:'REGRASCONTAGEM_DESCRICAO',pic:'',nv:''},{av:'AV43ddo_RegrasContagem_RegraTitleControlIdToReplace',fld:'vDDO_REGRASCONTAGEM_REGRATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_RegrasContagem_DataTitleControlIdToReplace',fld:'vDDO_REGRASCONTAGEM_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV55ddo_RegrasContagem_ValidadeTitleControlIdToReplace',fld:'vDDO_REGRASCONTAGEM_VALIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV59ddo_RegrasContagem_ResponsavelTitleControlIdToReplace',fld:'vDDO_REGRASCONTAGEM_RESPONSAVELTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37RegrasContagem_AreaTrabalhoCod',fld:'vREGRASCONTAGEM_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV31RegrasContagem_Regra1',fld:'vREGRASCONTAGEM_REGRA1',pic:'',nv:''},{av:'AV16RegrasContagem_Data1',fld:'vREGRASCONTAGEM_DATA1',pic:'',nv:''},{av:'AV17RegrasContagem_Data_To1',fld:'vREGRASCONTAGEM_DATA_TO1',pic:'',nv:''},{av:'AV32RegrasContagem_Responsavel1',fld:'vREGRASCONTAGEM_RESPONSAVEL1',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV33RegrasContagem_Regra2',fld:'vREGRASCONTAGEM_REGRA2',pic:'',nv:''},{av:'AV20RegrasContagem_Data2',fld:'vREGRASCONTAGEM_DATA2',pic:'',nv:''},{av:'AV21RegrasContagem_Data_To2',fld:'vREGRASCONTAGEM_DATA_TO2',pic:'',nv:''},{av:'AV34RegrasContagem_Responsavel2',fld:'vREGRASCONTAGEM_RESPONSAVEL2',pic:'@!',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV35RegrasContagem_Regra3',fld:'vREGRASCONTAGEM_REGRA3',pic:'',nv:''},{av:'AV24RegrasContagem_Data3',fld:'vREGRASCONTAGEM_DATA3',pic:'',nv:''},{av:'AV25RegrasContagem_Data_To3',fld:'vREGRASCONTAGEM_DATA_TO3',pic:'',nv:''},{av:'AV36RegrasContagem_Responsavel3',fld:'vREGRASCONTAGEM_RESPONSAVEL3',pic:'@!',nv:''},{av:'AV41TFRegrasContagem_Regra',fld:'vTFREGRASCONTAGEM_REGRA',pic:'',nv:''},{av:'AV42TFRegrasContagem_Regra_Sel',fld:'vTFREGRASCONTAGEM_REGRA_SEL',pic:'',nv:''},{av:'AV45TFRegrasContagem_Data',fld:'vTFREGRASCONTAGEM_DATA',pic:'',nv:''},{av:'AV46TFRegrasContagem_Data_To',fld:'vTFREGRASCONTAGEM_DATA_TO',pic:'',nv:''},{av:'AV51TFRegrasContagem_Validade',fld:'vTFREGRASCONTAGEM_VALIDADE',pic:'',nv:''},{av:'AV52TFRegrasContagem_Validade_To',fld:'vTFREGRASCONTAGEM_VALIDADE_TO',pic:'',nv:''},{av:'AV57TFRegrasContagem_Responsavel',fld:'vTFREGRASCONTAGEM_RESPONSAVEL',pic:'@!',nv:''},{av:'AV58TFRegrasContagem_Responsavel_Sel',fld:'vTFREGRASCONTAGEM_RESPONSAVEL_SEL',pic:'@!',nv:''},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV94Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV40RegrasContagem_RegraTitleFilterData',fld:'vREGRASCONTAGEM_REGRATITLEFILTERDATA',pic:'',nv:null},{av:'AV44RegrasContagem_DataTitleFilterData',fld:'vREGRASCONTAGEM_DATATITLEFILTERDATA',pic:'',nv:null},{av:'AV50RegrasContagem_ValidadeTitleFilterData',fld:'vREGRASCONTAGEM_VALIDADETITLEFILTERDATA',pic:'',nv:null},{av:'AV56RegrasContagem_ResponsavelTitleFilterData',fld:'vREGRASCONTAGEM_RESPONSAVELTITLEFILTERDATA',pic:'',nv:null},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'edtRegrasContagem_Regra_Titleformat',ctrl:'REGRASCONTAGEM_REGRA',prop:'Titleformat'},{av:'edtRegrasContagem_Regra_Title',ctrl:'REGRASCONTAGEM_REGRA',prop:'Title'},{av:'edtRegrasContagem_Data_Titleformat',ctrl:'REGRASCONTAGEM_DATA',prop:'Titleformat'},{av:'edtRegrasContagem_Data_Title',ctrl:'REGRASCONTAGEM_DATA',prop:'Title'},{av:'edtRegrasContagem_Validade_Titleformat',ctrl:'REGRASCONTAGEM_VALIDADE',prop:'Titleformat'},{av:'edtRegrasContagem_Validade_Title',ctrl:'REGRASCONTAGEM_VALIDADE',prop:'Title'},{av:'edtRegrasContagem_Responsavel_Titleformat',ctrl:'REGRASCONTAGEM_RESPONSAVEL',prop:'Titleformat'},{av:'edtRegrasContagem_Responsavel_Title',ctrl:'REGRASCONTAGEM_RESPONSAVEL',prop:'Title'},{av:'AV62GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV63GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'imgInsert_Visible',ctrl:'INSERT',prop:'Visible'},{av:'edtavUpdate_Visible',ctrl:'vUPDATE',prop:'Visible'},{av:'edtavDelete_Visible',ctrl:'vDELETE',prop:'Visible'},{av:'imgInsert_Link',ctrl:'INSERT',prop:'Link'},{av:'imgInsert_Enabled',ctrl:'INSERT',prop:'Enabled'},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E11FC2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV31RegrasContagem_Regra1',fld:'vREGRASCONTAGEM_REGRA1',pic:'',nv:''},{av:'AV16RegrasContagem_Data1',fld:'vREGRASCONTAGEM_DATA1',pic:'',nv:''},{av:'AV17RegrasContagem_Data_To1',fld:'vREGRASCONTAGEM_DATA_TO1',pic:'',nv:''},{av:'AV32RegrasContagem_Responsavel1',fld:'vREGRASCONTAGEM_RESPONSAVEL1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV33RegrasContagem_Regra2',fld:'vREGRASCONTAGEM_REGRA2',pic:'',nv:''},{av:'AV20RegrasContagem_Data2',fld:'vREGRASCONTAGEM_DATA2',pic:'',nv:''},{av:'AV21RegrasContagem_Data_To2',fld:'vREGRASCONTAGEM_DATA_TO2',pic:'',nv:''},{av:'AV34RegrasContagem_Responsavel2',fld:'vREGRASCONTAGEM_RESPONSAVEL2',pic:'@!',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV35RegrasContagem_Regra3',fld:'vREGRASCONTAGEM_REGRA3',pic:'',nv:''},{av:'AV24RegrasContagem_Data3',fld:'vREGRASCONTAGEM_DATA3',pic:'',nv:''},{av:'AV25RegrasContagem_Data_To3',fld:'vREGRASCONTAGEM_DATA_TO3',pic:'',nv:''},{av:'AV36RegrasContagem_Responsavel3',fld:'vREGRASCONTAGEM_RESPONSAVEL3',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV41TFRegrasContagem_Regra',fld:'vTFREGRASCONTAGEM_REGRA',pic:'',nv:''},{av:'AV42TFRegrasContagem_Regra_Sel',fld:'vTFREGRASCONTAGEM_REGRA_SEL',pic:'',nv:''},{av:'AV45TFRegrasContagem_Data',fld:'vTFREGRASCONTAGEM_DATA',pic:'',nv:''},{av:'AV46TFRegrasContagem_Data_To',fld:'vTFREGRASCONTAGEM_DATA_TO',pic:'',nv:''},{av:'AV51TFRegrasContagem_Validade',fld:'vTFREGRASCONTAGEM_VALIDADE',pic:'',nv:''},{av:'AV52TFRegrasContagem_Validade_To',fld:'vTFREGRASCONTAGEM_VALIDADE_TO',pic:'',nv:''},{av:'AV57TFRegrasContagem_Responsavel',fld:'vTFREGRASCONTAGEM_RESPONSAVEL',pic:'@!',nv:''},{av:'AV58TFRegrasContagem_Responsavel_Sel',fld:'vTFREGRASCONTAGEM_RESPONSAVEL_SEL',pic:'@!',nv:''},{av:'AV43ddo_RegrasContagem_RegraTitleControlIdToReplace',fld:'vDDO_REGRASCONTAGEM_REGRATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_RegrasContagem_DataTitleControlIdToReplace',fld:'vDDO_REGRASCONTAGEM_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV55ddo_RegrasContagem_ValidadeTitleControlIdToReplace',fld:'vDDO_REGRASCONTAGEM_VALIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV59ddo_RegrasContagem_ResponsavelTitleControlIdToReplace',fld:'vDDO_REGRASCONTAGEM_RESPONSAVELTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37RegrasContagem_AreaTrabalhoCod',fld:'vREGRASCONTAGEM_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV94Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A860RegrasContagem_Regra',fld:'REGRASCONTAGEM_REGRA',pic:'',hsh:true,nv:''},{av:'A864RegrasContagem_Descricao',fld:'REGRASCONTAGEM_DESCRICAO',pic:'',nv:''},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("DDO_REGRASCONTAGEM_REGRA.ONOPTIONCLICKED","{handler:'E12FC2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV31RegrasContagem_Regra1',fld:'vREGRASCONTAGEM_REGRA1',pic:'',nv:''},{av:'AV16RegrasContagem_Data1',fld:'vREGRASCONTAGEM_DATA1',pic:'',nv:''},{av:'AV17RegrasContagem_Data_To1',fld:'vREGRASCONTAGEM_DATA_TO1',pic:'',nv:''},{av:'AV32RegrasContagem_Responsavel1',fld:'vREGRASCONTAGEM_RESPONSAVEL1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV33RegrasContagem_Regra2',fld:'vREGRASCONTAGEM_REGRA2',pic:'',nv:''},{av:'AV20RegrasContagem_Data2',fld:'vREGRASCONTAGEM_DATA2',pic:'',nv:''},{av:'AV21RegrasContagem_Data_To2',fld:'vREGRASCONTAGEM_DATA_TO2',pic:'',nv:''},{av:'AV34RegrasContagem_Responsavel2',fld:'vREGRASCONTAGEM_RESPONSAVEL2',pic:'@!',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV35RegrasContagem_Regra3',fld:'vREGRASCONTAGEM_REGRA3',pic:'',nv:''},{av:'AV24RegrasContagem_Data3',fld:'vREGRASCONTAGEM_DATA3',pic:'',nv:''},{av:'AV25RegrasContagem_Data_To3',fld:'vREGRASCONTAGEM_DATA_TO3',pic:'',nv:''},{av:'AV36RegrasContagem_Responsavel3',fld:'vREGRASCONTAGEM_RESPONSAVEL3',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV41TFRegrasContagem_Regra',fld:'vTFREGRASCONTAGEM_REGRA',pic:'',nv:''},{av:'AV42TFRegrasContagem_Regra_Sel',fld:'vTFREGRASCONTAGEM_REGRA_SEL',pic:'',nv:''},{av:'AV45TFRegrasContagem_Data',fld:'vTFREGRASCONTAGEM_DATA',pic:'',nv:''},{av:'AV46TFRegrasContagem_Data_To',fld:'vTFREGRASCONTAGEM_DATA_TO',pic:'',nv:''},{av:'AV51TFRegrasContagem_Validade',fld:'vTFREGRASCONTAGEM_VALIDADE',pic:'',nv:''},{av:'AV52TFRegrasContagem_Validade_To',fld:'vTFREGRASCONTAGEM_VALIDADE_TO',pic:'',nv:''},{av:'AV57TFRegrasContagem_Responsavel',fld:'vTFREGRASCONTAGEM_RESPONSAVEL',pic:'@!',nv:''},{av:'AV58TFRegrasContagem_Responsavel_Sel',fld:'vTFREGRASCONTAGEM_RESPONSAVEL_SEL',pic:'@!',nv:''},{av:'AV43ddo_RegrasContagem_RegraTitleControlIdToReplace',fld:'vDDO_REGRASCONTAGEM_REGRATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_RegrasContagem_DataTitleControlIdToReplace',fld:'vDDO_REGRASCONTAGEM_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV55ddo_RegrasContagem_ValidadeTitleControlIdToReplace',fld:'vDDO_REGRASCONTAGEM_VALIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV59ddo_RegrasContagem_ResponsavelTitleControlIdToReplace',fld:'vDDO_REGRASCONTAGEM_RESPONSAVELTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37RegrasContagem_AreaTrabalhoCod',fld:'vREGRASCONTAGEM_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV94Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A860RegrasContagem_Regra',fld:'REGRASCONTAGEM_REGRA',pic:'',hsh:true,nv:''},{av:'A864RegrasContagem_Descricao',fld:'REGRASCONTAGEM_DESCRICAO',pic:'',nv:''},{av:'Ddo_regrascontagem_regra_Activeeventkey',ctrl:'DDO_REGRASCONTAGEM_REGRA',prop:'ActiveEventKey'},{av:'Ddo_regrascontagem_regra_Filteredtext_get',ctrl:'DDO_REGRASCONTAGEM_REGRA',prop:'FilteredText_get'},{av:'Ddo_regrascontagem_regra_Selectedvalue_get',ctrl:'DDO_REGRASCONTAGEM_REGRA',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_regrascontagem_regra_Sortedstatus',ctrl:'DDO_REGRASCONTAGEM_REGRA',prop:'SortedStatus'},{av:'AV41TFRegrasContagem_Regra',fld:'vTFREGRASCONTAGEM_REGRA',pic:'',nv:''},{av:'AV42TFRegrasContagem_Regra_Sel',fld:'vTFREGRASCONTAGEM_REGRA_SEL',pic:'',nv:''},{av:'Ddo_regrascontagem_data_Sortedstatus',ctrl:'DDO_REGRASCONTAGEM_DATA',prop:'SortedStatus'},{av:'Ddo_regrascontagem_validade_Sortedstatus',ctrl:'DDO_REGRASCONTAGEM_VALIDADE',prop:'SortedStatus'},{av:'Ddo_regrascontagem_responsavel_Sortedstatus',ctrl:'DDO_REGRASCONTAGEM_RESPONSAVEL',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_REGRASCONTAGEM_DATA.ONOPTIONCLICKED","{handler:'E13FC2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV31RegrasContagem_Regra1',fld:'vREGRASCONTAGEM_REGRA1',pic:'',nv:''},{av:'AV16RegrasContagem_Data1',fld:'vREGRASCONTAGEM_DATA1',pic:'',nv:''},{av:'AV17RegrasContagem_Data_To1',fld:'vREGRASCONTAGEM_DATA_TO1',pic:'',nv:''},{av:'AV32RegrasContagem_Responsavel1',fld:'vREGRASCONTAGEM_RESPONSAVEL1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV33RegrasContagem_Regra2',fld:'vREGRASCONTAGEM_REGRA2',pic:'',nv:''},{av:'AV20RegrasContagem_Data2',fld:'vREGRASCONTAGEM_DATA2',pic:'',nv:''},{av:'AV21RegrasContagem_Data_To2',fld:'vREGRASCONTAGEM_DATA_TO2',pic:'',nv:''},{av:'AV34RegrasContagem_Responsavel2',fld:'vREGRASCONTAGEM_RESPONSAVEL2',pic:'@!',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV35RegrasContagem_Regra3',fld:'vREGRASCONTAGEM_REGRA3',pic:'',nv:''},{av:'AV24RegrasContagem_Data3',fld:'vREGRASCONTAGEM_DATA3',pic:'',nv:''},{av:'AV25RegrasContagem_Data_To3',fld:'vREGRASCONTAGEM_DATA_TO3',pic:'',nv:''},{av:'AV36RegrasContagem_Responsavel3',fld:'vREGRASCONTAGEM_RESPONSAVEL3',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV41TFRegrasContagem_Regra',fld:'vTFREGRASCONTAGEM_REGRA',pic:'',nv:''},{av:'AV42TFRegrasContagem_Regra_Sel',fld:'vTFREGRASCONTAGEM_REGRA_SEL',pic:'',nv:''},{av:'AV45TFRegrasContagem_Data',fld:'vTFREGRASCONTAGEM_DATA',pic:'',nv:''},{av:'AV46TFRegrasContagem_Data_To',fld:'vTFREGRASCONTAGEM_DATA_TO',pic:'',nv:''},{av:'AV51TFRegrasContagem_Validade',fld:'vTFREGRASCONTAGEM_VALIDADE',pic:'',nv:''},{av:'AV52TFRegrasContagem_Validade_To',fld:'vTFREGRASCONTAGEM_VALIDADE_TO',pic:'',nv:''},{av:'AV57TFRegrasContagem_Responsavel',fld:'vTFREGRASCONTAGEM_RESPONSAVEL',pic:'@!',nv:''},{av:'AV58TFRegrasContagem_Responsavel_Sel',fld:'vTFREGRASCONTAGEM_RESPONSAVEL_SEL',pic:'@!',nv:''},{av:'AV43ddo_RegrasContagem_RegraTitleControlIdToReplace',fld:'vDDO_REGRASCONTAGEM_REGRATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_RegrasContagem_DataTitleControlIdToReplace',fld:'vDDO_REGRASCONTAGEM_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV55ddo_RegrasContagem_ValidadeTitleControlIdToReplace',fld:'vDDO_REGRASCONTAGEM_VALIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV59ddo_RegrasContagem_ResponsavelTitleControlIdToReplace',fld:'vDDO_REGRASCONTAGEM_RESPONSAVELTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37RegrasContagem_AreaTrabalhoCod',fld:'vREGRASCONTAGEM_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV94Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A860RegrasContagem_Regra',fld:'REGRASCONTAGEM_REGRA',pic:'',hsh:true,nv:''},{av:'A864RegrasContagem_Descricao',fld:'REGRASCONTAGEM_DESCRICAO',pic:'',nv:''},{av:'Ddo_regrascontagem_data_Activeeventkey',ctrl:'DDO_REGRASCONTAGEM_DATA',prop:'ActiveEventKey'},{av:'Ddo_regrascontagem_data_Filteredtext_get',ctrl:'DDO_REGRASCONTAGEM_DATA',prop:'FilteredText_get'},{av:'Ddo_regrascontagem_data_Filteredtextto_get',ctrl:'DDO_REGRASCONTAGEM_DATA',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_regrascontagem_data_Sortedstatus',ctrl:'DDO_REGRASCONTAGEM_DATA',prop:'SortedStatus'},{av:'AV45TFRegrasContagem_Data',fld:'vTFREGRASCONTAGEM_DATA',pic:'',nv:''},{av:'AV46TFRegrasContagem_Data_To',fld:'vTFREGRASCONTAGEM_DATA_TO',pic:'',nv:''},{av:'Ddo_regrascontagem_regra_Sortedstatus',ctrl:'DDO_REGRASCONTAGEM_REGRA',prop:'SortedStatus'},{av:'Ddo_regrascontagem_validade_Sortedstatus',ctrl:'DDO_REGRASCONTAGEM_VALIDADE',prop:'SortedStatus'},{av:'Ddo_regrascontagem_responsavel_Sortedstatus',ctrl:'DDO_REGRASCONTAGEM_RESPONSAVEL',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_REGRASCONTAGEM_VALIDADE.ONOPTIONCLICKED","{handler:'E14FC2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV31RegrasContagem_Regra1',fld:'vREGRASCONTAGEM_REGRA1',pic:'',nv:''},{av:'AV16RegrasContagem_Data1',fld:'vREGRASCONTAGEM_DATA1',pic:'',nv:''},{av:'AV17RegrasContagem_Data_To1',fld:'vREGRASCONTAGEM_DATA_TO1',pic:'',nv:''},{av:'AV32RegrasContagem_Responsavel1',fld:'vREGRASCONTAGEM_RESPONSAVEL1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV33RegrasContagem_Regra2',fld:'vREGRASCONTAGEM_REGRA2',pic:'',nv:''},{av:'AV20RegrasContagem_Data2',fld:'vREGRASCONTAGEM_DATA2',pic:'',nv:''},{av:'AV21RegrasContagem_Data_To2',fld:'vREGRASCONTAGEM_DATA_TO2',pic:'',nv:''},{av:'AV34RegrasContagem_Responsavel2',fld:'vREGRASCONTAGEM_RESPONSAVEL2',pic:'@!',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV35RegrasContagem_Regra3',fld:'vREGRASCONTAGEM_REGRA3',pic:'',nv:''},{av:'AV24RegrasContagem_Data3',fld:'vREGRASCONTAGEM_DATA3',pic:'',nv:''},{av:'AV25RegrasContagem_Data_To3',fld:'vREGRASCONTAGEM_DATA_TO3',pic:'',nv:''},{av:'AV36RegrasContagem_Responsavel3',fld:'vREGRASCONTAGEM_RESPONSAVEL3',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV41TFRegrasContagem_Regra',fld:'vTFREGRASCONTAGEM_REGRA',pic:'',nv:''},{av:'AV42TFRegrasContagem_Regra_Sel',fld:'vTFREGRASCONTAGEM_REGRA_SEL',pic:'',nv:''},{av:'AV45TFRegrasContagem_Data',fld:'vTFREGRASCONTAGEM_DATA',pic:'',nv:''},{av:'AV46TFRegrasContagem_Data_To',fld:'vTFREGRASCONTAGEM_DATA_TO',pic:'',nv:''},{av:'AV51TFRegrasContagem_Validade',fld:'vTFREGRASCONTAGEM_VALIDADE',pic:'',nv:''},{av:'AV52TFRegrasContagem_Validade_To',fld:'vTFREGRASCONTAGEM_VALIDADE_TO',pic:'',nv:''},{av:'AV57TFRegrasContagem_Responsavel',fld:'vTFREGRASCONTAGEM_RESPONSAVEL',pic:'@!',nv:''},{av:'AV58TFRegrasContagem_Responsavel_Sel',fld:'vTFREGRASCONTAGEM_RESPONSAVEL_SEL',pic:'@!',nv:''},{av:'AV43ddo_RegrasContagem_RegraTitleControlIdToReplace',fld:'vDDO_REGRASCONTAGEM_REGRATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_RegrasContagem_DataTitleControlIdToReplace',fld:'vDDO_REGRASCONTAGEM_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV55ddo_RegrasContagem_ValidadeTitleControlIdToReplace',fld:'vDDO_REGRASCONTAGEM_VALIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV59ddo_RegrasContagem_ResponsavelTitleControlIdToReplace',fld:'vDDO_REGRASCONTAGEM_RESPONSAVELTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37RegrasContagem_AreaTrabalhoCod',fld:'vREGRASCONTAGEM_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV94Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A860RegrasContagem_Regra',fld:'REGRASCONTAGEM_REGRA',pic:'',hsh:true,nv:''},{av:'A864RegrasContagem_Descricao',fld:'REGRASCONTAGEM_DESCRICAO',pic:'',nv:''},{av:'Ddo_regrascontagem_validade_Activeeventkey',ctrl:'DDO_REGRASCONTAGEM_VALIDADE',prop:'ActiveEventKey'},{av:'Ddo_regrascontagem_validade_Filteredtext_get',ctrl:'DDO_REGRASCONTAGEM_VALIDADE',prop:'FilteredText_get'},{av:'Ddo_regrascontagem_validade_Filteredtextto_get',ctrl:'DDO_REGRASCONTAGEM_VALIDADE',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_regrascontagem_validade_Sortedstatus',ctrl:'DDO_REGRASCONTAGEM_VALIDADE',prop:'SortedStatus'},{av:'AV51TFRegrasContagem_Validade',fld:'vTFREGRASCONTAGEM_VALIDADE',pic:'',nv:''},{av:'AV52TFRegrasContagem_Validade_To',fld:'vTFREGRASCONTAGEM_VALIDADE_TO',pic:'',nv:''},{av:'Ddo_regrascontagem_regra_Sortedstatus',ctrl:'DDO_REGRASCONTAGEM_REGRA',prop:'SortedStatus'},{av:'Ddo_regrascontagem_data_Sortedstatus',ctrl:'DDO_REGRASCONTAGEM_DATA',prop:'SortedStatus'},{av:'Ddo_regrascontagem_responsavel_Sortedstatus',ctrl:'DDO_REGRASCONTAGEM_RESPONSAVEL',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_REGRASCONTAGEM_RESPONSAVEL.ONOPTIONCLICKED","{handler:'E15FC2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV31RegrasContagem_Regra1',fld:'vREGRASCONTAGEM_REGRA1',pic:'',nv:''},{av:'AV16RegrasContagem_Data1',fld:'vREGRASCONTAGEM_DATA1',pic:'',nv:''},{av:'AV17RegrasContagem_Data_To1',fld:'vREGRASCONTAGEM_DATA_TO1',pic:'',nv:''},{av:'AV32RegrasContagem_Responsavel1',fld:'vREGRASCONTAGEM_RESPONSAVEL1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV33RegrasContagem_Regra2',fld:'vREGRASCONTAGEM_REGRA2',pic:'',nv:''},{av:'AV20RegrasContagem_Data2',fld:'vREGRASCONTAGEM_DATA2',pic:'',nv:''},{av:'AV21RegrasContagem_Data_To2',fld:'vREGRASCONTAGEM_DATA_TO2',pic:'',nv:''},{av:'AV34RegrasContagem_Responsavel2',fld:'vREGRASCONTAGEM_RESPONSAVEL2',pic:'@!',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV35RegrasContagem_Regra3',fld:'vREGRASCONTAGEM_REGRA3',pic:'',nv:''},{av:'AV24RegrasContagem_Data3',fld:'vREGRASCONTAGEM_DATA3',pic:'',nv:''},{av:'AV25RegrasContagem_Data_To3',fld:'vREGRASCONTAGEM_DATA_TO3',pic:'',nv:''},{av:'AV36RegrasContagem_Responsavel3',fld:'vREGRASCONTAGEM_RESPONSAVEL3',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV41TFRegrasContagem_Regra',fld:'vTFREGRASCONTAGEM_REGRA',pic:'',nv:''},{av:'AV42TFRegrasContagem_Regra_Sel',fld:'vTFREGRASCONTAGEM_REGRA_SEL',pic:'',nv:''},{av:'AV45TFRegrasContagem_Data',fld:'vTFREGRASCONTAGEM_DATA',pic:'',nv:''},{av:'AV46TFRegrasContagem_Data_To',fld:'vTFREGRASCONTAGEM_DATA_TO',pic:'',nv:''},{av:'AV51TFRegrasContagem_Validade',fld:'vTFREGRASCONTAGEM_VALIDADE',pic:'',nv:''},{av:'AV52TFRegrasContagem_Validade_To',fld:'vTFREGRASCONTAGEM_VALIDADE_TO',pic:'',nv:''},{av:'AV57TFRegrasContagem_Responsavel',fld:'vTFREGRASCONTAGEM_RESPONSAVEL',pic:'@!',nv:''},{av:'AV58TFRegrasContagem_Responsavel_Sel',fld:'vTFREGRASCONTAGEM_RESPONSAVEL_SEL',pic:'@!',nv:''},{av:'AV43ddo_RegrasContagem_RegraTitleControlIdToReplace',fld:'vDDO_REGRASCONTAGEM_REGRATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_RegrasContagem_DataTitleControlIdToReplace',fld:'vDDO_REGRASCONTAGEM_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV55ddo_RegrasContagem_ValidadeTitleControlIdToReplace',fld:'vDDO_REGRASCONTAGEM_VALIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV59ddo_RegrasContagem_ResponsavelTitleControlIdToReplace',fld:'vDDO_REGRASCONTAGEM_RESPONSAVELTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37RegrasContagem_AreaTrabalhoCod',fld:'vREGRASCONTAGEM_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV94Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A860RegrasContagem_Regra',fld:'REGRASCONTAGEM_REGRA',pic:'',hsh:true,nv:''},{av:'A864RegrasContagem_Descricao',fld:'REGRASCONTAGEM_DESCRICAO',pic:'',nv:''},{av:'Ddo_regrascontagem_responsavel_Activeeventkey',ctrl:'DDO_REGRASCONTAGEM_RESPONSAVEL',prop:'ActiveEventKey'},{av:'Ddo_regrascontagem_responsavel_Filteredtext_get',ctrl:'DDO_REGRASCONTAGEM_RESPONSAVEL',prop:'FilteredText_get'},{av:'Ddo_regrascontagem_responsavel_Selectedvalue_get',ctrl:'DDO_REGRASCONTAGEM_RESPONSAVEL',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_regrascontagem_responsavel_Sortedstatus',ctrl:'DDO_REGRASCONTAGEM_RESPONSAVEL',prop:'SortedStatus'},{av:'AV57TFRegrasContagem_Responsavel',fld:'vTFREGRASCONTAGEM_RESPONSAVEL',pic:'@!',nv:''},{av:'AV58TFRegrasContagem_Responsavel_Sel',fld:'vTFREGRASCONTAGEM_RESPONSAVEL_SEL',pic:'@!',nv:''},{av:'Ddo_regrascontagem_regra_Sortedstatus',ctrl:'DDO_REGRASCONTAGEM_REGRA',prop:'SortedStatus'},{av:'Ddo_regrascontagem_data_Sortedstatus',ctrl:'DDO_REGRASCONTAGEM_DATA',prop:'SortedStatus'},{av:'Ddo_regrascontagem_validade_Sortedstatus',ctrl:'DDO_REGRASCONTAGEM_VALIDADE',prop:'SortedStatus'}]}");
         setEventMetadata("GRID.LOAD","{handler:'E29FC2',iparms:[{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A860RegrasContagem_Regra',fld:'REGRASCONTAGEM_REGRA',pic:'',hsh:true,nv:''},{av:'A864RegrasContagem_Descricao',fld:'REGRASCONTAGEM_DESCRICAO',pic:'',nv:''}],oparms:[{av:'edtavUpdate_Tooltiptext',ctrl:'vUPDATE',prop:'Tooltiptext'},{av:'edtavUpdate_Link',ctrl:'vUPDATE',prop:'Link'},{av:'AV28Update',fld:'vUPDATE',pic:'',nv:''},{av:'edtavUpdate_Enabled',ctrl:'vUPDATE',prop:'Enabled'},{av:'edtavDelete_Tooltiptext',ctrl:'vDELETE',prop:'Tooltiptext'},{av:'edtavDelete_Link',ctrl:'vDELETE',prop:'Link'},{av:'AV29Delete',fld:'vDELETE',pic:'',nv:''},{av:'edtavDelete_Enabled',ctrl:'vDELETE',prop:'Enabled'},{av:'edtRegrasContagem_Data_Link',ctrl:'REGRASCONTAGEM_DATA',prop:'Link'},{av:'edtRegrasContagem_Regra_Tooltiptext',ctrl:'REGRASCONTAGEM_REGRA',prop:'Tooltiptext'}]}");
         setEventMetadata("VORDEREDBY.CLICK","{handler:'E17FC2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV31RegrasContagem_Regra1',fld:'vREGRASCONTAGEM_REGRA1',pic:'',nv:''},{av:'AV16RegrasContagem_Data1',fld:'vREGRASCONTAGEM_DATA1',pic:'',nv:''},{av:'AV17RegrasContagem_Data_To1',fld:'vREGRASCONTAGEM_DATA_TO1',pic:'',nv:''},{av:'AV32RegrasContagem_Responsavel1',fld:'vREGRASCONTAGEM_RESPONSAVEL1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV33RegrasContagem_Regra2',fld:'vREGRASCONTAGEM_REGRA2',pic:'',nv:''},{av:'AV20RegrasContagem_Data2',fld:'vREGRASCONTAGEM_DATA2',pic:'',nv:''},{av:'AV21RegrasContagem_Data_To2',fld:'vREGRASCONTAGEM_DATA_TO2',pic:'',nv:''},{av:'AV34RegrasContagem_Responsavel2',fld:'vREGRASCONTAGEM_RESPONSAVEL2',pic:'@!',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV35RegrasContagem_Regra3',fld:'vREGRASCONTAGEM_REGRA3',pic:'',nv:''},{av:'AV24RegrasContagem_Data3',fld:'vREGRASCONTAGEM_DATA3',pic:'',nv:''},{av:'AV25RegrasContagem_Data_To3',fld:'vREGRASCONTAGEM_DATA_TO3',pic:'',nv:''},{av:'AV36RegrasContagem_Responsavel3',fld:'vREGRASCONTAGEM_RESPONSAVEL3',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV41TFRegrasContagem_Regra',fld:'vTFREGRASCONTAGEM_REGRA',pic:'',nv:''},{av:'AV42TFRegrasContagem_Regra_Sel',fld:'vTFREGRASCONTAGEM_REGRA_SEL',pic:'',nv:''},{av:'AV45TFRegrasContagem_Data',fld:'vTFREGRASCONTAGEM_DATA',pic:'',nv:''},{av:'AV46TFRegrasContagem_Data_To',fld:'vTFREGRASCONTAGEM_DATA_TO',pic:'',nv:''},{av:'AV51TFRegrasContagem_Validade',fld:'vTFREGRASCONTAGEM_VALIDADE',pic:'',nv:''},{av:'AV52TFRegrasContagem_Validade_To',fld:'vTFREGRASCONTAGEM_VALIDADE_TO',pic:'',nv:''},{av:'AV57TFRegrasContagem_Responsavel',fld:'vTFREGRASCONTAGEM_RESPONSAVEL',pic:'@!',nv:''},{av:'AV58TFRegrasContagem_Responsavel_Sel',fld:'vTFREGRASCONTAGEM_RESPONSAVEL_SEL',pic:'@!',nv:''},{av:'AV43ddo_RegrasContagem_RegraTitleControlIdToReplace',fld:'vDDO_REGRASCONTAGEM_REGRATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_RegrasContagem_DataTitleControlIdToReplace',fld:'vDDO_REGRASCONTAGEM_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV55ddo_RegrasContagem_ValidadeTitleControlIdToReplace',fld:'vDDO_REGRASCONTAGEM_VALIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV59ddo_RegrasContagem_ResponsavelTitleControlIdToReplace',fld:'vDDO_REGRASCONTAGEM_RESPONSAVELTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37RegrasContagem_AreaTrabalhoCod',fld:'vREGRASCONTAGEM_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV94Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A860RegrasContagem_Regra',fld:'REGRASCONTAGEM_REGRA',pic:'',hsh:true,nv:''},{av:'A864RegrasContagem_Descricao',fld:'REGRASCONTAGEM_DESCRICAO',pic:'',nv:''}],oparms:[]}");
         setEventMetadata("'ADDDYNAMICFILTERS1'","{handler:'E23FC2',iparms:[],oparms:[{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS1'","{handler:'E18FC2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV31RegrasContagem_Regra1',fld:'vREGRASCONTAGEM_REGRA1',pic:'',nv:''},{av:'AV16RegrasContagem_Data1',fld:'vREGRASCONTAGEM_DATA1',pic:'',nv:''},{av:'AV17RegrasContagem_Data_To1',fld:'vREGRASCONTAGEM_DATA_TO1',pic:'',nv:''},{av:'AV32RegrasContagem_Responsavel1',fld:'vREGRASCONTAGEM_RESPONSAVEL1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV33RegrasContagem_Regra2',fld:'vREGRASCONTAGEM_REGRA2',pic:'',nv:''},{av:'AV20RegrasContagem_Data2',fld:'vREGRASCONTAGEM_DATA2',pic:'',nv:''},{av:'AV21RegrasContagem_Data_To2',fld:'vREGRASCONTAGEM_DATA_TO2',pic:'',nv:''},{av:'AV34RegrasContagem_Responsavel2',fld:'vREGRASCONTAGEM_RESPONSAVEL2',pic:'@!',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV35RegrasContagem_Regra3',fld:'vREGRASCONTAGEM_REGRA3',pic:'',nv:''},{av:'AV24RegrasContagem_Data3',fld:'vREGRASCONTAGEM_DATA3',pic:'',nv:''},{av:'AV25RegrasContagem_Data_To3',fld:'vREGRASCONTAGEM_DATA_TO3',pic:'',nv:''},{av:'AV36RegrasContagem_Responsavel3',fld:'vREGRASCONTAGEM_RESPONSAVEL3',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV41TFRegrasContagem_Regra',fld:'vTFREGRASCONTAGEM_REGRA',pic:'',nv:''},{av:'AV42TFRegrasContagem_Regra_Sel',fld:'vTFREGRASCONTAGEM_REGRA_SEL',pic:'',nv:''},{av:'AV45TFRegrasContagem_Data',fld:'vTFREGRASCONTAGEM_DATA',pic:'',nv:''},{av:'AV46TFRegrasContagem_Data_To',fld:'vTFREGRASCONTAGEM_DATA_TO',pic:'',nv:''},{av:'AV51TFRegrasContagem_Validade',fld:'vTFREGRASCONTAGEM_VALIDADE',pic:'',nv:''},{av:'AV52TFRegrasContagem_Validade_To',fld:'vTFREGRASCONTAGEM_VALIDADE_TO',pic:'',nv:''},{av:'AV57TFRegrasContagem_Responsavel',fld:'vTFREGRASCONTAGEM_RESPONSAVEL',pic:'@!',nv:''},{av:'AV58TFRegrasContagem_Responsavel_Sel',fld:'vTFREGRASCONTAGEM_RESPONSAVEL_SEL',pic:'@!',nv:''},{av:'AV43ddo_RegrasContagem_RegraTitleControlIdToReplace',fld:'vDDO_REGRASCONTAGEM_REGRATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_RegrasContagem_DataTitleControlIdToReplace',fld:'vDDO_REGRASCONTAGEM_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV55ddo_RegrasContagem_ValidadeTitleControlIdToReplace',fld:'vDDO_REGRASCONTAGEM_VALIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV59ddo_RegrasContagem_ResponsavelTitleControlIdToReplace',fld:'vDDO_REGRASCONTAGEM_RESPONSAVELTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37RegrasContagem_AreaTrabalhoCod',fld:'vREGRASCONTAGEM_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV94Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A860RegrasContagem_Regra',fld:'REGRASCONTAGEM_REGRA',pic:'',hsh:true,nv:''},{av:'A864RegrasContagem_Descricao',fld:'REGRASCONTAGEM_DESCRICAO',pic:'',nv:''}],oparms:[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV33RegrasContagem_Regra2',fld:'vREGRASCONTAGEM_REGRA2',pic:'',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV35RegrasContagem_Regra3',fld:'vREGRASCONTAGEM_REGRA3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV31RegrasContagem_Regra1',fld:'vREGRASCONTAGEM_REGRA1',pic:'',nv:''},{av:'AV16RegrasContagem_Data1',fld:'vREGRASCONTAGEM_DATA1',pic:'',nv:''},{av:'AV17RegrasContagem_Data_To1',fld:'vREGRASCONTAGEM_DATA_TO1',pic:'',nv:''},{av:'AV32RegrasContagem_Responsavel1',fld:'vREGRASCONTAGEM_RESPONSAVEL1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV20RegrasContagem_Data2',fld:'vREGRASCONTAGEM_DATA2',pic:'',nv:''},{av:'AV21RegrasContagem_Data_To2',fld:'vREGRASCONTAGEM_DATA_TO2',pic:'',nv:''},{av:'AV34RegrasContagem_Responsavel2',fld:'vREGRASCONTAGEM_RESPONSAVEL2',pic:'@!',nv:''},{av:'AV24RegrasContagem_Data3',fld:'vREGRASCONTAGEM_DATA3',pic:'',nv:''},{av:'AV25RegrasContagem_Data_To3',fld:'vREGRASCONTAGEM_DATA_TO3',pic:'',nv:''},{av:'AV36RegrasContagem_Responsavel3',fld:'vREGRASCONTAGEM_RESPONSAVEL3',pic:'@!',nv:''},{av:'edtavRegrascontagem_regra2_Visible',ctrl:'vREGRASCONTAGEM_REGRA2',prop:'Visible'},{av:'tblTablemergeddynamicfiltersregrascontagem_data2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSREGRASCONTAGEM_DATA2',prop:'Visible'},{av:'edtavRegrascontagem_responsavel2_Visible',ctrl:'vREGRASCONTAGEM_RESPONSAVEL2',prop:'Visible'},{av:'edtavRegrascontagem_regra3_Visible',ctrl:'vREGRASCONTAGEM_REGRA3',prop:'Visible'},{av:'tblTablemergeddynamicfiltersregrascontagem_data3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSREGRASCONTAGEM_DATA3',prop:'Visible'},{av:'edtavRegrascontagem_responsavel3_Visible',ctrl:'vREGRASCONTAGEM_RESPONSAVEL3',prop:'Visible'},{av:'edtavRegrascontagem_regra1_Visible',ctrl:'vREGRASCONTAGEM_REGRA1',prop:'Visible'},{av:'tblTablemergeddynamicfiltersregrascontagem_data1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSREGRASCONTAGEM_DATA1',prop:'Visible'},{av:'edtavRegrascontagem_responsavel1_Visible',ctrl:'vREGRASCONTAGEM_RESPONSAVEL1',prop:'Visible'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR1.CLICK","{handler:'E24FC2',iparms:[{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''}],oparms:[{av:'edtavRegrascontagem_regra1_Visible',ctrl:'vREGRASCONTAGEM_REGRA1',prop:'Visible'},{av:'tblTablemergeddynamicfiltersregrascontagem_data1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSREGRASCONTAGEM_DATA1',prop:'Visible'},{av:'edtavRegrascontagem_responsavel1_Visible',ctrl:'vREGRASCONTAGEM_RESPONSAVEL1',prop:'Visible'}]}");
         setEventMetadata("'ADDDYNAMICFILTERS2'","{handler:'E25FC2',iparms:[],oparms:[{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS2'","{handler:'E19FC2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV31RegrasContagem_Regra1',fld:'vREGRASCONTAGEM_REGRA1',pic:'',nv:''},{av:'AV16RegrasContagem_Data1',fld:'vREGRASCONTAGEM_DATA1',pic:'',nv:''},{av:'AV17RegrasContagem_Data_To1',fld:'vREGRASCONTAGEM_DATA_TO1',pic:'',nv:''},{av:'AV32RegrasContagem_Responsavel1',fld:'vREGRASCONTAGEM_RESPONSAVEL1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV33RegrasContagem_Regra2',fld:'vREGRASCONTAGEM_REGRA2',pic:'',nv:''},{av:'AV20RegrasContagem_Data2',fld:'vREGRASCONTAGEM_DATA2',pic:'',nv:''},{av:'AV21RegrasContagem_Data_To2',fld:'vREGRASCONTAGEM_DATA_TO2',pic:'',nv:''},{av:'AV34RegrasContagem_Responsavel2',fld:'vREGRASCONTAGEM_RESPONSAVEL2',pic:'@!',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV35RegrasContagem_Regra3',fld:'vREGRASCONTAGEM_REGRA3',pic:'',nv:''},{av:'AV24RegrasContagem_Data3',fld:'vREGRASCONTAGEM_DATA3',pic:'',nv:''},{av:'AV25RegrasContagem_Data_To3',fld:'vREGRASCONTAGEM_DATA_TO3',pic:'',nv:''},{av:'AV36RegrasContagem_Responsavel3',fld:'vREGRASCONTAGEM_RESPONSAVEL3',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV41TFRegrasContagem_Regra',fld:'vTFREGRASCONTAGEM_REGRA',pic:'',nv:''},{av:'AV42TFRegrasContagem_Regra_Sel',fld:'vTFREGRASCONTAGEM_REGRA_SEL',pic:'',nv:''},{av:'AV45TFRegrasContagem_Data',fld:'vTFREGRASCONTAGEM_DATA',pic:'',nv:''},{av:'AV46TFRegrasContagem_Data_To',fld:'vTFREGRASCONTAGEM_DATA_TO',pic:'',nv:''},{av:'AV51TFRegrasContagem_Validade',fld:'vTFREGRASCONTAGEM_VALIDADE',pic:'',nv:''},{av:'AV52TFRegrasContagem_Validade_To',fld:'vTFREGRASCONTAGEM_VALIDADE_TO',pic:'',nv:''},{av:'AV57TFRegrasContagem_Responsavel',fld:'vTFREGRASCONTAGEM_RESPONSAVEL',pic:'@!',nv:''},{av:'AV58TFRegrasContagem_Responsavel_Sel',fld:'vTFREGRASCONTAGEM_RESPONSAVEL_SEL',pic:'@!',nv:''},{av:'AV43ddo_RegrasContagem_RegraTitleControlIdToReplace',fld:'vDDO_REGRASCONTAGEM_REGRATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_RegrasContagem_DataTitleControlIdToReplace',fld:'vDDO_REGRASCONTAGEM_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV55ddo_RegrasContagem_ValidadeTitleControlIdToReplace',fld:'vDDO_REGRASCONTAGEM_VALIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV59ddo_RegrasContagem_ResponsavelTitleControlIdToReplace',fld:'vDDO_REGRASCONTAGEM_RESPONSAVELTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37RegrasContagem_AreaTrabalhoCod',fld:'vREGRASCONTAGEM_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV94Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A860RegrasContagem_Regra',fld:'REGRASCONTAGEM_REGRA',pic:'',hsh:true,nv:''},{av:'A864RegrasContagem_Descricao',fld:'REGRASCONTAGEM_DESCRICAO',pic:'',nv:''}],oparms:[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV33RegrasContagem_Regra2',fld:'vREGRASCONTAGEM_REGRA2',pic:'',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV35RegrasContagem_Regra3',fld:'vREGRASCONTAGEM_REGRA3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV31RegrasContagem_Regra1',fld:'vREGRASCONTAGEM_REGRA1',pic:'',nv:''},{av:'AV16RegrasContagem_Data1',fld:'vREGRASCONTAGEM_DATA1',pic:'',nv:''},{av:'AV17RegrasContagem_Data_To1',fld:'vREGRASCONTAGEM_DATA_TO1',pic:'',nv:''},{av:'AV32RegrasContagem_Responsavel1',fld:'vREGRASCONTAGEM_RESPONSAVEL1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV20RegrasContagem_Data2',fld:'vREGRASCONTAGEM_DATA2',pic:'',nv:''},{av:'AV21RegrasContagem_Data_To2',fld:'vREGRASCONTAGEM_DATA_TO2',pic:'',nv:''},{av:'AV34RegrasContagem_Responsavel2',fld:'vREGRASCONTAGEM_RESPONSAVEL2',pic:'@!',nv:''},{av:'AV24RegrasContagem_Data3',fld:'vREGRASCONTAGEM_DATA3',pic:'',nv:''},{av:'AV25RegrasContagem_Data_To3',fld:'vREGRASCONTAGEM_DATA_TO3',pic:'',nv:''},{av:'AV36RegrasContagem_Responsavel3',fld:'vREGRASCONTAGEM_RESPONSAVEL3',pic:'@!',nv:''},{av:'edtavRegrascontagem_regra2_Visible',ctrl:'vREGRASCONTAGEM_REGRA2',prop:'Visible'},{av:'tblTablemergeddynamicfiltersregrascontagem_data2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSREGRASCONTAGEM_DATA2',prop:'Visible'},{av:'edtavRegrascontagem_responsavel2_Visible',ctrl:'vREGRASCONTAGEM_RESPONSAVEL2',prop:'Visible'},{av:'edtavRegrascontagem_regra3_Visible',ctrl:'vREGRASCONTAGEM_REGRA3',prop:'Visible'},{av:'tblTablemergeddynamicfiltersregrascontagem_data3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSREGRASCONTAGEM_DATA3',prop:'Visible'},{av:'edtavRegrascontagem_responsavel3_Visible',ctrl:'vREGRASCONTAGEM_RESPONSAVEL3',prop:'Visible'},{av:'edtavRegrascontagem_regra1_Visible',ctrl:'vREGRASCONTAGEM_REGRA1',prop:'Visible'},{av:'tblTablemergeddynamicfiltersregrascontagem_data1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSREGRASCONTAGEM_DATA1',prop:'Visible'},{av:'edtavRegrascontagem_responsavel1_Visible',ctrl:'vREGRASCONTAGEM_RESPONSAVEL1',prop:'Visible'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR2.CLICK","{handler:'E26FC2',iparms:[{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''}],oparms:[{av:'edtavRegrascontagem_regra2_Visible',ctrl:'vREGRASCONTAGEM_REGRA2',prop:'Visible'},{av:'tblTablemergeddynamicfiltersregrascontagem_data2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSREGRASCONTAGEM_DATA2',prop:'Visible'},{av:'edtavRegrascontagem_responsavel2_Visible',ctrl:'vREGRASCONTAGEM_RESPONSAVEL2',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS3'","{handler:'E20FC2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV31RegrasContagem_Regra1',fld:'vREGRASCONTAGEM_REGRA1',pic:'',nv:''},{av:'AV16RegrasContagem_Data1',fld:'vREGRASCONTAGEM_DATA1',pic:'',nv:''},{av:'AV17RegrasContagem_Data_To1',fld:'vREGRASCONTAGEM_DATA_TO1',pic:'',nv:''},{av:'AV32RegrasContagem_Responsavel1',fld:'vREGRASCONTAGEM_RESPONSAVEL1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV33RegrasContagem_Regra2',fld:'vREGRASCONTAGEM_REGRA2',pic:'',nv:''},{av:'AV20RegrasContagem_Data2',fld:'vREGRASCONTAGEM_DATA2',pic:'',nv:''},{av:'AV21RegrasContagem_Data_To2',fld:'vREGRASCONTAGEM_DATA_TO2',pic:'',nv:''},{av:'AV34RegrasContagem_Responsavel2',fld:'vREGRASCONTAGEM_RESPONSAVEL2',pic:'@!',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV35RegrasContagem_Regra3',fld:'vREGRASCONTAGEM_REGRA3',pic:'',nv:''},{av:'AV24RegrasContagem_Data3',fld:'vREGRASCONTAGEM_DATA3',pic:'',nv:''},{av:'AV25RegrasContagem_Data_To3',fld:'vREGRASCONTAGEM_DATA_TO3',pic:'',nv:''},{av:'AV36RegrasContagem_Responsavel3',fld:'vREGRASCONTAGEM_RESPONSAVEL3',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV41TFRegrasContagem_Regra',fld:'vTFREGRASCONTAGEM_REGRA',pic:'',nv:''},{av:'AV42TFRegrasContagem_Regra_Sel',fld:'vTFREGRASCONTAGEM_REGRA_SEL',pic:'',nv:''},{av:'AV45TFRegrasContagem_Data',fld:'vTFREGRASCONTAGEM_DATA',pic:'',nv:''},{av:'AV46TFRegrasContagem_Data_To',fld:'vTFREGRASCONTAGEM_DATA_TO',pic:'',nv:''},{av:'AV51TFRegrasContagem_Validade',fld:'vTFREGRASCONTAGEM_VALIDADE',pic:'',nv:''},{av:'AV52TFRegrasContagem_Validade_To',fld:'vTFREGRASCONTAGEM_VALIDADE_TO',pic:'',nv:''},{av:'AV57TFRegrasContagem_Responsavel',fld:'vTFREGRASCONTAGEM_RESPONSAVEL',pic:'@!',nv:''},{av:'AV58TFRegrasContagem_Responsavel_Sel',fld:'vTFREGRASCONTAGEM_RESPONSAVEL_SEL',pic:'@!',nv:''},{av:'AV43ddo_RegrasContagem_RegraTitleControlIdToReplace',fld:'vDDO_REGRASCONTAGEM_REGRATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_RegrasContagem_DataTitleControlIdToReplace',fld:'vDDO_REGRASCONTAGEM_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV55ddo_RegrasContagem_ValidadeTitleControlIdToReplace',fld:'vDDO_REGRASCONTAGEM_VALIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV59ddo_RegrasContagem_ResponsavelTitleControlIdToReplace',fld:'vDDO_REGRASCONTAGEM_RESPONSAVELTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37RegrasContagem_AreaTrabalhoCod',fld:'vREGRASCONTAGEM_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV94Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A860RegrasContagem_Regra',fld:'REGRASCONTAGEM_REGRA',pic:'',hsh:true,nv:''},{av:'A864RegrasContagem_Descricao',fld:'REGRASCONTAGEM_DESCRICAO',pic:'',nv:''}],oparms:[{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV33RegrasContagem_Regra2',fld:'vREGRASCONTAGEM_REGRA2',pic:'',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV35RegrasContagem_Regra3',fld:'vREGRASCONTAGEM_REGRA3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV31RegrasContagem_Regra1',fld:'vREGRASCONTAGEM_REGRA1',pic:'',nv:''},{av:'AV16RegrasContagem_Data1',fld:'vREGRASCONTAGEM_DATA1',pic:'',nv:''},{av:'AV17RegrasContagem_Data_To1',fld:'vREGRASCONTAGEM_DATA_TO1',pic:'',nv:''},{av:'AV32RegrasContagem_Responsavel1',fld:'vREGRASCONTAGEM_RESPONSAVEL1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV20RegrasContagem_Data2',fld:'vREGRASCONTAGEM_DATA2',pic:'',nv:''},{av:'AV21RegrasContagem_Data_To2',fld:'vREGRASCONTAGEM_DATA_TO2',pic:'',nv:''},{av:'AV34RegrasContagem_Responsavel2',fld:'vREGRASCONTAGEM_RESPONSAVEL2',pic:'@!',nv:''},{av:'AV24RegrasContagem_Data3',fld:'vREGRASCONTAGEM_DATA3',pic:'',nv:''},{av:'AV25RegrasContagem_Data_To3',fld:'vREGRASCONTAGEM_DATA_TO3',pic:'',nv:''},{av:'AV36RegrasContagem_Responsavel3',fld:'vREGRASCONTAGEM_RESPONSAVEL3',pic:'@!',nv:''},{av:'edtavRegrascontagem_regra2_Visible',ctrl:'vREGRASCONTAGEM_REGRA2',prop:'Visible'},{av:'tblTablemergeddynamicfiltersregrascontagem_data2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSREGRASCONTAGEM_DATA2',prop:'Visible'},{av:'edtavRegrascontagem_responsavel2_Visible',ctrl:'vREGRASCONTAGEM_RESPONSAVEL2',prop:'Visible'},{av:'edtavRegrascontagem_regra3_Visible',ctrl:'vREGRASCONTAGEM_REGRA3',prop:'Visible'},{av:'tblTablemergeddynamicfiltersregrascontagem_data3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSREGRASCONTAGEM_DATA3',prop:'Visible'},{av:'edtavRegrascontagem_responsavel3_Visible',ctrl:'vREGRASCONTAGEM_RESPONSAVEL3',prop:'Visible'},{av:'edtavRegrascontagem_regra1_Visible',ctrl:'vREGRASCONTAGEM_REGRA1',prop:'Visible'},{av:'tblTablemergeddynamicfiltersregrascontagem_data1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSREGRASCONTAGEM_DATA1',prop:'Visible'},{av:'edtavRegrascontagem_responsavel1_Visible',ctrl:'vREGRASCONTAGEM_RESPONSAVEL1',prop:'Visible'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR3.CLICK","{handler:'E27FC2',iparms:[{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''}],oparms:[{av:'edtavRegrascontagem_regra3_Visible',ctrl:'vREGRASCONTAGEM_REGRA3',prop:'Visible'},{av:'tblTablemergeddynamicfiltersregrascontagem_data3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSREGRASCONTAGEM_DATA3',prop:'Visible'},{av:'edtavRegrascontagem_responsavel3_Visible',ctrl:'vREGRASCONTAGEM_RESPONSAVEL3',prop:'Visible'}]}");
         setEventMetadata("'DOCLEANFILTERS'","{handler:'E21FC2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV31RegrasContagem_Regra1',fld:'vREGRASCONTAGEM_REGRA1',pic:'',nv:''},{av:'AV16RegrasContagem_Data1',fld:'vREGRASCONTAGEM_DATA1',pic:'',nv:''},{av:'AV17RegrasContagem_Data_To1',fld:'vREGRASCONTAGEM_DATA_TO1',pic:'',nv:''},{av:'AV32RegrasContagem_Responsavel1',fld:'vREGRASCONTAGEM_RESPONSAVEL1',pic:'@!',nv:''},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV33RegrasContagem_Regra2',fld:'vREGRASCONTAGEM_REGRA2',pic:'',nv:''},{av:'AV20RegrasContagem_Data2',fld:'vREGRASCONTAGEM_DATA2',pic:'',nv:''},{av:'AV21RegrasContagem_Data_To2',fld:'vREGRASCONTAGEM_DATA_TO2',pic:'',nv:''},{av:'AV34RegrasContagem_Responsavel2',fld:'vREGRASCONTAGEM_RESPONSAVEL2',pic:'@!',nv:''},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV35RegrasContagem_Regra3',fld:'vREGRASCONTAGEM_REGRA3',pic:'',nv:''},{av:'AV24RegrasContagem_Data3',fld:'vREGRASCONTAGEM_DATA3',pic:'',nv:''},{av:'AV25RegrasContagem_Data_To3',fld:'vREGRASCONTAGEM_DATA_TO3',pic:'',nv:''},{av:'AV36RegrasContagem_Responsavel3',fld:'vREGRASCONTAGEM_RESPONSAVEL3',pic:'@!',nv:''},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV41TFRegrasContagem_Regra',fld:'vTFREGRASCONTAGEM_REGRA',pic:'',nv:''},{av:'AV42TFRegrasContagem_Regra_Sel',fld:'vTFREGRASCONTAGEM_REGRA_SEL',pic:'',nv:''},{av:'AV45TFRegrasContagem_Data',fld:'vTFREGRASCONTAGEM_DATA',pic:'',nv:''},{av:'AV46TFRegrasContagem_Data_To',fld:'vTFREGRASCONTAGEM_DATA_TO',pic:'',nv:''},{av:'AV51TFRegrasContagem_Validade',fld:'vTFREGRASCONTAGEM_VALIDADE',pic:'',nv:''},{av:'AV52TFRegrasContagem_Validade_To',fld:'vTFREGRASCONTAGEM_VALIDADE_TO',pic:'',nv:''},{av:'AV57TFRegrasContagem_Responsavel',fld:'vTFREGRASCONTAGEM_RESPONSAVEL',pic:'@!',nv:''},{av:'AV58TFRegrasContagem_Responsavel_Sel',fld:'vTFREGRASCONTAGEM_RESPONSAVEL_SEL',pic:'@!',nv:''},{av:'AV43ddo_RegrasContagem_RegraTitleControlIdToReplace',fld:'vDDO_REGRASCONTAGEM_REGRATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_RegrasContagem_DataTitleControlIdToReplace',fld:'vDDO_REGRASCONTAGEM_DATATITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV55ddo_RegrasContagem_ValidadeTitleControlIdToReplace',fld:'vDDO_REGRASCONTAGEM_VALIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV59ddo_RegrasContagem_ResponsavelTitleControlIdToReplace',fld:'vDDO_REGRASCONTAGEM_RESPONSAVELTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37RegrasContagem_AreaTrabalhoCod',fld:'vREGRASCONTAGEM_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV94Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV27DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV26DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A860RegrasContagem_Regra',fld:'REGRASCONTAGEM_REGRA',pic:'',hsh:true,nv:''},{av:'A864RegrasContagem_Descricao',fld:'REGRASCONTAGEM_DESCRICAO',pic:'',nv:''}],oparms:[{av:'AV37RegrasContagem_AreaTrabalhoCod',fld:'vREGRASCONTAGEM_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV41TFRegrasContagem_Regra',fld:'vTFREGRASCONTAGEM_REGRA',pic:'',nv:''},{av:'Ddo_regrascontagem_regra_Filteredtext_set',ctrl:'DDO_REGRASCONTAGEM_REGRA',prop:'FilteredText_set'},{av:'AV42TFRegrasContagem_Regra_Sel',fld:'vTFREGRASCONTAGEM_REGRA_SEL',pic:'',nv:''},{av:'Ddo_regrascontagem_regra_Selectedvalue_set',ctrl:'DDO_REGRASCONTAGEM_REGRA',prop:'SelectedValue_set'},{av:'AV45TFRegrasContagem_Data',fld:'vTFREGRASCONTAGEM_DATA',pic:'',nv:''},{av:'Ddo_regrascontagem_data_Filteredtext_set',ctrl:'DDO_REGRASCONTAGEM_DATA',prop:'FilteredText_set'},{av:'AV46TFRegrasContagem_Data_To',fld:'vTFREGRASCONTAGEM_DATA_TO',pic:'',nv:''},{av:'Ddo_regrascontagem_data_Filteredtextto_set',ctrl:'DDO_REGRASCONTAGEM_DATA',prop:'FilteredTextTo_set'},{av:'AV51TFRegrasContagem_Validade',fld:'vTFREGRASCONTAGEM_VALIDADE',pic:'',nv:''},{av:'Ddo_regrascontagem_validade_Filteredtext_set',ctrl:'DDO_REGRASCONTAGEM_VALIDADE',prop:'FilteredText_set'},{av:'AV52TFRegrasContagem_Validade_To',fld:'vTFREGRASCONTAGEM_VALIDADE_TO',pic:'',nv:''},{av:'Ddo_regrascontagem_validade_Filteredtextto_set',ctrl:'DDO_REGRASCONTAGEM_VALIDADE',prop:'FilteredTextTo_set'},{av:'AV57TFRegrasContagem_Responsavel',fld:'vTFREGRASCONTAGEM_RESPONSAVEL',pic:'@!',nv:''},{av:'Ddo_regrascontagem_responsavel_Filteredtext_set',ctrl:'DDO_REGRASCONTAGEM_RESPONSAVEL',prop:'FilteredText_set'},{av:'AV58TFRegrasContagem_Responsavel_Sel',fld:'vTFREGRASCONTAGEM_RESPONSAVEL_SEL',pic:'@!',nv:''},{av:'Ddo_regrascontagem_responsavel_Selectedvalue_set',ctrl:'DDO_REGRASCONTAGEM_RESPONSAVEL',prop:'SelectedValue_set'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV31RegrasContagem_Regra1',fld:'vREGRASCONTAGEM_REGRA1',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'edtavRegrascontagem_regra1_Visible',ctrl:'vREGRASCONTAGEM_REGRA1',prop:'Visible'},{av:'tblTablemergeddynamicfiltersregrascontagem_data1_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSREGRASCONTAGEM_DATA1',prop:'Visible'},{av:'edtavRegrascontagem_responsavel1_Visible',ctrl:'vREGRASCONTAGEM_RESPONSAVEL1',prop:'Visible'},{av:'AV18DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV19DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV33RegrasContagem_Regra2',fld:'vREGRASCONTAGEM_REGRA2',pic:'',nv:''},{av:'AV22DynamicFiltersEnabled3',fld:'vDYNAMICFILTERSENABLED3',pic:'',nv:false},{av:'AV23DynamicFiltersSelector3',fld:'vDYNAMICFILTERSSELECTOR3',pic:'',nv:''},{av:'AV35RegrasContagem_Regra3',fld:'vREGRASCONTAGEM_REGRA3',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'imgAdddynamicfilters2_Visible',ctrl:'ADDDYNAMICFILTERS2',prop:'Visible'},{av:'imgRemovedynamicfilters2_Visible',ctrl:'REMOVEDYNAMICFILTERS2',prop:'Visible'},{av:'AV16RegrasContagem_Data1',fld:'vREGRASCONTAGEM_DATA1',pic:'',nv:''},{av:'AV17RegrasContagem_Data_To1',fld:'vREGRASCONTAGEM_DATA_TO1',pic:'',nv:''},{av:'AV32RegrasContagem_Responsavel1',fld:'vREGRASCONTAGEM_RESPONSAVEL1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV20RegrasContagem_Data2',fld:'vREGRASCONTAGEM_DATA2',pic:'',nv:''},{av:'AV21RegrasContagem_Data_To2',fld:'vREGRASCONTAGEM_DATA_TO2',pic:'',nv:''},{av:'AV34RegrasContagem_Responsavel2',fld:'vREGRASCONTAGEM_RESPONSAVEL2',pic:'@!',nv:''},{av:'AV24RegrasContagem_Data3',fld:'vREGRASCONTAGEM_DATA3',pic:'',nv:''},{av:'AV25RegrasContagem_Data_To3',fld:'vREGRASCONTAGEM_DATA_TO3',pic:'',nv:''},{av:'AV36RegrasContagem_Responsavel3',fld:'vREGRASCONTAGEM_RESPONSAVEL3',pic:'@!',nv:''},{av:'edtavRegrascontagem_regra2_Visible',ctrl:'vREGRASCONTAGEM_REGRA2',prop:'Visible'},{av:'tblTablemergeddynamicfiltersregrascontagem_data2_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSREGRASCONTAGEM_DATA2',prop:'Visible'},{av:'edtavRegrascontagem_responsavel2_Visible',ctrl:'vREGRASCONTAGEM_RESPONSAVEL2',prop:'Visible'},{av:'edtavRegrascontagem_regra3_Visible',ctrl:'vREGRASCONTAGEM_REGRA3',prop:'Visible'},{av:'tblTablemergeddynamicfiltersregrascontagem_data3_Visible',ctrl:'TABLEMERGEDDYNAMICFILTERSREGRASCONTAGEM_DATA3',prop:'Visible'},{av:'edtavRegrascontagem_responsavel3_Visible',ctrl:'vREGRASCONTAGEM_RESPONSAVEL3',prop:'Visible'}]}");
         setEventMetadata("'DOINSERT'","{handler:'E22FC2',iparms:[{av:'A860RegrasContagem_Regra',fld:'REGRASCONTAGEM_REGRA',pic:'',hsh:true,nv:''}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Gridpaginationbar_Selectedpage = "";
         Ddo_regrascontagem_regra_Activeeventkey = "";
         Ddo_regrascontagem_regra_Filteredtext_get = "";
         Ddo_regrascontagem_regra_Selectedvalue_get = "";
         Ddo_regrascontagem_data_Activeeventkey = "";
         Ddo_regrascontagem_data_Filteredtext_get = "";
         Ddo_regrascontagem_data_Filteredtextto_get = "";
         Ddo_regrascontagem_validade_Activeeventkey = "";
         Ddo_regrascontagem_validade_Filteredtext_get = "";
         Ddo_regrascontagem_validade_Filteredtextto_get = "";
         Ddo_regrascontagem_responsavel_Activeeventkey = "";
         Ddo_regrascontagem_responsavel_Filteredtext_get = "";
         Ddo_regrascontagem_responsavel_Selectedvalue_get = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV15DynamicFiltersSelector1 = "";
         AV31RegrasContagem_Regra1 = "";
         AV16RegrasContagem_Data1 = DateTime.MinValue;
         AV17RegrasContagem_Data_To1 = DateTime.MinValue;
         AV32RegrasContagem_Responsavel1 = "";
         AV19DynamicFiltersSelector2 = "";
         AV33RegrasContagem_Regra2 = "";
         AV20RegrasContagem_Data2 = DateTime.MinValue;
         AV21RegrasContagem_Data_To2 = DateTime.MinValue;
         AV34RegrasContagem_Responsavel2 = "";
         AV23DynamicFiltersSelector3 = "";
         AV35RegrasContagem_Regra3 = "";
         AV24RegrasContagem_Data3 = DateTime.MinValue;
         AV25RegrasContagem_Data_To3 = DateTime.MinValue;
         AV36RegrasContagem_Responsavel3 = "";
         AV41TFRegrasContagem_Regra = "";
         AV42TFRegrasContagem_Regra_Sel = "";
         AV45TFRegrasContagem_Data = DateTime.MinValue;
         AV46TFRegrasContagem_Data_To = DateTime.MinValue;
         AV51TFRegrasContagem_Validade = DateTime.MinValue;
         AV52TFRegrasContagem_Validade_To = DateTime.MinValue;
         AV57TFRegrasContagem_Responsavel = "";
         AV58TFRegrasContagem_Responsavel_Sel = "";
         AV43ddo_RegrasContagem_RegraTitleControlIdToReplace = "";
         AV49ddo_RegrasContagem_DataTitleControlIdToReplace = "";
         AV55ddo_RegrasContagem_ValidadeTitleControlIdToReplace = "";
         AV59ddo_RegrasContagem_ResponsavelTitleControlIdToReplace = "";
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV94Pgmname = "";
         AV10GridState = new wwpbaseobjects.SdtWWPGridState(context);
         A860RegrasContagem_Regra = "";
         A864RegrasContagem_Descricao = "";
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV60DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV40RegrasContagem_RegraTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV44RegrasContagem_DataTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV50RegrasContagem_ValidadeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV56RegrasContagem_ResponsavelTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         Ddo_regrascontagem_regra_Filteredtext_set = "";
         Ddo_regrascontagem_regra_Selectedvalue_set = "";
         Ddo_regrascontagem_regra_Sortedstatus = "";
         Ddo_regrascontagem_data_Filteredtext_set = "";
         Ddo_regrascontagem_data_Filteredtextto_set = "";
         Ddo_regrascontagem_data_Sortedstatus = "";
         Ddo_regrascontagem_validade_Filteredtext_set = "";
         Ddo_regrascontagem_validade_Filteredtextto_set = "";
         Ddo_regrascontagem_validade_Sortedstatus = "";
         Ddo_regrascontagem_responsavel_Filteredtext_set = "";
         Ddo_regrascontagem_responsavel_Selectedvalue_set = "";
         Ddo_regrascontagem_responsavel_Sortedstatus = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         AV47DDO_RegrasContagem_DataAuxDate = DateTime.MinValue;
         AV48DDO_RegrasContagem_DataAuxDateTo = DateTime.MinValue;
         AV53DDO_RegrasContagem_ValidadeAuxDate = DateTime.MinValue;
         AV54DDO_RegrasContagem_ValidadeAuxDateTo = DateTime.MinValue;
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV28Update = "";
         AV92Update_GXI = "";
         AV29Delete = "";
         AV93Delete_GXI = "";
         A861RegrasContagem_Data = DateTime.MinValue;
         A862RegrasContagem_Validade = DateTime.MinValue;
         A863RegrasContagem_Responsavel = "";
         GridContainer = new GXWebGrid( context);
         scmdbuf = "";
         lV68WWRegrasContagemDS_3_Regrascontagem_regra1 = "";
         lV71WWRegrasContagemDS_6_Regrascontagem_responsavel1 = "";
         lV74WWRegrasContagemDS_9_Regrascontagem_regra2 = "";
         lV77WWRegrasContagemDS_12_Regrascontagem_responsavel2 = "";
         lV80WWRegrasContagemDS_15_Regrascontagem_regra3 = "";
         lV83WWRegrasContagemDS_18_Regrascontagem_responsavel3 = "";
         lV84WWRegrasContagemDS_19_Tfregrascontagem_regra = "";
         lV90WWRegrasContagemDS_25_Tfregrascontagem_responsavel = "";
         AV67WWRegrasContagemDS_2_Dynamicfiltersselector1 = "";
         AV68WWRegrasContagemDS_3_Regrascontagem_regra1 = "";
         AV69WWRegrasContagemDS_4_Regrascontagem_data1 = DateTime.MinValue;
         AV70WWRegrasContagemDS_5_Regrascontagem_data_to1 = DateTime.MinValue;
         AV71WWRegrasContagemDS_6_Regrascontagem_responsavel1 = "";
         AV73WWRegrasContagemDS_8_Dynamicfiltersselector2 = "";
         AV74WWRegrasContagemDS_9_Regrascontagem_regra2 = "";
         AV75WWRegrasContagemDS_10_Regrascontagem_data2 = DateTime.MinValue;
         AV76WWRegrasContagemDS_11_Regrascontagem_data_to2 = DateTime.MinValue;
         AV77WWRegrasContagemDS_12_Regrascontagem_responsavel2 = "";
         AV79WWRegrasContagemDS_14_Dynamicfiltersselector3 = "";
         AV80WWRegrasContagemDS_15_Regrascontagem_regra3 = "";
         AV81WWRegrasContagemDS_16_Regrascontagem_data3 = DateTime.MinValue;
         AV82WWRegrasContagemDS_17_Regrascontagem_data_to3 = DateTime.MinValue;
         AV83WWRegrasContagemDS_18_Regrascontagem_responsavel3 = "";
         AV85WWRegrasContagemDS_20_Tfregrascontagem_regra_sel = "";
         AV84WWRegrasContagemDS_19_Tfregrascontagem_regra = "";
         AV86WWRegrasContagemDS_21_Tfregrascontagem_data = DateTime.MinValue;
         AV87WWRegrasContagemDS_22_Tfregrascontagem_data_to = DateTime.MinValue;
         AV88WWRegrasContagemDS_23_Tfregrascontagem_validade = DateTime.MinValue;
         AV89WWRegrasContagemDS_24_Tfregrascontagem_validade_to = DateTime.MinValue;
         AV91WWRegrasContagemDS_26_Tfregrascontagem_responsavel_sel = "";
         AV90WWRegrasContagemDS_25_Tfregrascontagem_responsavel = "";
         H00FC2_A865RegrasContagem_AreaTrabalhoCod = new int[1] ;
         H00FC2_A864RegrasContagem_Descricao = new String[] {""} ;
         H00FC2_A863RegrasContagem_Responsavel = new String[] {""} ;
         H00FC2_A862RegrasContagem_Validade = new DateTime[] {DateTime.MinValue} ;
         H00FC2_n862RegrasContagem_Validade = new bool[] {false} ;
         H00FC2_A861RegrasContagem_Data = new DateTime[] {DateTime.MinValue} ;
         H00FC2_A860RegrasContagem_Regra = new String[] {""} ;
         H00FC3_AGRID_nRecordCount = new long[1] ;
         imgAdddynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters1_Jsonclick = "";
         imgAdddynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters2_Jsonclick = "";
         imgRemovedynamicfilters3_Jsonclick = "";
         imgCleanfilters_Jsonclick = "";
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         GridRow = new GXWebRow();
         imgInsert_Link = "";
         AV30Session = context.GetSession();
         AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV7HTTPRequest = new GxHttpRequest( context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblRegrascontagemtitle_Jsonclick = "";
         lblOrderedtext_Jsonclick = "";
         lblFiltertextregrascontagem_areatrabalhocod_Jsonclick = "";
         lblJsdynamicfilters_Jsonclick = "";
         bttBtn_search_Jsonclick = "";
         lblDynamicfiltersprefix1_Jsonclick = "";
         lblDynamicfiltersmiddle1_Jsonclick = "";
         lblDynamicfiltersprefix2_Jsonclick = "";
         lblDynamicfiltersmiddle2_Jsonclick = "";
         lblDynamicfiltersprefix3_Jsonclick = "";
         lblDynamicfiltersmiddle3_Jsonclick = "";
         lblDynamicfiltersregrascontagem_data_rangemiddletext3_Jsonclick = "";
         lblDynamicfiltersregrascontagem_data_rangemiddletext2_Jsonclick = "";
         lblDynamicfiltersregrascontagem_data_rangemiddletext1_Jsonclick = "";
         imgInsert_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wwregrascontagem__default(),
            new Object[][] {
                new Object[] {
               H00FC2_A865RegrasContagem_AreaTrabalhoCod, H00FC2_A864RegrasContagem_Descricao, H00FC2_A863RegrasContagem_Responsavel, H00FC2_A862RegrasContagem_Validade, H00FC2_n862RegrasContagem_Validade, H00FC2_A861RegrasContagem_Data, H00FC2_A860RegrasContagem_Regra
               }
               , new Object[] {
               H00FC3_AGRID_nRecordCount
               }
            }
         );
         AV94Pgmname = "WWRegrasContagem";
         /* GeneXus formulas. */
         AV94Pgmname = "WWRegrasContagem";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_106 ;
      private short nGXsfl_106_idx=1 ;
      private short AV13OrderedBy ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_106_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short edtRegrasContagem_Regra_Titleformat ;
      private short edtRegrasContagem_Data_Titleformat ;
      private short edtRegrasContagem_Validade_Titleformat ;
      private short edtRegrasContagem_Responsavel_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int subGrid_Rows ;
      private int AV37RegrasContagem_AreaTrabalhoCod ;
      private int Gridpaginationbar_Pagestoshow ;
      private int Ddo_regrascontagem_regra_Datalistupdateminimumcharacters ;
      private int Ddo_regrascontagem_responsavel_Datalistupdateminimumcharacters ;
      private int edtavTfregrascontagem_regra_Visible ;
      private int edtavTfregrascontagem_regra_sel_Visible ;
      private int edtavTfregrascontagem_data_Visible ;
      private int edtavTfregrascontagem_data_to_Visible ;
      private int edtavTfregrascontagem_validade_Visible ;
      private int edtavTfregrascontagem_validade_to_Visible ;
      private int edtavTfregrascontagem_responsavel_Visible ;
      private int edtavTfregrascontagem_responsavel_sel_Visible ;
      private int edtavDdo_regrascontagem_regratitlecontrolidtoreplace_Visible ;
      private int edtavDdo_regrascontagem_datatitlecontrolidtoreplace_Visible ;
      private int edtavDdo_regrascontagem_validadetitlecontrolidtoreplace_Visible ;
      private int edtavDdo_regrascontagem_responsaveltitlecontrolidtoreplace_Visible ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int AV6WWPContext_gxTpr_Areatrabalho_codigo ;
      private int A865RegrasContagem_AreaTrabalhoCod ;
      private int AV66WWRegrasContagemDS_1_Regrascontagem_areatrabalhocod ;
      private int edtavOrdereddsc_Visible ;
      private int imgInsert_Visible ;
      private int edtavUpdate_Visible ;
      private int edtavDelete_Visible ;
      private int AV61PageToGo ;
      private int edtavUpdate_Enabled ;
      private int edtavDelete_Enabled ;
      private int imgAdddynamicfilters1_Visible ;
      private int imgRemovedynamicfilters1_Visible ;
      private int imgAdddynamicfilters2_Visible ;
      private int imgRemovedynamicfilters2_Visible ;
      private int imgInsert_Enabled ;
      private int edtavRegrascontagem_regra1_Visible ;
      private int tblTablemergeddynamicfiltersregrascontagem_data1_Visible ;
      private int edtavRegrascontagem_responsavel1_Visible ;
      private int edtavRegrascontagem_regra2_Visible ;
      private int tblTablemergeddynamicfiltersregrascontagem_data2_Visible ;
      private int edtavRegrascontagem_responsavel2_Visible ;
      private int edtavRegrascontagem_regra3_Visible ;
      private int tblTablemergeddynamicfiltersregrascontagem_data3_Visible ;
      private int edtavRegrascontagem_responsavel3_Visible ;
      private int AV95GXV1 ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private long GRID_nFirstRecordOnPage ;
      private long AV62GridCurrentPage ;
      private long AV63GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private String Gridpaginationbar_Selectedpage ;
      private String Ddo_regrascontagem_regra_Activeeventkey ;
      private String Ddo_regrascontagem_regra_Filteredtext_get ;
      private String Ddo_regrascontagem_regra_Selectedvalue_get ;
      private String Ddo_regrascontagem_data_Activeeventkey ;
      private String Ddo_regrascontagem_data_Filteredtext_get ;
      private String Ddo_regrascontagem_data_Filteredtextto_get ;
      private String Ddo_regrascontagem_validade_Activeeventkey ;
      private String Ddo_regrascontagem_validade_Filteredtext_get ;
      private String Ddo_regrascontagem_validade_Filteredtextto_get ;
      private String Ddo_regrascontagem_responsavel_Activeeventkey ;
      private String Ddo_regrascontagem_responsavel_Filteredtext_get ;
      private String Ddo_regrascontagem_responsavel_Selectedvalue_get ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_106_idx="0001" ;
      private String AV32RegrasContagem_Responsavel1 ;
      private String AV34RegrasContagem_Responsavel2 ;
      private String AV36RegrasContagem_Responsavel3 ;
      private String AV57TFRegrasContagem_Responsavel ;
      private String AV58TFRegrasContagem_Responsavel_Sel ;
      private String AV94Pgmname ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Ddo_regrascontagem_regra_Caption ;
      private String Ddo_regrascontagem_regra_Tooltip ;
      private String Ddo_regrascontagem_regra_Cls ;
      private String Ddo_regrascontagem_regra_Filteredtext_set ;
      private String Ddo_regrascontagem_regra_Selectedvalue_set ;
      private String Ddo_regrascontagem_regra_Dropdownoptionstype ;
      private String Ddo_regrascontagem_regra_Titlecontrolidtoreplace ;
      private String Ddo_regrascontagem_regra_Sortedstatus ;
      private String Ddo_regrascontagem_regra_Filtertype ;
      private String Ddo_regrascontagem_regra_Datalisttype ;
      private String Ddo_regrascontagem_regra_Datalistproc ;
      private String Ddo_regrascontagem_regra_Sortasc ;
      private String Ddo_regrascontagem_regra_Sortdsc ;
      private String Ddo_regrascontagem_regra_Loadingdata ;
      private String Ddo_regrascontagem_regra_Cleanfilter ;
      private String Ddo_regrascontagem_regra_Noresultsfound ;
      private String Ddo_regrascontagem_regra_Searchbuttontext ;
      private String Ddo_regrascontagem_data_Caption ;
      private String Ddo_regrascontagem_data_Tooltip ;
      private String Ddo_regrascontagem_data_Cls ;
      private String Ddo_regrascontagem_data_Filteredtext_set ;
      private String Ddo_regrascontagem_data_Filteredtextto_set ;
      private String Ddo_regrascontagem_data_Dropdownoptionstype ;
      private String Ddo_regrascontagem_data_Titlecontrolidtoreplace ;
      private String Ddo_regrascontagem_data_Sortedstatus ;
      private String Ddo_regrascontagem_data_Filtertype ;
      private String Ddo_regrascontagem_data_Sortasc ;
      private String Ddo_regrascontagem_data_Sortdsc ;
      private String Ddo_regrascontagem_data_Cleanfilter ;
      private String Ddo_regrascontagem_data_Rangefilterfrom ;
      private String Ddo_regrascontagem_data_Rangefilterto ;
      private String Ddo_regrascontagem_data_Searchbuttontext ;
      private String Ddo_regrascontagem_validade_Caption ;
      private String Ddo_regrascontagem_validade_Tooltip ;
      private String Ddo_regrascontagem_validade_Cls ;
      private String Ddo_regrascontagem_validade_Filteredtext_set ;
      private String Ddo_regrascontagem_validade_Filteredtextto_set ;
      private String Ddo_regrascontagem_validade_Dropdownoptionstype ;
      private String Ddo_regrascontagem_validade_Titlecontrolidtoreplace ;
      private String Ddo_regrascontagem_validade_Sortedstatus ;
      private String Ddo_regrascontagem_validade_Filtertype ;
      private String Ddo_regrascontagem_validade_Sortasc ;
      private String Ddo_regrascontagem_validade_Sortdsc ;
      private String Ddo_regrascontagem_validade_Cleanfilter ;
      private String Ddo_regrascontagem_validade_Rangefilterfrom ;
      private String Ddo_regrascontagem_validade_Rangefilterto ;
      private String Ddo_regrascontagem_validade_Searchbuttontext ;
      private String Ddo_regrascontagem_responsavel_Caption ;
      private String Ddo_regrascontagem_responsavel_Tooltip ;
      private String Ddo_regrascontagem_responsavel_Cls ;
      private String Ddo_regrascontagem_responsavel_Filteredtext_set ;
      private String Ddo_regrascontagem_responsavel_Selectedvalue_set ;
      private String Ddo_regrascontagem_responsavel_Dropdownoptionstype ;
      private String Ddo_regrascontagem_responsavel_Titlecontrolidtoreplace ;
      private String Ddo_regrascontagem_responsavel_Sortedstatus ;
      private String Ddo_regrascontagem_responsavel_Filtertype ;
      private String Ddo_regrascontagem_responsavel_Datalisttype ;
      private String Ddo_regrascontagem_responsavel_Datalistproc ;
      private String Ddo_regrascontagem_responsavel_Sortasc ;
      private String Ddo_regrascontagem_responsavel_Sortdsc ;
      private String Ddo_regrascontagem_responsavel_Loadingdata ;
      private String Ddo_regrascontagem_responsavel_Cleanfilter ;
      private String Ddo_regrascontagem_responsavel_Noresultsfound ;
      private String Ddo_regrascontagem_responsavel_Searchbuttontext ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String chkavDynamicfiltersenabled2_Internalname ;
      private String chkavDynamicfiltersenabled3_Internalname ;
      private String edtavTfregrascontagem_regra_Internalname ;
      private String edtavTfregrascontagem_regra_Jsonclick ;
      private String edtavTfregrascontagem_regra_sel_Internalname ;
      private String edtavTfregrascontagem_regra_sel_Jsonclick ;
      private String edtavTfregrascontagem_data_Internalname ;
      private String edtavTfregrascontagem_data_Jsonclick ;
      private String edtavTfregrascontagem_data_to_Internalname ;
      private String edtavTfregrascontagem_data_to_Jsonclick ;
      private String divDdo_regrascontagem_dataauxdates_Internalname ;
      private String edtavDdo_regrascontagem_dataauxdate_Internalname ;
      private String edtavDdo_regrascontagem_dataauxdate_Jsonclick ;
      private String edtavDdo_regrascontagem_dataauxdateto_Internalname ;
      private String edtavDdo_regrascontagem_dataauxdateto_Jsonclick ;
      private String edtavTfregrascontagem_validade_Internalname ;
      private String edtavTfregrascontagem_validade_Jsonclick ;
      private String edtavTfregrascontagem_validade_to_Internalname ;
      private String edtavTfregrascontagem_validade_to_Jsonclick ;
      private String divDdo_regrascontagem_validadeauxdates_Internalname ;
      private String edtavDdo_regrascontagem_validadeauxdate_Internalname ;
      private String edtavDdo_regrascontagem_validadeauxdate_Jsonclick ;
      private String edtavDdo_regrascontagem_validadeauxdateto_Internalname ;
      private String edtavDdo_regrascontagem_validadeauxdateto_Jsonclick ;
      private String edtavTfregrascontagem_responsavel_Internalname ;
      private String edtavTfregrascontagem_responsavel_Jsonclick ;
      private String edtavTfregrascontagem_responsavel_sel_Internalname ;
      private String edtavTfregrascontagem_responsavel_sel_Jsonclick ;
      private String edtavDdo_regrascontagem_regratitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_regrascontagem_datatitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_regrascontagem_validadetitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_regrascontagem_responsaveltitlecontrolidtoreplace_Internalname ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavUpdate_Internalname ;
      private String edtavDelete_Internalname ;
      private String edtRegrasContagem_Regra_Internalname ;
      private String edtRegrasContagem_Data_Internalname ;
      private String edtRegrasContagem_Validade_Internalname ;
      private String A863RegrasContagem_Responsavel ;
      private String edtRegrasContagem_Responsavel_Internalname ;
      private String cmbavOrderedby_Internalname ;
      private String scmdbuf ;
      private String lV71WWRegrasContagemDS_6_Regrascontagem_responsavel1 ;
      private String lV77WWRegrasContagemDS_12_Regrascontagem_responsavel2 ;
      private String lV83WWRegrasContagemDS_18_Regrascontagem_responsavel3 ;
      private String lV90WWRegrasContagemDS_25_Tfregrascontagem_responsavel ;
      private String AV71WWRegrasContagemDS_6_Regrascontagem_responsavel1 ;
      private String AV77WWRegrasContagemDS_12_Regrascontagem_responsavel2 ;
      private String AV83WWRegrasContagemDS_18_Regrascontagem_responsavel3 ;
      private String AV91WWRegrasContagemDS_26_Tfregrascontagem_responsavel_sel ;
      private String AV90WWRegrasContagemDS_25_Tfregrascontagem_responsavel ;
      private String edtavOrdereddsc_Internalname ;
      private String edtavRegrascontagem_areatrabalhocod_Internalname ;
      private String cmbavDynamicfiltersselector1_Internalname ;
      private String edtavRegrascontagem_regra1_Internalname ;
      private String edtavRegrascontagem_data1_Internalname ;
      private String edtavRegrascontagem_data_to1_Internalname ;
      private String edtavRegrascontagem_responsavel1_Internalname ;
      private String cmbavDynamicfiltersselector2_Internalname ;
      private String edtavRegrascontagem_regra2_Internalname ;
      private String edtavRegrascontagem_data2_Internalname ;
      private String edtavRegrascontagem_data_to2_Internalname ;
      private String edtavRegrascontagem_responsavel2_Internalname ;
      private String cmbavDynamicfiltersselector3_Internalname ;
      private String edtavRegrascontagem_regra3_Internalname ;
      private String edtavRegrascontagem_data3_Internalname ;
      private String edtavRegrascontagem_data_to3_Internalname ;
      private String edtavRegrascontagem_responsavel3_Internalname ;
      private String lblJsdynamicfilters_Caption ;
      private String lblJsdynamicfilters_Internalname ;
      private String imgAdddynamicfilters1_Jsonclick ;
      private String imgAdddynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters1_Jsonclick ;
      private String imgRemovedynamicfilters1_Internalname ;
      private String imgAdddynamicfilters2_Jsonclick ;
      private String imgAdddynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters2_Jsonclick ;
      private String imgRemovedynamicfilters2_Internalname ;
      private String imgRemovedynamicfilters3_Jsonclick ;
      private String imgRemovedynamicfilters3_Internalname ;
      private String subGrid_Internalname ;
      private String Ddo_regrascontagem_regra_Internalname ;
      private String Ddo_regrascontagem_data_Internalname ;
      private String Ddo_regrascontagem_validade_Internalname ;
      private String Ddo_regrascontagem_responsavel_Internalname ;
      private String imgCleanfilters_Jsonclick ;
      private String imgCleanfilters_Internalname ;
      private String edtRegrasContagem_Regra_Title ;
      private String edtRegrasContagem_Data_Title ;
      private String edtRegrasContagem_Validade_Title ;
      private String edtRegrasContagem_Responsavel_Title ;
      private String imgInsert_Internalname ;
      private String edtavUpdate_Tooltiptext ;
      private String edtavUpdate_Link ;
      private String edtavDelete_Tooltiptext ;
      private String edtavDelete_Link ;
      private String edtRegrasContagem_Data_Link ;
      private String edtRegrasContagem_Regra_Tooltiptext ;
      private String imgInsert_Link ;
      private String tblTablemergeddynamicfiltersregrascontagem_data1_Internalname ;
      private String tblTablemergeddynamicfiltersregrascontagem_data2_Internalname ;
      private String tblTablemergeddynamicfiltersregrascontagem_data3_Internalname ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTablegridheader_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTableheader_Internalname ;
      private String lblRegrascontagemtitle_Internalname ;
      private String lblRegrascontagemtitle_Jsonclick ;
      private String lblOrderedtext_Internalname ;
      private String lblOrderedtext_Jsonclick ;
      private String cmbavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Jsonclick ;
      private String tblTablefilters_Internalname ;
      private String lblFiltertextregrascontagem_areatrabalhocod_Internalname ;
      private String lblFiltertextregrascontagem_areatrabalhocod_Jsonclick ;
      private String edtavRegrascontagem_areatrabalhocod_Jsonclick ;
      private String lblJsdynamicfilters_Jsonclick ;
      private String bttBtn_search_Internalname ;
      private String bttBtn_search_Jsonclick ;
      private String tblTabledynamicfilters_Internalname ;
      private String lblDynamicfiltersprefix1_Internalname ;
      private String lblDynamicfiltersprefix1_Jsonclick ;
      private String cmbavDynamicfiltersselector1_Jsonclick ;
      private String lblDynamicfiltersmiddle1_Internalname ;
      private String lblDynamicfiltersmiddle1_Jsonclick ;
      private String edtavRegrascontagem_regra1_Jsonclick ;
      private String edtavRegrascontagem_responsavel1_Jsonclick ;
      private String lblDynamicfiltersprefix2_Internalname ;
      private String lblDynamicfiltersprefix2_Jsonclick ;
      private String cmbavDynamicfiltersselector2_Jsonclick ;
      private String lblDynamicfiltersmiddle2_Internalname ;
      private String lblDynamicfiltersmiddle2_Jsonclick ;
      private String edtavRegrascontagem_regra2_Jsonclick ;
      private String edtavRegrascontagem_responsavel2_Jsonclick ;
      private String lblDynamicfiltersprefix3_Internalname ;
      private String lblDynamicfiltersprefix3_Jsonclick ;
      private String cmbavDynamicfiltersselector3_Jsonclick ;
      private String lblDynamicfiltersmiddle3_Internalname ;
      private String lblDynamicfiltersmiddle3_Jsonclick ;
      private String edtavRegrascontagem_regra3_Jsonclick ;
      private String edtavRegrascontagem_responsavel3_Jsonclick ;
      private String edtavRegrascontagem_data3_Jsonclick ;
      private String lblDynamicfiltersregrascontagem_data_rangemiddletext3_Internalname ;
      private String lblDynamicfiltersregrascontagem_data_rangemiddletext3_Jsonclick ;
      private String edtavRegrascontagem_data_to3_Jsonclick ;
      private String edtavRegrascontagem_data2_Jsonclick ;
      private String lblDynamicfiltersregrascontagem_data_rangemiddletext2_Internalname ;
      private String lblDynamicfiltersregrascontagem_data_rangemiddletext2_Jsonclick ;
      private String edtavRegrascontagem_data_to2_Jsonclick ;
      private String edtavRegrascontagem_data1_Jsonclick ;
      private String lblDynamicfiltersregrascontagem_data_rangemiddletext1_Internalname ;
      private String lblDynamicfiltersregrascontagem_data_rangemiddletext1_Jsonclick ;
      private String edtavRegrascontagem_data_to1_Jsonclick ;
      private String tblTableactions_Internalname ;
      private String imgInsert_Jsonclick ;
      private String sGXsfl_106_fel_idx="0001" ;
      private String ROClassString ;
      private String edtRegrasContagem_Regra_Jsonclick ;
      private String edtRegrasContagem_Data_Jsonclick ;
      private String edtRegrasContagem_Validade_Jsonclick ;
      private String edtRegrasContagem_Responsavel_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private DateTime AV16RegrasContagem_Data1 ;
      private DateTime AV17RegrasContagem_Data_To1 ;
      private DateTime AV20RegrasContagem_Data2 ;
      private DateTime AV21RegrasContagem_Data_To2 ;
      private DateTime AV24RegrasContagem_Data3 ;
      private DateTime AV25RegrasContagem_Data_To3 ;
      private DateTime AV45TFRegrasContagem_Data ;
      private DateTime AV46TFRegrasContagem_Data_To ;
      private DateTime AV51TFRegrasContagem_Validade ;
      private DateTime AV52TFRegrasContagem_Validade_To ;
      private DateTime AV47DDO_RegrasContagem_DataAuxDate ;
      private DateTime AV48DDO_RegrasContagem_DataAuxDateTo ;
      private DateTime AV53DDO_RegrasContagem_ValidadeAuxDate ;
      private DateTime AV54DDO_RegrasContagem_ValidadeAuxDateTo ;
      private DateTime A861RegrasContagem_Data ;
      private DateTime A862RegrasContagem_Validade ;
      private DateTime AV69WWRegrasContagemDS_4_Regrascontagem_data1 ;
      private DateTime AV70WWRegrasContagemDS_5_Regrascontagem_data_to1 ;
      private DateTime AV75WWRegrasContagemDS_10_Regrascontagem_data2 ;
      private DateTime AV76WWRegrasContagemDS_11_Regrascontagem_data_to2 ;
      private DateTime AV81WWRegrasContagemDS_16_Regrascontagem_data3 ;
      private DateTime AV82WWRegrasContagemDS_17_Regrascontagem_data_to3 ;
      private DateTime AV86WWRegrasContagemDS_21_Tfregrascontagem_data ;
      private DateTime AV87WWRegrasContagemDS_22_Tfregrascontagem_data_to ;
      private DateTime AV88WWRegrasContagemDS_23_Tfregrascontagem_validade ;
      private DateTime AV89WWRegrasContagemDS_24_Tfregrascontagem_validade_to ;
      private bool entryPointCalled ;
      private bool AV14OrderedDsc ;
      private bool AV18DynamicFiltersEnabled2 ;
      private bool AV22DynamicFiltersEnabled3 ;
      private bool AV27DynamicFiltersIgnoreFirst ;
      private bool AV26DynamicFiltersRemoving ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool Ddo_regrascontagem_regra_Includesortasc ;
      private bool Ddo_regrascontagem_regra_Includesortdsc ;
      private bool Ddo_regrascontagem_regra_Includefilter ;
      private bool Ddo_regrascontagem_regra_Filterisrange ;
      private bool Ddo_regrascontagem_regra_Includedatalist ;
      private bool Ddo_regrascontagem_data_Includesortasc ;
      private bool Ddo_regrascontagem_data_Includesortdsc ;
      private bool Ddo_regrascontagem_data_Includefilter ;
      private bool Ddo_regrascontagem_data_Filterisrange ;
      private bool Ddo_regrascontagem_data_Includedatalist ;
      private bool Ddo_regrascontagem_validade_Includesortasc ;
      private bool Ddo_regrascontagem_validade_Includesortdsc ;
      private bool Ddo_regrascontagem_validade_Includefilter ;
      private bool Ddo_regrascontagem_validade_Filterisrange ;
      private bool Ddo_regrascontagem_validade_Includedatalist ;
      private bool Ddo_regrascontagem_responsavel_Includesortasc ;
      private bool Ddo_regrascontagem_responsavel_Includesortdsc ;
      private bool Ddo_regrascontagem_responsavel_Includefilter ;
      private bool Ddo_regrascontagem_responsavel_Filterisrange ;
      private bool Ddo_regrascontagem_responsavel_Includedatalist ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n862RegrasContagem_Validade ;
      private bool AV72WWRegrasContagemDS_7_Dynamicfiltersenabled2 ;
      private bool AV78WWRegrasContagemDS_13_Dynamicfiltersenabled3 ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV28Update_IsBlob ;
      private bool AV29Delete_IsBlob ;
      private String A864RegrasContagem_Descricao ;
      private String AV15DynamicFiltersSelector1 ;
      private String AV31RegrasContagem_Regra1 ;
      private String AV19DynamicFiltersSelector2 ;
      private String AV33RegrasContagem_Regra2 ;
      private String AV23DynamicFiltersSelector3 ;
      private String AV35RegrasContagem_Regra3 ;
      private String AV41TFRegrasContagem_Regra ;
      private String AV42TFRegrasContagem_Regra_Sel ;
      private String AV43ddo_RegrasContagem_RegraTitleControlIdToReplace ;
      private String AV49ddo_RegrasContagem_DataTitleControlIdToReplace ;
      private String AV55ddo_RegrasContagem_ValidadeTitleControlIdToReplace ;
      private String AV59ddo_RegrasContagem_ResponsavelTitleControlIdToReplace ;
      private String A860RegrasContagem_Regra ;
      private String AV92Update_GXI ;
      private String AV93Delete_GXI ;
      private String lV68WWRegrasContagemDS_3_Regrascontagem_regra1 ;
      private String lV74WWRegrasContagemDS_9_Regrascontagem_regra2 ;
      private String lV80WWRegrasContagemDS_15_Regrascontagem_regra3 ;
      private String lV84WWRegrasContagemDS_19_Tfregrascontagem_regra ;
      private String AV67WWRegrasContagemDS_2_Dynamicfiltersselector1 ;
      private String AV68WWRegrasContagemDS_3_Regrascontagem_regra1 ;
      private String AV73WWRegrasContagemDS_8_Dynamicfiltersselector2 ;
      private String AV74WWRegrasContagemDS_9_Regrascontagem_regra2 ;
      private String AV79WWRegrasContagemDS_14_Dynamicfiltersselector3 ;
      private String AV80WWRegrasContagemDS_15_Regrascontagem_regra3 ;
      private String AV85WWRegrasContagemDS_20_Tfregrascontagem_regra_sel ;
      private String AV84WWRegrasContagemDS_19_Tfregrascontagem_regra ;
      private String AV28Update ;
      private String AV29Delete ;
      private String imgInsert_Bitmap ;
      private IGxSession AV30Session ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbavOrderedby ;
      private GXCombobox cmbavDynamicfiltersselector1 ;
      private GXCombobox cmbavDynamicfiltersselector2 ;
      private GXCombobox cmbavDynamicfiltersselector3 ;
      private GXCheckbox chkavDynamicfiltersenabled2 ;
      private GXCheckbox chkavDynamicfiltersenabled3 ;
      private IDataStoreProvider pr_default ;
      private int[] H00FC2_A865RegrasContagem_AreaTrabalhoCod ;
      private String[] H00FC2_A864RegrasContagem_Descricao ;
      private String[] H00FC2_A863RegrasContagem_Responsavel ;
      private DateTime[] H00FC2_A862RegrasContagem_Validade ;
      private bool[] H00FC2_n862RegrasContagem_Validade ;
      private DateTime[] H00FC2_A861RegrasContagem_Data ;
      private String[] H00FC2_A860RegrasContagem_Regra ;
      private long[] H00FC3_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV7HTTPRequest ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV40RegrasContagem_RegraTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV44RegrasContagem_DataTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV50RegrasContagem_ValidadeTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV56RegrasContagem_ResponsavelTitleFilterData ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV8TrnContext ;
      private wwpbaseobjects.SdtWWPGridState AV10GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV11GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV12GridStateDynamicFilter ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV60DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 ;
   }

   public class wwregrascontagem__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00FC2( IGxContext context ,
                                             String AV67WWRegrasContagemDS_2_Dynamicfiltersselector1 ,
                                             String AV68WWRegrasContagemDS_3_Regrascontagem_regra1 ,
                                             DateTime AV69WWRegrasContagemDS_4_Regrascontagem_data1 ,
                                             DateTime AV70WWRegrasContagemDS_5_Regrascontagem_data_to1 ,
                                             String AV71WWRegrasContagemDS_6_Regrascontagem_responsavel1 ,
                                             bool AV72WWRegrasContagemDS_7_Dynamicfiltersenabled2 ,
                                             String AV73WWRegrasContagemDS_8_Dynamicfiltersselector2 ,
                                             String AV74WWRegrasContagemDS_9_Regrascontagem_regra2 ,
                                             DateTime AV75WWRegrasContagemDS_10_Regrascontagem_data2 ,
                                             DateTime AV76WWRegrasContagemDS_11_Regrascontagem_data_to2 ,
                                             String AV77WWRegrasContagemDS_12_Regrascontagem_responsavel2 ,
                                             bool AV78WWRegrasContagemDS_13_Dynamicfiltersenabled3 ,
                                             String AV79WWRegrasContagemDS_14_Dynamicfiltersselector3 ,
                                             String AV80WWRegrasContagemDS_15_Regrascontagem_regra3 ,
                                             DateTime AV81WWRegrasContagemDS_16_Regrascontagem_data3 ,
                                             DateTime AV82WWRegrasContagemDS_17_Regrascontagem_data_to3 ,
                                             String AV83WWRegrasContagemDS_18_Regrascontagem_responsavel3 ,
                                             String AV85WWRegrasContagemDS_20_Tfregrascontagem_regra_sel ,
                                             String AV84WWRegrasContagemDS_19_Tfregrascontagem_regra ,
                                             DateTime AV86WWRegrasContagemDS_21_Tfregrascontagem_data ,
                                             DateTime AV87WWRegrasContagemDS_22_Tfregrascontagem_data_to ,
                                             DateTime AV88WWRegrasContagemDS_23_Tfregrascontagem_validade ,
                                             DateTime AV89WWRegrasContagemDS_24_Tfregrascontagem_validade_to ,
                                             String AV91WWRegrasContagemDS_26_Tfregrascontagem_responsavel_sel ,
                                             String AV90WWRegrasContagemDS_25_Tfregrascontagem_responsavel ,
                                             String A860RegrasContagem_Regra ,
                                             DateTime A861RegrasContagem_Data ,
                                             String A863RegrasContagem_Responsavel ,
                                             DateTime A862RegrasContagem_Validade ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc ,
                                             int A865RegrasContagem_AreaTrabalhoCod ,
                                             int AV6WWPContext_gxTpr_Areatrabalho_codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [26] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " [RegrasContagem_AreaTrabalhoCod], [RegrasContagem_Descricao], [RegrasContagem_Responsavel], [RegrasContagem_Validade], [RegrasContagem_Data], [RegrasContagem_Regra]";
         sFromString = " FROM [RegrasContagem] WITH (NOLOCK)";
         sOrderString = "";
         sWhereString = sWhereString + " WHERE ([RegrasContagem_AreaTrabalhoCod] = @AV6WWPCo_1Areatrabalho_codigo)";
         if ( ( StringUtil.StrCmp(AV67WWRegrasContagemDS_2_Dynamicfiltersselector1, "REGRASCONTAGEM_REGRA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV68WWRegrasContagemDS_3_Regrascontagem_regra1)) ) )
         {
            sWhereString = sWhereString + " and ([RegrasContagem_Regra] like '%' + @lV68WWRegrasContagemDS_3_Regrascontagem_regra1 + '%')";
         }
         else
         {
            GXv_int2[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV67WWRegrasContagemDS_2_Dynamicfiltersselector1, "REGRASCONTAGEM_DATA") == 0 ) && ( ! (DateTime.MinValue==AV69WWRegrasContagemDS_4_Regrascontagem_data1) ) )
         {
            sWhereString = sWhereString + " and ([RegrasContagem_Data] >= @AV69WWRegrasContagemDS_4_Regrascontagem_data1)";
         }
         else
         {
            GXv_int2[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV67WWRegrasContagemDS_2_Dynamicfiltersselector1, "REGRASCONTAGEM_DATA") == 0 ) && ( ! (DateTime.MinValue==AV70WWRegrasContagemDS_5_Regrascontagem_data_to1) ) )
         {
            sWhereString = sWhereString + " and ([RegrasContagem_Data] <= @AV70WWRegrasContagemDS_5_Regrascontagem_data_to1)";
         }
         else
         {
            GXv_int2[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV67WWRegrasContagemDS_2_Dynamicfiltersselector1, "REGRASCONTAGEM_RESPONSAVEL") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV71WWRegrasContagemDS_6_Regrascontagem_responsavel1)) ) )
         {
            sWhereString = sWhereString + " and ([RegrasContagem_Responsavel] like '%' + @lV71WWRegrasContagemDS_6_Regrascontagem_responsavel1 + '%')";
         }
         else
         {
            GXv_int2[4] = 1;
         }
         if ( AV72WWRegrasContagemDS_7_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV73WWRegrasContagemDS_8_Dynamicfiltersselector2, "REGRASCONTAGEM_REGRA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV74WWRegrasContagemDS_9_Regrascontagem_regra2)) ) )
         {
            sWhereString = sWhereString + " and ([RegrasContagem_Regra] like '%' + @lV74WWRegrasContagemDS_9_Regrascontagem_regra2 + '%')";
         }
         else
         {
            GXv_int2[5] = 1;
         }
         if ( AV72WWRegrasContagemDS_7_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV73WWRegrasContagemDS_8_Dynamicfiltersselector2, "REGRASCONTAGEM_DATA") == 0 ) && ( ! (DateTime.MinValue==AV75WWRegrasContagemDS_10_Regrascontagem_data2) ) )
         {
            sWhereString = sWhereString + " and ([RegrasContagem_Data] >= @AV75WWRegrasContagemDS_10_Regrascontagem_data2)";
         }
         else
         {
            GXv_int2[6] = 1;
         }
         if ( AV72WWRegrasContagemDS_7_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV73WWRegrasContagemDS_8_Dynamicfiltersselector2, "REGRASCONTAGEM_DATA") == 0 ) && ( ! (DateTime.MinValue==AV76WWRegrasContagemDS_11_Regrascontagem_data_to2) ) )
         {
            sWhereString = sWhereString + " and ([RegrasContagem_Data] <= @AV76WWRegrasContagemDS_11_Regrascontagem_data_to2)";
         }
         else
         {
            GXv_int2[7] = 1;
         }
         if ( AV72WWRegrasContagemDS_7_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV73WWRegrasContagemDS_8_Dynamicfiltersselector2, "REGRASCONTAGEM_RESPONSAVEL") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV77WWRegrasContagemDS_12_Regrascontagem_responsavel2)) ) )
         {
            sWhereString = sWhereString + " and ([RegrasContagem_Responsavel] like '%' + @lV77WWRegrasContagemDS_12_Regrascontagem_responsavel2 + '%')";
         }
         else
         {
            GXv_int2[8] = 1;
         }
         if ( AV78WWRegrasContagemDS_13_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV79WWRegrasContagemDS_14_Dynamicfiltersselector3, "REGRASCONTAGEM_REGRA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV80WWRegrasContagemDS_15_Regrascontagem_regra3)) ) )
         {
            sWhereString = sWhereString + " and ([RegrasContagem_Regra] like '%' + @lV80WWRegrasContagemDS_15_Regrascontagem_regra3 + '%')";
         }
         else
         {
            GXv_int2[9] = 1;
         }
         if ( AV78WWRegrasContagemDS_13_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV79WWRegrasContagemDS_14_Dynamicfiltersselector3, "REGRASCONTAGEM_DATA") == 0 ) && ( ! (DateTime.MinValue==AV81WWRegrasContagemDS_16_Regrascontagem_data3) ) )
         {
            sWhereString = sWhereString + " and ([RegrasContagem_Data] >= @AV81WWRegrasContagemDS_16_Regrascontagem_data3)";
         }
         else
         {
            GXv_int2[10] = 1;
         }
         if ( AV78WWRegrasContagemDS_13_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV79WWRegrasContagemDS_14_Dynamicfiltersselector3, "REGRASCONTAGEM_DATA") == 0 ) && ( ! (DateTime.MinValue==AV82WWRegrasContagemDS_17_Regrascontagem_data_to3) ) )
         {
            sWhereString = sWhereString + " and ([RegrasContagem_Data] <= @AV82WWRegrasContagemDS_17_Regrascontagem_data_to3)";
         }
         else
         {
            GXv_int2[11] = 1;
         }
         if ( AV78WWRegrasContagemDS_13_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV79WWRegrasContagemDS_14_Dynamicfiltersselector3, "REGRASCONTAGEM_RESPONSAVEL") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV83WWRegrasContagemDS_18_Regrascontagem_responsavel3)) ) )
         {
            sWhereString = sWhereString + " and ([RegrasContagem_Responsavel] like '%' + @lV83WWRegrasContagemDS_18_Regrascontagem_responsavel3 + '%')";
         }
         else
         {
            GXv_int2[12] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV85WWRegrasContagemDS_20_Tfregrascontagem_regra_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV84WWRegrasContagemDS_19_Tfregrascontagem_regra)) ) )
         {
            sWhereString = sWhereString + " and ([RegrasContagem_Regra] like @lV84WWRegrasContagemDS_19_Tfregrascontagem_regra)";
         }
         else
         {
            GXv_int2[13] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV85WWRegrasContagemDS_20_Tfregrascontagem_regra_sel)) )
         {
            sWhereString = sWhereString + " and ([RegrasContagem_Regra] = @AV85WWRegrasContagemDS_20_Tfregrascontagem_regra_sel)";
         }
         else
         {
            GXv_int2[14] = 1;
         }
         if ( ! (DateTime.MinValue==AV86WWRegrasContagemDS_21_Tfregrascontagem_data) )
         {
            sWhereString = sWhereString + " and ([RegrasContagem_Data] >= @AV86WWRegrasContagemDS_21_Tfregrascontagem_data)";
         }
         else
         {
            GXv_int2[15] = 1;
         }
         if ( ! (DateTime.MinValue==AV87WWRegrasContagemDS_22_Tfregrascontagem_data_to) )
         {
            sWhereString = sWhereString + " and ([RegrasContagem_Data] <= @AV87WWRegrasContagemDS_22_Tfregrascontagem_data_to)";
         }
         else
         {
            GXv_int2[16] = 1;
         }
         if ( ! (DateTime.MinValue==AV88WWRegrasContagemDS_23_Tfregrascontagem_validade) )
         {
            sWhereString = sWhereString + " and ([RegrasContagem_Validade] >= @AV88WWRegrasContagemDS_23_Tfregrascontagem_validade)";
         }
         else
         {
            GXv_int2[17] = 1;
         }
         if ( ! (DateTime.MinValue==AV89WWRegrasContagemDS_24_Tfregrascontagem_validade_to) )
         {
            sWhereString = sWhereString + " and ([RegrasContagem_Validade] <= @AV89WWRegrasContagemDS_24_Tfregrascontagem_validade_to)";
         }
         else
         {
            GXv_int2[18] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV91WWRegrasContagemDS_26_Tfregrascontagem_responsavel_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV90WWRegrasContagemDS_25_Tfregrascontagem_responsavel)) ) )
         {
            sWhereString = sWhereString + " and ([RegrasContagem_Responsavel] like @lV90WWRegrasContagemDS_25_Tfregrascontagem_responsavel)";
         }
         else
         {
            GXv_int2[19] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV91WWRegrasContagemDS_26_Tfregrascontagem_responsavel_sel)) )
         {
            sWhereString = sWhereString + " and ([RegrasContagem_Responsavel] = @AV91WWRegrasContagemDS_26_Tfregrascontagem_responsavel_sel)";
         }
         else
         {
            GXv_int2[20] = 1;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [RegrasContagem_Data]";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [RegrasContagem_Data] DESC";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [RegrasContagem_Regra]";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [RegrasContagem_Regra] DESC";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [RegrasContagem_Validade]";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [RegrasContagem_Validade] DESC";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [RegrasContagem_Responsavel]";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [RegrasContagem_Responsavel] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY [RegrasContagem_Regra]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      protected Object[] conditional_H00FC3( IGxContext context ,
                                             String AV67WWRegrasContagemDS_2_Dynamicfiltersselector1 ,
                                             String AV68WWRegrasContagemDS_3_Regrascontagem_regra1 ,
                                             DateTime AV69WWRegrasContagemDS_4_Regrascontagem_data1 ,
                                             DateTime AV70WWRegrasContagemDS_5_Regrascontagem_data_to1 ,
                                             String AV71WWRegrasContagemDS_6_Regrascontagem_responsavel1 ,
                                             bool AV72WWRegrasContagemDS_7_Dynamicfiltersenabled2 ,
                                             String AV73WWRegrasContagemDS_8_Dynamicfiltersselector2 ,
                                             String AV74WWRegrasContagemDS_9_Regrascontagem_regra2 ,
                                             DateTime AV75WWRegrasContagemDS_10_Regrascontagem_data2 ,
                                             DateTime AV76WWRegrasContagemDS_11_Regrascontagem_data_to2 ,
                                             String AV77WWRegrasContagemDS_12_Regrascontagem_responsavel2 ,
                                             bool AV78WWRegrasContagemDS_13_Dynamicfiltersenabled3 ,
                                             String AV79WWRegrasContagemDS_14_Dynamicfiltersselector3 ,
                                             String AV80WWRegrasContagemDS_15_Regrascontagem_regra3 ,
                                             DateTime AV81WWRegrasContagemDS_16_Regrascontagem_data3 ,
                                             DateTime AV82WWRegrasContagemDS_17_Regrascontagem_data_to3 ,
                                             String AV83WWRegrasContagemDS_18_Regrascontagem_responsavel3 ,
                                             String AV85WWRegrasContagemDS_20_Tfregrascontagem_regra_sel ,
                                             String AV84WWRegrasContagemDS_19_Tfregrascontagem_regra ,
                                             DateTime AV86WWRegrasContagemDS_21_Tfregrascontagem_data ,
                                             DateTime AV87WWRegrasContagemDS_22_Tfregrascontagem_data_to ,
                                             DateTime AV88WWRegrasContagemDS_23_Tfregrascontagem_validade ,
                                             DateTime AV89WWRegrasContagemDS_24_Tfregrascontagem_validade_to ,
                                             String AV91WWRegrasContagemDS_26_Tfregrascontagem_responsavel_sel ,
                                             String AV90WWRegrasContagemDS_25_Tfregrascontagem_responsavel ,
                                             String A860RegrasContagem_Regra ,
                                             DateTime A861RegrasContagem_Data ,
                                             String A863RegrasContagem_Responsavel ,
                                             DateTime A862RegrasContagem_Validade ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc ,
                                             int A865RegrasContagem_AreaTrabalhoCod ,
                                             int AV6WWPContext_gxTpr_Areatrabalho_codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [21] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM [RegrasContagem] WITH (NOLOCK)";
         scmdbuf = scmdbuf + " WHERE ([RegrasContagem_AreaTrabalhoCod] = @AV6WWPCo_1Areatrabalho_codigo)";
         if ( ( StringUtil.StrCmp(AV67WWRegrasContagemDS_2_Dynamicfiltersselector1, "REGRASCONTAGEM_REGRA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV68WWRegrasContagemDS_3_Regrascontagem_regra1)) ) )
         {
            sWhereString = sWhereString + " and ([RegrasContagem_Regra] like '%' + @lV68WWRegrasContagemDS_3_Regrascontagem_regra1 + '%')";
         }
         else
         {
            GXv_int4[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV67WWRegrasContagemDS_2_Dynamicfiltersselector1, "REGRASCONTAGEM_DATA") == 0 ) && ( ! (DateTime.MinValue==AV69WWRegrasContagemDS_4_Regrascontagem_data1) ) )
         {
            sWhereString = sWhereString + " and ([RegrasContagem_Data] >= @AV69WWRegrasContagemDS_4_Regrascontagem_data1)";
         }
         else
         {
            GXv_int4[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV67WWRegrasContagemDS_2_Dynamicfiltersselector1, "REGRASCONTAGEM_DATA") == 0 ) && ( ! (DateTime.MinValue==AV70WWRegrasContagemDS_5_Regrascontagem_data_to1) ) )
         {
            sWhereString = sWhereString + " and ([RegrasContagem_Data] <= @AV70WWRegrasContagemDS_5_Regrascontagem_data_to1)";
         }
         else
         {
            GXv_int4[3] = 1;
         }
         if ( ( StringUtil.StrCmp(AV67WWRegrasContagemDS_2_Dynamicfiltersselector1, "REGRASCONTAGEM_RESPONSAVEL") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV71WWRegrasContagemDS_6_Regrascontagem_responsavel1)) ) )
         {
            sWhereString = sWhereString + " and ([RegrasContagem_Responsavel] like '%' + @lV71WWRegrasContagemDS_6_Regrascontagem_responsavel1 + '%')";
         }
         else
         {
            GXv_int4[4] = 1;
         }
         if ( AV72WWRegrasContagemDS_7_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV73WWRegrasContagemDS_8_Dynamicfiltersselector2, "REGRASCONTAGEM_REGRA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV74WWRegrasContagemDS_9_Regrascontagem_regra2)) ) )
         {
            sWhereString = sWhereString + " and ([RegrasContagem_Regra] like '%' + @lV74WWRegrasContagemDS_9_Regrascontagem_regra2 + '%')";
         }
         else
         {
            GXv_int4[5] = 1;
         }
         if ( AV72WWRegrasContagemDS_7_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV73WWRegrasContagemDS_8_Dynamicfiltersselector2, "REGRASCONTAGEM_DATA") == 0 ) && ( ! (DateTime.MinValue==AV75WWRegrasContagemDS_10_Regrascontagem_data2) ) )
         {
            sWhereString = sWhereString + " and ([RegrasContagem_Data] >= @AV75WWRegrasContagemDS_10_Regrascontagem_data2)";
         }
         else
         {
            GXv_int4[6] = 1;
         }
         if ( AV72WWRegrasContagemDS_7_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV73WWRegrasContagemDS_8_Dynamicfiltersselector2, "REGRASCONTAGEM_DATA") == 0 ) && ( ! (DateTime.MinValue==AV76WWRegrasContagemDS_11_Regrascontagem_data_to2) ) )
         {
            sWhereString = sWhereString + " and ([RegrasContagem_Data] <= @AV76WWRegrasContagemDS_11_Regrascontagem_data_to2)";
         }
         else
         {
            GXv_int4[7] = 1;
         }
         if ( AV72WWRegrasContagemDS_7_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV73WWRegrasContagemDS_8_Dynamicfiltersselector2, "REGRASCONTAGEM_RESPONSAVEL") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV77WWRegrasContagemDS_12_Regrascontagem_responsavel2)) ) )
         {
            sWhereString = sWhereString + " and ([RegrasContagem_Responsavel] like '%' + @lV77WWRegrasContagemDS_12_Regrascontagem_responsavel2 + '%')";
         }
         else
         {
            GXv_int4[8] = 1;
         }
         if ( AV78WWRegrasContagemDS_13_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV79WWRegrasContagemDS_14_Dynamicfiltersselector3, "REGRASCONTAGEM_REGRA") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV80WWRegrasContagemDS_15_Regrascontagem_regra3)) ) )
         {
            sWhereString = sWhereString + " and ([RegrasContagem_Regra] like '%' + @lV80WWRegrasContagemDS_15_Regrascontagem_regra3 + '%')";
         }
         else
         {
            GXv_int4[9] = 1;
         }
         if ( AV78WWRegrasContagemDS_13_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV79WWRegrasContagemDS_14_Dynamicfiltersselector3, "REGRASCONTAGEM_DATA") == 0 ) && ( ! (DateTime.MinValue==AV81WWRegrasContagemDS_16_Regrascontagem_data3) ) )
         {
            sWhereString = sWhereString + " and ([RegrasContagem_Data] >= @AV81WWRegrasContagemDS_16_Regrascontagem_data3)";
         }
         else
         {
            GXv_int4[10] = 1;
         }
         if ( AV78WWRegrasContagemDS_13_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV79WWRegrasContagemDS_14_Dynamicfiltersselector3, "REGRASCONTAGEM_DATA") == 0 ) && ( ! (DateTime.MinValue==AV82WWRegrasContagemDS_17_Regrascontagem_data_to3) ) )
         {
            sWhereString = sWhereString + " and ([RegrasContagem_Data] <= @AV82WWRegrasContagemDS_17_Regrascontagem_data_to3)";
         }
         else
         {
            GXv_int4[11] = 1;
         }
         if ( AV78WWRegrasContagemDS_13_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV79WWRegrasContagemDS_14_Dynamicfiltersselector3, "REGRASCONTAGEM_RESPONSAVEL") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV83WWRegrasContagemDS_18_Regrascontagem_responsavel3)) ) )
         {
            sWhereString = sWhereString + " and ([RegrasContagem_Responsavel] like '%' + @lV83WWRegrasContagemDS_18_Regrascontagem_responsavel3 + '%')";
         }
         else
         {
            GXv_int4[12] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV85WWRegrasContagemDS_20_Tfregrascontagem_regra_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV84WWRegrasContagemDS_19_Tfregrascontagem_regra)) ) )
         {
            sWhereString = sWhereString + " and ([RegrasContagem_Regra] like @lV84WWRegrasContagemDS_19_Tfregrascontagem_regra)";
         }
         else
         {
            GXv_int4[13] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV85WWRegrasContagemDS_20_Tfregrascontagem_regra_sel)) )
         {
            sWhereString = sWhereString + " and ([RegrasContagem_Regra] = @AV85WWRegrasContagemDS_20_Tfregrascontagem_regra_sel)";
         }
         else
         {
            GXv_int4[14] = 1;
         }
         if ( ! (DateTime.MinValue==AV86WWRegrasContagemDS_21_Tfregrascontagem_data) )
         {
            sWhereString = sWhereString + " and ([RegrasContagem_Data] >= @AV86WWRegrasContagemDS_21_Tfregrascontagem_data)";
         }
         else
         {
            GXv_int4[15] = 1;
         }
         if ( ! (DateTime.MinValue==AV87WWRegrasContagemDS_22_Tfregrascontagem_data_to) )
         {
            sWhereString = sWhereString + " and ([RegrasContagem_Data] <= @AV87WWRegrasContagemDS_22_Tfregrascontagem_data_to)";
         }
         else
         {
            GXv_int4[16] = 1;
         }
         if ( ! (DateTime.MinValue==AV88WWRegrasContagemDS_23_Tfregrascontagem_validade) )
         {
            sWhereString = sWhereString + " and ([RegrasContagem_Validade] >= @AV88WWRegrasContagemDS_23_Tfregrascontagem_validade)";
         }
         else
         {
            GXv_int4[17] = 1;
         }
         if ( ! (DateTime.MinValue==AV89WWRegrasContagemDS_24_Tfregrascontagem_validade_to) )
         {
            sWhereString = sWhereString + " and ([RegrasContagem_Validade] <= @AV89WWRegrasContagemDS_24_Tfregrascontagem_validade_to)";
         }
         else
         {
            GXv_int4[18] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV91WWRegrasContagemDS_26_Tfregrascontagem_responsavel_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV90WWRegrasContagemDS_25_Tfregrascontagem_responsavel)) ) )
         {
            sWhereString = sWhereString + " and ([RegrasContagem_Responsavel] like @lV90WWRegrasContagemDS_25_Tfregrascontagem_responsavel)";
         }
         else
         {
            GXv_int4[19] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV91WWRegrasContagemDS_26_Tfregrascontagem_responsavel_sel)) )
         {
            sWhereString = sWhereString + " and ([RegrasContagem_Responsavel] = @AV91WWRegrasContagemDS_26_Tfregrascontagem_responsavel_sel)";
         }
         else
         {
            GXv_int4[20] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H00FC2(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (DateTime)dynConstraints[2] , (DateTime)dynConstraints[3] , (String)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (DateTime)dynConstraints[8] , (DateTime)dynConstraints[9] , (String)dynConstraints[10] , (bool)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (DateTime)dynConstraints[14] , (DateTime)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (DateTime)dynConstraints[19] , (DateTime)dynConstraints[20] , (DateTime)dynConstraints[21] , (DateTime)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] , (DateTime)dynConstraints[26] , (String)dynConstraints[27] , (DateTime)dynConstraints[28] , (short)dynConstraints[29] , (bool)dynConstraints[30] , (int)dynConstraints[31] , (int)dynConstraints[32] );
               case 1 :
                     return conditional_H00FC3(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (DateTime)dynConstraints[2] , (DateTime)dynConstraints[3] , (String)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (DateTime)dynConstraints[8] , (DateTime)dynConstraints[9] , (String)dynConstraints[10] , (bool)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (DateTime)dynConstraints[14] , (DateTime)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (DateTime)dynConstraints[19] , (DateTime)dynConstraints[20] , (DateTime)dynConstraints[21] , (DateTime)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] , (DateTime)dynConstraints[26] , (String)dynConstraints[27] , (DateTime)dynConstraints[28] , (short)dynConstraints[29] , (bool)dynConstraints[30] , (int)dynConstraints[31] , (int)dynConstraints[32] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00FC2 ;
          prmH00FC2 = new Object[] {
          new Object[] {"@AV6WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV68WWRegrasContagemDS_3_Regrascontagem_regra1",SqlDbType.VarChar,255,0} ,
          new Object[] {"@AV69WWRegrasContagemDS_4_Regrascontagem_data1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV70WWRegrasContagemDS_5_Regrascontagem_data_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV71WWRegrasContagemDS_6_Regrascontagem_responsavel1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV74WWRegrasContagemDS_9_Regrascontagem_regra2",SqlDbType.VarChar,255,0} ,
          new Object[] {"@AV75WWRegrasContagemDS_10_Regrascontagem_data2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV76WWRegrasContagemDS_11_Regrascontagem_data_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV77WWRegrasContagemDS_12_Regrascontagem_responsavel2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV80WWRegrasContagemDS_15_Regrascontagem_regra3",SqlDbType.VarChar,255,0} ,
          new Object[] {"@AV81WWRegrasContagemDS_16_Regrascontagem_data3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV82WWRegrasContagemDS_17_Regrascontagem_data_to3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV83WWRegrasContagemDS_18_Regrascontagem_responsavel3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV84WWRegrasContagemDS_19_Tfregrascontagem_regra",SqlDbType.VarChar,255,0} ,
          new Object[] {"@AV85WWRegrasContagemDS_20_Tfregrascontagem_regra_sel",SqlDbType.VarChar,255,0} ,
          new Object[] {"@AV86WWRegrasContagemDS_21_Tfregrascontagem_data",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV87WWRegrasContagemDS_22_Tfregrascontagem_data_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV88WWRegrasContagemDS_23_Tfregrascontagem_validade",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV89WWRegrasContagemDS_24_Tfregrascontagem_validade_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV90WWRegrasContagemDS_25_Tfregrascontagem_responsavel",SqlDbType.Char,50,0} ,
          new Object[] {"@AV91WWRegrasContagemDS_26_Tfregrascontagem_responsavel_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH00FC3 ;
          prmH00FC3 = new Object[] {
          new Object[] {"@AV6WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV68WWRegrasContagemDS_3_Regrascontagem_regra1",SqlDbType.VarChar,255,0} ,
          new Object[] {"@AV69WWRegrasContagemDS_4_Regrascontagem_data1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV70WWRegrasContagemDS_5_Regrascontagem_data_to1",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV71WWRegrasContagemDS_6_Regrascontagem_responsavel1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV74WWRegrasContagemDS_9_Regrascontagem_regra2",SqlDbType.VarChar,255,0} ,
          new Object[] {"@AV75WWRegrasContagemDS_10_Regrascontagem_data2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV76WWRegrasContagemDS_11_Regrascontagem_data_to2",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV77WWRegrasContagemDS_12_Regrascontagem_responsavel2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV80WWRegrasContagemDS_15_Regrascontagem_regra3",SqlDbType.VarChar,255,0} ,
          new Object[] {"@AV81WWRegrasContagemDS_16_Regrascontagem_data3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV82WWRegrasContagemDS_17_Regrascontagem_data_to3",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV83WWRegrasContagemDS_18_Regrascontagem_responsavel3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV84WWRegrasContagemDS_19_Tfregrascontagem_regra",SqlDbType.VarChar,255,0} ,
          new Object[] {"@AV85WWRegrasContagemDS_20_Tfregrascontagem_regra_sel",SqlDbType.VarChar,255,0} ,
          new Object[] {"@AV86WWRegrasContagemDS_21_Tfregrascontagem_data",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV87WWRegrasContagemDS_22_Tfregrascontagem_data_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV88WWRegrasContagemDS_23_Tfregrascontagem_validade",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV89WWRegrasContagemDS_24_Tfregrascontagem_validade_to",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV90WWRegrasContagemDS_25_Tfregrascontagem_responsavel",SqlDbType.Char,50,0} ,
          new Object[] {"@AV91WWRegrasContagemDS_26_Tfregrascontagem_responsavel_sel",SqlDbType.Char,50,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00FC2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00FC2,11,0,true,false )
             ,new CursorDef("H00FC3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00FC3,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getLongVarchar(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 50) ;
                ((DateTime[]) buf[3])[0] = rslt.getGXDate(4) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((DateTime[]) buf[5])[0] = rslt.getGXDate(5) ;
                ((String[]) buf[6])[0] = rslt.getVarchar(6) ;
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[26]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[28]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[29]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[31]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[32]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[33]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[36]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[37]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[38]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[39]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[40]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[41]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[42]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[43]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[44]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[45]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[46]);
                }
                if ( (short)parms[21] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[47]);
                }
                if ( (short)parms[22] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[48]);
                }
                if ( (short)parms[23] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[49]);
                }
                if ( (short)parms[24] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[50]);
                }
                if ( (short)parms[25] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[51]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[21]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[23]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[24]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[27]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[28]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[30]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[31]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[32]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[33]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[34]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[35]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[36]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[37]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[38]);
                }
                if ( (short)parms[18] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[39]);
                }
                if ( (short)parms[19] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[40]);
                }
                if ( (short)parms[20] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[41]);
                }
                return;
       }
    }

 }

}
