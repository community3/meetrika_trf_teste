/*
               File: type_SdtQueryViewerAxes_Axis_Format_ConditionalStyle
        Description: QueryViewerAxes
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/29/2020 0:29:57.93
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "QueryViewerAxes.Axis.Format.ConditionalStyle" )]
   [XmlType(TypeName =  "QueryViewerAxes.Axis.Format.ConditionalStyle" , Namespace = "GxEv3Up14_MeetrikaVs3" )]
   [Serializable]
   public class SdtQueryViewerAxes_Axis_Format_ConditionalStyle : GxUserType
   {
      public SdtQueryViewerAxes_Axis_Format_ConditionalStyle( )
      {
         /* Constructor for serialization */
         gxTv_SdtQueryViewerAxes_Axis_Format_ConditionalStyle_Operator = "";
         gxTv_SdtQueryViewerAxes_Axis_Format_ConditionalStyle_Value1 = "";
         gxTv_SdtQueryViewerAxes_Axis_Format_ConditionalStyle_Value2 = "";
         gxTv_SdtQueryViewerAxes_Axis_Format_ConditionalStyle_Styleorclass = "";
      }

      public SdtQueryViewerAxes_Axis_Format_ConditionalStyle( IGxContext context )
      {
         this.context = context;
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         xmls = new XmlSerializer(this.GetType(), sNameSpace);
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtQueryViewerAxes_Axis_Format_ConditionalStyle deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "GxEv3Up14_MeetrikaVs3" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtQueryViewerAxes_Axis_Format_ConditionalStyle)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtQueryViewerAxes_Axis_Format_ConditionalStyle obj ;
         obj = this;
         obj.gxTpr_Operator = deserialized.gxTpr_Operator;
         obj.gxTpr_Value1 = deserialized.gxTpr_Value1;
         obj.gxTpr_Value2 = deserialized.gxTpr_Value2;
         obj.gxTpr_Styleorclass = deserialized.gxTpr_Styleorclass;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "Operator") )
               {
                  gxTv_SdtQueryViewerAxes_Axis_Format_ConditionalStyle_Operator = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Value1") )
               {
                  gxTv_SdtQueryViewerAxes_Axis_Format_ConditionalStyle_Value1 = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Value2") )
               {
                  gxTv_SdtQueryViewerAxes_Axis_Format_ConditionalStyle_Value2 = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "StyleOrClass") )
               {
                  gxTv_SdtQueryViewerAxes_Axis_Format_ConditionalStyle_Styleorclass = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "QueryViewerAxes.Axis.Format.ConditionalStyle";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("Operator", StringUtil.RTrim( gxTv_SdtQueryViewerAxes_Axis_Format_ConditionalStyle_Operator));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Value1", StringUtil.RTrim( gxTv_SdtQueryViewerAxes_Axis_Format_ConditionalStyle_Value1));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Value2", StringUtil.RTrim( gxTv_SdtQueryViewerAxes_Axis_Format_ConditionalStyle_Value2));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("StyleOrClass", StringUtil.RTrim( gxTv_SdtQueryViewerAxes_Axis_Format_ConditionalStyle_Styleorclass));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("Operator", gxTv_SdtQueryViewerAxes_Axis_Format_ConditionalStyle_Operator, false);
         AddObjectProperty("Value1", gxTv_SdtQueryViewerAxes_Axis_Format_ConditionalStyle_Value1, false);
         AddObjectProperty("Value2", gxTv_SdtQueryViewerAxes_Axis_Format_ConditionalStyle_Value2, false);
         AddObjectProperty("StyleOrClass", gxTv_SdtQueryViewerAxes_Axis_Format_ConditionalStyle_Styleorclass, false);
         return  ;
      }

      [  SoapElement( ElementName = "Operator" )]
      [  XmlElement( ElementName = "Operator"   )]
      public String gxTpr_Operator
      {
         get {
            return gxTv_SdtQueryViewerAxes_Axis_Format_ConditionalStyle_Operator ;
         }

         set {
            gxTv_SdtQueryViewerAxes_Axis_Format_ConditionalStyle_Operator = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Value1" )]
      [  XmlElement( ElementName = "Value1"   )]
      public String gxTpr_Value1
      {
         get {
            return gxTv_SdtQueryViewerAxes_Axis_Format_ConditionalStyle_Value1 ;
         }

         set {
            gxTv_SdtQueryViewerAxes_Axis_Format_ConditionalStyle_Value1 = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Value2" )]
      [  XmlElement( ElementName = "Value2"   )]
      public String gxTpr_Value2
      {
         get {
            return gxTv_SdtQueryViewerAxes_Axis_Format_ConditionalStyle_Value2 ;
         }

         set {
            gxTv_SdtQueryViewerAxes_Axis_Format_ConditionalStyle_Value2 = (String)(value);
         }

      }

      [  SoapElement( ElementName = "StyleOrClass" )]
      [  XmlElement( ElementName = "StyleOrClass"   )]
      public String gxTpr_Styleorclass
      {
         get {
            return gxTv_SdtQueryViewerAxes_Axis_Format_ConditionalStyle_Styleorclass ;
         }

         set {
            gxTv_SdtQueryViewerAxes_Axis_Format_ConditionalStyle_Styleorclass = (String)(value);
         }

      }

      public void initialize( )
      {
         gxTv_SdtQueryViewerAxes_Axis_Format_ConditionalStyle_Operator = "";
         gxTv_SdtQueryViewerAxes_Axis_Format_ConditionalStyle_Value1 = "";
         gxTv_SdtQueryViewerAxes_Axis_Format_ConditionalStyle_Value2 = "";
         gxTv_SdtQueryViewerAxes_Axis_Format_ConditionalStyle_Styleorclass = "";
         sTagName = "";
         return  ;
      }

      protected short readOk ;
      protected short nOutParmCount ;
      protected String gxTv_SdtQueryViewerAxes_Axis_Format_ConditionalStyle_Operator ;
      protected String gxTv_SdtQueryViewerAxes_Axis_Format_ConditionalStyle_Value1 ;
      protected String gxTv_SdtQueryViewerAxes_Axis_Format_ConditionalStyle_Value2 ;
      protected String gxTv_SdtQueryViewerAxes_Axis_Format_ConditionalStyle_Styleorclass ;
      protected String sTagName ;
   }

   [DataContract(Name = @"QueryViewerAxes.Axis.Format.ConditionalStyle", Namespace = "GxEv3Up14_MeetrikaVs3")]
   public class SdtQueryViewerAxes_Axis_Format_ConditionalStyle_RESTInterface : GxGenericCollectionItem<SdtQueryViewerAxes_Axis_Format_ConditionalStyle>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtQueryViewerAxes_Axis_Format_ConditionalStyle_RESTInterface( ) : base()
      {
      }

      public SdtQueryViewerAxes_Axis_Format_ConditionalStyle_RESTInterface( SdtQueryViewerAxes_Axis_Format_ConditionalStyle psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "Operator" , Order = 0 )]
      public String gxTpr_Operator
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Operator) ;
         }

         set {
            sdt.gxTpr_Operator = (String)(value);
         }

      }

      [DataMember( Name = "Value1" , Order = 1 )]
      public String gxTpr_Value1
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Value1) ;
         }

         set {
            sdt.gxTpr_Value1 = (String)(value);
         }

      }

      [DataMember( Name = "Value2" , Order = 2 )]
      public String gxTpr_Value2
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Value2) ;
         }

         set {
            sdt.gxTpr_Value2 = (String)(value);
         }

      }

      [DataMember( Name = "StyleOrClass" , Order = 3 )]
      public String gxTpr_Styleorclass
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Styleorclass) ;
         }

         set {
            sdt.gxTpr_Styleorclass = (String)(value);
         }

      }

      public SdtQueryViewerAxes_Axis_Format_ConditionalStyle sdt
      {
         get {
            return (SdtQueryViewerAxes_Axis_Format_ConditionalStyle)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtQueryViewerAxes_Axis_Format_ConditionalStyle() ;
         }
      }

   }

}
