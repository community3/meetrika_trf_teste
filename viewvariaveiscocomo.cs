/*
               File: ViewVariaveisCocomo
        Description: View Variaveis Cocomo
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/7/2020 0:1:8.20
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class viewvariaveiscocomo : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public viewvariaveiscocomo( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public viewvariaveiscocomo( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_VariavelCocomo_AreaTrabalhoCod ,
                           String aP1_VariavelCocomo_Sigla ,
                           String aP2_VariavelCocomo_Tipo ,
                           String aP3_TabCode )
      {
         this.AV9VariavelCocomo_AreaTrabalhoCod = aP0_VariavelCocomo_AreaTrabalhoCod;
         this.AV10VariavelCocomo_Sigla = aP1_VariavelCocomo_Sigla;
         this.AV11VariavelCocomo_Tipo = aP2_VariavelCocomo_Tipo;
         this.AV7TabCode = aP3_TabCode;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               AV9VariavelCocomo_AreaTrabalhoCod = (int)(NumberUtil.Val( gxfirstwebparm, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9VariavelCocomo_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9VariavelCocomo_AreaTrabalhoCod), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vVARIAVELCOCOMO_AREATRABALHOCOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV9VariavelCocomo_AreaTrabalhoCod), "ZZZZZ9")));
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
               {
                  AV10VariavelCocomo_Sigla = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10VariavelCocomo_Sigla", AV10VariavelCocomo_Sigla);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vVARIAVELCOCOMO_SIGLA", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV10VariavelCocomo_Sigla, "@!"))));
                  AV11VariavelCocomo_Tipo = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11VariavelCocomo_Tipo", AV11VariavelCocomo_Tipo);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vVARIAVELCOCOMO_TIPO", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV11VariavelCocomo_Tipo, ""))));
                  AV7TabCode = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7TabCode", AV7TabCode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vTABCODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV7TabCode, ""))));
               }
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PAGJ2( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            STARTGJ2( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20205701825");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("viewvariaveiscocomo.aspx") + "?" + UrlEncode("" +AV9VariavelCocomo_AreaTrabalhoCod) + "," + UrlEncode(StringUtil.RTrim(AV10VariavelCocomo_Sigla)) + "," + UrlEncode(StringUtil.RTrim(AV11VariavelCocomo_Tipo)) + "," + UrlEncode(StringUtil.RTrim(AV7TabCode))+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "vVARIAVELCOCOMO_AREATRABALHOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV9VariavelCocomo_AreaTrabalhoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vVARIAVELCOCOMO_SIGLA", StringUtil.RTrim( AV10VariavelCocomo_Sigla));
         GxWebStd.gx_hidden_field( context, "vVARIAVELCOCOMO_TIPO", StringUtil.RTrim( AV11VariavelCocomo_Tipo));
         GxWebStd.gx_hidden_field( context, "vTABCODE", StringUtil.RTrim( AV7TabCode));
         GxWebStd.gx_hidden_field( context, "gxhash_VARIAVELCOCOMO_NOME", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A963VariavelCocomo_Nome, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vVARIAVELCOCOMO_AREATRABALHOCOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV9VariavelCocomo_AreaTrabalhoCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vVARIAVELCOCOMO_SIGLA", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV10VariavelCocomo_Sigla, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vVARIAVELCOCOMO_TIPO", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV11VariavelCocomo_Tipo, ""))));
         GxWebStd.gx_hidden_field( context, "gxhash_vTABCODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV7TabCode, ""))));
         GxWebStd.gx_hidden_field( context, "gxhash_vVARIAVELCOCOMO_AREATRABALHOCOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV9VariavelCocomo_AreaTrabalhoCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vVARIAVELCOCOMO_SIGLA", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV10VariavelCocomo_Sigla, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vVARIAVELCOCOMO_TIPO", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV11VariavelCocomo_Tipo, ""))));
         GxWebStd.gx_hidden_field( context, "gxhash_vTABCODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV7TabCode, ""))));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = "hsh" + "ViewVariaveisCocomo";
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( A963VariavelCocomo_Nome, "@!"));
         GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("viewvariaveiscocomo:[SendSecurityCheck value for]"+"VariavelCocomo_Nome:"+StringUtil.RTrim( context.localUtil.Format( A963VariavelCocomo_Nome, "@!")));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
         if ( ! ( WebComp_Tabbedview == null ) )
         {
            WebComp_Tabbedview.componentjscripts();
         }
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WEGJ2( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVTGJ2( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return false ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("viewvariaveiscocomo.aspx") + "?" + UrlEncode("" +AV9VariavelCocomo_AreaTrabalhoCod) + "," + UrlEncode(StringUtil.RTrim(AV10VariavelCocomo_Sigla)) + "," + UrlEncode(StringUtil.RTrim(AV11VariavelCocomo_Tipo)) + "," + UrlEncode(StringUtil.RTrim(AV7TabCode)) ;
      }

      public override String GetPgmname( )
      {
         return "ViewVariaveisCocomo" ;
      }

      public override String GetPgmdesc( )
      {
         return "View Variaveis Cocomo" ;
      }

      protected void WBGJ0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, "", "", "", "false");
            wb_table1_2_GJ2( true) ;
         }
         else
         {
            wb_table1_2_GJ2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_GJ2e( bool wbgen )
      {
         if ( wbgen )
         {
         }
         wbLoad = true;
      }

      protected void STARTGJ2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "View Variaveis Cocomo", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPGJ0( ) ;
      }

      protected void WSGJ2( )
      {
         STARTGJ2( ) ;
         EVTGJ2( ) ;
      }

      protected void EVTGJ2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E11GJ2 */
                              E11GJ2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E12GJ2 */
                              E12GJ2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              context.wbHandled = 1;
                              if ( ! wbErr )
                              {
                                 Rfr0gs = false;
                                 if ( ! Rfr0gs )
                                 {
                                 }
                                 dynload_actions( ) ;
                              }
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                        }
                     }
                     else if ( StringUtil.StrCmp(sEvtType, "W") == 0 )
                     {
                        sEvtType = StringUtil.Left( sEvt, 4);
                        sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-4));
                        nCmpId = (short)(NumberUtil.Val( sEvtType, "."));
                        if ( nCmpId == 16 )
                        {
                           OldTabbedview = cgiGet( "W0016");
                           if ( ( StringUtil.Len( OldTabbedview) == 0 ) || ( StringUtil.StrCmp(OldTabbedview, WebComp_Tabbedview_Component) != 0 ) )
                           {
                              WebComp_Tabbedview = getWebComponent(GetType(), "GeneXus.Programs", OldTabbedview, new Object[] {context} );
                              WebComp_Tabbedview.ComponentInit();
                              WebComp_Tabbedview.Name = "OldTabbedview";
                              WebComp_Tabbedview_Component = OldTabbedview;
                           }
                           if ( StringUtil.Len( WebComp_Tabbedview_Component) != 0 )
                           {
                              WebComp_Tabbedview.componentprocess("W0016", "", sEvt);
                           }
                           WebComp_Tabbedview_Component = OldTabbedview;
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEGJ2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PAGJ2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFGJ2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      protected void RFGJ2( )
      {
         initialize_formulas( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            if ( 1 != 0 )
            {
               if ( StringUtil.Len( WebComp_Tabbedview_Component) != 0 )
               {
                  WebComp_Tabbedview.componentstart();
               }
            }
         }
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Using cursor H00GJ2 */
            pr_default.execute(0, new Object[] {AV9VariavelCocomo_AreaTrabalhoCod, AV10VariavelCocomo_Sigla, AV11VariavelCocomo_Tipo});
            while ( (pr_default.getStatus(0) != 101) )
            {
               A964VariavelCocomo_Tipo = H00GJ2_A964VariavelCocomo_Tipo[0];
               A962VariavelCocomo_Sigla = H00GJ2_A962VariavelCocomo_Sigla[0];
               A961VariavelCocomo_AreaTrabalhoCod = H00GJ2_A961VariavelCocomo_AreaTrabalhoCod[0];
               A963VariavelCocomo_Nome = H00GJ2_A963VariavelCocomo_Nome[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A963VariavelCocomo_Nome", A963VariavelCocomo_Nome);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_VARIAVELCOCOMO_NOME", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A963VariavelCocomo_Nome, "@!"))));
               /* Execute user event: E12GJ2 */
               E12GJ2 ();
               pr_default.readNext(0);
            }
            pr_default.close(0);
            WBGJ0( ) ;
         }
      }

      protected void STRUPGJ0( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11GJ2 */
         E11GJ2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            A963VariavelCocomo_Nome = StringUtil.Upper( cgiGet( edtVariavelCocomo_Nome_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A963VariavelCocomo_Nome", A963VariavelCocomo_Nome);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_VARIAVELCOCOMO_NOME", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A963VariavelCocomo_Nome, "@!"))));
            /* Read saved values. */
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            forbiddenHiddens = "hsh" + "ViewVariaveisCocomo";
            A963VariavelCocomo_Nome = cgiGet( edtVariavelCocomo_Nome_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A963VariavelCocomo_Nome", A963VariavelCocomo_Nome);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_VARIAVELCOCOMO_NOME", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A963VariavelCocomo_Nome, "@!"))));
            forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( A963VariavelCocomo_Nome, "@!"));
            hsh = cgiGet( "hsh");
            if ( ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
            {
               GXUtil.WriteLog("viewvariaveiscocomo:[SecurityCheckFailed value for]"+"VariavelCocomo_Nome:"+StringUtil.RTrim( context.localUtil.Format( A963VariavelCocomo_Nome, "@!")));
               GxWebError = 1;
               context.HttpContext.Response.StatusDescription = 403.ToString();
               context.HttpContext.Response.StatusCode = 403;
               context.WriteHtmlText( "<title>403 Forbidden</title>") ;
               context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
               context.WriteHtmlText( "<p /><hr />") ;
               GXUtil.WriteLog("send_http_error_code " + 403.ToString());
               return  ;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E11GJ2 */
         E11GJ2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E11GJ2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         lblWorkwithlink_Link = formatLink("wwvariaveiscocomo.aspx") ;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblWorkwithlink_Internalname, "Link", lblWorkwithlink_Link);
         AV16GXLvl9 = 0;
         /* Using cursor H00GJ3 */
         pr_default.execute(1, new Object[] {AV9VariavelCocomo_AreaTrabalhoCod, AV10VariavelCocomo_Sigla, AV11VariavelCocomo_Tipo});
         while ( (pr_default.getStatus(1) != 101) )
         {
            A964VariavelCocomo_Tipo = H00GJ3_A964VariavelCocomo_Tipo[0];
            A962VariavelCocomo_Sigla = H00GJ3_A962VariavelCocomo_Sigla[0];
            A961VariavelCocomo_AreaTrabalhoCod = H00GJ3_A961VariavelCocomo_AreaTrabalhoCod[0];
            A963VariavelCocomo_Nome = H00GJ3_A963VariavelCocomo_Nome[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A963VariavelCocomo_Nome", A963VariavelCocomo_Nome);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_VARIAVELCOCOMO_NOME", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A963VariavelCocomo_Nome, "@!"))));
            AV16GXLvl9 = 1;
            Form.Caption = A963VariavelCocomo_Nome;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
            AV8Exists = true;
            pr_default.readNext(1);
         }
         pr_default.close(1);
         if ( AV16GXLvl9 == 0 )
         {
            Form.Caption = "Registro n�o encontrado ";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
            AV8Exists = false;
         }
         if ( AV8Exists )
         {
            /* Execute user subroutine: 'LOADTABS' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            /* Object Property */
            if ( StringUtil.StrCmp(StringUtil.Lower( WebComp_Tabbedview_Component), StringUtil.Lower( "WWPBaseObjects.WWPTabbedView")) != 0 )
            {
               WebComp_Tabbedview = getWebComponent(GetType(), "GeneXus.Programs", "wwpbaseobjects.wwptabbedview", new Object[] {context} );
               WebComp_Tabbedview.ComponentInit();
               WebComp_Tabbedview.Name = "WWPBaseObjects.WWPTabbedView";
               WebComp_Tabbedview_Component = "WWPBaseObjects.WWPTabbedView";
            }
            if ( StringUtil.Len( WebComp_Tabbedview_Component) != 0 )
            {
               WebComp_Tabbedview.setjustcreated();
               WebComp_Tabbedview.componentprepare(new Object[] {(String)"W0016",(String)"",(IGxCollection)AV12Tabs,(String)AV7TabCode});
               WebComp_Tabbedview.componentbind(new Object[] {(String)"",(String)""});
            }
         }
      }

      protected void nextLoad( )
      {
      }

      protected void E12GJ2( )
      {
         /* Load Routine */
      }

      protected void S112( )
      {
         /* 'LOADTABS' Routine */
         AV12Tabs = new GxObjectCollection( context, "WWPTabOptions.TabOptionsItem", "GxEv3Up14_MeetrikaVs3", "wwpbaseobjects.SdtWWPTabOptions_TabOptionsItem", "GeneXus.Programs");
         AV13Tab = new wwpbaseobjects.SdtWWPTabOptions_TabOptionsItem(context);
         AV13Tab.gxTpr_Code = "General";
         AV13Tab.gxTpr_Description = "Dados";
         AV13Tab.gxTpr_Webcomponent = formatLink("variaveiscocomogeneral.aspx") + "?" + UrlEncode("" +AV9VariavelCocomo_AreaTrabalhoCod) + "," + UrlEncode(StringUtil.RTrim(AV10VariavelCocomo_Sigla)) + "," + UrlEncode(StringUtil.RTrim(AV11VariavelCocomo_Tipo));
         AV13Tab.gxTpr_Link = formatLink("viewvariaveiscocomo.aspx") + "?" + UrlEncode("" +AV9VariavelCocomo_AreaTrabalhoCod) + "," + UrlEncode(StringUtil.RTrim(AV10VariavelCocomo_Sigla)) + "," + UrlEncode(StringUtil.RTrim(AV11VariavelCocomo_Tipo)) + "," + UrlEncode(StringUtil.RTrim(AV13Tab.gxTpr_Code));
         AV13Tab.gxTpr_Includeinpanel = 0;
         AV13Tab.gxTpr_Collapsable = true;
         AV13Tab.gxTpr_Collapsedbydefault = false;
         AV12Tabs.Add(AV13Tab, 0);
      }

      protected void wb_table1_2_GJ2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableContentNoMargin", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TextBlockTitleCell'>") ;
            wb_table2_5_GJ2( true) ;
         }
         else
         {
            wb_table2_5_GJ2( false) ;
         }
         return  ;
      }

      protected void wb_table2_5_GJ2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td data-align=\"right\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-right;text-align:-moz-right;text-align:-webkit-right")+"\" class='TableTitleCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblWorkwithlink_Internalname, "Voltar", lblWorkwithlink_Link, "", lblWorkwithlink_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockLink", 0, "", 1, 1, 0, "HLP_ViewVariaveisCocomo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"3\" >") ;
            if ( ! isFullAjaxMode( ) )
            {
               /* WebComponent */
               GxWebStd.gx_hidden_field( context, "W0016"+"", StringUtil.RTrim( WebComp_Tabbedview_Component));
               context.WriteHtmlText( "<div") ;
               GxWebStd.ClassAttribute( context, "gxwebcomponent");
               context.WriteHtmlText( " id=\""+"gxHTMLWrpW0016"+""+"\""+"") ;
               context.WriteHtmlText( ">") ;
               if ( StringUtil.Len( WebComp_Tabbedview_Component) != 0 )
               {
                  if ( StringUtil.StrCmp(StringUtil.Lower( OldTabbedview), StringUtil.Lower( WebComp_Tabbedview_Component)) != 0 )
                  {
                     context.httpAjaxContext.ajax_rspStartCmp("gxHTMLWrpW0016"+"");
                  }
                  WebComp_Tabbedview.componentdraw();
                  if ( StringUtil.StrCmp(StringUtil.Lower( OldTabbedview), StringUtil.Lower( WebComp_Tabbedview_Component)) != 0 )
                  {
                     context.httpAjaxContext.ajax_rspEndCmp();
                  }
               }
               context.WriteHtmlText( "</div>") ;
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_GJ2e( true) ;
         }
         else
         {
            wb_table1_2_GJ2e( false) ;
         }
      }

      protected void wb_table2_5_GJ2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedviewtitle_Internalname, tblTablemergedviewtitle_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblViewtitle_Internalname, "Variaveis Cocomo :: ", "", "", lblViewtitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_ViewVariaveisCocomo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtVariavelCocomo_Nome_Internalname, StringUtil.RTrim( A963VariavelCocomo_Nome), StringUtil.RTrim( context.localUtil.Format( A963VariavelCocomo_Nome, "@!")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtVariavelCocomo_Nome_Jsonclick, 0, "AttributeTitleWWP", "", "", "", 1, 0, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Nome", "left", true, "HLP_ViewVariaveisCocomo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_GJ2e( true) ;
         }
         else
         {
            wb_table2_5_GJ2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV9VariavelCocomo_AreaTrabalhoCod = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9VariavelCocomo_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9VariavelCocomo_AreaTrabalhoCod), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vVARIAVELCOCOMO_AREATRABALHOCOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV9VariavelCocomo_AreaTrabalhoCod), "ZZZZZ9")));
         AV10VariavelCocomo_Sigla = (String)getParm(obj,1);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10VariavelCocomo_Sigla", AV10VariavelCocomo_Sigla);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vVARIAVELCOCOMO_SIGLA", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV10VariavelCocomo_Sigla, "@!"))));
         AV11VariavelCocomo_Tipo = (String)getParm(obj,2);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11VariavelCocomo_Tipo", AV11VariavelCocomo_Tipo);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vVARIAVELCOCOMO_TIPO", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV11VariavelCocomo_Tipo, ""))));
         AV7TabCode = (String)getParm(obj,3);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7TabCode", AV7TabCode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vTABCODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV7TabCode, ""))));
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAGJ2( ) ;
         WSGJ2( ) ;
         WEGJ2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         if ( ! ( WebComp_Tabbedview == null ) )
         {
            if ( StringUtil.Len( WebComp_Tabbedview_Component) != 0 )
            {
               WebComp_Tabbedview.componentthemes();
            }
         }
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20205701847");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("viewvariaveiscocomo.js", "?20205701847");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblViewtitle_Internalname = "VIEWTITLE";
         edtVariavelCocomo_Nome_Internalname = "VARIAVELCOCOMO_NOME";
         tblTablemergedviewtitle_Internalname = "TABLEMERGEDVIEWTITLE";
         lblWorkwithlink_Internalname = "WORKWITHLINK";
         tblTablemain_Internalname = "TABLEMAIN";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtVariavelCocomo_Nome_Jsonclick = "";
         lblWorkwithlink_Link = "";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "View Variaveis Cocomo";
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         wcpOAV10VariavelCocomo_Sigla = "";
         wcpOAV11VariavelCocomo_Tipo = "";
         wcpOAV7TabCode = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         A963VariavelCocomo_Nome = "";
         GXKey = "";
         forbiddenHiddens = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         OldTabbedview = "";
         WebComp_Tabbedview_Component = "";
         scmdbuf = "";
         H00GJ2_A992VariavelCocomo_Sequencial = new short[1] ;
         H00GJ2_A964VariavelCocomo_Tipo = new String[] {""} ;
         H00GJ2_A962VariavelCocomo_Sigla = new String[] {""} ;
         H00GJ2_A961VariavelCocomo_AreaTrabalhoCod = new int[1] ;
         H00GJ2_A963VariavelCocomo_Nome = new String[] {""} ;
         A964VariavelCocomo_Tipo = "";
         A962VariavelCocomo_Sigla = "";
         hsh = "";
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         H00GJ3_A992VariavelCocomo_Sequencial = new short[1] ;
         H00GJ3_A964VariavelCocomo_Tipo = new String[] {""} ;
         H00GJ3_A962VariavelCocomo_Sigla = new String[] {""} ;
         H00GJ3_A961VariavelCocomo_AreaTrabalhoCod = new int[1] ;
         H00GJ3_A963VariavelCocomo_Nome = new String[] {""} ;
         AV12Tabs = new GxObjectCollection( context, "WWPTabOptions.TabOptionsItem", "GxEv3Up14_MeetrikaVs3", "wwpbaseobjects.SdtWWPTabOptions_TabOptionsItem", "GeneXus.Programs");
         AV13Tab = new wwpbaseobjects.SdtWWPTabOptions_TabOptionsItem(context);
         sStyleString = "";
         lblWorkwithlink_Jsonclick = "";
         lblViewtitle_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.viewvariaveiscocomo__default(),
            new Object[][] {
                new Object[] {
               H00GJ2_A992VariavelCocomo_Sequencial, H00GJ2_A964VariavelCocomo_Tipo, H00GJ2_A962VariavelCocomo_Sigla, H00GJ2_A961VariavelCocomo_AreaTrabalhoCod, H00GJ2_A963VariavelCocomo_Nome
               }
               , new Object[] {
               H00GJ3_A992VariavelCocomo_Sequencial, H00GJ3_A964VariavelCocomo_Tipo, H00GJ3_A962VariavelCocomo_Sigla, H00GJ3_A961VariavelCocomo_AreaTrabalhoCod, H00GJ3_A963VariavelCocomo_Nome
               }
            }
         );
         WebComp_Tabbedview = new GeneXus.Http.GXNullWebComponent();
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short wbEnd ;
      private short wbStart ;
      private short nCmpId ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short AV16GXLvl9 ;
      private short nGXWrapped ;
      private int AV9VariavelCocomo_AreaTrabalhoCod ;
      private int wcpOAV9VariavelCocomo_AreaTrabalhoCod ;
      private int A961VariavelCocomo_AreaTrabalhoCod ;
      private int idxLst ;
      private String AV10VariavelCocomo_Sigla ;
      private String AV11VariavelCocomo_Tipo ;
      private String AV7TabCode ;
      private String wcpOAV10VariavelCocomo_Sigla ;
      private String wcpOAV11VariavelCocomo_Tipo ;
      private String wcpOAV7TabCode ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String A963VariavelCocomo_Nome ;
      private String GXKey ;
      private String forbiddenHiddens ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String OldTabbedview ;
      private String WebComp_Tabbedview_Component ;
      private String scmdbuf ;
      private String A964VariavelCocomo_Tipo ;
      private String A962VariavelCocomo_Sigla ;
      private String edtVariavelCocomo_Nome_Internalname ;
      private String hsh ;
      private String lblWorkwithlink_Link ;
      private String lblWorkwithlink_Internalname ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String lblWorkwithlink_Jsonclick ;
      private String tblTablemergedviewtitle_Internalname ;
      private String lblViewtitle_Internalname ;
      private String lblViewtitle_Jsonclick ;
      private String edtVariavelCocomo_Nome_Jsonclick ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool returnInSub ;
      private bool AV8Exists ;
      private GXWebComponent WebComp_Tabbedview ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private short[] H00GJ2_A992VariavelCocomo_Sequencial ;
      private String[] H00GJ2_A964VariavelCocomo_Tipo ;
      private String[] H00GJ2_A962VariavelCocomo_Sigla ;
      private int[] H00GJ2_A961VariavelCocomo_AreaTrabalhoCod ;
      private String[] H00GJ2_A963VariavelCocomo_Nome ;
      private short[] H00GJ3_A992VariavelCocomo_Sequencial ;
      private String[] H00GJ3_A964VariavelCocomo_Tipo ;
      private String[] H00GJ3_A962VariavelCocomo_Sigla ;
      private int[] H00GJ3_A961VariavelCocomo_AreaTrabalhoCod ;
      private String[] H00GJ3_A963VariavelCocomo_Nome ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtWWPTabOptions_TabOptionsItem ))]
      private IGxCollection AV12Tabs ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTabOptions_TabOptionsItem AV13Tab ;
   }

   public class viewvariaveiscocomo__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00GJ2 ;
          prmH00GJ2 = new Object[] {
          new Object[] {"@AV9VariavelCocomo_AreaTrabalhoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV10VariavelCocomo_Sigla",SqlDbType.Char,15,0} ,
          new Object[] {"@AV11VariavelCocomo_Tipo",SqlDbType.Char,1,0}
          } ;
          Object[] prmH00GJ3 ;
          prmH00GJ3 = new Object[] {
          new Object[] {"@AV9VariavelCocomo_AreaTrabalhoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV10VariavelCocomo_Sigla",SqlDbType.Char,15,0} ,
          new Object[] {"@AV11VariavelCocomo_Tipo",SqlDbType.Char,1,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00GJ2", "SELECT [VariavelCocomo_Sequencial], [VariavelCocomo_Tipo], [VariavelCocomo_Sigla], [VariavelCocomo_AreaTrabalhoCod], [VariavelCocomo_Nome] FROM [VariaveisCocomo] WITH (NOLOCK) WHERE [VariavelCocomo_AreaTrabalhoCod] = @AV9VariavelCocomo_AreaTrabalhoCod and [VariavelCocomo_Sigla] = @AV10VariavelCocomo_Sigla and [VariavelCocomo_Tipo] = @AV11VariavelCocomo_Tipo ORDER BY [VariavelCocomo_AreaTrabalhoCod], [VariavelCocomo_Sigla], [VariavelCocomo_Tipo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00GJ2,100,0,true,false )
             ,new CursorDef("H00GJ3", "SELECT [VariavelCocomo_Sequencial], [VariavelCocomo_Tipo], [VariavelCocomo_Sigla], [VariavelCocomo_AreaTrabalhoCod], [VariavelCocomo_Nome] FROM [VariaveisCocomo] WITH (NOLOCK) WHERE ([VariavelCocomo_AreaTrabalhoCod] = @AV9VariavelCocomo_AreaTrabalhoCod and [VariavelCocomo_Sigla] = @AV10VariavelCocomo_Sigla and [VariavelCocomo_Tipo] = @AV11VariavelCocomo_Tipo) AND ([VariavelCocomo_AreaTrabalhoCod] = @AV9VariavelCocomo_AreaTrabalhoCod and [VariavelCocomo_Sigla] = @AV10VariavelCocomo_Sigla and [VariavelCocomo_Tipo] = @AV11VariavelCocomo_Tipo) ORDER BY [VariavelCocomo_AreaTrabalhoCod], [VariavelCocomo_Sigla], [VariavelCocomo_Tipo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00GJ3,100,0,false,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 1) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 15) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                ((String[]) buf[4])[0] = rslt.getString(5, 50) ;
                return;
             case 1 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 1) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 15) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                ((String[]) buf[4])[0] = rslt.getString(5, 50) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                stmt.SetParameter(3, (String)parms[2]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                stmt.SetParameter(3, (String)parms[2]);
                return;
       }
    }

 }

}
