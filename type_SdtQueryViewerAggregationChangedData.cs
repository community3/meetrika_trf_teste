/*
               File: type_SdtQueryViewerAggregationChangedData
        Description: QueryViewerAggregationChangedData
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/29/2020 0:29:57.48
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "QueryViewerAggregationChangedData" )]
   [XmlType(TypeName =  "QueryViewerAggregationChangedData" , Namespace = "GxEv3Up14_MeetrikaVs3" )]
   [Serializable]
   public class SdtQueryViewerAggregationChangedData : GxUserType
   {
      public SdtQueryViewerAggregationChangedData( )
      {
         /* Constructor for serialization */
         gxTv_SdtQueryViewerAggregationChangedData_Name = "";
         gxTv_SdtQueryViewerAggregationChangedData_Aggregation = "";
      }

      public SdtQueryViewerAggregationChangedData( IGxContext context )
      {
         this.context = context;
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         xmls = new XmlSerializer(this.GetType(), sNameSpace);
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtQueryViewerAggregationChangedData deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "GxEv3Up14_MeetrikaVs3" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtQueryViewerAggregationChangedData)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtQueryViewerAggregationChangedData obj ;
         obj = this;
         obj.gxTpr_Name = deserialized.gxTpr_Name;
         obj.gxTpr_Aggregation = deserialized.gxTpr_Aggregation;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "Name") )
               {
                  gxTv_SdtQueryViewerAggregationChangedData_Name = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Aggregation") )
               {
                  gxTv_SdtQueryViewerAggregationChangedData_Aggregation = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "QueryViewerAggregationChangedData";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sNameSpace)) )
         {
            sNameSpace = "GxEv3Up14_MeetrikaVs3";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("Name", StringUtil.RTrim( gxTv_SdtQueryViewerAggregationChangedData_Name));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Aggregation", StringUtil.RTrim( gxTv_SdtQueryViewerAggregationChangedData_Aggregation));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("Name", gxTv_SdtQueryViewerAggregationChangedData_Name, false);
         AddObjectProperty("Aggregation", gxTv_SdtQueryViewerAggregationChangedData_Aggregation, false);
         return  ;
      }

      [  SoapElement( ElementName = "Name" )]
      [  XmlElement( ElementName = "Name"   )]
      public String gxTpr_Name
      {
         get {
            return gxTv_SdtQueryViewerAggregationChangedData_Name ;
         }

         set {
            gxTv_SdtQueryViewerAggregationChangedData_Name = (String)(value);
         }

      }

      [  SoapElement( ElementName = "Aggregation" )]
      [  XmlElement( ElementName = "Aggregation"   )]
      public String gxTpr_Aggregation
      {
         get {
            return gxTv_SdtQueryViewerAggregationChangedData_Aggregation ;
         }

         set {
            gxTv_SdtQueryViewerAggregationChangedData_Aggregation = (String)(value);
         }

      }

      public void initialize( )
      {
         gxTv_SdtQueryViewerAggregationChangedData_Name = "";
         gxTv_SdtQueryViewerAggregationChangedData_Aggregation = "";
         sTagName = "";
         return  ;
      }

      protected short readOk ;
      protected short nOutParmCount ;
      protected String gxTv_SdtQueryViewerAggregationChangedData_Name ;
      protected String gxTv_SdtQueryViewerAggregationChangedData_Aggregation ;
      protected String sTagName ;
   }

   [DataContract(Name = @"QueryViewerAggregationChangedData", Namespace = "GxEv3Up14_MeetrikaVs3")]
   public class SdtQueryViewerAggregationChangedData_RESTInterface : GxGenericCollectionItem<SdtQueryViewerAggregationChangedData>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtQueryViewerAggregationChangedData_RESTInterface( ) : base()
      {
      }

      public SdtQueryViewerAggregationChangedData_RESTInterface( SdtQueryViewerAggregationChangedData psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "Name" , Order = 0 )]
      public String gxTpr_Name
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Name) ;
         }

         set {
            sdt.gxTpr_Name = (String)(value);
         }

      }

      [DataMember( Name = "Aggregation" , Order = 1 )]
      public String gxTpr_Aggregation
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Aggregation) ;
         }

         set {
            sdt.gxTpr_Aggregation = (String)(value);
         }

      }

      public SdtQueryViewerAggregationChangedData sdt
      {
         get {
            return (SdtQueryViewerAggregationChangedData)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtQueryViewerAggregationChangedData() ;
         }
      }

   }

}
