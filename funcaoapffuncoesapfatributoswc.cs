/*
               File: FuncaoAPFFuncoesAPFAtributosWC
        Description: Funcao APFFuncoes APFAtributos WC
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:50:35.88
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class funcaoapffuncoesapfatributoswc : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public funcaoapffuncoesapfatributoswc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public funcaoapffuncoesapfatributoswc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_FuncaoAPF_Codigo ,
                           int aP1_FuncaoAPF_SistemaCod )
      {
         this.AV19FuncaoAPF_Codigo = aP0_FuncaoAPF_Codigo;
         this.AV20FuncaoAPF_SistemaCod = aP1_FuncaoAPF_SistemaCod;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
         cmbavOrderedby = new GXCombobox();
         cmbavDynamicfiltersselector1 = new GXCombobox();
         cmbavDynamicfiltersselector2 = new GXCombobox();
         cmbavFuncaoapfatributos_funcaodadostip = new GXCombobox();
         chkavDynamicfiltersenabled2 = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  AV19FuncaoAPF_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV19FuncaoAPF_Codigo), 6, 0)));
                  AV20FuncaoAPF_SistemaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20FuncaoAPF_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20FuncaoAPF_SistemaCod), 6, 0)));
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(int)AV19FuncaoAPF_Codigo,(int)AV20FuncaoAPF_SistemaCod});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
               {
                  nRC_GXsfl_57 = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  nGXsfl_57_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  sGXsfl_57_idx = GetNextPar( );
                  sPrefix = GetNextPar( );
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxnrGrid_newrow( ) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
               {
                  subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  AV30OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV30OrderedBy), 4, 0)));
                  AV31OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV31OrderedDsc", AV31OrderedDsc);
                  AV16DynamicFiltersSelector1 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
                  AV21FuncaoAPFAtributos_AtributosNom1 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21FuncaoAPFAtributos_AtributosNom1", AV21FuncaoAPFAtributos_AtributosNom1);
                  AV23FuncaoAPFAtributos_AtrTabelaNom1 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23FuncaoAPFAtributos_AtrTabelaNom1", AV23FuncaoAPFAtributos_AtrTabelaNom1);
                  AV17DynamicFiltersSelector2 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17DynamicFiltersSelector2", AV17DynamicFiltersSelector2);
                  AV22FuncaoAPFAtributos_AtributosNom2 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22FuncaoAPFAtributos_AtributosNom2", AV22FuncaoAPFAtributos_AtributosNom2);
                  AV24FuncaoAPFAtributos_AtrTabelaNom2 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24FuncaoAPFAtributos_AtrTabelaNom2", AV24FuncaoAPFAtributos_AtrTabelaNom2);
                  AV9DynamicFiltersEnabled2 = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV9DynamicFiltersEnabled2", AV9DynamicFiltersEnabled2);
                  AV43TFFuncaoAPFAtributos_AtributosNom = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV43TFFuncaoAPFAtributos_AtributosNom", AV43TFFuncaoAPFAtributos_AtributosNom);
                  AV44TFFuncaoAPFAtributos_AtributosNom_Sel = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV44TFFuncaoAPFAtributos_AtributosNom_Sel", AV44TFFuncaoAPFAtributos_AtributosNom_Sel);
                  AV47TFFuncaoAPFAtributos_AtrTabelaNom = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV47TFFuncaoAPFAtributos_AtrTabelaNom", AV47TFFuncaoAPFAtributos_AtrTabelaNom);
                  AV48TFFuncaoAPFAtributos_AtrTabelaNom_Sel = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV48TFFuncaoAPFAtributos_AtrTabelaNom_Sel", AV48TFFuncaoAPFAtributos_AtrTabelaNom_Sel);
                  AV51TFFuncaoAPFAtributos_FuncaoDadosNom = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV51TFFuncaoAPFAtributos_FuncaoDadosNom", AV51TFFuncaoAPFAtributos_FuncaoDadosNom);
                  AV52TFFuncaoAPFAtributos_FuncaoDadosNom_Sel = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV52TFFuncaoAPFAtributos_FuncaoDadosNom_Sel", AV52TFFuncaoAPFAtributos_FuncaoDadosNom_Sel);
                  AV19FuncaoAPF_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV19FuncaoAPF_Codigo), 6, 0)));
                  AV45ddo_FuncaoAPFAtributos_AtributosNomTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV45ddo_FuncaoAPFAtributos_AtributosNomTitleControlIdToReplace", AV45ddo_FuncaoAPFAtributos_AtributosNomTitleControlIdToReplace);
                  AV49ddo_FuncaoAPFAtributos_AtrTabelaNomTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV49ddo_FuncaoAPFAtributos_AtrTabelaNomTitleControlIdToReplace", AV49ddo_FuncaoAPFAtributos_AtrTabelaNomTitleControlIdToReplace);
                  AV53ddo_FuncaoAPFAtributos_FuncaoDadosNomTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV53ddo_FuncaoAPFAtributos_FuncaoDadosNomTitleControlIdToReplace", AV53ddo_FuncaoAPFAtributos_FuncaoDadosNomTitleControlIdToReplace);
                  AV62Pgmname = GetNextPar( );
                  AV20FuncaoAPF_SistemaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20FuncaoAPF_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20FuncaoAPF_SistemaCod), 6, 0)));
                  ajax_req_read_hidden_sdt(GetNextPar( ), AV25GridState);
                  AV11DynamicFiltersIgnoreFirst = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV11DynamicFiltersIgnoreFirst", AV11DynamicFiltersIgnoreFirst);
                  AV15DynamicFiltersRemoving = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15DynamicFiltersRemoving", AV15DynamicFiltersRemoving);
                  A393FuncaoAPFAtributos_SistemaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  n393FuncaoAPFAtributos_SistemaCod = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A393FuncaoAPFAtributos_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A393FuncaoAPFAtributos_SistemaCod), 6, 0)));
                  A415FuncaoAPFAtributos_FuncaoDadosTip = GetNextPar( );
                  n415FuncaoAPFAtributos_FuncaoDadosTip = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A415FuncaoAPFAtributos_FuncaoDadosTip", A415FuncaoAPFAtributos_FuncaoDadosTip);
                  A378FuncaoAPFAtributos_FuncaoDadosCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  n378FuncaoAPFAtributos_FuncaoDadosCod = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A378FuncaoAPFAtributos_FuncaoDadosCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A378FuncaoAPFAtributos_FuncaoDadosCod), 6, 0)));
                  A391FuncaoDados_FuncaoDadosCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  n391FuncaoDados_FuncaoDadosCod = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A391FuncaoDados_FuncaoDadosCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A391FuncaoDados_FuncaoDadosCod), 6, 0)));
                  AV38FuncaoAPFAtributos_FuncaoDadosCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV38FuncaoAPFAtributos_FuncaoDadosCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV38FuncaoAPFAtributos_FuncaoDadosCod), 6, 0)));
                  A373FuncaoDados_Tipo = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A373FuncaoDados_Tipo", A373FuncaoDados_Tipo);
                  sPrefix = GetNextPar( );
                  init_default_properties( ) ;
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxgrGrid_refresh( subGrid_Rows, AV30OrderedBy, AV31OrderedDsc, AV16DynamicFiltersSelector1, AV21FuncaoAPFAtributos_AtributosNom1, AV23FuncaoAPFAtributos_AtrTabelaNom1, AV17DynamicFiltersSelector2, AV22FuncaoAPFAtributos_AtributosNom2, AV24FuncaoAPFAtributos_AtrTabelaNom2, AV9DynamicFiltersEnabled2, AV43TFFuncaoAPFAtributos_AtributosNom, AV44TFFuncaoAPFAtributos_AtributosNom_Sel, AV47TFFuncaoAPFAtributos_AtrTabelaNom, AV48TFFuncaoAPFAtributos_AtrTabelaNom_Sel, AV51TFFuncaoAPFAtributos_FuncaoDadosNom, AV52TFFuncaoAPFAtributos_FuncaoDadosNom_Sel, AV19FuncaoAPF_Codigo, AV45ddo_FuncaoAPFAtributos_AtributosNomTitleControlIdToReplace, AV49ddo_FuncaoAPFAtributos_AtrTabelaNomTitleControlIdToReplace, AV53ddo_FuncaoAPFAtributos_FuncaoDadosNomTitleControlIdToReplace, AV62Pgmname, AV20FuncaoAPF_SistemaCod, AV25GridState, AV11DynamicFiltersIgnoreFirst, AV15DynamicFiltersRemoving, A393FuncaoAPFAtributos_SistemaCod, A415FuncaoAPFAtributos_FuncaoDadosTip, A378FuncaoAPFAtributos_FuncaoDadosCod, A391FuncaoDados_FuncaoDadosCod, AV38FuncaoAPFAtributos_FuncaoDadosCod, A373FuncaoDados_Tipo, sPrefix) ;
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
                  return  ;
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PA982( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV62Pgmname = "FuncaoAPFFuncoesAPFAtributosWC";
               context.Gx_err = 0;
               cmbavFuncaoapfatributos_funcaodadostip.Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavFuncaoapfatributos_funcaodadostip_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavFuncaoapfatributos_funcaodadostip.Enabled), 5, 0)));
               WS982( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "Funcao APFFuncoes APFAtributos WC") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?202042822503621");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
            context.WriteHtmlText( "<body") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("funcaoapffuncoesapfatributoswc.aspx") + "?" + UrlEncode("" +AV19FuncaoAPF_Codigo) + "," + UrlEncode("" +AV20FuncaoAPF_SistemaCod)+"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV30OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vORDEREDDSC", StringUtil.BoolToStr( AV31OrderedDsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vDYNAMICFILTERSSELECTOR1", AV16DynamicFiltersSelector1);
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM1", StringUtil.RTrim( AV21FuncaoAPFAtributos_AtributosNom1));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vFUNCAOAPFATRIBUTOS_ATRTABELANOM1", StringUtil.RTrim( AV23FuncaoAPFAtributos_AtrTabelaNom1));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vDYNAMICFILTERSSELECTOR2", AV17DynamicFiltersSelector2);
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM2", StringUtil.RTrim( AV22FuncaoAPFAtributos_AtributosNom2));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vFUNCAOAPFATRIBUTOS_ATRTABELANOM2", StringUtil.RTrim( AV24FuncaoAPFAtributos_AtrTabelaNom2));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vDYNAMICFILTERSENABLED2", StringUtil.BoolToStr( AV9DynamicFiltersEnabled2));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM", StringUtil.RTrim( AV43TFFuncaoAPFAtributos_AtributosNom));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM_SEL", StringUtil.RTrim( AV44TFFuncaoAPFAtributos_AtributosNom_Sel));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFFUNCAOAPFATRIBUTOS_ATRTABELANOM", StringUtil.RTrim( AV47TFFuncaoAPFAtributos_AtrTabelaNom));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFFUNCAOAPFATRIBUTOS_ATRTABELANOM_SEL", StringUtil.RTrim( AV48TFFuncaoAPFAtributos_AtrTabelaNom_Sel));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFFUNCAOAPFATRIBUTOS_FUNCAODADOSNOM", AV51TFFuncaoAPFAtributos_FuncaoDadosNom);
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFFUNCAOAPFATRIBUTOS_FUNCAODADOSNOM_SEL", AV52TFFuncaoAPFAtributos_FuncaoDadosNom_Sel);
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, sPrefix+"nRC_GXsfl_57", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_57), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV56GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV57GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vDDO_TITLESETTINGSICONS", AV54DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vDDO_TITLESETTINGSICONS", AV54DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vFUNCAOAPFATRIBUTOS_ATRIBUTOSNOMTITLEFILTERDATA", AV42FuncaoAPFAtributos_AtributosNomTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vFUNCAOAPFATRIBUTOS_ATRIBUTOSNOMTITLEFILTERDATA", AV42FuncaoAPFAtributos_AtributosNomTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vFUNCAOAPFATRIBUTOS_ATRTABELANOMTITLEFILTERDATA", AV46FuncaoAPFAtributos_AtrTabelaNomTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vFUNCAOAPFATRIBUTOS_ATRTABELANOMTITLEFILTERDATA", AV46FuncaoAPFAtributos_AtrTabelaNomTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vFUNCAOAPFATRIBUTOS_FUNCAODADOSNOMTITLEFILTERDATA", AV50FuncaoAPFAtributos_FuncaoDadosNomTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vFUNCAOAPFATRIBUTOS_FUNCAODADOSNOMTITLEFILTERDATA", AV50FuncaoAPFAtributos_FuncaoDadosNomTitleFilterData);
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOAV19FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOAV19FuncaoAPF_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOAV20FuncaoAPF_SistemaCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOAV20FuncaoAPF_SistemaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vFUNCAOAPF_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV19FuncaoAPF_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vPGMNAME", StringUtil.RTrim( AV62Pgmname));
         GxWebStd.gx_hidden_field( context, sPrefix+"vFUNCAOAPF_SISTEMACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV20FuncaoAPF_SistemaCod), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vGRIDSTATE", AV25GridState);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vGRIDSTATE", AV25GridState);
         }
         GxWebStd.gx_boolean_hidden_field( context, sPrefix+"vDYNAMICFILTERSIGNOREFIRST", AV11DynamicFiltersIgnoreFirst);
         GxWebStd.gx_boolean_hidden_field( context, sPrefix+"vDYNAMICFILTERSREMOVING", AV15DynamicFiltersRemoving);
         GxWebStd.gx_hidden_field( context, sPrefix+"FUNCAOAPFATRIBUTOS_SISTEMACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A393FuncaoAPFAtributos_SistemaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"FUNCAOAPFATRIBUTOS_FUNCAODADOSTIP", StringUtil.RTrim( A415FuncaoAPFAtributos_FuncaoDadosTip));
         GxWebStd.gx_hidden_field( context, sPrefix+"FUNCAOAPFATRIBUTOS_FUNCAODADOSCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A378FuncaoAPFAtributos_FuncaoDadosCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"FUNCAODADOS_FUNCAODADOSCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A391FuncaoDados_FuncaoDadosCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vFUNCAOAPFATRIBUTOS_FUNCAODADOSCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV38FuncaoAPFAtributos_FuncaoDadosCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"FUNCAODADOS_TIPO", StringUtil.RTrim( A373FuncaoDados_Tipo));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM_Caption", StringUtil.RTrim( Ddo_funcaoapfatributos_atributosnom_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM_Tooltip", StringUtil.RTrim( Ddo_funcaoapfatributos_atributosnom_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM_Cls", StringUtil.RTrim( Ddo_funcaoapfatributos_atributosnom_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM_Filteredtext_set", StringUtil.RTrim( Ddo_funcaoapfatributos_atributosnom_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM_Selectedvalue_set", StringUtil.RTrim( Ddo_funcaoapfatributos_atributosnom_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM_Dropdownoptionstype", StringUtil.RTrim( Ddo_funcaoapfatributos_atributosnom_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_funcaoapfatributos_atributosnom_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM_Includesortasc", StringUtil.BoolToStr( Ddo_funcaoapfatributos_atributosnom_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM_Includesortdsc", StringUtil.BoolToStr( Ddo_funcaoapfatributos_atributosnom_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM_Sortedstatus", StringUtil.RTrim( Ddo_funcaoapfatributos_atributosnom_Sortedstatus));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM_Includefilter", StringUtil.BoolToStr( Ddo_funcaoapfatributos_atributosnom_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM_Filtertype", StringUtil.RTrim( Ddo_funcaoapfatributos_atributosnom_Filtertype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM_Filterisrange", StringUtil.BoolToStr( Ddo_funcaoapfatributos_atributosnom_Filterisrange));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM_Includedatalist", StringUtil.BoolToStr( Ddo_funcaoapfatributos_atributosnom_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM_Datalisttype", StringUtil.RTrim( Ddo_funcaoapfatributos_atributosnom_Datalisttype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM_Datalistfixedvalues", StringUtil.RTrim( Ddo_funcaoapfatributos_atributosnom_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM_Datalistproc", StringUtil.RTrim( Ddo_funcaoapfatributos_atributosnom_Datalistproc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_funcaoapfatributos_atributosnom_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM_Sortasc", StringUtil.RTrim( Ddo_funcaoapfatributos_atributosnom_Sortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM_Sortdsc", StringUtil.RTrim( Ddo_funcaoapfatributos_atributosnom_Sortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM_Loadingdata", StringUtil.RTrim( Ddo_funcaoapfatributos_atributosnom_Loadingdata));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM_Cleanfilter", StringUtil.RTrim( Ddo_funcaoapfatributos_atributosnom_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM_Rangefilterfrom", StringUtil.RTrim( Ddo_funcaoapfatributos_atributosnom_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM_Rangefilterto", StringUtil.RTrim( Ddo_funcaoapfatributos_atributosnom_Rangefilterto));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM_Noresultsfound", StringUtil.RTrim( Ddo_funcaoapfatributos_atributosnom_Noresultsfound));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM_Searchbuttontext", StringUtil.RTrim( Ddo_funcaoapfatributos_atributosnom_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFATRIBUTOS_ATRTABELANOM_Caption", StringUtil.RTrim( Ddo_funcaoapfatributos_atrtabelanom_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFATRIBUTOS_ATRTABELANOM_Tooltip", StringUtil.RTrim( Ddo_funcaoapfatributos_atrtabelanom_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFATRIBUTOS_ATRTABELANOM_Cls", StringUtil.RTrim( Ddo_funcaoapfatributos_atrtabelanom_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFATRIBUTOS_ATRTABELANOM_Filteredtext_set", StringUtil.RTrim( Ddo_funcaoapfatributos_atrtabelanom_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFATRIBUTOS_ATRTABELANOM_Selectedvalue_set", StringUtil.RTrim( Ddo_funcaoapfatributos_atrtabelanom_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFATRIBUTOS_ATRTABELANOM_Dropdownoptionstype", StringUtil.RTrim( Ddo_funcaoapfatributos_atrtabelanom_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFATRIBUTOS_ATRTABELANOM_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_funcaoapfatributos_atrtabelanom_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFATRIBUTOS_ATRTABELANOM_Includesortasc", StringUtil.BoolToStr( Ddo_funcaoapfatributos_atrtabelanom_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFATRIBUTOS_ATRTABELANOM_Includesortdsc", StringUtil.BoolToStr( Ddo_funcaoapfatributos_atrtabelanom_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFATRIBUTOS_ATRTABELANOM_Sortedstatus", StringUtil.RTrim( Ddo_funcaoapfatributos_atrtabelanom_Sortedstatus));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFATRIBUTOS_ATRTABELANOM_Includefilter", StringUtil.BoolToStr( Ddo_funcaoapfatributos_atrtabelanom_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFATRIBUTOS_ATRTABELANOM_Filtertype", StringUtil.RTrim( Ddo_funcaoapfatributos_atrtabelanom_Filtertype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFATRIBUTOS_ATRTABELANOM_Filterisrange", StringUtil.BoolToStr( Ddo_funcaoapfatributos_atrtabelanom_Filterisrange));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFATRIBUTOS_ATRTABELANOM_Includedatalist", StringUtil.BoolToStr( Ddo_funcaoapfatributos_atrtabelanom_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFATRIBUTOS_ATRTABELANOM_Datalisttype", StringUtil.RTrim( Ddo_funcaoapfatributos_atrtabelanom_Datalisttype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFATRIBUTOS_ATRTABELANOM_Datalistfixedvalues", StringUtil.RTrim( Ddo_funcaoapfatributos_atrtabelanom_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFATRIBUTOS_ATRTABELANOM_Datalistproc", StringUtil.RTrim( Ddo_funcaoapfatributos_atrtabelanom_Datalistproc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFATRIBUTOS_ATRTABELANOM_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_funcaoapfatributos_atrtabelanom_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFATRIBUTOS_ATRTABELANOM_Sortasc", StringUtil.RTrim( Ddo_funcaoapfatributos_atrtabelanom_Sortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFATRIBUTOS_ATRTABELANOM_Sortdsc", StringUtil.RTrim( Ddo_funcaoapfatributos_atrtabelanom_Sortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFATRIBUTOS_ATRTABELANOM_Loadingdata", StringUtil.RTrim( Ddo_funcaoapfatributos_atrtabelanom_Loadingdata));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFATRIBUTOS_ATRTABELANOM_Cleanfilter", StringUtil.RTrim( Ddo_funcaoapfatributos_atrtabelanom_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFATRIBUTOS_ATRTABELANOM_Rangefilterfrom", StringUtil.RTrim( Ddo_funcaoapfatributos_atrtabelanom_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFATRIBUTOS_ATRTABELANOM_Rangefilterto", StringUtil.RTrim( Ddo_funcaoapfatributos_atrtabelanom_Rangefilterto));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFATRIBUTOS_ATRTABELANOM_Noresultsfound", StringUtil.RTrim( Ddo_funcaoapfatributos_atrtabelanom_Noresultsfound));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFATRIBUTOS_ATRTABELANOM_Searchbuttontext", StringUtil.RTrim( Ddo_funcaoapfatributos_atrtabelanom_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSNOM_Caption", StringUtil.RTrim( Ddo_funcaoapfatributos_funcaodadosnom_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSNOM_Tooltip", StringUtil.RTrim( Ddo_funcaoapfatributos_funcaodadosnom_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSNOM_Cls", StringUtil.RTrim( Ddo_funcaoapfatributos_funcaodadosnom_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSNOM_Filteredtext_set", StringUtil.RTrim( Ddo_funcaoapfatributos_funcaodadosnom_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSNOM_Selectedvalue_set", StringUtil.RTrim( Ddo_funcaoapfatributos_funcaodadosnom_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSNOM_Dropdownoptionstype", StringUtil.RTrim( Ddo_funcaoapfatributos_funcaodadosnom_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSNOM_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_funcaoapfatributos_funcaodadosnom_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSNOM_Includesortasc", StringUtil.BoolToStr( Ddo_funcaoapfatributos_funcaodadosnom_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSNOM_Includesortdsc", StringUtil.BoolToStr( Ddo_funcaoapfatributos_funcaodadosnom_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSNOM_Sortedstatus", StringUtil.RTrim( Ddo_funcaoapfatributos_funcaodadosnom_Sortedstatus));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSNOM_Includefilter", StringUtil.BoolToStr( Ddo_funcaoapfatributos_funcaodadosnom_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSNOM_Filtertype", StringUtil.RTrim( Ddo_funcaoapfatributos_funcaodadosnom_Filtertype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSNOM_Filterisrange", StringUtil.BoolToStr( Ddo_funcaoapfatributos_funcaodadosnom_Filterisrange));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSNOM_Includedatalist", StringUtil.BoolToStr( Ddo_funcaoapfatributos_funcaodadosnom_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSNOM_Datalisttype", StringUtil.RTrim( Ddo_funcaoapfatributos_funcaodadosnom_Datalisttype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSNOM_Datalistfixedvalues", StringUtil.RTrim( Ddo_funcaoapfatributos_funcaodadosnom_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSNOM_Datalistproc", StringUtil.RTrim( Ddo_funcaoapfatributos_funcaodadosnom_Datalistproc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSNOM_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_funcaoapfatributos_funcaodadosnom_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSNOM_Sortasc", StringUtil.RTrim( Ddo_funcaoapfatributos_funcaodadosnom_Sortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSNOM_Sortdsc", StringUtil.RTrim( Ddo_funcaoapfatributos_funcaodadosnom_Sortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSNOM_Loadingdata", StringUtil.RTrim( Ddo_funcaoapfatributos_funcaodadosnom_Loadingdata));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSNOM_Cleanfilter", StringUtil.RTrim( Ddo_funcaoapfatributos_funcaodadosnom_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSNOM_Rangefilterfrom", StringUtil.RTrim( Ddo_funcaoapfatributos_funcaodadosnom_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSNOM_Rangefilterto", StringUtil.RTrim( Ddo_funcaoapfatributos_funcaodadosnom_Rangefilterto));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSNOM_Noresultsfound", StringUtil.RTrim( Ddo_funcaoapfatributos_funcaodadosnom_Noresultsfound));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSNOM_Searchbuttontext", StringUtil.RTrim( Ddo_funcaoapfatributos_funcaodadosnom_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM_Activeeventkey", StringUtil.RTrim( Ddo_funcaoapfatributos_atributosnom_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM_Filteredtext_get", StringUtil.RTrim( Ddo_funcaoapfatributos_atributosnom_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM_Selectedvalue_get", StringUtil.RTrim( Ddo_funcaoapfatributos_atributosnom_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFATRIBUTOS_ATRTABELANOM_Activeeventkey", StringUtil.RTrim( Ddo_funcaoapfatributos_atrtabelanom_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFATRIBUTOS_ATRTABELANOM_Filteredtext_get", StringUtil.RTrim( Ddo_funcaoapfatributos_atrtabelanom_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFATRIBUTOS_ATRTABELANOM_Selectedvalue_get", StringUtil.RTrim( Ddo_funcaoapfatributos_atrtabelanom_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSNOM_Activeeventkey", StringUtil.RTrim( Ddo_funcaoapfatributos_funcaodadosnom_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSNOM_Filteredtext_get", StringUtil.RTrim( Ddo_funcaoapfatributos_funcaodadosnom_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSNOM_Selectedvalue_get", StringUtil.RTrim( Ddo_funcaoapfatributos_funcaodadosnom_Selectedvalue_get));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseForm982( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("funcaoapffuncoesapfatributoswc.js", "?202042822503754");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            context.WriteHtmlTextNl( "</form>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "FuncaoAPFFuncoesAPFAtributosWC" ;
      }

      public override String GetPgmdesc( )
      {
         return "Funcao APFFuncoes APFAtributos WC" ;
      }

      protected void WB980( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "funcaoapffuncoesapfatributoswc.aspx");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
               context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
            }
            wb_table1_2_982( true) ;
         }
         else
         {
            wb_table1_2_982( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_982e( bool wbgen )
      {
         if ( wbgen )
         {
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 69,'" + sPrefix + "',false,'" + sGXsfl_57_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled2_Internalname, StringUtil.BoolToStr( AV9DynamicFiltersEnabled2), "", "", chkavDynamicfiltersenabled2.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(69, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,69);\"");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 70,'" + sPrefix + "',false,'" + sGXsfl_57_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTffuncaoapfatributos_atributosnom_Internalname, StringUtil.RTrim( AV43TFFuncaoAPFAtributos_AtributosNom), StringUtil.RTrim( context.localUtil.Format( AV43TFFuncaoAPFAtributos_AtributosNom, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,70);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTffuncaoapfatributos_atributosnom_Jsonclick, 0, "Attribute", "", "", "", edtavTffuncaoapfatributos_atributosnom_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_FuncaoAPFFuncoesAPFAtributosWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 71,'" + sPrefix + "',false,'" + sGXsfl_57_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTffuncaoapfatributos_atributosnom_sel_Internalname, StringUtil.RTrim( AV44TFFuncaoAPFAtributos_AtributosNom_Sel), StringUtil.RTrim( context.localUtil.Format( AV44TFFuncaoAPFAtributos_AtributosNom_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,71);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTffuncaoapfatributos_atributosnom_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTffuncaoapfatributos_atributosnom_sel_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_FuncaoAPFFuncoesAPFAtributosWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 72,'" + sPrefix + "',false,'" + sGXsfl_57_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTffuncaoapfatributos_atrtabelanom_Internalname, StringUtil.RTrim( AV47TFFuncaoAPFAtributos_AtrTabelaNom), StringUtil.RTrim( context.localUtil.Format( AV47TFFuncaoAPFAtributos_AtrTabelaNom, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,72);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTffuncaoapfatributos_atrtabelanom_Jsonclick, 0, "Attribute", "", "", "", edtavTffuncaoapfatributos_atrtabelanom_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_FuncaoAPFFuncoesAPFAtributosWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 73,'" + sPrefix + "',false,'" + sGXsfl_57_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTffuncaoapfatributos_atrtabelanom_sel_Internalname, StringUtil.RTrim( AV48TFFuncaoAPFAtributos_AtrTabelaNom_Sel), StringUtil.RTrim( context.localUtil.Format( AV48TFFuncaoAPFAtributos_AtrTabelaNom_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,73);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTffuncaoapfatributos_atrtabelanom_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTffuncaoapfatributos_atrtabelanom_sel_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_FuncaoAPFFuncoesAPFAtributosWC.htm");
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 74,'" + sPrefix + "',false,'" + sGXsfl_57_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavTffuncaoapfatributos_funcaodadosnom_Internalname, AV51TFFuncaoAPFAtributos_FuncaoDadosNom, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,74);\"", 0, edtavTffuncaoapfatributos_funcaodadosnom_Visible, 1, 0, 80, "chr", 3, "row", StyleString, ClassString, "", "200", -1, "", "", -1, true, "", "HLP_FuncaoAPFFuncoesAPFAtributosWC.htm");
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 75,'" + sPrefix + "',false,'" + sGXsfl_57_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavTffuncaoapfatributos_funcaodadosnom_sel_Internalname, AV52TFFuncaoAPFAtributos_FuncaoDadosNom_Sel, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,75);\"", 0, edtavTffuncaoapfatributos_funcaodadosnom_sel_Visible, 1, 0, 80, "chr", 3, "row", StyleString, ClassString, "", "200", -1, "", "", -1, true, "", "HLP_FuncaoAPFFuncoesAPFAtributosWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOMContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 77,'" + sPrefix + "',false,'" + sGXsfl_57_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_funcaoapfatributos_atributosnomtitlecontrolidtoreplace_Internalname, AV45ddo_FuncaoAPFAtributos_AtributosNomTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,77);\"", 0, edtavDdo_funcaoapfatributos_atributosnomtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_FuncaoAPFFuncoesAPFAtributosWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_FUNCAOAPFATRIBUTOS_ATRTABELANOMContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 79,'" + sPrefix + "',false,'" + sGXsfl_57_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_funcaoapfatributos_atrtabelanomtitlecontrolidtoreplace_Internalname, AV49ddo_FuncaoAPFAtributos_AtrTabelaNomTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,79);\"", 0, edtavDdo_funcaoapfatributos_atrtabelanomtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_FuncaoAPFFuncoesAPFAtributosWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSNOMContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 81,'" + sPrefix + "',false,'" + sGXsfl_57_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_funcaoapfatributos_funcaodadosnomtitlecontrolidtoreplace_Internalname, AV53ddo_FuncaoAPFAtributos_FuncaoDadosNomTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,81);\"", 0, edtavDdo_funcaoapfatributos_funcaodadosnomtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_FuncaoAPFFuncoesAPFAtributosWC.htm");
         }
         wbLoad = true;
      }

      protected void START982( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "Funcao APFFuncoes APFAtributos WC", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUP980( ) ;
            }
         }
      }

      protected void WS982( )
      {
         START982( ) ;
         EVT982( ) ;
      }

      protected void EVT982( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP980( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP980( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E11982 */
                                    E11982 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP980( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E12982 */
                                    E12982 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_FUNCAOAPFATRIBUTOS_ATRTABELANOM.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP980( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E13982 */
                                    E13982 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSNOM.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP980( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E14982 */
                                    E14982 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VORDEREDBY.CLICK") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP980( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E15982 */
                                    E15982 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS1'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP980( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E16982 */
                                    E16982 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS2'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP980( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E17982 */
                                    E17982 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOCLEANFILTERS'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP980( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E18982 */
                                    E18982 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOINSERTATRIBUTO'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP980( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E19982 */
                                    E19982 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS1'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP980( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E20982 */
                                    E20982 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR1.CLICK") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP980( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E21982 */
                                    E21982 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR2.CLICK") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP980( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E22982 */
                                    E22982 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP980( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    GX_FocusControl = cmbavFuncaoapfatributos_funcaodadostip_Internalname;
                                    context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                 }
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 21), "VDELETEATRIBUTO.CLICK") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 21), "VDELETEATRIBUTO.CLICK") == 0 ) )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP980( ) ;
                              }
                              nGXsfl_57_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_57_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_57_idx), 4, 0)), 4, "0");
                              SubsflControlProps_572( ) ;
                              AV39DeleteAtributo = cgiGet( edtavDeleteatributo_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDeleteatributo_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV39DeleteAtributo)) ? AV61Deleteatributo_GXI : context.convertURL( context.PathToRelativeUrl( AV39DeleteAtributo))));
                              A165FuncaoAPF_Codigo = (int)(context.localUtil.CToN( cgiGet( edtFuncaoAPF_Codigo_Internalname), ",", "."));
                              A364FuncaoAPFAtributos_AtributosCod = (int)(context.localUtil.CToN( cgiGet( edtFuncaoAPFAtributos_AtributosCod_Internalname), ",", "."));
                              A365FuncaoAPFAtributos_AtributosNom = StringUtil.Upper( cgiGet( edtFuncaoAPFAtributos_AtributosNom_Internalname));
                              n365FuncaoAPFAtributos_AtributosNom = false;
                              A367FuncaoAPFAtributos_AtrTabelaNom = StringUtil.Upper( cgiGet( edtFuncaoAPFAtributos_AtrTabelaNom_Internalname));
                              n367FuncaoAPFAtributos_AtrTabelaNom = false;
                              A412FuncaoAPFAtributos_FuncaoDadosNom = cgiGet( edtFuncaoAPFAtributos_FuncaoDadosNom_Internalname);
                              n412FuncaoAPFAtributos_FuncaoDadosNom = false;
                              cmbavFuncaoapfatributos_funcaodadostip.Name = cmbavFuncaoapfatributos_funcaodadostip_Internalname;
                              cmbavFuncaoapfatributos_funcaodadostip.CurrentValue = cgiGet( cmbavFuncaoapfatributos_funcaodadostip_Internalname);
                              AV37FuncaoAPFAtributos_FuncaoDadosTip = cgiGet( cmbavFuncaoapfatributos_funcaodadostip_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, cmbavFuncaoapfatributos_funcaodadostip_Internalname, AV37FuncaoAPFAtributos_FuncaoDadosTip);
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = cmbavFuncaoapfatributos_funcaodadostip_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E23982 */
                                          E23982 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = cmbavFuncaoapfatributos_funcaodadostip_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E24982 */
                                          E24982 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = cmbavFuncaoapfatributos_funcaodadostip_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E25982 */
                                          E25982 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "VDELETEATRIBUTO.CLICK") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = cmbavFuncaoapfatributos_funcaodadostip_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E26982 */
                                          E26982 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          if ( ! wbErr )
                                          {
                                             Rfr0gs = false;
                                             /* Set Refresh If Orderedby Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV30OrderedBy )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Ordereddsc Changed */
                                             if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vORDEREDDSC")) != AV31OrderedDsc )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Dynamicfiltersselector1 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vDYNAMICFILTERSSELECTOR1"), AV16DynamicFiltersSelector1) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Funcaoapfatributos_atributosnom1 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM1"), AV21FuncaoAPFAtributos_AtributosNom1) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Funcaoapfatributos_atrtabelanom1 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vFUNCAOAPFATRIBUTOS_ATRTABELANOM1"), AV23FuncaoAPFAtributos_AtrTabelaNom1) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Dynamicfiltersselector2 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vDYNAMICFILTERSSELECTOR2"), AV17DynamicFiltersSelector2) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Funcaoapfatributos_atributosnom2 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM2"), AV22FuncaoAPFAtributos_AtributosNom2) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Funcaoapfatributos_atrtabelanom2 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vFUNCAOAPFATRIBUTOS_ATRTABELANOM2"), AV24FuncaoAPFAtributos_AtrTabelaNom2) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Dynamicfiltersenabled2 Changed */
                                             if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vDYNAMICFILTERSENABLED2")) != AV9DynamicFiltersEnabled2 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tffuncaoapfatributos_atributosnom Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM"), AV43TFFuncaoAPFAtributos_AtributosNom) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tffuncaoapfatributos_atributosnom_sel Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM_SEL"), AV44TFFuncaoAPFAtributos_AtributosNom_Sel) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tffuncaoapfatributos_atrtabelanom Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFFUNCAOAPFATRIBUTOS_ATRTABELANOM"), AV47TFFuncaoAPFAtributos_AtrTabelaNom) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tffuncaoapfatributos_atrtabelanom_sel Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFFUNCAOAPFATRIBUTOS_ATRTABELANOM_SEL"), AV48TFFuncaoAPFAtributos_AtrTabelaNom_Sel) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tffuncaoapfatributos_funcaodadosnom Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFFUNCAOAPFATRIBUTOS_FUNCAODADOSNOM"), AV51TFFuncaoAPFAtributos_FuncaoDadosNom) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tffuncaoapfatributos_funcaodadosnom_sel Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFFUNCAOAPFATRIBUTOS_FUNCAODADOSNOM_SEL"), AV52TFFuncaoAPFAtributos_FuncaoDadosNom_Sel) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             if ( ! Rfr0gs )
                                             {
                                             }
                                             dynload_actions( ) ;
                                          }
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = cmbavFuncaoapfatributos_funcaodadostip_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                                    {
                                       STRUP980( ) ;
                                    }
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = cmbavFuncaoapfatributos_funcaodadostip_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WE982( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseForm982( ) ;
            }
         }
      }

      protected void PA982( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            cmbavOrderedby.Name = "vORDEREDBY";
            cmbavOrderedby.WebTags = "";
            if ( cmbavOrderedby.ItemCount > 0 )
            {
               AV30OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV30OrderedBy), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV30OrderedBy), 4, 0)));
            }
            cmbavDynamicfiltersselector1.Name = "vDYNAMICFILTERSSELECTOR1";
            cmbavDynamicfiltersselector1.WebTags = "";
            cmbavDynamicfiltersselector1.addItem("FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM", "Nome", 0);
            cmbavDynamicfiltersselector1.addItem("FUNCAOAPFATRIBUTOS_ATRTABELANOM", "Tabela", 0);
            if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
            {
               AV16DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV16DynamicFiltersSelector1);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
            }
            cmbavDynamicfiltersselector2.Name = "vDYNAMICFILTERSSELECTOR2";
            cmbavDynamicfiltersselector2.WebTags = "";
            cmbavDynamicfiltersselector2.addItem("FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM", "Nome", 0);
            cmbavDynamicfiltersselector2.addItem("FUNCAOAPFATRIBUTOS_ATRTABELANOM", "Tabela", 0);
            if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
            {
               AV17DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV17DynamicFiltersSelector2);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17DynamicFiltersSelector2", AV17DynamicFiltersSelector2);
            }
            GXCCtl = "vFUNCAOAPFATRIBUTOS_FUNCAODADOSTIP_" + sGXsfl_57_idx;
            cmbavFuncaoapfatributos_funcaodadostip.Name = GXCCtl;
            cmbavFuncaoapfatributos_funcaodadostip.WebTags = "";
            cmbavFuncaoapfatributos_funcaodadostip.addItem("", "(Nenhum)", 0);
            cmbavFuncaoapfatributos_funcaodadostip.addItem("ALI", "ALI", 0);
            cmbavFuncaoapfatributos_funcaodadostip.addItem("AIE", "AIE", 0);
            cmbavFuncaoapfatributos_funcaodadostip.addItem("DC", "DC", 0);
            if ( cmbavFuncaoapfatributos_funcaodadostip.ItemCount > 0 )
            {
               AV37FuncaoAPFAtributos_FuncaoDadosTip = cmbavFuncaoapfatributos_funcaodadostip.getValidValue(AV37FuncaoAPFAtributos_FuncaoDadosTip);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, cmbavFuncaoapfatributos_funcaodadostip_Internalname, AV37FuncaoAPFAtributos_FuncaoDadosTip);
            }
            chkavDynamicfiltersenabled2.Name = "vDYNAMICFILTERSENABLED2";
            chkavDynamicfiltersenabled2.WebTags = "";
            chkavDynamicfiltersenabled2.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkavDynamicfiltersenabled2_Internalname, "TitleCaption", chkavDynamicfiltersenabled2.Caption);
            chkavDynamicfiltersenabled2.CheckedValue = "false";
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_572( ) ;
         while ( nGXsfl_57_idx <= nRC_GXsfl_57 )
         {
            sendrow_572( ) ;
            nGXsfl_57_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_57_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_57_idx+1));
            sGXsfl_57_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_57_idx), 4, 0)), 4, "0");
            SubsflControlProps_572( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV30OrderedBy ,
                                       bool AV31OrderedDsc ,
                                       String AV16DynamicFiltersSelector1 ,
                                       String AV21FuncaoAPFAtributos_AtributosNom1 ,
                                       String AV23FuncaoAPFAtributos_AtrTabelaNom1 ,
                                       String AV17DynamicFiltersSelector2 ,
                                       String AV22FuncaoAPFAtributos_AtributosNom2 ,
                                       String AV24FuncaoAPFAtributos_AtrTabelaNom2 ,
                                       bool AV9DynamicFiltersEnabled2 ,
                                       String AV43TFFuncaoAPFAtributos_AtributosNom ,
                                       String AV44TFFuncaoAPFAtributos_AtributosNom_Sel ,
                                       String AV47TFFuncaoAPFAtributos_AtrTabelaNom ,
                                       String AV48TFFuncaoAPFAtributos_AtrTabelaNom_Sel ,
                                       String AV51TFFuncaoAPFAtributos_FuncaoDadosNom ,
                                       String AV52TFFuncaoAPFAtributos_FuncaoDadosNom_Sel ,
                                       int AV19FuncaoAPF_Codigo ,
                                       String AV45ddo_FuncaoAPFAtributos_AtributosNomTitleControlIdToReplace ,
                                       String AV49ddo_FuncaoAPFAtributos_AtrTabelaNomTitleControlIdToReplace ,
                                       String AV53ddo_FuncaoAPFAtributos_FuncaoDadosNomTitleControlIdToReplace ,
                                       String AV62Pgmname ,
                                       int AV20FuncaoAPF_SistemaCod ,
                                       wwpbaseobjects.SdtWWPGridState AV25GridState ,
                                       bool AV11DynamicFiltersIgnoreFirst ,
                                       bool AV15DynamicFiltersRemoving ,
                                       int A393FuncaoAPFAtributos_SistemaCod ,
                                       String A415FuncaoAPFAtributos_FuncaoDadosTip ,
                                       int A378FuncaoAPFAtributos_FuncaoDadosCod ,
                                       int A391FuncaoDados_FuncaoDadosCod ,
                                       int AV38FuncaoAPFAtributos_FuncaoDadosCod ,
                                       String A373FuncaoDados_Tipo ,
                                       String sPrefix )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RF982( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavOrderedby.ItemCount > 0 )
         {
            AV30OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV30OrderedBy), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV30OrderedBy), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
         {
            AV16DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV16DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
         }
         if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
         {
            AV17DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV17DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17DynamicFiltersSelector2", AV17DynamicFiltersSelector2);
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RF982( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV62Pgmname = "FuncaoAPFFuncoesAPFAtributosWC";
         context.Gx_err = 0;
         cmbavFuncaoapfatributos_funcaodadostip.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavFuncaoapfatributos_funcaodadostip_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavFuncaoapfatributos_funcaodadostip.Enabled), 5, 0)));
      }

      protected void RF982( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 57;
         /* Execute user event: E24982 */
         E24982 ();
         nGXsfl_57_idx = 1;
         sGXsfl_57_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_57_idx), 4, 0)), 4, "0");
         SubsflControlProps_572( ) ;
         nGXsfl_57_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", sPrefix);
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_572( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 AV16DynamicFiltersSelector1 ,
                                                 AV21FuncaoAPFAtributos_AtributosNom1 ,
                                                 AV23FuncaoAPFAtributos_AtrTabelaNom1 ,
                                                 AV9DynamicFiltersEnabled2 ,
                                                 AV17DynamicFiltersSelector2 ,
                                                 AV22FuncaoAPFAtributos_AtributosNom2 ,
                                                 AV24FuncaoAPFAtributos_AtrTabelaNom2 ,
                                                 AV44TFFuncaoAPFAtributos_AtributosNom_Sel ,
                                                 AV43TFFuncaoAPFAtributos_AtributosNom ,
                                                 AV48TFFuncaoAPFAtributos_AtrTabelaNom_Sel ,
                                                 AV47TFFuncaoAPFAtributos_AtrTabelaNom ,
                                                 AV52TFFuncaoAPFAtributos_FuncaoDadosNom_Sel ,
                                                 AV51TFFuncaoAPFAtributos_FuncaoDadosNom ,
                                                 A365FuncaoAPFAtributos_AtributosNom ,
                                                 A367FuncaoAPFAtributos_AtrTabelaNom ,
                                                 A412FuncaoAPFAtributos_FuncaoDadosNom ,
                                                 AV30OrderedBy ,
                                                 AV31OrderedDsc ,
                                                 A165FuncaoAPF_Codigo ,
                                                 AV19FuncaoAPF_Codigo ,
                                                 A180Atributos_Ativo },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.SHORT,
                                                 TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN
                                                 }
            });
            lV21FuncaoAPFAtributos_AtributosNom1 = StringUtil.PadR( StringUtil.RTrim( AV21FuncaoAPFAtributos_AtributosNom1), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21FuncaoAPFAtributos_AtributosNom1", AV21FuncaoAPFAtributos_AtributosNom1);
            lV23FuncaoAPFAtributos_AtrTabelaNom1 = StringUtil.PadR( StringUtil.RTrim( AV23FuncaoAPFAtributos_AtrTabelaNom1), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23FuncaoAPFAtributos_AtrTabelaNom1", AV23FuncaoAPFAtributos_AtrTabelaNom1);
            lV22FuncaoAPFAtributos_AtributosNom2 = StringUtil.PadR( StringUtil.RTrim( AV22FuncaoAPFAtributos_AtributosNom2), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22FuncaoAPFAtributos_AtributosNom2", AV22FuncaoAPFAtributos_AtributosNom2);
            lV24FuncaoAPFAtributos_AtrTabelaNom2 = StringUtil.PadR( StringUtil.RTrim( AV24FuncaoAPFAtributos_AtrTabelaNom2), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24FuncaoAPFAtributos_AtrTabelaNom2", AV24FuncaoAPFAtributos_AtrTabelaNom2);
            lV43TFFuncaoAPFAtributos_AtributosNom = StringUtil.PadR( StringUtil.RTrim( AV43TFFuncaoAPFAtributos_AtributosNom), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV43TFFuncaoAPFAtributos_AtributosNom", AV43TFFuncaoAPFAtributos_AtributosNom);
            lV47TFFuncaoAPFAtributos_AtrTabelaNom = StringUtil.PadR( StringUtil.RTrim( AV47TFFuncaoAPFAtributos_AtrTabelaNom), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV47TFFuncaoAPFAtributos_AtrTabelaNom", AV47TFFuncaoAPFAtributos_AtrTabelaNom);
            lV51TFFuncaoAPFAtributos_FuncaoDadosNom = StringUtil.Concat( StringUtil.RTrim( AV51TFFuncaoAPFAtributos_FuncaoDadosNom), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV51TFFuncaoAPFAtributos_FuncaoDadosNom", AV51TFFuncaoAPFAtributos_FuncaoDadosNom);
            /* Using cursor H00982 */
            pr_default.execute(0, new Object[] {AV19FuncaoAPF_Codigo, lV21FuncaoAPFAtributos_AtributosNom1, lV23FuncaoAPFAtributos_AtrTabelaNom1, lV22FuncaoAPFAtributos_AtributosNom2, lV24FuncaoAPFAtributos_AtrTabelaNom2, lV43TFFuncaoAPFAtributos_AtributosNom, AV44TFFuncaoAPFAtributos_AtributosNom_Sel, lV47TFFuncaoAPFAtributos_AtrTabelaNom, AV48TFFuncaoAPFAtributos_AtrTabelaNom_Sel, lV51TFFuncaoAPFAtributos_FuncaoDadosNom, AV52TFFuncaoAPFAtributos_FuncaoDadosNom_Sel, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_57_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A366FuncaoAPFAtributos_AtrTabelaCod = H00982_A366FuncaoAPFAtributos_AtrTabelaCod[0];
               n366FuncaoAPFAtributos_AtrTabelaCod = H00982_n366FuncaoAPFAtributos_AtrTabelaCod[0];
               A180Atributos_Ativo = H00982_A180Atributos_Ativo[0];
               n180Atributos_Ativo = H00982_n180Atributos_Ativo[0];
               A393FuncaoAPFAtributos_SistemaCod = H00982_A393FuncaoAPFAtributos_SistemaCod[0];
               n393FuncaoAPFAtributos_SistemaCod = H00982_n393FuncaoAPFAtributos_SistemaCod[0];
               A415FuncaoAPFAtributos_FuncaoDadosTip = H00982_A415FuncaoAPFAtributos_FuncaoDadosTip[0];
               n415FuncaoAPFAtributos_FuncaoDadosTip = H00982_n415FuncaoAPFAtributos_FuncaoDadosTip[0];
               A378FuncaoAPFAtributos_FuncaoDadosCod = H00982_A378FuncaoAPFAtributos_FuncaoDadosCod[0];
               n378FuncaoAPFAtributos_FuncaoDadosCod = H00982_n378FuncaoAPFAtributos_FuncaoDadosCod[0];
               A412FuncaoAPFAtributos_FuncaoDadosNom = H00982_A412FuncaoAPFAtributos_FuncaoDadosNom[0];
               n412FuncaoAPFAtributos_FuncaoDadosNom = H00982_n412FuncaoAPFAtributos_FuncaoDadosNom[0];
               A367FuncaoAPFAtributos_AtrTabelaNom = H00982_A367FuncaoAPFAtributos_AtrTabelaNom[0];
               n367FuncaoAPFAtributos_AtrTabelaNom = H00982_n367FuncaoAPFAtributos_AtrTabelaNom[0];
               A365FuncaoAPFAtributos_AtributosNom = H00982_A365FuncaoAPFAtributos_AtributosNom[0];
               n365FuncaoAPFAtributos_AtributosNom = H00982_n365FuncaoAPFAtributos_AtributosNom[0];
               A364FuncaoAPFAtributos_AtributosCod = H00982_A364FuncaoAPFAtributos_AtributosCod[0];
               A165FuncaoAPF_Codigo = H00982_A165FuncaoAPF_Codigo[0];
               A415FuncaoAPFAtributos_FuncaoDadosTip = H00982_A415FuncaoAPFAtributos_FuncaoDadosTip[0];
               n415FuncaoAPFAtributos_FuncaoDadosTip = H00982_n415FuncaoAPFAtributos_FuncaoDadosTip[0];
               A412FuncaoAPFAtributos_FuncaoDadosNom = H00982_A412FuncaoAPFAtributos_FuncaoDadosNom[0];
               n412FuncaoAPFAtributos_FuncaoDadosNom = H00982_n412FuncaoAPFAtributos_FuncaoDadosNom[0];
               A366FuncaoAPFAtributos_AtrTabelaCod = H00982_A366FuncaoAPFAtributos_AtrTabelaCod[0];
               n366FuncaoAPFAtributos_AtrTabelaCod = H00982_n366FuncaoAPFAtributos_AtrTabelaCod[0];
               A180Atributos_Ativo = H00982_A180Atributos_Ativo[0];
               n180Atributos_Ativo = H00982_n180Atributos_Ativo[0];
               A365FuncaoAPFAtributos_AtributosNom = H00982_A365FuncaoAPFAtributos_AtributosNom[0];
               n365FuncaoAPFAtributos_AtributosNom = H00982_n365FuncaoAPFAtributos_AtributosNom[0];
               A393FuncaoAPFAtributos_SistemaCod = H00982_A393FuncaoAPFAtributos_SistemaCod[0];
               n393FuncaoAPFAtributos_SistemaCod = H00982_n393FuncaoAPFAtributos_SistemaCod[0];
               A367FuncaoAPFAtributos_AtrTabelaNom = H00982_A367FuncaoAPFAtributos_AtrTabelaNom[0];
               n367FuncaoAPFAtributos_AtrTabelaNom = H00982_n367FuncaoAPFAtributos_AtrTabelaNom[0];
               /* Execute user event: E25982 */
               E25982 ();
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 57;
            WB980( ) ;
         }
         nGXsfl_57_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV16DynamicFiltersSelector1 ,
                                              AV21FuncaoAPFAtributos_AtributosNom1 ,
                                              AV23FuncaoAPFAtributos_AtrTabelaNom1 ,
                                              AV9DynamicFiltersEnabled2 ,
                                              AV17DynamicFiltersSelector2 ,
                                              AV22FuncaoAPFAtributos_AtributosNom2 ,
                                              AV24FuncaoAPFAtributos_AtrTabelaNom2 ,
                                              AV44TFFuncaoAPFAtributos_AtributosNom_Sel ,
                                              AV43TFFuncaoAPFAtributos_AtributosNom ,
                                              AV48TFFuncaoAPFAtributos_AtrTabelaNom_Sel ,
                                              AV47TFFuncaoAPFAtributos_AtrTabelaNom ,
                                              AV52TFFuncaoAPFAtributos_FuncaoDadosNom_Sel ,
                                              AV51TFFuncaoAPFAtributos_FuncaoDadosNom ,
                                              A365FuncaoAPFAtributos_AtributosNom ,
                                              A367FuncaoAPFAtributos_AtrTabelaNom ,
                                              A412FuncaoAPFAtributos_FuncaoDadosNom ,
                                              AV30OrderedBy ,
                                              AV31OrderedDsc ,
                                              A165FuncaoAPF_Codigo ,
                                              AV19FuncaoAPF_Codigo ,
                                              A180Atributos_Ativo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.SHORT,
                                              TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN
                                              }
         });
         lV21FuncaoAPFAtributos_AtributosNom1 = StringUtil.PadR( StringUtil.RTrim( AV21FuncaoAPFAtributos_AtributosNom1), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21FuncaoAPFAtributos_AtributosNom1", AV21FuncaoAPFAtributos_AtributosNom1);
         lV23FuncaoAPFAtributos_AtrTabelaNom1 = StringUtil.PadR( StringUtil.RTrim( AV23FuncaoAPFAtributos_AtrTabelaNom1), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23FuncaoAPFAtributos_AtrTabelaNom1", AV23FuncaoAPFAtributos_AtrTabelaNom1);
         lV22FuncaoAPFAtributos_AtributosNom2 = StringUtil.PadR( StringUtil.RTrim( AV22FuncaoAPFAtributos_AtributosNom2), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22FuncaoAPFAtributos_AtributosNom2", AV22FuncaoAPFAtributos_AtributosNom2);
         lV24FuncaoAPFAtributos_AtrTabelaNom2 = StringUtil.PadR( StringUtil.RTrim( AV24FuncaoAPFAtributos_AtrTabelaNom2), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24FuncaoAPFAtributos_AtrTabelaNom2", AV24FuncaoAPFAtributos_AtrTabelaNom2);
         lV43TFFuncaoAPFAtributos_AtributosNom = StringUtil.PadR( StringUtil.RTrim( AV43TFFuncaoAPFAtributos_AtributosNom), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV43TFFuncaoAPFAtributos_AtributosNom", AV43TFFuncaoAPFAtributos_AtributosNom);
         lV47TFFuncaoAPFAtributos_AtrTabelaNom = StringUtil.PadR( StringUtil.RTrim( AV47TFFuncaoAPFAtributos_AtrTabelaNom), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV47TFFuncaoAPFAtributos_AtrTabelaNom", AV47TFFuncaoAPFAtributos_AtrTabelaNom);
         lV51TFFuncaoAPFAtributos_FuncaoDadosNom = StringUtil.Concat( StringUtil.RTrim( AV51TFFuncaoAPFAtributos_FuncaoDadosNom), "%", "");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV51TFFuncaoAPFAtributos_FuncaoDadosNom", AV51TFFuncaoAPFAtributos_FuncaoDadosNom);
         /* Using cursor H00983 */
         pr_default.execute(1, new Object[] {AV19FuncaoAPF_Codigo, lV21FuncaoAPFAtributos_AtributosNom1, lV23FuncaoAPFAtributos_AtrTabelaNom1, lV22FuncaoAPFAtributos_AtributosNom2, lV24FuncaoAPFAtributos_AtrTabelaNom2, lV43TFFuncaoAPFAtributos_AtributosNom, AV44TFFuncaoAPFAtributos_AtributosNom_Sel, lV47TFFuncaoAPFAtributos_AtrTabelaNom, AV48TFFuncaoAPFAtributos_AtrTabelaNom_Sel, lV51TFFuncaoAPFAtributos_FuncaoDadosNom, AV52TFFuncaoAPFAtributos_FuncaoDadosNom_Sel});
         GRID_nRecordCount = H00983_AGRID_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV30OrderedBy, AV31OrderedDsc, AV16DynamicFiltersSelector1, AV21FuncaoAPFAtributos_AtributosNom1, AV23FuncaoAPFAtributos_AtrTabelaNom1, AV17DynamicFiltersSelector2, AV22FuncaoAPFAtributos_AtributosNom2, AV24FuncaoAPFAtributos_AtrTabelaNom2, AV9DynamicFiltersEnabled2, AV43TFFuncaoAPFAtributos_AtributosNom, AV44TFFuncaoAPFAtributos_AtributosNom_Sel, AV47TFFuncaoAPFAtributos_AtrTabelaNom, AV48TFFuncaoAPFAtributos_AtrTabelaNom_Sel, AV51TFFuncaoAPFAtributos_FuncaoDadosNom, AV52TFFuncaoAPFAtributos_FuncaoDadosNom_Sel, AV19FuncaoAPF_Codigo, AV45ddo_FuncaoAPFAtributos_AtributosNomTitleControlIdToReplace, AV49ddo_FuncaoAPFAtributos_AtrTabelaNomTitleControlIdToReplace, AV53ddo_FuncaoAPFAtributos_FuncaoDadosNomTitleControlIdToReplace, AV62Pgmname, AV20FuncaoAPF_SistemaCod, AV25GridState, AV11DynamicFiltersIgnoreFirst, AV15DynamicFiltersRemoving, A393FuncaoAPFAtributos_SistemaCod, A415FuncaoAPFAtributos_FuncaoDadosTip, A378FuncaoAPFAtributos_FuncaoDadosCod, A391FuncaoDados_FuncaoDadosCod, AV38FuncaoAPFAtributos_FuncaoDadosCod, A373FuncaoDados_Tipo, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV30OrderedBy, AV31OrderedDsc, AV16DynamicFiltersSelector1, AV21FuncaoAPFAtributos_AtributosNom1, AV23FuncaoAPFAtributos_AtrTabelaNom1, AV17DynamicFiltersSelector2, AV22FuncaoAPFAtributos_AtributosNom2, AV24FuncaoAPFAtributos_AtrTabelaNom2, AV9DynamicFiltersEnabled2, AV43TFFuncaoAPFAtributos_AtributosNom, AV44TFFuncaoAPFAtributos_AtributosNom_Sel, AV47TFFuncaoAPFAtributos_AtrTabelaNom, AV48TFFuncaoAPFAtributos_AtrTabelaNom_Sel, AV51TFFuncaoAPFAtributos_FuncaoDadosNom, AV52TFFuncaoAPFAtributos_FuncaoDadosNom_Sel, AV19FuncaoAPF_Codigo, AV45ddo_FuncaoAPFAtributos_AtributosNomTitleControlIdToReplace, AV49ddo_FuncaoAPFAtributos_AtrTabelaNomTitleControlIdToReplace, AV53ddo_FuncaoAPFAtributos_FuncaoDadosNomTitleControlIdToReplace, AV62Pgmname, AV20FuncaoAPF_SistemaCod, AV25GridState, AV11DynamicFiltersIgnoreFirst, AV15DynamicFiltersRemoving, A393FuncaoAPFAtributos_SistemaCod, A415FuncaoAPFAtributos_FuncaoDadosTip, A378FuncaoAPFAtributos_FuncaoDadosCod, A391FuncaoDados_FuncaoDadosCod, AV38FuncaoAPFAtributos_FuncaoDadosCod, A373FuncaoDados_Tipo, sPrefix) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV30OrderedBy, AV31OrderedDsc, AV16DynamicFiltersSelector1, AV21FuncaoAPFAtributos_AtributosNom1, AV23FuncaoAPFAtributos_AtrTabelaNom1, AV17DynamicFiltersSelector2, AV22FuncaoAPFAtributos_AtributosNom2, AV24FuncaoAPFAtributos_AtrTabelaNom2, AV9DynamicFiltersEnabled2, AV43TFFuncaoAPFAtributos_AtributosNom, AV44TFFuncaoAPFAtributos_AtributosNom_Sel, AV47TFFuncaoAPFAtributos_AtrTabelaNom, AV48TFFuncaoAPFAtributos_AtrTabelaNom_Sel, AV51TFFuncaoAPFAtributos_FuncaoDadosNom, AV52TFFuncaoAPFAtributos_FuncaoDadosNom_Sel, AV19FuncaoAPF_Codigo, AV45ddo_FuncaoAPFAtributos_AtributosNomTitleControlIdToReplace, AV49ddo_FuncaoAPFAtributos_AtrTabelaNomTitleControlIdToReplace, AV53ddo_FuncaoAPFAtributos_FuncaoDadosNomTitleControlIdToReplace, AV62Pgmname, AV20FuncaoAPF_SistemaCod, AV25GridState, AV11DynamicFiltersIgnoreFirst, AV15DynamicFiltersRemoving, A393FuncaoAPFAtributos_SistemaCod, A415FuncaoAPFAtributos_FuncaoDadosTip, A378FuncaoAPFAtributos_FuncaoDadosCod, A391FuncaoDados_FuncaoDadosCod, AV38FuncaoAPFAtributos_FuncaoDadosCod, A373FuncaoDados_Tipo, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV30OrderedBy, AV31OrderedDsc, AV16DynamicFiltersSelector1, AV21FuncaoAPFAtributos_AtributosNom1, AV23FuncaoAPFAtributos_AtrTabelaNom1, AV17DynamicFiltersSelector2, AV22FuncaoAPFAtributos_AtributosNom2, AV24FuncaoAPFAtributos_AtrTabelaNom2, AV9DynamicFiltersEnabled2, AV43TFFuncaoAPFAtributos_AtributosNom, AV44TFFuncaoAPFAtributos_AtributosNom_Sel, AV47TFFuncaoAPFAtributos_AtrTabelaNom, AV48TFFuncaoAPFAtributos_AtrTabelaNom_Sel, AV51TFFuncaoAPFAtributos_FuncaoDadosNom, AV52TFFuncaoAPFAtributos_FuncaoDadosNom_Sel, AV19FuncaoAPF_Codigo, AV45ddo_FuncaoAPFAtributos_AtributosNomTitleControlIdToReplace, AV49ddo_FuncaoAPFAtributos_AtrTabelaNomTitleControlIdToReplace, AV53ddo_FuncaoAPFAtributos_FuncaoDadosNomTitleControlIdToReplace, AV62Pgmname, AV20FuncaoAPF_SistemaCod, AV25GridState, AV11DynamicFiltersIgnoreFirst, AV15DynamicFiltersRemoving, A393FuncaoAPFAtributos_SistemaCod, A415FuncaoAPFAtributos_FuncaoDadosTip, A378FuncaoAPFAtributos_FuncaoDadosCod, A391FuncaoDados_FuncaoDadosCod, AV38FuncaoAPFAtributos_FuncaoDadosCod, A373FuncaoDados_Tipo, sPrefix) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV30OrderedBy, AV31OrderedDsc, AV16DynamicFiltersSelector1, AV21FuncaoAPFAtributos_AtributosNom1, AV23FuncaoAPFAtributos_AtrTabelaNom1, AV17DynamicFiltersSelector2, AV22FuncaoAPFAtributos_AtributosNom2, AV24FuncaoAPFAtributos_AtrTabelaNom2, AV9DynamicFiltersEnabled2, AV43TFFuncaoAPFAtributos_AtributosNom, AV44TFFuncaoAPFAtributos_AtributosNom_Sel, AV47TFFuncaoAPFAtributos_AtrTabelaNom, AV48TFFuncaoAPFAtributos_AtrTabelaNom_Sel, AV51TFFuncaoAPFAtributos_FuncaoDadosNom, AV52TFFuncaoAPFAtributos_FuncaoDadosNom_Sel, AV19FuncaoAPF_Codigo, AV45ddo_FuncaoAPFAtributos_AtributosNomTitleControlIdToReplace, AV49ddo_FuncaoAPFAtributos_AtrTabelaNomTitleControlIdToReplace, AV53ddo_FuncaoAPFAtributos_FuncaoDadosNomTitleControlIdToReplace, AV62Pgmname, AV20FuncaoAPF_SistemaCod, AV25GridState, AV11DynamicFiltersIgnoreFirst, AV15DynamicFiltersRemoving, A393FuncaoAPFAtributos_SistemaCod, A415FuncaoAPFAtributos_FuncaoDadosTip, A378FuncaoAPFAtributos_FuncaoDadosCod, A391FuncaoDados_FuncaoDadosCod, AV38FuncaoAPFAtributos_FuncaoDadosCod, A373FuncaoDados_Tipo, sPrefix) ;
         }
         return (int)(0) ;
      }

      protected void STRUP980( )
      {
         /* Before Start, stand alone formulas. */
         AV62Pgmname = "FuncaoAPFFuncoesAPFAtributosWC";
         context.Gx_err = 0;
         cmbavFuncaoapfatributos_funcaodadostip.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavFuncaoapfatributos_funcaodadostip_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavFuncaoapfatributos_funcaodadostip.Enabled), 5, 0)));
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E23982 */
         E23982 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vDDO_TITLESETTINGSICONS"), AV54DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vFUNCAOAPFATRIBUTOS_ATRIBUTOSNOMTITLEFILTERDATA"), AV42FuncaoAPFAtributos_AtributosNomTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vFUNCAOAPFATRIBUTOS_ATRTABELANOMTITLEFILTERDATA"), AV46FuncaoAPFAtributos_AtrTabelaNomTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vFUNCAOAPFATRIBUTOS_FUNCAODADOSNOMTITLEFILTERDATA"), AV50FuncaoAPFAtributos_FuncaoDadosNomTitleFilterData);
            /* Read variables values. */
            cmbavOrderedby.Name = cmbavOrderedby_Internalname;
            cmbavOrderedby.CurrentValue = cgiGet( cmbavOrderedby_Internalname);
            AV30OrderedBy = (short)(NumberUtil.Val( cgiGet( cmbavOrderedby_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV30OrderedBy), 4, 0)));
            AV31OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV31OrderedDsc", AV31OrderedDsc);
            cmbavDynamicfiltersselector1.Name = cmbavDynamicfiltersselector1_Internalname;
            cmbavDynamicfiltersselector1.CurrentValue = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            AV16DynamicFiltersSelector1 = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
            AV21FuncaoAPFAtributos_AtributosNom1 = StringUtil.Upper( cgiGet( edtavFuncaoapfatributos_atributosnom1_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21FuncaoAPFAtributos_AtributosNom1", AV21FuncaoAPFAtributos_AtributosNom1);
            AV23FuncaoAPFAtributos_AtrTabelaNom1 = StringUtil.Upper( cgiGet( edtavFuncaoapfatributos_atrtabelanom1_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23FuncaoAPFAtributos_AtrTabelaNom1", AV23FuncaoAPFAtributos_AtrTabelaNom1);
            cmbavDynamicfiltersselector2.Name = cmbavDynamicfiltersselector2_Internalname;
            cmbavDynamicfiltersselector2.CurrentValue = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            AV17DynamicFiltersSelector2 = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17DynamicFiltersSelector2", AV17DynamicFiltersSelector2);
            AV22FuncaoAPFAtributos_AtributosNom2 = StringUtil.Upper( cgiGet( edtavFuncaoapfatributos_atributosnom2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22FuncaoAPFAtributos_AtributosNom2", AV22FuncaoAPFAtributos_AtributosNom2);
            AV24FuncaoAPFAtributos_AtrTabelaNom2 = StringUtil.Upper( cgiGet( edtavFuncaoapfatributos_atrtabelanom2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24FuncaoAPFAtributos_AtrTabelaNom2", AV24FuncaoAPFAtributos_AtrTabelaNom2);
            AV9DynamicFiltersEnabled2 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV9DynamicFiltersEnabled2", AV9DynamicFiltersEnabled2);
            AV43TFFuncaoAPFAtributos_AtributosNom = StringUtil.Upper( cgiGet( edtavTffuncaoapfatributos_atributosnom_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV43TFFuncaoAPFAtributos_AtributosNom", AV43TFFuncaoAPFAtributos_AtributosNom);
            AV44TFFuncaoAPFAtributos_AtributosNom_Sel = StringUtil.Upper( cgiGet( edtavTffuncaoapfatributos_atributosnom_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV44TFFuncaoAPFAtributos_AtributosNom_Sel", AV44TFFuncaoAPFAtributos_AtributosNom_Sel);
            AV47TFFuncaoAPFAtributos_AtrTabelaNom = StringUtil.Upper( cgiGet( edtavTffuncaoapfatributos_atrtabelanom_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV47TFFuncaoAPFAtributos_AtrTabelaNom", AV47TFFuncaoAPFAtributos_AtrTabelaNom);
            AV48TFFuncaoAPFAtributos_AtrTabelaNom_Sel = StringUtil.Upper( cgiGet( edtavTffuncaoapfatributos_atrtabelanom_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV48TFFuncaoAPFAtributos_AtrTabelaNom_Sel", AV48TFFuncaoAPFAtributos_AtrTabelaNom_Sel);
            AV51TFFuncaoAPFAtributos_FuncaoDadosNom = cgiGet( edtavTffuncaoapfatributos_funcaodadosnom_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV51TFFuncaoAPFAtributos_FuncaoDadosNom", AV51TFFuncaoAPFAtributos_FuncaoDadosNom);
            AV52TFFuncaoAPFAtributos_FuncaoDadosNom_Sel = cgiGet( edtavTffuncaoapfatributos_funcaodadosnom_sel_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV52TFFuncaoAPFAtributos_FuncaoDadosNom_Sel", AV52TFFuncaoAPFAtributos_FuncaoDadosNom_Sel);
            AV45ddo_FuncaoAPFAtributos_AtributosNomTitleControlIdToReplace = cgiGet( edtavDdo_funcaoapfatributos_atributosnomtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV45ddo_FuncaoAPFAtributos_AtributosNomTitleControlIdToReplace", AV45ddo_FuncaoAPFAtributos_AtributosNomTitleControlIdToReplace);
            AV49ddo_FuncaoAPFAtributos_AtrTabelaNomTitleControlIdToReplace = cgiGet( edtavDdo_funcaoapfatributos_atrtabelanomtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV49ddo_FuncaoAPFAtributos_AtrTabelaNomTitleControlIdToReplace", AV49ddo_FuncaoAPFAtributos_AtrTabelaNomTitleControlIdToReplace);
            AV53ddo_FuncaoAPFAtributos_FuncaoDadosNomTitleControlIdToReplace = cgiGet( edtavDdo_funcaoapfatributos_funcaodadosnomtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV53ddo_FuncaoAPFAtributos_FuncaoDadosNomTitleControlIdToReplace", AV53ddo_FuncaoAPFAtributos_FuncaoDadosNomTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_57 = (short)(context.localUtil.CToN( cgiGet( sPrefix+"nRC_GXsfl_57"), ",", "."));
            AV56GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( sPrefix+"vGRIDCURRENTPAGE"), ",", "."));
            AV57GridPageCount = (long)(context.localUtil.CToN( cgiGet( sPrefix+"vGRIDPAGECOUNT"), ",", "."));
            wcpOAV19FuncaoAPF_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV19FuncaoAPF_Codigo"), ",", "."));
            wcpOAV20FuncaoAPF_SistemaCod = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV20FuncaoAPF_SistemaCod"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( sPrefix+"GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Emptygridcaption");
            Ddo_funcaoapfatributos_atributosnom_Caption = cgiGet( sPrefix+"DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM_Caption");
            Ddo_funcaoapfatributos_atributosnom_Tooltip = cgiGet( sPrefix+"DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM_Tooltip");
            Ddo_funcaoapfatributos_atributosnom_Cls = cgiGet( sPrefix+"DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM_Cls");
            Ddo_funcaoapfatributos_atributosnom_Filteredtext_set = cgiGet( sPrefix+"DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM_Filteredtext_set");
            Ddo_funcaoapfatributos_atributosnom_Selectedvalue_set = cgiGet( sPrefix+"DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM_Selectedvalue_set");
            Ddo_funcaoapfatributos_atributosnom_Dropdownoptionstype = cgiGet( sPrefix+"DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM_Dropdownoptionstype");
            Ddo_funcaoapfatributos_atributosnom_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM_Titlecontrolidtoreplace");
            Ddo_funcaoapfatributos_atributosnom_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM_Includesortasc"));
            Ddo_funcaoapfatributos_atributosnom_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM_Includesortdsc"));
            Ddo_funcaoapfatributos_atributosnom_Sortedstatus = cgiGet( sPrefix+"DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM_Sortedstatus");
            Ddo_funcaoapfatributos_atributosnom_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM_Includefilter"));
            Ddo_funcaoapfatributos_atributosnom_Filtertype = cgiGet( sPrefix+"DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM_Filtertype");
            Ddo_funcaoapfatributos_atributosnom_Filterisrange = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM_Filterisrange"));
            Ddo_funcaoapfatributos_atributosnom_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM_Includedatalist"));
            Ddo_funcaoapfatributos_atributosnom_Datalisttype = cgiGet( sPrefix+"DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM_Datalisttype");
            Ddo_funcaoapfatributos_atributosnom_Datalistfixedvalues = cgiGet( sPrefix+"DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM_Datalistfixedvalues");
            Ddo_funcaoapfatributos_atributosnom_Datalistproc = cgiGet( sPrefix+"DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM_Datalistproc");
            Ddo_funcaoapfatributos_atributosnom_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( sPrefix+"DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_funcaoapfatributos_atributosnom_Sortasc = cgiGet( sPrefix+"DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM_Sortasc");
            Ddo_funcaoapfatributos_atributosnom_Sortdsc = cgiGet( sPrefix+"DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM_Sortdsc");
            Ddo_funcaoapfatributos_atributosnom_Loadingdata = cgiGet( sPrefix+"DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM_Loadingdata");
            Ddo_funcaoapfatributos_atributosnom_Cleanfilter = cgiGet( sPrefix+"DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM_Cleanfilter");
            Ddo_funcaoapfatributos_atributosnom_Rangefilterfrom = cgiGet( sPrefix+"DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM_Rangefilterfrom");
            Ddo_funcaoapfatributos_atributosnom_Rangefilterto = cgiGet( sPrefix+"DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM_Rangefilterto");
            Ddo_funcaoapfatributos_atributosnom_Noresultsfound = cgiGet( sPrefix+"DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM_Noresultsfound");
            Ddo_funcaoapfatributos_atributosnom_Searchbuttontext = cgiGet( sPrefix+"DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM_Searchbuttontext");
            Ddo_funcaoapfatributos_atrtabelanom_Caption = cgiGet( sPrefix+"DDO_FUNCAOAPFATRIBUTOS_ATRTABELANOM_Caption");
            Ddo_funcaoapfatributos_atrtabelanom_Tooltip = cgiGet( sPrefix+"DDO_FUNCAOAPFATRIBUTOS_ATRTABELANOM_Tooltip");
            Ddo_funcaoapfatributos_atrtabelanom_Cls = cgiGet( sPrefix+"DDO_FUNCAOAPFATRIBUTOS_ATRTABELANOM_Cls");
            Ddo_funcaoapfatributos_atrtabelanom_Filteredtext_set = cgiGet( sPrefix+"DDO_FUNCAOAPFATRIBUTOS_ATRTABELANOM_Filteredtext_set");
            Ddo_funcaoapfatributos_atrtabelanom_Selectedvalue_set = cgiGet( sPrefix+"DDO_FUNCAOAPFATRIBUTOS_ATRTABELANOM_Selectedvalue_set");
            Ddo_funcaoapfatributos_atrtabelanom_Dropdownoptionstype = cgiGet( sPrefix+"DDO_FUNCAOAPFATRIBUTOS_ATRTABELANOM_Dropdownoptionstype");
            Ddo_funcaoapfatributos_atrtabelanom_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_FUNCAOAPFATRIBUTOS_ATRTABELANOM_Titlecontrolidtoreplace");
            Ddo_funcaoapfatributos_atrtabelanom_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_FUNCAOAPFATRIBUTOS_ATRTABELANOM_Includesortasc"));
            Ddo_funcaoapfatributos_atrtabelanom_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_FUNCAOAPFATRIBUTOS_ATRTABELANOM_Includesortdsc"));
            Ddo_funcaoapfatributos_atrtabelanom_Sortedstatus = cgiGet( sPrefix+"DDO_FUNCAOAPFATRIBUTOS_ATRTABELANOM_Sortedstatus");
            Ddo_funcaoapfatributos_atrtabelanom_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_FUNCAOAPFATRIBUTOS_ATRTABELANOM_Includefilter"));
            Ddo_funcaoapfatributos_atrtabelanom_Filtertype = cgiGet( sPrefix+"DDO_FUNCAOAPFATRIBUTOS_ATRTABELANOM_Filtertype");
            Ddo_funcaoapfatributos_atrtabelanom_Filterisrange = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_FUNCAOAPFATRIBUTOS_ATRTABELANOM_Filterisrange"));
            Ddo_funcaoapfatributos_atrtabelanom_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_FUNCAOAPFATRIBUTOS_ATRTABELANOM_Includedatalist"));
            Ddo_funcaoapfatributos_atrtabelanom_Datalisttype = cgiGet( sPrefix+"DDO_FUNCAOAPFATRIBUTOS_ATRTABELANOM_Datalisttype");
            Ddo_funcaoapfatributos_atrtabelanom_Datalistfixedvalues = cgiGet( sPrefix+"DDO_FUNCAOAPFATRIBUTOS_ATRTABELANOM_Datalistfixedvalues");
            Ddo_funcaoapfatributos_atrtabelanom_Datalistproc = cgiGet( sPrefix+"DDO_FUNCAOAPFATRIBUTOS_ATRTABELANOM_Datalistproc");
            Ddo_funcaoapfatributos_atrtabelanom_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( sPrefix+"DDO_FUNCAOAPFATRIBUTOS_ATRTABELANOM_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_funcaoapfatributos_atrtabelanom_Sortasc = cgiGet( sPrefix+"DDO_FUNCAOAPFATRIBUTOS_ATRTABELANOM_Sortasc");
            Ddo_funcaoapfatributos_atrtabelanom_Sortdsc = cgiGet( sPrefix+"DDO_FUNCAOAPFATRIBUTOS_ATRTABELANOM_Sortdsc");
            Ddo_funcaoapfatributos_atrtabelanom_Loadingdata = cgiGet( sPrefix+"DDO_FUNCAOAPFATRIBUTOS_ATRTABELANOM_Loadingdata");
            Ddo_funcaoapfatributos_atrtabelanom_Cleanfilter = cgiGet( sPrefix+"DDO_FUNCAOAPFATRIBUTOS_ATRTABELANOM_Cleanfilter");
            Ddo_funcaoapfatributos_atrtabelanom_Rangefilterfrom = cgiGet( sPrefix+"DDO_FUNCAOAPFATRIBUTOS_ATRTABELANOM_Rangefilterfrom");
            Ddo_funcaoapfatributos_atrtabelanom_Rangefilterto = cgiGet( sPrefix+"DDO_FUNCAOAPFATRIBUTOS_ATRTABELANOM_Rangefilterto");
            Ddo_funcaoapfatributos_atrtabelanom_Noresultsfound = cgiGet( sPrefix+"DDO_FUNCAOAPFATRIBUTOS_ATRTABELANOM_Noresultsfound");
            Ddo_funcaoapfatributos_atrtabelanom_Searchbuttontext = cgiGet( sPrefix+"DDO_FUNCAOAPFATRIBUTOS_ATRTABELANOM_Searchbuttontext");
            Ddo_funcaoapfatributos_funcaodadosnom_Caption = cgiGet( sPrefix+"DDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSNOM_Caption");
            Ddo_funcaoapfatributos_funcaodadosnom_Tooltip = cgiGet( sPrefix+"DDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSNOM_Tooltip");
            Ddo_funcaoapfatributos_funcaodadosnom_Cls = cgiGet( sPrefix+"DDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSNOM_Cls");
            Ddo_funcaoapfatributos_funcaodadosnom_Filteredtext_set = cgiGet( sPrefix+"DDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSNOM_Filteredtext_set");
            Ddo_funcaoapfatributos_funcaodadosnom_Selectedvalue_set = cgiGet( sPrefix+"DDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSNOM_Selectedvalue_set");
            Ddo_funcaoapfatributos_funcaodadosnom_Dropdownoptionstype = cgiGet( sPrefix+"DDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSNOM_Dropdownoptionstype");
            Ddo_funcaoapfatributos_funcaodadosnom_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSNOM_Titlecontrolidtoreplace");
            Ddo_funcaoapfatributos_funcaodadosnom_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSNOM_Includesortasc"));
            Ddo_funcaoapfatributos_funcaodadosnom_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSNOM_Includesortdsc"));
            Ddo_funcaoapfatributos_funcaodadosnom_Sortedstatus = cgiGet( sPrefix+"DDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSNOM_Sortedstatus");
            Ddo_funcaoapfatributos_funcaodadosnom_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSNOM_Includefilter"));
            Ddo_funcaoapfatributos_funcaodadosnom_Filtertype = cgiGet( sPrefix+"DDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSNOM_Filtertype");
            Ddo_funcaoapfatributos_funcaodadosnom_Filterisrange = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSNOM_Filterisrange"));
            Ddo_funcaoapfatributos_funcaodadosnom_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSNOM_Includedatalist"));
            Ddo_funcaoapfatributos_funcaodadosnom_Datalisttype = cgiGet( sPrefix+"DDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSNOM_Datalisttype");
            Ddo_funcaoapfatributos_funcaodadosnom_Datalistfixedvalues = cgiGet( sPrefix+"DDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSNOM_Datalistfixedvalues");
            Ddo_funcaoapfatributos_funcaodadosnom_Datalistproc = cgiGet( sPrefix+"DDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSNOM_Datalistproc");
            Ddo_funcaoapfatributos_funcaodadosnom_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( sPrefix+"DDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSNOM_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_funcaoapfatributos_funcaodadosnom_Sortasc = cgiGet( sPrefix+"DDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSNOM_Sortasc");
            Ddo_funcaoapfatributos_funcaodadosnom_Sortdsc = cgiGet( sPrefix+"DDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSNOM_Sortdsc");
            Ddo_funcaoapfatributos_funcaodadosnom_Loadingdata = cgiGet( sPrefix+"DDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSNOM_Loadingdata");
            Ddo_funcaoapfatributos_funcaodadosnom_Cleanfilter = cgiGet( sPrefix+"DDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSNOM_Cleanfilter");
            Ddo_funcaoapfatributos_funcaodadosnom_Rangefilterfrom = cgiGet( sPrefix+"DDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSNOM_Rangefilterfrom");
            Ddo_funcaoapfatributos_funcaodadosnom_Rangefilterto = cgiGet( sPrefix+"DDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSNOM_Rangefilterto");
            Ddo_funcaoapfatributos_funcaodadosnom_Noresultsfound = cgiGet( sPrefix+"DDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSNOM_Noresultsfound");
            Ddo_funcaoapfatributos_funcaodadosnom_Searchbuttontext = cgiGet( sPrefix+"DDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSNOM_Searchbuttontext");
            Gridpaginationbar_Selectedpage = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Selectedpage");
            Ddo_funcaoapfatributos_atributosnom_Activeeventkey = cgiGet( sPrefix+"DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM_Activeeventkey");
            Ddo_funcaoapfatributos_atributosnom_Filteredtext_get = cgiGet( sPrefix+"DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM_Filteredtext_get");
            Ddo_funcaoapfatributos_atributosnom_Selectedvalue_get = cgiGet( sPrefix+"DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM_Selectedvalue_get");
            Ddo_funcaoapfatributos_atrtabelanom_Activeeventkey = cgiGet( sPrefix+"DDO_FUNCAOAPFATRIBUTOS_ATRTABELANOM_Activeeventkey");
            Ddo_funcaoapfatributos_atrtabelanom_Filteredtext_get = cgiGet( sPrefix+"DDO_FUNCAOAPFATRIBUTOS_ATRTABELANOM_Filteredtext_get");
            Ddo_funcaoapfatributos_atrtabelanom_Selectedvalue_get = cgiGet( sPrefix+"DDO_FUNCAOAPFATRIBUTOS_ATRTABELANOM_Selectedvalue_get");
            Ddo_funcaoapfatributos_funcaodadosnom_Activeeventkey = cgiGet( sPrefix+"DDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSNOM_Activeeventkey");
            Ddo_funcaoapfatributos_funcaodadosnom_Filteredtext_get = cgiGet( sPrefix+"DDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSNOM_Filteredtext_get");
            Ddo_funcaoapfatributos_funcaodadosnom_Selectedvalue_get = cgiGet( sPrefix+"DDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSNOM_Selectedvalue_get");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV30OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vORDEREDDSC")) != AV31OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vDYNAMICFILTERSSELECTOR1"), AV16DynamicFiltersSelector1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM1"), AV21FuncaoAPFAtributos_AtributosNom1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vFUNCAOAPFATRIBUTOS_ATRTABELANOM1"), AV23FuncaoAPFAtributos_AtrTabelaNom1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vDYNAMICFILTERSSELECTOR2"), AV17DynamicFiltersSelector2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM2"), AV22FuncaoAPFAtributos_AtributosNom2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vFUNCAOAPFATRIBUTOS_ATRTABELANOM2"), AV24FuncaoAPFAtributos_AtrTabelaNom2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vDYNAMICFILTERSENABLED2")) != AV9DynamicFiltersEnabled2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM"), AV43TFFuncaoAPFAtributos_AtributosNom) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM_SEL"), AV44TFFuncaoAPFAtributos_AtributosNom_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFFUNCAOAPFATRIBUTOS_ATRTABELANOM"), AV47TFFuncaoAPFAtributos_AtrTabelaNom) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFFUNCAOAPFATRIBUTOS_ATRTABELANOM_SEL"), AV48TFFuncaoAPFAtributos_AtrTabelaNom_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFFUNCAOAPFATRIBUTOS_FUNCAODADOSNOM"), AV51TFFuncaoAPFAtributos_FuncaoDadosNom) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFFUNCAOAPFATRIBUTOS_FUNCAODADOSNOM_SEL"), AV52TFFuncaoAPFAtributos_FuncaoDadosNom_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E23982 */
         E23982 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E23982( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         lblJsdynamicfilters_Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         chkavDynamicfiltersenabled2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkavDynamicfiltersenabled2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled2.Visible), 5, 0)));
         AV16DynamicFiltersSelector1 = "FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV17DynamicFiltersSelector2 = "FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17DynamicFiltersSelector2", AV17DynamicFiltersSelector2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         imgAdddynamicfilters1_Jsonclick = "WWPDynFilterShow(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgAdddynamicfilters1_Internalname, "Jsonclick", imgAdddynamicfilters1_Jsonclick);
         imgRemovedynamicfilters1_Jsonclick = "WWPDynFilterHideLast(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters1_Internalname, "Jsonclick", imgRemovedynamicfilters1_Jsonclick);
         imgRemovedynamicfilters2_Jsonclick = "WWPDynFilterHideLast(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters2_Internalname, "Jsonclick", imgRemovedynamicfilters2_Jsonclick);
         edtavTffuncaoapfatributos_atributosnom_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTffuncaoapfatributos_atributosnom_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTffuncaoapfatributos_atributosnom_Visible), 5, 0)));
         edtavTffuncaoapfatributos_atributosnom_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTffuncaoapfatributos_atributosnom_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTffuncaoapfatributos_atributosnom_sel_Visible), 5, 0)));
         edtavTffuncaoapfatributos_atrtabelanom_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTffuncaoapfatributos_atrtabelanom_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTffuncaoapfatributos_atrtabelanom_Visible), 5, 0)));
         edtavTffuncaoapfatributos_atrtabelanom_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTffuncaoapfatributos_atrtabelanom_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTffuncaoapfatributos_atrtabelanom_sel_Visible), 5, 0)));
         edtavTffuncaoapfatributos_funcaodadosnom_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTffuncaoapfatributos_funcaodadosnom_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTffuncaoapfatributos_funcaodadosnom_Visible), 5, 0)));
         edtavTffuncaoapfatributos_funcaodadosnom_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTffuncaoapfatributos_funcaodadosnom_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTffuncaoapfatributos_funcaodadosnom_sel_Visible), 5, 0)));
         Ddo_funcaoapfatributos_atributosnom_Titlecontrolidtoreplace = subGrid_Internalname+"_FuncaoAPFAtributos_AtributosNom";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaoapfatributos_atributosnom_Internalname, "TitleControlIdToReplace", Ddo_funcaoapfatributos_atributosnom_Titlecontrolidtoreplace);
         AV45ddo_FuncaoAPFAtributos_AtributosNomTitleControlIdToReplace = Ddo_funcaoapfatributos_atributosnom_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV45ddo_FuncaoAPFAtributos_AtributosNomTitleControlIdToReplace", AV45ddo_FuncaoAPFAtributos_AtributosNomTitleControlIdToReplace);
         edtavDdo_funcaoapfatributos_atributosnomtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_funcaoapfatributos_atributosnomtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_funcaoapfatributos_atributosnomtitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_funcaoapfatributos_atrtabelanom_Titlecontrolidtoreplace = subGrid_Internalname+"_FuncaoAPFAtributos_AtrTabelaNom";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaoapfatributos_atrtabelanom_Internalname, "TitleControlIdToReplace", Ddo_funcaoapfatributos_atrtabelanom_Titlecontrolidtoreplace);
         AV49ddo_FuncaoAPFAtributos_AtrTabelaNomTitleControlIdToReplace = Ddo_funcaoapfatributos_atrtabelanom_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV49ddo_FuncaoAPFAtributos_AtrTabelaNomTitleControlIdToReplace", AV49ddo_FuncaoAPFAtributos_AtrTabelaNomTitleControlIdToReplace);
         edtavDdo_funcaoapfatributos_atrtabelanomtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_funcaoapfatributos_atrtabelanomtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_funcaoapfatributos_atrtabelanomtitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_funcaoapfatributos_funcaodadosnom_Titlecontrolidtoreplace = subGrid_Internalname+"_FuncaoAPFAtributos_FuncaoDadosNom";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaoapfatributos_funcaodadosnom_Internalname, "TitleControlIdToReplace", Ddo_funcaoapfatributos_funcaodadosnom_Titlecontrolidtoreplace);
         AV53ddo_FuncaoAPFAtributos_FuncaoDadosNomTitleControlIdToReplace = Ddo_funcaoapfatributos_funcaodadosnom_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV53ddo_FuncaoAPFAtributos_FuncaoDadosNomTitleControlIdToReplace", AV53ddo_FuncaoAPFAtributos_FuncaoDadosNomTitleControlIdToReplace);
         edtavDdo_funcaoapfatributos_funcaodadosnomtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_funcaoapfatributos_funcaodadosnomtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_funcaoapfatributos_funcaodadosnomtitlecontrolidtoreplace_Visible), 5, 0)));
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavOrderedby.removeAllItems();
         cmbavOrderedby.addItem("1", "Fun��o de Dados", 0);
         cmbavOrderedby.addItem("2", "Nome", 0);
         cmbavOrderedby.addItem("3", "Tabela", 0);
         cmbavOrderedby.addItem("4", "Fun��o de Dados", 0);
         if ( AV30OrderedBy < 1 )
         {
            AV30OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV30OrderedBy), 4, 0)));
            /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         imgCleanfilters_Jsonclick = "WWPDynFilterHideAll(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgCleanfilters_Internalname, "Jsonclick", imgCleanfilters_Jsonclick);
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = AV54DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1) ;
         AV54DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1;
      }

      protected void E24982( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV42FuncaoAPFAtributos_AtributosNomTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV46FuncaoAPFAtributos_AtrTabelaNomTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV50FuncaoAPFAtributos_FuncaoDadosNomTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV36WWPContext) ;
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtFuncaoAPFAtributos_AtributosNom_Titleformat = 2;
         edtFuncaoAPFAtributos_AtributosNom_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Nome", AV45ddo_FuncaoAPFAtributos_AtributosNomTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtFuncaoAPFAtributos_AtributosNom_Internalname, "Title", edtFuncaoAPFAtributos_AtributosNom_Title);
         edtFuncaoAPFAtributos_AtrTabelaNom_Titleformat = 2;
         edtFuncaoAPFAtributos_AtrTabelaNom_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Tabela", AV49ddo_FuncaoAPFAtributos_AtrTabelaNomTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtFuncaoAPFAtributos_AtrTabelaNom_Internalname, "Title", edtFuncaoAPFAtributos_AtrTabelaNom_Title);
         edtFuncaoAPFAtributos_FuncaoDadosNom_Titleformat = 2;
         edtFuncaoAPFAtributos_FuncaoDadosNom_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Fun��o de Dados", AV53ddo_FuncaoAPFAtributos_FuncaoDadosNomTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtFuncaoAPFAtributos_FuncaoDadosNom_Internalname, "Title", edtFuncaoAPFAtributos_FuncaoDadosNom_Title);
         AV56GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV56GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV56GridCurrentPage), 10, 0)));
         AV57GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV57GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV57GridPageCount), 10, 0)));
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV42FuncaoAPFAtributos_AtributosNomTitleFilterData", AV42FuncaoAPFAtributos_AtributosNomTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV46FuncaoAPFAtributos_AtrTabelaNomTitleFilterData", AV46FuncaoAPFAtributos_AtrTabelaNomTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV50FuncaoAPFAtributos_FuncaoDadosNomTitleFilterData", AV50FuncaoAPFAtributos_FuncaoDadosNomTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV25GridState", AV25GridState);
      }

      protected void E11982( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV55PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV55PageToGo) ;
         }
      }

      protected void E12982( )
      {
         /* Ddo_funcaoapfatributos_atributosnom_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_funcaoapfatributos_atributosnom_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV30OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV30OrderedBy), 4, 0)));
            AV31OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV31OrderedDsc", AV31OrderedDsc);
            Ddo_funcaoapfatributos_atributosnom_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaoapfatributos_atributosnom_Internalname, "SortedStatus", Ddo_funcaoapfatributos_atributosnom_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_funcaoapfatributos_atributosnom_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV30OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV30OrderedBy), 4, 0)));
            AV31OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV31OrderedDsc", AV31OrderedDsc);
            Ddo_funcaoapfatributos_atributosnom_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaoapfatributos_atributosnom_Internalname, "SortedStatus", Ddo_funcaoapfatributos_atributosnom_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_funcaoapfatributos_atributosnom_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV43TFFuncaoAPFAtributos_AtributosNom = Ddo_funcaoapfatributos_atributosnom_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV43TFFuncaoAPFAtributos_AtributosNom", AV43TFFuncaoAPFAtributos_AtributosNom);
            AV44TFFuncaoAPFAtributos_AtributosNom_Sel = Ddo_funcaoapfatributos_atributosnom_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV44TFFuncaoAPFAtributos_AtributosNom_Sel", AV44TFFuncaoAPFAtributos_AtributosNom_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV30OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E13982( )
      {
         /* Ddo_funcaoapfatributos_atrtabelanom_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_funcaoapfatributos_atrtabelanom_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV30OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV30OrderedBy), 4, 0)));
            AV31OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV31OrderedDsc", AV31OrderedDsc);
            Ddo_funcaoapfatributos_atrtabelanom_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaoapfatributos_atrtabelanom_Internalname, "SortedStatus", Ddo_funcaoapfatributos_atrtabelanom_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_funcaoapfatributos_atrtabelanom_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV30OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV30OrderedBy), 4, 0)));
            AV31OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV31OrderedDsc", AV31OrderedDsc);
            Ddo_funcaoapfatributos_atrtabelanom_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaoapfatributos_atrtabelanom_Internalname, "SortedStatus", Ddo_funcaoapfatributos_atrtabelanom_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_funcaoapfatributos_atrtabelanom_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV47TFFuncaoAPFAtributos_AtrTabelaNom = Ddo_funcaoapfatributos_atrtabelanom_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV47TFFuncaoAPFAtributos_AtrTabelaNom", AV47TFFuncaoAPFAtributos_AtrTabelaNom);
            AV48TFFuncaoAPFAtributos_AtrTabelaNom_Sel = Ddo_funcaoapfatributos_atrtabelanom_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV48TFFuncaoAPFAtributos_AtrTabelaNom_Sel", AV48TFFuncaoAPFAtributos_AtrTabelaNom_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV30OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E14982( )
      {
         /* Ddo_funcaoapfatributos_funcaodadosnom_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_funcaoapfatributos_funcaodadosnom_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV30OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV30OrderedBy), 4, 0)));
            AV31OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV31OrderedDsc", AV31OrderedDsc);
            Ddo_funcaoapfatributos_funcaodadosnom_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaoapfatributos_funcaodadosnom_Internalname, "SortedStatus", Ddo_funcaoapfatributos_funcaodadosnom_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_funcaoapfatributos_funcaodadosnom_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV30OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV30OrderedBy), 4, 0)));
            AV31OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV31OrderedDsc", AV31OrderedDsc);
            Ddo_funcaoapfatributos_funcaodadosnom_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaoapfatributos_funcaodadosnom_Internalname, "SortedStatus", Ddo_funcaoapfatributos_funcaodadosnom_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_funcaoapfatributos_funcaodadosnom_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV51TFFuncaoAPFAtributos_FuncaoDadosNom = Ddo_funcaoapfatributos_funcaodadosnom_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV51TFFuncaoAPFAtributos_FuncaoDadosNom", AV51TFFuncaoAPFAtributos_FuncaoDadosNom);
            AV52TFFuncaoAPFAtributos_FuncaoDadosNom_Sel = Ddo_funcaoapfatributos_funcaodadosnom_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV52TFFuncaoAPFAtributos_FuncaoDadosNom_Sel", AV52TFFuncaoAPFAtributos_FuncaoDadosNom_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV30OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      private void E25982( )
      {
         /* Grid_Load Routine */
         AV39DeleteAtributo = context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavDeleteatributo_Internalname, AV39DeleteAtributo);
         AV61Deleteatributo_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )));
         edtavDeleteatributo_Tooltiptext = "Desvincular atributo da Fun��o";
         AV39DeleteAtributo = context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavDeleteatributo_Internalname, AV39DeleteAtributo);
         AV61Deleteatributo_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )));
         edtavDeleteatributo_Tooltiptext = "Desvincular atributo da Fun��o";
         if ( AV20FuncaoAPF_SistemaCod == A393FuncaoAPFAtributos_SistemaCod )
         {
            AV37FuncaoAPFAtributos_FuncaoDadosTip = A415FuncaoAPFAtributos_FuncaoDadosTip;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, cmbavFuncaoapfatributos_funcaodadostip_Internalname, AV37FuncaoAPFAtributos_FuncaoDadosTip);
         }
         else
         {
            AV38FuncaoAPFAtributos_FuncaoDadosCod = A378FuncaoAPFAtributos_FuncaoDadosCod;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV38FuncaoAPFAtributos_FuncaoDadosCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV38FuncaoAPFAtributos_FuncaoDadosCod), 6, 0)));
            /* Execute user subroutine: 'FUNCAODADOS_TIPO' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 57;
         }
         sendrow_572( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_57_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(57, GridRow);
         }
         cmbavFuncaoapfatributos_funcaodadostip.CurrentValue = StringUtil.RTrim( AV37FuncaoAPFAtributos_FuncaoDadosTip);
      }

      protected void E15982( )
      {
         /* Orderedby_Click Routine */
         context.DoAjaxRefreshCmp(sPrefix);
      }

      protected void E20982( )
      {
         /* 'AddDynamicFilters1' Routine */
         AV9DynamicFiltersEnabled2 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV9DynamicFiltersEnabled2", AV9DynamicFiltersEnabled2);
         imgAdddynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
      }

      protected void E16982( )
      {
         /* 'RemoveDynamicFilters1' Routine */
         AV15DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15DynamicFiltersRemoving", AV15DynamicFiltersRemoving);
         AV11DynamicFiltersIgnoreFirst = true;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV11DynamicFiltersIgnoreFirst", AV11DynamicFiltersIgnoreFirst);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV15DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15DynamicFiltersRemoving", AV15DynamicFiltersRemoving);
         AV11DynamicFiltersIgnoreFirst = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV11DynamicFiltersIgnoreFirst", AV11DynamicFiltersIgnoreFirst);
         gxgrGrid_refresh( subGrid_Rows, AV30OrderedBy, AV31OrderedDsc, AV16DynamicFiltersSelector1, AV21FuncaoAPFAtributos_AtributosNom1, AV23FuncaoAPFAtributos_AtrTabelaNom1, AV17DynamicFiltersSelector2, AV22FuncaoAPFAtributos_AtributosNom2, AV24FuncaoAPFAtributos_AtrTabelaNom2, AV9DynamicFiltersEnabled2, AV43TFFuncaoAPFAtributos_AtributosNom, AV44TFFuncaoAPFAtributos_AtributosNom_Sel, AV47TFFuncaoAPFAtributos_AtrTabelaNom, AV48TFFuncaoAPFAtributos_AtrTabelaNom_Sel, AV51TFFuncaoAPFAtributos_FuncaoDadosNom, AV52TFFuncaoAPFAtributos_FuncaoDadosNom_Sel, AV19FuncaoAPF_Codigo, AV45ddo_FuncaoAPFAtributos_AtributosNomTitleControlIdToReplace, AV49ddo_FuncaoAPFAtributos_AtrTabelaNomTitleControlIdToReplace, AV53ddo_FuncaoAPFAtributos_FuncaoDadosNomTitleControlIdToReplace, AV62Pgmname, AV20FuncaoAPF_SistemaCod, AV25GridState, AV11DynamicFiltersIgnoreFirst, AV15DynamicFiltersRemoving, A393FuncaoAPFAtributos_SistemaCod, A415FuncaoAPFAtributos_FuncaoDadosTip, A378FuncaoAPFAtributos_FuncaoDadosCod, A391FuncaoDados_FuncaoDadosCod, AV38FuncaoAPFAtributos_FuncaoDadosCod, A373FuncaoDados_Tipo, sPrefix) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV25GridState", AV25GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV17DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV16DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
      }

      protected void E21982( )
      {
         /* Dynamicfiltersselector1_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E17982( )
      {
         /* 'RemoveDynamicFilters2' Routine */
         AV15DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15DynamicFiltersRemoving", AV15DynamicFiltersRemoving);
         AV9DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV9DynamicFiltersEnabled2", AV9DynamicFiltersEnabled2);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV15DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15DynamicFiltersRemoving", AV15DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV30OrderedBy, AV31OrderedDsc, AV16DynamicFiltersSelector1, AV21FuncaoAPFAtributos_AtributosNom1, AV23FuncaoAPFAtributos_AtrTabelaNom1, AV17DynamicFiltersSelector2, AV22FuncaoAPFAtributos_AtributosNom2, AV24FuncaoAPFAtributos_AtrTabelaNom2, AV9DynamicFiltersEnabled2, AV43TFFuncaoAPFAtributos_AtributosNom, AV44TFFuncaoAPFAtributos_AtributosNom_Sel, AV47TFFuncaoAPFAtributos_AtrTabelaNom, AV48TFFuncaoAPFAtributos_AtrTabelaNom_Sel, AV51TFFuncaoAPFAtributos_FuncaoDadosNom, AV52TFFuncaoAPFAtributos_FuncaoDadosNom_Sel, AV19FuncaoAPF_Codigo, AV45ddo_FuncaoAPFAtributos_AtributosNomTitleControlIdToReplace, AV49ddo_FuncaoAPFAtributos_AtrTabelaNomTitleControlIdToReplace, AV53ddo_FuncaoAPFAtributos_FuncaoDadosNomTitleControlIdToReplace, AV62Pgmname, AV20FuncaoAPF_SistemaCod, AV25GridState, AV11DynamicFiltersIgnoreFirst, AV15DynamicFiltersRemoving, A393FuncaoAPFAtributos_SistemaCod, A415FuncaoAPFAtributos_FuncaoDadosTip, A378FuncaoAPFAtributos_FuncaoDadosCod, A391FuncaoDados_FuncaoDadosCod, AV38FuncaoAPFAtributos_FuncaoDadosCod, A373FuncaoDados_Tipo, sPrefix) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV25GridState", AV25GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV17DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV16DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
      }

      protected void E22982( )
      {
         /* Dynamicfiltersselector2_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E18982( )
      {
         /* 'DoCleanFilters' Routine */
         /* Execute user subroutine: 'CLEANFILTERS' */
         S222 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_firstpage( ) ;
         context.DoAjaxRefreshCmp(sPrefix);
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV16DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV25GridState", AV25GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV17DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
      }

      protected void E26982( )
      {
         /* Deleteatributo_Click Routine */
         new prc_funcaoapfatributosdlt(context ).execute( ref  A165FuncaoAPF_Codigo, ref  A364FuncaoAPFAtributos_AtributosCod) ;
         context.DoAjaxRefreshCmp(sPrefix);
      }

      protected void E19982( )
      {
         /* 'DoInsertAtributo' Routine */
         context.PopUp(formatLink("wp_funcaoapfatributosins.aspx") + "?" + UrlEncode("" +AV19FuncaoAPF_Codigo) + "," + UrlEncode("" +AV20FuncaoAPF_SistemaCod), new Object[] {});
         context.DoAjaxRefreshCmp(sPrefix);
      }

      protected void S172( )
      {
         /* 'RESETDDOSORTEDSTATUS' Routine */
         Ddo_funcaoapfatributos_atributosnom_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaoapfatributos_atributosnom_Internalname, "SortedStatus", Ddo_funcaoapfatributos_atributosnom_Sortedstatus);
         Ddo_funcaoapfatributos_atrtabelanom_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaoapfatributos_atrtabelanom_Internalname, "SortedStatus", Ddo_funcaoapfatributos_atrtabelanom_Sortedstatus);
         Ddo_funcaoapfatributos_funcaodadosnom_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaoapfatributos_funcaodadosnom_Internalname, "SortedStatus", Ddo_funcaoapfatributos_funcaodadosnom_Sortedstatus);
      }

      protected void S152( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV30OrderedBy == 2 )
         {
            Ddo_funcaoapfatributos_atributosnom_Sortedstatus = (AV31OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaoapfatributos_atributosnom_Internalname, "SortedStatus", Ddo_funcaoapfatributos_atributosnom_Sortedstatus);
         }
         else if ( AV30OrderedBy == 3 )
         {
            Ddo_funcaoapfatributos_atrtabelanom_Sortedstatus = (AV31OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaoapfatributos_atrtabelanom_Internalname, "SortedStatus", Ddo_funcaoapfatributos_atrtabelanom_Sortedstatus);
         }
         else if ( AV30OrderedBy == 4 )
         {
            Ddo_funcaoapfatributos_funcaodadosnom_Sortedstatus = (AV31OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaoapfatributos_funcaodadosnom_Internalname, "SortedStatus", Ddo_funcaoapfatributos_funcaodadosnom_Sortedstatus);
         }
      }

      protected void S112( )
      {
         /* 'ENABLEDYNAMICFILTERS1' Routine */
         edtavFuncaoapfatributos_atributosnom1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavFuncaoapfatributos_atributosnom1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncaoapfatributos_atributosnom1_Visible), 5, 0)));
         edtavFuncaoapfatributos_atrtabelanom1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavFuncaoapfatributos_atrtabelanom1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncaoapfatributos_atrtabelanom1_Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM") == 0 )
         {
            edtavFuncaoapfatributos_atributosnom1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavFuncaoapfatributos_atributosnom1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncaoapfatributos_atributosnom1_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "FUNCAOAPFATRIBUTOS_ATRTABELANOM") == 0 )
         {
            edtavFuncaoapfatributos_atrtabelanom1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavFuncaoapfatributos_atrtabelanom1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncaoapfatributos_atrtabelanom1_Visible), 5, 0)));
         }
      }

      protected void S122( )
      {
         /* 'ENABLEDYNAMICFILTERS2' Routine */
         edtavFuncaoapfatributos_atributosnom2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavFuncaoapfatributos_atributosnom2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncaoapfatributos_atributosnom2_Visible), 5, 0)));
         edtavFuncaoapfatributos_atrtabelanom2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavFuncaoapfatributos_atrtabelanom2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncaoapfatributos_atrtabelanom2_Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV17DynamicFiltersSelector2, "FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM") == 0 )
         {
            edtavFuncaoapfatributos_atributosnom2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavFuncaoapfatributos_atributosnom2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncaoapfatributos_atributosnom2_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV17DynamicFiltersSelector2, "FUNCAOAPFATRIBUTOS_ATRTABELANOM") == 0 )
         {
            edtavFuncaoapfatributos_atrtabelanom2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavFuncaoapfatributos_atrtabelanom2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncaoapfatributos_atrtabelanom2_Visible), 5, 0)));
         }
      }

      protected void S202( )
      {
         /* 'RESETDYNFILTERS' Routine */
         AV9DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV9DynamicFiltersEnabled2", AV9DynamicFiltersEnabled2);
         AV17DynamicFiltersSelector2 = "FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17DynamicFiltersSelector2", AV17DynamicFiltersSelector2);
         AV22FuncaoAPFAtributos_AtributosNom2 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22FuncaoAPFAtributos_AtributosNom2", AV22FuncaoAPFAtributos_AtributosNom2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S222( )
      {
         /* 'CLEANFILTERS' Routine */
         AV43TFFuncaoAPFAtributos_AtributosNom = "";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV43TFFuncaoAPFAtributos_AtributosNom", AV43TFFuncaoAPFAtributos_AtributosNom);
         Ddo_funcaoapfatributos_atributosnom_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaoapfatributos_atributosnom_Internalname, "FilteredText_set", Ddo_funcaoapfatributos_atributosnom_Filteredtext_set);
         AV44TFFuncaoAPFAtributos_AtributosNom_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV44TFFuncaoAPFAtributos_AtributosNom_Sel", AV44TFFuncaoAPFAtributos_AtributosNom_Sel);
         Ddo_funcaoapfatributos_atributosnom_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaoapfatributos_atributosnom_Internalname, "SelectedValue_set", Ddo_funcaoapfatributos_atributosnom_Selectedvalue_set);
         AV47TFFuncaoAPFAtributos_AtrTabelaNom = "";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV47TFFuncaoAPFAtributos_AtrTabelaNom", AV47TFFuncaoAPFAtributos_AtrTabelaNom);
         Ddo_funcaoapfatributos_atrtabelanom_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaoapfatributos_atrtabelanom_Internalname, "FilteredText_set", Ddo_funcaoapfatributos_atrtabelanom_Filteredtext_set);
         AV48TFFuncaoAPFAtributos_AtrTabelaNom_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV48TFFuncaoAPFAtributos_AtrTabelaNom_Sel", AV48TFFuncaoAPFAtributos_AtrTabelaNom_Sel);
         Ddo_funcaoapfatributos_atrtabelanom_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaoapfatributos_atrtabelanom_Internalname, "SelectedValue_set", Ddo_funcaoapfatributos_atrtabelanom_Selectedvalue_set);
         AV51TFFuncaoAPFAtributos_FuncaoDadosNom = "";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV51TFFuncaoAPFAtributos_FuncaoDadosNom", AV51TFFuncaoAPFAtributos_FuncaoDadosNom);
         Ddo_funcaoapfatributos_funcaodadosnom_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaoapfatributos_funcaodadosnom_Internalname, "FilteredText_set", Ddo_funcaoapfatributos_funcaodadosnom_Filteredtext_set);
         AV52TFFuncaoAPFAtributos_FuncaoDadosNom_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV52TFFuncaoAPFAtributos_FuncaoDadosNom_Sel", AV52TFFuncaoAPFAtributos_FuncaoDadosNom_Sel);
         Ddo_funcaoapfatributos_funcaodadosnom_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaoapfatributos_funcaodadosnom_Internalname, "SelectedValue_set", Ddo_funcaoapfatributos_funcaodadosnom_Selectedvalue_set);
         AV16DynamicFiltersSelector1 = "FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
         AV21FuncaoAPFAtributos_AtributosNom1 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21FuncaoAPFAtributos_AtributosNom1", AV21FuncaoAPFAtributos_AtributosNom1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV25GridState.gxTpr_Dynamicfilters.Clear();
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         context.DoAjaxRefreshCmp(sPrefix);
      }

      protected void S142( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV32Session.Get(AV62Pgmname+"GridState"), "") == 0 )
         {
            AV25GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  AV62Pgmname+"GridState"), "");
         }
         else
         {
            AV25GridState.FromXml(AV32Session.Get(AV62Pgmname+"GridState"), "");
         }
         AV30OrderedBy = AV25GridState.gxTpr_Orderedby;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV30OrderedBy), 4, 0)));
         AV31OrderedDsc = AV25GridState.gxTpr_Ordereddsc;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV31OrderedDsc", AV31OrderedDsc);
         /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADREGFILTERSSTATE' */
         S232 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S232( )
      {
         /* 'LOADREGFILTERSSTATE' Routine */
         AV63GXV1 = 1;
         while ( AV63GXV1 <= AV25GridState.gxTpr_Filtervalues.Count )
         {
            AV27GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV25GridState.gxTpr_Filtervalues.Item(AV63GXV1));
            if ( StringUtil.StrCmp(AV27GridStateFilterValue.gxTpr_Name, "TFFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM") == 0 )
            {
               AV43TFFuncaoAPFAtributos_AtributosNom = AV27GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV43TFFuncaoAPFAtributos_AtributosNom", AV43TFFuncaoAPFAtributos_AtributosNom);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV43TFFuncaoAPFAtributos_AtributosNom)) )
               {
                  Ddo_funcaoapfatributos_atributosnom_Filteredtext_set = AV43TFFuncaoAPFAtributos_AtributosNom;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaoapfatributos_atributosnom_Internalname, "FilteredText_set", Ddo_funcaoapfatributos_atributosnom_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV27GridStateFilterValue.gxTpr_Name, "TFFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM_SEL") == 0 )
            {
               AV44TFFuncaoAPFAtributos_AtributosNom_Sel = AV27GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV44TFFuncaoAPFAtributos_AtributosNom_Sel", AV44TFFuncaoAPFAtributos_AtributosNom_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV44TFFuncaoAPFAtributos_AtributosNom_Sel)) )
               {
                  Ddo_funcaoapfatributos_atributosnom_Selectedvalue_set = AV44TFFuncaoAPFAtributos_AtributosNom_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaoapfatributos_atributosnom_Internalname, "SelectedValue_set", Ddo_funcaoapfatributos_atributosnom_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV27GridStateFilterValue.gxTpr_Name, "TFFUNCAOAPFATRIBUTOS_ATRTABELANOM") == 0 )
            {
               AV47TFFuncaoAPFAtributos_AtrTabelaNom = AV27GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV47TFFuncaoAPFAtributos_AtrTabelaNom", AV47TFFuncaoAPFAtributos_AtrTabelaNom);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV47TFFuncaoAPFAtributos_AtrTabelaNom)) )
               {
                  Ddo_funcaoapfatributos_atrtabelanom_Filteredtext_set = AV47TFFuncaoAPFAtributos_AtrTabelaNom;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaoapfatributos_atrtabelanom_Internalname, "FilteredText_set", Ddo_funcaoapfatributos_atrtabelanom_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV27GridStateFilterValue.gxTpr_Name, "TFFUNCAOAPFATRIBUTOS_ATRTABELANOM_SEL") == 0 )
            {
               AV48TFFuncaoAPFAtributos_AtrTabelaNom_Sel = AV27GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV48TFFuncaoAPFAtributos_AtrTabelaNom_Sel", AV48TFFuncaoAPFAtributos_AtrTabelaNom_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV48TFFuncaoAPFAtributos_AtrTabelaNom_Sel)) )
               {
                  Ddo_funcaoapfatributos_atrtabelanom_Selectedvalue_set = AV48TFFuncaoAPFAtributos_AtrTabelaNom_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaoapfatributos_atrtabelanom_Internalname, "SelectedValue_set", Ddo_funcaoapfatributos_atrtabelanom_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV27GridStateFilterValue.gxTpr_Name, "TFFUNCAOAPFATRIBUTOS_FUNCAODADOSNOM") == 0 )
            {
               AV51TFFuncaoAPFAtributos_FuncaoDadosNom = AV27GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV51TFFuncaoAPFAtributos_FuncaoDadosNom", AV51TFFuncaoAPFAtributos_FuncaoDadosNom);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV51TFFuncaoAPFAtributos_FuncaoDadosNom)) )
               {
                  Ddo_funcaoapfatributos_funcaodadosnom_Filteredtext_set = AV51TFFuncaoAPFAtributos_FuncaoDadosNom;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaoapfatributos_funcaodadosnom_Internalname, "FilteredText_set", Ddo_funcaoapfatributos_funcaodadosnom_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV27GridStateFilterValue.gxTpr_Name, "TFFUNCAOAPFATRIBUTOS_FUNCAODADOSNOM_SEL") == 0 )
            {
               AV52TFFuncaoAPFAtributos_FuncaoDadosNom_Sel = AV27GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV52TFFuncaoAPFAtributos_FuncaoDadosNom_Sel", AV52TFFuncaoAPFAtributos_FuncaoDadosNom_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV52TFFuncaoAPFAtributos_FuncaoDadosNom_Sel)) )
               {
                  Ddo_funcaoapfatributos_funcaodadosnom_Selectedvalue_set = AV52TFFuncaoAPFAtributos_FuncaoDadosNom_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_funcaoapfatributos_funcaodadosnom_Internalname, "SelectedValue_set", Ddo_funcaoapfatributos_funcaodadosnom_Selectedvalue_set);
               }
            }
            AV63GXV1 = (int)(AV63GXV1+1);
         }
      }

      protected void S212( )
      {
         /* 'LOADDYNFILTERSSTATE' Routine */
         imgAdddynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         if ( AV25GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV26GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV25GridState.gxTpr_Dynamicfilters.Item(1));
            AV16DynamicFiltersSelector1 = AV26GridStateDynamicFilter.gxTpr_Selected;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
            if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM") == 0 )
            {
               AV21FuncaoAPFAtributos_AtributosNom1 = AV26GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21FuncaoAPFAtributos_AtributosNom1", AV21FuncaoAPFAtributos_AtributosNom1);
            }
            else if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "FUNCAOAPFATRIBUTOS_ATRTABELANOM") == 0 )
            {
               AV23FuncaoAPFAtributos_AtrTabelaNom1 = AV26GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23FuncaoAPFAtributos_AtrTabelaNom1", AV23FuncaoAPFAtributos_AtrTabelaNom1);
            }
            /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            if ( AV25GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               lblJsdynamicfilters_Caption = "<script type=\"text/javascript\">";
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(2);";
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               imgAdddynamicfilters1_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
               imgRemovedynamicfilters1_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
               AV9DynamicFiltersEnabled2 = true;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV9DynamicFiltersEnabled2", AV9DynamicFiltersEnabled2);
               AV26GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV25GridState.gxTpr_Dynamicfilters.Item(2));
               AV17DynamicFiltersSelector2 = AV26GridStateDynamicFilter.gxTpr_Selected;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17DynamicFiltersSelector2", AV17DynamicFiltersSelector2);
               if ( StringUtil.StrCmp(AV17DynamicFiltersSelector2, "FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM") == 0 )
               {
                  AV22FuncaoAPFAtributos_AtributosNom2 = AV26GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV22FuncaoAPFAtributos_AtributosNom2", AV22FuncaoAPFAtributos_AtributosNom2);
               }
               else if ( StringUtil.StrCmp(AV17DynamicFiltersSelector2, "FUNCAOAPFATRIBUTOS_ATRTABELANOM") == 0 )
               {
                  AV24FuncaoAPFAtributos_AtrTabelaNom2 = AV26GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24FuncaoAPFAtributos_AtrTabelaNom2", AV24FuncaoAPFAtributos_AtrTabelaNom2);
               }
               /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
               S122 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"</script>";
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
            }
         }
         if ( AV15DynamicFiltersRemoving )
         {
            lblJsdynamicfilters_Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         }
      }

      protected void S162( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV25GridState.FromXml(AV32Session.Get(AV62Pgmname+"GridState"), "");
         AV25GridState.gxTpr_Orderedby = AV30OrderedBy;
         AV25GridState.gxTpr_Ordereddsc = AV31OrderedDsc;
         AV25GridState.gxTpr_Filtervalues.Clear();
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV43TFFuncaoAPFAtributos_AtributosNom)) )
         {
            AV27GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV27GridStateFilterValue.gxTpr_Name = "TFFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM";
            AV27GridStateFilterValue.gxTpr_Value = AV43TFFuncaoAPFAtributos_AtributosNom;
            AV25GridState.gxTpr_Filtervalues.Add(AV27GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV44TFFuncaoAPFAtributos_AtributosNom_Sel)) )
         {
            AV27GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV27GridStateFilterValue.gxTpr_Name = "TFFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM_SEL";
            AV27GridStateFilterValue.gxTpr_Value = AV44TFFuncaoAPFAtributos_AtributosNom_Sel;
            AV25GridState.gxTpr_Filtervalues.Add(AV27GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV47TFFuncaoAPFAtributos_AtrTabelaNom)) )
         {
            AV27GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV27GridStateFilterValue.gxTpr_Name = "TFFUNCAOAPFATRIBUTOS_ATRTABELANOM";
            AV27GridStateFilterValue.gxTpr_Value = AV47TFFuncaoAPFAtributos_AtrTabelaNom;
            AV25GridState.gxTpr_Filtervalues.Add(AV27GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV48TFFuncaoAPFAtributos_AtrTabelaNom_Sel)) )
         {
            AV27GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV27GridStateFilterValue.gxTpr_Name = "TFFUNCAOAPFATRIBUTOS_ATRTABELANOM_SEL";
            AV27GridStateFilterValue.gxTpr_Value = AV48TFFuncaoAPFAtributos_AtrTabelaNom_Sel;
            AV25GridState.gxTpr_Filtervalues.Add(AV27GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV51TFFuncaoAPFAtributos_FuncaoDadosNom)) )
         {
            AV27GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV27GridStateFilterValue.gxTpr_Name = "TFFUNCAOAPFATRIBUTOS_FUNCAODADOSNOM";
            AV27GridStateFilterValue.gxTpr_Value = AV51TFFuncaoAPFAtributos_FuncaoDadosNom;
            AV25GridState.gxTpr_Filtervalues.Add(AV27GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV52TFFuncaoAPFAtributos_FuncaoDadosNom_Sel)) )
         {
            AV27GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV27GridStateFilterValue.gxTpr_Name = "TFFUNCAOAPFATRIBUTOS_FUNCAODADOSNOM_SEL";
            AV27GridStateFilterValue.gxTpr_Value = AV52TFFuncaoAPFAtributos_FuncaoDadosNom_Sel;
            AV25GridState.gxTpr_Filtervalues.Add(AV27GridStateFilterValue, 0);
         }
         if ( ! (0==AV19FuncaoAPF_Codigo) )
         {
            AV27GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV27GridStateFilterValue.gxTpr_Name = "PARM_&FUNCAOAPF_CODIGO";
            AV27GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV19FuncaoAPF_Codigo), 6, 0);
            AV25GridState.gxTpr_Filtervalues.Add(AV27GridStateFilterValue, 0);
         }
         if ( ! (0==AV20FuncaoAPF_SistemaCod) )
         {
            AV27GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV27GridStateFilterValue.gxTpr_Name = "PARM_&FUNCAOAPF_SISTEMACOD";
            AV27GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV20FuncaoAPF_SistemaCod), 6, 0);
            AV25GridState.gxTpr_Filtervalues.Add(AV27GridStateFilterValue, 0);
         }
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         new wwpbaseobjects.savegridstate(context ).execute(  AV62Pgmname+"GridState",  AV25GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_MeetrikaVs3")) ;
      }

      protected void S192( )
      {
         /* 'SAVEDYNFILTERSSTATE' Routine */
         AV25GridState.gxTpr_Dynamicfilters.Clear();
         if ( ! AV11DynamicFiltersIgnoreFirst )
         {
            AV26GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV26GridStateDynamicFilter.gxTpr_Selected = AV16DynamicFiltersSelector1;
            if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV21FuncaoAPFAtributos_AtributosNom1)) )
            {
               AV26GridStateDynamicFilter.gxTpr_Value = AV21FuncaoAPFAtributos_AtributosNom1;
            }
            else if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "FUNCAOAPFATRIBUTOS_ATRTABELANOM") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV23FuncaoAPFAtributos_AtrTabelaNom1)) )
            {
               AV26GridStateDynamicFilter.gxTpr_Value = AV23FuncaoAPFAtributos_AtrTabelaNom1;
            }
            if ( AV15DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV26GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV25GridState.gxTpr_Dynamicfilters.Add(AV26GridStateDynamicFilter, 0);
            }
         }
         if ( AV9DynamicFiltersEnabled2 )
         {
            AV26GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV26GridStateDynamicFilter.gxTpr_Selected = AV17DynamicFiltersSelector2;
            if ( ( StringUtil.StrCmp(AV17DynamicFiltersSelector2, "FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV22FuncaoAPFAtributos_AtributosNom2)) )
            {
               AV26GridStateDynamicFilter.gxTpr_Value = AV22FuncaoAPFAtributos_AtributosNom2;
            }
            else if ( ( StringUtil.StrCmp(AV17DynamicFiltersSelector2, "FUNCAOAPFATRIBUTOS_ATRTABELANOM") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV24FuncaoAPFAtributos_AtrTabelaNom2)) )
            {
               AV26GridStateDynamicFilter.gxTpr_Value = AV24FuncaoAPFAtributos_AtrTabelaNom2;
            }
            if ( AV15DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV26GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV25GridState.gxTpr_Dynamicfilters.Add(AV26GridStateDynamicFilter, 0);
            }
         }
      }

      protected void S132( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV33TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV33TrnContext.gxTpr_Callerobject = AV62Pgmname;
         AV33TrnContext.gxTpr_Callerondelete = true;
         AV33TrnContext.gxTpr_Callerurl = AV28HTTPRequest.ScriptName+"?"+AV28HTTPRequest.QueryString;
         AV33TrnContext.gxTpr_Transactionname = "FuncoesAPFAtributos";
         AV34TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV34TrnContextAtt.gxTpr_Attributename = "FuncaoAPF_Codigo";
         AV34TrnContextAtt.gxTpr_Attributevalue = StringUtil.Str( (decimal)(AV19FuncaoAPF_Codigo), 6, 0);
         AV33TrnContext.gxTpr_Attributes.Add(AV34TrnContextAtt, 0);
         AV32Session.Set("TrnContext", AV33TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_MeetrikaVs3"));
      }

      protected void S182( )
      {
         /* 'FUNCAODADOS_TIPO' Routine */
         /* Using cursor H00984 */
         pr_default.execute(2, new Object[] {AV38FuncaoAPFAtributos_FuncaoDadosCod});
         while ( (pr_default.getStatus(2) != 101) )
         {
            A391FuncaoDados_FuncaoDadosCod = H00984_A391FuncaoDados_FuncaoDadosCod[0];
            n391FuncaoDados_FuncaoDadosCod = H00984_n391FuncaoDados_FuncaoDadosCod[0];
            A373FuncaoDados_Tipo = H00984_A373FuncaoDados_Tipo[0];
            AV37FuncaoAPFAtributos_FuncaoDadosTip = A373FuncaoDados_Tipo;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, cmbavFuncaoapfatributos_funcaodadostip_Internalname, AV37FuncaoAPFAtributos_FuncaoDadosTip);
            pr_default.readNext(2);
         }
         pr_default.close(2);
      }

      protected void wb_table1_2_982( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, sPrefix, "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='TableSearchCell'>") ;
            wb_table2_8_982( true) ;
         }
         else
         {
            wb_table2_8_982( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_982e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_982e( true) ;
         }
         else
         {
            wb_table1_2_982e( false) ;
         }
      }

      protected void wb_table2_8_982( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablesearch_Internalname, tblTablesearch_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_11_982( true) ;
         }
         else
         {
            wb_table3_11_982( false) ;
         }
         return  ;
      }

      protected void wb_table3_11_982e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='OrderedByDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblOrderedtext_Internalname, "Ordenado por", "", "", lblOrderedtext_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "OrderedByDescription", 0, "", 1, 1, 0, "HLP_FuncaoAPFFuncoesAPFAtributosWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'" + sPrefix + "',false,'" + sGXsfl_57_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavOrderedby, cmbavOrderedby_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV30OrderedBy), 4, 0)), 1, cmbavOrderedby_Jsonclick, 5, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"EVORDEREDBY.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,18);\"", "", true, "HLP_FuncaoAPFFuncoesAPFAtributosWC.htm");
            cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV30OrderedBy), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavOrderedby_Internalname, "Values", (String)(cmbavOrderedby.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 19,'" + sPrefix + "',false,'" + sGXsfl_57_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV31OrderedDsc), StringUtil.BoolToStr( AV31OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,19);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_FuncaoAPFFuncoesAPFAtributosWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableFiltersCell'>") ;
            wb_table4_21_982( true) ;
         }
         else
         {
            wb_table4_21_982( false) ;
         }
         return  ;
      }

      protected void wb_table4_21_982e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table5_54_982( true) ;
         }
         else
         {
            wb_table5_54_982( false) ;
         }
         return  ;
      }

      protected void wb_table5_54_982e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_982e( true) ;
         }
         else
         {
            wb_table2_8_982e( false) ;
         }
      }

      protected void wb_table5_54_982( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"DivS\" data-gxgridid=\"57\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Fun��o de Transa��o") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Atributo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(200), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtFuncaoAPFAtributos_AtributosNom_Titleformat == 0 )
               {
                  context.SendWebValue( edtFuncaoAPFAtributos_AtributosNom_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtFuncaoAPFAtributos_AtributosNom_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(200), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtFuncaoAPFAtributos_AtrTabelaNom_Titleformat == 0 )
               {
                  context.SendWebValue( edtFuncaoAPFAtributos_AtrTabelaNom_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtFuncaoAPFAtributos_AtrTabelaNom_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(450), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtFuncaoAPFAtributos_FuncaoDadosNom_Titleformat == 0 )
               {
                  context.SendWebValue( edtFuncaoAPFAtributos_FuncaoDadosNom_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtFuncaoAPFAtributos_FuncaoDadosNom_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(80), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Tipo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", sPrefix);
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV39DeleteAtributo));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDeleteatributo_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A165FuncaoAPF_Codigo), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A364FuncaoAPFAtributos_AtributosCod), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A365FuncaoAPFAtributos_AtributosNom));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtFuncaoAPFAtributos_AtributosNom_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtFuncaoAPFAtributos_AtributosNom_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A367FuncaoAPFAtributos_AtrTabelaNom));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtFuncaoAPFAtributos_AtrTabelaNom_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtFuncaoAPFAtributos_AtrTabelaNom_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A412FuncaoAPFAtributos_FuncaoDadosNom);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtFuncaoAPFAtributos_FuncaoDadosNom_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtFuncaoAPFAtributos_FuncaoDadosNom_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( AV37FuncaoAPFAtributos_FuncaoDadosTip));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbavFuncaoapfatributos_funcaodadostip.Enabled), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 57 )
         {
            wbEnd = 0;
            nRC_GXsfl_57 = (short)(nGXsfl_57_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid(sPrefix+"_"+"Grid", GridContainer);
               if ( ! isAjaxCallMode( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+sPrefix+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_54_982e( true) ;
         }
         else
         {
            wb_table5_54_982e( false) ;
         }
      }

      protected void wb_table4_21_982( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 24,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgCleanfilters_Internalname, context.GetImagePath( "effedd9a-25cf-4497-952f-5a5ac032b8be", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Limpar filtros", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgCleanfilters_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOCLEANFILTERS\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_FuncaoAPFFuncoesAPFAtributosWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataFilterContentCell'>") ;
            wb_table6_26_982( true) ;
         }
         else
         {
            wb_table6_26_982( false) ;
         }
         return  ;
      }

      protected void wb_table6_26_982e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblJsdynamicfilters_Internalname, lblJsdynamicfilters_Caption, "", "", lblJsdynamicfilters_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", 0, "", 1, 1, 1, "HLP_FuncaoAPFFuncoesAPFAtributosWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_21_982e( true) ;
         }
         else
         {
            wb_table4_21_982e( false) ;
         }
      }

      protected void wb_table6_26_982( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabledynamicfilters_Internalname, tblTabledynamicfilters_Internalname, "", "TableDynamicFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix1_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix1_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_FuncaoAPFFuncoesAPFAtributosWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 31,'" + sPrefix + "',false,'" + sGXsfl_57_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector1, cmbavDynamicfiltersselector1_Internalname, StringUtil.RTrim( AV16DynamicFiltersSelector1), 1, cmbavDynamicfiltersselector1_Jsonclick, 5, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"EVDYNAMICFILTERSSELECTOR1.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,31);\"", "", true, "HLP_FuncaoAPFFuncoesAPFAtributosWC.htm");
            cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV16DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector1_Internalname, "Values", (String)(cmbavDynamicfiltersselector1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle1_Internalname, "valor", "", "", lblDynamicfiltersmiddle1_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_FuncaoAPFFuncoesAPFAtributosWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 35,'" + sPrefix + "',false,'" + sGXsfl_57_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavFuncaoapfatributos_atributosnom1_Internalname, StringUtil.RTrim( AV21FuncaoAPFAtributos_AtributosNom1), StringUtil.RTrim( context.localUtil.Format( AV21FuncaoAPFAtributos_AtributosNom1, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,35);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavFuncaoapfatributos_atributosnom1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavFuncaoapfatributos_atributosnom1_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_FuncaoAPFFuncoesAPFAtributosWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 36,'" + sPrefix + "',false,'" + sGXsfl_57_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavFuncaoapfatributos_atrtabelanom1_Internalname, StringUtil.RTrim( AV23FuncaoAPFAtributos_AtrTabelaNom1), StringUtil.RTrim( context.localUtil.Format( AV23FuncaoAPFAtributos_AtrTabelaNom1, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,36);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavFuncaoapfatributos_atrtabelanom1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavFuncaoapfatributos_atrtabelanom1_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_FuncaoAPFFuncoesAPFAtributosWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 38,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters1_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters1_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters1_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'ADDDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_FuncaoAPFFuncoesAPFAtributosWC.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 39,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters1_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters1_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters1_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'REMOVEDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_FuncaoAPFFuncoesAPFAtributosWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow2\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix2_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix2_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_FuncaoAPFFuncoesAPFAtributosWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 44,'" + sPrefix + "',false,'" + sGXsfl_57_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector2, cmbavDynamicfiltersselector2_Internalname, StringUtil.RTrim( AV17DynamicFiltersSelector2), 1, cmbavDynamicfiltersselector2_Jsonclick, 5, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"EVDYNAMICFILTERSSELECTOR2.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,44);\"", "", true, "HLP_FuncaoAPFFuncoesAPFAtributosWC.htm");
            cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV17DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector2_Internalname, "Values", (String)(cmbavDynamicfiltersselector2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle2_Internalname, "valor", "", "", lblDynamicfiltersmiddle2_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_FuncaoAPFFuncoesAPFAtributosWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 48,'" + sPrefix + "',false,'" + sGXsfl_57_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavFuncaoapfatributos_atributosnom2_Internalname, StringUtil.RTrim( AV22FuncaoAPFAtributos_AtributosNom2), StringUtil.RTrim( context.localUtil.Format( AV22FuncaoAPFAtributos_AtributosNom2, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,48);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavFuncaoapfatributos_atributosnom2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavFuncaoapfatributos_atributosnom2_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_FuncaoAPFFuncoesAPFAtributosWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 49,'" + sPrefix + "',false,'" + sGXsfl_57_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavFuncaoapfatributos_atrtabelanom2_Internalname, StringUtil.RTrim( AV24FuncaoAPFAtributos_AtrTabelaNom2), StringUtil.RTrim( context.localUtil.Format( AV24FuncaoAPFAtributos_AtrTabelaNom2, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,49);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavFuncaoapfatributos_atrtabelanom2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavFuncaoapfatributos_atrtabelanom2_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_FuncaoAPFFuncoesAPFAtributosWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 51,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters2_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters2_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'REMOVEDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_FuncaoAPFFuncoesAPFAtributosWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_26_982e( true) ;
         }
         else
         {
            wb_table6_26_982e( false) ;
         }
      }

      protected void wb_table3_11_982( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "Table", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 14,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgInsertatributo_Internalname, context.GetImagePath( "5649fbb8-8ce0-4810-a5ce-bd649ea83c3a", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgInsertatributo_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOINSERTATRIBUTO\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_FuncaoAPFFuncoesAPFAtributosWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_11_982e( true) ;
         }
         else
         {
            wb_table3_11_982e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV19FuncaoAPF_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV19FuncaoAPF_Codigo), 6, 0)));
         AV20FuncaoAPF_SistemaCod = Convert.ToInt32(getParm(obj,1));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20FuncaoAPF_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20FuncaoAPF_SistemaCod), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA982( ) ;
         WS982( ) ;
         WE982( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlAV19FuncaoAPF_Codigo = (String)((String)getParm(obj,0));
         sCtrlAV20FuncaoAPF_SistemaCod = (String)((String)getParm(obj,1));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PA982( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "funcaoapffuncoesapfatributoswc");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PA982( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            AV19FuncaoAPF_Codigo = Convert.ToInt32(getParm(obj,2));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV19FuncaoAPF_Codigo), 6, 0)));
            AV20FuncaoAPF_SistemaCod = Convert.ToInt32(getParm(obj,3));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20FuncaoAPF_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20FuncaoAPF_SistemaCod), 6, 0)));
         }
         wcpOAV19FuncaoAPF_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV19FuncaoAPF_Codigo"), ",", "."));
         wcpOAV20FuncaoAPF_SistemaCod = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV20FuncaoAPF_SistemaCod"), ",", "."));
         if ( ! GetJustCreated( ) && ( ( AV19FuncaoAPF_Codigo != wcpOAV19FuncaoAPF_Codigo ) || ( AV20FuncaoAPF_SistemaCod != wcpOAV20FuncaoAPF_SistemaCod ) ) )
         {
            setjustcreated();
         }
         wcpOAV19FuncaoAPF_Codigo = AV19FuncaoAPF_Codigo;
         wcpOAV20FuncaoAPF_SistemaCod = AV20FuncaoAPF_SistemaCod;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlAV19FuncaoAPF_Codigo = cgiGet( sPrefix+"AV19FuncaoAPF_Codigo_CTRL");
         if ( StringUtil.Len( sCtrlAV19FuncaoAPF_Codigo) > 0 )
         {
            AV19FuncaoAPF_Codigo = (int)(context.localUtil.CToN( cgiGet( sCtrlAV19FuncaoAPF_Codigo), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19FuncaoAPF_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV19FuncaoAPF_Codigo), 6, 0)));
         }
         else
         {
            AV19FuncaoAPF_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"AV19FuncaoAPF_Codigo_PARM"), ",", "."));
         }
         sCtrlAV20FuncaoAPF_SistemaCod = cgiGet( sPrefix+"AV20FuncaoAPF_SistemaCod_CTRL");
         if ( StringUtil.Len( sCtrlAV20FuncaoAPF_SistemaCod) > 0 )
         {
            AV20FuncaoAPF_SistemaCod = (int)(context.localUtil.CToN( cgiGet( sCtrlAV20FuncaoAPF_SistemaCod), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20FuncaoAPF_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20FuncaoAPF_SistemaCod), 6, 0)));
         }
         else
         {
            AV20FuncaoAPF_SistemaCod = (int)(context.localUtil.CToN( cgiGet( sPrefix+"AV20FuncaoAPF_SistemaCod_PARM"), ",", "."));
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PA982( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WS982( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WS982( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"AV19FuncaoAPF_Codigo_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV19FuncaoAPF_Codigo), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlAV19FuncaoAPF_Codigo)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"AV19FuncaoAPF_Codigo_CTRL", StringUtil.RTrim( sCtrlAV19FuncaoAPF_Codigo));
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"AV20FuncaoAPF_SistemaCod_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV20FuncaoAPF_SistemaCod), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlAV20FuncaoAPF_SistemaCod)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"AV20FuncaoAPF_SistemaCod_CTRL", StringUtil.RTrim( sCtrlAV20FuncaoAPF_SistemaCod));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WE982( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?2249787");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20204282250425");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("funcaoapffuncoesapfatributoswc.js", "?20204282250425");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_572( )
      {
         edtavDeleteatributo_Internalname = sPrefix+"vDELETEATRIBUTO_"+sGXsfl_57_idx;
         edtFuncaoAPF_Codigo_Internalname = sPrefix+"FUNCAOAPF_CODIGO_"+sGXsfl_57_idx;
         edtFuncaoAPFAtributos_AtributosCod_Internalname = sPrefix+"FUNCAOAPFATRIBUTOS_ATRIBUTOSCOD_"+sGXsfl_57_idx;
         edtFuncaoAPFAtributos_AtributosNom_Internalname = sPrefix+"FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM_"+sGXsfl_57_idx;
         edtFuncaoAPFAtributos_AtrTabelaNom_Internalname = sPrefix+"FUNCAOAPFATRIBUTOS_ATRTABELANOM_"+sGXsfl_57_idx;
         edtFuncaoAPFAtributos_FuncaoDadosNom_Internalname = sPrefix+"FUNCAOAPFATRIBUTOS_FUNCAODADOSNOM_"+sGXsfl_57_idx;
         cmbavFuncaoapfatributos_funcaodadostip_Internalname = sPrefix+"vFUNCAOAPFATRIBUTOS_FUNCAODADOSTIP_"+sGXsfl_57_idx;
      }

      protected void SubsflControlProps_fel_572( )
      {
         edtavDeleteatributo_Internalname = sPrefix+"vDELETEATRIBUTO_"+sGXsfl_57_fel_idx;
         edtFuncaoAPF_Codigo_Internalname = sPrefix+"FUNCAOAPF_CODIGO_"+sGXsfl_57_fel_idx;
         edtFuncaoAPFAtributos_AtributosCod_Internalname = sPrefix+"FUNCAOAPFATRIBUTOS_ATRIBUTOSCOD_"+sGXsfl_57_fel_idx;
         edtFuncaoAPFAtributos_AtributosNom_Internalname = sPrefix+"FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM_"+sGXsfl_57_fel_idx;
         edtFuncaoAPFAtributos_AtrTabelaNom_Internalname = sPrefix+"FUNCAOAPFATRIBUTOS_ATRTABELANOM_"+sGXsfl_57_fel_idx;
         edtFuncaoAPFAtributos_FuncaoDadosNom_Internalname = sPrefix+"FUNCAOAPFATRIBUTOS_FUNCAODADOSNOM_"+sGXsfl_57_fel_idx;
         cmbavFuncaoapfatributos_funcaodadostip_Internalname = sPrefix+"vFUNCAOAPFATRIBUTOS_FUNCAODADOSTIP_"+sGXsfl_57_fel_idx;
      }

      protected void sendrow_572( )
      {
         SubsflControlProps_572( ) ;
         WB980( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_57_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_57_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_57_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Active Bitmap Variable */
            TempTags = " " + ((edtavDeleteatributo_Enabled!=0)&&(edtavDeleteatributo_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 58,'"+sPrefix+"',false,'',57)\"" : " ");
            ClassString = "Image";
            StyleString = "";
            AV39DeleteAtributo_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV39DeleteAtributo))&&String.IsNullOrEmpty(StringUtil.RTrim( AV61Deleteatributo_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV39DeleteAtributo)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDeleteatributo_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV39DeleteAtributo)) ? AV61Deleteatributo_GXI : context.PathToRelativeUrl( AV39DeleteAtributo)),(String)"",(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavDeleteatributo_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)5,(String)edtavDeleteatributo_Jsonclick,"'"+sPrefix+"'"+",false,"+"'"+sPrefix+"EVDELETEATRIBUTO.CLICK."+sGXsfl_57_idx+"'",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)""+TempTags,(String)"",(String)"",(short)1,(bool)AV39DeleteAtributo_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtFuncaoAPF_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A165FuncaoAPF_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A165FuncaoAPF_Codigo), "ZZZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtFuncaoAPF_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)57,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtFuncaoAPFAtributos_AtributosCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A364FuncaoAPFAtributos_AtributosCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A364FuncaoAPFAtributos_AtributosCod), "ZZZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtFuncaoAPFAtributos_AtributosCod_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)57,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtFuncaoAPFAtributos_AtributosNom_Internalname,StringUtil.RTrim( A365FuncaoAPFAtributos_AtributosNom),StringUtil.RTrim( context.localUtil.Format( A365FuncaoAPFAtributos_AtributosNom, "@!")),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtFuncaoAPFAtributos_AtributosNom_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)200,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)57,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtFuncaoAPFAtributos_AtrTabelaNom_Internalname,StringUtil.RTrim( A367FuncaoAPFAtributos_AtrTabelaNom),StringUtil.RTrim( context.localUtil.Format( A367FuncaoAPFAtributos_AtrTabelaNom, "@!")),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtFuncaoAPFAtributos_AtrTabelaNom_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)200,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)57,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtFuncaoAPFAtributos_FuncaoDadosNom_Internalname,(String)A412FuncaoAPFAtributos_FuncaoDadosNom,(String)"",(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtFuncaoAPFAtributos_FuncaoDadosNom_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)450,(String)"px",(short)17,(String)"px",(short)200,(short)0,(short)0,(short)57,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            TempTags = " " + ((cmbavFuncaoapfatributos_funcaodadostip.Enabled!=0)&&(cmbavFuncaoapfatributos_funcaodadostip.Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 64,'"+sPrefix+"',false,'"+sGXsfl_57_idx+"',57)\"" : " ");
            if ( ( nGXsfl_57_idx == 1 ) && isAjaxCallMode( ) )
            {
               GXCCtl = "vFUNCAOAPFATRIBUTOS_FUNCAODADOSTIP_" + sGXsfl_57_idx;
               cmbavFuncaoapfatributos_funcaodadostip.Name = GXCCtl;
               cmbavFuncaoapfatributos_funcaodadostip.WebTags = "";
               cmbavFuncaoapfatributos_funcaodadostip.addItem("", "(Nenhum)", 0);
               cmbavFuncaoapfatributos_funcaodadostip.addItem("ALI", "ALI", 0);
               cmbavFuncaoapfatributos_funcaodadostip.addItem("AIE", "AIE", 0);
               cmbavFuncaoapfatributos_funcaodadostip.addItem("DC", "DC", 0);
               if ( cmbavFuncaoapfatributos_funcaodadostip.ItemCount > 0 )
               {
                  AV37FuncaoAPFAtributos_FuncaoDadosTip = cmbavFuncaoapfatributos_funcaodadostip.getValidValue(AV37FuncaoAPFAtributos_FuncaoDadosTip);
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, cmbavFuncaoapfatributos_funcaodadostip_Internalname, AV37FuncaoAPFAtributos_FuncaoDadosTip);
               }
            }
            /* ComboBox */
            GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbavFuncaoapfatributos_funcaodadostip,(String)cmbavFuncaoapfatributos_funcaodadostip_Internalname,StringUtil.RTrim( AV37FuncaoAPFAtributos_FuncaoDadosTip),(short)1,(String)cmbavFuncaoapfatributos_funcaodadostip_Jsonclick,(short)0,(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"char",(String)"",(short)-1,cmbavFuncaoapfatributos_funcaodadostip.Enabled,(short)0,(short)0,(short)80,(String)"px",(short)0,(String)"px",(String)"",(String)"BootstrapAttribute",(String)"",TempTags+((cmbavFuncaoapfatributos_funcaodadostip.Enabled!=0)&&(cmbavFuncaoapfatributos_funcaodadostip.Visible!=0) ? " onchange=\"gx.evt.onchange(this)\" " : " ")+((cmbavFuncaoapfatributos_funcaodadostip.Enabled!=0)&&(cmbavFuncaoapfatributos_funcaodadostip.Visible!=0) ? " onblur=\""+""+";gx.evt.onblur(this,64);\"" : " "),(String)"",(bool)true});
            cmbavFuncaoapfatributos_funcaodadostip.CurrentValue = StringUtil.RTrim( AV37FuncaoAPFAtributos_FuncaoDadosTip);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavFuncaoapfatributos_funcaodadostip_Internalname, "Values", (String)(cmbavFuncaoapfatributos_funcaodadostip.ToJavascriptSource()));
            GridContainer.AddRow(GridRow);
            nGXsfl_57_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_57_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_57_idx+1));
            sGXsfl_57_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_57_idx), 4, 0)), 4, "0");
            SubsflControlProps_572( ) ;
         }
         /* End function sendrow_572 */
      }

      protected void init_default_properties( )
      {
         imgInsertatributo_Internalname = sPrefix+"INSERTATRIBUTO";
         tblTableactions_Internalname = sPrefix+"TABLEACTIONS";
         lblOrderedtext_Internalname = sPrefix+"ORDEREDTEXT";
         cmbavOrderedby_Internalname = sPrefix+"vORDEREDBY";
         edtavOrdereddsc_Internalname = sPrefix+"vORDEREDDSC";
         imgCleanfilters_Internalname = sPrefix+"CLEANFILTERS";
         lblDynamicfiltersprefix1_Internalname = sPrefix+"DYNAMICFILTERSPREFIX1";
         cmbavDynamicfiltersselector1_Internalname = sPrefix+"vDYNAMICFILTERSSELECTOR1";
         lblDynamicfiltersmiddle1_Internalname = sPrefix+"DYNAMICFILTERSMIDDLE1";
         edtavFuncaoapfatributos_atributosnom1_Internalname = sPrefix+"vFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM1";
         edtavFuncaoapfatributos_atrtabelanom1_Internalname = sPrefix+"vFUNCAOAPFATRIBUTOS_ATRTABELANOM1";
         imgAdddynamicfilters1_Internalname = sPrefix+"ADDDYNAMICFILTERS1";
         imgRemovedynamicfilters1_Internalname = sPrefix+"REMOVEDYNAMICFILTERS1";
         lblDynamicfiltersprefix2_Internalname = sPrefix+"DYNAMICFILTERSPREFIX2";
         cmbavDynamicfiltersselector2_Internalname = sPrefix+"vDYNAMICFILTERSSELECTOR2";
         lblDynamicfiltersmiddle2_Internalname = sPrefix+"DYNAMICFILTERSMIDDLE2";
         edtavFuncaoapfatributos_atributosnom2_Internalname = sPrefix+"vFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM2";
         edtavFuncaoapfatributos_atrtabelanom2_Internalname = sPrefix+"vFUNCAOAPFATRIBUTOS_ATRTABELANOM2";
         imgRemovedynamicfilters2_Internalname = sPrefix+"REMOVEDYNAMICFILTERS2";
         tblTabledynamicfilters_Internalname = sPrefix+"TABLEDYNAMICFILTERS";
         lblJsdynamicfilters_Internalname = sPrefix+"JSDYNAMICFILTERS";
         tblTablefilters_Internalname = sPrefix+"TABLEFILTERS";
         edtavDeleteatributo_Internalname = sPrefix+"vDELETEATRIBUTO";
         edtFuncaoAPF_Codigo_Internalname = sPrefix+"FUNCAOAPF_CODIGO";
         edtFuncaoAPFAtributos_AtributosCod_Internalname = sPrefix+"FUNCAOAPFATRIBUTOS_ATRIBUTOSCOD";
         edtFuncaoAPFAtributos_AtributosNom_Internalname = sPrefix+"FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM";
         edtFuncaoAPFAtributos_AtrTabelaNom_Internalname = sPrefix+"FUNCAOAPFATRIBUTOS_ATRTABELANOM";
         edtFuncaoAPFAtributos_FuncaoDadosNom_Internalname = sPrefix+"FUNCAOAPFATRIBUTOS_FUNCAODADOSNOM";
         cmbavFuncaoapfatributos_funcaodadostip_Internalname = sPrefix+"vFUNCAOAPFATRIBUTOS_FUNCAODADOSTIP";
         Gridpaginationbar_Internalname = sPrefix+"GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = sPrefix+"GRIDTABLEWITHPAGINATIONBAR";
         tblTablesearch_Internalname = sPrefix+"TABLESEARCH";
         tblTablegridheader_Internalname = sPrefix+"TABLEGRIDHEADER";
         Workwithplusutilities1_Internalname = sPrefix+"WORKWITHPLUSUTILITIES1";
         chkavDynamicfiltersenabled2_Internalname = sPrefix+"vDYNAMICFILTERSENABLED2";
         edtavTffuncaoapfatributos_atributosnom_Internalname = sPrefix+"vTFFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM";
         edtavTffuncaoapfatributos_atributosnom_sel_Internalname = sPrefix+"vTFFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM_SEL";
         edtavTffuncaoapfatributos_atrtabelanom_Internalname = sPrefix+"vTFFUNCAOAPFATRIBUTOS_ATRTABELANOM";
         edtavTffuncaoapfatributos_atrtabelanom_sel_Internalname = sPrefix+"vTFFUNCAOAPFATRIBUTOS_ATRTABELANOM_SEL";
         edtavTffuncaoapfatributos_funcaodadosnom_Internalname = sPrefix+"vTFFUNCAOAPFATRIBUTOS_FUNCAODADOSNOM";
         edtavTffuncaoapfatributos_funcaodadosnom_sel_Internalname = sPrefix+"vTFFUNCAOAPFATRIBUTOS_FUNCAODADOSNOM_SEL";
         Ddo_funcaoapfatributos_atributosnom_Internalname = sPrefix+"DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM";
         edtavDdo_funcaoapfatributos_atributosnomtitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOMTITLECONTROLIDTOREPLACE";
         Ddo_funcaoapfatributos_atrtabelanom_Internalname = sPrefix+"DDO_FUNCAOAPFATRIBUTOS_ATRTABELANOM";
         edtavDdo_funcaoapfatributos_atrtabelanomtitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_FUNCAOAPFATRIBUTOS_ATRTABELANOMTITLECONTROLIDTOREPLACE";
         Ddo_funcaoapfatributos_funcaodadosnom_Internalname = sPrefix+"DDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSNOM";
         edtavDdo_funcaoapfatributos_funcaodadosnomtitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSNOMTITLECONTROLIDTOREPLACE";
         Form.Internalname = sPrefix+"FORM";
         subGrid_Internalname = sPrefix+"GRID";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         cmbavFuncaoapfatributos_funcaodadostip_Jsonclick = "";
         cmbavFuncaoapfatributos_funcaodadostip.Visible = -1;
         edtFuncaoAPFAtributos_FuncaoDadosNom_Jsonclick = "";
         edtFuncaoAPFAtributos_AtrTabelaNom_Jsonclick = "";
         edtFuncaoAPFAtributos_AtributosNom_Jsonclick = "";
         edtFuncaoAPFAtributos_AtributosCod_Jsonclick = "";
         edtFuncaoAPF_Codigo_Jsonclick = "";
         edtavDeleteatributo_Jsonclick = "";
         edtavDeleteatributo_Visible = -1;
         edtavDeleteatributo_Enabled = 1;
         edtavFuncaoapfatributos_atrtabelanom2_Jsonclick = "";
         edtavFuncaoapfatributos_atributosnom2_Jsonclick = "";
         cmbavDynamicfiltersselector2_Jsonclick = "";
         imgRemovedynamicfilters1_Visible = 1;
         imgAdddynamicfilters1_Visible = 1;
         edtavFuncaoapfatributos_atrtabelanom1_Jsonclick = "";
         edtavFuncaoapfatributos_atributosnom1_Jsonclick = "";
         cmbavDynamicfiltersselector1_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         cmbavFuncaoapfatributos_funcaodadostip.Enabled = 1;
         edtavDeleteatributo_Tooltiptext = "Desvincular atributo da Fun��o";
         edtFuncaoAPFAtributos_FuncaoDadosNom_Titleformat = 0;
         edtFuncaoAPFAtributos_AtrTabelaNom_Titleformat = 0;
         edtFuncaoAPFAtributos_AtributosNom_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         edtavOrdereddsc_Jsonclick = "";
         cmbavOrderedby_Jsonclick = "";
         edtavFuncaoapfatributos_atrtabelanom2_Visible = 1;
         edtavFuncaoapfatributos_atributosnom2_Visible = 1;
         edtavFuncaoapfatributos_atrtabelanom1_Visible = 1;
         edtavFuncaoapfatributos_atributosnom1_Visible = 1;
         edtFuncaoAPFAtributos_FuncaoDadosNom_Title = "Fun��o de Dados";
         edtFuncaoAPFAtributos_AtrTabelaNom_Title = "Tabela";
         edtFuncaoAPFAtributos_AtributosNom_Title = "Nome";
         edtavOrdereddsc_Visible = 1;
         lblJsdynamicfilters_Caption = "JSDynamicFilters";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         chkavDynamicfiltersenabled2.Caption = "";
         edtavDdo_funcaoapfatributos_funcaodadosnomtitlecontrolidtoreplace_Visible = 1;
         edtavDdo_funcaoapfatributos_atrtabelanomtitlecontrolidtoreplace_Visible = 1;
         edtavDdo_funcaoapfatributos_atributosnomtitlecontrolidtoreplace_Visible = 1;
         edtavTffuncaoapfatributos_funcaodadosnom_sel_Visible = 1;
         edtavTffuncaoapfatributos_funcaodadosnom_Visible = 1;
         edtavTffuncaoapfatributos_atrtabelanom_sel_Jsonclick = "";
         edtavTffuncaoapfatributos_atrtabelanom_sel_Visible = 1;
         edtavTffuncaoapfatributos_atrtabelanom_Jsonclick = "";
         edtavTffuncaoapfatributos_atrtabelanom_Visible = 1;
         edtavTffuncaoapfatributos_atributosnom_sel_Jsonclick = "";
         edtavTffuncaoapfatributos_atributosnom_sel_Visible = 1;
         edtavTffuncaoapfatributos_atributosnom_Jsonclick = "";
         edtavTffuncaoapfatributos_atributosnom_Visible = 1;
         chkavDynamicfiltersenabled2.Visible = 1;
         Ddo_funcaoapfatributos_funcaodadosnom_Searchbuttontext = "Pesquisar";
         Ddo_funcaoapfatributos_funcaodadosnom_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_funcaoapfatributos_funcaodadosnom_Rangefilterto = "At�";
         Ddo_funcaoapfatributos_funcaodadosnom_Rangefilterfrom = "Desde";
         Ddo_funcaoapfatributos_funcaodadosnom_Cleanfilter = "Limpar pesquisa";
         Ddo_funcaoapfatributos_funcaodadosnom_Loadingdata = "Carregando dados...";
         Ddo_funcaoapfatributos_funcaodadosnom_Sortdsc = "Ordenar de Z � A";
         Ddo_funcaoapfatributos_funcaodadosnom_Sortasc = "Ordenar de A � Z";
         Ddo_funcaoapfatributos_funcaodadosnom_Datalistupdateminimumcharacters = 0;
         Ddo_funcaoapfatributos_funcaodadosnom_Datalistproc = "GetFuncaoAPFFuncoesAPFAtributosWCFilterData";
         Ddo_funcaoapfatributos_funcaodadosnom_Datalistfixedvalues = "";
         Ddo_funcaoapfatributos_funcaodadosnom_Datalisttype = "Dynamic";
         Ddo_funcaoapfatributos_funcaodadosnom_Includedatalist = Convert.ToBoolean( -1);
         Ddo_funcaoapfatributos_funcaodadosnom_Filterisrange = Convert.ToBoolean( 0);
         Ddo_funcaoapfatributos_funcaodadosnom_Filtertype = "Character";
         Ddo_funcaoapfatributos_funcaodadosnom_Includefilter = Convert.ToBoolean( -1);
         Ddo_funcaoapfatributos_funcaodadosnom_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_funcaoapfatributos_funcaodadosnom_Includesortasc = Convert.ToBoolean( -1);
         Ddo_funcaoapfatributos_funcaodadosnom_Titlecontrolidtoreplace = "";
         Ddo_funcaoapfatributos_funcaodadosnom_Dropdownoptionstype = "GridTitleSettings";
         Ddo_funcaoapfatributos_funcaodadosnom_Cls = "ColumnSettings";
         Ddo_funcaoapfatributos_funcaodadosnom_Tooltip = "Op��es";
         Ddo_funcaoapfatributos_funcaodadosnom_Caption = "";
         Ddo_funcaoapfatributos_atrtabelanom_Searchbuttontext = "Pesquisar";
         Ddo_funcaoapfatributos_atrtabelanom_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_funcaoapfatributos_atrtabelanom_Rangefilterto = "At�";
         Ddo_funcaoapfatributos_atrtabelanom_Rangefilterfrom = "Desde";
         Ddo_funcaoapfatributos_atrtabelanom_Cleanfilter = "Limpar pesquisa";
         Ddo_funcaoapfatributos_atrtabelanom_Loadingdata = "Carregando dados...";
         Ddo_funcaoapfatributos_atrtabelanom_Sortdsc = "Ordenar de Z � A";
         Ddo_funcaoapfatributos_atrtabelanom_Sortasc = "Ordenar de A � Z";
         Ddo_funcaoapfatributos_atrtabelanom_Datalistupdateminimumcharacters = 0;
         Ddo_funcaoapfatributos_atrtabelanom_Datalistproc = "GetFuncaoAPFFuncoesAPFAtributosWCFilterData";
         Ddo_funcaoapfatributos_atrtabelanom_Datalistfixedvalues = "";
         Ddo_funcaoapfatributos_atrtabelanom_Datalisttype = "Dynamic";
         Ddo_funcaoapfatributos_atrtabelanom_Includedatalist = Convert.ToBoolean( -1);
         Ddo_funcaoapfatributos_atrtabelanom_Filterisrange = Convert.ToBoolean( 0);
         Ddo_funcaoapfatributos_atrtabelanom_Filtertype = "Character";
         Ddo_funcaoapfatributos_atrtabelanom_Includefilter = Convert.ToBoolean( -1);
         Ddo_funcaoapfatributos_atrtabelanom_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_funcaoapfatributos_atrtabelanom_Includesortasc = Convert.ToBoolean( -1);
         Ddo_funcaoapfatributos_atrtabelanom_Titlecontrolidtoreplace = "";
         Ddo_funcaoapfatributos_atrtabelanom_Dropdownoptionstype = "GridTitleSettings";
         Ddo_funcaoapfatributos_atrtabelanom_Cls = "ColumnSettings";
         Ddo_funcaoapfatributos_atrtabelanom_Tooltip = "Op��es";
         Ddo_funcaoapfatributos_atrtabelanom_Caption = "";
         Ddo_funcaoapfatributos_atributosnom_Searchbuttontext = "Pesquisar";
         Ddo_funcaoapfatributos_atributosnom_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_funcaoapfatributos_atributosnom_Rangefilterto = "At�";
         Ddo_funcaoapfatributos_atributosnom_Rangefilterfrom = "Desde";
         Ddo_funcaoapfatributos_atributosnom_Cleanfilter = "Limpar pesquisa";
         Ddo_funcaoapfatributos_atributosnom_Loadingdata = "Carregando dados...";
         Ddo_funcaoapfatributos_atributosnom_Sortdsc = "Ordenar de Z � A";
         Ddo_funcaoapfatributos_atributosnom_Sortasc = "Ordenar de A � Z";
         Ddo_funcaoapfatributos_atributosnom_Datalistupdateminimumcharacters = 0;
         Ddo_funcaoapfatributos_atributosnom_Datalistproc = "GetFuncaoAPFFuncoesAPFAtributosWCFilterData";
         Ddo_funcaoapfatributos_atributosnom_Datalistfixedvalues = "";
         Ddo_funcaoapfatributos_atributosnom_Datalisttype = "Dynamic";
         Ddo_funcaoapfatributos_atributosnom_Includedatalist = Convert.ToBoolean( -1);
         Ddo_funcaoapfatributos_atributosnom_Filterisrange = Convert.ToBoolean( 0);
         Ddo_funcaoapfatributos_atributosnom_Filtertype = "Character";
         Ddo_funcaoapfatributos_atributosnom_Includefilter = Convert.ToBoolean( -1);
         Ddo_funcaoapfatributos_atributosnom_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_funcaoapfatributos_atributosnom_Includesortasc = Convert.ToBoolean( -1);
         Ddo_funcaoapfatributos_atributosnom_Titlecontrolidtoreplace = "";
         Ddo_funcaoapfatributos_atributosnom_Dropdownoptionstype = "GridTitleSettings";
         Ddo_funcaoapfatributos_atributosnom_Cls = "ColumnSettings";
         Ddo_funcaoapfatributos_atributosnom_Tooltip = "Op��es";
         Ddo_funcaoapfatributos_atributosnom_Caption = "";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         subGrid_Rows = 0;
         context.GX_msglist.DisplayMode = 1;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A393FuncaoAPFAtributos_SistemaCod',fld:'FUNCAOAPFATRIBUTOS_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'A415FuncaoAPFAtributos_FuncaoDadosTip',fld:'FUNCAOAPFATRIBUTOS_FUNCAODADOSTIP',pic:'',nv:''},{av:'A378FuncaoAPFAtributos_FuncaoDadosCod',fld:'FUNCAOAPFATRIBUTOS_FUNCAODADOSCOD',pic:'ZZZZZ9',nv:0},{av:'A391FuncaoDados_FuncaoDadosCod',fld:'FUNCAODADOS_FUNCAODADOSCOD',pic:'ZZZZZ9',nv:0},{av:'AV38FuncaoAPFAtributos_FuncaoDadosCod',fld:'vFUNCAOAPFATRIBUTOS_FUNCAODADOSCOD',pic:'ZZZZZ9',nv:0},{av:'A373FuncaoDados_Tipo',fld:'FUNCAODADOS_TIPO',pic:'',nv:''},{av:'sPrefix',nv:''},{av:'AV45ddo_FuncaoAPFAtributos_AtributosNomTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_FuncaoAPFAtributos_AtrTabelaNomTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFATRIBUTOS_ATRTABELANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_FuncaoAPFAtributos_FuncaoDadosNomTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSNOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV62Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV30OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV31OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV43TFFuncaoAPFAtributos_AtributosNom',fld:'vTFFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM',pic:'@!',nv:''},{av:'AV44TFFuncaoAPFAtributos_AtributosNom_Sel',fld:'vTFFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM_SEL',pic:'@!',nv:''},{av:'AV47TFFuncaoAPFAtributos_AtrTabelaNom',fld:'vTFFUNCAOAPFATRIBUTOS_ATRTABELANOM',pic:'@!',nv:''},{av:'AV48TFFuncaoAPFAtributos_AtrTabelaNom_Sel',fld:'vTFFUNCAOAPFATRIBUTOS_ATRTABELANOM_SEL',pic:'@!',nv:''},{av:'AV51TFFuncaoAPFAtributos_FuncaoDadosNom',fld:'vTFFUNCAOAPFATRIBUTOS_FUNCAODADOSNOM',pic:'',nv:''},{av:'AV52TFFuncaoAPFAtributos_FuncaoDadosNom_Sel',fld:'vTFFUNCAOAPFATRIBUTOS_FUNCAODADOSNOM_SEL',pic:'',nv:''},{av:'AV19FuncaoAPF_Codigo',fld:'vFUNCAOAPF_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV20FuncaoAPF_SistemaCod',fld:'vFUNCAOAPF_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'AV25GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV11DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV21FuncaoAPFAtributos_AtributosNom1',fld:'vFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM1',pic:'@!',nv:''},{av:'AV23FuncaoAPFAtributos_AtrTabelaNom1',fld:'vFUNCAOAPFATRIBUTOS_ATRTABELANOM1',pic:'@!',nv:''},{av:'AV15DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV9DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV17DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22FuncaoAPFAtributos_AtributosNom2',fld:'vFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM2',pic:'@!',nv:''},{av:'AV24FuncaoAPFAtributos_AtrTabelaNom2',fld:'vFUNCAOAPFATRIBUTOS_ATRTABELANOM2',pic:'@!',nv:''}],oparms:[{av:'AV42FuncaoAPFAtributos_AtributosNomTitleFilterData',fld:'vFUNCAOAPFATRIBUTOS_ATRIBUTOSNOMTITLEFILTERDATA',pic:'',nv:null},{av:'AV46FuncaoAPFAtributos_AtrTabelaNomTitleFilterData',fld:'vFUNCAOAPFATRIBUTOS_ATRTABELANOMTITLEFILTERDATA',pic:'',nv:null},{av:'AV50FuncaoAPFAtributos_FuncaoDadosNomTitleFilterData',fld:'vFUNCAOAPFATRIBUTOS_FUNCAODADOSNOMTITLEFILTERDATA',pic:'',nv:null},{av:'edtFuncaoAPFAtributos_AtributosNom_Titleformat',ctrl:'FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM',prop:'Titleformat'},{av:'edtFuncaoAPFAtributos_AtributosNom_Title',ctrl:'FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM',prop:'Title'},{av:'edtFuncaoAPFAtributos_AtrTabelaNom_Titleformat',ctrl:'FUNCAOAPFATRIBUTOS_ATRTABELANOM',prop:'Titleformat'},{av:'edtFuncaoAPFAtributos_AtrTabelaNom_Title',ctrl:'FUNCAOAPFATRIBUTOS_ATRTABELANOM',prop:'Title'},{av:'edtFuncaoAPFAtributos_FuncaoDadosNom_Titleformat',ctrl:'FUNCAOAPFATRIBUTOS_FUNCAODADOSNOM',prop:'Titleformat'},{av:'edtFuncaoAPFAtributos_FuncaoDadosNom_Title',ctrl:'FUNCAOAPFATRIBUTOS_FUNCAODADOSNOM',prop:'Title'},{av:'AV56GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV57GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'AV25GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E11982',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV30OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV31OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV21FuncaoAPFAtributos_AtributosNom1',fld:'vFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM1',pic:'@!',nv:''},{av:'AV23FuncaoAPFAtributos_AtrTabelaNom1',fld:'vFUNCAOAPFATRIBUTOS_ATRTABELANOM1',pic:'@!',nv:''},{av:'AV17DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22FuncaoAPFAtributos_AtributosNom2',fld:'vFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM2',pic:'@!',nv:''},{av:'AV24FuncaoAPFAtributos_AtrTabelaNom2',fld:'vFUNCAOAPFATRIBUTOS_ATRTABELANOM2',pic:'@!',nv:''},{av:'AV9DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV43TFFuncaoAPFAtributos_AtributosNom',fld:'vTFFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM',pic:'@!',nv:''},{av:'AV44TFFuncaoAPFAtributos_AtributosNom_Sel',fld:'vTFFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM_SEL',pic:'@!',nv:''},{av:'AV47TFFuncaoAPFAtributos_AtrTabelaNom',fld:'vTFFUNCAOAPFATRIBUTOS_ATRTABELANOM',pic:'@!',nv:''},{av:'AV48TFFuncaoAPFAtributos_AtrTabelaNom_Sel',fld:'vTFFUNCAOAPFATRIBUTOS_ATRTABELANOM_SEL',pic:'@!',nv:''},{av:'AV51TFFuncaoAPFAtributos_FuncaoDadosNom',fld:'vTFFUNCAOAPFATRIBUTOS_FUNCAODADOSNOM',pic:'',nv:''},{av:'AV52TFFuncaoAPFAtributos_FuncaoDadosNom_Sel',fld:'vTFFUNCAOAPFATRIBUTOS_FUNCAODADOSNOM_SEL',pic:'',nv:''},{av:'AV19FuncaoAPF_Codigo',fld:'vFUNCAOAPF_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV45ddo_FuncaoAPFAtributos_AtributosNomTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_FuncaoAPFAtributos_AtrTabelaNomTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFATRIBUTOS_ATRTABELANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_FuncaoAPFAtributos_FuncaoDadosNomTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSNOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV62Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV20FuncaoAPF_SistemaCod',fld:'vFUNCAOAPF_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'AV25GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV11DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV15DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A393FuncaoAPFAtributos_SistemaCod',fld:'FUNCAOAPFATRIBUTOS_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'A415FuncaoAPFAtributos_FuncaoDadosTip',fld:'FUNCAOAPFATRIBUTOS_FUNCAODADOSTIP',pic:'',nv:''},{av:'A378FuncaoAPFAtributos_FuncaoDadosCod',fld:'FUNCAOAPFATRIBUTOS_FUNCAODADOSCOD',pic:'ZZZZZ9',nv:0},{av:'A391FuncaoDados_FuncaoDadosCod',fld:'FUNCAODADOS_FUNCAODADOSCOD',pic:'ZZZZZ9',nv:0},{av:'AV38FuncaoAPFAtributos_FuncaoDadosCod',fld:'vFUNCAOAPFATRIBUTOS_FUNCAODADOSCOD',pic:'ZZZZZ9',nv:0},{av:'A373FuncaoDados_Tipo',fld:'FUNCAODADOS_TIPO',pic:'',nv:''},{av:'sPrefix',nv:''},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM.ONOPTIONCLICKED","{handler:'E12982',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV30OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV31OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV21FuncaoAPFAtributos_AtributosNom1',fld:'vFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM1',pic:'@!',nv:''},{av:'AV23FuncaoAPFAtributos_AtrTabelaNom1',fld:'vFUNCAOAPFATRIBUTOS_ATRTABELANOM1',pic:'@!',nv:''},{av:'AV17DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22FuncaoAPFAtributos_AtributosNom2',fld:'vFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM2',pic:'@!',nv:''},{av:'AV24FuncaoAPFAtributos_AtrTabelaNom2',fld:'vFUNCAOAPFATRIBUTOS_ATRTABELANOM2',pic:'@!',nv:''},{av:'AV9DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV43TFFuncaoAPFAtributos_AtributosNom',fld:'vTFFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM',pic:'@!',nv:''},{av:'AV44TFFuncaoAPFAtributos_AtributosNom_Sel',fld:'vTFFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM_SEL',pic:'@!',nv:''},{av:'AV47TFFuncaoAPFAtributos_AtrTabelaNom',fld:'vTFFUNCAOAPFATRIBUTOS_ATRTABELANOM',pic:'@!',nv:''},{av:'AV48TFFuncaoAPFAtributos_AtrTabelaNom_Sel',fld:'vTFFUNCAOAPFATRIBUTOS_ATRTABELANOM_SEL',pic:'@!',nv:''},{av:'AV51TFFuncaoAPFAtributos_FuncaoDadosNom',fld:'vTFFUNCAOAPFATRIBUTOS_FUNCAODADOSNOM',pic:'',nv:''},{av:'AV52TFFuncaoAPFAtributos_FuncaoDadosNom_Sel',fld:'vTFFUNCAOAPFATRIBUTOS_FUNCAODADOSNOM_SEL',pic:'',nv:''},{av:'AV19FuncaoAPF_Codigo',fld:'vFUNCAOAPF_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV45ddo_FuncaoAPFAtributos_AtributosNomTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_FuncaoAPFAtributos_AtrTabelaNomTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFATRIBUTOS_ATRTABELANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_FuncaoAPFAtributos_FuncaoDadosNomTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSNOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV62Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV20FuncaoAPF_SistemaCod',fld:'vFUNCAOAPF_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'AV25GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV11DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV15DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A393FuncaoAPFAtributos_SistemaCod',fld:'FUNCAOAPFATRIBUTOS_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'A415FuncaoAPFAtributos_FuncaoDadosTip',fld:'FUNCAOAPFATRIBUTOS_FUNCAODADOSTIP',pic:'',nv:''},{av:'A378FuncaoAPFAtributos_FuncaoDadosCod',fld:'FUNCAOAPFATRIBUTOS_FUNCAODADOSCOD',pic:'ZZZZZ9',nv:0},{av:'A391FuncaoDados_FuncaoDadosCod',fld:'FUNCAODADOS_FUNCAODADOSCOD',pic:'ZZZZZ9',nv:0},{av:'AV38FuncaoAPFAtributos_FuncaoDadosCod',fld:'vFUNCAOAPFATRIBUTOS_FUNCAODADOSCOD',pic:'ZZZZZ9',nv:0},{av:'A373FuncaoDados_Tipo',fld:'FUNCAODADOS_TIPO',pic:'',nv:''},{av:'sPrefix',nv:''},{av:'Ddo_funcaoapfatributos_atributosnom_Activeeventkey',ctrl:'DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM',prop:'ActiveEventKey'},{av:'Ddo_funcaoapfatributos_atributosnom_Filteredtext_get',ctrl:'DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM',prop:'FilteredText_get'},{av:'Ddo_funcaoapfatributos_atributosnom_Selectedvalue_get',ctrl:'DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM',prop:'SelectedValue_get'}],oparms:[{av:'AV30OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV31OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_funcaoapfatributos_atributosnom_Sortedstatus',ctrl:'DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM',prop:'SortedStatus'},{av:'AV43TFFuncaoAPFAtributos_AtributosNom',fld:'vTFFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM',pic:'@!',nv:''},{av:'AV44TFFuncaoAPFAtributos_AtributosNom_Sel',fld:'vTFFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM_SEL',pic:'@!',nv:''},{av:'Ddo_funcaoapfatributos_atrtabelanom_Sortedstatus',ctrl:'DDO_FUNCAOAPFATRIBUTOS_ATRTABELANOM',prop:'SortedStatus'},{av:'Ddo_funcaoapfatributos_funcaodadosnom_Sortedstatus',ctrl:'DDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSNOM',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_FUNCAOAPFATRIBUTOS_ATRTABELANOM.ONOPTIONCLICKED","{handler:'E13982',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV30OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV31OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV21FuncaoAPFAtributos_AtributosNom1',fld:'vFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM1',pic:'@!',nv:''},{av:'AV23FuncaoAPFAtributos_AtrTabelaNom1',fld:'vFUNCAOAPFATRIBUTOS_ATRTABELANOM1',pic:'@!',nv:''},{av:'AV17DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22FuncaoAPFAtributos_AtributosNom2',fld:'vFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM2',pic:'@!',nv:''},{av:'AV24FuncaoAPFAtributos_AtrTabelaNom2',fld:'vFUNCAOAPFATRIBUTOS_ATRTABELANOM2',pic:'@!',nv:''},{av:'AV9DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV43TFFuncaoAPFAtributos_AtributosNom',fld:'vTFFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM',pic:'@!',nv:''},{av:'AV44TFFuncaoAPFAtributos_AtributosNom_Sel',fld:'vTFFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM_SEL',pic:'@!',nv:''},{av:'AV47TFFuncaoAPFAtributos_AtrTabelaNom',fld:'vTFFUNCAOAPFATRIBUTOS_ATRTABELANOM',pic:'@!',nv:''},{av:'AV48TFFuncaoAPFAtributos_AtrTabelaNom_Sel',fld:'vTFFUNCAOAPFATRIBUTOS_ATRTABELANOM_SEL',pic:'@!',nv:''},{av:'AV51TFFuncaoAPFAtributos_FuncaoDadosNom',fld:'vTFFUNCAOAPFATRIBUTOS_FUNCAODADOSNOM',pic:'',nv:''},{av:'AV52TFFuncaoAPFAtributos_FuncaoDadosNom_Sel',fld:'vTFFUNCAOAPFATRIBUTOS_FUNCAODADOSNOM_SEL',pic:'',nv:''},{av:'AV19FuncaoAPF_Codigo',fld:'vFUNCAOAPF_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV45ddo_FuncaoAPFAtributos_AtributosNomTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_FuncaoAPFAtributos_AtrTabelaNomTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFATRIBUTOS_ATRTABELANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_FuncaoAPFAtributos_FuncaoDadosNomTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSNOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV62Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV20FuncaoAPF_SistemaCod',fld:'vFUNCAOAPF_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'AV25GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV11DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV15DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A393FuncaoAPFAtributos_SistemaCod',fld:'FUNCAOAPFATRIBUTOS_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'A415FuncaoAPFAtributos_FuncaoDadosTip',fld:'FUNCAOAPFATRIBUTOS_FUNCAODADOSTIP',pic:'',nv:''},{av:'A378FuncaoAPFAtributos_FuncaoDadosCod',fld:'FUNCAOAPFATRIBUTOS_FUNCAODADOSCOD',pic:'ZZZZZ9',nv:0},{av:'A391FuncaoDados_FuncaoDadosCod',fld:'FUNCAODADOS_FUNCAODADOSCOD',pic:'ZZZZZ9',nv:0},{av:'AV38FuncaoAPFAtributos_FuncaoDadosCod',fld:'vFUNCAOAPFATRIBUTOS_FUNCAODADOSCOD',pic:'ZZZZZ9',nv:0},{av:'A373FuncaoDados_Tipo',fld:'FUNCAODADOS_TIPO',pic:'',nv:''},{av:'sPrefix',nv:''},{av:'Ddo_funcaoapfatributos_atrtabelanom_Activeeventkey',ctrl:'DDO_FUNCAOAPFATRIBUTOS_ATRTABELANOM',prop:'ActiveEventKey'},{av:'Ddo_funcaoapfatributos_atrtabelanom_Filteredtext_get',ctrl:'DDO_FUNCAOAPFATRIBUTOS_ATRTABELANOM',prop:'FilteredText_get'},{av:'Ddo_funcaoapfatributos_atrtabelanom_Selectedvalue_get',ctrl:'DDO_FUNCAOAPFATRIBUTOS_ATRTABELANOM',prop:'SelectedValue_get'}],oparms:[{av:'AV30OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV31OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_funcaoapfatributos_atrtabelanom_Sortedstatus',ctrl:'DDO_FUNCAOAPFATRIBUTOS_ATRTABELANOM',prop:'SortedStatus'},{av:'AV47TFFuncaoAPFAtributos_AtrTabelaNom',fld:'vTFFUNCAOAPFATRIBUTOS_ATRTABELANOM',pic:'@!',nv:''},{av:'AV48TFFuncaoAPFAtributos_AtrTabelaNom_Sel',fld:'vTFFUNCAOAPFATRIBUTOS_ATRTABELANOM_SEL',pic:'@!',nv:''},{av:'Ddo_funcaoapfatributos_atributosnom_Sortedstatus',ctrl:'DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM',prop:'SortedStatus'},{av:'Ddo_funcaoapfatributos_funcaodadosnom_Sortedstatus',ctrl:'DDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSNOM',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSNOM.ONOPTIONCLICKED","{handler:'E14982',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV30OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV31OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV21FuncaoAPFAtributos_AtributosNom1',fld:'vFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM1',pic:'@!',nv:''},{av:'AV23FuncaoAPFAtributos_AtrTabelaNom1',fld:'vFUNCAOAPFATRIBUTOS_ATRTABELANOM1',pic:'@!',nv:''},{av:'AV17DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22FuncaoAPFAtributos_AtributosNom2',fld:'vFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM2',pic:'@!',nv:''},{av:'AV24FuncaoAPFAtributos_AtrTabelaNom2',fld:'vFUNCAOAPFATRIBUTOS_ATRTABELANOM2',pic:'@!',nv:''},{av:'AV9DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV43TFFuncaoAPFAtributos_AtributosNom',fld:'vTFFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM',pic:'@!',nv:''},{av:'AV44TFFuncaoAPFAtributos_AtributosNom_Sel',fld:'vTFFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM_SEL',pic:'@!',nv:''},{av:'AV47TFFuncaoAPFAtributos_AtrTabelaNom',fld:'vTFFUNCAOAPFATRIBUTOS_ATRTABELANOM',pic:'@!',nv:''},{av:'AV48TFFuncaoAPFAtributos_AtrTabelaNom_Sel',fld:'vTFFUNCAOAPFATRIBUTOS_ATRTABELANOM_SEL',pic:'@!',nv:''},{av:'AV51TFFuncaoAPFAtributos_FuncaoDadosNom',fld:'vTFFUNCAOAPFATRIBUTOS_FUNCAODADOSNOM',pic:'',nv:''},{av:'AV52TFFuncaoAPFAtributos_FuncaoDadosNom_Sel',fld:'vTFFUNCAOAPFATRIBUTOS_FUNCAODADOSNOM_SEL',pic:'',nv:''},{av:'AV19FuncaoAPF_Codigo',fld:'vFUNCAOAPF_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV45ddo_FuncaoAPFAtributos_AtributosNomTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_FuncaoAPFAtributos_AtrTabelaNomTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFATRIBUTOS_ATRTABELANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_FuncaoAPFAtributos_FuncaoDadosNomTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSNOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV62Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV20FuncaoAPF_SistemaCod',fld:'vFUNCAOAPF_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'AV25GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV11DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV15DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A393FuncaoAPFAtributos_SistemaCod',fld:'FUNCAOAPFATRIBUTOS_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'A415FuncaoAPFAtributos_FuncaoDadosTip',fld:'FUNCAOAPFATRIBUTOS_FUNCAODADOSTIP',pic:'',nv:''},{av:'A378FuncaoAPFAtributos_FuncaoDadosCod',fld:'FUNCAOAPFATRIBUTOS_FUNCAODADOSCOD',pic:'ZZZZZ9',nv:0},{av:'A391FuncaoDados_FuncaoDadosCod',fld:'FUNCAODADOS_FUNCAODADOSCOD',pic:'ZZZZZ9',nv:0},{av:'AV38FuncaoAPFAtributos_FuncaoDadosCod',fld:'vFUNCAOAPFATRIBUTOS_FUNCAODADOSCOD',pic:'ZZZZZ9',nv:0},{av:'A373FuncaoDados_Tipo',fld:'FUNCAODADOS_TIPO',pic:'',nv:''},{av:'sPrefix',nv:''},{av:'Ddo_funcaoapfatributos_funcaodadosnom_Activeeventkey',ctrl:'DDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSNOM',prop:'ActiveEventKey'},{av:'Ddo_funcaoapfatributos_funcaodadosnom_Filteredtext_get',ctrl:'DDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSNOM',prop:'FilteredText_get'},{av:'Ddo_funcaoapfatributos_funcaodadosnom_Selectedvalue_get',ctrl:'DDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSNOM',prop:'SelectedValue_get'}],oparms:[{av:'AV30OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV31OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_funcaoapfatributos_funcaodadosnom_Sortedstatus',ctrl:'DDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSNOM',prop:'SortedStatus'},{av:'AV51TFFuncaoAPFAtributos_FuncaoDadosNom',fld:'vTFFUNCAOAPFATRIBUTOS_FUNCAODADOSNOM',pic:'',nv:''},{av:'AV52TFFuncaoAPFAtributos_FuncaoDadosNom_Sel',fld:'vTFFUNCAOAPFATRIBUTOS_FUNCAODADOSNOM_SEL',pic:'',nv:''},{av:'Ddo_funcaoapfatributos_atributosnom_Sortedstatus',ctrl:'DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM',prop:'SortedStatus'},{av:'Ddo_funcaoapfatributos_atrtabelanom_Sortedstatus',ctrl:'DDO_FUNCAOAPFATRIBUTOS_ATRTABELANOM',prop:'SortedStatus'}]}");
         setEventMetadata("GRID.LOAD","{handler:'E25982',iparms:[{av:'AV20FuncaoAPF_SistemaCod',fld:'vFUNCAOAPF_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'A393FuncaoAPFAtributos_SistemaCod',fld:'FUNCAOAPFATRIBUTOS_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'A415FuncaoAPFAtributos_FuncaoDadosTip',fld:'FUNCAOAPFATRIBUTOS_FUNCAODADOSTIP',pic:'',nv:''},{av:'A378FuncaoAPFAtributos_FuncaoDadosCod',fld:'FUNCAOAPFATRIBUTOS_FUNCAODADOSCOD',pic:'ZZZZZ9',nv:0},{av:'A391FuncaoDados_FuncaoDadosCod',fld:'FUNCAODADOS_FUNCAODADOSCOD',pic:'ZZZZZ9',nv:0},{av:'AV38FuncaoAPFAtributos_FuncaoDadosCod',fld:'vFUNCAOAPFATRIBUTOS_FUNCAODADOSCOD',pic:'ZZZZZ9',nv:0},{av:'A373FuncaoDados_Tipo',fld:'FUNCAODADOS_TIPO',pic:'',nv:''}],oparms:[{av:'AV39DeleteAtributo',fld:'vDELETEATRIBUTO',pic:'',nv:''},{av:'edtavDeleteatributo_Tooltiptext',ctrl:'vDELETEATRIBUTO',prop:'Tooltiptext'},{av:'AV37FuncaoAPFAtributos_FuncaoDadosTip',fld:'vFUNCAOAPFATRIBUTOS_FUNCAODADOSTIP',pic:'',nv:''},{av:'AV38FuncaoAPFAtributos_FuncaoDadosCod',fld:'vFUNCAOAPFATRIBUTOS_FUNCAODADOSCOD',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("VORDEREDBY.CLICK","{handler:'E15982',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV30OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV31OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV21FuncaoAPFAtributos_AtributosNom1',fld:'vFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM1',pic:'@!',nv:''},{av:'AV23FuncaoAPFAtributos_AtrTabelaNom1',fld:'vFUNCAOAPFATRIBUTOS_ATRTABELANOM1',pic:'@!',nv:''},{av:'AV17DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22FuncaoAPFAtributos_AtributosNom2',fld:'vFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM2',pic:'@!',nv:''},{av:'AV24FuncaoAPFAtributos_AtrTabelaNom2',fld:'vFUNCAOAPFATRIBUTOS_ATRTABELANOM2',pic:'@!',nv:''},{av:'AV9DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV43TFFuncaoAPFAtributos_AtributosNom',fld:'vTFFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM',pic:'@!',nv:''},{av:'AV44TFFuncaoAPFAtributos_AtributosNom_Sel',fld:'vTFFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM_SEL',pic:'@!',nv:''},{av:'AV47TFFuncaoAPFAtributos_AtrTabelaNom',fld:'vTFFUNCAOAPFATRIBUTOS_ATRTABELANOM',pic:'@!',nv:''},{av:'AV48TFFuncaoAPFAtributos_AtrTabelaNom_Sel',fld:'vTFFUNCAOAPFATRIBUTOS_ATRTABELANOM_SEL',pic:'@!',nv:''},{av:'AV51TFFuncaoAPFAtributos_FuncaoDadosNom',fld:'vTFFUNCAOAPFATRIBUTOS_FUNCAODADOSNOM',pic:'',nv:''},{av:'AV52TFFuncaoAPFAtributos_FuncaoDadosNom_Sel',fld:'vTFFUNCAOAPFATRIBUTOS_FUNCAODADOSNOM_SEL',pic:'',nv:''},{av:'AV19FuncaoAPF_Codigo',fld:'vFUNCAOAPF_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV45ddo_FuncaoAPFAtributos_AtributosNomTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_FuncaoAPFAtributos_AtrTabelaNomTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFATRIBUTOS_ATRTABELANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_FuncaoAPFAtributos_FuncaoDadosNomTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSNOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV62Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV20FuncaoAPF_SistemaCod',fld:'vFUNCAOAPF_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'AV25GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV11DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV15DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A393FuncaoAPFAtributos_SistemaCod',fld:'FUNCAOAPFATRIBUTOS_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'A415FuncaoAPFAtributos_FuncaoDadosTip',fld:'FUNCAOAPFATRIBUTOS_FUNCAODADOSTIP',pic:'',nv:''},{av:'A378FuncaoAPFAtributos_FuncaoDadosCod',fld:'FUNCAOAPFATRIBUTOS_FUNCAODADOSCOD',pic:'ZZZZZ9',nv:0},{av:'A391FuncaoDados_FuncaoDadosCod',fld:'FUNCAODADOS_FUNCAODADOSCOD',pic:'ZZZZZ9',nv:0},{av:'AV38FuncaoAPFAtributos_FuncaoDadosCod',fld:'vFUNCAOAPFATRIBUTOS_FUNCAODADOSCOD',pic:'ZZZZZ9',nv:0},{av:'A373FuncaoDados_Tipo',fld:'FUNCAODADOS_TIPO',pic:'',nv:''},{av:'sPrefix',nv:''}],oparms:[]}");
         setEventMetadata("'ADDDYNAMICFILTERS1'","{handler:'E20982',iparms:[],oparms:[{av:'AV9DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS1'","{handler:'E16982',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV30OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV31OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV21FuncaoAPFAtributos_AtributosNom1',fld:'vFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM1',pic:'@!',nv:''},{av:'AV23FuncaoAPFAtributos_AtrTabelaNom1',fld:'vFUNCAOAPFATRIBUTOS_ATRTABELANOM1',pic:'@!',nv:''},{av:'AV17DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22FuncaoAPFAtributos_AtributosNom2',fld:'vFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM2',pic:'@!',nv:''},{av:'AV24FuncaoAPFAtributos_AtrTabelaNom2',fld:'vFUNCAOAPFATRIBUTOS_ATRTABELANOM2',pic:'@!',nv:''},{av:'AV9DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV43TFFuncaoAPFAtributos_AtributosNom',fld:'vTFFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM',pic:'@!',nv:''},{av:'AV44TFFuncaoAPFAtributos_AtributosNom_Sel',fld:'vTFFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM_SEL',pic:'@!',nv:''},{av:'AV47TFFuncaoAPFAtributos_AtrTabelaNom',fld:'vTFFUNCAOAPFATRIBUTOS_ATRTABELANOM',pic:'@!',nv:''},{av:'AV48TFFuncaoAPFAtributos_AtrTabelaNom_Sel',fld:'vTFFUNCAOAPFATRIBUTOS_ATRTABELANOM_SEL',pic:'@!',nv:''},{av:'AV51TFFuncaoAPFAtributos_FuncaoDadosNom',fld:'vTFFUNCAOAPFATRIBUTOS_FUNCAODADOSNOM',pic:'',nv:''},{av:'AV52TFFuncaoAPFAtributos_FuncaoDadosNom_Sel',fld:'vTFFUNCAOAPFATRIBUTOS_FUNCAODADOSNOM_SEL',pic:'',nv:''},{av:'AV19FuncaoAPF_Codigo',fld:'vFUNCAOAPF_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV45ddo_FuncaoAPFAtributos_AtributosNomTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_FuncaoAPFAtributos_AtrTabelaNomTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFATRIBUTOS_ATRTABELANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_FuncaoAPFAtributos_FuncaoDadosNomTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSNOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV62Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV20FuncaoAPF_SistemaCod',fld:'vFUNCAOAPF_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'AV25GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV11DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV15DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A393FuncaoAPFAtributos_SistemaCod',fld:'FUNCAOAPFATRIBUTOS_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'A415FuncaoAPFAtributos_FuncaoDadosTip',fld:'FUNCAOAPFATRIBUTOS_FUNCAODADOSTIP',pic:'',nv:''},{av:'A378FuncaoAPFAtributos_FuncaoDadosCod',fld:'FUNCAOAPFATRIBUTOS_FUNCAODADOSCOD',pic:'ZZZZZ9',nv:0},{av:'A391FuncaoDados_FuncaoDadosCod',fld:'FUNCAODADOS_FUNCAODADOSCOD',pic:'ZZZZZ9',nv:0},{av:'AV38FuncaoAPFAtributos_FuncaoDadosCod',fld:'vFUNCAOAPFATRIBUTOS_FUNCAODADOSCOD',pic:'ZZZZZ9',nv:0},{av:'A373FuncaoDados_Tipo',fld:'FUNCAODADOS_TIPO',pic:'',nv:''},{av:'sPrefix',nv:''}],oparms:[{av:'AV15DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV11DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV25GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV9DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV17DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22FuncaoAPFAtributos_AtributosNom2',fld:'vFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM2',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV21FuncaoAPFAtributos_AtributosNom1',fld:'vFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM1',pic:'@!',nv:''},{av:'AV23FuncaoAPFAtributos_AtrTabelaNom1',fld:'vFUNCAOAPFATRIBUTOS_ATRTABELANOM1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV24FuncaoAPFAtributos_AtrTabelaNom2',fld:'vFUNCAOAPFATRIBUTOS_ATRTABELANOM2',pic:'@!',nv:''},{av:'edtavFuncaoapfatributos_atributosnom2_Visible',ctrl:'vFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM2',prop:'Visible'},{av:'edtavFuncaoapfatributos_atrtabelanom2_Visible',ctrl:'vFUNCAOAPFATRIBUTOS_ATRTABELANOM2',prop:'Visible'},{av:'edtavFuncaoapfatributos_atributosnom1_Visible',ctrl:'vFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM1',prop:'Visible'},{av:'edtavFuncaoapfatributos_atrtabelanom1_Visible',ctrl:'vFUNCAOAPFATRIBUTOS_ATRTABELANOM1',prop:'Visible'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR1.CLICK","{handler:'E21982',iparms:[{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''}],oparms:[{av:'edtavFuncaoapfatributos_atributosnom1_Visible',ctrl:'vFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM1',prop:'Visible'},{av:'edtavFuncaoapfatributos_atrtabelanom1_Visible',ctrl:'vFUNCAOAPFATRIBUTOS_ATRTABELANOM1',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS2'","{handler:'E17982',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV30OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV31OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV21FuncaoAPFAtributos_AtributosNom1',fld:'vFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM1',pic:'@!',nv:''},{av:'AV23FuncaoAPFAtributos_AtrTabelaNom1',fld:'vFUNCAOAPFATRIBUTOS_ATRTABELANOM1',pic:'@!',nv:''},{av:'AV17DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22FuncaoAPFAtributos_AtributosNom2',fld:'vFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM2',pic:'@!',nv:''},{av:'AV24FuncaoAPFAtributos_AtrTabelaNom2',fld:'vFUNCAOAPFATRIBUTOS_ATRTABELANOM2',pic:'@!',nv:''},{av:'AV9DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV43TFFuncaoAPFAtributos_AtributosNom',fld:'vTFFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM',pic:'@!',nv:''},{av:'AV44TFFuncaoAPFAtributos_AtributosNom_Sel',fld:'vTFFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM_SEL',pic:'@!',nv:''},{av:'AV47TFFuncaoAPFAtributos_AtrTabelaNom',fld:'vTFFUNCAOAPFATRIBUTOS_ATRTABELANOM',pic:'@!',nv:''},{av:'AV48TFFuncaoAPFAtributos_AtrTabelaNom_Sel',fld:'vTFFUNCAOAPFATRIBUTOS_ATRTABELANOM_SEL',pic:'@!',nv:''},{av:'AV51TFFuncaoAPFAtributos_FuncaoDadosNom',fld:'vTFFUNCAOAPFATRIBUTOS_FUNCAODADOSNOM',pic:'',nv:''},{av:'AV52TFFuncaoAPFAtributos_FuncaoDadosNom_Sel',fld:'vTFFUNCAOAPFATRIBUTOS_FUNCAODADOSNOM_SEL',pic:'',nv:''},{av:'AV19FuncaoAPF_Codigo',fld:'vFUNCAOAPF_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV45ddo_FuncaoAPFAtributos_AtributosNomTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_FuncaoAPFAtributos_AtrTabelaNomTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFATRIBUTOS_ATRTABELANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_FuncaoAPFAtributos_FuncaoDadosNomTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSNOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV62Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV20FuncaoAPF_SistemaCod',fld:'vFUNCAOAPF_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'AV25GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV11DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV15DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A393FuncaoAPFAtributos_SistemaCod',fld:'FUNCAOAPFATRIBUTOS_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'A415FuncaoAPFAtributos_FuncaoDadosTip',fld:'FUNCAOAPFATRIBUTOS_FUNCAODADOSTIP',pic:'',nv:''},{av:'A378FuncaoAPFAtributos_FuncaoDadosCod',fld:'FUNCAOAPFATRIBUTOS_FUNCAODADOSCOD',pic:'ZZZZZ9',nv:0},{av:'A391FuncaoDados_FuncaoDadosCod',fld:'FUNCAODADOS_FUNCAODADOSCOD',pic:'ZZZZZ9',nv:0},{av:'AV38FuncaoAPFAtributos_FuncaoDadosCod',fld:'vFUNCAOAPFATRIBUTOS_FUNCAODADOSCOD',pic:'ZZZZZ9',nv:0},{av:'A373FuncaoDados_Tipo',fld:'FUNCAODADOS_TIPO',pic:'',nv:''},{av:'sPrefix',nv:''}],oparms:[{av:'AV15DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV9DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV25GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV17DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22FuncaoAPFAtributos_AtributosNom2',fld:'vFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM2',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV21FuncaoAPFAtributos_AtributosNom1',fld:'vFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM1',pic:'@!',nv:''},{av:'AV23FuncaoAPFAtributos_AtrTabelaNom1',fld:'vFUNCAOAPFATRIBUTOS_ATRTABELANOM1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV24FuncaoAPFAtributos_AtrTabelaNom2',fld:'vFUNCAOAPFATRIBUTOS_ATRTABELANOM2',pic:'@!',nv:''},{av:'edtavFuncaoapfatributos_atributosnom2_Visible',ctrl:'vFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM2',prop:'Visible'},{av:'edtavFuncaoapfatributos_atrtabelanom2_Visible',ctrl:'vFUNCAOAPFATRIBUTOS_ATRTABELANOM2',prop:'Visible'},{av:'edtavFuncaoapfatributos_atributosnom1_Visible',ctrl:'vFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM1',prop:'Visible'},{av:'edtavFuncaoapfatributos_atrtabelanom1_Visible',ctrl:'vFUNCAOAPFATRIBUTOS_ATRTABELANOM1',prop:'Visible'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR2.CLICK","{handler:'E22982',iparms:[{av:'AV17DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''}],oparms:[{av:'edtavFuncaoapfatributos_atributosnom2_Visible',ctrl:'vFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM2',prop:'Visible'},{av:'edtavFuncaoapfatributos_atrtabelanom2_Visible',ctrl:'vFUNCAOAPFATRIBUTOS_ATRTABELANOM2',prop:'Visible'}]}");
         setEventMetadata("'DOCLEANFILTERS'","{handler:'E18982',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV30OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV31OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV21FuncaoAPFAtributos_AtributosNom1',fld:'vFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM1',pic:'@!',nv:''},{av:'AV23FuncaoAPFAtributos_AtrTabelaNom1',fld:'vFUNCAOAPFATRIBUTOS_ATRTABELANOM1',pic:'@!',nv:''},{av:'AV17DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22FuncaoAPFAtributos_AtributosNom2',fld:'vFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM2',pic:'@!',nv:''},{av:'AV24FuncaoAPFAtributos_AtrTabelaNom2',fld:'vFUNCAOAPFATRIBUTOS_ATRTABELANOM2',pic:'@!',nv:''},{av:'AV9DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV43TFFuncaoAPFAtributos_AtributosNom',fld:'vTFFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM',pic:'@!',nv:''},{av:'AV44TFFuncaoAPFAtributos_AtributosNom_Sel',fld:'vTFFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM_SEL',pic:'@!',nv:''},{av:'AV47TFFuncaoAPFAtributos_AtrTabelaNom',fld:'vTFFUNCAOAPFATRIBUTOS_ATRTABELANOM',pic:'@!',nv:''},{av:'AV48TFFuncaoAPFAtributos_AtrTabelaNom_Sel',fld:'vTFFUNCAOAPFATRIBUTOS_ATRTABELANOM_SEL',pic:'@!',nv:''},{av:'AV51TFFuncaoAPFAtributos_FuncaoDadosNom',fld:'vTFFUNCAOAPFATRIBUTOS_FUNCAODADOSNOM',pic:'',nv:''},{av:'AV52TFFuncaoAPFAtributos_FuncaoDadosNom_Sel',fld:'vTFFUNCAOAPFATRIBUTOS_FUNCAODADOSNOM_SEL',pic:'',nv:''},{av:'AV19FuncaoAPF_Codigo',fld:'vFUNCAOAPF_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV45ddo_FuncaoAPFAtributos_AtributosNomTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_FuncaoAPFAtributos_AtrTabelaNomTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFATRIBUTOS_ATRTABELANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_FuncaoAPFAtributos_FuncaoDadosNomTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSNOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV62Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV20FuncaoAPF_SistemaCod',fld:'vFUNCAOAPF_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'AV25GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV11DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV15DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A393FuncaoAPFAtributos_SistemaCod',fld:'FUNCAOAPFATRIBUTOS_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'A415FuncaoAPFAtributos_FuncaoDadosTip',fld:'FUNCAOAPFATRIBUTOS_FUNCAODADOSTIP',pic:'',nv:''},{av:'A378FuncaoAPFAtributos_FuncaoDadosCod',fld:'FUNCAOAPFATRIBUTOS_FUNCAODADOSCOD',pic:'ZZZZZ9',nv:0},{av:'A391FuncaoDados_FuncaoDadosCod',fld:'FUNCAODADOS_FUNCAODADOSCOD',pic:'ZZZZZ9',nv:0},{av:'AV38FuncaoAPFAtributos_FuncaoDadosCod',fld:'vFUNCAOAPFATRIBUTOS_FUNCAODADOSCOD',pic:'ZZZZZ9',nv:0},{av:'A373FuncaoDados_Tipo',fld:'FUNCAODADOS_TIPO',pic:'',nv:''},{av:'sPrefix',nv:''}],oparms:[{av:'AV43TFFuncaoAPFAtributos_AtributosNom',fld:'vTFFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM',pic:'@!',nv:''},{av:'Ddo_funcaoapfatributos_atributosnom_Filteredtext_set',ctrl:'DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM',prop:'FilteredText_set'},{av:'AV44TFFuncaoAPFAtributos_AtributosNom_Sel',fld:'vTFFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM_SEL',pic:'@!',nv:''},{av:'Ddo_funcaoapfatributos_atributosnom_Selectedvalue_set',ctrl:'DDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM',prop:'SelectedValue_set'},{av:'AV47TFFuncaoAPFAtributos_AtrTabelaNom',fld:'vTFFUNCAOAPFATRIBUTOS_ATRTABELANOM',pic:'@!',nv:''},{av:'Ddo_funcaoapfatributos_atrtabelanom_Filteredtext_set',ctrl:'DDO_FUNCAOAPFATRIBUTOS_ATRTABELANOM',prop:'FilteredText_set'},{av:'AV48TFFuncaoAPFAtributos_AtrTabelaNom_Sel',fld:'vTFFUNCAOAPFATRIBUTOS_ATRTABELANOM_SEL',pic:'@!',nv:''},{av:'Ddo_funcaoapfatributos_atrtabelanom_Selectedvalue_set',ctrl:'DDO_FUNCAOAPFATRIBUTOS_ATRTABELANOM',prop:'SelectedValue_set'},{av:'AV51TFFuncaoAPFAtributos_FuncaoDadosNom',fld:'vTFFUNCAOAPFATRIBUTOS_FUNCAODADOSNOM',pic:'',nv:''},{av:'Ddo_funcaoapfatributos_funcaodadosnom_Filteredtext_set',ctrl:'DDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSNOM',prop:'FilteredText_set'},{av:'AV52TFFuncaoAPFAtributos_FuncaoDadosNom_Sel',fld:'vTFFUNCAOAPFATRIBUTOS_FUNCAODADOSNOM_SEL',pic:'',nv:''},{av:'Ddo_funcaoapfatributos_funcaodadosnom_Selectedvalue_set',ctrl:'DDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSNOM',prop:'SelectedValue_set'},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV21FuncaoAPFAtributos_AtributosNom1',fld:'vFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM1',pic:'@!',nv:''},{av:'AV25GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'edtavFuncaoapfatributos_atributosnom1_Visible',ctrl:'vFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM1',prop:'Visible'},{av:'edtavFuncaoapfatributos_atrtabelanom1_Visible',ctrl:'vFUNCAOAPFATRIBUTOS_ATRTABELANOM1',prop:'Visible'},{av:'AV9DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV17DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22FuncaoAPFAtributos_AtributosNom2',fld:'vFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM2',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'AV23FuncaoAPFAtributos_AtrTabelaNom1',fld:'vFUNCAOAPFATRIBUTOS_ATRTABELANOM1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV24FuncaoAPFAtributos_AtrTabelaNom2',fld:'vFUNCAOAPFATRIBUTOS_ATRTABELANOM2',pic:'@!',nv:''},{av:'edtavFuncaoapfatributos_atributosnom2_Visible',ctrl:'vFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM2',prop:'Visible'},{av:'edtavFuncaoapfatributos_atrtabelanom2_Visible',ctrl:'vFUNCAOAPFATRIBUTOS_ATRTABELANOM2',prop:'Visible'}]}");
         setEventMetadata("VDELETEATRIBUTO.CLICK","{handler:'E26982',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV30OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV31OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV21FuncaoAPFAtributos_AtributosNom1',fld:'vFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM1',pic:'@!',nv:''},{av:'AV23FuncaoAPFAtributos_AtrTabelaNom1',fld:'vFUNCAOAPFATRIBUTOS_ATRTABELANOM1',pic:'@!',nv:''},{av:'AV17DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22FuncaoAPFAtributos_AtributosNom2',fld:'vFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM2',pic:'@!',nv:''},{av:'AV24FuncaoAPFAtributos_AtrTabelaNom2',fld:'vFUNCAOAPFATRIBUTOS_ATRTABELANOM2',pic:'@!',nv:''},{av:'AV9DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV43TFFuncaoAPFAtributos_AtributosNom',fld:'vTFFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM',pic:'@!',nv:''},{av:'AV44TFFuncaoAPFAtributos_AtributosNom_Sel',fld:'vTFFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM_SEL',pic:'@!',nv:''},{av:'AV47TFFuncaoAPFAtributos_AtrTabelaNom',fld:'vTFFUNCAOAPFATRIBUTOS_ATRTABELANOM',pic:'@!',nv:''},{av:'AV48TFFuncaoAPFAtributos_AtrTabelaNom_Sel',fld:'vTFFUNCAOAPFATRIBUTOS_ATRTABELANOM_SEL',pic:'@!',nv:''},{av:'AV51TFFuncaoAPFAtributos_FuncaoDadosNom',fld:'vTFFUNCAOAPFATRIBUTOS_FUNCAODADOSNOM',pic:'',nv:''},{av:'AV52TFFuncaoAPFAtributos_FuncaoDadosNom_Sel',fld:'vTFFUNCAOAPFATRIBUTOS_FUNCAODADOSNOM_SEL',pic:'',nv:''},{av:'AV19FuncaoAPF_Codigo',fld:'vFUNCAOAPF_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV45ddo_FuncaoAPFAtributos_AtributosNomTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_FuncaoAPFAtributos_AtrTabelaNomTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFATRIBUTOS_ATRTABELANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_FuncaoAPFAtributos_FuncaoDadosNomTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSNOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV62Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV20FuncaoAPF_SistemaCod',fld:'vFUNCAOAPF_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'AV25GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV11DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV15DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A393FuncaoAPFAtributos_SistemaCod',fld:'FUNCAOAPFATRIBUTOS_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'A415FuncaoAPFAtributos_FuncaoDadosTip',fld:'FUNCAOAPFATRIBUTOS_FUNCAODADOSTIP',pic:'',nv:''},{av:'A378FuncaoAPFAtributos_FuncaoDadosCod',fld:'FUNCAOAPFATRIBUTOS_FUNCAODADOSCOD',pic:'ZZZZZ9',nv:0},{av:'A391FuncaoDados_FuncaoDadosCod',fld:'FUNCAODADOS_FUNCAODADOSCOD',pic:'ZZZZZ9',nv:0},{av:'AV38FuncaoAPFAtributos_FuncaoDadosCod',fld:'vFUNCAOAPFATRIBUTOS_FUNCAODADOSCOD',pic:'ZZZZZ9',nv:0},{av:'A373FuncaoDados_Tipo',fld:'FUNCAODADOS_TIPO',pic:'',nv:''},{av:'sPrefix',nv:''},{av:'A165FuncaoAPF_Codigo',fld:'FUNCAOAPF_CODIGO',pic:'ZZZZZ9',nv:0},{av:'A364FuncaoAPFAtributos_AtributosCod',fld:'FUNCAOAPFATRIBUTOS_ATRIBUTOSCOD',pic:'ZZZZZ9',nv:0}],oparms:[{av:'A364FuncaoAPFAtributos_AtributosCod',fld:'FUNCAOAPFATRIBUTOS_ATRIBUTOSCOD',pic:'ZZZZZ9',nv:0},{av:'A165FuncaoAPF_Codigo',fld:'FUNCAOAPF_CODIGO',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("'DOINSERTATRIBUTO'","{handler:'E19982',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV30OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV31OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV21FuncaoAPFAtributos_AtributosNom1',fld:'vFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM1',pic:'@!',nv:''},{av:'AV23FuncaoAPFAtributos_AtrTabelaNom1',fld:'vFUNCAOAPFATRIBUTOS_ATRTABELANOM1',pic:'@!',nv:''},{av:'AV17DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV22FuncaoAPFAtributos_AtributosNom2',fld:'vFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM2',pic:'@!',nv:''},{av:'AV24FuncaoAPFAtributos_AtrTabelaNom2',fld:'vFUNCAOAPFATRIBUTOS_ATRTABELANOM2',pic:'@!',nv:''},{av:'AV9DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV43TFFuncaoAPFAtributos_AtributosNom',fld:'vTFFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM',pic:'@!',nv:''},{av:'AV44TFFuncaoAPFAtributos_AtributosNom_Sel',fld:'vTFFUNCAOAPFATRIBUTOS_ATRIBUTOSNOM_SEL',pic:'@!',nv:''},{av:'AV47TFFuncaoAPFAtributos_AtrTabelaNom',fld:'vTFFUNCAOAPFATRIBUTOS_ATRTABELANOM',pic:'@!',nv:''},{av:'AV48TFFuncaoAPFAtributos_AtrTabelaNom_Sel',fld:'vTFFUNCAOAPFATRIBUTOS_ATRTABELANOM_SEL',pic:'@!',nv:''},{av:'AV51TFFuncaoAPFAtributos_FuncaoDadosNom',fld:'vTFFUNCAOAPFATRIBUTOS_FUNCAODADOSNOM',pic:'',nv:''},{av:'AV52TFFuncaoAPFAtributos_FuncaoDadosNom_Sel',fld:'vTFFUNCAOAPFATRIBUTOS_FUNCAODADOSNOM_SEL',pic:'',nv:''},{av:'AV19FuncaoAPF_Codigo',fld:'vFUNCAOAPF_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV45ddo_FuncaoAPFAtributos_AtributosNomTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFATRIBUTOS_ATRIBUTOSNOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_FuncaoAPFAtributos_AtrTabelaNomTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFATRIBUTOS_ATRTABELANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_FuncaoAPFAtributos_FuncaoDadosNomTitleControlIdToReplace',fld:'vDDO_FUNCAOAPFATRIBUTOS_FUNCAODADOSNOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV62Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV20FuncaoAPF_SistemaCod',fld:'vFUNCAOAPF_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'AV25GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV11DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV15DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A393FuncaoAPFAtributos_SistemaCod',fld:'FUNCAOAPFATRIBUTOS_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'A415FuncaoAPFAtributos_FuncaoDadosTip',fld:'FUNCAOAPFATRIBUTOS_FUNCAODADOSTIP',pic:'',nv:''},{av:'A378FuncaoAPFAtributos_FuncaoDadosCod',fld:'FUNCAOAPFATRIBUTOS_FUNCAODADOSCOD',pic:'ZZZZZ9',nv:0},{av:'A391FuncaoDados_FuncaoDadosCod',fld:'FUNCAODADOS_FUNCAODADOSCOD',pic:'ZZZZZ9',nv:0},{av:'AV38FuncaoAPFAtributos_FuncaoDadosCod',fld:'vFUNCAOAPFATRIBUTOS_FUNCAODADOSCOD',pic:'ZZZZZ9',nv:0},{av:'A373FuncaoDados_Tipo',fld:'FUNCAODADOS_TIPO',pic:'',nv:''},{av:'sPrefix',nv:''}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Gridpaginationbar_Selectedpage = "";
         Ddo_funcaoapfatributos_atributosnom_Activeeventkey = "";
         Ddo_funcaoapfatributos_atributosnom_Filteredtext_get = "";
         Ddo_funcaoapfatributos_atributosnom_Selectedvalue_get = "";
         Ddo_funcaoapfatributos_atrtabelanom_Activeeventkey = "";
         Ddo_funcaoapfatributos_atrtabelanom_Filteredtext_get = "";
         Ddo_funcaoapfatributos_atrtabelanom_Selectedvalue_get = "";
         Ddo_funcaoapfatributos_funcaodadosnom_Activeeventkey = "";
         Ddo_funcaoapfatributos_funcaodadosnom_Filteredtext_get = "";
         Ddo_funcaoapfatributos_funcaodadosnom_Selectedvalue_get = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         AV16DynamicFiltersSelector1 = "";
         AV21FuncaoAPFAtributos_AtributosNom1 = "";
         AV23FuncaoAPFAtributos_AtrTabelaNom1 = "";
         AV17DynamicFiltersSelector2 = "";
         AV22FuncaoAPFAtributos_AtributosNom2 = "";
         AV24FuncaoAPFAtributos_AtrTabelaNom2 = "";
         AV43TFFuncaoAPFAtributos_AtributosNom = "";
         AV44TFFuncaoAPFAtributos_AtributosNom_Sel = "";
         AV47TFFuncaoAPFAtributos_AtrTabelaNom = "";
         AV48TFFuncaoAPFAtributos_AtrTabelaNom_Sel = "";
         AV51TFFuncaoAPFAtributos_FuncaoDadosNom = "";
         AV52TFFuncaoAPFAtributos_FuncaoDadosNom_Sel = "";
         AV45ddo_FuncaoAPFAtributos_AtributosNomTitleControlIdToReplace = "";
         AV49ddo_FuncaoAPFAtributos_AtrTabelaNomTitleControlIdToReplace = "";
         AV53ddo_FuncaoAPFAtributos_FuncaoDadosNomTitleControlIdToReplace = "";
         AV62Pgmname = "";
         AV25GridState = new wwpbaseobjects.SdtWWPGridState(context);
         A415FuncaoAPFAtributos_FuncaoDadosTip = "";
         A373FuncaoDados_Tipo = "";
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV54DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV42FuncaoAPFAtributos_AtributosNomTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV46FuncaoAPFAtributos_AtrTabelaNomTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV50FuncaoAPFAtributos_FuncaoDadosNomTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         Ddo_funcaoapfatributos_atributosnom_Filteredtext_set = "";
         Ddo_funcaoapfatributos_atributosnom_Selectedvalue_set = "";
         Ddo_funcaoapfatributos_atributosnom_Sortedstatus = "";
         Ddo_funcaoapfatributos_atrtabelanom_Filteredtext_set = "";
         Ddo_funcaoapfatributos_atrtabelanom_Selectedvalue_set = "";
         Ddo_funcaoapfatributos_atrtabelanom_Sortedstatus = "";
         Ddo_funcaoapfatributos_funcaodadosnom_Filteredtext_set = "";
         Ddo_funcaoapfatributos_funcaodadosnom_Selectedvalue_set = "";
         Ddo_funcaoapfatributos_funcaodadosnom_Sortedstatus = "";
         GX_FocusControl = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV39DeleteAtributo = "";
         AV61Deleteatributo_GXI = "";
         A365FuncaoAPFAtributos_AtributosNom = "";
         A367FuncaoAPFAtributos_AtrTabelaNom = "";
         A412FuncaoAPFAtributos_FuncaoDadosNom = "";
         AV37FuncaoAPFAtributos_FuncaoDadosTip = "";
         GXCCtl = "";
         GridContainer = new GXWebGrid( context);
         scmdbuf = "";
         lV21FuncaoAPFAtributos_AtributosNom1 = "";
         lV23FuncaoAPFAtributos_AtrTabelaNom1 = "";
         lV22FuncaoAPFAtributos_AtributosNom2 = "";
         lV24FuncaoAPFAtributos_AtrTabelaNom2 = "";
         lV43TFFuncaoAPFAtributos_AtributosNom = "";
         lV47TFFuncaoAPFAtributos_AtrTabelaNom = "";
         lV51TFFuncaoAPFAtributos_FuncaoDadosNom = "";
         H00982_A366FuncaoAPFAtributos_AtrTabelaCod = new int[1] ;
         H00982_n366FuncaoAPFAtributos_AtrTabelaCod = new bool[] {false} ;
         H00982_A180Atributos_Ativo = new bool[] {false} ;
         H00982_n180Atributos_Ativo = new bool[] {false} ;
         H00982_A393FuncaoAPFAtributos_SistemaCod = new int[1] ;
         H00982_n393FuncaoAPFAtributos_SistemaCod = new bool[] {false} ;
         H00982_A415FuncaoAPFAtributos_FuncaoDadosTip = new String[] {""} ;
         H00982_n415FuncaoAPFAtributos_FuncaoDadosTip = new bool[] {false} ;
         H00982_A378FuncaoAPFAtributos_FuncaoDadosCod = new int[1] ;
         H00982_n378FuncaoAPFAtributos_FuncaoDadosCod = new bool[] {false} ;
         H00982_A412FuncaoAPFAtributos_FuncaoDadosNom = new String[] {""} ;
         H00982_n412FuncaoAPFAtributos_FuncaoDadosNom = new bool[] {false} ;
         H00982_A367FuncaoAPFAtributos_AtrTabelaNom = new String[] {""} ;
         H00982_n367FuncaoAPFAtributos_AtrTabelaNom = new bool[] {false} ;
         H00982_A365FuncaoAPFAtributos_AtributosNom = new String[] {""} ;
         H00982_n365FuncaoAPFAtributos_AtributosNom = new bool[] {false} ;
         H00982_A364FuncaoAPFAtributos_AtributosCod = new int[1] ;
         H00982_A165FuncaoAPF_Codigo = new int[1] ;
         H00983_AGRID_nRecordCount = new long[1] ;
         imgAdddynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters2_Jsonclick = "";
         imgCleanfilters_Jsonclick = "";
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV36WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         GridRow = new GXWebRow();
         AV32Session = context.GetSession();
         AV27GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV26GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV33TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV28HTTPRequest = new GxHttpRequest( context);
         AV34TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         H00984_A368FuncaoDados_Codigo = new int[1] ;
         H00984_A391FuncaoDados_FuncaoDadosCod = new int[1] ;
         H00984_n391FuncaoDados_FuncaoDadosCod = new bool[] {false} ;
         H00984_A373FuncaoDados_Tipo = new String[] {""} ;
         sStyleString = "";
         lblOrderedtext_Jsonclick = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblJsdynamicfilters_Jsonclick = "";
         lblDynamicfiltersprefix1_Jsonclick = "";
         lblDynamicfiltersmiddle1_Jsonclick = "";
         lblDynamicfiltersprefix2_Jsonclick = "";
         lblDynamicfiltersmiddle2_Jsonclick = "";
         imgInsertatributo_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlAV19FuncaoAPF_Codigo = "";
         sCtrlAV20FuncaoAPF_SistemaCod = "";
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.funcaoapffuncoesapfatributoswc__default(),
            new Object[][] {
                new Object[] {
               H00982_A366FuncaoAPFAtributos_AtrTabelaCod, H00982_n366FuncaoAPFAtributos_AtrTabelaCod, H00982_A180Atributos_Ativo, H00982_n180Atributos_Ativo, H00982_A393FuncaoAPFAtributos_SistemaCod, H00982_n393FuncaoAPFAtributos_SistemaCod, H00982_A415FuncaoAPFAtributos_FuncaoDadosTip, H00982_n415FuncaoAPFAtributos_FuncaoDadosTip, H00982_A378FuncaoAPFAtributos_FuncaoDadosCod, H00982_n378FuncaoAPFAtributos_FuncaoDadosCod,
               H00982_A412FuncaoAPFAtributos_FuncaoDadosNom, H00982_n412FuncaoAPFAtributos_FuncaoDadosNom, H00982_A367FuncaoAPFAtributos_AtrTabelaNom, H00982_n367FuncaoAPFAtributos_AtrTabelaNom, H00982_A365FuncaoAPFAtributos_AtributosNom, H00982_n365FuncaoAPFAtributos_AtributosNom, H00982_A364FuncaoAPFAtributos_AtributosCod, H00982_A165FuncaoAPF_Codigo
               }
               , new Object[] {
               H00983_AGRID_nRecordCount
               }
               , new Object[] {
               H00984_A368FuncaoDados_Codigo, H00984_A391FuncaoDados_FuncaoDadosCod, H00984_n391FuncaoDados_FuncaoDadosCod, H00984_A373FuncaoDados_Tipo
               }
            }
         );
         AV62Pgmname = "FuncaoAPFFuncoesAPFAtributosWC";
         /* GeneXus formulas. */
         AV62Pgmname = "FuncaoAPFFuncoesAPFAtributosWC";
         context.Gx_err = 0;
         cmbavFuncaoapfatributos_funcaodadostip.Enabled = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short nRC_GXsfl_57 ;
      private short nGXsfl_57_idx=1 ;
      private short AV30OrderedBy ;
      private short initialized ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDraw ;
      private short nDoneStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_57_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short edtFuncaoAPFAtributos_AtributosNom_Titleformat ;
      private short edtFuncaoAPFAtributos_AtrTabelaNom_Titleformat ;
      private short edtFuncaoAPFAtributos_FuncaoDadosNom_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int AV19FuncaoAPF_Codigo ;
      private int AV20FuncaoAPF_SistemaCod ;
      private int wcpOAV19FuncaoAPF_Codigo ;
      private int wcpOAV20FuncaoAPF_SistemaCod ;
      private int subGrid_Rows ;
      private int A393FuncaoAPFAtributos_SistemaCod ;
      private int A378FuncaoAPFAtributos_FuncaoDadosCod ;
      private int A391FuncaoDados_FuncaoDadosCod ;
      private int AV38FuncaoAPFAtributos_FuncaoDadosCod ;
      private int Gridpaginationbar_Pagestoshow ;
      private int Ddo_funcaoapfatributos_atributosnom_Datalistupdateminimumcharacters ;
      private int Ddo_funcaoapfatributos_atrtabelanom_Datalistupdateminimumcharacters ;
      private int Ddo_funcaoapfatributos_funcaodadosnom_Datalistupdateminimumcharacters ;
      private int edtavTffuncaoapfatributos_atributosnom_Visible ;
      private int edtavTffuncaoapfatributos_atributosnom_sel_Visible ;
      private int edtavTffuncaoapfatributos_atrtabelanom_Visible ;
      private int edtavTffuncaoapfatributos_atrtabelanom_sel_Visible ;
      private int edtavTffuncaoapfatributos_funcaodadosnom_Visible ;
      private int edtavTffuncaoapfatributos_funcaodadosnom_sel_Visible ;
      private int edtavDdo_funcaoapfatributos_atributosnomtitlecontrolidtoreplace_Visible ;
      private int edtavDdo_funcaoapfatributos_atrtabelanomtitlecontrolidtoreplace_Visible ;
      private int edtavDdo_funcaoapfatributos_funcaodadosnomtitlecontrolidtoreplace_Visible ;
      private int A165FuncaoAPF_Codigo ;
      private int A364FuncaoAPFAtributos_AtributosCod ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int A366FuncaoAPFAtributos_AtrTabelaCod ;
      private int edtavOrdereddsc_Visible ;
      private int AV55PageToGo ;
      private int imgAdddynamicfilters1_Visible ;
      private int imgRemovedynamicfilters1_Visible ;
      private int edtavFuncaoapfatributos_atributosnom1_Visible ;
      private int edtavFuncaoapfatributos_atrtabelanom1_Visible ;
      private int edtavFuncaoapfatributos_atributosnom2_Visible ;
      private int edtavFuncaoapfatributos_atrtabelanom2_Visible ;
      private int AV63GXV1 ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private int edtavDeleteatributo_Enabled ;
      private int edtavDeleteatributo_Visible ;
      private long GRID_nFirstRecordOnPage ;
      private long AV56GridCurrentPage ;
      private long AV57GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private String Gridpaginationbar_Selectedpage ;
      private String Ddo_funcaoapfatributos_atributosnom_Activeeventkey ;
      private String Ddo_funcaoapfatributos_atributosnom_Filteredtext_get ;
      private String Ddo_funcaoapfatributos_atributosnom_Selectedvalue_get ;
      private String Ddo_funcaoapfatributos_atrtabelanom_Activeeventkey ;
      private String Ddo_funcaoapfatributos_atrtabelanom_Filteredtext_get ;
      private String Ddo_funcaoapfatributos_atrtabelanom_Selectedvalue_get ;
      private String Ddo_funcaoapfatributos_funcaodadosnom_Activeeventkey ;
      private String Ddo_funcaoapfatributos_funcaodadosnom_Filteredtext_get ;
      private String Ddo_funcaoapfatributos_funcaodadosnom_Selectedvalue_get ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String sGXsfl_57_idx="0001" ;
      private String AV21FuncaoAPFAtributos_AtributosNom1 ;
      private String AV23FuncaoAPFAtributos_AtrTabelaNom1 ;
      private String AV22FuncaoAPFAtributos_AtributosNom2 ;
      private String AV24FuncaoAPFAtributos_AtrTabelaNom2 ;
      private String AV43TFFuncaoAPFAtributos_AtributosNom ;
      private String AV44TFFuncaoAPFAtributos_AtributosNom_Sel ;
      private String AV47TFFuncaoAPFAtributos_AtrTabelaNom ;
      private String AV48TFFuncaoAPFAtributos_AtrTabelaNom_Sel ;
      private String AV62Pgmname ;
      private String A415FuncaoAPFAtributos_FuncaoDadosTip ;
      private String A373FuncaoDados_Tipo ;
      private String GXKey ;
      private String cmbavFuncaoapfatributos_funcaodadostip_Internalname ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Ddo_funcaoapfatributos_atributosnom_Caption ;
      private String Ddo_funcaoapfatributos_atributosnom_Tooltip ;
      private String Ddo_funcaoapfatributos_atributosnom_Cls ;
      private String Ddo_funcaoapfatributos_atributosnom_Filteredtext_set ;
      private String Ddo_funcaoapfatributos_atributosnom_Selectedvalue_set ;
      private String Ddo_funcaoapfatributos_atributosnom_Dropdownoptionstype ;
      private String Ddo_funcaoapfatributos_atributosnom_Titlecontrolidtoreplace ;
      private String Ddo_funcaoapfatributos_atributosnom_Sortedstatus ;
      private String Ddo_funcaoapfatributos_atributosnom_Filtertype ;
      private String Ddo_funcaoapfatributos_atributosnom_Datalisttype ;
      private String Ddo_funcaoapfatributos_atributosnom_Datalistfixedvalues ;
      private String Ddo_funcaoapfatributos_atributosnom_Datalistproc ;
      private String Ddo_funcaoapfatributos_atributosnom_Sortasc ;
      private String Ddo_funcaoapfatributos_atributosnom_Sortdsc ;
      private String Ddo_funcaoapfatributos_atributosnom_Loadingdata ;
      private String Ddo_funcaoapfatributos_atributosnom_Cleanfilter ;
      private String Ddo_funcaoapfatributos_atributosnom_Rangefilterfrom ;
      private String Ddo_funcaoapfatributos_atributosnom_Rangefilterto ;
      private String Ddo_funcaoapfatributos_atributosnom_Noresultsfound ;
      private String Ddo_funcaoapfatributos_atributosnom_Searchbuttontext ;
      private String Ddo_funcaoapfatributos_atrtabelanom_Caption ;
      private String Ddo_funcaoapfatributos_atrtabelanom_Tooltip ;
      private String Ddo_funcaoapfatributos_atrtabelanom_Cls ;
      private String Ddo_funcaoapfatributos_atrtabelanom_Filteredtext_set ;
      private String Ddo_funcaoapfatributos_atrtabelanom_Selectedvalue_set ;
      private String Ddo_funcaoapfatributos_atrtabelanom_Dropdownoptionstype ;
      private String Ddo_funcaoapfatributos_atrtabelanom_Titlecontrolidtoreplace ;
      private String Ddo_funcaoapfatributos_atrtabelanom_Sortedstatus ;
      private String Ddo_funcaoapfatributos_atrtabelanom_Filtertype ;
      private String Ddo_funcaoapfatributos_atrtabelanom_Datalisttype ;
      private String Ddo_funcaoapfatributos_atrtabelanom_Datalistfixedvalues ;
      private String Ddo_funcaoapfatributos_atrtabelanom_Datalistproc ;
      private String Ddo_funcaoapfatributos_atrtabelanom_Sortasc ;
      private String Ddo_funcaoapfatributos_atrtabelanom_Sortdsc ;
      private String Ddo_funcaoapfatributos_atrtabelanom_Loadingdata ;
      private String Ddo_funcaoapfatributos_atrtabelanom_Cleanfilter ;
      private String Ddo_funcaoapfatributos_atrtabelanom_Rangefilterfrom ;
      private String Ddo_funcaoapfatributos_atrtabelanom_Rangefilterto ;
      private String Ddo_funcaoapfatributos_atrtabelanom_Noresultsfound ;
      private String Ddo_funcaoapfatributos_atrtabelanom_Searchbuttontext ;
      private String Ddo_funcaoapfatributos_funcaodadosnom_Caption ;
      private String Ddo_funcaoapfatributos_funcaodadosnom_Tooltip ;
      private String Ddo_funcaoapfatributos_funcaodadosnom_Cls ;
      private String Ddo_funcaoapfatributos_funcaodadosnom_Filteredtext_set ;
      private String Ddo_funcaoapfatributos_funcaodadosnom_Selectedvalue_set ;
      private String Ddo_funcaoapfatributos_funcaodadosnom_Dropdownoptionstype ;
      private String Ddo_funcaoapfatributos_funcaodadosnom_Titlecontrolidtoreplace ;
      private String Ddo_funcaoapfatributos_funcaodadosnom_Sortedstatus ;
      private String Ddo_funcaoapfatributos_funcaodadosnom_Filtertype ;
      private String Ddo_funcaoapfatributos_funcaodadosnom_Datalisttype ;
      private String Ddo_funcaoapfatributos_funcaodadosnom_Datalistfixedvalues ;
      private String Ddo_funcaoapfatributos_funcaodadosnom_Datalistproc ;
      private String Ddo_funcaoapfatributos_funcaodadosnom_Sortasc ;
      private String Ddo_funcaoapfatributos_funcaodadosnom_Sortdsc ;
      private String Ddo_funcaoapfatributos_funcaodadosnom_Loadingdata ;
      private String Ddo_funcaoapfatributos_funcaodadosnom_Cleanfilter ;
      private String Ddo_funcaoapfatributos_funcaodadosnom_Rangefilterfrom ;
      private String Ddo_funcaoapfatributos_funcaodadosnom_Rangefilterto ;
      private String Ddo_funcaoapfatributos_funcaodadosnom_Noresultsfound ;
      private String Ddo_funcaoapfatributos_funcaodadosnom_Searchbuttontext ;
      private String GX_FocusControl ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String chkavDynamicfiltersenabled2_Internalname ;
      private String edtavTffuncaoapfatributos_atributosnom_Internalname ;
      private String edtavTffuncaoapfatributos_atributosnom_Jsonclick ;
      private String edtavTffuncaoapfatributos_atributosnom_sel_Internalname ;
      private String edtavTffuncaoapfatributos_atributosnom_sel_Jsonclick ;
      private String edtavTffuncaoapfatributos_atrtabelanom_Internalname ;
      private String edtavTffuncaoapfatributos_atrtabelanom_Jsonclick ;
      private String edtavTffuncaoapfatributos_atrtabelanom_sel_Internalname ;
      private String edtavTffuncaoapfatributos_atrtabelanom_sel_Jsonclick ;
      private String edtavTffuncaoapfatributos_funcaodadosnom_Internalname ;
      private String edtavTffuncaoapfatributos_funcaodadosnom_sel_Internalname ;
      private String edtavDdo_funcaoapfatributos_atributosnomtitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_funcaoapfatributos_atrtabelanomtitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_funcaoapfatributos_funcaodadosnomtitlecontrolidtoreplace_Internalname ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavDeleteatributo_Internalname ;
      private String edtFuncaoAPF_Codigo_Internalname ;
      private String edtFuncaoAPFAtributos_AtributosCod_Internalname ;
      private String A365FuncaoAPFAtributos_AtributosNom ;
      private String edtFuncaoAPFAtributos_AtributosNom_Internalname ;
      private String A367FuncaoAPFAtributos_AtrTabelaNom ;
      private String edtFuncaoAPFAtributos_AtrTabelaNom_Internalname ;
      private String edtFuncaoAPFAtributos_FuncaoDadosNom_Internalname ;
      private String AV37FuncaoAPFAtributos_FuncaoDadosTip ;
      private String GXCCtl ;
      private String cmbavOrderedby_Internalname ;
      private String scmdbuf ;
      private String lV21FuncaoAPFAtributos_AtributosNom1 ;
      private String lV23FuncaoAPFAtributos_AtrTabelaNom1 ;
      private String lV22FuncaoAPFAtributos_AtributosNom2 ;
      private String lV24FuncaoAPFAtributos_AtrTabelaNom2 ;
      private String lV43TFFuncaoAPFAtributos_AtributosNom ;
      private String lV47TFFuncaoAPFAtributos_AtrTabelaNom ;
      private String edtavOrdereddsc_Internalname ;
      private String cmbavDynamicfiltersselector1_Internalname ;
      private String edtavFuncaoapfatributos_atributosnom1_Internalname ;
      private String edtavFuncaoapfatributos_atrtabelanom1_Internalname ;
      private String cmbavDynamicfiltersselector2_Internalname ;
      private String edtavFuncaoapfatributos_atributosnom2_Internalname ;
      private String edtavFuncaoapfatributos_atrtabelanom2_Internalname ;
      private String lblJsdynamicfilters_Caption ;
      private String lblJsdynamicfilters_Internalname ;
      private String imgAdddynamicfilters1_Jsonclick ;
      private String imgAdddynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters1_Jsonclick ;
      private String imgRemovedynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters2_Jsonclick ;
      private String imgRemovedynamicfilters2_Internalname ;
      private String subGrid_Internalname ;
      private String Ddo_funcaoapfatributos_atributosnom_Internalname ;
      private String Ddo_funcaoapfatributos_atrtabelanom_Internalname ;
      private String Ddo_funcaoapfatributos_funcaodadosnom_Internalname ;
      private String imgCleanfilters_Jsonclick ;
      private String imgCleanfilters_Internalname ;
      private String edtFuncaoAPFAtributos_AtributosNom_Title ;
      private String edtFuncaoAPFAtributos_AtrTabelaNom_Title ;
      private String edtFuncaoAPFAtributos_FuncaoDadosNom_Title ;
      private String edtavDeleteatributo_Tooltiptext ;
      private String sStyleString ;
      private String tblTablegridheader_Internalname ;
      private String tblTablesearch_Internalname ;
      private String lblOrderedtext_Internalname ;
      private String lblOrderedtext_Jsonclick ;
      private String cmbavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Jsonclick ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTablefilters_Internalname ;
      private String lblJsdynamicfilters_Jsonclick ;
      private String tblTabledynamicfilters_Internalname ;
      private String lblDynamicfiltersprefix1_Internalname ;
      private String lblDynamicfiltersprefix1_Jsonclick ;
      private String cmbavDynamicfiltersselector1_Jsonclick ;
      private String lblDynamicfiltersmiddle1_Internalname ;
      private String lblDynamicfiltersmiddle1_Jsonclick ;
      private String edtavFuncaoapfatributos_atributosnom1_Jsonclick ;
      private String edtavFuncaoapfatributos_atrtabelanom1_Jsonclick ;
      private String lblDynamicfiltersprefix2_Internalname ;
      private String lblDynamicfiltersprefix2_Jsonclick ;
      private String cmbavDynamicfiltersselector2_Jsonclick ;
      private String lblDynamicfiltersmiddle2_Internalname ;
      private String lblDynamicfiltersmiddle2_Jsonclick ;
      private String edtavFuncaoapfatributos_atributosnom2_Jsonclick ;
      private String edtavFuncaoapfatributos_atrtabelanom2_Jsonclick ;
      private String tblTableactions_Internalname ;
      private String imgInsertatributo_Internalname ;
      private String imgInsertatributo_Jsonclick ;
      private String sCtrlAV19FuncaoAPF_Codigo ;
      private String sCtrlAV20FuncaoAPF_SistemaCod ;
      private String sGXsfl_57_fel_idx="0001" ;
      private String edtavDeleteatributo_Jsonclick ;
      private String ROClassString ;
      private String edtFuncaoAPF_Codigo_Jsonclick ;
      private String edtFuncaoAPFAtributos_AtributosCod_Jsonclick ;
      private String edtFuncaoAPFAtributos_AtributosNom_Jsonclick ;
      private String edtFuncaoAPFAtributos_AtrTabelaNom_Jsonclick ;
      private String edtFuncaoAPFAtributos_FuncaoDadosNom_Jsonclick ;
      private String cmbavFuncaoapfatributos_funcaodadostip_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private bool entryPointCalled ;
      private bool AV31OrderedDsc ;
      private bool AV9DynamicFiltersEnabled2 ;
      private bool AV11DynamicFiltersIgnoreFirst ;
      private bool AV15DynamicFiltersRemoving ;
      private bool n393FuncaoAPFAtributos_SistemaCod ;
      private bool n415FuncaoAPFAtributos_FuncaoDadosTip ;
      private bool n378FuncaoAPFAtributos_FuncaoDadosCod ;
      private bool n391FuncaoDados_FuncaoDadosCod ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool Ddo_funcaoapfatributos_atributosnom_Includesortasc ;
      private bool Ddo_funcaoapfatributos_atributosnom_Includesortdsc ;
      private bool Ddo_funcaoapfatributos_atributosnom_Includefilter ;
      private bool Ddo_funcaoapfatributos_atributosnom_Filterisrange ;
      private bool Ddo_funcaoapfatributos_atributosnom_Includedatalist ;
      private bool Ddo_funcaoapfatributos_atrtabelanom_Includesortasc ;
      private bool Ddo_funcaoapfatributos_atrtabelanom_Includesortdsc ;
      private bool Ddo_funcaoapfatributos_atrtabelanom_Includefilter ;
      private bool Ddo_funcaoapfatributos_atrtabelanom_Filterisrange ;
      private bool Ddo_funcaoapfatributos_atrtabelanom_Includedatalist ;
      private bool Ddo_funcaoapfatributos_funcaodadosnom_Includesortasc ;
      private bool Ddo_funcaoapfatributos_funcaodadosnom_Includesortdsc ;
      private bool Ddo_funcaoapfatributos_funcaodadosnom_Includefilter ;
      private bool Ddo_funcaoapfatributos_funcaodadosnom_Filterisrange ;
      private bool Ddo_funcaoapfatributos_funcaodadosnom_Includedatalist ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n365FuncaoAPFAtributos_AtributosNom ;
      private bool n367FuncaoAPFAtributos_AtrTabelaNom ;
      private bool n412FuncaoAPFAtributos_FuncaoDadosNom ;
      private bool A180Atributos_Ativo ;
      private bool n366FuncaoAPFAtributos_AtrTabelaCod ;
      private bool n180Atributos_Ativo ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV39DeleteAtributo_IsBlob ;
      private String AV16DynamicFiltersSelector1 ;
      private String AV17DynamicFiltersSelector2 ;
      private String AV51TFFuncaoAPFAtributos_FuncaoDadosNom ;
      private String AV52TFFuncaoAPFAtributos_FuncaoDadosNom_Sel ;
      private String AV45ddo_FuncaoAPFAtributos_AtributosNomTitleControlIdToReplace ;
      private String AV49ddo_FuncaoAPFAtributos_AtrTabelaNomTitleControlIdToReplace ;
      private String AV53ddo_FuncaoAPFAtributos_FuncaoDadosNomTitleControlIdToReplace ;
      private String AV61Deleteatributo_GXI ;
      private String A412FuncaoAPFAtributos_FuncaoDadosNom ;
      private String lV51TFFuncaoAPFAtributos_FuncaoDadosNom ;
      private String AV39DeleteAtributo ;
      private IGxSession AV32Session ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbavOrderedby ;
      private GXCombobox cmbavDynamicfiltersselector1 ;
      private GXCombobox cmbavDynamicfiltersselector2 ;
      private GXCombobox cmbavFuncaoapfatributos_funcaodadostip ;
      private GXCheckbox chkavDynamicfiltersenabled2 ;
      private IDataStoreProvider pr_default ;
      private int[] H00982_A366FuncaoAPFAtributos_AtrTabelaCod ;
      private bool[] H00982_n366FuncaoAPFAtributos_AtrTabelaCod ;
      private bool[] H00982_A180Atributos_Ativo ;
      private bool[] H00982_n180Atributos_Ativo ;
      private int[] H00982_A393FuncaoAPFAtributos_SistemaCod ;
      private bool[] H00982_n393FuncaoAPFAtributos_SistemaCod ;
      private String[] H00982_A415FuncaoAPFAtributos_FuncaoDadosTip ;
      private bool[] H00982_n415FuncaoAPFAtributos_FuncaoDadosTip ;
      private int[] H00982_A378FuncaoAPFAtributos_FuncaoDadosCod ;
      private bool[] H00982_n378FuncaoAPFAtributos_FuncaoDadosCod ;
      private String[] H00982_A412FuncaoAPFAtributos_FuncaoDadosNom ;
      private bool[] H00982_n412FuncaoAPFAtributos_FuncaoDadosNom ;
      private String[] H00982_A367FuncaoAPFAtributos_AtrTabelaNom ;
      private bool[] H00982_n367FuncaoAPFAtributos_AtrTabelaNom ;
      private String[] H00982_A365FuncaoAPFAtributos_AtributosNom ;
      private bool[] H00982_n365FuncaoAPFAtributos_AtributosNom ;
      private int[] H00982_A364FuncaoAPFAtributos_AtributosCod ;
      private int[] H00982_A165FuncaoAPF_Codigo ;
      private long[] H00983_AGRID_nRecordCount ;
      private int[] H00984_A368FuncaoDados_Codigo ;
      private int[] H00984_A391FuncaoDados_FuncaoDadosCod ;
      private bool[] H00984_n391FuncaoDados_FuncaoDadosCod ;
      private String[] H00984_A373FuncaoDados_Tipo ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV28HTTPRequest ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV42FuncaoAPFAtributos_AtributosNomTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV46FuncaoAPFAtributos_AtrTabelaNomTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV50FuncaoAPFAtributos_FuncaoDadosNomTitleFilterData ;
      private wwpbaseobjects.SdtWWPGridState AV25GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV27GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV26GridStateDynamicFilter ;
      private wwpbaseobjects.SdtWWPTransactionContext AV33TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV34TrnContextAtt ;
      private wwpbaseobjects.SdtWWPContext AV36WWPContext ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV54DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 ;
   }

   public class funcaoapffuncoesapfatributoswc__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00982( IGxContext context ,
                                             String AV16DynamicFiltersSelector1 ,
                                             String AV21FuncaoAPFAtributos_AtributosNom1 ,
                                             String AV23FuncaoAPFAtributos_AtrTabelaNom1 ,
                                             bool AV9DynamicFiltersEnabled2 ,
                                             String AV17DynamicFiltersSelector2 ,
                                             String AV22FuncaoAPFAtributos_AtributosNom2 ,
                                             String AV24FuncaoAPFAtributos_AtrTabelaNom2 ,
                                             String AV44TFFuncaoAPFAtributos_AtributosNom_Sel ,
                                             String AV43TFFuncaoAPFAtributos_AtributosNom ,
                                             String AV48TFFuncaoAPFAtributos_AtrTabelaNom_Sel ,
                                             String AV47TFFuncaoAPFAtributos_AtrTabelaNom ,
                                             String AV52TFFuncaoAPFAtributos_FuncaoDadosNom_Sel ,
                                             String AV51TFFuncaoAPFAtributos_FuncaoDadosNom ,
                                             String A365FuncaoAPFAtributos_AtributosNom ,
                                             String A367FuncaoAPFAtributos_AtrTabelaNom ,
                                             String A412FuncaoAPFAtributos_FuncaoDadosNom ,
                                             short AV30OrderedBy ,
                                             bool AV31OrderedDsc ,
                                             int A165FuncaoAPF_Codigo ,
                                             int AV19FuncaoAPF_Codigo ,
                                             bool A180Atributos_Ativo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [16] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " T3.[Atributos_TabelaCod] AS FuncaoAPFAtributos_AtrTabelaCod, T3.[Atributos_Ativo], T4.[Tabela_SistemaCod] AS FuncaoAPFAtributos_SistemaCod, T2.[FuncaoDados_Tipo] AS FuncaoAPFAtributos_FuncaoDadosTip, T1.[FuncaoAPFAtributos_FuncaoDadosCod] AS FuncaoAPFAtributos_FuncaoDadosCod, T2.[FuncaoDados_Nome] AS FuncaoAPFAtributos_FuncaoDadosNom, T4.[Tabela_Nome] AS FuncaoAPFAtributos_AtrTabelaNom, T3.[Atributos_Nome] AS FuncaoAPFAtributos_AtributosNom, T1.[FuncaoAPFAtributos_AtributosCod] AS FuncaoAPFAtributos_AtributosCod, T1.[FuncaoAPF_Codigo]";
         sFromString = " FROM ((([FuncoesAPFAtributos] T1 WITH (NOLOCK) LEFT JOIN [FuncaoDados] T2 WITH (NOLOCK) ON T2.[FuncaoDados_Codigo] = T1.[FuncaoAPFAtributos_FuncaoDadosCod]) INNER JOIN [Atributos] T3 WITH (NOLOCK) ON T3.[Atributos_Codigo] = T1.[FuncaoAPFAtributos_AtributosCod]) LEFT JOIN [Tabela] T4 WITH (NOLOCK) ON T4.[Tabela_Codigo] = T3.[Atributos_TabelaCod])";
         sOrderString = "";
         sWhereString = sWhereString + " WHERE (T1.[FuncaoAPF_Codigo] = @AV19FuncaoAPF_Codigo)";
         sWhereString = sWhereString + " and (T3.[Atributos_Ativo] = 1)";
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV21FuncaoAPFAtributos_AtributosNom1)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Atributos_Nome] like '%' + @lV21FuncaoAPFAtributos_AtributosNom1)";
         }
         else
         {
            GXv_int2[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "FUNCAOAPFATRIBUTOS_ATRTABELANOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV23FuncaoAPFAtributos_AtrTabelaNom1)) ) )
         {
            sWhereString = sWhereString + " and (T4.[Tabela_Nome] like '%' + @lV23FuncaoAPFAtributos_AtrTabelaNom1)";
         }
         else
         {
            GXv_int2[2] = 1;
         }
         if ( AV9DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV17DynamicFiltersSelector2, "FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV22FuncaoAPFAtributos_AtributosNom2)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Atributos_Nome] like '%' + @lV22FuncaoAPFAtributos_AtributosNom2)";
         }
         else
         {
            GXv_int2[3] = 1;
         }
         if ( AV9DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV17DynamicFiltersSelector2, "FUNCAOAPFATRIBUTOS_ATRTABELANOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV24FuncaoAPFAtributos_AtrTabelaNom2)) ) )
         {
            sWhereString = sWhereString + " and (T4.[Tabela_Nome] like '%' + @lV24FuncaoAPFAtributos_AtrTabelaNom2)";
         }
         else
         {
            GXv_int2[4] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV44TFFuncaoAPFAtributos_AtributosNom_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV43TFFuncaoAPFAtributos_AtributosNom)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Atributos_Nome] like @lV43TFFuncaoAPFAtributos_AtributosNom)";
         }
         else
         {
            GXv_int2[5] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV44TFFuncaoAPFAtributos_AtributosNom_Sel)) )
         {
            sWhereString = sWhereString + " and (T3.[Atributos_Nome] = @AV44TFFuncaoAPFAtributos_AtributosNom_Sel)";
         }
         else
         {
            GXv_int2[6] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV48TFFuncaoAPFAtributos_AtrTabelaNom_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV47TFFuncaoAPFAtributos_AtrTabelaNom)) ) )
         {
            sWhereString = sWhereString + " and (T4.[Tabela_Nome] like @lV47TFFuncaoAPFAtributos_AtrTabelaNom)";
         }
         else
         {
            GXv_int2[7] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV48TFFuncaoAPFAtributos_AtrTabelaNom_Sel)) )
         {
            sWhereString = sWhereString + " and (T4.[Tabela_Nome] = @AV48TFFuncaoAPFAtributos_AtrTabelaNom_Sel)";
         }
         else
         {
            GXv_int2[8] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV52TFFuncaoAPFAtributos_FuncaoDadosNom_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV51TFFuncaoAPFAtributos_FuncaoDadosNom)) ) )
         {
            sWhereString = sWhereString + " and (T2.[FuncaoDados_Nome] like @lV51TFFuncaoAPFAtributos_FuncaoDadosNom)";
         }
         else
         {
            GXv_int2[9] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV52TFFuncaoAPFAtributos_FuncaoDadosNom_Sel)) )
         {
            sWhereString = sWhereString + " and (T2.[FuncaoDados_Nome] = @AV52TFFuncaoAPFAtributos_FuncaoDadosNom_Sel)";
         }
         else
         {
            GXv_int2[10] = 1;
         }
         if ( AV30OrderedBy == 1 )
         {
            sOrderString = sOrderString + " ORDER BY T2.[FuncaoDados_Nome], T4.[Tabela_Nome], T3.[Atributos_Nome]";
         }
         else if ( ( AV30OrderedBy == 2 ) && ! AV31OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[FuncaoAPF_Codigo], T3.[Atributos_Nome]";
         }
         else if ( ( AV30OrderedBy == 2 ) && ( AV31OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[FuncaoAPF_Codigo] DESC, T3.[Atributos_Nome] DESC";
         }
         else if ( ( AV30OrderedBy == 3 ) && ! AV31OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[FuncaoAPF_Codigo], T4.[Tabela_Nome]";
         }
         else if ( ( AV30OrderedBy == 3 ) && ( AV31OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[FuncaoAPF_Codigo] DESC, T4.[Tabela_Nome] DESC";
         }
         else if ( ( AV30OrderedBy == 4 ) && ! AV31OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[FuncaoAPF_Codigo], T2.[FuncaoDados_Nome]";
         }
         else if ( ( AV30OrderedBy == 4 ) && ( AV31OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[FuncaoAPF_Codigo] DESC, T2.[FuncaoDados_Nome] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY T1.[FuncaoAPF_Codigo], T1.[FuncaoAPFAtributos_AtributosCod]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      protected Object[] conditional_H00983( IGxContext context ,
                                             String AV16DynamicFiltersSelector1 ,
                                             String AV21FuncaoAPFAtributos_AtributosNom1 ,
                                             String AV23FuncaoAPFAtributos_AtrTabelaNom1 ,
                                             bool AV9DynamicFiltersEnabled2 ,
                                             String AV17DynamicFiltersSelector2 ,
                                             String AV22FuncaoAPFAtributos_AtributosNom2 ,
                                             String AV24FuncaoAPFAtributos_AtrTabelaNom2 ,
                                             String AV44TFFuncaoAPFAtributos_AtributosNom_Sel ,
                                             String AV43TFFuncaoAPFAtributos_AtributosNom ,
                                             String AV48TFFuncaoAPFAtributos_AtrTabelaNom_Sel ,
                                             String AV47TFFuncaoAPFAtributos_AtrTabelaNom ,
                                             String AV52TFFuncaoAPFAtributos_FuncaoDadosNom_Sel ,
                                             String AV51TFFuncaoAPFAtributos_FuncaoDadosNom ,
                                             String A365FuncaoAPFAtributos_AtributosNom ,
                                             String A367FuncaoAPFAtributos_AtrTabelaNom ,
                                             String A412FuncaoAPFAtributos_FuncaoDadosNom ,
                                             short AV30OrderedBy ,
                                             bool AV31OrderedDsc ,
                                             int A165FuncaoAPF_Codigo ,
                                             int AV19FuncaoAPF_Codigo ,
                                             bool A180Atributos_Ativo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [11] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM ((([FuncoesAPFAtributos] T1 WITH (NOLOCK) LEFT JOIN [FuncaoDados] T4 WITH (NOLOCK) ON T4.[FuncaoDados_Codigo] = T1.[FuncaoAPFAtributos_FuncaoDadosCod]) INNER JOIN [Atributos] T2 WITH (NOLOCK) ON T2.[Atributos_Codigo] = T1.[FuncaoAPFAtributos_AtributosCod]) LEFT JOIN [Tabela] T3 WITH (NOLOCK) ON T3.[Tabela_Codigo] = T2.[Atributos_TabelaCod])";
         scmdbuf = scmdbuf + " WHERE (T1.[FuncaoAPF_Codigo] = @AV19FuncaoAPF_Codigo)";
         scmdbuf = scmdbuf + " and (T2.[Atributos_Ativo] = 1)";
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV21FuncaoAPFAtributos_AtributosNom1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Atributos_Nome] like '%' + @lV21FuncaoAPFAtributos_AtributosNom1)";
         }
         else
         {
            GXv_int4[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "FUNCAOAPFATRIBUTOS_ATRTABELANOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV23FuncaoAPFAtributos_AtrTabelaNom1)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Tabela_Nome] like '%' + @lV23FuncaoAPFAtributos_AtrTabelaNom1)";
         }
         else
         {
            GXv_int4[2] = 1;
         }
         if ( AV9DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV17DynamicFiltersSelector2, "FUNCAOAPFATRIBUTOS_ATRIBUTOSNOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV22FuncaoAPFAtributos_AtributosNom2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Atributos_Nome] like '%' + @lV22FuncaoAPFAtributos_AtributosNom2)";
         }
         else
         {
            GXv_int4[3] = 1;
         }
         if ( AV9DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV17DynamicFiltersSelector2, "FUNCAOAPFATRIBUTOS_ATRTABELANOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV24FuncaoAPFAtributos_AtrTabelaNom2)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Tabela_Nome] like '%' + @lV24FuncaoAPFAtributos_AtrTabelaNom2)";
         }
         else
         {
            GXv_int4[4] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV44TFFuncaoAPFAtributos_AtributosNom_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV43TFFuncaoAPFAtributos_AtributosNom)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Atributos_Nome] like @lV43TFFuncaoAPFAtributos_AtributosNom)";
         }
         else
         {
            GXv_int4[5] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV44TFFuncaoAPFAtributos_AtributosNom_Sel)) )
         {
            sWhereString = sWhereString + " and (T2.[Atributos_Nome] = @AV44TFFuncaoAPFAtributos_AtributosNom_Sel)";
         }
         else
         {
            GXv_int4[6] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV48TFFuncaoAPFAtributos_AtrTabelaNom_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV47TFFuncaoAPFAtributos_AtrTabelaNom)) ) )
         {
            sWhereString = sWhereString + " and (T3.[Tabela_Nome] like @lV47TFFuncaoAPFAtributos_AtrTabelaNom)";
         }
         else
         {
            GXv_int4[7] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV48TFFuncaoAPFAtributos_AtrTabelaNom_Sel)) )
         {
            sWhereString = sWhereString + " and (T3.[Tabela_Nome] = @AV48TFFuncaoAPFAtributos_AtrTabelaNom_Sel)";
         }
         else
         {
            GXv_int4[8] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV52TFFuncaoAPFAtributos_FuncaoDadosNom_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV51TFFuncaoAPFAtributos_FuncaoDadosNom)) ) )
         {
            sWhereString = sWhereString + " and (T4.[FuncaoDados_Nome] like @lV51TFFuncaoAPFAtributos_FuncaoDadosNom)";
         }
         else
         {
            GXv_int4[9] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV52TFFuncaoAPFAtributos_FuncaoDadosNom_Sel)) )
         {
            sWhereString = sWhereString + " and (T4.[FuncaoDados_Nome] = @AV52TFFuncaoAPFAtributos_FuncaoDadosNom_Sel)";
         }
         else
         {
            GXv_int4[10] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         if ( AV30OrderedBy == 1 )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV30OrderedBy == 2 ) && ! AV31OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV30OrderedBy == 2 ) && ( AV31OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV30OrderedBy == 3 ) && ! AV31OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV30OrderedBy == 3 ) && ( AV31OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV30OrderedBy == 4 ) && ! AV31OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV30OrderedBy == 4 ) && ( AV31OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H00982(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (String)dynConstraints[2] , (bool)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (short)dynConstraints[16] , (bool)dynConstraints[17] , (int)dynConstraints[18] , (int)dynConstraints[19] , (bool)dynConstraints[20] );
               case 1 :
                     return conditional_H00983(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (String)dynConstraints[2] , (bool)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (short)dynConstraints[16] , (bool)dynConstraints[17] , (int)dynConstraints[18] , (int)dynConstraints[19] , (bool)dynConstraints[20] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00984 ;
          prmH00984 = new Object[] {
          new Object[] {"@AV38FuncaoAPFAtributos_FuncaoDadosCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00982 ;
          prmH00982 = new Object[] {
          new Object[] {"@AV19FuncaoAPF_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV21FuncaoAPFAtributos_AtributosNom1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV23FuncaoAPFAtributos_AtrTabelaNom1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV22FuncaoAPFAtributos_AtributosNom2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV24FuncaoAPFAtributos_AtrTabelaNom2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV43TFFuncaoAPFAtributos_AtributosNom",SqlDbType.Char,50,0} ,
          new Object[] {"@AV44TFFuncaoAPFAtributos_AtributosNom_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV47TFFuncaoAPFAtributos_AtrTabelaNom",SqlDbType.Char,50,0} ,
          new Object[] {"@AV48TFFuncaoAPFAtributos_AtrTabelaNom_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV51TFFuncaoAPFAtributos_FuncaoDadosNom",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV52TFFuncaoAPFAtributos_FuncaoDadosNom_Sel",SqlDbType.VarChar,200,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH00983 ;
          prmH00983 = new Object[] {
          new Object[] {"@AV19FuncaoAPF_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV21FuncaoAPFAtributos_AtributosNom1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV23FuncaoAPFAtributos_AtrTabelaNom1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV22FuncaoAPFAtributos_AtributosNom2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV24FuncaoAPFAtributos_AtrTabelaNom2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV43TFFuncaoAPFAtributos_AtributosNom",SqlDbType.Char,50,0} ,
          new Object[] {"@AV44TFFuncaoAPFAtributos_AtributosNom_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV47TFFuncaoAPFAtributos_AtrTabelaNom",SqlDbType.Char,50,0} ,
          new Object[] {"@AV48TFFuncaoAPFAtributos_AtrTabelaNom_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV51TFFuncaoAPFAtributos_FuncaoDadosNom",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV52TFFuncaoAPFAtributos_FuncaoDadosNom_Sel",SqlDbType.VarChar,200,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00982", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00982,11,0,true,false )
             ,new CursorDef("H00983", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00983,1,0,true,false )
             ,new CursorDef("H00984", "SELECT [FuncaoDados_Codigo], [FuncaoDados_FuncaoDadosCod], [FuncaoDados_Tipo] FROM [FuncaoDados] WITH (NOLOCK) WHERE [FuncaoDados_FuncaoDadosCod] = @AV38FuncaoAPFAtributos_FuncaoDadosCod ORDER BY [FuncaoDados_FuncaoDadosCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00984,100,0,false,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((bool[]) buf[2])[0] = rslt.getBool(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((String[]) buf[6])[0] = rslt.getString(4, 3) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((int[]) buf[8])[0] = rslt.getInt(5) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(5);
                ((String[]) buf[10])[0] = rslt.getVarchar(6) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(6);
                ((String[]) buf[12])[0] = rslt.getString(7, 50) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(7);
                ((String[]) buf[14])[0] = rslt.getString(8, 50) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(8);
                ((int[]) buf[16])[0] = rslt.getInt(9) ;
                ((int[]) buf[17])[0] = rslt.getInt(10) ;
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getString(3, 3) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[16]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[27]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[28]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[29]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[30]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[31]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[11]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
