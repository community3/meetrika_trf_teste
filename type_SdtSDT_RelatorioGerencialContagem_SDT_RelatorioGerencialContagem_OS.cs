/*
               File: type_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS
        Description: SDT_RelatorioGerencialContagem
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/29/2020 0:30:7.29
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "SDT_RelatorioGerencialContagem.SDT_RelatorioGerencialContagem.OS" )]
   [XmlType(TypeName =  "SDT_RelatorioGerencialContagem.SDT_RelatorioGerencialContagem.OS" , Namespace = "GxEv3Up14_MeetrikaVs3" )]
   [System.Xml.Serialization.XmlInclude( typeof( SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_OSVinculada ))]
   [Serializable]
   public class SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS : GxUserType
   {
      public SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS( )
      {
         /* Constructor for serialization */
         gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_datadmn = DateTime.MinValue;
         gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_descricao = "";
         gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_nnref = "";
         gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_demanda = "";
         gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_demandafm = "";
         gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_statusdmn = "";
         gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_statusdescricao = "";
         gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_servicosigla = "";
         gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_datacontageminicial = DateTime.MinValue;
         gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_datacontagemfinal = DateTime.MinValue;
         gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_dmnvinculada = "";
      }

      public SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS( IGxContext context )
      {
         this.context = context;
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         xmls = new XmlSerializer(this.GetType(), sNameSpace);
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "GxEv3Up14_MeetrikaVs3" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS obj ;
         obj = this;
         obj.gxTpr_Contagemresultado_codigo = deserialized.gxTpr_Contagemresultado_codigo;
         obj.gxTpr_Contagemresultado_datadmn = deserialized.gxTpr_Contagemresultado_datadmn;
         obj.gxTpr_Contagemresultado_descricao = deserialized.gxTpr_Contagemresultado_descricao;
         obj.gxTpr_Contagemresultado_nnref = deserialized.gxTpr_Contagemresultado_nnref;
         obj.gxTpr_Contagemresultado_demanda = deserialized.gxTpr_Contagemresultado_demanda;
         obj.gxTpr_Contagemresultado_demandafm = deserialized.gxTpr_Contagemresultado_demandafm;
         obj.gxTpr_Contagemresultado_statusdmn = deserialized.gxTpr_Contagemresultado_statusdmn;
         obj.gxTpr_Contagemresultado_statusdescricao = deserialized.gxTpr_Contagemresultado_statusdescricao;
         obj.gxTpr_Contagemresultado_cntsrvcod = deserialized.gxTpr_Contagemresultado_cntsrvcod;
         obj.gxTpr_Contagemresultado_servicosigla = deserialized.gxTpr_Contagemresultado_servicosigla;
         obj.gxTpr_Contagemresultado_datacontageminicial = deserialized.gxTpr_Contagemresultado_datacontageminicial;
         obj.gxTpr_Contagemresultado_pflfs_inicial = deserialized.gxTpr_Contagemresultado_pflfs_inicial;
         obj.gxTpr_Contagemresultado_datacontagemfinal = deserialized.gxTpr_Contagemresultado_datacontagemfinal;
         obj.gxTpr_Contagemresultado_pflfs_final = deserialized.gxTpr_Contagemresultado_pflfs_final;
         obj.gxTpr_Contagemresultado_pflfs_diferenca = deserialized.gxTpr_Contagemresultado_pflfs_diferenca;
         obj.gxTpr_Contagemresultado_osvinculada = deserialized.gxTpr_Contagemresultado_osvinculada;
         obj.gxTpr_Contagemresultado_dmnvinculada = deserialized.gxTpr_Contagemresultado_dmnvinculada;
         obj.gxTpr_Contagemresultado_pf = deserialized.gxTpr_Contagemresultado_pf;
         obj.gxTpr_Osvinculada = deserialized.gxTpr_Osvinculada;
         obj.gxTpr_Contagemresultado_pflfs_inicial_total = deserialized.gxTpr_Contagemresultado_pflfs_inicial_total;
         obj.gxTpr_Contagemresultado_pflfs_final_total = deserialized.gxTpr_Contagemresultado_pflfs_final_total;
         obj.gxTpr_Contagemresultado_pflfs_diferenca_total = deserialized.gxTpr_Contagemresultado_pflfs_diferenca_total;
         obj.gxTpr_Contagemresultado_pf_total = deserialized.gxTpr_Contagemresultado_pf_total;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_Codigo") )
               {
                  gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_codigo = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_DataDmn") )
               {
                  if ( ( StringUtil.StrCmp(oReader.Value, "0000-00-00") == 0 ) || ( oReader.ExistsAttribute("xsi:nil") == 1 ) )
                  {
                     gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_datadmn = DateTime.MinValue;
                  }
                  else
                  {
                     gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_datadmn = context.localUtil.YMDToD( (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 1, 4), ".")), (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 6, 2), ".")), (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 9, 2), ".")));
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_Descricao") )
               {
                  gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_descricao = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_NNRef") )
               {
                  gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_nnref = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_Demanda") )
               {
                  gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_demanda = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_DemandaFM") )
               {
                  gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_demandafm = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_StatusDmn") )
               {
                  gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_statusdmn = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_StatusDescricao") )
               {
                  gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_statusdescricao = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_CntSrvCod") )
               {
                  gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_cntsrvcod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_ServicoSigla") )
               {
                  gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_servicosigla = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_DataContagemInicial") )
               {
                  if ( ( StringUtil.StrCmp(oReader.Value, "0000-00-00") == 0 ) || ( oReader.ExistsAttribute("xsi:nil") == 1 ) )
                  {
                     gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_datacontageminicial = DateTime.MinValue;
                  }
                  else
                  {
                     gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_datacontageminicial = context.localUtil.YMDToD( (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 1, 4), ".")), (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 6, 2), ".")), (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 9, 2), ".")));
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_PFLFS_Inicial") )
               {
                  gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_pflfs_inicial = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_DataContagemFinal") )
               {
                  if ( ( StringUtil.StrCmp(oReader.Value, "0000-00-00") == 0 ) || ( oReader.ExistsAttribute("xsi:nil") == 1 ) )
                  {
                     gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_datacontagemfinal = DateTime.MinValue;
                  }
                  else
                  {
                     gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_datacontagemfinal = context.localUtil.YMDToD( (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 1, 4), ".")), (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 6, 2), ".")), (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 9, 2), ".")));
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_PFLFS_Final") )
               {
                  gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_pflfs_final = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_PFLFS_Diferenca") )
               {
                  gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_pflfs_diferenca = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_OSVinculada") )
               {
                  gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_osvinculada = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_DmnVinculada") )
               {
                  gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_dmnvinculada = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_PF") )
               {
                  gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_pf = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "OSVinculada") )
               {
                  if ( gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Osvinculada == null )
                  {
                     gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Osvinculada = new GxObjectCollection( context, "SDT_RelatorioGerencialContagem.SDT_RelatorioGerencialContagem.OS.OSVinculada", "", "SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_OSVinculada", "GeneXus.Programs");
                  }
                  if ( oReader.IsSimple == 0 )
                  {
                     GXSoapError = gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Osvinculada.readxmlcollection(oReader, "OSVinculada", "OSVinculada");
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_PFLFS_Inicial_Total") )
               {
                  gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_pflfs_inicial_total = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_PFLFS_Final_Total") )
               {
                  gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_pflfs_final_total = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_PFLFS_Diferenca_Total") )
               {
                  gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_pflfs_diferenca_total = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_PF_Total") )
               {
                  gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_pf_total = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "SDT_RelatorioGerencialContagem.SDT_RelatorioGerencialContagem.OS";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("ContagemResultado_Codigo", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_codigo), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         if ( (DateTime.MinValue==gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_datadmn) )
         {
            oWriter.WriteStartElement("ContagemResultado_DataDmn");
            oWriter.WriteAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
            oWriter.WriteAttribute("xsi:nil", "true");
            oWriter.WriteEndElement();
         }
         else
         {
            sDateCnv = "";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_datadmn)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_datadmn)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_datadmn)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            oWriter.WriteElement("ContagemResultado_DataDmn", sDateCnv);
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
         }
         oWriter.WriteElement("ContagemResultado_Descricao", StringUtil.RTrim( gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_descricao));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContagemResultado_NNRef", StringUtil.RTrim( gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_nnref));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContagemResultado_Demanda", StringUtil.RTrim( gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_demanda));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContagemResultado_DemandaFM", StringUtil.RTrim( gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_demandafm));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContagemResultado_StatusDmn", StringUtil.RTrim( gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_statusdmn));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContagemResultado_StatusDescricao", StringUtil.RTrim( gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_statusdescricao));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContagemResultado_CntSrvCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_cntsrvcod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContagemResultado_ServicoSigla", StringUtil.RTrim( gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_servicosigla));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         if ( (DateTime.MinValue==gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_datacontageminicial) )
         {
            oWriter.WriteStartElement("ContagemResultado_DataContagemInicial");
            oWriter.WriteAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
            oWriter.WriteAttribute("xsi:nil", "true");
            oWriter.WriteEndElement();
         }
         else
         {
            sDateCnv = "";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_datacontageminicial)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_datacontageminicial)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_datacontageminicial)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            oWriter.WriteElement("ContagemResultado_DataContagemInicial", sDateCnv);
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
         }
         oWriter.WriteElement("ContagemResultado_PFLFS_Inicial", StringUtil.Trim( StringUtil.Str( gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_pflfs_inicial, 14, 5)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         if ( (DateTime.MinValue==gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_datacontagemfinal) )
         {
            oWriter.WriteStartElement("ContagemResultado_DataContagemFinal");
            oWriter.WriteAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
            oWriter.WriteAttribute("xsi:nil", "true");
            oWriter.WriteEndElement();
         }
         else
         {
            sDateCnv = "";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_datacontagemfinal)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_datacontagemfinal)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_datacontagemfinal)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            oWriter.WriteElement("ContagemResultado_DataContagemFinal", sDateCnv);
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
         }
         oWriter.WriteElement("ContagemResultado_PFLFS_Final", StringUtil.Trim( StringUtil.Str( gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_pflfs_final, 14, 5)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContagemResultado_PFLFS_Diferenca", StringUtil.Trim( StringUtil.Str( gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_pflfs_diferenca, 14, 5)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContagemResultado_OSVinculada", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_osvinculada), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContagemResultado_DmnVinculada", StringUtil.RTrim( gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_dmnvinculada));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContagemResultado_PF", StringUtil.Trim( StringUtil.Str( gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_pf, 14, 5)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         if ( gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Osvinculada != null )
         {
            String sNameSpace1 ;
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") == 0 )
            {
               sNameSpace1 = "[*:nosend]" + "GxEv3Up14_MeetrikaVs3";
            }
            else
            {
               sNameSpace1 = "GxEv3Up14_MeetrikaVs3";
            }
            gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Osvinculada.writexmlcollection(oWriter, "OSVinculada", sNameSpace1, "OSVinculada", sNameSpace1);
         }
         oWriter.WriteElement("ContagemResultado_PFLFS_Inicial_Total", StringUtil.Trim( StringUtil.Str( gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_pflfs_inicial_total, 14, 5)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContagemResultado_PFLFS_Final_Total", StringUtil.Trim( StringUtil.Str( gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_pflfs_final_total, 14, 5)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContagemResultado_PFLFS_Diferenca_Total", StringUtil.Trim( StringUtil.Str( gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_pflfs_diferenca_total, 14, 5)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContagemResultado_PF_Total", StringUtil.Trim( StringUtil.Str( gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_pf_total, 14, 5)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("ContagemResultado_Codigo", gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_codigo, false);
         sDateCnv = "";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_datadmn)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_datadmn)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_datadmn)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         AddObjectProperty("ContagemResultado_DataDmn", sDateCnv, false);
         AddObjectProperty("ContagemResultado_Descricao", gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_descricao, false);
         AddObjectProperty("ContagemResultado_NNRef", gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_nnref, false);
         AddObjectProperty("ContagemResultado_Demanda", gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_demanda, false);
         AddObjectProperty("ContagemResultado_DemandaFM", gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_demandafm, false);
         AddObjectProperty("ContagemResultado_StatusDmn", gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_statusdmn, false);
         AddObjectProperty("ContagemResultado_StatusDescricao", gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_statusdescricao, false);
         AddObjectProperty("ContagemResultado_CntSrvCod", gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_cntsrvcod, false);
         AddObjectProperty("ContagemResultado_ServicoSigla", gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_servicosigla, false);
         sDateCnv = "";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_datacontageminicial)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_datacontageminicial)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_datacontageminicial)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         AddObjectProperty("ContagemResultado_DataContagemInicial", sDateCnv, false);
         AddObjectProperty("ContagemResultado_PFLFS_Inicial", gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_pflfs_inicial, false);
         sDateCnv = "";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_datacontagemfinal)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_datacontagemfinal)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_datacontagemfinal)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         AddObjectProperty("ContagemResultado_DataContagemFinal", sDateCnv, false);
         AddObjectProperty("ContagemResultado_PFLFS_Final", gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_pflfs_final, false);
         AddObjectProperty("ContagemResultado_PFLFS_Diferenca", gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_pflfs_diferenca, false);
         AddObjectProperty("ContagemResultado_OSVinculada", gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_osvinculada, false);
         AddObjectProperty("ContagemResultado_DmnVinculada", gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_dmnvinculada, false);
         AddObjectProperty("ContagemResultado_PF", gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_pf, false);
         if ( gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Osvinculada != null )
         {
            AddObjectProperty("OSVinculada", gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Osvinculada, false);
         }
         AddObjectProperty("ContagemResultado_PFLFS_Inicial_Total", gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_pflfs_inicial_total, false);
         AddObjectProperty("ContagemResultado_PFLFS_Final_Total", gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_pflfs_final_total, false);
         AddObjectProperty("ContagemResultado_PFLFS_Diferenca_Total", gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_pflfs_diferenca_total, false);
         AddObjectProperty("ContagemResultado_PF_Total", gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_pf_total, false);
         return  ;
      }

      [  SoapElement( ElementName = "ContagemResultado_Codigo" )]
      [  XmlElement( ElementName = "ContagemResultado_Codigo"   )]
      public int gxTpr_Contagemresultado_codigo
      {
         get {
            return gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_codigo ;
         }

         set {
            gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_codigo = (int)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_DataDmn" )]
      [  XmlElement( ElementName = "ContagemResultado_DataDmn"  , IsNullable=true )]
      public string gxTpr_Contagemresultado_datadmn_Nullable
      {
         get {
            if ( gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_datadmn == DateTime.MinValue)
               return null;
            return new GxDateString(gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_datadmn).value ;
         }

         set {
            if (value == null || value == GxDateString.NullValue )
               gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_datadmn = DateTime.MinValue;
            else
               gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_datadmn = DateTime.Parse( value);
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public DateTime gxTpr_Contagemresultado_datadmn
      {
         get {
            return gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_datadmn ;
         }

         set {
            gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_datadmn = (DateTime)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_Descricao" )]
      [  XmlElement( ElementName = "ContagemResultado_Descricao"   )]
      public String gxTpr_Contagemresultado_descricao
      {
         get {
            return gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_descricao ;
         }

         set {
            gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_descricao = (String)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_NNRef" )]
      [  XmlElement( ElementName = "ContagemResultado_NNRef"   )]
      public String gxTpr_Contagemresultado_nnref
      {
         get {
            return gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_nnref ;
         }

         set {
            gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_nnref = (String)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_Demanda" )]
      [  XmlElement( ElementName = "ContagemResultado_Demanda"   )]
      public String gxTpr_Contagemresultado_demanda
      {
         get {
            return gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_demanda ;
         }

         set {
            gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_demanda = (String)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_DemandaFM" )]
      [  XmlElement( ElementName = "ContagemResultado_DemandaFM"   )]
      public String gxTpr_Contagemresultado_demandafm
      {
         get {
            return gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_demandafm ;
         }

         set {
            gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_demandafm = (String)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_StatusDmn" )]
      [  XmlElement( ElementName = "ContagemResultado_StatusDmn"   )]
      public String gxTpr_Contagemresultado_statusdmn
      {
         get {
            return gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_statusdmn ;
         }

         set {
            gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_statusdmn = (String)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_StatusDescricao" )]
      [  XmlElement( ElementName = "ContagemResultado_StatusDescricao"   )]
      public String gxTpr_Contagemresultado_statusdescricao
      {
         get {
            return gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_statusdescricao ;
         }

         set {
            gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_statusdescricao = (String)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_CntSrvCod" )]
      [  XmlElement( ElementName = "ContagemResultado_CntSrvCod"   )]
      public int gxTpr_Contagemresultado_cntsrvcod
      {
         get {
            return gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_cntsrvcod ;
         }

         set {
            gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_cntsrvcod = (int)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_ServicoSigla" )]
      [  XmlElement( ElementName = "ContagemResultado_ServicoSigla"   )]
      public String gxTpr_Contagemresultado_servicosigla
      {
         get {
            return gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_servicosigla ;
         }

         set {
            gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_servicosigla = (String)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_DataContagemInicial" )]
      [  XmlElement( ElementName = "ContagemResultado_DataContagemInicial"  , IsNullable=true )]
      public string gxTpr_Contagemresultado_datacontageminicial_Nullable
      {
         get {
            if ( gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_datacontageminicial == DateTime.MinValue)
               return null;
            return new GxDateString(gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_datacontageminicial).value ;
         }

         set {
            if (value == null || value == GxDateString.NullValue )
               gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_datacontageminicial = DateTime.MinValue;
            else
               gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_datacontageminicial = DateTime.Parse( value);
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public DateTime gxTpr_Contagemresultado_datacontageminicial
      {
         get {
            return gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_datacontageminicial ;
         }

         set {
            gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_datacontageminicial = (DateTime)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_PFLFS_Inicial" )]
      [  XmlElement( ElementName = "ContagemResultado_PFLFS_Inicial"   )]
      public double gxTpr_Contagemresultado_pflfs_inicial_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_pflfs_inicial) ;
         }

         set {
            gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_pflfs_inicial = (decimal)(Convert.ToDecimal( value));
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Contagemresultado_pflfs_inicial
      {
         get {
            return gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_pflfs_inicial ;
         }

         set {
            gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_pflfs_inicial = (decimal)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_DataContagemFinal" )]
      [  XmlElement( ElementName = "ContagemResultado_DataContagemFinal"  , IsNullable=true )]
      public string gxTpr_Contagemresultado_datacontagemfinal_Nullable
      {
         get {
            if ( gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_datacontagemfinal == DateTime.MinValue)
               return null;
            return new GxDateString(gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_datacontagemfinal).value ;
         }

         set {
            if (value == null || value == GxDateString.NullValue )
               gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_datacontagemfinal = DateTime.MinValue;
            else
               gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_datacontagemfinal = DateTime.Parse( value);
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public DateTime gxTpr_Contagemresultado_datacontagemfinal
      {
         get {
            return gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_datacontagemfinal ;
         }

         set {
            gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_datacontagemfinal = (DateTime)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_PFLFS_Final" )]
      [  XmlElement( ElementName = "ContagemResultado_PFLFS_Final"   )]
      public double gxTpr_Contagemresultado_pflfs_final_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_pflfs_final) ;
         }

         set {
            gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_pflfs_final = (decimal)(Convert.ToDecimal( value));
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Contagemresultado_pflfs_final
      {
         get {
            return gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_pflfs_final ;
         }

         set {
            gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_pflfs_final = (decimal)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_PFLFS_Diferenca" )]
      [  XmlElement( ElementName = "ContagemResultado_PFLFS_Diferenca"   )]
      public double gxTpr_Contagemresultado_pflfs_diferenca_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_pflfs_diferenca) ;
         }

         set {
            gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_pflfs_diferenca = (decimal)(Convert.ToDecimal( value));
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Contagemresultado_pflfs_diferenca
      {
         get {
            return gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_pflfs_diferenca ;
         }

         set {
            gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_pflfs_diferenca = (decimal)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_OSVinculada" )]
      [  XmlElement( ElementName = "ContagemResultado_OSVinculada"   )]
      public int gxTpr_Contagemresultado_osvinculada
      {
         get {
            return gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_osvinculada ;
         }

         set {
            gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_osvinculada = (int)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_DmnVinculada" )]
      [  XmlElement( ElementName = "ContagemResultado_DmnVinculada"   )]
      public String gxTpr_Contagemresultado_dmnvinculada
      {
         get {
            return gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_dmnvinculada ;
         }

         set {
            gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_dmnvinculada = (String)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_PF" )]
      [  XmlElement( ElementName = "ContagemResultado_PF"   )]
      public double gxTpr_Contagemresultado_pf_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_pf) ;
         }

         set {
            gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_pf = (decimal)(Convert.ToDecimal( value));
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Contagemresultado_pf
      {
         get {
            return gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_pf ;
         }

         set {
            gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_pf = (decimal)(value);
         }

      }

      public class gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Osvinculada_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_OSVinculada_80compatibility:SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_OSVinculada {}
      [  SoapElement( ElementName = "OSVinculada" )]
      [  XmlArray( ElementName = "OSVinculada"  )]
      [  XmlArrayItemAttribute( Type= typeof( SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_OSVinculada ), ElementName= "OSVinculada"  , IsNullable=false)]
      [  XmlArrayItemAttribute( Type= typeof( gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Osvinculada_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_OSVinculada_80compatibility ), ElementName= "SDT_RelatorioGerencialContagem.SDT_RelatorioGerencialContagem.OS.OSVinculada"  , IsNullable=false)]
      public GxObjectCollection gxTpr_Osvinculada_GxObjectCollection
      {
         get {
            if ( gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Osvinculada == null )
            {
               gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Osvinculada = new GxObjectCollection( context, "SDT_RelatorioGerencialContagem.SDT_RelatorioGerencialContagem.OS.OSVinculada", "", "SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_OSVinculada", "GeneXus.Programs");
            }
            return (GxObjectCollection)gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Osvinculada ;
         }

         set {
            if ( gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Osvinculada == null )
            {
               gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Osvinculada = new GxObjectCollection( context, "SDT_RelatorioGerencialContagem.SDT_RelatorioGerencialContagem.OS.OSVinculada", "", "SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_OSVinculada", "GeneXus.Programs");
            }
            gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Osvinculada = (GxObjectCollection) value;
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public IGxCollection gxTpr_Osvinculada
      {
         get {
            if ( gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Osvinculada == null )
            {
               gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Osvinculada = new GxObjectCollection( context, "SDT_RelatorioGerencialContagem.SDT_RelatorioGerencialContagem.OS.OSVinculada", "", "SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_OSVinculada", "GeneXus.Programs");
            }
            return gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Osvinculada ;
         }

         set {
            gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Osvinculada = value;
         }

      }

      public void gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Osvinculada_SetNull( )
      {
         gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Osvinculada = null;
         return  ;
      }

      public bool gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Osvinculada_IsNull( )
      {
         if ( gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Osvinculada == null )
         {
            return true ;
         }
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_PFLFS_Inicial_Total" )]
      [  XmlElement( ElementName = "ContagemResultado_PFLFS_Inicial_Total"   )]
      public double gxTpr_Contagemresultado_pflfs_inicial_total_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_pflfs_inicial_total) ;
         }

         set {
            gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_pflfs_inicial_total = (decimal)(Convert.ToDecimal( value));
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Contagemresultado_pflfs_inicial_total
      {
         get {
            return gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_pflfs_inicial_total ;
         }

         set {
            gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_pflfs_inicial_total = (decimal)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_PFLFS_Final_Total" )]
      [  XmlElement( ElementName = "ContagemResultado_PFLFS_Final_Total"   )]
      public double gxTpr_Contagemresultado_pflfs_final_total_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_pflfs_final_total) ;
         }

         set {
            gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_pflfs_final_total = (decimal)(Convert.ToDecimal( value));
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Contagemresultado_pflfs_final_total
      {
         get {
            return gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_pflfs_final_total ;
         }

         set {
            gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_pflfs_final_total = (decimal)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_PFLFS_Diferenca_Total" )]
      [  XmlElement( ElementName = "ContagemResultado_PFLFS_Diferenca_Total"   )]
      public double gxTpr_Contagemresultado_pflfs_diferenca_total_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_pflfs_diferenca_total) ;
         }

         set {
            gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_pflfs_diferenca_total = (decimal)(Convert.ToDecimal( value));
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Contagemresultado_pflfs_diferenca_total
      {
         get {
            return gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_pflfs_diferenca_total ;
         }

         set {
            gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_pflfs_diferenca_total = (decimal)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_PF_Total" )]
      [  XmlElement( ElementName = "ContagemResultado_PF_Total"   )]
      public double gxTpr_Contagemresultado_pf_total_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_pf_total) ;
         }

         set {
            gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_pf_total = (decimal)(Convert.ToDecimal( value));
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Contagemresultado_pf_total
      {
         get {
            return gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_pf_total ;
         }

         set {
            gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_pf_total = (decimal)(value);
         }

      }

      public void initialize( )
      {
         gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_datadmn = DateTime.MinValue;
         gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_descricao = "";
         gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_nnref = "";
         gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_demanda = "";
         gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_demandafm = "";
         gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_statusdmn = "";
         gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_statusdescricao = "";
         gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_servicosigla = "";
         gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_datacontageminicial = DateTime.MinValue;
         gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_datacontagemfinal = DateTime.MinValue;
         gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_dmnvinculada = "";
         sTagName = "";
         sDateCnv = "";
         sNumToPad = "";
         return  ;
      }

      protected short readOk ;
      protected short nOutParmCount ;
      protected int gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_codigo ;
      protected int gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_cntsrvcod ;
      protected int gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_osvinculada ;
      protected decimal gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_pflfs_inicial ;
      protected decimal gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_pflfs_final ;
      protected decimal gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_pflfs_diferenca ;
      protected decimal gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_pf ;
      protected decimal gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_pflfs_inicial_total ;
      protected decimal gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_pflfs_final_total ;
      protected decimal gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_pflfs_diferenca_total ;
      protected decimal gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_pf_total ;
      protected String gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_statusdmn ;
      protected String gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_servicosigla ;
      protected String sTagName ;
      protected String sDateCnv ;
      protected String sNumToPad ;
      protected DateTime gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_datadmn ;
      protected DateTime gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_datacontageminicial ;
      protected DateTime gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_datacontagemfinal ;
      protected String gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_descricao ;
      protected String gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_nnref ;
      protected String gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_demanda ;
      protected String gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_demandafm ;
      protected String gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_statusdescricao ;
      protected String gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Contagemresultado_dmnvinculada ;
      [ObjectCollection(ItemType=typeof( SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_OSVinculada ))]
      protected IGxCollection gxTv_SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_Osvinculada=null ;
   }

   [DataContract(Name = @"SDT_RelatorioGerencialContagem.SDT_RelatorioGerencialContagem.OS", Namespace = "GxEv3Up14_MeetrikaVs3")]
   public class SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_RESTInterface : GxGenericCollectionItem<SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_RESTInterface( ) : base()
      {
      }

      public SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_RESTInterface( SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "ContagemResultado_Codigo" , Order = 0 )]
      public Nullable<int> gxTpr_Contagemresultado_codigo
      {
         get {
            return sdt.gxTpr_Contagemresultado_codigo ;
         }

         set {
            sdt.gxTpr_Contagemresultado_codigo = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContagemResultado_DataDmn" , Order = 1 )]
      public String gxTpr_Contagemresultado_datadmn
      {
         get {
            return DateTimeUtil.DToC2( sdt.gxTpr_Contagemresultado_datadmn) ;
         }

         set {
            sdt.gxTpr_Contagemresultado_datadmn = DateTimeUtil.CToD2( (String)(value));
         }

      }

      [DataMember( Name = "ContagemResultado_Descricao" , Order = 2 )]
      public String gxTpr_Contagemresultado_descricao
      {
         get {
            return sdt.gxTpr_Contagemresultado_descricao ;
         }

         set {
            sdt.gxTpr_Contagemresultado_descricao = (String)(value);
         }

      }

      [DataMember( Name = "ContagemResultado_NNRef" , Order = 3 )]
      public String gxTpr_Contagemresultado_nnref
      {
         get {
            return sdt.gxTpr_Contagemresultado_nnref ;
         }

         set {
            sdt.gxTpr_Contagemresultado_nnref = (String)(value);
         }

      }

      [DataMember( Name = "ContagemResultado_Demanda" , Order = 4 )]
      public String gxTpr_Contagemresultado_demanda
      {
         get {
            return sdt.gxTpr_Contagemresultado_demanda ;
         }

         set {
            sdt.gxTpr_Contagemresultado_demanda = (String)(value);
         }

      }

      [DataMember( Name = "ContagemResultado_DemandaFM" , Order = 5 )]
      public String gxTpr_Contagemresultado_demandafm
      {
         get {
            return sdt.gxTpr_Contagemresultado_demandafm ;
         }

         set {
            sdt.gxTpr_Contagemresultado_demandafm = (String)(value);
         }

      }

      [DataMember( Name = "ContagemResultado_StatusDmn" , Order = 6 )]
      public String gxTpr_Contagemresultado_statusdmn
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Contagemresultado_statusdmn) ;
         }

         set {
            sdt.gxTpr_Contagemresultado_statusdmn = (String)(value);
         }

      }

      [DataMember( Name = "ContagemResultado_StatusDescricao" , Order = 7 )]
      public String gxTpr_Contagemresultado_statusdescricao
      {
         get {
            return sdt.gxTpr_Contagemresultado_statusdescricao ;
         }

         set {
            sdt.gxTpr_Contagemresultado_statusdescricao = (String)(value);
         }

      }

      [DataMember( Name = "ContagemResultado_CntSrvCod" , Order = 8 )]
      public Nullable<int> gxTpr_Contagemresultado_cntsrvcod
      {
         get {
            return sdt.gxTpr_Contagemresultado_cntsrvcod ;
         }

         set {
            sdt.gxTpr_Contagemresultado_cntsrvcod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContagemResultado_ServicoSigla" , Order = 9 )]
      public String gxTpr_Contagemresultado_servicosigla
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Contagemresultado_servicosigla) ;
         }

         set {
            sdt.gxTpr_Contagemresultado_servicosigla = (String)(value);
         }

      }

      [DataMember( Name = "ContagemResultado_DataContagemInicial" , Order = 10 )]
      public String gxTpr_Contagemresultado_datacontageminicial
      {
         get {
            return DateTimeUtil.DToC2( sdt.gxTpr_Contagemresultado_datacontageminicial) ;
         }

         set {
            sdt.gxTpr_Contagemresultado_datacontageminicial = DateTimeUtil.CToD2( (String)(value));
         }

      }

      [DataMember( Name = "ContagemResultado_PFLFS_Inicial" , Order = 11 )]
      public String gxTpr_Contagemresultado_pflfs_inicial
      {
         get {
            return StringUtil.LTrim( StringUtil.Str( sdt.gxTpr_Contagemresultado_pflfs_inicial, 14, 5)) ;
         }

         set {
            sdt.gxTpr_Contagemresultado_pflfs_inicial = NumberUtil.Val( (String)(value), ".");
         }

      }

      [DataMember( Name = "ContagemResultado_DataContagemFinal" , Order = 12 )]
      public String gxTpr_Contagemresultado_datacontagemfinal
      {
         get {
            return DateTimeUtil.DToC2( sdt.gxTpr_Contagemresultado_datacontagemfinal) ;
         }

         set {
            sdt.gxTpr_Contagemresultado_datacontagemfinal = DateTimeUtil.CToD2( (String)(value));
         }

      }

      [DataMember( Name = "ContagemResultado_PFLFS_Final" , Order = 13 )]
      public String gxTpr_Contagemresultado_pflfs_final
      {
         get {
            return StringUtil.LTrim( StringUtil.Str( sdt.gxTpr_Contagemresultado_pflfs_final, 14, 5)) ;
         }

         set {
            sdt.gxTpr_Contagemresultado_pflfs_final = NumberUtil.Val( (String)(value), ".");
         }

      }

      [DataMember( Name = "ContagemResultado_PFLFS_Diferenca" , Order = 14 )]
      public String gxTpr_Contagemresultado_pflfs_diferenca
      {
         get {
            return StringUtil.LTrim( StringUtil.Str( sdt.gxTpr_Contagemresultado_pflfs_diferenca, 14, 5)) ;
         }

         set {
            sdt.gxTpr_Contagemresultado_pflfs_diferenca = NumberUtil.Val( (String)(value), ".");
         }

      }

      [DataMember( Name = "ContagemResultado_OSVinculada" , Order = 15 )]
      public Nullable<int> gxTpr_Contagemresultado_osvinculada
      {
         get {
            return sdt.gxTpr_Contagemresultado_osvinculada ;
         }

         set {
            sdt.gxTpr_Contagemresultado_osvinculada = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContagemResultado_DmnVinculada" , Order = 16 )]
      public String gxTpr_Contagemresultado_dmnvinculada
      {
         get {
            return sdt.gxTpr_Contagemresultado_dmnvinculada ;
         }

         set {
            sdt.gxTpr_Contagemresultado_dmnvinculada = (String)(value);
         }

      }

      [DataMember( Name = "ContagemResultado_PF" , Order = 17 )]
      public String gxTpr_Contagemresultado_pf
      {
         get {
            return StringUtil.LTrim( StringUtil.Str( sdt.gxTpr_Contagemresultado_pf, 14, 5)) ;
         }

         set {
            sdt.gxTpr_Contagemresultado_pf = NumberUtil.Val( (String)(value), ".");
         }

      }

      [DataMember( Name = "OSVinculada" , Order = 18 )]
      public GxGenericCollection<SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_OSVinculada_RESTInterface> gxTpr_Osvinculada
      {
         get {
            return new GxGenericCollection<SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS_OSVinculada_RESTInterface>(sdt.gxTpr_Osvinculada) ;
         }

         set {
            value.LoadCollection(sdt.gxTpr_Osvinculada);
         }

      }

      [DataMember( Name = "ContagemResultado_PFLFS_Inicial_Total" , Order = 19 )]
      public String gxTpr_Contagemresultado_pflfs_inicial_total
      {
         get {
            return StringUtil.LTrim( StringUtil.Str( sdt.gxTpr_Contagemresultado_pflfs_inicial_total, 14, 5)) ;
         }

         set {
            sdt.gxTpr_Contagemresultado_pflfs_inicial_total = NumberUtil.Val( (String)(value), ".");
         }

      }

      [DataMember( Name = "ContagemResultado_PFLFS_Final_Total" , Order = 20 )]
      public String gxTpr_Contagemresultado_pflfs_final_total
      {
         get {
            return StringUtil.LTrim( StringUtil.Str( sdt.gxTpr_Contagemresultado_pflfs_final_total, 14, 5)) ;
         }

         set {
            sdt.gxTpr_Contagemresultado_pflfs_final_total = NumberUtil.Val( (String)(value), ".");
         }

      }

      [DataMember( Name = "ContagemResultado_PFLFS_Diferenca_Total" , Order = 21 )]
      public String gxTpr_Contagemresultado_pflfs_diferenca_total
      {
         get {
            return StringUtil.LTrim( StringUtil.Str( sdt.gxTpr_Contagemresultado_pflfs_diferenca_total, 14, 5)) ;
         }

         set {
            sdt.gxTpr_Contagemresultado_pflfs_diferenca_total = NumberUtil.Val( (String)(value), ".");
         }

      }

      [DataMember( Name = "ContagemResultado_PF_Total" , Order = 22 )]
      public String gxTpr_Contagemresultado_pf_total
      {
         get {
            return StringUtil.LTrim( StringUtil.Str( sdt.gxTpr_Contagemresultado_pf_total, 14, 5)) ;
         }

         set {
            sdt.gxTpr_Contagemresultado_pf_total = NumberUtil.Val( (String)(value), ".");
         }

      }

      public SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS sdt
      {
         get {
            return (SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtSDT_RelatorioGerencialContagem_SDT_RelatorioGerencialContagem_OS() ;
         }
      }

   }

}
