/*
               File: type_SdtSDT_VariaveisCocomo_Item
        Description: SDT_VariaveisCocomo
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/29/2020 0:30:8.10
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "SDT_VariaveisCocomo.Item" )]
   [XmlType(TypeName =  "SDT_VariaveisCocomo.Item" , Namespace = "GxEv3Up14_MeetrikaVs3" )]
   [Serializable]
   public class SdtSDT_VariaveisCocomo_Item : GxUserType
   {
      public SdtSDT_VariaveisCocomo_Item( )
      {
         /* Constructor for serialization */
      }

      public SdtSDT_VariaveisCocomo_Item( IGxContext context )
      {
         this.context = context;
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         xmls = new XmlSerializer(this.GetType(), sNameSpace);
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtSDT_VariaveisCocomo_Item deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "GxEv3Up14_MeetrikaVs3" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtSDT_VariaveisCocomo_Item)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtSDT_VariaveisCocomo_Item obj ;
         obj = this;
         obj.gxTpr_Variavelcocomo_nominal = deserialized.gxTpr_Variavelcocomo_nominal;
         obj.gxTpr_Variavelcocomo_baixo = deserialized.gxTpr_Variavelcocomo_baixo;
         obj.gxTpr_Variavelcocomo_muitobaixo = deserialized.gxTpr_Variavelcocomo_muitobaixo;
         obj.gxTpr_Variavelcocomo_extrabaixo = deserialized.gxTpr_Variavelcocomo_extrabaixo;
         obj.gxTpr_Variavelcocomo_alto = deserialized.gxTpr_Variavelcocomo_alto;
         obj.gxTpr_Variavelcocomo_usado = deserialized.gxTpr_Variavelcocomo_usado;
         obj.gxTpr_Variavelcocomo_muitoalto = deserialized.gxTpr_Variavelcocomo_muitoalto;
         obj.gxTpr_Variavelcocomo_extraalto = deserialized.gxTpr_Variavelcocomo_extraalto;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "VariavelCocomo_Nominal") )
               {
                  gxTv_SdtSDT_VariaveisCocomo_Item_Variavelcocomo_nominal = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "VariavelCocomo_Baixo") )
               {
                  gxTv_SdtSDT_VariaveisCocomo_Item_Variavelcocomo_baixo = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "VariavelCocomo_MuitoBaixo") )
               {
                  gxTv_SdtSDT_VariaveisCocomo_Item_Variavelcocomo_muitobaixo = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "VariavelCocomo_ExtraBaixo") )
               {
                  gxTv_SdtSDT_VariaveisCocomo_Item_Variavelcocomo_extrabaixo = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "VariavelCocomo_Alto") )
               {
                  gxTv_SdtSDT_VariaveisCocomo_Item_Variavelcocomo_alto = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "VariavelCocomo_Usado") )
               {
                  gxTv_SdtSDT_VariaveisCocomo_Item_Variavelcocomo_usado = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "VariavelCocomo_MuitoAlto") )
               {
                  gxTv_SdtSDT_VariaveisCocomo_Item_Variavelcocomo_muitoalto = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "VariavelCocomo_ExtraAlto") )
               {
                  gxTv_SdtSDT_VariaveisCocomo_Item_Variavelcocomo_extraalto = NumberUtil.Val( oReader.Value, ".");
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "SDT_VariaveisCocomo.Item";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("VariavelCocomo_Nominal", StringUtil.Trim( StringUtil.Str( gxTv_SdtSDT_VariaveisCocomo_Item_Variavelcocomo_nominal, 12, 2)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("VariavelCocomo_Baixo", StringUtil.Trim( StringUtil.Str( gxTv_SdtSDT_VariaveisCocomo_Item_Variavelcocomo_baixo, 12, 2)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("VariavelCocomo_MuitoBaixo", StringUtil.Trim( StringUtil.Str( gxTv_SdtSDT_VariaveisCocomo_Item_Variavelcocomo_muitobaixo, 12, 2)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("VariavelCocomo_ExtraBaixo", StringUtil.Trim( StringUtil.Str( gxTv_SdtSDT_VariaveisCocomo_Item_Variavelcocomo_extrabaixo, 12, 2)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("VariavelCocomo_Alto", StringUtil.Trim( StringUtil.Str( gxTv_SdtSDT_VariaveisCocomo_Item_Variavelcocomo_alto, 12, 2)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("VariavelCocomo_Usado", StringUtil.Trim( StringUtil.Str( gxTv_SdtSDT_VariaveisCocomo_Item_Variavelcocomo_usado, 12, 2)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("VariavelCocomo_MuitoAlto", StringUtil.Trim( StringUtil.Str( gxTv_SdtSDT_VariaveisCocomo_Item_Variavelcocomo_muitoalto, 12, 2)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("VariavelCocomo_ExtraAlto", StringUtil.Trim( StringUtil.Str( gxTv_SdtSDT_VariaveisCocomo_Item_Variavelcocomo_extraalto, 12, 2)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("VariavelCocomo_Nominal", gxTv_SdtSDT_VariaveisCocomo_Item_Variavelcocomo_nominal, false);
         AddObjectProperty("VariavelCocomo_Baixo", gxTv_SdtSDT_VariaveisCocomo_Item_Variavelcocomo_baixo, false);
         AddObjectProperty("VariavelCocomo_MuitoBaixo", gxTv_SdtSDT_VariaveisCocomo_Item_Variavelcocomo_muitobaixo, false);
         AddObjectProperty("VariavelCocomo_ExtraBaixo", gxTv_SdtSDT_VariaveisCocomo_Item_Variavelcocomo_extrabaixo, false);
         AddObjectProperty("VariavelCocomo_Alto", gxTv_SdtSDT_VariaveisCocomo_Item_Variavelcocomo_alto, false);
         AddObjectProperty("VariavelCocomo_Usado", gxTv_SdtSDT_VariaveisCocomo_Item_Variavelcocomo_usado, false);
         AddObjectProperty("VariavelCocomo_MuitoAlto", gxTv_SdtSDT_VariaveisCocomo_Item_Variavelcocomo_muitoalto, false);
         AddObjectProperty("VariavelCocomo_ExtraAlto", gxTv_SdtSDT_VariaveisCocomo_Item_Variavelcocomo_extraalto, false);
         return  ;
      }

      [  SoapElement( ElementName = "VariavelCocomo_Nominal" )]
      [  XmlElement( ElementName = "VariavelCocomo_Nominal"   )]
      public double gxTpr_Variavelcocomo_nominal_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtSDT_VariaveisCocomo_Item_Variavelcocomo_nominal) ;
         }

         set {
            gxTv_SdtSDT_VariaveisCocomo_Item_Variavelcocomo_nominal = (decimal)(Convert.ToDecimal( value));
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Variavelcocomo_nominal
      {
         get {
            return gxTv_SdtSDT_VariaveisCocomo_Item_Variavelcocomo_nominal ;
         }

         set {
            gxTv_SdtSDT_VariaveisCocomo_Item_Variavelcocomo_nominal = (decimal)(value);
         }

      }

      [  SoapElement( ElementName = "VariavelCocomo_Baixo" )]
      [  XmlElement( ElementName = "VariavelCocomo_Baixo"   )]
      public double gxTpr_Variavelcocomo_baixo_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtSDT_VariaveisCocomo_Item_Variavelcocomo_baixo) ;
         }

         set {
            gxTv_SdtSDT_VariaveisCocomo_Item_Variavelcocomo_baixo = (decimal)(Convert.ToDecimal( value));
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Variavelcocomo_baixo
      {
         get {
            return gxTv_SdtSDT_VariaveisCocomo_Item_Variavelcocomo_baixo ;
         }

         set {
            gxTv_SdtSDT_VariaveisCocomo_Item_Variavelcocomo_baixo = (decimal)(value);
         }

      }

      [  SoapElement( ElementName = "VariavelCocomo_MuitoBaixo" )]
      [  XmlElement( ElementName = "VariavelCocomo_MuitoBaixo"   )]
      public double gxTpr_Variavelcocomo_muitobaixo_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtSDT_VariaveisCocomo_Item_Variavelcocomo_muitobaixo) ;
         }

         set {
            gxTv_SdtSDT_VariaveisCocomo_Item_Variavelcocomo_muitobaixo = (decimal)(Convert.ToDecimal( value));
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Variavelcocomo_muitobaixo
      {
         get {
            return gxTv_SdtSDT_VariaveisCocomo_Item_Variavelcocomo_muitobaixo ;
         }

         set {
            gxTv_SdtSDT_VariaveisCocomo_Item_Variavelcocomo_muitobaixo = (decimal)(value);
         }

      }

      [  SoapElement( ElementName = "VariavelCocomo_ExtraBaixo" )]
      [  XmlElement( ElementName = "VariavelCocomo_ExtraBaixo"   )]
      public double gxTpr_Variavelcocomo_extrabaixo_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtSDT_VariaveisCocomo_Item_Variavelcocomo_extrabaixo) ;
         }

         set {
            gxTv_SdtSDT_VariaveisCocomo_Item_Variavelcocomo_extrabaixo = (decimal)(Convert.ToDecimal( value));
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Variavelcocomo_extrabaixo
      {
         get {
            return gxTv_SdtSDT_VariaveisCocomo_Item_Variavelcocomo_extrabaixo ;
         }

         set {
            gxTv_SdtSDT_VariaveisCocomo_Item_Variavelcocomo_extrabaixo = (decimal)(value);
         }

      }

      [  SoapElement( ElementName = "VariavelCocomo_Alto" )]
      [  XmlElement( ElementName = "VariavelCocomo_Alto"   )]
      public double gxTpr_Variavelcocomo_alto_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtSDT_VariaveisCocomo_Item_Variavelcocomo_alto) ;
         }

         set {
            gxTv_SdtSDT_VariaveisCocomo_Item_Variavelcocomo_alto = (decimal)(Convert.ToDecimal( value));
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Variavelcocomo_alto
      {
         get {
            return gxTv_SdtSDT_VariaveisCocomo_Item_Variavelcocomo_alto ;
         }

         set {
            gxTv_SdtSDT_VariaveisCocomo_Item_Variavelcocomo_alto = (decimal)(value);
         }

      }

      [  SoapElement( ElementName = "VariavelCocomo_Usado" )]
      [  XmlElement( ElementName = "VariavelCocomo_Usado"   )]
      public double gxTpr_Variavelcocomo_usado_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtSDT_VariaveisCocomo_Item_Variavelcocomo_usado) ;
         }

         set {
            gxTv_SdtSDT_VariaveisCocomo_Item_Variavelcocomo_usado = (decimal)(Convert.ToDecimal( value));
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Variavelcocomo_usado
      {
         get {
            return gxTv_SdtSDT_VariaveisCocomo_Item_Variavelcocomo_usado ;
         }

         set {
            gxTv_SdtSDT_VariaveisCocomo_Item_Variavelcocomo_usado = (decimal)(value);
         }

      }

      [  SoapElement( ElementName = "VariavelCocomo_MuitoAlto" )]
      [  XmlElement( ElementName = "VariavelCocomo_MuitoAlto"   )]
      public double gxTpr_Variavelcocomo_muitoalto_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtSDT_VariaveisCocomo_Item_Variavelcocomo_muitoalto) ;
         }

         set {
            gxTv_SdtSDT_VariaveisCocomo_Item_Variavelcocomo_muitoalto = (decimal)(Convert.ToDecimal( value));
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Variavelcocomo_muitoalto
      {
         get {
            return gxTv_SdtSDT_VariaveisCocomo_Item_Variavelcocomo_muitoalto ;
         }

         set {
            gxTv_SdtSDT_VariaveisCocomo_Item_Variavelcocomo_muitoalto = (decimal)(value);
         }

      }

      [  SoapElement( ElementName = "VariavelCocomo_ExtraAlto" )]
      [  XmlElement( ElementName = "VariavelCocomo_ExtraAlto"   )]
      public double gxTpr_Variavelcocomo_extraalto_double
      {
         get {
            return Convert.ToDouble(gxTv_SdtSDT_VariaveisCocomo_Item_Variavelcocomo_extraalto) ;
         }

         set {
            gxTv_SdtSDT_VariaveisCocomo_Item_Variavelcocomo_extraalto = (decimal)(Convert.ToDecimal( value));
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public decimal gxTpr_Variavelcocomo_extraalto
      {
         get {
            return gxTv_SdtSDT_VariaveisCocomo_Item_Variavelcocomo_extraalto ;
         }

         set {
            gxTv_SdtSDT_VariaveisCocomo_Item_Variavelcocomo_extraalto = (decimal)(value);
         }

      }

      public void initialize( )
      {
         sTagName = "";
         return  ;
      }

      protected short readOk ;
      protected short nOutParmCount ;
      protected decimal gxTv_SdtSDT_VariaveisCocomo_Item_Variavelcocomo_nominal ;
      protected decimal gxTv_SdtSDT_VariaveisCocomo_Item_Variavelcocomo_baixo ;
      protected decimal gxTv_SdtSDT_VariaveisCocomo_Item_Variavelcocomo_muitobaixo ;
      protected decimal gxTv_SdtSDT_VariaveisCocomo_Item_Variavelcocomo_extrabaixo ;
      protected decimal gxTv_SdtSDT_VariaveisCocomo_Item_Variavelcocomo_alto ;
      protected decimal gxTv_SdtSDT_VariaveisCocomo_Item_Variavelcocomo_usado ;
      protected decimal gxTv_SdtSDT_VariaveisCocomo_Item_Variavelcocomo_muitoalto ;
      protected decimal gxTv_SdtSDT_VariaveisCocomo_Item_Variavelcocomo_extraalto ;
      protected String sTagName ;
   }

   [DataContract(Name = @"SDT_VariaveisCocomo.Item", Namespace = "GxEv3Up14_MeetrikaVs3")]
   public class SdtSDT_VariaveisCocomo_Item_RESTInterface : GxGenericCollectionItem<SdtSDT_VariaveisCocomo_Item>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtSDT_VariaveisCocomo_Item_RESTInterface( ) : base()
      {
      }

      public SdtSDT_VariaveisCocomo_Item_RESTInterface( SdtSDT_VariaveisCocomo_Item psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "VariavelCocomo_Nominal" , Order = 0 )]
      public String gxTpr_Variavelcocomo_nominal
      {
         get {
            return StringUtil.LTrim( StringUtil.Str( sdt.gxTpr_Variavelcocomo_nominal, 12, 2)) ;
         }

         set {
            sdt.gxTpr_Variavelcocomo_nominal = NumberUtil.Val( (String)(value), ".");
         }

      }

      [DataMember( Name = "VariavelCocomo_Baixo" , Order = 1 )]
      public String gxTpr_Variavelcocomo_baixo
      {
         get {
            return StringUtil.LTrim( StringUtil.Str( sdt.gxTpr_Variavelcocomo_baixo, 12, 2)) ;
         }

         set {
            sdt.gxTpr_Variavelcocomo_baixo = NumberUtil.Val( (String)(value), ".");
         }

      }

      [DataMember( Name = "VariavelCocomo_MuitoBaixo" , Order = 2 )]
      public String gxTpr_Variavelcocomo_muitobaixo
      {
         get {
            return StringUtil.LTrim( StringUtil.Str( sdt.gxTpr_Variavelcocomo_muitobaixo, 12, 2)) ;
         }

         set {
            sdt.gxTpr_Variavelcocomo_muitobaixo = NumberUtil.Val( (String)(value), ".");
         }

      }

      [DataMember( Name = "VariavelCocomo_ExtraBaixo" , Order = 3 )]
      public String gxTpr_Variavelcocomo_extrabaixo
      {
         get {
            return StringUtil.LTrim( StringUtil.Str( sdt.gxTpr_Variavelcocomo_extrabaixo, 12, 2)) ;
         }

         set {
            sdt.gxTpr_Variavelcocomo_extrabaixo = NumberUtil.Val( (String)(value), ".");
         }

      }

      [DataMember( Name = "VariavelCocomo_Alto" , Order = 4 )]
      public String gxTpr_Variavelcocomo_alto
      {
         get {
            return StringUtil.LTrim( StringUtil.Str( sdt.gxTpr_Variavelcocomo_alto, 12, 2)) ;
         }

         set {
            sdt.gxTpr_Variavelcocomo_alto = NumberUtil.Val( (String)(value), ".");
         }

      }

      [DataMember( Name = "VariavelCocomo_Usado" , Order = 5 )]
      public String gxTpr_Variavelcocomo_usado
      {
         get {
            return StringUtil.LTrim( StringUtil.Str( sdt.gxTpr_Variavelcocomo_usado, 12, 2)) ;
         }

         set {
            sdt.gxTpr_Variavelcocomo_usado = NumberUtil.Val( (String)(value), ".");
         }

      }

      [DataMember( Name = "VariavelCocomo_MuitoAlto" , Order = 6 )]
      public String gxTpr_Variavelcocomo_muitoalto
      {
         get {
            return StringUtil.LTrim( StringUtil.Str( sdt.gxTpr_Variavelcocomo_muitoalto, 12, 2)) ;
         }

         set {
            sdt.gxTpr_Variavelcocomo_muitoalto = NumberUtil.Val( (String)(value), ".");
         }

      }

      [DataMember( Name = "VariavelCocomo_ExtraAlto" , Order = 7 )]
      public String gxTpr_Variavelcocomo_extraalto
      {
         get {
            return StringUtil.LTrim( StringUtil.Str( sdt.gxTpr_Variavelcocomo_extraalto, 12, 2)) ;
         }

         set {
            sdt.gxTpr_Variavelcocomo_extraalto = NumberUtil.Val( (String)(value), ".");
         }

      }

      public SdtSDT_VariaveisCocomo_Item sdt
      {
         get {
            return (SdtSDT_VariaveisCocomo_Item)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtSDT_VariaveisCocomo_Item() ;
         }
      }

   }

}
