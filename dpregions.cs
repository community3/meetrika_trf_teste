/*
               File: DPRegions
        Description: DPRegions
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/29/2020 0:4:56.22
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class dpregions : GXProcedure
   {
      public dpregions( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public dpregions( IGxContext context )
      {
         this.context = context;
         IsMain = false;
      }

      public void release( )
      {
      }

      public void execute( out IGxCollection aP0_Gxm2rootcol )
      {
         this.Gxm2rootcol = new GxObjectCollection( context, "GeoRegions.GeoRegionsItem", "GxEv3Up14_MeetrikaVs3", "SdtGeoRegions_GeoRegionsItem", "GeneXus.Programs") ;
         initialize();
         executePrivate();
         aP0_Gxm2rootcol=this.Gxm2rootcol;
      }

      public IGxCollection executeUdp( )
      {
         this.Gxm2rootcol = new GxObjectCollection( context, "GeoRegions.GeoRegionsItem", "GxEv3Up14_MeetrikaVs3", "SdtGeoRegions_GeoRegionsItem", "GeneXus.Programs") ;
         initialize();
         executePrivate();
         aP0_Gxm2rootcol=this.Gxm2rootcol;
         return Gxm2rootcol ;
      }

      public void executeSubmit( out IGxCollection aP0_Gxm2rootcol )
      {
         dpregions objdpregions;
         objdpregions = new dpregions();
         objdpregions.Gxm2rootcol = new GxObjectCollection( context, "GeoRegions.GeoRegionsItem", "GxEv3Up14_MeetrikaVs3", "SdtGeoRegions_GeoRegionsItem", "GeneXus.Programs") ;
         objdpregions.context.SetSubmitInitialConfig(context);
         objdpregions.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objdpregions);
         aP0_Gxm2rootcol=this.Gxm2rootcol;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((dpregions)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         Gxm1georegions = new SdtGeoRegions_GeoRegionsItem(context);
         Gxm2rootcol.Add(Gxm1georegions, 0);
         Gxm1georegions.gxTpr_Rvalnum = 1;
         Gxm1georegions.gxTpr_Rvaltext = "Montevideo";
         Gxm1georegions.gxTpr_Rcountry = "Uruguay";
         Gxm1georegions = new SdtGeoRegions_GeoRegionsItem(context);
         Gxm2rootcol.Add(Gxm1georegions, 0);
         Gxm1georegions.gxTpr_Rvalnum = 1;
         Gxm1georegions.gxTpr_Rvaltext = "Santiago";
         Gxm1georegions.gxTpr_Rcountry = "Chile";
         Gxm1georegions = new SdtGeoRegions_GeoRegionsItem(context);
         Gxm2rootcol.Add(Gxm1georegions, 0);
         Gxm1georegions.gxTpr_Rvalnum = 1;
         Gxm1georegions.gxTpr_Rvaltext = "Asuncion";
         Gxm1georegions.gxTpr_Rcountry = "Paraguay";
         Gxm1georegions = new SdtGeoRegions_GeoRegionsItem(context);
         Gxm2rootcol.Add(Gxm1georegions, 0);
         Gxm1georegions.gxTpr_Rvalnum = 4;
         Gxm1georegions.gxTpr_Rvaltext = "Bs As-Rosario-Rio Cuarto";
         Gxm1georegions.gxTpr_Rcountry = "Argentina";
         Gxm1georegions = new SdtGeoRegions_GeoRegionsItem(context);
         Gxm2rootcol.Add(Gxm1georegions, 0);
         Gxm1georegions.gxTpr_Rvalnum = 1;
         Gxm1georegions.gxTpr_Rvaltext = "Lombardia";
         Gxm1georegions.gxTpr_Rcountry = "Italy";
         Gxm1georegions = new SdtGeoRegions_GeoRegionsItem(context);
         Gxm2rootcol.Add(Gxm1georegions, 0);
         Gxm1georegions.gxTpr_Rvalnum = 2;
         Gxm1georegions.gxTpr_Rvaltext = "Puebla-Monterrey";
         Gxm1georegions.gxTpr_Rcountry = "Mexico";
         Gxm1georegions = new SdtGeoRegions_GeoRegionsItem(context);
         Gxm2rootcol.Add(Gxm1georegions, 0);
         Gxm1georegions.gxTpr_Rvalnum = 1;
         Gxm1georegions.gxTpr_Rvaltext = "Barcelona";
         Gxm1georegions.gxTpr_Rcountry = "Spain";
         Gxm1georegions = new SdtGeoRegions_GeoRegionsItem(context);
         Gxm2rootcol.Add(Gxm1georegions, 0);
         Gxm1georegions.gxTpr_Rvalnum = 1;
         Gxm1georegions.gxTpr_Rvaltext = "Cuenca";
         Gxm1georegions.gxTpr_Rcountry = "Ecuador";
         Gxm1georegions = new SdtGeoRegions_GeoRegionsItem(context);
         Gxm2rootcol.Add(Gxm1georegions, 0);
         Gxm1georegions.gxTpr_Rvalnum = 1;
         Gxm1georegions.gxTpr_Rvaltext = "Grupo en el eje cafetero";
         Gxm1georegions.gxTpr_Rcountry = "Colombia";
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Gxm1georegions = new SdtGeoRegions_GeoRegionsItem(context);
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private IGxCollection aP0_Gxm2rootcol ;
      [ObjectCollection(ItemType=typeof( SdtGeoRegions_GeoRegionsItem ))]
      private IGxCollection Gxm2rootcol ;
      private SdtGeoRegions_GeoRegionsItem Gxm1georegions ;
   }

}
