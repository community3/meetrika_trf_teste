/*
               File: GetWWStatusFilterData
        Description: Get WWStatus Filter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:53:54.67
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getwwstatusfilterdata : GXProcedure
   {
      public getwwstatusfilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getwwstatusfilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV16DDOName = aP0_DDOName;
         this.AV14SearchTxt = aP1_SearchTxt;
         this.AV15SearchTxtTo = aP2_SearchTxtTo;
         this.AV20OptionsJson = "" ;
         this.AV23OptionsDescJson = "" ;
         this.AV25OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV20OptionsJson;
         aP4_OptionsDescJson=this.AV23OptionsDescJson;
         aP5_OptionIndexesJson=this.AV25OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV16DDOName = aP0_DDOName;
         this.AV14SearchTxt = aP1_SearchTxt;
         this.AV15SearchTxtTo = aP2_SearchTxtTo;
         this.AV20OptionsJson = "" ;
         this.AV23OptionsDescJson = "" ;
         this.AV25OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV20OptionsJson;
         aP4_OptionsDescJson=this.AV23OptionsDescJson;
         aP5_OptionIndexesJson=this.AV25OptionIndexesJson;
         return AV25OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getwwstatusfilterdata objgetwwstatusfilterdata;
         objgetwwstatusfilterdata = new getwwstatusfilterdata();
         objgetwwstatusfilterdata.AV16DDOName = aP0_DDOName;
         objgetwwstatusfilterdata.AV14SearchTxt = aP1_SearchTxt;
         objgetwwstatusfilterdata.AV15SearchTxtTo = aP2_SearchTxtTo;
         objgetwwstatusfilterdata.AV20OptionsJson = "" ;
         objgetwwstatusfilterdata.AV23OptionsDescJson = "" ;
         objgetwwstatusfilterdata.AV25OptionIndexesJson = "" ;
         objgetwwstatusfilterdata.context.SetSubmitInitialConfig(context);
         objgetwwstatusfilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetwwstatusfilterdata);
         aP3_OptionsJson=this.AV20OptionsJson;
         aP4_OptionsDescJson=this.AV23OptionsDescJson;
         aP5_OptionIndexesJson=this.AV25OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getwwstatusfilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV19Options = (IGxCollection)(new GxSimpleCollection());
         AV22OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV24OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV16DDOName), "DDO_STATUS_NOME") == 0 )
         {
            /* Execute user subroutine: 'LOADSTATUS_NOMEOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV20OptionsJson = AV19Options.ToJSonString(false);
         AV23OptionsDescJson = AV22OptionsDesc.ToJSonString(false);
         AV25OptionIndexesJson = AV24OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV27Session.Get("WWStatusGridState"), "") == 0 )
         {
            AV29GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "WWStatusGridState"), "");
         }
         else
         {
            AV29GridState.FromXml(AV27Session.Get("WWStatusGridState"), "");
         }
         AV43GXV1 = 1;
         while ( AV43GXV1 <= AV29GridState.gxTpr_Filtervalues.Count )
         {
            AV30GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV29GridState.gxTpr_Filtervalues.Item(AV43GXV1));
            if ( StringUtil.StrCmp(AV30GridStateFilterValue.gxTpr_Name, "STATUS_AREATRABALHOCOD") == 0 )
            {
               AV32Status_AreaTrabalhoCod = (int)(NumberUtil.Val( AV30GridStateFilterValue.gxTpr_Value, "."));
            }
            else if ( StringUtil.StrCmp(AV30GridStateFilterValue.gxTpr_Name, "TFSTATUS_NOME") == 0 )
            {
               AV10TFStatus_Nome = AV30GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV30GridStateFilterValue.gxTpr_Name, "TFSTATUS_NOME_SEL") == 0 )
            {
               AV11TFStatus_Nome_Sel = AV30GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV30GridStateFilterValue.gxTpr_Name, "TFSTATUS_TIPO_SEL") == 0 )
            {
               AV12TFStatus_Tipo_SelsJson = AV30GridStateFilterValue.gxTpr_Value;
               AV13TFStatus_Tipo_Sels.FromJSonString(AV12TFStatus_Tipo_SelsJson);
            }
            AV43GXV1 = (int)(AV43GXV1+1);
         }
         if ( AV29GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV31GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV29GridState.gxTpr_Dynamicfilters.Item(1));
            AV33DynamicFiltersSelector1 = AV31GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV33DynamicFiltersSelector1, "STATUS_NOME") == 0 )
            {
               AV34Status_Nome1 = AV31GridStateDynamicFilter.gxTpr_Value;
            }
            if ( AV29GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV35DynamicFiltersEnabled2 = true;
               AV31GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV29GridState.gxTpr_Dynamicfilters.Item(2));
               AV36DynamicFiltersSelector2 = AV31GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV36DynamicFiltersSelector2, "STATUS_NOME") == 0 )
               {
                  AV37Status_Nome2 = AV31GridStateDynamicFilter.gxTpr_Value;
               }
               if ( AV29GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  AV38DynamicFiltersEnabled3 = true;
                  AV31GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV29GridState.gxTpr_Dynamicfilters.Item(3));
                  AV39DynamicFiltersSelector3 = AV31GridStateDynamicFilter.gxTpr_Selected;
                  if ( StringUtil.StrCmp(AV39DynamicFiltersSelector3, "STATUS_NOME") == 0 )
                  {
                     AV40Status_Nome3 = AV31GridStateDynamicFilter.gxTpr_Value;
                  }
               }
            }
         }
      }

      protected void S121( )
      {
         /* 'LOADSTATUS_NOMEOPTIONS' Routine */
         AV10TFStatus_Nome = AV14SearchTxt;
         AV11TFStatus_Nome_Sel = "";
         AV45WWStatusDS_1_Status_areatrabalhocod = AV32Status_AreaTrabalhoCod;
         AV46WWStatusDS_2_Dynamicfiltersselector1 = AV33DynamicFiltersSelector1;
         AV47WWStatusDS_3_Status_nome1 = AV34Status_Nome1;
         AV48WWStatusDS_4_Dynamicfiltersenabled2 = AV35DynamicFiltersEnabled2;
         AV49WWStatusDS_5_Dynamicfiltersselector2 = AV36DynamicFiltersSelector2;
         AV50WWStatusDS_6_Status_nome2 = AV37Status_Nome2;
         AV51WWStatusDS_7_Dynamicfiltersenabled3 = AV38DynamicFiltersEnabled3;
         AV52WWStatusDS_8_Dynamicfiltersselector3 = AV39DynamicFiltersSelector3;
         AV53WWStatusDS_9_Status_nome3 = AV40Status_Nome3;
         AV54WWStatusDS_10_Tfstatus_nome = AV10TFStatus_Nome;
         AV55WWStatusDS_11_Tfstatus_nome_sel = AV11TFStatus_Nome_Sel;
         AV56WWStatusDS_12_Tfstatus_tipo_sels = AV13TFStatus_Tipo_Sels;
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              A425Status_Tipo ,
                                              AV56WWStatusDS_12_Tfstatus_tipo_sels ,
                                              AV46WWStatusDS_2_Dynamicfiltersselector1 ,
                                              AV47WWStatusDS_3_Status_nome1 ,
                                              AV48WWStatusDS_4_Dynamicfiltersenabled2 ,
                                              AV49WWStatusDS_5_Dynamicfiltersselector2 ,
                                              AV50WWStatusDS_6_Status_nome2 ,
                                              AV51WWStatusDS_7_Dynamicfiltersenabled3 ,
                                              AV52WWStatusDS_8_Dynamicfiltersselector3 ,
                                              AV53WWStatusDS_9_Status_nome3 ,
                                              AV55WWStatusDS_11_Tfstatus_nome_sel ,
                                              AV54WWStatusDS_10_Tfstatus_nome ,
                                              AV56WWStatusDS_12_Tfstatus_tipo_sels.Count ,
                                              A424Status_Nome ,
                                              A421Status_AreaTrabalhoCod ,
                                              AV9WWPContext.gxTpr_Areatrabalho_codigo },
                                              new int[] {
                                              TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT
                                              }
         });
         lV47WWStatusDS_3_Status_nome1 = StringUtil.PadR( StringUtil.RTrim( AV47WWStatusDS_3_Status_nome1), 50, "%");
         lV50WWStatusDS_6_Status_nome2 = StringUtil.PadR( StringUtil.RTrim( AV50WWStatusDS_6_Status_nome2), 50, "%");
         lV53WWStatusDS_9_Status_nome3 = StringUtil.PadR( StringUtil.RTrim( AV53WWStatusDS_9_Status_nome3), 50, "%");
         lV54WWStatusDS_10_Tfstatus_nome = StringUtil.PadR( StringUtil.RTrim( AV54WWStatusDS_10_Tfstatus_nome), 50, "%");
         /* Using cursor P00M92 */
         pr_default.execute(0, new Object[] {AV9WWPContext.gxTpr_Areatrabalho_codigo, lV47WWStatusDS_3_Status_nome1, lV50WWStatusDS_6_Status_nome2, lV53WWStatusDS_9_Status_nome3, lV54WWStatusDS_10_Tfstatus_nome, AV55WWStatusDS_11_Tfstatus_nome_sel});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKM92 = false;
            A421Status_AreaTrabalhoCod = P00M92_A421Status_AreaTrabalhoCod[0];
            A424Status_Nome = P00M92_A424Status_Nome[0];
            A425Status_Tipo = P00M92_A425Status_Tipo[0];
            A423Status_Codigo = P00M92_A423Status_Codigo[0];
            AV26count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( StringUtil.StrCmp(P00M92_A424Status_Nome[0], A424Status_Nome) == 0 ) )
            {
               BRKM92 = false;
               A423Status_Codigo = P00M92_A423Status_Codigo[0];
               AV26count = (long)(AV26count+1);
               BRKM92 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A424Status_Nome)) )
            {
               AV18Option = A424Status_Nome;
               AV19Options.Add(AV18Option, 0);
               AV24OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV26count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV19Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKM92 )
            {
               BRKM92 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV19Options = new GxSimpleCollection();
         AV22OptionsDesc = new GxSimpleCollection();
         AV24OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV27Session = context.GetSession();
         AV29GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV30GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV10TFStatus_Nome = "";
         AV11TFStatus_Nome_Sel = "";
         AV12TFStatus_Tipo_SelsJson = "";
         AV13TFStatus_Tipo_Sels = new GxSimpleCollection();
         AV31GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV33DynamicFiltersSelector1 = "";
         AV34Status_Nome1 = "";
         AV36DynamicFiltersSelector2 = "";
         AV37Status_Nome2 = "";
         AV39DynamicFiltersSelector3 = "";
         AV40Status_Nome3 = "";
         AV46WWStatusDS_2_Dynamicfiltersselector1 = "";
         AV47WWStatusDS_3_Status_nome1 = "";
         AV49WWStatusDS_5_Dynamicfiltersselector2 = "";
         AV50WWStatusDS_6_Status_nome2 = "";
         AV52WWStatusDS_8_Dynamicfiltersselector3 = "";
         AV53WWStatusDS_9_Status_nome3 = "";
         AV54WWStatusDS_10_Tfstatus_nome = "";
         AV55WWStatusDS_11_Tfstatus_nome_sel = "";
         AV56WWStatusDS_12_Tfstatus_tipo_sels = new GxSimpleCollection();
         scmdbuf = "";
         lV47WWStatusDS_3_Status_nome1 = "";
         lV50WWStatusDS_6_Status_nome2 = "";
         lV53WWStatusDS_9_Status_nome3 = "";
         lV54WWStatusDS_10_Tfstatus_nome = "";
         A424Status_Nome = "";
         P00M92_A421Status_AreaTrabalhoCod = new int[1] ;
         P00M92_A424Status_Nome = new String[] {""} ;
         P00M92_A425Status_Tipo = new short[1] ;
         P00M92_A423Status_Codigo = new int[1] ;
         AV18Option = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getwwstatusfilterdata__default(),
            new Object[][] {
                new Object[] {
               P00M92_A421Status_AreaTrabalhoCod, P00M92_A424Status_Nome, P00M92_A425Status_Tipo, P00M92_A423Status_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short A425Status_Tipo ;
      private int AV43GXV1 ;
      private int AV32Status_AreaTrabalhoCod ;
      private int AV45WWStatusDS_1_Status_areatrabalhocod ;
      private int AV56WWStatusDS_12_Tfstatus_tipo_sels_Count ;
      private int AV9WWPContext_gxTpr_Areatrabalho_codigo ;
      private int A421Status_AreaTrabalhoCod ;
      private int A423Status_Codigo ;
      private long AV26count ;
      private String AV10TFStatus_Nome ;
      private String AV11TFStatus_Nome_Sel ;
      private String AV34Status_Nome1 ;
      private String AV37Status_Nome2 ;
      private String AV40Status_Nome3 ;
      private String AV47WWStatusDS_3_Status_nome1 ;
      private String AV50WWStatusDS_6_Status_nome2 ;
      private String AV53WWStatusDS_9_Status_nome3 ;
      private String AV54WWStatusDS_10_Tfstatus_nome ;
      private String AV55WWStatusDS_11_Tfstatus_nome_sel ;
      private String scmdbuf ;
      private String lV47WWStatusDS_3_Status_nome1 ;
      private String lV50WWStatusDS_6_Status_nome2 ;
      private String lV53WWStatusDS_9_Status_nome3 ;
      private String lV54WWStatusDS_10_Tfstatus_nome ;
      private String A424Status_Nome ;
      private bool returnInSub ;
      private bool AV35DynamicFiltersEnabled2 ;
      private bool AV38DynamicFiltersEnabled3 ;
      private bool AV48WWStatusDS_4_Dynamicfiltersenabled2 ;
      private bool AV51WWStatusDS_7_Dynamicfiltersenabled3 ;
      private bool BRKM92 ;
      private String AV25OptionIndexesJson ;
      private String AV20OptionsJson ;
      private String AV23OptionsDescJson ;
      private String AV12TFStatus_Tipo_SelsJson ;
      private String AV16DDOName ;
      private String AV14SearchTxt ;
      private String AV15SearchTxtTo ;
      private String AV33DynamicFiltersSelector1 ;
      private String AV36DynamicFiltersSelector2 ;
      private String AV39DynamicFiltersSelector3 ;
      private String AV46WWStatusDS_2_Dynamicfiltersselector1 ;
      private String AV49WWStatusDS_5_Dynamicfiltersselector2 ;
      private String AV52WWStatusDS_8_Dynamicfiltersselector3 ;
      private String AV18Option ;
      private IGxSession AV27Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00M92_A421Status_AreaTrabalhoCod ;
      private String[] P00M92_A424Status_Nome ;
      private short[] P00M92_A425Status_Tipo ;
      private int[] P00M92_A423Status_Codigo ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV13TFStatus_Tipo_Sels ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV56WWStatusDS_12_Tfstatus_tipo_sels ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV19Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV22OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV24OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV29GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV30GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV31GridStateDynamicFilter ;
   }

   public class getwwstatusfilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00M92( IGxContext context ,
                                             short A425Status_Tipo ,
                                             IGxCollection AV56WWStatusDS_12_Tfstatus_tipo_sels ,
                                             String AV46WWStatusDS_2_Dynamicfiltersselector1 ,
                                             String AV47WWStatusDS_3_Status_nome1 ,
                                             bool AV48WWStatusDS_4_Dynamicfiltersenabled2 ,
                                             String AV49WWStatusDS_5_Dynamicfiltersselector2 ,
                                             String AV50WWStatusDS_6_Status_nome2 ,
                                             bool AV51WWStatusDS_7_Dynamicfiltersenabled3 ,
                                             String AV52WWStatusDS_8_Dynamicfiltersselector3 ,
                                             String AV53WWStatusDS_9_Status_nome3 ,
                                             String AV55WWStatusDS_11_Tfstatus_nome_sel ,
                                             String AV54WWStatusDS_10_Tfstatus_nome ,
                                             int AV56WWStatusDS_12_Tfstatus_tipo_sels_Count ,
                                             String A424Status_Nome ,
                                             int A421Status_AreaTrabalhoCod ,
                                             int AV9WWPContext_gxTpr_Areatrabalho_codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [6] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT [Status_AreaTrabalhoCod], [Status_Nome], [Status_Tipo], [Status_Codigo] FROM [Status] WITH (NOLOCK)";
         scmdbuf = scmdbuf + " WHERE ([Status_AreaTrabalhoCod] = @AV9WWPCo_1Areatrabalho_codigo)";
         if ( ( StringUtil.StrCmp(AV46WWStatusDS_2_Dynamicfiltersselector1, "STATUS_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV47WWStatusDS_3_Status_nome1)) ) )
         {
            sWhereString = sWhereString + " and ([Status_Nome] like '%' + @lV47WWStatusDS_3_Status_nome1)";
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( AV48WWStatusDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV49WWStatusDS_5_Dynamicfiltersselector2, "STATUS_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV50WWStatusDS_6_Status_nome2)) ) )
         {
            sWhereString = sWhereString + " and ([Status_Nome] like '%' + @lV50WWStatusDS_6_Status_nome2)";
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( AV51WWStatusDS_7_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV52WWStatusDS_8_Dynamicfiltersselector3, "STATUS_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV53WWStatusDS_9_Status_nome3)) ) )
         {
            sWhereString = sWhereString + " and ([Status_Nome] like '%' + @lV53WWStatusDS_9_Status_nome3)";
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV55WWStatusDS_11_Tfstatus_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV54WWStatusDS_10_Tfstatus_nome)) ) )
         {
            sWhereString = sWhereString + " and ([Status_Nome] like @lV54WWStatusDS_10_Tfstatus_nome)";
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV55WWStatusDS_11_Tfstatus_nome_sel)) )
         {
            sWhereString = sWhereString + " and ([Status_Nome] = @AV55WWStatusDS_11_Tfstatus_nome_sel)";
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( AV56WWStatusDS_12_Tfstatus_tipo_sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV56WWStatusDS_12_Tfstatus_tipo_sels, "[Status_Tipo] IN (", ")") + ")";
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY [Status_Nome]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00M92(context, (short)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (bool)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (bool)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (int)dynConstraints[12] , (String)dynConstraints[13] , (int)dynConstraints[14] , (int)dynConstraints[15] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00M92 ;
          prmP00M92 = new Object[] {
          new Object[] {"@AV9WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV47WWStatusDS_3_Status_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV50WWStatusDS_6_Status_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV53WWStatusDS_9_Status_nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV54WWStatusDS_10_Tfstatus_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV55WWStatusDS_11_Tfstatus_nome_sel",SqlDbType.Char,50,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00M92", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00M92,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((short[]) buf[2])[0] = rslt.getShort(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[6]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[7]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[8]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[9]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[10]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[11]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getwwstatusfilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getwwstatusfilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getwwstatusfilterdata") )
          {
             return  ;
          }
          getwwstatusfilterdata worker = new getwwstatusfilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
