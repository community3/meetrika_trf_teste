/*
               File: DP_TabsMenuDataSectionsAdm
        Description: Tabs Menu Data Sections Administrator
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/29/2020 0:4:9.30
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class dp_tabsmenudatasectionsadm : GXProcedure
   {
      public dp_tabsmenudatasectionsadm( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public dp_tabsmenudatasectionsadm( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Menu_PaiCod ,
                           out IGxCollection aP1_Gxm2rootcol )
      {
         this.AV5Menu_PaiCod = aP0_Menu_PaiCod;
         this.Gxm2rootcol = new GxObjectCollection( context, "SectionsItem", "GxEv3Up14_MeetrikaVs3", "SdtSectionsItem", "GeneXus.Programs") ;
         initialize();
         executePrivate();
         aP1_Gxm2rootcol=this.Gxm2rootcol;
      }

      public IGxCollection executeUdp( int aP0_Menu_PaiCod )
      {
         this.AV5Menu_PaiCod = aP0_Menu_PaiCod;
         this.Gxm2rootcol = new GxObjectCollection( context, "SectionsItem", "GxEv3Up14_MeetrikaVs3", "SdtSectionsItem", "GeneXus.Programs") ;
         initialize();
         executePrivate();
         aP1_Gxm2rootcol=this.Gxm2rootcol;
         return Gxm2rootcol ;
      }

      public void executeSubmit( int aP0_Menu_PaiCod ,
                                 out IGxCollection aP1_Gxm2rootcol )
      {
         dp_tabsmenudatasectionsadm objdp_tabsmenudatasectionsadm;
         objdp_tabsmenudatasectionsadm = new dp_tabsmenudatasectionsadm();
         objdp_tabsmenudatasectionsadm.AV5Menu_PaiCod = aP0_Menu_PaiCod;
         objdp_tabsmenudatasectionsadm.Gxm2rootcol = new GxObjectCollection( context, "SectionsItem", "GxEv3Up14_MeetrikaVs3", "SdtSectionsItem", "GeneXus.Programs") ;
         objdp_tabsmenudatasectionsadm.context.SetSubmitInitialConfig(context);
         objdp_tabsmenudatasectionsadm.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objdp_tabsmenudatasectionsadm);
         aP1_Gxm2rootcol=this.Gxm2rootcol;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((dp_tabsmenudatasectionsadm)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P00062 */
         pr_default.execute(0, new Object[] {AV5Menu_PaiCod});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A285Menu_PaiCod = P00062_A285Menu_PaiCod[0];
            n285Menu_PaiCod = P00062_n285Menu_PaiCod[0];
            A284Menu_Ativo = P00062_A284Menu_Ativo[0];
            A280Menu_Tipo = P00062_A280Menu_Tipo[0];
            A283Menu_Ordem = P00062_A283Menu_Ordem[0];
            A279Menu_Descricao = P00062_A279Menu_Descricao[0];
            n279Menu_Descricao = P00062_n279Menu_Descricao[0];
            A281Menu_Link = P00062_A281Menu_Link[0];
            n281Menu_Link = P00062_n281Menu_Link[0];
            A278Menu_Nome = P00062_A278Menu_Nome[0];
            A277Menu_Codigo = P00062_A277Menu_Codigo[0];
            Gxm1sectionsitem = new SdtSectionsItem(context);
            Gxm2rootcol.Add(Gxm1sectionsitem, 0);
            Gxm1sectionsitem.gxTpr_Sectionid = (short)(A277Menu_Codigo);
            Gxm1sectionsitem.gxTpr_Sectiontitle = A278Menu_Nome;
            Gxm1sectionsitem.gxTpr_Sectiondescription = A279Menu_Descricao;
            Gxm1sectionsitem.gxTpr_Sectionurl = A281Menu_Link;
            Gxm1sectionsitem.gxTpr_Sectionorder = A283Menu_Ordem;
            pr_default.readNext(0);
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00062_A285Menu_PaiCod = new int[1] ;
         P00062_n285Menu_PaiCod = new bool[] {false} ;
         P00062_A284Menu_Ativo = new bool[] {false} ;
         P00062_A280Menu_Tipo = new short[1] ;
         P00062_A283Menu_Ordem = new short[1] ;
         P00062_A279Menu_Descricao = new String[] {""} ;
         P00062_n279Menu_Descricao = new bool[] {false} ;
         P00062_A281Menu_Link = new String[] {""} ;
         P00062_n281Menu_Link = new bool[] {false} ;
         P00062_A278Menu_Nome = new String[] {""} ;
         P00062_A277Menu_Codigo = new int[1] ;
         A279Menu_Descricao = "";
         A281Menu_Link = "";
         A278Menu_Nome = "";
         Gxm1sectionsitem = new SdtSectionsItem(context);
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.dp_tabsmenudatasectionsadm__default(),
            new Object[][] {
                new Object[] {
               P00062_A285Menu_PaiCod, P00062_n285Menu_PaiCod, P00062_A284Menu_Ativo, P00062_A280Menu_Tipo, P00062_A283Menu_Ordem, P00062_A279Menu_Descricao, P00062_n279Menu_Descricao, P00062_A281Menu_Link, P00062_n281Menu_Link, P00062_A278Menu_Nome,
               P00062_A277Menu_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short A280Menu_Tipo ;
      private short A283Menu_Ordem ;
      private int AV5Menu_PaiCod ;
      private int A285Menu_PaiCod ;
      private int A277Menu_Codigo ;
      private String scmdbuf ;
      private String A278Menu_Nome ;
      private bool n285Menu_PaiCod ;
      private bool A284Menu_Ativo ;
      private bool n279Menu_Descricao ;
      private bool n281Menu_Link ;
      private String A279Menu_Descricao ;
      private String A281Menu_Link ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00062_A285Menu_PaiCod ;
      private bool[] P00062_n285Menu_PaiCod ;
      private bool[] P00062_A284Menu_Ativo ;
      private short[] P00062_A280Menu_Tipo ;
      private short[] P00062_A283Menu_Ordem ;
      private String[] P00062_A279Menu_Descricao ;
      private bool[] P00062_n279Menu_Descricao ;
      private String[] P00062_A281Menu_Link ;
      private bool[] P00062_n281Menu_Link ;
      private String[] P00062_A278Menu_Nome ;
      private int[] P00062_A277Menu_Codigo ;
      private IGxCollection aP1_Gxm2rootcol ;
      [ObjectCollection(ItemType=typeof( SdtSectionsItem ))]
      private IGxCollection Gxm2rootcol ;
      private SdtSectionsItem Gxm1sectionsitem ;
   }

   public class dp_tabsmenudatasectionsadm__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00062 ;
          prmP00062 = new Object[] {
          new Object[] {"@AV5Menu_PaiCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00062", "SELECT DISTINCT NULL AS [Menu_PaiCod], NULL AS [Menu_Ativo], NULL AS [Menu_Tipo], [Menu_Ordem], [Menu_Descricao], [Menu_Link], [Menu_Nome], [Menu_Codigo] FROM ( SELECT TOP(100) PERCENT [Menu_PaiCod], [Menu_Ativo], [Menu_Tipo], [Menu_Ordem], [Menu_Descricao], [Menu_Link], [Menu_Nome], [Menu_Codigo] FROM [Menu] WITH (NOLOCK) WHERE ([Menu_PaiCod] = @AV5Menu_PaiCod) AND ([Menu_Tipo] = 1) AND ([Menu_Ativo] = 1) ORDER BY [Menu_PaiCod]) DistinctT ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00062,100,0,false,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((bool[]) buf[2])[0] = rslt.getBool(2) ;
                ((short[]) buf[3])[0] = rslt.getShort(3) ;
                ((short[]) buf[4])[0] = rslt.getShort(4) ;
                ((String[]) buf[5])[0] = rslt.getVarchar(5) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((String[]) buf[7])[0] = rslt.getVarchar(6) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((String[]) buf[9])[0] = rslt.getString(7, 30) ;
                ((int[]) buf[10])[0] = rslt.getInt(8) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
