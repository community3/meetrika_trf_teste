/*
               File: type_SdtSolicitacaoServico
        Description: Solicitacao Servico
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/2/2020 16:18:20.76
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "SolicitacaoServico" )]
   [XmlType(TypeName =  "SolicitacaoServico" , Namespace = "GxEv3Up14_MeetrikaVs3" )]
   [Serializable]
   public class SdtSolicitacaoServico : GxSilentTrnSdt, System.Web.SessionState.IRequiresSessionState
   {
      public SdtSolicitacaoServico( )
      {
         /* Constructor for serialization */
         gxTv_SdtSolicitacaoServico_Contagemresultado_descricao = "";
         gxTv_SdtSolicitacaoServico_Contagemresultado_observacao = "";
         gxTv_SdtSolicitacaoServico_Contagemresultado_dataentrega = DateTime.MinValue;
         gxTv_SdtSolicitacaoServico_Contagemresultado_horaentrega = (DateTime)(DateTime.MinValue);
         gxTv_SdtSolicitacaoServico_Contagemresultado_dataprevista = (DateTime)(DateTime.MinValue);
         gxTv_SdtSolicitacaoServico_Contagemresultado_demandafm = "";
         gxTv_SdtSolicitacaoServico_Contagemresultado_referencia = "";
         gxTv_SdtSolicitacaoServico_Contagemresultado_restricoes = "";
         gxTv_SdtSolicitacaoServico_Contagemresultado_prioridadeprevista = "";
         gxTv_SdtSolicitacaoServico_Contagemresultado_statusdmn = "";
         gxTv_SdtSolicitacaoServico_Contagemresultado_datadmn = DateTime.MinValue;
         gxTv_SdtSolicitacaoServico_Contagemresultado_datacadastro = (DateTime)(DateTime.MinValue);
         gxTv_SdtSolicitacaoServico_Mode = "";
         gxTv_SdtSolicitacaoServico_Contagemresultado_descricao_Z = "";
         gxTv_SdtSolicitacaoServico_Contagemresultado_dataentrega_Z = DateTime.MinValue;
         gxTv_SdtSolicitacaoServico_Contagemresultado_horaentrega_Z = (DateTime)(DateTime.MinValue);
         gxTv_SdtSolicitacaoServico_Contagemresultado_dataprevista_Z = (DateTime)(DateTime.MinValue);
         gxTv_SdtSolicitacaoServico_Contagemresultado_demandafm_Z = "";
         gxTv_SdtSolicitacaoServico_Contagemresultado_referencia_Z = "";
         gxTv_SdtSolicitacaoServico_Contagemresultado_restricoes_Z = "";
         gxTv_SdtSolicitacaoServico_Contagemresultado_prioridadeprevista_Z = "";
         gxTv_SdtSolicitacaoServico_Contagemresultado_statusdmn_Z = "";
         gxTv_SdtSolicitacaoServico_Contagemresultado_datadmn_Z = DateTime.MinValue;
         gxTv_SdtSolicitacaoServico_Contagemresultado_datacadastro_Z = (DateTime)(DateTime.MinValue);
      }

      public SdtSolicitacaoServico( IGxContext context )
      {
         this.context = context;
         constructorCallingAssembly = Assembly.GetCallingAssembly();
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public void Load( int AV456ContagemResultado_Codigo )
      {
         IGxSilentTrn obj ;
         obj = getTransaction();
         obj.LoadKey(new Object[] {(int)AV456ContagemResultado_Codigo});
         return  ;
      }

      public override Object[][] GetBCKey( )
      {
         return (Object[][])(new Object[][]{new Object[]{"ContagemResultado_Codigo", typeof(int)}}) ;
      }

      public override IGxCollection GetMessages( )
      {
         short item = 1 ;
         IGxCollection msgs = new GxObjectCollection( context, "Messages.Message", "Genexus", "SdtMessages_Message", "GeneXus.Programs") ;
         SdtMessages_Message m1 ;
         IGxSilentTrn trn = getTransaction() ;
         msglist msgList = trn.GetMessages() ;
         while ( item <= msgList.ItemCount )
         {
            m1 = new SdtMessages_Message(context);
            m1.gxTpr_Id = msgList.getItemValue(item);
            m1.gxTpr_Description = msgList.getItemText(item);
            m1.gxTpr_Type = msgList.getItemType(item);
            msgs.Add(m1, 0);
            item = (short)(item+1);
         }
         return msgs ;
      }

      public override GXProperties GetMetadata( )
      {
         GXProperties metadata = new GXProperties() ;
         metadata.Set("Name", "SolicitacaoServico");
         metadata.Set("BT", "ContagemResultado");
         metadata.Set("PK", "[ \"ContagemResultado_Codigo\" ]");
         metadata.Set("PKAssigned", "[ \"ContagemResultado_Codigo\" ]");
         metadata.Set("FKList", "[ { \"FK\":[ \"ContagemResultadoLiqLog_Codigo\" ],\"FKMap\":[ \"ContagemResultado_LiqLogCod-ContagemResultadoLiqLog_Codigo\" ] },{ \"FK\":[ \"ContagemResultado_Codigo\" ],\"FKMap\":[ \"ContagemResultado_OSVinculada-ContagemResultado_Codigo\" ] },{ \"FK\":[ \"Contratada_Codigo\" ],\"FKMap\":[ \"ContagemResultado_ContratadaCod-Contratada_Codigo\" ] },{ \"FK\":[ \"Contratada_Codigo\" ],\"FKMap\":[ \"ContagemResultado_ContratadaOrigemCod-Contratada_Codigo\" ] },{ \"FK\":[ \"ContratoServicos_Codigo\" ],\"FKMap\":[ \"ContagemResultado_CntSrvCod-ContratoServicos_Codigo\" ] },{ \"FK\":[ \"Lote_Codigo\" ],\"FKMap\":[ \"ContagemResultado_LoteAceiteCod-Lote_Codigo\" ] },{ \"FK\":[ \"Modulo_Codigo\" ],\"FKMap\":[  ] },{ \"FK\":[ \"NaoConformidade_Codigo\" ],\"FKMap\":[ \"ContagemResultado_NaoCnfDmnCod-NaoConformidade_Codigo\" ] },{ \"FK\":[ \"Servico_Codigo\" ],\"FKMap\":[ \"ContagemResultado_ServicoSS-Servico_Codigo\" ] },{ \"FK\":[ \"Sistema_Codigo\" ],\"FKMap\":[ \"ContagemResultado_SistemaCod-Sistema_Codigo\" ] },{ \"FK\":[ \"Usuario_Codigo\" ],\"FKMap\":[ \"ContagemResultado_ContadorFSCod-Usuario_Codigo\" ] },{ \"FK\":[ \"Usuario_Codigo\" ],\"FKMap\":[ \"ContagemResultado_Responsavel-Usuario_Codigo\" ] } ]");
         metadata.Set("AllowInsert", "True");
         metadata.Set("AllowUpdate", "True");
         metadata.Set("AllowDelete", "True");
         return metadata ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         if ( ! includeState )
         {
            XmlAttributeOverrides ov = new XmlAttributeOverrides();
            XmlAttributes attrs = new XmlAttributes();
            attrs.XmlIgnore = true;
            ov.Add(this.GetType(),  "gxTpr_Mode" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Initialized" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultado_codigo_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultado_owner_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultado_descricao_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultado_dataentrega_Z_Nullable" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultado_horaentrega_Z_Nullable" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultado_dataprevista_Z_Nullable" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultado_servicoss_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultado_ss_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultado_demandafm_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultado_uoowner_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultado_evento_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultado_tiporegistro_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultado_referencia_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultado_restricoes_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultado_prioridadeprevista_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultado_statusdmn_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultado_datadmn_Z_Nullable" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultado_cntsrvcod_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultado_osvinculada_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultado_naocnfdmncod_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultado_loteaceitecod_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultado_contratadaorigemcod_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultado_contadorfscod_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultado_responsavel_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultado_liqlogcod_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Modulo_codigo_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultado_contratadacod_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultado_sistemacod_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultado_fncusrcod_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultado_prazoinicialdias_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultado_datacadastro_Z_Nullable" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultado_osmanual_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultado_baseline_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultado_ehvalidacao_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultado_descricao_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultado_observacao_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultado_dataentrega_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultado_horaentrega_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultado_dataprevista_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultado_servicoss_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultado_ss_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultado_demandafm_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultado_uoowner_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultado_evento_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultado_referencia_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultado_restricoes_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultado_prioridadeprevista_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultado_statusdmn_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultado_cntsrvcod_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultado_osvinculada_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultado_naocnfdmncod_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultado_loteaceitecod_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultado_contratadaorigemcod_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultado_contadorfscod_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultado_responsavel_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultado_liqlogcod_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Modulo_codigo_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultado_contratadacod_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultado_sistemacod_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultado_fncusrcod_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultado_prazoinicialdias_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultado_datacadastro_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultado_osmanual_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultado_baseline_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Contagemresultado_ehvalidacao_N" , attrs);
            xmls = new XmlSerializer(this.GetType(), ov, new Type[0], null, sNameSpace);
         }
         else
         {
            xmls = new XmlSerializer(this.GetType(), sNameSpace);
         }
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtSolicitacaoServico deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "GxEv3Up14_MeetrikaVs3" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtSolicitacaoServico)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtSolicitacaoServico obj ;
         obj = this;
         obj.gxTpr_Contagemresultado_codigo = deserialized.gxTpr_Contagemresultado_codigo;
         obj.gxTpr_Contagemresultado_owner = deserialized.gxTpr_Contagemresultado_owner;
         obj.gxTpr_Contagemresultado_descricao = deserialized.gxTpr_Contagemresultado_descricao;
         obj.gxTpr_Contagemresultado_observacao = deserialized.gxTpr_Contagemresultado_observacao;
         obj.gxTpr_Contagemresultado_dataentrega = deserialized.gxTpr_Contagemresultado_dataentrega;
         obj.gxTpr_Contagemresultado_horaentrega = deserialized.gxTpr_Contagemresultado_horaentrega;
         obj.gxTpr_Contagemresultado_dataprevista = deserialized.gxTpr_Contagemresultado_dataprevista;
         obj.gxTpr_Contagemresultado_servicoss = deserialized.gxTpr_Contagemresultado_servicoss;
         obj.gxTpr_Contagemresultado_ss = deserialized.gxTpr_Contagemresultado_ss;
         obj.gxTpr_Contagemresultado_demandafm = deserialized.gxTpr_Contagemresultado_demandafm;
         obj.gxTpr_Contagemresultado_uoowner = deserialized.gxTpr_Contagemresultado_uoowner;
         obj.gxTpr_Contagemresultado_evento = deserialized.gxTpr_Contagemresultado_evento;
         obj.gxTpr_Contagemresultado_tiporegistro = deserialized.gxTpr_Contagemresultado_tiporegistro;
         obj.gxTpr_Contagemresultado_referencia = deserialized.gxTpr_Contagemresultado_referencia;
         obj.gxTpr_Contagemresultado_restricoes = deserialized.gxTpr_Contagemresultado_restricoes;
         obj.gxTpr_Contagemresultado_prioridadeprevista = deserialized.gxTpr_Contagemresultado_prioridadeprevista;
         obj.gxTpr_Contagemresultado_statusdmn = deserialized.gxTpr_Contagemresultado_statusdmn;
         obj.gxTpr_Contagemresultado_datadmn = deserialized.gxTpr_Contagemresultado_datadmn;
         obj.gxTpr_Contagemresultado_cntsrvcod = deserialized.gxTpr_Contagemresultado_cntsrvcod;
         obj.gxTpr_Contagemresultado_osvinculada = deserialized.gxTpr_Contagemresultado_osvinculada;
         obj.gxTpr_Contagemresultado_naocnfdmncod = deserialized.gxTpr_Contagemresultado_naocnfdmncod;
         obj.gxTpr_Contagemresultado_loteaceitecod = deserialized.gxTpr_Contagemresultado_loteaceitecod;
         obj.gxTpr_Contagemresultado_contratadaorigemcod = deserialized.gxTpr_Contagemresultado_contratadaorigemcod;
         obj.gxTpr_Contagemresultado_contadorfscod = deserialized.gxTpr_Contagemresultado_contadorfscod;
         obj.gxTpr_Contagemresultado_responsavel = deserialized.gxTpr_Contagemresultado_responsavel;
         obj.gxTpr_Contagemresultado_liqlogcod = deserialized.gxTpr_Contagemresultado_liqlogcod;
         obj.gxTpr_Modulo_codigo = deserialized.gxTpr_Modulo_codigo;
         obj.gxTpr_Contagemresultado_contratadacod = deserialized.gxTpr_Contagemresultado_contratadacod;
         obj.gxTpr_Contagemresultado_sistemacod = deserialized.gxTpr_Contagemresultado_sistemacod;
         obj.gxTpr_Contagemresultado_fncusrcod = deserialized.gxTpr_Contagemresultado_fncusrcod;
         obj.gxTpr_Contagemresultado_prazoinicialdias = deserialized.gxTpr_Contagemresultado_prazoinicialdias;
         obj.gxTpr_Contagemresultado_datacadastro = deserialized.gxTpr_Contagemresultado_datacadastro;
         obj.gxTpr_Contagemresultado_osmanual = deserialized.gxTpr_Contagemresultado_osmanual;
         obj.gxTpr_Contagemresultado_baseline = deserialized.gxTpr_Contagemresultado_baseline;
         obj.gxTpr_Contagemresultado_ehvalidacao = deserialized.gxTpr_Contagemresultado_ehvalidacao;
         obj.gxTpr_Mode = deserialized.gxTpr_Mode;
         obj.gxTpr_Initialized = deserialized.gxTpr_Initialized;
         obj.gxTpr_Contagemresultado_codigo_Z = deserialized.gxTpr_Contagemresultado_codigo_Z;
         obj.gxTpr_Contagemresultado_owner_Z = deserialized.gxTpr_Contagemresultado_owner_Z;
         obj.gxTpr_Contagemresultado_descricao_Z = deserialized.gxTpr_Contagemresultado_descricao_Z;
         obj.gxTpr_Contagemresultado_dataentrega_Z = deserialized.gxTpr_Contagemresultado_dataentrega_Z;
         obj.gxTpr_Contagemresultado_horaentrega_Z = deserialized.gxTpr_Contagemresultado_horaentrega_Z;
         obj.gxTpr_Contagemresultado_dataprevista_Z = deserialized.gxTpr_Contagemresultado_dataprevista_Z;
         obj.gxTpr_Contagemresultado_servicoss_Z = deserialized.gxTpr_Contagemresultado_servicoss_Z;
         obj.gxTpr_Contagemresultado_ss_Z = deserialized.gxTpr_Contagemresultado_ss_Z;
         obj.gxTpr_Contagemresultado_demandafm_Z = deserialized.gxTpr_Contagemresultado_demandafm_Z;
         obj.gxTpr_Contagemresultado_uoowner_Z = deserialized.gxTpr_Contagemresultado_uoowner_Z;
         obj.gxTpr_Contagemresultado_evento_Z = deserialized.gxTpr_Contagemresultado_evento_Z;
         obj.gxTpr_Contagemresultado_tiporegistro_Z = deserialized.gxTpr_Contagemresultado_tiporegistro_Z;
         obj.gxTpr_Contagemresultado_referencia_Z = deserialized.gxTpr_Contagemresultado_referencia_Z;
         obj.gxTpr_Contagemresultado_restricoes_Z = deserialized.gxTpr_Contagemresultado_restricoes_Z;
         obj.gxTpr_Contagemresultado_prioridadeprevista_Z = deserialized.gxTpr_Contagemresultado_prioridadeprevista_Z;
         obj.gxTpr_Contagemresultado_statusdmn_Z = deserialized.gxTpr_Contagemresultado_statusdmn_Z;
         obj.gxTpr_Contagemresultado_datadmn_Z = deserialized.gxTpr_Contagemresultado_datadmn_Z;
         obj.gxTpr_Contagemresultado_cntsrvcod_Z = deserialized.gxTpr_Contagemresultado_cntsrvcod_Z;
         obj.gxTpr_Contagemresultado_osvinculada_Z = deserialized.gxTpr_Contagemresultado_osvinculada_Z;
         obj.gxTpr_Contagemresultado_naocnfdmncod_Z = deserialized.gxTpr_Contagemresultado_naocnfdmncod_Z;
         obj.gxTpr_Contagemresultado_loteaceitecod_Z = deserialized.gxTpr_Contagemresultado_loteaceitecod_Z;
         obj.gxTpr_Contagemresultado_contratadaorigemcod_Z = deserialized.gxTpr_Contagemresultado_contratadaorigemcod_Z;
         obj.gxTpr_Contagemresultado_contadorfscod_Z = deserialized.gxTpr_Contagemresultado_contadorfscod_Z;
         obj.gxTpr_Contagemresultado_responsavel_Z = deserialized.gxTpr_Contagemresultado_responsavel_Z;
         obj.gxTpr_Contagemresultado_liqlogcod_Z = deserialized.gxTpr_Contagemresultado_liqlogcod_Z;
         obj.gxTpr_Modulo_codigo_Z = deserialized.gxTpr_Modulo_codigo_Z;
         obj.gxTpr_Contagemresultado_contratadacod_Z = deserialized.gxTpr_Contagemresultado_contratadacod_Z;
         obj.gxTpr_Contagemresultado_sistemacod_Z = deserialized.gxTpr_Contagemresultado_sistemacod_Z;
         obj.gxTpr_Contagemresultado_fncusrcod_Z = deserialized.gxTpr_Contagemresultado_fncusrcod_Z;
         obj.gxTpr_Contagemresultado_prazoinicialdias_Z = deserialized.gxTpr_Contagemresultado_prazoinicialdias_Z;
         obj.gxTpr_Contagemresultado_datacadastro_Z = deserialized.gxTpr_Contagemresultado_datacadastro_Z;
         obj.gxTpr_Contagemresultado_osmanual_Z = deserialized.gxTpr_Contagemresultado_osmanual_Z;
         obj.gxTpr_Contagemresultado_baseline_Z = deserialized.gxTpr_Contagemresultado_baseline_Z;
         obj.gxTpr_Contagemresultado_ehvalidacao_Z = deserialized.gxTpr_Contagemresultado_ehvalidacao_Z;
         obj.gxTpr_Contagemresultado_descricao_N = deserialized.gxTpr_Contagemresultado_descricao_N;
         obj.gxTpr_Contagemresultado_observacao_N = deserialized.gxTpr_Contagemresultado_observacao_N;
         obj.gxTpr_Contagemresultado_dataentrega_N = deserialized.gxTpr_Contagemresultado_dataentrega_N;
         obj.gxTpr_Contagemresultado_horaentrega_N = deserialized.gxTpr_Contagemresultado_horaentrega_N;
         obj.gxTpr_Contagemresultado_dataprevista_N = deserialized.gxTpr_Contagemresultado_dataprevista_N;
         obj.gxTpr_Contagemresultado_servicoss_N = deserialized.gxTpr_Contagemresultado_servicoss_N;
         obj.gxTpr_Contagemresultado_ss_N = deserialized.gxTpr_Contagemresultado_ss_N;
         obj.gxTpr_Contagemresultado_demandafm_N = deserialized.gxTpr_Contagemresultado_demandafm_N;
         obj.gxTpr_Contagemresultado_uoowner_N = deserialized.gxTpr_Contagemresultado_uoowner_N;
         obj.gxTpr_Contagemresultado_evento_N = deserialized.gxTpr_Contagemresultado_evento_N;
         obj.gxTpr_Contagemresultado_referencia_N = deserialized.gxTpr_Contagemresultado_referencia_N;
         obj.gxTpr_Contagemresultado_restricoes_N = deserialized.gxTpr_Contagemresultado_restricoes_N;
         obj.gxTpr_Contagemresultado_prioridadeprevista_N = deserialized.gxTpr_Contagemresultado_prioridadeprevista_N;
         obj.gxTpr_Contagemresultado_statusdmn_N = deserialized.gxTpr_Contagemresultado_statusdmn_N;
         obj.gxTpr_Contagemresultado_cntsrvcod_N = deserialized.gxTpr_Contagemresultado_cntsrvcod_N;
         obj.gxTpr_Contagemresultado_osvinculada_N = deserialized.gxTpr_Contagemresultado_osvinculada_N;
         obj.gxTpr_Contagemresultado_naocnfdmncod_N = deserialized.gxTpr_Contagemresultado_naocnfdmncod_N;
         obj.gxTpr_Contagemresultado_loteaceitecod_N = deserialized.gxTpr_Contagemresultado_loteaceitecod_N;
         obj.gxTpr_Contagemresultado_contratadaorigemcod_N = deserialized.gxTpr_Contagemresultado_contratadaorigemcod_N;
         obj.gxTpr_Contagemresultado_contadorfscod_N = deserialized.gxTpr_Contagemresultado_contadorfscod_N;
         obj.gxTpr_Contagemresultado_responsavel_N = deserialized.gxTpr_Contagemresultado_responsavel_N;
         obj.gxTpr_Contagemresultado_liqlogcod_N = deserialized.gxTpr_Contagemresultado_liqlogcod_N;
         obj.gxTpr_Modulo_codigo_N = deserialized.gxTpr_Modulo_codigo_N;
         obj.gxTpr_Contagemresultado_contratadacod_N = deserialized.gxTpr_Contagemresultado_contratadacod_N;
         obj.gxTpr_Contagemresultado_sistemacod_N = deserialized.gxTpr_Contagemresultado_sistemacod_N;
         obj.gxTpr_Contagemresultado_fncusrcod_N = deserialized.gxTpr_Contagemresultado_fncusrcod_N;
         obj.gxTpr_Contagemresultado_prazoinicialdias_N = deserialized.gxTpr_Contagemresultado_prazoinicialdias_N;
         obj.gxTpr_Contagemresultado_datacadastro_N = deserialized.gxTpr_Contagemresultado_datacadastro_N;
         obj.gxTpr_Contagemresultado_osmanual_N = deserialized.gxTpr_Contagemresultado_osmanual_N;
         obj.gxTpr_Contagemresultado_baseline_N = deserialized.gxTpr_Contagemresultado_baseline_N;
         obj.gxTpr_Contagemresultado_ehvalidacao_N = deserialized.gxTpr_Contagemresultado_ehvalidacao_N;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_Codigo") )
               {
                  gxTv_SdtSolicitacaoServico_Contagemresultado_codigo = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_Owner") )
               {
                  gxTv_SdtSolicitacaoServico_Contagemresultado_owner = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_Descricao") )
               {
                  gxTv_SdtSolicitacaoServico_Contagemresultado_descricao = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_Observacao") )
               {
                  gxTv_SdtSolicitacaoServico_Contagemresultado_observacao = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_DataEntrega") )
               {
                  if ( ( StringUtil.StrCmp(oReader.Value, "0000-00-00") == 0 ) || ( oReader.ExistsAttribute("xsi:nil") == 1 ) )
                  {
                     gxTv_SdtSolicitacaoServico_Contagemresultado_dataentrega = DateTime.MinValue;
                  }
                  else
                  {
                     gxTv_SdtSolicitacaoServico_Contagemresultado_dataentrega = context.localUtil.YMDToD( (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 1, 4), ".")), (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 6, 2), ".")), (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 9, 2), ".")));
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_HoraEntrega") )
               {
                  if ( ( StringUtil.StrCmp(oReader.Value, "0000-00-00T00:00:00") == 0 ) || ( oReader.ExistsAttribute("xsi:nil") == 1 ) )
                  {
                     gxTv_SdtSolicitacaoServico_Contagemresultado_horaentrega = (DateTime)(DateTime.MinValue);
                  }
                  else
                  {
                     gxTv_SdtSolicitacaoServico_Contagemresultado_horaentrega = DateTimeUtil.ResetDate(context.localUtil.YMDHMSToT( (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 1, 4), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 6, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 9, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 12, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 15, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 18, 2), "."))));
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_DataPrevista") )
               {
                  if ( ( StringUtil.StrCmp(oReader.Value, "0000-00-00T00:00:00") == 0 ) || ( oReader.ExistsAttribute("xsi:nil") == 1 ) )
                  {
                     gxTv_SdtSolicitacaoServico_Contagemresultado_dataprevista = (DateTime)(DateTime.MinValue);
                  }
                  else
                  {
                     gxTv_SdtSolicitacaoServico_Contagemresultado_dataprevista = context.localUtil.YMDHMSToT( (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 1, 4), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 6, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 9, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 12, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 15, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 18, 2), ".")));
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_ServicoSS") )
               {
                  gxTv_SdtSolicitacaoServico_Contagemresultado_servicoss = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_SS") )
               {
                  gxTv_SdtSolicitacaoServico_Contagemresultado_ss = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_DemandaFM") )
               {
                  gxTv_SdtSolicitacaoServico_Contagemresultado_demandafm = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_UOOwner") )
               {
                  gxTv_SdtSolicitacaoServico_Contagemresultado_uoowner = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_Evento") )
               {
                  gxTv_SdtSolicitacaoServico_Contagemresultado_evento = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_TipoRegistro") )
               {
                  gxTv_SdtSolicitacaoServico_Contagemresultado_tiporegistro = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_Referencia") )
               {
                  gxTv_SdtSolicitacaoServico_Contagemresultado_referencia = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_Restricoes") )
               {
                  gxTv_SdtSolicitacaoServico_Contagemresultado_restricoes = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_PrioridadePrevista") )
               {
                  gxTv_SdtSolicitacaoServico_Contagemresultado_prioridadeprevista = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_StatusDmn") )
               {
                  gxTv_SdtSolicitacaoServico_Contagemresultado_statusdmn = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_DataDmn") )
               {
                  if ( ( StringUtil.StrCmp(oReader.Value, "0000-00-00") == 0 ) || ( oReader.ExistsAttribute("xsi:nil") == 1 ) )
                  {
                     gxTv_SdtSolicitacaoServico_Contagemresultado_datadmn = DateTime.MinValue;
                  }
                  else
                  {
                     gxTv_SdtSolicitacaoServico_Contagemresultado_datadmn = context.localUtil.YMDToD( (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 1, 4), ".")), (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 6, 2), ".")), (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 9, 2), ".")));
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_CntSrvCod") )
               {
                  gxTv_SdtSolicitacaoServico_Contagemresultado_cntsrvcod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_OSVinculada") )
               {
                  gxTv_SdtSolicitacaoServico_Contagemresultado_osvinculada = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_NaoCnfDmnCod") )
               {
                  gxTv_SdtSolicitacaoServico_Contagemresultado_naocnfdmncod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_LoteAceiteCod") )
               {
                  gxTv_SdtSolicitacaoServico_Contagemresultado_loteaceitecod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_ContratadaOrigemCod") )
               {
                  gxTv_SdtSolicitacaoServico_Contagemresultado_contratadaorigemcod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_ContadorFSCod") )
               {
                  gxTv_SdtSolicitacaoServico_Contagemresultado_contadorfscod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_Responsavel") )
               {
                  gxTv_SdtSolicitacaoServico_Contagemresultado_responsavel = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_LiqLogCod") )
               {
                  gxTv_SdtSolicitacaoServico_Contagemresultado_liqlogcod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Modulo_Codigo") )
               {
                  gxTv_SdtSolicitacaoServico_Modulo_codigo = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_ContratadaCod") )
               {
                  gxTv_SdtSolicitacaoServico_Contagemresultado_contratadacod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_SistemaCod") )
               {
                  gxTv_SdtSolicitacaoServico_Contagemresultado_sistemacod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_FncUsrCod") )
               {
                  gxTv_SdtSolicitacaoServico_Contagemresultado_fncusrcod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_PrazoInicialDias") )
               {
                  gxTv_SdtSolicitacaoServico_Contagemresultado_prazoinicialdias = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_DataCadastro") )
               {
                  if ( ( StringUtil.StrCmp(oReader.Value, "0000-00-00T00:00:00") == 0 ) || ( oReader.ExistsAttribute("xsi:nil") == 1 ) )
                  {
                     gxTv_SdtSolicitacaoServico_Contagemresultado_datacadastro = (DateTime)(DateTime.MinValue);
                  }
                  else
                  {
                     gxTv_SdtSolicitacaoServico_Contagemresultado_datacadastro = context.localUtil.YMDHMSToT( (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 1, 4), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 6, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 9, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 12, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 15, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 18, 2), ".")));
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_OSManual") )
               {
                  gxTv_SdtSolicitacaoServico_Contagemresultado_osmanual = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contagemresultado_baseline") )
               {
                  gxTv_SdtSolicitacaoServico_Contagemresultado_baseline = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contagemresultado_ehvalidacao") )
               {
                  gxTv_SdtSolicitacaoServico_Contagemresultado_ehvalidacao = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Mode") )
               {
                  gxTv_SdtSolicitacaoServico_Mode = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Initialized") )
               {
                  gxTv_SdtSolicitacaoServico_Initialized = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_Codigo_Z") )
               {
                  gxTv_SdtSolicitacaoServico_Contagemresultado_codigo_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_Owner_Z") )
               {
                  gxTv_SdtSolicitacaoServico_Contagemresultado_owner_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_Descricao_Z") )
               {
                  gxTv_SdtSolicitacaoServico_Contagemresultado_descricao_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_DataEntrega_Z") )
               {
                  if ( ( StringUtil.StrCmp(oReader.Value, "0000-00-00") == 0 ) || ( oReader.ExistsAttribute("xsi:nil") == 1 ) )
                  {
                     gxTv_SdtSolicitacaoServico_Contagemresultado_dataentrega_Z = DateTime.MinValue;
                  }
                  else
                  {
                     gxTv_SdtSolicitacaoServico_Contagemresultado_dataentrega_Z = context.localUtil.YMDToD( (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 1, 4), ".")), (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 6, 2), ".")), (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 9, 2), ".")));
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_HoraEntrega_Z") )
               {
                  if ( ( StringUtil.StrCmp(oReader.Value, "0000-00-00T00:00:00") == 0 ) || ( oReader.ExistsAttribute("xsi:nil") == 1 ) )
                  {
                     gxTv_SdtSolicitacaoServico_Contagemresultado_horaentrega_Z = (DateTime)(DateTime.MinValue);
                  }
                  else
                  {
                     gxTv_SdtSolicitacaoServico_Contagemresultado_horaentrega_Z = DateTimeUtil.ResetDate(context.localUtil.YMDHMSToT( (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 1, 4), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 6, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 9, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 12, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 15, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 18, 2), "."))));
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_DataPrevista_Z") )
               {
                  if ( ( StringUtil.StrCmp(oReader.Value, "0000-00-00T00:00:00") == 0 ) || ( oReader.ExistsAttribute("xsi:nil") == 1 ) )
                  {
                     gxTv_SdtSolicitacaoServico_Contagemresultado_dataprevista_Z = (DateTime)(DateTime.MinValue);
                  }
                  else
                  {
                     gxTv_SdtSolicitacaoServico_Contagemresultado_dataprevista_Z = context.localUtil.YMDHMSToT( (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 1, 4), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 6, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 9, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 12, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 15, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 18, 2), ".")));
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_ServicoSS_Z") )
               {
                  gxTv_SdtSolicitacaoServico_Contagemresultado_servicoss_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_SS_Z") )
               {
                  gxTv_SdtSolicitacaoServico_Contagemresultado_ss_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_DemandaFM_Z") )
               {
                  gxTv_SdtSolicitacaoServico_Contagemresultado_demandafm_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_UOOwner_Z") )
               {
                  gxTv_SdtSolicitacaoServico_Contagemresultado_uoowner_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_Evento_Z") )
               {
                  gxTv_SdtSolicitacaoServico_Contagemresultado_evento_Z = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_TipoRegistro_Z") )
               {
                  gxTv_SdtSolicitacaoServico_Contagemresultado_tiporegistro_Z = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_Referencia_Z") )
               {
                  gxTv_SdtSolicitacaoServico_Contagemresultado_referencia_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_Restricoes_Z") )
               {
                  gxTv_SdtSolicitacaoServico_Contagemresultado_restricoes_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_PrioridadePrevista_Z") )
               {
                  gxTv_SdtSolicitacaoServico_Contagemresultado_prioridadeprevista_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_StatusDmn_Z") )
               {
                  gxTv_SdtSolicitacaoServico_Contagemresultado_statusdmn_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_DataDmn_Z") )
               {
                  if ( ( StringUtil.StrCmp(oReader.Value, "0000-00-00") == 0 ) || ( oReader.ExistsAttribute("xsi:nil") == 1 ) )
                  {
                     gxTv_SdtSolicitacaoServico_Contagemresultado_datadmn_Z = DateTime.MinValue;
                  }
                  else
                  {
                     gxTv_SdtSolicitacaoServico_Contagemresultado_datadmn_Z = context.localUtil.YMDToD( (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 1, 4), ".")), (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 6, 2), ".")), (int)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 9, 2), ".")));
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_CntSrvCod_Z") )
               {
                  gxTv_SdtSolicitacaoServico_Contagemresultado_cntsrvcod_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_OSVinculada_Z") )
               {
                  gxTv_SdtSolicitacaoServico_Contagemresultado_osvinculada_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_NaoCnfDmnCod_Z") )
               {
                  gxTv_SdtSolicitacaoServico_Contagemresultado_naocnfdmncod_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_LoteAceiteCod_Z") )
               {
                  gxTv_SdtSolicitacaoServico_Contagemresultado_loteaceitecod_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_ContratadaOrigemCod_Z") )
               {
                  gxTv_SdtSolicitacaoServico_Contagemresultado_contratadaorigemcod_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_ContadorFSCod_Z") )
               {
                  gxTv_SdtSolicitacaoServico_Contagemresultado_contadorfscod_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_Responsavel_Z") )
               {
                  gxTv_SdtSolicitacaoServico_Contagemresultado_responsavel_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_LiqLogCod_Z") )
               {
                  gxTv_SdtSolicitacaoServico_Contagemresultado_liqlogcod_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Modulo_Codigo_Z") )
               {
                  gxTv_SdtSolicitacaoServico_Modulo_codigo_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_ContratadaCod_Z") )
               {
                  gxTv_SdtSolicitacaoServico_Contagemresultado_contratadacod_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_SistemaCod_Z") )
               {
                  gxTv_SdtSolicitacaoServico_Contagemresultado_sistemacod_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_FncUsrCod_Z") )
               {
                  gxTv_SdtSolicitacaoServico_Contagemresultado_fncusrcod_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_PrazoInicialDias_Z") )
               {
                  gxTv_SdtSolicitacaoServico_Contagemresultado_prazoinicialdias_Z = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_DataCadastro_Z") )
               {
                  if ( ( StringUtil.StrCmp(oReader.Value, "0000-00-00T00:00:00") == 0 ) || ( oReader.ExistsAttribute("xsi:nil") == 1 ) )
                  {
                     gxTv_SdtSolicitacaoServico_Contagemresultado_datacadastro_Z = (DateTime)(DateTime.MinValue);
                  }
                  else
                  {
                     gxTv_SdtSolicitacaoServico_Contagemresultado_datacadastro_Z = context.localUtil.YMDHMSToT( (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 1, 4), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 6, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 9, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 12, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 15, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 18, 2), ".")));
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_OSManual_Z") )
               {
                  gxTv_SdtSolicitacaoServico_Contagemresultado_osmanual_Z = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contagemresultado_baseline_Z") )
               {
                  gxTv_SdtSolicitacaoServico_Contagemresultado_baseline_Z = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contagemresultado_ehvalidacao_Z") )
               {
                  gxTv_SdtSolicitacaoServico_Contagemresultado_ehvalidacao_Z = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_Descricao_N") )
               {
                  gxTv_SdtSolicitacaoServico_Contagemresultado_descricao_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_Observacao_N") )
               {
                  gxTv_SdtSolicitacaoServico_Contagemresultado_observacao_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_DataEntrega_N") )
               {
                  gxTv_SdtSolicitacaoServico_Contagemresultado_dataentrega_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_HoraEntrega_N") )
               {
                  gxTv_SdtSolicitacaoServico_Contagemresultado_horaentrega_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_DataPrevista_N") )
               {
                  gxTv_SdtSolicitacaoServico_Contagemresultado_dataprevista_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_ServicoSS_N") )
               {
                  gxTv_SdtSolicitacaoServico_Contagemresultado_servicoss_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_SS_N") )
               {
                  gxTv_SdtSolicitacaoServico_Contagemresultado_ss_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_DemandaFM_N") )
               {
                  gxTv_SdtSolicitacaoServico_Contagemresultado_demandafm_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_UOOwner_N") )
               {
                  gxTv_SdtSolicitacaoServico_Contagemresultado_uoowner_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_Evento_N") )
               {
                  gxTv_SdtSolicitacaoServico_Contagemresultado_evento_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_Referencia_N") )
               {
                  gxTv_SdtSolicitacaoServico_Contagemresultado_referencia_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_Restricoes_N") )
               {
                  gxTv_SdtSolicitacaoServico_Contagemresultado_restricoes_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_PrioridadePrevista_N") )
               {
                  gxTv_SdtSolicitacaoServico_Contagemresultado_prioridadeprevista_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_StatusDmn_N") )
               {
                  gxTv_SdtSolicitacaoServico_Contagemresultado_statusdmn_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_CntSrvCod_N") )
               {
                  gxTv_SdtSolicitacaoServico_Contagemresultado_cntsrvcod_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_OSVinculada_N") )
               {
                  gxTv_SdtSolicitacaoServico_Contagemresultado_osvinculada_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_NaoCnfDmnCod_N") )
               {
                  gxTv_SdtSolicitacaoServico_Contagemresultado_naocnfdmncod_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_LoteAceiteCod_N") )
               {
                  gxTv_SdtSolicitacaoServico_Contagemresultado_loteaceitecod_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_ContratadaOrigemCod_N") )
               {
                  gxTv_SdtSolicitacaoServico_Contagemresultado_contratadaorigemcod_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_ContadorFSCod_N") )
               {
                  gxTv_SdtSolicitacaoServico_Contagemresultado_contadorfscod_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_Responsavel_N") )
               {
                  gxTv_SdtSolicitacaoServico_Contagemresultado_responsavel_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_LiqLogCod_N") )
               {
                  gxTv_SdtSolicitacaoServico_Contagemresultado_liqlogcod_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Modulo_Codigo_N") )
               {
                  gxTv_SdtSolicitacaoServico_Modulo_codigo_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_ContratadaCod_N") )
               {
                  gxTv_SdtSolicitacaoServico_Contagemresultado_contratadacod_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_SistemaCod_N") )
               {
                  gxTv_SdtSolicitacaoServico_Contagemresultado_sistemacod_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_FncUsrCod_N") )
               {
                  gxTv_SdtSolicitacaoServico_Contagemresultado_fncusrcod_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_PrazoInicialDias_N") )
               {
                  gxTv_SdtSolicitacaoServico_Contagemresultado_prazoinicialdias_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_DataCadastro_N") )
               {
                  gxTv_SdtSolicitacaoServico_Contagemresultado_datacadastro_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ContagemResultado_OSManual_N") )
               {
                  gxTv_SdtSolicitacaoServico_Contagemresultado_osmanual_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contagemresultado_baseline_N") )
               {
                  gxTv_SdtSolicitacaoServico_Contagemresultado_baseline_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Contagemresultado_ehvalidacao_N") )
               {
                  gxTv_SdtSolicitacaoServico_Contagemresultado_ehvalidacao_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "SolicitacaoServico";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sNameSpace)) )
         {
            sNameSpace = "GxEv3Up14_MeetrikaVs3";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("ContagemResultado_Codigo", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSolicitacaoServico_Contagemresultado_codigo), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContagemResultado_Owner", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSolicitacaoServico_Contagemresultado_owner), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContagemResultado_Descricao", StringUtil.RTrim( gxTv_SdtSolicitacaoServico_Contagemresultado_descricao));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContagemResultado_Observacao", StringUtil.RTrim( gxTv_SdtSolicitacaoServico_Contagemresultado_observacao));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         if ( (DateTime.MinValue==gxTv_SdtSolicitacaoServico_Contagemresultado_dataentrega) )
         {
            oWriter.WriteStartElement("ContagemResultado_DataEntrega");
            oWriter.WriteAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
            oWriter.WriteAttribute("xsi:nil", "true");
            oWriter.WriteEndElement();
         }
         else
         {
            sDateCnv = "";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtSolicitacaoServico_Contagemresultado_dataentrega)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtSolicitacaoServico_Contagemresultado_dataentrega)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtSolicitacaoServico_Contagemresultado_dataentrega)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            oWriter.WriteElement("ContagemResultado_DataEntrega", sDateCnv);
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
         }
         if ( (DateTime.MinValue==gxTv_SdtSolicitacaoServico_Contagemresultado_horaentrega) )
         {
            oWriter.WriteStartElement("ContagemResultado_HoraEntrega");
            oWriter.WriteAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
            oWriter.WriteAttribute("xsi:nil", "true");
            oWriter.WriteEndElement();
         }
         else
         {
            sDateCnv = "";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtSolicitacaoServico_Contagemresultado_horaentrega)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtSolicitacaoServico_Contagemresultado_horaentrega)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtSolicitacaoServico_Contagemresultado_horaentrega)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "T";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Hour( gxTv_SdtSolicitacaoServico_Contagemresultado_horaentrega)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + ":";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Minute( gxTv_SdtSolicitacaoServico_Contagemresultado_horaentrega)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + ":";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Second( gxTv_SdtSolicitacaoServico_Contagemresultado_horaentrega)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            oWriter.WriteElement("ContagemResultado_HoraEntrega", sDateCnv);
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
         }
         if ( (DateTime.MinValue==gxTv_SdtSolicitacaoServico_Contagemresultado_dataprevista) )
         {
            oWriter.WriteStartElement("ContagemResultado_DataPrevista");
            oWriter.WriteAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
            oWriter.WriteAttribute("xsi:nil", "true");
            oWriter.WriteEndElement();
         }
         else
         {
            sDateCnv = "";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtSolicitacaoServico_Contagemresultado_dataprevista)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtSolicitacaoServico_Contagemresultado_dataprevista)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtSolicitacaoServico_Contagemresultado_dataprevista)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "T";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Hour( gxTv_SdtSolicitacaoServico_Contagemresultado_dataprevista)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + ":";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Minute( gxTv_SdtSolicitacaoServico_Contagemresultado_dataprevista)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + ":";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Second( gxTv_SdtSolicitacaoServico_Contagemresultado_dataprevista)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            oWriter.WriteElement("ContagemResultado_DataPrevista", sDateCnv);
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
         }
         oWriter.WriteElement("ContagemResultado_ServicoSS", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSolicitacaoServico_Contagemresultado_servicoss), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContagemResultado_SS", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSolicitacaoServico_Contagemresultado_ss), 8, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContagemResultado_DemandaFM", StringUtil.RTrim( gxTv_SdtSolicitacaoServico_Contagemresultado_demandafm));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContagemResultado_UOOwner", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSolicitacaoServico_Contagemresultado_uoowner), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContagemResultado_Evento", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSolicitacaoServico_Contagemresultado_evento), 2, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContagemResultado_TipoRegistro", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSolicitacaoServico_Contagemresultado_tiporegistro), 4, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContagemResultado_Referencia", StringUtil.RTrim( gxTv_SdtSolicitacaoServico_Contagemresultado_referencia));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContagemResultado_Restricoes", StringUtil.RTrim( gxTv_SdtSolicitacaoServico_Contagemresultado_restricoes));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContagemResultado_PrioridadePrevista", StringUtil.RTrim( gxTv_SdtSolicitacaoServico_Contagemresultado_prioridadeprevista));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContagemResultado_StatusDmn", StringUtil.RTrim( gxTv_SdtSolicitacaoServico_Contagemresultado_statusdmn));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         if ( (DateTime.MinValue==gxTv_SdtSolicitacaoServico_Contagemresultado_datadmn) )
         {
            oWriter.WriteStartElement("ContagemResultado_DataDmn");
            oWriter.WriteAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
            oWriter.WriteAttribute("xsi:nil", "true");
            oWriter.WriteEndElement();
         }
         else
         {
            sDateCnv = "";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtSolicitacaoServico_Contagemresultado_datadmn)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtSolicitacaoServico_Contagemresultado_datadmn)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtSolicitacaoServico_Contagemresultado_datadmn)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            oWriter.WriteElement("ContagemResultado_DataDmn", sDateCnv);
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
         }
         oWriter.WriteElement("ContagemResultado_CntSrvCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSolicitacaoServico_Contagemresultado_cntsrvcod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContagemResultado_OSVinculada", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSolicitacaoServico_Contagemresultado_osvinculada), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContagemResultado_NaoCnfDmnCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSolicitacaoServico_Contagemresultado_naocnfdmncod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContagemResultado_LoteAceiteCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSolicitacaoServico_Contagemresultado_loteaceitecod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContagemResultado_ContratadaOrigemCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSolicitacaoServico_Contagemresultado_contratadaorigemcod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContagemResultado_ContadorFSCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSolicitacaoServico_Contagemresultado_contadorfscod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContagemResultado_Responsavel", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSolicitacaoServico_Contagemresultado_responsavel), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContagemResultado_LiqLogCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSolicitacaoServico_Contagemresultado_liqlogcod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Modulo_Codigo", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSolicitacaoServico_Modulo_codigo), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContagemResultado_ContratadaCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSolicitacaoServico_Contagemresultado_contratadacod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContagemResultado_SistemaCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSolicitacaoServico_Contagemresultado_sistemacod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContagemResultado_FncUsrCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSolicitacaoServico_Contagemresultado_fncusrcod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ContagemResultado_PrazoInicialDias", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSolicitacaoServico_Contagemresultado_prazoinicialdias), 4, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         if ( (DateTime.MinValue==gxTv_SdtSolicitacaoServico_Contagemresultado_datacadastro) )
         {
            oWriter.WriteStartElement("ContagemResultado_DataCadastro");
            oWriter.WriteAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
            oWriter.WriteAttribute("xsi:nil", "true");
            oWriter.WriteEndElement();
         }
         else
         {
            sDateCnv = "";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtSolicitacaoServico_Contagemresultado_datacadastro)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtSolicitacaoServico_Contagemresultado_datacadastro)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtSolicitacaoServico_Contagemresultado_datacadastro)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "T";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Hour( gxTv_SdtSolicitacaoServico_Contagemresultado_datacadastro)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + ":";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Minute( gxTv_SdtSolicitacaoServico_Contagemresultado_datacadastro)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + ":";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Second( gxTv_SdtSolicitacaoServico_Contagemresultado_datacadastro)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            oWriter.WriteElement("ContagemResultado_DataCadastro", sDateCnv);
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
         }
         oWriter.WriteElement("ContagemResultado_OSManual", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtSolicitacaoServico_Contagemresultado_osmanual)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Contagemresultado_baseline", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtSolicitacaoServico_Contagemresultado_baseline)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("Contagemresultado_ehvalidacao", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtSolicitacaoServico_Contagemresultado_ehvalidacao)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         if ( sIncludeState )
         {
            oWriter.WriteElement("Mode", StringUtil.RTrim( gxTv_SdtSolicitacaoServico_Mode));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Initialized", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSolicitacaoServico_Initialized), 4, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemResultado_Codigo_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSolicitacaoServico_Contagemresultado_codigo_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemResultado_Owner_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSolicitacaoServico_Contagemresultado_owner_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemResultado_Descricao_Z", StringUtil.RTrim( gxTv_SdtSolicitacaoServico_Contagemresultado_descricao_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            if ( (DateTime.MinValue==gxTv_SdtSolicitacaoServico_Contagemresultado_dataentrega_Z) )
            {
               oWriter.WriteStartElement("ContagemResultado_DataEntrega_Z");
               oWriter.WriteAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
               oWriter.WriteAttribute("xsi:nil", "true");
               oWriter.WriteEndElement();
            }
            else
            {
               sDateCnv = "";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtSolicitacaoServico_Contagemresultado_dataentrega_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + "-";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtSolicitacaoServico_Contagemresultado_dataentrega_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + "-";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtSolicitacaoServico_Contagemresultado_dataentrega_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               oWriter.WriteElement("ContagemResultado_DataEntrega_Z", sDateCnv);
               if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
               {
                  oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
               }
            }
            if ( (DateTime.MinValue==gxTv_SdtSolicitacaoServico_Contagemresultado_horaentrega_Z) )
            {
               oWriter.WriteStartElement("ContagemResultado_HoraEntrega_Z");
               oWriter.WriteAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
               oWriter.WriteAttribute("xsi:nil", "true");
               oWriter.WriteEndElement();
            }
            else
            {
               sDateCnv = "";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtSolicitacaoServico_Contagemresultado_horaentrega_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + "-";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtSolicitacaoServico_Contagemresultado_horaentrega_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + "-";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtSolicitacaoServico_Contagemresultado_horaentrega_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + "T";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Hour( gxTv_SdtSolicitacaoServico_Contagemresultado_horaentrega_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + ":";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Minute( gxTv_SdtSolicitacaoServico_Contagemresultado_horaentrega_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + ":";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Second( gxTv_SdtSolicitacaoServico_Contagemresultado_horaentrega_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               oWriter.WriteElement("ContagemResultado_HoraEntrega_Z", sDateCnv);
               if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
               {
                  oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
               }
            }
            if ( (DateTime.MinValue==gxTv_SdtSolicitacaoServico_Contagemresultado_dataprevista_Z) )
            {
               oWriter.WriteStartElement("ContagemResultado_DataPrevista_Z");
               oWriter.WriteAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
               oWriter.WriteAttribute("xsi:nil", "true");
               oWriter.WriteEndElement();
            }
            else
            {
               sDateCnv = "";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtSolicitacaoServico_Contagemresultado_dataprevista_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + "-";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtSolicitacaoServico_Contagemresultado_dataprevista_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + "-";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtSolicitacaoServico_Contagemresultado_dataprevista_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + "T";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Hour( gxTv_SdtSolicitacaoServico_Contagemresultado_dataprevista_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + ":";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Minute( gxTv_SdtSolicitacaoServico_Contagemresultado_dataprevista_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + ":";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Second( gxTv_SdtSolicitacaoServico_Contagemresultado_dataprevista_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               oWriter.WriteElement("ContagemResultado_DataPrevista_Z", sDateCnv);
               if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
               {
                  oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
               }
            }
            oWriter.WriteElement("ContagemResultado_ServicoSS_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSolicitacaoServico_Contagemresultado_servicoss_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemResultado_SS_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSolicitacaoServico_Contagemresultado_ss_Z), 8, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemResultado_DemandaFM_Z", StringUtil.RTrim( gxTv_SdtSolicitacaoServico_Contagemresultado_demandafm_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemResultado_UOOwner_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSolicitacaoServico_Contagemresultado_uoowner_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemResultado_Evento_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSolicitacaoServico_Contagemresultado_evento_Z), 2, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemResultado_TipoRegistro_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSolicitacaoServico_Contagemresultado_tiporegistro_Z), 4, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemResultado_Referencia_Z", StringUtil.RTrim( gxTv_SdtSolicitacaoServico_Contagemresultado_referencia_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemResultado_Restricoes_Z", StringUtil.RTrim( gxTv_SdtSolicitacaoServico_Contagemresultado_restricoes_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemResultado_PrioridadePrevista_Z", StringUtil.RTrim( gxTv_SdtSolicitacaoServico_Contagemresultado_prioridadeprevista_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemResultado_StatusDmn_Z", StringUtil.RTrim( gxTv_SdtSolicitacaoServico_Contagemresultado_statusdmn_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            if ( (DateTime.MinValue==gxTv_SdtSolicitacaoServico_Contagemresultado_datadmn_Z) )
            {
               oWriter.WriteStartElement("ContagemResultado_DataDmn_Z");
               oWriter.WriteAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
               oWriter.WriteAttribute("xsi:nil", "true");
               oWriter.WriteEndElement();
            }
            else
            {
               sDateCnv = "";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtSolicitacaoServico_Contagemresultado_datadmn_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + "-";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtSolicitacaoServico_Contagemresultado_datadmn_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + "-";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtSolicitacaoServico_Contagemresultado_datadmn_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               oWriter.WriteElement("ContagemResultado_DataDmn_Z", sDateCnv);
               if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
               {
                  oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
               }
            }
            oWriter.WriteElement("ContagemResultado_CntSrvCod_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSolicitacaoServico_Contagemresultado_cntsrvcod_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemResultado_OSVinculada_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSolicitacaoServico_Contagemresultado_osvinculada_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemResultado_NaoCnfDmnCod_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSolicitacaoServico_Contagemresultado_naocnfdmncod_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemResultado_LoteAceiteCod_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSolicitacaoServico_Contagemresultado_loteaceitecod_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemResultado_ContratadaOrigemCod_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSolicitacaoServico_Contagemresultado_contratadaorigemcod_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemResultado_ContadorFSCod_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSolicitacaoServico_Contagemresultado_contadorfscod_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemResultado_Responsavel_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSolicitacaoServico_Contagemresultado_responsavel_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemResultado_LiqLogCod_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSolicitacaoServico_Contagemresultado_liqlogcod_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Modulo_Codigo_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSolicitacaoServico_Modulo_codigo_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemResultado_ContratadaCod_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSolicitacaoServico_Contagemresultado_contratadacod_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemResultado_SistemaCod_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSolicitacaoServico_Contagemresultado_sistemacod_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemResultado_FncUsrCod_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSolicitacaoServico_Contagemresultado_fncusrcod_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemResultado_PrazoInicialDias_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSolicitacaoServico_Contagemresultado_prazoinicialdias_Z), 4, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            if ( (DateTime.MinValue==gxTv_SdtSolicitacaoServico_Contagemresultado_datacadastro_Z) )
            {
               oWriter.WriteStartElement("ContagemResultado_DataCadastro_Z");
               oWriter.WriteAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
               oWriter.WriteAttribute("xsi:nil", "true");
               oWriter.WriteEndElement();
            }
            else
            {
               sDateCnv = "";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtSolicitacaoServico_Contagemresultado_datacadastro_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + "-";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtSolicitacaoServico_Contagemresultado_datacadastro_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + "-";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtSolicitacaoServico_Contagemresultado_datacadastro_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + "T";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Hour( gxTv_SdtSolicitacaoServico_Contagemresultado_datacadastro_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + ":";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Minute( gxTv_SdtSolicitacaoServico_Contagemresultado_datacadastro_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + ":";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Second( gxTv_SdtSolicitacaoServico_Contagemresultado_datacadastro_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               oWriter.WriteElement("ContagemResultado_DataCadastro_Z", sDateCnv);
               if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
               {
                  oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
               }
            }
            oWriter.WriteElement("ContagemResultado_OSManual_Z", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtSolicitacaoServico_Contagemresultado_osmanual_Z)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Contagemresultado_baseline_Z", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtSolicitacaoServico_Contagemresultado_baseline_Z)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Contagemresultado_ehvalidacao_Z", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtSolicitacaoServico_Contagemresultado_ehvalidacao_Z)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemResultado_Descricao_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSolicitacaoServico_Contagemresultado_descricao_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemResultado_Observacao_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSolicitacaoServico_Contagemresultado_observacao_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemResultado_DataEntrega_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSolicitacaoServico_Contagemresultado_dataentrega_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemResultado_HoraEntrega_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSolicitacaoServico_Contagemresultado_horaentrega_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemResultado_DataPrevista_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSolicitacaoServico_Contagemresultado_dataprevista_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemResultado_ServicoSS_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSolicitacaoServico_Contagemresultado_servicoss_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemResultado_SS_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSolicitacaoServico_Contagemresultado_ss_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemResultado_DemandaFM_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSolicitacaoServico_Contagemresultado_demandafm_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemResultado_UOOwner_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSolicitacaoServico_Contagemresultado_uoowner_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemResultado_Evento_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSolicitacaoServico_Contagemresultado_evento_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemResultado_Referencia_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSolicitacaoServico_Contagemresultado_referencia_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemResultado_Restricoes_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSolicitacaoServico_Contagemresultado_restricoes_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemResultado_PrioridadePrevista_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSolicitacaoServico_Contagemresultado_prioridadeprevista_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemResultado_StatusDmn_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSolicitacaoServico_Contagemresultado_statusdmn_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemResultado_CntSrvCod_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSolicitacaoServico_Contagemresultado_cntsrvcod_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemResultado_OSVinculada_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSolicitacaoServico_Contagemresultado_osvinculada_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemResultado_NaoCnfDmnCod_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSolicitacaoServico_Contagemresultado_naocnfdmncod_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemResultado_LoteAceiteCod_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSolicitacaoServico_Contagemresultado_loteaceitecod_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemResultado_ContratadaOrigemCod_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSolicitacaoServico_Contagemresultado_contratadaorigemcod_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemResultado_ContadorFSCod_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSolicitacaoServico_Contagemresultado_contadorfscod_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemResultado_Responsavel_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSolicitacaoServico_Contagemresultado_responsavel_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemResultado_LiqLogCod_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSolicitacaoServico_Contagemresultado_liqlogcod_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Modulo_Codigo_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSolicitacaoServico_Modulo_codigo_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemResultado_ContratadaCod_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSolicitacaoServico_Contagemresultado_contratadacod_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemResultado_SistemaCod_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSolicitacaoServico_Contagemresultado_sistemacod_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemResultado_FncUsrCod_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSolicitacaoServico_Contagemresultado_fncusrcod_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemResultado_PrazoInicialDias_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSolicitacaoServico_Contagemresultado_prazoinicialdias_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemResultado_DataCadastro_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSolicitacaoServico_Contagemresultado_datacadastro_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ContagemResultado_OSManual_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSolicitacaoServico_Contagemresultado_osmanual_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Contagemresultado_baseline_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSolicitacaoServico_Contagemresultado_baseline_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Contagemresultado_ehvalidacao_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtSolicitacaoServico_Contagemresultado_ehvalidacao_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("ContagemResultado_Codigo", gxTv_SdtSolicitacaoServico_Contagemresultado_codigo, false);
         AddObjectProperty("ContagemResultado_Owner", gxTv_SdtSolicitacaoServico_Contagemresultado_owner, false);
         AddObjectProperty("ContagemResultado_Descricao", gxTv_SdtSolicitacaoServico_Contagemresultado_descricao, false);
         AddObjectProperty("ContagemResultado_Observacao", gxTv_SdtSolicitacaoServico_Contagemresultado_observacao, false);
         sDateCnv = "";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtSolicitacaoServico_Contagemresultado_dataentrega)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtSolicitacaoServico_Contagemresultado_dataentrega)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtSolicitacaoServico_Contagemresultado_dataentrega)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         AddObjectProperty("ContagemResultado_DataEntrega", sDateCnv, false);
         datetime_STZ = gxTv_SdtSolicitacaoServico_Contagemresultado_horaentrega;
         sDateCnv = "";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "T";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Hour( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + ":";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Minute( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + ":";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Second( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         AddObjectProperty("ContagemResultado_HoraEntrega", sDateCnv, false);
         datetime_STZ = DateTimeUtil.ResetDate(gxTv_SdtSolicitacaoServico_Contagemresultado_dataprevista);
         sDateCnv = "";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "T";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Hour( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + ":";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Minute( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + ":";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Second( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         AddObjectProperty("ContagemResultado_DataPrevista", sDateCnv, false);
         AddObjectProperty("ContagemResultado_ServicoSS", gxTv_SdtSolicitacaoServico_Contagemresultado_servicoss, false);
         AddObjectProperty("ContagemResultado_SS", gxTv_SdtSolicitacaoServico_Contagemresultado_ss, false);
         AddObjectProperty("ContagemResultado_DemandaFM", gxTv_SdtSolicitacaoServico_Contagemresultado_demandafm, false);
         AddObjectProperty("ContagemResultado_UOOwner", gxTv_SdtSolicitacaoServico_Contagemresultado_uoowner, false);
         AddObjectProperty("ContagemResultado_Evento", gxTv_SdtSolicitacaoServico_Contagemresultado_evento, false);
         AddObjectProperty("ContagemResultado_TipoRegistro", gxTv_SdtSolicitacaoServico_Contagemresultado_tiporegistro, false);
         AddObjectProperty("ContagemResultado_Referencia", gxTv_SdtSolicitacaoServico_Contagemresultado_referencia, false);
         AddObjectProperty("ContagemResultado_Restricoes", gxTv_SdtSolicitacaoServico_Contagemresultado_restricoes, false);
         AddObjectProperty("ContagemResultado_PrioridadePrevista", gxTv_SdtSolicitacaoServico_Contagemresultado_prioridadeprevista, false);
         AddObjectProperty("ContagemResultado_StatusDmn", gxTv_SdtSolicitacaoServico_Contagemresultado_statusdmn, false);
         sDateCnv = "";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtSolicitacaoServico_Contagemresultado_datadmn)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtSolicitacaoServico_Contagemresultado_datadmn)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtSolicitacaoServico_Contagemresultado_datadmn)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         AddObjectProperty("ContagemResultado_DataDmn", sDateCnv, false);
         AddObjectProperty("ContagemResultado_CntSrvCod", gxTv_SdtSolicitacaoServico_Contagemresultado_cntsrvcod, false);
         AddObjectProperty("ContagemResultado_OSVinculada", gxTv_SdtSolicitacaoServico_Contagemresultado_osvinculada, false);
         AddObjectProperty("ContagemResultado_NaoCnfDmnCod", gxTv_SdtSolicitacaoServico_Contagemresultado_naocnfdmncod, false);
         AddObjectProperty("ContagemResultado_LoteAceiteCod", gxTv_SdtSolicitacaoServico_Contagemresultado_loteaceitecod, false);
         AddObjectProperty("ContagemResultado_ContratadaOrigemCod", gxTv_SdtSolicitacaoServico_Contagemresultado_contratadaorigemcod, false);
         AddObjectProperty("ContagemResultado_ContadorFSCod", gxTv_SdtSolicitacaoServico_Contagemresultado_contadorfscod, false);
         AddObjectProperty("ContagemResultado_Responsavel", gxTv_SdtSolicitacaoServico_Contagemresultado_responsavel, false);
         AddObjectProperty("ContagemResultado_LiqLogCod", gxTv_SdtSolicitacaoServico_Contagemresultado_liqlogcod, false);
         AddObjectProperty("Modulo_Codigo", gxTv_SdtSolicitacaoServico_Modulo_codigo, false);
         AddObjectProperty("ContagemResultado_ContratadaCod", gxTv_SdtSolicitacaoServico_Contagemresultado_contratadacod, false);
         AddObjectProperty("ContagemResultado_SistemaCod", gxTv_SdtSolicitacaoServico_Contagemresultado_sistemacod, false);
         AddObjectProperty("ContagemResultado_FncUsrCod", gxTv_SdtSolicitacaoServico_Contagemresultado_fncusrcod, false);
         AddObjectProperty("ContagemResultado_PrazoInicialDias", gxTv_SdtSolicitacaoServico_Contagemresultado_prazoinicialdias, false);
         datetime_STZ = DateTimeUtil.ResetDate(gxTv_SdtSolicitacaoServico_Contagemresultado_datacadastro);
         sDateCnv = "";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "T";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Hour( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + ":";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Minute( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + ":";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Second( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         AddObjectProperty("ContagemResultado_DataCadastro", sDateCnv, false);
         AddObjectProperty("ContagemResultado_OSManual", gxTv_SdtSolicitacaoServico_Contagemresultado_osmanual, false);
         AddObjectProperty("Contagemresultado_baseline", gxTv_SdtSolicitacaoServico_Contagemresultado_baseline, false);
         AddObjectProperty("Contagemresultado_ehvalidacao", gxTv_SdtSolicitacaoServico_Contagemresultado_ehvalidacao, false);
         if ( includeState )
         {
            AddObjectProperty("Mode", gxTv_SdtSolicitacaoServico_Mode, false);
            AddObjectProperty("Initialized", gxTv_SdtSolicitacaoServico_Initialized, false);
            AddObjectProperty("ContagemResultado_Codigo_Z", gxTv_SdtSolicitacaoServico_Contagemresultado_codigo_Z, false);
            AddObjectProperty("ContagemResultado_Owner_Z", gxTv_SdtSolicitacaoServico_Contagemresultado_owner_Z, false);
            AddObjectProperty("ContagemResultado_Descricao_Z", gxTv_SdtSolicitacaoServico_Contagemresultado_descricao_Z, false);
            sDateCnv = "";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtSolicitacaoServico_Contagemresultado_dataentrega_Z)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtSolicitacaoServico_Contagemresultado_dataentrega_Z)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtSolicitacaoServico_Contagemresultado_dataentrega_Z)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            AddObjectProperty("ContagemResultado_DataEntrega_Z", sDateCnv, false);
            datetime_STZ = gxTv_SdtSolicitacaoServico_Contagemresultado_horaentrega_Z;
            sDateCnv = "";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( datetime_STZ)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( datetime_STZ)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( datetime_STZ)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "T";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Hour( datetime_STZ)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + ":";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Minute( datetime_STZ)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + ":";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Second( datetime_STZ)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            AddObjectProperty("ContagemResultado_HoraEntrega_Z", sDateCnv, false);
            datetime_STZ = DateTimeUtil.ResetDate(gxTv_SdtSolicitacaoServico_Contagemresultado_dataprevista_Z);
            sDateCnv = "";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( datetime_STZ)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( datetime_STZ)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( datetime_STZ)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "T";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Hour( datetime_STZ)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + ":";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Minute( datetime_STZ)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + ":";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Second( datetime_STZ)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            AddObjectProperty("ContagemResultado_DataPrevista_Z", sDateCnv, false);
            AddObjectProperty("ContagemResultado_ServicoSS_Z", gxTv_SdtSolicitacaoServico_Contagemresultado_servicoss_Z, false);
            AddObjectProperty("ContagemResultado_SS_Z", gxTv_SdtSolicitacaoServico_Contagemresultado_ss_Z, false);
            AddObjectProperty("ContagemResultado_DemandaFM_Z", gxTv_SdtSolicitacaoServico_Contagemresultado_demandafm_Z, false);
            AddObjectProperty("ContagemResultado_UOOwner_Z", gxTv_SdtSolicitacaoServico_Contagemresultado_uoowner_Z, false);
            AddObjectProperty("ContagemResultado_Evento_Z", gxTv_SdtSolicitacaoServico_Contagemresultado_evento_Z, false);
            AddObjectProperty("ContagemResultado_TipoRegistro_Z", gxTv_SdtSolicitacaoServico_Contagemresultado_tiporegistro_Z, false);
            AddObjectProperty("ContagemResultado_Referencia_Z", gxTv_SdtSolicitacaoServico_Contagemresultado_referencia_Z, false);
            AddObjectProperty("ContagemResultado_Restricoes_Z", gxTv_SdtSolicitacaoServico_Contagemresultado_restricoes_Z, false);
            AddObjectProperty("ContagemResultado_PrioridadePrevista_Z", gxTv_SdtSolicitacaoServico_Contagemresultado_prioridadeprevista_Z, false);
            AddObjectProperty("ContagemResultado_StatusDmn_Z", gxTv_SdtSolicitacaoServico_Contagemresultado_statusdmn_Z, false);
            sDateCnv = "";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtSolicitacaoServico_Contagemresultado_datadmn_Z)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtSolicitacaoServico_Contagemresultado_datadmn_Z)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtSolicitacaoServico_Contagemresultado_datadmn_Z)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            AddObjectProperty("ContagemResultado_DataDmn_Z", sDateCnv, false);
            AddObjectProperty("ContagemResultado_CntSrvCod_Z", gxTv_SdtSolicitacaoServico_Contagemresultado_cntsrvcod_Z, false);
            AddObjectProperty("ContagemResultado_OSVinculada_Z", gxTv_SdtSolicitacaoServico_Contagemresultado_osvinculada_Z, false);
            AddObjectProperty("ContagemResultado_NaoCnfDmnCod_Z", gxTv_SdtSolicitacaoServico_Contagemresultado_naocnfdmncod_Z, false);
            AddObjectProperty("ContagemResultado_LoteAceiteCod_Z", gxTv_SdtSolicitacaoServico_Contagemresultado_loteaceitecod_Z, false);
            AddObjectProperty("ContagemResultado_ContratadaOrigemCod_Z", gxTv_SdtSolicitacaoServico_Contagemresultado_contratadaorigemcod_Z, false);
            AddObjectProperty("ContagemResultado_ContadorFSCod_Z", gxTv_SdtSolicitacaoServico_Contagemresultado_contadorfscod_Z, false);
            AddObjectProperty("ContagemResultado_Responsavel_Z", gxTv_SdtSolicitacaoServico_Contagemresultado_responsavel_Z, false);
            AddObjectProperty("ContagemResultado_LiqLogCod_Z", gxTv_SdtSolicitacaoServico_Contagemresultado_liqlogcod_Z, false);
            AddObjectProperty("Modulo_Codigo_Z", gxTv_SdtSolicitacaoServico_Modulo_codigo_Z, false);
            AddObjectProperty("ContagemResultado_ContratadaCod_Z", gxTv_SdtSolicitacaoServico_Contagemresultado_contratadacod_Z, false);
            AddObjectProperty("ContagemResultado_SistemaCod_Z", gxTv_SdtSolicitacaoServico_Contagemresultado_sistemacod_Z, false);
            AddObjectProperty("ContagemResultado_FncUsrCod_Z", gxTv_SdtSolicitacaoServico_Contagemresultado_fncusrcod_Z, false);
            AddObjectProperty("ContagemResultado_PrazoInicialDias_Z", gxTv_SdtSolicitacaoServico_Contagemresultado_prazoinicialdias_Z, false);
            datetime_STZ = DateTimeUtil.ResetDate(gxTv_SdtSolicitacaoServico_Contagemresultado_datacadastro_Z);
            sDateCnv = "";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( datetime_STZ)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( datetime_STZ)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( datetime_STZ)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "T";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Hour( datetime_STZ)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + ":";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Minute( datetime_STZ)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + ":";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Second( datetime_STZ)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            AddObjectProperty("ContagemResultado_DataCadastro_Z", sDateCnv, false);
            AddObjectProperty("ContagemResultado_OSManual_Z", gxTv_SdtSolicitacaoServico_Contagemresultado_osmanual_Z, false);
            AddObjectProperty("Contagemresultado_baseline_Z", gxTv_SdtSolicitacaoServico_Contagemresultado_baseline_Z, false);
            AddObjectProperty("Contagemresultado_ehvalidacao_Z", gxTv_SdtSolicitacaoServico_Contagemresultado_ehvalidacao_Z, false);
            AddObjectProperty("ContagemResultado_Descricao_N", gxTv_SdtSolicitacaoServico_Contagemresultado_descricao_N, false);
            AddObjectProperty("ContagemResultado_Observacao_N", gxTv_SdtSolicitacaoServico_Contagemresultado_observacao_N, false);
            AddObjectProperty("ContagemResultado_DataEntrega_N", gxTv_SdtSolicitacaoServico_Contagemresultado_dataentrega_N, false);
            AddObjectProperty("ContagemResultado_HoraEntrega_N", gxTv_SdtSolicitacaoServico_Contagemresultado_horaentrega_N, false);
            AddObjectProperty("ContagemResultado_DataPrevista_N", gxTv_SdtSolicitacaoServico_Contagemresultado_dataprevista_N, false);
            AddObjectProperty("ContagemResultado_ServicoSS_N", gxTv_SdtSolicitacaoServico_Contagemresultado_servicoss_N, false);
            AddObjectProperty("ContagemResultado_SS_N", gxTv_SdtSolicitacaoServico_Contagemresultado_ss_N, false);
            AddObjectProperty("ContagemResultado_DemandaFM_N", gxTv_SdtSolicitacaoServico_Contagemresultado_demandafm_N, false);
            AddObjectProperty("ContagemResultado_UOOwner_N", gxTv_SdtSolicitacaoServico_Contagemresultado_uoowner_N, false);
            AddObjectProperty("ContagemResultado_Evento_N", gxTv_SdtSolicitacaoServico_Contagemresultado_evento_N, false);
            AddObjectProperty("ContagemResultado_Referencia_N", gxTv_SdtSolicitacaoServico_Contagemresultado_referencia_N, false);
            AddObjectProperty("ContagemResultado_Restricoes_N", gxTv_SdtSolicitacaoServico_Contagemresultado_restricoes_N, false);
            AddObjectProperty("ContagemResultado_PrioridadePrevista_N", gxTv_SdtSolicitacaoServico_Contagemresultado_prioridadeprevista_N, false);
            AddObjectProperty("ContagemResultado_StatusDmn_N", gxTv_SdtSolicitacaoServico_Contagemresultado_statusdmn_N, false);
            AddObjectProperty("ContagemResultado_CntSrvCod_N", gxTv_SdtSolicitacaoServico_Contagemresultado_cntsrvcod_N, false);
            AddObjectProperty("ContagemResultado_OSVinculada_N", gxTv_SdtSolicitacaoServico_Contagemresultado_osvinculada_N, false);
            AddObjectProperty("ContagemResultado_NaoCnfDmnCod_N", gxTv_SdtSolicitacaoServico_Contagemresultado_naocnfdmncod_N, false);
            AddObjectProperty("ContagemResultado_LoteAceiteCod_N", gxTv_SdtSolicitacaoServico_Contagemresultado_loteaceitecod_N, false);
            AddObjectProperty("ContagemResultado_ContratadaOrigemCod_N", gxTv_SdtSolicitacaoServico_Contagemresultado_contratadaorigemcod_N, false);
            AddObjectProperty("ContagemResultado_ContadorFSCod_N", gxTv_SdtSolicitacaoServico_Contagemresultado_contadorfscod_N, false);
            AddObjectProperty("ContagemResultado_Responsavel_N", gxTv_SdtSolicitacaoServico_Contagemresultado_responsavel_N, false);
            AddObjectProperty("ContagemResultado_LiqLogCod_N", gxTv_SdtSolicitacaoServico_Contagemresultado_liqlogcod_N, false);
            AddObjectProperty("Modulo_Codigo_N", gxTv_SdtSolicitacaoServico_Modulo_codigo_N, false);
            AddObjectProperty("ContagemResultado_ContratadaCod_N", gxTv_SdtSolicitacaoServico_Contagemresultado_contratadacod_N, false);
            AddObjectProperty("ContagemResultado_SistemaCod_N", gxTv_SdtSolicitacaoServico_Contagemresultado_sistemacod_N, false);
            AddObjectProperty("ContagemResultado_FncUsrCod_N", gxTv_SdtSolicitacaoServico_Contagemresultado_fncusrcod_N, false);
            AddObjectProperty("ContagemResultado_PrazoInicialDias_N", gxTv_SdtSolicitacaoServico_Contagemresultado_prazoinicialdias_N, false);
            AddObjectProperty("ContagemResultado_DataCadastro_N", gxTv_SdtSolicitacaoServico_Contagemresultado_datacadastro_N, false);
            AddObjectProperty("ContagemResultado_OSManual_N", gxTv_SdtSolicitacaoServico_Contagemresultado_osmanual_N, false);
            AddObjectProperty("Contagemresultado_baseline_N", gxTv_SdtSolicitacaoServico_Contagemresultado_baseline_N, false);
            AddObjectProperty("Contagemresultado_ehvalidacao_N", gxTv_SdtSolicitacaoServico_Contagemresultado_ehvalidacao_N, false);
         }
         return  ;
      }

      [  SoapElement( ElementName = "ContagemResultado_Codigo" )]
      [  XmlElement( ElementName = "ContagemResultado_Codigo"   )]
      public int gxTpr_Contagemresultado_codigo
      {
         get {
            return gxTv_SdtSolicitacaoServico_Contagemresultado_codigo ;
         }

         set {
            if ( gxTv_SdtSolicitacaoServico_Contagemresultado_codigo != value )
            {
               gxTv_SdtSolicitacaoServico_Mode = "INS";
               this.gxTv_SdtSolicitacaoServico_Contagemresultado_codigo_Z_SetNull( );
               this.gxTv_SdtSolicitacaoServico_Contagemresultado_owner_Z_SetNull( );
               this.gxTv_SdtSolicitacaoServico_Contagemresultado_descricao_Z_SetNull( );
               this.gxTv_SdtSolicitacaoServico_Contagemresultado_dataentrega_Z_SetNull( );
               this.gxTv_SdtSolicitacaoServico_Contagemresultado_horaentrega_Z_SetNull( );
               this.gxTv_SdtSolicitacaoServico_Contagemresultado_dataprevista_Z_SetNull( );
               this.gxTv_SdtSolicitacaoServico_Contagemresultado_servicoss_Z_SetNull( );
               this.gxTv_SdtSolicitacaoServico_Contagemresultado_ss_Z_SetNull( );
               this.gxTv_SdtSolicitacaoServico_Contagemresultado_demandafm_Z_SetNull( );
               this.gxTv_SdtSolicitacaoServico_Contagemresultado_uoowner_Z_SetNull( );
               this.gxTv_SdtSolicitacaoServico_Contagemresultado_evento_Z_SetNull( );
               this.gxTv_SdtSolicitacaoServico_Contagemresultado_tiporegistro_Z_SetNull( );
               this.gxTv_SdtSolicitacaoServico_Contagemresultado_referencia_Z_SetNull( );
               this.gxTv_SdtSolicitacaoServico_Contagemresultado_restricoes_Z_SetNull( );
               this.gxTv_SdtSolicitacaoServico_Contagemresultado_prioridadeprevista_Z_SetNull( );
               this.gxTv_SdtSolicitacaoServico_Contagemresultado_statusdmn_Z_SetNull( );
               this.gxTv_SdtSolicitacaoServico_Contagemresultado_datadmn_Z_SetNull( );
               this.gxTv_SdtSolicitacaoServico_Contagemresultado_cntsrvcod_Z_SetNull( );
               this.gxTv_SdtSolicitacaoServico_Contagemresultado_osvinculada_Z_SetNull( );
               this.gxTv_SdtSolicitacaoServico_Contagemresultado_naocnfdmncod_Z_SetNull( );
               this.gxTv_SdtSolicitacaoServico_Contagemresultado_loteaceitecod_Z_SetNull( );
               this.gxTv_SdtSolicitacaoServico_Contagemresultado_contratadaorigemcod_Z_SetNull( );
               this.gxTv_SdtSolicitacaoServico_Contagemresultado_contadorfscod_Z_SetNull( );
               this.gxTv_SdtSolicitacaoServico_Contagemresultado_responsavel_Z_SetNull( );
               this.gxTv_SdtSolicitacaoServico_Contagemresultado_liqlogcod_Z_SetNull( );
               this.gxTv_SdtSolicitacaoServico_Modulo_codigo_Z_SetNull( );
               this.gxTv_SdtSolicitacaoServico_Contagemresultado_contratadacod_Z_SetNull( );
               this.gxTv_SdtSolicitacaoServico_Contagemresultado_sistemacod_Z_SetNull( );
               this.gxTv_SdtSolicitacaoServico_Contagemresultado_fncusrcod_Z_SetNull( );
               this.gxTv_SdtSolicitacaoServico_Contagemresultado_prazoinicialdias_Z_SetNull( );
               this.gxTv_SdtSolicitacaoServico_Contagemresultado_datacadastro_Z_SetNull( );
               this.gxTv_SdtSolicitacaoServico_Contagemresultado_osmanual_Z_SetNull( );
               this.gxTv_SdtSolicitacaoServico_Contagemresultado_baseline_Z_SetNull( );
               this.gxTv_SdtSolicitacaoServico_Contagemresultado_ehvalidacao_Z_SetNull( );
            }
            gxTv_SdtSolicitacaoServico_Contagemresultado_codigo = (int)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_Owner" )]
      [  XmlElement( ElementName = "ContagemResultado_Owner"   )]
      public int gxTpr_Contagemresultado_owner
      {
         get {
            return gxTv_SdtSolicitacaoServico_Contagemresultado_owner ;
         }

         set {
            gxTv_SdtSolicitacaoServico_Contagemresultado_owner = (int)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_Descricao" )]
      [  XmlElement( ElementName = "ContagemResultado_Descricao"   )]
      public String gxTpr_Contagemresultado_descricao
      {
         get {
            return gxTv_SdtSolicitacaoServico_Contagemresultado_descricao ;
         }

         set {
            gxTv_SdtSolicitacaoServico_Contagemresultado_descricao_N = 0;
            gxTv_SdtSolicitacaoServico_Contagemresultado_descricao = (String)(value);
         }

      }

      public void gxTv_SdtSolicitacaoServico_Contagemresultado_descricao_SetNull( )
      {
         gxTv_SdtSolicitacaoServico_Contagemresultado_descricao_N = 1;
         gxTv_SdtSolicitacaoServico_Contagemresultado_descricao = "";
         return  ;
      }

      public bool gxTv_SdtSolicitacaoServico_Contagemresultado_descricao_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_Observacao" )]
      [  XmlElement( ElementName = "ContagemResultado_Observacao"   )]
      public String gxTpr_Contagemresultado_observacao
      {
         get {
            return gxTv_SdtSolicitacaoServico_Contagemresultado_observacao ;
         }

         set {
            gxTv_SdtSolicitacaoServico_Contagemresultado_observacao_N = 0;
            gxTv_SdtSolicitacaoServico_Contagemresultado_observacao = (String)(value);
         }

      }

      public void gxTv_SdtSolicitacaoServico_Contagemresultado_observacao_SetNull( )
      {
         gxTv_SdtSolicitacaoServico_Contagemresultado_observacao_N = 1;
         gxTv_SdtSolicitacaoServico_Contagemresultado_observacao = "";
         return  ;
      }

      public bool gxTv_SdtSolicitacaoServico_Contagemresultado_observacao_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_DataEntrega" )]
      [  XmlElement( ElementName = "ContagemResultado_DataEntrega"  , IsNullable=true )]
      public string gxTpr_Contagemresultado_dataentrega_Nullable
      {
         get {
            if ( gxTv_SdtSolicitacaoServico_Contagemresultado_dataentrega == DateTime.MinValue)
               return null;
            return new GxDateString(gxTv_SdtSolicitacaoServico_Contagemresultado_dataentrega).value ;
         }

         set {
            gxTv_SdtSolicitacaoServico_Contagemresultado_dataentrega_N = 0;
            if (value == null || value == GxDateString.NullValue )
               gxTv_SdtSolicitacaoServico_Contagemresultado_dataentrega = DateTime.MinValue;
            else
               gxTv_SdtSolicitacaoServico_Contagemresultado_dataentrega = DateTime.Parse( value);
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public DateTime gxTpr_Contagemresultado_dataentrega
      {
         get {
            return gxTv_SdtSolicitacaoServico_Contagemresultado_dataentrega ;
         }

         set {
            gxTv_SdtSolicitacaoServico_Contagemresultado_dataentrega_N = 0;
            gxTv_SdtSolicitacaoServico_Contagemresultado_dataentrega = (DateTime)(value);
         }

      }

      public void gxTv_SdtSolicitacaoServico_Contagemresultado_dataentrega_SetNull( )
      {
         gxTv_SdtSolicitacaoServico_Contagemresultado_dataentrega_N = 1;
         gxTv_SdtSolicitacaoServico_Contagemresultado_dataentrega = (DateTime)(DateTime.MinValue);
         return  ;
      }

      public bool gxTv_SdtSolicitacaoServico_Contagemresultado_dataentrega_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_HoraEntrega" )]
      [  XmlElement( ElementName = "ContagemResultado_HoraEntrega"  , IsNullable=true )]
      public string gxTpr_Contagemresultado_horaentrega_Nullable
      {
         get {
            if ( gxTv_SdtSolicitacaoServico_Contagemresultado_horaentrega == DateTime.MinValue)
               return null;
            return new GxDatetimeString(gxTv_SdtSolicitacaoServico_Contagemresultado_horaentrega).value ;
         }

         set {
            gxTv_SdtSolicitacaoServico_Contagemresultado_horaentrega_N = 0;
            if (value == null || value == GxDatetimeString.NullValue )
               gxTv_SdtSolicitacaoServico_Contagemresultado_horaentrega = DateTime.MinValue;
            else
               gxTv_SdtSolicitacaoServico_Contagemresultado_horaentrega = DateTime.Parse( value);
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public DateTime gxTpr_Contagemresultado_horaentrega
      {
         get {
            return gxTv_SdtSolicitacaoServico_Contagemresultado_horaentrega ;
         }

         set {
            gxTv_SdtSolicitacaoServico_Contagemresultado_horaentrega_N = 0;
            gxTv_SdtSolicitacaoServico_Contagemresultado_horaentrega = (DateTime)(value);
         }

      }

      public void gxTv_SdtSolicitacaoServico_Contagemresultado_horaentrega_SetNull( )
      {
         gxTv_SdtSolicitacaoServico_Contagemresultado_horaentrega_N = 1;
         gxTv_SdtSolicitacaoServico_Contagemresultado_horaentrega = (DateTime)(DateTime.MinValue);
         return  ;
      }

      public bool gxTv_SdtSolicitacaoServico_Contagemresultado_horaentrega_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_DataPrevista" )]
      [  XmlElement( ElementName = "ContagemResultado_DataPrevista"  , IsNullable=true )]
      public string gxTpr_Contagemresultado_dataprevista_Nullable
      {
         get {
            if ( gxTv_SdtSolicitacaoServico_Contagemresultado_dataprevista == DateTime.MinValue)
               return null;
            return new GxDatetimeString(gxTv_SdtSolicitacaoServico_Contagemresultado_dataprevista).value ;
         }

         set {
            gxTv_SdtSolicitacaoServico_Contagemresultado_dataprevista_N = 0;
            if (value == null || value == GxDatetimeString.NullValue )
               gxTv_SdtSolicitacaoServico_Contagemresultado_dataprevista = DateTime.MinValue;
            else
               gxTv_SdtSolicitacaoServico_Contagemresultado_dataprevista = DateTime.Parse( value);
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public DateTime gxTpr_Contagemresultado_dataprevista
      {
         get {
            return gxTv_SdtSolicitacaoServico_Contagemresultado_dataprevista ;
         }

         set {
            gxTv_SdtSolicitacaoServico_Contagemresultado_dataprevista_N = 0;
            gxTv_SdtSolicitacaoServico_Contagemresultado_dataprevista = (DateTime)(value);
         }

      }

      public void gxTv_SdtSolicitacaoServico_Contagemresultado_dataprevista_SetNull( )
      {
         gxTv_SdtSolicitacaoServico_Contagemresultado_dataprevista_N = 1;
         gxTv_SdtSolicitacaoServico_Contagemresultado_dataprevista = (DateTime)(DateTime.MinValue);
         return  ;
      }

      public bool gxTv_SdtSolicitacaoServico_Contagemresultado_dataprevista_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_ServicoSS" )]
      [  XmlElement( ElementName = "ContagemResultado_ServicoSS"   )]
      public int gxTpr_Contagemresultado_servicoss
      {
         get {
            return gxTv_SdtSolicitacaoServico_Contagemresultado_servicoss ;
         }

         set {
            gxTv_SdtSolicitacaoServico_Contagemresultado_servicoss_N = 0;
            gxTv_SdtSolicitacaoServico_Contagemresultado_servicoss = (int)(value);
         }

      }

      public void gxTv_SdtSolicitacaoServico_Contagemresultado_servicoss_SetNull( )
      {
         gxTv_SdtSolicitacaoServico_Contagemresultado_servicoss_N = 1;
         gxTv_SdtSolicitacaoServico_Contagemresultado_servicoss = 0;
         return  ;
      }

      public bool gxTv_SdtSolicitacaoServico_Contagemresultado_servicoss_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_SS" )]
      [  XmlElement( ElementName = "ContagemResultado_SS"   )]
      public int gxTpr_Contagemresultado_ss
      {
         get {
            return gxTv_SdtSolicitacaoServico_Contagemresultado_ss ;
         }

         set {
            gxTv_SdtSolicitacaoServico_Contagemresultado_ss_N = 0;
            gxTv_SdtSolicitacaoServico_Contagemresultado_ss = (int)(value);
         }

      }

      public void gxTv_SdtSolicitacaoServico_Contagemresultado_ss_SetNull( )
      {
         gxTv_SdtSolicitacaoServico_Contagemresultado_ss_N = 1;
         gxTv_SdtSolicitacaoServico_Contagemresultado_ss = 0;
         return  ;
      }

      public bool gxTv_SdtSolicitacaoServico_Contagemresultado_ss_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_DemandaFM" )]
      [  XmlElement( ElementName = "ContagemResultado_DemandaFM"   )]
      public String gxTpr_Contagemresultado_demandafm
      {
         get {
            return gxTv_SdtSolicitacaoServico_Contagemresultado_demandafm ;
         }

         set {
            gxTv_SdtSolicitacaoServico_Contagemresultado_demandafm_N = 0;
            gxTv_SdtSolicitacaoServico_Contagemresultado_demandafm = (String)(value);
         }

      }

      public void gxTv_SdtSolicitacaoServico_Contagemresultado_demandafm_SetNull( )
      {
         gxTv_SdtSolicitacaoServico_Contagemresultado_demandafm_N = 1;
         gxTv_SdtSolicitacaoServico_Contagemresultado_demandafm = "";
         return  ;
      }

      public bool gxTv_SdtSolicitacaoServico_Contagemresultado_demandafm_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_UOOwner" )]
      [  XmlElement( ElementName = "ContagemResultado_UOOwner"   )]
      public int gxTpr_Contagemresultado_uoowner
      {
         get {
            return gxTv_SdtSolicitacaoServico_Contagemresultado_uoowner ;
         }

         set {
            gxTv_SdtSolicitacaoServico_Contagemresultado_uoowner_N = 0;
            gxTv_SdtSolicitacaoServico_Contagemresultado_uoowner = (int)(value);
         }

      }

      public void gxTv_SdtSolicitacaoServico_Contagemresultado_uoowner_SetNull( )
      {
         gxTv_SdtSolicitacaoServico_Contagemresultado_uoowner_N = 1;
         gxTv_SdtSolicitacaoServico_Contagemresultado_uoowner = 0;
         return  ;
      }

      public bool gxTv_SdtSolicitacaoServico_Contagemresultado_uoowner_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_Evento" )]
      [  XmlElement( ElementName = "ContagemResultado_Evento"   )]
      public short gxTpr_Contagemresultado_evento
      {
         get {
            return gxTv_SdtSolicitacaoServico_Contagemresultado_evento ;
         }

         set {
            gxTv_SdtSolicitacaoServico_Contagemresultado_evento_N = 0;
            gxTv_SdtSolicitacaoServico_Contagemresultado_evento = (short)(value);
         }

      }

      public void gxTv_SdtSolicitacaoServico_Contagemresultado_evento_SetNull( )
      {
         gxTv_SdtSolicitacaoServico_Contagemresultado_evento_N = 1;
         gxTv_SdtSolicitacaoServico_Contagemresultado_evento = 0;
         return  ;
      }

      public bool gxTv_SdtSolicitacaoServico_Contagemresultado_evento_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_TipoRegistro" )]
      [  XmlElement( ElementName = "ContagemResultado_TipoRegistro"   )]
      public short gxTpr_Contagemresultado_tiporegistro
      {
         get {
            return gxTv_SdtSolicitacaoServico_Contagemresultado_tiporegistro ;
         }

         set {
            gxTv_SdtSolicitacaoServico_Contagemresultado_tiporegistro = (short)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_Referencia" )]
      [  XmlElement( ElementName = "ContagemResultado_Referencia"   )]
      public String gxTpr_Contagemresultado_referencia
      {
         get {
            return gxTv_SdtSolicitacaoServico_Contagemresultado_referencia ;
         }

         set {
            gxTv_SdtSolicitacaoServico_Contagemresultado_referencia_N = 0;
            gxTv_SdtSolicitacaoServico_Contagemresultado_referencia = (String)(value);
         }

      }

      public void gxTv_SdtSolicitacaoServico_Contagemresultado_referencia_SetNull( )
      {
         gxTv_SdtSolicitacaoServico_Contagemresultado_referencia_N = 1;
         gxTv_SdtSolicitacaoServico_Contagemresultado_referencia = "";
         return  ;
      }

      public bool gxTv_SdtSolicitacaoServico_Contagemresultado_referencia_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_Restricoes" )]
      [  XmlElement( ElementName = "ContagemResultado_Restricoes"   )]
      public String gxTpr_Contagemresultado_restricoes
      {
         get {
            return gxTv_SdtSolicitacaoServico_Contagemresultado_restricoes ;
         }

         set {
            gxTv_SdtSolicitacaoServico_Contagemresultado_restricoes_N = 0;
            gxTv_SdtSolicitacaoServico_Contagemresultado_restricoes = (String)(value);
         }

      }

      public void gxTv_SdtSolicitacaoServico_Contagemresultado_restricoes_SetNull( )
      {
         gxTv_SdtSolicitacaoServico_Contagemresultado_restricoes_N = 1;
         gxTv_SdtSolicitacaoServico_Contagemresultado_restricoes = "";
         return  ;
      }

      public bool gxTv_SdtSolicitacaoServico_Contagemresultado_restricoes_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_PrioridadePrevista" )]
      [  XmlElement( ElementName = "ContagemResultado_PrioridadePrevista"   )]
      public String gxTpr_Contagemresultado_prioridadeprevista
      {
         get {
            return gxTv_SdtSolicitacaoServico_Contagemresultado_prioridadeprevista ;
         }

         set {
            gxTv_SdtSolicitacaoServico_Contagemresultado_prioridadeprevista_N = 0;
            gxTv_SdtSolicitacaoServico_Contagemresultado_prioridadeprevista = (String)(value);
         }

      }

      public void gxTv_SdtSolicitacaoServico_Contagemresultado_prioridadeprevista_SetNull( )
      {
         gxTv_SdtSolicitacaoServico_Contagemresultado_prioridadeprevista_N = 1;
         gxTv_SdtSolicitacaoServico_Contagemresultado_prioridadeprevista = "";
         return  ;
      }

      public bool gxTv_SdtSolicitacaoServico_Contagemresultado_prioridadeprevista_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_StatusDmn" )]
      [  XmlElement( ElementName = "ContagemResultado_StatusDmn"   )]
      public String gxTpr_Contagemresultado_statusdmn
      {
         get {
            return gxTv_SdtSolicitacaoServico_Contagemresultado_statusdmn ;
         }

         set {
            gxTv_SdtSolicitacaoServico_Contagemresultado_statusdmn_N = 0;
            gxTv_SdtSolicitacaoServico_Contagemresultado_statusdmn = (String)(value);
         }

      }

      public void gxTv_SdtSolicitacaoServico_Contagemresultado_statusdmn_SetNull( )
      {
         gxTv_SdtSolicitacaoServico_Contagemresultado_statusdmn_N = 1;
         gxTv_SdtSolicitacaoServico_Contagemresultado_statusdmn = "";
         return  ;
      }

      public bool gxTv_SdtSolicitacaoServico_Contagemresultado_statusdmn_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_DataDmn" )]
      [  XmlElement( ElementName = "ContagemResultado_DataDmn"  , IsNullable=true )]
      public string gxTpr_Contagemresultado_datadmn_Nullable
      {
         get {
            if ( gxTv_SdtSolicitacaoServico_Contagemresultado_datadmn == DateTime.MinValue)
               return null;
            return new GxDateString(gxTv_SdtSolicitacaoServico_Contagemresultado_datadmn).value ;
         }

         set {
            if (value == null || value == GxDateString.NullValue )
               gxTv_SdtSolicitacaoServico_Contagemresultado_datadmn = DateTime.MinValue;
            else
               gxTv_SdtSolicitacaoServico_Contagemresultado_datadmn = DateTime.Parse( value);
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public DateTime gxTpr_Contagemresultado_datadmn
      {
         get {
            return gxTv_SdtSolicitacaoServico_Contagemresultado_datadmn ;
         }

         set {
            gxTv_SdtSolicitacaoServico_Contagemresultado_datadmn = (DateTime)(value);
         }

      }

      [  SoapElement( ElementName = "ContagemResultado_CntSrvCod" )]
      [  XmlElement( ElementName = "ContagemResultado_CntSrvCod"   )]
      public int gxTpr_Contagemresultado_cntsrvcod
      {
         get {
            return gxTv_SdtSolicitacaoServico_Contagemresultado_cntsrvcod ;
         }

         set {
            gxTv_SdtSolicitacaoServico_Contagemresultado_cntsrvcod_N = 0;
            gxTv_SdtSolicitacaoServico_Contagemresultado_cntsrvcod = (int)(value);
         }

      }

      public void gxTv_SdtSolicitacaoServico_Contagemresultado_cntsrvcod_SetNull( )
      {
         gxTv_SdtSolicitacaoServico_Contagemresultado_cntsrvcod_N = 1;
         gxTv_SdtSolicitacaoServico_Contagemresultado_cntsrvcod = 0;
         return  ;
      }

      public bool gxTv_SdtSolicitacaoServico_Contagemresultado_cntsrvcod_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_OSVinculada" )]
      [  XmlElement( ElementName = "ContagemResultado_OSVinculada"   )]
      public int gxTpr_Contagemresultado_osvinculada
      {
         get {
            return gxTv_SdtSolicitacaoServico_Contagemresultado_osvinculada ;
         }

         set {
            gxTv_SdtSolicitacaoServico_Contagemresultado_osvinculada_N = 0;
            gxTv_SdtSolicitacaoServico_Contagemresultado_osvinculada = (int)(value);
         }

      }

      public void gxTv_SdtSolicitacaoServico_Contagemresultado_osvinculada_SetNull( )
      {
         gxTv_SdtSolicitacaoServico_Contagemresultado_osvinculada_N = 1;
         gxTv_SdtSolicitacaoServico_Contagemresultado_osvinculada = 0;
         return  ;
      }

      public bool gxTv_SdtSolicitacaoServico_Contagemresultado_osvinculada_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_NaoCnfDmnCod" )]
      [  XmlElement( ElementName = "ContagemResultado_NaoCnfDmnCod"   )]
      public int gxTpr_Contagemresultado_naocnfdmncod
      {
         get {
            return gxTv_SdtSolicitacaoServico_Contagemresultado_naocnfdmncod ;
         }

         set {
            gxTv_SdtSolicitacaoServico_Contagemresultado_naocnfdmncod_N = 0;
            gxTv_SdtSolicitacaoServico_Contagemresultado_naocnfdmncod = (int)(value);
         }

      }

      public void gxTv_SdtSolicitacaoServico_Contagemresultado_naocnfdmncod_SetNull( )
      {
         gxTv_SdtSolicitacaoServico_Contagemresultado_naocnfdmncod_N = 1;
         gxTv_SdtSolicitacaoServico_Contagemresultado_naocnfdmncod = 0;
         return  ;
      }

      public bool gxTv_SdtSolicitacaoServico_Contagemresultado_naocnfdmncod_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_LoteAceiteCod" )]
      [  XmlElement( ElementName = "ContagemResultado_LoteAceiteCod"   )]
      public int gxTpr_Contagemresultado_loteaceitecod
      {
         get {
            return gxTv_SdtSolicitacaoServico_Contagemresultado_loteaceitecod ;
         }

         set {
            gxTv_SdtSolicitacaoServico_Contagemresultado_loteaceitecod_N = 0;
            gxTv_SdtSolicitacaoServico_Contagemresultado_loteaceitecod = (int)(value);
         }

      }

      public void gxTv_SdtSolicitacaoServico_Contagemresultado_loteaceitecod_SetNull( )
      {
         gxTv_SdtSolicitacaoServico_Contagemresultado_loteaceitecod_N = 1;
         gxTv_SdtSolicitacaoServico_Contagemresultado_loteaceitecod = 0;
         return  ;
      }

      public bool gxTv_SdtSolicitacaoServico_Contagemresultado_loteaceitecod_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_ContratadaOrigemCod" )]
      [  XmlElement( ElementName = "ContagemResultado_ContratadaOrigemCod"   )]
      public int gxTpr_Contagemresultado_contratadaorigemcod
      {
         get {
            return gxTv_SdtSolicitacaoServico_Contagemresultado_contratadaorigemcod ;
         }

         set {
            gxTv_SdtSolicitacaoServico_Contagemresultado_contratadaorigemcod_N = 0;
            gxTv_SdtSolicitacaoServico_Contagemresultado_contratadaorigemcod = (int)(value);
         }

      }

      public void gxTv_SdtSolicitacaoServico_Contagemresultado_contratadaorigemcod_SetNull( )
      {
         gxTv_SdtSolicitacaoServico_Contagemresultado_contratadaorigemcod_N = 1;
         gxTv_SdtSolicitacaoServico_Contagemresultado_contratadaorigemcod = 0;
         return  ;
      }

      public bool gxTv_SdtSolicitacaoServico_Contagemresultado_contratadaorigemcod_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_ContadorFSCod" )]
      [  XmlElement( ElementName = "ContagemResultado_ContadorFSCod"   )]
      public int gxTpr_Contagemresultado_contadorfscod
      {
         get {
            return gxTv_SdtSolicitacaoServico_Contagemresultado_contadorfscod ;
         }

         set {
            gxTv_SdtSolicitacaoServico_Contagemresultado_contadorfscod_N = 0;
            gxTv_SdtSolicitacaoServico_Contagemresultado_contadorfscod = (int)(value);
         }

      }

      public void gxTv_SdtSolicitacaoServico_Contagemresultado_contadorfscod_SetNull( )
      {
         gxTv_SdtSolicitacaoServico_Contagemresultado_contadorfscod_N = 1;
         gxTv_SdtSolicitacaoServico_Contagemresultado_contadorfscod = 0;
         return  ;
      }

      public bool gxTv_SdtSolicitacaoServico_Contagemresultado_contadorfscod_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_Responsavel" )]
      [  XmlElement( ElementName = "ContagemResultado_Responsavel"   )]
      public int gxTpr_Contagemresultado_responsavel
      {
         get {
            return gxTv_SdtSolicitacaoServico_Contagemresultado_responsavel ;
         }

         set {
            gxTv_SdtSolicitacaoServico_Contagemresultado_responsavel_N = 0;
            gxTv_SdtSolicitacaoServico_Contagemresultado_responsavel = (int)(value);
         }

      }

      public void gxTv_SdtSolicitacaoServico_Contagemresultado_responsavel_SetNull( )
      {
         gxTv_SdtSolicitacaoServico_Contagemresultado_responsavel_N = 1;
         gxTv_SdtSolicitacaoServico_Contagemresultado_responsavel = 0;
         return  ;
      }

      public bool gxTv_SdtSolicitacaoServico_Contagemresultado_responsavel_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_LiqLogCod" )]
      [  XmlElement( ElementName = "ContagemResultado_LiqLogCod"   )]
      public int gxTpr_Contagemresultado_liqlogcod
      {
         get {
            return gxTv_SdtSolicitacaoServico_Contagemresultado_liqlogcod ;
         }

         set {
            gxTv_SdtSolicitacaoServico_Contagemresultado_liqlogcod_N = 0;
            gxTv_SdtSolicitacaoServico_Contagemresultado_liqlogcod = (int)(value);
         }

      }

      public void gxTv_SdtSolicitacaoServico_Contagemresultado_liqlogcod_SetNull( )
      {
         gxTv_SdtSolicitacaoServico_Contagemresultado_liqlogcod_N = 1;
         gxTv_SdtSolicitacaoServico_Contagemresultado_liqlogcod = 0;
         return  ;
      }

      public bool gxTv_SdtSolicitacaoServico_Contagemresultado_liqlogcod_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Modulo_Codigo" )]
      [  XmlElement( ElementName = "Modulo_Codigo"   )]
      public int gxTpr_Modulo_codigo
      {
         get {
            return gxTv_SdtSolicitacaoServico_Modulo_codigo ;
         }

         set {
            gxTv_SdtSolicitacaoServico_Modulo_codigo_N = 0;
            gxTv_SdtSolicitacaoServico_Modulo_codigo = (int)(value);
         }

      }

      public void gxTv_SdtSolicitacaoServico_Modulo_codigo_SetNull( )
      {
         gxTv_SdtSolicitacaoServico_Modulo_codigo_N = 1;
         gxTv_SdtSolicitacaoServico_Modulo_codigo = 0;
         return  ;
      }

      public bool gxTv_SdtSolicitacaoServico_Modulo_codigo_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_ContratadaCod" )]
      [  XmlElement( ElementName = "ContagemResultado_ContratadaCod"   )]
      public int gxTpr_Contagemresultado_contratadacod
      {
         get {
            return gxTv_SdtSolicitacaoServico_Contagemresultado_contratadacod ;
         }

         set {
            gxTv_SdtSolicitacaoServico_Contagemresultado_contratadacod_N = 0;
            gxTv_SdtSolicitacaoServico_Contagemresultado_contratadacod = (int)(value);
         }

      }

      public void gxTv_SdtSolicitacaoServico_Contagemresultado_contratadacod_SetNull( )
      {
         gxTv_SdtSolicitacaoServico_Contagemresultado_contratadacod_N = 1;
         gxTv_SdtSolicitacaoServico_Contagemresultado_contratadacod = 0;
         return  ;
      }

      public bool gxTv_SdtSolicitacaoServico_Contagemresultado_contratadacod_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_SistemaCod" )]
      [  XmlElement( ElementName = "ContagemResultado_SistemaCod"   )]
      public int gxTpr_Contagemresultado_sistemacod
      {
         get {
            return gxTv_SdtSolicitacaoServico_Contagemresultado_sistemacod ;
         }

         set {
            gxTv_SdtSolicitacaoServico_Contagemresultado_sistemacod_N = 0;
            gxTv_SdtSolicitacaoServico_Contagemresultado_sistemacod = (int)(value);
         }

      }

      public void gxTv_SdtSolicitacaoServico_Contagemresultado_sistemacod_SetNull( )
      {
         gxTv_SdtSolicitacaoServico_Contagemresultado_sistemacod_N = 1;
         gxTv_SdtSolicitacaoServico_Contagemresultado_sistemacod = 0;
         return  ;
      }

      public bool gxTv_SdtSolicitacaoServico_Contagemresultado_sistemacod_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_FncUsrCod" )]
      [  XmlElement( ElementName = "ContagemResultado_FncUsrCod"   )]
      public int gxTpr_Contagemresultado_fncusrcod
      {
         get {
            return gxTv_SdtSolicitacaoServico_Contagemresultado_fncusrcod ;
         }

         set {
            gxTv_SdtSolicitacaoServico_Contagemresultado_fncusrcod_N = 0;
            gxTv_SdtSolicitacaoServico_Contagemresultado_fncusrcod = (int)(value);
         }

      }

      public void gxTv_SdtSolicitacaoServico_Contagemresultado_fncusrcod_SetNull( )
      {
         gxTv_SdtSolicitacaoServico_Contagemresultado_fncusrcod_N = 1;
         gxTv_SdtSolicitacaoServico_Contagemresultado_fncusrcod = 0;
         return  ;
      }

      public bool gxTv_SdtSolicitacaoServico_Contagemresultado_fncusrcod_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_PrazoInicialDias" )]
      [  XmlElement( ElementName = "ContagemResultado_PrazoInicialDias"   )]
      public short gxTpr_Contagemresultado_prazoinicialdias
      {
         get {
            return gxTv_SdtSolicitacaoServico_Contagemresultado_prazoinicialdias ;
         }

         set {
            gxTv_SdtSolicitacaoServico_Contagemresultado_prazoinicialdias_N = 0;
            gxTv_SdtSolicitacaoServico_Contagemresultado_prazoinicialdias = (short)(value);
         }

      }

      public void gxTv_SdtSolicitacaoServico_Contagemresultado_prazoinicialdias_SetNull( )
      {
         gxTv_SdtSolicitacaoServico_Contagemresultado_prazoinicialdias_N = 1;
         gxTv_SdtSolicitacaoServico_Contagemresultado_prazoinicialdias = 0;
         return  ;
      }

      public bool gxTv_SdtSolicitacaoServico_Contagemresultado_prazoinicialdias_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_DataCadastro" )]
      [  XmlElement( ElementName = "ContagemResultado_DataCadastro"  , IsNullable=true )]
      public string gxTpr_Contagemresultado_datacadastro_Nullable
      {
         get {
            if ( gxTv_SdtSolicitacaoServico_Contagemresultado_datacadastro == DateTime.MinValue)
               return null;
            return new GxDatetimeString(gxTv_SdtSolicitacaoServico_Contagemresultado_datacadastro).value ;
         }

         set {
            gxTv_SdtSolicitacaoServico_Contagemresultado_datacadastro_N = 0;
            if (value == null || value == GxDatetimeString.NullValue )
               gxTv_SdtSolicitacaoServico_Contagemresultado_datacadastro = DateTime.MinValue;
            else
               gxTv_SdtSolicitacaoServico_Contagemresultado_datacadastro = DateTime.Parse( value);
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public DateTime gxTpr_Contagemresultado_datacadastro
      {
         get {
            return gxTv_SdtSolicitacaoServico_Contagemresultado_datacadastro ;
         }

         set {
            gxTv_SdtSolicitacaoServico_Contagemresultado_datacadastro_N = 0;
            gxTv_SdtSolicitacaoServico_Contagemresultado_datacadastro = (DateTime)(value);
         }

      }

      public void gxTv_SdtSolicitacaoServico_Contagemresultado_datacadastro_SetNull( )
      {
         gxTv_SdtSolicitacaoServico_Contagemresultado_datacadastro_N = 1;
         gxTv_SdtSolicitacaoServico_Contagemresultado_datacadastro = (DateTime)(DateTime.MinValue);
         return  ;
      }

      public bool gxTv_SdtSolicitacaoServico_Contagemresultado_datacadastro_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_OSManual" )]
      [  XmlElement( ElementName = "ContagemResultado_OSManual"   )]
      public bool gxTpr_Contagemresultado_osmanual
      {
         get {
            return gxTv_SdtSolicitacaoServico_Contagemresultado_osmanual ;
         }

         set {
            gxTv_SdtSolicitacaoServico_Contagemresultado_osmanual_N = 0;
            gxTv_SdtSolicitacaoServico_Contagemresultado_osmanual = value;
         }

      }

      public void gxTv_SdtSolicitacaoServico_Contagemresultado_osmanual_SetNull( )
      {
         gxTv_SdtSolicitacaoServico_Contagemresultado_osmanual_N = 1;
         gxTv_SdtSolicitacaoServico_Contagemresultado_osmanual = false;
         return  ;
      }

      public bool gxTv_SdtSolicitacaoServico_Contagemresultado_osmanual_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contagemresultado_baseline" )]
      [  XmlElement( ElementName = "Contagemresultado_baseline"   )]
      public bool gxTpr_Contagemresultado_baseline
      {
         get {
            return gxTv_SdtSolicitacaoServico_Contagemresultado_baseline ;
         }

         set {
            gxTv_SdtSolicitacaoServico_Contagemresultado_baseline_N = 0;
            gxTv_SdtSolicitacaoServico_Contagemresultado_baseline = value;
         }

      }

      public void gxTv_SdtSolicitacaoServico_Contagemresultado_baseline_SetNull( )
      {
         gxTv_SdtSolicitacaoServico_Contagemresultado_baseline_N = 1;
         gxTv_SdtSolicitacaoServico_Contagemresultado_baseline = false;
         return  ;
      }

      public bool gxTv_SdtSolicitacaoServico_Contagemresultado_baseline_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contagemresultado_ehvalidacao" )]
      [  XmlElement( ElementName = "Contagemresultado_ehvalidacao"   )]
      public bool gxTpr_Contagemresultado_ehvalidacao
      {
         get {
            return gxTv_SdtSolicitacaoServico_Contagemresultado_ehvalidacao ;
         }

         set {
            gxTv_SdtSolicitacaoServico_Contagemresultado_ehvalidacao_N = 0;
            gxTv_SdtSolicitacaoServico_Contagemresultado_ehvalidacao = value;
         }

      }

      public void gxTv_SdtSolicitacaoServico_Contagemresultado_ehvalidacao_SetNull( )
      {
         gxTv_SdtSolicitacaoServico_Contagemresultado_ehvalidacao_N = 1;
         gxTv_SdtSolicitacaoServico_Contagemresultado_ehvalidacao = false;
         return  ;
      }

      public bool gxTv_SdtSolicitacaoServico_Contagemresultado_ehvalidacao_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Mode" )]
      [  XmlElement( ElementName = "Mode"   )]
      public String gxTpr_Mode
      {
         get {
            return gxTv_SdtSolicitacaoServico_Mode ;
         }

         set {
            gxTv_SdtSolicitacaoServico_Mode = (String)(value);
         }

      }

      public void gxTv_SdtSolicitacaoServico_Mode_SetNull( )
      {
         gxTv_SdtSolicitacaoServico_Mode = "";
         return  ;
      }

      public bool gxTv_SdtSolicitacaoServico_Mode_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Initialized" )]
      [  XmlElement( ElementName = "Initialized"   )]
      public short gxTpr_Initialized
      {
         get {
            return gxTv_SdtSolicitacaoServico_Initialized ;
         }

         set {
            gxTv_SdtSolicitacaoServico_Initialized = (short)(value);
         }

      }

      public void gxTv_SdtSolicitacaoServico_Initialized_SetNull( )
      {
         gxTv_SdtSolicitacaoServico_Initialized = 0;
         return  ;
      }

      public bool gxTv_SdtSolicitacaoServico_Initialized_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_Codigo_Z" )]
      [  XmlElement( ElementName = "ContagemResultado_Codigo_Z"   )]
      public int gxTpr_Contagemresultado_codigo_Z
      {
         get {
            return gxTv_SdtSolicitacaoServico_Contagemresultado_codigo_Z ;
         }

         set {
            gxTv_SdtSolicitacaoServico_Contagemresultado_codigo_Z = (int)(value);
         }

      }

      public void gxTv_SdtSolicitacaoServico_Contagemresultado_codigo_Z_SetNull( )
      {
         gxTv_SdtSolicitacaoServico_Contagemresultado_codigo_Z = 0;
         return  ;
      }

      public bool gxTv_SdtSolicitacaoServico_Contagemresultado_codigo_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_Owner_Z" )]
      [  XmlElement( ElementName = "ContagemResultado_Owner_Z"   )]
      public int gxTpr_Contagemresultado_owner_Z
      {
         get {
            return gxTv_SdtSolicitacaoServico_Contagemresultado_owner_Z ;
         }

         set {
            gxTv_SdtSolicitacaoServico_Contagemresultado_owner_Z = (int)(value);
         }

      }

      public void gxTv_SdtSolicitacaoServico_Contagemresultado_owner_Z_SetNull( )
      {
         gxTv_SdtSolicitacaoServico_Contagemresultado_owner_Z = 0;
         return  ;
      }

      public bool gxTv_SdtSolicitacaoServico_Contagemresultado_owner_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_Descricao_Z" )]
      [  XmlElement( ElementName = "ContagemResultado_Descricao_Z"   )]
      public String gxTpr_Contagemresultado_descricao_Z
      {
         get {
            return gxTv_SdtSolicitacaoServico_Contagemresultado_descricao_Z ;
         }

         set {
            gxTv_SdtSolicitacaoServico_Contagemresultado_descricao_Z = (String)(value);
         }

      }

      public void gxTv_SdtSolicitacaoServico_Contagemresultado_descricao_Z_SetNull( )
      {
         gxTv_SdtSolicitacaoServico_Contagemresultado_descricao_Z = "";
         return  ;
      }

      public bool gxTv_SdtSolicitacaoServico_Contagemresultado_descricao_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_DataEntrega_Z" )]
      [  XmlElement( ElementName = "ContagemResultado_DataEntrega_Z"  , IsNullable=true )]
      public string gxTpr_Contagemresultado_dataentrega_Z_Nullable
      {
         get {
            if ( gxTv_SdtSolicitacaoServico_Contagemresultado_dataentrega_Z == DateTime.MinValue)
               return null;
            return new GxDateString(gxTv_SdtSolicitacaoServico_Contagemresultado_dataentrega_Z).value ;
         }

         set {
            if (value == null || value == GxDateString.NullValue )
               gxTv_SdtSolicitacaoServico_Contagemresultado_dataentrega_Z = DateTime.MinValue;
            else
               gxTv_SdtSolicitacaoServico_Contagemresultado_dataentrega_Z = DateTime.Parse( value);
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public DateTime gxTpr_Contagemresultado_dataentrega_Z
      {
         get {
            return gxTv_SdtSolicitacaoServico_Contagemresultado_dataentrega_Z ;
         }

         set {
            gxTv_SdtSolicitacaoServico_Contagemresultado_dataentrega_Z = (DateTime)(value);
         }

      }

      public void gxTv_SdtSolicitacaoServico_Contagemresultado_dataentrega_Z_SetNull( )
      {
         gxTv_SdtSolicitacaoServico_Contagemresultado_dataentrega_Z = (DateTime)(DateTime.MinValue);
         return  ;
      }

      public bool gxTv_SdtSolicitacaoServico_Contagemresultado_dataentrega_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_HoraEntrega_Z" )]
      [  XmlElement( ElementName = "ContagemResultado_HoraEntrega_Z"  , IsNullable=true )]
      public string gxTpr_Contagemresultado_horaentrega_Z_Nullable
      {
         get {
            if ( gxTv_SdtSolicitacaoServico_Contagemresultado_horaentrega_Z == DateTime.MinValue)
               return null;
            return new GxDatetimeString(gxTv_SdtSolicitacaoServico_Contagemresultado_horaentrega_Z).value ;
         }

         set {
            if (value == null || value == GxDatetimeString.NullValue )
               gxTv_SdtSolicitacaoServico_Contagemresultado_horaentrega_Z = DateTime.MinValue;
            else
               gxTv_SdtSolicitacaoServico_Contagemresultado_horaentrega_Z = DateTime.Parse( value);
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public DateTime gxTpr_Contagemresultado_horaentrega_Z
      {
         get {
            return gxTv_SdtSolicitacaoServico_Contagemresultado_horaentrega_Z ;
         }

         set {
            gxTv_SdtSolicitacaoServico_Contagemresultado_horaentrega_Z = (DateTime)(value);
         }

      }

      public void gxTv_SdtSolicitacaoServico_Contagemresultado_horaentrega_Z_SetNull( )
      {
         gxTv_SdtSolicitacaoServico_Contagemresultado_horaentrega_Z = (DateTime)(DateTime.MinValue);
         return  ;
      }

      public bool gxTv_SdtSolicitacaoServico_Contagemresultado_horaentrega_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_DataPrevista_Z" )]
      [  XmlElement( ElementName = "ContagemResultado_DataPrevista_Z"  , IsNullable=true )]
      public string gxTpr_Contagemresultado_dataprevista_Z_Nullable
      {
         get {
            if ( gxTv_SdtSolicitacaoServico_Contagemresultado_dataprevista_Z == DateTime.MinValue)
               return null;
            return new GxDatetimeString(gxTv_SdtSolicitacaoServico_Contagemresultado_dataprevista_Z).value ;
         }

         set {
            if (value == null || value == GxDatetimeString.NullValue )
               gxTv_SdtSolicitacaoServico_Contagemresultado_dataprevista_Z = DateTime.MinValue;
            else
               gxTv_SdtSolicitacaoServico_Contagemresultado_dataprevista_Z = DateTime.Parse( value);
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public DateTime gxTpr_Contagemresultado_dataprevista_Z
      {
         get {
            return gxTv_SdtSolicitacaoServico_Contagemresultado_dataprevista_Z ;
         }

         set {
            gxTv_SdtSolicitacaoServico_Contagemresultado_dataprevista_Z = (DateTime)(value);
         }

      }

      public void gxTv_SdtSolicitacaoServico_Contagemresultado_dataprevista_Z_SetNull( )
      {
         gxTv_SdtSolicitacaoServico_Contagemresultado_dataprevista_Z = (DateTime)(DateTime.MinValue);
         return  ;
      }

      public bool gxTv_SdtSolicitacaoServico_Contagemresultado_dataprevista_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_ServicoSS_Z" )]
      [  XmlElement( ElementName = "ContagemResultado_ServicoSS_Z"   )]
      public int gxTpr_Contagemresultado_servicoss_Z
      {
         get {
            return gxTv_SdtSolicitacaoServico_Contagemresultado_servicoss_Z ;
         }

         set {
            gxTv_SdtSolicitacaoServico_Contagemresultado_servicoss_Z = (int)(value);
         }

      }

      public void gxTv_SdtSolicitacaoServico_Contagemresultado_servicoss_Z_SetNull( )
      {
         gxTv_SdtSolicitacaoServico_Contagemresultado_servicoss_Z = 0;
         return  ;
      }

      public bool gxTv_SdtSolicitacaoServico_Contagemresultado_servicoss_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_SS_Z" )]
      [  XmlElement( ElementName = "ContagemResultado_SS_Z"   )]
      public int gxTpr_Contagemresultado_ss_Z
      {
         get {
            return gxTv_SdtSolicitacaoServico_Contagemresultado_ss_Z ;
         }

         set {
            gxTv_SdtSolicitacaoServico_Contagemresultado_ss_Z = (int)(value);
         }

      }

      public void gxTv_SdtSolicitacaoServico_Contagemresultado_ss_Z_SetNull( )
      {
         gxTv_SdtSolicitacaoServico_Contagemresultado_ss_Z = 0;
         return  ;
      }

      public bool gxTv_SdtSolicitacaoServico_Contagemresultado_ss_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_DemandaFM_Z" )]
      [  XmlElement( ElementName = "ContagemResultado_DemandaFM_Z"   )]
      public String gxTpr_Contagemresultado_demandafm_Z
      {
         get {
            return gxTv_SdtSolicitacaoServico_Contagemresultado_demandafm_Z ;
         }

         set {
            gxTv_SdtSolicitacaoServico_Contagemresultado_demandafm_Z = (String)(value);
         }

      }

      public void gxTv_SdtSolicitacaoServico_Contagemresultado_demandafm_Z_SetNull( )
      {
         gxTv_SdtSolicitacaoServico_Contagemresultado_demandafm_Z = "";
         return  ;
      }

      public bool gxTv_SdtSolicitacaoServico_Contagemresultado_demandafm_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_UOOwner_Z" )]
      [  XmlElement( ElementName = "ContagemResultado_UOOwner_Z"   )]
      public int gxTpr_Contagemresultado_uoowner_Z
      {
         get {
            return gxTv_SdtSolicitacaoServico_Contagemresultado_uoowner_Z ;
         }

         set {
            gxTv_SdtSolicitacaoServico_Contagemresultado_uoowner_Z = (int)(value);
         }

      }

      public void gxTv_SdtSolicitacaoServico_Contagemresultado_uoowner_Z_SetNull( )
      {
         gxTv_SdtSolicitacaoServico_Contagemresultado_uoowner_Z = 0;
         return  ;
      }

      public bool gxTv_SdtSolicitacaoServico_Contagemresultado_uoowner_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_Evento_Z" )]
      [  XmlElement( ElementName = "ContagemResultado_Evento_Z"   )]
      public short gxTpr_Contagemresultado_evento_Z
      {
         get {
            return gxTv_SdtSolicitacaoServico_Contagemresultado_evento_Z ;
         }

         set {
            gxTv_SdtSolicitacaoServico_Contagemresultado_evento_Z = (short)(value);
         }

      }

      public void gxTv_SdtSolicitacaoServico_Contagemresultado_evento_Z_SetNull( )
      {
         gxTv_SdtSolicitacaoServico_Contagemresultado_evento_Z = 0;
         return  ;
      }

      public bool gxTv_SdtSolicitacaoServico_Contagemresultado_evento_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_TipoRegistro_Z" )]
      [  XmlElement( ElementName = "ContagemResultado_TipoRegistro_Z"   )]
      public short gxTpr_Contagemresultado_tiporegistro_Z
      {
         get {
            return gxTv_SdtSolicitacaoServico_Contagemresultado_tiporegistro_Z ;
         }

         set {
            gxTv_SdtSolicitacaoServico_Contagemresultado_tiporegistro_Z = (short)(value);
         }

      }

      public void gxTv_SdtSolicitacaoServico_Contagemresultado_tiporegistro_Z_SetNull( )
      {
         gxTv_SdtSolicitacaoServico_Contagemresultado_tiporegistro_Z = 0;
         return  ;
      }

      public bool gxTv_SdtSolicitacaoServico_Contagemresultado_tiporegistro_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_Referencia_Z" )]
      [  XmlElement( ElementName = "ContagemResultado_Referencia_Z"   )]
      public String gxTpr_Contagemresultado_referencia_Z
      {
         get {
            return gxTv_SdtSolicitacaoServico_Contagemresultado_referencia_Z ;
         }

         set {
            gxTv_SdtSolicitacaoServico_Contagemresultado_referencia_Z = (String)(value);
         }

      }

      public void gxTv_SdtSolicitacaoServico_Contagemresultado_referencia_Z_SetNull( )
      {
         gxTv_SdtSolicitacaoServico_Contagemresultado_referencia_Z = "";
         return  ;
      }

      public bool gxTv_SdtSolicitacaoServico_Contagemresultado_referencia_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_Restricoes_Z" )]
      [  XmlElement( ElementName = "ContagemResultado_Restricoes_Z"   )]
      public String gxTpr_Contagemresultado_restricoes_Z
      {
         get {
            return gxTv_SdtSolicitacaoServico_Contagemresultado_restricoes_Z ;
         }

         set {
            gxTv_SdtSolicitacaoServico_Contagemresultado_restricoes_Z = (String)(value);
         }

      }

      public void gxTv_SdtSolicitacaoServico_Contagemresultado_restricoes_Z_SetNull( )
      {
         gxTv_SdtSolicitacaoServico_Contagemresultado_restricoes_Z = "";
         return  ;
      }

      public bool gxTv_SdtSolicitacaoServico_Contagemresultado_restricoes_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_PrioridadePrevista_Z" )]
      [  XmlElement( ElementName = "ContagemResultado_PrioridadePrevista_Z"   )]
      public String gxTpr_Contagemresultado_prioridadeprevista_Z
      {
         get {
            return gxTv_SdtSolicitacaoServico_Contagemresultado_prioridadeprevista_Z ;
         }

         set {
            gxTv_SdtSolicitacaoServico_Contagemresultado_prioridadeprevista_Z = (String)(value);
         }

      }

      public void gxTv_SdtSolicitacaoServico_Contagemresultado_prioridadeprevista_Z_SetNull( )
      {
         gxTv_SdtSolicitacaoServico_Contagemresultado_prioridadeprevista_Z = "";
         return  ;
      }

      public bool gxTv_SdtSolicitacaoServico_Contagemresultado_prioridadeprevista_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_StatusDmn_Z" )]
      [  XmlElement( ElementName = "ContagemResultado_StatusDmn_Z"   )]
      public String gxTpr_Contagemresultado_statusdmn_Z
      {
         get {
            return gxTv_SdtSolicitacaoServico_Contagemresultado_statusdmn_Z ;
         }

         set {
            gxTv_SdtSolicitacaoServico_Contagemresultado_statusdmn_Z = (String)(value);
         }

      }

      public void gxTv_SdtSolicitacaoServico_Contagemresultado_statusdmn_Z_SetNull( )
      {
         gxTv_SdtSolicitacaoServico_Contagemresultado_statusdmn_Z = "";
         return  ;
      }

      public bool gxTv_SdtSolicitacaoServico_Contagemresultado_statusdmn_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_DataDmn_Z" )]
      [  XmlElement( ElementName = "ContagemResultado_DataDmn_Z"  , IsNullable=true )]
      public string gxTpr_Contagemresultado_datadmn_Z_Nullable
      {
         get {
            if ( gxTv_SdtSolicitacaoServico_Contagemresultado_datadmn_Z == DateTime.MinValue)
               return null;
            return new GxDateString(gxTv_SdtSolicitacaoServico_Contagemresultado_datadmn_Z).value ;
         }

         set {
            if (value == null || value == GxDateString.NullValue )
               gxTv_SdtSolicitacaoServico_Contagemresultado_datadmn_Z = DateTime.MinValue;
            else
               gxTv_SdtSolicitacaoServico_Contagemresultado_datadmn_Z = DateTime.Parse( value);
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public DateTime gxTpr_Contagemresultado_datadmn_Z
      {
         get {
            return gxTv_SdtSolicitacaoServico_Contagemresultado_datadmn_Z ;
         }

         set {
            gxTv_SdtSolicitacaoServico_Contagemresultado_datadmn_Z = (DateTime)(value);
         }

      }

      public void gxTv_SdtSolicitacaoServico_Contagemresultado_datadmn_Z_SetNull( )
      {
         gxTv_SdtSolicitacaoServico_Contagemresultado_datadmn_Z = (DateTime)(DateTime.MinValue);
         return  ;
      }

      public bool gxTv_SdtSolicitacaoServico_Contagemresultado_datadmn_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_CntSrvCod_Z" )]
      [  XmlElement( ElementName = "ContagemResultado_CntSrvCod_Z"   )]
      public int gxTpr_Contagemresultado_cntsrvcod_Z
      {
         get {
            return gxTv_SdtSolicitacaoServico_Contagemresultado_cntsrvcod_Z ;
         }

         set {
            gxTv_SdtSolicitacaoServico_Contagemresultado_cntsrvcod_Z = (int)(value);
         }

      }

      public void gxTv_SdtSolicitacaoServico_Contagemresultado_cntsrvcod_Z_SetNull( )
      {
         gxTv_SdtSolicitacaoServico_Contagemresultado_cntsrvcod_Z = 0;
         return  ;
      }

      public bool gxTv_SdtSolicitacaoServico_Contagemresultado_cntsrvcod_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_OSVinculada_Z" )]
      [  XmlElement( ElementName = "ContagemResultado_OSVinculada_Z"   )]
      public int gxTpr_Contagemresultado_osvinculada_Z
      {
         get {
            return gxTv_SdtSolicitacaoServico_Contagemresultado_osvinculada_Z ;
         }

         set {
            gxTv_SdtSolicitacaoServico_Contagemresultado_osvinculada_Z = (int)(value);
         }

      }

      public void gxTv_SdtSolicitacaoServico_Contagemresultado_osvinculada_Z_SetNull( )
      {
         gxTv_SdtSolicitacaoServico_Contagemresultado_osvinculada_Z = 0;
         return  ;
      }

      public bool gxTv_SdtSolicitacaoServico_Contagemresultado_osvinculada_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_NaoCnfDmnCod_Z" )]
      [  XmlElement( ElementName = "ContagemResultado_NaoCnfDmnCod_Z"   )]
      public int gxTpr_Contagemresultado_naocnfdmncod_Z
      {
         get {
            return gxTv_SdtSolicitacaoServico_Contagemresultado_naocnfdmncod_Z ;
         }

         set {
            gxTv_SdtSolicitacaoServico_Contagemresultado_naocnfdmncod_Z = (int)(value);
         }

      }

      public void gxTv_SdtSolicitacaoServico_Contagemresultado_naocnfdmncod_Z_SetNull( )
      {
         gxTv_SdtSolicitacaoServico_Contagemresultado_naocnfdmncod_Z = 0;
         return  ;
      }

      public bool gxTv_SdtSolicitacaoServico_Contagemresultado_naocnfdmncod_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_LoteAceiteCod_Z" )]
      [  XmlElement( ElementName = "ContagemResultado_LoteAceiteCod_Z"   )]
      public int gxTpr_Contagemresultado_loteaceitecod_Z
      {
         get {
            return gxTv_SdtSolicitacaoServico_Contagemresultado_loteaceitecod_Z ;
         }

         set {
            gxTv_SdtSolicitacaoServico_Contagemresultado_loteaceitecod_Z = (int)(value);
         }

      }

      public void gxTv_SdtSolicitacaoServico_Contagemresultado_loteaceitecod_Z_SetNull( )
      {
         gxTv_SdtSolicitacaoServico_Contagemresultado_loteaceitecod_Z = 0;
         return  ;
      }

      public bool gxTv_SdtSolicitacaoServico_Contagemresultado_loteaceitecod_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_ContratadaOrigemCod_Z" )]
      [  XmlElement( ElementName = "ContagemResultado_ContratadaOrigemCod_Z"   )]
      public int gxTpr_Contagemresultado_contratadaorigemcod_Z
      {
         get {
            return gxTv_SdtSolicitacaoServico_Contagemresultado_contratadaorigemcod_Z ;
         }

         set {
            gxTv_SdtSolicitacaoServico_Contagemresultado_contratadaorigemcod_Z = (int)(value);
         }

      }

      public void gxTv_SdtSolicitacaoServico_Contagemresultado_contratadaorigemcod_Z_SetNull( )
      {
         gxTv_SdtSolicitacaoServico_Contagemresultado_contratadaorigemcod_Z = 0;
         return  ;
      }

      public bool gxTv_SdtSolicitacaoServico_Contagemresultado_contratadaorigemcod_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_ContadorFSCod_Z" )]
      [  XmlElement( ElementName = "ContagemResultado_ContadorFSCod_Z"   )]
      public int gxTpr_Contagemresultado_contadorfscod_Z
      {
         get {
            return gxTv_SdtSolicitacaoServico_Contagemresultado_contadorfscod_Z ;
         }

         set {
            gxTv_SdtSolicitacaoServico_Contagemresultado_contadorfscod_Z = (int)(value);
         }

      }

      public void gxTv_SdtSolicitacaoServico_Contagemresultado_contadorfscod_Z_SetNull( )
      {
         gxTv_SdtSolicitacaoServico_Contagemresultado_contadorfscod_Z = 0;
         return  ;
      }

      public bool gxTv_SdtSolicitacaoServico_Contagemresultado_contadorfscod_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_Responsavel_Z" )]
      [  XmlElement( ElementName = "ContagemResultado_Responsavel_Z"   )]
      public int gxTpr_Contagemresultado_responsavel_Z
      {
         get {
            return gxTv_SdtSolicitacaoServico_Contagemresultado_responsavel_Z ;
         }

         set {
            gxTv_SdtSolicitacaoServico_Contagemresultado_responsavel_Z = (int)(value);
         }

      }

      public void gxTv_SdtSolicitacaoServico_Contagemresultado_responsavel_Z_SetNull( )
      {
         gxTv_SdtSolicitacaoServico_Contagemresultado_responsavel_Z = 0;
         return  ;
      }

      public bool gxTv_SdtSolicitacaoServico_Contagemresultado_responsavel_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_LiqLogCod_Z" )]
      [  XmlElement( ElementName = "ContagemResultado_LiqLogCod_Z"   )]
      public int gxTpr_Contagemresultado_liqlogcod_Z
      {
         get {
            return gxTv_SdtSolicitacaoServico_Contagemresultado_liqlogcod_Z ;
         }

         set {
            gxTv_SdtSolicitacaoServico_Contagemresultado_liqlogcod_Z = (int)(value);
         }

      }

      public void gxTv_SdtSolicitacaoServico_Contagemresultado_liqlogcod_Z_SetNull( )
      {
         gxTv_SdtSolicitacaoServico_Contagemresultado_liqlogcod_Z = 0;
         return  ;
      }

      public bool gxTv_SdtSolicitacaoServico_Contagemresultado_liqlogcod_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Modulo_Codigo_Z" )]
      [  XmlElement( ElementName = "Modulo_Codigo_Z"   )]
      public int gxTpr_Modulo_codigo_Z
      {
         get {
            return gxTv_SdtSolicitacaoServico_Modulo_codigo_Z ;
         }

         set {
            gxTv_SdtSolicitacaoServico_Modulo_codigo_Z = (int)(value);
         }

      }

      public void gxTv_SdtSolicitacaoServico_Modulo_codigo_Z_SetNull( )
      {
         gxTv_SdtSolicitacaoServico_Modulo_codigo_Z = 0;
         return  ;
      }

      public bool gxTv_SdtSolicitacaoServico_Modulo_codigo_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_ContratadaCod_Z" )]
      [  XmlElement( ElementName = "ContagemResultado_ContratadaCod_Z"   )]
      public int gxTpr_Contagemresultado_contratadacod_Z
      {
         get {
            return gxTv_SdtSolicitacaoServico_Contagemresultado_contratadacod_Z ;
         }

         set {
            gxTv_SdtSolicitacaoServico_Contagemresultado_contratadacod_Z = (int)(value);
         }

      }

      public void gxTv_SdtSolicitacaoServico_Contagemresultado_contratadacod_Z_SetNull( )
      {
         gxTv_SdtSolicitacaoServico_Contagemresultado_contratadacod_Z = 0;
         return  ;
      }

      public bool gxTv_SdtSolicitacaoServico_Contagemresultado_contratadacod_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_SistemaCod_Z" )]
      [  XmlElement( ElementName = "ContagemResultado_SistemaCod_Z"   )]
      public int gxTpr_Contagemresultado_sistemacod_Z
      {
         get {
            return gxTv_SdtSolicitacaoServico_Contagemresultado_sistemacod_Z ;
         }

         set {
            gxTv_SdtSolicitacaoServico_Contagemresultado_sistemacod_Z = (int)(value);
         }

      }

      public void gxTv_SdtSolicitacaoServico_Contagemresultado_sistemacod_Z_SetNull( )
      {
         gxTv_SdtSolicitacaoServico_Contagemresultado_sistemacod_Z = 0;
         return  ;
      }

      public bool gxTv_SdtSolicitacaoServico_Contagemresultado_sistemacod_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_FncUsrCod_Z" )]
      [  XmlElement( ElementName = "ContagemResultado_FncUsrCod_Z"   )]
      public int gxTpr_Contagemresultado_fncusrcod_Z
      {
         get {
            return gxTv_SdtSolicitacaoServico_Contagemresultado_fncusrcod_Z ;
         }

         set {
            gxTv_SdtSolicitacaoServico_Contagemresultado_fncusrcod_Z = (int)(value);
         }

      }

      public void gxTv_SdtSolicitacaoServico_Contagemresultado_fncusrcod_Z_SetNull( )
      {
         gxTv_SdtSolicitacaoServico_Contagemresultado_fncusrcod_Z = 0;
         return  ;
      }

      public bool gxTv_SdtSolicitacaoServico_Contagemresultado_fncusrcod_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_PrazoInicialDias_Z" )]
      [  XmlElement( ElementName = "ContagemResultado_PrazoInicialDias_Z"   )]
      public short gxTpr_Contagemresultado_prazoinicialdias_Z
      {
         get {
            return gxTv_SdtSolicitacaoServico_Contagemresultado_prazoinicialdias_Z ;
         }

         set {
            gxTv_SdtSolicitacaoServico_Contagemresultado_prazoinicialdias_Z = (short)(value);
         }

      }

      public void gxTv_SdtSolicitacaoServico_Contagemresultado_prazoinicialdias_Z_SetNull( )
      {
         gxTv_SdtSolicitacaoServico_Contagemresultado_prazoinicialdias_Z = 0;
         return  ;
      }

      public bool gxTv_SdtSolicitacaoServico_Contagemresultado_prazoinicialdias_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_DataCadastro_Z" )]
      [  XmlElement( ElementName = "ContagemResultado_DataCadastro_Z"  , IsNullable=true )]
      public string gxTpr_Contagemresultado_datacadastro_Z_Nullable
      {
         get {
            if ( gxTv_SdtSolicitacaoServico_Contagemresultado_datacadastro_Z == DateTime.MinValue)
               return null;
            return new GxDatetimeString(gxTv_SdtSolicitacaoServico_Contagemresultado_datacadastro_Z).value ;
         }

         set {
            if (value == null || value == GxDatetimeString.NullValue )
               gxTv_SdtSolicitacaoServico_Contagemresultado_datacadastro_Z = DateTime.MinValue;
            else
               gxTv_SdtSolicitacaoServico_Contagemresultado_datacadastro_Z = DateTime.Parse( value);
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public DateTime gxTpr_Contagemresultado_datacadastro_Z
      {
         get {
            return gxTv_SdtSolicitacaoServico_Contagemresultado_datacadastro_Z ;
         }

         set {
            gxTv_SdtSolicitacaoServico_Contagemresultado_datacadastro_Z = (DateTime)(value);
         }

      }

      public void gxTv_SdtSolicitacaoServico_Contagemresultado_datacadastro_Z_SetNull( )
      {
         gxTv_SdtSolicitacaoServico_Contagemresultado_datacadastro_Z = (DateTime)(DateTime.MinValue);
         return  ;
      }

      public bool gxTv_SdtSolicitacaoServico_Contagemresultado_datacadastro_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_OSManual_Z" )]
      [  XmlElement( ElementName = "ContagemResultado_OSManual_Z"   )]
      public bool gxTpr_Contagemresultado_osmanual_Z
      {
         get {
            return gxTv_SdtSolicitacaoServico_Contagemresultado_osmanual_Z ;
         }

         set {
            gxTv_SdtSolicitacaoServico_Contagemresultado_osmanual_Z = value;
         }

      }

      public void gxTv_SdtSolicitacaoServico_Contagemresultado_osmanual_Z_SetNull( )
      {
         gxTv_SdtSolicitacaoServico_Contagemresultado_osmanual_Z = false;
         return  ;
      }

      public bool gxTv_SdtSolicitacaoServico_Contagemresultado_osmanual_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contagemresultado_baseline_Z" )]
      [  XmlElement( ElementName = "Contagemresultado_baseline_Z"   )]
      public bool gxTpr_Contagemresultado_baseline_Z
      {
         get {
            return gxTv_SdtSolicitacaoServico_Contagemresultado_baseline_Z ;
         }

         set {
            gxTv_SdtSolicitacaoServico_Contagemresultado_baseline_Z = value;
         }

      }

      public void gxTv_SdtSolicitacaoServico_Contagemresultado_baseline_Z_SetNull( )
      {
         gxTv_SdtSolicitacaoServico_Contagemresultado_baseline_Z = false;
         return  ;
      }

      public bool gxTv_SdtSolicitacaoServico_Contagemresultado_baseline_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contagemresultado_ehvalidacao_Z" )]
      [  XmlElement( ElementName = "Contagemresultado_ehvalidacao_Z"   )]
      public bool gxTpr_Contagemresultado_ehvalidacao_Z
      {
         get {
            return gxTv_SdtSolicitacaoServico_Contagemresultado_ehvalidacao_Z ;
         }

         set {
            gxTv_SdtSolicitacaoServico_Contagemresultado_ehvalidacao_Z = value;
         }

      }

      public void gxTv_SdtSolicitacaoServico_Contagemresultado_ehvalidacao_Z_SetNull( )
      {
         gxTv_SdtSolicitacaoServico_Contagemresultado_ehvalidacao_Z = false;
         return  ;
      }

      public bool gxTv_SdtSolicitacaoServico_Contagemresultado_ehvalidacao_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_Descricao_N" )]
      [  XmlElement( ElementName = "ContagemResultado_Descricao_N"   )]
      public short gxTpr_Contagemresultado_descricao_N
      {
         get {
            return gxTv_SdtSolicitacaoServico_Contagemresultado_descricao_N ;
         }

         set {
            gxTv_SdtSolicitacaoServico_Contagemresultado_descricao_N = (short)(value);
         }

      }

      public void gxTv_SdtSolicitacaoServico_Contagemresultado_descricao_N_SetNull( )
      {
         gxTv_SdtSolicitacaoServico_Contagemresultado_descricao_N = 0;
         return  ;
      }

      public bool gxTv_SdtSolicitacaoServico_Contagemresultado_descricao_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_Observacao_N" )]
      [  XmlElement( ElementName = "ContagemResultado_Observacao_N"   )]
      public short gxTpr_Contagemresultado_observacao_N
      {
         get {
            return gxTv_SdtSolicitacaoServico_Contagemresultado_observacao_N ;
         }

         set {
            gxTv_SdtSolicitacaoServico_Contagemresultado_observacao_N = (short)(value);
         }

      }

      public void gxTv_SdtSolicitacaoServico_Contagemresultado_observacao_N_SetNull( )
      {
         gxTv_SdtSolicitacaoServico_Contagemresultado_observacao_N = 0;
         return  ;
      }

      public bool gxTv_SdtSolicitacaoServico_Contagemresultado_observacao_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_DataEntrega_N" )]
      [  XmlElement( ElementName = "ContagemResultado_DataEntrega_N"   )]
      public short gxTpr_Contagemresultado_dataentrega_N
      {
         get {
            return gxTv_SdtSolicitacaoServico_Contagemresultado_dataentrega_N ;
         }

         set {
            gxTv_SdtSolicitacaoServico_Contagemresultado_dataentrega_N = (short)(value);
         }

      }

      public void gxTv_SdtSolicitacaoServico_Contagemresultado_dataentrega_N_SetNull( )
      {
         gxTv_SdtSolicitacaoServico_Contagemresultado_dataentrega_N = 0;
         return  ;
      }

      public bool gxTv_SdtSolicitacaoServico_Contagemresultado_dataentrega_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_HoraEntrega_N" )]
      [  XmlElement( ElementName = "ContagemResultado_HoraEntrega_N"   )]
      public short gxTpr_Contagemresultado_horaentrega_N
      {
         get {
            return gxTv_SdtSolicitacaoServico_Contagemresultado_horaentrega_N ;
         }

         set {
            gxTv_SdtSolicitacaoServico_Contagemresultado_horaentrega_N = (short)(value);
         }

      }

      public void gxTv_SdtSolicitacaoServico_Contagemresultado_horaentrega_N_SetNull( )
      {
         gxTv_SdtSolicitacaoServico_Contagemresultado_horaentrega_N = 0;
         return  ;
      }

      public bool gxTv_SdtSolicitacaoServico_Contagemresultado_horaentrega_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_DataPrevista_N" )]
      [  XmlElement( ElementName = "ContagemResultado_DataPrevista_N"   )]
      public short gxTpr_Contagemresultado_dataprevista_N
      {
         get {
            return gxTv_SdtSolicitacaoServico_Contagemresultado_dataprevista_N ;
         }

         set {
            gxTv_SdtSolicitacaoServico_Contagemresultado_dataprevista_N = (short)(value);
         }

      }

      public void gxTv_SdtSolicitacaoServico_Contagemresultado_dataprevista_N_SetNull( )
      {
         gxTv_SdtSolicitacaoServico_Contagemresultado_dataprevista_N = 0;
         return  ;
      }

      public bool gxTv_SdtSolicitacaoServico_Contagemresultado_dataprevista_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_ServicoSS_N" )]
      [  XmlElement( ElementName = "ContagemResultado_ServicoSS_N"   )]
      public short gxTpr_Contagemresultado_servicoss_N
      {
         get {
            return gxTv_SdtSolicitacaoServico_Contagemresultado_servicoss_N ;
         }

         set {
            gxTv_SdtSolicitacaoServico_Contagemresultado_servicoss_N = (short)(value);
         }

      }

      public void gxTv_SdtSolicitacaoServico_Contagemresultado_servicoss_N_SetNull( )
      {
         gxTv_SdtSolicitacaoServico_Contagemresultado_servicoss_N = 0;
         return  ;
      }

      public bool gxTv_SdtSolicitacaoServico_Contagemresultado_servicoss_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_SS_N" )]
      [  XmlElement( ElementName = "ContagemResultado_SS_N"   )]
      public short gxTpr_Contagemresultado_ss_N
      {
         get {
            return gxTv_SdtSolicitacaoServico_Contagemresultado_ss_N ;
         }

         set {
            gxTv_SdtSolicitacaoServico_Contagemresultado_ss_N = (short)(value);
         }

      }

      public void gxTv_SdtSolicitacaoServico_Contagemresultado_ss_N_SetNull( )
      {
         gxTv_SdtSolicitacaoServico_Contagemresultado_ss_N = 0;
         return  ;
      }

      public bool gxTv_SdtSolicitacaoServico_Contagemresultado_ss_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_DemandaFM_N" )]
      [  XmlElement( ElementName = "ContagemResultado_DemandaFM_N"   )]
      public short gxTpr_Contagemresultado_demandafm_N
      {
         get {
            return gxTv_SdtSolicitacaoServico_Contagemresultado_demandafm_N ;
         }

         set {
            gxTv_SdtSolicitacaoServico_Contagemresultado_demandafm_N = (short)(value);
         }

      }

      public void gxTv_SdtSolicitacaoServico_Contagemresultado_demandafm_N_SetNull( )
      {
         gxTv_SdtSolicitacaoServico_Contagemresultado_demandafm_N = 0;
         return  ;
      }

      public bool gxTv_SdtSolicitacaoServico_Contagemresultado_demandafm_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_UOOwner_N" )]
      [  XmlElement( ElementName = "ContagemResultado_UOOwner_N"   )]
      public short gxTpr_Contagemresultado_uoowner_N
      {
         get {
            return gxTv_SdtSolicitacaoServico_Contagemresultado_uoowner_N ;
         }

         set {
            gxTv_SdtSolicitacaoServico_Contagemresultado_uoowner_N = (short)(value);
         }

      }

      public void gxTv_SdtSolicitacaoServico_Contagemresultado_uoowner_N_SetNull( )
      {
         gxTv_SdtSolicitacaoServico_Contagemresultado_uoowner_N = 0;
         return  ;
      }

      public bool gxTv_SdtSolicitacaoServico_Contagemresultado_uoowner_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_Evento_N" )]
      [  XmlElement( ElementName = "ContagemResultado_Evento_N"   )]
      public short gxTpr_Contagemresultado_evento_N
      {
         get {
            return gxTv_SdtSolicitacaoServico_Contagemresultado_evento_N ;
         }

         set {
            gxTv_SdtSolicitacaoServico_Contagemresultado_evento_N = (short)(value);
         }

      }

      public void gxTv_SdtSolicitacaoServico_Contagemresultado_evento_N_SetNull( )
      {
         gxTv_SdtSolicitacaoServico_Contagemresultado_evento_N = 0;
         return  ;
      }

      public bool gxTv_SdtSolicitacaoServico_Contagemresultado_evento_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_Referencia_N" )]
      [  XmlElement( ElementName = "ContagemResultado_Referencia_N"   )]
      public short gxTpr_Contagemresultado_referencia_N
      {
         get {
            return gxTv_SdtSolicitacaoServico_Contagemresultado_referencia_N ;
         }

         set {
            gxTv_SdtSolicitacaoServico_Contagemresultado_referencia_N = (short)(value);
         }

      }

      public void gxTv_SdtSolicitacaoServico_Contagemresultado_referencia_N_SetNull( )
      {
         gxTv_SdtSolicitacaoServico_Contagemresultado_referencia_N = 0;
         return  ;
      }

      public bool gxTv_SdtSolicitacaoServico_Contagemresultado_referencia_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_Restricoes_N" )]
      [  XmlElement( ElementName = "ContagemResultado_Restricoes_N"   )]
      public short gxTpr_Contagemresultado_restricoes_N
      {
         get {
            return gxTv_SdtSolicitacaoServico_Contagemresultado_restricoes_N ;
         }

         set {
            gxTv_SdtSolicitacaoServico_Contagemresultado_restricoes_N = (short)(value);
         }

      }

      public void gxTv_SdtSolicitacaoServico_Contagemresultado_restricoes_N_SetNull( )
      {
         gxTv_SdtSolicitacaoServico_Contagemresultado_restricoes_N = 0;
         return  ;
      }

      public bool gxTv_SdtSolicitacaoServico_Contagemresultado_restricoes_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_PrioridadePrevista_N" )]
      [  XmlElement( ElementName = "ContagemResultado_PrioridadePrevista_N"   )]
      public short gxTpr_Contagemresultado_prioridadeprevista_N
      {
         get {
            return gxTv_SdtSolicitacaoServico_Contagemresultado_prioridadeprevista_N ;
         }

         set {
            gxTv_SdtSolicitacaoServico_Contagemresultado_prioridadeprevista_N = (short)(value);
         }

      }

      public void gxTv_SdtSolicitacaoServico_Contagemresultado_prioridadeprevista_N_SetNull( )
      {
         gxTv_SdtSolicitacaoServico_Contagemresultado_prioridadeprevista_N = 0;
         return  ;
      }

      public bool gxTv_SdtSolicitacaoServico_Contagemresultado_prioridadeprevista_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_StatusDmn_N" )]
      [  XmlElement( ElementName = "ContagemResultado_StatusDmn_N"   )]
      public short gxTpr_Contagemresultado_statusdmn_N
      {
         get {
            return gxTv_SdtSolicitacaoServico_Contagemresultado_statusdmn_N ;
         }

         set {
            gxTv_SdtSolicitacaoServico_Contagemresultado_statusdmn_N = (short)(value);
         }

      }

      public void gxTv_SdtSolicitacaoServico_Contagemresultado_statusdmn_N_SetNull( )
      {
         gxTv_SdtSolicitacaoServico_Contagemresultado_statusdmn_N = 0;
         return  ;
      }

      public bool gxTv_SdtSolicitacaoServico_Contagemresultado_statusdmn_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_CntSrvCod_N" )]
      [  XmlElement( ElementName = "ContagemResultado_CntSrvCod_N"   )]
      public short gxTpr_Contagemresultado_cntsrvcod_N
      {
         get {
            return gxTv_SdtSolicitacaoServico_Contagemresultado_cntsrvcod_N ;
         }

         set {
            gxTv_SdtSolicitacaoServico_Contagemresultado_cntsrvcod_N = (short)(value);
         }

      }

      public void gxTv_SdtSolicitacaoServico_Contagemresultado_cntsrvcod_N_SetNull( )
      {
         gxTv_SdtSolicitacaoServico_Contagemresultado_cntsrvcod_N = 0;
         return  ;
      }

      public bool gxTv_SdtSolicitacaoServico_Contagemresultado_cntsrvcod_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_OSVinculada_N" )]
      [  XmlElement( ElementName = "ContagemResultado_OSVinculada_N"   )]
      public short gxTpr_Contagemresultado_osvinculada_N
      {
         get {
            return gxTv_SdtSolicitacaoServico_Contagemresultado_osvinculada_N ;
         }

         set {
            gxTv_SdtSolicitacaoServico_Contagemresultado_osvinculada_N = (short)(value);
         }

      }

      public void gxTv_SdtSolicitacaoServico_Contagemresultado_osvinculada_N_SetNull( )
      {
         gxTv_SdtSolicitacaoServico_Contagemresultado_osvinculada_N = 0;
         return  ;
      }

      public bool gxTv_SdtSolicitacaoServico_Contagemresultado_osvinculada_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_NaoCnfDmnCod_N" )]
      [  XmlElement( ElementName = "ContagemResultado_NaoCnfDmnCod_N"   )]
      public short gxTpr_Contagemresultado_naocnfdmncod_N
      {
         get {
            return gxTv_SdtSolicitacaoServico_Contagemresultado_naocnfdmncod_N ;
         }

         set {
            gxTv_SdtSolicitacaoServico_Contagemresultado_naocnfdmncod_N = (short)(value);
         }

      }

      public void gxTv_SdtSolicitacaoServico_Contagemresultado_naocnfdmncod_N_SetNull( )
      {
         gxTv_SdtSolicitacaoServico_Contagemresultado_naocnfdmncod_N = 0;
         return  ;
      }

      public bool gxTv_SdtSolicitacaoServico_Contagemresultado_naocnfdmncod_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_LoteAceiteCod_N" )]
      [  XmlElement( ElementName = "ContagemResultado_LoteAceiteCod_N"   )]
      public short gxTpr_Contagemresultado_loteaceitecod_N
      {
         get {
            return gxTv_SdtSolicitacaoServico_Contagemresultado_loteaceitecod_N ;
         }

         set {
            gxTv_SdtSolicitacaoServico_Contagemresultado_loteaceitecod_N = (short)(value);
         }

      }

      public void gxTv_SdtSolicitacaoServico_Contagemresultado_loteaceitecod_N_SetNull( )
      {
         gxTv_SdtSolicitacaoServico_Contagemresultado_loteaceitecod_N = 0;
         return  ;
      }

      public bool gxTv_SdtSolicitacaoServico_Contagemresultado_loteaceitecod_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_ContratadaOrigemCod_N" )]
      [  XmlElement( ElementName = "ContagemResultado_ContratadaOrigemCod_N"   )]
      public short gxTpr_Contagemresultado_contratadaorigemcod_N
      {
         get {
            return gxTv_SdtSolicitacaoServico_Contagemresultado_contratadaorigemcod_N ;
         }

         set {
            gxTv_SdtSolicitacaoServico_Contagemresultado_contratadaorigemcod_N = (short)(value);
         }

      }

      public void gxTv_SdtSolicitacaoServico_Contagemresultado_contratadaorigemcod_N_SetNull( )
      {
         gxTv_SdtSolicitacaoServico_Contagemresultado_contratadaorigemcod_N = 0;
         return  ;
      }

      public bool gxTv_SdtSolicitacaoServico_Contagemresultado_contratadaorigemcod_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_ContadorFSCod_N" )]
      [  XmlElement( ElementName = "ContagemResultado_ContadorFSCod_N"   )]
      public short gxTpr_Contagemresultado_contadorfscod_N
      {
         get {
            return gxTv_SdtSolicitacaoServico_Contagemresultado_contadorfscod_N ;
         }

         set {
            gxTv_SdtSolicitacaoServico_Contagemresultado_contadorfscod_N = (short)(value);
         }

      }

      public void gxTv_SdtSolicitacaoServico_Contagemresultado_contadorfscod_N_SetNull( )
      {
         gxTv_SdtSolicitacaoServico_Contagemresultado_contadorfscod_N = 0;
         return  ;
      }

      public bool gxTv_SdtSolicitacaoServico_Contagemresultado_contadorfscod_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_Responsavel_N" )]
      [  XmlElement( ElementName = "ContagemResultado_Responsavel_N"   )]
      public short gxTpr_Contagemresultado_responsavel_N
      {
         get {
            return gxTv_SdtSolicitacaoServico_Contagemresultado_responsavel_N ;
         }

         set {
            gxTv_SdtSolicitacaoServico_Contagemresultado_responsavel_N = (short)(value);
         }

      }

      public void gxTv_SdtSolicitacaoServico_Contagemresultado_responsavel_N_SetNull( )
      {
         gxTv_SdtSolicitacaoServico_Contagemresultado_responsavel_N = 0;
         return  ;
      }

      public bool gxTv_SdtSolicitacaoServico_Contagemresultado_responsavel_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_LiqLogCod_N" )]
      [  XmlElement( ElementName = "ContagemResultado_LiqLogCod_N"   )]
      public short gxTpr_Contagemresultado_liqlogcod_N
      {
         get {
            return gxTv_SdtSolicitacaoServico_Contagemresultado_liqlogcod_N ;
         }

         set {
            gxTv_SdtSolicitacaoServico_Contagemresultado_liqlogcod_N = (short)(value);
         }

      }

      public void gxTv_SdtSolicitacaoServico_Contagemresultado_liqlogcod_N_SetNull( )
      {
         gxTv_SdtSolicitacaoServico_Contagemresultado_liqlogcod_N = 0;
         return  ;
      }

      public bool gxTv_SdtSolicitacaoServico_Contagemresultado_liqlogcod_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Modulo_Codigo_N" )]
      [  XmlElement( ElementName = "Modulo_Codigo_N"   )]
      public short gxTpr_Modulo_codigo_N
      {
         get {
            return gxTv_SdtSolicitacaoServico_Modulo_codigo_N ;
         }

         set {
            gxTv_SdtSolicitacaoServico_Modulo_codigo_N = (short)(value);
         }

      }

      public void gxTv_SdtSolicitacaoServico_Modulo_codigo_N_SetNull( )
      {
         gxTv_SdtSolicitacaoServico_Modulo_codigo_N = 0;
         return  ;
      }

      public bool gxTv_SdtSolicitacaoServico_Modulo_codigo_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_ContratadaCod_N" )]
      [  XmlElement( ElementName = "ContagemResultado_ContratadaCod_N"   )]
      public short gxTpr_Contagemresultado_contratadacod_N
      {
         get {
            return gxTv_SdtSolicitacaoServico_Contagemresultado_contratadacod_N ;
         }

         set {
            gxTv_SdtSolicitacaoServico_Contagemresultado_contratadacod_N = (short)(value);
         }

      }

      public void gxTv_SdtSolicitacaoServico_Contagemresultado_contratadacod_N_SetNull( )
      {
         gxTv_SdtSolicitacaoServico_Contagemresultado_contratadacod_N = 0;
         return  ;
      }

      public bool gxTv_SdtSolicitacaoServico_Contagemresultado_contratadacod_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_SistemaCod_N" )]
      [  XmlElement( ElementName = "ContagemResultado_SistemaCod_N"   )]
      public short gxTpr_Contagemresultado_sistemacod_N
      {
         get {
            return gxTv_SdtSolicitacaoServico_Contagemresultado_sistemacod_N ;
         }

         set {
            gxTv_SdtSolicitacaoServico_Contagemresultado_sistemacod_N = (short)(value);
         }

      }

      public void gxTv_SdtSolicitacaoServico_Contagemresultado_sistemacod_N_SetNull( )
      {
         gxTv_SdtSolicitacaoServico_Contagemresultado_sistemacod_N = 0;
         return  ;
      }

      public bool gxTv_SdtSolicitacaoServico_Contagemresultado_sistemacod_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_FncUsrCod_N" )]
      [  XmlElement( ElementName = "ContagemResultado_FncUsrCod_N"   )]
      public short gxTpr_Contagemresultado_fncusrcod_N
      {
         get {
            return gxTv_SdtSolicitacaoServico_Contagemresultado_fncusrcod_N ;
         }

         set {
            gxTv_SdtSolicitacaoServico_Contagemresultado_fncusrcod_N = (short)(value);
         }

      }

      public void gxTv_SdtSolicitacaoServico_Contagemresultado_fncusrcod_N_SetNull( )
      {
         gxTv_SdtSolicitacaoServico_Contagemresultado_fncusrcod_N = 0;
         return  ;
      }

      public bool gxTv_SdtSolicitacaoServico_Contagemresultado_fncusrcod_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_PrazoInicialDias_N" )]
      [  XmlElement( ElementName = "ContagemResultado_PrazoInicialDias_N"   )]
      public short gxTpr_Contagemresultado_prazoinicialdias_N
      {
         get {
            return gxTv_SdtSolicitacaoServico_Contagemresultado_prazoinicialdias_N ;
         }

         set {
            gxTv_SdtSolicitacaoServico_Contagemresultado_prazoinicialdias_N = (short)(value);
         }

      }

      public void gxTv_SdtSolicitacaoServico_Contagemresultado_prazoinicialdias_N_SetNull( )
      {
         gxTv_SdtSolicitacaoServico_Contagemresultado_prazoinicialdias_N = 0;
         return  ;
      }

      public bool gxTv_SdtSolicitacaoServico_Contagemresultado_prazoinicialdias_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_DataCadastro_N" )]
      [  XmlElement( ElementName = "ContagemResultado_DataCadastro_N"   )]
      public short gxTpr_Contagemresultado_datacadastro_N
      {
         get {
            return gxTv_SdtSolicitacaoServico_Contagemresultado_datacadastro_N ;
         }

         set {
            gxTv_SdtSolicitacaoServico_Contagemresultado_datacadastro_N = (short)(value);
         }

      }

      public void gxTv_SdtSolicitacaoServico_Contagemresultado_datacadastro_N_SetNull( )
      {
         gxTv_SdtSolicitacaoServico_Contagemresultado_datacadastro_N = 0;
         return  ;
      }

      public bool gxTv_SdtSolicitacaoServico_Contagemresultado_datacadastro_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ContagemResultado_OSManual_N" )]
      [  XmlElement( ElementName = "ContagemResultado_OSManual_N"   )]
      public short gxTpr_Contagemresultado_osmanual_N
      {
         get {
            return gxTv_SdtSolicitacaoServico_Contagemresultado_osmanual_N ;
         }

         set {
            gxTv_SdtSolicitacaoServico_Contagemresultado_osmanual_N = (short)(value);
         }

      }

      public void gxTv_SdtSolicitacaoServico_Contagemresultado_osmanual_N_SetNull( )
      {
         gxTv_SdtSolicitacaoServico_Contagemresultado_osmanual_N = 0;
         return  ;
      }

      public bool gxTv_SdtSolicitacaoServico_Contagemresultado_osmanual_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contagemresultado_baseline_N" )]
      [  XmlElement( ElementName = "Contagemresultado_baseline_N"   )]
      public short gxTpr_Contagemresultado_baseline_N
      {
         get {
            return gxTv_SdtSolicitacaoServico_Contagemresultado_baseline_N ;
         }

         set {
            gxTv_SdtSolicitacaoServico_Contagemresultado_baseline_N = (short)(value);
         }

      }

      public void gxTv_SdtSolicitacaoServico_Contagemresultado_baseline_N_SetNull( )
      {
         gxTv_SdtSolicitacaoServico_Contagemresultado_baseline_N = 0;
         return  ;
      }

      public bool gxTv_SdtSolicitacaoServico_Contagemresultado_baseline_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Contagemresultado_ehvalidacao_N" )]
      [  XmlElement( ElementName = "Contagemresultado_ehvalidacao_N"   )]
      public short gxTpr_Contagemresultado_ehvalidacao_N
      {
         get {
            return gxTv_SdtSolicitacaoServico_Contagemresultado_ehvalidacao_N ;
         }

         set {
            gxTv_SdtSolicitacaoServico_Contagemresultado_ehvalidacao_N = (short)(value);
         }

      }

      public void gxTv_SdtSolicitacaoServico_Contagemresultado_ehvalidacao_N_SetNull( )
      {
         gxTv_SdtSolicitacaoServico_Contagemresultado_ehvalidacao_N = 0;
         return  ;
      }

      public bool gxTv_SdtSolicitacaoServico_Contagemresultado_ehvalidacao_N_IsNull( )
      {
         return false ;
      }

      public void initialize( )
      {
         gxTv_SdtSolicitacaoServico_Contagemresultado_descricao = "";
         gxTv_SdtSolicitacaoServico_Contagemresultado_observacao = "";
         gxTv_SdtSolicitacaoServico_Contagemresultado_dataentrega = DateTime.MinValue;
         gxTv_SdtSolicitacaoServico_Contagemresultado_horaentrega = (DateTime)(DateTime.MinValue);
         gxTv_SdtSolicitacaoServico_Contagemresultado_dataprevista = (DateTime)(DateTime.MinValue);
         gxTv_SdtSolicitacaoServico_Contagemresultado_demandafm = "";
         gxTv_SdtSolicitacaoServico_Contagemresultado_referencia = "";
         gxTv_SdtSolicitacaoServico_Contagemresultado_restricoes = "";
         gxTv_SdtSolicitacaoServico_Contagemresultado_prioridadeprevista = "";
         gxTv_SdtSolicitacaoServico_Contagemresultado_statusdmn = "";
         gxTv_SdtSolicitacaoServico_Contagemresultado_datadmn = DateTime.MinValue;
         gxTv_SdtSolicitacaoServico_Contagemresultado_datacadastro = DateTimeUtil.ServerNow( (IGxContext)(context), "DEFAULT");
         gxTv_SdtSolicitacaoServico_Mode = "";
         gxTv_SdtSolicitacaoServico_Contagemresultado_descricao_Z = "";
         gxTv_SdtSolicitacaoServico_Contagemresultado_dataentrega_Z = DateTime.MinValue;
         gxTv_SdtSolicitacaoServico_Contagemresultado_horaentrega_Z = (DateTime)(DateTime.MinValue);
         gxTv_SdtSolicitacaoServico_Contagemresultado_dataprevista_Z = (DateTime)(DateTime.MinValue);
         gxTv_SdtSolicitacaoServico_Contagemresultado_demandafm_Z = "";
         gxTv_SdtSolicitacaoServico_Contagemresultado_referencia_Z = "";
         gxTv_SdtSolicitacaoServico_Contagemresultado_restricoes_Z = "";
         gxTv_SdtSolicitacaoServico_Contagemresultado_prioridadeprevista_Z = "";
         gxTv_SdtSolicitacaoServico_Contagemresultado_statusdmn_Z = "";
         gxTv_SdtSolicitacaoServico_Contagemresultado_datadmn_Z = DateTime.MinValue;
         gxTv_SdtSolicitacaoServico_Contagemresultado_datacadastro_Z = (DateTime)(DateTime.MinValue);
         gxTv_SdtSolicitacaoServico_Contagemresultado_ss = 0;
         gxTv_SdtSolicitacaoServico_Contagemresultado_evento = 1;
         gxTv_SdtSolicitacaoServico_Contagemresultado_tiporegistro = 1;
         sTagName = "";
         sDateCnv = "";
         sNumToPad = "";
         datetime_STZ = (DateTime)(DateTime.MinValue);
         IGxSilentTrn obj ;
         obj = (IGxSilentTrn)ClassLoader.FindInstance( "solicitacaoservico", "GeneXus.Programs.solicitacaoservico_bc", new Object[] {context}, constructorCallingAssembly);
         obj.initialize();
         obj.SetSDT(this, 1);
         setTransaction( obj) ;
         obj.SetMode("INS");
         return  ;
      }

      private short gxTv_SdtSolicitacaoServico_Contagemresultado_evento ;
      private short gxTv_SdtSolicitacaoServico_Contagemresultado_tiporegistro ;
      private short gxTv_SdtSolicitacaoServico_Contagemresultado_prazoinicialdias ;
      private short gxTv_SdtSolicitacaoServico_Initialized ;
      private short gxTv_SdtSolicitacaoServico_Contagemresultado_evento_Z ;
      private short gxTv_SdtSolicitacaoServico_Contagemresultado_tiporegistro_Z ;
      private short gxTv_SdtSolicitacaoServico_Contagemresultado_prazoinicialdias_Z ;
      private short gxTv_SdtSolicitacaoServico_Contagemresultado_descricao_N ;
      private short gxTv_SdtSolicitacaoServico_Contagemresultado_observacao_N ;
      private short gxTv_SdtSolicitacaoServico_Contagemresultado_dataentrega_N ;
      private short gxTv_SdtSolicitacaoServico_Contagemresultado_horaentrega_N ;
      private short gxTv_SdtSolicitacaoServico_Contagemresultado_dataprevista_N ;
      private short gxTv_SdtSolicitacaoServico_Contagemresultado_servicoss_N ;
      private short gxTv_SdtSolicitacaoServico_Contagemresultado_ss_N ;
      private short gxTv_SdtSolicitacaoServico_Contagemresultado_demandafm_N ;
      private short gxTv_SdtSolicitacaoServico_Contagemresultado_uoowner_N ;
      private short gxTv_SdtSolicitacaoServico_Contagemresultado_evento_N ;
      private short gxTv_SdtSolicitacaoServico_Contagemresultado_referencia_N ;
      private short gxTv_SdtSolicitacaoServico_Contagemresultado_restricoes_N ;
      private short gxTv_SdtSolicitacaoServico_Contagemresultado_prioridadeprevista_N ;
      private short gxTv_SdtSolicitacaoServico_Contagemresultado_statusdmn_N ;
      private short gxTv_SdtSolicitacaoServico_Contagemresultado_cntsrvcod_N ;
      private short gxTv_SdtSolicitacaoServico_Contagemresultado_osvinculada_N ;
      private short gxTv_SdtSolicitacaoServico_Contagemresultado_naocnfdmncod_N ;
      private short gxTv_SdtSolicitacaoServico_Contagemresultado_loteaceitecod_N ;
      private short gxTv_SdtSolicitacaoServico_Contagemresultado_contratadaorigemcod_N ;
      private short gxTv_SdtSolicitacaoServico_Contagemresultado_contadorfscod_N ;
      private short gxTv_SdtSolicitacaoServico_Contagemresultado_responsavel_N ;
      private short gxTv_SdtSolicitacaoServico_Contagemresultado_liqlogcod_N ;
      private short gxTv_SdtSolicitacaoServico_Modulo_codigo_N ;
      private short gxTv_SdtSolicitacaoServico_Contagemresultado_contratadacod_N ;
      private short gxTv_SdtSolicitacaoServico_Contagemresultado_sistemacod_N ;
      private short gxTv_SdtSolicitacaoServico_Contagemresultado_fncusrcod_N ;
      private short gxTv_SdtSolicitacaoServico_Contagemresultado_prazoinicialdias_N ;
      private short gxTv_SdtSolicitacaoServico_Contagemresultado_datacadastro_N ;
      private short gxTv_SdtSolicitacaoServico_Contagemresultado_osmanual_N ;
      private short gxTv_SdtSolicitacaoServico_Contagemresultado_baseline_N ;
      private short gxTv_SdtSolicitacaoServico_Contagemresultado_ehvalidacao_N ;
      private short readOk ;
      private short nOutParmCount ;
      private int gxTv_SdtSolicitacaoServico_Contagemresultado_codigo ;
      private int gxTv_SdtSolicitacaoServico_Contagemresultado_owner ;
      private int gxTv_SdtSolicitacaoServico_Contagemresultado_servicoss ;
      private int gxTv_SdtSolicitacaoServico_Contagemresultado_ss ;
      private int gxTv_SdtSolicitacaoServico_Contagemresultado_uoowner ;
      private int gxTv_SdtSolicitacaoServico_Contagemresultado_cntsrvcod ;
      private int gxTv_SdtSolicitacaoServico_Contagemresultado_osvinculada ;
      private int gxTv_SdtSolicitacaoServico_Contagemresultado_naocnfdmncod ;
      private int gxTv_SdtSolicitacaoServico_Contagemresultado_loteaceitecod ;
      private int gxTv_SdtSolicitacaoServico_Contagemresultado_contratadaorigemcod ;
      private int gxTv_SdtSolicitacaoServico_Contagemresultado_contadorfscod ;
      private int gxTv_SdtSolicitacaoServico_Contagemresultado_responsavel ;
      private int gxTv_SdtSolicitacaoServico_Contagemresultado_liqlogcod ;
      private int gxTv_SdtSolicitacaoServico_Modulo_codigo ;
      private int gxTv_SdtSolicitacaoServico_Contagemresultado_contratadacod ;
      private int gxTv_SdtSolicitacaoServico_Contagemresultado_sistemacod ;
      private int gxTv_SdtSolicitacaoServico_Contagemresultado_fncusrcod ;
      private int gxTv_SdtSolicitacaoServico_Contagemresultado_codigo_Z ;
      private int gxTv_SdtSolicitacaoServico_Contagemresultado_owner_Z ;
      private int gxTv_SdtSolicitacaoServico_Contagemresultado_servicoss_Z ;
      private int gxTv_SdtSolicitacaoServico_Contagemresultado_ss_Z ;
      private int gxTv_SdtSolicitacaoServico_Contagemresultado_uoowner_Z ;
      private int gxTv_SdtSolicitacaoServico_Contagemresultado_cntsrvcod_Z ;
      private int gxTv_SdtSolicitacaoServico_Contagemresultado_osvinculada_Z ;
      private int gxTv_SdtSolicitacaoServico_Contagemresultado_naocnfdmncod_Z ;
      private int gxTv_SdtSolicitacaoServico_Contagemresultado_loteaceitecod_Z ;
      private int gxTv_SdtSolicitacaoServico_Contagemresultado_contratadaorigemcod_Z ;
      private int gxTv_SdtSolicitacaoServico_Contagemresultado_contadorfscod_Z ;
      private int gxTv_SdtSolicitacaoServico_Contagemresultado_responsavel_Z ;
      private int gxTv_SdtSolicitacaoServico_Contagemresultado_liqlogcod_Z ;
      private int gxTv_SdtSolicitacaoServico_Modulo_codigo_Z ;
      private int gxTv_SdtSolicitacaoServico_Contagemresultado_contratadacod_Z ;
      private int gxTv_SdtSolicitacaoServico_Contagemresultado_sistemacod_Z ;
      private int gxTv_SdtSolicitacaoServico_Contagemresultado_fncusrcod_Z ;
      private String gxTv_SdtSolicitacaoServico_Contagemresultado_statusdmn ;
      private String gxTv_SdtSolicitacaoServico_Mode ;
      private String gxTv_SdtSolicitacaoServico_Contagemresultado_statusdmn_Z ;
      private String sTagName ;
      private String sDateCnv ;
      private String sNumToPad ;
      private DateTime gxTv_SdtSolicitacaoServico_Contagemresultado_horaentrega ;
      private DateTime gxTv_SdtSolicitacaoServico_Contagemresultado_dataprevista ;
      private DateTime gxTv_SdtSolicitacaoServico_Contagemresultado_datacadastro ;
      private DateTime gxTv_SdtSolicitacaoServico_Contagemresultado_horaentrega_Z ;
      private DateTime gxTv_SdtSolicitacaoServico_Contagemresultado_dataprevista_Z ;
      private DateTime gxTv_SdtSolicitacaoServico_Contagemresultado_datacadastro_Z ;
      private DateTime datetime_STZ ;
      private DateTime gxTv_SdtSolicitacaoServico_Contagemresultado_dataentrega ;
      private DateTime gxTv_SdtSolicitacaoServico_Contagemresultado_datadmn ;
      private DateTime gxTv_SdtSolicitacaoServico_Contagemresultado_dataentrega_Z ;
      private DateTime gxTv_SdtSolicitacaoServico_Contagemresultado_datadmn_Z ;
      private bool gxTv_SdtSolicitacaoServico_Contagemresultado_osmanual ;
      private bool gxTv_SdtSolicitacaoServico_Contagemresultado_baseline ;
      private bool gxTv_SdtSolicitacaoServico_Contagemresultado_ehvalidacao ;
      private bool gxTv_SdtSolicitacaoServico_Contagemresultado_osmanual_Z ;
      private bool gxTv_SdtSolicitacaoServico_Contagemresultado_baseline_Z ;
      private bool gxTv_SdtSolicitacaoServico_Contagemresultado_ehvalidacao_Z ;
      private String gxTv_SdtSolicitacaoServico_Contagemresultado_observacao ;
      private String gxTv_SdtSolicitacaoServico_Contagemresultado_descricao ;
      private String gxTv_SdtSolicitacaoServico_Contagemresultado_demandafm ;
      private String gxTv_SdtSolicitacaoServico_Contagemresultado_referencia ;
      private String gxTv_SdtSolicitacaoServico_Contagemresultado_restricoes ;
      private String gxTv_SdtSolicitacaoServico_Contagemresultado_prioridadeprevista ;
      private String gxTv_SdtSolicitacaoServico_Contagemresultado_descricao_Z ;
      private String gxTv_SdtSolicitacaoServico_Contagemresultado_demandafm_Z ;
      private String gxTv_SdtSolicitacaoServico_Contagemresultado_referencia_Z ;
      private String gxTv_SdtSolicitacaoServico_Contagemresultado_restricoes_Z ;
      private String gxTv_SdtSolicitacaoServico_Contagemresultado_prioridadeprevista_Z ;
      private Assembly constructorCallingAssembly ;
   }

   [DataContract(Name = @"SolicitacaoServico", Namespace = "GxEv3Up14_MeetrikaVs3")]
   public class SdtSolicitacaoServico_RESTInterface : GxGenericCollectionItem<SdtSolicitacaoServico>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtSolicitacaoServico_RESTInterface( ) : base()
      {
      }

      public SdtSolicitacaoServico_RESTInterface( SdtSolicitacaoServico psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "ContagemResultado_Codigo" , Order = 0 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Contagemresultado_codigo
      {
         get {
            return sdt.gxTpr_Contagemresultado_codigo ;
         }

         set {
            sdt.gxTpr_Contagemresultado_codigo = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContagemResultado_Owner" , Order = 1 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Contagemresultado_owner
      {
         get {
            return sdt.gxTpr_Contagemresultado_owner ;
         }

         set {
            sdt.gxTpr_Contagemresultado_owner = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContagemResultado_Descricao" , Order = 2 )]
      [GxSeudo()]
      public String gxTpr_Contagemresultado_descricao
      {
         get {
            return sdt.gxTpr_Contagemresultado_descricao ;
         }

         set {
            sdt.gxTpr_Contagemresultado_descricao = (String)(value);
         }

      }

      [DataMember( Name = "ContagemResultado_Observacao" , Order = 3 )]
      public String gxTpr_Contagemresultado_observacao
      {
         get {
            return sdt.gxTpr_Contagemresultado_observacao ;
         }

         set {
            sdt.gxTpr_Contagemresultado_observacao = (String)(value);
         }

      }

      [DataMember( Name = "ContagemResultado_DataEntrega" , Order = 4 )]
      [GxSeudo()]
      public String gxTpr_Contagemresultado_dataentrega
      {
         get {
            return DateTimeUtil.DToC2( sdt.gxTpr_Contagemresultado_dataentrega) ;
         }

         set {
            sdt.gxTpr_Contagemresultado_dataentrega = DateTimeUtil.CToD2( (String)(value));
         }

      }

      [DataMember( Name = "ContagemResultado_HoraEntrega" , Order = 5 )]
      [GxSeudo()]
      public String gxTpr_Contagemresultado_horaentrega
      {
         get {
            return DateTimeUtil.TToC2( sdt.gxTpr_Contagemresultado_horaentrega) ;
         }

         set {
            GXt_dtime1 = DateTimeUtil.ResetDate(DateTimeUtil.CToT2( (String)(value)));
            sdt.gxTpr_Contagemresultado_horaentrega = GXt_dtime1;
         }

      }

      [DataMember( Name = "ContagemResultado_DataPrevista" , Order = 6 )]
      [GxSeudo()]
      public String gxTpr_Contagemresultado_dataprevista
      {
         get {
            return DateTimeUtil.TToC2( sdt.gxTpr_Contagemresultado_dataprevista) ;
         }

         set {
            sdt.gxTpr_Contagemresultado_dataprevista = DateTimeUtil.CToT2( (String)(value));
         }

      }

      [DataMember( Name = "ContagemResultado_ServicoSS" , Order = 7 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Contagemresultado_servicoss
      {
         get {
            return sdt.gxTpr_Contagemresultado_servicoss ;
         }

         set {
            sdt.gxTpr_Contagemresultado_servicoss = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContagemResultado_SS" , Order = 8 )]
      [GxSeudo()]
      public String gxTpr_Contagemresultado_ss
      {
         get {
            return StringUtil.LTrim( StringUtil.Str( (decimal)(sdt.gxTpr_Contagemresultado_ss), 8, 0)) ;
         }

         set {
            sdt.gxTpr_Contagemresultado_ss = (int)(NumberUtil.Val( (String)(value), "."));
         }

      }

      [DataMember( Name = "ContagemResultado_DemandaFM" , Order = 9 )]
      [GxSeudo()]
      public String gxTpr_Contagemresultado_demandafm
      {
         get {
            return sdt.gxTpr_Contagemresultado_demandafm ;
         }

         set {
            sdt.gxTpr_Contagemresultado_demandafm = (String)(value);
         }

      }

      [DataMember( Name = "ContagemResultado_UOOwner" , Order = 10 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Contagemresultado_uoowner
      {
         get {
            return sdt.gxTpr_Contagemresultado_uoowner ;
         }

         set {
            sdt.gxTpr_Contagemresultado_uoowner = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContagemResultado_Evento" , Order = 11 )]
      [GxSeudo()]
      public Nullable<short> gxTpr_Contagemresultado_evento
      {
         get {
            return sdt.gxTpr_Contagemresultado_evento ;
         }

         set {
            sdt.gxTpr_Contagemresultado_evento = (short)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContagemResultado_TipoRegistro" , Order = 12 )]
      [GxSeudo()]
      public Nullable<short> gxTpr_Contagemresultado_tiporegistro
      {
         get {
            return sdt.gxTpr_Contagemresultado_tiporegistro ;
         }

         set {
            sdt.gxTpr_Contagemresultado_tiporegistro = (short)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContagemResultado_Referencia" , Order = 13 )]
      [GxSeudo()]
      public String gxTpr_Contagemresultado_referencia
      {
         get {
            return sdt.gxTpr_Contagemresultado_referencia ;
         }

         set {
            sdt.gxTpr_Contagemresultado_referencia = (String)(value);
         }

      }

      [DataMember( Name = "ContagemResultado_Restricoes" , Order = 14 )]
      [GxSeudo()]
      public String gxTpr_Contagemresultado_restricoes
      {
         get {
            return sdt.gxTpr_Contagemresultado_restricoes ;
         }

         set {
            sdt.gxTpr_Contagemresultado_restricoes = (String)(value);
         }

      }

      [DataMember( Name = "ContagemResultado_PrioridadePrevista" , Order = 15 )]
      [GxSeudo()]
      public String gxTpr_Contagemresultado_prioridadeprevista
      {
         get {
            return sdt.gxTpr_Contagemresultado_prioridadeprevista ;
         }

         set {
            sdt.gxTpr_Contagemresultado_prioridadeprevista = (String)(value);
         }

      }

      [DataMember( Name = "ContagemResultado_StatusDmn" , Order = 16 )]
      [GxSeudo()]
      public String gxTpr_Contagemresultado_statusdmn
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Contagemresultado_statusdmn) ;
         }

         set {
            sdt.gxTpr_Contagemresultado_statusdmn = (String)(value);
         }

      }

      [DataMember( Name = "ContagemResultado_DataDmn" , Order = 17 )]
      [GxSeudo()]
      public String gxTpr_Contagemresultado_datadmn
      {
         get {
            return DateTimeUtil.DToC2( sdt.gxTpr_Contagemresultado_datadmn) ;
         }

         set {
            sdt.gxTpr_Contagemresultado_datadmn = DateTimeUtil.CToD2( (String)(value));
         }

      }

      [DataMember( Name = "ContagemResultado_CntSrvCod" , Order = 18 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Contagemresultado_cntsrvcod
      {
         get {
            return sdt.gxTpr_Contagemresultado_cntsrvcod ;
         }

         set {
            sdt.gxTpr_Contagemresultado_cntsrvcod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContagemResultado_OSVinculada" , Order = 19 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Contagemresultado_osvinculada
      {
         get {
            return sdt.gxTpr_Contagemresultado_osvinculada ;
         }

         set {
            sdt.gxTpr_Contagemresultado_osvinculada = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContagemResultado_NaoCnfDmnCod" , Order = 20 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Contagemresultado_naocnfdmncod
      {
         get {
            return sdt.gxTpr_Contagemresultado_naocnfdmncod ;
         }

         set {
            sdt.gxTpr_Contagemresultado_naocnfdmncod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContagemResultado_LoteAceiteCod" , Order = 21 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Contagemresultado_loteaceitecod
      {
         get {
            return sdt.gxTpr_Contagemresultado_loteaceitecod ;
         }

         set {
            sdt.gxTpr_Contagemresultado_loteaceitecod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContagemResultado_ContratadaOrigemCod" , Order = 22 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Contagemresultado_contratadaorigemcod
      {
         get {
            return sdt.gxTpr_Contagemresultado_contratadaorigemcod ;
         }

         set {
            sdt.gxTpr_Contagemresultado_contratadaorigemcod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContagemResultado_ContadorFSCod" , Order = 23 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Contagemresultado_contadorfscod
      {
         get {
            return sdt.gxTpr_Contagemresultado_contadorfscod ;
         }

         set {
            sdt.gxTpr_Contagemresultado_contadorfscod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContagemResultado_Responsavel" , Order = 24 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Contagemresultado_responsavel
      {
         get {
            return sdt.gxTpr_Contagemresultado_responsavel ;
         }

         set {
            sdt.gxTpr_Contagemresultado_responsavel = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContagemResultado_LiqLogCod" , Order = 25 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Contagemresultado_liqlogcod
      {
         get {
            return sdt.gxTpr_Contagemresultado_liqlogcod ;
         }

         set {
            sdt.gxTpr_Contagemresultado_liqlogcod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Modulo_Codigo" , Order = 26 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Modulo_codigo
      {
         get {
            return sdt.gxTpr_Modulo_codigo ;
         }

         set {
            sdt.gxTpr_Modulo_codigo = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContagemResultado_ContratadaCod" , Order = 27 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Contagemresultado_contratadacod
      {
         get {
            return sdt.gxTpr_Contagemresultado_contratadacod ;
         }

         set {
            sdt.gxTpr_Contagemresultado_contratadacod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContagemResultado_SistemaCod" , Order = 28 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Contagemresultado_sistemacod
      {
         get {
            return sdt.gxTpr_Contagemresultado_sistemacod ;
         }

         set {
            sdt.gxTpr_Contagemresultado_sistemacod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContagemResultado_FncUsrCod" , Order = 29 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Contagemresultado_fncusrcod
      {
         get {
            return sdt.gxTpr_Contagemresultado_fncusrcod ;
         }

         set {
            sdt.gxTpr_Contagemresultado_fncusrcod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContagemResultado_PrazoInicialDias" , Order = 30 )]
      [GxSeudo()]
      public Nullable<short> gxTpr_Contagemresultado_prazoinicialdias
      {
         get {
            return sdt.gxTpr_Contagemresultado_prazoinicialdias ;
         }

         set {
            sdt.gxTpr_Contagemresultado_prazoinicialdias = (short)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ContagemResultado_DataCadastro" , Order = 31 )]
      [GxSeudo()]
      public String gxTpr_Contagemresultado_datacadastro
      {
         get {
            return DateTimeUtil.TToC2( sdt.gxTpr_Contagemresultado_datacadastro) ;
         }

         set {
            sdt.gxTpr_Contagemresultado_datacadastro = DateTimeUtil.CToT2( (String)(value));
         }

      }

      [DataMember( Name = "ContagemResultado_OSManual" , Order = 32 )]
      [GxSeudo()]
      public bool gxTpr_Contagemresultado_osmanual
      {
         get {
            return sdt.gxTpr_Contagemresultado_osmanual ;
         }

         set {
            sdt.gxTpr_Contagemresultado_osmanual = value;
         }

      }

      [DataMember( Name = "Contagemresultado_baseline" , Order = 33 )]
      [GxSeudo()]
      public bool gxTpr_Contagemresultado_baseline
      {
         get {
            return sdt.gxTpr_Contagemresultado_baseline ;
         }

         set {
            sdt.gxTpr_Contagemresultado_baseline = value;
         }

      }

      [DataMember( Name = "Contagemresultado_ehvalidacao" , Order = 34 )]
      [GxSeudo()]
      public bool gxTpr_Contagemresultado_ehvalidacao
      {
         get {
            return sdt.gxTpr_Contagemresultado_ehvalidacao ;
         }

         set {
            sdt.gxTpr_Contagemresultado_ehvalidacao = value;
         }

      }

      public SdtSolicitacaoServico sdt
      {
         get {
            return (SdtSolicitacaoServico)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtSolicitacaoServico() ;
         }
      }

      [DataMember( Name = "gx_md5_hash", Order = 102 )]
      public string Hash
      {
         get {
            if ( StringUtil.StrCmp(md5Hash, null) == 0 )
            {
               md5Hash = (String)(getHash());
            }
            return md5Hash ;
         }

         set {
            md5Hash = value ;
         }

      }

      private String md5Hash ;
      private DateTime GXt_dtime1 ;
   }

}
