/*
               File: ContratoAutorizacaoConsumoWC
        Description: Contrato Autorizacao Consumo WC
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/18/2020 12:55:40.19
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class contratoautorizacaoconsumowc : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public contratoautorizacaoconsumowc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public contratoautorizacaoconsumowc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Contrato_Codigo )
      {
         this.AV7Contrato_Codigo = aP0_Contrato_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
         cmbAutorizacaoConsumo_Status = new GXCombobox();
         chkAutorizacaoConsumo_Ativo = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  AV7Contrato_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Contrato_Codigo), 6, 0)));
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(int)AV7Contrato_Codigo});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
               {
                  nRC_GXsfl_19 = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  nGXsfl_19_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  sGXsfl_19_idx = GetNextPar( );
                  sPrefix = GetNextPar( );
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxnrGrid_newrow( ) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
               {
                  subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  AV13OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
                  AV14OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
                  AV23TFAutorizacaoConsumo_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23TFAutorizacaoConsumo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV23TFAutorizacaoConsumo_Codigo), 6, 0)));
                  AV24TFAutorizacaoConsumo_Codigo_To = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24TFAutorizacaoConsumo_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24TFAutorizacaoConsumo_Codigo_To), 6, 0)));
                  AV27TFAutorizacaoConsumo_VigenciaInicio = context.localUtil.ParseDateParm( GetNextPar( ));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27TFAutorizacaoConsumo_VigenciaInicio", context.localUtil.Format(AV27TFAutorizacaoConsumo_VigenciaInicio, "99/99/99"));
                  AV28TFAutorizacaoConsumo_VigenciaInicio_To = context.localUtil.ParseDateParm( GetNextPar( ));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV28TFAutorizacaoConsumo_VigenciaInicio_To", context.localUtil.Format(AV28TFAutorizacaoConsumo_VigenciaInicio_To, "99/99/99"));
                  AV33TFAutorizacaoConsumo_VigenciaFim = context.localUtil.ParseDateParm( GetNextPar( ));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV33TFAutorizacaoConsumo_VigenciaFim", context.localUtil.Format(AV33TFAutorizacaoConsumo_VigenciaFim, "99/99/99"));
                  AV34TFAutorizacaoConsumo_VigenciaFim_To = context.localUtil.ParseDateParm( GetNextPar( ));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV34TFAutorizacaoConsumo_VigenciaFim_To", context.localUtil.Format(AV34TFAutorizacaoConsumo_VigenciaFim_To, "99/99/99"));
                  AV39TFAutorizacaoConsumo_UnidadeMedicaoNom = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV39TFAutorizacaoConsumo_UnidadeMedicaoNom", AV39TFAutorizacaoConsumo_UnidadeMedicaoNom);
                  AV40TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV40TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel", AV40TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel);
                  AV43TFAutorizacaoConsumo_Quantidade = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV43TFAutorizacaoConsumo_Quantidade", StringUtil.LTrim( StringUtil.Str( (decimal)(AV43TFAutorizacaoConsumo_Quantidade), 3, 0)));
                  AV44TFAutorizacaoConsumo_Quantidade_To = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV44TFAutorizacaoConsumo_Quantidade_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV44TFAutorizacaoConsumo_Quantidade_To), 3, 0)));
                  AV47TFAutorizacaoConsumo_Valor = NumberUtil.Val( GetNextPar( ), ".");
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV47TFAutorizacaoConsumo_Valor", StringUtil.LTrim( StringUtil.Str( AV47TFAutorizacaoConsumo_Valor, 18, 5)));
                  AV48TFAutorizacaoConsumo_Valor_To = NumberUtil.Val( GetNextPar( ), ".");
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV48TFAutorizacaoConsumo_Valor_To", StringUtil.LTrim( StringUtil.Str( AV48TFAutorizacaoConsumo_Valor_To, 18, 5)));
                  AV55TFAutorizacaoConsumo_Ativo_Sel = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV55TFAutorizacaoConsumo_Ativo_Sel", StringUtil.Str( (decimal)(AV55TFAutorizacaoConsumo_Ativo_Sel), 1, 0));
                  AV7Contrato_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Contrato_Codigo), 6, 0)));
                  AV25ddo_AutorizacaoConsumo_CodigoTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25ddo_AutorizacaoConsumo_CodigoTitleControlIdToReplace", AV25ddo_AutorizacaoConsumo_CodigoTitleControlIdToReplace);
                  AV31ddo_AutorizacaoConsumo_VigenciaInicioTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV31ddo_AutorizacaoConsumo_VigenciaInicioTitleControlIdToReplace", AV31ddo_AutorizacaoConsumo_VigenciaInicioTitleControlIdToReplace);
                  AV37ddo_AutorizacaoConsumo_VigenciaFimTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV37ddo_AutorizacaoConsumo_VigenciaFimTitleControlIdToReplace", AV37ddo_AutorizacaoConsumo_VigenciaFimTitleControlIdToReplace);
                  AV41ddo_AutorizacaoConsumo_UnidadeMedicaoNomTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV41ddo_AutorizacaoConsumo_UnidadeMedicaoNomTitleControlIdToReplace", AV41ddo_AutorizacaoConsumo_UnidadeMedicaoNomTitleControlIdToReplace);
                  AV45ddo_AutorizacaoConsumo_QuantidadeTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV45ddo_AutorizacaoConsumo_QuantidadeTitleControlIdToReplace", AV45ddo_AutorizacaoConsumo_QuantidadeTitleControlIdToReplace);
                  AV49ddo_AutorizacaoConsumo_ValorTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV49ddo_AutorizacaoConsumo_ValorTitleControlIdToReplace", AV49ddo_AutorizacaoConsumo_ValorTitleControlIdToReplace);
                  AV53ddo_AutorizacaoConsumo_StatusTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV53ddo_AutorizacaoConsumo_StatusTitleControlIdToReplace", AV53ddo_AutorizacaoConsumo_StatusTitleControlIdToReplace);
                  AV56ddo_AutorizacaoConsumo_AtivoTitleControlIdToReplace = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV56ddo_AutorizacaoConsumo_AtivoTitleControlIdToReplace", AV56ddo_AutorizacaoConsumo_AtivoTitleControlIdToReplace);
                  AV66Pgmname = GetNextPar( );
                  ajax_req_read_hidden_sdt(GetNextPar( ), AV52TFAutorizacaoConsumo_Status_Sels);
                  A1780AutorizacaoConsumo_Ativo = (bool)(BooleanUtil.Val(GetNextPar( )));
                  sPrefix = GetNextPar( );
                  init_default_properties( ) ;
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV23TFAutorizacaoConsumo_Codigo, AV24TFAutorizacaoConsumo_Codigo_To, AV27TFAutorizacaoConsumo_VigenciaInicio, AV28TFAutorizacaoConsumo_VigenciaInicio_To, AV33TFAutorizacaoConsumo_VigenciaFim, AV34TFAutorizacaoConsumo_VigenciaFim_To, AV39TFAutorizacaoConsumo_UnidadeMedicaoNom, AV40TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel, AV43TFAutorizacaoConsumo_Quantidade, AV44TFAutorizacaoConsumo_Quantidade_To, AV47TFAutorizacaoConsumo_Valor, AV48TFAutorizacaoConsumo_Valor_To, AV55TFAutorizacaoConsumo_Ativo_Sel, AV7Contrato_Codigo, AV25ddo_AutorizacaoConsumo_CodigoTitleControlIdToReplace, AV31ddo_AutorizacaoConsumo_VigenciaInicioTitleControlIdToReplace, AV37ddo_AutorizacaoConsumo_VigenciaFimTitleControlIdToReplace, AV41ddo_AutorizacaoConsumo_UnidadeMedicaoNomTitleControlIdToReplace, AV45ddo_AutorizacaoConsumo_QuantidadeTitleControlIdToReplace, AV49ddo_AutorizacaoConsumo_ValorTitleControlIdToReplace, AV53ddo_AutorizacaoConsumo_StatusTitleControlIdToReplace, AV56ddo_AutorizacaoConsumo_AtivoTitleControlIdToReplace, AV66Pgmname, AV52TFAutorizacaoConsumo_Status_Sels, A1780AutorizacaoConsumo_Ativo, sPrefix) ;
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  forbiddenHiddens = sPrefix + "hsh" + "ContratoAutorizacaoConsumoWC";
                  forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A74Contrato_Codigo), "ZZZZZ9");
                  GxWebStd.gx_hidden_field( context, sPrefix+"hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
                  GXUtil.WriteLog("contratoautorizacaoconsumowc:[SendSecurityCheck value for]"+"Contrato_Codigo:"+context.localUtil.Format( (decimal)(A74Contrato_Codigo), "ZZZZZ9"));
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
                  return  ;
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PANB2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV66Pgmname = "ContratoAutorizacaoConsumoWC";
               context.Gx_err = 0;
               WSNB2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "Contrato Autorizacao Consumo WC") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?202051812554061");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/ConfirmPanel/BootstrapConfirmPanelRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/ConfirmPanel/BootstrapConfirmPanelRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/ConfirmPanel/BootstrapConfirmPanelRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
            context.WriteHtmlText( "<body") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("contratoautorizacaoconsumowc.aspx") + "?" + UrlEncode("" +AV7Contrato_Codigo)+"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vORDEREDDSC", StringUtil.BoolToStr( AV14OrderedDsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFAUTORIZACAOCONSUMO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV23TFAutorizacaoConsumo_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFAUTORIZACAOCONSUMO_CODIGO_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV24TFAutorizacaoConsumo_Codigo_To), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFAUTORIZACAOCONSUMO_VIGENCIAINICIO", context.localUtil.Format(AV27TFAutorizacaoConsumo_VigenciaInicio, "99/99/99"));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO", context.localUtil.Format(AV28TFAutorizacaoConsumo_VigenciaInicio_To, "99/99/99"));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFAUTORIZACAOCONSUMO_VIGENCIAFIM", context.localUtil.Format(AV33TFAutorizacaoConsumo_VigenciaFim, "99/99/99"));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFAUTORIZACAOCONSUMO_VIGENCIAFIM_TO", context.localUtil.Format(AV34TFAutorizacaoConsumo_VigenciaFim_To, "99/99/99"));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFAUTORIZACAOCONSUMO_UNIDADEMEDICAONOM", StringUtil.RTrim( AV39TFAutorizacaoConsumo_UnidadeMedicaoNom));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFAUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_SEL", StringUtil.RTrim( AV40TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFAUTORIZACAOCONSUMO_QUANTIDADE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV43TFAutorizacaoConsumo_Quantidade), 3, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFAUTORIZACAOCONSUMO_QUANTIDADE_TO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV44TFAutorizacaoConsumo_Quantidade_To), 3, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFAUTORIZACAOCONSUMO_VALOR", StringUtil.LTrim( StringUtil.NToC( AV47TFAutorizacaoConsumo_Valor, 18, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFAUTORIZACAOCONSUMO_VALOR_TO", StringUtil.LTrim( StringUtil.NToC( AV48TFAutorizacaoConsumo_Valor_To, 18, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vTFAUTORIZACAOCONSUMO_ATIVO_SEL", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV55TFAutorizacaoConsumo_Ativo_Sel), 1, 0, ",", "")));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, sPrefix+"nRC_GXsfl_19", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_19), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV59GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV60GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vDDO_TITLESETTINGSICONS", AV57DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vDDO_TITLESETTINGSICONS", AV57DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vAUTORIZACAOCONSUMO_CODIGOTITLEFILTERDATA", AV22AutorizacaoConsumo_CodigoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vAUTORIZACAOCONSUMO_CODIGOTITLEFILTERDATA", AV22AutorizacaoConsumo_CodigoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vAUTORIZACAOCONSUMO_VIGENCIAINICIOTITLEFILTERDATA", AV26AutorizacaoConsumo_VigenciaInicioTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vAUTORIZACAOCONSUMO_VIGENCIAINICIOTITLEFILTERDATA", AV26AutorizacaoConsumo_VigenciaInicioTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vAUTORIZACAOCONSUMO_VIGENCIAFIMTITLEFILTERDATA", AV32AutorizacaoConsumo_VigenciaFimTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vAUTORIZACAOCONSUMO_VIGENCIAFIMTITLEFILTERDATA", AV32AutorizacaoConsumo_VigenciaFimTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vAUTORIZACAOCONSUMO_UNIDADEMEDICAONOMTITLEFILTERDATA", AV38AutorizacaoConsumo_UnidadeMedicaoNomTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vAUTORIZACAOCONSUMO_UNIDADEMEDICAONOMTITLEFILTERDATA", AV38AutorizacaoConsumo_UnidadeMedicaoNomTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vAUTORIZACAOCONSUMO_QUANTIDADETITLEFILTERDATA", AV42AutorizacaoConsumo_QuantidadeTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vAUTORIZACAOCONSUMO_QUANTIDADETITLEFILTERDATA", AV42AutorizacaoConsumo_QuantidadeTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vAUTORIZACAOCONSUMO_VALORTITLEFILTERDATA", AV46AutorizacaoConsumo_ValorTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vAUTORIZACAOCONSUMO_VALORTITLEFILTERDATA", AV46AutorizacaoConsumo_ValorTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vAUTORIZACAOCONSUMO_STATUSTITLEFILTERDATA", AV50AutorizacaoConsumo_StatusTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vAUTORIZACAOCONSUMO_STATUSTITLEFILTERDATA", AV50AutorizacaoConsumo_StatusTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vAUTORIZACAOCONSUMO_ATIVOTITLEFILTERDATA", AV54AutorizacaoConsumo_AtivoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vAUTORIZACAOCONSUMO_ATIVOTITLEFILTERDATA", AV54AutorizacaoConsumo_AtivoTitleFilterData);
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOAV7Contrato_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOAV7Contrato_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vCONTRATO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7Contrato_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vPGMNAME", StringUtil.RTrim( AV66Pgmname));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vTFAUTORIZACAOCONSUMO_STATUS_SELS", AV52TFAutorizacaoConsumo_Status_Sels);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vTFAUTORIZACAOCONSUMO_STATUS_SELS", AV52TFAutorizacaoConsumo_Status_Sels);
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DVELOP_CONFIRMPANEL_CANCELARAUTORIZCAOCONSUMO_Title", StringUtil.RTrim( Dvelop_confirmpanel_cancelarautorizcaoconsumo_Title));
         GxWebStd.gx_hidden_field( context, sPrefix+"DVELOP_CONFIRMPANEL_CANCELARAUTORIZCAOCONSUMO_Confirmationtext", StringUtil.RTrim( Dvelop_confirmpanel_cancelarautorizcaoconsumo_Confirmationtext));
         GxWebStd.gx_hidden_field( context, sPrefix+"DVELOP_CONFIRMPANEL_CANCELARAUTORIZCAOCONSUMO_Yesbuttoncaption", StringUtil.RTrim( Dvelop_confirmpanel_cancelarautorizcaoconsumo_Yesbuttoncaption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DVELOP_CONFIRMPANEL_CANCELARAUTORIZCAOCONSUMO_Nobuttoncaption", StringUtil.RTrim( Dvelop_confirmpanel_cancelarautorizcaoconsumo_Nobuttoncaption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DVELOP_CONFIRMPANEL_CANCELARAUTORIZCAOCONSUMO_Cancelbuttoncaption", StringUtil.RTrim( Dvelop_confirmpanel_cancelarautorizcaoconsumo_Cancelbuttoncaption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DVELOP_CONFIRMPANEL_CANCELARAUTORIZCAOCONSUMO_Yesbuttonposition", StringUtil.RTrim( Dvelop_confirmpanel_cancelarautorizcaoconsumo_Yesbuttonposition));
         GxWebStd.gx_hidden_field( context, sPrefix+"DVELOP_CONFIRMPANEL_CANCELARAUTORIZCAOCONSUMO_Confirmtype", StringUtil.RTrim( Dvelop_confirmpanel_cancelarautorizcaoconsumo_Confirmtype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DVELOP_CONFIRMPANEL_AUTORIZAR_Title", StringUtil.RTrim( Dvelop_confirmpanel_autorizar_Title));
         GxWebStd.gx_hidden_field( context, sPrefix+"DVELOP_CONFIRMPANEL_AUTORIZAR_Confirmationtext", StringUtil.RTrim( Dvelop_confirmpanel_autorizar_Confirmationtext));
         GxWebStd.gx_hidden_field( context, sPrefix+"DVELOP_CONFIRMPANEL_AUTORIZAR_Yesbuttoncaption", StringUtil.RTrim( Dvelop_confirmpanel_autorizar_Yesbuttoncaption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DVELOP_CONFIRMPANEL_AUTORIZAR_Nobuttoncaption", StringUtil.RTrim( Dvelop_confirmpanel_autorizar_Nobuttoncaption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DVELOP_CONFIRMPANEL_AUTORIZAR_Cancelbuttoncaption", StringUtil.RTrim( Dvelop_confirmpanel_autorizar_Cancelbuttoncaption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DVELOP_CONFIRMPANEL_AUTORIZAR_Yesbuttonposition", StringUtil.RTrim( Dvelop_confirmpanel_autorizar_Yesbuttonposition));
         GxWebStd.gx_hidden_field( context, sPrefix+"DVELOP_CONFIRMPANEL_AUTORIZAR_Confirmtype", StringUtil.RTrim( Dvelop_confirmpanel_autorizar_Confirmtype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DVELOP_CONFIRMPANEL_ENCERRAR_Title", StringUtil.RTrim( Dvelop_confirmpanel_encerrar_Title));
         GxWebStd.gx_hidden_field( context, sPrefix+"DVELOP_CONFIRMPANEL_ENCERRAR_Confirmationtext", StringUtil.RTrim( Dvelop_confirmpanel_encerrar_Confirmationtext));
         GxWebStd.gx_hidden_field( context, sPrefix+"DVELOP_CONFIRMPANEL_ENCERRAR_Yesbuttoncaption", StringUtil.RTrim( Dvelop_confirmpanel_encerrar_Yesbuttoncaption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DVELOP_CONFIRMPANEL_ENCERRAR_Nobuttoncaption", StringUtil.RTrim( Dvelop_confirmpanel_encerrar_Nobuttoncaption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DVELOP_CONFIRMPANEL_ENCERRAR_Cancelbuttoncaption", StringUtil.RTrim( Dvelop_confirmpanel_encerrar_Cancelbuttoncaption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DVELOP_CONFIRMPANEL_ENCERRAR_Yesbuttonposition", StringUtil.RTrim( Dvelop_confirmpanel_encerrar_Yesbuttonposition));
         GxWebStd.gx_hidden_field( context, sPrefix+"DVELOP_CONFIRMPANEL_ENCERRAR_Confirmtype", StringUtil.RTrim( Dvelop_confirmpanel_encerrar_Confirmtype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_CODIGO_Caption", StringUtil.RTrim( Ddo_autorizacaoconsumo_codigo_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_CODIGO_Tooltip", StringUtil.RTrim( Ddo_autorizacaoconsumo_codigo_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_CODIGO_Cls", StringUtil.RTrim( Ddo_autorizacaoconsumo_codigo_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_CODIGO_Filteredtext_set", StringUtil.RTrim( Ddo_autorizacaoconsumo_codigo_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_CODIGO_Filteredtextto_set", StringUtil.RTrim( Ddo_autorizacaoconsumo_codigo_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_CODIGO_Dropdownoptionstype", StringUtil.RTrim( Ddo_autorizacaoconsumo_codigo_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_CODIGO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_autorizacaoconsumo_codigo_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_CODIGO_Includesortasc", StringUtil.BoolToStr( Ddo_autorizacaoconsumo_codigo_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_CODIGO_Includesortdsc", StringUtil.BoolToStr( Ddo_autorizacaoconsumo_codigo_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_CODIGO_Sortedstatus", StringUtil.RTrim( Ddo_autorizacaoconsumo_codigo_Sortedstatus));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_CODIGO_Includefilter", StringUtil.BoolToStr( Ddo_autorizacaoconsumo_codigo_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_CODIGO_Filtertype", StringUtil.RTrim( Ddo_autorizacaoconsumo_codigo_Filtertype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_CODIGO_Filterisrange", StringUtil.BoolToStr( Ddo_autorizacaoconsumo_codigo_Filterisrange));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_CODIGO_Includedatalist", StringUtil.BoolToStr( Ddo_autorizacaoconsumo_codigo_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_CODIGO_Sortasc", StringUtil.RTrim( Ddo_autorizacaoconsumo_codigo_Sortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_CODIGO_Sortdsc", StringUtil.RTrim( Ddo_autorizacaoconsumo_codigo_Sortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_CODIGO_Cleanfilter", StringUtil.RTrim( Ddo_autorizacaoconsumo_codigo_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_CODIGO_Rangefilterfrom", StringUtil.RTrim( Ddo_autorizacaoconsumo_codigo_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_CODIGO_Rangefilterto", StringUtil.RTrim( Ddo_autorizacaoconsumo_codigo_Rangefilterto));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_CODIGO_Searchbuttontext", StringUtil.RTrim( Ddo_autorizacaoconsumo_codigo_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIO_Caption", StringUtil.RTrim( Ddo_autorizacaoconsumo_vigenciainicio_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIO_Tooltip", StringUtil.RTrim( Ddo_autorizacaoconsumo_vigenciainicio_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIO_Cls", StringUtil.RTrim( Ddo_autorizacaoconsumo_vigenciainicio_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIO_Filteredtext_set", StringUtil.RTrim( Ddo_autorizacaoconsumo_vigenciainicio_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIO_Filteredtextto_set", StringUtil.RTrim( Ddo_autorizacaoconsumo_vigenciainicio_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIO_Dropdownoptionstype", StringUtil.RTrim( Ddo_autorizacaoconsumo_vigenciainicio_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_autorizacaoconsumo_vigenciainicio_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIO_Includesortasc", StringUtil.BoolToStr( Ddo_autorizacaoconsumo_vigenciainicio_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIO_Includesortdsc", StringUtil.BoolToStr( Ddo_autorizacaoconsumo_vigenciainicio_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIO_Sortedstatus", StringUtil.RTrim( Ddo_autorizacaoconsumo_vigenciainicio_Sortedstatus));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIO_Includefilter", StringUtil.BoolToStr( Ddo_autorizacaoconsumo_vigenciainicio_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIO_Filtertype", StringUtil.RTrim( Ddo_autorizacaoconsumo_vigenciainicio_Filtertype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIO_Filterisrange", StringUtil.BoolToStr( Ddo_autorizacaoconsumo_vigenciainicio_Filterisrange));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIO_Includedatalist", StringUtil.BoolToStr( Ddo_autorizacaoconsumo_vigenciainicio_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIO_Sortasc", StringUtil.RTrim( Ddo_autorizacaoconsumo_vigenciainicio_Sortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIO_Sortdsc", StringUtil.RTrim( Ddo_autorizacaoconsumo_vigenciainicio_Sortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIO_Cleanfilter", StringUtil.RTrim( Ddo_autorizacaoconsumo_vigenciainicio_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIO_Rangefilterfrom", StringUtil.RTrim( Ddo_autorizacaoconsumo_vigenciainicio_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIO_Rangefilterto", StringUtil.RTrim( Ddo_autorizacaoconsumo_vigenciainicio_Rangefilterto));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIO_Searchbuttontext", StringUtil.RTrim( Ddo_autorizacaoconsumo_vigenciainicio_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_VIGENCIAFIM_Caption", StringUtil.RTrim( Ddo_autorizacaoconsumo_vigenciafim_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_VIGENCIAFIM_Tooltip", StringUtil.RTrim( Ddo_autorizacaoconsumo_vigenciafim_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_VIGENCIAFIM_Cls", StringUtil.RTrim( Ddo_autorizacaoconsumo_vigenciafim_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_VIGENCIAFIM_Filteredtext_set", StringUtil.RTrim( Ddo_autorizacaoconsumo_vigenciafim_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_VIGENCIAFIM_Filteredtextto_set", StringUtil.RTrim( Ddo_autorizacaoconsumo_vigenciafim_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_VIGENCIAFIM_Dropdownoptionstype", StringUtil.RTrim( Ddo_autorizacaoconsumo_vigenciafim_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_VIGENCIAFIM_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_autorizacaoconsumo_vigenciafim_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_VIGENCIAFIM_Includesortasc", StringUtil.BoolToStr( Ddo_autorizacaoconsumo_vigenciafim_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_VIGENCIAFIM_Includesortdsc", StringUtil.BoolToStr( Ddo_autorizacaoconsumo_vigenciafim_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_VIGENCIAFIM_Sortedstatus", StringUtil.RTrim( Ddo_autorizacaoconsumo_vigenciafim_Sortedstatus));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_VIGENCIAFIM_Includefilter", StringUtil.BoolToStr( Ddo_autorizacaoconsumo_vigenciafim_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_VIGENCIAFIM_Filtertype", StringUtil.RTrim( Ddo_autorizacaoconsumo_vigenciafim_Filtertype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_VIGENCIAFIM_Filterisrange", StringUtil.BoolToStr( Ddo_autorizacaoconsumo_vigenciafim_Filterisrange));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_VIGENCIAFIM_Includedatalist", StringUtil.BoolToStr( Ddo_autorizacaoconsumo_vigenciafim_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_VIGENCIAFIM_Sortasc", StringUtil.RTrim( Ddo_autorizacaoconsumo_vigenciafim_Sortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_VIGENCIAFIM_Sortdsc", StringUtil.RTrim( Ddo_autorizacaoconsumo_vigenciafim_Sortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_VIGENCIAFIM_Cleanfilter", StringUtil.RTrim( Ddo_autorizacaoconsumo_vigenciafim_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_VIGENCIAFIM_Rangefilterfrom", StringUtil.RTrim( Ddo_autorizacaoconsumo_vigenciafim_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_VIGENCIAFIM_Rangefilterto", StringUtil.RTrim( Ddo_autorizacaoconsumo_vigenciafim_Rangefilterto));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_VIGENCIAFIM_Searchbuttontext", StringUtil.RTrim( Ddo_autorizacaoconsumo_vigenciafim_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_Caption", StringUtil.RTrim( Ddo_autorizacaoconsumo_unidademedicaonom_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_Tooltip", StringUtil.RTrim( Ddo_autorizacaoconsumo_unidademedicaonom_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_Cls", StringUtil.RTrim( Ddo_autorizacaoconsumo_unidademedicaonom_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_Filteredtext_set", StringUtil.RTrim( Ddo_autorizacaoconsumo_unidademedicaonom_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_Selectedvalue_set", StringUtil.RTrim( Ddo_autorizacaoconsumo_unidademedicaonom_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_Dropdownoptionstype", StringUtil.RTrim( Ddo_autorizacaoconsumo_unidademedicaonom_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_autorizacaoconsumo_unidademedicaonom_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_Includesortasc", StringUtil.BoolToStr( Ddo_autorizacaoconsumo_unidademedicaonom_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_Includesortdsc", StringUtil.BoolToStr( Ddo_autorizacaoconsumo_unidademedicaonom_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_Sortedstatus", StringUtil.RTrim( Ddo_autorizacaoconsumo_unidademedicaonom_Sortedstatus));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_Includefilter", StringUtil.BoolToStr( Ddo_autorizacaoconsumo_unidademedicaonom_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_Filtertype", StringUtil.RTrim( Ddo_autorizacaoconsumo_unidademedicaonom_Filtertype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_Filterisrange", StringUtil.BoolToStr( Ddo_autorizacaoconsumo_unidademedicaonom_Filterisrange));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_Includedatalist", StringUtil.BoolToStr( Ddo_autorizacaoconsumo_unidademedicaonom_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_Datalisttype", StringUtil.RTrim( Ddo_autorizacaoconsumo_unidademedicaonom_Datalisttype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_Datalistproc", StringUtil.RTrim( Ddo_autorizacaoconsumo_unidademedicaonom_Datalistproc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_autorizacaoconsumo_unidademedicaonom_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_Sortasc", StringUtil.RTrim( Ddo_autorizacaoconsumo_unidademedicaonom_Sortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_Sortdsc", StringUtil.RTrim( Ddo_autorizacaoconsumo_unidademedicaonom_Sortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_Loadingdata", StringUtil.RTrim( Ddo_autorizacaoconsumo_unidademedicaonom_Loadingdata));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_Cleanfilter", StringUtil.RTrim( Ddo_autorizacaoconsumo_unidademedicaonom_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_Noresultsfound", StringUtil.RTrim( Ddo_autorizacaoconsumo_unidademedicaonom_Noresultsfound));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_Searchbuttontext", StringUtil.RTrim( Ddo_autorizacaoconsumo_unidademedicaonom_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_QUANTIDADE_Caption", StringUtil.RTrim( Ddo_autorizacaoconsumo_quantidade_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_QUANTIDADE_Tooltip", StringUtil.RTrim( Ddo_autorizacaoconsumo_quantidade_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_QUANTIDADE_Cls", StringUtil.RTrim( Ddo_autorizacaoconsumo_quantidade_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_QUANTIDADE_Filteredtext_set", StringUtil.RTrim( Ddo_autorizacaoconsumo_quantidade_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_QUANTIDADE_Filteredtextto_set", StringUtil.RTrim( Ddo_autorizacaoconsumo_quantidade_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_QUANTIDADE_Dropdownoptionstype", StringUtil.RTrim( Ddo_autorizacaoconsumo_quantidade_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_QUANTIDADE_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_autorizacaoconsumo_quantidade_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_QUANTIDADE_Includesortasc", StringUtil.BoolToStr( Ddo_autorizacaoconsumo_quantidade_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_QUANTIDADE_Includesortdsc", StringUtil.BoolToStr( Ddo_autorizacaoconsumo_quantidade_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_QUANTIDADE_Sortedstatus", StringUtil.RTrim( Ddo_autorizacaoconsumo_quantidade_Sortedstatus));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_QUANTIDADE_Includefilter", StringUtil.BoolToStr( Ddo_autorizacaoconsumo_quantidade_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_QUANTIDADE_Filtertype", StringUtil.RTrim( Ddo_autorizacaoconsumo_quantidade_Filtertype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_QUANTIDADE_Filterisrange", StringUtil.BoolToStr( Ddo_autorizacaoconsumo_quantidade_Filterisrange));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_QUANTIDADE_Includedatalist", StringUtil.BoolToStr( Ddo_autorizacaoconsumo_quantidade_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_QUANTIDADE_Sortasc", StringUtil.RTrim( Ddo_autorizacaoconsumo_quantidade_Sortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_QUANTIDADE_Sortdsc", StringUtil.RTrim( Ddo_autorizacaoconsumo_quantidade_Sortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_QUANTIDADE_Cleanfilter", StringUtil.RTrim( Ddo_autorizacaoconsumo_quantidade_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_QUANTIDADE_Rangefilterfrom", StringUtil.RTrim( Ddo_autorizacaoconsumo_quantidade_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_QUANTIDADE_Rangefilterto", StringUtil.RTrim( Ddo_autorizacaoconsumo_quantidade_Rangefilterto));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_QUANTIDADE_Searchbuttontext", StringUtil.RTrim( Ddo_autorizacaoconsumo_quantidade_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_VALOR_Caption", StringUtil.RTrim( Ddo_autorizacaoconsumo_valor_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_VALOR_Tooltip", StringUtil.RTrim( Ddo_autorizacaoconsumo_valor_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_VALOR_Cls", StringUtil.RTrim( Ddo_autorizacaoconsumo_valor_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_VALOR_Filteredtext_set", StringUtil.RTrim( Ddo_autorizacaoconsumo_valor_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_VALOR_Filteredtextto_set", StringUtil.RTrim( Ddo_autorizacaoconsumo_valor_Filteredtextto_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_VALOR_Dropdownoptionstype", StringUtil.RTrim( Ddo_autorizacaoconsumo_valor_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_VALOR_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_autorizacaoconsumo_valor_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_VALOR_Includesortasc", StringUtil.BoolToStr( Ddo_autorizacaoconsumo_valor_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_VALOR_Includesortdsc", StringUtil.BoolToStr( Ddo_autorizacaoconsumo_valor_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_VALOR_Sortedstatus", StringUtil.RTrim( Ddo_autorizacaoconsumo_valor_Sortedstatus));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_VALOR_Includefilter", StringUtil.BoolToStr( Ddo_autorizacaoconsumo_valor_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_VALOR_Filtertype", StringUtil.RTrim( Ddo_autorizacaoconsumo_valor_Filtertype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_VALOR_Filterisrange", StringUtil.BoolToStr( Ddo_autorizacaoconsumo_valor_Filterisrange));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_VALOR_Includedatalist", StringUtil.BoolToStr( Ddo_autorizacaoconsumo_valor_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_VALOR_Sortasc", StringUtil.RTrim( Ddo_autorizacaoconsumo_valor_Sortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_VALOR_Sortdsc", StringUtil.RTrim( Ddo_autorizacaoconsumo_valor_Sortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_VALOR_Cleanfilter", StringUtil.RTrim( Ddo_autorizacaoconsumo_valor_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_VALOR_Rangefilterfrom", StringUtil.RTrim( Ddo_autorizacaoconsumo_valor_Rangefilterfrom));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_VALOR_Rangefilterto", StringUtil.RTrim( Ddo_autorizacaoconsumo_valor_Rangefilterto));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_VALOR_Searchbuttontext", StringUtil.RTrim( Ddo_autorizacaoconsumo_valor_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_STATUS_Caption", StringUtil.RTrim( Ddo_autorizacaoconsumo_status_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_STATUS_Tooltip", StringUtil.RTrim( Ddo_autorizacaoconsumo_status_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_STATUS_Cls", StringUtil.RTrim( Ddo_autorizacaoconsumo_status_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_STATUS_Selectedvalue_set", StringUtil.RTrim( Ddo_autorizacaoconsumo_status_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_STATUS_Dropdownoptionstype", StringUtil.RTrim( Ddo_autorizacaoconsumo_status_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_STATUS_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_autorizacaoconsumo_status_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_STATUS_Includesortasc", StringUtil.BoolToStr( Ddo_autorizacaoconsumo_status_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_STATUS_Includesortdsc", StringUtil.BoolToStr( Ddo_autorizacaoconsumo_status_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_STATUS_Sortedstatus", StringUtil.RTrim( Ddo_autorizacaoconsumo_status_Sortedstatus));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_STATUS_Includefilter", StringUtil.BoolToStr( Ddo_autorizacaoconsumo_status_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_STATUS_Includedatalist", StringUtil.BoolToStr( Ddo_autorizacaoconsumo_status_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_STATUS_Datalisttype", StringUtil.RTrim( Ddo_autorizacaoconsumo_status_Datalisttype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_STATUS_Allowmultipleselection", StringUtil.BoolToStr( Ddo_autorizacaoconsumo_status_Allowmultipleselection));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_STATUS_Datalistfixedvalues", StringUtil.RTrim( Ddo_autorizacaoconsumo_status_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_STATUS_Sortasc", StringUtil.RTrim( Ddo_autorizacaoconsumo_status_Sortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_STATUS_Sortdsc", StringUtil.RTrim( Ddo_autorizacaoconsumo_status_Sortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_STATUS_Cleanfilter", StringUtil.RTrim( Ddo_autorizacaoconsumo_status_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_STATUS_Searchbuttontext", StringUtil.RTrim( Ddo_autorizacaoconsumo_status_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_ATIVO_Caption", StringUtil.RTrim( Ddo_autorizacaoconsumo_ativo_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_ATIVO_Tooltip", StringUtil.RTrim( Ddo_autorizacaoconsumo_ativo_Tooltip));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_ATIVO_Cls", StringUtil.RTrim( Ddo_autorizacaoconsumo_ativo_Cls));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_ATIVO_Selectedvalue_set", StringUtil.RTrim( Ddo_autorizacaoconsumo_ativo_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_ATIVO_Dropdownoptionstype", StringUtil.RTrim( Ddo_autorizacaoconsumo_ativo_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_ATIVO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_autorizacaoconsumo_ativo_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_ATIVO_Includesortasc", StringUtil.BoolToStr( Ddo_autorizacaoconsumo_ativo_Includesortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_ATIVO_Includesortdsc", StringUtil.BoolToStr( Ddo_autorizacaoconsumo_ativo_Includesortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_ATIVO_Sortedstatus", StringUtil.RTrim( Ddo_autorizacaoconsumo_ativo_Sortedstatus));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_ATIVO_Includefilter", StringUtil.BoolToStr( Ddo_autorizacaoconsumo_ativo_Includefilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_ATIVO_Includedatalist", StringUtil.BoolToStr( Ddo_autorizacaoconsumo_ativo_Includedatalist));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_ATIVO_Datalisttype", StringUtil.RTrim( Ddo_autorizacaoconsumo_ativo_Datalisttype));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_ATIVO_Datalistfixedvalues", StringUtil.RTrim( Ddo_autorizacaoconsumo_ativo_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_ATIVO_Sortasc", StringUtil.RTrim( Ddo_autorizacaoconsumo_ativo_Sortasc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_ATIVO_Sortdsc", StringUtil.RTrim( Ddo_autorizacaoconsumo_ativo_Sortdsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_ATIVO_Cleanfilter", StringUtil.RTrim( Ddo_autorizacaoconsumo_ativo_Cleanfilter));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_ATIVO_Searchbuttontext", StringUtil.RTrim( Ddo_autorizacaoconsumo_ativo_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_CODIGO_Activeeventkey", StringUtil.RTrim( Ddo_autorizacaoconsumo_codigo_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_CODIGO_Filteredtext_get", StringUtil.RTrim( Ddo_autorizacaoconsumo_codigo_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_CODIGO_Filteredtextto_get", StringUtil.RTrim( Ddo_autorizacaoconsumo_codigo_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIO_Activeeventkey", StringUtil.RTrim( Ddo_autorizacaoconsumo_vigenciainicio_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIO_Filteredtext_get", StringUtil.RTrim( Ddo_autorizacaoconsumo_vigenciainicio_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIO_Filteredtextto_get", StringUtil.RTrim( Ddo_autorizacaoconsumo_vigenciainicio_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_VIGENCIAFIM_Activeeventkey", StringUtil.RTrim( Ddo_autorizacaoconsumo_vigenciafim_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_VIGENCIAFIM_Filteredtext_get", StringUtil.RTrim( Ddo_autorizacaoconsumo_vigenciafim_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_VIGENCIAFIM_Filteredtextto_get", StringUtil.RTrim( Ddo_autorizacaoconsumo_vigenciafim_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_Activeeventkey", StringUtil.RTrim( Ddo_autorizacaoconsumo_unidademedicaonom_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_Filteredtext_get", StringUtil.RTrim( Ddo_autorizacaoconsumo_unidademedicaonom_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_Selectedvalue_get", StringUtil.RTrim( Ddo_autorizacaoconsumo_unidademedicaonom_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_QUANTIDADE_Activeeventkey", StringUtil.RTrim( Ddo_autorizacaoconsumo_quantidade_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_QUANTIDADE_Filteredtext_get", StringUtil.RTrim( Ddo_autorizacaoconsumo_quantidade_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_QUANTIDADE_Filteredtextto_get", StringUtil.RTrim( Ddo_autorizacaoconsumo_quantidade_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_VALOR_Activeeventkey", StringUtil.RTrim( Ddo_autorizacaoconsumo_valor_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_VALOR_Filteredtext_get", StringUtil.RTrim( Ddo_autorizacaoconsumo_valor_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_VALOR_Filteredtextto_get", StringUtil.RTrim( Ddo_autorizacaoconsumo_valor_Filteredtextto_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_STATUS_Activeeventkey", StringUtil.RTrim( Ddo_autorizacaoconsumo_status_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_STATUS_Selectedvalue_get", StringUtil.RTrim( Ddo_autorizacaoconsumo_status_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_ATIVO_Activeeventkey", StringUtil.RTrim( Ddo_autorizacaoconsumo_ativo_Activeeventkey));
         GxWebStd.gx_hidden_field( context, sPrefix+"DDO_AUTORIZACAOCONSUMO_ATIVO_Selectedvalue_get", StringUtil.RTrim( Ddo_autorizacaoconsumo_ativo_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, sPrefix+"DVELOP_CONFIRMPANEL_CANCELARAUTORIZCAOCONSUMO_Result", StringUtil.RTrim( Dvelop_confirmpanel_cancelarautorizcaoconsumo_Result));
         GxWebStd.gx_hidden_field( context, sPrefix+"DVELOP_CONFIRMPANEL_AUTORIZAR_Result", StringUtil.RTrim( Dvelop_confirmpanel_autorizar_Result));
         GxWebStd.gx_hidden_field( context, sPrefix+"DVELOP_CONFIRMPANEL_ENCERRAR_Result", StringUtil.RTrim( Dvelop_confirmpanel_encerrar_Result));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = sPrefix + "hsh" + "ContratoAutorizacaoConsumoWC";
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A74Contrato_Codigo), "ZZZZZ9");
         GxWebStd.gx_hidden_field( context, sPrefix+"hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("contratoautorizacaoconsumowc:[SendSecurityCheck value for]"+"Contrato_Codigo:"+context.localUtil.Format( (decimal)(A74Contrato_Codigo), "ZZZZZ9"));
      }

      protected void RenderHtmlCloseFormNB2( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("contratoautorizacaoconsumowc.js", "?202051812554396");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            context.WriteHtmlTextNl( "</form>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "ContratoAutorizacaoConsumoWC" ;
      }

      public override String GetPgmdesc( )
      {
         return "Contrato Autorizacao Consumo WC" ;
      }

      protected void WBNB0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "contratoautorizacaoconsumowc.aspx");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
               context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
               context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/ConfirmPanel/BootstrapConfirmPanelRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/ConfirmPanel/BootstrapConfirmPanelRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/ConfirmPanel/BootstrapConfirmPanelRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
               context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
            }
            wb_table1_2_NB2( true) ;
         }
         else
         {
            wb_table1_2_NB2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_NB2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContrato_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A74Contrato_Codigo), 6, 0, ",", "")), context.localUtil.Format( (decimal)(A74Contrato_Codigo), "ZZZZZ9"), "", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContrato_Codigo_Jsonclick, 0, "Attribute", "", "", "", edtContrato_Codigo_Visible, 0, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContratoAutorizacaoConsumoWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 38,'" + sPrefix + "',false,'" + sGXsfl_19_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavContrato_codigo_selected_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV19Contrato_Codigo_Selected), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV19Contrato_Codigo_Selected), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,38);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavContrato_codigo_selected_Jsonclick, 0, "Attribute", "", "", "", edtavContrato_codigo_selected_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoAutorizacaoConsumoWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 39,'" + sPrefix + "',false,'" + sGXsfl_19_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavAutorizacaoconsumo_codigo_selected_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV18AutorizacaoConsumo_Codigo_Selected), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV18AutorizacaoConsumo_Codigo_Selected), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,39);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavAutorizacaoconsumo_codigo_selected_Jsonclick, 0, "Attribute", "", "", "", edtavAutorizacaoconsumo_codigo_selected_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoAutorizacaoConsumoWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 40,'" + sPrefix + "',false,'" + sGXsfl_19_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrderedby_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13OrderedBy), 4, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV13OrderedBy), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,40);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrderedby_Jsonclick, 0, "Attribute", "", "", "", edtavOrderedby_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoAutorizacaoConsumoWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 41,'" + sPrefix + "',false,'" + sGXsfl_19_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV14OrderedDsc), StringUtil.BoolToStr( AV14OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,41);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_ContratoAutorizacaoConsumoWC.htm");
            wb_table2_42_NB2( true) ;
         }
         else
         {
            wb_table2_42_NB2( false) ;
         }
         return  ;
      }

      protected void wb_table2_42_NB2e( bool wbgen )
      {
         if ( wbgen )
         {
            wb_table3_47_NB2( true) ;
         }
         else
         {
            wb_table3_47_NB2( false) ;
         }
         return  ;
      }

      protected void wb_table3_47_NB2e( bool wbgen )
      {
         if ( wbgen )
         {
            wb_table4_52_NB2( true) ;
         }
         else
         {
            wb_table4_52_NB2( false) ;
         }
         return  ;
      }

      protected void wb_table4_52_NB2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 57,'" + sPrefix + "',false,'" + sGXsfl_19_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfautorizacaoconsumo_codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV23TFAutorizacaoConsumo_Codigo), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV23TFAutorizacaoConsumo_Codigo), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,57);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfautorizacaoconsumo_codigo_Jsonclick, 0, "Attribute", "", "", "", edtavTfautorizacaoconsumo_codigo_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoAutorizacaoConsumoWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 58,'" + sPrefix + "',false,'" + sGXsfl_19_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfautorizacaoconsumo_codigo_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV24TFAutorizacaoConsumo_Codigo_To), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV24TFAutorizacaoConsumo_Codigo_To), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,58);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfautorizacaoconsumo_codigo_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfautorizacaoconsumo_codigo_to_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoAutorizacaoConsumoWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 59,'" + sPrefix + "',false,'" + sGXsfl_19_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTfautorizacaoconsumo_vigenciainicio_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTfautorizacaoconsumo_vigenciainicio_Internalname, context.localUtil.Format(AV27TFAutorizacaoConsumo_VigenciaInicio, "99/99/99"), context.localUtil.Format( AV27TFAutorizacaoConsumo_VigenciaInicio, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,59);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfautorizacaoconsumo_vigenciainicio_Jsonclick, 0, "Attribute", "", "", "", edtavTfautorizacaoconsumo_vigenciainicio_Visible, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoAutorizacaoConsumoWC.htm");
            GxWebStd.gx_bitmap( context, edtavTfautorizacaoconsumo_vigenciainicio_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTfautorizacaoconsumo_vigenciainicio_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_ContratoAutorizacaoConsumoWC.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 60,'" + sPrefix + "',false,'" + sGXsfl_19_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTfautorizacaoconsumo_vigenciainicio_to_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTfautorizacaoconsumo_vigenciainicio_to_Internalname, context.localUtil.Format(AV28TFAutorizacaoConsumo_VigenciaInicio_To, "99/99/99"), context.localUtil.Format( AV28TFAutorizacaoConsumo_VigenciaInicio_To, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,60);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfautorizacaoconsumo_vigenciainicio_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfautorizacaoconsumo_vigenciainicio_to_Visible, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoAutorizacaoConsumoWC.htm");
            GxWebStd.gx_bitmap( context, edtavTfautorizacaoconsumo_vigenciainicio_to_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTfautorizacaoconsumo_vigenciainicio_to_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_ContratoAutorizacaoConsumoWC.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divDdo_autorizacaoconsumo_vigenciainicioauxdates_Internalname, 1, 0, "px", 0, "px", "Invisible", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 62,'" + sPrefix + "',false,'" + sGXsfl_19_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_autorizacaoconsumo_vigenciainicioauxdate_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_autorizacaoconsumo_vigenciainicioauxdate_Internalname, context.localUtil.Format(AV29DDO_AutorizacaoConsumo_VigenciaInicioAuxDate, "99/99/99"), context.localUtil.Format( AV29DDO_AutorizacaoConsumo_VigenciaInicioAuxDate, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,62);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_autorizacaoconsumo_vigenciainicioauxdate_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoAutorizacaoConsumoWC.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_autorizacaoconsumo_vigenciainicioauxdate_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_ContratoAutorizacaoConsumoWC.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 63,'" + sPrefix + "',false,'" + sGXsfl_19_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_autorizacaoconsumo_vigenciainicioauxdateto_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_autorizacaoconsumo_vigenciainicioauxdateto_Internalname, context.localUtil.Format(AV30DDO_AutorizacaoConsumo_VigenciaInicioAuxDateTo, "99/99/99"), context.localUtil.Format( AV30DDO_AutorizacaoConsumo_VigenciaInicioAuxDateTo, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,63);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_autorizacaoconsumo_vigenciainicioauxdateto_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoAutorizacaoConsumoWC.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_autorizacaoconsumo_vigenciainicioauxdateto_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_ContratoAutorizacaoConsumoWC.htm");
            context.WriteHtmlTextNl( "</div>") ;
            GxWebStd.gx_div_end( context, "left", "top");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 64,'" + sPrefix + "',false,'" + sGXsfl_19_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTfautorizacaoconsumo_vigenciafim_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTfautorizacaoconsumo_vigenciafim_Internalname, context.localUtil.Format(AV33TFAutorizacaoConsumo_VigenciaFim, "99/99/99"), context.localUtil.Format( AV33TFAutorizacaoConsumo_VigenciaFim, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,64);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfautorizacaoconsumo_vigenciafim_Jsonclick, 0, "Attribute", "", "", "", edtavTfautorizacaoconsumo_vigenciafim_Visible, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoAutorizacaoConsumoWC.htm");
            GxWebStd.gx_bitmap( context, edtavTfautorizacaoconsumo_vigenciafim_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTfautorizacaoconsumo_vigenciafim_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_ContratoAutorizacaoConsumoWC.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 65,'" + sPrefix + "',false,'" + sGXsfl_19_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavTfautorizacaoconsumo_vigenciafim_to_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavTfautorizacaoconsumo_vigenciafim_to_Internalname, context.localUtil.Format(AV34TFAutorizacaoConsumo_VigenciaFim_To, "99/99/99"), context.localUtil.Format( AV34TFAutorizacaoConsumo_VigenciaFim_To, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,65);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfautorizacaoconsumo_vigenciafim_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfautorizacaoconsumo_vigenciafim_to_Visible, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoAutorizacaoConsumoWC.htm");
            GxWebStd.gx_bitmap( context, edtavTfautorizacaoconsumo_vigenciafim_to_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((edtavTfautorizacaoconsumo_vigenciafim_to_Visible==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_ContratoAutorizacaoConsumoWC.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divDdo_autorizacaoconsumo_vigenciafimauxdates_Internalname, 1, 0, "px", 0, "px", "Invisible", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 67,'" + sPrefix + "',false,'" + sGXsfl_19_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_autorizacaoconsumo_vigenciafimauxdate_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_autorizacaoconsumo_vigenciafimauxdate_Internalname, context.localUtil.Format(AV35DDO_AutorizacaoConsumo_VigenciaFimAuxDate, "99/99/99"), context.localUtil.Format( AV35DDO_AutorizacaoConsumo_VigenciaFimAuxDate, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,67);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_autorizacaoconsumo_vigenciafimauxdate_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoAutorizacaoConsumoWC.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_autorizacaoconsumo_vigenciafimauxdate_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_ContratoAutorizacaoConsumoWC.htm");
            context.WriteHtmlTextNl( "</div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 68,'" + sPrefix + "',false,'" + sGXsfl_19_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavDdo_autorizacaoconsumo_vigenciafimauxdateto_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavDdo_autorizacaoconsumo_vigenciafimauxdateto_Internalname, context.localUtil.Format(AV36DDO_AutorizacaoConsumo_VigenciaFimAuxDateTo, "99/99/99"), context.localUtil.Format( AV36DDO_AutorizacaoConsumo_VigenciaFimAuxDateTo, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,68);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDdo_autorizacaoconsumo_vigenciafimauxdateto_Jsonclick, 0, "Attribute", "", "", "", 1, 1, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoAutorizacaoConsumoWC.htm");
            GxWebStd.gx_bitmap( context, edtavDdo_autorizacaoconsumo_vigenciafimauxdateto_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(1==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_ContratoAutorizacaoConsumoWC.htm");
            context.WriteHtmlTextNl( "</div>") ;
            GxWebStd.gx_div_end( context, "left", "top");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 69,'" + sPrefix + "',false,'" + sGXsfl_19_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfautorizacaoconsumo_unidademedicaonom_Internalname, StringUtil.RTrim( AV39TFAutorizacaoConsumo_UnidadeMedicaoNom), StringUtil.RTrim( context.localUtil.Format( AV39TFAutorizacaoConsumo_UnidadeMedicaoNom, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,69);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfautorizacaoconsumo_unidademedicaonom_Jsonclick, 0, "Attribute", "", "", "", edtavTfautorizacaoconsumo_unidademedicaonom_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ContratoAutorizacaoConsumoWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 70,'" + sPrefix + "',false,'" + sGXsfl_19_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfautorizacaoconsumo_unidademedicaonom_sel_Internalname, StringUtil.RTrim( AV40TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel), StringUtil.RTrim( context.localUtil.Format( AV40TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,70);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfautorizacaoconsumo_unidademedicaonom_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfautorizacaoconsumo_unidademedicaonom_sel_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ContratoAutorizacaoConsumoWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 71,'" + sPrefix + "',false,'" + sGXsfl_19_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfautorizacaoconsumo_quantidade_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV43TFAutorizacaoConsumo_Quantidade), 3, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV43TFAutorizacaoConsumo_Quantidade), "ZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,71);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfautorizacaoconsumo_quantidade_Jsonclick, 0, "Attribute", "", "", "", edtavTfautorizacaoconsumo_quantidade_Visible, 1, 0, "text", "", 3, "chr", 1, "row", 3, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoAutorizacaoConsumoWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 72,'" + sPrefix + "',false,'" + sGXsfl_19_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfautorizacaoconsumo_quantidade_to_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV44TFAutorizacaoConsumo_Quantidade_To), 3, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV44TFAutorizacaoConsumo_Quantidade_To), "ZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,72);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfautorizacaoconsumo_quantidade_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfautorizacaoconsumo_quantidade_to_Visible, 1, 0, "text", "", 3, "chr", 1, "row", 3, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoAutorizacaoConsumoWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 73,'" + sPrefix + "',false,'" + sGXsfl_19_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfautorizacaoconsumo_valor_Internalname, StringUtil.LTrim( StringUtil.NToC( AV47TFAutorizacaoConsumo_Valor, 18, 5, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV47TFAutorizacaoConsumo_Valor, "ZZZ,ZZZ,ZZZ,ZZ9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,73);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfautorizacaoconsumo_valor_Jsonclick, 0, "Attribute", "", "", "", edtavTfautorizacaoconsumo_valor_Visible, 1, 0, "text", "", 18, "chr", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoAutorizacaoConsumoWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 74,'" + sPrefix + "',false,'" + sGXsfl_19_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfautorizacaoconsumo_valor_to_Internalname, StringUtil.LTrim( StringUtil.NToC( AV48TFAutorizacaoConsumo_Valor_To, 18, 5, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV48TFAutorizacaoConsumo_Valor_To, "ZZZ,ZZZ,ZZZ,ZZ9.99")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,74);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfautorizacaoconsumo_valor_to_Jsonclick, 0, "Attribute", "", "", "", edtavTfautorizacaoconsumo_valor_to_Visible, 1, 0, "text", "", 18, "chr", 1, "row", 18, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoAutorizacaoConsumoWC.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 75,'" + sPrefix + "',false,'" + sGXsfl_19_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfautorizacaoconsumo_ativo_sel_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV55TFAutorizacaoConsumo_Ativo_Sel), 1, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV55TFAutorizacaoConsumo_Ativo_Sel), "9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,75);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfautorizacaoconsumo_ativo_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfautorizacaoconsumo_ativo_sel_Visible, 1, 0, "text", "", 1, "chr", 1, "row", 1, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContratoAutorizacaoConsumoWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_AUTORIZACAOCONSUMO_CODIGOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 77,'" + sPrefix + "',false,'" + sGXsfl_19_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_autorizacaoconsumo_codigotitlecontrolidtoreplace_Internalname, AV25ddo_AutorizacaoConsumo_CodigoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,77);\"", 0, edtavDdo_autorizacaoconsumo_codigotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_ContratoAutorizacaoConsumoWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 79,'" + sPrefix + "',false,'" + sGXsfl_19_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_autorizacaoconsumo_vigenciainiciotitlecontrolidtoreplace_Internalname, AV31ddo_AutorizacaoConsumo_VigenciaInicioTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,79);\"", 0, edtavDdo_autorizacaoconsumo_vigenciainiciotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_ContratoAutorizacaoConsumoWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_AUTORIZACAOCONSUMO_VIGENCIAFIMContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 81,'" + sPrefix + "',false,'" + sGXsfl_19_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_autorizacaoconsumo_vigenciafimtitlecontrolidtoreplace_Internalname, AV37ddo_AutorizacaoConsumo_VigenciaFimTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,81);\"", 0, edtavDdo_autorizacaoconsumo_vigenciafimtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_ContratoAutorizacaoConsumoWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOMContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 83,'" + sPrefix + "',false,'" + sGXsfl_19_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_autorizacaoconsumo_unidademedicaonomtitlecontrolidtoreplace_Internalname, AV41ddo_AutorizacaoConsumo_UnidadeMedicaoNomTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,83);\"", 0, edtavDdo_autorizacaoconsumo_unidademedicaonomtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_ContratoAutorizacaoConsumoWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_AUTORIZACAOCONSUMO_QUANTIDADEContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 85,'" + sPrefix + "',false,'" + sGXsfl_19_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_autorizacaoconsumo_quantidadetitlecontrolidtoreplace_Internalname, AV45ddo_AutorizacaoConsumo_QuantidadeTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,85);\"", 0, edtavDdo_autorizacaoconsumo_quantidadetitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_ContratoAutorizacaoConsumoWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_AUTORIZACAOCONSUMO_VALORContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 87,'" + sPrefix + "',false,'" + sGXsfl_19_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_autorizacaoconsumo_valortitlecontrolidtoreplace_Internalname, AV49ddo_AutorizacaoConsumo_ValorTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,87);\"", 0, edtavDdo_autorizacaoconsumo_valortitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_ContratoAutorizacaoConsumoWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_AUTORIZACAOCONSUMO_STATUSContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 89,'" + sPrefix + "',false,'" + sGXsfl_19_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_autorizacaoconsumo_statustitlecontrolidtoreplace_Internalname, AV53ddo_AutorizacaoConsumo_StatusTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,89);\"", 0, edtavDdo_autorizacaoconsumo_statustitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_ContratoAutorizacaoConsumoWC.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DDO_AUTORIZACAOCONSUMO_ATIVOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 91,'" + sPrefix + "',false,'" + sGXsfl_19_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_autorizacaoconsumo_ativotitlecontrolidtoreplace_Internalname, AV56ddo_AutorizacaoConsumo_AtivoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,91);\"", 0, edtavDdo_autorizacaoconsumo_ativotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_ContratoAutorizacaoConsumoWC.htm");
         }
         wbLoad = true;
      }

      protected void STARTNB2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "Contrato Autorizacao Consumo WC", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUPNB0( ) ;
            }
         }
      }

      protected void WSNB2( )
      {
         STARTNB2( ) ;
         EVTNB2( ) ;
      }

      protected void EVTNB2( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPNB0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPNB0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E11NB2 */
                                    E11NB2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DVELOP_CONFIRMPANEL_CANCELARAUTORIZCAOCONSUMO.CLOSE") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPNB0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E12NB2 */
                                    E12NB2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DVELOP_CONFIRMPANEL_AUTORIZAR.CLOSE") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPNB0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E13NB2 */
                                    E13NB2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DVELOP_CONFIRMPANEL_ENCERRAR.CLOSE") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPNB0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E14NB2 */
                                    E14NB2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_AUTORIZACAOCONSUMO_CODIGO.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPNB0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E15NB2 */
                                    E15NB2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIO.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPNB0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E16NB2 */
                                    E16NB2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_AUTORIZACAOCONSUMO_VIGENCIAFIM.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPNB0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E17NB2 */
                                    E17NB2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPNB0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E18NB2 */
                                    E18NB2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_AUTORIZACAOCONSUMO_QUANTIDADE.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPNB0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E19NB2 */
                                    E19NB2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_AUTORIZACAOCONSUMO_VALOR.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPNB0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E20NB2 */
                                    E20NB2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_AUTORIZACAOCONSUMO_STATUS.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPNB0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E21NB2 */
                                    E21NB2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_AUTORIZACAOCONSUMO_ATIVO.ONOPTIONCLICKED") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPNB0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E22NB2 */
                                    E22NB2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOINSERT'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPNB0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E23NB2 */
                                    E23NB2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPNB0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    GX_FocusControl = edtavContrato_codigo_selected_Internalname;
                                    context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                 }
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUPNB0( ) ;
                              }
                              nGXsfl_19_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_19_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_19_idx), 4, 0)), 4, "0");
                              SubsflControlProps_192( ) ;
                              AV17CancelarAutorizcaoConsumo = cgiGet( edtavCancelarautorizcaoconsumo_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavCancelarautorizcaoconsumo_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV17CancelarAutorizcaoConsumo)) ? AV63Cancelarautorizcaoconsumo_GXI : context.convertURL( context.PathToRelativeUrl( AV17CancelarAutorizcaoConsumo))));
                              AV20Autorizar = cgiGet( edtavAutorizar_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavAutorizar_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV20Autorizar)) ? AV64Autorizar_GXI : context.convertURL( context.PathToRelativeUrl( AV20Autorizar))));
                              AV21Encerrar = cgiGet( edtavEncerrar_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavEncerrar_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV21Encerrar)) ? AV65Encerrar_GXI : context.convertURL( context.PathToRelativeUrl( AV21Encerrar))));
                              A1774AutorizacaoConsumo_Codigo = (int)(context.localUtil.CToN( cgiGet( edtAutorizacaoConsumo_Codigo_Internalname), ",", "."));
                              A1775AutorizacaoConsumo_VigenciaInicio = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtAutorizacaoConsumo_VigenciaInicio_Internalname), 0));
                              A1776AutorizacaoConsumo_VigenciaFim = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtAutorizacaoConsumo_VigenciaFim_Internalname), 0));
                              A1778AutorizacaoConsumo_UnidadeMedicaoNom = StringUtil.Upper( cgiGet( edtAutorizacaoConsumo_UnidadeMedicaoNom_Internalname));
                              n1778AutorizacaoConsumo_UnidadeMedicaoNom = false;
                              A1779AutorizacaoConsumo_Quantidade = (short)(context.localUtil.CToN( cgiGet( edtAutorizacaoConsumo_Quantidade_Internalname), ",", "."));
                              A1782AutorizacaoConsumo_Valor = context.localUtil.CToN( cgiGet( edtAutorizacaoConsumo_Valor_Internalname), ",", ".");
                              cmbAutorizacaoConsumo_Status.Name = cmbAutorizacaoConsumo_Status_Internalname;
                              cmbAutorizacaoConsumo_Status.CurrentValue = cgiGet( cmbAutorizacaoConsumo_Status_Internalname);
                              A1787AutorizacaoConsumo_Status = cgiGet( cmbAutorizacaoConsumo_Status_Internalname);
                              A1780AutorizacaoConsumo_Ativo = StringUtil.StrToBool( cgiGet( chkAutorizacaoConsumo_Ativo_Internalname));
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavContrato_codigo_selected_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E24NB2 */
                                          E24NB2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavContrato_codigo_selected_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E25NB2 */
                                          E25NB2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavContrato_codigo_selected_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E26NB2 */
                                          E26NB2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          if ( ! wbErr )
                                          {
                                             Rfr0gs = false;
                                             /* Set Refresh If Orderedby Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Ordereddsc Changed */
                                             if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vORDEREDDSC")) != AV14OrderedDsc )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfautorizacaoconsumo_codigo Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFAUTORIZACAOCONSUMO_CODIGO"), ",", ".") != Convert.ToDecimal( AV23TFAutorizacaoConsumo_Codigo )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfautorizacaoconsumo_codigo_to Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFAUTORIZACAOCONSUMO_CODIGO_TO"), ",", ".") != Convert.ToDecimal( AV24TFAutorizacaoConsumo_Codigo_To )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfautorizacaoconsumo_vigenciainicio Changed */
                                             if ( context.localUtil.CToT( cgiGet( sPrefix+"GXH_vTFAUTORIZACAOCONSUMO_VIGENCIAINICIO"), 0) != AV27TFAutorizacaoConsumo_VigenciaInicio )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfautorizacaoconsumo_vigenciainicio_to Changed */
                                             if ( context.localUtil.CToT( cgiGet( sPrefix+"GXH_vTFAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO"), 0) != AV28TFAutorizacaoConsumo_VigenciaInicio_To )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfautorizacaoconsumo_vigenciafim Changed */
                                             if ( context.localUtil.CToT( cgiGet( sPrefix+"GXH_vTFAUTORIZACAOCONSUMO_VIGENCIAFIM"), 0) != AV33TFAutorizacaoConsumo_VigenciaFim )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfautorizacaoconsumo_vigenciafim_to Changed */
                                             if ( context.localUtil.CToT( cgiGet( sPrefix+"GXH_vTFAUTORIZACAOCONSUMO_VIGENCIAFIM_TO"), 0) != AV34TFAutorizacaoConsumo_VigenciaFim_To )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfautorizacaoconsumo_unidademedicaonom Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFAUTORIZACAOCONSUMO_UNIDADEMEDICAONOM"), AV39TFAutorizacaoConsumo_UnidadeMedicaoNom) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfautorizacaoconsumo_unidademedicaonom_sel Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFAUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_SEL"), AV40TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfautorizacaoconsumo_quantidade Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFAUTORIZACAOCONSUMO_QUANTIDADE"), ",", ".") != Convert.ToDecimal( AV43TFAutorizacaoConsumo_Quantidade )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfautorizacaoconsumo_quantidade_to Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFAUTORIZACAOCONSUMO_QUANTIDADE_TO"), ",", ".") != Convert.ToDecimal( AV44TFAutorizacaoConsumo_Quantidade_To )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfautorizacaoconsumo_valor Changed */
                                             if ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFAUTORIZACAOCONSUMO_VALOR"), ",", ".") != AV47TFAutorizacaoConsumo_Valor )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfautorizacaoconsumo_valor_to Changed */
                                             if ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFAUTORIZACAOCONSUMO_VALOR_TO"), ",", ".") != AV48TFAutorizacaoConsumo_Valor_To )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Tfautorizacaoconsumo_ativo_sel Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFAUTORIZACAOCONSUMO_ATIVO_SEL"), ",", ".") != Convert.ToDecimal( AV55TFAutorizacaoConsumo_Ativo_Sel )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             if ( ! Rfr0gs )
                                             {
                                             }
                                             dynload_actions( ) ;
                                          }
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavContrato_codigo_selected_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                                    {
                                       STRUPNB0( ) ;
                                    }
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavContrato_codigo_selected_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WENB2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormNB2( ) ;
            }
         }
      }

      protected void PANB2( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            GXCCtl = "AUTORIZACAOCONSUMO_STATUS_" + sGXsfl_19_idx;
            cmbAutorizacaoConsumo_Status.Name = GXCCtl;
            cmbAutorizacaoConsumo_Status.WebTags = "";
            cmbAutorizacaoConsumo_Status.addItem("CRI", "Criada", 0);
            cmbAutorizacaoConsumo_Status.addItem("AUT", "Autorizada", 0);
            cmbAutorizacaoConsumo_Status.addItem("CAN", "Cancelada", 0);
            cmbAutorizacaoConsumo_Status.addItem("UTI", "Utilizada", 0);
            cmbAutorizacaoConsumo_Status.addItem("ENC", "Encerrada", 0);
            if ( cmbAutorizacaoConsumo_Status.ItemCount > 0 )
            {
               A1787AutorizacaoConsumo_Status = cmbAutorizacaoConsumo_Status.getValidValue(A1787AutorizacaoConsumo_Status);
            }
            GXCCtl = "AUTORIZACAOCONSUMO_ATIVO_" + sGXsfl_19_idx;
            chkAutorizacaoConsumo_Ativo.Name = GXCCtl;
            chkAutorizacaoConsumo_Ativo.WebTags = "";
            chkAutorizacaoConsumo_Ativo.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkAutorizacaoConsumo_Ativo_Internalname, "TitleCaption", chkAutorizacaoConsumo_Ativo.Caption);
            chkAutorizacaoConsumo_Ativo.CheckedValue = "false";
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = edtavContrato_codigo_selected_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_192( ) ;
         while ( nGXsfl_19_idx <= nRC_GXsfl_19 )
         {
            sendrow_192( ) ;
            nGXsfl_19_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_19_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_19_idx+1));
            sGXsfl_19_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_19_idx), 4, 0)), 4, "0");
            SubsflControlProps_192( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV13OrderedBy ,
                                       bool AV14OrderedDsc ,
                                       int AV23TFAutorizacaoConsumo_Codigo ,
                                       int AV24TFAutorizacaoConsumo_Codigo_To ,
                                       DateTime AV27TFAutorizacaoConsumo_VigenciaInicio ,
                                       DateTime AV28TFAutorizacaoConsumo_VigenciaInicio_To ,
                                       DateTime AV33TFAutorizacaoConsumo_VigenciaFim ,
                                       DateTime AV34TFAutorizacaoConsumo_VigenciaFim_To ,
                                       String AV39TFAutorizacaoConsumo_UnidadeMedicaoNom ,
                                       String AV40TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel ,
                                       short AV43TFAutorizacaoConsumo_Quantidade ,
                                       short AV44TFAutorizacaoConsumo_Quantidade_To ,
                                       decimal AV47TFAutorizacaoConsumo_Valor ,
                                       decimal AV48TFAutorizacaoConsumo_Valor_To ,
                                       short AV55TFAutorizacaoConsumo_Ativo_Sel ,
                                       int AV7Contrato_Codigo ,
                                       String AV25ddo_AutorizacaoConsumo_CodigoTitleControlIdToReplace ,
                                       String AV31ddo_AutorizacaoConsumo_VigenciaInicioTitleControlIdToReplace ,
                                       String AV37ddo_AutorizacaoConsumo_VigenciaFimTitleControlIdToReplace ,
                                       String AV41ddo_AutorizacaoConsumo_UnidadeMedicaoNomTitleControlIdToReplace ,
                                       String AV45ddo_AutorizacaoConsumo_QuantidadeTitleControlIdToReplace ,
                                       String AV49ddo_AutorizacaoConsumo_ValorTitleControlIdToReplace ,
                                       String AV53ddo_AutorizacaoConsumo_StatusTitleControlIdToReplace ,
                                       String AV56ddo_AutorizacaoConsumo_AtivoTitleControlIdToReplace ,
                                       String AV66Pgmname ,
                                       IGxCollection AV52TFAutorizacaoConsumo_Status_Sels ,
                                       bool A1780AutorizacaoConsumo_Ativo ,
                                       String sPrefix )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RFNB2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_AUTORIZACAOCONSUMO_CODIGO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1774AutorizacaoConsumo_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"AUTORIZACAOCONSUMO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1774AutorizacaoConsumo_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_AUTORIZACAOCONSUMO_VIGENCIAINICIO", GetSecureSignedToken( sPrefix, A1775AutorizacaoConsumo_VigenciaInicio));
         GxWebStd.gx_hidden_field( context, sPrefix+"AUTORIZACAOCONSUMO_VIGENCIAINICIO", context.localUtil.Format(A1775AutorizacaoConsumo_VigenciaInicio, "99/99/99"));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_AUTORIZACAOCONSUMO_VIGENCIAFIM", GetSecureSignedToken( sPrefix, A1776AutorizacaoConsumo_VigenciaFim));
         GxWebStd.gx_hidden_field( context, sPrefix+"AUTORIZACAOCONSUMO_VIGENCIAFIM", context.localUtil.Format(A1776AutorizacaoConsumo_VigenciaFim, "99/99/99"));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_AUTORIZACAOCONSUMO_QUANTIDADE", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A1779AutorizacaoConsumo_Quantidade), "ZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"AUTORIZACAOCONSUMO_QUANTIDADE", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1779AutorizacaoConsumo_Quantidade), 3, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_AUTORIZACAOCONSUMO_VALOR", GetSecureSignedToken( sPrefix, context.localUtil.Format( A1782AutorizacaoConsumo_Valor, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
         GxWebStd.gx_hidden_field( context, sPrefix+"AUTORIZACAOCONSUMO_VALOR", StringUtil.LTrim( StringUtil.NToC( A1782AutorizacaoConsumo_Valor, 18, 5, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_AUTORIZACAOCONSUMO_STATUS", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A1787AutorizacaoConsumo_Status, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"AUTORIZACAOCONSUMO_STATUS", StringUtil.RTrim( A1787AutorizacaoConsumo_Status));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_AUTORIZACAOCONSUMO_ATIVO", GetSecureSignedToken( sPrefix, A1780AutorizacaoConsumo_Ativo));
         GxWebStd.gx_hidden_field( context, sPrefix+"AUTORIZACAOCONSUMO_ATIVO", StringUtil.BoolToStr( A1780AutorizacaoConsumo_Ativo));
      }

      protected void fix_multi_value_controls( )
      {
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFNB2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV66Pgmname = "ContratoAutorizacaoConsumoWC";
         context.Gx_err = 0;
      }

      protected void RFNB2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 19;
         /* Execute user event: E25NB2 */
         E25NB2 ();
         nGXsfl_19_idx = 1;
         sGXsfl_19_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_19_idx), 4, 0)), 4, "0");
         SubsflControlProps_192( ) ;
         nGXsfl_19_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", sPrefix);
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_192( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 A1787AutorizacaoConsumo_Status ,
                                                 AV52TFAutorizacaoConsumo_Status_Sels ,
                                                 AV23TFAutorizacaoConsumo_Codigo ,
                                                 AV24TFAutorizacaoConsumo_Codigo_To ,
                                                 AV27TFAutorizacaoConsumo_VigenciaInicio ,
                                                 AV28TFAutorizacaoConsumo_VigenciaInicio_To ,
                                                 AV33TFAutorizacaoConsumo_VigenciaFim ,
                                                 AV34TFAutorizacaoConsumo_VigenciaFim_To ,
                                                 AV40TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel ,
                                                 AV39TFAutorizacaoConsumo_UnidadeMedicaoNom ,
                                                 AV43TFAutorizacaoConsumo_Quantidade ,
                                                 AV44TFAutorizacaoConsumo_Quantidade_To ,
                                                 AV47TFAutorizacaoConsumo_Valor ,
                                                 AV48TFAutorizacaoConsumo_Valor_To ,
                                                 AV52TFAutorizacaoConsumo_Status_Sels.Count ,
                                                 AV55TFAutorizacaoConsumo_Ativo_Sel ,
                                                 A1774AutorizacaoConsumo_Codigo ,
                                                 A1775AutorizacaoConsumo_VigenciaInicio ,
                                                 A1776AutorizacaoConsumo_VigenciaFim ,
                                                 A1778AutorizacaoConsumo_UnidadeMedicaoNom ,
                                                 A1779AutorizacaoConsumo_Quantidade ,
                                                 A1782AutorizacaoConsumo_Valor ,
                                                 A1780AutorizacaoConsumo_Ativo ,
                                                 AV13OrderedBy ,
                                                 AV14OrderedDsc ,
                                                 A74Contrato_Codigo ,
                                                 AV7Contrato_Codigo },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT,
                                                 TypeConstants.SHORT, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.INT, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                                 TypeConstants.SHORT, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT
                                                 }
            });
            lV39TFAutorizacaoConsumo_UnidadeMedicaoNom = StringUtil.PadR( StringUtil.RTrim( AV39TFAutorizacaoConsumo_UnidadeMedicaoNom), 50, "%");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV39TFAutorizacaoConsumo_UnidadeMedicaoNom", AV39TFAutorizacaoConsumo_UnidadeMedicaoNom);
            /* Using cursor H00NB2 */
            pr_default.execute(0, new Object[] {AV7Contrato_Codigo, AV23TFAutorizacaoConsumo_Codigo, AV24TFAutorizacaoConsumo_Codigo_To, AV27TFAutorizacaoConsumo_VigenciaInicio, AV28TFAutorizacaoConsumo_VigenciaInicio_To, AV33TFAutorizacaoConsumo_VigenciaFim, AV34TFAutorizacaoConsumo_VigenciaFim_To, lV39TFAutorizacaoConsumo_UnidadeMedicaoNom, AV40TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel, AV43TFAutorizacaoConsumo_Quantidade, AV44TFAutorizacaoConsumo_Quantidade_To, AV47TFAutorizacaoConsumo_Valor, AV48TFAutorizacaoConsumo_Valor_To, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_19_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A1777AutorizacaoConsumo_UnidadeMedicaoCod = H00NB2_A1777AutorizacaoConsumo_UnidadeMedicaoCod[0];
               A74Contrato_Codigo = H00NB2_A74Contrato_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A74Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A74Contrato_Codigo), 6, 0)));
               A1780AutorizacaoConsumo_Ativo = H00NB2_A1780AutorizacaoConsumo_Ativo[0];
               A1787AutorizacaoConsumo_Status = H00NB2_A1787AutorizacaoConsumo_Status[0];
               A1782AutorizacaoConsumo_Valor = H00NB2_A1782AutorizacaoConsumo_Valor[0];
               A1779AutorizacaoConsumo_Quantidade = H00NB2_A1779AutorizacaoConsumo_Quantidade[0];
               A1778AutorizacaoConsumo_UnidadeMedicaoNom = H00NB2_A1778AutorizacaoConsumo_UnidadeMedicaoNom[0];
               n1778AutorizacaoConsumo_UnidadeMedicaoNom = H00NB2_n1778AutorizacaoConsumo_UnidadeMedicaoNom[0];
               A1776AutorizacaoConsumo_VigenciaFim = H00NB2_A1776AutorizacaoConsumo_VigenciaFim[0];
               A1775AutorizacaoConsumo_VigenciaInicio = H00NB2_A1775AutorizacaoConsumo_VigenciaInicio[0];
               A1774AutorizacaoConsumo_Codigo = H00NB2_A1774AutorizacaoConsumo_Codigo[0];
               A1778AutorizacaoConsumo_UnidadeMedicaoNom = H00NB2_A1778AutorizacaoConsumo_UnidadeMedicaoNom[0];
               n1778AutorizacaoConsumo_UnidadeMedicaoNom = H00NB2_n1778AutorizacaoConsumo_UnidadeMedicaoNom[0];
               /* Execute user event: E26NB2 */
               E26NB2 ();
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 19;
            WBNB0( ) ;
         }
         nGXsfl_19_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              A1787AutorizacaoConsumo_Status ,
                                              AV52TFAutorizacaoConsumo_Status_Sels ,
                                              AV23TFAutorizacaoConsumo_Codigo ,
                                              AV24TFAutorizacaoConsumo_Codigo_To ,
                                              AV27TFAutorizacaoConsumo_VigenciaInicio ,
                                              AV28TFAutorizacaoConsumo_VigenciaInicio_To ,
                                              AV33TFAutorizacaoConsumo_VigenciaFim ,
                                              AV34TFAutorizacaoConsumo_VigenciaFim_To ,
                                              AV40TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel ,
                                              AV39TFAutorizacaoConsumo_UnidadeMedicaoNom ,
                                              AV43TFAutorizacaoConsumo_Quantidade ,
                                              AV44TFAutorizacaoConsumo_Quantidade_To ,
                                              AV47TFAutorizacaoConsumo_Valor ,
                                              AV48TFAutorizacaoConsumo_Valor_To ,
                                              AV52TFAutorizacaoConsumo_Status_Sels.Count ,
                                              AV55TFAutorizacaoConsumo_Ativo_Sel ,
                                              A1774AutorizacaoConsumo_Codigo ,
                                              A1775AutorizacaoConsumo_VigenciaInicio ,
                                              A1776AutorizacaoConsumo_VigenciaFim ,
                                              A1778AutorizacaoConsumo_UnidadeMedicaoNom ,
                                              A1779AutorizacaoConsumo_Quantidade ,
                                              A1782AutorizacaoConsumo_Valor ,
                                              A1780AutorizacaoConsumo_Ativo ,
                                              AV13OrderedBy ,
                                              AV14OrderedDsc ,
                                              A74Contrato_Codigo ,
                                              AV7Contrato_Codigo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.INT, TypeConstants.INT, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT,
                                              TypeConstants.SHORT, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.INT, TypeConstants.SHORT, TypeConstants.INT, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.BOOLEAN,
                                              TypeConstants.SHORT, TypeConstants.DECIMAL, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.INT
                                              }
         });
         lV39TFAutorizacaoConsumo_UnidadeMedicaoNom = StringUtil.PadR( StringUtil.RTrim( AV39TFAutorizacaoConsumo_UnidadeMedicaoNom), 50, "%");
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV39TFAutorizacaoConsumo_UnidadeMedicaoNom", AV39TFAutorizacaoConsumo_UnidadeMedicaoNom);
         /* Using cursor H00NB3 */
         pr_default.execute(1, new Object[] {AV7Contrato_Codigo, AV23TFAutorizacaoConsumo_Codigo, AV24TFAutorizacaoConsumo_Codigo_To, AV27TFAutorizacaoConsumo_VigenciaInicio, AV28TFAutorizacaoConsumo_VigenciaInicio_To, AV33TFAutorizacaoConsumo_VigenciaFim, AV34TFAutorizacaoConsumo_VigenciaFim_To, lV39TFAutorizacaoConsumo_UnidadeMedicaoNom, AV40TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel, AV43TFAutorizacaoConsumo_Quantidade, AV44TFAutorizacaoConsumo_Quantidade_To, AV47TFAutorizacaoConsumo_Valor, AV48TFAutorizacaoConsumo_Valor_To});
         GRID_nRecordCount = H00NB3_AGRID_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV23TFAutorizacaoConsumo_Codigo, AV24TFAutorizacaoConsumo_Codigo_To, AV27TFAutorizacaoConsumo_VigenciaInicio, AV28TFAutorizacaoConsumo_VigenciaInicio_To, AV33TFAutorizacaoConsumo_VigenciaFim, AV34TFAutorizacaoConsumo_VigenciaFim_To, AV39TFAutorizacaoConsumo_UnidadeMedicaoNom, AV40TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel, AV43TFAutorizacaoConsumo_Quantidade, AV44TFAutorizacaoConsumo_Quantidade_To, AV47TFAutorizacaoConsumo_Valor, AV48TFAutorizacaoConsumo_Valor_To, AV55TFAutorizacaoConsumo_Ativo_Sel, AV7Contrato_Codigo, AV25ddo_AutorizacaoConsumo_CodigoTitleControlIdToReplace, AV31ddo_AutorizacaoConsumo_VigenciaInicioTitleControlIdToReplace, AV37ddo_AutorizacaoConsumo_VigenciaFimTitleControlIdToReplace, AV41ddo_AutorizacaoConsumo_UnidadeMedicaoNomTitleControlIdToReplace, AV45ddo_AutorizacaoConsumo_QuantidadeTitleControlIdToReplace, AV49ddo_AutorizacaoConsumo_ValorTitleControlIdToReplace, AV53ddo_AutorizacaoConsumo_StatusTitleControlIdToReplace, AV56ddo_AutorizacaoConsumo_AtivoTitleControlIdToReplace, AV66Pgmname, AV52TFAutorizacaoConsumo_Status_Sels, A1780AutorizacaoConsumo_Ativo, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV23TFAutorizacaoConsumo_Codigo, AV24TFAutorizacaoConsumo_Codigo_To, AV27TFAutorizacaoConsumo_VigenciaInicio, AV28TFAutorizacaoConsumo_VigenciaInicio_To, AV33TFAutorizacaoConsumo_VigenciaFim, AV34TFAutorizacaoConsumo_VigenciaFim_To, AV39TFAutorizacaoConsumo_UnidadeMedicaoNom, AV40TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel, AV43TFAutorizacaoConsumo_Quantidade, AV44TFAutorizacaoConsumo_Quantidade_To, AV47TFAutorizacaoConsumo_Valor, AV48TFAutorizacaoConsumo_Valor_To, AV55TFAutorizacaoConsumo_Ativo_Sel, AV7Contrato_Codigo, AV25ddo_AutorizacaoConsumo_CodigoTitleControlIdToReplace, AV31ddo_AutorizacaoConsumo_VigenciaInicioTitleControlIdToReplace, AV37ddo_AutorizacaoConsumo_VigenciaFimTitleControlIdToReplace, AV41ddo_AutorizacaoConsumo_UnidadeMedicaoNomTitleControlIdToReplace, AV45ddo_AutorizacaoConsumo_QuantidadeTitleControlIdToReplace, AV49ddo_AutorizacaoConsumo_ValorTitleControlIdToReplace, AV53ddo_AutorizacaoConsumo_StatusTitleControlIdToReplace, AV56ddo_AutorizacaoConsumo_AtivoTitleControlIdToReplace, AV66Pgmname, AV52TFAutorizacaoConsumo_Status_Sels, A1780AutorizacaoConsumo_Ativo, sPrefix) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV23TFAutorizacaoConsumo_Codigo, AV24TFAutorizacaoConsumo_Codigo_To, AV27TFAutorizacaoConsumo_VigenciaInicio, AV28TFAutorizacaoConsumo_VigenciaInicio_To, AV33TFAutorizacaoConsumo_VigenciaFim, AV34TFAutorizacaoConsumo_VigenciaFim_To, AV39TFAutorizacaoConsumo_UnidadeMedicaoNom, AV40TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel, AV43TFAutorizacaoConsumo_Quantidade, AV44TFAutorizacaoConsumo_Quantidade_To, AV47TFAutorizacaoConsumo_Valor, AV48TFAutorizacaoConsumo_Valor_To, AV55TFAutorizacaoConsumo_Ativo_Sel, AV7Contrato_Codigo, AV25ddo_AutorizacaoConsumo_CodigoTitleControlIdToReplace, AV31ddo_AutorizacaoConsumo_VigenciaInicioTitleControlIdToReplace, AV37ddo_AutorizacaoConsumo_VigenciaFimTitleControlIdToReplace, AV41ddo_AutorizacaoConsumo_UnidadeMedicaoNomTitleControlIdToReplace, AV45ddo_AutorizacaoConsumo_QuantidadeTitleControlIdToReplace, AV49ddo_AutorizacaoConsumo_ValorTitleControlIdToReplace, AV53ddo_AutorizacaoConsumo_StatusTitleControlIdToReplace, AV56ddo_AutorizacaoConsumo_AtivoTitleControlIdToReplace, AV66Pgmname, AV52TFAutorizacaoConsumo_Status_Sels, A1780AutorizacaoConsumo_Ativo, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV23TFAutorizacaoConsumo_Codigo, AV24TFAutorizacaoConsumo_Codigo_To, AV27TFAutorizacaoConsumo_VigenciaInicio, AV28TFAutorizacaoConsumo_VigenciaInicio_To, AV33TFAutorizacaoConsumo_VigenciaFim, AV34TFAutorizacaoConsumo_VigenciaFim_To, AV39TFAutorizacaoConsumo_UnidadeMedicaoNom, AV40TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel, AV43TFAutorizacaoConsumo_Quantidade, AV44TFAutorizacaoConsumo_Quantidade_To, AV47TFAutorizacaoConsumo_Valor, AV48TFAutorizacaoConsumo_Valor_To, AV55TFAutorizacaoConsumo_Ativo_Sel, AV7Contrato_Codigo, AV25ddo_AutorizacaoConsumo_CodigoTitleControlIdToReplace, AV31ddo_AutorizacaoConsumo_VigenciaInicioTitleControlIdToReplace, AV37ddo_AutorizacaoConsumo_VigenciaFimTitleControlIdToReplace, AV41ddo_AutorizacaoConsumo_UnidadeMedicaoNomTitleControlIdToReplace, AV45ddo_AutorizacaoConsumo_QuantidadeTitleControlIdToReplace, AV49ddo_AutorizacaoConsumo_ValorTitleControlIdToReplace, AV53ddo_AutorizacaoConsumo_StatusTitleControlIdToReplace, AV56ddo_AutorizacaoConsumo_AtivoTitleControlIdToReplace, AV66Pgmname, AV52TFAutorizacaoConsumo_Status_Sels, A1780AutorizacaoConsumo_Ativo, sPrefix) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV23TFAutorizacaoConsumo_Codigo, AV24TFAutorizacaoConsumo_Codigo_To, AV27TFAutorizacaoConsumo_VigenciaInicio, AV28TFAutorizacaoConsumo_VigenciaInicio_To, AV33TFAutorizacaoConsumo_VigenciaFim, AV34TFAutorizacaoConsumo_VigenciaFim_To, AV39TFAutorizacaoConsumo_UnidadeMedicaoNom, AV40TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel, AV43TFAutorizacaoConsumo_Quantidade, AV44TFAutorizacaoConsumo_Quantidade_To, AV47TFAutorizacaoConsumo_Valor, AV48TFAutorizacaoConsumo_Valor_To, AV55TFAutorizacaoConsumo_Ativo_Sel, AV7Contrato_Codigo, AV25ddo_AutorizacaoConsumo_CodigoTitleControlIdToReplace, AV31ddo_AutorizacaoConsumo_VigenciaInicioTitleControlIdToReplace, AV37ddo_AutorizacaoConsumo_VigenciaFimTitleControlIdToReplace, AV41ddo_AutorizacaoConsumo_UnidadeMedicaoNomTitleControlIdToReplace, AV45ddo_AutorizacaoConsumo_QuantidadeTitleControlIdToReplace, AV49ddo_AutorizacaoConsumo_ValorTitleControlIdToReplace, AV53ddo_AutorizacaoConsumo_StatusTitleControlIdToReplace, AV56ddo_AutorizacaoConsumo_AtivoTitleControlIdToReplace, AV66Pgmname, AV52TFAutorizacaoConsumo_Status_Sels, A1780AutorizacaoConsumo_Ativo, sPrefix) ;
         }
         return (int)(0) ;
      }

      protected void STRUPNB0( )
      {
         /* Before Start, stand alone formulas. */
         AV66Pgmname = "ContratoAutorizacaoConsumoWC";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E24NB2 */
         E24NB2 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vDDO_TITLESETTINGSICONS"), AV57DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vAUTORIZACAOCONSUMO_CODIGOTITLEFILTERDATA"), AV22AutorizacaoConsumo_CodigoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vAUTORIZACAOCONSUMO_VIGENCIAINICIOTITLEFILTERDATA"), AV26AutorizacaoConsumo_VigenciaInicioTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vAUTORIZACAOCONSUMO_VIGENCIAFIMTITLEFILTERDATA"), AV32AutorizacaoConsumo_VigenciaFimTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vAUTORIZACAOCONSUMO_UNIDADEMEDICAONOMTITLEFILTERDATA"), AV38AutorizacaoConsumo_UnidadeMedicaoNomTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vAUTORIZACAOCONSUMO_QUANTIDADETITLEFILTERDATA"), AV42AutorizacaoConsumo_QuantidadeTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vAUTORIZACAOCONSUMO_VALORTITLEFILTERDATA"), AV46AutorizacaoConsumo_ValorTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vAUTORIZACAOCONSUMO_STATUSTITLEFILTERDATA"), AV50AutorizacaoConsumo_StatusTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"vAUTORIZACAOCONSUMO_ATIVOTITLEFILTERDATA"), AV54AutorizacaoConsumo_AtivoTitleFilterData);
            /* Read variables values. */
            A74Contrato_Codigo = (int)(context.localUtil.CToN( cgiGet( edtContrato_Codigo_Internalname), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A74Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A74Contrato_Codigo), 6, 0)));
            if ( ( ( context.localUtil.CToN( cgiGet( edtavContrato_codigo_selected_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavContrato_codigo_selected_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vCONTRATO_CODIGO_SELECTED");
               GX_FocusControl = edtavContrato_codigo_selected_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV19Contrato_Codigo_Selected = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19Contrato_Codigo_Selected", StringUtil.LTrim( StringUtil.Str( (decimal)(AV19Contrato_Codigo_Selected), 6, 0)));
            }
            else
            {
               AV19Contrato_Codigo_Selected = (int)(context.localUtil.CToN( cgiGet( edtavContrato_codigo_selected_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19Contrato_Codigo_Selected", StringUtil.LTrim( StringUtil.Str( (decimal)(AV19Contrato_Codigo_Selected), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavAutorizacaoconsumo_codigo_selected_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavAutorizacaoconsumo_codigo_selected_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vAUTORIZACAOCONSUMO_CODIGO_SELECTED");
               GX_FocusControl = edtavAutorizacaoconsumo_codigo_selected_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV18AutorizacaoConsumo_Codigo_Selected = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18AutorizacaoConsumo_Codigo_Selected", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18AutorizacaoConsumo_Codigo_Selected), 6, 0)));
            }
            else
            {
               AV18AutorizacaoConsumo_Codigo_Selected = (int)(context.localUtil.CToN( cgiGet( edtavAutorizacaoconsumo_codigo_selected_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18AutorizacaoConsumo_Codigo_Selected", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18AutorizacaoConsumo_Codigo_Selected), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavOrderedby_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavOrderedby_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vORDEREDBY");
               GX_FocusControl = edtavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV13OrderedBy = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            }
            else
            {
               AV13OrderedBy = (short)(context.localUtil.CToN( cgiGet( edtavOrderedby_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            }
            AV14OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfautorizacaoconsumo_codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfautorizacaoconsumo_codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFAUTORIZACAOCONSUMO_CODIGO");
               GX_FocusControl = edtavTfautorizacaoconsumo_codigo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV23TFAutorizacaoConsumo_Codigo = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23TFAutorizacaoConsumo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV23TFAutorizacaoConsumo_Codigo), 6, 0)));
            }
            else
            {
               AV23TFAutorizacaoConsumo_Codigo = (int)(context.localUtil.CToN( cgiGet( edtavTfautorizacaoconsumo_codigo_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23TFAutorizacaoConsumo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV23TFAutorizacaoConsumo_Codigo), 6, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfautorizacaoconsumo_codigo_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfautorizacaoconsumo_codigo_to_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFAUTORIZACAOCONSUMO_CODIGO_TO");
               GX_FocusControl = edtavTfautorizacaoconsumo_codigo_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV24TFAutorizacaoConsumo_Codigo_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24TFAutorizacaoConsumo_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24TFAutorizacaoConsumo_Codigo_To), 6, 0)));
            }
            else
            {
               AV24TFAutorizacaoConsumo_Codigo_To = (int)(context.localUtil.CToN( cgiGet( edtavTfautorizacaoconsumo_codigo_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24TFAutorizacaoConsumo_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24TFAutorizacaoConsumo_Codigo_To), 6, 0)));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavTfautorizacaoconsumo_vigenciainicio_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"TFAutorizacao Consumo_Vigencia Inicio"}), 1, "vTFAUTORIZACAOCONSUMO_VIGENCIAINICIO");
               GX_FocusControl = edtavTfautorizacaoconsumo_vigenciainicio_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV27TFAutorizacaoConsumo_VigenciaInicio = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27TFAutorizacaoConsumo_VigenciaInicio", context.localUtil.Format(AV27TFAutorizacaoConsumo_VigenciaInicio, "99/99/99"));
            }
            else
            {
               AV27TFAutorizacaoConsumo_VigenciaInicio = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavTfautorizacaoconsumo_vigenciainicio_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27TFAutorizacaoConsumo_VigenciaInicio", context.localUtil.Format(AV27TFAutorizacaoConsumo_VigenciaInicio, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavTfautorizacaoconsumo_vigenciainicio_to_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"TFAutorizacao Consumo_Vigencia Inicio_To"}), 1, "vTFAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO");
               GX_FocusControl = edtavTfautorizacaoconsumo_vigenciainicio_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV28TFAutorizacaoConsumo_VigenciaInicio_To = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV28TFAutorizacaoConsumo_VigenciaInicio_To", context.localUtil.Format(AV28TFAutorizacaoConsumo_VigenciaInicio_To, "99/99/99"));
            }
            else
            {
               AV28TFAutorizacaoConsumo_VigenciaInicio_To = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavTfautorizacaoconsumo_vigenciainicio_to_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV28TFAutorizacaoConsumo_VigenciaInicio_To", context.localUtil.Format(AV28TFAutorizacaoConsumo_VigenciaInicio_To, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_autorizacaoconsumo_vigenciainicioauxdate_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Autorizacao Consumo_Vigencia Inicio Aux Date"}), 1, "vDDO_AUTORIZACAOCONSUMO_VIGENCIAINICIOAUXDATE");
               GX_FocusControl = edtavDdo_autorizacaoconsumo_vigenciainicioauxdate_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV29DDO_AutorizacaoConsumo_VigenciaInicioAuxDate = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29DDO_AutorizacaoConsumo_VigenciaInicioAuxDate", context.localUtil.Format(AV29DDO_AutorizacaoConsumo_VigenciaInicioAuxDate, "99/99/99"));
            }
            else
            {
               AV29DDO_AutorizacaoConsumo_VigenciaInicioAuxDate = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_autorizacaoconsumo_vigenciainicioauxdate_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29DDO_AutorizacaoConsumo_VigenciaInicioAuxDate", context.localUtil.Format(AV29DDO_AutorizacaoConsumo_VigenciaInicioAuxDate, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_autorizacaoconsumo_vigenciainicioauxdateto_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Autorizacao Consumo_Vigencia Inicio Aux Date To"}), 1, "vDDO_AUTORIZACAOCONSUMO_VIGENCIAINICIOAUXDATETO");
               GX_FocusControl = edtavDdo_autorizacaoconsumo_vigenciainicioauxdateto_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV30DDO_AutorizacaoConsumo_VigenciaInicioAuxDateTo = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30DDO_AutorizacaoConsumo_VigenciaInicioAuxDateTo", context.localUtil.Format(AV30DDO_AutorizacaoConsumo_VigenciaInicioAuxDateTo, "99/99/99"));
            }
            else
            {
               AV30DDO_AutorizacaoConsumo_VigenciaInicioAuxDateTo = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_autorizacaoconsumo_vigenciainicioauxdateto_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30DDO_AutorizacaoConsumo_VigenciaInicioAuxDateTo", context.localUtil.Format(AV30DDO_AutorizacaoConsumo_VigenciaInicioAuxDateTo, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavTfautorizacaoconsumo_vigenciafim_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"TFAutorizacao Consumo_Vigencia Fim"}), 1, "vTFAUTORIZACAOCONSUMO_VIGENCIAFIM");
               GX_FocusControl = edtavTfautorizacaoconsumo_vigenciafim_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV33TFAutorizacaoConsumo_VigenciaFim = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV33TFAutorizacaoConsumo_VigenciaFim", context.localUtil.Format(AV33TFAutorizacaoConsumo_VigenciaFim, "99/99/99"));
            }
            else
            {
               AV33TFAutorizacaoConsumo_VigenciaFim = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavTfautorizacaoconsumo_vigenciafim_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV33TFAutorizacaoConsumo_VigenciaFim", context.localUtil.Format(AV33TFAutorizacaoConsumo_VigenciaFim, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavTfautorizacaoconsumo_vigenciafim_to_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"TFAutorizacao Consumo_Vigencia Fim_To"}), 1, "vTFAUTORIZACAOCONSUMO_VIGENCIAFIM_TO");
               GX_FocusControl = edtavTfautorizacaoconsumo_vigenciafim_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV34TFAutorizacaoConsumo_VigenciaFim_To = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV34TFAutorizacaoConsumo_VigenciaFim_To", context.localUtil.Format(AV34TFAutorizacaoConsumo_VigenciaFim_To, "99/99/99"));
            }
            else
            {
               AV34TFAutorizacaoConsumo_VigenciaFim_To = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavTfautorizacaoconsumo_vigenciafim_to_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV34TFAutorizacaoConsumo_VigenciaFim_To", context.localUtil.Format(AV34TFAutorizacaoConsumo_VigenciaFim_To, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_autorizacaoconsumo_vigenciafimauxdate_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Autorizacao Consumo_Vigencia Fim Aux Date"}), 1, "vDDO_AUTORIZACAOCONSUMO_VIGENCIAFIMAUXDATE");
               GX_FocusControl = edtavDdo_autorizacaoconsumo_vigenciafimauxdate_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV35DDO_AutorizacaoConsumo_VigenciaFimAuxDate = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV35DDO_AutorizacaoConsumo_VigenciaFimAuxDate", context.localUtil.Format(AV35DDO_AutorizacaoConsumo_VigenciaFimAuxDate, "99/99/99"));
            }
            else
            {
               AV35DDO_AutorizacaoConsumo_VigenciaFimAuxDate = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_autorizacaoconsumo_vigenciafimauxdate_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV35DDO_AutorizacaoConsumo_VigenciaFimAuxDate", context.localUtil.Format(AV35DDO_AutorizacaoConsumo_VigenciaFimAuxDate, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavDdo_autorizacaoconsumo_vigenciafimauxdateto_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"DDO_Autorizacao Consumo_Vigencia Fim Aux Date To"}), 1, "vDDO_AUTORIZACAOCONSUMO_VIGENCIAFIMAUXDATETO");
               GX_FocusControl = edtavDdo_autorizacaoconsumo_vigenciafimauxdateto_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV36DDO_AutorizacaoConsumo_VigenciaFimAuxDateTo = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV36DDO_AutorizacaoConsumo_VigenciaFimAuxDateTo", context.localUtil.Format(AV36DDO_AutorizacaoConsumo_VigenciaFimAuxDateTo, "99/99/99"));
            }
            else
            {
               AV36DDO_AutorizacaoConsumo_VigenciaFimAuxDateTo = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavDdo_autorizacaoconsumo_vigenciafimauxdateto_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV36DDO_AutorizacaoConsumo_VigenciaFimAuxDateTo", context.localUtil.Format(AV36DDO_AutorizacaoConsumo_VigenciaFimAuxDateTo, "99/99/99"));
            }
            AV39TFAutorizacaoConsumo_UnidadeMedicaoNom = StringUtil.Upper( cgiGet( edtavTfautorizacaoconsumo_unidademedicaonom_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV39TFAutorizacaoConsumo_UnidadeMedicaoNom", AV39TFAutorizacaoConsumo_UnidadeMedicaoNom);
            AV40TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel = StringUtil.Upper( cgiGet( edtavTfautorizacaoconsumo_unidademedicaonom_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV40TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel", AV40TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfautorizacaoconsumo_quantidade_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfautorizacaoconsumo_quantidade_Internalname), ",", ".") > Convert.ToDecimal( 999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFAUTORIZACAOCONSUMO_QUANTIDADE");
               GX_FocusControl = edtavTfautorizacaoconsumo_quantidade_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV43TFAutorizacaoConsumo_Quantidade = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV43TFAutorizacaoConsumo_Quantidade", StringUtil.LTrim( StringUtil.Str( (decimal)(AV43TFAutorizacaoConsumo_Quantidade), 3, 0)));
            }
            else
            {
               AV43TFAutorizacaoConsumo_Quantidade = (short)(context.localUtil.CToN( cgiGet( edtavTfautorizacaoconsumo_quantidade_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV43TFAutorizacaoConsumo_Quantidade", StringUtil.LTrim( StringUtil.Str( (decimal)(AV43TFAutorizacaoConsumo_Quantidade), 3, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfautorizacaoconsumo_quantidade_to_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfautorizacaoconsumo_quantidade_to_Internalname), ",", ".") > Convert.ToDecimal( 999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFAUTORIZACAOCONSUMO_QUANTIDADE_TO");
               GX_FocusControl = edtavTfautorizacaoconsumo_quantidade_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV44TFAutorizacaoConsumo_Quantidade_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV44TFAutorizacaoConsumo_Quantidade_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV44TFAutorizacaoConsumo_Quantidade_To), 3, 0)));
            }
            else
            {
               AV44TFAutorizacaoConsumo_Quantidade_To = (short)(context.localUtil.CToN( cgiGet( edtavTfautorizacaoconsumo_quantidade_to_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV44TFAutorizacaoConsumo_Quantidade_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV44TFAutorizacaoConsumo_Quantidade_To), 3, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfautorizacaoconsumo_valor_Internalname), ",", ".") < -99999999999.99999m ) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfautorizacaoconsumo_valor_Internalname), ",", ".") > 999999999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFAUTORIZACAOCONSUMO_VALOR");
               GX_FocusControl = edtavTfautorizacaoconsumo_valor_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV47TFAutorizacaoConsumo_Valor = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV47TFAutorizacaoConsumo_Valor", StringUtil.LTrim( StringUtil.Str( AV47TFAutorizacaoConsumo_Valor, 18, 5)));
            }
            else
            {
               AV47TFAutorizacaoConsumo_Valor = context.localUtil.CToN( cgiGet( edtavTfautorizacaoconsumo_valor_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV47TFAutorizacaoConsumo_Valor", StringUtil.LTrim( StringUtil.Str( AV47TFAutorizacaoConsumo_Valor, 18, 5)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfautorizacaoconsumo_valor_to_Internalname), ",", ".") < -99999999999.99999m ) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfautorizacaoconsumo_valor_to_Internalname), ",", ".") > 999999999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFAUTORIZACAOCONSUMO_VALOR_TO");
               GX_FocusControl = edtavTfautorizacaoconsumo_valor_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV48TFAutorizacaoConsumo_Valor_To = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV48TFAutorizacaoConsumo_Valor_To", StringUtil.LTrim( StringUtil.Str( AV48TFAutorizacaoConsumo_Valor_To, 18, 5)));
            }
            else
            {
               AV48TFAutorizacaoConsumo_Valor_To = context.localUtil.CToN( cgiGet( edtavTfautorizacaoconsumo_valor_to_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV48TFAutorizacaoConsumo_Valor_To", StringUtil.LTrim( StringUtil.Str( AV48TFAutorizacaoConsumo_Valor_To, 18, 5)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfautorizacaoconsumo_ativo_sel_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfautorizacaoconsumo_ativo_sel_Internalname), ",", ".") > Convert.ToDecimal( 9 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFAUTORIZACAOCONSUMO_ATIVO_SEL");
               GX_FocusControl = edtavTfautorizacaoconsumo_ativo_sel_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV55TFAutorizacaoConsumo_Ativo_Sel = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV55TFAutorizacaoConsumo_Ativo_Sel", StringUtil.Str( (decimal)(AV55TFAutorizacaoConsumo_Ativo_Sel), 1, 0));
            }
            else
            {
               AV55TFAutorizacaoConsumo_Ativo_Sel = (short)(context.localUtil.CToN( cgiGet( edtavTfautorizacaoconsumo_ativo_sel_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV55TFAutorizacaoConsumo_Ativo_Sel", StringUtil.Str( (decimal)(AV55TFAutorizacaoConsumo_Ativo_Sel), 1, 0));
            }
            AV25ddo_AutorizacaoConsumo_CodigoTitleControlIdToReplace = cgiGet( edtavDdo_autorizacaoconsumo_codigotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25ddo_AutorizacaoConsumo_CodigoTitleControlIdToReplace", AV25ddo_AutorizacaoConsumo_CodigoTitleControlIdToReplace);
            AV31ddo_AutorizacaoConsumo_VigenciaInicioTitleControlIdToReplace = cgiGet( edtavDdo_autorizacaoconsumo_vigenciainiciotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV31ddo_AutorizacaoConsumo_VigenciaInicioTitleControlIdToReplace", AV31ddo_AutorizacaoConsumo_VigenciaInicioTitleControlIdToReplace);
            AV37ddo_AutorizacaoConsumo_VigenciaFimTitleControlIdToReplace = cgiGet( edtavDdo_autorizacaoconsumo_vigenciafimtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV37ddo_AutorizacaoConsumo_VigenciaFimTitleControlIdToReplace", AV37ddo_AutorizacaoConsumo_VigenciaFimTitleControlIdToReplace);
            AV41ddo_AutorizacaoConsumo_UnidadeMedicaoNomTitleControlIdToReplace = cgiGet( edtavDdo_autorizacaoconsumo_unidademedicaonomtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV41ddo_AutorizacaoConsumo_UnidadeMedicaoNomTitleControlIdToReplace", AV41ddo_AutorizacaoConsumo_UnidadeMedicaoNomTitleControlIdToReplace);
            AV45ddo_AutorizacaoConsumo_QuantidadeTitleControlIdToReplace = cgiGet( edtavDdo_autorizacaoconsumo_quantidadetitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV45ddo_AutorizacaoConsumo_QuantidadeTitleControlIdToReplace", AV45ddo_AutorizacaoConsumo_QuantidadeTitleControlIdToReplace);
            AV49ddo_AutorizacaoConsumo_ValorTitleControlIdToReplace = cgiGet( edtavDdo_autorizacaoconsumo_valortitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV49ddo_AutorizacaoConsumo_ValorTitleControlIdToReplace", AV49ddo_AutorizacaoConsumo_ValorTitleControlIdToReplace);
            AV53ddo_AutorizacaoConsumo_StatusTitleControlIdToReplace = cgiGet( edtavDdo_autorizacaoconsumo_statustitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV53ddo_AutorizacaoConsumo_StatusTitleControlIdToReplace", AV53ddo_AutorizacaoConsumo_StatusTitleControlIdToReplace);
            AV56ddo_AutorizacaoConsumo_AtivoTitleControlIdToReplace = cgiGet( edtavDdo_autorizacaoconsumo_ativotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV56ddo_AutorizacaoConsumo_AtivoTitleControlIdToReplace", AV56ddo_AutorizacaoConsumo_AtivoTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_19 = (short)(context.localUtil.CToN( cgiGet( sPrefix+"nRC_GXsfl_19"), ",", "."));
            AV59GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( sPrefix+"vGRIDCURRENTPAGE"), ",", "."));
            AV60GridPageCount = (long)(context.localUtil.CToN( cgiGet( sPrefix+"vGRIDPAGECOUNT"), ",", "."));
            wcpOAV7Contrato_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV7Contrato_Codigo"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( sPrefix+"GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Emptygridcaption");
            Dvelop_confirmpanel_cancelarautorizcaoconsumo_Title = cgiGet( sPrefix+"DVELOP_CONFIRMPANEL_CANCELARAUTORIZCAOCONSUMO_Title");
            Dvelop_confirmpanel_cancelarautorizcaoconsumo_Confirmationtext = cgiGet( sPrefix+"DVELOP_CONFIRMPANEL_CANCELARAUTORIZCAOCONSUMO_Confirmationtext");
            Dvelop_confirmpanel_cancelarautorizcaoconsumo_Yesbuttoncaption = cgiGet( sPrefix+"DVELOP_CONFIRMPANEL_CANCELARAUTORIZCAOCONSUMO_Yesbuttoncaption");
            Dvelop_confirmpanel_cancelarautorizcaoconsumo_Nobuttoncaption = cgiGet( sPrefix+"DVELOP_CONFIRMPANEL_CANCELARAUTORIZCAOCONSUMO_Nobuttoncaption");
            Dvelop_confirmpanel_cancelarautorizcaoconsumo_Cancelbuttoncaption = cgiGet( sPrefix+"DVELOP_CONFIRMPANEL_CANCELARAUTORIZCAOCONSUMO_Cancelbuttoncaption");
            Dvelop_confirmpanel_cancelarautorizcaoconsumo_Yesbuttonposition = cgiGet( sPrefix+"DVELOP_CONFIRMPANEL_CANCELARAUTORIZCAOCONSUMO_Yesbuttonposition");
            Dvelop_confirmpanel_cancelarautorizcaoconsumo_Confirmtype = cgiGet( sPrefix+"DVELOP_CONFIRMPANEL_CANCELARAUTORIZCAOCONSUMO_Confirmtype");
            Dvelop_confirmpanel_autorizar_Title = cgiGet( sPrefix+"DVELOP_CONFIRMPANEL_AUTORIZAR_Title");
            Dvelop_confirmpanel_autorizar_Confirmationtext = cgiGet( sPrefix+"DVELOP_CONFIRMPANEL_AUTORIZAR_Confirmationtext");
            Dvelop_confirmpanel_autorizar_Yesbuttoncaption = cgiGet( sPrefix+"DVELOP_CONFIRMPANEL_AUTORIZAR_Yesbuttoncaption");
            Dvelop_confirmpanel_autorizar_Nobuttoncaption = cgiGet( sPrefix+"DVELOP_CONFIRMPANEL_AUTORIZAR_Nobuttoncaption");
            Dvelop_confirmpanel_autorizar_Cancelbuttoncaption = cgiGet( sPrefix+"DVELOP_CONFIRMPANEL_AUTORIZAR_Cancelbuttoncaption");
            Dvelop_confirmpanel_autorizar_Yesbuttonposition = cgiGet( sPrefix+"DVELOP_CONFIRMPANEL_AUTORIZAR_Yesbuttonposition");
            Dvelop_confirmpanel_autorizar_Confirmtype = cgiGet( sPrefix+"DVELOP_CONFIRMPANEL_AUTORIZAR_Confirmtype");
            Dvelop_confirmpanel_encerrar_Title = cgiGet( sPrefix+"DVELOP_CONFIRMPANEL_ENCERRAR_Title");
            Dvelop_confirmpanel_encerrar_Confirmationtext = cgiGet( sPrefix+"DVELOP_CONFIRMPANEL_ENCERRAR_Confirmationtext");
            Dvelop_confirmpanel_encerrar_Yesbuttoncaption = cgiGet( sPrefix+"DVELOP_CONFIRMPANEL_ENCERRAR_Yesbuttoncaption");
            Dvelop_confirmpanel_encerrar_Nobuttoncaption = cgiGet( sPrefix+"DVELOP_CONFIRMPANEL_ENCERRAR_Nobuttoncaption");
            Dvelop_confirmpanel_encerrar_Cancelbuttoncaption = cgiGet( sPrefix+"DVELOP_CONFIRMPANEL_ENCERRAR_Cancelbuttoncaption");
            Dvelop_confirmpanel_encerrar_Yesbuttonposition = cgiGet( sPrefix+"DVELOP_CONFIRMPANEL_ENCERRAR_Yesbuttonposition");
            Dvelop_confirmpanel_encerrar_Confirmtype = cgiGet( sPrefix+"DVELOP_CONFIRMPANEL_ENCERRAR_Confirmtype");
            Ddo_autorizacaoconsumo_codigo_Caption = cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_CODIGO_Caption");
            Ddo_autorizacaoconsumo_codigo_Tooltip = cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_CODIGO_Tooltip");
            Ddo_autorizacaoconsumo_codigo_Cls = cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_CODIGO_Cls");
            Ddo_autorizacaoconsumo_codigo_Filteredtext_set = cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_CODIGO_Filteredtext_set");
            Ddo_autorizacaoconsumo_codigo_Filteredtextto_set = cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_CODIGO_Filteredtextto_set");
            Ddo_autorizacaoconsumo_codigo_Dropdownoptionstype = cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_CODIGO_Dropdownoptionstype");
            Ddo_autorizacaoconsumo_codigo_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_CODIGO_Titlecontrolidtoreplace");
            Ddo_autorizacaoconsumo_codigo_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_CODIGO_Includesortasc"));
            Ddo_autorizacaoconsumo_codigo_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_CODIGO_Includesortdsc"));
            Ddo_autorizacaoconsumo_codigo_Sortedstatus = cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_CODIGO_Sortedstatus");
            Ddo_autorizacaoconsumo_codigo_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_CODIGO_Includefilter"));
            Ddo_autorizacaoconsumo_codigo_Filtertype = cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_CODIGO_Filtertype");
            Ddo_autorizacaoconsumo_codigo_Filterisrange = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_CODIGO_Filterisrange"));
            Ddo_autorizacaoconsumo_codigo_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_CODIGO_Includedatalist"));
            Ddo_autorizacaoconsumo_codigo_Sortasc = cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_CODIGO_Sortasc");
            Ddo_autorizacaoconsumo_codigo_Sortdsc = cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_CODIGO_Sortdsc");
            Ddo_autorizacaoconsumo_codigo_Cleanfilter = cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_CODIGO_Cleanfilter");
            Ddo_autorizacaoconsumo_codigo_Rangefilterfrom = cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_CODIGO_Rangefilterfrom");
            Ddo_autorizacaoconsumo_codigo_Rangefilterto = cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_CODIGO_Rangefilterto");
            Ddo_autorizacaoconsumo_codigo_Searchbuttontext = cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_CODIGO_Searchbuttontext");
            Ddo_autorizacaoconsumo_vigenciainicio_Caption = cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIO_Caption");
            Ddo_autorizacaoconsumo_vigenciainicio_Tooltip = cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIO_Tooltip");
            Ddo_autorizacaoconsumo_vigenciainicio_Cls = cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIO_Cls");
            Ddo_autorizacaoconsumo_vigenciainicio_Filteredtext_set = cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIO_Filteredtext_set");
            Ddo_autorizacaoconsumo_vigenciainicio_Filteredtextto_set = cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIO_Filteredtextto_set");
            Ddo_autorizacaoconsumo_vigenciainicio_Dropdownoptionstype = cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIO_Dropdownoptionstype");
            Ddo_autorizacaoconsumo_vigenciainicio_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIO_Titlecontrolidtoreplace");
            Ddo_autorizacaoconsumo_vigenciainicio_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIO_Includesortasc"));
            Ddo_autorizacaoconsumo_vigenciainicio_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIO_Includesortdsc"));
            Ddo_autorizacaoconsumo_vigenciainicio_Sortedstatus = cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIO_Sortedstatus");
            Ddo_autorizacaoconsumo_vigenciainicio_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIO_Includefilter"));
            Ddo_autorizacaoconsumo_vigenciainicio_Filtertype = cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIO_Filtertype");
            Ddo_autorizacaoconsumo_vigenciainicio_Filterisrange = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIO_Filterisrange"));
            Ddo_autorizacaoconsumo_vigenciainicio_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIO_Includedatalist"));
            Ddo_autorizacaoconsumo_vigenciainicio_Sortasc = cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIO_Sortasc");
            Ddo_autorizacaoconsumo_vigenciainicio_Sortdsc = cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIO_Sortdsc");
            Ddo_autorizacaoconsumo_vigenciainicio_Cleanfilter = cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIO_Cleanfilter");
            Ddo_autorizacaoconsumo_vigenciainicio_Rangefilterfrom = cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIO_Rangefilterfrom");
            Ddo_autorizacaoconsumo_vigenciainicio_Rangefilterto = cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIO_Rangefilterto");
            Ddo_autorizacaoconsumo_vigenciainicio_Searchbuttontext = cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIO_Searchbuttontext");
            Ddo_autorizacaoconsumo_vigenciafim_Caption = cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_VIGENCIAFIM_Caption");
            Ddo_autorizacaoconsumo_vigenciafim_Tooltip = cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_VIGENCIAFIM_Tooltip");
            Ddo_autorizacaoconsumo_vigenciafim_Cls = cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_VIGENCIAFIM_Cls");
            Ddo_autorizacaoconsumo_vigenciafim_Filteredtext_set = cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_VIGENCIAFIM_Filteredtext_set");
            Ddo_autorizacaoconsumo_vigenciafim_Filteredtextto_set = cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_VIGENCIAFIM_Filteredtextto_set");
            Ddo_autorizacaoconsumo_vigenciafim_Dropdownoptionstype = cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_VIGENCIAFIM_Dropdownoptionstype");
            Ddo_autorizacaoconsumo_vigenciafim_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_VIGENCIAFIM_Titlecontrolidtoreplace");
            Ddo_autorizacaoconsumo_vigenciafim_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_VIGENCIAFIM_Includesortasc"));
            Ddo_autorizacaoconsumo_vigenciafim_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_VIGENCIAFIM_Includesortdsc"));
            Ddo_autorizacaoconsumo_vigenciafim_Sortedstatus = cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_VIGENCIAFIM_Sortedstatus");
            Ddo_autorizacaoconsumo_vigenciafim_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_VIGENCIAFIM_Includefilter"));
            Ddo_autorizacaoconsumo_vigenciafim_Filtertype = cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_VIGENCIAFIM_Filtertype");
            Ddo_autorizacaoconsumo_vigenciafim_Filterisrange = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_VIGENCIAFIM_Filterisrange"));
            Ddo_autorizacaoconsumo_vigenciafim_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_VIGENCIAFIM_Includedatalist"));
            Ddo_autorizacaoconsumo_vigenciafim_Sortasc = cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_VIGENCIAFIM_Sortasc");
            Ddo_autorizacaoconsumo_vigenciafim_Sortdsc = cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_VIGENCIAFIM_Sortdsc");
            Ddo_autorizacaoconsumo_vigenciafim_Cleanfilter = cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_VIGENCIAFIM_Cleanfilter");
            Ddo_autorizacaoconsumo_vigenciafim_Rangefilterfrom = cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_VIGENCIAFIM_Rangefilterfrom");
            Ddo_autorizacaoconsumo_vigenciafim_Rangefilterto = cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_VIGENCIAFIM_Rangefilterto");
            Ddo_autorizacaoconsumo_vigenciafim_Searchbuttontext = cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_VIGENCIAFIM_Searchbuttontext");
            Ddo_autorizacaoconsumo_unidademedicaonom_Caption = cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_Caption");
            Ddo_autorizacaoconsumo_unidademedicaonom_Tooltip = cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_Tooltip");
            Ddo_autorizacaoconsumo_unidademedicaonom_Cls = cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_Cls");
            Ddo_autorizacaoconsumo_unidademedicaonom_Filteredtext_set = cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_Filteredtext_set");
            Ddo_autorizacaoconsumo_unidademedicaonom_Selectedvalue_set = cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_Selectedvalue_set");
            Ddo_autorizacaoconsumo_unidademedicaonom_Dropdownoptionstype = cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_Dropdownoptionstype");
            Ddo_autorizacaoconsumo_unidademedicaonom_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_Titlecontrolidtoreplace");
            Ddo_autorizacaoconsumo_unidademedicaonom_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_Includesortasc"));
            Ddo_autorizacaoconsumo_unidademedicaonom_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_Includesortdsc"));
            Ddo_autorizacaoconsumo_unidademedicaonom_Sortedstatus = cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_Sortedstatus");
            Ddo_autorizacaoconsumo_unidademedicaonom_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_Includefilter"));
            Ddo_autorizacaoconsumo_unidademedicaonom_Filtertype = cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_Filtertype");
            Ddo_autorizacaoconsumo_unidademedicaonom_Filterisrange = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_Filterisrange"));
            Ddo_autorizacaoconsumo_unidademedicaonom_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_Includedatalist"));
            Ddo_autorizacaoconsumo_unidademedicaonom_Datalisttype = cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_Datalisttype");
            Ddo_autorizacaoconsumo_unidademedicaonom_Datalistproc = cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_Datalistproc");
            Ddo_autorizacaoconsumo_unidademedicaonom_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_autorizacaoconsumo_unidademedicaonom_Sortasc = cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_Sortasc");
            Ddo_autorizacaoconsumo_unidademedicaonom_Sortdsc = cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_Sortdsc");
            Ddo_autorizacaoconsumo_unidademedicaonom_Loadingdata = cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_Loadingdata");
            Ddo_autorizacaoconsumo_unidademedicaonom_Cleanfilter = cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_Cleanfilter");
            Ddo_autorizacaoconsumo_unidademedicaonom_Noresultsfound = cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_Noresultsfound");
            Ddo_autorizacaoconsumo_unidademedicaonom_Searchbuttontext = cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_Searchbuttontext");
            Ddo_autorizacaoconsumo_quantidade_Caption = cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_QUANTIDADE_Caption");
            Ddo_autorizacaoconsumo_quantidade_Tooltip = cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_QUANTIDADE_Tooltip");
            Ddo_autorizacaoconsumo_quantidade_Cls = cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_QUANTIDADE_Cls");
            Ddo_autorizacaoconsumo_quantidade_Filteredtext_set = cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_QUANTIDADE_Filteredtext_set");
            Ddo_autorizacaoconsumo_quantidade_Filteredtextto_set = cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_QUANTIDADE_Filteredtextto_set");
            Ddo_autorizacaoconsumo_quantidade_Dropdownoptionstype = cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_QUANTIDADE_Dropdownoptionstype");
            Ddo_autorizacaoconsumo_quantidade_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_QUANTIDADE_Titlecontrolidtoreplace");
            Ddo_autorizacaoconsumo_quantidade_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_QUANTIDADE_Includesortasc"));
            Ddo_autorizacaoconsumo_quantidade_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_QUANTIDADE_Includesortdsc"));
            Ddo_autorizacaoconsumo_quantidade_Sortedstatus = cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_QUANTIDADE_Sortedstatus");
            Ddo_autorizacaoconsumo_quantidade_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_QUANTIDADE_Includefilter"));
            Ddo_autorizacaoconsumo_quantidade_Filtertype = cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_QUANTIDADE_Filtertype");
            Ddo_autorizacaoconsumo_quantidade_Filterisrange = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_QUANTIDADE_Filterisrange"));
            Ddo_autorizacaoconsumo_quantidade_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_QUANTIDADE_Includedatalist"));
            Ddo_autorizacaoconsumo_quantidade_Sortasc = cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_QUANTIDADE_Sortasc");
            Ddo_autorizacaoconsumo_quantidade_Sortdsc = cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_QUANTIDADE_Sortdsc");
            Ddo_autorizacaoconsumo_quantidade_Cleanfilter = cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_QUANTIDADE_Cleanfilter");
            Ddo_autorizacaoconsumo_quantidade_Rangefilterfrom = cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_QUANTIDADE_Rangefilterfrom");
            Ddo_autorizacaoconsumo_quantidade_Rangefilterto = cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_QUANTIDADE_Rangefilterto");
            Ddo_autorizacaoconsumo_quantidade_Searchbuttontext = cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_QUANTIDADE_Searchbuttontext");
            Ddo_autorizacaoconsumo_valor_Caption = cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_VALOR_Caption");
            Ddo_autorizacaoconsumo_valor_Tooltip = cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_VALOR_Tooltip");
            Ddo_autorizacaoconsumo_valor_Cls = cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_VALOR_Cls");
            Ddo_autorizacaoconsumo_valor_Filteredtext_set = cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_VALOR_Filteredtext_set");
            Ddo_autorizacaoconsumo_valor_Filteredtextto_set = cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_VALOR_Filteredtextto_set");
            Ddo_autorizacaoconsumo_valor_Dropdownoptionstype = cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_VALOR_Dropdownoptionstype");
            Ddo_autorizacaoconsumo_valor_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_VALOR_Titlecontrolidtoreplace");
            Ddo_autorizacaoconsumo_valor_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_VALOR_Includesortasc"));
            Ddo_autorizacaoconsumo_valor_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_VALOR_Includesortdsc"));
            Ddo_autorizacaoconsumo_valor_Sortedstatus = cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_VALOR_Sortedstatus");
            Ddo_autorizacaoconsumo_valor_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_VALOR_Includefilter"));
            Ddo_autorizacaoconsumo_valor_Filtertype = cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_VALOR_Filtertype");
            Ddo_autorizacaoconsumo_valor_Filterisrange = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_VALOR_Filterisrange"));
            Ddo_autorizacaoconsumo_valor_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_VALOR_Includedatalist"));
            Ddo_autorizacaoconsumo_valor_Sortasc = cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_VALOR_Sortasc");
            Ddo_autorizacaoconsumo_valor_Sortdsc = cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_VALOR_Sortdsc");
            Ddo_autorizacaoconsumo_valor_Cleanfilter = cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_VALOR_Cleanfilter");
            Ddo_autorizacaoconsumo_valor_Rangefilterfrom = cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_VALOR_Rangefilterfrom");
            Ddo_autorizacaoconsumo_valor_Rangefilterto = cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_VALOR_Rangefilterto");
            Ddo_autorizacaoconsumo_valor_Searchbuttontext = cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_VALOR_Searchbuttontext");
            Ddo_autorizacaoconsumo_status_Caption = cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_STATUS_Caption");
            Ddo_autorizacaoconsumo_status_Tooltip = cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_STATUS_Tooltip");
            Ddo_autorizacaoconsumo_status_Cls = cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_STATUS_Cls");
            Ddo_autorizacaoconsumo_status_Selectedvalue_set = cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_STATUS_Selectedvalue_set");
            Ddo_autorizacaoconsumo_status_Dropdownoptionstype = cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_STATUS_Dropdownoptionstype");
            Ddo_autorizacaoconsumo_status_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_STATUS_Titlecontrolidtoreplace");
            Ddo_autorizacaoconsumo_status_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_STATUS_Includesortasc"));
            Ddo_autorizacaoconsumo_status_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_STATUS_Includesortdsc"));
            Ddo_autorizacaoconsumo_status_Sortedstatus = cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_STATUS_Sortedstatus");
            Ddo_autorizacaoconsumo_status_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_STATUS_Includefilter"));
            Ddo_autorizacaoconsumo_status_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_STATUS_Includedatalist"));
            Ddo_autorizacaoconsumo_status_Datalisttype = cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_STATUS_Datalisttype");
            Ddo_autorizacaoconsumo_status_Allowmultipleselection = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_STATUS_Allowmultipleselection"));
            Ddo_autorizacaoconsumo_status_Datalistfixedvalues = cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_STATUS_Datalistfixedvalues");
            Ddo_autorizacaoconsumo_status_Sortasc = cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_STATUS_Sortasc");
            Ddo_autorizacaoconsumo_status_Sortdsc = cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_STATUS_Sortdsc");
            Ddo_autorizacaoconsumo_status_Cleanfilter = cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_STATUS_Cleanfilter");
            Ddo_autorizacaoconsumo_status_Searchbuttontext = cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_STATUS_Searchbuttontext");
            Ddo_autorizacaoconsumo_ativo_Caption = cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_ATIVO_Caption");
            Ddo_autorizacaoconsumo_ativo_Tooltip = cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_ATIVO_Tooltip");
            Ddo_autorizacaoconsumo_ativo_Cls = cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_ATIVO_Cls");
            Ddo_autorizacaoconsumo_ativo_Selectedvalue_set = cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_ATIVO_Selectedvalue_set");
            Ddo_autorizacaoconsumo_ativo_Dropdownoptionstype = cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_ATIVO_Dropdownoptionstype");
            Ddo_autorizacaoconsumo_ativo_Titlecontrolidtoreplace = cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_ATIVO_Titlecontrolidtoreplace");
            Ddo_autorizacaoconsumo_ativo_Includesortasc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_ATIVO_Includesortasc"));
            Ddo_autorizacaoconsumo_ativo_Includesortdsc = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_ATIVO_Includesortdsc"));
            Ddo_autorizacaoconsumo_ativo_Sortedstatus = cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_ATIVO_Sortedstatus");
            Ddo_autorizacaoconsumo_ativo_Includefilter = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_ATIVO_Includefilter"));
            Ddo_autorizacaoconsumo_ativo_Includedatalist = StringUtil.StrToBool( cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_ATIVO_Includedatalist"));
            Ddo_autorizacaoconsumo_ativo_Datalisttype = cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_ATIVO_Datalisttype");
            Ddo_autorizacaoconsumo_ativo_Datalistfixedvalues = cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_ATIVO_Datalistfixedvalues");
            Ddo_autorizacaoconsumo_ativo_Sortasc = cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_ATIVO_Sortasc");
            Ddo_autorizacaoconsumo_ativo_Sortdsc = cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_ATIVO_Sortdsc");
            Ddo_autorizacaoconsumo_ativo_Cleanfilter = cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_ATIVO_Cleanfilter");
            Ddo_autorizacaoconsumo_ativo_Searchbuttontext = cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_ATIVO_Searchbuttontext");
            Gridpaginationbar_Selectedpage = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Selectedpage");
            Ddo_autorizacaoconsumo_codigo_Activeeventkey = cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_CODIGO_Activeeventkey");
            Ddo_autorizacaoconsumo_codigo_Filteredtext_get = cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_CODIGO_Filteredtext_get");
            Ddo_autorizacaoconsumo_codigo_Filteredtextto_get = cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_CODIGO_Filteredtextto_get");
            Ddo_autorizacaoconsumo_vigenciainicio_Activeeventkey = cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIO_Activeeventkey");
            Ddo_autorizacaoconsumo_vigenciainicio_Filteredtext_get = cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIO_Filteredtext_get");
            Ddo_autorizacaoconsumo_vigenciainicio_Filteredtextto_get = cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIO_Filteredtextto_get");
            Ddo_autorizacaoconsumo_vigenciafim_Activeeventkey = cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_VIGENCIAFIM_Activeeventkey");
            Ddo_autorizacaoconsumo_vigenciafim_Filteredtext_get = cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_VIGENCIAFIM_Filteredtext_get");
            Ddo_autorizacaoconsumo_vigenciafim_Filteredtextto_get = cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_VIGENCIAFIM_Filteredtextto_get");
            Ddo_autorizacaoconsumo_unidademedicaonom_Activeeventkey = cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_Activeeventkey");
            Ddo_autorizacaoconsumo_unidademedicaonom_Filteredtext_get = cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_Filteredtext_get");
            Ddo_autorizacaoconsumo_unidademedicaonom_Selectedvalue_get = cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_Selectedvalue_get");
            Ddo_autorizacaoconsumo_quantidade_Activeeventkey = cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_QUANTIDADE_Activeeventkey");
            Ddo_autorizacaoconsumo_quantidade_Filteredtext_get = cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_QUANTIDADE_Filteredtext_get");
            Ddo_autorizacaoconsumo_quantidade_Filteredtextto_get = cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_QUANTIDADE_Filteredtextto_get");
            Ddo_autorizacaoconsumo_valor_Activeeventkey = cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_VALOR_Activeeventkey");
            Ddo_autorizacaoconsumo_valor_Filteredtext_get = cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_VALOR_Filteredtext_get");
            Ddo_autorizacaoconsumo_valor_Filteredtextto_get = cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_VALOR_Filteredtextto_get");
            Ddo_autorizacaoconsumo_status_Activeeventkey = cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_STATUS_Activeeventkey");
            Ddo_autorizacaoconsumo_status_Selectedvalue_get = cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_STATUS_Selectedvalue_get");
            Ddo_autorizacaoconsumo_ativo_Activeeventkey = cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_ATIVO_Activeeventkey");
            Ddo_autorizacaoconsumo_ativo_Selectedvalue_get = cgiGet( sPrefix+"DDO_AUTORIZACAOCONSUMO_ATIVO_Selectedvalue_get");
            Dvelop_confirmpanel_cancelarautorizcaoconsumo_Result = cgiGet( sPrefix+"DVELOP_CONFIRMPANEL_CANCELARAUTORIZCAOCONSUMO_Result");
            Dvelop_confirmpanel_autorizar_Result = cgiGet( sPrefix+"DVELOP_CONFIRMPANEL_AUTORIZAR_Result");
            Dvelop_confirmpanel_encerrar_Result = cgiGet( sPrefix+"DVELOP_CONFIRMPANEL_ENCERRAR_Result");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            forbiddenHiddens = sPrefix + "hsh" + "ContratoAutorizacaoConsumoWC";
            A74Contrato_Codigo = (int)(context.localUtil.CToN( cgiGet( edtContrato_Codigo_Internalname), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A74Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A74Contrato_Codigo), 6, 0)));
            forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A74Contrato_Codigo), "ZZZZZ9");
            hsh = cgiGet( sPrefix+"hsh");
            if ( ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
            {
               GXUtil.WriteLog("contratoautorizacaoconsumowc:[SecurityCheckFailed value for]"+"Contrato_Codigo:"+context.localUtil.Format( (decimal)(A74Contrato_Codigo), "ZZZZZ9"));
               GxWebError = 1;
               context.HttpContext.Response.StatusDescription = 403.ToString();
               context.HttpContext.Response.StatusCode = 403;
               context.WriteHtmlText( "<title>403 Forbidden</title>") ;
               context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
               context.WriteHtmlText( "<p /><hr />") ;
               GXUtil.WriteLog("send_http_error_code " + 403.ToString());
               return  ;
            }
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vORDEREDDSC")) != AV14OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFAUTORIZACAOCONSUMO_CODIGO"), ",", ".") != Convert.ToDecimal( AV23TFAutorizacaoConsumo_Codigo )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFAUTORIZACAOCONSUMO_CODIGO_TO"), ",", ".") != Convert.ToDecimal( AV24TFAutorizacaoConsumo_Codigo_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( sPrefix+"GXH_vTFAUTORIZACAOCONSUMO_VIGENCIAINICIO"), 0) != AV27TFAutorizacaoConsumo_VigenciaInicio )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( sPrefix+"GXH_vTFAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO"), 0) != AV28TFAutorizacaoConsumo_VigenciaInicio_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( sPrefix+"GXH_vTFAUTORIZACAOCONSUMO_VIGENCIAFIM"), 0) != AV33TFAutorizacaoConsumo_VigenciaFim )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( sPrefix+"GXH_vTFAUTORIZACAOCONSUMO_VIGENCIAFIM_TO"), 0) != AV34TFAutorizacaoConsumo_VigenciaFim_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFAUTORIZACAOCONSUMO_UNIDADEMEDICAONOM"), AV39TFAutorizacaoConsumo_UnidadeMedicaoNom) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vTFAUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_SEL"), AV40TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFAUTORIZACAOCONSUMO_QUANTIDADE"), ",", ".") != Convert.ToDecimal( AV43TFAutorizacaoConsumo_Quantidade )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFAUTORIZACAOCONSUMO_QUANTIDADE_TO"), ",", ".") != Convert.ToDecimal( AV44TFAutorizacaoConsumo_Quantidade_To )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFAUTORIZACAOCONSUMO_VALOR"), ",", ".") != AV47TFAutorizacaoConsumo_Valor )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFAUTORIZACAOCONSUMO_VALOR_TO"), ",", ".") != AV48TFAutorizacaoConsumo_Valor_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vTFAUTORIZACAOCONSUMO_ATIVO_SEL"), ",", ".") != Convert.ToDecimal( AV55TFAutorizacaoConsumo_Ativo_Sel )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E24NB2 */
         E24NB2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E24NB2( )
      {
         /* Start Routine */
         edtavContrato_codigo_selected_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavContrato_codigo_selected_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavContrato_codigo_selected_Visible), 5, 0)));
         edtavAutorizacaoconsumo_codigo_selected_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavAutorizacaoconsumo_codigo_selected_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavAutorizacaoconsumo_codigo_selected_Visible), 5, 0)));
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         edtavTfautorizacaoconsumo_codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfautorizacaoconsumo_codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfautorizacaoconsumo_codigo_Visible), 5, 0)));
         edtavTfautorizacaoconsumo_codigo_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfautorizacaoconsumo_codigo_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfautorizacaoconsumo_codigo_to_Visible), 5, 0)));
         edtavTfautorizacaoconsumo_vigenciainicio_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfautorizacaoconsumo_vigenciainicio_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfautorizacaoconsumo_vigenciainicio_Visible), 5, 0)));
         edtavTfautorizacaoconsumo_vigenciainicio_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfautorizacaoconsumo_vigenciainicio_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfautorizacaoconsumo_vigenciainicio_to_Visible), 5, 0)));
         edtavTfautorizacaoconsumo_vigenciafim_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfautorizacaoconsumo_vigenciafim_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfautorizacaoconsumo_vigenciafim_Visible), 5, 0)));
         edtavTfautorizacaoconsumo_vigenciafim_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfautorizacaoconsumo_vigenciafim_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfautorizacaoconsumo_vigenciafim_to_Visible), 5, 0)));
         edtavTfautorizacaoconsumo_unidademedicaonom_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfautorizacaoconsumo_unidademedicaonom_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfautorizacaoconsumo_unidademedicaonom_Visible), 5, 0)));
         edtavTfautorizacaoconsumo_unidademedicaonom_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfautorizacaoconsumo_unidademedicaonom_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfautorizacaoconsumo_unidademedicaonom_sel_Visible), 5, 0)));
         edtavTfautorizacaoconsumo_quantidade_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfautorizacaoconsumo_quantidade_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfautorizacaoconsumo_quantidade_Visible), 5, 0)));
         edtavTfautorizacaoconsumo_quantidade_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfautorizacaoconsumo_quantidade_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfautorizacaoconsumo_quantidade_to_Visible), 5, 0)));
         edtavTfautorizacaoconsumo_valor_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfautorizacaoconsumo_valor_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfautorizacaoconsumo_valor_Visible), 5, 0)));
         edtavTfautorizacaoconsumo_valor_to_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfautorizacaoconsumo_valor_to_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfautorizacaoconsumo_valor_to_Visible), 5, 0)));
         edtavTfautorizacaoconsumo_ativo_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavTfautorizacaoconsumo_ativo_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfautorizacaoconsumo_ativo_sel_Visible), 5, 0)));
         Ddo_autorizacaoconsumo_codigo_Titlecontrolidtoreplace = subGrid_Internalname+"_AutorizacaoConsumo_Codigo";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_autorizacaoconsumo_codigo_Internalname, "TitleControlIdToReplace", Ddo_autorizacaoconsumo_codigo_Titlecontrolidtoreplace);
         AV25ddo_AutorizacaoConsumo_CodigoTitleControlIdToReplace = Ddo_autorizacaoconsumo_codigo_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25ddo_AutorizacaoConsumo_CodigoTitleControlIdToReplace", AV25ddo_AutorizacaoConsumo_CodigoTitleControlIdToReplace);
         edtavDdo_autorizacaoconsumo_codigotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_autorizacaoconsumo_codigotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_autorizacaoconsumo_codigotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_autorizacaoconsumo_vigenciainicio_Titlecontrolidtoreplace = subGrid_Internalname+"_AutorizacaoConsumo_VigenciaInicio";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_autorizacaoconsumo_vigenciainicio_Internalname, "TitleControlIdToReplace", Ddo_autorizacaoconsumo_vigenciainicio_Titlecontrolidtoreplace);
         AV31ddo_AutorizacaoConsumo_VigenciaInicioTitleControlIdToReplace = Ddo_autorizacaoconsumo_vigenciainicio_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV31ddo_AutorizacaoConsumo_VigenciaInicioTitleControlIdToReplace", AV31ddo_AutorizacaoConsumo_VigenciaInicioTitleControlIdToReplace);
         edtavDdo_autorizacaoconsumo_vigenciainiciotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_autorizacaoconsumo_vigenciainiciotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_autorizacaoconsumo_vigenciainiciotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_autorizacaoconsumo_vigenciafim_Titlecontrolidtoreplace = subGrid_Internalname+"_AutorizacaoConsumo_VigenciaFim";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_autorizacaoconsumo_vigenciafim_Internalname, "TitleControlIdToReplace", Ddo_autorizacaoconsumo_vigenciafim_Titlecontrolidtoreplace);
         AV37ddo_AutorizacaoConsumo_VigenciaFimTitleControlIdToReplace = Ddo_autorizacaoconsumo_vigenciafim_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV37ddo_AutorizacaoConsumo_VigenciaFimTitleControlIdToReplace", AV37ddo_AutorizacaoConsumo_VigenciaFimTitleControlIdToReplace);
         edtavDdo_autorizacaoconsumo_vigenciafimtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_autorizacaoconsumo_vigenciafimtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_autorizacaoconsumo_vigenciafimtitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_autorizacaoconsumo_unidademedicaonom_Titlecontrolidtoreplace = subGrid_Internalname+"_AutorizacaoConsumo_UnidadeMedicaoNom";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_autorizacaoconsumo_unidademedicaonom_Internalname, "TitleControlIdToReplace", Ddo_autorizacaoconsumo_unidademedicaonom_Titlecontrolidtoreplace);
         AV41ddo_AutorizacaoConsumo_UnidadeMedicaoNomTitleControlIdToReplace = Ddo_autorizacaoconsumo_unidademedicaonom_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV41ddo_AutorizacaoConsumo_UnidadeMedicaoNomTitleControlIdToReplace", AV41ddo_AutorizacaoConsumo_UnidadeMedicaoNomTitleControlIdToReplace);
         edtavDdo_autorizacaoconsumo_unidademedicaonomtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_autorizacaoconsumo_unidademedicaonomtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_autorizacaoconsumo_unidademedicaonomtitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_autorizacaoconsumo_quantidade_Titlecontrolidtoreplace = subGrid_Internalname+"_AutorizacaoConsumo_Quantidade";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_autorizacaoconsumo_quantidade_Internalname, "TitleControlIdToReplace", Ddo_autorizacaoconsumo_quantidade_Titlecontrolidtoreplace);
         AV45ddo_AutorizacaoConsumo_QuantidadeTitleControlIdToReplace = Ddo_autorizacaoconsumo_quantidade_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV45ddo_AutorizacaoConsumo_QuantidadeTitleControlIdToReplace", AV45ddo_AutorizacaoConsumo_QuantidadeTitleControlIdToReplace);
         edtavDdo_autorizacaoconsumo_quantidadetitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_autorizacaoconsumo_quantidadetitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_autorizacaoconsumo_quantidadetitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_autorizacaoconsumo_valor_Titlecontrolidtoreplace = subGrid_Internalname+"_AutorizacaoConsumo_Valor";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_autorizacaoconsumo_valor_Internalname, "TitleControlIdToReplace", Ddo_autorizacaoconsumo_valor_Titlecontrolidtoreplace);
         AV49ddo_AutorizacaoConsumo_ValorTitleControlIdToReplace = Ddo_autorizacaoconsumo_valor_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV49ddo_AutorizacaoConsumo_ValorTitleControlIdToReplace", AV49ddo_AutorizacaoConsumo_ValorTitleControlIdToReplace);
         edtavDdo_autorizacaoconsumo_valortitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_autorizacaoconsumo_valortitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_autorizacaoconsumo_valortitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_autorizacaoconsumo_status_Titlecontrolidtoreplace = subGrid_Internalname+"_AutorizacaoConsumo_Status";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_autorizacaoconsumo_status_Internalname, "TitleControlIdToReplace", Ddo_autorizacaoconsumo_status_Titlecontrolidtoreplace);
         AV53ddo_AutorizacaoConsumo_StatusTitleControlIdToReplace = Ddo_autorizacaoconsumo_status_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV53ddo_AutorizacaoConsumo_StatusTitleControlIdToReplace", AV53ddo_AutorizacaoConsumo_StatusTitleControlIdToReplace);
         edtavDdo_autorizacaoconsumo_statustitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_autorizacaoconsumo_statustitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_autorizacaoconsumo_statustitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_autorizacaoconsumo_ativo_Titlecontrolidtoreplace = subGrid_Internalname+"_AutorizacaoConsumo_Ativo";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_autorizacaoconsumo_ativo_Internalname, "TitleControlIdToReplace", Ddo_autorizacaoconsumo_ativo_Titlecontrolidtoreplace);
         AV56ddo_AutorizacaoConsumo_AtivoTitleControlIdToReplace = Ddo_autorizacaoconsumo_ativo_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV56ddo_AutorizacaoConsumo_AtivoTitleControlIdToReplace", AV56ddo_AutorizacaoConsumo_AtivoTitleControlIdToReplace);
         edtavDdo_autorizacaoconsumo_ativotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDdo_autorizacaoconsumo_ativotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_autorizacaoconsumo_ativotitlecontrolidtoreplace_Visible), 5, 0)));
         edtContrato_Codigo_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtContrato_Codigo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContrato_Codigo_Visible), 5, 0)));
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtavOrderedby_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavOrderedby_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrderedby_Visible), 5, 0)));
         if ( AV13OrderedBy < 1 )
         {
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
            S132 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = AV57DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1) ;
         AV57DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1;
      }

      protected void E25NB2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV22AutorizacaoConsumo_CodigoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV26AutorizacaoConsumo_VigenciaInicioTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV32AutorizacaoConsumo_VigenciaFimTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV38AutorizacaoConsumo_UnidadeMedicaoNomTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV42AutorizacaoConsumo_QuantidadeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV46AutorizacaoConsumo_ValorTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV50AutorizacaoConsumo_StatusTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV54AutorizacaoConsumo_AtivoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtAutorizacaoConsumo_Codigo_Titleformat = 2;
         edtAutorizacaoConsumo_Codigo_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Autorizacao Consumo", AV25ddo_AutorizacaoConsumo_CodigoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtAutorizacaoConsumo_Codigo_Internalname, "Title", edtAutorizacaoConsumo_Codigo_Title);
         edtAutorizacaoConsumo_VigenciaInicio_Titleformat = 2;
         edtAutorizacaoConsumo_VigenciaInicio_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "In�cio da Vig�ncia", AV31ddo_AutorizacaoConsumo_VigenciaInicioTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtAutorizacaoConsumo_VigenciaInicio_Internalname, "Title", edtAutorizacaoConsumo_VigenciaInicio_Title);
         edtAutorizacaoConsumo_VigenciaFim_Titleformat = 2;
         edtAutorizacaoConsumo_VigenciaFim_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Fim da Vig�ncia", AV37ddo_AutorizacaoConsumo_VigenciaFimTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtAutorizacaoConsumo_VigenciaFim_Internalname, "Title", edtAutorizacaoConsumo_VigenciaFim_Title);
         edtAutorizacaoConsumo_UnidadeMedicaoNom_Titleformat = 2;
         edtAutorizacaoConsumo_UnidadeMedicaoNom_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Unidade de Medi��o", AV41ddo_AutorizacaoConsumo_UnidadeMedicaoNomTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtAutorizacaoConsumo_UnidadeMedicaoNom_Internalname, "Title", edtAutorizacaoConsumo_UnidadeMedicaoNom_Title);
         edtAutorizacaoConsumo_Quantidade_Titleformat = 2;
         edtAutorizacaoConsumo_Quantidade_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Quantidade", AV45ddo_AutorizacaoConsumo_QuantidadeTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtAutorizacaoConsumo_Quantidade_Internalname, "Title", edtAutorizacaoConsumo_Quantidade_Title);
         edtAutorizacaoConsumo_Valor_Titleformat = 2;
         edtAutorizacaoConsumo_Valor_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Valor", AV49ddo_AutorizacaoConsumo_ValorTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtAutorizacaoConsumo_Valor_Internalname, "Title", edtAutorizacaoConsumo_Valor_Title);
         cmbAutorizacaoConsumo_Status_Titleformat = 2;
         cmbAutorizacaoConsumo_Status.Title.Text = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Status", AV53ddo_AutorizacaoConsumo_StatusTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbAutorizacaoConsumo_Status_Internalname, "Title", cmbAutorizacaoConsumo_Status.Title.Text);
         chkAutorizacaoConsumo_Ativo_Titleformat = 2;
         chkAutorizacaoConsumo_Ativo.Title.Text = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Ativo", AV56ddo_AutorizacaoConsumo_AtivoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkAutorizacaoConsumo_Ativo_Internalname, "Title", chkAutorizacaoConsumo_Ativo.Title.Text);
         AV59GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV59GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV59GridCurrentPage), 10, 0)));
         AV60GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV60GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV60GridPageCount), 10, 0)));
         imgInsert_Visible = (AV6WWPContext.gxTpr_Insert ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgInsert_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgInsert_Visible), 5, 0)));
         edtavCancelarautorizcaoconsumo_Visible = (AV6WWPContext.gxTpr_Update ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavCancelarautorizcaoconsumo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCancelarautorizcaoconsumo_Visible), 5, 0)));
         edtavAutorizar_Visible = (AV6WWPContext.gxTpr_Update ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavAutorizar_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavAutorizar_Visible), 5, 0)));
         edtavEncerrar_Visible = (AV6WWPContext.gxTpr_Update ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavEncerrar_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavEncerrar_Visible), 5, 0)));
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV22AutorizacaoConsumo_CodigoTitleFilterData", AV22AutorizacaoConsumo_CodigoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV26AutorizacaoConsumo_VigenciaInicioTitleFilterData", AV26AutorizacaoConsumo_VigenciaInicioTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV32AutorizacaoConsumo_VigenciaFimTitleFilterData", AV32AutorizacaoConsumo_VigenciaFimTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV38AutorizacaoConsumo_UnidadeMedicaoNomTitleFilterData", AV38AutorizacaoConsumo_UnidadeMedicaoNomTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV42AutorizacaoConsumo_QuantidadeTitleFilterData", AV42AutorizacaoConsumo_QuantidadeTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV46AutorizacaoConsumo_ValorTitleFilterData", AV46AutorizacaoConsumo_ValorTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV50AutorizacaoConsumo_StatusTitleFilterData", AV50AutorizacaoConsumo_StatusTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV54AutorizacaoConsumo_AtivoTitleFilterData", AV54AutorizacaoConsumo_AtivoTitleFilterData);
      }

      protected void E11NB2( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV58PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV58PageToGo) ;
         }
      }

      protected void E15NB2( )
      {
         /* Ddo_autorizacaoconsumo_codigo_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_autorizacaoconsumo_codigo_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_autorizacaoconsumo_codigo_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_autorizacaoconsumo_codigo_Internalname, "SortedStatus", Ddo_autorizacaoconsumo_codigo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_autorizacaoconsumo_codigo_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_autorizacaoconsumo_codigo_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_autorizacaoconsumo_codigo_Internalname, "SortedStatus", Ddo_autorizacaoconsumo_codigo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_autorizacaoconsumo_codigo_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV23TFAutorizacaoConsumo_Codigo = (int)(NumberUtil.Val( Ddo_autorizacaoconsumo_codigo_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23TFAutorizacaoConsumo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV23TFAutorizacaoConsumo_Codigo), 6, 0)));
            AV24TFAutorizacaoConsumo_Codigo_To = (int)(NumberUtil.Val( Ddo_autorizacaoconsumo_codigo_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24TFAutorizacaoConsumo_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24TFAutorizacaoConsumo_Codigo_To), 6, 0)));
            subgrid_firstpage( ) ;
         }
      }

      protected void E16NB2( )
      {
         /* Ddo_autorizacaoconsumo_vigenciainicio_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_autorizacaoconsumo_vigenciainicio_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_autorizacaoconsumo_vigenciainicio_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_autorizacaoconsumo_vigenciainicio_Internalname, "SortedStatus", Ddo_autorizacaoconsumo_vigenciainicio_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_autorizacaoconsumo_vigenciainicio_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_autorizacaoconsumo_vigenciainicio_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_autorizacaoconsumo_vigenciainicio_Internalname, "SortedStatus", Ddo_autorizacaoconsumo_vigenciainicio_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_autorizacaoconsumo_vigenciainicio_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV27TFAutorizacaoConsumo_VigenciaInicio = context.localUtil.CToD( Ddo_autorizacaoconsumo_vigenciainicio_Filteredtext_get, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27TFAutorizacaoConsumo_VigenciaInicio", context.localUtil.Format(AV27TFAutorizacaoConsumo_VigenciaInicio, "99/99/99"));
            AV28TFAutorizacaoConsumo_VigenciaInicio_To = context.localUtil.CToD( Ddo_autorizacaoconsumo_vigenciainicio_Filteredtextto_get, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV28TFAutorizacaoConsumo_VigenciaInicio_To", context.localUtil.Format(AV28TFAutorizacaoConsumo_VigenciaInicio_To, "99/99/99"));
            subgrid_firstpage( ) ;
         }
      }

      protected void E17NB2( )
      {
         /* Ddo_autorizacaoconsumo_vigenciafim_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_autorizacaoconsumo_vigenciafim_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_autorizacaoconsumo_vigenciafim_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_autorizacaoconsumo_vigenciafim_Internalname, "SortedStatus", Ddo_autorizacaoconsumo_vigenciafim_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_autorizacaoconsumo_vigenciafim_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_autorizacaoconsumo_vigenciafim_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_autorizacaoconsumo_vigenciafim_Internalname, "SortedStatus", Ddo_autorizacaoconsumo_vigenciafim_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_autorizacaoconsumo_vigenciafim_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV33TFAutorizacaoConsumo_VigenciaFim = context.localUtil.CToD( Ddo_autorizacaoconsumo_vigenciafim_Filteredtext_get, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV33TFAutorizacaoConsumo_VigenciaFim", context.localUtil.Format(AV33TFAutorizacaoConsumo_VigenciaFim, "99/99/99"));
            AV34TFAutorizacaoConsumo_VigenciaFim_To = context.localUtil.CToD( Ddo_autorizacaoconsumo_vigenciafim_Filteredtextto_get, 2);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV34TFAutorizacaoConsumo_VigenciaFim_To", context.localUtil.Format(AV34TFAutorizacaoConsumo_VigenciaFim_To, "99/99/99"));
            subgrid_firstpage( ) ;
         }
      }

      protected void E18NB2( )
      {
         /* Ddo_autorizacaoconsumo_unidademedicaonom_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_autorizacaoconsumo_unidademedicaonom_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_autorizacaoconsumo_unidademedicaonom_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_autorizacaoconsumo_unidademedicaonom_Internalname, "SortedStatus", Ddo_autorizacaoconsumo_unidademedicaonom_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_autorizacaoconsumo_unidademedicaonom_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_autorizacaoconsumo_unidademedicaonom_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_autorizacaoconsumo_unidademedicaonom_Internalname, "SortedStatus", Ddo_autorizacaoconsumo_unidademedicaonom_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_autorizacaoconsumo_unidademedicaonom_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV39TFAutorizacaoConsumo_UnidadeMedicaoNom = Ddo_autorizacaoconsumo_unidademedicaonom_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV39TFAutorizacaoConsumo_UnidadeMedicaoNom", AV39TFAutorizacaoConsumo_UnidadeMedicaoNom);
            AV40TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel = Ddo_autorizacaoconsumo_unidademedicaonom_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV40TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel", AV40TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel);
            subgrid_firstpage( ) ;
         }
      }

      protected void E19NB2( )
      {
         /* Ddo_autorizacaoconsumo_quantidade_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_autorizacaoconsumo_quantidade_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 5;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_autorizacaoconsumo_quantidade_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_autorizacaoconsumo_quantidade_Internalname, "SortedStatus", Ddo_autorizacaoconsumo_quantidade_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_autorizacaoconsumo_quantidade_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 5;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_autorizacaoconsumo_quantidade_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_autorizacaoconsumo_quantidade_Internalname, "SortedStatus", Ddo_autorizacaoconsumo_quantidade_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_autorizacaoconsumo_quantidade_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV43TFAutorizacaoConsumo_Quantidade = (short)(NumberUtil.Val( Ddo_autorizacaoconsumo_quantidade_Filteredtext_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV43TFAutorizacaoConsumo_Quantidade", StringUtil.LTrim( StringUtil.Str( (decimal)(AV43TFAutorizacaoConsumo_Quantidade), 3, 0)));
            AV44TFAutorizacaoConsumo_Quantidade_To = (short)(NumberUtil.Val( Ddo_autorizacaoconsumo_quantidade_Filteredtextto_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV44TFAutorizacaoConsumo_Quantidade_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV44TFAutorizacaoConsumo_Quantidade_To), 3, 0)));
            subgrid_firstpage( ) ;
         }
      }

      protected void E20NB2( )
      {
         /* Ddo_autorizacaoconsumo_valor_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_autorizacaoconsumo_valor_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 6;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_autorizacaoconsumo_valor_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_autorizacaoconsumo_valor_Internalname, "SortedStatus", Ddo_autorizacaoconsumo_valor_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_autorizacaoconsumo_valor_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 6;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_autorizacaoconsumo_valor_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_autorizacaoconsumo_valor_Internalname, "SortedStatus", Ddo_autorizacaoconsumo_valor_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_autorizacaoconsumo_valor_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV47TFAutorizacaoConsumo_Valor = NumberUtil.Val( Ddo_autorizacaoconsumo_valor_Filteredtext_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV47TFAutorizacaoConsumo_Valor", StringUtil.LTrim( StringUtil.Str( AV47TFAutorizacaoConsumo_Valor, 18, 5)));
            AV48TFAutorizacaoConsumo_Valor_To = NumberUtil.Val( Ddo_autorizacaoconsumo_valor_Filteredtextto_get, ".");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV48TFAutorizacaoConsumo_Valor_To", StringUtil.LTrim( StringUtil.Str( AV48TFAutorizacaoConsumo_Valor_To, 18, 5)));
            subgrid_firstpage( ) ;
         }
      }

      protected void E21NB2( )
      {
         /* Ddo_autorizacaoconsumo_status_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_autorizacaoconsumo_status_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 7;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_autorizacaoconsumo_status_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_autorizacaoconsumo_status_Internalname, "SortedStatus", Ddo_autorizacaoconsumo_status_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_autorizacaoconsumo_status_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 7;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_autorizacaoconsumo_status_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_autorizacaoconsumo_status_Internalname, "SortedStatus", Ddo_autorizacaoconsumo_status_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_autorizacaoconsumo_status_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV51TFAutorizacaoConsumo_Status_SelsJson = Ddo_autorizacaoconsumo_status_Selectedvalue_get;
            AV52TFAutorizacaoConsumo_Status_Sels.FromJSonString(AV51TFAutorizacaoConsumo_Status_SelsJson);
            subgrid_firstpage( ) ;
         }
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV52TFAutorizacaoConsumo_Status_Sels", AV52TFAutorizacaoConsumo_Status_Sels);
      }

      protected void E22NB2( )
      {
         /* Ddo_autorizacaoconsumo_ativo_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_autorizacaoconsumo_ativo_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 8;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_autorizacaoconsumo_ativo_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_autorizacaoconsumo_ativo_Internalname, "SortedStatus", Ddo_autorizacaoconsumo_ativo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_autorizacaoconsumo_ativo_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 8;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_autorizacaoconsumo_ativo_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_autorizacaoconsumo_ativo_Internalname, "SortedStatus", Ddo_autorizacaoconsumo_ativo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefreshCmp(sPrefix);
         }
         else if ( StringUtil.StrCmp(Ddo_autorizacaoconsumo_ativo_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV55TFAutorizacaoConsumo_Ativo_Sel = (short)(NumberUtil.Val( Ddo_autorizacaoconsumo_ativo_Selectedvalue_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV55TFAutorizacaoConsumo_Ativo_Sel", StringUtil.Str( (decimal)(AV55TFAutorizacaoConsumo_Ativo_Sel), 1, 0));
            subgrid_firstpage( ) ;
         }
      }

      private void E26NB2( )
      {
         /* Grid_Load Routine */
         edtavCancelarautorizcaoconsumo_Tooltiptext = "Cancelar Autoriza��o de Consumo";
         if ( A1780AutorizacaoConsumo_Ativo )
         {
            AV17CancelarAutorizcaoConsumo = context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavCancelarautorizcaoconsumo_Internalname, AV17CancelarAutorizcaoConsumo);
            AV63Cancelarautorizcaoconsumo_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )));
            edtavCancelarautorizcaoconsumo_Enabled = 1;
         }
         else
         {
            AV17CancelarAutorizcaoConsumo = context.GetImagePath( "138fdfa8-ae9b-4664-8007-e04d47468418", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavCancelarautorizcaoconsumo_Internalname, AV17CancelarAutorizcaoConsumo);
            AV63Cancelarautorizcaoconsumo_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "138fdfa8-ae9b-4664-8007-e04d47468418", "", context.GetTheme( )));
            edtavCancelarautorizcaoconsumo_Enabled = 0;
         }
         AV20Autorizar = context.GetImagePath( "653f6166-5d82-407a-af84-19e0dde65efd", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavAutorizar_Internalname, AV20Autorizar);
         AV64Autorizar_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "653f6166-5d82-407a-af84-19e0dde65efd", "", context.GetTheme( )));
         edtavAutorizar_Tooltiptext = "Autorizar";
         AV21Encerrar = context.GetImagePath( "70d5df5e-e723-4cca-9402-f0ce8679d27f", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavEncerrar_Internalname, AV21Encerrar);
         AV65Encerrar_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "70d5df5e-e723-4cca-9402-f0ce8679d27f", "", context.GetTheme( )));
         edtavEncerrar_Tooltiptext = "Encerrar";
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 19;
         }
         sendrow_192( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_19_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(19, GridRow);
         }
      }

      protected void E23NB2( )
      {
         /* 'DoInsert' Routine */
         context.wjLoc = formatLink("autorizacaoconsumo.aspx") + "?" + UrlEncode(StringUtil.RTrim("INS")) + "," + UrlEncode("" +0);
         context.wjLocDisableFrm = 1;
      }

      protected void E12NB2( )
      {
         /* Dvelop_confirmpanel_cancelarautorizcaoconsumo_Close Routine */
         if ( StringUtil.StrCmp(Dvelop_confirmpanel_cancelarautorizcaoconsumo_Result, "Yes") == 0 )
         {
            /* Execute user subroutine: 'DO CANCELARAUTORIZCAOCONSUMO' */
            S162 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         context.DoAjaxRefreshCmp(sPrefix);
      }

      protected void E13NB2( )
      {
         /* Dvelop_confirmpanel_autorizar_Close Routine */
         if ( StringUtil.StrCmp(Dvelop_confirmpanel_autorizar_Result, "Yes") == 0 )
         {
            /* Execute user subroutine: 'DO AUTORIZAR' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
      }

      protected void E14NB2( )
      {
         /* Dvelop_confirmpanel_encerrar_Close Routine */
         if ( StringUtil.StrCmp(Dvelop_confirmpanel_encerrar_Result, "Yes") == 0 )
         {
            /* Execute user subroutine: 'DO ENCERRAR' */
            S182 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
      }

      protected void S152( )
      {
         /* 'RESETDDOSORTEDSTATUS' Routine */
         Ddo_autorizacaoconsumo_codigo_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_autorizacaoconsumo_codigo_Internalname, "SortedStatus", Ddo_autorizacaoconsumo_codigo_Sortedstatus);
         Ddo_autorizacaoconsumo_vigenciainicio_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_autorizacaoconsumo_vigenciainicio_Internalname, "SortedStatus", Ddo_autorizacaoconsumo_vigenciainicio_Sortedstatus);
         Ddo_autorizacaoconsumo_vigenciafim_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_autorizacaoconsumo_vigenciafim_Internalname, "SortedStatus", Ddo_autorizacaoconsumo_vigenciafim_Sortedstatus);
         Ddo_autorizacaoconsumo_unidademedicaonom_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_autorizacaoconsumo_unidademedicaonom_Internalname, "SortedStatus", Ddo_autorizacaoconsumo_unidademedicaonom_Sortedstatus);
         Ddo_autorizacaoconsumo_quantidade_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_autorizacaoconsumo_quantidade_Internalname, "SortedStatus", Ddo_autorizacaoconsumo_quantidade_Sortedstatus);
         Ddo_autorizacaoconsumo_valor_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_autorizacaoconsumo_valor_Internalname, "SortedStatus", Ddo_autorizacaoconsumo_valor_Sortedstatus);
         Ddo_autorizacaoconsumo_status_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_autorizacaoconsumo_status_Internalname, "SortedStatus", Ddo_autorizacaoconsumo_status_Sortedstatus);
         Ddo_autorizacaoconsumo_ativo_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_autorizacaoconsumo_ativo_Internalname, "SortedStatus", Ddo_autorizacaoconsumo_ativo_Sortedstatus);
      }

      protected void S132( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV13OrderedBy == 1 )
         {
            Ddo_autorizacaoconsumo_codigo_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_autorizacaoconsumo_codigo_Internalname, "SortedStatus", Ddo_autorizacaoconsumo_codigo_Sortedstatus);
         }
         else if ( AV13OrderedBy == 2 )
         {
            Ddo_autorizacaoconsumo_vigenciainicio_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_autorizacaoconsumo_vigenciainicio_Internalname, "SortedStatus", Ddo_autorizacaoconsumo_vigenciainicio_Sortedstatus);
         }
         else if ( AV13OrderedBy == 3 )
         {
            Ddo_autorizacaoconsumo_vigenciafim_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_autorizacaoconsumo_vigenciafim_Internalname, "SortedStatus", Ddo_autorizacaoconsumo_vigenciafim_Sortedstatus);
         }
         else if ( AV13OrderedBy == 4 )
         {
            Ddo_autorizacaoconsumo_unidademedicaonom_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_autorizacaoconsumo_unidademedicaonom_Internalname, "SortedStatus", Ddo_autorizacaoconsumo_unidademedicaonom_Sortedstatus);
         }
         else if ( AV13OrderedBy == 5 )
         {
            Ddo_autorizacaoconsumo_quantidade_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_autorizacaoconsumo_quantidade_Internalname, "SortedStatus", Ddo_autorizacaoconsumo_quantidade_Sortedstatus);
         }
         else if ( AV13OrderedBy == 6 )
         {
            Ddo_autorizacaoconsumo_valor_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_autorizacaoconsumo_valor_Internalname, "SortedStatus", Ddo_autorizacaoconsumo_valor_Sortedstatus);
         }
         else if ( AV13OrderedBy == 7 )
         {
            Ddo_autorizacaoconsumo_status_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_autorizacaoconsumo_status_Internalname, "SortedStatus", Ddo_autorizacaoconsumo_status_Sortedstatus);
         }
         else if ( AV13OrderedBy == 8 )
         {
            Ddo_autorizacaoconsumo_ativo_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_autorizacaoconsumo_ativo_Internalname, "SortedStatus", Ddo_autorizacaoconsumo_ativo_Sortedstatus);
         }
      }

      protected void S162( )
      {
         /* 'DO CANCELARAUTORIZCAOCONSUMO' Routine */
         new prc_saldocontratoreservado_cancelar(context ).execute(  AV19Contrato_Codigo_Selected) ;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19Contrato_Codigo_Selected", StringUtil.LTrim( StringUtil.Str( (decimal)(AV19Contrato_Codigo_Selected), 6, 0)));
         context.CommitDataStores( "ContratoAutorizacaoConsumoWC");
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV23TFAutorizacaoConsumo_Codigo, AV24TFAutorizacaoConsumo_Codigo_To, AV27TFAutorizacaoConsumo_VigenciaInicio, AV28TFAutorizacaoConsumo_VigenciaInicio_To, AV33TFAutorizacaoConsumo_VigenciaFim, AV34TFAutorizacaoConsumo_VigenciaFim_To, AV39TFAutorizacaoConsumo_UnidadeMedicaoNom, AV40TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel, AV43TFAutorizacaoConsumo_Quantidade, AV44TFAutorizacaoConsumo_Quantidade_To, AV47TFAutorizacaoConsumo_Valor, AV48TFAutorizacaoConsumo_Valor_To, AV55TFAutorizacaoConsumo_Ativo_Sel, AV7Contrato_Codigo, AV25ddo_AutorizacaoConsumo_CodigoTitleControlIdToReplace, AV31ddo_AutorizacaoConsumo_VigenciaInicioTitleControlIdToReplace, AV37ddo_AutorizacaoConsumo_VigenciaFimTitleControlIdToReplace, AV41ddo_AutorizacaoConsumo_UnidadeMedicaoNomTitleControlIdToReplace, AV45ddo_AutorizacaoConsumo_QuantidadeTitleControlIdToReplace, AV49ddo_AutorizacaoConsumo_ValorTitleControlIdToReplace, AV53ddo_AutorizacaoConsumo_StatusTitleControlIdToReplace, AV56ddo_AutorizacaoConsumo_AtivoTitleControlIdToReplace, AV66Pgmname, AV52TFAutorizacaoConsumo_Status_Sels, A1780AutorizacaoConsumo_Ativo, sPrefix) ;
      }

      protected void S172( )
      {
         /* 'DO AUTORIZAR' Routine */
         new prc_autorizacaoconsumoaprovar(context ).execute(  AV18AutorizacaoConsumo_Codigo_Selected) ;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18AutorizacaoConsumo_Codigo_Selected", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18AutorizacaoConsumo_Codigo_Selected), 6, 0)));
         context.CommitDataStores( "ContratoAutorizacaoConsumoWC");
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV23TFAutorizacaoConsumo_Codigo, AV24TFAutorizacaoConsumo_Codigo_To, AV27TFAutorizacaoConsumo_VigenciaInicio, AV28TFAutorizacaoConsumo_VigenciaInicio_To, AV33TFAutorizacaoConsumo_VigenciaFim, AV34TFAutorizacaoConsumo_VigenciaFim_To, AV39TFAutorizacaoConsumo_UnidadeMedicaoNom, AV40TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel, AV43TFAutorizacaoConsumo_Quantidade, AV44TFAutorizacaoConsumo_Quantidade_To, AV47TFAutorizacaoConsumo_Valor, AV48TFAutorizacaoConsumo_Valor_To, AV55TFAutorizacaoConsumo_Ativo_Sel, AV7Contrato_Codigo, AV25ddo_AutorizacaoConsumo_CodigoTitleControlIdToReplace, AV31ddo_AutorizacaoConsumo_VigenciaInicioTitleControlIdToReplace, AV37ddo_AutorizacaoConsumo_VigenciaFimTitleControlIdToReplace, AV41ddo_AutorizacaoConsumo_UnidadeMedicaoNomTitleControlIdToReplace, AV45ddo_AutorizacaoConsumo_QuantidadeTitleControlIdToReplace, AV49ddo_AutorizacaoConsumo_ValorTitleControlIdToReplace, AV53ddo_AutorizacaoConsumo_StatusTitleControlIdToReplace, AV56ddo_AutorizacaoConsumo_AtivoTitleControlIdToReplace, AV66Pgmname, AV52TFAutorizacaoConsumo_Status_Sels, A1780AutorizacaoConsumo_Ativo, sPrefix) ;
      }

      protected void S182( )
      {
         /* 'DO ENCERRAR' Routine */
         new prc_autorizacaoconsumo_encerrar(context ).execute( ref  AV18AutorizacaoConsumo_Codigo_Selected) ;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18AutorizacaoConsumo_Codigo_Selected", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18AutorizacaoConsumo_Codigo_Selected), 6, 0)));
         context.CommitDataStores( "ContratoAutorizacaoConsumoWC");
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV23TFAutorizacaoConsumo_Codigo, AV24TFAutorizacaoConsumo_Codigo_To, AV27TFAutorizacaoConsumo_VigenciaInicio, AV28TFAutorizacaoConsumo_VigenciaInicio_To, AV33TFAutorizacaoConsumo_VigenciaFim, AV34TFAutorizacaoConsumo_VigenciaFim_To, AV39TFAutorizacaoConsumo_UnidadeMedicaoNom, AV40TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel, AV43TFAutorizacaoConsumo_Quantidade, AV44TFAutorizacaoConsumo_Quantidade_To, AV47TFAutorizacaoConsumo_Valor, AV48TFAutorizacaoConsumo_Valor_To, AV55TFAutorizacaoConsumo_Ativo_Sel, AV7Contrato_Codigo, AV25ddo_AutorizacaoConsumo_CodigoTitleControlIdToReplace, AV31ddo_AutorizacaoConsumo_VigenciaInicioTitleControlIdToReplace, AV37ddo_AutorizacaoConsumo_VigenciaFimTitleControlIdToReplace, AV41ddo_AutorizacaoConsumo_UnidadeMedicaoNomTitleControlIdToReplace, AV45ddo_AutorizacaoConsumo_QuantidadeTitleControlIdToReplace, AV49ddo_AutorizacaoConsumo_ValorTitleControlIdToReplace, AV53ddo_AutorizacaoConsumo_StatusTitleControlIdToReplace, AV56ddo_AutorizacaoConsumo_AtivoTitleControlIdToReplace, AV66Pgmname, AV52TFAutorizacaoConsumo_Status_Sels, A1780AutorizacaoConsumo_Ativo, sPrefix) ;
      }

      protected void S122( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV15Session.Get(AV66Pgmname+"GridState"), "") == 0 )
         {
            AV11GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  AV66Pgmname+"GridState"), "");
         }
         else
         {
            AV11GridState.FromXml(AV15Session.Get(AV66Pgmname+"GridState"), "");
         }
         AV13OrderedBy = AV11GridState.gxTpr_Orderedby;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         AV14OrderedDsc = AV11GridState.gxTpr_Ordereddsc;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
         /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV67GXV1 = 1;
         while ( AV67GXV1 <= AV11GridState.gxTpr_Filtervalues.Count )
         {
            AV12GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV11GridState.gxTpr_Filtervalues.Item(AV67GXV1));
            if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFAUTORIZACAOCONSUMO_CODIGO") == 0 )
            {
               AV23TFAutorizacaoConsumo_Codigo = (int)(NumberUtil.Val( AV12GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV23TFAutorizacaoConsumo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV23TFAutorizacaoConsumo_Codigo), 6, 0)));
               AV24TFAutorizacaoConsumo_Codigo_To = (int)(NumberUtil.Val( AV12GridStateFilterValue.gxTpr_Valueto, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24TFAutorizacaoConsumo_Codigo_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24TFAutorizacaoConsumo_Codigo_To), 6, 0)));
               if ( ! (0==AV23TFAutorizacaoConsumo_Codigo) )
               {
                  Ddo_autorizacaoconsumo_codigo_Filteredtext_set = StringUtil.Str( (decimal)(AV23TFAutorizacaoConsumo_Codigo), 6, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_autorizacaoconsumo_codigo_Internalname, "FilteredText_set", Ddo_autorizacaoconsumo_codigo_Filteredtext_set);
               }
               if ( ! (0==AV24TFAutorizacaoConsumo_Codigo_To) )
               {
                  Ddo_autorizacaoconsumo_codigo_Filteredtextto_set = StringUtil.Str( (decimal)(AV24TFAutorizacaoConsumo_Codigo_To), 6, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_autorizacaoconsumo_codigo_Internalname, "FilteredTextTo_set", Ddo_autorizacaoconsumo_codigo_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFAUTORIZACAOCONSUMO_VIGENCIAINICIO") == 0 )
            {
               AV27TFAutorizacaoConsumo_VigenciaInicio = context.localUtil.CToD( AV12GridStateFilterValue.gxTpr_Value, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27TFAutorizacaoConsumo_VigenciaInicio", context.localUtil.Format(AV27TFAutorizacaoConsumo_VigenciaInicio, "99/99/99"));
               AV28TFAutorizacaoConsumo_VigenciaInicio_To = context.localUtil.CToD( AV12GridStateFilterValue.gxTpr_Valueto, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV28TFAutorizacaoConsumo_VigenciaInicio_To", context.localUtil.Format(AV28TFAutorizacaoConsumo_VigenciaInicio_To, "99/99/99"));
               if ( ! (DateTime.MinValue==AV27TFAutorizacaoConsumo_VigenciaInicio) )
               {
                  Ddo_autorizacaoconsumo_vigenciainicio_Filteredtext_set = context.localUtil.DToC( AV27TFAutorizacaoConsumo_VigenciaInicio, 2, "/");
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_autorizacaoconsumo_vigenciainicio_Internalname, "FilteredText_set", Ddo_autorizacaoconsumo_vigenciainicio_Filteredtext_set);
               }
               if ( ! (DateTime.MinValue==AV28TFAutorizacaoConsumo_VigenciaInicio_To) )
               {
                  Ddo_autorizacaoconsumo_vigenciainicio_Filteredtextto_set = context.localUtil.DToC( AV28TFAutorizacaoConsumo_VigenciaInicio_To, 2, "/");
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_autorizacaoconsumo_vigenciainicio_Internalname, "FilteredTextTo_set", Ddo_autorizacaoconsumo_vigenciainicio_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFAUTORIZACAOCONSUMO_VIGENCIAFIM") == 0 )
            {
               AV33TFAutorizacaoConsumo_VigenciaFim = context.localUtil.CToD( AV12GridStateFilterValue.gxTpr_Value, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV33TFAutorizacaoConsumo_VigenciaFim", context.localUtil.Format(AV33TFAutorizacaoConsumo_VigenciaFim, "99/99/99"));
               AV34TFAutorizacaoConsumo_VigenciaFim_To = context.localUtil.CToD( AV12GridStateFilterValue.gxTpr_Valueto, 2);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV34TFAutorizacaoConsumo_VigenciaFim_To", context.localUtil.Format(AV34TFAutorizacaoConsumo_VigenciaFim_To, "99/99/99"));
               if ( ! (DateTime.MinValue==AV33TFAutorizacaoConsumo_VigenciaFim) )
               {
                  Ddo_autorizacaoconsumo_vigenciafim_Filteredtext_set = context.localUtil.DToC( AV33TFAutorizacaoConsumo_VigenciaFim, 2, "/");
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_autorizacaoconsumo_vigenciafim_Internalname, "FilteredText_set", Ddo_autorizacaoconsumo_vigenciafim_Filteredtext_set);
               }
               if ( ! (DateTime.MinValue==AV34TFAutorizacaoConsumo_VigenciaFim_To) )
               {
                  Ddo_autorizacaoconsumo_vigenciafim_Filteredtextto_set = context.localUtil.DToC( AV34TFAutorizacaoConsumo_VigenciaFim_To, 2, "/");
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_autorizacaoconsumo_vigenciafim_Internalname, "FilteredTextTo_set", Ddo_autorizacaoconsumo_vigenciafim_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFAUTORIZACAOCONSUMO_UNIDADEMEDICAONOM") == 0 )
            {
               AV39TFAutorizacaoConsumo_UnidadeMedicaoNom = AV12GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV39TFAutorizacaoConsumo_UnidadeMedicaoNom", AV39TFAutorizacaoConsumo_UnidadeMedicaoNom);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV39TFAutorizacaoConsumo_UnidadeMedicaoNom)) )
               {
                  Ddo_autorizacaoconsumo_unidademedicaonom_Filteredtext_set = AV39TFAutorizacaoConsumo_UnidadeMedicaoNom;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_autorizacaoconsumo_unidademedicaonom_Internalname, "FilteredText_set", Ddo_autorizacaoconsumo_unidademedicaonom_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFAUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_SEL") == 0 )
            {
               AV40TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel = AV12GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV40TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel", AV40TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV40TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel)) )
               {
                  Ddo_autorizacaoconsumo_unidademedicaonom_Selectedvalue_set = AV40TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_autorizacaoconsumo_unidademedicaonom_Internalname, "SelectedValue_set", Ddo_autorizacaoconsumo_unidademedicaonom_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFAUTORIZACAOCONSUMO_QUANTIDADE") == 0 )
            {
               AV43TFAutorizacaoConsumo_Quantidade = (short)(NumberUtil.Val( AV12GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV43TFAutorizacaoConsumo_Quantidade", StringUtil.LTrim( StringUtil.Str( (decimal)(AV43TFAutorizacaoConsumo_Quantidade), 3, 0)));
               AV44TFAutorizacaoConsumo_Quantidade_To = (short)(NumberUtil.Val( AV12GridStateFilterValue.gxTpr_Valueto, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV44TFAutorizacaoConsumo_Quantidade_To", StringUtil.LTrim( StringUtil.Str( (decimal)(AV44TFAutorizacaoConsumo_Quantidade_To), 3, 0)));
               if ( ! (0==AV43TFAutorizacaoConsumo_Quantidade) )
               {
                  Ddo_autorizacaoconsumo_quantidade_Filteredtext_set = StringUtil.Str( (decimal)(AV43TFAutorizacaoConsumo_Quantidade), 3, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_autorizacaoconsumo_quantidade_Internalname, "FilteredText_set", Ddo_autorizacaoconsumo_quantidade_Filteredtext_set);
               }
               if ( ! (0==AV44TFAutorizacaoConsumo_Quantidade_To) )
               {
                  Ddo_autorizacaoconsumo_quantidade_Filteredtextto_set = StringUtil.Str( (decimal)(AV44TFAutorizacaoConsumo_Quantidade_To), 3, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_autorizacaoconsumo_quantidade_Internalname, "FilteredTextTo_set", Ddo_autorizacaoconsumo_quantidade_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFAUTORIZACAOCONSUMO_VALOR") == 0 )
            {
               AV47TFAutorizacaoConsumo_Valor = NumberUtil.Val( AV12GridStateFilterValue.gxTpr_Value, ".");
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV47TFAutorizacaoConsumo_Valor", StringUtil.LTrim( StringUtil.Str( AV47TFAutorizacaoConsumo_Valor, 18, 5)));
               AV48TFAutorizacaoConsumo_Valor_To = NumberUtil.Val( AV12GridStateFilterValue.gxTpr_Valueto, ".");
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV48TFAutorizacaoConsumo_Valor_To", StringUtil.LTrim( StringUtil.Str( AV48TFAutorizacaoConsumo_Valor_To, 18, 5)));
               if ( ! (Convert.ToDecimal(0)==AV47TFAutorizacaoConsumo_Valor) )
               {
                  Ddo_autorizacaoconsumo_valor_Filteredtext_set = StringUtil.Str( AV47TFAutorizacaoConsumo_Valor, 18, 5);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_autorizacaoconsumo_valor_Internalname, "FilteredText_set", Ddo_autorizacaoconsumo_valor_Filteredtext_set);
               }
               if ( ! (Convert.ToDecimal(0)==AV48TFAutorizacaoConsumo_Valor_To) )
               {
                  Ddo_autorizacaoconsumo_valor_Filteredtextto_set = StringUtil.Str( AV48TFAutorizacaoConsumo_Valor_To, 18, 5);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_autorizacaoconsumo_valor_Internalname, "FilteredTextTo_set", Ddo_autorizacaoconsumo_valor_Filteredtextto_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFAUTORIZACAOCONSUMO_STATUS_SEL") == 0 )
            {
               AV51TFAutorizacaoConsumo_Status_SelsJson = AV12GridStateFilterValue.gxTpr_Value;
               AV52TFAutorizacaoConsumo_Status_Sels.FromJSonString(AV51TFAutorizacaoConsumo_Status_SelsJson);
               if ( ! ( AV52TFAutorizacaoConsumo_Status_Sels.Count == 0 ) )
               {
                  Ddo_autorizacaoconsumo_status_Selectedvalue_set = AV51TFAutorizacaoConsumo_Status_SelsJson;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_autorizacaoconsumo_status_Internalname, "SelectedValue_set", Ddo_autorizacaoconsumo_status_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV12GridStateFilterValue.gxTpr_Name, "TFAUTORIZACAOCONSUMO_ATIVO_SEL") == 0 )
            {
               AV55TFAutorizacaoConsumo_Ativo_Sel = (short)(NumberUtil.Val( AV12GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV55TFAutorizacaoConsumo_Ativo_Sel", StringUtil.Str( (decimal)(AV55TFAutorizacaoConsumo_Ativo_Sel), 1, 0));
               if ( ! (0==AV55TFAutorizacaoConsumo_Ativo_Sel) )
               {
                  Ddo_autorizacaoconsumo_ativo_Selectedvalue_set = StringUtil.Str( (decimal)(AV55TFAutorizacaoConsumo_Ativo_Sel), 1, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop(sPrefix, false, Ddo_autorizacaoconsumo_ativo_Internalname, "SelectedValue_set", Ddo_autorizacaoconsumo_ativo_Selectedvalue_set);
               }
            }
            AV67GXV1 = (int)(AV67GXV1+1);
         }
      }

      protected void S142( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV11GridState.FromXml(AV15Session.Get(AV66Pgmname+"GridState"), "");
         AV11GridState.gxTpr_Orderedby = AV13OrderedBy;
         AV11GridState.gxTpr_Ordereddsc = AV14OrderedDsc;
         AV11GridState.gxTpr_Filtervalues.Clear();
         if ( ! ( (0==AV23TFAutorizacaoConsumo_Codigo) && (0==AV24TFAutorizacaoConsumo_Codigo_To) ) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFAUTORIZACAOCONSUMO_CODIGO";
            AV12GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV23TFAutorizacaoConsumo_Codigo), 6, 0);
            AV12GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( (decimal)(AV24TFAutorizacaoConsumo_Codigo_To), 6, 0);
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! ( (DateTime.MinValue==AV27TFAutorizacaoConsumo_VigenciaInicio) && (DateTime.MinValue==AV28TFAutorizacaoConsumo_VigenciaInicio_To) ) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFAUTORIZACAOCONSUMO_VIGENCIAINICIO";
            AV12GridStateFilterValue.gxTpr_Value = context.localUtil.DToC( AV27TFAutorizacaoConsumo_VigenciaInicio, 2, "/");
            AV12GridStateFilterValue.gxTpr_Valueto = context.localUtil.DToC( AV28TFAutorizacaoConsumo_VigenciaInicio_To, 2, "/");
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! ( (DateTime.MinValue==AV33TFAutorizacaoConsumo_VigenciaFim) && (DateTime.MinValue==AV34TFAutorizacaoConsumo_VigenciaFim_To) ) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFAUTORIZACAOCONSUMO_VIGENCIAFIM";
            AV12GridStateFilterValue.gxTpr_Value = context.localUtil.DToC( AV33TFAutorizacaoConsumo_VigenciaFim, 2, "/");
            AV12GridStateFilterValue.gxTpr_Valueto = context.localUtil.DToC( AV34TFAutorizacaoConsumo_VigenciaFim_To, 2, "/");
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV39TFAutorizacaoConsumo_UnidadeMedicaoNom)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFAUTORIZACAOCONSUMO_UNIDADEMEDICAONOM";
            AV12GridStateFilterValue.gxTpr_Value = AV39TFAutorizacaoConsumo_UnidadeMedicaoNom;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV40TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel)) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFAUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_SEL";
            AV12GridStateFilterValue.gxTpr_Value = AV40TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel;
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! ( (0==AV43TFAutorizacaoConsumo_Quantidade) && (0==AV44TFAutorizacaoConsumo_Quantidade_To) ) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFAUTORIZACAOCONSUMO_QUANTIDADE";
            AV12GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV43TFAutorizacaoConsumo_Quantidade), 3, 0);
            AV12GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( (decimal)(AV44TFAutorizacaoConsumo_Quantidade_To), 3, 0);
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! ( (Convert.ToDecimal(0)==AV47TFAutorizacaoConsumo_Valor) && (Convert.ToDecimal(0)==AV48TFAutorizacaoConsumo_Valor_To) ) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFAUTORIZACAOCONSUMO_VALOR";
            AV12GridStateFilterValue.gxTpr_Value = StringUtil.Str( AV47TFAutorizacaoConsumo_Valor, 18, 5);
            AV12GridStateFilterValue.gxTpr_Valueto = StringUtil.Str( AV48TFAutorizacaoConsumo_Valor_To, 18, 5);
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! ( AV52TFAutorizacaoConsumo_Status_Sels.Count == 0 ) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFAUTORIZACAOCONSUMO_STATUS_SEL";
            AV12GridStateFilterValue.gxTpr_Value = AV52TFAutorizacaoConsumo_Status_Sels.ToJSonString(false);
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! (0==AV55TFAutorizacaoConsumo_Ativo_Sel) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "TFAUTORIZACAOCONSUMO_ATIVO_SEL";
            AV12GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV55TFAutorizacaoConsumo_Ativo_Sel), 1, 0);
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         if ( ! (0==AV7Contrato_Codigo) )
         {
            AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV12GridStateFilterValue.gxTpr_Name = "PARM_&CONTRATO_CODIGO";
            AV12GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV7Contrato_Codigo), 6, 0);
            AV11GridState.gxTpr_Filtervalues.Add(AV12GridStateFilterValue, 0);
         }
         new wwpbaseobjects.savegridstate(context ).execute(  AV66Pgmname+"GridState",  AV11GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_MeetrikaVs3")) ;
      }

      protected void S112( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV9TrnContext.gxTpr_Callerobject = AV66Pgmname;
         AV9TrnContext.gxTpr_Callerondelete = true;
         AV9TrnContext.gxTpr_Callerurl = AV8HTTPRequest.ScriptName+"?"+AV8HTTPRequest.QueryString;
         AV9TrnContext.gxTpr_Transactionname = "AutorizacaoConsumo";
         AV10TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV10TrnContextAtt.gxTpr_Attributename = "Contrato_Codigo";
         AV10TrnContextAtt.gxTpr_Attributevalue = StringUtil.Str( (decimal)(AV7Contrato_Codigo), 6, 0);
         AV9TrnContext.gxTpr_Attributes.Add(AV10TrnContextAtt, 0);
         AV15Session.Set("TrnContext", AV9TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_MeetrikaVs3"));
      }

      protected void S192( )
      {
         /* 'CHECKSECURITYFORACTIONS' Routine */
         GXt_boolean2 = AV16TempBoolean;
         GXt_boolean3 = false;
         new prc_autorizacaoconsumo_verificaativo(context ).execute(  A74Contrato_Codigo,  (GXt_boolean2 ? 1 : 0), out  GXt_boolean3) ;
         AV16TempBoolean = (bool)(!GXt_boolean2);
      }

      protected void wb_table4_52_NB2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabledvelop_confirmpanel_encerrar_Internalname, tblTabledvelop_confirmpanel_encerrar_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\">") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DVELOP_CONFIRMPANEL_ENCERRARContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+sPrefix+"DVELOP_CONFIRMPANEL_ENCERRARContainer"+"Body"+"\" style=\"display:none;\">") ;
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_52_NB2e( true) ;
         }
         else
         {
            wb_table4_52_NB2e( false) ;
         }
      }

      protected void wb_table3_47_NB2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabledvelop_confirmpanel_autorizar_Internalname, tblTabledvelop_confirmpanel_autorizar_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\">") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DVELOP_CONFIRMPANEL_AUTORIZARContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+sPrefix+"DVELOP_CONFIRMPANEL_AUTORIZARContainer"+"Body"+"\" style=\"display:none;\">") ;
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_47_NB2e( true) ;
         }
         else
         {
            wb_table3_47_NB2e( false) ;
         }
      }

      protected void wb_table2_42_NB2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabledvelop_confirmpanel_cancelarautorizcaoconsumo_Internalname, tblTabledvelop_confirmpanel_cancelarautorizcaoconsumo_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\">") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"DVELOP_CONFIRMPANEL_CANCELARAUTORIZCAOCONSUMOContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+sPrefix+"DVELOP_CONFIRMPANEL_CANCELARAUTORIZCAOCONSUMOContainer"+"Body"+"\" style=\"display:none;\">") ;
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_42_NB2e( true) ;
         }
         else
         {
            wb_table2_42_NB2e( false) ;
         }
      }

      protected void wb_table1_2_NB2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, sPrefix, "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='TableSearchCell'>") ;
            wb_table5_8_NB2( true) ;
         }
         else
         {
            wb_table5_8_NB2( false) ;
         }
         return  ;
      }

      protected void wb_table5_8_NB2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table6_13_NB2( true) ;
         }
         else
         {
            wb_table6_13_NB2( false) ;
         }
         return  ;
      }

      protected void wb_table6_13_NB2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_NB2e( true) ;
         }
         else
         {
            wb_table1_2_NB2e( false) ;
         }
      }

      protected void wb_table6_13_NB2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedgrid_Internalname, tblTablemergedgrid_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table7_16_NB2( true) ;
         }
         else
         {
            wb_table7_16_NB2( false) ;
         }
         return  ;
      }

      protected void wb_table7_16_NB2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 35,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgInsert_Internalname, context.GetImagePath( "5649fbb8-8ce0-4810-a5ce-bd649ea83c3a", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgInsert_Visible, 1, "", "Inserir", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgInsert_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOINSERT\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContratoAutorizacaoConsumoWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_13_NB2e( true) ;
         }
         else
         {
            wb_table6_13_NB2e( false) ;
         }
      }

      protected void wb_table7_16_NB2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"DivS\" data-gxgridid=\"19\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+((edtavCancelarautorizcaoconsumo_Visible==0) ? "display:none;" : "")+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+((edtavAutorizar_Visible==0) ? "display:none;" : "")+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+((edtavEncerrar_Visible==0) ? "display:none;" : "")+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtAutorizacaoConsumo_Codigo_Titleformat == 0 )
               {
                  context.SendWebValue( edtAutorizacaoConsumo_Codigo_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtAutorizacaoConsumo_Codigo_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtAutorizacaoConsumo_VigenciaInicio_Titleformat == 0 )
               {
                  context.SendWebValue( edtAutorizacaoConsumo_VigenciaInicio_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtAutorizacaoConsumo_VigenciaInicio_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtAutorizacaoConsumo_VigenciaFim_Titleformat == 0 )
               {
                  context.SendWebValue( edtAutorizacaoConsumo_VigenciaFim_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtAutorizacaoConsumo_VigenciaFim_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtAutorizacaoConsumo_UnidadeMedicaoNom_Titleformat == 0 )
               {
                  context.SendWebValue( edtAutorizacaoConsumo_UnidadeMedicaoNom_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtAutorizacaoConsumo_UnidadeMedicaoNom_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtAutorizacaoConsumo_Quantidade_Titleformat == 0 )
               {
                  context.SendWebValue( edtAutorizacaoConsumo_Quantidade_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtAutorizacaoConsumo_Quantidade_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtAutorizacaoConsumo_Valor_Titleformat == 0 )
               {
                  context.SendWebValue( edtAutorizacaoConsumo_Valor_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtAutorizacaoConsumo_Valor_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( cmbAutorizacaoConsumo_Status_Titleformat == 0 )
               {
                  context.SendWebValue( cmbAutorizacaoConsumo_Status.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( cmbAutorizacaoConsumo_Status.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( chkAutorizacaoConsumo_Ativo_Titleformat == 0 )
               {
                  context.SendWebValue( chkAutorizacaoConsumo_Ativo.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( chkAutorizacaoConsumo_Ativo.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", sPrefix);
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV17CancelarAutorizcaoConsumo));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavCancelarautorizcaoconsumo_Enabled), 5, 0, ".", "")));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavCancelarautorizcaoconsumo_Tooltiptext));
               GridColumn.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavCancelarautorizcaoconsumo_Visible), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV20Autorizar));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavAutorizar_Tooltiptext));
               GridColumn.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavAutorizar_Visible), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV21Encerrar));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavEncerrar_Tooltiptext));
               GridColumn.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavEncerrar_Visible), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1774AutorizacaoConsumo_Codigo), 6, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtAutorizacaoConsumo_Codigo_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtAutorizacaoConsumo_Codigo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.localUtil.Format(A1775AutorizacaoConsumo_VigenciaInicio, "99/99/99"));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtAutorizacaoConsumo_VigenciaInicio_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtAutorizacaoConsumo_VigenciaInicio_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.localUtil.Format(A1776AutorizacaoConsumo_VigenciaFim, "99/99/99"));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtAutorizacaoConsumo_VigenciaFim_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtAutorizacaoConsumo_VigenciaFim_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A1778AutorizacaoConsumo_UnidadeMedicaoNom));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtAutorizacaoConsumo_UnidadeMedicaoNom_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtAutorizacaoConsumo_UnidadeMedicaoNom_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1779AutorizacaoConsumo_Quantidade), 3, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtAutorizacaoConsumo_Quantidade_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtAutorizacaoConsumo_Quantidade_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( A1782AutorizacaoConsumo_Valor, 18, 5, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtAutorizacaoConsumo_Valor_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtAutorizacaoConsumo_Valor_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A1787AutorizacaoConsumo_Status));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( cmbAutorizacaoConsumo_Status.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbAutorizacaoConsumo_Status_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.BoolToStr( A1780AutorizacaoConsumo_Ativo));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( chkAutorizacaoConsumo_Ativo.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(chkAutorizacaoConsumo_Ativo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 19 )
         {
            wbEnd = 0;
            nRC_GXsfl_19 = (short)(nGXsfl_19_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid(sPrefix+"_"+"Grid", GridContainer);
               if ( ! isAjaxCallMode( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+sPrefix+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_16_NB2e( true) ;
         }
         else
         {
            wb_table7_16_NB2e( false) ;
         }
      }

      protected void wb_table5_8_NB2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablesearch_Internalname, tblTablesearch_Internalname, "", "TableSearch", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_8_NB2e( true) ;
         }
         else
         {
            wb_table5_8_NB2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV7Contrato_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Contrato_Codigo), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PANB2( ) ;
         WSNB2( ) ;
         WENB2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlAV7Contrato_Codigo = (String)((String)getParm(obj,0));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PANB2( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "contratoautorizacaoconsumowc");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PANB2( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            AV7Contrato_Codigo = Convert.ToInt32(getParm(obj,2));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Contrato_Codigo), 6, 0)));
         }
         wcpOAV7Contrato_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV7Contrato_Codigo"), ",", "."));
         if ( ! GetJustCreated( ) && ( ( AV7Contrato_Codigo != wcpOAV7Contrato_Codigo ) ) )
         {
            setjustcreated();
         }
         wcpOAV7Contrato_Codigo = AV7Contrato_Codigo;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlAV7Contrato_Codigo = cgiGet( sPrefix+"AV7Contrato_Codigo_CTRL");
         if ( StringUtil.Len( sCtrlAV7Contrato_Codigo) > 0 )
         {
            AV7Contrato_Codigo = (int)(context.localUtil.CToN( cgiGet( sCtrlAV7Contrato_Codigo), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7Contrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7Contrato_Codigo), 6, 0)));
         }
         else
         {
            AV7Contrato_Codigo = (int)(context.localUtil.CToN( cgiGet( sPrefix+"AV7Contrato_Codigo_PARM"), ",", "."));
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PANB2( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WSNB2( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WSNB2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"AV7Contrato_Codigo_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7Contrato_Codigo), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlAV7Contrato_Codigo)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"AV7Contrato_Codigo_CTRL", StringUtil.RTrim( sCtrlAV7Contrato_Codigo));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WENB2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?202051812555286");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("contratoautorizacaoconsumowc.js", "?202051812555287");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/ConfirmPanel/BootstrapConfirmPanelRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/ConfirmPanel/BootstrapConfirmPanelRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/ConfirmPanel/BootstrapConfirmPanelRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_192( )
      {
         edtavCancelarautorizcaoconsumo_Internalname = sPrefix+"vCANCELARAUTORIZCAOCONSUMO_"+sGXsfl_19_idx;
         edtavAutorizar_Internalname = sPrefix+"vAUTORIZAR_"+sGXsfl_19_idx;
         edtavEncerrar_Internalname = sPrefix+"vENCERRAR_"+sGXsfl_19_idx;
         edtAutorizacaoConsumo_Codigo_Internalname = sPrefix+"AUTORIZACAOCONSUMO_CODIGO_"+sGXsfl_19_idx;
         edtAutorizacaoConsumo_VigenciaInicio_Internalname = sPrefix+"AUTORIZACAOCONSUMO_VIGENCIAINICIO_"+sGXsfl_19_idx;
         edtAutorizacaoConsumo_VigenciaFim_Internalname = sPrefix+"AUTORIZACAOCONSUMO_VIGENCIAFIM_"+sGXsfl_19_idx;
         edtAutorizacaoConsumo_UnidadeMedicaoNom_Internalname = sPrefix+"AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_"+sGXsfl_19_idx;
         edtAutorizacaoConsumo_Quantidade_Internalname = sPrefix+"AUTORIZACAOCONSUMO_QUANTIDADE_"+sGXsfl_19_idx;
         edtAutorizacaoConsumo_Valor_Internalname = sPrefix+"AUTORIZACAOCONSUMO_VALOR_"+sGXsfl_19_idx;
         cmbAutorizacaoConsumo_Status_Internalname = sPrefix+"AUTORIZACAOCONSUMO_STATUS_"+sGXsfl_19_idx;
         chkAutorizacaoConsumo_Ativo_Internalname = sPrefix+"AUTORIZACAOCONSUMO_ATIVO_"+sGXsfl_19_idx;
      }

      protected void SubsflControlProps_fel_192( )
      {
         edtavCancelarautorizcaoconsumo_Internalname = sPrefix+"vCANCELARAUTORIZCAOCONSUMO_"+sGXsfl_19_fel_idx;
         edtavAutorizar_Internalname = sPrefix+"vAUTORIZAR_"+sGXsfl_19_fel_idx;
         edtavEncerrar_Internalname = sPrefix+"vENCERRAR_"+sGXsfl_19_fel_idx;
         edtAutorizacaoConsumo_Codigo_Internalname = sPrefix+"AUTORIZACAOCONSUMO_CODIGO_"+sGXsfl_19_fel_idx;
         edtAutorizacaoConsumo_VigenciaInicio_Internalname = sPrefix+"AUTORIZACAOCONSUMO_VIGENCIAINICIO_"+sGXsfl_19_fel_idx;
         edtAutorizacaoConsumo_VigenciaFim_Internalname = sPrefix+"AUTORIZACAOCONSUMO_VIGENCIAFIM_"+sGXsfl_19_fel_idx;
         edtAutorizacaoConsumo_UnidadeMedicaoNom_Internalname = sPrefix+"AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_"+sGXsfl_19_fel_idx;
         edtAutorizacaoConsumo_Quantidade_Internalname = sPrefix+"AUTORIZACAOCONSUMO_QUANTIDADE_"+sGXsfl_19_fel_idx;
         edtAutorizacaoConsumo_Valor_Internalname = sPrefix+"AUTORIZACAOCONSUMO_VALOR_"+sGXsfl_19_fel_idx;
         cmbAutorizacaoConsumo_Status_Internalname = sPrefix+"AUTORIZACAOCONSUMO_STATUS_"+sGXsfl_19_fel_idx;
         chkAutorizacaoConsumo_Ativo_Internalname = sPrefix+"AUTORIZACAOCONSUMO_ATIVO_"+sGXsfl_19_fel_idx;
      }

      protected void sendrow_192( )
      {
         SubsflControlProps_192( ) ;
         WBNB0( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_19_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_19_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_19_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+((edtavCancelarautorizcaoconsumo_Visible==0) ? "display:none;" : "")+"\">") ;
            }
            /* Active Bitmap Variable */
            TempTags = " " + ((edtavCancelarautorizcaoconsumo_Enabled!=0)&&(edtavCancelarautorizcaoconsumo_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 20,'"+sPrefix+"',false,'',19)\"" : " ");
            ClassString = "Image";
            StyleString = "";
            AV17CancelarAutorizcaoConsumo_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV17CancelarAutorizcaoConsumo))&&String.IsNullOrEmpty(StringUtil.RTrim( AV63Cancelarautorizcaoconsumo_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV17CancelarAutorizcaoConsumo)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavCancelarautorizcaoconsumo_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV17CancelarAutorizcaoConsumo)) ? AV63Cancelarautorizcaoconsumo_GXI : context.PathToRelativeUrl( AV17CancelarAutorizcaoConsumo)),(String)"",(String)"",(String)"",context.GetTheme( ),(int)edtavCancelarautorizcaoconsumo_Visible,(int)edtavCancelarautorizcaoconsumo_Enabled,(String)"",(String)edtavCancelarautorizcaoconsumo_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)7,(String)edtavCancelarautorizcaoconsumo_Jsonclick,(String)"'"+sPrefix+"'"+",false,"+"'"+"e27nb2_client"+"'",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)""+TempTags,(String)"",(String)"",(short)1,(bool)AV17CancelarAutorizcaoConsumo_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+((edtavAutorizar_Visible==0) ? "display:none;" : "")+"\">") ;
            }
            /* Active Bitmap Variable */
            TempTags = " " + ((edtavAutorizar_Enabled!=0)&&(edtavAutorizar_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 21,'"+sPrefix+"',false,'',19)\"" : " ");
            ClassString = "Image";
            StyleString = "";
            AV20Autorizar_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV20Autorizar))&&String.IsNullOrEmpty(StringUtil.RTrim( AV64Autorizar_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV20Autorizar)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavAutorizar_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV20Autorizar)) ? AV64Autorizar_GXI : context.PathToRelativeUrl( AV20Autorizar)),(String)"",(String)"",(String)"",context.GetTheme( ),(int)edtavAutorizar_Visible,(short)1,(String)"",(String)edtavAutorizar_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)7,(String)edtavAutorizar_Jsonclick,(String)"'"+sPrefix+"'"+",false,"+"'"+"e28nb2_client"+"'",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)""+TempTags,(String)"",(String)"",(short)1,(bool)AV20Autorizar_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+((edtavEncerrar_Visible==0) ? "display:none;" : "")+"\">") ;
            }
            /* Active Bitmap Variable */
            TempTags = " " + ((edtavEncerrar_Enabled!=0)&&(edtavEncerrar_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 22,'"+sPrefix+"',false,'',19)\"" : " ");
            ClassString = "Image";
            StyleString = "";
            AV21Encerrar_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV21Encerrar))&&String.IsNullOrEmpty(StringUtil.RTrim( AV65Encerrar_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV21Encerrar)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavEncerrar_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV21Encerrar)) ? AV65Encerrar_GXI : context.PathToRelativeUrl( AV21Encerrar)),(String)"",(String)"",(String)"",context.GetTheme( ),(int)edtavEncerrar_Visible,(short)1,(String)"",(String)edtavEncerrar_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)7,(String)edtavEncerrar_Jsonclick,(String)"'"+sPrefix+"'"+",false,"+"'"+"e29nb2_client"+"'",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)""+TempTags,(String)"",(String)"",(short)1,(bool)AV21Encerrar_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtAutorizacaoConsumo_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A1774AutorizacaoConsumo_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A1774AutorizacaoConsumo_Codigo), "ZZZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtAutorizacaoConsumo_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)19,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtAutorizacaoConsumo_VigenciaInicio_Internalname,context.localUtil.Format(A1775AutorizacaoConsumo_VigenciaInicio, "99/99/99"),context.localUtil.Format( A1775AutorizacaoConsumo_VigenciaInicio, "99/99/99"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtAutorizacaoConsumo_VigenciaInicio_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)8,(short)0,(short)0,(short)19,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtAutorizacaoConsumo_VigenciaFim_Internalname,context.localUtil.Format(A1776AutorizacaoConsumo_VigenciaFim, "99/99/99"),context.localUtil.Format( A1776AutorizacaoConsumo_VigenciaFim, "99/99/99"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtAutorizacaoConsumo_VigenciaFim_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)8,(short)0,(short)0,(short)19,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtAutorizacaoConsumo_UnidadeMedicaoNom_Internalname,StringUtil.RTrim( A1778AutorizacaoConsumo_UnidadeMedicaoNom),StringUtil.RTrim( context.localUtil.Format( A1778AutorizacaoConsumo_UnidadeMedicaoNom, "@!")),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtAutorizacaoConsumo_UnidadeMedicaoNom_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)19,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtAutorizacaoConsumo_Quantidade_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A1779AutorizacaoConsumo_Quantidade), 3, 0, ",", "")),context.localUtil.Format( (decimal)(A1779AutorizacaoConsumo_Quantidade), "ZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtAutorizacaoConsumo_Quantidade_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)3,(short)0,(short)0,(short)19,(short)1,(short)-1,(short)0,(bool)true,(String)"Quantidade",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtAutorizacaoConsumo_Valor_Internalname,StringUtil.LTrim( StringUtil.NToC( A1782AutorizacaoConsumo_Valor, 18, 5, ",", "")),context.localUtil.Format( A1782AutorizacaoConsumo_Valor, "ZZZ,ZZZ,ZZZ,ZZ9.99"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtAutorizacaoConsumo_Valor_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)18,(short)0,(short)0,(short)19,(short)1,(short)-1,(short)0,(bool)true,(String)"Valor",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            if ( ( nGXsfl_19_idx == 1 ) && isAjaxCallMode( ) )
            {
               GXCCtl = "AUTORIZACAOCONSUMO_STATUS_" + sGXsfl_19_idx;
               cmbAutorizacaoConsumo_Status.Name = GXCCtl;
               cmbAutorizacaoConsumo_Status.WebTags = "";
               cmbAutorizacaoConsumo_Status.addItem("CRI", "Criada", 0);
               cmbAutorizacaoConsumo_Status.addItem("AUT", "Autorizada", 0);
               cmbAutorizacaoConsumo_Status.addItem("CAN", "Cancelada", 0);
               cmbAutorizacaoConsumo_Status.addItem("UTI", "Utilizada", 0);
               cmbAutorizacaoConsumo_Status.addItem("ENC", "Encerrada", 0);
               if ( cmbAutorizacaoConsumo_Status.ItemCount > 0 )
               {
                  A1787AutorizacaoConsumo_Status = cmbAutorizacaoConsumo_Status.getValidValue(A1787AutorizacaoConsumo_Status);
               }
            }
            /* ComboBox */
            GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbAutorizacaoConsumo_Status,(String)cmbAutorizacaoConsumo_Status_Internalname,StringUtil.RTrim( A1787AutorizacaoConsumo_Status),(short)1,(String)cmbAutorizacaoConsumo_Status_Jsonclick,(short)0,(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"char",(String)"",(short)-1,(short)0,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"BootstrapAttribute",(String)"",(String)"",(String)"",(bool)true});
            cmbAutorizacaoConsumo_Status.CurrentValue = StringUtil.RTrim( A1787AutorizacaoConsumo_Status);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbAutorizacaoConsumo_Status_Internalname, "Values", (String)(cmbAutorizacaoConsumo_Status.ToJavascriptSource()));
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Check box */
            ClassString = "AttSemBordaCheckBox";
            StyleString = "";
            GridRow.AddColumnProperties("checkbox", 1, isAjaxCallMode( ), new Object[] {(String)chkAutorizacaoConsumo_Ativo_Internalname,StringUtil.BoolToStr( A1780AutorizacaoConsumo_Ativo),(String)"",(String)"",(short)-1,(short)0,(String)"true",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)""});
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_AUTORIZACAOCONSUMO_CODIGO"+"_"+sGXsfl_19_idx, GetSecureSignedToken( sPrefix+sGXsfl_19_idx, context.localUtil.Format( (decimal)(A1774AutorizacaoConsumo_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_AUTORIZACAOCONSUMO_VIGENCIAINICIO"+"_"+sGXsfl_19_idx, GetSecureSignedToken( sPrefix+sGXsfl_19_idx, A1775AutorizacaoConsumo_VigenciaInicio));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_AUTORIZACAOCONSUMO_VIGENCIAFIM"+"_"+sGXsfl_19_idx, GetSecureSignedToken( sPrefix+sGXsfl_19_idx, A1776AutorizacaoConsumo_VigenciaFim));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_AUTORIZACAOCONSUMO_QUANTIDADE"+"_"+sGXsfl_19_idx, GetSecureSignedToken( sPrefix+sGXsfl_19_idx, context.localUtil.Format( (decimal)(A1779AutorizacaoConsumo_Quantidade), "ZZ9")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_AUTORIZACAOCONSUMO_VALOR"+"_"+sGXsfl_19_idx, GetSecureSignedToken( sPrefix+sGXsfl_19_idx, context.localUtil.Format( A1782AutorizacaoConsumo_Valor, "ZZZ,ZZZ,ZZZ,ZZ9.99")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_AUTORIZACAOCONSUMO_STATUS"+"_"+sGXsfl_19_idx, GetSecureSignedToken( sPrefix+sGXsfl_19_idx, StringUtil.RTrim( context.localUtil.Format( A1787AutorizacaoConsumo_Status, ""))));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_AUTORIZACAOCONSUMO_ATIVO"+"_"+sGXsfl_19_idx, GetSecureSignedToken( sPrefix+sGXsfl_19_idx, A1780AutorizacaoConsumo_Ativo));
            GridContainer.AddRow(GridRow);
            nGXsfl_19_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_19_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_19_idx+1));
            sGXsfl_19_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_19_idx), 4, 0)), 4, "0");
            SubsflControlProps_192( ) ;
         }
         /* End function sendrow_192 */
      }

      protected void init_default_properties( )
      {
         tblTablesearch_Internalname = sPrefix+"TABLESEARCH";
         edtavCancelarautorizcaoconsumo_Internalname = sPrefix+"vCANCELARAUTORIZCAOCONSUMO";
         edtavAutorizar_Internalname = sPrefix+"vAUTORIZAR";
         edtavEncerrar_Internalname = sPrefix+"vENCERRAR";
         edtAutorizacaoConsumo_Codigo_Internalname = sPrefix+"AUTORIZACAOCONSUMO_CODIGO";
         edtAutorizacaoConsumo_VigenciaInicio_Internalname = sPrefix+"AUTORIZACAOCONSUMO_VIGENCIAINICIO";
         edtAutorizacaoConsumo_VigenciaFim_Internalname = sPrefix+"AUTORIZACAOCONSUMO_VIGENCIAFIM";
         edtAutorizacaoConsumo_UnidadeMedicaoNom_Internalname = sPrefix+"AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM";
         edtAutorizacaoConsumo_Quantidade_Internalname = sPrefix+"AUTORIZACAOCONSUMO_QUANTIDADE";
         edtAutorizacaoConsumo_Valor_Internalname = sPrefix+"AUTORIZACAOCONSUMO_VALOR";
         cmbAutorizacaoConsumo_Status_Internalname = sPrefix+"AUTORIZACAOCONSUMO_STATUS";
         chkAutorizacaoConsumo_Ativo_Internalname = sPrefix+"AUTORIZACAOCONSUMO_ATIVO";
         Gridpaginationbar_Internalname = sPrefix+"GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = sPrefix+"GRIDTABLEWITHPAGINATIONBAR";
         imgInsert_Internalname = sPrefix+"INSERT";
         tblTablemergedgrid_Internalname = sPrefix+"TABLEMERGEDGRID";
         tblTablegridheader_Internalname = sPrefix+"TABLEGRIDHEADER";
         edtContrato_Codigo_Internalname = sPrefix+"CONTRATO_CODIGO";
         Workwithplusutilities1_Internalname = sPrefix+"WORKWITHPLUSUTILITIES1";
         edtavContrato_codigo_selected_Internalname = sPrefix+"vCONTRATO_CODIGO_SELECTED";
         edtavAutorizacaoconsumo_codigo_selected_Internalname = sPrefix+"vAUTORIZACAOCONSUMO_CODIGO_SELECTED";
         edtavOrderedby_Internalname = sPrefix+"vORDEREDBY";
         edtavOrdereddsc_Internalname = sPrefix+"vORDEREDDSC";
         Dvelop_confirmpanel_cancelarautorizcaoconsumo_Internalname = sPrefix+"DVELOP_CONFIRMPANEL_CANCELARAUTORIZCAOCONSUMO";
         tblTabledvelop_confirmpanel_cancelarautorizcaoconsumo_Internalname = sPrefix+"TABLEDVELOP_CONFIRMPANEL_CANCELARAUTORIZCAOCONSUMO";
         Dvelop_confirmpanel_autorizar_Internalname = sPrefix+"DVELOP_CONFIRMPANEL_AUTORIZAR";
         tblTabledvelop_confirmpanel_autorizar_Internalname = sPrefix+"TABLEDVELOP_CONFIRMPANEL_AUTORIZAR";
         Dvelop_confirmpanel_encerrar_Internalname = sPrefix+"DVELOP_CONFIRMPANEL_ENCERRAR";
         tblTabledvelop_confirmpanel_encerrar_Internalname = sPrefix+"TABLEDVELOP_CONFIRMPANEL_ENCERRAR";
         edtavTfautorizacaoconsumo_codigo_Internalname = sPrefix+"vTFAUTORIZACAOCONSUMO_CODIGO";
         edtavTfautorizacaoconsumo_codigo_to_Internalname = sPrefix+"vTFAUTORIZACAOCONSUMO_CODIGO_TO";
         edtavTfautorizacaoconsumo_vigenciainicio_Internalname = sPrefix+"vTFAUTORIZACAOCONSUMO_VIGENCIAINICIO";
         edtavTfautorizacaoconsumo_vigenciainicio_to_Internalname = sPrefix+"vTFAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO";
         edtavDdo_autorizacaoconsumo_vigenciainicioauxdate_Internalname = sPrefix+"vDDO_AUTORIZACAOCONSUMO_VIGENCIAINICIOAUXDATE";
         edtavDdo_autorizacaoconsumo_vigenciainicioauxdateto_Internalname = sPrefix+"vDDO_AUTORIZACAOCONSUMO_VIGENCIAINICIOAUXDATETO";
         divDdo_autorizacaoconsumo_vigenciainicioauxdates_Internalname = sPrefix+"DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIOAUXDATES";
         edtavTfautorizacaoconsumo_vigenciafim_Internalname = sPrefix+"vTFAUTORIZACAOCONSUMO_VIGENCIAFIM";
         edtavTfautorizacaoconsumo_vigenciafim_to_Internalname = sPrefix+"vTFAUTORIZACAOCONSUMO_VIGENCIAFIM_TO";
         edtavDdo_autorizacaoconsumo_vigenciafimauxdate_Internalname = sPrefix+"vDDO_AUTORIZACAOCONSUMO_VIGENCIAFIMAUXDATE";
         edtavDdo_autorizacaoconsumo_vigenciafimauxdateto_Internalname = sPrefix+"vDDO_AUTORIZACAOCONSUMO_VIGENCIAFIMAUXDATETO";
         divDdo_autorizacaoconsumo_vigenciafimauxdates_Internalname = sPrefix+"DDO_AUTORIZACAOCONSUMO_VIGENCIAFIMAUXDATES";
         edtavTfautorizacaoconsumo_unidademedicaonom_Internalname = sPrefix+"vTFAUTORIZACAOCONSUMO_UNIDADEMEDICAONOM";
         edtavTfautorizacaoconsumo_unidademedicaonom_sel_Internalname = sPrefix+"vTFAUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_SEL";
         edtavTfautorizacaoconsumo_quantidade_Internalname = sPrefix+"vTFAUTORIZACAOCONSUMO_QUANTIDADE";
         edtavTfautorizacaoconsumo_quantidade_to_Internalname = sPrefix+"vTFAUTORIZACAOCONSUMO_QUANTIDADE_TO";
         edtavTfautorizacaoconsumo_valor_Internalname = sPrefix+"vTFAUTORIZACAOCONSUMO_VALOR";
         edtavTfautorizacaoconsumo_valor_to_Internalname = sPrefix+"vTFAUTORIZACAOCONSUMO_VALOR_TO";
         edtavTfautorizacaoconsumo_ativo_sel_Internalname = sPrefix+"vTFAUTORIZACAOCONSUMO_ATIVO_SEL";
         Ddo_autorizacaoconsumo_codigo_Internalname = sPrefix+"DDO_AUTORIZACAOCONSUMO_CODIGO";
         edtavDdo_autorizacaoconsumo_codigotitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_AUTORIZACAOCONSUMO_CODIGOTITLECONTROLIDTOREPLACE";
         Ddo_autorizacaoconsumo_vigenciainicio_Internalname = sPrefix+"DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIO";
         edtavDdo_autorizacaoconsumo_vigenciainiciotitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_AUTORIZACAOCONSUMO_VIGENCIAINICIOTITLECONTROLIDTOREPLACE";
         Ddo_autorizacaoconsumo_vigenciafim_Internalname = sPrefix+"DDO_AUTORIZACAOCONSUMO_VIGENCIAFIM";
         edtavDdo_autorizacaoconsumo_vigenciafimtitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_AUTORIZACAOCONSUMO_VIGENCIAFIMTITLECONTROLIDTOREPLACE";
         Ddo_autorizacaoconsumo_unidademedicaonom_Internalname = sPrefix+"DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM";
         edtavDdo_autorizacaoconsumo_unidademedicaonomtitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOMTITLECONTROLIDTOREPLACE";
         Ddo_autorizacaoconsumo_quantidade_Internalname = sPrefix+"DDO_AUTORIZACAOCONSUMO_QUANTIDADE";
         edtavDdo_autorizacaoconsumo_quantidadetitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_AUTORIZACAOCONSUMO_QUANTIDADETITLECONTROLIDTOREPLACE";
         Ddo_autorizacaoconsumo_valor_Internalname = sPrefix+"DDO_AUTORIZACAOCONSUMO_VALOR";
         edtavDdo_autorizacaoconsumo_valortitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_AUTORIZACAOCONSUMO_VALORTITLECONTROLIDTOREPLACE";
         Ddo_autorizacaoconsumo_status_Internalname = sPrefix+"DDO_AUTORIZACAOCONSUMO_STATUS";
         edtavDdo_autorizacaoconsumo_statustitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_AUTORIZACAOCONSUMO_STATUSTITLECONTROLIDTOREPLACE";
         Ddo_autorizacaoconsumo_ativo_Internalname = sPrefix+"DDO_AUTORIZACAOCONSUMO_ATIVO";
         edtavDdo_autorizacaoconsumo_ativotitlecontrolidtoreplace_Internalname = sPrefix+"vDDO_AUTORIZACAOCONSUMO_ATIVOTITLECONTROLIDTOREPLACE";
         Form.Internalname = sPrefix+"FORM";
         subGrid_Internalname = sPrefix+"GRID";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         cmbAutorizacaoConsumo_Status_Jsonclick = "";
         edtAutorizacaoConsumo_Valor_Jsonclick = "";
         edtAutorizacaoConsumo_Quantidade_Jsonclick = "";
         edtAutorizacaoConsumo_UnidadeMedicaoNom_Jsonclick = "";
         edtAutorizacaoConsumo_VigenciaFim_Jsonclick = "";
         edtAutorizacaoConsumo_VigenciaInicio_Jsonclick = "";
         edtAutorizacaoConsumo_Codigo_Jsonclick = "";
         edtavEncerrar_Jsonclick = "";
         edtavEncerrar_Enabled = 1;
         edtavAutorizar_Jsonclick = "";
         edtavAutorizar_Enabled = 1;
         edtavCancelarautorizcaoconsumo_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtavEncerrar_Tooltiptext = "Encerrar";
         edtavAutorizar_Tooltiptext = "Autorizar";
         edtavCancelarautorizcaoconsumo_Tooltiptext = "Cancelar Autoriza��o de Consumo";
         edtavCancelarautorizcaoconsumo_Enabled = 1;
         chkAutorizacaoConsumo_Ativo_Titleformat = 0;
         cmbAutorizacaoConsumo_Status_Titleformat = 0;
         edtAutorizacaoConsumo_Valor_Titleformat = 0;
         edtAutorizacaoConsumo_Quantidade_Titleformat = 0;
         edtAutorizacaoConsumo_UnidadeMedicaoNom_Titleformat = 0;
         edtAutorizacaoConsumo_VigenciaFim_Titleformat = 0;
         edtAutorizacaoConsumo_VigenciaInicio_Titleformat = 0;
         edtAutorizacaoConsumo_Codigo_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         imgInsert_Visible = 1;
         edtavEncerrar_Visible = -1;
         edtavAutorizar_Visible = -1;
         edtavCancelarautorizcaoconsumo_Visible = -1;
         chkAutorizacaoConsumo_Ativo.Title.Text = "Ativo";
         cmbAutorizacaoConsumo_Status.Title.Text = "Status";
         edtAutorizacaoConsumo_Valor_Title = "Valor";
         edtAutorizacaoConsumo_Quantidade_Title = "Quantidade";
         edtAutorizacaoConsumo_UnidadeMedicaoNom_Title = "Unidade de Medi��o";
         edtAutorizacaoConsumo_VigenciaFim_Title = "Fim da Vig�ncia";
         edtAutorizacaoConsumo_VigenciaInicio_Title = "In�cio da Vig�ncia";
         edtAutorizacaoConsumo_Codigo_Title = "Autorizacao Consumo";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         chkAutorizacaoConsumo_Ativo.Caption = "";
         edtavDdo_autorizacaoconsumo_ativotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_autorizacaoconsumo_statustitlecontrolidtoreplace_Visible = 1;
         edtavDdo_autorizacaoconsumo_valortitlecontrolidtoreplace_Visible = 1;
         edtavDdo_autorizacaoconsumo_quantidadetitlecontrolidtoreplace_Visible = 1;
         edtavDdo_autorizacaoconsumo_unidademedicaonomtitlecontrolidtoreplace_Visible = 1;
         edtavDdo_autorizacaoconsumo_vigenciafimtitlecontrolidtoreplace_Visible = 1;
         edtavDdo_autorizacaoconsumo_vigenciainiciotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_autorizacaoconsumo_codigotitlecontrolidtoreplace_Visible = 1;
         edtavTfautorizacaoconsumo_ativo_sel_Jsonclick = "";
         edtavTfautorizacaoconsumo_ativo_sel_Visible = 1;
         edtavTfautorizacaoconsumo_valor_to_Jsonclick = "";
         edtavTfautorizacaoconsumo_valor_to_Visible = 1;
         edtavTfautorizacaoconsumo_valor_Jsonclick = "";
         edtavTfautorizacaoconsumo_valor_Visible = 1;
         edtavTfautorizacaoconsumo_quantidade_to_Jsonclick = "";
         edtavTfautorizacaoconsumo_quantidade_to_Visible = 1;
         edtavTfautorizacaoconsumo_quantidade_Jsonclick = "";
         edtavTfautorizacaoconsumo_quantidade_Visible = 1;
         edtavTfautorizacaoconsumo_unidademedicaonom_sel_Jsonclick = "";
         edtavTfautorizacaoconsumo_unidademedicaonom_sel_Visible = 1;
         edtavTfautorizacaoconsumo_unidademedicaonom_Jsonclick = "";
         edtavTfautorizacaoconsumo_unidademedicaonom_Visible = 1;
         edtavDdo_autorizacaoconsumo_vigenciafimauxdateto_Jsonclick = "";
         edtavDdo_autorizacaoconsumo_vigenciafimauxdate_Jsonclick = "";
         edtavTfautorizacaoconsumo_vigenciafim_to_Jsonclick = "";
         edtavTfautorizacaoconsumo_vigenciafim_to_Visible = 1;
         edtavTfautorizacaoconsumo_vigenciafim_Jsonclick = "";
         edtavTfautorizacaoconsumo_vigenciafim_Visible = 1;
         edtavDdo_autorizacaoconsumo_vigenciainicioauxdateto_Jsonclick = "";
         edtavDdo_autorizacaoconsumo_vigenciainicioauxdate_Jsonclick = "";
         edtavTfautorizacaoconsumo_vigenciainicio_to_Jsonclick = "";
         edtavTfautorizacaoconsumo_vigenciainicio_to_Visible = 1;
         edtavTfautorizacaoconsumo_vigenciainicio_Jsonclick = "";
         edtavTfautorizacaoconsumo_vigenciainicio_Visible = 1;
         edtavTfautorizacaoconsumo_codigo_to_Jsonclick = "";
         edtavTfautorizacaoconsumo_codigo_to_Visible = 1;
         edtavTfautorizacaoconsumo_codigo_Jsonclick = "";
         edtavTfautorizacaoconsumo_codigo_Visible = 1;
         edtavOrdereddsc_Jsonclick = "";
         edtavOrdereddsc_Visible = 1;
         edtavOrderedby_Jsonclick = "";
         edtavOrderedby_Visible = 1;
         edtavAutorizacaoconsumo_codigo_selected_Jsonclick = "";
         edtavAutorizacaoconsumo_codigo_selected_Visible = 1;
         edtavContrato_codigo_selected_Jsonclick = "";
         edtavContrato_codigo_selected_Visible = 1;
         edtContrato_Codigo_Jsonclick = "";
         edtContrato_Codigo_Visible = 1;
         Ddo_autorizacaoconsumo_ativo_Searchbuttontext = "Pesquisar";
         Ddo_autorizacaoconsumo_ativo_Cleanfilter = "Limpar pesquisa";
         Ddo_autorizacaoconsumo_ativo_Sortdsc = "Ordenar de Z � A";
         Ddo_autorizacaoconsumo_ativo_Sortasc = "Ordenar de A � Z";
         Ddo_autorizacaoconsumo_ativo_Datalistfixedvalues = "1:Marcado,2:Desmarcado";
         Ddo_autorizacaoconsumo_ativo_Datalisttype = "FixedValues";
         Ddo_autorizacaoconsumo_ativo_Includedatalist = Convert.ToBoolean( -1);
         Ddo_autorizacaoconsumo_ativo_Includefilter = Convert.ToBoolean( 0);
         Ddo_autorizacaoconsumo_ativo_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_autorizacaoconsumo_ativo_Includesortasc = Convert.ToBoolean( -1);
         Ddo_autorizacaoconsumo_ativo_Titlecontrolidtoreplace = "";
         Ddo_autorizacaoconsumo_ativo_Dropdownoptionstype = "GridTitleSettings";
         Ddo_autorizacaoconsumo_ativo_Cls = "ColumnSettings";
         Ddo_autorizacaoconsumo_ativo_Tooltip = "Op��es";
         Ddo_autorizacaoconsumo_ativo_Caption = "";
         Ddo_autorizacaoconsumo_status_Searchbuttontext = "Filtrar Selecionados";
         Ddo_autorizacaoconsumo_status_Cleanfilter = "Limpar pesquisa";
         Ddo_autorizacaoconsumo_status_Sortdsc = "Ordenar de Z � A";
         Ddo_autorizacaoconsumo_status_Sortasc = "Ordenar de A � Z";
         Ddo_autorizacaoconsumo_status_Datalistfixedvalues = "CRI:Criada,AUT:Autorizada,CAN:Cancelada,UTI:Utilizada,ENC:Encerrada";
         Ddo_autorizacaoconsumo_status_Allowmultipleselection = Convert.ToBoolean( -1);
         Ddo_autorizacaoconsumo_status_Datalisttype = "FixedValues";
         Ddo_autorizacaoconsumo_status_Includedatalist = Convert.ToBoolean( -1);
         Ddo_autorizacaoconsumo_status_Includefilter = Convert.ToBoolean( 0);
         Ddo_autorizacaoconsumo_status_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_autorizacaoconsumo_status_Includesortasc = Convert.ToBoolean( -1);
         Ddo_autorizacaoconsumo_status_Titlecontrolidtoreplace = "";
         Ddo_autorizacaoconsumo_status_Dropdownoptionstype = "GridTitleSettings";
         Ddo_autorizacaoconsumo_status_Cls = "ColumnSettings";
         Ddo_autorizacaoconsumo_status_Tooltip = "Op��es";
         Ddo_autorizacaoconsumo_status_Caption = "";
         Ddo_autorizacaoconsumo_valor_Searchbuttontext = "Pesquisar";
         Ddo_autorizacaoconsumo_valor_Rangefilterto = "At�";
         Ddo_autorizacaoconsumo_valor_Rangefilterfrom = "Desde";
         Ddo_autorizacaoconsumo_valor_Cleanfilter = "Limpar pesquisa";
         Ddo_autorizacaoconsumo_valor_Sortdsc = "Ordenar de Z � A";
         Ddo_autorizacaoconsumo_valor_Sortasc = "Ordenar de A � Z";
         Ddo_autorizacaoconsumo_valor_Includedatalist = Convert.ToBoolean( 0);
         Ddo_autorizacaoconsumo_valor_Filterisrange = Convert.ToBoolean( -1);
         Ddo_autorizacaoconsumo_valor_Filtertype = "Numeric";
         Ddo_autorizacaoconsumo_valor_Includefilter = Convert.ToBoolean( -1);
         Ddo_autorizacaoconsumo_valor_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_autorizacaoconsumo_valor_Includesortasc = Convert.ToBoolean( -1);
         Ddo_autorizacaoconsumo_valor_Titlecontrolidtoreplace = "";
         Ddo_autorizacaoconsumo_valor_Dropdownoptionstype = "GridTitleSettings";
         Ddo_autorizacaoconsumo_valor_Cls = "ColumnSettings";
         Ddo_autorizacaoconsumo_valor_Tooltip = "Op��es";
         Ddo_autorizacaoconsumo_valor_Caption = "";
         Ddo_autorizacaoconsumo_quantidade_Searchbuttontext = "Pesquisar";
         Ddo_autorizacaoconsumo_quantidade_Rangefilterto = "At�";
         Ddo_autorizacaoconsumo_quantidade_Rangefilterfrom = "Desde";
         Ddo_autorizacaoconsumo_quantidade_Cleanfilter = "Limpar pesquisa";
         Ddo_autorizacaoconsumo_quantidade_Sortdsc = "Ordenar de Z � A";
         Ddo_autorizacaoconsumo_quantidade_Sortasc = "Ordenar de A � Z";
         Ddo_autorizacaoconsumo_quantidade_Includedatalist = Convert.ToBoolean( 0);
         Ddo_autorizacaoconsumo_quantidade_Filterisrange = Convert.ToBoolean( -1);
         Ddo_autorizacaoconsumo_quantidade_Filtertype = "Numeric";
         Ddo_autorizacaoconsumo_quantidade_Includefilter = Convert.ToBoolean( -1);
         Ddo_autorizacaoconsumo_quantidade_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_autorizacaoconsumo_quantidade_Includesortasc = Convert.ToBoolean( -1);
         Ddo_autorizacaoconsumo_quantidade_Titlecontrolidtoreplace = "";
         Ddo_autorizacaoconsumo_quantidade_Dropdownoptionstype = "GridTitleSettings";
         Ddo_autorizacaoconsumo_quantidade_Cls = "ColumnSettings";
         Ddo_autorizacaoconsumo_quantidade_Tooltip = "Op��es";
         Ddo_autorizacaoconsumo_quantidade_Caption = "";
         Ddo_autorizacaoconsumo_unidademedicaonom_Searchbuttontext = "Pesquisar";
         Ddo_autorizacaoconsumo_unidademedicaonom_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_autorizacaoconsumo_unidademedicaonom_Cleanfilter = "Limpar pesquisa";
         Ddo_autorizacaoconsumo_unidademedicaonom_Loadingdata = "Carregando dados...";
         Ddo_autorizacaoconsumo_unidademedicaonom_Sortdsc = "Ordenar de Z � A";
         Ddo_autorizacaoconsumo_unidademedicaonom_Sortasc = "Ordenar de A � Z";
         Ddo_autorizacaoconsumo_unidademedicaonom_Datalistupdateminimumcharacters = 0;
         Ddo_autorizacaoconsumo_unidademedicaonom_Datalistproc = "GetContratoAutorizacaoConsumoWCFilterData";
         Ddo_autorizacaoconsumo_unidademedicaonom_Datalisttype = "Dynamic";
         Ddo_autorizacaoconsumo_unidademedicaonom_Includedatalist = Convert.ToBoolean( -1);
         Ddo_autorizacaoconsumo_unidademedicaonom_Filterisrange = Convert.ToBoolean( 0);
         Ddo_autorizacaoconsumo_unidademedicaonom_Filtertype = "Character";
         Ddo_autorizacaoconsumo_unidademedicaonom_Includefilter = Convert.ToBoolean( -1);
         Ddo_autorizacaoconsumo_unidademedicaonom_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_autorizacaoconsumo_unidademedicaonom_Includesortasc = Convert.ToBoolean( -1);
         Ddo_autorizacaoconsumo_unidademedicaonom_Titlecontrolidtoreplace = "";
         Ddo_autorizacaoconsumo_unidademedicaonom_Dropdownoptionstype = "GridTitleSettings";
         Ddo_autorizacaoconsumo_unidademedicaonom_Cls = "ColumnSettings";
         Ddo_autorizacaoconsumo_unidademedicaonom_Tooltip = "Op��es";
         Ddo_autorizacaoconsumo_unidademedicaonom_Caption = "";
         Ddo_autorizacaoconsumo_vigenciafim_Searchbuttontext = "Pesquisar";
         Ddo_autorizacaoconsumo_vigenciafim_Rangefilterto = "At�";
         Ddo_autorizacaoconsumo_vigenciafim_Rangefilterfrom = "Desde";
         Ddo_autorizacaoconsumo_vigenciafim_Cleanfilter = "Limpar pesquisa";
         Ddo_autorizacaoconsumo_vigenciafim_Sortdsc = "Ordenar de Z � A";
         Ddo_autorizacaoconsumo_vigenciafim_Sortasc = "Ordenar de A � Z";
         Ddo_autorizacaoconsumo_vigenciafim_Includedatalist = Convert.ToBoolean( 0);
         Ddo_autorizacaoconsumo_vigenciafim_Filterisrange = Convert.ToBoolean( -1);
         Ddo_autorizacaoconsumo_vigenciafim_Filtertype = "Date";
         Ddo_autorizacaoconsumo_vigenciafim_Includefilter = Convert.ToBoolean( -1);
         Ddo_autorizacaoconsumo_vigenciafim_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_autorizacaoconsumo_vigenciafim_Includesortasc = Convert.ToBoolean( -1);
         Ddo_autorizacaoconsumo_vigenciafim_Titlecontrolidtoreplace = "";
         Ddo_autorizacaoconsumo_vigenciafim_Dropdownoptionstype = "GridTitleSettings";
         Ddo_autorizacaoconsumo_vigenciafim_Cls = "ColumnSettings";
         Ddo_autorizacaoconsumo_vigenciafim_Tooltip = "Op��es";
         Ddo_autorizacaoconsumo_vigenciafim_Caption = "";
         Ddo_autorizacaoconsumo_vigenciainicio_Searchbuttontext = "Pesquisar";
         Ddo_autorizacaoconsumo_vigenciainicio_Rangefilterto = "At�";
         Ddo_autorizacaoconsumo_vigenciainicio_Rangefilterfrom = "Desde";
         Ddo_autorizacaoconsumo_vigenciainicio_Cleanfilter = "Limpar pesquisa";
         Ddo_autorizacaoconsumo_vigenciainicio_Sortdsc = "Ordenar de Z � A";
         Ddo_autorizacaoconsumo_vigenciainicio_Sortasc = "Ordenar de A � Z";
         Ddo_autorizacaoconsumo_vigenciainicio_Includedatalist = Convert.ToBoolean( 0);
         Ddo_autorizacaoconsumo_vigenciainicio_Filterisrange = Convert.ToBoolean( -1);
         Ddo_autorizacaoconsumo_vigenciainicio_Filtertype = "Date";
         Ddo_autorizacaoconsumo_vigenciainicio_Includefilter = Convert.ToBoolean( -1);
         Ddo_autorizacaoconsumo_vigenciainicio_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_autorizacaoconsumo_vigenciainicio_Includesortasc = Convert.ToBoolean( -1);
         Ddo_autorizacaoconsumo_vigenciainicio_Titlecontrolidtoreplace = "";
         Ddo_autorizacaoconsumo_vigenciainicio_Dropdownoptionstype = "GridTitleSettings";
         Ddo_autorizacaoconsumo_vigenciainicio_Cls = "ColumnSettings";
         Ddo_autorizacaoconsumo_vigenciainicio_Tooltip = "Op��es";
         Ddo_autorizacaoconsumo_vigenciainicio_Caption = "";
         Ddo_autorizacaoconsumo_codigo_Searchbuttontext = "Pesquisar";
         Ddo_autorizacaoconsumo_codigo_Rangefilterto = "At�";
         Ddo_autorizacaoconsumo_codigo_Rangefilterfrom = "Desde";
         Ddo_autorizacaoconsumo_codigo_Cleanfilter = "Limpar pesquisa";
         Ddo_autorizacaoconsumo_codigo_Sortdsc = "Ordenar de Z � A";
         Ddo_autorizacaoconsumo_codigo_Sortasc = "Ordenar de A � Z";
         Ddo_autorizacaoconsumo_codigo_Includedatalist = Convert.ToBoolean( 0);
         Ddo_autorizacaoconsumo_codigo_Filterisrange = Convert.ToBoolean( -1);
         Ddo_autorizacaoconsumo_codigo_Filtertype = "Numeric";
         Ddo_autorizacaoconsumo_codigo_Includefilter = Convert.ToBoolean( -1);
         Ddo_autorizacaoconsumo_codigo_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_autorizacaoconsumo_codigo_Includesortasc = Convert.ToBoolean( -1);
         Ddo_autorizacaoconsumo_codigo_Titlecontrolidtoreplace = "";
         Ddo_autorizacaoconsumo_codigo_Dropdownoptionstype = "GridTitleSettings";
         Ddo_autorizacaoconsumo_codigo_Cls = "ColumnSettings";
         Ddo_autorizacaoconsumo_codigo_Tooltip = "Op��es";
         Ddo_autorizacaoconsumo_codigo_Caption = "";
         Dvelop_confirmpanel_encerrar_Confirmtype = "1";
         Dvelop_confirmpanel_encerrar_Yesbuttonposition = "left";
         Dvelop_confirmpanel_encerrar_Cancelbuttoncaption = "Cancelar";
         Dvelop_confirmpanel_encerrar_Nobuttoncaption = "N�o";
         Dvelop_confirmpanel_encerrar_Yesbuttoncaption = "Sim";
         Dvelop_confirmpanel_encerrar_Confirmationtext = "Deseja realmente encerrar essa Autoriza��o de Consumo";
         Dvelop_confirmpanel_encerrar_Title = "Encerrar Autoriza��o de Consumo";
         Dvelop_confirmpanel_autorizar_Confirmtype = "1";
         Dvelop_confirmpanel_autorizar_Yesbuttonposition = "left";
         Dvelop_confirmpanel_autorizar_Cancelbuttoncaption = "Cancelar";
         Dvelop_confirmpanel_autorizar_Nobuttoncaption = "N�o";
         Dvelop_confirmpanel_autorizar_Yesbuttoncaption = "Sim";
         Dvelop_confirmpanel_autorizar_Confirmationtext = "Autoriza��o de Consumo";
         Dvelop_confirmpanel_autorizar_Title = "Deseja realmente aprovar essa Autoriza��o de Consumo?";
         Dvelop_confirmpanel_cancelarautorizcaoconsumo_Confirmtype = "1";
         Dvelop_confirmpanel_cancelarautorizcaoconsumo_Yesbuttonposition = "left";
         Dvelop_confirmpanel_cancelarautorizcaoconsumo_Cancelbuttoncaption = "Cancelar";
         Dvelop_confirmpanel_cancelarautorizcaoconsumo_Nobuttoncaption = "N�o";
         Dvelop_confirmpanel_cancelarautorizcaoconsumo_Yesbuttoncaption = "Sim";
         Dvelop_confirmpanel_cancelarautorizcaoconsumo_Confirmationtext = "Deseja realmente cancelar essa autoriz��o de consumo?";
         Dvelop_confirmpanel_cancelarautorizcaoconsumo_Title = "Cancelar Autoriza��o de Consumo";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         subGrid_Rows = 0;
         context.GX_msglist.DisplayMode = 1;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A1780AutorizacaoConsumo_Ativo',fld:'AUTORIZACAOCONSUMO_ATIVO',pic:'',hsh:true,nv:false},{av:'sPrefix',nv:''},{av:'AV25ddo_AutorizacaoConsumo_CodigoTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV31ddo_AutorizacaoConsumo_VigenciaInicioTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_VIGENCIAINICIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_AutorizacaoConsumo_VigenciaFimTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_VIGENCIAFIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_AutorizacaoConsumo_UnidadeMedicaoNomTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_AutorizacaoConsumo_QuantidadeTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_QUANTIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_AutorizacaoConsumo_ValorTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_VALORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_AutorizacaoConsumo_StatusTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_STATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_AutorizacaoConsumo_AtivoTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV23TFAutorizacaoConsumo_Codigo',fld:'vTFAUTORIZACAOCONSUMO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV24TFAutorizacaoConsumo_Codigo_To',fld:'vTFAUTORIZACAOCONSUMO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV27TFAutorizacaoConsumo_VigenciaInicio',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAINICIO',pic:'',nv:''},{av:'AV28TFAutorizacaoConsumo_VigenciaInicio_To',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO',pic:'',nv:''},{av:'AV33TFAutorizacaoConsumo_VigenciaFim',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAFIM',pic:'',nv:''},{av:'AV34TFAutorizacaoConsumo_VigenciaFim_To',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAFIM_TO',pic:'',nv:''},{av:'AV39TFAutorizacaoConsumo_UnidadeMedicaoNom',fld:'vTFAUTORIZACAOCONSUMO_UNIDADEMEDICAONOM',pic:'@!',nv:''},{av:'AV40TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel',fld:'vTFAUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_SEL',pic:'@!',nv:''},{av:'AV43TFAutorizacaoConsumo_Quantidade',fld:'vTFAUTORIZACAOCONSUMO_QUANTIDADE',pic:'ZZ9',nv:0},{av:'AV44TFAutorizacaoConsumo_Quantidade_To',fld:'vTFAUTORIZACAOCONSUMO_QUANTIDADE_TO',pic:'ZZ9',nv:0},{av:'AV47TFAutorizacaoConsumo_Valor',fld:'vTFAUTORIZACAOCONSUMO_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV48TFAutorizacaoConsumo_Valor_To',fld:'vTFAUTORIZACAOCONSUMO_VALOR_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV52TFAutorizacaoConsumo_Status_Sels',fld:'vTFAUTORIZACAOCONSUMO_STATUS_SELS',pic:'',nv:null},{av:'AV55TFAutorizacaoConsumo_Ativo_Sel',fld:'vTFAUTORIZACAOCONSUMO_ATIVO_SEL',pic:'9',nv:0},{av:'AV7Contrato_Codigo',fld:'vCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV22AutorizacaoConsumo_CodigoTitleFilterData',fld:'vAUTORIZACAOCONSUMO_CODIGOTITLEFILTERDATA',pic:'',nv:null},{av:'AV26AutorizacaoConsumo_VigenciaInicioTitleFilterData',fld:'vAUTORIZACAOCONSUMO_VIGENCIAINICIOTITLEFILTERDATA',pic:'',nv:null},{av:'AV32AutorizacaoConsumo_VigenciaFimTitleFilterData',fld:'vAUTORIZACAOCONSUMO_VIGENCIAFIMTITLEFILTERDATA',pic:'',nv:null},{av:'AV38AutorizacaoConsumo_UnidadeMedicaoNomTitleFilterData',fld:'vAUTORIZACAOCONSUMO_UNIDADEMEDICAONOMTITLEFILTERDATA',pic:'',nv:null},{av:'AV42AutorizacaoConsumo_QuantidadeTitleFilterData',fld:'vAUTORIZACAOCONSUMO_QUANTIDADETITLEFILTERDATA',pic:'',nv:null},{av:'AV46AutorizacaoConsumo_ValorTitleFilterData',fld:'vAUTORIZACAOCONSUMO_VALORTITLEFILTERDATA',pic:'',nv:null},{av:'AV50AutorizacaoConsumo_StatusTitleFilterData',fld:'vAUTORIZACAOCONSUMO_STATUSTITLEFILTERDATA',pic:'',nv:null},{av:'AV54AutorizacaoConsumo_AtivoTitleFilterData',fld:'vAUTORIZACAOCONSUMO_ATIVOTITLEFILTERDATA',pic:'',nv:null},{av:'edtAutorizacaoConsumo_Codigo_Titleformat',ctrl:'AUTORIZACAOCONSUMO_CODIGO',prop:'Titleformat'},{av:'edtAutorizacaoConsumo_Codigo_Title',ctrl:'AUTORIZACAOCONSUMO_CODIGO',prop:'Title'},{av:'edtAutorizacaoConsumo_VigenciaInicio_Titleformat',ctrl:'AUTORIZACAOCONSUMO_VIGENCIAINICIO',prop:'Titleformat'},{av:'edtAutorizacaoConsumo_VigenciaInicio_Title',ctrl:'AUTORIZACAOCONSUMO_VIGENCIAINICIO',prop:'Title'},{av:'edtAutorizacaoConsumo_VigenciaFim_Titleformat',ctrl:'AUTORIZACAOCONSUMO_VIGENCIAFIM',prop:'Titleformat'},{av:'edtAutorizacaoConsumo_VigenciaFim_Title',ctrl:'AUTORIZACAOCONSUMO_VIGENCIAFIM',prop:'Title'},{av:'edtAutorizacaoConsumo_UnidadeMedicaoNom_Titleformat',ctrl:'AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM',prop:'Titleformat'},{av:'edtAutorizacaoConsumo_UnidadeMedicaoNom_Title',ctrl:'AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM',prop:'Title'},{av:'edtAutorizacaoConsumo_Quantidade_Titleformat',ctrl:'AUTORIZACAOCONSUMO_QUANTIDADE',prop:'Titleformat'},{av:'edtAutorizacaoConsumo_Quantidade_Title',ctrl:'AUTORIZACAOCONSUMO_QUANTIDADE',prop:'Title'},{av:'edtAutorizacaoConsumo_Valor_Titleformat',ctrl:'AUTORIZACAOCONSUMO_VALOR',prop:'Titleformat'},{av:'edtAutorizacaoConsumo_Valor_Title',ctrl:'AUTORIZACAOCONSUMO_VALOR',prop:'Title'},{av:'cmbAutorizacaoConsumo_Status'},{av:'chkAutorizacaoConsumo_Ativo_Titleformat',ctrl:'AUTORIZACAOCONSUMO_ATIVO',prop:'Titleformat'},{av:'chkAutorizacaoConsumo_Ativo.Title.Text',ctrl:'AUTORIZACAOCONSUMO_ATIVO',prop:'Title'},{av:'AV59GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV60GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'imgInsert_Visible',ctrl:'INSERT',prop:'Visible'},{av:'edtavCancelarautorizcaoconsumo_Visible',ctrl:'vCANCELARAUTORIZCAOCONSUMO',prop:'Visible'},{av:'edtavAutorizar_Visible',ctrl:'vAUTORIZAR',prop:'Visible'},{av:'edtavEncerrar_Visible',ctrl:'vENCERRAR',prop:'Visible'}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E11NB2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV23TFAutorizacaoConsumo_Codigo',fld:'vTFAUTORIZACAOCONSUMO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV24TFAutorizacaoConsumo_Codigo_To',fld:'vTFAUTORIZACAOCONSUMO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV27TFAutorizacaoConsumo_VigenciaInicio',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAINICIO',pic:'',nv:''},{av:'AV28TFAutorizacaoConsumo_VigenciaInicio_To',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO',pic:'',nv:''},{av:'AV33TFAutorizacaoConsumo_VigenciaFim',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAFIM',pic:'',nv:''},{av:'AV34TFAutorizacaoConsumo_VigenciaFim_To',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAFIM_TO',pic:'',nv:''},{av:'AV39TFAutorizacaoConsumo_UnidadeMedicaoNom',fld:'vTFAUTORIZACAOCONSUMO_UNIDADEMEDICAONOM',pic:'@!',nv:''},{av:'AV40TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel',fld:'vTFAUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_SEL',pic:'@!',nv:''},{av:'AV43TFAutorizacaoConsumo_Quantidade',fld:'vTFAUTORIZACAOCONSUMO_QUANTIDADE',pic:'ZZ9',nv:0},{av:'AV44TFAutorizacaoConsumo_Quantidade_To',fld:'vTFAUTORIZACAOCONSUMO_QUANTIDADE_TO',pic:'ZZ9',nv:0},{av:'AV47TFAutorizacaoConsumo_Valor',fld:'vTFAUTORIZACAOCONSUMO_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV48TFAutorizacaoConsumo_Valor_To',fld:'vTFAUTORIZACAOCONSUMO_VALOR_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV55TFAutorizacaoConsumo_Ativo_Sel',fld:'vTFAUTORIZACAOCONSUMO_ATIVO_SEL',pic:'9',nv:0},{av:'AV7Contrato_Codigo',fld:'vCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV25ddo_AutorizacaoConsumo_CodigoTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV31ddo_AutorizacaoConsumo_VigenciaInicioTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_VIGENCIAINICIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_AutorizacaoConsumo_VigenciaFimTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_VIGENCIAFIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_AutorizacaoConsumo_UnidadeMedicaoNomTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_AutorizacaoConsumo_QuantidadeTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_QUANTIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_AutorizacaoConsumo_ValorTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_VALORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_AutorizacaoConsumo_StatusTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_STATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_AutorizacaoConsumo_AtivoTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV52TFAutorizacaoConsumo_Status_Sels',fld:'vTFAUTORIZACAOCONSUMO_STATUS_SELS',pic:'',nv:null},{av:'A1780AutorizacaoConsumo_Ativo',fld:'AUTORIZACAOCONSUMO_ATIVO',pic:'',hsh:true,nv:false},{av:'sPrefix',nv:''},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("DDO_AUTORIZACAOCONSUMO_CODIGO.ONOPTIONCLICKED","{handler:'E15NB2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV23TFAutorizacaoConsumo_Codigo',fld:'vTFAUTORIZACAOCONSUMO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV24TFAutorizacaoConsumo_Codigo_To',fld:'vTFAUTORIZACAOCONSUMO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV27TFAutorizacaoConsumo_VigenciaInicio',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAINICIO',pic:'',nv:''},{av:'AV28TFAutorizacaoConsumo_VigenciaInicio_To',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO',pic:'',nv:''},{av:'AV33TFAutorizacaoConsumo_VigenciaFim',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAFIM',pic:'',nv:''},{av:'AV34TFAutorizacaoConsumo_VigenciaFim_To',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAFIM_TO',pic:'',nv:''},{av:'AV39TFAutorizacaoConsumo_UnidadeMedicaoNom',fld:'vTFAUTORIZACAOCONSUMO_UNIDADEMEDICAONOM',pic:'@!',nv:''},{av:'AV40TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel',fld:'vTFAUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_SEL',pic:'@!',nv:''},{av:'AV43TFAutorizacaoConsumo_Quantidade',fld:'vTFAUTORIZACAOCONSUMO_QUANTIDADE',pic:'ZZ9',nv:0},{av:'AV44TFAutorizacaoConsumo_Quantidade_To',fld:'vTFAUTORIZACAOCONSUMO_QUANTIDADE_TO',pic:'ZZ9',nv:0},{av:'AV47TFAutorizacaoConsumo_Valor',fld:'vTFAUTORIZACAOCONSUMO_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV48TFAutorizacaoConsumo_Valor_To',fld:'vTFAUTORIZACAOCONSUMO_VALOR_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV55TFAutorizacaoConsumo_Ativo_Sel',fld:'vTFAUTORIZACAOCONSUMO_ATIVO_SEL',pic:'9',nv:0},{av:'AV7Contrato_Codigo',fld:'vCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV25ddo_AutorizacaoConsumo_CodigoTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV31ddo_AutorizacaoConsumo_VigenciaInicioTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_VIGENCIAINICIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_AutorizacaoConsumo_VigenciaFimTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_VIGENCIAFIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_AutorizacaoConsumo_UnidadeMedicaoNomTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_AutorizacaoConsumo_QuantidadeTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_QUANTIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_AutorizacaoConsumo_ValorTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_VALORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_AutorizacaoConsumo_StatusTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_STATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_AutorizacaoConsumo_AtivoTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV52TFAutorizacaoConsumo_Status_Sels',fld:'vTFAUTORIZACAOCONSUMO_STATUS_SELS',pic:'',nv:null},{av:'A1780AutorizacaoConsumo_Ativo',fld:'AUTORIZACAOCONSUMO_ATIVO',pic:'',hsh:true,nv:false},{av:'sPrefix',nv:''},{av:'Ddo_autorizacaoconsumo_codigo_Activeeventkey',ctrl:'DDO_AUTORIZACAOCONSUMO_CODIGO',prop:'ActiveEventKey'},{av:'Ddo_autorizacaoconsumo_codigo_Filteredtext_get',ctrl:'DDO_AUTORIZACAOCONSUMO_CODIGO',prop:'FilteredText_get'},{av:'Ddo_autorizacaoconsumo_codigo_Filteredtextto_get',ctrl:'DDO_AUTORIZACAOCONSUMO_CODIGO',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_autorizacaoconsumo_codigo_Sortedstatus',ctrl:'DDO_AUTORIZACAOCONSUMO_CODIGO',prop:'SortedStatus'},{av:'AV23TFAutorizacaoConsumo_Codigo',fld:'vTFAUTORIZACAOCONSUMO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV24TFAutorizacaoConsumo_Codigo_To',fld:'vTFAUTORIZACAOCONSUMO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'Ddo_autorizacaoconsumo_vigenciainicio_Sortedstatus',ctrl:'DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIO',prop:'SortedStatus'},{av:'Ddo_autorizacaoconsumo_vigenciafim_Sortedstatus',ctrl:'DDO_AUTORIZACAOCONSUMO_VIGENCIAFIM',prop:'SortedStatus'},{av:'Ddo_autorizacaoconsumo_unidademedicaonom_Sortedstatus',ctrl:'DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM',prop:'SortedStatus'},{av:'Ddo_autorizacaoconsumo_quantidade_Sortedstatus',ctrl:'DDO_AUTORIZACAOCONSUMO_QUANTIDADE',prop:'SortedStatus'},{av:'Ddo_autorizacaoconsumo_valor_Sortedstatus',ctrl:'DDO_AUTORIZACAOCONSUMO_VALOR',prop:'SortedStatus'},{av:'Ddo_autorizacaoconsumo_status_Sortedstatus',ctrl:'DDO_AUTORIZACAOCONSUMO_STATUS',prop:'SortedStatus'},{av:'Ddo_autorizacaoconsumo_ativo_Sortedstatus',ctrl:'DDO_AUTORIZACAOCONSUMO_ATIVO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIO.ONOPTIONCLICKED","{handler:'E16NB2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV23TFAutorizacaoConsumo_Codigo',fld:'vTFAUTORIZACAOCONSUMO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV24TFAutorizacaoConsumo_Codigo_To',fld:'vTFAUTORIZACAOCONSUMO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV27TFAutorizacaoConsumo_VigenciaInicio',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAINICIO',pic:'',nv:''},{av:'AV28TFAutorizacaoConsumo_VigenciaInicio_To',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO',pic:'',nv:''},{av:'AV33TFAutorizacaoConsumo_VigenciaFim',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAFIM',pic:'',nv:''},{av:'AV34TFAutorizacaoConsumo_VigenciaFim_To',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAFIM_TO',pic:'',nv:''},{av:'AV39TFAutorizacaoConsumo_UnidadeMedicaoNom',fld:'vTFAUTORIZACAOCONSUMO_UNIDADEMEDICAONOM',pic:'@!',nv:''},{av:'AV40TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel',fld:'vTFAUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_SEL',pic:'@!',nv:''},{av:'AV43TFAutorizacaoConsumo_Quantidade',fld:'vTFAUTORIZACAOCONSUMO_QUANTIDADE',pic:'ZZ9',nv:0},{av:'AV44TFAutorizacaoConsumo_Quantidade_To',fld:'vTFAUTORIZACAOCONSUMO_QUANTIDADE_TO',pic:'ZZ9',nv:0},{av:'AV47TFAutorizacaoConsumo_Valor',fld:'vTFAUTORIZACAOCONSUMO_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV48TFAutorizacaoConsumo_Valor_To',fld:'vTFAUTORIZACAOCONSUMO_VALOR_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV55TFAutorizacaoConsumo_Ativo_Sel',fld:'vTFAUTORIZACAOCONSUMO_ATIVO_SEL',pic:'9',nv:0},{av:'AV7Contrato_Codigo',fld:'vCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV25ddo_AutorizacaoConsumo_CodigoTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV31ddo_AutorizacaoConsumo_VigenciaInicioTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_VIGENCIAINICIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_AutorizacaoConsumo_VigenciaFimTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_VIGENCIAFIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_AutorizacaoConsumo_UnidadeMedicaoNomTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_AutorizacaoConsumo_QuantidadeTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_QUANTIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_AutorizacaoConsumo_ValorTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_VALORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_AutorizacaoConsumo_StatusTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_STATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_AutorizacaoConsumo_AtivoTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV52TFAutorizacaoConsumo_Status_Sels',fld:'vTFAUTORIZACAOCONSUMO_STATUS_SELS',pic:'',nv:null},{av:'A1780AutorizacaoConsumo_Ativo',fld:'AUTORIZACAOCONSUMO_ATIVO',pic:'',hsh:true,nv:false},{av:'sPrefix',nv:''},{av:'Ddo_autorizacaoconsumo_vigenciainicio_Activeeventkey',ctrl:'DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIO',prop:'ActiveEventKey'},{av:'Ddo_autorizacaoconsumo_vigenciainicio_Filteredtext_get',ctrl:'DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIO',prop:'FilteredText_get'},{av:'Ddo_autorizacaoconsumo_vigenciainicio_Filteredtextto_get',ctrl:'DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIO',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_autorizacaoconsumo_vigenciainicio_Sortedstatus',ctrl:'DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIO',prop:'SortedStatus'},{av:'AV27TFAutorizacaoConsumo_VigenciaInicio',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAINICIO',pic:'',nv:''},{av:'AV28TFAutorizacaoConsumo_VigenciaInicio_To',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO',pic:'',nv:''},{av:'Ddo_autorizacaoconsumo_codigo_Sortedstatus',ctrl:'DDO_AUTORIZACAOCONSUMO_CODIGO',prop:'SortedStatus'},{av:'Ddo_autorizacaoconsumo_vigenciafim_Sortedstatus',ctrl:'DDO_AUTORIZACAOCONSUMO_VIGENCIAFIM',prop:'SortedStatus'},{av:'Ddo_autorizacaoconsumo_unidademedicaonom_Sortedstatus',ctrl:'DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM',prop:'SortedStatus'},{av:'Ddo_autorizacaoconsumo_quantidade_Sortedstatus',ctrl:'DDO_AUTORIZACAOCONSUMO_QUANTIDADE',prop:'SortedStatus'},{av:'Ddo_autorizacaoconsumo_valor_Sortedstatus',ctrl:'DDO_AUTORIZACAOCONSUMO_VALOR',prop:'SortedStatus'},{av:'Ddo_autorizacaoconsumo_status_Sortedstatus',ctrl:'DDO_AUTORIZACAOCONSUMO_STATUS',prop:'SortedStatus'},{av:'Ddo_autorizacaoconsumo_ativo_Sortedstatus',ctrl:'DDO_AUTORIZACAOCONSUMO_ATIVO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_AUTORIZACAOCONSUMO_VIGENCIAFIM.ONOPTIONCLICKED","{handler:'E17NB2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV23TFAutorizacaoConsumo_Codigo',fld:'vTFAUTORIZACAOCONSUMO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV24TFAutorizacaoConsumo_Codigo_To',fld:'vTFAUTORIZACAOCONSUMO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV27TFAutorizacaoConsumo_VigenciaInicio',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAINICIO',pic:'',nv:''},{av:'AV28TFAutorizacaoConsumo_VigenciaInicio_To',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO',pic:'',nv:''},{av:'AV33TFAutorizacaoConsumo_VigenciaFim',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAFIM',pic:'',nv:''},{av:'AV34TFAutorizacaoConsumo_VigenciaFim_To',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAFIM_TO',pic:'',nv:''},{av:'AV39TFAutorizacaoConsumo_UnidadeMedicaoNom',fld:'vTFAUTORIZACAOCONSUMO_UNIDADEMEDICAONOM',pic:'@!',nv:''},{av:'AV40TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel',fld:'vTFAUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_SEL',pic:'@!',nv:''},{av:'AV43TFAutorizacaoConsumo_Quantidade',fld:'vTFAUTORIZACAOCONSUMO_QUANTIDADE',pic:'ZZ9',nv:0},{av:'AV44TFAutorizacaoConsumo_Quantidade_To',fld:'vTFAUTORIZACAOCONSUMO_QUANTIDADE_TO',pic:'ZZ9',nv:0},{av:'AV47TFAutorizacaoConsumo_Valor',fld:'vTFAUTORIZACAOCONSUMO_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV48TFAutorizacaoConsumo_Valor_To',fld:'vTFAUTORIZACAOCONSUMO_VALOR_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV55TFAutorizacaoConsumo_Ativo_Sel',fld:'vTFAUTORIZACAOCONSUMO_ATIVO_SEL',pic:'9',nv:0},{av:'AV7Contrato_Codigo',fld:'vCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV25ddo_AutorizacaoConsumo_CodigoTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV31ddo_AutorizacaoConsumo_VigenciaInicioTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_VIGENCIAINICIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_AutorizacaoConsumo_VigenciaFimTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_VIGENCIAFIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_AutorizacaoConsumo_UnidadeMedicaoNomTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_AutorizacaoConsumo_QuantidadeTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_QUANTIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_AutorizacaoConsumo_ValorTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_VALORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_AutorizacaoConsumo_StatusTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_STATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_AutorizacaoConsumo_AtivoTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV52TFAutorizacaoConsumo_Status_Sels',fld:'vTFAUTORIZACAOCONSUMO_STATUS_SELS',pic:'',nv:null},{av:'A1780AutorizacaoConsumo_Ativo',fld:'AUTORIZACAOCONSUMO_ATIVO',pic:'',hsh:true,nv:false},{av:'sPrefix',nv:''},{av:'Ddo_autorizacaoconsumo_vigenciafim_Activeeventkey',ctrl:'DDO_AUTORIZACAOCONSUMO_VIGENCIAFIM',prop:'ActiveEventKey'},{av:'Ddo_autorizacaoconsumo_vigenciafim_Filteredtext_get',ctrl:'DDO_AUTORIZACAOCONSUMO_VIGENCIAFIM',prop:'FilteredText_get'},{av:'Ddo_autorizacaoconsumo_vigenciafim_Filteredtextto_get',ctrl:'DDO_AUTORIZACAOCONSUMO_VIGENCIAFIM',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_autorizacaoconsumo_vigenciafim_Sortedstatus',ctrl:'DDO_AUTORIZACAOCONSUMO_VIGENCIAFIM',prop:'SortedStatus'},{av:'AV33TFAutorizacaoConsumo_VigenciaFim',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAFIM',pic:'',nv:''},{av:'AV34TFAutorizacaoConsumo_VigenciaFim_To',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAFIM_TO',pic:'',nv:''},{av:'Ddo_autorizacaoconsumo_codigo_Sortedstatus',ctrl:'DDO_AUTORIZACAOCONSUMO_CODIGO',prop:'SortedStatus'},{av:'Ddo_autorizacaoconsumo_vigenciainicio_Sortedstatus',ctrl:'DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIO',prop:'SortedStatus'},{av:'Ddo_autorizacaoconsumo_unidademedicaonom_Sortedstatus',ctrl:'DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM',prop:'SortedStatus'},{av:'Ddo_autorizacaoconsumo_quantidade_Sortedstatus',ctrl:'DDO_AUTORIZACAOCONSUMO_QUANTIDADE',prop:'SortedStatus'},{av:'Ddo_autorizacaoconsumo_valor_Sortedstatus',ctrl:'DDO_AUTORIZACAOCONSUMO_VALOR',prop:'SortedStatus'},{av:'Ddo_autorizacaoconsumo_status_Sortedstatus',ctrl:'DDO_AUTORIZACAOCONSUMO_STATUS',prop:'SortedStatus'},{av:'Ddo_autorizacaoconsumo_ativo_Sortedstatus',ctrl:'DDO_AUTORIZACAOCONSUMO_ATIVO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM.ONOPTIONCLICKED","{handler:'E18NB2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV23TFAutorizacaoConsumo_Codigo',fld:'vTFAUTORIZACAOCONSUMO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV24TFAutorizacaoConsumo_Codigo_To',fld:'vTFAUTORIZACAOCONSUMO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV27TFAutorizacaoConsumo_VigenciaInicio',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAINICIO',pic:'',nv:''},{av:'AV28TFAutorizacaoConsumo_VigenciaInicio_To',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO',pic:'',nv:''},{av:'AV33TFAutorizacaoConsumo_VigenciaFim',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAFIM',pic:'',nv:''},{av:'AV34TFAutorizacaoConsumo_VigenciaFim_To',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAFIM_TO',pic:'',nv:''},{av:'AV39TFAutorizacaoConsumo_UnidadeMedicaoNom',fld:'vTFAUTORIZACAOCONSUMO_UNIDADEMEDICAONOM',pic:'@!',nv:''},{av:'AV40TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel',fld:'vTFAUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_SEL',pic:'@!',nv:''},{av:'AV43TFAutorizacaoConsumo_Quantidade',fld:'vTFAUTORIZACAOCONSUMO_QUANTIDADE',pic:'ZZ9',nv:0},{av:'AV44TFAutorizacaoConsumo_Quantidade_To',fld:'vTFAUTORIZACAOCONSUMO_QUANTIDADE_TO',pic:'ZZ9',nv:0},{av:'AV47TFAutorizacaoConsumo_Valor',fld:'vTFAUTORIZACAOCONSUMO_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV48TFAutorizacaoConsumo_Valor_To',fld:'vTFAUTORIZACAOCONSUMO_VALOR_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV55TFAutorizacaoConsumo_Ativo_Sel',fld:'vTFAUTORIZACAOCONSUMO_ATIVO_SEL',pic:'9',nv:0},{av:'AV7Contrato_Codigo',fld:'vCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV25ddo_AutorizacaoConsumo_CodigoTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV31ddo_AutorizacaoConsumo_VigenciaInicioTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_VIGENCIAINICIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_AutorizacaoConsumo_VigenciaFimTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_VIGENCIAFIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_AutorizacaoConsumo_UnidadeMedicaoNomTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_AutorizacaoConsumo_QuantidadeTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_QUANTIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_AutorizacaoConsumo_ValorTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_VALORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_AutorizacaoConsumo_StatusTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_STATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_AutorizacaoConsumo_AtivoTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV52TFAutorizacaoConsumo_Status_Sels',fld:'vTFAUTORIZACAOCONSUMO_STATUS_SELS',pic:'',nv:null},{av:'A1780AutorizacaoConsumo_Ativo',fld:'AUTORIZACAOCONSUMO_ATIVO',pic:'',hsh:true,nv:false},{av:'sPrefix',nv:''},{av:'Ddo_autorizacaoconsumo_unidademedicaonom_Activeeventkey',ctrl:'DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM',prop:'ActiveEventKey'},{av:'Ddo_autorizacaoconsumo_unidademedicaonom_Filteredtext_get',ctrl:'DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM',prop:'FilteredText_get'},{av:'Ddo_autorizacaoconsumo_unidademedicaonom_Selectedvalue_get',ctrl:'DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_autorizacaoconsumo_unidademedicaonom_Sortedstatus',ctrl:'DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM',prop:'SortedStatus'},{av:'AV39TFAutorizacaoConsumo_UnidadeMedicaoNom',fld:'vTFAUTORIZACAOCONSUMO_UNIDADEMEDICAONOM',pic:'@!',nv:''},{av:'AV40TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel',fld:'vTFAUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_SEL',pic:'@!',nv:''},{av:'Ddo_autorizacaoconsumo_codigo_Sortedstatus',ctrl:'DDO_AUTORIZACAOCONSUMO_CODIGO',prop:'SortedStatus'},{av:'Ddo_autorizacaoconsumo_vigenciainicio_Sortedstatus',ctrl:'DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIO',prop:'SortedStatus'},{av:'Ddo_autorizacaoconsumo_vigenciafim_Sortedstatus',ctrl:'DDO_AUTORIZACAOCONSUMO_VIGENCIAFIM',prop:'SortedStatus'},{av:'Ddo_autorizacaoconsumo_quantidade_Sortedstatus',ctrl:'DDO_AUTORIZACAOCONSUMO_QUANTIDADE',prop:'SortedStatus'},{av:'Ddo_autorizacaoconsumo_valor_Sortedstatus',ctrl:'DDO_AUTORIZACAOCONSUMO_VALOR',prop:'SortedStatus'},{av:'Ddo_autorizacaoconsumo_status_Sortedstatus',ctrl:'DDO_AUTORIZACAOCONSUMO_STATUS',prop:'SortedStatus'},{av:'Ddo_autorizacaoconsumo_ativo_Sortedstatus',ctrl:'DDO_AUTORIZACAOCONSUMO_ATIVO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_AUTORIZACAOCONSUMO_QUANTIDADE.ONOPTIONCLICKED","{handler:'E19NB2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV23TFAutorizacaoConsumo_Codigo',fld:'vTFAUTORIZACAOCONSUMO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV24TFAutorizacaoConsumo_Codigo_To',fld:'vTFAUTORIZACAOCONSUMO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV27TFAutorizacaoConsumo_VigenciaInicio',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAINICIO',pic:'',nv:''},{av:'AV28TFAutorizacaoConsumo_VigenciaInicio_To',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO',pic:'',nv:''},{av:'AV33TFAutorizacaoConsumo_VigenciaFim',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAFIM',pic:'',nv:''},{av:'AV34TFAutorizacaoConsumo_VigenciaFim_To',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAFIM_TO',pic:'',nv:''},{av:'AV39TFAutorizacaoConsumo_UnidadeMedicaoNom',fld:'vTFAUTORIZACAOCONSUMO_UNIDADEMEDICAONOM',pic:'@!',nv:''},{av:'AV40TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel',fld:'vTFAUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_SEL',pic:'@!',nv:''},{av:'AV43TFAutorizacaoConsumo_Quantidade',fld:'vTFAUTORIZACAOCONSUMO_QUANTIDADE',pic:'ZZ9',nv:0},{av:'AV44TFAutorizacaoConsumo_Quantidade_To',fld:'vTFAUTORIZACAOCONSUMO_QUANTIDADE_TO',pic:'ZZ9',nv:0},{av:'AV47TFAutorizacaoConsumo_Valor',fld:'vTFAUTORIZACAOCONSUMO_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV48TFAutorizacaoConsumo_Valor_To',fld:'vTFAUTORIZACAOCONSUMO_VALOR_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV55TFAutorizacaoConsumo_Ativo_Sel',fld:'vTFAUTORIZACAOCONSUMO_ATIVO_SEL',pic:'9',nv:0},{av:'AV7Contrato_Codigo',fld:'vCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV25ddo_AutorizacaoConsumo_CodigoTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV31ddo_AutorizacaoConsumo_VigenciaInicioTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_VIGENCIAINICIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_AutorizacaoConsumo_VigenciaFimTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_VIGENCIAFIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_AutorizacaoConsumo_UnidadeMedicaoNomTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_AutorizacaoConsumo_QuantidadeTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_QUANTIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_AutorizacaoConsumo_ValorTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_VALORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_AutorizacaoConsumo_StatusTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_STATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_AutorizacaoConsumo_AtivoTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV52TFAutorizacaoConsumo_Status_Sels',fld:'vTFAUTORIZACAOCONSUMO_STATUS_SELS',pic:'',nv:null},{av:'A1780AutorizacaoConsumo_Ativo',fld:'AUTORIZACAOCONSUMO_ATIVO',pic:'',hsh:true,nv:false},{av:'sPrefix',nv:''},{av:'Ddo_autorizacaoconsumo_quantidade_Activeeventkey',ctrl:'DDO_AUTORIZACAOCONSUMO_QUANTIDADE',prop:'ActiveEventKey'},{av:'Ddo_autorizacaoconsumo_quantidade_Filteredtext_get',ctrl:'DDO_AUTORIZACAOCONSUMO_QUANTIDADE',prop:'FilteredText_get'},{av:'Ddo_autorizacaoconsumo_quantidade_Filteredtextto_get',ctrl:'DDO_AUTORIZACAOCONSUMO_QUANTIDADE',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_autorizacaoconsumo_quantidade_Sortedstatus',ctrl:'DDO_AUTORIZACAOCONSUMO_QUANTIDADE',prop:'SortedStatus'},{av:'AV43TFAutorizacaoConsumo_Quantidade',fld:'vTFAUTORIZACAOCONSUMO_QUANTIDADE',pic:'ZZ9',nv:0},{av:'AV44TFAutorizacaoConsumo_Quantidade_To',fld:'vTFAUTORIZACAOCONSUMO_QUANTIDADE_TO',pic:'ZZ9',nv:0},{av:'Ddo_autorizacaoconsumo_codigo_Sortedstatus',ctrl:'DDO_AUTORIZACAOCONSUMO_CODIGO',prop:'SortedStatus'},{av:'Ddo_autorizacaoconsumo_vigenciainicio_Sortedstatus',ctrl:'DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIO',prop:'SortedStatus'},{av:'Ddo_autorizacaoconsumo_vigenciafim_Sortedstatus',ctrl:'DDO_AUTORIZACAOCONSUMO_VIGENCIAFIM',prop:'SortedStatus'},{av:'Ddo_autorizacaoconsumo_unidademedicaonom_Sortedstatus',ctrl:'DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM',prop:'SortedStatus'},{av:'Ddo_autorizacaoconsumo_valor_Sortedstatus',ctrl:'DDO_AUTORIZACAOCONSUMO_VALOR',prop:'SortedStatus'},{av:'Ddo_autorizacaoconsumo_status_Sortedstatus',ctrl:'DDO_AUTORIZACAOCONSUMO_STATUS',prop:'SortedStatus'},{av:'Ddo_autorizacaoconsumo_ativo_Sortedstatus',ctrl:'DDO_AUTORIZACAOCONSUMO_ATIVO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_AUTORIZACAOCONSUMO_VALOR.ONOPTIONCLICKED","{handler:'E20NB2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV23TFAutorizacaoConsumo_Codigo',fld:'vTFAUTORIZACAOCONSUMO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV24TFAutorizacaoConsumo_Codigo_To',fld:'vTFAUTORIZACAOCONSUMO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV27TFAutorizacaoConsumo_VigenciaInicio',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAINICIO',pic:'',nv:''},{av:'AV28TFAutorizacaoConsumo_VigenciaInicio_To',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO',pic:'',nv:''},{av:'AV33TFAutorizacaoConsumo_VigenciaFim',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAFIM',pic:'',nv:''},{av:'AV34TFAutorizacaoConsumo_VigenciaFim_To',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAFIM_TO',pic:'',nv:''},{av:'AV39TFAutorizacaoConsumo_UnidadeMedicaoNom',fld:'vTFAUTORIZACAOCONSUMO_UNIDADEMEDICAONOM',pic:'@!',nv:''},{av:'AV40TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel',fld:'vTFAUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_SEL',pic:'@!',nv:''},{av:'AV43TFAutorizacaoConsumo_Quantidade',fld:'vTFAUTORIZACAOCONSUMO_QUANTIDADE',pic:'ZZ9',nv:0},{av:'AV44TFAutorizacaoConsumo_Quantidade_To',fld:'vTFAUTORIZACAOCONSUMO_QUANTIDADE_TO',pic:'ZZ9',nv:0},{av:'AV47TFAutorizacaoConsumo_Valor',fld:'vTFAUTORIZACAOCONSUMO_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV48TFAutorizacaoConsumo_Valor_To',fld:'vTFAUTORIZACAOCONSUMO_VALOR_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV55TFAutorizacaoConsumo_Ativo_Sel',fld:'vTFAUTORIZACAOCONSUMO_ATIVO_SEL',pic:'9',nv:0},{av:'AV7Contrato_Codigo',fld:'vCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV25ddo_AutorizacaoConsumo_CodigoTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV31ddo_AutorizacaoConsumo_VigenciaInicioTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_VIGENCIAINICIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_AutorizacaoConsumo_VigenciaFimTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_VIGENCIAFIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_AutorizacaoConsumo_UnidadeMedicaoNomTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_AutorizacaoConsumo_QuantidadeTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_QUANTIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_AutorizacaoConsumo_ValorTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_VALORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_AutorizacaoConsumo_StatusTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_STATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_AutorizacaoConsumo_AtivoTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV52TFAutorizacaoConsumo_Status_Sels',fld:'vTFAUTORIZACAOCONSUMO_STATUS_SELS',pic:'',nv:null},{av:'A1780AutorizacaoConsumo_Ativo',fld:'AUTORIZACAOCONSUMO_ATIVO',pic:'',hsh:true,nv:false},{av:'sPrefix',nv:''},{av:'Ddo_autorizacaoconsumo_valor_Activeeventkey',ctrl:'DDO_AUTORIZACAOCONSUMO_VALOR',prop:'ActiveEventKey'},{av:'Ddo_autorizacaoconsumo_valor_Filteredtext_get',ctrl:'DDO_AUTORIZACAOCONSUMO_VALOR',prop:'FilteredText_get'},{av:'Ddo_autorizacaoconsumo_valor_Filteredtextto_get',ctrl:'DDO_AUTORIZACAOCONSUMO_VALOR',prop:'FilteredTextTo_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_autorizacaoconsumo_valor_Sortedstatus',ctrl:'DDO_AUTORIZACAOCONSUMO_VALOR',prop:'SortedStatus'},{av:'AV47TFAutorizacaoConsumo_Valor',fld:'vTFAUTORIZACAOCONSUMO_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV48TFAutorizacaoConsumo_Valor_To',fld:'vTFAUTORIZACAOCONSUMO_VALOR_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'Ddo_autorizacaoconsumo_codigo_Sortedstatus',ctrl:'DDO_AUTORIZACAOCONSUMO_CODIGO',prop:'SortedStatus'},{av:'Ddo_autorizacaoconsumo_vigenciainicio_Sortedstatus',ctrl:'DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIO',prop:'SortedStatus'},{av:'Ddo_autorizacaoconsumo_vigenciafim_Sortedstatus',ctrl:'DDO_AUTORIZACAOCONSUMO_VIGENCIAFIM',prop:'SortedStatus'},{av:'Ddo_autorizacaoconsumo_unidademedicaonom_Sortedstatus',ctrl:'DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM',prop:'SortedStatus'},{av:'Ddo_autorizacaoconsumo_quantidade_Sortedstatus',ctrl:'DDO_AUTORIZACAOCONSUMO_QUANTIDADE',prop:'SortedStatus'},{av:'Ddo_autorizacaoconsumo_status_Sortedstatus',ctrl:'DDO_AUTORIZACAOCONSUMO_STATUS',prop:'SortedStatus'},{av:'Ddo_autorizacaoconsumo_ativo_Sortedstatus',ctrl:'DDO_AUTORIZACAOCONSUMO_ATIVO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_AUTORIZACAOCONSUMO_STATUS.ONOPTIONCLICKED","{handler:'E21NB2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV23TFAutorizacaoConsumo_Codigo',fld:'vTFAUTORIZACAOCONSUMO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV24TFAutorizacaoConsumo_Codigo_To',fld:'vTFAUTORIZACAOCONSUMO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV27TFAutorizacaoConsumo_VigenciaInicio',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAINICIO',pic:'',nv:''},{av:'AV28TFAutorizacaoConsumo_VigenciaInicio_To',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO',pic:'',nv:''},{av:'AV33TFAutorizacaoConsumo_VigenciaFim',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAFIM',pic:'',nv:''},{av:'AV34TFAutorizacaoConsumo_VigenciaFim_To',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAFIM_TO',pic:'',nv:''},{av:'AV39TFAutorizacaoConsumo_UnidadeMedicaoNom',fld:'vTFAUTORIZACAOCONSUMO_UNIDADEMEDICAONOM',pic:'@!',nv:''},{av:'AV40TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel',fld:'vTFAUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_SEL',pic:'@!',nv:''},{av:'AV43TFAutorizacaoConsumo_Quantidade',fld:'vTFAUTORIZACAOCONSUMO_QUANTIDADE',pic:'ZZ9',nv:0},{av:'AV44TFAutorizacaoConsumo_Quantidade_To',fld:'vTFAUTORIZACAOCONSUMO_QUANTIDADE_TO',pic:'ZZ9',nv:0},{av:'AV47TFAutorizacaoConsumo_Valor',fld:'vTFAUTORIZACAOCONSUMO_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV48TFAutorizacaoConsumo_Valor_To',fld:'vTFAUTORIZACAOCONSUMO_VALOR_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV55TFAutorizacaoConsumo_Ativo_Sel',fld:'vTFAUTORIZACAOCONSUMO_ATIVO_SEL',pic:'9',nv:0},{av:'AV7Contrato_Codigo',fld:'vCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV25ddo_AutorizacaoConsumo_CodigoTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV31ddo_AutorizacaoConsumo_VigenciaInicioTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_VIGENCIAINICIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_AutorizacaoConsumo_VigenciaFimTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_VIGENCIAFIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_AutorizacaoConsumo_UnidadeMedicaoNomTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_AutorizacaoConsumo_QuantidadeTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_QUANTIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_AutorizacaoConsumo_ValorTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_VALORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_AutorizacaoConsumo_StatusTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_STATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_AutorizacaoConsumo_AtivoTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV52TFAutorizacaoConsumo_Status_Sels',fld:'vTFAUTORIZACAOCONSUMO_STATUS_SELS',pic:'',nv:null},{av:'A1780AutorizacaoConsumo_Ativo',fld:'AUTORIZACAOCONSUMO_ATIVO',pic:'',hsh:true,nv:false},{av:'sPrefix',nv:''},{av:'Ddo_autorizacaoconsumo_status_Activeeventkey',ctrl:'DDO_AUTORIZACAOCONSUMO_STATUS',prop:'ActiveEventKey'},{av:'Ddo_autorizacaoconsumo_status_Selectedvalue_get',ctrl:'DDO_AUTORIZACAOCONSUMO_STATUS',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_autorizacaoconsumo_status_Sortedstatus',ctrl:'DDO_AUTORIZACAOCONSUMO_STATUS',prop:'SortedStatus'},{av:'AV52TFAutorizacaoConsumo_Status_Sels',fld:'vTFAUTORIZACAOCONSUMO_STATUS_SELS',pic:'',nv:null},{av:'Ddo_autorizacaoconsumo_codigo_Sortedstatus',ctrl:'DDO_AUTORIZACAOCONSUMO_CODIGO',prop:'SortedStatus'},{av:'Ddo_autorizacaoconsumo_vigenciainicio_Sortedstatus',ctrl:'DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIO',prop:'SortedStatus'},{av:'Ddo_autorizacaoconsumo_vigenciafim_Sortedstatus',ctrl:'DDO_AUTORIZACAOCONSUMO_VIGENCIAFIM',prop:'SortedStatus'},{av:'Ddo_autorizacaoconsumo_unidademedicaonom_Sortedstatus',ctrl:'DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM',prop:'SortedStatus'},{av:'Ddo_autorizacaoconsumo_quantidade_Sortedstatus',ctrl:'DDO_AUTORIZACAOCONSUMO_QUANTIDADE',prop:'SortedStatus'},{av:'Ddo_autorizacaoconsumo_valor_Sortedstatus',ctrl:'DDO_AUTORIZACAOCONSUMO_VALOR',prop:'SortedStatus'},{av:'Ddo_autorizacaoconsumo_ativo_Sortedstatus',ctrl:'DDO_AUTORIZACAOCONSUMO_ATIVO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_AUTORIZACAOCONSUMO_ATIVO.ONOPTIONCLICKED","{handler:'E22NB2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV23TFAutorizacaoConsumo_Codigo',fld:'vTFAUTORIZACAOCONSUMO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV24TFAutorizacaoConsumo_Codigo_To',fld:'vTFAUTORIZACAOCONSUMO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV27TFAutorizacaoConsumo_VigenciaInicio',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAINICIO',pic:'',nv:''},{av:'AV28TFAutorizacaoConsumo_VigenciaInicio_To',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO',pic:'',nv:''},{av:'AV33TFAutorizacaoConsumo_VigenciaFim',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAFIM',pic:'',nv:''},{av:'AV34TFAutorizacaoConsumo_VigenciaFim_To',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAFIM_TO',pic:'',nv:''},{av:'AV39TFAutorizacaoConsumo_UnidadeMedicaoNom',fld:'vTFAUTORIZACAOCONSUMO_UNIDADEMEDICAONOM',pic:'@!',nv:''},{av:'AV40TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel',fld:'vTFAUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_SEL',pic:'@!',nv:''},{av:'AV43TFAutorizacaoConsumo_Quantidade',fld:'vTFAUTORIZACAOCONSUMO_QUANTIDADE',pic:'ZZ9',nv:0},{av:'AV44TFAutorizacaoConsumo_Quantidade_To',fld:'vTFAUTORIZACAOCONSUMO_QUANTIDADE_TO',pic:'ZZ9',nv:0},{av:'AV47TFAutorizacaoConsumo_Valor',fld:'vTFAUTORIZACAOCONSUMO_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV48TFAutorizacaoConsumo_Valor_To',fld:'vTFAUTORIZACAOCONSUMO_VALOR_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV55TFAutorizacaoConsumo_Ativo_Sel',fld:'vTFAUTORIZACAOCONSUMO_ATIVO_SEL',pic:'9',nv:0},{av:'AV7Contrato_Codigo',fld:'vCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV25ddo_AutorizacaoConsumo_CodigoTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV31ddo_AutorizacaoConsumo_VigenciaInicioTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_VIGENCIAINICIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_AutorizacaoConsumo_VigenciaFimTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_VIGENCIAFIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_AutorizacaoConsumo_UnidadeMedicaoNomTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_AutorizacaoConsumo_QuantidadeTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_QUANTIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_AutorizacaoConsumo_ValorTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_VALORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_AutorizacaoConsumo_StatusTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_STATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_AutorizacaoConsumo_AtivoTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV52TFAutorizacaoConsumo_Status_Sels',fld:'vTFAUTORIZACAOCONSUMO_STATUS_SELS',pic:'',nv:null},{av:'A1780AutorizacaoConsumo_Ativo',fld:'AUTORIZACAOCONSUMO_ATIVO',pic:'',hsh:true,nv:false},{av:'sPrefix',nv:''},{av:'Ddo_autorizacaoconsumo_ativo_Activeeventkey',ctrl:'DDO_AUTORIZACAOCONSUMO_ATIVO',prop:'ActiveEventKey'},{av:'Ddo_autorizacaoconsumo_ativo_Selectedvalue_get',ctrl:'DDO_AUTORIZACAOCONSUMO_ATIVO',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_autorizacaoconsumo_ativo_Sortedstatus',ctrl:'DDO_AUTORIZACAOCONSUMO_ATIVO',prop:'SortedStatus'},{av:'AV55TFAutorizacaoConsumo_Ativo_Sel',fld:'vTFAUTORIZACAOCONSUMO_ATIVO_SEL',pic:'9',nv:0},{av:'Ddo_autorizacaoconsumo_codigo_Sortedstatus',ctrl:'DDO_AUTORIZACAOCONSUMO_CODIGO',prop:'SortedStatus'},{av:'Ddo_autorizacaoconsumo_vigenciainicio_Sortedstatus',ctrl:'DDO_AUTORIZACAOCONSUMO_VIGENCIAINICIO',prop:'SortedStatus'},{av:'Ddo_autorizacaoconsumo_vigenciafim_Sortedstatus',ctrl:'DDO_AUTORIZACAOCONSUMO_VIGENCIAFIM',prop:'SortedStatus'},{av:'Ddo_autorizacaoconsumo_unidademedicaonom_Sortedstatus',ctrl:'DDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOM',prop:'SortedStatus'},{av:'Ddo_autorizacaoconsumo_quantidade_Sortedstatus',ctrl:'DDO_AUTORIZACAOCONSUMO_QUANTIDADE',prop:'SortedStatus'},{av:'Ddo_autorizacaoconsumo_valor_Sortedstatus',ctrl:'DDO_AUTORIZACAOCONSUMO_VALOR',prop:'SortedStatus'},{av:'Ddo_autorizacaoconsumo_status_Sortedstatus',ctrl:'DDO_AUTORIZACAOCONSUMO_STATUS',prop:'SortedStatus'}]}");
         setEventMetadata("GRID.LOAD","{handler:'E26NB2',iparms:[{av:'A1780AutorizacaoConsumo_Ativo',fld:'AUTORIZACAOCONSUMO_ATIVO',pic:'',hsh:true,nv:false}],oparms:[{av:'edtavCancelarautorizcaoconsumo_Tooltiptext',ctrl:'vCANCELARAUTORIZCAOCONSUMO',prop:'Tooltiptext'},{av:'AV17CancelarAutorizcaoConsumo',fld:'vCANCELARAUTORIZCAOCONSUMO',pic:'',nv:''},{av:'edtavCancelarautorizcaoconsumo_Enabled',ctrl:'vCANCELARAUTORIZCAOCONSUMO',prop:'Enabled'},{av:'AV20Autorizar',fld:'vAUTORIZAR',pic:'',nv:''},{av:'edtavAutorizar_Tooltiptext',ctrl:'vAUTORIZAR',prop:'Tooltiptext'},{av:'AV21Encerrar',fld:'vENCERRAR',pic:'',nv:''},{av:'edtavEncerrar_Tooltiptext',ctrl:'vENCERRAR',prop:'Tooltiptext'}]}");
         setEventMetadata("'DOINSERT'","{handler:'E23NB2',iparms:[{av:'A1774AutorizacaoConsumo_Codigo',fld:'AUTORIZACAOCONSUMO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         setEventMetadata("'DOCANCELARAUTORIZCAOCONSUMO'","{handler:'E27NB2',iparms:[{av:'A74Contrato_Codigo',fld:'CONTRATO_CODIGO',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV19Contrato_Codigo_Selected',fld:'vCONTRATO_CODIGO_SELECTED',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("DVELOP_CONFIRMPANEL_CANCELARAUTORIZCAOCONSUMO.CLOSE","{handler:'E12NB2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV23TFAutorizacaoConsumo_Codigo',fld:'vTFAUTORIZACAOCONSUMO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV24TFAutorizacaoConsumo_Codigo_To',fld:'vTFAUTORIZACAOCONSUMO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV27TFAutorizacaoConsumo_VigenciaInicio',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAINICIO',pic:'',nv:''},{av:'AV28TFAutorizacaoConsumo_VigenciaInicio_To',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO',pic:'',nv:''},{av:'AV33TFAutorizacaoConsumo_VigenciaFim',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAFIM',pic:'',nv:''},{av:'AV34TFAutorizacaoConsumo_VigenciaFim_To',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAFIM_TO',pic:'',nv:''},{av:'AV39TFAutorizacaoConsumo_UnidadeMedicaoNom',fld:'vTFAUTORIZACAOCONSUMO_UNIDADEMEDICAONOM',pic:'@!',nv:''},{av:'AV40TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel',fld:'vTFAUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_SEL',pic:'@!',nv:''},{av:'AV43TFAutorizacaoConsumo_Quantidade',fld:'vTFAUTORIZACAOCONSUMO_QUANTIDADE',pic:'ZZ9',nv:0},{av:'AV44TFAutorizacaoConsumo_Quantidade_To',fld:'vTFAUTORIZACAOCONSUMO_QUANTIDADE_TO',pic:'ZZ9',nv:0},{av:'AV47TFAutorizacaoConsumo_Valor',fld:'vTFAUTORIZACAOCONSUMO_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV48TFAutorizacaoConsumo_Valor_To',fld:'vTFAUTORIZACAOCONSUMO_VALOR_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV55TFAutorizacaoConsumo_Ativo_Sel',fld:'vTFAUTORIZACAOCONSUMO_ATIVO_SEL',pic:'9',nv:0},{av:'AV7Contrato_Codigo',fld:'vCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV25ddo_AutorizacaoConsumo_CodigoTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV31ddo_AutorizacaoConsumo_VigenciaInicioTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_VIGENCIAINICIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_AutorizacaoConsumo_VigenciaFimTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_VIGENCIAFIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_AutorizacaoConsumo_UnidadeMedicaoNomTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_AutorizacaoConsumo_QuantidadeTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_QUANTIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_AutorizacaoConsumo_ValorTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_VALORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_AutorizacaoConsumo_StatusTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_STATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_AutorizacaoConsumo_AtivoTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV52TFAutorizacaoConsumo_Status_Sels',fld:'vTFAUTORIZACAOCONSUMO_STATUS_SELS',pic:'',nv:null},{av:'A1780AutorizacaoConsumo_Ativo',fld:'AUTORIZACAOCONSUMO_ATIVO',pic:'',hsh:true,nv:false},{av:'sPrefix',nv:''},{av:'Dvelop_confirmpanel_cancelarautorizcaoconsumo_Result',ctrl:'DVELOP_CONFIRMPANEL_CANCELARAUTORIZCAOCONSUMO',prop:'Result'},{av:'AV19Contrato_Codigo_Selected',fld:'vCONTRATO_CODIGO_SELECTED',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         setEventMetadata("'DOAUTORIZAR'","{handler:'E28NB2',iparms:[{av:'A1774AutorizacaoConsumo_Codigo',fld:'AUTORIZACAOCONSUMO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV18AutorizacaoConsumo_Codigo_Selected',fld:'vAUTORIZACAOCONSUMO_CODIGO_SELECTED',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("DVELOP_CONFIRMPANEL_AUTORIZAR.CLOSE","{handler:'E13NB2',iparms:[{av:'Dvelop_confirmpanel_autorizar_Result',ctrl:'DVELOP_CONFIRMPANEL_AUTORIZAR',prop:'Result'},{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV23TFAutorizacaoConsumo_Codigo',fld:'vTFAUTORIZACAOCONSUMO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV24TFAutorizacaoConsumo_Codigo_To',fld:'vTFAUTORIZACAOCONSUMO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV27TFAutorizacaoConsumo_VigenciaInicio',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAINICIO',pic:'',nv:''},{av:'AV28TFAutorizacaoConsumo_VigenciaInicio_To',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO',pic:'',nv:''},{av:'AV33TFAutorizacaoConsumo_VigenciaFim',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAFIM',pic:'',nv:''},{av:'AV34TFAutorizacaoConsumo_VigenciaFim_To',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAFIM_TO',pic:'',nv:''},{av:'AV39TFAutorizacaoConsumo_UnidadeMedicaoNom',fld:'vTFAUTORIZACAOCONSUMO_UNIDADEMEDICAONOM',pic:'@!',nv:''},{av:'AV40TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel',fld:'vTFAUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_SEL',pic:'@!',nv:''},{av:'AV43TFAutorizacaoConsumo_Quantidade',fld:'vTFAUTORIZACAOCONSUMO_QUANTIDADE',pic:'ZZ9',nv:0},{av:'AV44TFAutorizacaoConsumo_Quantidade_To',fld:'vTFAUTORIZACAOCONSUMO_QUANTIDADE_TO',pic:'ZZ9',nv:0},{av:'AV47TFAutorizacaoConsumo_Valor',fld:'vTFAUTORIZACAOCONSUMO_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV48TFAutorizacaoConsumo_Valor_To',fld:'vTFAUTORIZACAOCONSUMO_VALOR_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV55TFAutorizacaoConsumo_Ativo_Sel',fld:'vTFAUTORIZACAOCONSUMO_ATIVO_SEL',pic:'9',nv:0},{av:'AV7Contrato_Codigo',fld:'vCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV25ddo_AutorizacaoConsumo_CodigoTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV31ddo_AutorizacaoConsumo_VigenciaInicioTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_VIGENCIAINICIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_AutorizacaoConsumo_VigenciaFimTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_VIGENCIAFIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_AutorizacaoConsumo_UnidadeMedicaoNomTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_AutorizacaoConsumo_QuantidadeTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_QUANTIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_AutorizacaoConsumo_ValorTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_VALORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_AutorizacaoConsumo_StatusTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_STATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_AutorizacaoConsumo_AtivoTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV52TFAutorizacaoConsumo_Status_Sels',fld:'vTFAUTORIZACAOCONSUMO_STATUS_SELS',pic:'',nv:null},{av:'A1780AutorizacaoConsumo_Ativo',fld:'AUTORIZACAOCONSUMO_ATIVO',pic:'',hsh:true,nv:false},{av:'sPrefix',nv:''},{av:'AV18AutorizacaoConsumo_Codigo_Selected',fld:'vAUTORIZACAOCONSUMO_CODIGO_SELECTED',pic:'ZZZZZ9',nv:0}],oparms:[]}");
         setEventMetadata("'DOENCERRAR'","{handler:'E29NB2',iparms:[{av:'A1774AutorizacaoConsumo_Codigo',fld:'AUTORIZACAOCONSUMO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV18AutorizacaoConsumo_Codigo_Selected',fld:'vAUTORIZACAOCONSUMO_CODIGO_SELECTED',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("DVELOP_CONFIRMPANEL_ENCERRAR.CLOSE","{handler:'E14NB2',iparms:[{av:'Dvelop_confirmpanel_encerrar_Result',ctrl:'DVELOP_CONFIRMPANEL_ENCERRAR',prop:'Result'},{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV23TFAutorizacaoConsumo_Codigo',fld:'vTFAUTORIZACAOCONSUMO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV24TFAutorizacaoConsumo_Codigo_To',fld:'vTFAUTORIZACAOCONSUMO_CODIGO_TO',pic:'ZZZZZ9',nv:0},{av:'AV27TFAutorizacaoConsumo_VigenciaInicio',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAINICIO',pic:'',nv:''},{av:'AV28TFAutorizacaoConsumo_VigenciaInicio_To',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAINICIO_TO',pic:'',nv:''},{av:'AV33TFAutorizacaoConsumo_VigenciaFim',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAFIM',pic:'',nv:''},{av:'AV34TFAutorizacaoConsumo_VigenciaFim_To',fld:'vTFAUTORIZACAOCONSUMO_VIGENCIAFIM_TO',pic:'',nv:''},{av:'AV39TFAutorizacaoConsumo_UnidadeMedicaoNom',fld:'vTFAUTORIZACAOCONSUMO_UNIDADEMEDICAONOM',pic:'@!',nv:''},{av:'AV40TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel',fld:'vTFAUTORIZACAOCONSUMO_UNIDADEMEDICAONOM_SEL',pic:'@!',nv:''},{av:'AV43TFAutorizacaoConsumo_Quantidade',fld:'vTFAUTORIZACAOCONSUMO_QUANTIDADE',pic:'ZZ9',nv:0},{av:'AV44TFAutorizacaoConsumo_Quantidade_To',fld:'vTFAUTORIZACAOCONSUMO_QUANTIDADE_TO',pic:'ZZ9',nv:0},{av:'AV47TFAutorizacaoConsumo_Valor',fld:'vTFAUTORIZACAOCONSUMO_VALOR',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV48TFAutorizacaoConsumo_Valor_To',fld:'vTFAUTORIZACAOCONSUMO_VALOR_TO',pic:'ZZZ,ZZZ,ZZZ,ZZ9.99',nv:0.0},{av:'AV55TFAutorizacaoConsumo_Ativo_Sel',fld:'vTFAUTORIZACAOCONSUMO_ATIVO_SEL',pic:'9',nv:0},{av:'AV7Contrato_Codigo',fld:'vCONTRATO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV25ddo_AutorizacaoConsumo_CodigoTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_CODIGOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV31ddo_AutorizacaoConsumo_VigenciaInicioTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_VIGENCIAINICIOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV37ddo_AutorizacaoConsumo_VigenciaFimTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_VIGENCIAFIMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV41ddo_AutorizacaoConsumo_UnidadeMedicaoNomTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_UNIDADEMEDICAONOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV45ddo_AutorizacaoConsumo_QuantidadeTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_QUANTIDADETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV49ddo_AutorizacaoConsumo_ValorTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_VALORTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV53ddo_AutorizacaoConsumo_StatusTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_STATUSTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV56ddo_AutorizacaoConsumo_AtivoTitleControlIdToReplace',fld:'vDDO_AUTORIZACAOCONSUMO_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV66Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV52TFAutorizacaoConsumo_Status_Sels',fld:'vTFAUTORIZACAOCONSUMO_STATUS_SELS',pic:'',nv:null},{av:'A1780AutorizacaoConsumo_Ativo',fld:'AUTORIZACAOCONSUMO_ATIVO',pic:'',hsh:true,nv:false},{av:'sPrefix',nv:''},{av:'AV18AutorizacaoConsumo_Codigo_Selected',fld:'vAUTORIZACAOCONSUMO_CODIGO_SELECTED',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV18AutorizacaoConsumo_Codigo_Selected',fld:'vAUTORIZACAOCONSUMO_CODIGO_SELECTED',pic:'ZZZZZ9',nv:0}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Gridpaginationbar_Selectedpage = "";
         Ddo_autorizacaoconsumo_codigo_Activeeventkey = "";
         Ddo_autorizacaoconsumo_codigo_Filteredtext_get = "";
         Ddo_autorizacaoconsumo_codigo_Filteredtextto_get = "";
         Ddo_autorizacaoconsumo_vigenciainicio_Activeeventkey = "";
         Ddo_autorizacaoconsumo_vigenciainicio_Filteredtext_get = "";
         Ddo_autorizacaoconsumo_vigenciainicio_Filteredtextto_get = "";
         Ddo_autorizacaoconsumo_vigenciafim_Activeeventkey = "";
         Ddo_autorizacaoconsumo_vigenciafim_Filteredtext_get = "";
         Ddo_autorizacaoconsumo_vigenciafim_Filteredtextto_get = "";
         Ddo_autorizacaoconsumo_unidademedicaonom_Activeeventkey = "";
         Ddo_autorizacaoconsumo_unidademedicaonom_Filteredtext_get = "";
         Ddo_autorizacaoconsumo_unidademedicaonom_Selectedvalue_get = "";
         Ddo_autorizacaoconsumo_quantidade_Activeeventkey = "";
         Ddo_autorizacaoconsumo_quantidade_Filteredtext_get = "";
         Ddo_autorizacaoconsumo_quantidade_Filteredtextto_get = "";
         Ddo_autorizacaoconsumo_valor_Activeeventkey = "";
         Ddo_autorizacaoconsumo_valor_Filteredtext_get = "";
         Ddo_autorizacaoconsumo_valor_Filteredtextto_get = "";
         Ddo_autorizacaoconsumo_status_Activeeventkey = "";
         Ddo_autorizacaoconsumo_status_Selectedvalue_get = "";
         Ddo_autorizacaoconsumo_ativo_Activeeventkey = "";
         Ddo_autorizacaoconsumo_ativo_Selectedvalue_get = "";
         Dvelop_confirmpanel_cancelarautorizcaoconsumo_Result = "";
         Dvelop_confirmpanel_autorizar_Result = "";
         Dvelop_confirmpanel_encerrar_Result = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         AV27TFAutorizacaoConsumo_VigenciaInicio = DateTime.MinValue;
         AV28TFAutorizacaoConsumo_VigenciaInicio_To = DateTime.MinValue;
         AV33TFAutorizacaoConsumo_VigenciaFim = DateTime.MinValue;
         AV34TFAutorizacaoConsumo_VigenciaFim_To = DateTime.MinValue;
         AV39TFAutorizacaoConsumo_UnidadeMedicaoNom = "";
         AV40TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel = "";
         AV25ddo_AutorizacaoConsumo_CodigoTitleControlIdToReplace = "";
         AV31ddo_AutorizacaoConsumo_VigenciaInicioTitleControlIdToReplace = "";
         AV37ddo_AutorizacaoConsumo_VigenciaFimTitleControlIdToReplace = "";
         AV41ddo_AutorizacaoConsumo_UnidadeMedicaoNomTitleControlIdToReplace = "";
         AV45ddo_AutorizacaoConsumo_QuantidadeTitleControlIdToReplace = "";
         AV49ddo_AutorizacaoConsumo_ValorTitleControlIdToReplace = "";
         AV53ddo_AutorizacaoConsumo_StatusTitleControlIdToReplace = "";
         AV56ddo_AutorizacaoConsumo_AtivoTitleControlIdToReplace = "";
         AV66Pgmname = "";
         AV52TFAutorizacaoConsumo_Status_Sels = new GxSimpleCollection();
         GXKey = "";
         forbiddenHiddens = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV57DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV22AutorizacaoConsumo_CodigoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV26AutorizacaoConsumo_VigenciaInicioTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV32AutorizacaoConsumo_VigenciaFimTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV38AutorizacaoConsumo_UnidadeMedicaoNomTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV42AutorizacaoConsumo_QuantidadeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV46AutorizacaoConsumo_ValorTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV50AutorizacaoConsumo_StatusTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV54AutorizacaoConsumo_AtivoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         Ddo_autorizacaoconsumo_codigo_Filteredtext_set = "";
         Ddo_autorizacaoconsumo_codigo_Filteredtextto_set = "";
         Ddo_autorizacaoconsumo_codigo_Sortedstatus = "";
         Ddo_autorizacaoconsumo_vigenciainicio_Filteredtext_set = "";
         Ddo_autorizacaoconsumo_vigenciainicio_Filteredtextto_set = "";
         Ddo_autorizacaoconsumo_vigenciainicio_Sortedstatus = "";
         Ddo_autorizacaoconsumo_vigenciafim_Filteredtext_set = "";
         Ddo_autorizacaoconsumo_vigenciafim_Filteredtextto_set = "";
         Ddo_autorizacaoconsumo_vigenciafim_Sortedstatus = "";
         Ddo_autorizacaoconsumo_unidademedicaonom_Filteredtext_set = "";
         Ddo_autorizacaoconsumo_unidademedicaonom_Selectedvalue_set = "";
         Ddo_autorizacaoconsumo_unidademedicaonom_Sortedstatus = "";
         Ddo_autorizacaoconsumo_quantidade_Filteredtext_set = "";
         Ddo_autorizacaoconsumo_quantidade_Filteredtextto_set = "";
         Ddo_autorizacaoconsumo_quantidade_Sortedstatus = "";
         Ddo_autorizacaoconsumo_valor_Filteredtext_set = "";
         Ddo_autorizacaoconsumo_valor_Filteredtextto_set = "";
         Ddo_autorizacaoconsumo_valor_Sortedstatus = "";
         Ddo_autorizacaoconsumo_status_Selectedvalue_set = "";
         Ddo_autorizacaoconsumo_status_Sortedstatus = "";
         Ddo_autorizacaoconsumo_ativo_Selectedvalue_set = "";
         Ddo_autorizacaoconsumo_ativo_Sortedstatus = "";
         GX_FocusControl = "";
         TempTags = "";
         AV29DDO_AutorizacaoConsumo_VigenciaInicioAuxDate = DateTime.MinValue;
         AV30DDO_AutorizacaoConsumo_VigenciaInicioAuxDateTo = DateTime.MinValue;
         AV35DDO_AutorizacaoConsumo_VigenciaFimAuxDate = DateTime.MinValue;
         AV36DDO_AutorizacaoConsumo_VigenciaFimAuxDateTo = DateTime.MinValue;
         ClassString = "";
         StyleString = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV17CancelarAutorizcaoConsumo = "";
         AV63Cancelarautorizcaoconsumo_GXI = "";
         AV20Autorizar = "";
         AV64Autorizar_GXI = "";
         AV21Encerrar = "";
         AV65Encerrar_GXI = "";
         A1775AutorizacaoConsumo_VigenciaInicio = DateTime.MinValue;
         A1776AutorizacaoConsumo_VigenciaFim = DateTime.MinValue;
         A1778AutorizacaoConsumo_UnidadeMedicaoNom = "";
         A1787AutorizacaoConsumo_Status = "";
         GXCCtl = "";
         GridContainer = new GXWebGrid( context);
         scmdbuf = "";
         lV39TFAutorizacaoConsumo_UnidadeMedicaoNom = "";
         H00NB2_A1777AutorizacaoConsumo_UnidadeMedicaoCod = new int[1] ;
         H00NB2_A74Contrato_Codigo = new int[1] ;
         H00NB2_A1780AutorizacaoConsumo_Ativo = new bool[] {false} ;
         H00NB2_A1787AutorizacaoConsumo_Status = new String[] {""} ;
         H00NB2_A1782AutorizacaoConsumo_Valor = new decimal[1] ;
         H00NB2_A1779AutorizacaoConsumo_Quantidade = new short[1] ;
         H00NB2_A1778AutorizacaoConsumo_UnidadeMedicaoNom = new String[] {""} ;
         H00NB2_n1778AutorizacaoConsumo_UnidadeMedicaoNom = new bool[] {false} ;
         H00NB2_A1776AutorizacaoConsumo_VigenciaFim = new DateTime[] {DateTime.MinValue} ;
         H00NB2_A1775AutorizacaoConsumo_VigenciaInicio = new DateTime[] {DateTime.MinValue} ;
         H00NB2_A1774AutorizacaoConsumo_Codigo = new int[1] ;
         H00NB3_AGRID_nRecordCount = new long[1] ;
         hsh = "";
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV51TFAutorizacaoConsumo_Status_SelsJson = "";
         GridRow = new GXWebRow();
         AV15Session = context.GetSession();
         AV11GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV12GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8HTTPRequest = new GxHttpRequest( context);
         AV10TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         sStyleString = "";
         imgInsert_Jsonclick = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlAV7Contrato_Codigo = "";
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.contratoautorizacaoconsumowc__default(),
            new Object[][] {
                new Object[] {
               H00NB2_A1777AutorizacaoConsumo_UnidadeMedicaoCod, H00NB2_A74Contrato_Codigo, H00NB2_A1780AutorizacaoConsumo_Ativo, H00NB2_A1787AutorizacaoConsumo_Status, H00NB2_A1782AutorizacaoConsumo_Valor, H00NB2_A1779AutorizacaoConsumo_Quantidade, H00NB2_A1778AutorizacaoConsumo_UnidadeMedicaoNom, H00NB2_n1778AutorizacaoConsumo_UnidadeMedicaoNom, H00NB2_A1776AutorizacaoConsumo_VigenciaFim, H00NB2_A1775AutorizacaoConsumo_VigenciaInicio,
               H00NB2_A1774AutorizacaoConsumo_Codigo
               }
               , new Object[] {
               H00NB3_AGRID_nRecordCount
               }
            }
         );
         AV66Pgmname = "ContratoAutorizacaoConsumoWC";
         /* GeneXus formulas. */
         AV66Pgmname = "ContratoAutorizacaoConsumoWC";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short nRC_GXsfl_19 ;
      private short nGXsfl_19_idx=1 ;
      private short AV13OrderedBy ;
      private short AV43TFAutorizacaoConsumo_Quantidade ;
      private short AV44TFAutorizacaoConsumo_Quantidade_To ;
      private short AV55TFAutorizacaoConsumo_Ativo_Sel ;
      private short initialized ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDraw ;
      private short nDoneStart ;
      private short A1779AutorizacaoConsumo_Quantidade ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_19_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short edtAutorizacaoConsumo_Codigo_Titleformat ;
      private short edtAutorizacaoConsumo_VigenciaInicio_Titleformat ;
      private short edtAutorizacaoConsumo_VigenciaFim_Titleformat ;
      private short edtAutorizacaoConsumo_UnidadeMedicaoNom_Titleformat ;
      private short edtAutorizacaoConsumo_Quantidade_Titleformat ;
      private short edtAutorizacaoConsumo_Valor_Titleformat ;
      private short cmbAutorizacaoConsumo_Status_Titleformat ;
      private short chkAutorizacaoConsumo_Ativo_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int AV7Contrato_Codigo ;
      private int wcpOAV7Contrato_Codigo ;
      private int subGrid_Rows ;
      private int AV23TFAutorizacaoConsumo_Codigo ;
      private int AV24TFAutorizacaoConsumo_Codigo_To ;
      private int A74Contrato_Codigo ;
      private int Gridpaginationbar_Pagestoshow ;
      private int Ddo_autorizacaoconsumo_unidademedicaonom_Datalistupdateminimumcharacters ;
      private int edtContrato_Codigo_Visible ;
      private int AV19Contrato_Codigo_Selected ;
      private int edtavContrato_codigo_selected_Visible ;
      private int AV18AutorizacaoConsumo_Codigo_Selected ;
      private int edtavAutorizacaoconsumo_codigo_selected_Visible ;
      private int edtavOrderedby_Visible ;
      private int edtavOrdereddsc_Visible ;
      private int edtavTfautorizacaoconsumo_codigo_Visible ;
      private int edtavTfautorizacaoconsumo_codigo_to_Visible ;
      private int edtavTfautorizacaoconsumo_vigenciainicio_Visible ;
      private int edtavTfautorizacaoconsumo_vigenciainicio_to_Visible ;
      private int edtavTfautorizacaoconsumo_vigenciafim_Visible ;
      private int edtavTfautorizacaoconsumo_vigenciafim_to_Visible ;
      private int edtavTfautorizacaoconsumo_unidademedicaonom_Visible ;
      private int edtavTfautorizacaoconsumo_unidademedicaonom_sel_Visible ;
      private int edtavTfautorizacaoconsumo_quantidade_Visible ;
      private int edtavTfautorizacaoconsumo_quantidade_to_Visible ;
      private int edtavTfautorizacaoconsumo_valor_Visible ;
      private int edtavTfautorizacaoconsumo_valor_to_Visible ;
      private int edtavTfautorizacaoconsumo_ativo_sel_Visible ;
      private int edtavDdo_autorizacaoconsumo_codigotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_autorizacaoconsumo_vigenciainiciotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_autorizacaoconsumo_vigenciafimtitlecontrolidtoreplace_Visible ;
      private int edtavDdo_autorizacaoconsumo_unidademedicaonomtitlecontrolidtoreplace_Visible ;
      private int edtavDdo_autorizacaoconsumo_quantidadetitlecontrolidtoreplace_Visible ;
      private int edtavDdo_autorizacaoconsumo_valortitlecontrolidtoreplace_Visible ;
      private int edtavDdo_autorizacaoconsumo_statustitlecontrolidtoreplace_Visible ;
      private int edtavDdo_autorizacaoconsumo_ativotitlecontrolidtoreplace_Visible ;
      private int A1774AutorizacaoConsumo_Codigo ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int AV52TFAutorizacaoConsumo_Status_Sels_Count ;
      private int A1777AutorizacaoConsumo_UnidadeMedicaoCod ;
      private int imgInsert_Visible ;
      private int edtavCancelarautorizcaoconsumo_Visible ;
      private int edtavAutorizar_Visible ;
      private int edtavEncerrar_Visible ;
      private int AV58PageToGo ;
      private int edtavCancelarautorizcaoconsumo_Enabled ;
      private int AV67GXV1 ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private int edtavAutorizar_Enabled ;
      private int edtavEncerrar_Enabled ;
      private long GRID_nFirstRecordOnPage ;
      private long AV59GridCurrentPage ;
      private long AV60GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private decimal AV47TFAutorizacaoConsumo_Valor ;
      private decimal AV48TFAutorizacaoConsumo_Valor_To ;
      private decimal A1782AutorizacaoConsumo_Valor ;
      private String Gridpaginationbar_Selectedpage ;
      private String Ddo_autorizacaoconsumo_codigo_Activeeventkey ;
      private String Ddo_autorizacaoconsumo_codigo_Filteredtext_get ;
      private String Ddo_autorizacaoconsumo_codigo_Filteredtextto_get ;
      private String Ddo_autorizacaoconsumo_vigenciainicio_Activeeventkey ;
      private String Ddo_autorizacaoconsumo_vigenciainicio_Filteredtext_get ;
      private String Ddo_autorizacaoconsumo_vigenciainicio_Filteredtextto_get ;
      private String Ddo_autorizacaoconsumo_vigenciafim_Activeeventkey ;
      private String Ddo_autorizacaoconsumo_vigenciafim_Filteredtext_get ;
      private String Ddo_autorizacaoconsumo_vigenciafim_Filteredtextto_get ;
      private String Ddo_autorizacaoconsumo_unidademedicaonom_Activeeventkey ;
      private String Ddo_autorizacaoconsumo_unidademedicaonom_Filteredtext_get ;
      private String Ddo_autorizacaoconsumo_unidademedicaonom_Selectedvalue_get ;
      private String Ddo_autorizacaoconsumo_quantidade_Activeeventkey ;
      private String Ddo_autorizacaoconsumo_quantidade_Filteredtext_get ;
      private String Ddo_autorizacaoconsumo_quantidade_Filteredtextto_get ;
      private String Ddo_autorizacaoconsumo_valor_Activeeventkey ;
      private String Ddo_autorizacaoconsumo_valor_Filteredtext_get ;
      private String Ddo_autorizacaoconsumo_valor_Filteredtextto_get ;
      private String Ddo_autorizacaoconsumo_status_Activeeventkey ;
      private String Ddo_autorizacaoconsumo_status_Selectedvalue_get ;
      private String Ddo_autorizacaoconsumo_ativo_Activeeventkey ;
      private String Ddo_autorizacaoconsumo_ativo_Selectedvalue_get ;
      private String Dvelop_confirmpanel_cancelarautorizcaoconsumo_Result ;
      private String Dvelop_confirmpanel_autorizar_Result ;
      private String Dvelop_confirmpanel_encerrar_Result ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String sGXsfl_19_idx="0001" ;
      private String AV39TFAutorizacaoConsumo_UnidadeMedicaoNom ;
      private String AV40TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel ;
      private String AV66Pgmname ;
      private String GXKey ;
      private String forbiddenHiddens ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Dvelop_confirmpanel_cancelarautorizcaoconsumo_Title ;
      private String Dvelop_confirmpanel_cancelarautorizcaoconsumo_Confirmationtext ;
      private String Dvelop_confirmpanel_cancelarautorizcaoconsumo_Yesbuttoncaption ;
      private String Dvelop_confirmpanel_cancelarautorizcaoconsumo_Nobuttoncaption ;
      private String Dvelop_confirmpanel_cancelarautorizcaoconsumo_Cancelbuttoncaption ;
      private String Dvelop_confirmpanel_cancelarautorizcaoconsumo_Yesbuttonposition ;
      private String Dvelop_confirmpanel_cancelarautorizcaoconsumo_Confirmtype ;
      private String Dvelop_confirmpanel_autorizar_Title ;
      private String Dvelop_confirmpanel_autorizar_Confirmationtext ;
      private String Dvelop_confirmpanel_autorizar_Yesbuttoncaption ;
      private String Dvelop_confirmpanel_autorizar_Nobuttoncaption ;
      private String Dvelop_confirmpanel_autorizar_Cancelbuttoncaption ;
      private String Dvelop_confirmpanel_autorizar_Yesbuttonposition ;
      private String Dvelop_confirmpanel_autorizar_Confirmtype ;
      private String Dvelop_confirmpanel_encerrar_Title ;
      private String Dvelop_confirmpanel_encerrar_Confirmationtext ;
      private String Dvelop_confirmpanel_encerrar_Yesbuttoncaption ;
      private String Dvelop_confirmpanel_encerrar_Nobuttoncaption ;
      private String Dvelop_confirmpanel_encerrar_Cancelbuttoncaption ;
      private String Dvelop_confirmpanel_encerrar_Yesbuttonposition ;
      private String Dvelop_confirmpanel_encerrar_Confirmtype ;
      private String Ddo_autorizacaoconsumo_codigo_Caption ;
      private String Ddo_autorizacaoconsumo_codigo_Tooltip ;
      private String Ddo_autorizacaoconsumo_codigo_Cls ;
      private String Ddo_autorizacaoconsumo_codigo_Filteredtext_set ;
      private String Ddo_autorizacaoconsumo_codigo_Filteredtextto_set ;
      private String Ddo_autorizacaoconsumo_codigo_Dropdownoptionstype ;
      private String Ddo_autorizacaoconsumo_codigo_Titlecontrolidtoreplace ;
      private String Ddo_autorizacaoconsumo_codigo_Sortedstatus ;
      private String Ddo_autorizacaoconsumo_codigo_Filtertype ;
      private String Ddo_autorizacaoconsumo_codigo_Sortasc ;
      private String Ddo_autorizacaoconsumo_codigo_Sortdsc ;
      private String Ddo_autorizacaoconsumo_codigo_Cleanfilter ;
      private String Ddo_autorizacaoconsumo_codigo_Rangefilterfrom ;
      private String Ddo_autorizacaoconsumo_codigo_Rangefilterto ;
      private String Ddo_autorizacaoconsumo_codigo_Searchbuttontext ;
      private String Ddo_autorizacaoconsumo_vigenciainicio_Caption ;
      private String Ddo_autorizacaoconsumo_vigenciainicio_Tooltip ;
      private String Ddo_autorizacaoconsumo_vigenciainicio_Cls ;
      private String Ddo_autorizacaoconsumo_vigenciainicio_Filteredtext_set ;
      private String Ddo_autorizacaoconsumo_vigenciainicio_Filteredtextto_set ;
      private String Ddo_autorizacaoconsumo_vigenciainicio_Dropdownoptionstype ;
      private String Ddo_autorizacaoconsumo_vigenciainicio_Titlecontrolidtoreplace ;
      private String Ddo_autorizacaoconsumo_vigenciainicio_Sortedstatus ;
      private String Ddo_autorizacaoconsumo_vigenciainicio_Filtertype ;
      private String Ddo_autorizacaoconsumo_vigenciainicio_Sortasc ;
      private String Ddo_autorizacaoconsumo_vigenciainicio_Sortdsc ;
      private String Ddo_autorizacaoconsumo_vigenciainicio_Cleanfilter ;
      private String Ddo_autorizacaoconsumo_vigenciainicio_Rangefilterfrom ;
      private String Ddo_autorizacaoconsumo_vigenciainicio_Rangefilterto ;
      private String Ddo_autorizacaoconsumo_vigenciainicio_Searchbuttontext ;
      private String Ddo_autorizacaoconsumo_vigenciafim_Caption ;
      private String Ddo_autorizacaoconsumo_vigenciafim_Tooltip ;
      private String Ddo_autorizacaoconsumo_vigenciafim_Cls ;
      private String Ddo_autorizacaoconsumo_vigenciafim_Filteredtext_set ;
      private String Ddo_autorizacaoconsumo_vigenciafim_Filteredtextto_set ;
      private String Ddo_autorizacaoconsumo_vigenciafim_Dropdownoptionstype ;
      private String Ddo_autorizacaoconsumo_vigenciafim_Titlecontrolidtoreplace ;
      private String Ddo_autorizacaoconsumo_vigenciafim_Sortedstatus ;
      private String Ddo_autorizacaoconsumo_vigenciafim_Filtertype ;
      private String Ddo_autorizacaoconsumo_vigenciafim_Sortasc ;
      private String Ddo_autorizacaoconsumo_vigenciafim_Sortdsc ;
      private String Ddo_autorizacaoconsumo_vigenciafim_Cleanfilter ;
      private String Ddo_autorizacaoconsumo_vigenciafim_Rangefilterfrom ;
      private String Ddo_autorizacaoconsumo_vigenciafim_Rangefilterto ;
      private String Ddo_autorizacaoconsumo_vigenciafim_Searchbuttontext ;
      private String Ddo_autorizacaoconsumo_unidademedicaonom_Caption ;
      private String Ddo_autorizacaoconsumo_unidademedicaonom_Tooltip ;
      private String Ddo_autorizacaoconsumo_unidademedicaonom_Cls ;
      private String Ddo_autorizacaoconsumo_unidademedicaonom_Filteredtext_set ;
      private String Ddo_autorizacaoconsumo_unidademedicaonom_Selectedvalue_set ;
      private String Ddo_autorizacaoconsumo_unidademedicaonom_Dropdownoptionstype ;
      private String Ddo_autorizacaoconsumo_unidademedicaonom_Titlecontrolidtoreplace ;
      private String Ddo_autorizacaoconsumo_unidademedicaonom_Sortedstatus ;
      private String Ddo_autorizacaoconsumo_unidademedicaonom_Filtertype ;
      private String Ddo_autorizacaoconsumo_unidademedicaonom_Datalisttype ;
      private String Ddo_autorizacaoconsumo_unidademedicaonom_Datalistproc ;
      private String Ddo_autorizacaoconsumo_unidademedicaonom_Sortasc ;
      private String Ddo_autorizacaoconsumo_unidademedicaonom_Sortdsc ;
      private String Ddo_autorizacaoconsumo_unidademedicaonom_Loadingdata ;
      private String Ddo_autorizacaoconsumo_unidademedicaonom_Cleanfilter ;
      private String Ddo_autorizacaoconsumo_unidademedicaonom_Noresultsfound ;
      private String Ddo_autorizacaoconsumo_unidademedicaonom_Searchbuttontext ;
      private String Ddo_autorizacaoconsumo_quantidade_Caption ;
      private String Ddo_autorizacaoconsumo_quantidade_Tooltip ;
      private String Ddo_autorizacaoconsumo_quantidade_Cls ;
      private String Ddo_autorizacaoconsumo_quantidade_Filteredtext_set ;
      private String Ddo_autorizacaoconsumo_quantidade_Filteredtextto_set ;
      private String Ddo_autorizacaoconsumo_quantidade_Dropdownoptionstype ;
      private String Ddo_autorizacaoconsumo_quantidade_Titlecontrolidtoreplace ;
      private String Ddo_autorizacaoconsumo_quantidade_Sortedstatus ;
      private String Ddo_autorizacaoconsumo_quantidade_Filtertype ;
      private String Ddo_autorizacaoconsumo_quantidade_Sortasc ;
      private String Ddo_autorizacaoconsumo_quantidade_Sortdsc ;
      private String Ddo_autorizacaoconsumo_quantidade_Cleanfilter ;
      private String Ddo_autorizacaoconsumo_quantidade_Rangefilterfrom ;
      private String Ddo_autorizacaoconsumo_quantidade_Rangefilterto ;
      private String Ddo_autorizacaoconsumo_quantidade_Searchbuttontext ;
      private String Ddo_autorizacaoconsumo_valor_Caption ;
      private String Ddo_autorizacaoconsumo_valor_Tooltip ;
      private String Ddo_autorizacaoconsumo_valor_Cls ;
      private String Ddo_autorizacaoconsumo_valor_Filteredtext_set ;
      private String Ddo_autorizacaoconsumo_valor_Filteredtextto_set ;
      private String Ddo_autorizacaoconsumo_valor_Dropdownoptionstype ;
      private String Ddo_autorizacaoconsumo_valor_Titlecontrolidtoreplace ;
      private String Ddo_autorizacaoconsumo_valor_Sortedstatus ;
      private String Ddo_autorizacaoconsumo_valor_Filtertype ;
      private String Ddo_autorizacaoconsumo_valor_Sortasc ;
      private String Ddo_autorizacaoconsumo_valor_Sortdsc ;
      private String Ddo_autorizacaoconsumo_valor_Cleanfilter ;
      private String Ddo_autorizacaoconsumo_valor_Rangefilterfrom ;
      private String Ddo_autorizacaoconsumo_valor_Rangefilterto ;
      private String Ddo_autorizacaoconsumo_valor_Searchbuttontext ;
      private String Ddo_autorizacaoconsumo_status_Caption ;
      private String Ddo_autorizacaoconsumo_status_Tooltip ;
      private String Ddo_autorizacaoconsumo_status_Cls ;
      private String Ddo_autorizacaoconsumo_status_Selectedvalue_set ;
      private String Ddo_autorizacaoconsumo_status_Dropdownoptionstype ;
      private String Ddo_autorizacaoconsumo_status_Titlecontrolidtoreplace ;
      private String Ddo_autorizacaoconsumo_status_Sortedstatus ;
      private String Ddo_autorizacaoconsumo_status_Datalisttype ;
      private String Ddo_autorizacaoconsumo_status_Datalistfixedvalues ;
      private String Ddo_autorizacaoconsumo_status_Sortasc ;
      private String Ddo_autorizacaoconsumo_status_Sortdsc ;
      private String Ddo_autorizacaoconsumo_status_Cleanfilter ;
      private String Ddo_autorizacaoconsumo_status_Searchbuttontext ;
      private String Ddo_autorizacaoconsumo_ativo_Caption ;
      private String Ddo_autorizacaoconsumo_ativo_Tooltip ;
      private String Ddo_autorizacaoconsumo_ativo_Cls ;
      private String Ddo_autorizacaoconsumo_ativo_Selectedvalue_set ;
      private String Ddo_autorizacaoconsumo_ativo_Dropdownoptionstype ;
      private String Ddo_autorizacaoconsumo_ativo_Titlecontrolidtoreplace ;
      private String Ddo_autorizacaoconsumo_ativo_Sortedstatus ;
      private String Ddo_autorizacaoconsumo_ativo_Datalisttype ;
      private String Ddo_autorizacaoconsumo_ativo_Datalistfixedvalues ;
      private String Ddo_autorizacaoconsumo_ativo_Sortasc ;
      private String Ddo_autorizacaoconsumo_ativo_Sortdsc ;
      private String Ddo_autorizacaoconsumo_ativo_Cleanfilter ;
      private String Ddo_autorizacaoconsumo_ativo_Searchbuttontext ;
      private String GX_FocusControl ;
      private String edtContrato_Codigo_Internalname ;
      private String edtContrato_Codigo_Jsonclick ;
      private String TempTags ;
      private String edtavContrato_codigo_selected_Internalname ;
      private String edtavContrato_codigo_selected_Jsonclick ;
      private String edtavAutorizacaoconsumo_codigo_selected_Internalname ;
      private String edtavAutorizacaoconsumo_codigo_selected_Jsonclick ;
      private String edtavOrderedby_Internalname ;
      private String edtavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Internalname ;
      private String edtavOrdereddsc_Jsonclick ;
      private String edtavTfautorizacaoconsumo_codigo_Internalname ;
      private String edtavTfautorizacaoconsumo_codigo_Jsonclick ;
      private String edtavTfautorizacaoconsumo_codigo_to_Internalname ;
      private String edtavTfautorizacaoconsumo_codigo_to_Jsonclick ;
      private String edtavTfautorizacaoconsumo_vigenciainicio_Internalname ;
      private String edtavTfautorizacaoconsumo_vigenciainicio_Jsonclick ;
      private String edtavTfautorizacaoconsumo_vigenciainicio_to_Internalname ;
      private String edtavTfautorizacaoconsumo_vigenciainicio_to_Jsonclick ;
      private String divDdo_autorizacaoconsumo_vigenciainicioauxdates_Internalname ;
      private String edtavDdo_autorizacaoconsumo_vigenciainicioauxdate_Internalname ;
      private String edtavDdo_autorizacaoconsumo_vigenciainicioauxdate_Jsonclick ;
      private String edtavDdo_autorizacaoconsumo_vigenciainicioauxdateto_Internalname ;
      private String edtavDdo_autorizacaoconsumo_vigenciainicioauxdateto_Jsonclick ;
      private String edtavTfautorizacaoconsumo_vigenciafim_Internalname ;
      private String edtavTfautorizacaoconsumo_vigenciafim_Jsonclick ;
      private String edtavTfautorizacaoconsumo_vigenciafim_to_Internalname ;
      private String edtavTfautorizacaoconsumo_vigenciafim_to_Jsonclick ;
      private String divDdo_autorizacaoconsumo_vigenciafimauxdates_Internalname ;
      private String edtavDdo_autorizacaoconsumo_vigenciafimauxdate_Internalname ;
      private String edtavDdo_autorizacaoconsumo_vigenciafimauxdate_Jsonclick ;
      private String edtavDdo_autorizacaoconsumo_vigenciafimauxdateto_Internalname ;
      private String edtavDdo_autorizacaoconsumo_vigenciafimauxdateto_Jsonclick ;
      private String edtavTfautorizacaoconsumo_unidademedicaonom_Internalname ;
      private String edtavTfautorizacaoconsumo_unidademedicaonom_Jsonclick ;
      private String edtavTfautorizacaoconsumo_unidademedicaonom_sel_Internalname ;
      private String edtavTfautorizacaoconsumo_unidademedicaonom_sel_Jsonclick ;
      private String edtavTfautorizacaoconsumo_quantidade_Internalname ;
      private String edtavTfautorizacaoconsumo_quantidade_Jsonclick ;
      private String edtavTfautorizacaoconsumo_quantidade_to_Internalname ;
      private String edtavTfautorizacaoconsumo_quantidade_to_Jsonclick ;
      private String edtavTfautorizacaoconsumo_valor_Internalname ;
      private String edtavTfautorizacaoconsumo_valor_Jsonclick ;
      private String edtavTfautorizacaoconsumo_valor_to_Internalname ;
      private String edtavTfautorizacaoconsumo_valor_to_Jsonclick ;
      private String edtavTfautorizacaoconsumo_ativo_sel_Internalname ;
      private String edtavTfautorizacaoconsumo_ativo_sel_Jsonclick ;
      private String ClassString ;
      private String StyleString ;
      private String edtavDdo_autorizacaoconsumo_codigotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_autorizacaoconsumo_vigenciainiciotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_autorizacaoconsumo_vigenciafimtitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_autorizacaoconsumo_unidademedicaonomtitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_autorizacaoconsumo_quantidadetitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_autorizacaoconsumo_valortitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_autorizacaoconsumo_statustitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_autorizacaoconsumo_ativotitlecontrolidtoreplace_Internalname ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavCancelarautorizcaoconsumo_Internalname ;
      private String edtavAutorizar_Internalname ;
      private String edtavEncerrar_Internalname ;
      private String edtAutorizacaoConsumo_Codigo_Internalname ;
      private String edtAutorizacaoConsumo_VigenciaInicio_Internalname ;
      private String edtAutorizacaoConsumo_VigenciaFim_Internalname ;
      private String A1778AutorizacaoConsumo_UnidadeMedicaoNom ;
      private String edtAutorizacaoConsumo_UnidadeMedicaoNom_Internalname ;
      private String edtAutorizacaoConsumo_Quantidade_Internalname ;
      private String edtAutorizacaoConsumo_Valor_Internalname ;
      private String cmbAutorizacaoConsumo_Status_Internalname ;
      private String A1787AutorizacaoConsumo_Status ;
      private String chkAutorizacaoConsumo_Ativo_Internalname ;
      private String GXCCtl ;
      private String scmdbuf ;
      private String lV39TFAutorizacaoConsumo_UnidadeMedicaoNom ;
      private String hsh ;
      private String subGrid_Internalname ;
      private String Ddo_autorizacaoconsumo_codigo_Internalname ;
      private String Ddo_autorizacaoconsumo_vigenciainicio_Internalname ;
      private String Ddo_autorizacaoconsumo_vigenciafim_Internalname ;
      private String Ddo_autorizacaoconsumo_unidademedicaonom_Internalname ;
      private String Ddo_autorizacaoconsumo_quantidade_Internalname ;
      private String Ddo_autorizacaoconsumo_valor_Internalname ;
      private String Ddo_autorizacaoconsumo_status_Internalname ;
      private String Ddo_autorizacaoconsumo_ativo_Internalname ;
      private String edtAutorizacaoConsumo_Codigo_Title ;
      private String edtAutorizacaoConsumo_VigenciaInicio_Title ;
      private String edtAutorizacaoConsumo_VigenciaFim_Title ;
      private String edtAutorizacaoConsumo_UnidadeMedicaoNom_Title ;
      private String edtAutorizacaoConsumo_Quantidade_Title ;
      private String edtAutorizacaoConsumo_Valor_Title ;
      private String imgInsert_Internalname ;
      private String edtavCancelarautorizcaoconsumo_Tooltiptext ;
      private String edtavAutorizar_Tooltiptext ;
      private String edtavEncerrar_Tooltiptext ;
      private String sStyleString ;
      private String tblTabledvelop_confirmpanel_encerrar_Internalname ;
      private String tblTabledvelop_confirmpanel_autorizar_Internalname ;
      private String tblTabledvelop_confirmpanel_cancelarautorizcaoconsumo_Internalname ;
      private String tblTablegridheader_Internalname ;
      private String tblTablemergedgrid_Internalname ;
      private String imgInsert_Jsonclick ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTablesearch_Internalname ;
      private String sCtrlAV7Contrato_Codigo ;
      private String sGXsfl_19_fel_idx="0001" ;
      private String edtavCancelarautorizcaoconsumo_Jsonclick ;
      private String edtavAutorizar_Jsonclick ;
      private String edtavEncerrar_Jsonclick ;
      private String ROClassString ;
      private String edtAutorizacaoConsumo_Codigo_Jsonclick ;
      private String edtAutorizacaoConsumo_VigenciaInicio_Jsonclick ;
      private String edtAutorizacaoConsumo_VigenciaFim_Jsonclick ;
      private String edtAutorizacaoConsumo_UnidadeMedicaoNom_Jsonclick ;
      private String edtAutorizacaoConsumo_Quantidade_Jsonclick ;
      private String edtAutorizacaoConsumo_Valor_Jsonclick ;
      private String cmbAutorizacaoConsumo_Status_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private String Dvelop_confirmpanel_cancelarautorizcaoconsumo_Internalname ;
      private String Dvelop_confirmpanel_autorizar_Internalname ;
      private String Dvelop_confirmpanel_encerrar_Internalname ;
      private DateTime AV27TFAutorizacaoConsumo_VigenciaInicio ;
      private DateTime AV28TFAutorizacaoConsumo_VigenciaInicio_To ;
      private DateTime AV33TFAutorizacaoConsumo_VigenciaFim ;
      private DateTime AV34TFAutorizacaoConsumo_VigenciaFim_To ;
      private DateTime AV29DDO_AutorizacaoConsumo_VigenciaInicioAuxDate ;
      private DateTime AV30DDO_AutorizacaoConsumo_VigenciaInicioAuxDateTo ;
      private DateTime AV35DDO_AutorizacaoConsumo_VigenciaFimAuxDate ;
      private DateTime AV36DDO_AutorizacaoConsumo_VigenciaFimAuxDateTo ;
      private DateTime A1775AutorizacaoConsumo_VigenciaInicio ;
      private DateTime A1776AutorizacaoConsumo_VigenciaFim ;
      private bool entryPointCalled ;
      private bool AV14OrderedDsc ;
      private bool A1780AutorizacaoConsumo_Ativo ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool Ddo_autorizacaoconsumo_codigo_Includesortasc ;
      private bool Ddo_autorizacaoconsumo_codigo_Includesortdsc ;
      private bool Ddo_autorizacaoconsumo_codigo_Includefilter ;
      private bool Ddo_autorizacaoconsumo_codigo_Filterisrange ;
      private bool Ddo_autorizacaoconsumo_codigo_Includedatalist ;
      private bool Ddo_autorizacaoconsumo_vigenciainicio_Includesortasc ;
      private bool Ddo_autorizacaoconsumo_vigenciainicio_Includesortdsc ;
      private bool Ddo_autorizacaoconsumo_vigenciainicio_Includefilter ;
      private bool Ddo_autorizacaoconsumo_vigenciainicio_Filterisrange ;
      private bool Ddo_autorizacaoconsumo_vigenciainicio_Includedatalist ;
      private bool Ddo_autorizacaoconsumo_vigenciafim_Includesortasc ;
      private bool Ddo_autorizacaoconsumo_vigenciafim_Includesortdsc ;
      private bool Ddo_autorizacaoconsumo_vigenciafim_Includefilter ;
      private bool Ddo_autorizacaoconsumo_vigenciafim_Filterisrange ;
      private bool Ddo_autorizacaoconsumo_vigenciafim_Includedatalist ;
      private bool Ddo_autorizacaoconsumo_unidademedicaonom_Includesortasc ;
      private bool Ddo_autorizacaoconsumo_unidademedicaonom_Includesortdsc ;
      private bool Ddo_autorizacaoconsumo_unidademedicaonom_Includefilter ;
      private bool Ddo_autorizacaoconsumo_unidademedicaonom_Filterisrange ;
      private bool Ddo_autorizacaoconsumo_unidademedicaonom_Includedatalist ;
      private bool Ddo_autorizacaoconsumo_quantidade_Includesortasc ;
      private bool Ddo_autorizacaoconsumo_quantidade_Includesortdsc ;
      private bool Ddo_autorizacaoconsumo_quantidade_Includefilter ;
      private bool Ddo_autorizacaoconsumo_quantidade_Filterisrange ;
      private bool Ddo_autorizacaoconsumo_quantidade_Includedatalist ;
      private bool Ddo_autorizacaoconsumo_valor_Includesortasc ;
      private bool Ddo_autorizacaoconsumo_valor_Includesortdsc ;
      private bool Ddo_autorizacaoconsumo_valor_Includefilter ;
      private bool Ddo_autorizacaoconsumo_valor_Filterisrange ;
      private bool Ddo_autorizacaoconsumo_valor_Includedatalist ;
      private bool Ddo_autorizacaoconsumo_status_Includesortasc ;
      private bool Ddo_autorizacaoconsumo_status_Includesortdsc ;
      private bool Ddo_autorizacaoconsumo_status_Includefilter ;
      private bool Ddo_autorizacaoconsumo_status_Includedatalist ;
      private bool Ddo_autorizacaoconsumo_status_Allowmultipleselection ;
      private bool Ddo_autorizacaoconsumo_ativo_Includesortasc ;
      private bool Ddo_autorizacaoconsumo_ativo_Includesortdsc ;
      private bool Ddo_autorizacaoconsumo_ativo_Includefilter ;
      private bool Ddo_autorizacaoconsumo_ativo_Includedatalist ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n1778AutorizacaoConsumo_UnidadeMedicaoNom ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV16TempBoolean ;
      private bool GXt_boolean2 ;
      private bool GXt_boolean3 ;
      private bool AV17CancelarAutorizcaoConsumo_IsBlob ;
      private bool AV20Autorizar_IsBlob ;
      private bool AV21Encerrar_IsBlob ;
      private String AV51TFAutorizacaoConsumo_Status_SelsJson ;
      private String AV25ddo_AutorizacaoConsumo_CodigoTitleControlIdToReplace ;
      private String AV31ddo_AutorizacaoConsumo_VigenciaInicioTitleControlIdToReplace ;
      private String AV37ddo_AutorizacaoConsumo_VigenciaFimTitleControlIdToReplace ;
      private String AV41ddo_AutorizacaoConsumo_UnidadeMedicaoNomTitleControlIdToReplace ;
      private String AV45ddo_AutorizacaoConsumo_QuantidadeTitleControlIdToReplace ;
      private String AV49ddo_AutorizacaoConsumo_ValorTitleControlIdToReplace ;
      private String AV53ddo_AutorizacaoConsumo_StatusTitleControlIdToReplace ;
      private String AV56ddo_AutorizacaoConsumo_AtivoTitleControlIdToReplace ;
      private String AV63Cancelarautorizcaoconsumo_GXI ;
      private String AV64Autorizar_GXI ;
      private String AV65Encerrar_GXI ;
      private String AV17CancelarAutorizcaoConsumo ;
      private String AV20Autorizar ;
      private String AV21Encerrar ;
      private IGxSession AV15Session ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbAutorizacaoConsumo_Status ;
      private GXCheckbox chkAutorizacaoConsumo_Ativo ;
      private IDataStoreProvider pr_default ;
      private int[] H00NB2_A1777AutorizacaoConsumo_UnidadeMedicaoCod ;
      private int[] H00NB2_A74Contrato_Codigo ;
      private bool[] H00NB2_A1780AutorizacaoConsumo_Ativo ;
      private String[] H00NB2_A1787AutorizacaoConsumo_Status ;
      private decimal[] H00NB2_A1782AutorizacaoConsumo_Valor ;
      private short[] H00NB2_A1779AutorizacaoConsumo_Quantidade ;
      private String[] H00NB2_A1778AutorizacaoConsumo_UnidadeMedicaoNom ;
      private bool[] H00NB2_n1778AutorizacaoConsumo_UnidadeMedicaoNom ;
      private DateTime[] H00NB2_A1776AutorizacaoConsumo_VigenciaFim ;
      private DateTime[] H00NB2_A1775AutorizacaoConsumo_VigenciaInicio ;
      private int[] H00NB2_A1774AutorizacaoConsumo_Codigo ;
      private long[] H00NB3_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV8HTTPRequest ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV52TFAutorizacaoConsumo_Status_Sels ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV22AutorizacaoConsumo_CodigoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV26AutorizacaoConsumo_VigenciaInicioTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV32AutorizacaoConsumo_VigenciaFimTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV38AutorizacaoConsumo_UnidadeMedicaoNomTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV42AutorizacaoConsumo_QuantidadeTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV46AutorizacaoConsumo_ValorTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV50AutorizacaoConsumo_StatusTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV54AutorizacaoConsumo_AtivoTitleFilterData ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV10TrnContextAtt ;
      private wwpbaseobjects.SdtWWPGridState AV11GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV12GridStateFilterValue ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV57DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 ;
   }

   public class contratoautorizacaoconsumowc__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H00NB2( IGxContext context ,
                                             String A1787AutorizacaoConsumo_Status ,
                                             IGxCollection AV52TFAutorizacaoConsumo_Status_Sels ,
                                             int AV23TFAutorizacaoConsumo_Codigo ,
                                             int AV24TFAutorizacaoConsumo_Codigo_To ,
                                             DateTime AV27TFAutorizacaoConsumo_VigenciaInicio ,
                                             DateTime AV28TFAutorizacaoConsumo_VigenciaInicio_To ,
                                             DateTime AV33TFAutorizacaoConsumo_VigenciaFim ,
                                             DateTime AV34TFAutorizacaoConsumo_VigenciaFim_To ,
                                             String AV40TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel ,
                                             String AV39TFAutorizacaoConsumo_UnidadeMedicaoNom ,
                                             short AV43TFAutorizacaoConsumo_Quantidade ,
                                             short AV44TFAutorizacaoConsumo_Quantidade_To ,
                                             decimal AV47TFAutorizacaoConsumo_Valor ,
                                             decimal AV48TFAutorizacaoConsumo_Valor_To ,
                                             int AV52TFAutorizacaoConsumo_Status_Sels_Count ,
                                             short AV55TFAutorizacaoConsumo_Ativo_Sel ,
                                             int A1774AutorizacaoConsumo_Codigo ,
                                             DateTime A1775AutorizacaoConsumo_VigenciaInicio ,
                                             DateTime A1776AutorizacaoConsumo_VigenciaFim ,
                                             String A1778AutorizacaoConsumo_UnidadeMedicaoNom ,
                                             short A1779AutorizacaoConsumo_Quantidade ,
                                             decimal A1782AutorizacaoConsumo_Valor ,
                                             bool A1780AutorizacaoConsumo_Ativo ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc ,
                                             int A74Contrato_Codigo ,
                                             int AV7Contrato_Codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [18] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " T1.[AutorizacaoConsumo_UnidadeMedicaoCod] AS AutorizacaoConsumo_UnidadeMedicaoCod, T1.[Contrato_Codigo], T1.[AutorizacaoConsumo_Ativo], T1.[AutorizacaoConsumo_Status], T1.[AutorizacaoConsumo_Valor], T1.[AutorizacaoConsumo_Quantidade], T2.[UnidadeMedicao_Nome] AS AutorizacaoConsumo_UnidadeMedicaoNom, T1.[AutorizacaoConsumo_VigenciaFim], T1.[AutorizacaoConsumo_VigenciaInicio], T1.[AutorizacaoConsumo_Codigo]";
         sFromString = " FROM ([AutorizacaoConsumo] T1 WITH (NOLOCK) INNER JOIN [UnidadeMedicao] T2 WITH (NOLOCK) ON T2.[UnidadeMedicao_Codigo] = T1.[AutorizacaoConsumo_UnidadeMedicaoCod])";
         sOrderString = "";
         sWhereString = sWhereString + " WHERE (T1.[Contrato_Codigo] = @AV7Contrato_Codigo)";
         if ( ! (0==AV23TFAutorizacaoConsumo_Codigo) )
         {
            sWhereString = sWhereString + " and (T1.[AutorizacaoConsumo_Codigo] >= @AV23TFAutorizacaoConsumo_Codigo)";
         }
         else
         {
            GXv_int4[1] = 1;
         }
         if ( ! (0==AV24TFAutorizacaoConsumo_Codigo_To) )
         {
            sWhereString = sWhereString + " and (T1.[AutorizacaoConsumo_Codigo] <= @AV24TFAutorizacaoConsumo_Codigo_To)";
         }
         else
         {
            GXv_int4[2] = 1;
         }
         if ( ! (DateTime.MinValue==AV27TFAutorizacaoConsumo_VigenciaInicio) )
         {
            sWhereString = sWhereString + " and (T1.[AutorizacaoConsumo_VigenciaInicio] >= @AV27TFAutorizacaoConsumo_VigenciaInicio)";
         }
         else
         {
            GXv_int4[3] = 1;
         }
         if ( ! (DateTime.MinValue==AV28TFAutorizacaoConsumo_VigenciaInicio_To) )
         {
            sWhereString = sWhereString + " and (T1.[AutorizacaoConsumo_VigenciaInicio] <= @AV28TFAutorizacaoConsumo_VigenciaInicio_To)";
         }
         else
         {
            GXv_int4[4] = 1;
         }
         if ( ! (DateTime.MinValue==AV33TFAutorizacaoConsumo_VigenciaFim) )
         {
            sWhereString = sWhereString + " and (T1.[AutorizacaoConsumo_VigenciaFim] >= @AV33TFAutorizacaoConsumo_VigenciaFim)";
         }
         else
         {
            GXv_int4[5] = 1;
         }
         if ( ! (DateTime.MinValue==AV34TFAutorizacaoConsumo_VigenciaFim_To) )
         {
            sWhereString = sWhereString + " and (T1.[AutorizacaoConsumo_VigenciaFim] <= @AV34TFAutorizacaoConsumo_VigenciaFim_To)";
         }
         else
         {
            GXv_int4[6] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV40TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV39TFAutorizacaoConsumo_UnidadeMedicaoNom)) ) )
         {
            sWhereString = sWhereString + " and (T2.[UnidadeMedicao_Nome] like @lV39TFAutorizacaoConsumo_UnidadeMedicaoNom)";
         }
         else
         {
            GXv_int4[7] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV40TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel)) )
         {
            sWhereString = sWhereString + " and (T2.[UnidadeMedicao_Nome] = @AV40TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel)";
         }
         else
         {
            GXv_int4[8] = 1;
         }
         if ( ! (0==AV43TFAutorizacaoConsumo_Quantidade) )
         {
            sWhereString = sWhereString + " and (T1.[AutorizacaoConsumo_Quantidade] >= @AV43TFAutorizacaoConsumo_Quantidade)";
         }
         else
         {
            GXv_int4[9] = 1;
         }
         if ( ! (0==AV44TFAutorizacaoConsumo_Quantidade_To) )
         {
            sWhereString = sWhereString + " and (T1.[AutorizacaoConsumo_Quantidade] <= @AV44TFAutorizacaoConsumo_Quantidade_To)";
         }
         else
         {
            GXv_int4[10] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV47TFAutorizacaoConsumo_Valor) )
         {
            sWhereString = sWhereString + " and (T1.[AutorizacaoConsumo_Valor] >= @AV47TFAutorizacaoConsumo_Valor)";
         }
         else
         {
            GXv_int4[11] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV48TFAutorizacaoConsumo_Valor_To) )
         {
            sWhereString = sWhereString + " and (T1.[AutorizacaoConsumo_Valor] <= @AV48TFAutorizacaoConsumo_Valor_To)";
         }
         else
         {
            GXv_int4[12] = 1;
         }
         if ( AV52TFAutorizacaoConsumo_Status_Sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV52TFAutorizacaoConsumo_Status_Sels, "T1.[AutorizacaoConsumo_Status] IN (", ")") + ")";
         }
         if ( AV55TFAutorizacaoConsumo_Ativo_Sel == 1 )
         {
            sWhereString = sWhereString + " and (T1.[AutorizacaoConsumo_Ativo] = 1)";
         }
         if ( AV55TFAutorizacaoConsumo_Ativo_Sel == 2 )
         {
            sWhereString = sWhereString + " and (T1.[AutorizacaoConsumo_Ativo] = 0)";
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contrato_Codigo], T1.[AutorizacaoConsumo_Codigo]";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contrato_Codigo] DESC, T1.[AutorizacaoConsumo_Codigo] DESC";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contrato_Codigo], T1.[AutorizacaoConsumo_VigenciaInicio]";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contrato_Codigo] DESC, T1.[AutorizacaoConsumo_VigenciaInicio] DESC";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contrato_Codigo], T1.[AutorizacaoConsumo_VigenciaFim]";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contrato_Codigo] DESC, T1.[AutorizacaoConsumo_VigenciaFim] DESC";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contrato_Codigo], T2.[UnidadeMedicao_Nome]";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contrato_Codigo] DESC, T2.[UnidadeMedicao_Nome] DESC";
         }
         else if ( ( AV13OrderedBy == 5 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contrato_Codigo], T1.[AutorizacaoConsumo_Quantidade]";
         }
         else if ( ( AV13OrderedBy == 5 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contrato_Codigo] DESC, T1.[AutorizacaoConsumo_Quantidade] DESC";
         }
         else if ( ( AV13OrderedBy == 6 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contrato_Codigo], T1.[AutorizacaoConsumo_Valor]";
         }
         else if ( ( AV13OrderedBy == 6 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contrato_Codigo] DESC, T1.[AutorizacaoConsumo_Valor] DESC";
         }
         else if ( ( AV13OrderedBy == 7 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contrato_Codigo], T1.[AutorizacaoConsumo_Status]";
         }
         else if ( ( AV13OrderedBy == 7 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contrato_Codigo] DESC, T1.[AutorizacaoConsumo_Status] DESC";
         }
         else if ( ( AV13OrderedBy == 8 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contrato_Codigo], T1.[AutorizacaoConsumo_Ativo]";
         }
         else if ( ( AV13OrderedBy == 8 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Contrato_Codigo] DESC, T1.[AutorizacaoConsumo_Ativo] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY T1.[AutorizacaoConsumo_Codigo]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      protected Object[] conditional_H00NB3( IGxContext context ,
                                             String A1787AutorizacaoConsumo_Status ,
                                             IGxCollection AV52TFAutorizacaoConsumo_Status_Sels ,
                                             int AV23TFAutorizacaoConsumo_Codigo ,
                                             int AV24TFAutorizacaoConsumo_Codigo_To ,
                                             DateTime AV27TFAutorizacaoConsumo_VigenciaInicio ,
                                             DateTime AV28TFAutorizacaoConsumo_VigenciaInicio_To ,
                                             DateTime AV33TFAutorizacaoConsumo_VigenciaFim ,
                                             DateTime AV34TFAutorizacaoConsumo_VigenciaFim_To ,
                                             String AV40TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel ,
                                             String AV39TFAutorizacaoConsumo_UnidadeMedicaoNom ,
                                             short AV43TFAutorizacaoConsumo_Quantidade ,
                                             short AV44TFAutorizacaoConsumo_Quantidade_To ,
                                             decimal AV47TFAutorizacaoConsumo_Valor ,
                                             decimal AV48TFAutorizacaoConsumo_Valor_To ,
                                             int AV52TFAutorizacaoConsumo_Status_Sels_Count ,
                                             short AV55TFAutorizacaoConsumo_Ativo_Sel ,
                                             int A1774AutorizacaoConsumo_Codigo ,
                                             DateTime A1775AutorizacaoConsumo_VigenciaInicio ,
                                             DateTime A1776AutorizacaoConsumo_VigenciaFim ,
                                             String A1778AutorizacaoConsumo_UnidadeMedicaoNom ,
                                             short A1779AutorizacaoConsumo_Quantidade ,
                                             decimal A1782AutorizacaoConsumo_Valor ,
                                             bool A1780AutorizacaoConsumo_Ativo ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc ,
                                             int A74Contrato_Codigo ,
                                             int AV7Contrato_Codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int6 ;
         GXv_int6 = new short [13] ;
         Object[] GXv_Object7 ;
         GXv_Object7 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM ([AutorizacaoConsumo] T1 WITH (NOLOCK) INNER JOIN [UnidadeMedicao] T2 WITH (NOLOCK) ON T2.[UnidadeMedicao_Codigo] = T1.[AutorizacaoConsumo_UnidadeMedicaoCod])";
         scmdbuf = scmdbuf + " WHERE (T1.[Contrato_Codigo] = @AV7Contrato_Codigo)";
         if ( ! (0==AV23TFAutorizacaoConsumo_Codigo) )
         {
            sWhereString = sWhereString + " and (T1.[AutorizacaoConsumo_Codigo] >= @AV23TFAutorizacaoConsumo_Codigo)";
         }
         else
         {
            GXv_int6[1] = 1;
         }
         if ( ! (0==AV24TFAutorizacaoConsumo_Codigo_To) )
         {
            sWhereString = sWhereString + " and (T1.[AutorizacaoConsumo_Codigo] <= @AV24TFAutorizacaoConsumo_Codigo_To)";
         }
         else
         {
            GXv_int6[2] = 1;
         }
         if ( ! (DateTime.MinValue==AV27TFAutorizacaoConsumo_VigenciaInicio) )
         {
            sWhereString = sWhereString + " and (T1.[AutorizacaoConsumo_VigenciaInicio] >= @AV27TFAutorizacaoConsumo_VigenciaInicio)";
         }
         else
         {
            GXv_int6[3] = 1;
         }
         if ( ! (DateTime.MinValue==AV28TFAutorizacaoConsumo_VigenciaInicio_To) )
         {
            sWhereString = sWhereString + " and (T1.[AutorizacaoConsumo_VigenciaInicio] <= @AV28TFAutorizacaoConsumo_VigenciaInicio_To)";
         }
         else
         {
            GXv_int6[4] = 1;
         }
         if ( ! (DateTime.MinValue==AV33TFAutorizacaoConsumo_VigenciaFim) )
         {
            sWhereString = sWhereString + " and (T1.[AutorizacaoConsumo_VigenciaFim] >= @AV33TFAutorizacaoConsumo_VigenciaFim)";
         }
         else
         {
            GXv_int6[5] = 1;
         }
         if ( ! (DateTime.MinValue==AV34TFAutorizacaoConsumo_VigenciaFim_To) )
         {
            sWhereString = sWhereString + " and (T1.[AutorizacaoConsumo_VigenciaFim] <= @AV34TFAutorizacaoConsumo_VigenciaFim_To)";
         }
         else
         {
            GXv_int6[6] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV40TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV39TFAutorizacaoConsumo_UnidadeMedicaoNom)) ) )
         {
            sWhereString = sWhereString + " and (T2.[UnidadeMedicao_Nome] like @lV39TFAutorizacaoConsumo_UnidadeMedicaoNom)";
         }
         else
         {
            GXv_int6[7] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV40TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel)) )
         {
            sWhereString = sWhereString + " and (T2.[UnidadeMedicao_Nome] = @AV40TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel)";
         }
         else
         {
            GXv_int6[8] = 1;
         }
         if ( ! (0==AV43TFAutorizacaoConsumo_Quantidade) )
         {
            sWhereString = sWhereString + " and (T1.[AutorizacaoConsumo_Quantidade] >= @AV43TFAutorizacaoConsumo_Quantidade)";
         }
         else
         {
            GXv_int6[9] = 1;
         }
         if ( ! (0==AV44TFAutorizacaoConsumo_Quantidade_To) )
         {
            sWhereString = sWhereString + " and (T1.[AutorizacaoConsumo_Quantidade] <= @AV44TFAutorizacaoConsumo_Quantidade_To)";
         }
         else
         {
            GXv_int6[10] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV47TFAutorizacaoConsumo_Valor) )
         {
            sWhereString = sWhereString + " and (T1.[AutorizacaoConsumo_Valor] >= @AV47TFAutorizacaoConsumo_Valor)";
         }
         else
         {
            GXv_int6[11] = 1;
         }
         if ( ! (Convert.ToDecimal(0)==AV48TFAutorizacaoConsumo_Valor_To) )
         {
            sWhereString = sWhereString + " and (T1.[AutorizacaoConsumo_Valor] <= @AV48TFAutorizacaoConsumo_Valor_To)";
         }
         else
         {
            GXv_int6[12] = 1;
         }
         if ( AV52TFAutorizacaoConsumo_Status_Sels_Count > 0 )
         {
            sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV52TFAutorizacaoConsumo_Status_Sels, "T1.[AutorizacaoConsumo_Status] IN (", ")") + ")";
         }
         if ( AV55TFAutorizacaoConsumo_Ativo_Sel == 1 )
         {
            sWhereString = sWhereString + " and (T1.[AutorizacaoConsumo_Ativo] = 1)";
         }
         if ( AV55TFAutorizacaoConsumo_Ativo_Sel == 2 )
         {
            sWhereString = sWhereString + " and (T1.[AutorizacaoConsumo_Ativo] = 0)";
         }
         scmdbuf = scmdbuf + sWhereString;
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 5 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 5 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 6 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 6 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 7 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 7 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 8 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 8 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object7[0] = scmdbuf;
         GXv_Object7[1] = GXv_int6;
         return GXv_Object7 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H00NB2(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (int)dynConstraints[2] , (int)dynConstraints[3] , (DateTime)dynConstraints[4] , (DateTime)dynConstraints[5] , (DateTime)dynConstraints[6] , (DateTime)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (short)dynConstraints[10] , (short)dynConstraints[11] , (decimal)dynConstraints[12] , (decimal)dynConstraints[13] , (int)dynConstraints[14] , (short)dynConstraints[15] , (int)dynConstraints[16] , (DateTime)dynConstraints[17] , (DateTime)dynConstraints[18] , (String)dynConstraints[19] , (short)dynConstraints[20] , (decimal)dynConstraints[21] , (bool)dynConstraints[22] , (short)dynConstraints[23] , (bool)dynConstraints[24] , (int)dynConstraints[25] , (int)dynConstraints[26] );
               case 1 :
                     return conditional_H00NB3(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (int)dynConstraints[2] , (int)dynConstraints[3] , (DateTime)dynConstraints[4] , (DateTime)dynConstraints[5] , (DateTime)dynConstraints[6] , (DateTime)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (short)dynConstraints[10] , (short)dynConstraints[11] , (decimal)dynConstraints[12] , (decimal)dynConstraints[13] , (int)dynConstraints[14] , (short)dynConstraints[15] , (int)dynConstraints[16] , (DateTime)dynConstraints[17] , (DateTime)dynConstraints[18] , (String)dynConstraints[19] , (short)dynConstraints[20] , (decimal)dynConstraints[21] , (bool)dynConstraints[22] , (short)dynConstraints[23] , (bool)dynConstraints[24] , (int)dynConstraints[25] , (int)dynConstraints[26] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00NB2 ;
          prmH00NB2 = new Object[] {
          new Object[] {"@AV7Contrato_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV23TFAutorizacaoConsumo_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV24TFAutorizacaoConsumo_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV27TFAutorizacaoConsumo_VigenciaInicio",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV28TFAutorizacaoConsumo_VigenciaInicio_To",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV33TFAutorizacaoConsumo_VigenciaFim",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV34TFAutorizacaoConsumo_VigenciaFim_To",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV39TFAutorizacaoConsumo_UnidadeMedicaoNom",SqlDbType.Char,50,0} ,
          new Object[] {"@AV40TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@AV43TFAutorizacaoConsumo_Quantidade",SqlDbType.SmallInt,3,0} ,
          new Object[] {"@AV44TFAutorizacaoConsumo_Quantidade_To",SqlDbType.SmallInt,3,0} ,
          new Object[] {"@AV47TFAutorizacaoConsumo_Valor",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV48TFAutorizacaoConsumo_Valor_To",SqlDbType.Decimal,18,5} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH00NB3 ;
          prmH00NB3 = new Object[] {
          new Object[] {"@AV7Contrato_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV23TFAutorizacaoConsumo_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV24TFAutorizacaoConsumo_Codigo_To",SqlDbType.Int,6,0} ,
          new Object[] {"@AV27TFAutorizacaoConsumo_VigenciaInicio",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV28TFAutorizacaoConsumo_VigenciaInicio_To",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV33TFAutorizacaoConsumo_VigenciaFim",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV34TFAutorizacaoConsumo_VigenciaFim_To",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV39TFAutorizacaoConsumo_UnidadeMedicaoNom",SqlDbType.Char,50,0} ,
          new Object[] {"@AV40TFAutorizacaoConsumo_UnidadeMedicaoNom_Sel",SqlDbType.Char,50,0} ,
          new Object[] {"@AV43TFAutorizacaoConsumo_Quantidade",SqlDbType.SmallInt,3,0} ,
          new Object[] {"@AV44TFAutorizacaoConsumo_Quantidade_To",SqlDbType.SmallInt,3,0} ,
          new Object[] {"@AV47TFAutorizacaoConsumo_Valor",SqlDbType.Decimal,18,5} ,
          new Object[] {"@AV48TFAutorizacaoConsumo_Valor_To",SqlDbType.Decimal,18,5}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00NB2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00NB2,11,0,true,false )
             ,new CursorDef("H00NB3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00NB3,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.getBool(3) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 3) ;
                ((decimal[]) buf[4])[0] = rslt.getDecimal(5) ;
                ((short[]) buf[5])[0] = rslt.getShort(6) ;
                ((String[]) buf[6])[0] = rslt.getString(7, 50) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(7);
                ((DateTime[]) buf[8])[0] = rslt.getGXDate(8) ;
                ((DateTime[]) buf[9])[0] = rslt.getGXDate(9) ;
                ((int[]) buf[10])[0] = rslt.getInt(10) ;
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[18]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[19]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[20]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[21]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[22]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[23]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[24]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[27]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[28]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[29]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[30]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[31]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[32]);
                }
                if ( (short)parms[15] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[33]);
                }
                if ( (short)parms[16] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[34]);
                }
                if ( (short)parms[17] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[35]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[13]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[14]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[15]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[16]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[17]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[18]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[19]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[22]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[23]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[24]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (decimal)parms[25]);
                }
                return;
       }
    }

 }

}
