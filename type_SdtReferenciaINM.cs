/*
               File: type_SdtReferenciaINM
        Description: Referencia INM
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/6/2020 23:55:32.19
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "ReferenciaINM" )]
   [XmlType(TypeName =  "ReferenciaINM" , Namespace = "GxEv3Up14_MeetrikaVs3" )]
   [System.Xml.Serialization.XmlInclude( typeof( SdtReferenciaINM_Anexos ))]
   [Serializable]
   public class SdtReferenciaINM : GxSilentTrnSdt, System.Web.SessionState.IRequiresSessionState
   {
      public SdtReferenciaINM( )
      {
         /* Constructor for serialization */
         gxTv_SdtReferenciaINM_Referenciainm_descricao = "";
         gxTv_SdtReferenciaINM_Referenciainm_areatrabalhodes = "";
         gxTv_SdtReferenciaINM_Mode = "";
         gxTv_SdtReferenciaINM_Referenciainm_descricao_Z = "";
         gxTv_SdtReferenciaINM_Referenciainm_areatrabalhodes_Z = "";
      }

      public SdtReferenciaINM( IGxContext context )
      {
         this.context = context;
         constructorCallingAssembly = Assembly.GetCallingAssembly();
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public void Load( int AV709ReferenciaINM_Codigo )
      {
         IGxSilentTrn obj ;
         obj = getTransaction();
         obj.LoadKey(new Object[] {(int)AV709ReferenciaINM_Codigo});
         return  ;
      }

      public override Object[][] GetBCKey( )
      {
         return (Object[][])(new Object[][]{new Object[]{"ReferenciaINM_Codigo", typeof(int)}}) ;
      }

      public override IGxCollection GetMessages( )
      {
         short item = 1 ;
         IGxCollection msgs = new GxObjectCollection( context, "Messages.Message", "Genexus", "SdtMessages_Message", "GeneXus.Programs") ;
         SdtMessages_Message m1 ;
         IGxSilentTrn trn = getTransaction() ;
         msglist msgList = trn.GetMessages() ;
         while ( item <= msgList.ItemCount )
         {
            m1 = new SdtMessages_Message(context);
            m1.gxTpr_Id = msgList.getItemValue(item);
            m1.gxTpr_Description = msgList.getItemText(item);
            m1.gxTpr_Type = msgList.getItemType(item);
            msgs.Add(m1, 0);
            item = (short)(item+1);
         }
         return msgs ;
      }

      public override GXProperties GetMetadata( )
      {
         GXProperties metadata = new GXProperties() ;
         metadata.Set("Name", "ReferenciaINM");
         metadata.Set("BT", "ReferenciaINM");
         metadata.Set("PK", "[ \"ReferenciaINM_Codigo\" ]");
         metadata.Set("PKAssigned", "[ \"ReferenciaINM_Codigo\" ]");
         metadata.Set("Levels", "[ \"Anexos\" ]");
         metadata.Set("FKList", "[ { \"FK\":[ \"AreaTrabalho_Codigo\" ],\"FKMap\":[ \"ReferenciaINM_AreaTrabalhoCod-AreaTrabalho_Codigo\" ] } ]");
         metadata.Set("AllowInsert", "True");
         metadata.Set("AllowUpdate", "True");
         metadata.Set("AllowDelete", "True");
         return metadata ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         if ( ! includeState )
         {
            XmlAttributeOverrides ov = new XmlAttributeOverrides();
            XmlAttributes attrs = new XmlAttributes();
            attrs.XmlIgnore = true;
            ov.Add(this.GetType(),  "gxTpr_Anexos_GxSilentTrnGridCollection" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Mode" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Initialized" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Referenciainm_codigo_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Referenciainm_descricao_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Referenciainm_areatrabalhocod_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Referenciainm_areatrabalhodes_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Referenciainm_ativo_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Referenciainm_areatrabalhodes_N" , attrs);
            xmls = new XmlSerializer(this.GetType(), ov, new Type[0], null, sNameSpace);
         }
         else
         {
            xmls = new XmlSerializer(this.GetType(), sNameSpace);
         }
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtReferenciaINM deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "GxEv3Up14_MeetrikaVs3" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtReferenciaINM)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtReferenciaINM obj ;
         obj = this;
         obj.gxTpr_Referenciainm_codigo = deserialized.gxTpr_Referenciainm_codigo;
         obj.gxTpr_Referenciainm_descricao = deserialized.gxTpr_Referenciainm_descricao;
         obj.gxTpr_Referenciainm_areatrabalhocod = deserialized.gxTpr_Referenciainm_areatrabalhocod;
         obj.gxTpr_Referenciainm_areatrabalhodes = deserialized.gxTpr_Referenciainm_areatrabalhodes;
         obj.gxTpr_Referenciainm_ativo = deserialized.gxTpr_Referenciainm_ativo;
         obj.gxTpr_Anexos = deserialized.gxTpr_Anexos;
         obj.gxTpr_Mode = deserialized.gxTpr_Mode;
         obj.gxTpr_Initialized = deserialized.gxTpr_Initialized;
         obj.gxTpr_Referenciainm_codigo_Z = deserialized.gxTpr_Referenciainm_codigo_Z;
         obj.gxTpr_Referenciainm_descricao_Z = deserialized.gxTpr_Referenciainm_descricao_Z;
         obj.gxTpr_Referenciainm_areatrabalhocod_Z = deserialized.gxTpr_Referenciainm_areatrabalhocod_Z;
         obj.gxTpr_Referenciainm_areatrabalhodes_Z = deserialized.gxTpr_Referenciainm_areatrabalhodes_Z;
         obj.gxTpr_Referenciainm_ativo_Z = deserialized.gxTpr_Referenciainm_ativo_Z;
         obj.gxTpr_Referenciainm_areatrabalhodes_N = deserialized.gxTpr_Referenciainm_areatrabalhodes_N;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "ReferenciaINM_Codigo") )
               {
                  gxTv_SdtReferenciaINM_Referenciainm_codigo = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ReferenciaINM_Descricao") )
               {
                  gxTv_SdtReferenciaINM_Referenciainm_descricao = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ReferenciaINM_AreaTrabalhoCod") )
               {
                  gxTv_SdtReferenciaINM_Referenciainm_areatrabalhocod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ReferenciaINM_AreaTrabalhoDes") )
               {
                  gxTv_SdtReferenciaINM_Referenciainm_areatrabalhodes = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ReferenciaINM_Ativo") )
               {
                  gxTv_SdtReferenciaINM_Referenciainm_ativo = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Anexos") )
               {
                  if ( gxTv_SdtReferenciaINM_Anexos == null )
                  {
                     gxTv_SdtReferenciaINM_Anexos = new GxSilentTrnGridCollection( context, "ReferenciaINM.Anexos", "GxEv3Up14_MeetrikaVs3", "SdtReferenciaINM_Anexos", "GeneXus.Programs");
                  }
                  if ( ( oReader.IsSimple == 0 ) || ( oReader.AttributeCount > 0 ) )
                  {
                     GXSoapError = gxTv_SdtReferenciaINM_Anexos.readxml(oReader, "Anexos");
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Mode") )
               {
                  gxTv_SdtReferenciaINM_Mode = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Initialized") )
               {
                  gxTv_SdtReferenciaINM_Initialized = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ReferenciaINM_Codigo_Z") )
               {
                  gxTv_SdtReferenciaINM_Referenciainm_codigo_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ReferenciaINM_Descricao_Z") )
               {
                  gxTv_SdtReferenciaINM_Referenciainm_descricao_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ReferenciaINM_AreaTrabalhoCod_Z") )
               {
                  gxTv_SdtReferenciaINM_Referenciainm_areatrabalhocod_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ReferenciaINM_AreaTrabalhoDes_Z") )
               {
                  gxTv_SdtReferenciaINM_Referenciainm_areatrabalhodes_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ReferenciaINM_Ativo_Z") )
               {
                  gxTv_SdtReferenciaINM_Referenciainm_ativo_Z = (bool)(Convert.ToBoolean(((StringUtil.StrCmp(oReader.Value, "true")==0)||(StringUtil.StrCmp(oReader.Value, "1")==0) ? 1 : 0)));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "ReferenciaINM_AreaTrabalhoDes_N") )
               {
                  gxTv_SdtReferenciaINM_Referenciainm_areatrabalhodes_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "ReferenciaINM";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sNameSpace)) )
         {
            sNameSpace = "GxEv3Up14_MeetrikaVs3";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("ReferenciaINM_Codigo", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtReferenciaINM_Referenciainm_codigo), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ReferenciaINM_Descricao", StringUtil.RTrim( gxTv_SdtReferenciaINM_Referenciainm_descricao));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ReferenciaINM_AreaTrabalhoCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtReferenciaINM_Referenciainm_areatrabalhocod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ReferenciaINM_AreaTrabalhoDes", StringUtil.RTrim( gxTv_SdtReferenciaINM_Referenciainm_areatrabalhodes));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("ReferenciaINM_Ativo", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtReferenciaINM_Referenciainm_ativo)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         if ( sIncludeState )
         {
            if ( gxTv_SdtReferenciaINM_Anexos != null )
            {
               String sNameSpace1 ;
               if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") == 0 )
               {
                  sNameSpace1 = "[*:nosend]" + "GxEv3Up14_MeetrikaVs3";
               }
               else
               {
                  sNameSpace1 = "GxEv3Up14_MeetrikaVs3";
               }
               gxTv_SdtReferenciaINM_Anexos.writexml(oWriter, "Anexos", sNameSpace1);
            }
            oWriter.WriteElement("Mode", StringUtil.RTrim( gxTv_SdtReferenciaINM_Mode));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Initialized", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtReferenciaINM_Initialized), 4, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ReferenciaINM_Codigo_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtReferenciaINM_Referenciainm_codigo_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ReferenciaINM_Descricao_Z", StringUtil.RTrim( gxTv_SdtReferenciaINM_Referenciainm_descricao_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ReferenciaINM_AreaTrabalhoCod_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtReferenciaINM_Referenciainm_areatrabalhocod_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ReferenciaINM_AreaTrabalhoDes_Z", StringUtil.RTrim( gxTv_SdtReferenciaINM_Referenciainm_areatrabalhodes_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ReferenciaINM_Ativo_Z", StringUtil.RTrim( StringUtil.BoolToStr( gxTv_SdtReferenciaINM_Referenciainm_ativo_Z)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("ReferenciaINM_AreaTrabalhoDes_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtReferenciaINM_Referenciainm_areatrabalhodes_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("ReferenciaINM_Codigo", gxTv_SdtReferenciaINM_Referenciainm_codigo, false);
         AddObjectProperty("ReferenciaINM_Descricao", gxTv_SdtReferenciaINM_Referenciainm_descricao, false);
         AddObjectProperty("ReferenciaINM_AreaTrabalhoCod", gxTv_SdtReferenciaINM_Referenciainm_areatrabalhocod, false);
         AddObjectProperty("ReferenciaINM_AreaTrabalhoDes", gxTv_SdtReferenciaINM_Referenciainm_areatrabalhodes, false);
         AddObjectProperty("ReferenciaINM_Ativo", gxTv_SdtReferenciaINM_Referenciainm_ativo, false);
         if ( gxTv_SdtReferenciaINM_Anexos != null )
         {
            AddObjectProperty("Anexos", gxTv_SdtReferenciaINM_Anexos, includeState);
         }
         if ( includeState )
         {
            AddObjectProperty("Mode", gxTv_SdtReferenciaINM_Mode, false);
            AddObjectProperty("Initialized", gxTv_SdtReferenciaINM_Initialized, false);
            AddObjectProperty("ReferenciaINM_Codigo_Z", gxTv_SdtReferenciaINM_Referenciainm_codigo_Z, false);
            AddObjectProperty("ReferenciaINM_Descricao_Z", gxTv_SdtReferenciaINM_Referenciainm_descricao_Z, false);
            AddObjectProperty("ReferenciaINM_AreaTrabalhoCod_Z", gxTv_SdtReferenciaINM_Referenciainm_areatrabalhocod_Z, false);
            AddObjectProperty("ReferenciaINM_AreaTrabalhoDes_Z", gxTv_SdtReferenciaINM_Referenciainm_areatrabalhodes_Z, false);
            AddObjectProperty("ReferenciaINM_Ativo_Z", gxTv_SdtReferenciaINM_Referenciainm_ativo_Z, false);
            AddObjectProperty("ReferenciaINM_AreaTrabalhoDes_N", gxTv_SdtReferenciaINM_Referenciainm_areatrabalhodes_N, false);
         }
         return  ;
      }

      [  SoapElement( ElementName = "ReferenciaINM_Codigo" )]
      [  XmlElement( ElementName = "ReferenciaINM_Codigo"   )]
      public int gxTpr_Referenciainm_codigo
      {
         get {
            return gxTv_SdtReferenciaINM_Referenciainm_codigo ;
         }

         set {
            if ( gxTv_SdtReferenciaINM_Referenciainm_codigo != value )
            {
               gxTv_SdtReferenciaINM_Mode = "INS";
               this.gxTv_SdtReferenciaINM_Referenciainm_codigo_Z_SetNull( );
               this.gxTv_SdtReferenciaINM_Referenciainm_descricao_Z_SetNull( );
               this.gxTv_SdtReferenciaINM_Referenciainm_areatrabalhocod_Z_SetNull( );
               this.gxTv_SdtReferenciaINM_Referenciainm_areatrabalhodes_Z_SetNull( );
               this.gxTv_SdtReferenciaINM_Referenciainm_ativo_Z_SetNull( );
               if ( gxTv_SdtReferenciaINM_Anexos != null )
               {
                  GxSilentTrnGridCollection collectionAnexos = gxTv_SdtReferenciaINM_Anexos ;
                  SdtReferenciaINM_Anexos currItemAnexos ;
                  short idx = 1 ;
                  while ( idx <= collectionAnexos.Count )
                  {
                     currItemAnexos = ((SdtReferenciaINM_Anexos)collectionAnexos.Item(idx));
                     currItemAnexos.gxTpr_Mode = "INS";
                     currItemAnexos.gxTpr_Modified = 1;
                     idx = (short)(idx+1);
                  }
               }
            }
            gxTv_SdtReferenciaINM_Referenciainm_codigo = (int)(value);
         }

      }

      [  SoapElement( ElementName = "ReferenciaINM_Descricao" )]
      [  XmlElement( ElementName = "ReferenciaINM_Descricao"   )]
      public String gxTpr_Referenciainm_descricao
      {
         get {
            return gxTv_SdtReferenciaINM_Referenciainm_descricao ;
         }

         set {
            gxTv_SdtReferenciaINM_Referenciainm_descricao = (String)(value);
         }

      }

      [  SoapElement( ElementName = "ReferenciaINM_AreaTrabalhoCod" )]
      [  XmlElement( ElementName = "ReferenciaINM_AreaTrabalhoCod"   )]
      public int gxTpr_Referenciainm_areatrabalhocod
      {
         get {
            return gxTv_SdtReferenciaINM_Referenciainm_areatrabalhocod ;
         }

         set {
            gxTv_SdtReferenciaINM_Referenciainm_areatrabalhocod = (int)(value);
         }

      }

      [  SoapElement( ElementName = "ReferenciaINM_AreaTrabalhoDes" )]
      [  XmlElement( ElementName = "ReferenciaINM_AreaTrabalhoDes"   )]
      public String gxTpr_Referenciainm_areatrabalhodes
      {
         get {
            return gxTv_SdtReferenciaINM_Referenciainm_areatrabalhodes ;
         }

         set {
            gxTv_SdtReferenciaINM_Referenciainm_areatrabalhodes_N = 0;
            gxTv_SdtReferenciaINM_Referenciainm_areatrabalhodes = (String)(value);
         }

      }

      public void gxTv_SdtReferenciaINM_Referenciainm_areatrabalhodes_SetNull( )
      {
         gxTv_SdtReferenciaINM_Referenciainm_areatrabalhodes_N = 1;
         gxTv_SdtReferenciaINM_Referenciainm_areatrabalhodes = "";
         return  ;
      }

      public bool gxTv_SdtReferenciaINM_Referenciainm_areatrabalhodes_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ReferenciaINM_Ativo" )]
      [  XmlElement( ElementName = "ReferenciaINM_Ativo"   )]
      public bool gxTpr_Referenciainm_ativo
      {
         get {
            return gxTv_SdtReferenciaINM_Referenciainm_ativo ;
         }

         set {
            gxTv_SdtReferenciaINM_Referenciainm_ativo = value;
         }

      }

      public class gxTv_SdtReferenciaINM_Anexos_SdtReferenciaINM_Anexos_80compatibility:SdtReferenciaINM_Anexos {}
      [  SoapElement( ElementName = "Anexos" )]
      [  XmlArray( ElementName = "Anexos"  )]
      [  XmlArrayItemAttribute( Type= typeof( SdtReferenciaINM_Anexos ), ElementName= "ReferenciaINM.Anexos"  , IsNullable=false)]
      public GxSilentTrnGridCollection gxTpr_Anexos_GxSilentTrnGridCollection
      {
         get {
            if ( gxTv_SdtReferenciaINM_Anexos == null )
            {
               gxTv_SdtReferenciaINM_Anexos = new GxSilentTrnGridCollection( context, "ReferenciaINM.Anexos", "GxEv3Up14_MeetrikaVs3", "SdtReferenciaINM_Anexos", "GeneXus.Programs");
            }
            return (GxSilentTrnGridCollection)gxTv_SdtReferenciaINM_Anexos ;
         }

         set {
            if ( gxTv_SdtReferenciaINM_Anexos == null )
            {
               gxTv_SdtReferenciaINM_Anexos = new GxSilentTrnGridCollection( context, "ReferenciaINM.Anexos", "GxEv3Up14_MeetrikaVs3", "SdtReferenciaINM_Anexos", "GeneXus.Programs");
            }
            gxTv_SdtReferenciaINM_Anexos = (GxSilentTrnGridCollection) value;
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public GxSilentTrnGridCollection gxTpr_Anexos
      {
         get {
            if ( gxTv_SdtReferenciaINM_Anexos == null )
            {
               gxTv_SdtReferenciaINM_Anexos = new GxSilentTrnGridCollection( context, "ReferenciaINM.Anexos", "GxEv3Up14_MeetrikaVs3", "SdtReferenciaINM_Anexos", "GeneXus.Programs");
            }
            return gxTv_SdtReferenciaINM_Anexos ;
         }

         set {
            gxTv_SdtReferenciaINM_Anexos = value;
         }

      }

      public void gxTv_SdtReferenciaINM_Anexos_SetNull( )
      {
         gxTv_SdtReferenciaINM_Anexos = null;
         return  ;
      }

      public bool gxTv_SdtReferenciaINM_Anexos_IsNull( )
      {
         if ( gxTv_SdtReferenciaINM_Anexos == null )
         {
            return true ;
         }
         return false ;
      }

      [  SoapElement( ElementName = "Mode" )]
      [  XmlElement( ElementName = "Mode"   )]
      public String gxTpr_Mode
      {
         get {
            return gxTv_SdtReferenciaINM_Mode ;
         }

         set {
            gxTv_SdtReferenciaINM_Mode = (String)(value);
         }

      }

      public void gxTv_SdtReferenciaINM_Mode_SetNull( )
      {
         gxTv_SdtReferenciaINM_Mode = "";
         return  ;
      }

      public bool gxTv_SdtReferenciaINM_Mode_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Initialized" )]
      [  XmlElement( ElementName = "Initialized"   )]
      public short gxTpr_Initialized
      {
         get {
            return gxTv_SdtReferenciaINM_Initialized ;
         }

         set {
            gxTv_SdtReferenciaINM_Initialized = (short)(value);
         }

      }

      public void gxTv_SdtReferenciaINM_Initialized_SetNull( )
      {
         gxTv_SdtReferenciaINM_Initialized = 0;
         return  ;
      }

      public bool gxTv_SdtReferenciaINM_Initialized_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ReferenciaINM_Codigo_Z" )]
      [  XmlElement( ElementName = "ReferenciaINM_Codigo_Z"   )]
      public int gxTpr_Referenciainm_codigo_Z
      {
         get {
            return gxTv_SdtReferenciaINM_Referenciainm_codigo_Z ;
         }

         set {
            gxTv_SdtReferenciaINM_Referenciainm_codigo_Z = (int)(value);
         }

      }

      public void gxTv_SdtReferenciaINM_Referenciainm_codigo_Z_SetNull( )
      {
         gxTv_SdtReferenciaINM_Referenciainm_codigo_Z = 0;
         return  ;
      }

      public bool gxTv_SdtReferenciaINM_Referenciainm_codigo_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ReferenciaINM_Descricao_Z" )]
      [  XmlElement( ElementName = "ReferenciaINM_Descricao_Z"   )]
      public String gxTpr_Referenciainm_descricao_Z
      {
         get {
            return gxTv_SdtReferenciaINM_Referenciainm_descricao_Z ;
         }

         set {
            gxTv_SdtReferenciaINM_Referenciainm_descricao_Z = (String)(value);
         }

      }

      public void gxTv_SdtReferenciaINM_Referenciainm_descricao_Z_SetNull( )
      {
         gxTv_SdtReferenciaINM_Referenciainm_descricao_Z = "";
         return  ;
      }

      public bool gxTv_SdtReferenciaINM_Referenciainm_descricao_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ReferenciaINM_AreaTrabalhoCod_Z" )]
      [  XmlElement( ElementName = "ReferenciaINM_AreaTrabalhoCod_Z"   )]
      public int gxTpr_Referenciainm_areatrabalhocod_Z
      {
         get {
            return gxTv_SdtReferenciaINM_Referenciainm_areatrabalhocod_Z ;
         }

         set {
            gxTv_SdtReferenciaINM_Referenciainm_areatrabalhocod_Z = (int)(value);
         }

      }

      public void gxTv_SdtReferenciaINM_Referenciainm_areatrabalhocod_Z_SetNull( )
      {
         gxTv_SdtReferenciaINM_Referenciainm_areatrabalhocod_Z = 0;
         return  ;
      }

      public bool gxTv_SdtReferenciaINM_Referenciainm_areatrabalhocod_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ReferenciaINM_AreaTrabalhoDes_Z" )]
      [  XmlElement( ElementName = "ReferenciaINM_AreaTrabalhoDes_Z"   )]
      public String gxTpr_Referenciainm_areatrabalhodes_Z
      {
         get {
            return gxTv_SdtReferenciaINM_Referenciainm_areatrabalhodes_Z ;
         }

         set {
            gxTv_SdtReferenciaINM_Referenciainm_areatrabalhodes_Z = (String)(value);
         }

      }

      public void gxTv_SdtReferenciaINM_Referenciainm_areatrabalhodes_Z_SetNull( )
      {
         gxTv_SdtReferenciaINM_Referenciainm_areatrabalhodes_Z = "";
         return  ;
      }

      public bool gxTv_SdtReferenciaINM_Referenciainm_areatrabalhodes_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ReferenciaINM_Ativo_Z" )]
      [  XmlElement( ElementName = "ReferenciaINM_Ativo_Z"   )]
      public bool gxTpr_Referenciainm_ativo_Z
      {
         get {
            return gxTv_SdtReferenciaINM_Referenciainm_ativo_Z ;
         }

         set {
            gxTv_SdtReferenciaINM_Referenciainm_ativo_Z = value;
         }

      }

      public void gxTv_SdtReferenciaINM_Referenciainm_ativo_Z_SetNull( )
      {
         gxTv_SdtReferenciaINM_Referenciainm_ativo_Z = false;
         return  ;
      }

      public bool gxTv_SdtReferenciaINM_Referenciainm_ativo_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ReferenciaINM_AreaTrabalhoDes_N" )]
      [  XmlElement( ElementName = "ReferenciaINM_AreaTrabalhoDes_N"   )]
      public short gxTpr_Referenciainm_areatrabalhodes_N
      {
         get {
            return gxTv_SdtReferenciaINM_Referenciainm_areatrabalhodes_N ;
         }

         set {
            gxTv_SdtReferenciaINM_Referenciainm_areatrabalhodes_N = (short)(value);
         }

      }

      public void gxTv_SdtReferenciaINM_Referenciainm_areatrabalhodes_N_SetNull( )
      {
         gxTv_SdtReferenciaINM_Referenciainm_areatrabalhodes_N = 0;
         return  ;
      }

      public bool gxTv_SdtReferenciaINM_Referenciainm_areatrabalhodes_N_IsNull( )
      {
         return false ;
      }

      public void initialize( )
      {
         gxTv_SdtReferenciaINM_Referenciainm_descricao = "";
         gxTv_SdtReferenciaINM_Referenciainm_areatrabalhodes = "";
         gxTv_SdtReferenciaINM_Mode = "";
         gxTv_SdtReferenciaINM_Referenciainm_descricao_Z = "";
         gxTv_SdtReferenciaINM_Referenciainm_areatrabalhodes_Z = "";
         gxTv_SdtReferenciaINM_Referenciainm_ativo = true;
         sTagName = "";
         IGxSilentTrn obj ;
         obj = (IGxSilentTrn)ClassLoader.FindInstance( "referenciainm", "GeneXus.Programs.referenciainm_bc", new Object[] {context}, constructorCallingAssembly);
         obj.initialize();
         obj.SetSDT(this, 1);
         setTransaction( obj) ;
         obj.SetMode("INS");
         return  ;
      }

      private short gxTv_SdtReferenciaINM_Initialized ;
      private short gxTv_SdtReferenciaINM_Referenciainm_areatrabalhodes_N ;
      private short readOk ;
      private short nOutParmCount ;
      private int gxTv_SdtReferenciaINM_Referenciainm_codigo ;
      private int gxTv_SdtReferenciaINM_Referenciainm_areatrabalhocod ;
      private int gxTv_SdtReferenciaINM_Referenciainm_codigo_Z ;
      private int gxTv_SdtReferenciaINM_Referenciainm_areatrabalhocod_Z ;
      private String gxTv_SdtReferenciaINM_Mode ;
      private String sTagName ;
      private bool gxTv_SdtReferenciaINM_Referenciainm_ativo ;
      private bool gxTv_SdtReferenciaINM_Referenciainm_ativo_Z ;
      private String gxTv_SdtReferenciaINM_Referenciainm_descricao ;
      private String gxTv_SdtReferenciaINM_Referenciainm_areatrabalhodes ;
      private String gxTv_SdtReferenciaINM_Referenciainm_descricao_Z ;
      private String gxTv_SdtReferenciaINM_Referenciainm_areatrabalhodes_Z ;
      private Assembly constructorCallingAssembly ;
      [ObjectCollection(ItemType=typeof( SdtReferenciaINM_Anexos ))]
      private GxSilentTrnGridCollection gxTv_SdtReferenciaINM_Anexos=null ;
   }

   [DataContract(Name = @"ReferenciaINM", Namespace = "GxEv3Up14_MeetrikaVs3")]
   public class SdtReferenciaINM_RESTInterface : GxGenericCollectionItem<SdtReferenciaINM>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtReferenciaINM_RESTInterface( ) : base()
      {
      }

      public SdtReferenciaINM_RESTInterface( SdtReferenciaINM psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "ReferenciaINM_Codigo" , Order = 0 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Referenciainm_codigo
      {
         get {
            return sdt.gxTpr_Referenciainm_codigo ;
         }

         set {
            sdt.gxTpr_Referenciainm_codigo = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ReferenciaINM_Descricao" , Order = 1 )]
      [GxSeudo()]
      public String gxTpr_Referenciainm_descricao
      {
         get {
            return sdt.gxTpr_Referenciainm_descricao ;
         }

         set {
            sdt.gxTpr_Referenciainm_descricao = (String)(value);
         }

      }

      [DataMember( Name = "ReferenciaINM_AreaTrabalhoCod" , Order = 2 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Referenciainm_areatrabalhocod
      {
         get {
            return sdt.gxTpr_Referenciainm_areatrabalhocod ;
         }

         set {
            sdt.gxTpr_Referenciainm_areatrabalhocod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ReferenciaINM_AreaTrabalhoDes" , Order = 3 )]
      [GxSeudo()]
      public String gxTpr_Referenciainm_areatrabalhodes
      {
         get {
            return sdt.gxTpr_Referenciainm_areatrabalhodes ;
         }

         set {
            sdt.gxTpr_Referenciainm_areatrabalhodes = (String)(value);
         }

      }

      [DataMember( Name = "ReferenciaINM_Ativo" , Order = 4 )]
      [GxSeudo()]
      public bool gxTpr_Referenciainm_ativo
      {
         get {
            return sdt.gxTpr_Referenciainm_ativo ;
         }

         set {
            sdt.gxTpr_Referenciainm_ativo = value;
         }

      }

      [DataMember( Name = "Anexos" , Order = 5 )]
      public GxGenericCollection<SdtReferenciaINM_Anexos_RESTInterface> gxTpr_Anexos
      {
         get {
            return new GxGenericCollection<SdtReferenciaINM_Anexos_RESTInterface>(sdt.gxTpr_Anexos) ;
         }

         set {
            value.LoadCollection(sdt.gxTpr_Anexos);
         }

      }

      public SdtReferenciaINM sdt
      {
         get {
            return (SdtReferenciaINM)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtReferenciaINM() ;
         }
      }

      [DataMember( Name = "gx_md5_hash", Order = 14 )]
      public string Hash
      {
         get {
            if ( StringUtil.StrCmp(md5Hash, null) == 0 )
            {
               md5Hash = (String)(getHash());
            }
            return md5Hash ;
         }

         set {
            md5Hash = value ;
         }

      }

      private String md5Hash ;
   }

}
