/*
               File: WWPerfil
        Description:  Perfil
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/4/2020 0:18:44.16
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wwperfil : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wwperfil( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wwperfil( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavOrderedby = new GXCombobox();
         dynavPerfil_areatrabalhocod = new GXCombobox();
         cmbavDynamicfiltersselector1 = new GXCombobox();
         cmbPerfil_Tipo = new GXCombobox();
         chkPerfil_Ativo = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"vPERFIL_AREATRABALHOCOD") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               GXDLVvPERFIL_AREATRABALHOCOD0N2( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
            {
               nRC_GXsfl_54 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_54_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_54_idx = GetNextPar( );
               edtavBtnassociarmenu_Title = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavBtnassociarmenu_Internalname, "Title", edtavBtnassociarmenu_Title);
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
            {
               subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV13OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
               AV14OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
               AV15DynamicFiltersSelector1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
               AV17Perfil_Nome1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Perfil_Nome1", AV17Perfil_Nome1);
               AV63TFPerfil_Nome = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63TFPerfil_Nome", AV63TFPerfil_Nome);
               AV64TFPerfil_Nome_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64TFPerfil_Nome_Sel", AV64TFPerfil_Nome_Sel);
               AV67TFPerfil_Ativo_Sel = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67TFPerfil_Ativo_Sel", StringUtil.Str( (decimal)(AV67TFPerfil_Ativo_Sel), 1, 0));
               ajax_req_read_hidden_sdt(GetNextPar( ), AV6WWPContext);
               edtavBtnassociarmenu_Title = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavBtnassociarmenu_Internalname, "Title", edtavBtnassociarmenu_Title);
               AV65ddo_Perfil_NomeTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65ddo_Perfil_NomeTitleControlIdToReplace", AV65ddo_Perfil_NomeTitleControlIdToReplace);
               AV68ddo_Perfil_AtivoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV68ddo_Perfil_AtivoTitleControlIdToReplace", AV68ddo_Perfil_AtivoTitleControlIdToReplace);
               AV34Perfil_AreaTrabalhoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34Perfil_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV34Perfil_AreaTrabalhoCod), 6, 0)));
               AV94Pgmname = GetNextPar( );
               ajax_req_read_hidden_sdt(GetNextPar( ), AV10GridState);
               A329Perfil_GamId = (long)(NumberUtil.Val( GetNextPar( ), "."));
               A3Perfil_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               A276Perfil_Ativo = (bool)(BooleanUtil.Val(GetNextPar( )));
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV17Perfil_Nome1, AV63TFPerfil_Nome, AV64TFPerfil_Nome_Sel, AV67TFPerfil_Ativo_Sel, AV6WWPContext, AV65ddo_Perfil_NomeTitleControlIdToReplace, AV68ddo_Perfil_AtivoTitleControlIdToReplace, AV34Perfil_AreaTrabalhoCod, AV94Pgmname, AV10GridState, A329Perfil_GamId, A3Perfil_Codigo, A276Perfil_Ativo) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PA0N2( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            START0N2( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?2020640184435");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/ConfirmPanel/BootstrapConfirmPanelRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wwperfil.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDDSC", StringUtil.BoolToStr( AV14OrderedDsc));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR1", AV15DynamicFiltersSelector1);
         GxWebStd.gx_hidden_field( context, "GXH_vPERFIL_NOME1", StringUtil.RTrim( AV17Perfil_Nome1));
         GxWebStd.gx_hidden_field( context, "GXH_vTFPERFIL_NOME", StringUtil.RTrim( AV63TFPerfil_Nome));
         GxWebStd.gx_hidden_field( context, "GXH_vTFPERFIL_NOME_SEL", StringUtil.RTrim( AV64TFPerfil_Nome_Sel));
         GxWebStd.gx_hidden_field( context, "GXH_vTFPERFIL_ATIVO_SEL", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV67TFPerfil_Ativo_Sel), 1, 0, ",", "")));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_54", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_54), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV71GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV72GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vDDO_TITLESETTINGSICONS", AV69DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vDDO_TITLESETTINGSICONS", AV69DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vPERFIL_NOMETITLEFILTERDATA", AV62Perfil_NomeTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vPERFIL_NOMETITLEFILTERDATA", AV62Perfil_NomeTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vPERFIL_ATIVOTITLEFILTERDATA", AV66Perfil_AtivoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vPERFIL_ATIVOTITLEFILTERDATA", AV66Perfil_AtivoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vWWPCONTEXT", AV6WWPContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vWWPCONTEXT", AV6WWPContext);
         }
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV94Pgmname));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vGRIDSTATE", AV10GridState);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vGRIDSTATE", AV10GridState);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vNOTIFICATIONINFO", AV35NotificationInfo);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vNOTIFICATIONINFO", AV35NotificationInfo);
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, "DVELOP_CONFIRMPANEL_APAGAR_Title", StringUtil.RTrim( Dvelop_confirmpanel_apagar_Title));
         GxWebStd.gx_hidden_field( context, "DVELOP_CONFIRMPANEL_APAGAR_Confirmationtext", StringUtil.RTrim( Dvelop_confirmpanel_apagar_Confirmationtext));
         GxWebStd.gx_hidden_field( context, "DVELOP_CONFIRMPANEL_APAGAR_Yesbuttoncaption", StringUtil.RTrim( Dvelop_confirmpanel_apagar_Yesbuttoncaption));
         GxWebStd.gx_hidden_field( context, "DVELOP_CONFIRMPANEL_APAGAR_Nobuttoncaption", StringUtil.RTrim( Dvelop_confirmpanel_apagar_Nobuttoncaption));
         GxWebStd.gx_hidden_field( context, "DVELOP_CONFIRMPANEL_APAGAR_Cancelbuttoncaption", StringUtil.RTrim( Dvelop_confirmpanel_apagar_Cancelbuttoncaption));
         GxWebStd.gx_hidden_field( context, "DVELOP_CONFIRMPANEL_APAGAR_Yesbuttonposition", StringUtil.RTrim( Dvelop_confirmpanel_apagar_Yesbuttonposition));
         GxWebStd.gx_hidden_field( context, "DVELOP_CONFIRMPANEL_APAGAR_Confirmtype", StringUtil.RTrim( Dvelop_confirmpanel_apagar_Confirmtype));
         GxWebStd.gx_hidden_field( context, "DDO_PERFIL_NOME_Caption", StringUtil.RTrim( Ddo_perfil_nome_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_PERFIL_NOME_Tooltip", StringUtil.RTrim( Ddo_perfil_nome_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_PERFIL_NOME_Cls", StringUtil.RTrim( Ddo_perfil_nome_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_PERFIL_NOME_Filteredtext_set", StringUtil.RTrim( Ddo_perfil_nome_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_PERFIL_NOME_Selectedvalue_set", StringUtil.RTrim( Ddo_perfil_nome_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_PERFIL_NOME_Dropdownoptionstype", StringUtil.RTrim( Ddo_perfil_nome_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_PERFIL_NOME_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_perfil_nome_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_PERFIL_NOME_Includesortasc", StringUtil.BoolToStr( Ddo_perfil_nome_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_PERFIL_NOME_Includesortdsc", StringUtil.BoolToStr( Ddo_perfil_nome_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_PERFIL_NOME_Sortedstatus", StringUtil.RTrim( Ddo_perfil_nome_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_PERFIL_NOME_Includefilter", StringUtil.BoolToStr( Ddo_perfil_nome_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_PERFIL_NOME_Filtertype", StringUtil.RTrim( Ddo_perfil_nome_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_PERFIL_NOME_Filterisrange", StringUtil.BoolToStr( Ddo_perfil_nome_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_PERFIL_NOME_Includedatalist", StringUtil.BoolToStr( Ddo_perfil_nome_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_PERFIL_NOME_Datalisttype", StringUtil.RTrim( Ddo_perfil_nome_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_PERFIL_NOME_Datalistproc", StringUtil.RTrim( Ddo_perfil_nome_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_PERFIL_NOME_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_perfil_nome_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_PERFIL_NOME_Sortasc", StringUtil.RTrim( Ddo_perfil_nome_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_PERFIL_NOME_Sortdsc", StringUtil.RTrim( Ddo_perfil_nome_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_PERFIL_NOME_Loadingdata", StringUtil.RTrim( Ddo_perfil_nome_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_PERFIL_NOME_Cleanfilter", StringUtil.RTrim( Ddo_perfil_nome_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_PERFIL_NOME_Noresultsfound", StringUtil.RTrim( Ddo_perfil_nome_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_PERFIL_NOME_Searchbuttontext", StringUtil.RTrim( Ddo_perfil_nome_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_PERFIL_ATIVO_Caption", StringUtil.RTrim( Ddo_perfil_ativo_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_PERFIL_ATIVO_Tooltip", StringUtil.RTrim( Ddo_perfil_ativo_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_PERFIL_ATIVO_Cls", StringUtil.RTrim( Ddo_perfil_ativo_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_PERFIL_ATIVO_Selectedvalue_set", StringUtil.RTrim( Ddo_perfil_ativo_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_PERFIL_ATIVO_Dropdownoptionstype", StringUtil.RTrim( Ddo_perfil_ativo_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_PERFIL_ATIVO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_perfil_ativo_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_PERFIL_ATIVO_Includesortasc", StringUtil.BoolToStr( Ddo_perfil_ativo_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_PERFIL_ATIVO_Includesortdsc", StringUtil.BoolToStr( Ddo_perfil_ativo_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_PERFIL_ATIVO_Sortedstatus", StringUtil.RTrim( Ddo_perfil_ativo_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_PERFIL_ATIVO_Includefilter", StringUtil.BoolToStr( Ddo_perfil_ativo_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_PERFIL_ATIVO_Includedatalist", StringUtil.BoolToStr( Ddo_perfil_ativo_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_PERFIL_ATIVO_Datalisttype", StringUtil.RTrim( Ddo_perfil_ativo_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_PERFIL_ATIVO_Datalistfixedvalues", StringUtil.RTrim( Ddo_perfil_ativo_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_PERFIL_ATIVO_Sortasc", StringUtil.RTrim( Ddo_perfil_ativo_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_PERFIL_ATIVO_Sortdsc", StringUtil.RTrim( Ddo_perfil_ativo_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_PERFIL_ATIVO_Cleanfilter", StringUtil.RTrim( Ddo_perfil_ativo_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_PERFIL_ATIVO_Searchbuttontext", StringUtil.RTrim( Ddo_perfil_ativo_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "vBTNASSOCIARMENU_Title", StringUtil.RTrim( edtavBtnassociarmenu_Title));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, "DDO_PERFIL_NOME_Activeeventkey", StringUtil.RTrim( Ddo_perfil_nome_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_PERFIL_NOME_Filteredtext_get", StringUtil.RTrim( Ddo_perfil_nome_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_PERFIL_NOME_Selectedvalue_get", StringUtil.RTrim( Ddo_perfil_nome_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_PERFIL_ATIVO_Activeeventkey", StringUtil.RTrim( Ddo_perfil_ativo_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_PERFIL_ATIVO_Selectedvalue_get", StringUtil.RTrim( Ddo_perfil_ativo_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DVELOP_CONFIRMPANEL_APAGAR_Result", StringUtil.RTrim( Dvelop_confirmpanel_apagar_Result));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WE0N2( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVT0N2( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return false ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wwperfil.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "WWPerfil" ;
      }

      public override String GetPgmdesc( )
      {
         return " Perfil" ;
      }

      protected void WB0N0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            wb_table1_2_0N2( true) ;
         }
         else
         {
            wb_table1_2_0N2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_0N2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 68,'',false,'" + sGXsfl_54_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavPerfil_codigo_selected_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV81Perfil_Codigo_Selected), 6, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV81Perfil_Codigo_Selected), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,68);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavPerfil_codigo_selected_Jsonclick, 0, "Attribute", "", "", "", edtavPerfil_codigo_selected_Visible, 1, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWPerfil.htm");
            wb_table2_69_0N2( true) ;
         }
         else
         {
            wb_table2_69_0N2( false) ;
         }
         return  ;
      }

      protected void wb_table2_69_0N2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 74,'',false,'" + sGXsfl_54_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfperfil_nome_Internalname, StringUtil.RTrim( AV63TFPerfil_Nome), StringUtil.RTrim( context.localUtil.Format( AV63TFPerfil_Nome, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,74);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfperfil_nome_Jsonclick, 0, "Attribute", "", "", "", edtavTfperfil_nome_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWPerfil.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 75,'',false,'" + sGXsfl_54_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfperfil_nome_sel_Internalname, StringUtil.RTrim( AV64TFPerfil_Nome_Sel), StringUtil.RTrim( context.localUtil.Format( AV64TFPerfil_Nome_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,75);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfperfil_nome_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfperfil_nome_sel_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWPerfil.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 76,'',false,'" + sGXsfl_54_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfperfil_ativo_sel_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV67TFPerfil_Ativo_Sel), 1, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV67TFPerfil_Ativo_Sel), "9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,76);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfperfil_ativo_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfperfil_ativo_sel_Visible, 1, 0, "text", "", 1, "chr", 1, "row", 1, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWPerfil.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_PERFIL_NOMEContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 78,'',false,'" + sGXsfl_54_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_perfil_nometitlecontrolidtoreplace_Internalname, AV65ddo_Perfil_NomeTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,78);\"", 0, edtavDdo_perfil_nometitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWPerfil.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_PERFIL_ATIVOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 80,'',false,'" + sGXsfl_54_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_perfil_ativotitlecontrolidtoreplace_Internalname, AV68ddo_Perfil_AtivoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,80);\"", 0, edtavDdo_perfil_ativotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWPerfil.htm");
         }
         wbLoad = true;
      }

      protected void START0N2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", " Perfil", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUP0N0( ) ;
      }

      protected void WS0N2( )
      {
         START0N2( ) ;
         EVT0N2( ) ;
      }

      protected void EVT0N2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E110N2 */
                              E110N2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DVELOP_CONFIRMPANEL_APAGAR.CLOSE") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E120N2 */
                              E120N2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_PERFIL_NOME.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E130N2 */
                              E130N2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_PERFIL_ATIVO.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E140N2 */
                              E140N2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VORDEREDBY.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E150N2 */
                              E150N2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOCLEANFILTERS'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E160N2 */
                              E160N2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOINSERT'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E170N2 */
                              E170N2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR1.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E180N2 */
                              E180N2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ONMESSAGE_GX1") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E190N2 */
                              E190N2 ();
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 13), "ONMESSAGE_GX1") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              nGXsfl_54_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_54_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_54_idx), 4, 0)), 4, "0");
                              SubsflControlProps_542( ) ;
                              AV29Update = cgiGet( edtavUpdate_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUpdate_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV29Update)) ? AV90Update_GXI : context.convertURL( context.PathToRelativeUrl( AV29Update))));
                              AV80Apagar = cgiGet( edtavApagar_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavApagar_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV80Apagar)) ? AV91Apagar_GXI : context.convertURL( context.PathToRelativeUrl( AV80Apagar))));
                              AV57Display = cgiGet( edtavDisplay_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDisplay_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV57Display)) ? AV92Display_GXI : context.convertURL( context.PathToRelativeUrl( AV57Display))));
                              A3Perfil_Codigo = (int)(context.localUtil.CToN( cgiGet( edtPerfil_Codigo_Internalname), ",", "."));
                              A4Perfil_Nome = StringUtil.Upper( cgiGet( edtPerfil_Nome_Internalname));
                              cmbPerfil_Tipo.Name = cmbPerfil_Tipo_Internalname;
                              cmbPerfil_Tipo.CurrentValue = cgiGet( cmbPerfil_Tipo_Internalname);
                              A275Perfil_Tipo = (short)(NumberUtil.Val( cgiGet( cmbPerfil_Tipo_Internalname), "."));
                              A276Perfil_Ativo = StringUtil.StrToBool( cgiGet( chkPerfil_Ativo_Internalname));
                              A329Perfil_GamId = (long)(context.localUtil.CToN( cgiGet( edtPerfil_GamId_Internalname), ",", "."));
                              AV59btnAssociarMenu = cgiGet( edtavBtnassociarmenu_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavBtnassociarmenu_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV59btnAssociarMenu)) ? AV93Btnassociarmenu_GXI : context.convertURL( context.PathToRelativeUrl( AV59btnAssociarMenu))));
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E200N2 */
                                    E200N2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E210N2 */
                                    E210N2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E220N2 */
                                    E220N2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ONMESSAGE_GX1") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E190N2 */
                                    E190N2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       /* Set Refresh If Orderedby Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Ordereddsc Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Perfil_nome1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vPERFIL_NOME1"), AV17Perfil_Nome1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfperfil_nome Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFPERFIL_NOME"), AV63TFPerfil_Nome) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfperfil_nome_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFPERFIL_NOME_SEL"), AV64TFPerfil_Nome_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfperfil_ativo_sel Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFPERFIL_ATIVO_SEL"), ",", ".") != Convert.ToDecimal( AV67TFPerfil_Ativo_Sel )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ONMESSAGE_GX1") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E190N2 */
                                    E190N2 ();
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WE0N2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PA0N2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbavOrderedby.Name = "vORDEREDBY";
            cmbavOrderedby.WebTags = "";
            if ( cmbavOrderedby.ItemCount > 0 )
            {
               AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            }
            dynavPerfil_areatrabalhocod.Name = "vPERFIL_AREATRABALHOCOD";
            dynavPerfil_areatrabalhocod.WebTags = "";
            cmbavDynamicfiltersselector1.Name = "vDYNAMICFILTERSSELECTOR1";
            cmbavDynamicfiltersselector1.WebTags = "";
            cmbavDynamicfiltersselector1.addItem("PERFIL_NOME", "Nome", 0);
            if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
            {
               AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            }
            GXCCtl = "PERFIL_TIPO_" + sGXsfl_54_idx;
            cmbPerfil_Tipo.Name = GXCCtl;
            cmbPerfil_Tipo.WebTags = "";
            cmbPerfil_Tipo.addItem("0", "Desconhecido", 0);
            cmbPerfil_Tipo.addItem("1", "Auditor", 0);
            cmbPerfil_Tipo.addItem("2", "Contador", 0);
            cmbPerfil_Tipo.addItem("3", "Contratada", 0);
            cmbPerfil_Tipo.addItem("4", "Contratante", 0);
            cmbPerfil_Tipo.addItem("5", "Financeiro", 0);
            cmbPerfil_Tipo.addItem("6", "Usuario", 0);
            cmbPerfil_Tipo.addItem("10", "Licensiado", 0);
            cmbPerfil_Tipo.addItem("99", "Administrador GAM", 0);
            if ( cmbPerfil_Tipo.ItemCount > 0 )
            {
               A275Perfil_Tipo = (short)(NumberUtil.Val( cmbPerfil_Tipo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A275Perfil_Tipo), 4, 0))), "."));
            }
            GXCCtl = "PERFIL_ATIVO_" + sGXsfl_54_idx;
            chkPerfil_Ativo.Name = GXCCtl;
            chkPerfil_Ativo.WebTags = "";
            chkPerfil_Ativo.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkPerfil_Ativo_Internalname, "TitleCaption", chkPerfil_Ativo.Caption);
            chkPerfil_Ativo.CheckedValue = "false";
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void GXDLVvPERFIL_AREATRABALHOCOD0N2( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLVvPERFIL_AREATRABALHOCOD_data0N2( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXVvPERFIL_AREATRABALHOCOD_html0N2( )
      {
         int gxdynajaxvalue ;
         GXDLVvPERFIL_AREATRABALHOCOD_data0N2( ) ;
         gxdynajaxindex = 1;
         dynavPerfil_areatrabalhocod.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynavPerfil_areatrabalhocod.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
         if ( dynavPerfil_areatrabalhocod.ItemCount > 0 )
         {
            AV34Perfil_AreaTrabalhoCod = (int)(NumberUtil.Val( dynavPerfil_areatrabalhocod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV34Perfil_AreaTrabalhoCod), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34Perfil_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV34Perfil_AreaTrabalhoCod), 6, 0)));
         }
      }

      protected void GXDLVvPERFIL_AREATRABALHOCOD_data0N2( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("(Nenhum)");
         /* Using cursor H000N2 */
         pr_default.execute(0);
         while ( (pr_default.getStatus(0) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(H000N2_A5AreaTrabalho_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(H000N2_A6AreaTrabalho_Descricao[0]);
            pr_default.readNext(0);
         }
         pr_default.close(0);
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_542( ) ;
         while ( nGXsfl_54_idx <= nRC_GXsfl_54 )
         {
            sendrow_542( ) ;
            nGXsfl_54_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_54_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_54_idx+1));
            sGXsfl_54_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_54_idx), 4, 0)), 4, "0");
            SubsflControlProps_542( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV13OrderedBy ,
                                       bool AV14OrderedDsc ,
                                       String AV15DynamicFiltersSelector1 ,
                                       String AV17Perfil_Nome1 ,
                                       String AV63TFPerfil_Nome ,
                                       String AV64TFPerfil_Nome_Sel ,
                                       short AV67TFPerfil_Ativo_Sel ,
                                       wwpbaseobjects.SdtWWPContext AV6WWPContext ,
                                       String AV65ddo_Perfil_NomeTitleControlIdToReplace ,
                                       String AV68ddo_Perfil_AtivoTitleControlIdToReplace ,
                                       int AV34Perfil_AreaTrabalhoCod ,
                                       String AV94Pgmname ,
                                       wwpbaseobjects.SdtWWPGridState AV10GridState ,
                                       long A329Perfil_GamId ,
                                       int A3Perfil_Codigo ,
                                       bool A276Perfil_Ativo )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RF0N2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_PERFIL_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A3Perfil_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "PERFIL_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A3Perfil_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_PERFIL_NOME", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A4Perfil_Nome, "@!"))));
         GxWebStd.gx_hidden_field( context, "PERFIL_NOME", StringUtil.RTrim( A4Perfil_Nome));
         GxWebStd.gx_hidden_field( context, "gxhash_PERFIL_TIPO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A275Perfil_Tipo), "ZZZ9")));
         GxWebStd.gx_hidden_field( context, "PERFIL_TIPO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A275Perfil_Tipo), 4, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_PERFIL_ATIVO", GetSecureSignedToken( "", A276Perfil_Ativo));
         GxWebStd.gx_hidden_field( context, "PERFIL_ATIVO", StringUtil.BoolToStr( A276Perfil_Ativo));
         GxWebStd.gx_hidden_field( context, "gxhash_PERFIL_GAMID", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A329Perfil_GamId), "ZZZZZZZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "PERFIL_GAMID", StringUtil.LTrim( StringUtil.NToC( (decimal)(A329Perfil_GamId), 12, 0, ".", "")));
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavOrderedby.ItemCount > 0 )
         {
            AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         }
         if ( dynavPerfil_areatrabalhocod.ItemCount > 0 )
         {
            AV34Perfil_AreaTrabalhoCod = (int)(NumberUtil.Val( dynavPerfil_areatrabalhocod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV34Perfil_AreaTrabalhoCod), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34Perfil_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV34Perfil_AreaTrabalhoCod), 6, 0)));
         }
         if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
         {
            AV15DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RF0N2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV94Pgmname = "WWPerfil";
         context.Gx_err = 0;
      }

      protected void RF0N2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 54;
         /* Execute user event: E210N2 */
         E210N2 ();
         nGXsfl_54_idx = 1;
         sGXsfl_54_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_54_idx), 4, 0)), 4, "0");
         SubsflControlProps_542( ) ;
         nGXsfl_54_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", "");
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_542( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(1, new Object[]{ new Object[]{
                                                 AV85WWPerfilDS_2_Dynamicfiltersselector1 ,
                                                 AV86WWPerfilDS_3_Perfil_nome1 ,
                                                 AV88WWPerfilDS_5_Tfperfil_nome_sel ,
                                                 AV87WWPerfilDS_4_Tfperfil_nome ,
                                                 AV89WWPerfilDS_6_Tfperfil_ativo_sel ,
                                                 A4Perfil_Nome ,
                                                 A276Perfil_Ativo ,
                                                 AV13OrderedBy ,
                                                 AV14OrderedDsc ,
                                                 A7Perfil_AreaTrabalhoCod ,
                                                 AV6WWPContext.gxTpr_Areatrabalho_codigo },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.INT,
                                                 TypeConstants.INT
                                                 }
            });
            lV86WWPerfilDS_3_Perfil_nome1 = StringUtil.PadR( StringUtil.RTrim( AV86WWPerfilDS_3_Perfil_nome1), 50, "%");
            lV87WWPerfilDS_4_Tfperfil_nome = StringUtil.PadR( StringUtil.RTrim( AV87WWPerfilDS_4_Tfperfil_nome), 50, "%");
            /* Using cursor H000N3 */
            pr_default.execute(1, new Object[] {AV6WWPContext.gxTpr_Areatrabalho_codigo, lV86WWPerfilDS_3_Perfil_nome1, lV87WWPerfilDS_4_Tfperfil_nome, AV88WWPerfilDS_5_Tfperfil_nome_sel, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_54_idx = 1;
            while ( ( (pr_default.getStatus(1) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A7Perfil_AreaTrabalhoCod = H000N3_A7Perfil_AreaTrabalhoCod[0];
               A329Perfil_GamId = H000N3_A329Perfil_GamId[0];
               A276Perfil_Ativo = H000N3_A276Perfil_Ativo[0];
               A275Perfil_Tipo = H000N3_A275Perfil_Tipo[0];
               A4Perfil_Nome = H000N3_A4Perfil_Nome[0];
               A3Perfil_Codigo = H000N3_A3Perfil_Codigo[0];
               /* Execute user event: E220N2 */
               E220N2 ();
               pr_default.readNext(1);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(1) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(1);
            wbEnd = 54;
            WB0N0( ) ;
         }
         nGXsfl_54_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         AV84WWPerfilDS_1_Perfil_areatrabalhocod = AV34Perfil_AreaTrabalhoCod;
         AV85WWPerfilDS_2_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV86WWPerfilDS_3_Perfil_nome1 = AV17Perfil_Nome1;
         AV87WWPerfilDS_4_Tfperfil_nome = AV63TFPerfil_Nome;
         AV88WWPerfilDS_5_Tfperfil_nome_sel = AV64TFPerfil_Nome_Sel;
         AV89WWPerfilDS_6_Tfperfil_ativo_sel = AV67TFPerfil_Ativo_Sel;
         pr_default.dynParam(2, new Object[]{ new Object[]{
                                              AV85WWPerfilDS_2_Dynamicfiltersselector1 ,
                                              AV86WWPerfilDS_3_Perfil_nome1 ,
                                              AV88WWPerfilDS_5_Tfperfil_nome_sel ,
                                              AV87WWPerfilDS_4_Tfperfil_nome ,
                                              AV89WWPerfilDS_6_Tfperfil_ativo_sel ,
                                              A4Perfil_Nome ,
                                              A276Perfil_Ativo ,
                                              AV13OrderedBy ,
                                              AV14OrderedDsc ,
                                              A7Perfil_AreaTrabalhoCod ,
                                              AV6WWPContext.gxTpr_Areatrabalho_codigo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.INT,
                                              TypeConstants.INT
                                              }
         });
         lV86WWPerfilDS_3_Perfil_nome1 = StringUtil.PadR( StringUtil.RTrim( AV86WWPerfilDS_3_Perfil_nome1), 50, "%");
         lV87WWPerfilDS_4_Tfperfil_nome = StringUtil.PadR( StringUtil.RTrim( AV87WWPerfilDS_4_Tfperfil_nome), 50, "%");
         /* Using cursor H000N4 */
         pr_default.execute(2, new Object[] {AV6WWPContext.gxTpr_Areatrabalho_codigo, lV86WWPerfilDS_3_Perfil_nome1, lV87WWPerfilDS_4_Tfperfil_nome, AV88WWPerfilDS_5_Tfperfil_nome_sel});
         GRID_nRecordCount = H000N4_AGRID_nRecordCount[0];
         pr_default.close(2);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         AV84WWPerfilDS_1_Perfil_areatrabalhocod = AV34Perfil_AreaTrabalhoCod;
         AV85WWPerfilDS_2_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV86WWPerfilDS_3_Perfil_nome1 = AV17Perfil_Nome1;
         AV87WWPerfilDS_4_Tfperfil_nome = AV63TFPerfil_Nome;
         AV88WWPerfilDS_5_Tfperfil_nome_sel = AV64TFPerfil_Nome_Sel;
         AV89WWPerfilDS_6_Tfperfil_ativo_sel = AV67TFPerfil_Ativo_Sel;
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV17Perfil_Nome1, AV63TFPerfil_Nome, AV64TFPerfil_Nome_Sel, AV67TFPerfil_Ativo_Sel, AV6WWPContext, AV65ddo_Perfil_NomeTitleControlIdToReplace, AV68ddo_Perfil_AtivoTitleControlIdToReplace, AV34Perfil_AreaTrabalhoCod, AV94Pgmname, AV10GridState, A329Perfil_GamId, A3Perfil_Codigo, A276Perfil_Ativo) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         AV84WWPerfilDS_1_Perfil_areatrabalhocod = AV34Perfil_AreaTrabalhoCod;
         AV85WWPerfilDS_2_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV86WWPerfilDS_3_Perfil_nome1 = AV17Perfil_Nome1;
         AV87WWPerfilDS_4_Tfperfil_nome = AV63TFPerfil_Nome;
         AV88WWPerfilDS_5_Tfperfil_nome_sel = AV64TFPerfil_Nome_Sel;
         AV89WWPerfilDS_6_Tfperfil_ativo_sel = AV67TFPerfil_Ativo_Sel;
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV17Perfil_Nome1, AV63TFPerfil_Nome, AV64TFPerfil_Nome_Sel, AV67TFPerfil_Ativo_Sel, AV6WWPContext, AV65ddo_Perfil_NomeTitleControlIdToReplace, AV68ddo_Perfil_AtivoTitleControlIdToReplace, AV34Perfil_AreaTrabalhoCod, AV94Pgmname, AV10GridState, A329Perfil_GamId, A3Perfil_Codigo, A276Perfil_Ativo) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         AV84WWPerfilDS_1_Perfil_areatrabalhocod = AV34Perfil_AreaTrabalhoCod;
         AV85WWPerfilDS_2_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV86WWPerfilDS_3_Perfil_nome1 = AV17Perfil_Nome1;
         AV87WWPerfilDS_4_Tfperfil_nome = AV63TFPerfil_Nome;
         AV88WWPerfilDS_5_Tfperfil_nome_sel = AV64TFPerfil_Nome_Sel;
         AV89WWPerfilDS_6_Tfperfil_ativo_sel = AV67TFPerfil_Ativo_Sel;
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV17Perfil_Nome1, AV63TFPerfil_Nome, AV64TFPerfil_Nome_Sel, AV67TFPerfil_Ativo_Sel, AV6WWPContext, AV65ddo_Perfil_NomeTitleControlIdToReplace, AV68ddo_Perfil_AtivoTitleControlIdToReplace, AV34Perfil_AreaTrabalhoCod, AV94Pgmname, AV10GridState, A329Perfil_GamId, A3Perfil_Codigo, A276Perfil_Ativo) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         AV84WWPerfilDS_1_Perfil_areatrabalhocod = AV34Perfil_AreaTrabalhoCod;
         AV85WWPerfilDS_2_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV86WWPerfilDS_3_Perfil_nome1 = AV17Perfil_Nome1;
         AV87WWPerfilDS_4_Tfperfil_nome = AV63TFPerfil_Nome;
         AV88WWPerfilDS_5_Tfperfil_nome_sel = AV64TFPerfil_Nome_Sel;
         AV89WWPerfilDS_6_Tfperfil_ativo_sel = AV67TFPerfil_Ativo_Sel;
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV17Perfil_Nome1, AV63TFPerfil_Nome, AV64TFPerfil_Nome_Sel, AV67TFPerfil_Ativo_Sel, AV6WWPContext, AV65ddo_Perfil_NomeTitleControlIdToReplace, AV68ddo_Perfil_AtivoTitleControlIdToReplace, AV34Perfil_AreaTrabalhoCod, AV94Pgmname, AV10GridState, A329Perfil_GamId, A3Perfil_Codigo, A276Perfil_Ativo) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         AV84WWPerfilDS_1_Perfil_areatrabalhocod = AV34Perfil_AreaTrabalhoCod;
         AV85WWPerfilDS_2_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV86WWPerfilDS_3_Perfil_nome1 = AV17Perfil_Nome1;
         AV87WWPerfilDS_4_Tfperfil_nome = AV63TFPerfil_Nome;
         AV88WWPerfilDS_5_Tfperfil_nome_sel = AV64TFPerfil_Nome_Sel;
         AV89WWPerfilDS_6_Tfperfil_ativo_sel = AV67TFPerfil_Ativo_Sel;
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV17Perfil_Nome1, AV63TFPerfil_Nome, AV64TFPerfil_Nome_Sel, AV67TFPerfil_Ativo_Sel, AV6WWPContext, AV65ddo_Perfil_NomeTitleControlIdToReplace, AV68ddo_Perfil_AtivoTitleControlIdToReplace, AV34Perfil_AreaTrabalhoCod, AV94Pgmname, AV10GridState, A329Perfil_GamId, A3Perfil_Codigo, A276Perfil_Ativo) ;
         }
         return (int)(0) ;
      }

      protected void STRUP0N0( )
      {
         /* Before Start, stand alone formulas. */
         AV94Pgmname = "WWPerfil";
         context.Gx_err = 0;
         GXVvPERFIL_AREATRABALHOCOD_html0N2( ) ;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E200N2 */
         E200N2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "vDDO_TITLESETTINGSICONS"), AV69DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( "vPERFIL_NOMETITLEFILTERDATA"), AV62Perfil_NomeTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vPERFIL_ATIVOTITLEFILTERDATA"), AV66Perfil_AtivoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vNOTIFICATIONINFO"), AV35NotificationInfo);
            /* Read variables values. */
            cmbavOrderedby.Name = cmbavOrderedby_Internalname;
            cmbavOrderedby.CurrentValue = cgiGet( cmbavOrderedby_Internalname);
            AV13OrderedBy = (short)(NumberUtil.Val( cgiGet( cmbavOrderedby_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            dynavPerfil_areatrabalhocod.Name = dynavPerfil_areatrabalhocod_Internalname;
            dynavPerfil_areatrabalhocod.CurrentValue = cgiGet( dynavPerfil_areatrabalhocod_Internalname);
            AV34Perfil_AreaTrabalhoCod = (int)(NumberUtil.Val( cgiGet( dynavPerfil_areatrabalhocod_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34Perfil_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV34Perfil_AreaTrabalhoCod), 6, 0)));
            cmbavDynamicfiltersselector1.Name = cmbavDynamicfiltersselector1_Internalname;
            cmbavDynamicfiltersselector1.CurrentValue = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            AV15DynamicFiltersSelector1 = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            AV17Perfil_Nome1 = StringUtil.Upper( cgiGet( edtavPerfil_nome1_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Perfil_Nome1", AV17Perfil_Nome1);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavPerfil_codigo_selected_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavPerfil_codigo_selected_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vPERFIL_CODIGO_SELECTED");
               GX_FocusControl = edtavPerfil_codigo_selected_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV81Perfil_Codigo_Selected = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV81Perfil_Codigo_Selected", StringUtil.LTrim( StringUtil.Str( (decimal)(AV81Perfil_Codigo_Selected), 6, 0)));
            }
            else
            {
               AV81Perfil_Codigo_Selected = (int)(context.localUtil.CToN( cgiGet( edtavPerfil_codigo_selected_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV81Perfil_Codigo_Selected", StringUtil.LTrim( StringUtil.Str( (decimal)(AV81Perfil_Codigo_Selected), 6, 0)));
            }
            AV63TFPerfil_Nome = StringUtil.Upper( cgiGet( edtavTfperfil_nome_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63TFPerfil_Nome", AV63TFPerfil_Nome);
            AV64TFPerfil_Nome_Sel = StringUtil.Upper( cgiGet( edtavTfperfil_nome_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64TFPerfil_Nome_Sel", AV64TFPerfil_Nome_Sel);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfperfil_ativo_sel_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfperfil_ativo_sel_Internalname), ",", ".") > Convert.ToDecimal( 9 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFPERFIL_ATIVO_SEL");
               GX_FocusControl = edtavTfperfil_ativo_sel_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV67TFPerfil_Ativo_Sel = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67TFPerfil_Ativo_Sel", StringUtil.Str( (decimal)(AV67TFPerfil_Ativo_Sel), 1, 0));
            }
            else
            {
               AV67TFPerfil_Ativo_Sel = (short)(context.localUtil.CToN( cgiGet( edtavTfperfil_ativo_sel_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67TFPerfil_Ativo_Sel", StringUtil.Str( (decimal)(AV67TFPerfil_Ativo_Sel), 1, 0));
            }
            AV65ddo_Perfil_NomeTitleControlIdToReplace = cgiGet( edtavDdo_perfil_nometitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65ddo_Perfil_NomeTitleControlIdToReplace", AV65ddo_Perfil_NomeTitleControlIdToReplace);
            AV68ddo_Perfil_AtivoTitleControlIdToReplace = cgiGet( edtavDdo_perfil_ativotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV68ddo_Perfil_AtivoTitleControlIdToReplace", AV68ddo_Perfil_AtivoTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_54 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_54"), ",", "."));
            AV71GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( "vGRIDCURRENTPAGE"), ",", "."));
            AV72GridPageCount = (long)(context.localUtil.CToN( cgiGet( "vGRIDPAGECOUNT"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( "GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( "GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( "GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( "GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( "GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( "GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( "GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( "GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( "GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( "GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( "GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( "GRIDPAGINATIONBAR_Emptygridcaption");
            Dvelop_confirmpanel_apagar_Title = cgiGet( "DVELOP_CONFIRMPANEL_APAGAR_Title");
            Dvelop_confirmpanel_apagar_Confirmationtext = cgiGet( "DVELOP_CONFIRMPANEL_APAGAR_Confirmationtext");
            Dvelop_confirmpanel_apagar_Yesbuttoncaption = cgiGet( "DVELOP_CONFIRMPANEL_APAGAR_Yesbuttoncaption");
            Dvelop_confirmpanel_apagar_Nobuttoncaption = cgiGet( "DVELOP_CONFIRMPANEL_APAGAR_Nobuttoncaption");
            Dvelop_confirmpanel_apagar_Cancelbuttoncaption = cgiGet( "DVELOP_CONFIRMPANEL_APAGAR_Cancelbuttoncaption");
            Dvelop_confirmpanel_apagar_Yesbuttonposition = cgiGet( "DVELOP_CONFIRMPANEL_APAGAR_Yesbuttonposition");
            Dvelop_confirmpanel_apagar_Confirmtype = cgiGet( "DVELOP_CONFIRMPANEL_APAGAR_Confirmtype");
            Ddo_perfil_nome_Caption = cgiGet( "DDO_PERFIL_NOME_Caption");
            Ddo_perfil_nome_Tooltip = cgiGet( "DDO_PERFIL_NOME_Tooltip");
            Ddo_perfil_nome_Cls = cgiGet( "DDO_PERFIL_NOME_Cls");
            Ddo_perfil_nome_Filteredtext_set = cgiGet( "DDO_PERFIL_NOME_Filteredtext_set");
            Ddo_perfil_nome_Selectedvalue_set = cgiGet( "DDO_PERFIL_NOME_Selectedvalue_set");
            Ddo_perfil_nome_Dropdownoptionstype = cgiGet( "DDO_PERFIL_NOME_Dropdownoptionstype");
            Ddo_perfil_nome_Titlecontrolidtoreplace = cgiGet( "DDO_PERFIL_NOME_Titlecontrolidtoreplace");
            Ddo_perfil_nome_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_PERFIL_NOME_Includesortasc"));
            Ddo_perfil_nome_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_PERFIL_NOME_Includesortdsc"));
            Ddo_perfil_nome_Sortedstatus = cgiGet( "DDO_PERFIL_NOME_Sortedstatus");
            Ddo_perfil_nome_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_PERFIL_NOME_Includefilter"));
            Ddo_perfil_nome_Filtertype = cgiGet( "DDO_PERFIL_NOME_Filtertype");
            Ddo_perfil_nome_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_PERFIL_NOME_Filterisrange"));
            Ddo_perfil_nome_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_PERFIL_NOME_Includedatalist"));
            Ddo_perfil_nome_Datalisttype = cgiGet( "DDO_PERFIL_NOME_Datalisttype");
            Ddo_perfil_nome_Datalistproc = cgiGet( "DDO_PERFIL_NOME_Datalistproc");
            Ddo_perfil_nome_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_PERFIL_NOME_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_perfil_nome_Sortasc = cgiGet( "DDO_PERFIL_NOME_Sortasc");
            Ddo_perfil_nome_Sortdsc = cgiGet( "DDO_PERFIL_NOME_Sortdsc");
            Ddo_perfil_nome_Loadingdata = cgiGet( "DDO_PERFIL_NOME_Loadingdata");
            Ddo_perfil_nome_Cleanfilter = cgiGet( "DDO_PERFIL_NOME_Cleanfilter");
            Ddo_perfil_nome_Noresultsfound = cgiGet( "DDO_PERFIL_NOME_Noresultsfound");
            Ddo_perfil_nome_Searchbuttontext = cgiGet( "DDO_PERFIL_NOME_Searchbuttontext");
            Ddo_perfil_ativo_Caption = cgiGet( "DDO_PERFIL_ATIVO_Caption");
            Ddo_perfil_ativo_Tooltip = cgiGet( "DDO_PERFIL_ATIVO_Tooltip");
            Ddo_perfil_ativo_Cls = cgiGet( "DDO_PERFIL_ATIVO_Cls");
            Ddo_perfil_ativo_Selectedvalue_set = cgiGet( "DDO_PERFIL_ATIVO_Selectedvalue_set");
            Ddo_perfil_ativo_Dropdownoptionstype = cgiGet( "DDO_PERFIL_ATIVO_Dropdownoptionstype");
            Ddo_perfil_ativo_Titlecontrolidtoreplace = cgiGet( "DDO_PERFIL_ATIVO_Titlecontrolidtoreplace");
            Ddo_perfil_ativo_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_PERFIL_ATIVO_Includesortasc"));
            Ddo_perfil_ativo_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_PERFIL_ATIVO_Includesortdsc"));
            Ddo_perfil_ativo_Sortedstatus = cgiGet( "DDO_PERFIL_ATIVO_Sortedstatus");
            Ddo_perfil_ativo_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_PERFIL_ATIVO_Includefilter"));
            Ddo_perfil_ativo_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_PERFIL_ATIVO_Includedatalist"));
            Ddo_perfil_ativo_Datalisttype = cgiGet( "DDO_PERFIL_ATIVO_Datalisttype");
            Ddo_perfil_ativo_Datalistfixedvalues = cgiGet( "DDO_PERFIL_ATIVO_Datalistfixedvalues");
            Ddo_perfil_ativo_Sortasc = cgiGet( "DDO_PERFIL_ATIVO_Sortasc");
            Ddo_perfil_ativo_Sortdsc = cgiGet( "DDO_PERFIL_ATIVO_Sortdsc");
            Ddo_perfil_ativo_Cleanfilter = cgiGet( "DDO_PERFIL_ATIVO_Cleanfilter");
            Ddo_perfil_ativo_Searchbuttontext = cgiGet( "DDO_PERFIL_ATIVO_Searchbuttontext");
            Gridpaginationbar_Selectedpage = cgiGet( "GRIDPAGINATIONBAR_Selectedpage");
            Ddo_perfil_nome_Activeeventkey = cgiGet( "DDO_PERFIL_NOME_Activeeventkey");
            Ddo_perfil_nome_Filteredtext_get = cgiGet( "DDO_PERFIL_NOME_Filteredtext_get");
            Ddo_perfil_nome_Selectedvalue_get = cgiGet( "DDO_PERFIL_NOME_Selectedvalue_get");
            Ddo_perfil_ativo_Activeeventkey = cgiGet( "DDO_PERFIL_ATIVO_Activeeventkey");
            Ddo_perfil_ativo_Selectedvalue_get = cgiGet( "DDO_PERFIL_ATIVO_Selectedvalue_get");
            Dvelop_confirmpanel_apagar_Result = cgiGet( "DVELOP_CONFIRMPANEL_APAGAR_Result");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV15DynamicFiltersSelector1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vPERFIL_NOME1"), AV17Perfil_Nome1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFPERFIL_NOME"), AV63TFPerfil_Nome) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFPERFIL_NOME_SEL"), AV64TFPerfil_Nome_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFPERFIL_ATIVO_SEL"), ",", ".") != Convert.ToDecimal( AV67TFPerfil_Ativo_Sel )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E200N2 */
         E200N2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E200N2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         edtavPerfil_codigo_selected_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPerfil_codigo_selected_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPerfil_codigo_selected_Visible), 5, 0)));
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         AV15DynamicFiltersSelector1 = "PERFIL_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtavTfperfil_nome_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfperfil_nome_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfperfil_nome_Visible), 5, 0)));
         edtavTfperfil_nome_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfperfil_nome_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfperfil_nome_sel_Visible), 5, 0)));
         edtavTfperfil_ativo_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfperfil_ativo_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfperfil_ativo_sel_Visible), 5, 0)));
         Ddo_perfil_nome_Titlecontrolidtoreplace = subGrid_Internalname+"_Perfil_Nome";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_perfil_nome_Internalname, "TitleControlIdToReplace", Ddo_perfil_nome_Titlecontrolidtoreplace);
         AV65ddo_Perfil_NomeTitleControlIdToReplace = Ddo_perfil_nome_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65ddo_Perfil_NomeTitleControlIdToReplace", AV65ddo_Perfil_NomeTitleControlIdToReplace);
         edtavDdo_perfil_nometitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_perfil_nometitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_perfil_nometitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_perfil_ativo_Titlecontrolidtoreplace = subGrid_Internalname+"_Perfil_Ativo";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_perfil_ativo_Internalname, "TitleControlIdToReplace", Ddo_perfil_ativo_Titlecontrolidtoreplace);
         AV68ddo_Perfil_AtivoTitleControlIdToReplace = Ddo_perfil_ativo_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV68ddo_Perfil_AtivoTitleControlIdToReplace", AV68ddo_Perfil_AtivoTitleControlIdToReplace);
         edtavDdo_perfil_ativotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_perfil_ativotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_perfil_ativotitlecontrolidtoreplace_Visible), 5, 0)));
         Form.Caption = "Perfi(s)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavOrderedby.removeAllItems();
         cmbavOrderedby.addItem("1", "Nome", 0);
         cmbavOrderedby.addItem("2", "Ativo", 0);
         if ( AV13OrderedBy < 1 )
         {
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
            S142 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = AV69DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1) ;
         AV69DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1;
         edtavBtnassociarmenu_Title = "Menus";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavBtnassociarmenu_Internalname, "Title", edtavBtnassociarmenu_Title);
      }

      protected void E210N2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV62Perfil_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV66Perfil_AtivoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'CHECKSECURITYFORACTIONS' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtPerfil_Nome_Titleformat = 2;
         edtPerfil_Nome_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Nome", AV65ddo_Perfil_NomeTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtPerfil_Nome_Internalname, "Title", edtPerfil_Nome_Title);
         chkPerfil_Ativo_Titleformat = 2;
         chkPerfil_Ativo.Title.Text = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Ativo", AV68ddo_Perfil_AtivoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkPerfil_Ativo_Internalname, "Title", chkPerfil_Ativo.Title.Text);
         AV71GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV71GridCurrentPage), 10, 0)));
         AV72GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV72GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV72GridPageCount), 10, 0)));
         edtavUpdate_Visible = (AV6WWPContext.gxTpr_Userehadministradorgam&&AV6WWPContext.gxTpr_Update ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUpdate_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUpdate_Visible), 5, 0)));
         edtavApagar_Visible = (AV6WWPContext.gxTpr_Userehadministradorgam&&AV6WWPContext.gxTpr_Delete ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavApagar_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavApagar_Visible), 5, 0)));
         edtavDisplay_Visible = (AV6WWPContext.gxTpr_Userehadministradorgam&&AV6WWPContext.gxTpr_Display ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDisplay_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDisplay_Visible), 5, 0)));
         AV84WWPerfilDS_1_Perfil_areatrabalhocod = AV34Perfil_AreaTrabalhoCod;
         AV85WWPerfilDS_2_Dynamicfiltersselector1 = AV15DynamicFiltersSelector1;
         AV86WWPerfilDS_3_Perfil_nome1 = AV17Perfil_Nome1;
         AV87WWPerfilDS_4_Tfperfil_nome = AV63TFPerfil_Nome;
         AV88WWPerfilDS_5_Tfperfil_nome_sel = AV64TFPerfil_Nome_Sel;
         AV89WWPerfilDS_6_Tfperfil_ativo_sel = AV67TFPerfil_Ativo_Sel;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV62Perfil_NomeTitleFilterData", AV62Perfil_NomeTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV66Perfil_AtivoTitleFilterData", AV66Perfil_AtivoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV6WWPContext", AV6WWPContext);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
      }

      protected void E110N2( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV70PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV70PageToGo) ;
         }
      }

      protected void E130N2( )
      {
         /* Ddo_perfil_nome_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_perfil_nome_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_perfil_nome_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_perfil_nome_Internalname, "SortedStatus", Ddo_perfil_nome_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_perfil_nome_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_perfil_nome_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_perfil_nome_Internalname, "SortedStatus", Ddo_perfil_nome_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_perfil_nome_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV63TFPerfil_Nome = Ddo_perfil_nome_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63TFPerfil_Nome", AV63TFPerfil_Nome);
            AV64TFPerfil_Nome_Sel = Ddo_perfil_nome_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64TFPerfil_Nome_Sel", AV64TFPerfil_Nome_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E140N2( )
      {
         /* Ddo_perfil_ativo_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_perfil_ativo_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_perfil_ativo_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_perfil_ativo_Internalname, "SortedStatus", Ddo_perfil_ativo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_perfil_ativo_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_perfil_ativo_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_perfil_ativo_Internalname, "SortedStatus", Ddo_perfil_ativo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_perfil_ativo_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV67TFPerfil_Ativo_Sel = (short)(NumberUtil.Val( Ddo_perfil_ativo_Selectedvalue_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67TFPerfil_Ativo_Sel", StringUtil.Str( (decimal)(AV67TFPerfil_Ativo_Sel), 1, 0));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      private void E220N2( )
      {
         /* Grid_Load Routine */
         edtavUpdate_Tooltiptext = "Modifica";
         if ( AV6WWPContext.gxTpr_Update )
         {
            edtavUpdate_Link = formatLink("wp_novoperfil.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A329Perfil_GamId) + "," + UrlEncode("" +A3Perfil_Codigo);
            AV29Update = context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavUpdate_Internalname, AV29Update);
            AV90Update_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( )));
            edtavUpdate_Enabled = 1;
         }
         else
         {
            edtavUpdate_Link = "";
            AV29Update = context.GetImagePath( "0211baea-a1e2-4bb5-849c-f3afb69b09ee", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavUpdate_Internalname, AV29Update);
            AV90Update_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "0211baea-a1e2-4bb5-849c-f3afb69b09ee", "", context.GetTheme( )));
            edtavUpdate_Enabled = 0;
         }
         AV80Apagar = context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavApagar_Internalname, AV80Apagar);
         AV91Apagar_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )));
         edtavApagar_Tooltiptext = "";
         edtavDisplay_Tooltiptext = "Mostrar";
         if ( AV6WWPContext.gxTpr_Display )
         {
            edtavDisplay_Link = formatLink("viewperfil.aspx") + "?" + UrlEncode("" +A3Perfil_Codigo) + "," + UrlEncode(StringUtil.RTrim(""));
            AV57Display = context.GetImagePath( "f11923b6-6acd-4a79-bfc0-0cfc6f3bced5", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDisplay_Internalname, AV57Display);
            AV92Display_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "f11923b6-6acd-4a79-bfc0-0cfc6f3bced5", "", context.GetTheme( )));
            edtavDisplay_Enabled = 1;
         }
         else
         {
            edtavDisplay_Link = "";
            AV57Display = context.GetImagePath( "52c6f766-90b4-4f77-87a4-fb602a2ea1b6", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDisplay_Internalname, AV57Display);
            AV92Display_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "52c6f766-90b4-4f77-87a4-fb602a2ea1b6", "", context.GetTheme( )));
            edtavDisplay_Enabled = 0;
         }
         AV59btnAssociarMenu = context.GetImagePath( "6591e2a3-49b6-43b7-b8e3-a292564a32a4", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavBtnassociarmenu_Internalname, AV59btnAssociarMenu);
         AV93Btnassociarmenu_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "6591e2a3-49b6-43b7-b8e3-a292564a32a4", "", context.GetTheme( )));
         edtavBtnassociarmenu_Tooltiptext = "Clique aqui p/ Associar os Menu deste Perfil";
         if ( A276Perfil_Ativo )
         {
            AV79Color = GXUtil.RGB( 0, 0, 0);
         }
         else
         {
            AV79Color = GXUtil.RGB( 255, 0, 0);
         }
         edtPerfil_Nome_Forecolor = (int)(AV79Color);
         if ( ! AV6WWPContext.gxTpr_Userehadministradorgam )
         {
            edtavUpdate_Tooltiptext = "Seu usu�rio n�o tem permiss�o para essa a��o!";
            edtavApagar_Tooltiptext = "Seu usu�rio n�o tem permiss�o para essa a��o!";
            edtavDisplay_Tooltiptext = "Seu usu�rio n�o tem permiss�o para essa a��o!";
         }
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 54;
         }
         sendrow_542( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_54_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(54, GridRow);
         }
      }

      protected void E150N2( )
      {
         /* Orderedby_Click Routine */
         context.DoAjaxRefresh();
      }

      protected void E180N2( )
      {
         /* Dynamicfiltersselector1_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E160N2( )
      {
         /* 'DoCleanFilters' Routine */
         /* Execute user subroutine: 'CLEANFILTERS' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_firstpage( ) ;
         context.DoAjaxRefresh();
         dynavPerfil_areatrabalhocod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV34Perfil_AreaTrabalhoCod), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavPerfil_areatrabalhocod_Internalname, "Values", dynavPerfil_areatrabalhocod.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
      }

      protected void E120N2( )
      {
         /* Dvelop_confirmpanel_apagar_Close Routine */
         if ( StringUtil.StrCmp(Dvelop_confirmpanel_apagar_Result, "Yes") == 0 )
         {
            /* Execute user subroutine: 'DO APAGAR' */
            S192 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
      }

      protected void E170N2( )
      {
         /* 'DoInsert' Routine */
         context.wjLoc = formatLink("wp_novoperfil.aspx") + "?" + UrlEncode(StringUtil.RTrim("INS")) + "," + UrlEncode("" +0) + "," + UrlEncode("" +0);
         context.wjLocDisableFrm = 1;
      }

      protected void S172( )
      {
         /* 'RESETDDOSORTEDSTATUS' Routine */
         Ddo_perfil_nome_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_perfil_nome_Internalname, "SortedStatus", Ddo_perfil_nome_Sortedstatus);
         Ddo_perfil_ativo_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_perfil_ativo_Internalname, "SortedStatus", Ddo_perfil_ativo_Sortedstatus);
      }

      protected void S142( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV13OrderedBy == 1 )
         {
            Ddo_perfil_nome_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_perfil_nome_Internalname, "SortedStatus", Ddo_perfil_nome_Sortedstatus);
         }
         else if ( AV13OrderedBy == 2 )
         {
            Ddo_perfil_ativo_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_perfil_ativo_Internalname, "SortedStatus", Ddo_perfil_ativo_Sortedstatus);
         }
      }

      protected void S152( )
      {
         /* 'CHECKSECURITYFORACTIONS' Routine */
         if ( ! ( AV6WWPContext.gxTpr_Insert ) )
         {
            imgInsert_Link = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgInsert_Internalname, "Link", imgInsert_Link);
            imgInsert_Bitmap = context.GetImagePath( "13d28f37-c579-4dd9-a404-ba51ab71d1e3", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgInsert_Internalname, "Bitmap", context.convertURL( context.PathToRelativeUrl( imgInsert_Bitmap)));
            imgInsert_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgInsert_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgInsert_Enabled), 5, 0)));
         }
      }

      protected void S112( )
      {
         /* 'ENABLEDYNAMICFILTERS1' Routine */
         edtavPerfil_nome1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPerfil_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPerfil_nome1_Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "PERFIL_NOME") == 0 )
         {
            edtavPerfil_nome1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPerfil_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPerfil_nome1_Visible), 5, 0)));
         }
      }

      protected void S182( )
      {
         /* 'CLEANFILTERS' Routine */
         AV34Perfil_AreaTrabalhoCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34Perfil_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV34Perfil_AreaTrabalhoCod), 6, 0)));
         AV63TFPerfil_Nome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63TFPerfil_Nome", AV63TFPerfil_Nome);
         Ddo_perfil_nome_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_perfil_nome_Internalname, "FilteredText_set", Ddo_perfil_nome_Filteredtext_set);
         AV64TFPerfil_Nome_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64TFPerfil_Nome_Sel", AV64TFPerfil_Nome_Sel);
         Ddo_perfil_nome_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_perfil_nome_Internalname, "SelectedValue_set", Ddo_perfil_nome_Selectedvalue_set);
         AV67TFPerfil_Ativo_Sel = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67TFPerfil_Ativo_Sel", StringUtil.Str( (decimal)(AV67TFPerfil_Ativo_Sel), 1, 0));
         Ddo_perfil_ativo_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_perfil_ativo_Internalname, "SelectedValue_set", Ddo_perfil_ativo_Selectedvalue_set);
         AV15DynamicFiltersSelector1 = "PERFIL_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
         AV17Perfil_Nome1 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Perfil_Nome1", AV17Perfil_Nome1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         context.DoAjaxRefresh();
      }

      protected void S192( )
      {
         /* 'DO APAGAR' Routine */
         new prc_dltperfil(context ).execute( ref  AV81Perfil_Codigo_Selected) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV81Perfil_Codigo_Selected", StringUtil.LTrim( StringUtil.Str( (decimal)(AV81Perfil_Codigo_Selected), 6, 0)));
         context.DoAjaxRefresh();
      }

      protected void S132( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV32Session.Get(AV94Pgmname+"GridState"), "") == 0 )
         {
            AV10GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  AV94Pgmname+"GridState"), "");
         }
         else
         {
            AV10GridState.FromXml(AV32Session.Get(AV94Pgmname+"GridState"), "");
         }
         AV13OrderedBy = AV10GridState.gxTpr_Orderedby;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         AV14OrderedDsc = AV10GridState.gxTpr_Ordereddsc;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
         /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADREGFILTERSSTATE' */
         S222 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_gotopage( AV10GridState.gxTpr_Currentpage) ;
      }

      protected void S222( )
      {
         /* 'LOADREGFILTERSSTATE' Routine */
         AV95GXV1 = 1;
         while ( AV95GXV1 <= AV10GridState.gxTpr_Filtervalues.Count )
         {
            AV11GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV10GridState.gxTpr_Filtervalues.Item(AV95GXV1));
            if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "PERFIL_AREATRABALHOCOD") == 0 )
            {
               AV34Perfil_AreaTrabalhoCod = (int)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34Perfil_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV34Perfil_AreaTrabalhoCod), 6, 0)));
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFPERFIL_NOME") == 0 )
            {
               AV63TFPerfil_Nome = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63TFPerfil_Nome", AV63TFPerfil_Nome);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV63TFPerfil_Nome)) )
               {
                  Ddo_perfil_nome_Filteredtext_set = AV63TFPerfil_Nome;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_perfil_nome_Internalname, "FilteredText_set", Ddo_perfil_nome_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFPERFIL_NOME_SEL") == 0 )
            {
               AV64TFPerfil_Nome_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64TFPerfil_Nome_Sel", AV64TFPerfil_Nome_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV64TFPerfil_Nome_Sel)) )
               {
                  Ddo_perfil_nome_Selectedvalue_set = AV64TFPerfil_Nome_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_perfil_nome_Internalname, "SelectedValue_set", Ddo_perfil_nome_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFPERFIL_ATIVO_SEL") == 0 )
            {
               AV67TFPerfil_Ativo_Sel = (short)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV67TFPerfil_Ativo_Sel", StringUtil.Str( (decimal)(AV67TFPerfil_Ativo_Sel), 1, 0));
               if ( ! (0==AV67TFPerfil_Ativo_Sel) )
               {
                  Ddo_perfil_ativo_Selectedvalue_set = StringUtil.Str( (decimal)(AV67TFPerfil_Ativo_Sel), 1, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_perfil_ativo_Internalname, "SelectedValue_set", Ddo_perfil_ativo_Selectedvalue_set);
               }
            }
            AV95GXV1 = (int)(AV95GXV1+1);
         }
      }

      protected void S212( )
      {
         /* 'LOADDYNFILTERSSTATE' Routine */
         if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(1));
            AV15DynamicFiltersSelector1 = AV12GridStateDynamicFilter.gxTpr_Selected;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15DynamicFiltersSelector1", AV15DynamicFiltersSelector1);
            if ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "PERFIL_NOME") == 0 )
            {
               AV17Perfil_Nome1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Perfil_Nome1", AV17Perfil_Nome1);
            }
            /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
      }

      protected void S162( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV10GridState.FromXml(AV32Session.Get(AV94Pgmname+"GridState"), "");
         AV10GridState.gxTpr_Orderedby = AV13OrderedBy;
         AV10GridState.gxTpr_Ordereddsc = AV14OrderedDsc;
         AV10GridState.gxTpr_Filtervalues.Clear();
         if ( ! (0==AV34Perfil_AreaTrabalhoCod) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "PERFIL_AREATRABALHOCOD";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV34Perfil_AreaTrabalhoCod), 6, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV63TFPerfil_Nome)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFPERFIL_NOME";
            AV11GridStateFilterValue.gxTpr_Value = AV63TFPerfil_Nome;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV64TFPerfil_Nome_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFPERFIL_NOME_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV64TFPerfil_Nome_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! (0==AV67TFPerfil_Ativo_Sel) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFPERFIL_ATIVO_SEL";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV67TFPerfil_Ativo_Sel), 1, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S232 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Currentpage = (short)(subGrid_Currentpage( ));
         new wwpbaseobjects.savegridstate(context ).execute(  AV94Pgmname+"GridState",  AV10GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_MeetrikaVs3")) ;
      }

      protected void S232( )
      {
         /* 'SAVEDYNFILTERSSTATE' Routine */
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV12GridStateDynamicFilter.gxTpr_Selected = AV15DynamicFiltersSelector1;
         if ( ( StringUtil.StrCmp(AV15DynamicFiltersSelector1, "PERFIL_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV17Perfil_Nome1)) )
         {
            AV12GridStateDynamicFilter.gxTpr_Value = AV17Perfil_Nome1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
         {
            AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
         }
      }

      protected void S122( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8TrnContext.gxTpr_Callerobject = AV94Pgmname;
         AV8TrnContext.gxTpr_Callerondelete = true;
         AV8TrnContext.gxTpr_Callerurl = AV7HTTPRequest.ScriptName+"?"+AV7HTTPRequest.QueryString;
         AV8TrnContext.gxTpr_Transactionname = "Perfil";
         AV32Session.Set("TrnContext", AV8TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_MeetrikaVs3"));
      }

      protected void E190N2( )
      {
         /* Onmessage_gx1 Routine */
         if ( StringUtil.StrCmp(AV35NotificationInfo.gxTpr_Id, "ALTERAR_AREA_TRABALHO") == 0 )
         {
            new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
            AV34Perfil_AreaTrabalhoCod = AV6WWPContext.gxTpr_Areatrabalho_codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34Perfil_AreaTrabalhoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV34Perfil_AreaTrabalhoCod), 6, 0)));
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV15DynamicFiltersSelector1, AV17Perfil_Nome1, AV63TFPerfil_Nome, AV64TFPerfil_Nome_Sel, AV67TFPerfil_Ativo_Sel, AV6WWPContext, AV65ddo_Perfil_NomeTitleControlIdToReplace, AV68ddo_Perfil_AtivoTitleControlIdToReplace, AV34Perfil_AreaTrabalhoCod, AV94Pgmname, AV10GridState, A329Perfil_GamId, A3Perfil_Codigo, A276Perfil_Ativo) ;
         }
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV6WWPContext", AV6WWPContext);
         dynavPerfil_areatrabalhocod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV34Perfil_AreaTrabalhoCod), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavPerfil_areatrabalhocod_Internalname, "Values", dynavPerfil_areatrabalhocod.ToJavascriptSource());
      }

      protected void S202( )
      {
         /* 'RESETDYNFILTERS' Routine */
      }

      protected void wb_table2_69_0N2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabledvelop_confirmpanel_apagar_Internalname, tblTabledvelop_confirmpanel_apagar_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\">") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DVELOP_CONFIRMPANEL_APAGARContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"DVELOP_CONFIRMPANEL_APAGARContainer"+"Body"+"\" style=\"display:none;\">") ;
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_69_0N2e( true) ;
         }
         else
         {
            wb_table2_69_0N2e( false) ;
         }
      }

      protected void wb_table1_2_0N2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_8_0N2( true) ;
         }
         else
         {
            wb_table3_8_0N2( false) ;
         }
         return  ;
      }

      protected void wb_table3_8_0N2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_48_0N2( true) ;
         }
         else
         {
            wb_table4_48_0N2( false) ;
         }
         return  ;
      }

      protected void wb_table4_48_0N2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_0N2e( true) ;
         }
         else
         {
            wb_table1_2_0N2e( false) ;
         }
      }

      protected void wb_table4_48_0N2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table5_51_0N2( true) ;
         }
         else
         {
            wb_table5_51_0N2( false) ;
         }
         return  ;
      }

      protected void wb_table5_51_0N2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_48_0N2e( true) ;
         }
         else
         {
            wb_table4_48_0N2e( false) ;
         }
      }

      protected void wb_table5_51_0N2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"DivS\" data-gxgridid=\"54\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+((edtavUpdate_Visible==0) ? "display:none;" : "")+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+((edtavApagar_Visible==0) ? "display:none;" : "")+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+((edtavDisplay_Visible==0) ? "display:none;" : "")+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "C�digo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtPerfil_Nome_Titleformat == 0 )
               {
                  context.SendWebValue( edtPerfil_Nome_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtPerfil_Nome_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Tipo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( chkPerfil_Ativo_Titleformat == 0 )
               {
                  context.SendWebValue( chkPerfil_Ativo.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( chkPerfil_Ativo.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "ID(Gam)") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( edtavBtnassociarmenu_Title) ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", "");
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV29Update));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavUpdate_Enabled), 5, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavUpdate_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavUpdate_Tooltiptext));
               GridColumn.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavUpdate_Visible), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV80Apagar));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavApagar_Tooltiptext));
               GridColumn.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavApagar_Visible), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV57Display));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavDisplay_Enabled), 5, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavDisplay_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDisplay_Tooltiptext));
               GridColumn.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavDisplay_Visible), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A3Perfil_Codigo), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A4Perfil_Nome));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtPerfil_Nome_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtPerfil_Nome_Titleformat), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Forecolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtPerfil_Nome_Forecolor), 9, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A275Perfil_Tipo), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.BoolToStr( A276Perfil_Ativo));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( chkPerfil_Ativo.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(chkPerfil_Ativo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A329Perfil_GamId), 12, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV59btnAssociarMenu));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtavBtnassociarmenu_Title));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavBtnassociarmenu_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 54 )
         {
            wbEnd = 0;
            nRC_GXsfl_54 = (short)(nGXsfl_54_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_51_0N2e( true) ;
         }
         else
         {
            wb_table5_51_0N2e( false) ;
         }
      }

      protected void wb_table3_8_0N2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableheader_Internalname, tblTableheader_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            wb_table6_11_0N2( true) ;
         }
         else
         {
            wb_table6_11_0N2( false) ;
         }
         return  ;
      }

      protected void wb_table6_11_0N2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table7_23_0N2( true) ;
         }
         else
         {
            wb_table7_23_0N2( false) ;
         }
         return  ;
      }

      protected void wb_table7_23_0N2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_8_0N2e( true) ;
         }
         else
         {
            wb_table3_8_0N2e( false) ;
         }
      }

      protected void wb_table7_23_0N2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "TableFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 26,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgCleanfilters_Internalname, context.GetImagePath( "effedd9a-25cf-4497-952f-5a5ac032b8be", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Limpar filtros", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgCleanfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOCLEANFILTERS\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWPerfil.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblFiltertextperfil_areatrabalhocod_Internalname, "�rea de Trabalho", "", "", lblFiltertextperfil_areatrabalhocod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Invisible", 0, "", 1, 1, 0, "HLP_WWPerfil.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 30,'',false,'" + sGXsfl_54_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynavPerfil_areatrabalhocod, dynavPerfil_areatrabalhocod_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV34Perfil_AreaTrabalhoCod), 6, 0)), 1, dynavPerfil_areatrabalhocod_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Invisible", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,30);\"", "", true, "HLP_WWPerfil.htm");
            dynavPerfil_areatrabalhocod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV34Perfil_AreaTrabalhoCod), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynavPerfil_areatrabalhocod_Internalname, "Values", (String)(dynavPerfil_areatrabalhocod.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table8_32_0N2( true) ;
         }
         else
         {
            wb_table8_32_0N2( false) ;
         }
         return  ;
      }

      protected void wb_table8_32_0N2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_23_0N2e( true) ;
         }
         else
         {
            wb_table7_23_0N2e( false) ;
         }
      }

      protected void wb_table8_32_0N2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable1_Internalname, tblUnnamedtable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table9_35_0N2( true) ;
         }
         else
         {
            wb_table9_35_0N2( false) ;
         }
         return  ;
      }

      protected void wb_table9_35_0N2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table8_32_0N2e( true) ;
         }
         else
         {
            wb_table8_32_0N2e( false) ;
         }
      }

      protected void wb_table9_35_0N2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabledynamicfilters_Internalname, tblTabledynamicfilters_Internalname, "", "TableDynamicFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix1_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWPerfil.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 40,'',false,'" + sGXsfl_54_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector1, cmbavDynamicfiltersselector1_Internalname, StringUtil.RTrim( AV15DynamicFiltersSelector1), 1, cmbavDynamicfiltersselector1_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR1.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,40);\"", "", true, "HLP_WWPerfil.htm");
            cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV15DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", (String)(cmbavDynamicfiltersselector1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle1_Internalname, "valor", "", "", lblDynamicfiltersmiddle1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWPerfil.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 44,'',false,'" + sGXsfl_54_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavPerfil_nome1_Internalname, StringUtil.RTrim( AV17Perfil_Nome1), StringUtil.RTrim( context.localUtil.Format( AV17Perfil_Nome1, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,44);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavPerfil_nome1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavPerfil_nome1_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWPerfil.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table9_35_0N2e( true) ;
         }
         else
         {
            wb_table9_35_0N2e( false) ;
         }
      }

      protected void wb_table6_11_0N2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "Table", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='GridHeaderCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblPerfiltitle_Internalname, "Perfis", "", "", lblPerfiltitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_WWPerfil.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 16,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgInsert_Internalname, imgInsert_Bitmap, "", "", "", context.GetTheme( ), 1, imgInsert_Enabled, "", "Inserir", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgInsert_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOINSERT\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWPerfil.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='OrderedByDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblOrderedtext_Internalname, "Ordenado por", "", "", lblOrderedtext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "OrderedByDescription", 0, "", 1, 1, 0, "HLP_WWPerfil.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\">") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 20,'',false,'" + sGXsfl_54_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavOrderedby, cmbavOrderedby_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)), 1, cmbavOrderedby_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVORDEREDBY.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,20);\"", "", true, "HLP_WWPerfil.htm");
            cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", (String)(cmbavOrderedby.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'" + sGXsfl_54_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV14OrderedDsc), StringUtil.BoolToStr( AV14OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,21);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_WWPerfil.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_11_0N2e( true) ;
         }
         else
         {
            wb_table6_11_0N2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA0N2( ) ;
         WS0N2( ) ;
         WE0N2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?2020640184733");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wwperfil.js", "?2020640184733");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/ConfirmPanel/BootstrapConfirmPanelRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_542( )
      {
         edtavUpdate_Internalname = "vUPDATE_"+sGXsfl_54_idx;
         edtavApagar_Internalname = "vAPAGAR_"+sGXsfl_54_idx;
         edtavDisplay_Internalname = "vDISPLAY_"+sGXsfl_54_idx;
         edtPerfil_Codigo_Internalname = "PERFIL_CODIGO_"+sGXsfl_54_idx;
         edtPerfil_Nome_Internalname = "PERFIL_NOME_"+sGXsfl_54_idx;
         cmbPerfil_Tipo_Internalname = "PERFIL_TIPO_"+sGXsfl_54_idx;
         chkPerfil_Ativo_Internalname = "PERFIL_ATIVO_"+sGXsfl_54_idx;
         edtPerfil_GamId_Internalname = "PERFIL_GAMID_"+sGXsfl_54_idx;
         edtavBtnassociarmenu_Internalname = "vBTNASSOCIARMENU_"+sGXsfl_54_idx;
      }

      protected void SubsflControlProps_fel_542( )
      {
         edtavUpdate_Internalname = "vUPDATE_"+sGXsfl_54_fel_idx;
         edtavApagar_Internalname = "vAPAGAR_"+sGXsfl_54_fel_idx;
         edtavDisplay_Internalname = "vDISPLAY_"+sGXsfl_54_fel_idx;
         edtPerfil_Codigo_Internalname = "PERFIL_CODIGO_"+sGXsfl_54_fel_idx;
         edtPerfil_Nome_Internalname = "PERFIL_NOME_"+sGXsfl_54_fel_idx;
         cmbPerfil_Tipo_Internalname = "PERFIL_TIPO_"+sGXsfl_54_fel_idx;
         chkPerfil_Ativo_Internalname = "PERFIL_ATIVO_"+sGXsfl_54_fel_idx;
         edtPerfil_GamId_Internalname = "PERFIL_GAMID_"+sGXsfl_54_fel_idx;
         edtavBtnassociarmenu_Internalname = "vBTNASSOCIARMENU_"+sGXsfl_54_fel_idx;
      }

      protected void sendrow_542( )
      {
         SubsflControlProps_542( ) ;
         WB0N0( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_54_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_54_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_54_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+((edtavUpdate_Visible==0) ? "display:none;" : "")+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV29Update_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV29Update))&&String.IsNullOrEmpty(StringUtil.RTrim( AV90Update_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV29Update)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavUpdate_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV29Update)) ? AV90Update_GXI : context.PathToRelativeUrl( AV29Update)),(String)edtavUpdate_Link,(String)"",(String)"",context.GetTheme( ),(int)edtavUpdate_Visible,(int)edtavUpdate_Enabled,(String)"",(String)edtavUpdate_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV29Update_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+((edtavApagar_Visible==0) ? "display:none;" : "")+"\">") ;
            }
            /* Active Bitmap Variable */
            TempTags = " " + ((edtavApagar_Enabled!=0)&&(edtavApagar_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 56,'',false,'',54)\"" : " ");
            ClassString = "Image";
            StyleString = "";
            AV80Apagar_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV80Apagar))&&String.IsNullOrEmpty(StringUtil.RTrim( AV91Apagar_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV80Apagar)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavApagar_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV80Apagar)) ? AV91Apagar_GXI : context.PathToRelativeUrl( AV80Apagar)),(String)"",(String)"",(String)"",context.GetTheme( ),(int)edtavApagar_Visible,(short)1,(String)"",(String)edtavApagar_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)7,(String)edtavApagar_Jsonclick,(String)"'"+""+"'"+",false,"+"'"+"e230n2_client"+"'",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)""+TempTags,(String)"",(String)"",(short)1,(bool)AV80Apagar_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+((edtavDisplay_Visible==0) ? "display:none;" : "")+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV57Display_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV57Display))&&String.IsNullOrEmpty(StringUtil.RTrim( AV92Display_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV57Display)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDisplay_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV57Display)) ? AV92Display_GXI : context.PathToRelativeUrl( AV57Display)),(String)edtavDisplay_Link,(String)"",(String)"",context.GetTheme( ),(int)edtavDisplay_Visible,(int)edtavDisplay_Enabled,(String)"",(String)edtavDisplay_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV57Display_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtPerfil_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A3Perfil_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A3Perfil_Codigo), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtPerfil_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)54,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtPerfil_Nome_Internalname,StringUtil.RTrim( A4Perfil_Nome),StringUtil.RTrim( context.localUtil.Format( A4Perfil_Nome, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtPerfil_Nome_Jsonclick,(short)0,(String)"BootstrapAttribute","color:"+context.BuildHTMLColor( edtPerfil_Nome_Forecolor)+";",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)54,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            if ( ( nGXsfl_54_idx == 1 ) && isAjaxCallMode( ) )
            {
               GXCCtl = "PERFIL_TIPO_" + sGXsfl_54_idx;
               cmbPerfil_Tipo.Name = GXCCtl;
               cmbPerfil_Tipo.WebTags = "";
               cmbPerfil_Tipo.addItem("0", "Desconhecido", 0);
               cmbPerfil_Tipo.addItem("1", "Auditor", 0);
               cmbPerfil_Tipo.addItem("2", "Contador", 0);
               cmbPerfil_Tipo.addItem("3", "Contratada", 0);
               cmbPerfil_Tipo.addItem("4", "Contratante", 0);
               cmbPerfil_Tipo.addItem("5", "Financeiro", 0);
               cmbPerfil_Tipo.addItem("6", "Usuario", 0);
               cmbPerfil_Tipo.addItem("10", "Licensiado", 0);
               cmbPerfil_Tipo.addItem("99", "Administrador GAM", 0);
               if ( cmbPerfil_Tipo.ItemCount > 0 )
               {
                  A275Perfil_Tipo = (short)(NumberUtil.Val( cmbPerfil_Tipo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A275Perfil_Tipo), 4, 0))), "."));
               }
            }
            /* ComboBox */
            GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbPerfil_Tipo,(String)cmbPerfil_Tipo_Internalname,StringUtil.Trim( StringUtil.Str( (decimal)(A275Perfil_Tipo), 4, 0)),(short)1,(String)cmbPerfil_Tipo_Jsonclick,(short)0,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"int",(String)"",(short)0,(short)0,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"BootstrapAttribute",(String)"",(String)"",(String)"",(bool)true});
            cmbPerfil_Tipo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A275Perfil_Tipo), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbPerfil_Tipo_Internalname, "Values", (String)(cmbPerfil_Tipo.ToJavascriptSource()));
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Check box */
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            GridRow.AddColumnProperties("checkbox", 1, isAjaxCallMode( ), new Object[] {(String)chkPerfil_Ativo_Internalname,StringUtil.BoolToStr( A276Perfil_Ativo),(String)"",(String)"",(short)-1,(short)0,(String)"true",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)""});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtPerfil_GamId_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A329Perfil_GamId), 12, 0, ",", "")),context.localUtil.Format( (decimal)(A329Perfil_GamId), "ZZZZZZZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtPerfil_GamId_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)12,(short)0,(short)0,(short)54,(short)1,(short)-1,(short)0,(bool)true,(String)"GAMKeyNumLong",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Active Bitmap Variable */
            TempTags = " " + ((edtavBtnassociarmenu_Enabled!=0)&&(edtavBtnassociarmenu_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 63,'',false,'',54)\"" : " ");
            ClassString = "Image";
            StyleString = "";
            AV59btnAssociarMenu_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV59btnAssociarMenu))&&String.IsNullOrEmpty(StringUtil.RTrim( AV93Btnassociarmenu_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV59btnAssociarMenu)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavBtnassociarmenu_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV59btnAssociarMenu)) ? AV93Btnassociarmenu_GXI : context.PathToRelativeUrl( AV59btnAssociarMenu)),(String)"",(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavBtnassociarmenu_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)7,(String)edtavBtnassociarmenu_Jsonclick,(String)"'"+""+"'"+",false,"+"'"+"e240n2_client"+"'",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)""+TempTags,(String)"",(String)"",(short)1,(bool)AV59btnAssociarMenu_IsBlob,(bool)false});
            GxWebStd.gx_hidden_field( context, "gxhash_PERFIL_CODIGO"+"_"+sGXsfl_54_idx, GetSecureSignedToken( sGXsfl_54_idx, context.localUtil.Format( (decimal)(A3Perfil_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_PERFIL_NOME"+"_"+sGXsfl_54_idx, GetSecureSignedToken( sGXsfl_54_idx, StringUtil.RTrim( context.localUtil.Format( A4Perfil_Nome, "@!"))));
            GxWebStd.gx_hidden_field( context, "gxhash_PERFIL_TIPO"+"_"+sGXsfl_54_idx, GetSecureSignedToken( sGXsfl_54_idx, context.localUtil.Format( (decimal)(A275Perfil_Tipo), "ZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_PERFIL_ATIVO"+"_"+sGXsfl_54_idx, GetSecureSignedToken( sGXsfl_54_idx, A276Perfil_Ativo));
            GxWebStd.gx_hidden_field( context, "gxhash_PERFIL_GAMID"+"_"+sGXsfl_54_idx, GetSecureSignedToken( sGXsfl_54_idx, context.localUtil.Format( (decimal)(A329Perfil_GamId), "ZZZZZZZZZZZ9")));
            GridContainer.AddRow(GridRow);
            nGXsfl_54_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_54_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_54_idx+1));
            sGXsfl_54_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_54_idx), 4, 0)), 4, "0");
            SubsflControlProps_542( ) ;
         }
         /* End function sendrow_542 */
      }

      protected void init_default_properties( )
      {
         lblPerfiltitle_Internalname = "PERFILTITLE";
         imgInsert_Internalname = "INSERT";
         lblOrderedtext_Internalname = "ORDEREDTEXT";
         cmbavOrderedby_Internalname = "vORDEREDBY";
         edtavOrdereddsc_Internalname = "vORDEREDDSC";
         tblTableactions_Internalname = "TABLEACTIONS";
         imgCleanfilters_Internalname = "CLEANFILTERS";
         lblFiltertextperfil_areatrabalhocod_Internalname = "FILTERTEXTPERFIL_AREATRABALHOCOD";
         dynavPerfil_areatrabalhocod_Internalname = "vPERFIL_AREATRABALHOCOD";
         lblDynamicfiltersprefix1_Internalname = "DYNAMICFILTERSPREFIX1";
         cmbavDynamicfiltersselector1_Internalname = "vDYNAMICFILTERSSELECTOR1";
         lblDynamicfiltersmiddle1_Internalname = "DYNAMICFILTERSMIDDLE1";
         edtavPerfil_nome1_Internalname = "vPERFIL_NOME1";
         tblTabledynamicfilters_Internalname = "TABLEDYNAMICFILTERS";
         tblUnnamedtable1_Internalname = "UNNAMEDTABLE1";
         tblTablefilters_Internalname = "TABLEFILTERS";
         tblTableheader_Internalname = "TABLEHEADER";
         edtavUpdate_Internalname = "vUPDATE";
         edtavApagar_Internalname = "vAPAGAR";
         edtavDisplay_Internalname = "vDISPLAY";
         edtPerfil_Codigo_Internalname = "PERFIL_CODIGO";
         edtPerfil_Nome_Internalname = "PERFIL_NOME";
         cmbPerfil_Tipo_Internalname = "PERFIL_TIPO";
         chkPerfil_Ativo_Internalname = "PERFIL_ATIVO";
         edtPerfil_GamId_Internalname = "PERFIL_GAMID";
         edtavBtnassociarmenu_Internalname = "vBTNASSOCIARMENU";
         Gridpaginationbar_Internalname = "GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = "GRIDTABLEWITHPAGINATIONBAR";
         tblTablegridheader_Internalname = "TABLEGRIDHEADER";
         tblTablemain_Internalname = "TABLEMAIN";
         Workwithplusutilities1_Internalname = "WORKWITHPLUSUTILITIES1";
         edtavPerfil_codigo_selected_Internalname = "vPERFIL_CODIGO_SELECTED";
         Dvelop_confirmpanel_apagar_Internalname = "DVELOP_CONFIRMPANEL_APAGAR";
         tblTabledvelop_confirmpanel_apagar_Internalname = "TABLEDVELOP_CONFIRMPANEL_APAGAR";
         edtavTfperfil_nome_Internalname = "vTFPERFIL_NOME";
         edtavTfperfil_nome_sel_Internalname = "vTFPERFIL_NOME_SEL";
         edtavTfperfil_ativo_sel_Internalname = "vTFPERFIL_ATIVO_SEL";
         Ddo_perfil_nome_Internalname = "DDO_PERFIL_NOME";
         edtavDdo_perfil_nometitlecontrolidtoreplace_Internalname = "vDDO_PERFIL_NOMETITLECONTROLIDTOREPLACE";
         Ddo_perfil_ativo_Internalname = "DDO_PERFIL_ATIVO";
         edtavDdo_perfil_ativotitlecontrolidtoreplace_Internalname = "vDDO_PERFIL_ATIVOTITLECONTROLIDTOREPLACE";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtavBtnassociarmenu_Jsonclick = "";
         edtavBtnassociarmenu_Visible = -1;
         edtavBtnassociarmenu_Enabled = 1;
         edtPerfil_GamId_Jsonclick = "";
         cmbPerfil_Tipo_Jsonclick = "";
         edtPerfil_Nome_Jsonclick = "";
         edtPerfil_Codigo_Jsonclick = "";
         edtavApagar_Jsonclick = "";
         edtavApagar_Enabled = 1;
         edtavOrdereddsc_Jsonclick = "";
         cmbavOrderedby_Jsonclick = "";
         imgInsert_Enabled = 1;
         imgInsert_Bitmap = (String)(context.GetImagePath( "5649fbb8-8ce0-4810-a5ce-bd649ea83c3a", "", context.GetTheme( )));
         edtavPerfil_nome1_Jsonclick = "";
         cmbavDynamicfiltersselector1_Jsonclick = "";
         dynavPerfil_areatrabalhocod_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtavBtnassociarmenu_Tooltiptext = "Clique aqui p/ Associar os Menu deste Perfil";
         edtPerfil_Nome_Forecolor = (int)(0xFFFFFF);
         edtavDisplay_Tooltiptext = "Mostrar";
         edtavDisplay_Link = "";
         edtavDisplay_Enabled = 1;
         edtavApagar_Tooltiptext = "";
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = "";
         edtavUpdate_Enabled = 1;
         chkPerfil_Ativo_Titleformat = 0;
         edtPerfil_Nome_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         edtavPerfil_nome1_Visible = 1;
         edtavDisplay_Visible = -1;
         edtavApagar_Visible = -1;
         edtavUpdate_Visible = -1;
         chkPerfil_Ativo.Title.Text = "Ativo";
         edtPerfil_Nome_Title = "Nome";
         edtavOrdereddsc_Visible = 1;
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         chkPerfil_Ativo.Caption = "";
         edtavDdo_perfil_ativotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_perfil_nometitlecontrolidtoreplace_Visible = 1;
         edtavTfperfil_ativo_sel_Jsonclick = "";
         edtavTfperfil_ativo_sel_Visible = 1;
         edtavTfperfil_nome_sel_Jsonclick = "";
         edtavTfperfil_nome_sel_Visible = 1;
         edtavTfperfil_nome_Jsonclick = "";
         edtavTfperfil_nome_Visible = 1;
         edtavPerfil_codigo_selected_Jsonclick = "";
         edtavPerfil_codigo_selected_Visible = 1;
         Ddo_perfil_ativo_Searchbuttontext = "Pesquisar";
         Ddo_perfil_ativo_Cleanfilter = "Limpar pesquisa";
         Ddo_perfil_ativo_Sortdsc = "Ordenar de Z � A";
         Ddo_perfil_ativo_Sortasc = "Ordenar de A � Z";
         Ddo_perfil_ativo_Datalistfixedvalues = "1:Marcado,2:Desmarcado";
         Ddo_perfil_ativo_Datalisttype = "FixedValues";
         Ddo_perfil_ativo_Includedatalist = Convert.ToBoolean( -1);
         Ddo_perfil_ativo_Includefilter = Convert.ToBoolean( 0);
         Ddo_perfil_ativo_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_perfil_ativo_Includesortasc = Convert.ToBoolean( -1);
         Ddo_perfil_ativo_Titlecontrolidtoreplace = "";
         Ddo_perfil_ativo_Dropdownoptionstype = "GridTitleSettings";
         Ddo_perfil_ativo_Cls = "ColumnSettings";
         Ddo_perfil_ativo_Tooltip = "Op��es";
         Ddo_perfil_ativo_Caption = "";
         Ddo_perfil_nome_Searchbuttontext = "Pesquisar";
         Ddo_perfil_nome_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_perfil_nome_Cleanfilter = "Limpar pesquisa";
         Ddo_perfil_nome_Loadingdata = "Carregando dados...";
         Ddo_perfil_nome_Sortdsc = "Ordenar de Z � A";
         Ddo_perfil_nome_Sortasc = "Ordenar de A � Z";
         Ddo_perfil_nome_Datalistupdateminimumcharacters = 0;
         Ddo_perfil_nome_Datalistproc = "GetWWPerfilFilterData";
         Ddo_perfil_nome_Datalisttype = "Dynamic";
         Ddo_perfil_nome_Includedatalist = Convert.ToBoolean( -1);
         Ddo_perfil_nome_Filterisrange = Convert.ToBoolean( 0);
         Ddo_perfil_nome_Filtertype = "Character";
         Ddo_perfil_nome_Includefilter = Convert.ToBoolean( -1);
         Ddo_perfil_nome_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_perfil_nome_Includesortasc = Convert.ToBoolean( -1);
         Ddo_perfil_nome_Titlecontrolidtoreplace = "";
         Ddo_perfil_nome_Dropdownoptionstype = "GridTitleSettings";
         Ddo_perfil_nome_Cls = "ColumnSettings";
         Ddo_perfil_nome_Tooltip = "Op��es";
         Ddo_perfil_nome_Caption = "";
         Dvelop_confirmpanel_apagar_Confirmtype = "1";
         Dvelop_confirmpanel_apagar_Yesbuttonposition = "left";
         Dvelop_confirmpanel_apagar_Cancelbuttoncaption = "Cancelar";
         Dvelop_confirmpanel_apagar_Nobuttoncaption = "N�o";
         Dvelop_confirmpanel_apagar_Yesbuttoncaption = "Sim";
         Dvelop_confirmpanel_apagar_Confirmationtext = "Confirma apagar este Perfil desvinculando menus e usu�rios associados?";
         Dvelop_confirmpanel_apagar_Title = "Aten��o";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = " Perfil";
         subGrid_Rows = 0;
         edtavBtnassociarmenu_Title = "";
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'edtavBtnassociarmenu_Title',ctrl:'vBTNASSOCIARMENU',prop:'Title'},{av:'A329Perfil_GamId',fld:'PERFIL_GAMID',pic:'ZZZZZZZZZZZ9',hsh:true,nv:0},{av:'A3Perfil_Codigo',fld:'PERFIL_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A276Perfil_Ativo',fld:'PERFIL_ATIVO',pic:'',hsh:true,nv:false},{av:'AV65ddo_Perfil_NomeTitleControlIdToReplace',fld:'vDDO_PERFIL_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_Perfil_AtivoTitleControlIdToReplace',fld:'vDDO_PERFIL_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV34Perfil_AreaTrabalhoCod',fld:'vPERFIL_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17Perfil_Nome1',fld:'vPERFIL_NOME1',pic:'@!',nv:''},{av:'AV63TFPerfil_Nome',fld:'vTFPERFIL_NOME',pic:'@!',nv:''},{av:'AV64TFPerfil_Nome_Sel',fld:'vTFPERFIL_NOME_SEL',pic:'@!',nv:''},{av:'AV67TFPerfil_Ativo_Sel',fld:'vTFPERFIL_ATIVO_SEL',pic:'9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV94Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null}],oparms:[{av:'AV62Perfil_NomeTitleFilterData',fld:'vPERFIL_NOMETITLEFILTERDATA',pic:'',nv:null},{av:'AV66Perfil_AtivoTitleFilterData',fld:'vPERFIL_ATIVOTITLEFILTERDATA',pic:'',nv:null},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'edtPerfil_Nome_Titleformat',ctrl:'PERFIL_NOME',prop:'Titleformat'},{av:'edtPerfil_Nome_Title',ctrl:'PERFIL_NOME',prop:'Title'},{av:'chkPerfil_Ativo_Titleformat',ctrl:'PERFIL_ATIVO',prop:'Titleformat'},{av:'chkPerfil_Ativo.Title.Text',ctrl:'PERFIL_ATIVO',prop:'Title'},{av:'AV71GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV72GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'edtavUpdate_Visible',ctrl:'vUPDATE',prop:'Visible'},{av:'edtavApagar_Visible',ctrl:'vAPAGAR',prop:'Visible'},{av:'edtavDisplay_Visible',ctrl:'vDISPLAY',prop:'Visible'},{av:'imgInsert_Link',ctrl:'INSERT',prop:'Link'},{av:'imgInsert_Enabled',ctrl:'INSERT',prop:'Enabled'},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E110N2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17Perfil_Nome1',fld:'vPERFIL_NOME1',pic:'@!',nv:''},{av:'AV63TFPerfil_Nome',fld:'vTFPERFIL_NOME',pic:'@!',nv:''},{av:'AV64TFPerfil_Nome_Sel',fld:'vTFPERFIL_NOME_SEL',pic:'@!',nv:''},{av:'AV67TFPerfil_Ativo_Sel',fld:'vTFPERFIL_ATIVO_SEL',pic:'9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'edtavBtnassociarmenu_Title',ctrl:'vBTNASSOCIARMENU',prop:'Title'},{av:'AV65ddo_Perfil_NomeTitleControlIdToReplace',fld:'vDDO_PERFIL_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_Perfil_AtivoTitleControlIdToReplace',fld:'vDDO_PERFIL_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV34Perfil_AreaTrabalhoCod',fld:'vPERFIL_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV94Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'A329Perfil_GamId',fld:'PERFIL_GAMID',pic:'ZZZZZZZZZZZ9',hsh:true,nv:0},{av:'A3Perfil_Codigo',fld:'PERFIL_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A276Perfil_Ativo',fld:'PERFIL_ATIVO',pic:'',hsh:true,nv:false},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("DDO_PERFIL_NOME.ONOPTIONCLICKED","{handler:'E130N2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17Perfil_Nome1',fld:'vPERFIL_NOME1',pic:'@!',nv:''},{av:'AV63TFPerfil_Nome',fld:'vTFPERFIL_NOME',pic:'@!',nv:''},{av:'AV64TFPerfil_Nome_Sel',fld:'vTFPERFIL_NOME_SEL',pic:'@!',nv:''},{av:'AV67TFPerfil_Ativo_Sel',fld:'vTFPERFIL_ATIVO_SEL',pic:'9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'edtavBtnassociarmenu_Title',ctrl:'vBTNASSOCIARMENU',prop:'Title'},{av:'AV65ddo_Perfil_NomeTitleControlIdToReplace',fld:'vDDO_PERFIL_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_Perfil_AtivoTitleControlIdToReplace',fld:'vDDO_PERFIL_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV34Perfil_AreaTrabalhoCod',fld:'vPERFIL_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV94Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'A329Perfil_GamId',fld:'PERFIL_GAMID',pic:'ZZZZZZZZZZZ9',hsh:true,nv:0},{av:'A3Perfil_Codigo',fld:'PERFIL_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A276Perfil_Ativo',fld:'PERFIL_ATIVO',pic:'',hsh:true,nv:false},{av:'Ddo_perfil_nome_Activeeventkey',ctrl:'DDO_PERFIL_NOME',prop:'ActiveEventKey'},{av:'Ddo_perfil_nome_Filteredtext_get',ctrl:'DDO_PERFIL_NOME',prop:'FilteredText_get'},{av:'Ddo_perfil_nome_Selectedvalue_get',ctrl:'DDO_PERFIL_NOME',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_perfil_nome_Sortedstatus',ctrl:'DDO_PERFIL_NOME',prop:'SortedStatus'},{av:'AV63TFPerfil_Nome',fld:'vTFPERFIL_NOME',pic:'@!',nv:''},{av:'AV64TFPerfil_Nome_Sel',fld:'vTFPERFIL_NOME_SEL',pic:'@!',nv:''},{av:'Ddo_perfil_ativo_Sortedstatus',ctrl:'DDO_PERFIL_ATIVO',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_PERFIL_ATIVO.ONOPTIONCLICKED","{handler:'E140N2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17Perfil_Nome1',fld:'vPERFIL_NOME1',pic:'@!',nv:''},{av:'AV63TFPerfil_Nome',fld:'vTFPERFIL_NOME',pic:'@!',nv:''},{av:'AV64TFPerfil_Nome_Sel',fld:'vTFPERFIL_NOME_SEL',pic:'@!',nv:''},{av:'AV67TFPerfil_Ativo_Sel',fld:'vTFPERFIL_ATIVO_SEL',pic:'9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'edtavBtnassociarmenu_Title',ctrl:'vBTNASSOCIARMENU',prop:'Title'},{av:'AV65ddo_Perfil_NomeTitleControlIdToReplace',fld:'vDDO_PERFIL_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_Perfil_AtivoTitleControlIdToReplace',fld:'vDDO_PERFIL_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV34Perfil_AreaTrabalhoCod',fld:'vPERFIL_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV94Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'A329Perfil_GamId',fld:'PERFIL_GAMID',pic:'ZZZZZZZZZZZ9',hsh:true,nv:0},{av:'A3Perfil_Codigo',fld:'PERFIL_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A276Perfil_Ativo',fld:'PERFIL_ATIVO',pic:'',hsh:true,nv:false},{av:'Ddo_perfil_ativo_Activeeventkey',ctrl:'DDO_PERFIL_ATIVO',prop:'ActiveEventKey'},{av:'Ddo_perfil_ativo_Selectedvalue_get',ctrl:'DDO_PERFIL_ATIVO',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_perfil_ativo_Sortedstatus',ctrl:'DDO_PERFIL_ATIVO',prop:'SortedStatus'},{av:'AV67TFPerfil_Ativo_Sel',fld:'vTFPERFIL_ATIVO_SEL',pic:'9',nv:0},{av:'Ddo_perfil_nome_Sortedstatus',ctrl:'DDO_PERFIL_NOME',prop:'SortedStatus'}]}");
         setEventMetadata("GRID.LOAD","{handler:'E220N2',iparms:[{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A329Perfil_GamId',fld:'PERFIL_GAMID',pic:'ZZZZZZZZZZZ9',hsh:true,nv:0},{av:'A3Perfil_Codigo',fld:'PERFIL_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A276Perfil_Ativo',fld:'PERFIL_ATIVO',pic:'',hsh:true,nv:false}],oparms:[{av:'edtavUpdate_Tooltiptext',ctrl:'vUPDATE',prop:'Tooltiptext'},{av:'edtavUpdate_Link',ctrl:'vUPDATE',prop:'Link'},{av:'AV29Update',fld:'vUPDATE',pic:'',nv:''},{av:'edtavUpdate_Enabled',ctrl:'vUPDATE',prop:'Enabled'},{av:'AV80Apagar',fld:'vAPAGAR',pic:'',nv:''},{av:'edtavApagar_Tooltiptext',ctrl:'vAPAGAR',prop:'Tooltiptext'},{av:'edtavDisplay_Tooltiptext',ctrl:'vDISPLAY',prop:'Tooltiptext'},{av:'edtavDisplay_Link',ctrl:'vDISPLAY',prop:'Link'},{av:'AV57Display',fld:'vDISPLAY',pic:'',nv:''},{av:'edtavDisplay_Enabled',ctrl:'vDISPLAY',prop:'Enabled'},{av:'AV59btnAssociarMenu',fld:'vBTNASSOCIARMENU',pic:'',nv:''},{av:'edtavBtnassociarmenu_Tooltiptext',ctrl:'vBTNASSOCIARMENU',prop:'Tooltiptext'},{av:'edtPerfil_Nome_Forecolor',ctrl:'PERFIL_NOME',prop:'Forecolor'}]}");
         setEventMetadata("VORDEREDBY.CLICK","{handler:'E150N2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17Perfil_Nome1',fld:'vPERFIL_NOME1',pic:'@!',nv:''},{av:'AV63TFPerfil_Nome',fld:'vTFPERFIL_NOME',pic:'@!',nv:''},{av:'AV64TFPerfil_Nome_Sel',fld:'vTFPERFIL_NOME_SEL',pic:'@!',nv:''},{av:'AV67TFPerfil_Ativo_Sel',fld:'vTFPERFIL_ATIVO_SEL',pic:'9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'edtavBtnassociarmenu_Title',ctrl:'vBTNASSOCIARMENU',prop:'Title'},{av:'AV65ddo_Perfil_NomeTitleControlIdToReplace',fld:'vDDO_PERFIL_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_Perfil_AtivoTitleControlIdToReplace',fld:'vDDO_PERFIL_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV34Perfil_AreaTrabalhoCod',fld:'vPERFIL_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV94Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'A329Perfil_GamId',fld:'PERFIL_GAMID',pic:'ZZZZZZZZZZZ9',hsh:true,nv:0},{av:'A3Perfil_Codigo',fld:'PERFIL_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A276Perfil_Ativo',fld:'PERFIL_ATIVO',pic:'',hsh:true,nv:false}],oparms:[]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR1.CLICK","{handler:'E180N2',iparms:[{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''}],oparms:[{av:'edtavPerfil_nome1_Visible',ctrl:'vPERFIL_NOME1',prop:'Visible'}]}");
         setEventMetadata("'DOCLEANFILTERS'","{handler:'E160N2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17Perfil_Nome1',fld:'vPERFIL_NOME1',pic:'@!',nv:''},{av:'AV63TFPerfil_Nome',fld:'vTFPERFIL_NOME',pic:'@!',nv:''},{av:'AV64TFPerfil_Nome_Sel',fld:'vTFPERFIL_NOME_SEL',pic:'@!',nv:''},{av:'AV67TFPerfil_Ativo_Sel',fld:'vTFPERFIL_ATIVO_SEL',pic:'9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'edtavBtnassociarmenu_Title',ctrl:'vBTNASSOCIARMENU',prop:'Title'},{av:'AV65ddo_Perfil_NomeTitleControlIdToReplace',fld:'vDDO_PERFIL_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_Perfil_AtivoTitleControlIdToReplace',fld:'vDDO_PERFIL_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV34Perfil_AreaTrabalhoCod',fld:'vPERFIL_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV94Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'A329Perfil_GamId',fld:'PERFIL_GAMID',pic:'ZZZZZZZZZZZ9',hsh:true,nv:0},{av:'A3Perfil_Codigo',fld:'PERFIL_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A276Perfil_Ativo',fld:'PERFIL_ATIVO',pic:'',hsh:true,nv:false}],oparms:[{av:'AV34Perfil_AreaTrabalhoCod',fld:'vPERFIL_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV63TFPerfil_Nome',fld:'vTFPERFIL_NOME',pic:'@!',nv:''},{av:'Ddo_perfil_nome_Filteredtext_set',ctrl:'DDO_PERFIL_NOME',prop:'FilteredText_set'},{av:'AV64TFPerfil_Nome_Sel',fld:'vTFPERFIL_NOME_SEL',pic:'@!',nv:''},{av:'Ddo_perfil_nome_Selectedvalue_set',ctrl:'DDO_PERFIL_NOME',prop:'SelectedValue_set'},{av:'AV67TFPerfil_Ativo_Sel',fld:'vTFPERFIL_ATIVO_SEL',pic:'9',nv:0},{av:'Ddo_perfil_ativo_Selectedvalue_set',ctrl:'DDO_PERFIL_ATIVO',prop:'SelectedValue_set'},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17Perfil_Nome1',fld:'vPERFIL_NOME1',pic:'@!',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'edtavPerfil_nome1_Visible',ctrl:'vPERFIL_NOME1',prop:'Visible'}]}");
         setEventMetadata("'DOAPAGAR'","{handler:'E230N2',iparms:[{av:'A3Perfil_Codigo',fld:'PERFIL_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV81Perfil_Codigo_Selected',fld:'vPERFIL_CODIGO_SELECTED',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("DVELOP_CONFIRMPANEL_APAGAR.CLOSE","{handler:'E120N2',iparms:[{av:'Dvelop_confirmpanel_apagar_Result',ctrl:'DVELOP_CONFIRMPANEL_APAGAR',prop:'Result'},{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17Perfil_Nome1',fld:'vPERFIL_NOME1',pic:'@!',nv:''},{av:'AV63TFPerfil_Nome',fld:'vTFPERFIL_NOME',pic:'@!',nv:''},{av:'AV64TFPerfil_Nome_Sel',fld:'vTFPERFIL_NOME_SEL',pic:'@!',nv:''},{av:'AV67TFPerfil_Ativo_Sel',fld:'vTFPERFIL_ATIVO_SEL',pic:'9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'edtavBtnassociarmenu_Title',ctrl:'vBTNASSOCIARMENU',prop:'Title'},{av:'AV65ddo_Perfil_NomeTitleControlIdToReplace',fld:'vDDO_PERFIL_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_Perfil_AtivoTitleControlIdToReplace',fld:'vDDO_PERFIL_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV34Perfil_AreaTrabalhoCod',fld:'vPERFIL_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV94Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'A329Perfil_GamId',fld:'PERFIL_GAMID',pic:'ZZZZZZZZZZZ9',hsh:true,nv:0},{av:'A3Perfil_Codigo',fld:'PERFIL_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A276Perfil_Ativo',fld:'PERFIL_ATIVO',pic:'',hsh:true,nv:false},{av:'AV81Perfil_Codigo_Selected',fld:'vPERFIL_CODIGO_SELECTED',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV81Perfil_Codigo_Selected',fld:'vPERFIL_CODIGO_SELECTED',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("'DOBTNASSOCIARMENU'","{handler:'E240N2',iparms:[{av:'A3Perfil_Codigo',fld:'PERFIL_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         setEventMetadata("'DOINSERT'","{handler:'E170N2',iparms:[],oparms:[]}");
         setEventMetadata("ONMESSAGE_GX1","{handler:'E190N2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV15DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV17Perfil_Nome1',fld:'vPERFIL_NOME1',pic:'@!',nv:''},{av:'AV63TFPerfil_Nome',fld:'vTFPERFIL_NOME',pic:'@!',nv:''},{av:'AV64TFPerfil_Nome_Sel',fld:'vTFPERFIL_NOME_SEL',pic:'@!',nv:''},{av:'AV67TFPerfil_Ativo_Sel',fld:'vTFPERFIL_ATIVO_SEL',pic:'9',nv:0},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'edtavBtnassociarmenu_Title',ctrl:'vBTNASSOCIARMENU',prop:'Title'},{av:'AV65ddo_Perfil_NomeTitleControlIdToReplace',fld:'vDDO_PERFIL_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68ddo_Perfil_AtivoTitleControlIdToReplace',fld:'vDDO_PERFIL_ATIVOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV34Perfil_AreaTrabalhoCod',fld:'vPERFIL_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0},{av:'AV94Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'A329Perfil_GamId',fld:'PERFIL_GAMID',pic:'ZZZZZZZZZZZ9',hsh:true,nv:0},{av:'A3Perfil_Codigo',fld:'PERFIL_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A276Perfil_Ativo',fld:'PERFIL_ATIVO',pic:'',hsh:true,nv:false},{av:'AV35NotificationInfo',fld:'vNOTIFICATIONINFO',pic:'',nv:null}],oparms:[{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV34Perfil_AreaTrabalhoCod',fld:'vPERFIL_AREATRABALHOCOD',pic:'ZZZZZ9',nv:0}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Gridpaginationbar_Selectedpage = "";
         Ddo_perfil_nome_Activeeventkey = "";
         Ddo_perfil_nome_Filteredtext_get = "";
         Ddo_perfil_nome_Selectedvalue_get = "";
         Ddo_perfil_ativo_Activeeventkey = "";
         Ddo_perfil_ativo_Selectedvalue_get = "";
         Dvelop_confirmpanel_apagar_Result = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV15DynamicFiltersSelector1 = "";
         AV17Perfil_Nome1 = "";
         AV63TFPerfil_Nome = "";
         AV64TFPerfil_Nome_Sel = "";
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV65ddo_Perfil_NomeTitleControlIdToReplace = "";
         AV68ddo_Perfil_AtivoTitleControlIdToReplace = "";
         AV94Pgmname = "";
         AV10GridState = new wwpbaseobjects.SdtWWPGridState(context);
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV69DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV62Perfil_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV66Perfil_AtivoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV35NotificationInfo = new SdtNotificationInfo(context);
         Ddo_perfil_nome_Filteredtext_set = "";
         Ddo_perfil_nome_Selectedvalue_set = "";
         Ddo_perfil_nome_Sortedstatus = "";
         Ddo_perfil_ativo_Selectedvalue_set = "";
         Ddo_perfil_ativo_Sortedstatus = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV29Update = "";
         AV90Update_GXI = "";
         AV80Apagar = "";
         AV91Apagar_GXI = "";
         AV57Display = "";
         AV92Display_GXI = "";
         A4Perfil_Nome = "";
         AV59btnAssociarMenu = "";
         AV93Btnassociarmenu_GXI = "";
         GXCCtl = "";
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         scmdbuf = "";
         H000N2_A5AreaTrabalho_Codigo = new int[1] ;
         H000N2_A6AreaTrabalho_Descricao = new String[] {""} ;
         GridContainer = new GXWebGrid( context);
         lV86WWPerfilDS_3_Perfil_nome1 = "";
         lV87WWPerfilDS_4_Tfperfil_nome = "";
         AV85WWPerfilDS_2_Dynamicfiltersselector1 = "";
         AV86WWPerfilDS_3_Perfil_nome1 = "";
         AV88WWPerfilDS_5_Tfperfil_nome_sel = "";
         AV87WWPerfilDS_4_Tfperfil_nome = "";
         H000N3_A7Perfil_AreaTrabalhoCod = new int[1] ;
         H000N3_A329Perfil_GamId = new long[1] ;
         H000N3_A276Perfil_Ativo = new bool[] {false} ;
         H000N3_A275Perfil_Tipo = new short[1] ;
         H000N3_A4Perfil_Nome = new String[] {""} ;
         H000N3_A3Perfil_Codigo = new int[1] ;
         H000N4_AGRID_nRecordCount = new long[1] ;
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         GridRow = new GXWebRow();
         imgInsert_Link = "";
         AV32Session = context.GetSession();
         AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV7HTTPRequest = new GxHttpRequest( context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         imgCleanfilters_Jsonclick = "";
         lblFiltertextperfil_areatrabalhocod_Jsonclick = "";
         lblDynamicfiltersprefix1_Jsonclick = "";
         lblDynamicfiltersmiddle1_Jsonclick = "";
         lblPerfiltitle_Jsonclick = "";
         imgInsert_Jsonclick = "";
         lblOrderedtext_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wwperfil__default(),
            new Object[][] {
                new Object[] {
               H000N2_A5AreaTrabalho_Codigo, H000N2_A6AreaTrabalho_Descricao
               }
               , new Object[] {
               H000N3_A7Perfil_AreaTrabalhoCod, H000N3_A329Perfil_GamId, H000N3_A276Perfil_Ativo, H000N3_A275Perfil_Tipo, H000N3_A4Perfil_Nome, H000N3_A3Perfil_Codigo
               }
               , new Object[] {
               H000N4_AGRID_nRecordCount
               }
            }
         );
         AV94Pgmname = "WWPerfil";
         /* GeneXus formulas. */
         AV94Pgmname = "WWPerfil";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_54 ;
      private short nGXsfl_54_idx=1 ;
      private short AV13OrderedBy ;
      private short AV67TFPerfil_Ativo_Sel ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short A275Perfil_Tipo ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_54_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short AV89WWPerfilDS_6_Tfperfil_ativo_sel ;
      private short edtPerfil_Nome_Titleformat ;
      private short chkPerfil_Ativo_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int subGrid_Rows ;
      private int AV34Perfil_AreaTrabalhoCod ;
      private int A3Perfil_Codigo ;
      private int Gridpaginationbar_Pagestoshow ;
      private int Ddo_perfil_nome_Datalistupdateminimumcharacters ;
      private int AV81Perfil_Codigo_Selected ;
      private int edtavPerfil_codigo_selected_Visible ;
      private int edtavTfperfil_nome_Visible ;
      private int edtavTfperfil_nome_sel_Visible ;
      private int edtavTfperfil_ativo_sel_Visible ;
      private int edtavDdo_perfil_nometitlecontrolidtoreplace_Visible ;
      private int edtavDdo_perfil_ativotitlecontrolidtoreplace_Visible ;
      private int gxdynajaxindex ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int AV6WWPContext_gxTpr_Areatrabalho_codigo ;
      private int A7Perfil_AreaTrabalhoCod ;
      private int AV84WWPerfilDS_1_Perfil_areatrabalhocod ;
      private int edtavOrdereddsc_Visible ;
      private int edtavUpdate_Visible ;
      private int edtavApagar_Visible ;
      private int edtavDisplay_Visible ;
      private int AV70PageToGo ;
      private int edtavUpdate_Enabled ;
      private int edtavDisplay_Enabled ;
      private int edtPerfil_Nome_Forecolor ;
      private int imgInsert_Enabled ;
      private int edtavPerfil_nome1_Visible ;
      private int AV95GXV1 ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private int edtavApagar_Enabled ;
      private int edtavBtnassociarmenu_Enabled ;
      private int edtavBtnassociarmenu_Visible ;
      private long A329Perfil_GamId ;
      private long GRID_nFirstRecordOnPage ;
      private long AV71GridCurrentPage ;
      private long AV72GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private long AV79Color ;
      private String edtavBtnassociarmenu_Title ;
      private String Gridpaginationbar_Selectedpage ;
      private String Ddo_perfil_nome_Activeeventkey ;
      private String Ddo_perfil_nome_Filteredtext_get ;
      private String Ddo_perfil_nome_Selectedvalue_get ;
      private String Ddo_perfil_ativo_Activeeventkey ;
      private String Ddo_perfil_ativo_Selectedvalue_get ;
      private String Dvelop_confirmpanel_apagar_Result ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_54_idx="0001" ;
      private String edtavBtnassociarmenu_Internalname ;
      private String AV17Perfil_Nome1 ;
      private String AV63TFPerfil_Nome ;
      private String AV64TFPerfil_Nome_Sel ;
      private String AV94Pgmname ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Dvelop_confirmpanel_apagar_Title ;
      private String Dvelop_confirmpanel_apagar_Confirmationtext ;
      private String Dvelop_confirmpanel_apagar_Yesbuttoncaption ;
      private String Dvelop_confirmpanel_apagar_Nobuttoncaption ;
      private String Dvelop_confirmpanel_apagar_Cancelbuttoncaption ;
      private String Dvelop_confirmpanel_apagar_Yesbuttonposition ;
      private String Dvelop_confirmpanel_apagar_Confirmtype ;
      private String Ddo_perfil_nome_Caption ;
      private String Ddo_perfil_nome_Tooltip ;
      private String Ddo_perfil_nome_Cls ;
      private String Ddo_perfil_nome_Filteredtext_set ;
      private String Ddo_perfil_nome_Selectedvalue_set ;
      private String Ddo_perfil_nome_Dropdownoptionstype ;
      private String Ddo_perfil_nome_Titlecontrolidtoreplace ;
      private String Ddo_perfil_nome_Sortedstatus ;
      private String Ddo_perfil_nome_Filtertype ;
      private String Ddo_perfil_nome_Datalisttype ;
      private String Ddo_perfil_nome_Datalistproc ;
      private String Ddo_perfil_nome_Sortasc ;
      private String Ddo_perfil_nome_Sortdsc ;
      private String Ddo_perfil_nome_Loadingdata ;
      private String Ddo_perfil_nome_Cleanfilter ;
      private String Ddo_perfil_nome_Noresultsfound ;
      private String Ddo_perfil_nome_Searchbuttontext ;
      private String Ddo_perfil_ativo_Caption ;
      private String Ddo_perfil_ativo_Tooltip ;
      private String Ddo_perfil_ativo_Cls ;
      private String Ddo_perfil_ativo_Selectedvalue_set ;
      private String Ddo_perfil_ativo_Dropdownoptionstype ;
      private String Ddo_perfil_ativo_Titlecontrolidtoreplace ;
      private String Ddo_perfil_ativo_Sortedstatus ;
      private String Ddo_perfil_ativo_Datalisttype ;
      private String Ddo_perfil_ativo_Datalistfixedvalues ;
      private String Ddo_perfil_ativo_Sortasc ;
      private String Ddo_perfil_ativo_Sortdsc ;
      private String Ddo_perfil_ativo_Cleanfilter ;
      private String Ddo_perfil_ativo_Searchbuttontext ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String TempTags ;
      private String edtavPerfil_codigo_selected_Internalname ;
      private String edtavPerfil_codigo_selected_Jsonclick ;
      private String edtavTfperfil_nome_Internalname ;
      private String edtavTfperfil_nome_Jsonclick ;
      private String edtavTfperfil_nome_sel_Internalname ;
      private String edtavTfperfil_nome_sel_Jsonclick ;
      private String edtavTfperfil_ativo_sel_Internalname ;
      private String edtavTfperfil_ativo_sel_Jsonclick ;
      private String ClassString ;
      private String StyleString ;
      private String edtavDdo_perfil_nometitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_perfil_ativotitlecontrolidtoreplace_Internalname ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavUpdate_Internalname ;
      private String edtavApagar_Internalname ;
      private String edtavDisplay_Internalname ;
      private String edtPerfil_Codigo_Internalname ;
      private String A4Perfil_Nome ;
      private String edtPerfil_Nome_Internalname ;
      private String cmbPerfil_Tipo_Internalname ;
      private String chkPerfil_Ativo_Internalname ;
      private String edtPerfil_GamId_Internalname ;
      private String GXCCtl ;
      private String cmbavOrderedby_Internalname ;
      private String gxwrpcisep ;
      private String scmdbuf ;
      private String lV86WWPerfilDS_3_Perfil_nome1 ;
      private String lV87WWPerfilDS_4_Tfperfil_nome ;
      private String AV86WWPerfilDS_3_Perfil_nome1 ;
      private String AV88WWPerfilDS_5_Tfperfil_nome_sel ;
      private String AV87WWPerfilDS_4_Tfperfil_nome ;
      private String edtavOrdereddsc_Internalname ;
      private String dynavPerfil_areatrabalhocod_Internalname ;
      private String cmbavDynamicfiltersselector1_Internalname ;
      private String edtavPerfil_nome1_Internalname ;
      private String subGrid_Internalname ;
      private String Ddo_perfil_nome_Internalname ;
      private String Ddo_perfil_ativo_Internalname ;
      private String edtPerfil_Nome_Title ;
      private String edtavUpdate_Tooltiptext ;
      private String edtavUpdate_Link ;
      private String edtavApagar_Tooltiptext ;
      private String edtavDisplay_Tooltiptext ;
      private String edtavDisplay_Link ;
      private String edtavBtnassociarmenu_Tooltiptext ;
      private String imgInsert_Link ;
      private String imgInsert_Internalname ;
      private String sStyleString ;
      private String tblTabledvelop_confirmpanel_apagar_Internalname ;
      private String tblTablemain_Internalname ;
      private String tblTablegridheader_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTableheader_Internalname ;
      private String tblTablefilters_Internalname ;
      private String imgCleanfilters_Internalname ;
      private String imgCleanfilters_Jsonclick ;
      private String lblFiltertextperfil_areatrabalhocod_Internalname ;
      private String lblFiltertextperfil_areatrabalhocod_Jsonclick ;
      private String dynavPerfil_areatrabalhocod_Jsonclick ;
      private String tblUnnamedtable1_Internalname ;
      private String tblTabledynamicfilters_Internalname ;
      private String lblDynamicfiltersprefix1_Internalname ;
      private String lblDynamicfiltersprefix1_Jsonclick ;
      private String cmbavDynamicfiltersselector1_Jsonclick ;
      private String lblDynamicfiltersmiddle1_Internalname ;
      private String lblDynamicfiltersmiddle1_Jsonclick ;
      private String edtavPerfil_nome1_Jsonclick ;
      private String tblTableactions_Internalname ;
      private String lblPerfiltitle_Internalname ;
      private String lblPerfiltitle_Jsonclick ;
      private String imgInsert_Jsonclick ;
      private String lblOrderedtext_Internalname ;
      private String lblOrderedtext_Jsonclick ;
      private String cmbavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Jsonclick ;
      private String sGXsfl_54_fel_idx="0001" ;
      private String edtavApagar_Jsonclick ;
      private String ROClassString ;
      private String edtPerfil_Codigo_Jsonclick ;
      private String edtPerfil_Nome_Jsonclick ;
      private String cmbPerfil_Tipo_Jsonclick ;
      private String edtPerfil_GamId_Jsonclick ;
      private String edtavBtnassociarmenu_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private String Dvelop_confirmpanel_apagar_Internalname ;
      private bool entryPointCalled ;
      private bool AV14OrderedDsc ;
      private bool A276Perfil_Ativo ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool Ddo_perfil_nome_Includesortasc ;
      private bool Ddo_perfil_nome_Includesortdsc ;
      private bool Ddo_perfil_nome_Includefilter ;
      private bool Ddo_perfil_nome_Filterisrange ;
      private bool Ddo_perfil_nome_Includedatalist ;
      private bool Ddo_perfil_ativo_Includesortasc ;
      private bool Ddo_perfil_ativo_Includesortdsc ;
      private bool Ddo_perfil_ativo_Includefilter ;
      private bool Ddo_perfil_ativo_Includedatalist ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV29Update_IsBlob ;
      private bool AV80Apagar_IsBlob ;
      private bool AV57Display_IsBlob ;
      private bool AV59btnAssociarMenu_IsBlob ;
      private String AV15DynamicFiltersSelector1 ;
      private String AV65ddo_Perfil_NomeTitleControlIdToReplace ;
      private String AV68ddo_Perfil_AtivoTitleControlIdToReplace ;
      private String AV90Update_GXI ;
      private String AV91Apagar_GXI ;
      private String AV92Display_GXI ;
      private String AV93Btnassociarmenu_GXI ;
      private String AV85WWPerfilDS_2_Dynamicfiltersselector1 ;
      private String AV29Update ;
      private String AV80Apagar ;
      private String AV57Display ;
      private String AV59btnAssociarMenu ;
      private String imgInsert_Bitmap ;
      private IGxSession AV32Session ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbavOrderedby ;
      private GXCombobox dynavPerfil_areatrabalhocod ;
      private GXCombobox cmbavDynamicfiltersselector1 ;
      private GXCombobox cmbPerfil_Tipo ;
      private GXCheckbox chkPerfil_Ativo ;
      private IDataStoreProvider pr_default ;
      private int[] H000N2_A5AreaTrabalho_Codigo ;
      private String[] H000N2_A6AreaTrabalho_Descricao ;
      private int[] H000N3_A7Perfil_AreaTrabalhoCod ;
      private long[] H000N3_A329Perfil_GamId ;
      private bool[] H000N3_A276Perfil_Ativo ;
      private short[] H000N3_A275Perfil_Tipo ;
      private String[] H000N3_A4Perfil_Nome ;
      private int[] H000N3_A3Perfil_Codigo ;
      private long[] H000N4_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV7HTTPRequest ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV62Perfil_NomeTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV66Perfil_AtivoTitleFilterData ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV69DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 ;
      private wwpbaseobjects.SdtWWPGridState AV10GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV11GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV12GridStateDynamicFilter ;
      private SdtNotificationInfo AV35NotificationInfo ;
      private wwpbaseobjects.SdtWWPTransactionContext AV8TrnContext ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
   }

   public class wwperfil__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H000N3( IGxContext context ,
                                             String AV85WWPerfilDS_2_Dynamicfiltersselector1 ,
                                             String AV86WWPerfilDS_3_Perfil_nome1 ,
                                             String AV88WWPerfilDS_5_Tfperfil_nome_sel ,
                                             String AV87WWPerfilDS_4_Tfperfil_nome ,
                                             short AV89WWPerfilDS_6_Tfperfil_ativo_sel ,
                                             String A4Perfil_Nome ,
                                             bool A276Perfil_Ativo ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc ,
                                             int A7Perfil_AreaTrabalhoCod ,
                                             int AV6WWPContext_gxTpr_Areatrabalho_codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [9] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " [Perfil_AreaTrabalhoCod], [Perfil_GamId], [Perfil_Ativo], [Perfil_Tipo], [Perfil_Nome], [Perfil_Codigo]";
         sFromString = " FROM [Perfil] WITH (NOLOCK)";
         sOrderString = "";
         sWhereString = sWhereString + " WHERE ([Perfil_AreaTrabalhoCod] = @AV6WWPCo_1Areatrabalho_codigo)";
         if ( ( StringUtil.StrCmp(AV85WWPerfilDS_2_Dynamicfiltersselector1, "PERFIL_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV86WWPerfilDS_3_Perfil_nome1)) ) )
         {
            sWhereString = sWhereString + " and ([Perfil_Nome] like '%' + @lV86WWPerfilDS_3_Perfil_nome1)";
         }
         else
         {
            GXv_int2[1] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV88WWPerfilDS_5_Tfperfil_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV87WWPerfilDS_4_Tfperfil_nome)) ) )
         {
            sWhereString = sWhereString + " and ([Perfil_Nome] like @lV87WWPerfilDS_4_Tfperfil_nome)";
         }
         else
         {
            GXv_int2[2] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV88WWPerfilDS_5_Tfperfil_nome_sel)) )
         {
            sWhereString = sWhereString + " and ([Perfil_Nome] = @AV88WWPerfilDS_5_Tfperfil_nome_sel)";
         }
         else
         {
            GXv_int2[3] = 1;
         }
         if ( AV89WWPerfilDS_6_Tfperfil_ativo_sel == 1 )
         {
            sWhereString = sWhereString + " and ([Perfil_Ativo] = 1)";
         }
         if ( AV89WWPerfilDS_6_Tfperfil_ativo_sel == 2 )
         {
            sWhereString = sWhereString + " and ([Perfil_Ativo] = 0)";
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [Perfil_Nome]";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [Perfil_Nome] DESC";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY [Perfil_Ativo]";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY [Perfil_Ativo] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY [Perfil_Codigo]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      protected Object[] conditional_H000N4( IGxContext context ,
                                             String AV85WWPerfilDS_2_Dynamicfiltersselector1 ,
                                             String AV86WWPerfilDS_3_Perfil_nome1 ,
                                             String AV88WWPerfilDS_5_Tfperfil_nome_sel ,
                                             String AV87WWPerfilDS_4_Tfperfil_nome ,
                                             short AV89WWPerfilDS_6_Tfperfil_ativo_sel ,
                                             String A4Perfil_Nome ,
                                             bool A276Perfil_Ativo ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc ,
                                             int A7Perfil_AreaTrabalhoCod ,
                                             int AV6WWPContext_gxTpr_Areatrabalho_codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [4] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM [Perfil] WITH (NOLOCK)";
         scmdbuf = scmdbuf + " WHERE ([Perfil_AreaTrabalhoCod] = @AV6WWPCo_1Areatrabalho_codigo)";
         if ( ( StringUtil.StrCmp(AV85WWPerfilDS_2_Dynamicfiltersselector1, "PERFIL_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV86WWPerfilDS_3_Perfil_nome1)) ) )
         {
            sWhereString = sWhereString + " and ([Perfil_Nome] like '%' + @lV86WWPerfilDS_3_Perfil_nome1)";
         }
         else
         {
            GXv_int4[1] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV88WWPerfilDS_5_Tfperfil_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV87WWPerfilDS_4_Tfperfil_nome)) ) )
         {
            sWhereString = sWhereString + " and ([Perfil_Nome] like @lV87WWPerfilDS_4_Tfperfil_nome)";
         }
         else
         {
            GXv_int4[2] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV88WWPerfilDS_5_Tfperfil_nome_sel)) )
         {
            sWhereString = sWhereString + " and ([Perfil_Nome] = @AV88WWPerfilDS_5_Tfperfil_nome_sel)";
         }
         else
         {
            GXv_int4[3] = 1;
         }
         if ( AV89WWPerfilDS_6_Tfperfil_ativo_sel == 1 )
         {
            sWhereString = sWhereString + " and ([Perfil_Ativo] = 1)";
         }
         if ( AV89WWPerfilDS_6_Tfperfil_ativo_sel == 2 )
         {
            sWhereString = sWhereString + " and ([Perfil_Ativo] = 0)";
         }
         scmdbuf = scmdbuf + sWhereString;
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 1 :
                     return conditional_H000N3(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (short)dynConstraints[4] , (String)dynConstraints[5] , (bool)dynConstraints[6] , (short)dynConstraints[7] , (bool)dynConstraints[8] , (int)dynConstraints[9] , (int)dynConstraints[10] );
               case 2 :
                     return conditional_H000N4(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (short)dynConstraints[4] , (String)dynConstraints[5] , (bool)dynConstraints[6] , (short)dynConstraints[7] , (bool)dynConstraints[8] , (int)dynConstraints[9] , (int)dynConstraints[10] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH000N2 ;
          prmH000N2 = new Object[] {
          } ;
          Object[] prmH000N3 ;
          prmH000N3 = new Object[] {
          new Object[] {"@AV6WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV86WWPerfilDS_3_Perfil_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV87WWPerfilDS_4_Tfperfil_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV88WWPerfilDS_5_Tfperfil_nome_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH000N4 ;
          prmH000N4 = new Object[] {
          new Object[] {"@AV6WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV86WWPerfilDS_3_Perfil_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV87WWPerfilDS_4_Tfperfil_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV88WWPerfilDS_5_Tfperfil_nome_sel",SqlDbType.Char,50,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H000N2", "SELECT [AreaTrabalho_Codigo], [AreaTrabalho_Descricao] FROM [AreaTrabalho] WITH (NOLOCK) ORDER BY [AreaTrabalho_Descricao] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH000N2,0,0,true,false )
             ,new CursorDef("H000N3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH000N3,11,0,true,false )
             ,new CursorDef("H000N4", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH000N4,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((long[]) buf[1])[0] = rslt.getLong(2) ;
                ((bool[]) buf[2])[0] = rslt.getBool(3) ;
                ((short[]) buf[3])[0] = rslt.getShort(4) ;
                ((String[]) buf[4])[0] = rslt.getString(5, 50) ;
                ((int[]) buf[5])[0] = rslt.getInt(6) ;
                return;
             case 2 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[9]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[10]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[11]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[13]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[14]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[15]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[16]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[17]);
                }
                return;
             case 2 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[4]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[5]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[6]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[7]);
                }
                return;
       }
    }

 }

}
