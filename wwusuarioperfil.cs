/*
               File: WWUsuarioPerfil
        Description:  Usuario x Perfil
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/29/2020 9:35:39.42
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wwusuarioperfil : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wwusuarioperfil( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wwusuarioperfil( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavOrderedby = new GXCombobox();
         cmbavDynamicfiltersselector1 = new GXCombobox();
         cmbavDynamicfiltersselector2 = new GXCombobox();
         cmbPerfil_Tipo = new GXCombobox();
         chkUsuarioPerfil_Insert = new GXCheckbox();
         chkUsuarioPerfil_Update = new GXCheckbox();
         chkUsuarioPerfil_Delete = new GXCheckbox();
         chkUsuarioPerfil_Display = new GXCheckbox();
         chkavDynamicfiltersenabled2 = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
            {
               nRC_GXsfl_63 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_63_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_63_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
            {
               subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV13OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
               AV14OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
               AV16DynamicFiltersSelector1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
               AV18Perfil_Nome1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18Perfil_Nome1", AV18Perfil_Nome1);
               AV19Usuario_PessoaNom1 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19Usuario_PessoaNom1", AV19Usuario_PessoaNom1);
               AV21DynamicFiltersSelector2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
               AV23Perfil_Nome2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23Perfil_Nome2", AV23Perfil_Nome2);
               AV24Usuario_PessoaNom2 = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24Usuario_PessoaNom2", AV24Usuario_PessoaNom2);
               AV20DynamicFiltersEnabled2 = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled2", AV20DynamicFiltersEnabled2);
               AV55TFUsuario_Nome = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55TFUsuario_Nome", AV55TFUsuario_Nome);
               AV56TFUsuario_Nome_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56TFUsuario_Nome_Sel", AV56TFUsuario_Nome_Sel);
               AV59TFUsuario_PessoaNom = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59TFUsuario_PessoaNom", AV59TFUsuario_PessoaNom);
               AV60TFUsuario_PessoaNom_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60TFUsuario_PessoaNom_Sel", AV60TFUsuario_PessoaNom_Sel);
               AV63TFPerfil_Nome = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63TFPerfil_Nome", AV63TFPerfil_Nome);
               AV64TFPerfil_Nome_Sel = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64TFPerfil_Nome_Sel", AV64TFPerfil_Nome_Sel);
               AV71TFUsuarioPerfil_Insert_Sel = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71TFUsuarioPerfil_Insert_Sel", StringUtil.Str( (decimal)(AV71TFUsuarioPerfil_Insert_Sel), 1, 0));
               AV74TFUsuarioPerfil_Update_Sel = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV74TFUsuarioPerfil_Update_Sel", StringUtil.Str( (decimal)(AV74TFUsuarioPerfil_Update_Sel), 1, 0));
               AV77TFUsuarioPerfil_Delete_Sel = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV77TFUsuarioPerfil_Delete_Sel", StringUtil.Str( (decimal)(AV77TFUsuarioPerfil_Delete_Sel), 1, 0));
               AV80TFUsuarioPerfil_Display_Sel = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV80TFUsuarioPerfil_Display_Sel", StringUtil.Str( (decimal)(AV80TFUsuarioPerfil_Display_Sel), 1, 0));
               AV57ddo_Usuario_NomeTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57ddo_Usuario_NomeTitleControlIdToReplace", AV57ddo_Usuario_NomeTitleControlIdToReplace);
               AV61ddo_Usuario_PessoaNomTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61ddo_Usuario_PessoaNomTitleControlIdToReplace", AV61ddo_Usuario_PessoaNomTitleControlIdToReplace);
               AV65ddo_Perfil_NomeTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65ddo_Perfil_NomeTitleControlIdToReplace", AV65ddo_Perfil_NomeTitleControlIdToReplace);
               AV69ddo_Perfil_TipoTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69ddo_Perfil_TipoTitleControlIdToReplace", AV69ddo_Perfil_TipoTitleControlIdToReplace);
               AV72ddo_UsuarioPerfil_InsertTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV72ddo_UsuarioPerfil_InsertTitleControlIdToReplace", AV72ddo_UsuarioPerfil_InsertTitleControlIdToReplace);
               AV75ddo_UsuarioPerfil_UpdateTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV75ddo_UsuarioPerfil_UpdateTitleControlIdToReplace", AV75ddo_UsuarioPerfil_UpdateTitleControlIdToReplace);
               AV78ddo_UsuarioPerfil_DeleteTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV78ddo_UsuarioPerfil_DeleteTitleControlIdToReplace", AV78ddo_UsuarioPerfil_DeleteTitleControlIdToReplace);
               AV81ddo_UsuarioPerfil_DisplayTitleControlIdToReplace = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV81ddo_UsuarioPerfil_DisplayTitleControlIdToReplace", AV81ddo_UsuarioPerfil_DisplayTitleControlIdToReplace);
               ajax_req_read_hidden_sdt(GetNextPar( ), AV68TFPerfil_Tipo_Sels);
               AV108Pgmname = GetNextPar( );
               ajax_req_read_hidden_sdt(GetNextPar( ), AV10GridState);
               AV26DynamicFiltersIgnoreFirst = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersIgnoreFirst", AV26DynamicFiltersIgnoreFirst);
               AV25DynamicFiltersRemoving = (bool)(BooleanUtil.Val(GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersRemoving", AV25DynamicFiltersRemoving);
               A1Usuario_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               A3Perfil_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV16DynamicFiltersSelector1, AV18Perfil_Nome1, AV19Usuario_PessoaNom1, AV21DynamicFiltersSelector2, AV23Perfil_Nome2, AV24Usuario_PessoaNom2, AV20DynamicFiltersEnabled2, AV55TFUsuario_Nome, AV56TFUsuario_Nome_Sel, AV59TFUsuario_PessoaNom, AV60TFUsuario_PessoaNom_Sel, AV63TFPerfil_Nome, AV64TFPerfil_Nome_Sel, AV71TFUsuarioPerfil_Insert_Sel, AV74TFUsuarioPerfil_Update_Sel, AV77TFUsuarioPerfil_Delete_Sel, AV80TFUsuarioPerfil_Display_Sel, AV57ddo_Usuario_NomeTitleControlIdToReplace, AV61ddo_Usuario_PessoaNomTitleControlIdToReplace, AV65ddo_Perfil_NomeTitleControlIdToReplace, AV69ddo_Perfil_TipoTitleControlIdToReplace, AV72ddo_UsuarioPerfil_InsertTitleControlIdToReplace, AV75ddo_UsuarioPerfil_UpdateTitleControlIdToReplace, AV78ddo_UsuarioPerfil_DeleteTitleControlIdToReplace, AV81ddo_UsuarioPerfil_DisplayTitleControlIdToReplace, AV68TFPerfil_Tipo_Sels, AV108Pgmname, AV10GridState, AV26DynamicFiltersIgnoreFirst, AV25DynamicFiltersRemoving, A1Usuario_Codigo, A3Perfil_Codigo) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("workwithplusbootstrapmasterpage", "GeneXus.Programs.workwithplusbootstrapmasterpage", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PA8Y2( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            START8Y2( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20205299353980");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wwusuarioperfil.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vORDEREDDSC", StringUtil.BoolToStr( AV14OrderedDsc));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR1", AV16DynamicFiltersSelector1);
         GxWebStd.gx_hidden_field( context, "GXH_vPERFIL_NOME1", StringUtil.RTrim( AV18Perfil_Nome1));
         GxWebStd.gx_hidden_field( context, "GXH_vUSUARIO_PESSOANOM1", StringUtil.RTrim( AV19Usuario_PessoaNom1));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSSELECTOR2", AV21DynamicFiltersSelector2);
         GxWebStd.gx_hidden_field( context, "GXH_vPERFIL_NOME2", StringUtil.RTrim( AV23Perfil_Nome2));
         GxWebStd.gx_hidden_field( context, "GXH_vUSUARIO_PESSOANOM2", StringUtil.RTrim( AV24Usuario_PessoaNom2));
         GxWebStd.gx_hidden_field( context, "GXH_vDYNAMICFILTERSENABLED2", StringUtil.BoolToStr( AV20DynamicFiltersEnabled2));
         GxWebStd.gx_hidden_field( context, "GXH_vTFUSUARIO_NOME", StringUtil.RTrim( AV55TFUsuario_Nome));
         GxWebStd.gx_hidden_field( context, "GXH_vTFUSUARIO_NOME_SEL", StringUtil.RTrim( AV56TFUsuario_Nome_Sel));
         GxWebStd.gx_hidden_field( context, "GXH_vTFUSUARIO_PESSOANOM", StringUtil.RTrim( AV59TFUsuario_PessoaNom));
         GxWebStd.gx_hidden_field( context, "GXH_vTFUSUARIO_PESSOANOM_SEL", StringUtil.RTrim( AV60TFUsuario_PessoaNom_Sel));
         GxWebStd.gx_hidden_field( context, "GXH_vTFPERFIL_NOME", StringUtil.RTrim( AV63TFPerfil_Nome));
         GxWebStd.gx_hidden_field( context, "GXH_vTFPERFIL_NOME_SEL", StringUtil.RTrim( AV64TFPerfil_Nome_Sel));
         GxWebStd.gx_hidden_field( context, "GXH_vTFUSUARIOPERFIL_INSERT_SEL", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV71TFUsuarioPerfil_Insert_Sel), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFUSUARIOPERFIL_UPDATE_SEL", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV74TFUsuarioPerfil_Update_Sel), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFUSUARIOPERFIL_DELETE_SEL", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV77TFUsuarioPerfil_Delete_Sel), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vTFUSUARIOPERFIL_DISPLAY_SEL", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV80TFUsuarioPerfil_Display_Sel), 1, 0, ",", "")));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_63", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_63), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV84GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV85GridPageCount), 10, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vDDO_TITLESETTINGSICONS", AV82DDO_TitleSettingsIcons);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vDDO_TITLESETTINGSICONS", AV82DDO_TitleSettingsIcons);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vUSUARIO_NOMETITLEFILTERDATA", AV54Usuario_NomeTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vUSUARIO_NOMETITLEFILTERDATA", AV54Usuario_NomeTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vUSUARIO_PESSOANOMTITLEFILTERDATA", AV58Usuario_PessoaNomTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vUSUARIO_PESSOANOMTITLEFILTERDATA", AV58Usuario_PessoaNomTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vPERFIL_NOMETITLEFILTERDATA", AV62Perfil_NomeTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vPERFIL_NOMETITLEFILTERDATA", AV62Perfil_NomeTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vPERFIL_TIPOTITLEFILTERDATA", AV66Perfil_TipoTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vPERFIL_TIPOTITLEFILTERDATA", AV66Perfil_TipoTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vUSUARIOPERFIL_INSERTTITLEFILTERDATA", AV70UsuarioPerfil_InsertTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vUSUARIOPERFIL_INSERTTITLEFILTERDATA", AV70UsuarioPerfil_InsertTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vUSUARIOPERFIL_UPDATETITLEFILTERDATA", AV73UsuarioPerfil_UpdateTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vUSUARIOPERFIL_UPDATETITLEFILTERDATA", AV73UsuarioPerfil_UpdateTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vUSUARIOPERFIL_DELETETITLEFILTERDATA", AV76UsuarioPerfil_DeleteTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vUSUARIOPERFIL_DELETETITLEFILTERDATA", AV76UsuarioPerfil_DeleteTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vUSUARIOPERFIL_DISPLAYTITLEFILTERDATA", AV79UsuarioPerfil_DisplayTitleFilterData);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vUSUARIOPERFIL_DISPLAYTITLEFILTERDATA", AV79UsuarioPerfil_DisplayTitleFilterData);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTFPERFIL_TIPO_SELS", AV68TFPerfil_Tipo_Sels);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTFPERFIL_TIPO_SELS", AV68TFPerfil_Tipo_Sels);
         }
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV108Pgmname));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vGRIDSTATE", AV10GridState);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vGRIDSTATE", AV10GridState);
         }
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSIGNOREFIRST", AV26DynamicFiltersIgnoreFirst);
         GxWebStd.gx_boolean_hidden_field( context, "vDYNAMICFILTERSREMOVING", AV25DynamicFiltersRemoving);
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_NOME_Caption", StringUtil.RTrim( Ddo_usuario_nome_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_NOME_Tooltip", StringUtil.RTrim( Ddo_usuario_nome_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_NOME_Cls", StringUtil.RTrim( Ddo_usuario_nome_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_NOME_Filteredtext_set", StringUtil.RTrim( Ddo_usuario_nome_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_NOME_Selectedvalue_set", StringUtil.RTrim( Ddo_usuario_nome_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_NOME_Dropdownoptionstype", StringUtil.RTrim( Ddo_usuario_nome_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_NOME_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_usuario_nome_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_NOME_Includesortasc", StringUtil.BoolToStr( Ddo_usuario_nome_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_NOME_Includesortdsc", StringUtil.BoolToStr( Ddo_usuario_nome_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_NOME_Sortedstatus", StringUtil.RTrim( Ddo_usuario_nome_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_NOME_Includefilter", StringUtil.BoolToStr( Ddo_usuario_nome_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_NOME_Filtertype", StringUtil.RTrim( Ddo_usuario_nome_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_NOME_Filterisrange", StringUtil.BoolToStr( Ddo_usuario_nome_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_NOME_Includedatalist", StringUtil.BoolToStr( Ddo_usuario_nome_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_NOME_Datalisttype", StringUtil.RTrim( Ddo_usuario_nome_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_NOME_Datalistproc", StringUtil.RTrim( Ddo_usuario_nome_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_NOME_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_usuario_nome_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_NOME_Sortasc", StringUtil.RTrim( Ddo_usuario_nome_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_NOME_Sortdsc", StringUtil.RTrim( Ddo_usuario_nome_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_NOME_Loadingdata", StringUtil.RTrim( Ddo_usuario_nome_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_NOME_Cleanfilter", StringUtil.RTrim( Ddo_usuario_nome_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_NOME_Noresultsfound", StringUtil.RTrim( Ddo_usuario_nome_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_NOME_Searchbuttontext", StringUtil.RTrim( Ddo_usuario_nome_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_PESSOANOM_Caption", StringUtil.RTrim( Ddo_usuario_pessoanom_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_PESSOANOM_Tooltip", StringUtil.RTrim( Ddo_usuario_pessoanom_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_PESSOANOM_Cls", StringUtil.RTrim( Ddo_usuario_pessoanom_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_PESSOANOM_Filteredtext_set", StringUtil.RTrim( Ddo_usuario_pessoanom_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_PESSOANOM_Selectedvalue_set", StringUtil.RTrim( Ddo_usuario_pessoanom_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_PESSOANOM_Dropdownoptionstype", StringUtil.RTrim( Ddo_usuario_pessoanom_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_PESSOANOM_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_usuario_pessoanom_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_PESSOANOM_Includesortasc", StringUtil.BoolToStr( Ddo_usuario_pessoanom_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_PESSOANOM_Includesortdsc", StringUtil.BoolToStr( Ddo_usuario_pessoanom_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_PESSOANOM_Sortedstatus", StringUtil.RTrim( Ddo_usuario_pessoanom_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_PESSOANOM_Includefilter", StringUtil.BoolToStr( Ddo_usuario_pessoanom_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_PESSOANOM_Filtertype", StringUtil.RTrim( Ddo_usuario_pessoanom_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_PESSOANOM_Filterisrange", StringUtil.BoolToStr( Ddo_usuario_pessoanom_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_PESSOANOM_Includedatalist", StringUtil.BoolToStr( Ddo_usuario_pessoanom_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_PESSOANOM_Datalisttype", StringUtil.RTrim( Ddo_usuario_pessoanom_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_PESSOANOM_Datalistproc", StringUtil.RTrim( Ddo_usuario_pessoanom_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_PESSOANOM_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_usuario_pessoanom_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_PESSOANOM_Sortasc", StringUtil.RTrim( Ddo_usuario_pessoanom_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_PESSOANOM_Sortdsc", StringUtil.RTrim( Ddo_usuario_pessoanom_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_PESSOANOM_Loadingdata", StringUtil.RTrim( Ddo_usuario_pessoanom_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_PESSOANOM_Cleanfilter", StringUtil.RTrim( Ddo_usuario_pessoanom_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_PESSOANOM_Noresultsfound", StringUtil.RTrim( Ddo_usuario_pessoanom_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_PESSOANOM_Searchbuttontext", StringUtil.RTrim( Ddo_usuario_pessoanom_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_PERFIL_NOME_Caption", StringUtil.RTrim( Ddo_perfil_nome_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_PERFIL_NOME_Tooltip", StringUtil.RTrim( Ddo_perfil_nome_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_PERFIL_NOME_Cls", StringUtil.RTrim( Ddo_perfil_nome_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_PERFIL_NOME_Filteredtext_set", StringUtil.RTrim( Ddo_perfil_nome_Filteredtext_set));
         GxWebStd.gx_hidden_field( context, "DDO_PERFIL_NOME_Selectedvalue_set", StringUtil.RTrim( Ddo_perfil_nome_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_PERFIL_NOME_Dropdownoptionstype", StringUtil.RTrim( Ddo_perfil_nome_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_PERFIL_NOME_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_perfil_nome_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_PERFIL_NOME_Includesortasc", StringUtil.BoolToStr( Ddo_perfil_nome_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_PERFIL_NOME_Includesortdsc", StringUtil.BoolToStr( Ddo_perfil_nome_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_PERFIL_NOME_Sortedstatus", StringUtil.RTrim( Ddo_perfil_nome_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_PERFIL_NOME_Includefilter", StringUtil.BoolToStr( Ddo_perfil_nome_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_PERFIL_NOME_Filtertype", StringUtil.RTrim( Ddo_perfil_nome_Filtertype));
         GxWebStd.gx_hidden_field( context, "DDO_PERFIL_NOME_Filterisrange", StringUtil.BoolToStr( Ddo_perfil_nome_Filterisrange));
         GxWebStd.gx_hidden_field( context, "DDO_PERFIL_NOME_Includedatalist", StringUtil.BoolToStr( Ddo_perfil_nome_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_PERFIL_NOME_Datalisttype", StringUtil.RTrim( Ddo_perfil_nome_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_PERFIL_NOME_Datalistproc", StringUtil.RTrim( Ddo_perfil_nome_Datalistproc));
         GxWebStd.gx_hidden_field( context, "DDO_PERFIL_NOME_Datalistupdateminimumcharacters", StringUtil.LTrim( StringUtil.NToC( (decimal)(Ddo_perfil_nome_Datalistupdateminimumcharacters), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DDO_PERFIL_NOME_Sortasc", StringUtil.RTrim( Ddo_perfil_nome_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_PERFIL_NOME_Sortdsc", StringUtil.RTrim( Ddo_perfil_nome_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_PERFIL_NOME_Loadingdata", StringUtil.RTrim( Ddo_perfil_nome_Loadingdata));
         GxWebStd.gx_hidden_field( context, "DDO_PERFIL_NOME_Cleanfilter", StringUtil.RTrim( Ddo_perfil_nome_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_PERFIL_NOME_Noresultsfound", StringUtil.RTrim( Ddo_perfil_nome_Noresultsfound));
         GxWebStd.gx_hidden_field( context, "DDO_PERFIL_NOME_Searchbuttontext", StringUtil.RTrim( Ddo_perfil_nome_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_PERFIL_TIPO_Caption", StringUtil.RTrim( Ddo_perfil_tipo_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_PERFIL_TIPO_Tooltip", StringUtil.RTrim( Ddo_perfil_tipo_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_PERFIL_TIPO_Cls", StringUtil.RTrim( Ddo_perfil_tipo_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_PERFIL_TIPO_Selectedvalue_set", StringUtil.RTrim( Ddo_perfil_tipo_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_PERFIL_TIPO_Dropdownoptionstype", StringUtil.RTrim( Ddo_perfil_tipo_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_PERFIL_TIPO_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_perfil_tipo_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_PERFIL_TIPO_Includesortasc", StringUtil.BoolToStr( Ddo_perfil_tipo_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_PERFIL_TIPO_Includesortdsc", StringUtil.BoolToStr( Ddo_perfil_tipo_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_PERFIL_TIPO_Sortedstatus", StringUtil.RTrim( Ddo_perfil_tipo_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_PERFIL_TIPO_Includefilter", StringUtil.BoolToStr( Ddo_perfil_tipo_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_PERFIL_TIPO_Includedatalist", StringUtil.BoolToStr( Ddo_perfil_tipo_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_PERFIL_TIPO_Datalisttype", StringUtil.RTrim( Ddo_perfil_tipo_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_PERFIL_TIPO_Allowmultipleselection", StringUtil.BoolToStr( Ddo_perfil_tipo_Allowmultipleselection));
         GxWebStd.gx_hidden_field( context, "DDO_PERFIL_TIPO_Datalistfixedvalues", StringUtil.RTrim( Ddo_perfil_tipo_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_PERFIL_TIPO_Sortasc", StringUtil.RTrim( Ddo_perfil_tipo_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_PERFIL_TIPO_Sortdsc", StringUtil.RTrim( Ddo_perfil_tipo_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_PERFIL_TIPO_Cleanfilter", StringUtil.RTrim( Ddo_perfil_tipo_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_PERFIL_TIPO_Searchbuttontext", StringUtil.RTrim( Ddo_perfil_tipo_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIOPERFIL_INSERT_Caption", StringUtil.RTrim( Ddo_usuarioperfil_insert_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIOPERFIL_INSERT_Tooltip", StringUtil.RTrim( Ddo_usuarioperfil_insert_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIOPERFIL_INSERT_Cls", StringUtil.RTrim( Ddo_usuarioperfil_insert_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIOPERFIL_INSERT_Selectedvalue_set", StringUtil.RTrim( Ddo_usuarioperfil_insert_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIOPERFIL_INSERT_Dropdownoptionstype", StringUtil.RTrim( Ddo_usuarioperfil_insert_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIOPERFIL_INSERT_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_usuarioperfil_insert_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIOPERFIL_INSERT_Includesortasc", StringUtil.BoolToStr( Ddo_usuarioperfil_insert_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIOPERFIL_INSERT_Includesortdsc", StringUtil.BoolToStr( Ddo_usuarioperfil_insert_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIOPERFIL_INSERT_Sortedstatus", StringUtil.RTrim( Ddo_usuarioperfil_insert_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIOPERFIL_INSERT_Includefilter", StringUtil.BoolToStr( Ddo_usuarioperfil_insert_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIOPERFIL_INSERT_Includedatalist", StringUtil.BoolToStr( Ddo_usuarioperfil_insert_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIOPERFIL_INSERT_Datalisttype", StringUtil.RTrim( Ddo_usuarioperfil_insert_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIOPERFIL_INSERT_Datalistfixedvalues", StringUtil.RTrim( Ddo_usuarioperfil_insert_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIOPERFIL_INSERT_Sortasc", StringUtil.RTrim( Ddo_usuarioperfil_insert_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIOPERFIL_INSERT_Sortdsc", StringUtil.RTrim( Ddo_usuarioperfil_insert_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIOPERFIL_INSERT_Cleanfilter", StringUtil.RTrim( Ddo_usuarioperfil_insert_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIOPERFIL_INSERT_Searchbuttontext", StringUtil.RTrim( Ddo_usuarioperfil_insert_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIOPERFIL_UPDATE_Caption", StringUtil.RTrim( Ddo_usuarioperfil_update_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIOPERFIL_UPDATE_Tooltip", StringUtil.RTrim( Ddo_usuarioperfil_update_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIOPERFIL_UPDATE_Cls", StringUtil.RTrim( Ddo_usuarioperfil_update_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIOPERFIL_UPDATE_Selectedvalue_set", StringUtil.RTrim( Ddo_usuarioperfil_update_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIOPERFIL_UPDATE_Dropdownoptionstype", StringUtil.RTrim( Ddo_usuarioperfil_update_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIOPERFIL_UPDATE_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_usuarioperfil_update_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIOPERFIL_UPDATE_Includesortasc", StringUtil.BoolToStr( Ddo_usuarioperfil_update_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIOPERFIL_UPDATE_Includesortdsc", StringUtil.BoolToStr( Ddo_usuarioperfil_update_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIOPERFIL_UPDATE_Sortedstatus", StringUtil.RTrim( Ddo_usuarioperfil_update_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIOPERFIL_UPDATE_Includefilter", StringUtil.BoolToStr( Ddo_usuarioperfil_update_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIOPERFIL_UPDATE_Includedatalist", StringUtil.BoolToStr( Ddo_usuarioperfil_update_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIOPERFIL_UPDATE_Datalisttype", StringUtil.RTrim( Ddo_usuarioperfil_update_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIOPERFIL_UPDATE_Datalistfixedvalues", StringUtil.RTrim( Ddo_usuarioperfil_update_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIOPERFIL_UPDATE_Sortasc", StringUtil.RTrim( Ddo_usuarioperfil_update_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIOPERFIL_UPDATE_Sortdsc", StringUtil.RTrim( Ddo_usuarioperfil_update_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIOPERFIL_UPDATE_Cleanfilter", StringUtil.RTrim( Ddo_usuarioperfil_update_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIOPERFIL_UPDATE_Searchbuttontext", StringUtil.RTrim( Ddo_usuarioperfil_update_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIOPERFIL_DELETE_Caption", StringUtil.RTrim( Ddo_usuarioperfil_delete_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIOPERFIL_DELETE_Tooltip", StringUtil.RTrim( Ddo_usuarioperfil_delete_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIOPERFIL_DELETE_Cls", StringUtil.RTrim( Ddo_usuarioperfil_delete_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIOPERFIL_DELETE_Selectedvalue_set", StringUtil.RTrim( Ddo_usuarioperfil_delete_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIOPERFIL_DELETE_Dropdownoptionstype", StringUtil.RTrim( Ddo_usuarioperfil_delete_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIOPERFIL_DELETE_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_usuarioperfil_delete_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIOPERFIL_DELETE_Includesortasc", StringUtil.BoolToStr( Ddo_usuarioperfil_delete_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIOPERFIL_DELETE_Includesortdsc", StringUtil.BoolToStr( Ddo_usuarioperfil_delete_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIOPERFIL_DELETE_Sortedstatus", StringUtil.RTrim( Ddo_usuarioperfil_delete_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIOPERFIL_DELETE_Includefilter", StringUtil.BoolToStr( Ddo_usuarioperfil_delete_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIOPERFIL_DELETE_Includedatalist", StringUtil.BoolToStr( Ddo_usuarioperfil_delete_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIOPERFIL_DELETE_Datalisttype", StringUtil.RTrim( Ddo_usuarioperfil_delete_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIOPERFIL_DELETE_Datalistfixedvalues", StringUtil.RTrim( Ddo_usuarioperfil_delete_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIOPERFIL_DELETE_Sortasc", StringUtil.RTrim( Ddo_usuarioperfil_delete_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIOPERFIL_DELETE_Sortdsc", StringUtil.RTrim( Ddo_usuarioperfil_delete_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIOPERFIL_DELETE_Cleanfilter", StringUtil.RTrim( Ddo_usuarioperfil_delete_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIOPERFIL_DELETE_Searchbuttontext", StringUtil.RTrim( Ddo_usuarioperfil_delete_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIOPERFIL_DISPLAY_Caption", StringUtil.RTrim( Ddo_usuarioperfil_display_Caption));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIOPERFIL_DISPLAY_Tooltip", StringUtil.RTrim( Ddo_usuarioperfil_display_Tooltip));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIOPERFIL_DISPLAY_Cls", StringUtil.RTrim( Ddo_usuarioperfil_display_Cls));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIOPERFIL_DISPLAY_Selectedvalue_set", StringUtil.RTrim( Ddo_usuarioperfil_display_Selectedvalue_set));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIOPERFIL_DISPLAY_Dropdownoptionstype", StringUtil.RTrim( Ddo_usuarioperfil_display_Dropdownoptionstype));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIOPERFIL_DISPLAY_Titlecontrolidtoreplace", StringUtil.RTrim( Ddo_usuarioperfil_display_Titlecontrolidtoreplace));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIOPERFIL_DISPLAY_Includesortasc", StringUtil.BoolToStr( Ddo_usuarioperfil_display_Includesortasc));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIOPERFIL_DISPLAY_Includesortdsc", StringUtil.BoolToStr( Ddo_usuarioperfil_display_Includesortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIOPERFIL_DISPLAY_Sortedstatus", StringUtil.RTrim( Ddo_usuarioperfil_display_Sortedstatus));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIOPERFIL_DISPLAY_Includefilter", StringUtil.BoolToStr( Ddo_usuarioperfil_display_Includefilter));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIOPERFIL_DISPLAY_Includedatalist", StringUtil.BoolToStr( Ddo_usuarioperfil_display_Includedatalist));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIOPERFIL_DISPLAY_Datalisttype", StringUtil.RTrim( Ddo_usuarioperfil_display_Datalisttype));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIOPERFIL_DISPLAY_Datalistfixedvalues", StringUtil.RTrim( Ddo_usuarioperfil_display_Datalistfixedvalues));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIOPERFIL_DISPLAY_Sortasc", StringUtil.RTrim( Ddo_usuarioperfil_display_Sortasc));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIOPERFIL_DISPLAY_Sortdsc", StringUtil.RTrim( Ddo_usuarioperfil_display_Sortdsc));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIOPERFIL_DISPLAY_Cleanfilter", StringUtil.RTrim( Ddo_usuarioperfil_display_Cleanfilter));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIOPERFIL_DISPLAY_Searchbuttontext", StringUtil.RTrim( Ddo_usuarioperfil_display_Searchbuttontext));
         GxWebStd.gx_hidden_field( context, "GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_NOME_Activeeventkey", StringUtil.RTrim( Ddo_usuario_nome_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_NOME_Filteredtext_get", StringUtil.RTrim( Ddo_usuario_nome_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_NOME_Selectedvalue_get", StringUtil.RTrim( Ddo_usuario_nome_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_PESSOANOM_Activeeventkey", StringUtil.RTrim( Ddo_usuario_pessoanom_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_PESSOANOM_Filteredtext_get", StringUtil.RTrim( Ddo_usuario_pessoanom_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIO_PESSOANOM_Selectedvalue_get", StringUtil.RTrim( Ddo_usuario_pessoanom_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_PERFIL_NOME_Activeeventkey", StringUtil.RTrim( Ddo_perfil_nome_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_PERFIL_NOME_Filteredtext_get", StringUtil.RTrim( Ddo_perfil_nome_Filteredtext_get));
         GxWebStd.gx_hidden_field( context, "DDO_PERFIL_NOME_Selectedvalue_get", StringUtil.RTrim( Ddo_perfil_nome_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_PERFIL_TIPO_Activeeventkey", StringUtil.RTrim( Ddo_perfil_tipo_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_PERFIL_TIPO_Selectedvalue_get", StringUtil.RTrim( Ddo_perfil_tipo_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIOPERFIL_INSERT_Activeeventkey", StringUtil.RTrim( Ddo_usuarioperfil_insert_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIOPERFIL_INSERT_Selectedvalue_get", StringUtil.RTrim( Ddo_usuarioperfil_insert_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIOPERFIL_UPDATE_Activeeventkey", StringUtil.RTrim( Ddo_usuarioperfil_update_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIOPERFIL_UPDATE_Selectedvalue_get", StringUtil.RTrim( Ddo_usuarioperfil_update_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIOPERFIL_DELETE_Activeeventkey", StringUtil.RTrim( Ddo_usuarioperfil_delete_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIOPERFIL_DELETE_Selectedvalue_get", StringUtil.RTrim( Ddo_usuarioperfil_delete_Selectedvalue_get));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIOPERFIL_DISPLAY_Activeeventkey", StringUtil.RTrim( Ddo_usuarioperfil_display_Activeeventkey));
         GxWebStd.gx_hidden_field( context, "DDO_USUARIOPERFIL_DISPLAY_Selectedvalue_get", StringUtil.RTrim( Ddo_usuarioperfil_display_Selectedvalue_get));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WE8Y2( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVT8Y2( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return false ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wwusuarioperfil.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "WWUsuarioPerfil" ;
      }

      public override String GetPgmdesc( )
      {
         return " Usuario x Perfil" ;
      }

      protected void WB8Y0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            wb_table1_2_8Y2( true) ;
         }
         else
         {
            wb_table1_2_8Y2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_8Y2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 81,'',false,'" + sGXsfl_63_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled2_Internalname, StringUtil.BoolToStr( AV20DynamicFiltersEnabled2), "", "", chkavDynamicfiltersenabled2.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(81, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,81);\"");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 82,'',false,'" + sGXsfl_63_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfusuario_nome_Internalname, StringUtil.RTrim( AV55TFUsuario_Nome), StringUtil.RTrim( context.localUtil.Format( AV55TFUsuario_Nome, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,82);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfusuario_nome_Jsonclick, 0, "Attribute", "", "", "", edtavTfusuario_nome_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWUsuarioPerfil.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 83,'',false,'" + sGXsfl_63_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfusuario_nome_sel_Internalname, StringUtil.RTrim( AV56TFUsuario_Nome_Sel), StringUtil.RTrim( context.localUtil.Format( AV56TFUsuario_Nome_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,83);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfusuario_nome_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfusuario_nome_sel_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWUsuarioPerfil.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 84,'',false,'" + sGXsfl_63_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfusuario_pessoanom_Internalname, StringUtil.RTrim( AV59TFUsuario_PessoaNom), StringUtil.RTrim( context.localUtil.Format( AV59TFUsuario_PessoaNom, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,84);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfusuario_pessoanom_Jsonclick, 0, "BootstrapAttribute100", "", "", "", edtavTfusuario_pessoanom_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWUsuarioPerfil.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 85,'',false,'" + sGXsfl_63_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfusuario_pessoanom_sel_Internalname, StringUtil.RTrim( AV60TFUsuario_PessoaNom_Sel), StringUtil.RTrim( context.localUtil.Format( AV60TFUsuario_PessoaNom_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,85);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfusuario_pessoanom_sel_Jsonclick, 0, "BootstrapAttribute100", "", "", "", edtavTfusuario_pessoanom_sel_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWUsuarioPerfil.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 86,'',false,'" + sGXsfl_63_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfperfil_nome_Internalname, StringUtil.RTrim( AV63TFPerfil_Nome), StringUtil.RTrim( context.localUtil.Format( AV63TFPerfil_Nome, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,86);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfperfil_nome_Jsonclick, 0, "Attribute", "", "", "", edtavTfperfil_nome_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWUsuarioPerfil.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 87,'',false,'" + sGXsfl_63_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfperfil_nome_sel_Internalname, StringUtil.RTrim( AV64TFPerfil_Nome_Sel), StringUtil.RTrim( context.localUtil.Format( AV64TFPerfil_Nome_Sel, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,87);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfperfil_nome_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfperfil_nome_sel_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWUsuarioPerfil.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 88,'',false,'" + sGXsfl_63_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfusuarioperfil_insert_sel_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV71TFUsuarioPerfil_Insert_Sel), 1, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV71TFUsuarioPerfil_Insert_Sel), "9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,88);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfusuarioperfil_insert_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfusuarioperfil_insert_sel_Visible, 1, 0, "text", "", 1, "chr", 1, "row", 1, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWUsuarioPerfil.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 89,'',false,'" + sGXsfl_63_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfusuarioperfil_update_sel_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV74TFUsuarioPerfil_Update_Sel), 1, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV74TFUsuarioPerfil_Update_Sel), "9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,89);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfusuarioperfil_update_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfusuarioperfil_update_sel_Visible, 1, 0, "text", "", 1, "chr", 1, "row", 1, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWUsuarioPerfil.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 90,'',false,'" + sGXsfl_63_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfusuarioperfil_delete_sel_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV77TFUsuarioPerfil_Delete_Sel), 1, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV77TFUsuarioPerfil_Delete_Sel), "9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,90);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfusuarioperfil_delete_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfusuarioperfil_delete_sel_Visible, 1, 0, "text", "", 1, "chr", 1, "row", 1, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWUsuarioPerfil.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 91,'',false,'" + sGXsfl_63_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavTfusuarioperfil_display_sel_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV80TFUsuarioPerfil_Display_Sel), 1, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV80TFUsuarioPerfil_Display_Sel), "9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,91);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavTfusuarioperfil_display_sel_Jsonclick, 0, "Attribute", "", "", "", edtavTfusuarioperfil_display_sel_Visible, 1, 0, "text", "", 1, "chr", 1, "row", 1, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWUsuarioPerfil.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_USUARIO_NOMEContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 93,'',false,'" + sGXsfl_63_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_usuario_nometitlecontrolidtoreplace_Internalname, AV57ddo_Usuario_NomeTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,93);\"", 0, edtavDdo_usuario_nometitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWUsuarioPerfil.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_USUARIO_PESSOANOMContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 95,'',false,'" + sGXsfl_63_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_usuario_pessoanomtitlecontrolidtoreplace_Internalname, AV61ddo_Usuario_PessoaNomTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,95);\"", 0, edtavDdo_usuario_pessoanomtitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWUsuarioPerfil.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_PERFIL_NOMEContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 97,'',false,'" + sGXsfl_63_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_perfil_nometitlecontrolidtoreplace_Internalname, AV65ddo_Perfil_NomeTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,97);\"", 0, edtavDdo_perfil_nometitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWUsuarioPerfil.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_PERFIL_TIPOContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 99,'',false,'" + sGXsfl_63_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_perfil_tipotitlecontrolidtoreplace_Internalname, AV69ddo_Perfil_TipoTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,99);\"", 0, edtavDdo_perfil_tipotitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWUsuarioPerfil.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_USUARIOPERFIL_INSERTContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 101,'',false,'" + sGXsfl_63_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_usuarioperfil_inserttitlecontrolidtoreplace_Internalname, AV72ddo_UsuarioPerfil_InsertTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,101);\"", 0, edtavDdo_usuarioperfil_inserttitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWUsuarioPerfil.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_USUARIOPERFIL_UPDATEContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 103,'',false,'" + sGXsfl_63_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_usuarioperfil_updatetitlecontrolidtoreplace_Internalname, AV75ddo_UsuarioPerfil_UpdateTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,103);\"", 0, edtavDdo_usuarioperfil_updatetitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWUsuarioPerfil.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_USUARIOPERFIL_DELETEContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 105,'',false,'" + sGXsfl_63_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_usuarioperfil_deletetitlecontrolidtoreplace_Internalname, AV78ddo_UsuarioPerfil_DeleteTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,105);\"", 0, edtavDdo_usuarioperfil_deletetitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWUsuarioPerfil.htm");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DDO_USUARIOPERFIL_DISPLAYContainer"+"\"></div>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 107,'',false,'" + sGXsfl_63_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavDdo_usuarioperfil_displaytitlecontrolidtoreplace_Internalname, AV81ddo_UsuarioPerfil_DisplayTitleControlIdToReplace, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,107);\"", 0, edtavDdo_usuarioperfil_displaytitlecontrolidtoreplace_Visible, 1, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "300", -1, "", "", -1, true, "", "HLP_WWUsuarioPerfil.htm");
         }
         wbLoad = true;
      }

      protected void START8Y2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", " Usuario x Perfil", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUP8Y0( ) ;
      }

      protected void WS8Y2( )
      {
         START8Y2( ) ;
         EVT8Y2( ) ;
      }

      protected void EVT8Y2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E118Y2 */
                              E118Y2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_USUARIO_NOME.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E128Y2 */
                              E128Y2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_USUARIO_PESSOANOM.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E138Y2 */
                              E138Y2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_PERFIL_NOME.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E148Y2 */
                              E148Y2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_PERFIL_TIPO.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E158Y2 */
                              E158Y2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_USUARIOPERFIL_INSERT.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E168Y2 */
                              E168Y2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_USUARIOPERFIL_UPDATE.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E178Y2 */
                              E178Y2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_USUARIOPERFIL_DELETE.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E188Y2 */
                              E188Y2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "DDO_USUARIOPERFIL_DISPLAY.ONOPTIONCLICKED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E198Y2 */
                              E198Y2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VORDEREDBY.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E208Y2 */
                              E208Y2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS1'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E218Y2 */
                              E218Y2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS2'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E228Y2 */
                              E228Y2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOCLEANFILTERS'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E238Y2 */
                              E238Y2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOINSERT'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E248Y2 */
                              E248Y2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS1'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E258Y2 */
                              E258Y2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR1.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E268Y2 */
                              E268Y2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR2.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E278Y2 */
                              E278Y2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              nGXsfl_63_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_63_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_63_idx), 4, 0)), 4, "0");
                              SubsflControlProps_632( ) ;
                              AV27Update = cgiGet( edtavUpdate_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUpdate_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV27Update)) ? AV106Update_GXI : context.convertURL( context.PathToRelativeUrl( AV27Update))));
                              AV28Delete = cgiGet( edtavDelete_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDelete_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV28Delete)) ? AV107Delete_GXI : context.convertURL( context.PathToRelativeUrl( AV28Delete))));
                              A1Usuario_Codigo = (int)(context.localUtil.CToN( cgiGet( edtUsuario_Codigo_Internalname), ",", "."));
                              A3Perfil_Codigo = (int)(context.localUtil.CToN( cgiGet( edtPerfil_Codigo_Internalname), ",", "."));
                              A57Usuario_PessoaCod = (int)(context.localUtil.CToN( cgiGet( edtUsuario_PessoaCod_Internalname), ",", "."));
                              A2Usuario_Nome = StringUtil.Upper( cgiGet( edtUsuario_Nome_Internalname));
                              n2Usuario_Nome = false;
                              A58Usuario_PessoaNom = StringUtil.Upper( cgiGet( edtUsuario_PessoaNom_Internalname));
                              n58Usuario_PessoaNom = false;
                              A4Perfil_Nome = StringUtil.Upper( cgiGet( edtPerfil_Nome_Internalname));
                              cmbPerfil_Tipo.Name = cmbPerfil_Tipo_Internalname;
                              cmbPerfil_Tipo.CurrentValue = cgiGet( cmbPerfil_Tipo_Internalname);
                              A275Perfil_Tipo = (short)(NumberUtil.Val( cgiGet( cmbPerfil_Tipo_Internalname), "."));
                              A544UsuarioPerfil_Insert = StringUtil.StrToBool( cgiGet( chkUsuarioPerfil_Insert_Internalname));
                              n544UsuarioPerfil_Insert = false;
                              A659UsuarioPerfil_Update = StringUtil.StrToBool( cgiGet( chkUsuarioPerfil_Update_Internalname));
                              n659UsuarioPerfil_Update = false;
                              A546UsuarioPerfil_Delete = StringUtil.StrToBool( cgiGet( chkUsuarioPerfil_Delete_Internalname));
                              n546UsuarioPerfil_Delete = false;
                              A543UsuarioPerfil_Display = StringUtil.StrToBool( cgiGet( chkUsuarioPerfil_Display_Internalname));
                              n543UsuarioPerfil_Display = false;
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E288Y2 */
                                    E288Y2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E298Y2 */
                                    E298Y2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: E308Y2 */
                                    E308Y2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       /* Set Refresh If Orderedby Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Ordereddsc Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV16DynamicFiltersSelector1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Perfil_nome1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vPERFIL_NOME1"), AV18Perfil_Nome1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Usuario_pessoanom1 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vUSUARIO_PESSOANOM1"), AV19Usuario_PessoaNom1) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersselector2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV21DynamicFiltersSelector2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Perfil_nome2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vPERFIL_NOME2"), AV23Perfil_Nome2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Usuario_pessoanom2 Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vUSUARIO_PESSOANOM2"), AV24Usuario_PessoaNom2) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Dynamicfiltersenabled2 Changed */
                                       if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV20DynamicFiltersEnabled2 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfusuario_nome Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFUSUARIO_NOME"), AV55TFUsuario_Nome) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfusuario_nome_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFUSUARIO_NOME_SEL"), AV56TFUsuario_Nome_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfusuario_pessoanom Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFUSUARIO_PESSOANOM"), AV59TFUsuario_PessoaNom) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfusuario_pessoanom_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFUSUARIO_PESSOANOM_SEL"), AV60TFUsuario_PessoaNom_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfperfil_nome Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFPERFIL_NOME"), AV63TFPerfil_Nome) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfperfil_nome_sel Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vTFPERFIL_NOME_SEL"), AV64TFPerfil_Nome_Sel) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfusuarioperfil_insert_sel Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFUSUARIOPERFIL_INSERT_SEL"), ",", ".") != Convert.ToDecimal( AV71TFUsuarioPerfil_Insert_Sel )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfusuarioperfil_update_sel Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFUSUARIOPERFIL_UPDATE_SEL"), ",", ".") != Convert.ToDecimal( AV74TFUsuarioPerfil_Update_Sel )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfusuarioperfil_delete_sel Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFUSUARIOPERFIL_DELETE_SEL"), ",", ".") != Convert.ToDecimal( AV77TFUsuarioPerfil_Delete_Sel )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Tfusuarioperfil_display_sel Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFUSUARIOPERFIL_DISPLAY_SEL"), ",", ".") != Convert.ToDecimal( AV80TFUsuarioPerfil_Display_Sel )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WE8Y2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PA8Y2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbavOrderedby.Name = "vORDEREDBY";
            cmbavOrderedby.WebTags = "";
            if ( cmbavOrderedby.ItemCount > 0 )
            {
               AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            }
            cmbavDynamicfiltersselector1.Name = "vDYNAMICFILTERSSELECTOR1";
            cmbavDynamicfiltersselector1.WebTags = "";
            cmbavDynamicfiltersselector1.addItem("PERFIL_NOME", "Nome", 0);
            cmbavDynamicfiltersselector1.addItem("USUARIO_PESSOANOM", "Pessoa", 0);
            if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
            {
               AV16DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV16DynamicFiltersSelector1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
            }
            cmbavDynamicfiltersselector2.Name = "vDYNAMICFILTERSSELECTOR2";
            cmbavDynamicfiltersselector2.WebTags = "";
            cmbavDynamicfiltersselector2.addItem("PERFIL_NOME", "Nome", 0);
            cmbavDynamicfiltersselector2.addItem("USUARIO_PESSOANOM", "Pessoa", 0);
            if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
            {
               AV21DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV21DynamicFiltersSelector2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
            }
            GXCCtl = "PERFIL_TIPO_" + sGXsfl_63_idx;
            cmbPerfil_Tipo.Name = GXCCtl;
            cmbPerfil_Tipo.WebTags = "";
            cmbPerfil_Tipo.addItem("0", "Desconhecido", 0);
            cmbPerfil_Tipo.addItem("1", "Auditor", 0);
            cmbPerfil_Tipo.addItem("2", "Contador", 0);
            cmbPerfil_Tipo.addItem("3", "Contratada", 0);
            cmbPerfil_Tipo.addItem("4", "Contratante", 0);
            cmbPerfil_Tipo.addItem("5", "Financeiro", 0);
            cmbPerfil_Tipo.addItem("6", "Usuario", 0);
            cmbPerfil_Tipo.addItem("10", "Licensiado", 0);
            cmbPerfil_Tipo.addItem("99", "Administrador GAM", 0);
            if ( cmbPerfil_Tipo.ItemCount > 0 )
            {
               A275Perfil_Tipo = (short)(NumberUtil.Val( cmbPerfil_Tipo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A275Perfil_Tipo), 4, 0))), "."));
            }
            GXCCtl = "USUARIOPERFIL_INSERT_" + sGXsfl_63_idx;
            chkUsuarioPerfil_Insert.Name = GXCCtl;
            chkUsuarioPerfil_Insert.WebTags = "";
            chkUsuarioPerfil_Insert.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkUsuarioPerfil_Insert_Internalname, "TitleCaption", chkUsuarioPerfil_Insert.Caption);
            chkUsuarioPerfil_Insert.CheckedValue = "false";
            GXCCtl = "USUARIOPERFIL_UPDATE_" + sGXsfl_63_idx;
            chkUsuarioPerfil_Update.Name = GXCCtl;
            chkUsuarioPerfil_Update.WebTags = "";
            chkUsuarioPerfil_Update.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkUsuarioPerfil_Update_Internalname, "TitleCaption", chkUsuarioPerfil_Update.Caption);
            chkUsuarioPerfil_Update.CheckedValue = "false";
            GXCCtl = "USUARIOPERFIL_DELETE_" + sGXsfl_63_idx;
            chkUsuarioPerfil_Delete.Name = GXCCtl;
            chkUsuarioPerfil_Delete.WebTags = "";
            chkUsuarioPerfil_Delete.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkUsuarioPerfil_Delete_Internalname, "TitleCaption", chkUsuarioPerfil_Delete.Caption);
            chkUsuarioPerfil_Delete.CheckedValue = "false";
            GXCCtl = "USUARIOPERFIL_DISPLAY_" + sGXsfl_63_idx;
            chkUsuarioPerfil_Display.Name = GXCCtl;
            chkUsuarioPerfil_Display.WebTags = "";
            chkUsuarioPerfil_Display.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkUsuarioPerfil_Display_Internalname, "TitleCaption", chkUsuarioPerfil_Display.Caption);
            chkUsuarioPerfil_Display.CheckedValue = "false";
            chkavDynamicfiltersenabled2.Name = "vDYNAMICFILTERSENABLED2";
            chkavDynamicfiltersenabled2.WebTags = "";
            chkavDynamicfiltersenabled2.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "TitleCaption", chkavDynamicfiltersenabled2.Caption);
            chkavDynamicfiltersenabled2.CheckedValue = "false";
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_632( ) ;
         while ( nGXsfl_63_idx <= nRC_GXsfl_63 )
         {
            sendrow_632( ) ;
            nGXsfl_63_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_63_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_63_idx+1));
            sGXsfl_63_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_63_idx), 4, 0)), 4, "0");
            SubsflControlProps_632( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV13OrderedBy ,
                                       bool AV14OrderedDsc ,
                                       String AV16DynamicFiltersSelector1 ,
                                       String AV18Perfil_Nome1 ,
                                       String AV19Usuario_PessoaNom1 ,
                                       String AV21DynamicFiltersSelector2 ,
                                       String AV23Perfil_Nome2 ,
                                       String AV24Usuario_PessoaNom2 ,
                                       bool AV20DynamicFiltersEnabled2 ,
                                       String AV55TFUsuario_Nome ,
                                       String AV56TFUsuario_Nome_Sel ,
                                       String AV59TFUsuario_PessoaNom ,
                                       String AV60TFUsuario_PessoaNom_Sel ,
                                       String AV63TFPerfil_Nome ,
                                       String AV64TFPerfil_Nome_Sel ,
                                       short AV71TFUsuarioPerfil_Insert_Sel ,
                                       short AV74TFUsuarioPerfil_Update_Sel ,
                                       short AV77TFUsuarioPerfil_Delete_Sel ,
                                       short AV80TFUsuarioPerfil_Display_Sel ,
                                       String AV57ddo_Usuario_NomeTitleControlIdToReplace ,
                                       String AV61ddo_Usuario_PessoaNomTitleControlIdToReplace ,
                                       String AV65ddo_Perfil_NomeTitleControlIdToReplace ,
                                       String AV69ddo_Perfil_TipoTitleControlIdToReplace ,
                                       String AV72ddo_UsuarioPerfil_InsertTitleControlIdToReplace ,
                                       String AV75ddo_UsuarioPerfil_UpdateTitleControlIdToReplace ,
                                       String AV78ddo_UsuarioPerfil_DeleteTitleControlIdToReplace ,
                                       String AV81ddo_UsuarioPerfil_DisplayTitleControlIdToReplace ,
                                       IGxCollection AV68TFPerfil_Tipo_Sels ,
                                       String AV108Pgmname ,
                                       wwpbaseobjects.SdtWWPGridState AV10GridState ,
                                       bool AV26DynamicFiltersIgnoreFirst ,
                                       bool AV25DynamicFiltersRemoving ,
                                       int A1Usuario_Codigo ,
                                       int A3Perfil_Codigo )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RF8Y2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_USUARIO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A1Usuario_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "USUARIO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1Usuario_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_PERFIL_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A3Perfil_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "PERFIL_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A3Perfil_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_USUARIOPERFIL_INSERT", GetSecureSignedToken( "", A544UsuarioPerfil_Insert));
         GxWebStd.gx_hidden_field( context, "USUARIOPERFIL_INSERT", StringUtil.BoolToStr( A544UsuarioPerfil_Insert));
         GxWebStd.gx_hidden_field( context, "gxhash_USUARIOPERFIL_UPDATE", GetSecureSignedToken( "", A659UsuarioPerfil_Update));
         GxWebStd.gx_hidden_field( context, "USUARIOPERFIL_UPDATE", StringUtil.BoolToStr( A659UsuarioPerfil_Update));
         GxWebStd.gx_hidden_field( context, "gxhash_USUARIOPERFIL_DELETE", GetSecureSignedToken( "", A546UsuarioPerfil_Delete));
         GxWebStd.gx_hidden_field( context, "USUARIOPERFIL_DELETE", StringUtil.BoolToStr( A546UsuarioPerfil_Delete));
         GxWebStd.gx_hidden_field( context, "gxhash_USUARIOPERFIL_DISPLAY", GetSecureSignedToken( "", A543UsuarioPerfil_Display));
         GxWebStd.gx_hidden_field( context, "USUARIOPERFIL_DISPLAY", StringUtil.BoolToStr( A543UsuarioPerfil_Display));
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavOrderedby.ItemCount > 0 )
         {
            AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
         {
            AV16DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV16DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
         }
         if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
         {
            AV21DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV21DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RF8Y2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV108Pgmname = "WWUsuarioPerfil";
         context.Gx_err = 0;
      }

      protected void RF8Y2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 63;
         /* Execute user event: E298Y2 */
         E298Y2 ();
         nGXsfl_63_idx = 1;
         sGXsfl_63_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_63_idx), 4, 0)), 4, "0");
         SubsflControlProps_632( ) ;
         nGXsfl_63_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", "");
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_632( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 1 : GRID_nFirstRecordOnPage+1));
            GXPagingTo2 = (int)(((subGrid_Rows==0) ? 10000 : GRID_nFirstRecordOnPage+subGrid_Recordsperpage( )+1));
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 A275Perfil_Tipo ,
                                                 AV101WWUsuarioPerfilDS_14_Tfperfil_tipo_sels ,
                                                 AV88WWUsuarioPerfilDS_1_Dynamicfiltersselector1 ,
                                                 AV89WWUsuarioPerfilDS_2_Perfil_nome1 ,
                                                 AV90WWUsuarioPerfilDS_3_Usuario_pessoanom1 ,
                                                 AV91WWUsuarioPerfilDS_4_Dynamicfiltersenabled2 ,
                                                 AV92WWUsuarioPerfilDS_5_Dynamicfiltersselector2 ,
                                                 AV93WWUsuarioPerfilDS_6_Perfil_nome2 ,
                                                 AV94WWUsuarioPerfilDS_7_Usuario_pessoanom2 ,
                                                 AV96WWUsuarioPerfilDS_9_Tfusuario_nome_sel ,
                                                 AV95WWUsuarioPerfilDS_8_Tfusuario_nome ,
                                                 AV98WWUsuarioPerfilDS_11_Tfusuario_pessoanom_sel ,
                                                 AV97WWUsuarioPerfilDS_10_Tfusuario_pessoanom ,
                                                 AV100WWUsuarioPerfilDS_13_Tfperfil_nome_sel ,
                                                 AV99WWUsuarioPerfilDS_12_Tfperfil_nome ,
                                                 AV101WWUsuarioPerfilDS_14_Tfperfil_tipo_sels.Count ,
                                                 AV102WWUsuarioPerfilDS_15_Tfusuarioperfil_insert_sel ,
                                                 AV103WWUsuarioPerfilDS_16_Tfusuarioperfil_update_sel ,
                                                 AV104WWUsuarioPerfilDS_17_Tfusuarioperfil_delete_sel ,
                                                 AV105WWUsuarioPerfilDS_18_Tfusuarioperfil_display_sel ,
                                                 A4Perfil_Nome ,
                                                 A58Usuario_PessoaNom ,
                                                 A2Usuario_Nome ,
                                                 A544UsuarioPerfil_Insert ,
                                                 A659UsuarioPerfil_Update ,
                                                 A546UsuarioPerfil_Delete ,
                                                 A543UsuarioPerfil_Display ,
                                                 AV13OrderedBy ,
                                                 AV14OrderedDsc },
                                                 new int[] {
                                                 TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.STRING,
                                                 TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN,
                                                 TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                                 }
            });
            lV89WWUsuarioPerfilDS_2_Perfil_nome1 = StringUtil.PadR( StringUtil.RTrim( AV89WWUsuarioPerfilDS_2_Perfil_nome1), 50, "%");
            lV90WWUsuarioPerfilDS_3_Usuario_pessoanom1 = StringUtil.PadR( StringUtil.RTrim( AV90WWUsuarioPerfilDS_3_Usuario_pessoanom1), 100, "%");
            lV93WWUsuarioPerfilDS_6_Perfil_nome2 = StringUtil.PadR( StringUtil.RTrim( AV93WWUsuarioPerfilDS_6_Perfil_nome2), 50, "%");
            lV94WWUsuarioPerfilDS_7_Usuario_pessoanom2 = StringUtil.PadR( StringUtil.RTrim( AV94WWUsuarioPerfilDS_7_Usuario_pessoanom2), 100, "%");
            lV95WWUsuarioPerfilDS_8_Tfusuario_nome = StringUtil.PadR( StringUtil.RTrim( AV95WWUsuarioPerfilDS_8_Tfusuario_nome), 50, "%");
            lV97WWUsuarioPerfilDS_10_Tfusuario_pessoanom = StringUtil.PadR( StringUtil.RTrim( AV97WWUsuarioPerfilDS_10_Tfusuario_pessoanom), 100, "%");
            lV99WWUsuarioPerfilDS_12_Tfperfil_nome = StringUtil.PadR( StringUtil.RTrim( AV99WWUsuarioPerfilDS_12_Tfperfil_nome), 50, "%");
            /* Using cursor H008Y2 */
            pr_default.execute(0, new Object[] {lV89WWUsuarioPerfilDS_2_Perfil_nome1, lV90WWUsuarioPerfilDS_3_Usuario_pessoanom1, lV93WWUsuarioPerfilDS_6_Perfil_nome2, lV94WWUsuarioPerfilDS_7_Usuario_pessoanom2, lV95WWUsuarioPerfilDS_8_Tfusuario_nome, AV96WWUsuarioPerfilDS_9_Tfusuario_nome_sel, lV97WWUsuarioPerfilDS_10_Tfusuario_pessoanom, AV98WWUsuarioPerfilDS_11_Tfusuario_pessoanom_sel, lV99WWUsuarioPerfilDS_12_Tfperfil_nome, AV100WWUsuarioPerfilDS_13_Tfperfil_nome_sel, GXPagingFrom2, GXPagingTo2, GXPagingTo2, GXPagingFrom2, GXPagingFrom2});
            nGXsfl_63_idx = 1;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A543UsuarioPerfil_Display = H008Y2_A543UsuarioPerfil_Display[0];
               n543UsuarioPerfil_Display = H008Y2_n543UsuarioPerfil_Display[0];
               A546UsuarioPerfil_Delete = H008Y2_A546UsuarioPerfil_Delete[0];
               n546UsuarioPerfil_Delete = H008Y2_n546UsuarioPerfil_Delete[0];
               A659UsuarioPerfil_Update = H008Y2_A659UsuarioPerfil_Update[0];
               n659UsuarioPerfil_Update = H008Y2_n659UsuarioPerfil_Update[0];
               A544UsuarioPerfil_Insert = H008Y2_A544UsuarioPerfil_Insert[0];
               n544UsuarioPerfil_Insert = H008Y2_n544UsuarioPerfil_Insert[0];
               A275Perfil_Tipo = H008Y2_A275Perfil_Tipo[0];
               A4Perfil_Nome = H008Y2_A4Perfil_Nome[0];
               A58Usuario_PessoaNom = H008Y2_A58Usuario_PessoaNom[0];
               n58Usuario_PessoaNom = H008Y2_n58Usuario_PessoaNom[0];
               A2Usuario_Nome = H008Y2_A2Usuario_Nome[0];
               n2Usuario_Nome = H008Y2_n2Usuario_Nome[0];
               A57Usuario_PessoaCod = H008Y2_A57Usuario_PessoaCod[0];
               A3Perfil_Codigo = H008Y2_A3Perfil_Codigo[0];
               A1Usuario_Codigo = H008Y2_A1Usuario_Codigo[0];
               A275Perfil_Tipo = H008Y2_A275Perfil_Tipo[0];
               A4Perfil_Nome = H008Y2_A4Perfil_Nome[0];
               A2Usuario_Nome = H008Y2_A2Usuario_Nome[0];
               n2Usuario_Nome = H008Y2_n2Usuario_Nome[0];
               A57Usuario_PessoaCod = H008Y2_A57Usuario_PessoaCod[0];
               A58Usuario_PessoaNom = H008Y2_A58Usuario_PessoaNom[0];
               n58Usuario_PessoaNom = H008Y2_n58Usuario_PessoaNom[0];
               /* Execute user event: E308Y2 */
               E308Y2 ();
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 63;
            WB8Y0( ) ;
         }
         nGXsfl_63_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         AV88WWUsuarioPerfilDS_1_Dynamicfiltersselector1 = AV16DynamicFiltersSelector1;
         AV89WWUsuarioPerfilDS_2_Perfil_nome1 = AV18Perfil_Nome1;
         AV90WWUsuarioPerfilDS_3_Usuario_pessoanom1 = AV19Usuario_PessoaNom1;
         AV91WWUsuarioPerfilDS_4_Dynamicfiltersenabled2 = AV20DynamicFiltersEnabled2;
         AV92WWUsuarioPerfilDS_5_Dynamicfiltersselector2 = AV21DynamicFiltersSelector2;
         AV93WWUsuarioPerfilDS_6_Perfil_nome2 = AV23Perfil_Nome2;
         AV94WWUsuarioPerfilDS_7_Usuario_pessoanom2 = AV24Usuario_PessoaNom2;
         AV95WWUsuarioPerfilDS_8_Tfusuario_nome = AV55TFUsuario_Nome;
         AV96WWUsuarioPerfilDS_9_Tfusuario_nome_sel = AV56TFUsuario_Nome_Sel;
         AV97WWUsuarioPerfilDS_10_Tfusuario_pessoanom = AV59TFUsuario_PessoaNom;
         AV98WWUsuarioPerfilDS_11_Tfusuario_pessoanom_sel = AV60TFUsuario_PessoaNom_Sel;
         AV99WWUsuarioPerfilDS_12_Tfperfil_nome = AV63TFPerfil_Nome;
         AV100WWUsuarioPerfilDS_13_Tfperfil_nome_sel = AV64TFPerfil_Nome_Sel;
         AV101WWUsuarioPerfilDS_14_Tfperfil_tipo_sels = AV68TFPerfil_Tipo_Sels;
         AV102WWUsuarioPerfilDS_15_Tfusuarioperfil_insert_sel = AV71TFUsuarioPerfil_Insert_Sel;
         AV103WWUsuarioPerfilDS_16_Tfusuarioperfil_update_sel = AV74TFUsuarioPerfil_Update_Sel;
         AV104WWUsuarioPerfilDS_17_Tfusuarioperfil_delete_sel = AV77TFUsuarioPerfil_Delete_Sel;
         AV105WWUsuarioPerfilDS_18_Tfusuarioperfil_display_sel = AV80TFUsuarioPerfil_Display_Sel;
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              A275Perfil_Tipo ,
                                              AV101WWUsuarioPerfilDS_14_Tfperfil_tipo_sels ,
                                              AV88WWUsuarioPerfilDS_1_Dynamicfiltersselector1 ,
                                              AV89WWUsuarioPerfilDS_2_Perfil_nome1 ,
                                              AV90WWUsuarioPerfilDS_3_Usuario_pessoanom1 ,
                                              AV91WWUsuarioPerfilDS_4_Dynamicfiltersenabled2 ,
                                              AV92WWUsuarioPerfilDS_5_Dynamicfiltersselector2 ,
                                              AV93WWUsuarioPerfilDS_6_Perfil_nome2 ,
                                              AV94WWUsuarioPerfilDS_7_Usuario_pessoanom2 ,
                                              AV96WWUsuarioPerfilDS_9_Tfusuario_nome_sel ,
                                              AV95WWUsuarioPerfilDS_8_Tfusuario_nome ,
                                              AV98WWUsuarioPerfilDS_11_Tfusuario_pessoanom_sel ,
                                              AV97WWUsuarioPerfilDS_10_Tfusuario_pessoanom ,
                                              AV100WWUsuarioPerfilDS_13_Tfperfil_nome_sel ,
                                              AV99WWUsuarioPerfilDS_12_Tfperfil_nome ,
                                              AV101WWUsuarioPerfilDS_14_Tfperfil_tipo_sels.Count ,
                                              AV102WWUsuarioPerfilDS_15_Tfusuarioperfil_insert_sel ,
                                              AV103WWUsuarioPerfilDS_16_Tfusuarioperfil_update_sel ,
                                              AV104WWUsuarioPerfilDS_17_Tfusuarioperfil_delete_sel ,
                                              AV105WWUsuarioPerfilDS_18_Tfusuarioperfil_display_sel ,
                                              A4Perfil_Nome ,
                                              A58Usuario_PessoaNom ,
                                              A2Usuario_Nome ,
                                              A544UsuarioPerfil_Insert ,
                                              A659UsuarioPerfil_Update ,
                                              A546UsuarioPerfil_Delete ,
                                              A543UsuarioPerfil_Display ,
                                              AV13OrderedBy ,
                                              AV14OrderedDsc },
                                              new int[] {
                                              TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN,
                                              TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN
                                              }
         });
         lV89WWUsuarioPerfilDS_2_Perfil_nome1 = StringUtil.PadR( StringUtil.RTrim( AV89WWUsuarioPerfilDS_2_Perfil_nome1), 50, "%");
         lV90WWUsuarioPerfilDS_3_Usuario_pessoanom1 = StringUtil.PadR( StringUtil.RTrim( AV90WWUsuarioPerfilDS_3_Usuario_pessoanom1), 100, "%");
         lV93WWUsuarioPerfilDS_6_Perfil_nome2 = StringUtil.PadR( StringUtil.RTrim( AV93WWUsuarioPerfilDS_6_Perfil_nome2), 50, "%");
         lV94WWUsuarioPerfilDS_7_Usuario_pessoanom2 = StringUtil.PadR( StringUtil.RTrim( AV94WWUsuarioPerfilDS_7_Usuario_pessoanom2), 100, "%");
         lV95WWUsuarioPerfilDS_8_Tfusuario_nome = StringUtil.PadR( StringUtil.RTrim( AV95WWUsuarioPerfilDS_8_Tfusuario_nome), 50, "%");
         lV97WWUsuarioPerfilDS_10_Tfusuario_pessoanom = StringUtil.PadR( StringUtil.RTrim( AV97WWUsuarioPerfilDS_10_Tfusuario_pessoanom), 100, "%");
         lV99WWUsuarioPerfilDS_12_Tfperfil_nome = StringUtil.PadR( StringUtil.RTrim( AV99WWUsuarioPerfilDS_12_Tfperfil_nome), 50, "%");
         /* Using cursor H008Y3 */
         pr_default.execute(1, new Object[] {lV89WWUsuarioPerfilDS_2_Perfil_nome1, lV90WWUsuarioPerfilDS_3_Usuario_pessoanom1, lV93WWUsuarioPerfilDS_6_Perfil_nome2, lV94WWUsuarioPerfilDS_7_Usuario_pessoanom2, lV95WWUsuarioPerfilDS_8_Tfusuario_nome, AV96WWUsuarioPerfilDS_9_Tfusuario_nome_sel, lV97WWUsuarioPerfilDS_10_Tfusuario_pessoanom, AV98WWUsuarioPerfilDS_11_Tfusuario_pessoanom_sel, lV99WWUsuarioPerfilDS_12_Tfperfil_nome, AV100WWUsuarioPerfilDS_13_Tfperfil_nome_sel});
         GRID_nRecordCount = H008Y3_AGRID_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         AV88WWUsuarioPerfilDS_1_Dynamicfiltersselector1 = AV16DynamicFiltersSelector1;
         AV89WWUsuarioPerfilDS_2_Perfil_nome1 = AV18Perfil_Nome1;
         AV90WWUsuarioPerfilDS_3_Usuario_pessoanom1 = AV19Usuario_PessoaNom1;
         AV91WWUsuarioPerfilDS_4_Dynamicfiltersenabled2 = AV20DynamicFiltersEnabled2;
         AV92WWUsuarioPerfilDS_5_Dynamicfiltersselector2 = AV21DynamicFiltersSelector2;
         AV93WWUsuarioPerfilDS_6_Perfil_nome2 = AV23Perfil_Nome2;
         AV94WWUsuarioPerfilDS_7_Usuario_pessoanom2 = AV24Usuario_PessoaNom2;
         AV95WWUsuarioPerfilDS_8_Tfusuario_nome = AV55TFUsuario_Nome;
         AV96WWUsuarioPerfilDS_9_Tfusuario_nome_sel = AV56TFUsuario_Nome_Sel;
         AV97WWUsuarioPerfilDS_10_Tfusuario_pessoanom = AV59TFUsuario_PessoaNom;
         AV98WWUsuarioPerfilDS_11_Tfusuario_pessoanom_sel = AV60TFUsuario_PessoaNom_Sel;
         AV99WWUsuarioPerfilDS_12_Tfperfil_nome = AV63TFPerfil_Nome;
         AV100WWUsuarioPerfilDS_13_Tfperfil_nome_sel = AV64TFPerfil_Nome_Sel;
         AV101WWUsuarioPerfilDS_14_Tfperfil_tipo_sels = AV68TFPerfil_Tipo_Sels;
         AV102WWUsuarioPerfilDS_15_Tfusuarioperfil_insert_sel = AV71TFUsuarioPerfil_Insert_Sel;
         AV103WWUsuarioPerfilDS_16_Tfusuarioperfil_update_sel = AV74TFUsuarioPerfil_Update_Sel;
         AV104WWUsuarioPerfilDS_17_Tfusuarioperfil_delete_sel = AV77TFUsuarioPerfil_Delete_Sel;
         AV105WWUsuarioPerfilDS_18_Tfusuarioperfil_display_sel = AV80TFUsuarioPerfil_Display_Sel;
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV16DynamicFiltersSelector1, AV18Perfil_Nome1, AV19Usuario_PessoaNom1, AV21DynamicFiltersSelector2, AV23Perfil_Nome2, AV24Usuario_PessoaNom2, AV20DynamicFiltersEnabled2, AV55TFUsuario_Nome, AV56TFUsuario_Nome_Sel, AV59TFUsuario_PessoaNom, AV60TFUsuario_PessoaNom_Sel, AV63TFPerfil_Nome, AV64TFPerfil_Nome_Sel, AV71TFUsuarioPerfil_Insert_Sel, AV74TFUsuarioPerfil_Update_Sel, AV77TFUsuarioPerfil_Delete_Sel, AV80TFUsuarioPerfil_Display_Sel, AV57ddo_Usuario_NomeTitleControlIdToReplace, AV61ddo_Usuario_PessoaNomTitleControlIdToReplace, AV65ddo_Perfil_NomeTitleControlIdToReplace, AV69ddo_Perfil_TipoTitleControlIdToReplace, AV72ddo_UsuarioPerfil_InsertTitleControlIdToReplace, AV75ddo_UsuarioPerfil_UpdateTitleControlIdToReplace, AV78ddo_UsuarioPerfil_DeleteTitleControlIdToReplace, AV81ddo_UsuarioPerfil_DisplayTitleControlIdToReplace, AV68TFPerfil_Tipo_Sels, AV108Pgmname, AV10GridState, AV26DynamicFiltersIgnoreFirst, AV25DynamicFiltersRemoving, A1Usuario_Codigo, A3Perfil_Codigo) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         AV88WWUsuarioPerfilDS_1_Dynamicfiltersselector1 = AV16DynamicFiltersSelector1;
         AV89WWUsuarioPerfilDS_2_Perfil_nome1 = AV18Perfil_Nome1;
         AV90WWUsuarioPerfilDS_3_Usuario_pessoanom1 = AV19Usuario_PessoaNom1;
         AV91WWUsuarioPerfilDS_4_Dynamicfiltersenabled2 = AV20DynamicFiltersEnabled2;
         AV92WWUsuarioPerfilDS_5_Dynamicfiltersselector2 = AV21DynamicFiltersSelector2;
         AV93WWUsuarioPerfilDS_6_Perfil_nome2 = AV23Perfil_Nome2;
         AV94WWUsuarioPerfilDS_7_Usuario_pessoanom2 = AV24Usuario_PessoaNom2;
         AV95WWUsuarioPerfilDS_8_Tfusuario_nome = AV55TFUsuario_Nome;
         AV96WWUsuarioPerfilDS_9_Tfusuario_nome_sel = AV56TFUsuario_Nome_Sel;
         AV97WWUsuarioPerfilDS_10_Tfusuario_pessoanom = AV59TFUsuario_PessoaNom;
         AV98WWUsuarioPerfilDS_11_Tfusuario_pessoanom_sel = AV60TFUsuario_PessoaNom_Sel;
         AV99WWUsuarioPerfilDS_12_Tfperfil_nome = AV63TFPerfil_Nome;
         AV100WWUsuarioPerfilDS_13_Tfperfil_nome_sel = AV64TFPerfil_Nome_Sel;
         AV101WWUsuarioPerfilDS_14_Tfperfil_tipo_sels = AV68TFPerfil_Tipo_Sels;
         AV102WWUsuarioPerfilDS_15_Tfusuarioperfil_insert_sel = AV71TFUsuarioPerfil_Insert_Sel;
         AV103WWUsuarioPerfilDS_16_Tfusuarioperfil_update_sel = AV74TFUsuarioPerfil_Update_Sel;
         AV104WWUsuarioPerfilDS_17_Tfusuarioperfil_delete_sel = AV77TFUsuarioPerfil_Delete_Sel;
         AV105WWUsuarioPerfilDS_18_Tfusuarioperfil_display_sel = AV80TFUsuarioPerfil_Display_Sel;
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV16DynamicFiltersSelector1, AV18Perfil_Nome1, AV19Usuario_PessoaNom1, AV21DynamicFiltersSelector2, AV23Perfil_Nome2, AV24Usuario_PessoaNom2, AV20DynamicFiltersEnabled2, AV55TFUsuario_Nome, AV56TFUsuario_Nome_Sel, AV59TFUsuario_PessoaNom, AV60TFUsuario_PessoaNom_Sel, AV63TFPerfil_Nome, AV64TFPerfil_Nome_Sel, AV71TFUsuarioPerfil_Insert_Sel, AV74TFUsuarioPerfil_Update_Sel, AV77TFUsuarioPerfil_Delete_Sel, AV80TFUsuarioPerfil_Display_Sel, AV57ddo_Usuario_NomeTitleControlIdToReplace, AV61ddo_Usuario_PessoaNomTitleControlIdToReplace, AV65ddo_Perfil_NomeTitleControlIdToReplace, AV69ddo_Perfil_TipoTitleControlIdToReplace, AV72ddo_UsuarioPerfil_InsertTitleControlIdToReplace, AV75ddo_UsuarioPerfil_UpdateTitleControlIdToReplace, AV78ddo_UsuarioPerfil_DeleteTitleControlIdToReplace, AV81ddo_UsuarioPerfil_DisplayTitleControlIdToReplace, AV68TFPerfil_Tipo_Sels, AV108Pgmname, AV10GridState, AV26DynamicFiltersIgnoreFirst, AV25DynamicFiltersRemoving, A1Usuario_Codigo, A3Perfil_Codigo) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         AV88WWUsuarioPerfilDS_1_Dynamicfiltersselector1 = AV16DynamicFiltersSelector1;
         AV89WWUsuarioPerfilDS_2_Perfil_nome1 = AV18Perfil_Nome1;
         AV90WWUsuarioPerfilDS_3_Usuario_pessoanom1 = AV19Usuario_PessoaNom1;
         AV91WWUsuarioPerfilDS_4_Dynamicfiltersenabled2 = AV20DynamicFiltersEnabled2;
         AV92WWUsuarioPerfilDS_5_Dynamicfiltersselector2 = AV21DynamicFiltersSelector2;
         AV93WWUsuarioPerfilDS_6_Perfil_nome2 = AV23Perfil_Nome2;
         AV94WWUsuarioPerfilDS_7_Usuario_pessoanom2 = AV24Usuario_PessoaNom2;
         AV95WWUsuarioPerfilDS_8_Tfusuario_nome = AV55TFUsuario_Nome;
         AV96WWUsuarioPerfilDS_9_Tfusuario_nome_sel = AV56TFUsuario_Nome_Sel;
         AV97WWUsuarioPerfilDS_10_Tfusuario_pessoanom = AV59TFUsuario_PessoaNom;
         AV98WWUsuarioPerfilDS_11_Tfusuario_pessoanom_sel = AV60TFUsuario_PessoaNom_Sel;
         AV99WWUsuarioPerfilDS_12_Tfperfil_nome = AV63TFPerfil_Nome;
         AV100WWUsuarioPerfilDS_13_Tfperfil_nome_sel = AV64TFPerfil_Nome_Sel;
         AV101WWUsuarioPerfilDS_14_Tfperfil_tipo_sels = AV68TFPerfil_Tipo_Sels;
         AV102WWUsuarioPerfilDS_15_Tfusuarioperfil_insert_sel = AV71TFUsuarioPerfil_Insert_Sel;
         AV103WWUsuarioPerfilDS_16_Tfusuarioperfil_update_sel = AV74TFUsuarioPerfil_Update_Sel;
         AV104WWUsuarioPerfilDS_17_Tfusuarioperfil_delete_sel = AV77TFUsuarioPerfil_Delete_Sel;
         AV105WWUsuarioPerfilDS_18_Tfusuarioperfil_display_sel = AV80TFUsuarioPerfil_Display_Sel;
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV16DynamicFiltersSelector1, AV18Perfil_Nome1, AV19Usuario_PessoaNom1, AV21DynamicFiltersSelector2, AV23Perfil_Nome2, AV24Usuario_PessoaNom2, AV20DynamicFiltersEnabled2, AV55TFUsuario_Nome, AV56TFUsuario_Nome_Sel, AV59TFUsuario_PessoaNom, AV60TFUsuario_PessoaNom_Sel, AV63TFPerfil_Nome, AV64TFPerfil_Nome_Sel, AV71TFUsuarioPerfil_Insert_Sel, AV74TFUsuarioPerfil_Update_Sel, AV77TFUsuarioPerfil_Delete_Sel, AV80TFUsuarioPerfil_Display_Sel, AV57ddo_Usuario_NomeTitleControlIdToReplace, AV61ddo_Usuario_PessoaNomTitleControlIdToReplace, AV65ddo_Perfil_NomeTitleControlIdToReplace, AV69ddo_Perfil_TipoTitleControlIdToReplace, AV72ddo_UsuarioPerfil_InsertTitleControlIdToReplace, AV75ddo_UsuarioPerfil_UpdateTitleControlIdToReplace, AV78ddo_UsuarioPerfil_DeleteTitleControlIdToReplace, AV81ddo_UsuarioPerfil_DisplayTitleControlIdToReplace, AV68TFPerfil_Tipo_Sels, AV108Pgmname, AV10GridState, AV26DynamicFiltersIgnoreFirst, AV25DynamicFiltersRemoving, A1Usuario_Codigo, A3Perfil_Codigo) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         AV88WWUsuarioPerfilDS_1_Dynamicfiltersselector1 = AV16DynamicFiltersSelector1;
         AV89WWUsuarioPerfilDS_2_Perfil_nome1 = AV18Perfil_Nome1;
         AV90WWUsuarioPerfilDS_3_Usuario_pessoanom1 = AV19Usuario_PessoaNom1;
         AV91WWUsuarioPerfilDS_4_Dynamicfiltersenabled2 = AV20DynamicFiltersEnabled2;
         AV92WWUsuarioPerfilDS_5_Dynamicfiltersselector2 = AV21DynamicFiltersSelector2;
         AV93WWUsuarioPerfilDS_6_Perfil_nome2 = AV23Perfil_Nome2;
         AV94WWUsuarioPerfilDS_7_Usuario_pessoanom2 = AV24Usuario_PessoaNom2;
         AV95WWUsuarioPerfilDS_8_Tfusuario_nome = AV55TFUsuario_Nome;
         AV96WWUsuarioPerfilDS_9_Tfusuario_nome_sel = AV56TFUsuario_Nome_Sel;
         AV97WWUsuarioPerfilDS_10_Tfusuario_pessoanom = AV59TFUsuario_PessoaNom;
         AV98WWUsuarioPerfilDS_11_Tfusuario_pessoanom_sel = AV60TFUsuario_PessoaNom_Sel;
         AV99WWUsuarioPerfilDS_12_Tfperfil_nome = AV63TFPerfil_Nome;
         AV100WWUsuarioPerfilDS_13_Tfperfil_nome_sel = AV64TFPerfil_Nome_Sel;
         AV101WWUsuarioPerfilDS_14_Tfperfil_tipo_sels = AV68TFPerfil_Tipo_Sels;
         AV102WWUsuarioPerfilDS_15_Tfusuarioperfil_insert_sel = AV71TFUsuarioPerfil_Insert_Sel;
         AV103WWUsuarioPerfilDS_16_Tfusuarioperfil_update_sel = AV74TFUsuarioPerfil_Update_Sel;
         AV104WWUsuarioPerfilDS_17_Tfusuarioperfil_delete_sel = AV77TFUsuarioPerfil_Delete_Sel;
         AV105WWUsuarioPerfilDS_18_Tfusuarioperfil_display_sel = AV80TFUsuarioPerfil_Display_Sel;
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV16DynamicFiltersSelector1, AV18Perfil_Nome1, AV19Usuario_PessoaNom1, AV21DynamicFiltersSelector2, AV23Perfil_Nome2, AV24Usuario_PessoaNom2, AV20DynamicFiltersEnabled2, AV55TFUsuario_Nome, AV56TFUsuario_Nome_Sel, AV59TFUsuario_PessoaNom, AV60TFUsuario_PessoaNom_Sel, AV63TFPerfil_Nome, AV64TFPerfil_Nome_Sel, AV71TFUsuarioPerfil_Insert_Sel, AV74TFUsuarioPerfil_Update_Sel, AV77TFUsuarioPerfil_Delete_Sel, AV80TFUsuarioPerfil_Display_Sel, AV57ddo_Usuario_NomeTitleControlIdToReplace, AV61ddo_Usuario_PessoaNomTitleControlIdToReplace, AV65ddo_Perfil_NomeTitleControlIdToReplace, AV69ddo_Perfil_TipoTitleControlIdToReplace, AV72ddo_UsuarioPerfil_InsertTitleControlIdToReplace, AV75ddo_UsuarioPerfil_UpdateTitleControlIdToReplace, AV78ddo_UsuarioPerfil_DeleteTitleControlIdToReplace, AV81ddo_UsuarioPerfil_DisplayTitleControlIdToReplace, AV68TFPerfil_Tipo_Sels, AV108Pgmname, AV10GridState, AV26DynamicFiltersIgnoreFirst, AV25DynamicFiltersRemoving, A1Usuario_Codigo, A3Perfil_Codigo) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         AV88WWUsuarioPerfilDS_1_Dynamicfiltersselector1 = AV16DynamicFiltersSelector1;
         AV89WWUsuarioPerfilDS_2_Perfil_nome1 = AV18Perfil_Nome1;
         AV90WWUsuarioPerfilDS_3_Usuario_pessoanom1 = AV19Usuario_PessoaNom1;
         AV91WWUsuarioPerfilDS_4_Dynamicfiltersenabled2 = AV20DynamicFiltersEnabled2;
         AV92WWUsuarioPerfilDS_5_Dynamicfiltersselector2 = AV21DynamicFiltersSelector2;
         AV93WWUsuarioPerfilDS_6_Perfil_nome2 = AV23Perfil_Nome2;
         AV94WWUsuarioPerfilDS_7_Usuario_pessoanom2 = AV24Usuario_PessoaNom2;
         AV95WWUsuarioPerfilDS_8_Tfusuario_nome = AV55TFUsuario_Nome;
         AV96WWUsuarioPerfilDS_9_Tfusuario_nome_sel = AV56TFUsuario_Nome_Sel;
         AV97WWUsuarioPerfilDS_10_Tfusuario_pessoanom = AV59TFUsuario_PessoaNom;
         AV98WWUsuarioPerfilDS_11_Tfusuario_pessoanom_sel = AV60TFUsuario_PessoaNom_Sel;
         AV99WWUsuarioPerfilDS_12_Tfperfil_nome = AV63TFPerfil_Nome;
         AV100WWUsuarioPerfilDS_13_Tfperfil_nome_sel = AV64TFPerfil_Nome_Sel;
         AV101WWUsuarioPerfilDS_14_Tfperfil_tipo_sels = AV68TFPerfil_Tipo_Sels;
         AV102WWUsuarioPerfilDS_15_Tfusuarioperfil_insert_sel = AV71TFUsuarioPerfil_Insert_Sel;
         AV103WWUsuarioPerfilDS_16_Tfusuarioperfil_update_sel = AV74TFUsuarioPerfil_Update_Sel;
         AV104WWUsuarioPerfilDS_17_Tfusuarioperfil_delete_sel = AV77TFUsuarioPerfil_Delete_Sel;
         AV105WWUsuarioPerfilDS_18_Tfusuarioperfil_display_sel = AV80TFUsuarioPerfil_Display_Sel;
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV16DynamicFiltersSelector1, AV18Perfil_Nome1, AV19Usuario_PessoaNom1, AV21DynamicFiltersSelector2, AV23Perfil_Nome2, AV24Usuario_PessoaNom2, AV20DynamicFiltersEnabled2, AV55TFUsuario_Nome, AV56TFUsuario_Nome_Sel, AV59TFUsuario_PessoaNom, AV60TFUsuario_PessoaNom_Sel, AV63TFPerfil_Nome, AV64TFPerfil_Nome_Sel, AV71TFUsuarioPerfil_Insert_Sel, AV74TFUsuarioPerfil_Update_Sel, AV77TFUsuarioPerfil_Delete_Sel, AV80TFUsuarioPerfil_Display_Sel, AV57ddo_Usuario_NomeTitleControlIdToReplace, AV61ddo_Usuario_PessoaNomTitleControlIdToReplace, AV65ddo_Perfil_NomeTitleControlIdToReplace, AV69ddo_Perfil_TipoTitleControlIdToReplace, AV72ddo_UsuarioPerfil_InsertTitleControlIdToReplace, AV75ddo_UsuarioPerfil_UpdateTitleControlIdToReplace, AV78ddo_UsuarioPerfil_DeleteTitleControlIdToReplace, AV81ddo_UsuarioPerfil_DisplayTitleControlIdToReplace, AV68TFPerfil_Tipo_Sels, AV108Pgmname, AV10GridState, AV26DynamicFiltersIgnoreFirst, AV25DynamicFiltersRemoving, A1Usuario_Codigo, A3Perfil_Codigo) ;
         }
         return (int)(0) ;
      }

      protected void STRUP8Y0( )
      {
         /* Before Start, stand alone formulas. */
         AV108Pgmname = "WWUsuarioPerfil";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E288Y2 */
         E288Y2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "vDDO_TITLESETTINGSICONS"), AV82DDO_TitleSettingsIcons);
            ajax_req_read_hidden_sdt(cgiGet( "vUSUARIO_NOMETITLEFILTERDATA"), AV54Usuario_NomeTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vUSUARIO_PESSOANOMTITLEFILTERDATA"), AV58Usuario_PessoaNomTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vPERFIL_NOMETITLEFILTERDATA"), AV62Perfil_NomeTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vPERFIL_TIPOTITLEFILTERDATA"), AV66Perfil_TipoTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vUSUARIOPERFIL_INSERTTITLEFILTERDATA"), AV70UsuarioPerfil_InsertTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vUSUARIOPERFIL_UPDATETITLEFILTERDATA"), AV73UsuarioPerfil_UpdateTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vUSUARIOPERFIL_DELETETITLEFILTERDATA"), AV76UsuarioPerfil_DeleteTitleFilterData);
            ajax_req_read_hidden_sdt(cgiGet( "vUSUARIOPERFIL_DISPLAYTITLEFILTERDATA"), AV79UsuarioPerfil_DisplayTitleFilterData);
            /* Read variables values. */
            cmbavOrderedby.Name = cmbavOrderedby_Internalname;
            cmbavOrderedby.CurrentValue = cgiGet( cmbavOrderedby_Internalname);
            AV13OrderedBy = (short)(NumberUtil.Val( cgiGet( cmbavOrderedby_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            cmbavDynamicfiltersselector1.Name = cmbavDynamicfiltersselector1_Internalname;
            cmbavDynamicfiltersselector1.CurrentValue = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            AV16DynamicFiltersSelector1 = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
            AV18Perfil_Nome1 = StringUtil.Upper( cgiGet( edtavPerfil_nome1_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18Perfil_Nome1", AV18Perfil_Nome1);
            AV19Usuario_PessoaNom1 = StringUtil.Upper( cgiGet( edtavUsuario_pessoanom1_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19Usuario_PessoaNom1", AV19Usuario_PessoaNom1);
            cmbavDynamicfiltersselector2.Name = cmbavDynamicfiltersselector2_Internalname;
            cmbavDynamicfiltersselector2.CurrentValue = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            AV21DynamicFiltersSelector2 = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
            AV23Perfil_Nome2 = StringUtil.Upper( cgiGet( edtavPerfil_nome2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23Perfil_Nome2", AV23Perfil_Nome2);
            AV24Usuario_PessoaNom2 = StringUtil.Upper( cgiGet( edtavUsuario_pessoanom2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24Usuario_PessoaNom2", AV24Usuario_PessoaNom2);
            AV20DynamicFiltersEnabled2 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled2", AV20DynamicFiltersEnabled2);
            AV55TFUsuario_Nome = StringUtil.Upper( cgiGet( edtavTfusuario_nome_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55TFUsuario_Nome", AV55TFUsuario_Nome);
            AV56TFUsuario_Nome_Sel = StringUtil.Upper( cgiGet( edtavTfusuario_nome_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56TFUsuario_Nome_Sel", AV56TFUsuario_Nome_Sel);
            AV59TFUsuario_PessoaNom = StringUtil.Upper( cgiGet( edtavTfusuario_pessoanom_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59TFUsuario_PessoaNom", AV59TFUsuario_PessoaNom);
            AV60TFUsuario_PessoaNom_Sel = StringUtil.Upper( cgiGet( edtavTfusuario_pessoanom_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60TFUsuario_PessoaNom_Sel", AV60TFUsuario_PessoaNom_Sel);
            AV63TFPerfil_Nome = StringUtil.Upper( cgiGet( edtavTfperfil_nome_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63TFPerfil_Nome", AV63TFPerfil_Nome);
            AV64TFPerfil_Nome_Sel = StringUtil.Upper( cgiGet( edtavTfperfil_nome_sel_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64TFPerfil_Nome_Sel", AV64TFPerfil_Nome_Sel);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfusuarioperfil_insert_sel_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfusuarioperfil_insert_sel_Internalname), ",", ".") > Convert.ToDecimal( 9 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFUSUARIOPERFIL_INSERT_SEL");
               GX_FocusControl = edtavTfusuarioperfil_insert_sel_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV71TFUsuarioPerfil_Insert_Sel = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71TFUsuarioPerfil_Insert_Sel", StringUtil.Str( (decimal)(AV71TFUsuarioPerfil_Insert_Sel), 1, 0));
            }
            else
            {
               AV71TFUsuarioPerfil_Insert_Sel = (short)(context.localUtil.CToN( cgiGet( edtavTfusuarioperfil_insert_sel_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71TFUsuarioPerfil_Insert_Sel", StringUtil.Str( (decimal)(AV71TFUsuarioPerfil_Insert_Sel), 1, 0));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfusuarioperfil_update_sel_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfusuarioperfil_update_sel_Internalname), ",", ".") > Convert.ToDecimal( 9 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFUSUARIOPERFIL_UPDATE_SEL");
               GX_FocusControl = edtavTfusuarioperfil_update_sel_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV74TFUsuarioPerfil_Update_Sel = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV74TFUsuarioPerfil_Update_Sel", StringUtil.Str( (decimal)(AV74TFUsuarioPerfil_Update_Sel), 1, 0));
            }
            else
            {
               AV74TFUsuarioPerfil_Update_Sel = (short)(context.localUtil.CToN( cgiGet( edtavTfusuarioperfil_update_sel_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV74TFUsuarioPerfil_Update_Sel", StringUtil.Str( (decimal)(AV74TFUsuarioPerfil_Update_Sel), 1, 0));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfusuarioperfil_delete_sel_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfusuarioperfil_delete_sel_Internalname), ",", ".") > Convert.ToDecimal( 9 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFUSUARIOPERFIL_DELETE_SEL");
               GX_FocusControl = edtavTfusuarioperfil_delete_sel_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV77TFUsuarioPerfil_Delete_Sel = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV77TFUsuarioPerfil_Delete_Sel", StringUtil.Str( (decimal)(AV77TFUsuarioPerfil_Delete_Sel), 1, 0));
            }
            else
            {
               AV77TFUsuarioPerfil_Delete_Sel = (short)(context.localUtil.CToN( cgiGet( edtavTfusuarioperfil_delete_sel_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV77TFUsuarioPerfil_Delete_Sel", StringUtil.Str( (decimal)(AV77TFUsuarioPerfil_Delete_Sel), 1, 0));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavTfusuarioperfil_display_sel_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavTfusuarioperfil_display_sel_Internalname), ",", ".") > Convert.ToDecimal( 9 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vTFUSUARIOPERFIL_DISPLAY_SEL");
               GX_FocusControl = edtavTfusuarioperfil_display_sel_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV80TFUsuarioPerfil_Display_Sel = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV80TFUsuarioPerfil_Display_Sel", StringUtil.Str( (decimal)(AV80TFUsuarioPerfil_Display_Sel), 1, 0));
            }
            else
            {
               AV80TFUsuarioPerfil_Display_Sel = (short)(context.localUtil.CToN( cgiGet( edtavTfusuarioperfil_display_sel_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV80TFUsuarioPerfil_Display_Sel", StringUtil.Str( (decimal)(AV80TFUsuarioPerfil_Display_Sel), 1, 0));
            }
            AV57ddo_Usuario_NomeTitleControlIdToReplace = cgiGet( edtavDdo_usuario_nometitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57ddo_Usuario_NomeTitleControlIdToReplace", AV57ddo_Usuario_NomeTitleControlIdToReplace);
            AV61ddo_Usuario_PessoaNomTitleControlIdToReplace = cgiGet( edtavDdo_usuario_pessoanomtitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61ddo_Usuario_PessoaNomTitleControlIdToReplace", AV61ddo_Usuario_PessoaNomTitleControlIdToReplace);
            AV65ddo_Perfil_NomeTitleControlIdToReplace = cgiGet( edtavDdo_perfil_nometitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65ddo_Perfil_NomeTitleControlIdToReplace", AV65ddo_Perfil_NomeTitleControlIdToReplace);
            AV69ddo_Perfil_TipoTitleControlIdToReplace = cgiGet( edtavDdo_perfil_tipotitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69ddo_Perfil_TipoTitleControlIdToReplace", AV69ddo_Perfil_TipoTitleControlIdToReplace);
            AV72ddo_UsuarioPerfil_InsertTitleControlIdToReplace = cgiGet( edtavDdo_usuarioperfil_inserttitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV72ddo_UsuarioPerfil_InsertTitleControlIdToReplace", AV72ddo_UsuarioPerfil_InsertTitleControlIdToReplace);
            AV75ddo_UsuarioPerfil_UpdateTitleControlIdToReplace = cgiGet( edtavDdo_usuarioperfil_updatetitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV75ddo_UsuarioPerfil_UpdateTitleControlIdToReplace", AV75ddo_UsuarioPerfil_UpdateTitleControlIdToReplace);
            AV78ddo_UsuarioPerfil_DeleteTitleControlIdToReplace = cgiGet( edtavDdo_usuarioperfil_deletetitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV78ddo_UsuarioPerfil_DeleteTitleControlIdToReplace", AV78ddo_UsuarioPerfil_DeleteTitleControlIdToReplace);
            AV81ddo_UsuarioPerfil_DisplayTitleControlIdToReplace = cgiGet( edtavDdo_usuarioperfil_displaytitlecontrolidtoreplace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV81ddo_UsuarioPerfil_DisplayTitleControlIdToReplace", AV81ddo_UsuarioPerfil_DisplayTitleControlIdToReplace);
            /* Read saved values. */
            nRC_GXsfl_63 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_63"), ",", "."));
            AV84GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( "vGRIDCURRENTPAGE"), ",", "."));
            AV85GridPageCount = (long)(context.localUtil.CToN( cgiGet( "vGRIDPAGECOUNT"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( "GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( "GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( "GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( "GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( "GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( "GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( "GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( "GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( "GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( "GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( "GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( "GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( "GRIDPAGINATIONBAR_Emptygridcaption");
            Ddo_usuario_nome_Caption = cgiGet( "DDO_USUARIO_NOME_Caption");
            Ddo_usuario_nome_Tooltip = cgiGet( "DDO_USUARIO_NOME_Tooltip");
            Ddo_usuario_nome_Cls = cgiGet( "DDO_USUARIO_NOME_Cls");
            Ddo_usuario_nome_Filteredtext_set = cgiGet( "DDO_USUARIO_NOME_Filteredtext_set");
            Ddo_usuario_nome_Selectedvalue_set = cgiGet( "DDO_USUARIO_NOME_Selectedvalue_set");
            Ddo_usuario_nome_Dropdownoptionstype = cgiGet( "DDO_USUARIO_NOME_Dropdownoptionstype");
            Ddo_usuario_nome_Titlecontrolidtoreplace = cgiGet( "DDO_USUARIO_NOME_Titlecontrolidtoreplace");
            Ddo_usuario_nome_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_USUARIO_NOME_Includesortasc"));
            Ddo_usuario_nome_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_USUARIO_NOME_Includesortdsc"));
            Ddo_usuario_nome_Sortedstatus = cgiGet( "DDO_USUARIO_NOME_Sortedstatus");
            Ddo_usuario_nome_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_USUARIO_NOME_Includefilter"));
            Ddo_usuario_nome_Filtertype = cgiGet( "DDO_USUARIO_NOME_Filtertype");
            Ddo_usuario_nome_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_USUARIO_NOME_Filterisrange"));
            Ddo_usuario_nome_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_USUARIO_NOME_Includedatalist"));
            Ddo_usuario_nome_Datalisttype = cgiGet( "DDO_USUARIO_NOME_Datalisttype");
            Ddo_usuario_nome_Datalistproc = cgiGet( "DDO_USUARIO_NOME_Datalistproc");
            Ddo_usuario_nome_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_USUARIO_NOME_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_usuario_nome_Sortasc = cgiGet( "DDO_USUARIO_NOME_Sortasc");
            Ddo_usuario_nome_Sortdsc = cgiGet( "DDO_USUARIO_NOME_Sortdsc");
            Ddo_usuario_nome_Loadingdata = cgiGet( "DDO_USUARIO_NOME_Loadingdata");
            Ddo_usuario_nome_Cleanfilter = cgiGet( "DDO_USUARIO_NOME_Cleanfilter");
            Ddo_usuario_nome_Noresultsfound = cgiGet( "DDO_USUARIO_NOME_Noresultsfound");
            Ddo_usuario_nome_Searchbuttontext = cgiGet( "DDO_USUARIO_NOME_Searchbuttontext");
            Ddo_usuario_pessoanom_Caption = cgiGet( "DDO_USUARIO_PESSOANOM_Caption");
            Ddo_usuario_pessoanom_Tooltip = cgiGet( "DDO_USUARIO_PESSOANOM_Tooltip");
            Ddo_usuario_pessoanom_Cls = cgiGet( "DDO_USUARIO_PESSOANOM_Cls");
            Ddo_usuario_pessoanom_Filteredtext_set = cgiGet( "DDO_USUARIO_PESSOANOM_Filteredtext_set");
            Ddo_usuario_pessoanom_Selectedvalue_set = cgiGet( "DDO_USUARIO_PESSOANOM_Selectedvalue_set");
            Ddo_usuario_pessoanom_Dropdownoptionstype = cgiGet( "DDO_USUARIO_PESSOANOM_Dropdownoptionstype");
            Ddo_usuario_pessoanom_Titlecontrolidtoreplace = cgiGet( "DDO_USUARIO_PESSOANOM_Titlecontrolidtoreplace");
            Ddo_usuario_pessoanom_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_USUARIO_PESSOANOM_Includesortasc"));
            Ddo_usuario_pessoanom_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_USUARIO_PESSOANOM_Includesortdsc"));
            Ddo_usuario_pessoanom_Sortedstatus = cgiGet( "DDO_USUARIO_PESSOANOM_Sortedstatus");
            Ddo_usuario_pessoanom_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_USUARIO_PESSOANOM_Includefilter"));
            Ddo_usuario_pessoanom_Filtertype = cgiGet( "DDO_USUARIO_PESSOANOM_Filtertype");
            Ddo_usuario_pessoanom_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_USUARIO_PESSOANOM_Filterisrange"));
            Ddo_usuario_pessoanom_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_USUARIO_PESSOANOM_Includedatalist"));
            Ddo_usuario_pessoanom_Datalisttype = cgiGet( "DDO_USUARIO_PESSOANOM_Datalisttype");
            Ddo_usuario_pessoanom_Datalistproc = cgiGet( "DDO_USUARIO_PESSOANOM_Datalistproc");
            Ddo_usuario_pessoanom_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_USUARIO_PESSOANOM_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_usuario_pessoanom_Sortasc = cgiGet( "DDO_USUARIO_PESSOANOM_Sortasc");
            Ddo_usuario_pessoanom_Sortdsc = cgiGet( "DDO_USUARIO_PESSOANOM_Sortdsc");
            Ddo_usuario_pessoanom_Loadingdata = cgiGet( "DDO_USUARIO_PESSOANOM_Loadingdata");
            Ddo_usuario_pessoanom_Cleanfilter = cgiGet( "DDO_USUARIO_PESSOANOM_Cleanfilter");
            Ddo_usuario_pessoanom_Noresultsfound = cgiGet( "DDO_USUARIO_PESSOANOM_Noresultsfound");
            Ddo_usuario_pessoanom_Searchbuttontext = cgiGet( "DDO_USUARIO_PESSOANOM_Searchbuttontext");
            Ddo_perfil_nome_Caption = cgiGet( "DDO_PERFIL_NOME_Caption");
            Ddo_perfil_nome_Tooltip = cgiGet( "DDO_PERFIL_NOME_Tooltip");
            Ddo_perfil_nome_Cls = cgiGet( "DDO_PERFIL_NOME_Cls");
            Ddo_perfil_nome_Filteredtext_set = cgiGet( "DDO_PERFIL_NOME_Filteredtext_set");
            Ddo_perfil_nome_Selectedvalue_set = cgiGet( "DDO_PERFIL_NOME_Selectedvalue_set");
            Ddo_perfil_nome_Dropdownoptionstype = cgiGet( "DDO_PERFIL_NOME_Dropdownoptionstype");
            Ddo_perfil_nome_Titlecontrolidtoreplace = cgiGet( "DDO_PERFIL_NOME_Titlecontrolidtoreplace");
            Ddo_perfil_nome_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_PERFIL_NOME_Includesortasc"));
            Ddo_perfil_nome_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_PERFIL_NOME_Includesortdsc"));
            Ddo_perfil_nome_Sortedstatus = cgiGet( "DDO_PERFIL_NOME_Sortedstatus");
            Ddo_perfil_nome_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_PERFIL_NOME_Includefilter"));
            Ddo_perfil_nome_Filtertype = cgiGet( "DDO_PERFIL_NOME_Filtertype");
            Ddo_perfil_nome_Filterisrange = StringUtil.StrToBool( cgiGet( "DDO_PERFIL_NOME_Filterisrange"));
            Ddo_perfil_nome_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_PERFIL_NOME_Includedatalist"));
            Ddo_perfil_nome_Datalisttype = cgiGet( "DDO_PERFIL_NOME_Datalisttype");
            Ddo_perfil_nome_Datalistproc = cgiGet( "DDO_PERFIL_NOME_Datalistproc");
            Ddo_perfil_nome_Datalistupdateminimumcharacters = (int)(context.localUtil.CToN( cgiGet( "DDO_PERFIL_NOME_Datalistupdateminimumcharacters"), ",", "."));
            Ddo_perfil_nome_Sortasc = cgiGet( "DDO_PERFIL_NOME_Sortasc");
            Ddo_perfil_nome_Sortdsc = cgiGet( "DDO_PERFIL_NOME_Sortdsc");
            Ddo_perfil_nome_Loadingdata = cgiGet( "DDO_PERFIL_NOME_Loadingdata");
            Ddo_perfil_nome_Cleanfilter = cgiGet( "DDO_PERFIL_NOME_Cleanfilter");
            Ddo_perfil_nome_Noresultsfound = cgiGet( "DDO_PERFIL_NOME_Noresultsfound");
            Ddo_perfil_nome_Searchbuttontext = cgiGet( "DDO_PERFIL_NOME_Searchbuttontext");
            Ddo_perfil_tipo_Caption = cgiGet( "DDO_PERFIL_TIPO_Caption");
            Ddo_perfil_tipo_Tooltip = cgiGet( "DDO_PERFIL_TIPO_Tooltip");
            Ddo_perfil_tipo_Cls = cgiGet( "DDO_PERFIL_TIPO_Cls");
            Ddo_perfil_tipo_Selectedvalue_set = cgiGet( "DDO_PERFIL_TIPO_Selectedvalue_set");
            Ddo_perfil_tipo_Dropdownoptionstype = cgiGet( "DDO_PERFIL_TIPO_Dropdownoptionstype");
            Ddo_perfil_tipo_Titlecontrolidtoreplace = cgiGet( "DDO_PERFIL_TIPO_Titlecontrolidtoreplace");
            Ddo_perfil_tipo_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_PERFIL_TIPO_Includesortasc"));
            Ddo_perfil_tipo_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_PERFIL_TIPO_Includesortdsc"));
            Ddo_perfil_tipo_Sortedstatus = cgiGet( "DDO_PERFIL_TIPO_Sortedstatus");
            Ddo_perfil_tipo_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_PERFIL_TIPO_Includefilter"));
            Ddo_perfil_tipo_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_PERFIL_TIPO_Includedatalist"));
            Ddo_perfil_tipo_Datalisttype = cgiGet( "DDO_PERFIL_TIPO_Datalisttype");
            Ddo_perfil_tipo_Allowmultipleselection = StringUtil.StrToBool( cgiGet( "DDO_PERFIL_TIPO_Allowmultipleselection"));
            Ddo_perfil_tipo_Datalistfixedvalues = cgiGet( "DDO_PERFIL_TIPO_Datalistfixedvalues");
            Ddo_perfil_tipo_Sortasc = cgiGet( "DDO_PERFIL_TIPO_Sortasc");
            Ddo_perfil_tipo_Sortdsc = cgiGet( "DDO_PERFIL_TIPO_Sortdsc");
            Ddo_perfil_tipo_Cleanfilter = cgiGet( "DDO_PERFIL_TIPO_Cleanfilter");
            Ddo_perfil_tipo_Searchbuttontext = cgiGet( "DDO_PERFIL_TIPO_Searchbuttontext");
            Ddo_usuarioperfil_insert_Caption = cgiGet( "DDO_USUARIOPERFIL_INSERT_Caption");
            Ddo_usuarioperfil_insert_Tooltip = cgiGet( "DDO_USUARIOPERFIL_INSERT_Tooltip");
            Ddo_usuarioperfil_insert_Cls = cgiGet( "DDO_USUARIOPERFIL_INSERT_Cls");
            Ddo_usuarioperfil_insert_Selectedvalue_set = cgiGet( "DDO_USUARIOPERFIL_INSERT_Selectedvalue_set");
            Ddo_usuarioperfil_insert_Dropdownoptionstype = cgiGet( "DDO_USUARIOPERFIL_INSERT_Dropdownoptionstype");
            Ddo_usuarioperfil_insert_Titlecontrolidtoreplace = cgiGet( "DDO_USUARIOPERFIL_INSERT_Titlecontrolidtoreplace");
            Ddo_usuarioperfil_insert_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_USUARIOPERFIL_INSERT_Includesortasc"));
            Ddo_usuarioperfil_insert_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_USUARIOPERFIL_INSERT_Includesortdsc"));
            Ddo_usuarioperfil_insert_Sortedstatus = cgiGet( "DDO_USUARIOPERFIL_INSERT_Sortedstatus");
            Ddo_usuarioperfil_insert_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_USUARIOPERFIL_INSERT_Includefilter"));
            Ddo_usuarioperfil_insert_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_USUARIOPERFIL_INSERT_Includedatalist"));
            Ddo_usuarioperfil_insert_Datalisttype = cgiGet( "DDO_USUARIOPERFIL_INSERT_Datalisttype");
            Ddo_usuarioperfil_insert_Datalistfixedvalues = cgiGet( "DDO_USUARIOPERFIL_INSERT_Datalistfixedvalues");
            Ddo_usuarioperfil_insert_Sortasc = cgiGet( "DDO_USUARIOPERFIL_INSERT_Sortasc");
            Ddo_usuarioperfil_insert_Sortdsc = cgiGet( "DDO_USUARIOPERFIL_INSERT_Sortdsc");
            Ddo_usuarioperfil_insert_Cleanfilter = cgiGet( "DDO_USUARIOPERFIL_INSERT_Cleanfilter");
            Ddo_usuarioperfil_insert_Searchbuttontext = cgiGet( "DDO_USUARIOPERFIL_INSERT_Searchbuttontext");
            Ddo_usuarioperfil_update_Caption = cgiGet( "DDO_USUARIOPERFIL_UPDATE_Caption");
            Ddo_usuarioperfil_update_Tooltip = cgiGet( "DDO_USUARIOPERFIL_UPDATE_Tooltip");
            Ddo_usuarioperfil_update_Cls = cgiGet( "DDO_USUARIOPERFIL_UPDATE_Cls");
            Ddo_usuarioperfil_update_Selectedvalue_set = cgiGet( "DDO_USUARIOPERFIL_UPDATE_Selectedvalue_set");
            Ddo_usuarioperfil_update_Dropdownoptionstype = cgiGet( "DDO_USUARIOPERFIL_UPDATE_Dropdownoptionstype");
            Ddo_usuarioperfil_update_Titlecontrolidtoreplace = cgiGet( "DDO_USUARIOPERFIL_UPDATE_Titlecontrolidtoreplace");
            Ddo_usuarioperfil_update_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_USUARIOPERFIL_UPDATE_Includesortasc"));
            Ddo_usuarioperfil_update_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_USUARIOPERFIL_UPDATE_Includesortdsc"));
            Ddo_usuarioperfil_update_Sortedstatus = cgiGet( "DDO_USUARIOPERFIL_UPDATE_Sortedstatus");
            Ddo_usuarioperfil_update_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_USUARIOPERFIL_UPDATE_Includefilter"));
            Ddo_usuarioperfil_update_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_USUARIOPERFIL_UPDATE_Includedatalist"));
            Ddo_usuarioperfil_update_Datalisttype = cgiGet( "DDO_USUARIOPERFIL_UPDATE_Datalisttype");
            Ddo_usuarioperfil_update_Datalistfixedvalues = cgiGet( "DDO_USUARIOPERFIL_UPDATE_Datalistfixedvalues");
            Ddo_usuarioperfil_update_Sortasc = cgiGet( "DDO_USUARIOPERFIL_UPDATE_Sortasc");
            Ddo_usuarioperfil_update_Sortdsc = cgiGet( "DDO_USUARIOPERFIL_UPDATE_Sortdsc");
            Ddo_usuarioperfil_update_Cleanfilter = cgiGet( "DDO_USUARIOPERFIL_UPDATE_Cleanfilter");
            Ddo_usuarioperfil_update_Searchbuttontext = cgiGet( "DDO_USUARIOPERFIL_UPDATE_Searchbuttontext");
            Ddo_usuarioperfil_delete_Caption = cgiGet( "DDO_USUARIOPERFIL_DELETE_Caption");
            Ddo_usuarioperfil_delete_Tooltip = cgiGet( "DDO_USUARIOPERFIL_DELETE_Tooltip");
            Ddo_usuarioperfil_delete_Cls = cgiGet( "DDO_USUARIOPERFIL_DELETE_Cls");
            Ddo_usuarioperfil_delete_Selectedvalue_set = cgiGet( "DDO_USUARIOPERFIL_DELETE_Selectedvalue_set");
            Ddo_usuarioperfil_delete_Dropdownoptionstype = cgiGet( "DDO_USUARIOPERFIL_DELETE_Dropdownoptionstype");
            Ddo_usuarioperfil_delete_Titlecontrolidtoreplace = cgiGet( "DDO_USUARIOPERFIL_DELETE_Titlecontrolidtoreplace");
            Ddo_usuarioperfil_delete_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_USUARIOPERFIL_DELETE_Includesortasc"));
            Ddo_usuarioperfil_delete_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_USUARIOPERFIL_DELETE_Includesortdsc"));
            Ddo_usuarioperfil_delete_Sortedstatus = cgiGet( "DDO_USUARIOPERFIL_DELETE_Sortedstatus");
            Ddo_usuarioperfil_delete_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_USUARIOPERFIL_DELETE_Includefilter"));
            Ddo_usuarioperfil_delete_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_USUARIOPERFIL_DELETE_Includedatalist"));
            Ddo_usuarioperfil_delete_Datalisttype = cgiGet( "DDO_USUARIOPERFIL_DELETE_Datalisttype");
            Ddo_usuarioperfil_delete_Datalistfixedvalues = cgiGet( "DDO_USUARIOPERFIL_DELETE_Datalistfixedvalues");
            Ddo_usuarioperfil_delete_Sortasc = cgiGet( "DDO_USUARIOPERFIL_DELETE_Sortasc");
            Ddo_usuarioperfil_delete_Sortdsc = cgiGet( "DDO_USUARIOPERFIL_DELETE_Sortdsc");
            Ddo_usuarioperfil_delete_Cleanfilter = cgiGet( "DDO_USUARIOPERFIL_DELETE_Cleanfilter");
            Ddo_usuarioperfil_delete_Searchbuttontext = cgiGet( "DDO_USUARIOPERFIL_DELETE_Searchbuttontext");
            Ddo_usuarioperfil_display_Caption = cgiGet( "DDO_USUARIOPERFIL_DISPLAY_Caption");
            Ddo_usuarioperfil_display_Tooltip = cgiGet( "DDO_USUARIOPERFIL_DISPLAY_Tooltip");
            Ddo_usuarioperfil_display_Cls = cgiGet( "DDO_USUARIOPERFIL_DISPLAY_Cls");
            Ddo_usuarioperfil_display_Selectedvalue_set = cgiGet( "DDO_USUARIOPERFIL_DISPLAY_Selectedvalue_set");
            Ddo_usuarioperfil_display_Dropdownoptionstype = cgiGet( "DDO_USUARIOPERFIL_DISPLAY_Dropdownoptionstype");
            Ddo_usuarioperfil_display_Titlecontrolidtoreplace = cgiGet( "DDO_USUARIOPERFIL_DISPLAY_Titlecontrolidtoreplace");
            Ddo_usuarioperfil_display_Includesortasc = StringUtil.StrToBool( cgiGet( "DDO_USUARIOPERFIL_DISPLAY_Includesortasc"));
            Ddo_usuarioperfil_display_Includesortdsc = StringUtil.StrToBool( cgiGet( "DDO_USUARIOPERFIL_DISPLAY_Includesortdsc"));
            Ddo_usuarioperfil_display_Sortedstatus = cgiGet( "DDO_USUARIOPERFIL_DISPLAY_Sortedstatus");
            Ddo_usuarioperfil_display_Includefilter = StringUtil.StrToBool( cgiGet( "DDO_USUARIOPERFIL_DISPLAY_Includefilter"));
            Ddo_usuarioperfil_display_Includedatalist = StringUtil.StrToBool( cgiGet( "DDO_USUARIOPERFIL_DISPLAY_Includedatalist"));
            Ddo_usuarioperfil_display_Datalisttype = cgiGet( "DDO_USUARIOPERFIL_DISPLAY_Datalisttype");
            Ddo_usuarioperfil_display_Datalistfixedvalues = cgiGet( "DDO_USUARIOPERFIL_DISPLAY_Datalistfixedvalues");
            Ddo_usuarioperfil_display_Sortasc = cgiGet( "DDO_USUARIOPERFIL_DISPLAY_Sortasc");
            Ddo_usuarioperfil_display_Sortdsc = cgiGet( "DDO_USUARIOPERFIL_DISPLAY_Sortdsc");
            Ddo_usuarioperfil_display_Cleanfilter = cgiGet( "DDO_USUARIOPERFIL_DISPLAY_Cleanfilter");
            Ddo_usuarioperfil_display_Searchbuttontext = cgiGet( "DDO_USUARIOPERFIL_DISPLAY_Searchbuttontext");
            Gridpaginationbar_Selectedpage = cgiGet( "GRIDPAGINATIONBAR_Selectedpage");
            Ddo_usuario_nome_Activeeventkey = cgiGet( "DDO_USUARIO_NOME_Activeeventkey");
            Ddo_usuario_nome_Filteredtext_get = cgiGet( "DDO_USUARIO_NOME_Filteredtext_get");
            Ddo_usuario_nome_Selectedvalue_get = cgiGet( "DDO_USUARIO_NOME_Selectedvalue_get");
            Ddo_usuario_pessoanom_Activeeventkey = cgiGet( "DDO_USUARIO_PESSOANOM_Activeeventkey");
            Ddo_usuario_pessoanom_Filteredtext_get = cgiGet( "DDO_USUARIO_PESSOANOM_Filteredtext_get");
            Ddo_usuario_pessoanom_Selectedvalue_get = cgiGet( "DDO_USUARIO_PESSOANOM_Selectedvalue_get");
            Ddo_perfil_nome_Activeeventkey = cgiGet( "DDO_PERFIL_NOME_Activeeventkey");
            Ddo_perfil_nome_Filteredtext_get = cgiGet( "DDO_PERFIL_NOME_Filteredtext_get");
            Ddo_perfil_nome_Selectedvalue_get = cgiGet( "DDO_PERFIL_NOME_Selectedvalue_get");
            Ddo_perfil_tipo_Activeeventkey = cgiGet( "DDO_PERFIL_TIPO_Activeeventkey");
            Ddo_perfil_tipo_Selectedvalue_get = cgiGet( "DDO_PERFIL_TIPO_Selectedvalue_get");
            Ddo_usuarioperfil_insert_Activeeventkey = cgiGet( "DDO_USUARIOPERFIL_INSERT_Activeeventkey");
            Ddo_usuarioperfil_insert_Selectedvalue_get = cgiGet( "DDO_USUARIOPERFIL_INSERT_Selectedvalue_get");
            Ddo_usuarioperfil_update_Activeeventkey = cgiGet( "DDO_USUARIOPERFIL_UPDATE_Activeeventkey");
            Ddo_usuarioperfil_update_Selectedvalue_get = cgiGet( "DDO_USUARIOPERFIL_UPDATE_Selectedvalue_get");
            Ddo_usuarioperfil_delete_Activeeventkey = cgiGet( "DDO_USUARIOPERFIL_DELETE_Activeeventkey");
            Ddo_usuarioperfil_delete_Selectedvalue_get = cgiGet( "DDO_USUARIOPERFIL_DELETE_Selectedvalue_get");
            Ddo_usuarioperfil_display_Activeeventkey = cgiGet( "DDO_USUARIOPERFIL_DISPLAY_Activeeventkey");
            Ddo_usuarioperfil_display_Selectedvalue_get = cgiGet( "DDO_USUARIOPERFIL_DISPLAY_Selectedvalue_get");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vORDEREDDSC")) != AV14OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR1"), AV16DynamicFiltersSelector1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vPERFIL_NOME1"), AV18Perfil_Nome1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vUSUARIO_PESSOANOM1"), AV19Usuario_PessoaNom1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vDYNAMICFILTERSSELECTOR2"), AV21DynamicFiltersSelector2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vPERFIL_NOME2"), AV23Perfil_Nome2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vUSUARIO_PESSOANOM2"), AV24Usuario_PessoaNom2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( "GXH_vDYNAMICFILTERSENABLED2")) != AV20DynamicFiltersEnabled2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFUSUARIO_NOME"), AV55TFUsuario_Nome) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFUSUARIO_NOME_SEL"), AV56TFUsuario_Nome_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFUSUARIO_PESSOANOM"), AV59TFUsuario_PessoaNom) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFUSUARIO_PESSOANOM_SEL"), AV60TFUsuario_PessoaNom_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFPERFIL_NOME"), AV63TFPerfil_Nome) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vTFPERFIL_NOME_SEL"), AV64TFPerfil_Nome_Sel) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFUSUARIOPERFIL_INSERT_SEL"), ",", ".") != Convert.ToDecimal( AV71TFUsuarioPerfil_Insert_Sel )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFUSUARIOPERFIL_UPDATE_SEL"), ",", ".") != Convert.ToDecimal( AV74TFUsuarioPerfil_Update_Sel )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFUSUARIOPERFIL_DELETE_SEL"), ",", ".") != Convert.ToDecimal( AV77TFUsuarioPerfil_Delete_Sel )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vTFUSUARIOPERFIL_DISPLAY_SEL"), ",", ".") != Convert.ToDecimal( AV80TFUsuarioPerfil_Display_Sel )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E288Y2 */
         E288Y2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E288Y2( )
      {
         /* Start Routine */
         subGrid_Rows = 10;
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         lblJsdynamicfilters_Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         chkavDynamicfiltersenabled2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavDynamicfiltersenabled2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled2.Visible), 5, 0)));
         AV16DynamicFiltersSelector1 = "PERFIL_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV21DynamicFiltersSelector2 = "PERFIL_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         imgAdddynamicfilters1_Jsonclick = "WWPDynFilterShow(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Jsonclick", imgAdddynamicfilters1_Jsonclick);
         imgRemovedynamicfilters1_Jsonclick = "WWPDynFilterHideLast(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Jsonclick", imgRemovedynamicfilters1_Jsonclick);
         imgRemovedynamicfilters2_Jsonclick = "WWPDynFilterHideLast(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters2_Internalname, "Jsonclick", imgRemovedynamicfilters2_Jsonclick);
         edtavTfusuario_nome_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfusuario_nome_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfusuario_nome_Visible), 5, 0)));
         edtavTfusuario_nome_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfusuario_nome_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfusuario_nome_sel_Visible), 5, 0)));
         edtavTfusuario_pessoanom_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfusuario_pessoanom_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfusuario_pessoanom_Visible), 5, 0)));
         edtavTfusuario_pessoanom_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfusuario_pessoanom_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfusuario_pessoanom_sel_Visible), 5, 0)));
         edtavTfperfil_nome_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfperfil_nome_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfperfil_nome_Visible), 5, 0)));
         edtavTfperfil_nome_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfperfil_nome_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfperfil_nome_sel_Visible), 5, 0)));
         edtavTfusuarioperfil_insert_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfusuarioperfil_insert_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfusuarioperfil_insert_sel_Visible), 5, 0)));
         edtavTfusuarioperfil_update_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfusuarioperfil_update_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfusuarioperfil_update_sel_Visible), 5, 0)));
         edtavTfusuarioperfil_delete_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfusuarioperfil_delete_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfusuarioperfil_delete_sel_Visible), 5, 0)));
         edtavTfusuarioperfil_display_sel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavTfusuarioperfil_display_sel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavTfusuarioperfil_display_sel_Visible), 5, 0)));
         Ddo_usuario_nome_Titlecontrolidtoreplace = subGrid_Internalname+"_Usuario_Nome";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_usuario_nome_Internalname, "TitleControlIdToReplace", Ddo_usuario_nome_Titlecontrolidtoreplace);
         AV57ddo_Usuario_NomeTitleControlIdToReplace = Ddo_usuario_nome_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV57ddo_Usuario_NomeTitleControlIdToReplace", AV57ddo_Usuario_NomeTitleControlIdToReplace);
         edtavDdo_usuario_nometitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_usuario_nometitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_usuario_nometitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_usuario_pessoanom_Titlecontrolidtoreplace = subGrid_Internalname+"_Usuario_PessoaNom";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_usuario_pessoanom_Internalname, "TitleControlIdToReplace", Ddo_usuario_pessoanom_Titlecontrolidtoreplace);
         AV61ddo_Usuario_PessoaNomTitleControlIdToReplace = Ddo_usuario_pessoanom_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV61ddo_Usuario_PessoaNomTitleControlIdToReplace", AV61ddo_Usuario_PessoaNomTitleControlIdToReplace);
         edtavDdo_usuario_pessoanomtitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_usuario_pessoanomtitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_usuario_pessoanomtitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_perfil_nome_Titlecontrolidtoreplace = subGrid_Internalname+"_Perfil_Nome";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_perfil_nome_Internalname, "TitleControlIdToReplace", Ddo_perfil_nome_Titlecontrolidtoreplace);
         AV65ddo_Perfil_NomeTitleControlIdToReplace = Ddo_perfil_nome_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV65ddo_Perfil_NomeTitleControlIdToReplace", AV65ddo_Perfil_NomeTitleControlIdToReplace);
         edtavDdo_perfil_nometitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_perfil_nometitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_perfil_nometitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_perfil_tipo_Titlecontrolidtoreplace = subGrid_Internalname+"_Perfil_Tipo";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_perfil_tipo_Internalname, "TitleControlIdToReplace", Ddo_perfil_tipo_Titlecontrolidtoreplace);
         AV69ddo_Perfil_TipoTitleControlIdToReplace = Ddo_perfil_tipo_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV69ddo_Perfil_TipoTitleControlIdToReplace", AV69ddo_Perfil_TipoTitleControlIdToReplace);
         edtavDdo_perfil_tipotitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_perfil_tipotitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_perfil_tipotitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_usuarioperfil_insert_Titlecontrolidtoreplace = subGrid_Internalname+"_UsuarioPerfil_Insert";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_usuarioperfil_insert_Internalname, "TitleControlIdToReplace", Ddo_usuarioperfil_insert_Titlecontrolidtoreplace);
         AV72ddo_UsuarioPerfil_InsertTitleControlIdToReplace = Ddo_usuarioperfil_insert_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV72ddo_UsuarioPerfil_InsertTitleControlIdToReplace", AV72ddo_UsuarioPerfil_InsertTitleControlIdToReplace);
         edtavDdo_usuarioperfil_inserttitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_usuarioperfil_inserttitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_usuarioperfil_inserttitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_usuarioperfil_update_Titlecontrolidtoreplace = subGrid_Internalname+"_UsuarioPerfil_Update";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_usuarioperfil_update_Internalname, "TitleControlIdToReplace", Ddo_usuarioperfil_update_Titlecontrolidtoreplace);
         AV75ddo_UsuarioPerfil_UpdateTitleControlIdToReplace = Ddo_usuarioperfil_update_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV75ddo_UsuarioPerfil_UpdateTitleControlIdToReplace", AV75ddo_UsuarioPerfil_UpdateTitleControlIdToReplace);
         edtavDdo_usuarioperfil_updatetitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_usuarioperfil_updatetitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_usuarioperfil_updatetitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_usuarioperfil_delete_Titlecontrolidtoreplace = subGrid_Internalname+"_UsuarioPerfil_Delete";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_usuarioperfil_delete_Internalname, "TitleControlIdToReplace", Ddo_usuarioperfil_delete_Titlecontrolidtoreplace);
         AV78ddo_UsuarioPerfil_DeleteTitleControlIdToReplace = Ddo_usuarioperfil_delete_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV78ddo_UsuarioPerfil_DeleteTitleControlIdToReplace", AV78ddo_UsuarioPerfil_DeleteTitleControlIdToReplace);
         edtavDdo_usuarioperfil_deletetitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_usuarioperfil_deletetitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_usuarioperfil_deletetitlecontrolidtoreplace_Visible), 5, 0)));
         Ddo_usuarioperfil_display_Titlecontrolidtoreplace = subGrid_Internalname+"_UsuarioPerfil_Display";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_usuarioperfil_display_Internalname, "TitleControlIdToReplace", Ddo_usuarioperfil_display_Titlecontrolidtoreplace);
         AV81ddo_UsuarioPerfil_DisplayTitleControlIdToReplace = Ddo_usuarioperfil_display_Titlecontrolidtoreplace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV81ddo_UsuarioPerfil_DisplayTitleControlIdToReplace", AV81ddo_UsuarioPerfil_DisplayTitleControlIdToReplace);
         edtavDdo_usuarioperfil_displaytitlecontrolidtoreplace_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDdo_usuarioperfil_displaytitlecontrolidtoreplace_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDdo_usuarioperfil_displaytitlecontrolidtoreplace_Visible), 5, 0)));
         Form.Caption = " Usuario x Perfil";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavOrderedby.removeAllItems();
         cmbavOrderedby.addItem("1", "Nome", 0);
         cmbavOrderedby.addItem("2", "Usu�rio", 0);
         cmbavOrderedby.addItem("3", "Pessoa", 0);
         cmbavOrderedby.addItem("4", "Tipo do Perfil", 0);
         cmbavOrderedby.addItem("5", "Ins", 0);
         cmbavOrderedby.addItem("6", "Upd", 0);
         cmbavOrderedby.addItem("7", "Dlt", 0);
         cmbavOrderedby.addItem("8", "Dsp", 0);
         if ( AV13OrderedBy < 1 )
         {
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         imgCleanfilters_Jsonclick = "WWPDynFilterHideAll(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgCleanfilters_Internalname, "Jsonclick", imgCleanfilters_Jsonclick);
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = AV82DDO_TitleSettingsIcons;
         new wwpbaseobjects.getwwptitlesettingsicons(context ).execute( out  GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1) ;
         AV82DDO_TitleSettingsIcons = GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1;
      }

      protected void E298Y2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         AV54Usuario_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV58Usuario_PessoaNomTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV62Perfil_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV66Perfil_TipoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV70UsuarioPerfil_InsertTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV73UsuarioPerfil_UpdateTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV76UsuarioPerfil_DeleteTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV79UsuarioPerfil_DisplayTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtUsuario_Nome_Titleformat = 2;
         edtUsuario_Nome_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Usu�rio", AV57ddo_Usuario_NomeTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtUsuario_Nome_Internalname, "Title", edtUsuario_Nome_Title);
         edtUsuario_PessoaNom_Titleformat = 2;
         edtUsuario_PessoaNom_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Pessoa", AV61ddo_Usuario_PessoaNomTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtUsuario_PessoaNom_Internalname, "Title", edtUsuario_PessoaNom_Title);
         edtPerfil_Nome_Titleformat = 2;
         edtPerfil_Nome_Title = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Perfil", AV65ddo_Perfil_NomeTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtPerfil_Nome_Internalname, "Title", edtPerfil_Nome_Title);
         cmbPerfil_Tipo_Titleformat = 2;
         cmbPerfil_Tipo.Title.Text = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Tipo do Perfil", AV69ddo_Perfil_TipoTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbPerfil_Tipo_Internalname, "Title", cmbPerfil_Tipo.Title.Text);
         chkUsuarioPerfil_Insert_Titleformat = 2;
         chkUsuarioPerfil_Insert.Title.Text = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Ins", AV72ddo_UsuarioPerfil_InsertTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkUsuarioPerfil_Insert_Internalname, "Title", chkUsuarioPerfil_Insert.Title.Text);
         chkUsuarioPerfil_Update_Titleformat = 2;
         chkUsuarioPerfil_Update.Title.Text = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Upd", AV75ddo_UsuarioPerfil_UpdateTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkUsuarioPerfil_Update_Internalname, "Title", chkUsuarioPerfil_Update.Title.Text);
         chkUsuarioPerfil_Delete_Titleformat = 2;
         chkUsuarioPerfil_Delete.Title.Text = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Dlt", AV78ddo_UsuarioPerfil_DeleteTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkUsuarioPerfil_Delete_Internalname, "Title", chkUsuarioPerfil_Delete.Title.Text);
         chkUsuarioPerfil_Display_Titleformat = 2;
         chkUsuarioPerfil_Display.Title.Text = StringUtil.Format( "<div class='ColumnSettingsContainer''><span>%1</span><div id='%2'></div>", "Dsp", AV81ddo_UsuarioPerfil_DisplayTitleControlIdToReplace, "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkUsuarioPerfil_Display_Internalname, "Title", chkUsuarioPerfil_Display.Title.Text);
         AV84GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV84GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV84GridCurrentPage), 10, 0)));
         AV85GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV85GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV85GridPageCount), 10, 0)));
         edtavUpdate_Visible = (AV6WWPContext.gxTpr_Update ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUpdate_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUpdate_Visible), 5, 0)));
         edtavDelete_Visible = (AV6WWPContext.gxTpr_Delete ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDelete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDelete_Visible), 5, 0)));
         imgInsert_Visible = (AV6WWPContext.gxTpr_Insert ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgInsert_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgInsert_Visible), 5, 0)));
         AV88WWUsuarioPerfilDS_1_Dynamicfiltersselector1 = AV16DynamicFiltersSelector1;
         AV89WWUsuarioPerfilDS_2_Perfil_nome1 = AV18Perfil_Nome1;
         AV90WWUsuarioPerfilDS_3_Usuario_pessoanom1 = AV19Usuario_PessoaNom1;
         AV91WWUsuarioPerfilDS_4_Dynamicfiltersenabled2 = AV20DynamicFiltersEnabled2;
         AV92WWUsuarioPerfilDS_5_Dynamicfiltersselector2 = AV21DynamicFiltersSelector2;
         AV93WWUsuarioPerfilDS_6_Perfil_nome2 = AV23Perfil_Nome2;
         AV94WWUsuarioPerfilDS_7_Usuario_pessoanom2 = AV24Usuario_PessoaNom2;
         AV95WWUsuarioPerfilDS_8_Tfusuario_nome = AV55TFUsuario_Nome;
         AV96WWUsuarioPerfilDS_9_Tfusuario_nome_sel = AV56TFUsuario_Nome_Sel;
         AV97WWUsuarioPerfilDS_10_Tfusuario_pessoanom = AV59TFUsuario_PessoaNom;
         AV98WWUsuarioPerfilDS_11_Tfusuario_pessoanom_sel = AV60TFUsuario_PessoaNom_Sel;
         AV99WWUsuarioPerfilDS_12_Tfperfil_nome = AV63TFPerfil_Nome;
         AV100WWUsuarioPerfilDS_13_Tfperfil_nome_sel = AV64TFPerfil_Nome_Sel;
         AV101WWUsuarioPerfilDS_14_Tfperfil_tipo_sels = AV68TFPerfil_Tipo_Sels;
         AV102WWUsuarioPerfilDS_15_Tfusuarioperfil_insert_sel = AV71TFUsuarioPerfil_Insert_Sel;
         AV103WWUsuarioPerfilDS_16_Tfusuarioperfil_update_sel = AV74TFUsuarioPerfil_Update_Sel;
         AV104WWUsuarioPerfilDS_17_Tfusuarioperfil_delete_sel = AV77TFUsuarioPerfil_Delete_Sel;
         AV105WWUsuarioPerfilDS_18_Tfusuarioperfil_display_sel = AV80TFUsuarioPerfil_Display_Sel;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV54Usuario_NomeTitleFilterData", AV54Usuario_NomeTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV58Usuario_PessoaNomTitleFilterData", AV58Usuario_PessoaNomTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV62Perfil_NomeTitleFilterData", AV62Perfil_NomeTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV66Perfil_TipoTitleFilterData", AV66Perfil_TipoTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV70UsuarioPerfil_InsertTitleFilterData", AV70UsuarioPerfil_InsertTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV73UsuarioPerfil_UpdateTitleFilterData", AV73UsuarioPerfil_UpdateTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV76UsuarioPerfil_DeleteTitleFilterData", AV76UsuarioPerfil_DeleteTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV79UsuarioPerfil_DisplayTitleFilterData", AV79UsuarioPerfil_DisplayTitleFilterData);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
      }

      protected void E118Y2( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV83PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV83PageToGo) ;
         }
      }

      protected void E128Y2( )
      {
         /* Ddo_usuario_nome_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_usuario_nome_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_usuario_nome_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_usuario_nome_Internalname, "SortedStatus", Ddo_usuario_nome_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_usuario_nome_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_usuario_nome_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_usuario_nome_Internalname, "SortedStatus", Ddo_usuario_nome_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_usuario_nome_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV55TFUsuario_Nome = Ddo_usuario_nome_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55TFUsuario_Nome", AV55TFUsuario_Nome);
            AV56TFUsuario_Nome_Sel = Ddo_usuario_nome_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56TFUsuario_Nome_Sel", AV56TFUsuario_Nome_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E138Y2( )
      {
         /* Ddo_usuario_pessoanom_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_usuario_pessoanom_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_usuario_pessoanom_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_usuario_pessoanom_Internalname, "SortedStatus", Ddo_usuario_pessoanom_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_usuario_pessoanom_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_usuario_pessoanom_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_usuario_pessoanom_Internalname, "SortedStatus", Ddo_usuario_pessoanom_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_usuario_pessoanom_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV59TFUsuario_PessoaNom = Ddo_usuario_pessoanom_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59TFUsuario_PessoaNom", AV59TFUsuario_PessoaNom);
            AV60TFUsuario_PessoaNom_Sel = Ddo_usuario_pessoanom_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60TFUsuario_PessoaNom_Sel", AV60TFUsuario_PessoaNom_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E148Y2( )
      {
         /* Ddo_perfil_nome_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_perfil_nome_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_perfil_nome_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_perfil_nome_Internalname, "SortedStatus", Ddo_perfil_nome_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_perfil_nome_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_perfil_nome_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_perfil_nome_Internalname, "SortedStatus", Ddo_perfil_nome_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_perfil_nome_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV63TFPerfil_Nome = Ddo_perfil_nome_Filteredtext_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63TFPerfil_Nome", AV63TFPerfil_Nome);
            AV64TFPerfil_Nome_Sel = Ddo_perfil_nome_Selectedvalue_get;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64TFPerfil_Nome_Sel", AV64TFPerfil_Nome_Sel);
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E158Y2( )
      {
         /* Ddo_perfil_tipo_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_perfil_tipo_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_perfil_tipo_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_perfil_tipo_Internalname, "SortedStatus", Ddo_perfil_tipo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_perfil_tipo_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 4;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_perfil_tipo_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_perfil_tipo_Internalname, "SortedStatus", Ddo_perfil_tipo_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_perfil_tipo_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV67TFPerfil_Tipo_SelsJson = Ddo_perfil_tipo_Selectedvalue_get;
            AV68TFPerfil_Tipo_Sels.FromJSonString(StringUtil.StringReplace( AV67TFPerfil_Tipo_SelsJson, "\"", ""));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV68TFPerfil_Tipo_Sels", AV68TFPerfil_Tipo_Sels);
      }

      protected void E168Y2( )
      {
         /* Ddo_usuarioperfil_insert_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_usuarioperfil_insert_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 5;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_usuarioperfil_insert_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_usuarioperfil_insert_Internalname, "SortedStatus", Ddo_usuarioperfil_insert_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_usuarioperfil_insert_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 5;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_usuarioperfil_insert_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_usuarioperfil_insert_Internalname, "SortedStatus", Ddo_usuarioperfil_insert_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_usuarioperfil_insert_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV71TFUsuarioPerfil_Insert_Sel = (short)(NumberUtil.Val( Ddo_usuarioperfil_insert_Selectedvalue_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71TFUsuarioPerfil_Insert_Sel", StringUtil.Str( (decimal)(AV71TFUsuarioPerfil_Insert_Sel), 1, 0));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E178Y2( )
      {
         /* Ddo_usuarioperfil_update_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_usuarioperfil_update_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 6;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_usuarioperfil_update_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_usuarioperfil_update_Internalname, "SortedStatus", Ddo_usuarioperfil_update_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_usuarioperfil_update_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 6;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_usuarioperfil_update_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_usuarioperfil_update_Internalname, "SortedStatus", Ddo_usuarioperfil_update_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_usuarioperfil_update_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV74TFUsuarioPerfil_Update_Sel = (short)(NumberUtil.Val( Ddo_usuarioperfil_update_Selectedvalue_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV74TFUsuarioPerfil_Update_Sel", StringUtil.Str( (decimal)(AV74TFUsuarioPerfil_Update_Sel), 1, 0));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E188Y2( )
      {
         /* Ddo_usuarioperfil_delete_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_usuarioperfil_delete_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 7;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_usuarioperfil_delete_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_usuarioperfil_delete_Internalname, "SortedStatus", Ddo_usuarioperfil_delete_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_usuarioperfil_delete_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 7;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_usuarioperfil_delete_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_usuarioperfil_delete_Internalname, "SortedStatus", Ddo_usuarioperfil_delete_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_usuarioperfil_delete_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV77TFUsuarioPerfil_Delete_Sel = (short)(NumberUtil.Val( Ddo_usuarioperfil_delete_Selectedvalue_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV77TFUsuarioPerfil_Delete_Sel", StringUtil.Str( (decimal)(AV77TFUsuarioPerfil_Delete_Sel), 1, 0));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      protected void E198Y2( )
      {
         /* Ddo_usuarioperfil_display_Onoptionclicked Routine */
         if ( StringUtil.StrCmp(Ddo_usuarioperfil_display_Activeeventkey, "<#OrderASC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 8;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_usuarioperfil_display_Sortedstatus = "ASC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_usuarioperfil_display_Internalname, "SortedStatus", Ddo_usuarioperfil_display_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_usuarioperfil_display_Activeeventkey, "<#OrderDSC#>") == 0 )
         {
            /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            AV13OrderedBy = 8;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
            Ddo_usuarioperfil_display_Sortedstatus = "DSC";
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_usuarioperfil_display_Internalname, "SortedStatus", Ddo_usuarioperfil_display_Sortedstatus);
            subgrid_firstpage( ) ;
            context.DoAjaxRefresh();
         }
         else if ( StringUtil.StrCmp(Ddo_usuarioperfil_display_Activeeventkey, "<#Filter#>") == 0 )
         {
            AV80TFUsuarioPerfil_Display_Sel = (short)(NumberUtil.Val( Ddo_usuarioperfil_display_Selectedvalue_get, "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV80TFUsuarioPerfil_Display_Sel", StringUtil.Str( (decimal)(AV80TFUsuarioPerfil_Display_Sel), 1, 0));
            subgrid_firstpage( ) ;
         }
         cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", cmbavOrderedby.ToJavascriptSource());
      }

      private void E308Y2( )
      {
         /* Grid_Load Routine */
         AV27Update = context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavUpdate_Internalname, AV27Update);
         AV106Update_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( )));
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = formatLink("usuarioperfil.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A1Usuario_Codigo) + "," + UrlEncode("" +A3Perfil_Codigo);
         AV28Delete = context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavDelete_Internalname, AV28Delete);
         AV107Delete_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )));
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = formatLink("usuarioperfil.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A1Usuario_Codigo) + "," + UrlEncode("" +A3Perfil_Codigo);
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 63;
         }
         sendrow_632( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_63_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(63, GridRow);
         }
      }

      protected void E208Y2( )
      {
         /* Orderedby_Click Routine */
         context.DoAjaxRefresh();
      }

      protected void E258Y2( )
      {
         /* 'AddDynamicFilters1' Routine */
         AV20DynamicFiltersEnabled2 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled2", AV20DynamicFiltersEnabled2);
         imgAdddynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
      }

      protected void E218Y2( )
      {
         /* 'RemoveDynamicFilters1' Routine */
         AV25DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersRemoving", AV25DynamicFiltersRemoving);
         AV26DynamicFiltersIgnoreFirst = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersIgnoreFirst", AV26DynamicFiltersIgnoreFirst);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV25DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersRemoving", AV25DynamicFiltersRemoving);
         AV26DynamicFiltersIgnoreFirst = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26DynamicFiltersIgnoreFirst", AV26DynamicFiltersIgnoreFirst);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV16DynamicFiltersSelector1, AV18Perfil_Nome1, AV19Usuario_PessoaNom1, AV21DynamicFiltersSelector2, AV23Perfil_Nome2, AV24Usuario_PessoaNom2, AV20DynamicFiltersEnabled2, AV55TFUsuario_Nome, AV56TFUsuario_Nome_Sel, AV59TFUsuario_PessoaNom, AV60TFUsuario_PessoaNom_Sel, AV63TFPerfil_Nome, AV64TFPerfil_Nome_Sel, AV71TFUsuarioPerfil_Insert_Sel, AV74TFUsuarioPerfil_Update_Sel, AV77TFUsuarioPerfil_Delete_Sel, AV80TFUsuarioPerfil_Display_Sel, AV57ddo_Usuario_NomeTitleControlIdToReplace, AV61ddo_Usuario_PessoaNomTitleControlIdToReplace, AV65ddo_Perfil_NomeTitleControlIdToReplace, AV69ddo_Perfil_TipoTitleControlIdToReplace, AV72ddo_UsuarioPerfil_InsertTitleControlIdToReplace, AV75ddo_UsuarioPerfil_UpdateTitleControlIdToReplace, AV78ddo_UsuarioPerfil_DeleteTitleControlIdToReplace, AV81ddo_UsuarioPerfil_DisplayTitleControlIdToReplace, AV68TFPerfil_Tipo_Sels, AV108Pgmname, AV10GridState, AV26DynamicFiltersIgnoreFirst, AV25DynamicFiltersRemoving, A1Usuario_Codigo, A3Perfil_Codigo) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV16DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
      }

      protected void E268Y2( )
      {
         /* Dynamicfiltersselector1_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E228Y2( )
      {
         /* 'RemoveDynamicFilters2' Routine */
         AV25DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersRemoving", AV25DynamicFiltersRemoving);
         AV20DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled2", AV20DynamicFiltersEnabled2);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV25DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25DynamicFiltersRemoving", AV25DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV16DynamicFiltersSelector1, AV18Perfil_Nome1, AV19Usuario_PessoaNom1, AV21DynamicFiltersSelector2, AV23Perfil_Nome2, AV24Usuario_PessoaNom2, AV20DynamicFiltersEnabled2, AV55TFUsuario_Nome, AV56TFUsuario_Nome_Sel, AV59TFUsuario_PessoaNom, AV60TFUsuario_PessoaNom_Sel, AV63TFPerfil_Nome, AV64TFPerfil_Nome_Sel, AV71TFUsuarioPerfil_Insert_Sel, AV74TFUsuarioPerfil_Update_Sel, AV77TFUsuarioPerfil_Delete_Sel, AV80TFUsuarioPerfil_Display_Sel, AV57ddo_Usuario_NomeTitleControlIdToReplace, AV61ddo_Usuario_PessoaNomTitleControlIdToReplace, AV65ddo_Perfil_NomeTitleControlIdToReplace, AV69ddo_Perfil_TipoTitleControlIdToReplace, AV72ddo_UsuarioPerfil_InsertTitleControlIdToReplace, AV75ddo_UsuarioPerfil_UpdateTitleControlIdToReplace, AV78ddo_UsuarioPerfil_DeleteTitleControlIdToReplace, AV81ddo_UsuarioPerfil_DisplayTitleControlIdToReplace, AV68TFPerfil_Tipo_Sels, AV108Pgmname, AV10GridState, AV26DynamicFiltersIgnoreFirst, AV25DynamicFiltersRemoving, A1Usuario_Codigo, A3Perfil_Codigo) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV16DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
      }

      protected void E278Y2( )
      {
         /* Dynamicfiltersselector2_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E238Y2( )
      {
         /* 'DoCleanFilters' Routine */
         /* Execute user subroutine: 'CLEANFILTERS' */
         S212 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_firstpage( ) ;
         context.DoAjaxRefresh();
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV68TFPerfil_Tipo_Sels", AV68TFPerfil_Tipo_Sels);
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV16DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10GridState", AV10GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
      }

      protected void E248Y2( )
      {
         /* 'DoInsert' Routine */
         context.wjLoc = formatLink("usuarioperfil.aspx") + "?" + UrlEncode(StringUtil.RTrim("INS")) + "," + UrlEncode("" +0) + "," + UrlEncode("" +0);
         context.wjLocDisableFrm = 1;
      }

      protected void S172( )
      {
         /* 'RESETDDOSORTEDSTATUS' Routine */
         Ddo_usuario_nome_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_usuario_nome_Internalname, "SortedStatus", Ddo_usuario_nome_Sortedstatus);
         Ddo_usuario_pessoanom_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_usuario_pessoanom_Internalname, "SortedStatus", Ddo_usuario_pessoanom_Sortedstatus);
         Ddo_perfil_nome_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_perfil_nome_Internalname, "SortedStatus", Ddo_perfil_nome_Sortedstatus);
         Ddo_perfil_tipo_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_perfil_tipo_Internalname, "SortedStatus", Ddo_perfil_tipo_Sortedstatus);
         Ddo_usuarioperfil_insert_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_usuarioperfil_insert_Internalname, "SortedStatus", Ddo_usuarioperfil_insert_Sortedstatus);
         Ddo_usuarioperfil_update_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_usuarioperfil_update_Internalname, "SortedStatus", Ddo_usuarioperfil_update_Sortedstatus);
         Ddo_usuarioperfil_delete_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_usuarioperfil_delete_Internalname, "SortedStatus", Ddo_usuarioperfil_delete_Sortedstatus);
         Ddo_usuarioperfil_display_Sortedstatus = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_usuarioperfil_display_Internalname, "SortedStatus", Ddo_usuarioperfil_display_Sortedstatus);
      }

      protected void S152( )
      {
         /* 'SETDDOSORTEDSTATUS' Routine */
         /* Execute user subroutine: 'RESETDDOSORTEDSTATUS' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         if ( AV13OrderedBy == 2 )
         {
            Ddo_usuario_nome_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_usuario_nome_Internalname, "SortedStatus", Ddo_usuario_nome_Sortedstatus);
         }
         else if ( AV13OrderedBy == 3 )
         {
            Ddo_usuario_pessoanom_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_usuario_pessoanom_Internalname, "SortedStatus", Ddo_usuario_pessoanom_Sortedstatus);
         }
         else if ( AV13OrderedBy == 1 )
         {
            Ddo_perfil_nome_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_perfil_nome_Internalname, "SortedStatus", Ddo_perfil_nome_Sortedstatus);
         }
         else if ( AV13OrderedBy == 4 )
         {
            Ddo_perfil_tipo_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_perfil_tipo_Internalname, "SortedStatus", Ddo_perfil_tipo_Sortedstatus);
         }
         else if ( AV13OrderedBy == 5 )
         {
            Ddo_usuarioperfil_insert_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_usuarioperfil_insert_Internalname, "SortedStatus", Ddo_usuarioperfil_insert_Sortedstatus);
         }
         else if ( AV13OrderedBy == 6 )
         {
            Ddo_usuarioperfil_update_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_usuarioperfil_update_Internalname, "SortedStatus", Ddo_usuarioperfil_update_Sortedstatus);
         }
         else if ( AV13OrderedBy == 7 )
         {
            Ddo_usuarioperfil_delete_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_usuarioperfil_delete_Internalname, "SortedStatus", Ddo_usuarioperfil_delete_Sortedstatus);
         }
         else if ( AV13OrderedBy == 8 )
         {
            Ddo_usuarioperfil_display_Sortedstatus = (AV14OrderedDsc ? "DSC" : "ASC");
            context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_usuarioperfil_display_Internalname, "SortedStatus", Ddo_usuarioperfil_display_Sortedstatus);
         }
      }

      protected void S112( )
      {
         /* 'ENABLEDYNAMICFILTERS1' Routine */
         edtavPerfil_nome1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPerfil_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPerfil_nome1_Visible), 5, 0)));
         edtavUsuario_pessoanom1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUsuario_pessoanom1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUsuario_pessoanom1_Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "PERFIL_NOME") == 0 )
         {
            edtavPerfil_nome1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPerfil_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPerfil_nome1_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "USUARIO_PESSOANOM") == 0 )
         {
            edtavUsuario_pessoanom1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUsuario_pessoanom1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUsuario_pessoanom1_Visible), 5, 0)));
         }
      }

      protected void S122( )
      {
         /* 'ENABLEDYNAMICFILTERS2' Routine */
         edtavPerfil_nome2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPerfil_nome2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPerfil_nome2_Visible), 5, 0)));
         edtavUsuario_pessoanom2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUsuario_pessoanom2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUsuario_pessoanom2_Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "PERFIL_NOME") == 0 )
         {
            edtavPerfil_nome2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPerfil_nome2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPerfil_nome2_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "USUARIO_PESSOANOM") == 0 )
         {
            edtavUsuario_pessoanom2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavUsuario_pessoanom2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUsuario_pessoanom2_Visible), 5, 0)));
         }
      }

      protected void S192( )
      {
         /* 'RESETDYNFILTERS' Routine */
         AV20DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled2", AV20DynamicFiltersEnabled2);
         AV21DynamicFiltersSelector2 = "PERFIL_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
         AV23Perfil_Nome2 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23Perfil_Nome2", AV23Perfil_Nome2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S212( )
      {
         /* 'CLEANFILTERS' Routine */
         AV55TFUsuario_Nome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55TFUsuario_Nome", AV55TFUsuario_Nome);
         Ddo_usuario_nome_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_usuario_nome_Internalname, "FilteredText_set", Ddo_usuario_nome_Filteredtext_set);
         AV56TFUsuario_Nome_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56TFUsuario_Nome_Sel", AV56TFUsuario_Nome_Sel);
         Ddo_usuario_nome_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_usuario_nome_Internalname, "SelectedValue_set", Ddo_usuario_nome_Selectedvalue_set);
         AV59TFUsuario_PessoaNom = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59TFUsuario_PessoaNom", AV59TFUsuario_PessoaNom);
         Ddo_usuario_pessoanom_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_usuario_pessoanom_Internalname, "FilteredText_set", Ddo_usuario_pessoanom_Filteredtext_set);
         AV60TFUsuario_PessoaNom_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60TFUsuario_PessoaNom_Sel", AV60TFUsuario_PessoaNom_Sel);
         Ddo_usuario_pessoanom_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_usuario_pessoanom_Internalname, "SelectedValue_set", Ddo_usuario_pessoanom_Selectedvalue_set);
         AV63TFPerfil_Nome = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63TFPerfil_Nome", AV63TFPerfil_Nome);
         Ddo_perfil_nome_Filteredtext_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_perfil_nome_Internalname, "FilteredText_set", Ddo_perfil_nome_Filteredtext_set);
         AV64TFPerfil_Nome_Sel = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64TFPerfil_Nome_Sel", AV64TFPerfil_Nome_Sel);
         Ddo_perfil_nome_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_perfil_nome_Internalname, "SelectedValue_set", Ddo_perfil_nome_Selectedvalue_set);
         AV68TFPerfil_Tipo_Sels = (IGxCollection)(new GxSimpleCollection());
         Ddo_perfil_tipo_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_perfil_tipo_Internalname, "SelectedValue_set", Ddo_perfil_tipo_Selectedvalue_set);
         AV71TFUsuarioPerfil_Insert_Sel = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71TFUsuarioPerfil_Insert_Sel", StringUtil.Str( (decimal)(AV71TFUsuarioPerfil_Insert_Sel), 1, 0));
         Ddo_usuarioperfil_insert_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_usuarioperfil_insert_Internalname, "SelectedValue_set", Ddo_usuarioperfil_insert_Selectedvalue_set);
         AV74TFUsuarioPerfil_Update_Sel = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV74TFUsuarioPerfil_Update_Sel", StringUtil.Str( (decimal)(AV74TFUsuarioPerfil_Update_Sel), 1, 0));
         Ddo_usuarioperfil_update_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_usuarioperfil_update_Internalname, "SelectedValue_set", Ddo_usuarioperfil_update_Selectedvalue_set);
         AV77TFUsuarioPerfil_Delete_Sel = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV77TFUsuarioPerfil_Delete_Sel", StringUtil.Str( (decimal)(AV77TFUsuarioPerfil_Delete_Sel), 1, 0));
         Ddo_usuarioperfil_delete_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_usuarioperfil_delete_Internalname, "SelectedValue_set", Ddo_usuarioperfil_delete_Selectedvalue_set);
         AV80TFUsuarioPerfil_Display_Sel = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV80TFUsuarioPerfil_Display_Sel", StringUtil.Str( (decimal)(AV80TFUsuarioPerfil_Display_Sel), 1, 0));
         Ddo_usuarioperfil_display_Selectedvalue_set = "";
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_usuarioperfil_display_Internalname, "SelectedValue_set", Ddo_usuarioperfil_display_Selectedvalue_set);
         AV16DynamicFiltersSelector1 = "PERFIL_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
         AV18Perfil_Nome1 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18Perfil_Nome1", AV18Perfil_Nome1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S142( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV49Session.Get(AV108Pgmname+"GridState"), "") == 0 )
         {
            AV10GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  AV108Pgmname+"GridState"), "");
         }
         else
         {
            AV10GridState.FromXml(AV49Session.Get(AV108Pgmname+"GridState"), "");
         }
         AV13OrderedBy = AV10GridState.gxTpr_Orderedby;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         AV14OrderedDsc = AV10GridState.gxTpr_Ordereddsc;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14OrderedDsc", AV14OrderedDsc);
         /* Execute user subroutine: 'SETDDOSORTEDSTATUS' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADREGFILTERSSTATE' */
         S222 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_gotopage( AV10GridState.gxTpr_Currentpage) ;
      }

      protected void S222( )
      {
         /* 'LOADREGFILTERSSTATE' Routine */
         AV109GXV1 = 1;
         while ( AV109GXV1 <= AV10GridState.gxTpr_Filtervalues.Count )
         {
            AV11GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV10GridState.gxTpr_Filtervalues.Item(AV109GXV1));
            if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFUSUARIO_NOME") == 0 )
            {
               AV55TFUsuario_Nome = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55TFUsuario_Nome", AV55TFUsuario_Nome);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV55TFUsuario_Nome)) )
               {
                  Ddo_usuario_nome_Filteredtext_set = AV55TFUsuario_Nome;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_usuario_nome_Internalname, "FilteredText_set", Ddo_usuario_nome_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFUSUARIO_NOME_SEL") == 0 )
            {
               AV56TFUsuario_Nome_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV56TFUsuario_Nome_Sel", AV56TFUsuario_Nome_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV56TFUsuario_Nome_Sel)) )
               {
                  Ddo_usuario_nome_Selectedvalue_set = AV56TFUsuario_Nome_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_usuario_nome_Internalname, "SelectedValue_set", Ddo_usuario_nome_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFUSUARIO_PESSOANOM") == 0 )
            {
               AV59TFUsuario_PessoaNom = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV59TFUsuario_PessoaNom", AV59TFUsuario_PessoaNom);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV59TFUsuario_PessoaNom)) )
               {
                  Ddo_usuario_pessoanom_Filteredtext_set = AV59TFUsuario_PessoaNom;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_usuario_pessoanom_Internalname, "FilteredText_set", Ddo_usuario_pessoanom_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFUSUARIO_PESSOANOM_SEL") == 0 )
            {
               AV60TFUsuario_PessoaNom_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV60TFUsuario_PessoaNom_Sel", AV60TFUsuario_PessoaNom_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60TFUsuario_PessoaNom_Sel)) )
               {
                  Ddo_usuario_pessoanom_Selectedvalue_set = AV60TFUsuario_PessoaNom_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_usuario_pessoanom_Internalname, "SelectedValue_set", Ddo_usuario_pessoanom_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFPERFIL_NOME") == 0 )
            {
               AV63TFPerfil_Nome = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV63TFPerfil_Nome", AV63TFPerfil_Nome);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV63TFPerfil_Nome)) )
               {
                  Ddo_perfil_nome_Filteredtext_set = AV63TFPerfil_Nome;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_perfil_nome_Internalname, "FilteredText_set", Ddo_perfil_nome_Filteredtext_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFPERFIL_NOME_SEL") == 0 )
            {
               AV64TFPerfil_Nome_Sel = AV11GridStateFilterValue.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV64TFPerfil_Nome_Sel", AV64TFPerfil_Nome_Sel);
               if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV64TFPerfil_Nome_Sel)) )
               {
                  Ddo_perfil_nome_Selectedvalue_set = AV64TFPerfil_Nome_Sel;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_perfil_nome_Internalname, "SelectedValue_set", Ddo_perfil_nome_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFPERFIL_TIPO_SEL") == 0 )
            {
               AV67TFPerfil_Tipo_SelsJson = AV11GridStateFilterValue.gxTpr_Value;
               AV68TFPerfil_Tipo_Sels.FromJSonString(AV67TFPerfil_Tipo_SelsJson);
               if ( ! ( AV68TFPerfil_Tipo_Sels.Count == 0 ) )
               {
                  Ddo_perfil_tipo_Selectedvalue_set = AV67TFPerfil_Tipo_SelsJson;
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_perfil_tipo_Internalname, "SelectedValue_set", Ddo_perfil_tipo_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFUSUARIOPERFIL_INSERT_SEL") == 0 )
            {
               AV71TFUsuarioPerfil_Insert_Sel = (short)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV71TFUsuarioPerfil_Insert_Sel", StringUtil.Str( (decimal)(AV71TFUsuarioPerfil_Insert_Sel), 1, 0));
               if ( ! (0==AV71TFUsuarioPerfil_Insert_Sel) )
               {
                  Ddo_usuarioperfil_insert_Selectedvalue_set = StringUtil.Str( (decimal)(AV71TFUsuarioPerfil_Insert_Sel), 1, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_usuarioperfil_insert_Internalname, "SelectedValue_set", Ddo_usuarioperfil_insert_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFUSUARIOPERFIL_UPDATE_SEL") == 0 )
            {
               AV74TFUsuarioPerfil_Update_Sel = (short)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV74TFUsuarioPerfil_Update_Sel", StringUtil.Str( (decimal)(AV74TFUsuarioPerfil_Update_Sel), 1, 0));
               if ( ! (0==AV74TFUsuarioPerfil_Update_Sel) )
               {
                  Ddo_usuarioperfil_update_Selectedvalue_set = StringUtil.Str( (decimal)(AV74TFUsuarioPerfil_Update_Sel), 1, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_usuarioperfil_update_Internalname, "SelectedValue_set", Ddo_usuarioperfil_update_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFUSUARIOPERFIL_DELETE_SEL") == 0 )
            {
               AV77TFUsuarioPerfil_Delete_Sel = (short)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV77TFUsuarioPerfil_Delete_Sel", StringUtil.Str( (decimal)(AV77TFUsuarioPerfil_Delete_Sel), 1, 0));
               if ( ! (0==AV77TFUsuarioPerfil_Delete_Sel) )
               {
                  Ddo_usuarioperfil_delete_Selectedvalue_set = StringUtil.Str( (decimal)(AV77TFUsuarioPerfil_Delete_Sel), 1, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_usuarioperfil_delete_Internalname, "SelectedValue_set", Ddo_usuarioperfil_delete_Selectedvalue_set);
               }
            }
            else if ( StringUtil.StrCmp(AV11GridStateFilterValue.gxTpr_Name, "TFUSUARIOPERFIL_DISPLAY_SEL") == 0 )
            {
               AV80TFUsuarioPerfil_Display_Sel = (short)(NumberUtil.Val( AV11GridStateFilterValue.gxTpr_Value, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV80TFUsuarioPerfil_Display_Sel", StringUtil.Str( (decimal)(AV80TFUsuarioPerfil_Display_Sel), 1, 0));
               if ( ! (0==AV80TFUsuarioPerfil_Display_Sel) )
               {
                  Ddo_usuarioperfil_display_Selectedvalue_set = StringUtil.Str( (decimal)(AV80TFUsuarioPerfil_Display_Sel), 1, 0);
                  context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Ddo_usuarioperfil_display_Internalname, "SelectedValue_set", Ddo_usuarioperfil_display_Selectedvalue_set);
               }
            }
            AV109GXV1 = (int)(AV109GXV1+1);
         }
      }

      protected void S202( )
      {
         /* 'LOADDYNFILTERSSTATE' Routine */
         imgAdddynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(1));
            AV16DynamicFiltersSelector1 = AV12GridStateDynamicFilter.gxTpr_Selected;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16DynamicFiltersSelector1", AV16DynamicFiltersSelector1);
            if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "PERFIL_NOME") == 0 )
            {
               AV18Perfil_Nome1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18Perfil_Nome1", AV18Perfil_Nome1);
            }
            else if ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "USUARIO_PESSOANOM") == 0 )
            {
               AV19Usuario_PessoaNom1 = AV12GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19Usuario_PessoaNom1", AV19Usuario_PessoaNom1);
            }
            /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            if ( AV10GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               lblJsdynamicfilters_Caption = "<script type=\"text/javascript\">";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(2);";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               imgAdddynamicfilters1_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
               imgRemovedynamicfilters1_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
               AV20DynamicFiltersEnabled2 = true;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20DynamicFiltersEnabled2", AV20DynamicFiltersEnabled2);
               AV12GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV10GridState.gxTpr_Dynamicfilters.Item(2));
               AV21DynamicFiltersSelector2 = AV12GridStateDynamicFilter.gxTpr_Selected;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21DynamicFiltersSelector2", AV21DynamicFiltersSelector2);
               if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "PERFIL_NOME") == 0 )
               {
                  AV23Perfil_Nome2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23Perfil_Nome2", AV23Perfil_Nome2);
               }
               else if ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "USUARIO_PESSOANOM") == 0 )
               {
                  AV24Usuario_PessoaNom2 = AV12GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24Usuario_PessoaNom2", AV24Usuario_PessoaNom2);
               }
               /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
               S122 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"</script>";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
            }
         }
         if ( AV25DynamicFiltersRemoving )
         {
            lblJsdynamicfilters_Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         }
      }

      protected void S162( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV10GridState.FromXml(AV49Session.Get(AV108Pgmname+"GridState"), "");
         AV10GridState.gxTpr_Orderedby = AV13OrderedBy;
         AV10GridState.gxTpr_Ordereddsc = AV14OrderedDsc;
         AV10GridState.gxTpr_Filtervalues.Clear();
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV55TFUsuario_Nome)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFUSUARIO_NOME";
            AV11GridStateFilterValue.gxTpr_Value = AV55TFUsuario_Nome;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV56TFUsuario_Nome_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFUSUARIO_NOME_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV56TFUsuario_Nome_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV59TFUsuario_PessoaNom)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFUSUARIO_PESSOANOM";
            AV11GridStateFilterValue.gxTpr_Value = AV59TFUsuario_PessoaNom;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60TFUsuario_PessoaNom_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFUSUARIO_PESSOANOM_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV60TFUsuario_PessoaNom_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV63TFPerfil_Nome)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFPERFIL_NOME";
            AV11GridStateFilterValue.gxTpr_Value = AV63TFPerfil_Nome;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV64TFPerfil_Nome_Sel)) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFPERFIL_NOME_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV64TFPerfil_Nome_Sel;
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! ( AV68TFPerfil_Tipo_Sels.Count == 0 ) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFPERFIL_TIPO_SEL";
            AV11GridStateFilterValue.gxTpr_Value = AV68TFPerfil_Tipo_Sels.ToJSonString(false);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! (0==AV71TFUsuarioPerfil_Insert_Sel) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFUSUARIOPERFIL_INSERT_SEL";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV71TFUsuarioPerfil_Insert_Sel), 1, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! (0==AV74TFUsuarioPerfil_Update_Sel) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFUSUARIOPERFIL_UPDATE_SEL";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV74TFUsuarioPerfil_Update_Sel), 1, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! (0==AV77TFUsuarioPerfil_Delete_Sel) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFUSUARIOPERFIL_DELETE_SEL";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV77TFUsuarioPerfil_Delete_Sel), 1, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         if ( ! (0==AV80TFUsuarioPerfil_Display_Sel) )
         {
            AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
            AV11GridStateFilterValue.gxTpr_Name = "TFUSUARIOPERFIL_DISPLAY_SEL";
            AV11GridStateFilterValue.gxTpr_Value = StringUtil.Str( (decimal)(AV80TFUsuarioPerfil_Display_Sel), 1, 0);
            AV10GridState.gxTpr_Filtervalues.Add(AV11GridStateFilterValue, 0);
         }
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV10GridState.gxTpr_Currentpage = (short)(subGrid_Currentpage( ));
         new wwpbaseobjects.savegridstate(context ).execute(  AV108Pgmname+"GridState",  AV10GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_MeetrikaVs3")) ;
      }

      protected void S182( )
      {
         /* 'SAVEDYNFILTERSSTATE' Routine */
         AV10GridState.gxTpr_Dynamicfilters.Clear();
         if ( ! AV26DynamicFiltersIgnoreFirst )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV16DynamicFiltersSelector1;
            if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "PERFIL_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV18Perfil_Nome1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV18Perfil_Nome1;
            }
            else if ( ( StringUtil.StrCmp(AV16DynamicFiltersSelector1, "USUARIO_PESSOANOM") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV19Usuario_PessoaNom1)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV19Usuario_PessoaNom1;
            }
            if ( AV25DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
         if ( AV20DynamicFiltersEnabled2 )
         {
            AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV12GridStateDynamicFilter.gxTpr_Selected = AV21DynamicFiltersSelector2;
            if ( ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "PERFIL_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV23Perfil_Nome2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV23Perfil_Nome2;
            }
            else if ( ( StringUtil.StrCmp(AV21DynamicFiltersSelector2, "USUARIO_PESSOANOM") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV24Usuario_PessoaNom2)) )
            {
               AV12GridStateDynamicFilter.gxTpr_Value = AV24Usuario_PessoaNom2;
            }
            if ( AV25DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV12GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV10GridState.gxTpr_Dynamicfilters.Add(AV12GridStateDynamicFilter, 0);
            }
         }
      }

      protected void S132( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8TrnContext.gxTpr_Callerobject = AV108Pgmname;
         AV8TrnContext.gxTpr_Callerondelete = true;
         AV8TrnContext.gxTpr_Callerurl = AV7HTTPRequest.ScriptName+"?"+AV7HTTPRequest.QueryString;
         AV8TrnContext.gxTpr_Transactionname = "UsuarioPerfil";
         AV49Session.Set("TrnContext", AV8TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_MeetrikaVs3"));
      }

      protected void wb_table1_2_8Y2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_8Y2( true) ;
         }
         else
         {
            wb_table2_8_8Y2( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_8Y2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_57_8Y2( true) ;
         }
         else
         {
            wb_table3_57_8Y2( false) ;
         }
         return  ;
      }

      protected void wb_table3_57_8Y2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_8Y2e( true) ;
         }
         else
         {
            wb_table1_2_8Y2e( false) ;
         }
      }

      protected void wb_table3_57_8Y2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "TableGridHeader", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_60_8Y2( true) ;
         }
         else
         {
            wb_table4_60_8Y2( false) ;
         }
         return  ;
      }

      protected void wb_table4_60_8Y2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_57_8Y2e( true) ;
         }
         else
         {
            wb_table3_57_8Y2e( false) ;
         }
      }

      protected void wb_table4_60_8Y2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"DivS\" data-gxgridid=\"63\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+((edtavUpdate_Visible==0) ? "display:none;" : "")+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+((edtavDelete_Visible==0) ? "display:none;" : "")+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "C�digo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "C�digo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "C�d. da Pessoa") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtUsuario_Nome_Titleformat == 0 )
               {
                  context.SendWebValue( edtUsuario_Nome_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtUsuario_Nome_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtUsuario_PessoaNom_Titleformat == 0 )
               {
                  context.SendWebValue( edtUsuario_PessoaNom_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtUsuario_PessoaNom_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtPerfil_Nome_Titleformat == 0 )
               {
                  context.SendWebValue( edtPerfil_Nome_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtPerfil_Nome_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( cmbPerfil_Tipo_Titleformat == 0 )
               {
                  context.SendWebValue( cmbPerfil_Tipo.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( cmbPerfil_Tipo.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( chkUsuarioPerfil_Insert_Titleformat == 0 )
               {
                  context.SendWebValue( chkUsuarioPerfil_Insert.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( chkUsuarioPerfil_Insert.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( chkUsuarioPerfil_Update_Titleformat == 0 )
               {
                  context.SendWebValue( chkUsuarioPerfil_Update.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( chkUsuarioPerfil_Update.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( chkUsuarioPerfil_Delete_Titleformat == 0 )
               {
                  context.SendWebValue( chkUsuarioPerfil_Delete.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( chkUsuarioPerfil_Delete.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( chkUsuarioPerfil_Display_Titleformat == 0 )
               {
                  context.SendWebValue( chkUsuarioPerfil_Display.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( chkUsuarioPerfil_Display.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", "");
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV27Update));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavUpdate_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavUpdate_Tooltiptext));
               GridColumn.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavUpdate_Visible), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV28Delete));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavDelete_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDelete_Tooltiptext));
               GridColumn.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavDelete_Visible), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1Usuario_Codigo), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A3Perfil_Codigo), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A57Usuario_PessoaCod), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A2Usuario_Nome));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtUsuario_Nome_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtUsuario_Nome_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A58Usuario_PessoaNom));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtUsuario_PessoaNom_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtUsuario_PessoaNom_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A4Perfil_Nome));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtPerfil_Nome_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtPerfil_Nome_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A275Perfil_Tipo), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( cmbPerfil_Tipo.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbPerfil_Tipo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.BoolToStr( A544UsuarioPerfil_Insert));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( chkUsuarioPerfil_Insert.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(chkUsuarioPerfil_Insert_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.BoolToStr( A659UsuarioPerfil_Update));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( chkUsuarioPerfil_Update.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(chkUsuarioPerfil_Update_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.BoolToStr( A546UsuarioPerfil_Delete));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( chkUsuarioPerfil_Delete.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(chkUsuarioPerfil_Delete_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.BoolToStr( A543UsuarioPerfil_Display));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( chkUsuarioPerfil_Display.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(chkUsuarioPerfil_Display_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 63 )
         {
            wbEnd = 0;
            nRC_GXsfl_63 = (short)(nGXsfl_63_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_60_8Y2e( true) ;
         }
         else
         {
            wb_table4_60_8Y2e( false) ;
         }
      }

      protected void wb_table2_8_8Y2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableheader_Internalname, tblTableheader_Internalname, "", "TableSearch", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='GridHeaderCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblUsuarioperfiltitle_Internalname, "Usuario x Perfil", "", "", lblUsuarioperfiltitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_WWUsuarioPerfil.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='Width100'>") ;
            wb_table5_13_8Y2( true) ;
         }
         else
         {
            wb_table5_13_8Y2( false) ;
         }
         return  ;
      }

      protected void wb_table5_13_8Y2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='OrderedByDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblOrderedtext_Internalname, "Ordenado por", "", "", lblOrderedtext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "OrderedByDescription", 0, "", 1, 1, 0, "HLP_WWUsuarioPerfil.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 20,'',false,'" + sGXsfl_63_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavOrderedby, cmbavOrderedby_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)), 1, cmbavOrderedby_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVORDEREDBY.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,20);\"", "", true, "HLP_WWUsuarioPerfil.htm");
            cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavOrderedby_Internalname, "Values", (String)(cmbavOrderedby.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'" + sGXsfl_63_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV14OrderedDsc), StringUtil.BoolToStr( AV14OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,21);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_WWUsuarioPerfil.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableFiltersCell'>") ;
            wb_table6_23_8Y2( true) ;
         }
         else
         {
            wb_table6_23_8Y2( false) ;
         }
         return  ;
      }

      protected void wb_table6_23_8Y2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_8Y2e( true) ;
         }
         else
         {
            wb_table2_8_8Y2e( false) ;
         }
      }

      protected void wb_table6_23_8Y2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 26,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgCleanfilters_Internalname, context.GetImagePath( "effedd9a-25cf-4497-952f-5a5ac032b8be", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Limpar filtros", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgCleanfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOCLEANFILTERS\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWUsuarioPerfil.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table7_28_8Y2( true) ;
         }
         else
         {
            wb_table7_28_8Y2( false) ;
         }
         return  ;
      }

      protected void wb_table7_28_8Y2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblJsdynamicfilters_Internalname, lblJsdynamicfilters_Caption, "", "", lblJsdynamicfilters_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "", 0, "", 1, 1, 1, "HLP_WWUsuarioPerfil.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_23_8Y2e( true) ;
         }
         else
         {
            wb_table6_23_8Y2e( false) ;
         }
      }

      protected void wb_table7_28_8Y2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabledynamicfilters_Internalname, tblTabledynamicfilters_Internalname, "", "TableDynamicFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix1_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWUsuarioPerfil.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 33,'',false,'" + sGXsfl_63_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector1, cmbavDynamicfiltersselector1_Internalname, StringUtil.RTrim( AV16DynamicFiltersSelector1), 1, cmbavDynamicfiltersselector1_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR1.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,33);\"", "", true, "HLP_WWUsuarioPerfil.htm");
            cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV16DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector1_Internalname, "Values", (String)(cmbavDynamicfiltersselector1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle1_Internalname, "valor", "", "", lblDynamicfiltersmiddle1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWUsuarioPerfil.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 37,'',false,'" + sGXsfl_63_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavPerfil_nome1_Internalname, StringUtil.RTrim( AV18Perfil_Nome1), StringUtil.RTrim( context.localUtil.Format( AV18Perfil_Nome1, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,37);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavPerfil_nome1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavPerfil_nome1_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWUsuarioPerfil.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 38,'',false,'" + sGXsfl_63_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavUsuario_pessoanom1_Internalname, StringUtil.RTrim( AV19Usuario_PessoaNom1), StringUtil.RTrim( context.localUtil.Format( AV19Usuario_PessoaNom1, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,38);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavUsuario_pessoanom1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavUsuario_pessoanom1_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWUsuarioPerfil.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 40,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters1_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters1_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ADDDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWUsuarioPerfil.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 41,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters1_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters1_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters1_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWUsuarioPerfil.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow2\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix2_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWUsuarioPerfil.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 46,'',false,'" + sGXsfl_63_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector2, cmbavDynamicfiltersselector2_Internalname, StringUtil.RTrim( AV21DynamicFiltersSelector2), 1, cmbavDynamicfiltersselector2_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVDYNAMICFILTERSSELECTOR2.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,46);\"", "", true, "HLP_WWUsuarioPerfil.htm");
            cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV21DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDynamicfiltersselector2_Internalname, "Values", (String)(cmbavDynamicfiltersselector2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle2_Internalname, "valor", "", "", lblDynamicfiltersmiddle2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_WWUsuarioPerfil.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 50,'',false,'" + sGXsfl_63_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavPerfil_nome2_Internalname, StringUtil.RTrim( AV23Perfil_Nome2), StringUtil.RTrim( context.localUtil.Format( AV23Perfil_Nome2, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,50);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavPerfil_nome2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavPerfil_nome2_Visible, 1, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWUsuarioPerfil.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 51,'',false,'" + sGXsfl_63_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavUsuario_pessoanom2_Internalname, StringUtil.RTrim( AV24Usuario_PessoaNom2), StringUtil.RTrim( context.localUtil.Format( AV24Usuario_PessoaNom2, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,51);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavUsuario_pessoanom2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavUsuario_pessoanom2_Visible, 1, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWUsuarioPerfil.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 53,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters2_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters2_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REMOVEDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWUsuarioPerfil.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_28_8Y2e( true) ;
         }
         else
         {
            wb_table7_28_8Y2e( false) ;
         }
      }

      protected void wb_table5_13_8Y2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "Table", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 16,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgInsert_Internalname, context.GetImagePath( "5649fbb8-8ce0-4810-a5ce-bd649ea83c3a", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgInsert_Visible, 1, "", "Inserir", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgInsert_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOINSERT\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WWUsuarioPerfil.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_13_8Y2e( true) ;
         }
         else
         {
            wb_table5_13_8Y2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA8Y2( ) ;
         WS8Y2( ) ;
         WE8Y2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20205299354873");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wwusuarioperfil.js", "?20205299354873");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/DropDownOptions/BootstrapDropDownOptionsRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_632( )
      {
         edtavUpdate_Internalname = "vUPDATE_"+sGXsfl_63_idx;
         edtavDelete_Internalname = "vDELETE_"+sGXsfl_63_idx;
         edtUsuario_Codigo_Internalname = "USUARIO_CODIGO_"+sGXsfl_63_idx;
         edtPerfil_Codigo_Internalname = "PERFIL_CODIGO_"+sGXsfl_63_idx;
         edtUsuario_PessoaCod_Internalname = "USUARIO_PESSOACOD_"+sGXsfl_63_idx;
         edtUsuario_Nome_Internalname = "USUARIO_NOME_"+sGXsfl_63_idx;
         edtUsuario_PessoaNom_Internalname = "USUARIO_PESSOANOM_"+sGXsfl_63_idx;
         edtPerfil_Nome_Internalname = "PERFIL_NOME_"+sGXsfl_63_idx;
         cmbPerfil_Tipo_Internalname = "PERFIL_TIPO_"+sGXsfl_63_idx;
         chkUsuarioPerfil_Insert_Internalname = "USUARIOPERFIL_INSERT_"+sGXsfl_63_idx;
         chkUsuarioPerfil_Update_Internalname = "USUARIOPERFIL_UPDATE_"+sGXsfl_63_idx;
         chkUsuarioPerfil_Delete_Internalname = "USUARIOPERFIL_DELETE_"+sGXsfl_63_idx;
         chkUsuarioPerfil_Display_Internalname = "USUARIOPERFIL_DISPLAY_"+sGXsfl_63_idx;
      }

      protected void SubsflControlProps_fel_632( )
      {
         edtavUpdate_Internalname = "vUPDATE_"+sGXsfl_63_fel_idx;
         edtavDelete_Internalname = "vDELETE_"+sGXsfl_63_fel_idx;
         edtUsuario_Codigo_Internalname = "USUARIO_CODIGO_"+sGXsfl_63_fel_idx;
         edtPerfil_Codigo_Internalname = "PERFIL_CODIGO_"+sGXsfl_63_fel_idx;
         edtUsuario_PessoaCod_Internalname = "USUARIO_PESSOACOD_"+sGXsfl_63_fel_idx;
         edtUsuario_Nome_Internalname = "USUARIO_NOME_"+sGXsfl_63_fel_idx;
         edtUsuario_PessoaNom_Internalname = "USUARIO_PESSOANOM_"+sGXsfl_63_fel_idx;
         edtPerfil_Nome_Internalname = "PERFIL_NOME_"+sGXsfl_63_fel_idx;
         cmbPerfil_Tipo_Internalname = "PERFIL_TIPO_"+sGXsfl_63_fel_idx;
         chkUsuarioPerfil_Insert_Internalname = "USUARIOPERFIL_INSERT_"+sGXsfl_63_fel_idx;
         chkUsuarioPerfil_Update_Internalname = "USUARIOPERFIL_UPDATE_"+sGXsfl_63_fel_idx;
         chkUsuarioPerfil_Delete_Internalname = "USUARIOPERFIL_DELETE_"+sGXsfl_63_fel_idx;
         chkUsuarioPerfil_Display_Internalname = "USUARIOPERFIL_DISPLAY_"+sGXsfl_63_fel_idx;
      }

      protected void sendrow_632( )
      {
         SubsflControlProps_632( ) ;
         WB8Y0( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_63_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_63_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_63_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+((edtavUpdate_Visible==0) ? "display:none;" : "")+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV27Update_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV27Update))&&String.IsNullOrEmpty(StringUtil.RTrim( AV106Update_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV27Update)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavUpdate_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV27Update)) ? AV106Update_GXI : context.PathToRelativeUrl( AV27Update)),(String)edtavUpdate_Link,(String)"",(String)"",context.GetTheme( ),(int)edtavUpdate_Visible,(short)1,(String)"",(String)edtavUpdate_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV27Update_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+((edtavDelete_Visible==0) ? "display:none;" : "")+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV28Delete_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV28Delete))&&String.IsNullOrEmpty(StringUtil.RTrim( AV107Delete_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV28Delete)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDelete_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV28Delete)) ? AV107Delete_GXI : context.PathToRelativeUrl( AV28Delete)),(String)edtavDelete_Link,(String)"",(String)"",context.GetTheme( ),(int)edtavDelete_Visible,(short)1,(String)"",(String)edtavDelete_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV28Delete_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtUsuario_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A1Usuario_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A1Usuario_Codigo), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtUsuario_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)63,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtPerfil_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A3Perfil_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A3Perfil_Codigo), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtPerfil_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)63,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtUsuario_PessoaCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A57Usuario_PessoaCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A57Usuario_PessoaCod), "ZZZZZ9"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtUsuario_PessoaCod_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)63,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtUsuario_Nome_Internalname,StringUtil.RTrim( A2Usuario_Nome),StringUtil.RTrim( context.localUtil.Format( A2Usuario_Nome, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtUsuario_Nome_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)63,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtUsuario_PessoaNom_Internalname,StringUtil.RTrim( A58Usuario_PessoaNom),StringUtil.RTrim( context.localUtil.Format( A58Usuario_PessoaNom, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtUsuario_PessoaNom_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)100,(short)0,(short)0,(short)63,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome100",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtPerfil_Nome_Internalname,StringUtil.RTrim( A4Perfil_Nome),StringUtil.RTrim( context.localUtil.Format( A4Perfil_Nome, "@!")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtPerfil_Nome_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)63,(short)1,(short)-1,(short)-1,(bool)true,(String)"Nome",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            GXCCtl = "PERFIL_TIPO_" + sGXsfl_63_idx;
            cmbPerfil_Tipo.Name = GXCCtl;
            cmbPerfil_Tipo.WebTags = "";
            cmbPerfil_Tipo.addItem("0", "Desconhecido", 0);
            cmbPerfil_Tipo.addItem("1", "Auditor", 0);
            cmbPerfil_Tipo.addItem("2", "Contador", 0);
            cmbPerfil_Tipo.addItem("3", "Contratada", 0);
            cmbPerfil_Tipo.addItem("4", "Contratante", 0);
            cmbPerfil_Tipo.addItem("5", "Financeiro", 0);
            cmbPerfil_Tipo.addItem("6", "Usuario", 0);
            cmbPerfil_Tipo.addItem("10", "Licensiado", 0);
            cmbPerfil_Tipo.addItem("99", "Administrador GAM", 0);
            if ( cmbPerfil_Tipo.ItemCount > 0 )
            {
               A275Perfil_Tipo = (short)(NumberUtil.Val( cmbPerfil_Tipo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A275Perfil_Tipo), 4, 0))), "."));
            }
            /* ComboBox */
            GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbPerfil_Tipo,(String)cmbPerfil_Tipo_Internalname,StringUtil.Trim( StringUtil.Str( (decimal)(A275Perfil_Tipo), 4, 0)),(short)1,(String)cmbPerfil_Tipo_Jsonclick,(short)0,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"int",(String)"",(short)-1,(short)0,(short)1,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"BootstrapAttribute",(String)"",(String)"",(String)"",(bool)true});
            cmbPerfil_Tipo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A275Perfil_Tipo), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbPerfil_Tipo_Internalname, "Values", (String)(cmbPerfil_Tipo.ToJavascriptSource()));
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Check box */
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            GridRow.AddColumnProperties("checkbox", 1, isAjaxCallMode( ), new Object[] {(String)chkUsuarioPerfil_Insert_Internalname,StringUtil.BoolToStr( A544UsuarioPerfil_Insert),(String)"",(String)"",(short)-1,(short)0,(String)"true",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)""});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Check box */
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            GridRow.AddColumnProperties("checkbox", 1, isAjaxCallMode( ), new Object[] {(String)chkUsuarioPerfil_Update_Internalname,StringUtil.BoolToStr( A659UsuarioPerfil_Update),(String)"",(String)"",(short)-1,(short)0,(String)"true",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)""});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Check box */
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            GridRow.AddColumnProperties("checkbox", 1, isAjaxCallMode( ), new Object[] {(String)chkUsuarioPerfil_Delete_Internalname,StringUtil.BoolToStr( A546UsuarioPerfil_Delete),(String)"",(String)"",(short)-1,(short)0,(String)"true",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)""});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Check box */
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            GridRow.AddColumnProperties("checkbox", 1, isAjaxCallMode( ), new Object[] {(String)chkUsuarioPerfil_Display_Internalname,StringUtil.BoolToStr( A543UsuarioPerfil_Display),(String)"",(String)"",(short)-1,(short)0,(String)"true",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)""});
            GxWebStd.gx_hidden_field( context, "gxhash_USUARIO_CODIGO"+"_"+sGXsfl_63_idx, GetSecureSignedToken( sGXsfl_63_idx, context.localUtil.Format( (decimal)(A1Usuario_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_PERFIL_CODIGO"+"_"+sGXsfl_63_idx, GetSecureSignedToken( sGXsfl_63_idx, context.localUtil.Format( (decimal)(A3Perfil_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, "gxhash_USUARIOPERFIL_INSERT"+"_"+sGXsfl_63_idx, GetSecureSignedToken( sGXsfl_63_idx, A544UsuarioPerfil_Insert));
            GxWebStd.gx_hidden_field( context, "gxhash_USUARIOPERFIL_UPDATE"+"_"+sGXsfl_63_idx, GetSecureSignedToken( sGXsfl_63_idx, A659UsuarioPerfil_Update));
            GxWebStd.gx_hidden_field( context, "gxhash_USUARIOPERFIL_DELETE"+"_"+sGXsfl_63_idx, GetSecureSignedToken( sGXsfl_63_idx, A546UsuarioPerfil_Delete));
            GxWebStd.gx_hidden_field( context, "gxhash_USUARIOPERFIL_DISPLAY"+"_"+sGXsfl_63_idx, GetSecureSignedToken( sGXsfl_63_idx, A543UsuarioPerfil_Display));
            GridContainer.AddRow(GridRow);
            nGXsfl_63_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_63_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_63_idx+1));
            sGXsfl_63_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_63_idx), 4, 0)), 4, "0");
            SubsflControlProps_632( ) ;
         }
         /* End function sendrow_632 */
      }

      protected void init_default_properties( )
      {
         lblUsuarioperfiltitle_Internalname = "USUARIOPERFILTITLE";
         imgInsert_Internalname = "INSERT";
         tblTableactions_Internalname = "TABLEACTIONS";
         lblOrderedtext_Internalname = "ORDEREDTEXT";
         cmbavOrderedby_Internalname = "vORDEREDBY";
         edtavOrdereddsc_Internalname = "vORDEREDDSC";
         imgCleanfilters_Internalname = "CLEANFILTERS";
         lblDynamicfiltersprefix1_Internalname = "DYNAMICFILTERSPREFIX1";
         cmbavDynamicfiltersselector1_Internalname = "vDYNAMICFILTERSSELECTOR1";
         lblDynamicfiltersmiddle1_Internalname = "DYNAMICFILTERSMIDDLE1";
         edtavPerfil_nome1_Internalname = "vPERFIL_NOME1";
         edtavUsuario_pessoanom1_Internalname = "vUSUARIO_PESSOANOM1";
         imgAdddynamicfilters1_Internalname = "ADDDYNAMICFILTERS1";
         imgRemovedynamicfilters1_Internalname = "REMOVEDYNAMICFILTERS1";
         lblDynamicfiltersprefix2_Internalname = "DYNAMICFILTERSPREFIX2";
         cmbavDynamicfiltersselector2_Internalname = "vDYNAMICFILTERSSELECTOR2";
         lblDynamicfiltersmiddle2_Internalname = "DYNAMICFILTERSMIDDLE2";
         edtavPerfil_nome2_Internalname = "vPERFIL_NOME2";
         edtavUsuario_pessoanom2_Internalname = "vUSUARIO_PESSOANOM2";
         imgRemovedynamicfilters2_Internalname = "REMOVEDYNAMICFILTERS2";
         tblTabledynamicfilters_Internalname = "TABLEDYNAMICFILTERS";
         lblJsdynamicfilters_Internalname = "JSDYNAMICFILTERS";
         tblTablefilters_Internalname = "TABLEFILTERS";
         tblTableheader_Internalname = "TABLEHEADER";
         edtavUpdate_Internalname = "vUPDATE";
         edtavDelete_Internalname = "vDELETE";
         edtUsuario_Codigo_Internalname = "USUARIO_CODIGO";
         edtPerfil_Codigo_Internalname = "PERFIL_CODIGO";
         edtUsuario_PessoaCod_Internalname = "USUARIO_PESSOACOD";
         edtUsuario_Nome_Internalname = "USUARIO_NOME";
         edtUsuario_PessoaNom_Internalname = "USUARIO_PESSOANOM";
         edtPerfil_Nome_Internalname = "PERFIL_NOME";
         cmbPerfil_Tipo_Internalname = "PERFIL_TIPO";
         chkUsuarioPerfil_Insert_Internalname = "USUARIOPERFIL_INSERT";
         chkUsuarioPerfil_Update_Internalname = "USUARIOPERFIL_UPDATE";
         chkUsuarioPerfil_Delete_Internalname = "USUARIOPERFIL_DELETE";
         chkUsuarioPerfil_Display_Internalname = "USUARIOPERFIL_DISPLAY";
         Gridpaginationbar_Internalname = "GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = "GRIDTABLEWITHPAGINATIONBAR";
         tblTablegridheader_Internalname = "TABLEGRIDHEADER";
         tblTablemain_Internalname = "TABLEMAIN";
         Workwithplusutilities1_Internalname = "WORKWITHPLUSUTILITIES1";
         chkavDynamicfiltersenabled2_Internalname = "vDYNAMICFILTERSENABLED2";
         edtavTfusuario_nome_Internalname = "vTFUSUARIO_NOME";
         edtavTfusuario_nome_sel_Internalname = "vTFUSUARIO_NOME_SEL";
         edtavTfusuario_pessoanom_Internalname = "vTFUSUARIO_PESSOANOM";
         edtavTfusuario_pessoanom_sel_Internalname = "vTFUSUARIO_PESSOANOM_SEL";
         edtavTfperfil_nome_Internalname = "vTFPERFIL_NOME";
         edtavTfperfil_nome_sel_Internalname = "vTFPERFIL_NOME_SEL";
         edtavTfusuarioperfil_insert_sel_Internalname = "vTFUSUARIOPERFIL_INSERT_SEL";
         edtavTfusuarioperfil_update_sel_Internalname = "vTFUSUARIOPERFIL_UPDATE_SEL";
         edtavTfusuarioperfil_delete_sel_Internalname = "vTFUSUARIOPERFIL_DELETE_SEL";
         edtavTfusuarioperfil_display_sel_Internalname = "vTFUSUARIOPERFIL_DISPLAY_SEL";
         Ddo_usuario_nome_Internalname = "DDO_USUARIO_NOME";
         edtavDdo_usuario_nometitlecontrolidtoreplace_Internalname = "vDDO_USUARIO_NOMETITLECONTROLIDTOREPLACE";
         Ddo_usuario_pessoanom_Internalname = "DDO_USUARIO_PESSOANOM";
         edtavDdo_usuario_pessoanomtitlecontrolidtoreplace_Internalname = "vDDO_USUARIO_PESSOANOMTITLECONTROLIDTOREPLACE";
         Ddo_perfil_nome_Internalname = "DDO_PERFIL_NOME";
         edtavDdo_perfil_nometitlecontrolidtoreplace_Internalname = "vDDO_PERFIL_NOMETITLECONTROLIDTOREPLACE";
         Ddo_perfil_tipo_Internalname = "DDO_PERFIL_TIPO";
         edtavDdo_perfil_tipotitlecontrolidtoreplace_Internalname = "vDDO_PERFIL_TIPOTITLECONTROLIDTOREPLACE";
         Ddo_usuarioperfil_insert_Internalname = "DDO_USUARIOPERFIL_INSERT";
         edtavDdo_usuarioperfil_inserttitlecontrolidtoreplace_Internalname = "vDDO_USUARIOPERFIL_INSERTTITLECONTROLIDTOREPLACE";
         Ddo_usuarioperfil_update_Internalname = "DDO_USUARIOPERFIL_UPDATE";
         edtavDdo_usuarioperfil_updatetitlecontrolidtoreplace_Internalname = "vDDO_USUARIOPERFIL_UPDATETITLECONTROLIDTOREPLACE";
         Ddo_usuarioperfil_delete_Internalname = "DDO_USUARIOPERFIL_DELETE";
         edtavDdo_usuarioperfil_deletetitlecontrolidtoreplace_Internalname = "vDDO_USUARIOPERFIL_DELETETITLECONTROLIDTOREPLACE";
         Ddo_usuarioperfil_display_Internalname = "DDO_USUARIOPERFIL_DISPLAY";
         edtavDdo_usuarioperfil_displaytitlecontrolidtoreplace_Internalname = "vDDO_USUARIOPERFIL_DISPLAYTITLECONTROLIDTOREPLACE";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         cmbPerfil_Tipo_Jsonclick = "";
         edtPerfil_Nome_Jsonclick = "";
         edtUsuario_PessoaNom_Jsonclick = "";
         edtUsuario_Nome_Jsonclick = "";
         edtUsuario_PessoaCod_Jsonclick = "";
         edtPerfil_Codigo_Jsonclick = "";
         edtUsuario_Codigo_Jsonclick = "";
         imgInsert_Visible = 1;
         edtavUsuario_pessoanom2_Jsonclick = "";
         edtavPerfil_nome2_Jsonclick = "";
         cmbavDynamicfiltersselector2_Jsonclick = "";
         imgRemovedynamicfilters1_Visible = 1;
         imgAdddynamicfilters1_Visible = 1;
         edtavUsuario_pessoanom1_Jsonclick = "";
         edtavPerfil_nome1_Jsonclick = "";
         cmbavDynamicfiltersselector1_Jsonclick = "";
         edtavOrdereddsc_Jsonclick = "";
         cmbavOrderedby_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = "";
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = "";
         chkUsuarioPerfil_Display_Titleformat = 0;
         chkUsuarioPerfil_Delete_Titleformat = 0;
         chkUsuarioPerfil_Update_Titleformat = 0;
         chkUsuarioPerfil_Insert_Titleformat = 0;
         cmbPerfil_Tipo_Titleformat = 0;
         edtPerfil_Nome_Titleformat = 0;
         edtUsuario_PessoaNom_Titleformat = 0;
         edtUsuario_Nome_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         edtavUsuario_pessoanom2_Visible = 1;
         edtavPerfil_nome2_Visible = 1;
         edtavUsuario_pessoanom1_Visible = 1;
         edtavPerfil_nome1_Visible = 1;
         edtavDelete_Visible = -1;
         edtavUpdate_Visible = -1;
         chkUsuarioPerfil_Display.Title.Text = "Dsp";
         chkUsuarioPerfil_Delete.Title.Text = "Dlt";
         chkUsuarioPerfil_Update.Title.Text = "Upd";
         chkUsuarioPerfil_Insert.Title.Text = "Ins";
         cmbPerfil_Tipo.Title.Text = "Tipo do Perfil";
         edtPerfil_Nome_Title = "Perfil";
         edtUsuario_PessoaNom_Title = "Pessoa";
         edtUsuario_Nome_Title = "Usu�rio";
         edtavOrdereddsc_Visible = 1;
         lblJsdynamicfilters_Caption = "JSDynamicFilters";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         chkavDynamicfiltersenabled2.Caption = "";
         chkUsuarioPerfil_Display.Caption = "";
         chkUsuarioPerfil_Delete.Caption = "";
         chkUsuarioPerfil_Update.Caption = "";
         chkUsuarioPerfil_Insert.Caption = "";
         edtavDdo_usuarioperfil_displaytitlecontrolidtoreplace_Visible = 1;
         edtavDdo_usuarioperfil_deletetitlecontrolidtoreplace_Visible = 1;
         edtavDdo_usuarioperfil_updatetitlecontrolidtoreplace_Visible = 1;
         edtavDdo_usuarioperfil_inserttitlecontrolidtoreplace_Visible = 1;
         edtavDdo_perfil_tipotitlecontrolidtoreplace_Visible = 1;
         edtavDdo_perfil_nometitlecontrolidtoreplace_Visible = 1;
         edtavDdo_usuario_pessoanomtitlecontrolidtoreplace_Visible = 1;
         edtavDdo_usuario_nometitlecontrolidtoreplace_Visible = 1;
         edtavTfusuarioperfil_display_sel_Jsonclick = "";
         edtavTfusuarioperfil_display_sel_Visible = 1;
         edtavTfusuarioperfil_delete_sel_Jsonclick = "";
         edtavTfusuarioperfil_delete_sel_Visible = 1;
         edtavTfusuarioperfil_update_sel_Jsonclick = "";
         edtavTfusuarioperfil_update_sel_Visible = 1;
         edtavTfusuarioperfil_insert_sel_Jsonclick = "";
         edtavTfusuarioperfil_insert_sel_Visible = 1;
         edtavTfperfil_nome_sel_Jsonclick = "";
         edtavTfperfil_nome_sel_Visible = 1;
         edtavTfperfil_nome_Jsonclick = "";
         edtavTfperfil_nome_Visible = 1;
         edtavTfusuario_pessoanom_sel_Jsonclick = "";
         edtavTfusuario_pessoanom_sel_Visible = 1;
         edtavTfusuario_pessoanom_Jsonclick = "";
         edtavTfusuario_pessoanom_Visible = 1;
         edtavTfusuario_nome_sel_Jsonclick = "";
         edtavTfusuario_nome_sel_Visible = 1;
         edtavTfusuario_nome_Jsonclick = "";
         edtavTfusuario_nome_Visible = 1;
         chkavDynamicfiltersenabled2.Visible = 1;
         Ddo_usuarioperfil_display_Searchbuttontext = "Pesquisar";
         Ddo_usuarioperfil_display_Cleanfilter = "Limpar pesquisa";
         Ddo_usuarioperfil_display_Sortdsc = "Ordenar de Z � A";
         Ddo_usuarioperfil_display_Sortasc = "Ordenar de A � Z";
         Ddo_usuarioperfil_display_Datalistfixedvalues = "1:Marcado,2:Desmarcado";
         Ddo_usuarioperfil_display_Datalisttype = "FixedValues";
         Ddo_usuarioperfil_display_Includedatalist = Convert.ToBoolean( -1);
         Ddo_usuarioperfil_display_Includefilter = Convert.ToBoolean( 0);
         Ddo_usuarioperfil_display_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_usuarioperfil_display_Includesortasc = Convert.ToBoolean( -1);
         Ddo_usuarioperfil_display_Titlecontrolidtoreplace = "";
         Ddo_usuarioperfil_display_Dropdownoptionstype = "GridTitleSettings";
         Ddo_usuarioperfil_display_Cls = "ColumnSettings";
         Ddo_usuarioperfil_display_Tooltip = "Op��es";
         Ddo_usuarioperfil_display_Caption = "";
         Ddo_usuarioperfil_delete_Searchbuttontext = "Pesquisar";
         Ddo_usuarioperfil_delete_Cleanfilter = "Limpar pesquisa";
         Ddo_usuarioperfil_delete_Sortdsc = "Ordenar de Z � A";
         Ddo_usuarioperfil_delete_Sortasc = "Ordenar de A � Z";
         Ddo_usuarioperfil_delete_Datalistfixedvalues = "1:Marcado,2:Desmarcado";
         Ddo_usuarioperfil_delete_Datalisttype = "FixedValues";
         Ddo_usuarioperfil_delete_Includedatalist = Convert.ToBoolean( -1);
         Ddo_usuarioperfil_delete_Includefilter = Convert.ToBoolean( 0);
         Ddo_usuarioperfil_delete_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_usuarioperfil_delete_Includesortasc = Convert.ToBoolean( -1);
         Ddo_usuarioperfil_delete_Titlecontrolidtoreplace = "";
         Ddo_usuarioperfil_delete_Dropdownoptionstype = "GridTitleSettings";
         Ddo_usuarioperfil_delete_Cls = "ColumnSettings";
         Ddo_usuarioperfil_delete_Tooltip = "Op��es";
         Ddo_usuarioperfil_delete_Caption = "";
         Ddo_usuarioperfil_update_Searchbuttontext = "Pesquisar";
         Ddo_usuarioperfil_update_Cleanfilter = "Limpar pesquisa";
         Ddo_usuarioperfil_update_Sortdsc = "Ordenar de Z � A";
         Ddo_usuarioperfil_update_Sortasc = "Ordenar de A � Z";
         Ddo_usuarioperfil_update_Datalistfixedvalues = "1:Marcado,2:Desmarcado";
         Ddo_usuarioperfil_update_Datalisttype = "FixedValues";
         Ddo_usuarioperfil_update_Includedatalist = Convert.ToBoolean( -1);
         Ddo_usuarioperfil_update_Includefilter = Convert.ToBoolean( 0);
         Ddo_usuarioperfil_update_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_usuarioperfil_update_Includesortasc = Convert.ToBoolean( -1);
         Ddo_usuarioperfil_update_Titlecontrolidtoreplace = "";
         Ddo_usuarioperfil_update_Dropdownoptionstype = "GridTitleSettings";
         Ddo_usuarioperfil_update_Cls = "ColumnSettings";
         Ddo_usuarioperfil_update_Tooltip = "Op��es";
         Ddo_usuarioperfil_update_Caption = "";
         Ddo_usuarioperfil_insert_Searchbuttontext = "Pesquisar";
         Ddo_usuarioperfil_insert_Cleanfilter = "Limpar pesquisa";
         Ddo_usuarioperfil_insert_Sortdsc = "Ordenar de Z � A";
         Ddo_usuarioperfil_insert_Sortasc = "Ordenar de A � Z";
         Ddo_usuarioperfil_insert_Datalistfixedvalues = "1:Marcado,2:Desmarcado";
         Ddo_usuarioperfil_insert_Datalisttype = "FixedValues";
         Ddo_usuarioperfil_insert_Includedatalist = Convert.ToBoolean( -1);
         Ddo_usuarioperfil_insert_Includefilter = Convert.ToBoolean( 0);
         Ddo_usuarioperfil_insert_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_usuarioperfil_insert_Includesortasc = Convert.ToBoolean( -1);
         Ddo_usuarioperfil_insert_Titlecontrolidtoreplace = "";
         Ddo_usuarioperfil_insert_Dropdownoptionstype = "GridTitleSettings";
         Ddo_usuarioperfil_insert_Cls = "ColumnSettings";
         Ddo_usuarioperfil_insert_Tooltip = "Op��es";
         Ddo_usuarioperfil_insert_Caption = "";
         Ddo_perfil_tipo_Searchbuttontext = "Filtrar Selecionados";
         Ddo_perfil_tipo_Cleanfilter = "Limpar pesquisa";
         Ddo_perfil_tipo_Sortdsc = "Ordenar de Z � A";
         Ddo_perfil_tipo_Sortasc = "Ordenar de A � Z";
         Ddo_perfil_tipo_Datalistfixedvalues = "0:Desconhecido,1:Auditor,2:Contador,3:Contratada,4:Contratante,5:Financeiro,6:Usuario,10:Licensiado,99:Administrador GAM";
         Ddo_perfil_tipo_Allowmultipleselection = Convert.ToBoolean( -1);
         Ddo_perfil_tipo_Datalisttype = "FixedValues";
         Ddo_perfil_tipo_Includedatalist = Convert.ToBoolean( -1);
         Ddo_perfil_tipo_Includefilter = Convert.ToBoolean( 0);
         Ddo_perfil_tipo_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_perfil_tipo_Includesortasc = Convert.ToBoolean( -1);
         Ddo_perfil_tipo_Titlecontrolidtoreplace = "";
         Ddo_perfil_tipo_Dropdownoptionstype = "GridTitleSettings";
         Ddo_perfil_tipo_Cls = "ColumnSettings";
         Ddo_perfil_tipo_Tooltip = "Op��es";
         Ddo_perfil_tipo_Caption = "";
         Ddo_perfil_nome_Searchbuttontext = "Pesquisar";
         Ddo_perfil_nome_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_perfil_nome_Cleanfilter = "Limpar pesquisa";
         Ddo_perfil_nome_Loadingdata = "Carregando dados...";
         Ddo_perfil_nome_Sortdsc = "Ordenar de Z � A";
         Ddo_perfil_nome_Sortasc = "Ordenar de A � Z";
         Ddo_perfil_nome_Datalistupdateminimumcharacters = 0;
         Ddo_perfil_nome_Datalistproc = "GetWWUsuarioPerfilFilterData";
         Ddo_perfil_nome_Datalisttype = "Dynamic";
         Ddo_perfil_nome_Includedatalist = Convert.ToBoolean( -1);
         Ddo_perfil_nome_Filterisrange = Convert.ToBoolean( 0);
         Ddo_perfil_nome_Filtertype = "Character";
         Ddo_perfil_nome_Includefilter = Convert.ToBoolean( -1);
         Ddo_perfil_nome_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_perfil_nome_Includesortasc = Convert.ToBoolean( -1);
         Ddo_perfil_nome_Titlecontrolidtoreplace = "";
         Ddo_perfil_nome_Dropdownoptionstype = "GridTitleSettings";
         Ddo_perfil_nome_Cls = "ColumnSettings";
         Ddo_perfil_nome_Tooltip = "Op��es";
         Ddo_perfil_nome_Caption = "";
         Ddo_usuario_pessoanom_Searchbuttontext = "Pesquisar";
         Ddo_usuario_pessoanom_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_usuario_pessoanom_Cleanfilter = "Limpar pesquisa";
         Ddo_usuario_pessoanom_Loadingdata = "Carregando dados...";
         Ddo_usuario_pessoanom_Sortdsc = "Ordenar de Z � A";
         Ddo_usuario_pessoanom_Sortasc = "Ordenar de A � Z";
         Ddo_usuario_pessoanom_Datalistupdateminimumcharacters = 0;
         Ddo_usuario_pessoanom_Datalistproc = "GetWWUsuarioPerfilFilterData";
         Ddo_usuario_pessoanom_Datalisttype = "Dynamic";
         Ddo_usuario_pessoanom_Includedatalist = Convert.ToBoolean( -1);
         Ddo_usuario_pessoanom_Filterisrange = Convert.ToBoolean( 0);
         Ddo_usuario_pessoanom_Filtertype = "Character";
         Ddo_usuario_pessoanom_Includefilter = Convert.ToBoolean( -1);
         Ddo_usuario_pessoanom_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_usuario_pessoanom_Includesortasc = Convert.ToBoolean( -1);
         Ddo_usuario_pessoanom_Titlecontrolidtoreplace = "";
         Ddo_usuario_pessoanom_Dropdownoptionstype = "GridTitleSettings";
         Ddo_usuario_pessoanom_Cls = "ColumnSettings";
         Ddo_usuario_pessoanom_Tooltip = "Op��es";
         Ddo_usuario_pessoanom_Caption = "";
         Ddo_usuario_nome_Searchbuttontext = "Pesquisar";
         Ddo_usuario_nome_Noresultsfound = "- N�o se encontraram resultados -";
         Ddo_usuario_nome_Cleanfilter = "Limpar pesquisa";
         Ddo_usuario_nome_Loadingdata = "Carregando dados...";
         Ddo_usuario_nome_Sortdsc = "Ordenar de Z � A";
         Ddo_usuario_nome_Sortasc = "Ordenar de A � Z";
         Ddo_usuario_nome_Datalistupdateminimumcharacters = 0;
         Ddo_usuario_nome_Datalistproc = "GetWWUsuarioPerfilFilterData";
         Ddo_usuario_nome_Datalisttype = "Dynamic";
         Ddo_usuario_nome_Includedatalist = Convert.ToBoolean( -1);
         Ddo_usuario_nome_Filterisrange = Convert.ToBoolean( 0);
         Ddo_usuario_nome_Filtertype = "Character";
         Ddo_usuario_nome_Includefilter = Convert.ToBoolean( -1);
         Ddo_usuario_nome_Includesortdsc = Convert.ToBoolean( -1);
         Ddo_usuario_nome_Includesortasc = Convert.ToBoolean( -1);
         Ddo_usuario_nome_Titlecontrolidtoreplace = "";
         Ddo_usuario_nome_Dropdownoptionstype = "GridTitleSettings";
         Ddo_usuario_nome_Cls = "ColumnSettings";
         Ddo_usuario_nome_Tooltip = "Op��es";
         Ddo_usuario_nome_Caption = "";
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = " Usuario x Perfil";
         subGrid_Rows = 0;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A3Perfil_Codigo',fld:'PERFIL_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV57ddo_Usuario_NomeTitleControlIdToReplace',fld:'vDDO_USUARIO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_Usuario_PessoaNomTitleControlIdToReplace',fld:'vDDO_USUARIO_PESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_Perfil_NomeTitleControlIdToReplace',fld:'vDDO_PERFIL_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV69ddo_Perfil_TipoTitleControlIdToReplace',fld:'vDDO_PERFIL_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_UsuarioPerfil_InsertTitleControlIdToReplace',fld:'vDDO_USUARIOPERFIL_INSERTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75ddo_UsuarioPerfil_UpdateTitleControlIdToReplace',fld:'vDDO_USUARIOPERFIL_UPDATETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78ddo_UsuarioPerfil_DeleteTitleControlIdToReplace',fld:'vDDO_USUARIOPERFIL_DELETETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV81ddo_UsuarioPerfil_DisplayTitleControlIdToReplace',fld:'vDDO_USUARIOPERFIL_DISPLAYTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV18Perfil_Nome1',fld:'vPERFIL_NOME1',pic:'@!',nv:''},{av:'AV19Usuario_PessoaNom1',fld:'vUSUARIO_PESSOANOM1',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV23Perfil_Nome2',fld:'vPERFIL_NOME2',pic:'@!',nv:''},{av:'AV24Usuario_PessoaNom2',fld:'vUSUARIO_PESSOANOM2',pic:'@!',nv:''},{av:'AV55TFUsuario_Nome',fld:'vTFUSUARIO_NOME',pic:'@!',nv:''},{av:'AV56TFUsuario_Nome_Sel',fld:'vTFUSUARIO_NOME_SEL',pic:'@!',nv:''},{av:'AV59TFUsuario_PessoaNom',fld:'vTFUSUARIO_PESSOANOM',pic:'@!',nv:''},{av:'AV60TFUsuario_PessoaNom_Sel',fld:'vTFUSUARIO_PESSOANOM_SEL',pic:'@!',nv:''},{av:'AV63TFPerfil_Nome',fld:'vTFPERFIL_NOME',pic:'@!',nv:''},{av:'AV64TFPerfil_Nome_Sel',fld:'vTFPERFIL_NOME_SEL',pic:'@!',nv:''},{av:'AV68TFPerfil_Tipo_Sels',fld:'vTFPERFIL_TIPO_SELS',pic:'',nv:null},{av:'AV71TFUsuarioPerfil_Insert_Sel',fld:'vTFUSUARIOPERFIL_INSERT_SEL',pic:'9',nv:0},{av:'AV74TFUsuarioPerfil_Update_Sel',fld:'vTFUSUARIOPERFIL_UPDATE_SEL',pic:'9',nv:0},{av:'AV77TFUsuarioPerfil_Delete_Sel',fld:'vTFUSUARIOPERFIL_DELETE_SEL',pic:'9',nv:0},{av:'AV80TFUsuarioPerfil_Display_Sel',fld:'vTFUSUARIOPERFIL_DISPLAY_SEL',pic:'9',nv:0},{av:'AV108Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV26DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV25DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false}],oparms:[{av:'AV54Usuario_NomeTitleFilterData',fld:'vUSUARIO_NOMETITLEFILTERDATA',pic:'',nv:null},{av:'AV58Usuario_PessoaNomTitleFilterData',fld:'vUSUARIO_PESSOANOMTITLEFILTERDATA',pic:'',nv:null},{av:'AV62Perfil_NomeTitleFilterData',fld:'vPERFIL_NOMETITLEFILTERDATA',pic:'',nv:null},{av:'AV66Perfil_TipoTitleFilterData',fld:'vPERFIL_TIPOTITLEFILTERDATA',pic:'',nv:null},{av:'AV70UsuarioPerfil_InsertTitleFilterData',fld:'vUSUARIOPERFIL_INSERTTITLEFILTERDATA',pic:'',nv:null},{av:'AV73UsuarioPerfil_UpdateTitleFilterData',fld:'vUSUARIOPERFIL_UPDATETITLEFILTERDATA',pic:'',nv:null},{av:'AV76UsuarioPerfil_DeleteTitleFilterData',fld:'vUSUARIOPERFIL_DELETETITLEFILTERDATA',pic:'',nv:null},{av:'AV79UsuarioPerfil_DisplayTitleFilterData',fld:'vUSUARIOPERFIL_DISPLAYTITLEFILTERDATA',pic:'',nv:null},{av:'edtUsuario_Nome_Titleformat',ctrl:'USUARIO_NOME',prop:'Titleformat'},{av:'edtUsuario_Nome_Title',ctrl:'USUARIO_NOME',prop:'Title'},{av:'edtUsuario_PessoaNom_Titleformat',ctrl:'USUARIO_PESSOANOM',prop:'Titleformat'},{av:'edtUsuario_PessoaNom_Title',ctrl:'USUARIO_PESSOANOM',prop:'Title'},{av:'edtPerfil_Nome_Titleformat',ctrl:'PERFIL_NOME',prop:'Titleformat'},{av:'edtPerfil_Nome_Title',ctrl:'PERFIL_NOME',prop:'Title'},{av:'cmbPerfil_Tipo'},{av:'chkUsuarioPerfil_Insert_Titleformat',ctrl:'USUARIOPERFIL_INSERT',prop:'Titleformat'},{av:'chkUsuarioPerfil_Insert.Title.Text',ctrl:'USUARIOPERFIL_INSERT',prop:'Title'},{av:'chkUsuarioPerfil_Update_Titleformat',ctrl:'USUARIOPERFIL_UPDATE',prop:'Titleformat'},{av:'chkUsuarioPerfil_Update.Title.Text',ctrl:'USUARIOPERFIL_UPDATE',prop:'Title'},{av:'chkUsuarioPerfil_Delete_Titleformat',ctrl:'USUARIOPERFIL_DELETE',prop:'Titleformat'},{av:'chkUsuarioPerfil_Delete.Title.Text',ctrl:'USUARIOPERFIL_DELETE',prop:'Title'},{av:'chkUsuarioPerfil_Display_Titleformat',ctrl:'USUARIOPERFIL_DISPLAY',prop:'Titleformat'},{av:'chkUsuarioPerfil_Display.Title.Text',ctrl:'USUARIOPERFIL_DISPLAY',prop:'Title'},{av:'AV84GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV85GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'edtavUpdate_Visible',ctrl:'vUPDATE',prop:'Visible'},{av:'edtavDelete_Visible',ctrl:'vDELETE',prop:'Visible'},{av:'imgInsert_Visible',ctrl:'INSERT',prop:'Visible'},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E118Y2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV18Perfil_Nome1',fld:'vPERFIL_NOME1',pic:'@!',nv:''},{av:'AV19Usuario_PessoaNom1',fld:'vUSUARIO_PESSOANOM1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV23Perfil_Nome2',fld:'vPERFIL_NOME2',pic:'@!',nv:''},{av:'AV24Usuario_PessoaNom2',fld:'vUSUARIO_PESSOANOM2',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV55TFUsuario_Nome',fld:'vTFUSUARIO_NOME',pic:'@!',nv:''},{av:'AV56TFUsuario_Nome_Sel',fld:'vTFUSUARIO_NOME_SEL',pic:'@!',nv:''},{av:'AV59TFUsuario_PessoaNom',fld:'vTFUSUARIO_PESSOANOM',pic:'@!',nv:''},{av:'AV60TFUsuario_PessoaNom_Sel',fld:'vTFUSUARIO_PESSOANOM_SEL',pic:'@!',nv:''},{av:'AV63TFPerfil_Nome',fld:'vTFPERFIL_NOME',pic:'@!',nv:''},{av:'AV64TFPerfil_Nome_Sel',fld:'vTFPERFIL_NOME_SEL',pic:'@!',nv:''},{av:'AV71TFUsuarioPerfil_Insert_Sel',fld:'vTFUSUARIOPERFIL_INSERT_SEL',pic:'9',nv:0},{av:'AV74TFUsuarioPerfil_Update_Sel',fld:'vTFUSUARIOPERFIL_UPDATE_SEL',pic:'9',nv:0},{av:'AV77TFUsuarioPerfil_Delete_Sel',fld:'vTFUSUARIOPERFIL_DELETE_SEL',pic:'9',nv:0},{av:'AV80TFUsuarioPerfil_Display_Sel',fld:'vTFUSUARIOPERFIL_DISPLAY_SEL',pic:'9',nv:0},{av:'AV57ddo_Usuario_NomeTitleControlIdToReplace',fld:'vDDO_USUARIO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_Usuario_PessoaNomTitleControlIdToReplace',fld:'vDDO_USUARIO_PESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_Perfil_NomeTitleControlIdToReplace',fld:'vDDO_PERFIL_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV69ddo_Perfil_TipoTitleControlIdToReplace',fld:'vDDO_PERFIL_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_UsuarioPerfil_InsertTitleControlIdToReplace',fld:'vDDO_USUARIOPERFIL_INSERTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75ddo_UsuarioPerfil_UpdateTitleControlIdToReplace',fld:'vDDO_USUARIOPERFIL_UPDATETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78ddo_UsuarioPerfil_DeleteTitleControlIdToReplace',fld:'vDDO_USUARIOPERFIL_DELETETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV81ddo_UsuarioPerfil_DisplayTitleControlIdToReplace',fld:'vDDO_USUARIOPERFIL_DISPLAYTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68TFPerfil_Tipo_Sels',fld:'vTFPERFIL_TIPO_SELS',pic:'',nv:null},{av:'AV108Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV26DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV25DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A3Perfil_Codigo',fld:'PERFIL_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("DDO_USUARIO_NOME.ONOPTIONCLICKED","{handler:'E128Y2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV18Perfil_Nome1',fld:'vPERFIL_NOME1',pic:'@!',nv:''},{av:'AV19Usuario_PessoaNom1',fld:'vUSUARIO_PESSOANOM1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV23Perfil_Nome2',fld:'vPERFIL_NOME2',pic:'@!',nv:''},{av:'AV24Usuario_PessoaNom2',fld:'vUSUARIO_PESSOANOM2',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV55TFUsuario_Nome',fld:'vTFUSUARIO_NOME',pic:'@!',nv:''},{av:'AV56TFUsuario_Nome_Sel',fld:'vTFUSUARIO_NOME_SEL',pic:'@!',nv:''},{av:'AV59TFUsuario_PessoaNom',fld:'vTFUSUARIO_PESSOANOM',pic:'@!',nv:''},{av:'AV60TFUsuario_PessoaNom_Sel',fld:'vTFUSUARIO_PESSOANOM_SEL',pic:'@!',nv:''},{av:'AV63TFPerfil_Nome',fld:'vTFPERFIL_NOME',pic:'@!',nv:''},{av:'AV64TFPerfil_Nome_Sel',fld:'vTFPERFIL_NOME_SEL',pic:'@!',nv:''},{av:'AV71TFUsuarioPerfil_Insert_Sel',fld:'vTFUSUARIOPERFIL_INSERT_SEL',pic:'9',nv:0},{av:'AV74TFUsuarioPerfil_Update_Sel',fld:'vTFUSUARIOPERFIL_UPDATE_SEL',pic:'9',nv:0},{av:'AV77TFUsuarioPerfil_Delete_Sel',fld:'vTFUSUARIOPERFIL_DELETE_SEL',pic:'9',nv:0},{av:'AV80TFUsuarioPerfil_Display_Sel',fld:'vTFUSUARIOPERFIL_DISPLAY_SEL',pic:'9',nv:0},{av:'AV57ddo_Usuario_NomeTitleControlIdToReplace',fld:'vDDO_USUARIO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_Usuario_PessoaNomTitleControlIdToReplace',fld:'vDDO_USUARIO_PESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_Perfil_NomeTitleControlIdToReplace',fld:'vDDO_PERFIL_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV69ddo_Perfil_TipoTitleControlIdToReplace',fld:'vDDO_PERFIL_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_UsuarioPerfil_InsertTitleControlIdToReplace',fld:'vDDO_USUARIOPERFIL_INSERTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75ddo_UsuarioPerfil_UpdateTitleControlIdToReplace',fld:'vDDO_USUARIOPERFIL_UPDATETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78ddo_UsuarioPerfil_DeleteTitleControlIdToReplace',fld:'vDDO_USUARIOPERFIL_DELETETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV81ddo_UsuarioPerfil_DisplayTitleControlIdToReplace',fld:'vDDO_USUARIOPERFIL_DISPLAYTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68TFPerfil_Tipo_Sels',fld:'vTFPERFIL_TIPO_SELS',pic:'',nv:null},{av:'AV108Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV26DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV25DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A3Perfil_Codigo',fld:'PERFIL_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_usuario_nome_Activeeventkey',ctrl:'DDO_USUARIO_NOME',prop:'ActiveEventKey'},{av:'Ddo_usuario_nome_Filteredtext_get',ctrl:'DDO_USUARIO_NOME',prop:'FilteredText_get'},{av:'Ddo_usuario_nome_Selectedvalue_get',ctrl:'DDO_USUARIO_NOME',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_usuario_nome_Sortedstatus',ctrl:'DDO_USUARIO_NOME',prop:'SortedStatus'},{av:'AV55TFUsuario_Nome',fld:'vTFUSUARIO_NOME',pic:'@!',nv:''},{av:'AV56TFUsuario_Nome_Sel',fld:'vTFUSUARIO_NOME_SEL',pic:'@!',nv:''},{av:'Ddo_usuario_pessoanom_Sortedstatus',ctrl:'DDO_USUARIO_PESSOANOM',prop:'SortedStatus'},{av:'Ddo_perfil_nome_Sortedstatus',ctrl:'DDO_PERFIL_NOME',prop:'SortedStatus'},{av:'Ddo_perfil_tipo_Sortedstatus',ctrl:'DDO_PERFIL_TIPO',prop:'SortedStatus'},{av:'Ddo_usuarioperfil_insert_Sortedstatus',ctrl:'DDO_USUARIOPERFIL_INSERT',prop:'SortedStatus'},{av:'Ddo_usuarioperfil_update_Sortedstatus',ctrl:'DDO_USUARIOPERFIL_UPDATE',prop:'SortedStatus'},{av:'Ddo_usuarioperfil_delete_Sortedstatus',ctrl:'DDO_USUARIOPERFIL_DELETE',prop:'SortedStatus'},{av:'Ddo_usuarioperfil_display_Sortedstatus',ctrl:'DDO_USUARIOPERFIL_DISPLAY',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_USUARIO_PESSOANOM.ONOPTIONCLICKED","{handler:'E138Y2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV18Perfil_Nome1',fld:'vPERFIL_NOME1',pic:'@!',nv:''},{av:'AV19Usuario_PessoaNom1',fld:'vUSUARIO_PESSOANOM1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV23Perfil_Nome2',fld:'vPERFIL_NOME2',pic:'@!',nv:''},{av:'AV24Usuario_PessoaNom2',fld:'vUSUARIO_PESSOANOM2',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV55TFUsuario_Nome',fld:'vTFUSUARIO_NOME',pic:'@!',nv:''},{av:'AV56TFUsuario_Nome_Sel',fld:'vTFUSUARIO_NOME_SEL',pic:'@!',nv:''},{av:'AV59TFUsuario_PessoaNom',fld:'vTFUSUARIO_PESSOANOM',pic:'@!',nv:''},{av:'AV60TFUsuario_PessoaNom_Sel',fld:'vTFUSUARIO_PESSOANOM_SEL',pic:'@!',nv:''},{av:'AV63TFPerfil_Nome',fld:'vTFPERFIL_NOME',pic:'@!',nv:''},{av:'AV64TFPerfil_Nome_Sel',fld:'vTFPERFIL_NOME_SEL',pic:'@!',nv:''},{av:'AV71TFUsuarioPerfil_Insert_Sel',fld:'vTFUSUARIOPERFIL_INSERT_SEL',pic:'9',nv:0},{av:'AV74TFUsuarioPerfil_Update_Sel',fld:'vTFUSUARIOPERFIL_UPDATE_SEL',pic:'9',nv:0},{av:'AV77TFUsuarioPerfil_Delete_Sel',fld:'vTFUSUARIOPERFIL_DELETE_SEL',pic:'9',nv:0},{av:'AV80TFUsuarioPerfil_Display_Sel',fld:'vTFUSUARIOPERFIL_DISPLAY_SEL',pic:'9',nv:0},{av:'AV57ddo_Usuario_NomeTitleControlIdToReplace',fld:'vDDO_USUARIO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_Usuario_PessoaNomTitleControlIdToReplace',fld:'vDDO_USUARIO_PESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_Perfil_NomeTitleControlIdToReplace',fld:'vDDO_PERFIL_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV69ddo_Perfil_TipoTitleControlIdToReplace',fld:'vDDO_PERFIL_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_UsuarioPerfil_InsertTitleControlIdToReplace',fld:'vDDO_USUARIOPERFIL_INSERTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75ddo_UsuarioPerfil_UpdateTitleControlIdToReplace',fld:'vDDO_USUARIOPERFIL_UPDATETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78ddo_UsuarioPerfil_DeleteTitleControlIdToReplace',fld:'vDDO_USUARIOPERFIL_DELETETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV81ddo_UsuarioPerfil_DisplayTitleControlIdToReplace',fld:'vDDO_USUARIOPERFIL_DISPLAYTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68TFPerfil_Tipo_Sels',fld:'vTFPERFIL_TIPO_SELS',pic:'',nv:null},{av:'AV108Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV26DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV25DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A3Perfil_Codigo',fld:'PERFIL_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_usuario_pessoanom_Activeeventkey',ctrl:'DDO_USUARIO_PESSOANOM',prop:'ActiveEventKey'},{av:'Ddo_usuario_pessoanom_Filteredtext_get',ctrl:'DDO_USUARIO_PESSOANOM',prop:'FilteredText_get'},{av:'Ddo_usuario_pessoanom_Selectedvalue_get',ctrl:'DDO_USUARIO_PESSOANOM',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_usuario_pessoanom_Sortedstatus',ctrl:'DDO_USUARIO_PESSOANOM',prop:'SortedStatus'},{av:'AV59TFUsuario_PessoaNom',fld:'vTFUSUARIO_PESSOANOM',pic:'@!',nv:''},{av:'AV60TFUsuario_PessoaNom_Sel',fld:'vTFUSUARIO_PESSOANOM_SEL',pic:'@!',nv:''},{av:'Ddo_usuario_nome_Sortedstatus',ctrl:'DDO_USUARIO_NOME',prop:'SortedStatus'},{av:'Ddo_perfil_nome_Sortedstatus',ctrl:'DDO_PERFIL_NOME',prop:'SortedStatus'},{av:'Ddo_perfil_tipo_Sortedstatus',ctrl:'DDO_PERFIL_TIPO',prop:'SortedStatus'},{av:'Ddo_usuarioperfil_insert_Sortedstatus',ctrl:'DDO_USUARIOPERFIL_INSERT',prop:'SortedStatus'},{av:'Ddo_usuarioperfil_update_Sortedstatus',ctrl:'DDO_USUARIOPERFIL_UPDATE',prop:'SortedStatus'},{av:'Ddo_usuarioperfil_delete_Sortedstatus',ctrl:'DDO_USUARIOPERFIL_DELETE',prop:'SortedStatus'},{av:'Ddo_usuarioperfil_display_Sortedstatus',ctrl:'DDO_USUARIOPERFIL_DISPLAY',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_PERFIL_NOME.ONOPTIONCLICKED","{handler:'E148Y2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV18Perfil_Nome1',fld:'vPERFIL_NOME1',pic:'@!',nv:''},{av:'AV19Usuario_PessoaNom1',fld:'vUSUARIO_PESSOANOM1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV23Perfil_Nome2',fld:'vPERFIL_NOME2',pic:'@!',nv:''},{av:'AV24Usuario_PessoaNom2',fld:'vUSUARIO_PESSOANOM2',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV55TFUsuario_Nome',fld:'vTFUSUARIO_NOME',pic:'@!',nv:''},{av:'AV56TFUsuario_Nome_Sel',fld:'vTFUSUARIO_NOME_SEL',pic:'@!',nv:''},{av:'AV59TFUsuario_PessoaNom',fld:'vTFUSUARIO_PESSOANOM',pic:'@!',nv:''},{av:'AV60TFUsuario_PessoaNom_Sel',fld:'vTFUSUARIO_PESSOANOM_SEL',pic:'@!',nv:''},{av:'AV63TFPerfil_Nome',fld:'vTFPERFIL_NOME',pic:'@!',nv:''},{av:'AV64TFPerfil_Nome_Sel',fld:'vTFPERFIL_NOME_SEL',pic:'@!',nv:''},{av:'AV71TFUsuarioPerfil_Insert_Sel',fld:'vTFUSUARIOPERFIL_INSERT_SEL',pic:'9',nv:0},{av:'AV74TFUsuarioPerfil_Update_Sel',fld:'vTFUSUARIOPERFIL_UPDATE_SEL',pic:'9',nv:0},{av:'AV77TFUsuarioPerfil_Delete_Sel',fld:'vTFUSUARIOPERFIL_DELETE_SEL',pic:'9',nv:0},{av:'AV80TFUsuarioPerfil_Display_Sel',fld:'vTFUSUARIOPERFIL_DISPLAY_SEL',pic:'9',nv:0},{av:'AV57ddo_Usuario_NomeTitleControlIdToReplace',fld:'vDDO_USUARIO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_Usuario_PessoaNomTitleControlIdToReplace',fld:'vDDO_USUARIO_PESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_Perfil_NomeTitleControlIdToReplace',fld:'vDDO_PERFIL_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV69ddo_Perfil_TipoTitleControlIdToReplace',fld:'vDDO_PERFIL_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_UsuarioPerfil_InsertTitleControlIdToReplace',fld:'vDDO_USUARIOPERFIL_INSERTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75ddo_UsuarioPerfil_UpdateTitleControlIdToReplace',fld:'vDDO_USUARIOPERFIL_UPDATETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78ddo_UsuarioPerfil_DeleteTitleControlIdToReplace',fld:'vDDO_USUARIOPERFIL_DELETETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV81ddo_UsuarioPerfil_DisplayTitleControlIdToReplace',fld:'vDDO_USUARIOPERFIL_DISPLAYTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68TFPerfil_Tipo_Sels',fld:'vTFPERFIL_TIPO_SELS',pic:'',nv:null},{av:'AV108Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV26DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV25DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A3Perfil_Codigo',fld:'PERFIL_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_perfil_nome_Activeeventkey',ctrl:'DDO_PERFIL_NOME',prop:'ActiveEventKey'},{av:'Ddo_perfil_nome_Filteredtext_get',ctrl:'DDO_PERFIL_NOME',prop:'FilteredText_get'},{av:'Ddo_perfil_nome_Selectedvalue_get',ctrl:'DDO_PERFIL_NOME',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_perfil_nome_Sortedstatus',ctrl:'DDO_PERFIL_NOME',prop:'SortedStatus'},{av:'AV63TFPerfil_Nome',fld:'vTFPERFIL_NOME',pic:'@!',nv:''},{av:'AV64TFPerfil_Nome_Sel',fld:'vTFPERFIL_NOME_SEL',pic:'@!',nv:''},{av:'Ddo_usuario_nome_Sortedstatus',ctrl:'DDO_USUARIO_NOME',prop:'SortedStatus'},{av:'Ddo_usuario_pessoanom_Sortedstatus',ctrl:'DDO_USUARIO_PESSOANOM',prop:'SortedStatus'},{av:'Ddo_perfil_tipo_Sortedstatus',ctrl:'DDO_PERFIL_TIPO',prop:'SortedStatus'},{av:'Ddo_usuarioperfil_insert_Sortedstatus',ctrl:'DDO_USUARIOPERFIL_INSERT',prop:'SortedStatus'},{av:'Ddo_usuarioperfil_update_Sortedstatus',ctrl:'DDO_USUARIOPERFIL_UPDATE',prop:'SortedStatus'},{av:'Ddo_usuarioperfil_delete_Sortedstatus',ctrl:'DDO_USUARIOPERFIL_DELETE',prop:'SortedStatus'},{av:'Ddo_usuarioperfil_display_Sortedstatus',ctrl:'DDO_USUARIOPERFIL_DISPLAY',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_PERFIL_TIPO.ONOPTIONCLICKED","{handler:'E158Y2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV18Perfil_Nome1',fld:'vPERFIL_NOME1',pic:'@!',nv:''},{av:'AV19Usuario_PessoaNom1',fld:'vUSUARIO_PESSOANOM1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV23Perfil_Nome2',fld:'vPERFIL_NOME2',pic:'@!',nv:''},{av:'AV24Usuario_PessoaNom2',fld:'vUSUARIO_PESSOANOM2',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV55TFUsuario_Nome',fld:'vTFUSUARIO_NOME',pic:'@!',nv:''},{av:'AV56TFUsuario_Nome_Sel',fld:'vTFUSUARIO_NOME_SEL',pic:'@!',nv:''},{av:'AV59TFUsuario_PessoaNom',fld:'vTFUSUARIO_PESSOANOM',pic:'@!',nv:''},{av:'AV60TFUsuario_PessoaNom_Sel',fld:'vTFUSUARIO_PESSOANOM_SEL',pic:'@!',nv:''},{av:'AV63TFPerfil_Nome',fld:'vTFPERFIL_NOME',pic:'@!',nv:''},{av:'AV64TFPerfil_Nome_Sel',fld:'vTFPERFIL_NOME_SEL',pic:'@!',nv:''},{av:'AV71TFUsuarioPerfil_Insert_Sel',fld:'vTFUSUARIOPERFIL_INSERT_SEL',pic:'9',nv:0},{av:'AV74TFUsuarioPerfil_Update_Sel',fld:'vTFUSUARIOPERFIL_UPDATE_SEL',pic:'9',nv:0},{av:'AV77TFUsuarioPerfil_Delete_Sel',fld:'vTFUSUARIOPERFIL_DELETE_SEL',pic:'9',nv:0},{av:'AV80TFUsuarioPerfil_Display_Sel',fld:'vTFUSUARIOPERFIL_DISPLAY_SEL',pic:'9',nv:0},{av:'AV57ddo_Usuario_NomeTitleControlIdToReplace',fld:'vDDO_USUARIO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_Usuario_PessoaNomTitleControlIdToReplace',fld:'vDDO_USUARIO_PESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_Perfil_NomeTitleControlIdToReplace',fld:'vDDO_PERFIL_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV69ddo_Perfil_TipoTitleControlIdToReplace',fld:'vDDO_PERFIL_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_UsuarioPerfil_InsertTitleControlIdToReplace',fld:'vDDO_USUARIOPERFIL_INSERTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75ddo_UsuarioPerfil_UpdateTitleControlIdToReplace',fld:'vDDO_USUARIOPERFIL_UPDATETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78ddo_UsuarioPerfil_DeleteTitleControlIdToReplace',fld:'vDDO_USUARIOPERFIL_DELETETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV81ddo_UsuarioPerfil_DisplayTitleControlIdToReplace',fld:'vDDO_USUARIOPERFIL_DISPLAYTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68TFPerfil_Tipo_Sels',fld:'vTFPERFIL_TIPO_SELS',pic:'',nv:null},{av:'AV108Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV26DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV25DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A3Perfil_Codigo',fld:'PERFIL_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_perfil_tipo_Activeeventkey',ctrl:'DDO_PERFIL_TIPO',prop:'ActiveEventKey'},{av:'Ddo_perfil_tipo_Selectedvalue_get',ctrl:'DDO_PERFIL_TIPO',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_perfil_tipo_Sortedstatus',ctrl:'DDO_PERFIL_TIPO',prop:'SortedStatus'},{av:'AV68TFPerfil_Tipo_Sels',fld:'vTFPERFIL_TIPO_SELS',pic:'',nv:null},{av:'Ddo_usuario_nome_Sortedstatus',ctrl:'DDO_USUARIO_NOME',prop:'SortedStatus'},{av:'Ddo_usuario_pessoanom_Sortedstatus',ctrl:'DDO_USUARIO_PESSOANOM',prop:'SortedStatus'},{av:'Ddo_perfil_nome_Sortedstatus',ctrl:'DDO_PERFIL_NOME',prop:'SortedStatus'},{av:'Ddo_usuarioperfil_insert_Sortedstatus',ctrl:'DDO_USUARIOPERFIL_INSERT',prop:'SortedStatus'},{av:'Ddo_usuarioperfil_update_Sortedstatus',ctrl:'DDO_USUARIOPERFIL_UPDATE',prop:'SortedStatus'},{av:'Ddo_usuarioperfil_delete_Sortedstatus',ctrl:'DDO_USUARIOPERFIL_DELETE',prop:'SortedStatus'},{av:'Ddo_usuarioperfil_display_Sortedstatus',ctrl:'DDO_USUARIOPERFIL_DISPLAY',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_USUARIOPERFIL_INSERT.ONOPTIONCLICKED","{handler:'E168Y2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV18Perfil_Nome1',fld:'vPERFIL_NOME1',pic:'@!',nv:''},{av:'AV19Usuario_PessoaNom1',fld:'vUSUARIO_PESSOANOM1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV23Perfil_Nome2',fld:'vPERFIL_NOME2',pic:'@!',nv:''},{av:'AV24Usuario_PessoaNom2',fld:'vUSUARIO_PESSOANOM2',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV55TFUsuario_Nome',fld:'vTFUSUARIO_NOME',pic:'@!',nv:''},{av:'AV56TFUsuario_Nome_Sel',fld:'vTFUSUARIO_NOME_SEL',pic:'@!',nv:''},{av:'AV59TFUsuario_PessoaNom',fld:'vTFUSUARIO_PESSOANOM',pic:'@!',nv:''},{av:'AV60TFUsuario_PessoaNom_Sel',fld:'vTFUSUARIO_PESSOANOM_SEL',pic:'@!',nv:''},{av:'AV63TFPerfil_Nome',fld:'vTFPERFIL_NOME',pic:'@!',nv:''},{av:'AV64TFPerfil_Nome_Sel',fld:'vTFPERFIL_NOME_SEL',pic:'@!',nv:''},{av:'AV71TFUsuarioPerfil_Insert_Sel',fld:'vTFUSUARIOPERFIL_INSERT_SEL',pic:'9',nv:0},{av:'AV74TFUsuarioPerfil_Update_Sel',fld:'vTFUSUARIOPERFIL_UPDATE_SEL',pic:'9',nv:0},{av:'AV77TFUsuarioPerfil_Delete_Sel',fld:'vTFUSUARIOPERFIL_DELETE_SEL',pic:'9',nv:0},{av:'AV80TFUsuarioPerfil_Display_Sel',fld:'vTFUSUARIOPERFIL_DISPLAY_SEL',pic:'9',nv:0},{av:'AV57ddo_Usuario_NomeTitleControlIdToReplace',fld:'vDDO_USUARIO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_Usuario_PessoaNomTitleControlIdToReplace',fld:'vDDO_USUARIO_PESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_Perfil_NomeTitleControlIdToReplace',fld:'vDDO_PERFIL_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV69ddo_Perfil_TipoTitleControlIdToReplace',fld:'vDDO_PERFIL_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_UsuarioPerfil_InsertTitleControlIdToReplace',fld:'vDDO_USUARIOPERFIL_INSERTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75ddo_UsuarioPerfil_UpdateTitleControlIdToReplace',fld:'vDDO_USUARIOPERFIL_UPDATETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78ddo_UsuarioPerfil_DeleteTitleControlIdToReplace',fld:'vDDO_USUARIOPERFIL_DELETETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV81ddo_UsuarioPerfil_DisplayTitleControlIdToReplace',fld:'vDDO_USUARIOPERFIL_DISPLAYTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68TFPerfil_Tipo_Sels',fld:'vTFPERFIL_TIPO_SELS',pic:'',nv:null},{av:'AV108Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV26DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV25DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A3Perfil_Codigo',fld:'PERFIL_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_usuarioperfil_insert_Activeeventkey',ctrl:'DDO_USUARIOPERFIL_INSERT',prop:'ActiveEventKey'},{av:'Ddo_usuarioperfil_insert_Selectedvalue_get',ctrl:'DDO_USUARIOPERFIL_INSERT',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_usuarioperfil_insert_Sortedstatus',ctrl:'DDO_USUARIOPERFIL_INSERT',prop:'SortedStatus'},{av:'AV71TFUsuarioPerfil_Insert_Sel',fld:'vTFUSUARIOPERFIL_INSERT_SEL',pic:'9',nv:0},{av:'Ddo_usuario_nome_Sortedstatus',ctrl:'DDO_USUARIO_NOME',prop:'SortedStatus'},{av:'Ddo_usuario_pessoanom_Sortedstatus',ctrl:'DDO_USUARIO_PESSOANOM',prop:'SortedStatus'},{av:'Ddo_perfil_nome_Sortedstatus',ctrl:'DDO_PERFIL_NOME',prop:'SortedStatus'},{av:'Ddo_perfil_tipo_Sortedstatus',ctrl:'DDO_PERFIL_TIPO',prop:'SortedStatus'},{av:'Ddo_usuarioperfil_update_Sortedstatus',ctrl:'DDO_USUARIOPERFIL_UPDATE',prop:'SortedStatus'},{av:'Ddo_usuarioperfil_delete_Sortedstatus',ctrl:'DDO_USUARIOPERFIL_DELETE',prop:'SortedStatus'},{av:'Ddo_usuarioperfil_display_Sortedstatus',ctrl:'DDO_USUARIOPERFIL_DISPLAY',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_USUARIOPERFIL_UPDATE.ONOPTIONCLICKED","{handler:'E178Y2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV18Perfil_Nome1',fld:'vPERFIL_NOME1',pic:'@!',nv:''},{av:'AV19Usuario_PessoaNom1',fld:'vUSUARIO_PESSOANOM1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV23Perfil_Nome2',fld:'vPERFIL_NOME2',pic:'@!',nv:''},{av:'AV24Usuario_PessoaNom2',fld:'vUSUARIO_PESSOANOM2',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV55TFUsuario_Nome',fld:'vTFUSUARIO_NOME',pic:'@!',nv:''},{av:'AV56TFUsuario_Nome_Sel',fld:'vTFUSUARIO_NOME_SEL',pic:'@!',nv:''},{av:'AV59TFUsuario_PessoaNom',fld:'vTFUSUARIO_PESSOANOM',pic:'@!',nv:''},{av:'AV60TFUsuario_PessoaNom_Sel',fld:'vTFUSUARIO_PESSOANOM_SEL',pic:'@!',nv:''},{av:'AV63TFPerfil_Nome',fld:'vTFPERFIL_NOME',pic:'@!',nv:''},{av:'AV64TFPerfil_Nome_Sel',fld:'vTFPERFIL_NOME_SEL',pic:'@!',nv:''},{av:'AV71TFUsuarioPerfil_Insert_Sel',fld:'vTFUSUARIOPERFIL_INSERT_SEL',pic:'9',nv:0},{av:'AV74TFUsuarioPerfil_Update_Sel',fld:'vTFUSUARIOPERFIL_UPDATE_SEL',pic:'9',nv:0},{av:'AV77TFUsuarioPerfil_Delete_Sel',fld:'vTFUSUARIOPERFIL_DELETE_SEL',pic:'9',nv:0},{av:'AV80TFUsuarioPerfil_Display_Sel',fld:'vTFUSUARIOPERFIL_DISPLAY_SEL',pic:'9',nv:0},{av:'AV57ddo_Usuario_NomeTitleControlIdToReplace',fld:'vDDO_USUARIO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_Usuario_PessoaNomTitleControlIdToReplace',fld:'vDDO_USUARIO_PESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_Perfil_NomeTitleControlIdToReplace',fld:'vDDO_PERFIL_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV69ddo_Perfil_TipoTitleControlIdToReplace',fld:'vDDO_PERFIL_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_UsuarioPerfil_InsertTitleControlIdToReplace',fld:'vDDO_USUARIOPERFIL_INSERTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75ddo_UsuarioPerfil_UpdateTitleControlIdToReplace',fld:'vDDO_USUARIOPERFIL_UPDATETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78ddo_UsuarioPerfil_DeleteTitleControlIdToReplace',fld:'vDDO_USUARIOPERFIL_DELETETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV81ddo_UsuarioPerfil_DisplayTitleControlIdToReplace',fld:'vDDO_USUARIOPERFIL_DISPLAYTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68TFPerfil_Tipo_Sels',fld:'vTFPERFIL_TIPO_SELS',pic:'',nv:null},{av:'AV108Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV26DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV25DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A3Perfil_Codigo',fld:'PERFIL_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_usuarioperfil_update_Activeeventkey',ctrl:'DDO_USUARIOPERFIL_UPDATE',prop:'ActiveEventKey'},{av:'Ddo_usuarioperfil_update_Selectedvalue_get',ctrl:'DDO_USUARIOPERFIL_UPDATE',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_usuarioperfil_update_Sortedstatus',ctrl:'DDO_USUARIOPERFIL_UPDATE',prop:'SortedStatus'},{av:'AV74TFUsuarioPerfil_Update_Sel',fld:'vTFUSUARIOPERFIL_UPDATE_SEL',pic:'9',nv:0},{av:'Ddo_usuario_nome_Sortedstatus',ctrl:'DDO_USUARIO_NOME',prop:'SortedStatus'},{av:'Ddo_usuario_pessoanom_Sortedstatus',ctrl:'DDO_USUARIO_PESSOANOM',prop:'SortedStatus'},{av:'Ddo_perfil_nome_Sortedstatus',ctrl:'DDO_PERFIL_NOME',prop:'SortedStatus'},{av:'Ddo_perfil_tipo_Sortedstatus',ctrl:'DDO_PERFIL_TIPO',prop:'SortedStatus'},{av:'Ddo_usuarioperfil_insert_Sortedstatus',ctrl:'DDO_USUARIOPERFIL_INSERT',prop:'SortedStatus'},{av:'Ddo_usuarioperfil_delete_Sortedstatus',ctrl:'DDO_USUARIOPERFIL_DELETE',prop:'SortedStatus'},{av:'Ddo_usuarioperfil_display_Sortedstatus',ctrl:'DDO_USUARIOPERFIL_DISPLAY',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_USUARIOPERFIL_DELETE.ONOPTIONCLICKED","{handler:'E188Y2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV18Perfil_Nome1',fld:'vPERFIL_NOME1',pic:'@!',nv:''},{av:'AV19Usuario_PessoaNom1',fld:'vUSUARIO_PESSOANOM1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV23Perfil_Nome2',fld:'vPERFIL_NOME2',pic:'@!',nv:''},{av:'AV24Usuario_PessoaNom2',fld:'vUSUARIO_PESSOANOM2',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV55TFUsuario_Nome',fld:'vTFUSUARIO_NOME',pic:'@!',nv:''},{av:'AV56TFUsuario_Nome_Sel',fld:'vTFUSUARIO_NOME_SEL',pic:'@!',nv:''},{av:'AV59TFUsuario_PessoaNom',fld:'vTFUSUARIO_PESSOANOM',pic:'@!',nv:''},{av:'AV60TFUsuario_PessoaNom_Sel',fld:'vTFUSUARIO_PESSOANOM_SEL',pic:'@!',nv:''},{av:'AV63TFPerfil_Nome',fld:'vTFPERFIL_NOME',pic:'@!',nv:''},{av:'AV64TFPerfil_Nome_Sel',fld:'vTFPERFIL_NOME_SEL',pic:'@!',nv:''},{av:'AV71TFUsuarioPerfil_Insert_Sel',fld:'vTFUSUARIOPERFIL_INSERT_SEL',pic:'9',nv:0},{av:'AV74TFUsuarioPerfil_Update_Sel',fld:'vTFUSUARIOPERFIL_UPDATE_SEL',pic:'9',nv:0},{av:'AV77TFUsuarioPerfil_Delete_Sel',fld:'vTFUSUARIOPERFIL_DELETE_SEL',pic:'9',nv:0},{av:'AV80TFUsuarioPerfil_Display_Sel',fld:'vTFUSUARIOPERFIL_DISPLAY_SEL',pic:'9',nv:0},{av:'AV57ddo_Usuario_NomeTitleControlIdToReplace',fld:'vDDO_USUARIO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_Usuario_PessoaNomTitleControlIdToReplace',fld:'vDDO_USUARIO_PESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_Perfil_NomeTitleControlIdToReplace',fld:'vDDO_PERFIL_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV69ddo_Perfil_TipoTitleControlIdToReplace',fld:'vDDO_PERFIL_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_UsuarioPerfil_InsertTitleControlIdToReplace',fld:'vDDO_USUARIOPERFIL_INSERTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75ddo_UsuarioPerfil_UpdateTitleControlIdToReplace',fld:'vDDO_USUARIOPERFIL_UPDATETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78ddo_UsuarioPerfil_DeleteTitleControlIdToReplace',fld:'vDDO_USUARIOPERFIL_DELETETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV81ddo_UsuarioPerfil_DisplayTitleControlIdToReplace',fld:'vDDO_USUARIOPERFIL_DISPLAYTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68TFPerfil_Tipo_Sels',fld:'vTFPERFIL_TIPO_SELS',pic:'',nv:null},{av:'AV108Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV26DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV25DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A3Perfil_Codigo',fld:'PERFIL_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_usuarioperfil_delete_Activeeventkey',ctrl:'DDO_USUARIOPERFIL_DELETE',prop:'ActiveEventKey'},{av:'Ddo_usuarioperfil_delete_Selectedvalue_get',ctrl:'DDO_USUARIOPERFIL_DELETE',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_usuarioperfil_delete_Sortedstatus',ctrl:'DDO_USUARIOPERFIL_DELETE',prop:'SortedStatus'},{av:'AV77TFUsuarioPerfil_Delete_Sel',fld:'vTFUSUARIOPERFIL_DELETE_SEL',pic:'9',nv:0},{av:'Ddo_usuario_nome_Sortedstatus',ctrl:'DDO_USUARIO_NOME',prop:'SortedStatus'},{av:'Ddo_usuario_pessoanom_Sortedstatus',ctrl:'DDO_USUARIO_PESSOANOM',prop:'SortedStatus'},{av:'Ddo_perfil_nome_Sortedstatus',ctrl:'DDO_PERFIL_NOME',prop:'SortedStatus'},{av:'Ddo_perfil_tipo_Sortedstatus',ctrl:'DDO_PERFIL_TIPO',prop:'SortedStatus'},{av:'Ddo_usuarioperfil_insert_Sortedstatus',ctrl:'DDO_USUARIOPERFIL_INSERT',prop:'SortedStatus'},{av:'Ddo_usuarioperfil_update_Sortedstatus',ctrl:'DDO_USUARIOPERFIL_UPDATE',prop:'SortedStatus'},{av:'Ddo_usuarioperfil_display_Sortedstatus',ctrl:'DDO_USUARIOPERFIL_DISPLAY',prop:'SortedStatus'}]}");
         setEventMetadata("DDO_USUARIOPERFIL_DISPLAY.ONOPTIONCLICKED","{handler:'E198Y2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV18Perfil_Nome1',fld:'vPERFIL_NOME1',pic:'@!',nv:''},{av:'AV19Usuario_PessoaNom1',fld:'vUSUARIO_PESSOANOM1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV23Perfil_Nome2',fld:'vPERFIL_NOME2',pic:'@!',nv:''},{av:'AV24Usuario_PessoaNom2',fld:'vUSUARIO_PESSOANOM2',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV55TFUsuario_Nome',fld:'vTFUSUARIO_NOME',pic:'@!',nv:''},{av:'AV56TFUsuario_Nome_Sel',fld:'vTFUSUARIO_NOME_SEL',pic:'@!',nv:''},{av:'AV59TFUsuario_PessoaNom',fld:'vTFUSUARIO_PESSOANOM',pic:'@!',nv:''},{av:'AV60TFUsuario_PessoaNom_Sel',fld:'vTFUSUARIO_PESSOANOM_SEL',pic:'@!',nv:''},{av:'AV63TFPerfil_Nome',fld:'vTFPERFIL_NOME',pic:'@!',nv:''},{av:'AV64TFPerfil_Nome_Sel',fld:'vTFPERFIL_NOME_SEL',pic:'@!',nv:''},{av:'AV71TFUsuarioPerfil_Insert_Sel',fld:'vTFUSUARIOPERFIL_INSERT_SEL',pic:'9',nv:0},{av:'AV74TFUsuarioPerfil_Update_Sel',fld:'vTFUSUARIOPERFIL_UPDATE_SEL',pic:'9',nv:0},{av:'AV77TFUsuarioPerfil_Delete_Sel',fld:'vTFUSUARIOPERFIL_DELETE_SEL',pic:'9',nv:0},{av:'AV80TFUsuarioPerfil_Display_Sel',fld:'vTFUSUARIOPERFIL_DISPLAY_SEL',pic:'9',nv:0},{av:'AV57ddo_Usuario_NomeTitleControlIdToReplace',fld:'vDDO_USUARIO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_Usuario_PessoaNomTitleControlIdToReplace',fld:'vDDO_USUARIO_PESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_Perfil_NomeTitleControlIdToReplace',fld:'vDDO_PERFIL_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV69ddo_Perfil_TipoTitleControlIdToReplace',fld:'vDDO_PERFIL_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_UsuarioPerfil_InsertTitleControlIdToReplace',fld:'vDDO_USUARIOPERFIL_INSERTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75ddo_UsuarioPerfil_UpdateTitleControlIdToReplace',fld:'vDDO_USUARIOPERFIL_UPDATETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78ddo_UsuarioPerfil_DeleteTitleControlIdToReplace',fld:'vDDO_USUARIOPERFIL_DELETETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV81ddo_UsuarioPerfil_DisplayTitleControlIdToReplace',fld:'vDDO_USUARIOPERFIL_DISPLAYTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68TFPerfil_Tipo_Sels',fld:'vTFPERFIL_TIPO_SELS',pic:'',nv:null},{av:'AV108Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV26DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV25DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A3Perfil_Codigo',fld:'PERFIL_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'Ddo_usuarioperfil_display_Activeeventkey',ctrl:'DDO_USUARIOPERFIL_DISPLAY',prop:'ActiveEventKey'},{av:'Ddo_usuarioperfil_display_Selectedvalue_get',ctrl:'DDO_USUARIOPERFIL_DISPLAY',prop:'SelectedValue_get'}],oparms:[{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'Ddo_usuarioperfil_display_Sortedstatus',ctrl:'DDO_USUARIOPERFIL_DISPLAY',prop:'SortedStatus'},{av:'AV80TFUsuarioPerfil_Display_Sel',fld:'vTFUSUARIOPERFIL_DISPLAY_SEL',pic:'9',nv:0},{av:'Ddo_usuario_nome_Sortedstatus',ctrl:'DDO_USUARIO_NOME',prop:'SortedStatus'},{av:'Ddo_usuario_pessoanom_Sortedstatus',ctrl:'DDO_USUARIO_PESSOANOM',prop:'SortedStatus'},{av:'Ddo_perfil_nome_Sortedstatus',ctrl:'DDO_PERFIL_NOME',prop:'SortedStatus'},{av:'Ddo_perfil_tipo_Sortedstatus',ctrl:'DDO_PERFIL_TIPO',prop:'SortedStatus'},{av:'Ddo_usuarioperfil_insert_Sortedstatus',ctrl:'DDO_USUARIOPERFIL_INSERT',prop:'SortedStatus'},{av:'Ddo_usuarioperfil_update_Sortedstatus',ctrl:'DDO_USUARIOPERFIL_UPDATE',prop:'SortedStatus'},{av:'Ddo_usuarioperfil_delete_Sortedstatus',ctrl:'DDO_USUARIOPERFIL_DELETE',prop:'SortedStatus'}]}");
         setEventMetadata("GRID.LOAD","{handler:'E308Y2',iparms:[{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A3Perfil_Codigo',fld:'PERFIL_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV27Update',fld:'vUPDATE',pic:'',nv:''},{av:'edtavUpdate_Tooltiptext',ctrl:'vUPDATE',prop:'Tooltiptext'},{av:'edtavUpdate_Link',ctrl:'vUPDATE',prop:'Link'},{av:'AV28Delete',fld:'vDELETE',pic:'',nv:''},{av:'edtavDelete_Tooltiptext',ctrl:'vDELETE',prop:'Tooltiptext'},{av:'edtavDelete_Link',ctrl:'vDELETE',prop:'Link'}]}");
         setEventMetadata("VORDEREDBY.CLICK","{handler:'E208Y2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV18Perfil_Nome1',fld:'vPERFIL_NOME1',pic:'@!',nv:''},{av:'AV19Usuario_PessoaNom1',fld:'vUSUARIO_PESSOANOM1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV23Perfil_Nome2',fld:'vPERFIL_NOME2',pic:'@!',nv:''},{av:'AV24Usuario_PessoaNom2',fld:'vUSUARIO_PESSOANOM2',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV55TFUsuario_Nome',fld:'vTFUSUARIO_NOME',pic:'@!',nv:''},{av:'AV56TFUsuario_Nome_Sel',fld:'vTFUSUARIO_NOME_SEL',pic:'@!',nv:''},{av:'AV59TFUsuario_PessoaNom',fld:'vTFUSUARIO_PESSOANOM',pic:'@!',nv:''},{av:'AV60TFUsuario_PessoaNom_Sel',fld:'vTFUSUARIO_PESSOANOM_SEL',pic:'@!',nv:''},{av:'AV63TFPerfil_Nome',fld:'vTFPERFIL_NOME',pic:'@!',nv:''},{av:'AV64TFPerfil_Nome_Sel',fld:'vTFPERFIL_NOME_SEL',pic:'@!',nv:''},{av:'AV71TFUsuarioPerfil_Insert_Sel',fld:'vTFUSUARIOPERFIL_INSERT_SEL',pic:'9',nv:0},{av:'AV74TFUsuarioPerfil_Update_Sel',fld:'vTFUSUARIOPERFIL_UPDATE_SEL',pic:'9',nv:0},{av:'AV77TFUsuarioPerfil_Delete_Sel',fld:'vTFUSUARIOPERFIL_DELETE_SEL',pic:'9',nv:0},{av:'AV80TFUsuarioPerfil_Display_Sel',fld:'vTFUSUARIOPERFIL_DISPLAY_SEL',pic:'9',nv:0},{av:'AV57ddo_Usuario_NomeTitleControlIdToReplace',fld:'vDDO_USUARIO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_Usuario_PessoaNomTitleControlIdToReplace',fld:'vDDO_USUARIO_PESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_Perfil_NomeTitleControlIdToReplace',fld:'vDDO_PERFIL_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV69ddo_Perfil_TipoTitleControlIdToReplace',fld:'vDDO_PERFIL_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_UsuarioPerfil_InsertTitleControlIdToReplace',fld:'vDDO_USUARIOPERFIL_INSERTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75ddo_UsuarioPerfil_UpdateTitleControlIdToReplace',fld:'vDDO_USUARIOPERFIL_UPDATETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78ddo_UsuarioPerfil_DeleteTitleControlIdToReplace',fld:'vDDO_USUARIOPERFIL_DELETETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV81ddo_UsuarioPerfil_DisplayTitleControlIdToReplace',fld:'vDDO_USUARIOPERFIL_DISPLAYTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68TFPerfil_Tipo_Sels',fld:'vTFPERFIL_TIPO_SELS',pic:'',nv:null},{av:'AV108Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV26DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV25DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A3Perfil_Codigo',fld:'PERFIL_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         setEventMetadata("'ADDDYNAMICFILTERS1'","{handler:'E258Y2',iparms:[],oparms:[{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS1'","{handler:'E218Y2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV18Perfil_Nome1',fld:'vPERFIL_NOME1',pic:'@!',nv:''},{av:'AV19Usuario_PessoaNom1',fld:'vUSUARIO_PESSOANOM1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV23Perfil_Nome2',fld:'vPERFIL_NOME2',pic:'@!',nv:''},{av:'AV24Usuario_PessoaNom2',fld:'vUSUARIO_PESSOANOM2',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV55TFUsuario_Nome',fld:'vTFUSUARIO_NOME',pic:'@!',nv:''},{av:'AV56TFUsuario_Nome_Sel',fld:'vTFUSUARIO_NOME_SEL',pic:'@!',nv:''},{av:'AV59TFUsuario_PessoaNom',fld:'vTFUSUARIO_PESSOANOM',pic:'@!',nv:''},{av:'AV60TFUsuario_PessoaNom_Sel',fld:'vTFUSUARIO_PESSOANOM_SEL',pic:'@!',nv:''},{av:'AV63TFPerfil_Nome',fld:'vTFPERFIL_NOME',pic:'@!',nv:''},{av:'AV64TFPerfil_Nome_Sel',fld:'vTFPERFIL_NOME_SEL',pic:'@!',nv:''},{av:'AV71TFUsuarioPerfil_Insert_Sel',fld:'vTFUSUARIOPERFIL_INSERT_SEL',pic:'9',nv:0},{av:'AV74TFUsuarioPerfil_Update_Sel',fld:'vTFUSUARIOPERFIL_UPDATE_SEL',pic:'9',nv:0},{av:'AV77TFUsuarioPerfil_Delete_Sel',fld:'vTFUSUARIOPERFIL_DELETE_SEL',pic:'9',nv:0},{av:'AV80TFUsuarioPerfil_Display_Sel',fld:'vTFUSUARIOPERFIL_DISPLAY_SEL',pic:'9',nv:0},{av:'AV57ddo_Usuario_NomeTitleControlIdToReplace',fld:'vDDO_USUARIO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_Usuario_PessoaNomTitleControlIdToReplace',fld:'vDDO_USUARIO_PESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_Perfil_NomeTitleControlIdToReplace',fld:'vDDO_PERFIL_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV69ddo_Perfil_TipoTitleControlIdToReplace',fld:'vDDO_PERFIL_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_UsuarioPerfil_InsertTitleControlIdToReplace',fld:'vDDO_USUARIOPERFIL_INSERTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75ddo_UsuarioPerfil_UpdateTitleControlIdToReplace',fld:'vDDO_USUARIOPERFIL_UPDATETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78ddo_UsuarioPerfil_DeleteTitleControlIdToReplace',fld:'vDDO_USUARIOPERFIL_DELETETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV81ddo_UsuarioPerfil_DisplayTitleControlIdToReplace',fld:'vDDO_USUARIOPERFIL_DISPLAYTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68TFPerfil_Tipo_Sels',fld:'vTFPERFIL_TIPO_SELS',pic:'',nv:null},{av:'AV108Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV26DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV25DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A3Perfil_Codigo',fld:'PERFIL_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV25DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV26DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV23Perfil_Nome2',fld:'vPERFIL_NOME2',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV18Perfil_Nome1',fld:'vPERFIL_NOME1',pic:'@!',nv:''},{av:'AV19Usuario_PessoaNom1',fld:'vUSUARIO_PESSOANOM1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV24Usuario_PessoaNom2',fld:'vUSUARIO_PESSOANOM2',pic:'@!',nv:''},{av:'edtavPerfil_nome2_Visible',ctrl:'vPERFIL_NOME2',prop:'Visible'},{av:'edtavUsuario_pessoanom2_Visible',ctrl:'vUSUARIO_PESSOANOM2',prop:'Visible'},{av:'edtavPerfil_nome1_Visible',ctrl:'vPERFIL_NOME1',prop:'Visible'},{av:'edtavUsuario_pessoanom1_Visible',ctrl:'vUSUARIO_PESSOANOM1',prop:'Visible'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR1.CLICK","{handler:'E268Y2',iparms:[{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''}],oparms:[{av:'edtavPerfil_nome1_Visible',ctrl:'vPERFIL_NOME1',prop:'Visible'},{av:'edtavUsuario_pessoanom1_Visible',ctrl:'vUSUARIO_PESSOANOM1',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS2'","{handler:'E228Y2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV18Perfil_Nome1',fld:'vPERFIL_NOME1',pic:'@!',nv:''},{av:'AV19Usuario_PessoaNom1',fld:'vUSUARIO_PESSOANOM1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV23Perfil_Nome2',fld:'vPERFIL_NOME2',pic:'@!',nv:''},{av:'AV24Usuario_PessoaNom2',fld:'vUSUARIO_PESSOANOM2',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV55TFUsuario_Nome',fld:'vTFUSUARIO_NOME',pic:'@!',nv:''},{av:'AV56TFUsuario_Nome_Sel',fld:'vTFUSUARIO_NOME_SEL',pic:'@!',nv:''},{av:'AV59TFUsuario_PessoaNom',fld:'vTFUSUARIO_PESSOANOM',pic:'@!',nv:''},{av:'AV60TFUsuario_PessoaNom_Sel',fld:'vTFUSUARIO_PESSOANOM_SEL',pic:'@!',nv:''},{av:'AV63TFPerfil_Nome',fld:'vTFPERFIL_NOME',pic:'@!',nv:''},{av:'AV64TFPerfil_Nome_Sel',fld:'vTFPERFIL_NOME_SEL',pic:'@!',nv:''},{av:'AV71TFUsuarioPerfil_Insert_Sel',fld:'vTFUSUARIOPERFIL_INSERT_SEL',pic:'9',nv:0},{av:'AV74TFUsuarioPerfil_Update_Sel',fld:'vTFUSUARIOPERFIL_UPDATE_SEL',pic:'9',nv:0},{av:'AV77TFUsuarioPerfil_Delete_Sel',fld:'vTFUSUARIOPERFIL_DELETE_SEL',pic:'9',nv:0},{av:'AV80TFUsuarioPerfil_Display_Sel',fld:'vTFUSUARIOPERFIL_DISPLAY_SEL',pic:'9',nv:0},{av:'AV57ddo_Usuario_NomeTitleControlIdToReplace',fld:'vDDO_USUARIO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_Usuario_PessoaNomTitleControlIdToReplace',fld:'vDDO_USUARIO_PESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_Perfil_NomeTitleControlIdToReplace',fld:'vDDO_PERFIL_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV69ddo_Perfil_TipoTitleControlIdToReplace',fld:'vDDO_PERFIL_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_UsuarioPerfil_InsertTitleControlIdToReplace',fld:'vDDO_USUARIOPERFIL_INSERTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75ddo_UsuarioPerfil_UpdateTitleControlIdToReplace',fld:'vDDO_USUARIOPERFIL_UPDATETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78ddo_UsuarioPerfil_DeleteTitleControlIdToReplace',fld:'vDDO_USUARIOPERFIL_DELETETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV81ddo_UsuarioPerfil_DisplayTitleControlIdToReplace',fld:'vDDO_USUARIOPERFIL_DISPLAYTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68TFPerfil_Tipo_Sels',fld:'vTFPERFIL_TIPO_SELS',pic:'',nv:null},{av:'AV108Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV26DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV25DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A3Perfil_Codigo',fld:'PERFIL_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV25DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV23Perfil_Nome2',fld:'vPERFIL_NOME2',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV18Perfil_Nome1',fld:'vPERFIL_NOME1',pic:'@!',nv:''},{av:'AV19Usuario_PessoaNom1',fld:'vUSUARIO_PESSOANOM1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV24Usuario_PessoaNom2',fld:'vUSUARIO_PESSOANOM2',pic:'@!',nv:''},{av:'edtavPerfil_nome2_Visible',ctrl:'vPERFIL_NOME2',prop:'Visible'},{av:'edtavUsuario_pessoanom2_Visible',ctrl:'vUSUARIO_PESSOANOM2',prop:'Visible'},{av:'edtavPerfil_nome1_Visible',ctrl:'vPERFIL_NOME1',prop:'Visible'},{av:'edtavUsuario_pessoanom1_Visible',ctrl:'vUSUARIO_PESSOANOM1',prop:'Visible'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR2.CLICK","{handler:'E278Y2',iparms:[{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''}],oparms:[{av:'edtavPerfil_nome2_Visible',ctrl:'vPERFIL_NOME2',prop:'Visible'},{av:'edtavUsuario_pessoanom2_Visible',ctrl:'vUSUARIO_PESSOANOM2',prop:'Visible'}]}");
         setEventMetadata("'DOCLEANFILTERS'","{handler:'E238Y2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV18Perfil_Nome1',fld:'vPERFIL_NOME1',pic:'@!',nv:''},{av:'AV19Usuario_PessoaNom1',fld:'vUSUARIO_PESSOANOM1',pic:'@!',nv:''},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV23Perfil_Nome2',fld:'vPERFIL_NOME2',pic:'@!',nv:''},{av:'AV24Usuario_PessoaNom2',fld:'vUSUARIO_PESSOANOM2',pic:'@!',nv:''},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV55TFUsuario_Nome',fld:'vTFUSUARIO_NOME',pic:'@!',nv:''},{av:'AV56TFUsuario_Nome_Sel',fld:'vTFUSUARIO_NOME_SEL',pic:'@!',nv:''},{av:'AV59TFUsuario_PessoaNom',fld:'vTFUSUARIO_PESSOANOM',pic:'@!',nv:''},{av:'AV60TFUsuario_PessoaNom_Sel',fld:'vTFUSUARIO_PESSOANOM_SEL',pic:'@!',nv:''},{av:'AV63TFPerfil_Nome',fld:'vTFPERFIL_NOME',pic:'@!',nv:''},{av:'AV64TFPerfil_Nome_Sel',fld:'vTFPERFIL_NOME_SEL',pic:'@!',nv:''},{av:'AV71TFUsuarioPerfil_Insert_Sel',fld:'vTFUSUARIOPERFIL_INSERT_SEL',pic:'9',nv:0},{av:'AV74TFUsuarioPerfil_Update_Sel',fld:'vTFUSUARIOPERFIL_UPDATE_SEL',pic:'9',nv:0},{av:'AV77TFUsuarioPerfil_Delete_Sel',fld:'vTFUSUARIOPERFIL_DELETE_SEL',pic:'9',nv:0},{av:'AV80TFUsuarioPerfil_Display_Sel',fld:'vTFUSUARIOPERFIL_DISPLAY_SEL',pic:'9',nv:0},{av:'AV57ddo_Usuario_NomeTitleControlIdToReplace',fld:'vDDO_USUARIO_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV61ddo_Usuario_PessoaNomTitleControlIdToReplace',fld:'vDDO_USUARIO_PESSOANOMTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV65ddo_Perfil_NomeTitleControlIdToReplace',fld:'vDDO_PERFIL_NOMETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV69ddo_Perfil_TipoTitleControlIdToReplace',fld:'vDDO_PERFIL_TIPOTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV72ddo_UsuarioPerfil_InsertTitleControlIdToReplace',fld:'vDDO_USUARIOPERFIL_INSERTTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV75ddo_UsuarioPerfil_UpdateTitleControlIdToReplace',fld:'vDDO_USUARIOPERFIL_UPDATETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV78ddo_UsuarioPerfil_DeleteTitleControlIdToReplace',fld:'vDDO_USUARIOPERFIL_DELETETITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV81ddo_UsuarioPerfil_DisplayTitleControlIdToReplace',fld:'vDDO_USUARIOPERFIL_DISPLAYTITLECONTROLIDTOREPLACE',pic:'',nv:''},{av:'AV68TFPerfil_Tipo_Sels',fld:'vTFPERFIL_TIPO_SELS',pic:'',nv:null},{av:'AV108Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV26DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV25DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A3Perfil_Codigo',fld:'PERFIL_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[{av:'AV55TFUsuario_Nome',fld:'vTFUSUARIO_NOME',pic:'@!',nv:''},{av:'Ddo_usuario_nome_Filteredtext_set',ctrl:'DDO_USUARIO_NOME',prop:'FilteredText_set'},{av:'AV56TFUsuario_Nome_Sel',fld:'vTFUSUARIO_NOME_SEL',pic:'@!',nv:''},{av:'Ddo_usuario_nome_Selectedvalue_set',ctrl:'DDO_USUARIO_NOME',prop:'SelectedValue_set'},{av:'AV59TFUsuario_PessoaNom',fld:'vTFUSUARIO_PESSOANOM',pic:'@!',nv:''},{av:'Ddo_usuario_pessoanom_Filteredtext_set',ctrl:'DDO_USUARIO_PESSOANOM',prop:'FilteredText_set'},{av:'AV60TFUsuario_PessoaNom_Sel',fld:'vTFUSUARIO_PESSOANOM_SEL',pic:'@!',nv:''},{av:'Ddo_usuario_pessoanom_Selectedvalue_set',ctrl:'DDO_USUARIO_PESSOANOM',prop:'SelectedValue_set'},{av:'AV63TFPerfil_Nome',fld:'vTFPERFIL_NOME',pic:'@!',nv:''},{av:'Ddo_perfil_nome_Filteredtext_set',ctrl:'DDO_PERFIL_NOME',prop:'FilteredText_set'},{av:'AV64TFPerfil_Nome_Sel',fld:'vTFPERFIL_NOME_SEL',pic:'@!',nv:''},{av:'Ddo_perfil_nome_Selectedvalue_set',ctrl:'DDO_PERFIL_NOME',prop:'SelectedValue_set'},{av:'AV68TFPerfil_Tipo_Sels',fld:'vTFPERFIL_TIPO_SELS',pic:'',nv:null},{av:'Ddo_perfil_tipo_Selectedvalue_set',ctrl:'DDO_PERFIL_TIPO',prop:'SelectedValue_set'},{av:'AV71TFUsuarioPerfil_Insert_Sel',fld:'vTFUSUARIOPERFIL_INSERT_SEL',pic:'9',nv:0},{av:'Ddo_usuarioperfil_insert_Selectedvalue_set',ctrl:'DDO_USUARIOPERFIL_INSERT',prop:'SelectedValue_set'},{av:'AV74TFUsuarioPerfil_Update_Sel',fld:'vTFUSUARIOPERFIL_UPDATE_SEL',pic:'9',nv:0},{av:'Ddo_usuarioperfil_update_Selectedvalue_set',ctrl:'DDO_USUARIOPERFIL_UPDATE',prop:'SelectedValue_set'},{av:'AV77TFUsuarioPerfil_Delete_Sel',fld:'vTFUSUARIOPERFIL_DELETE_SEL',pic:'9',nv:0},{av:'Ddo_usuarioperfil_delete_Selectedvalue_set',ctrl:'DDO_USUARIOPERFIL_DELETE',prop:'SelectedValue_set'},{av:'AV80TFUsuarioPerfil_Display_Sel',fld:'vTFUSUARIOPERFIL_DISPLAY_SEL',pic:'9',nv:0},{av:'Ddo_usuarioperfil_display_Selectedvalue_set',ctrl:'DDO_USUARIOPERFIL_DISPLAY',prop:'SelectedValue_set'},{av:'AV16DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV18Perfil_Nome1',fld:'vPERFIL_NOME1',pic:'@!',nv:''},{av:'AV10GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'edtavPerfil_nome1_Visible',ctrl:'vPERFIL_NOME1',prop:'Visible'},{av:'edtavUsuario_pessoanom1_Visible',ctrl:'vUSUARIO_PESSOANOM1',prop:'Visible'},{av:'AV20DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV21DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV23Perfil_Nome2',fld:'vPERFIL_NOME2',pic:'@!',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'AV19Usuario_PessoaNom1',fld:'vUSUARIO_PESSOANOM1',pic:'@!',nv:''},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV24Usuario_PessoaNom2',fld:'vUSUARIO_PESSOANOM2',pic:'@!',nv:''},{av:'edtavPerfil_nome2_Visible',ctrl:'vPERFIL_NOME2',prop:'Visible'},{av:'edtavUsuario_pessoanom2_Visible',ctrl:'vUSUARIO_PESSOANOM2',prop:'Visible'}]}");
         setEventMetadata("'DOINSERT'","{handler:'E248Y2',iparms:[{av:'A1Usuario_Codigo',fld:'USUARIO_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A3Perfil_Codigo',fld:'PERFIL_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Gridpaginationbar_Selectedpage = "";
         Ddo_usuario_nome_Activeeventkey = "";
         Ddo_usuario_nome_Filteredtext_get = "";
         Ddo_usuario_nome_Selectedvalue_get = "";
         Ddo_usuario_pessoanom_Activeeventkey = "";
         Ddo_usuario_pessoanom_Filteredtext_get = "";
         Ddo_usuario_pessoanom_Selectedvalue_get = "";
         Ddo_perfil_nome_Activeeventkey = "";
         Ddo_perfil_nome_Filteredtext_get = "";
         Ddo_perfil_nome_Selectedvalue_get = "";
         Ddo_perfil_tipo_Activeeventkey = "";
         Ddo_perfil_tipo_Selectedvalue_get = "";
         Ddo_usuarioperfil_insert_Activeeventkey = "";
         Ddo_usuarioperfil_insert_Selectedvalue_get = "";
         Ddo_usuarioperfil_update_Activeeventkey = "";
         Ddo_usuarioperfil_update_Selectedvalue_get = "";
         Ddo_usuarioperfil_delete_Activeeventkey = "";
         Ddo_usuarioperfil_delete_Selectedvalue_get = "";
         Ddo_usuarioperfil_display_Activeeventkey = "";
         Ddo_usuarioperfil_display_Selectedvalue_get = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV16DynamicFiltersSelector1 = "";
         AV18Perfil_Nome1 = "";
         AV19Usuario_PessoaNom1 = "";
         AV21DynamicFiltersSelector2 = "";
         AV23Perfil_Nome2 = "";
         AV24Usuario_PessoaNom2 = "";
         AV55TFUsuario_Nome = "";
         AV56TFUsuario_Nome_Sel = "";
         AV59TFUsuario_PessoaNom = "";
         AV60TFUsuario_PessoaNom_Sel = "";
         AV63TFPerfil_Nome = "";
         AV64TFPerfil_Nome_Sel = "";
         AV57ddo_Usuario_NomeTitleControlIdToReplace = "";
         AV61ddo_Usuario_PessoaNomTitleControlIdToReplace = "";
         AV65ddo_Perfil_NomeTitleControlIdToReplace = "";
         AV69ddo_Perfil_TipoTitleControlIdToReplace = "";
         AV72ddo_UsuarioPerfil_InsertTitleControlIdToReplace = "";
         AV75ddo_UsuarioPerfil_UpdateTitleControlIdToReplace = "";
         AV78ddo_UsuarioPerfil_DeleteTitleControlIdToReplace = "";
         AV81ddo_UsuarioPerfil_DisplayTitleControlIdToReplace = "";
         AV68TFPerfil_Tipo_Sels = new GxSimpleCollection();
         AV108Pgmname = "";
         AV10GridState = new wwpbaseobjects.SdtWWPGridState(context);
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV82DDO_TitleSettingsIcons = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV54Usuario_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV58Usuario_PessoaNomTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV62Perfil_NomeTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV66Perfil_TipoTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV70UsuarioPerfil_InsertTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV73UsuarioPerfil_UpdateTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV76UsuarioPerfil_DeleteTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         AV79UsuarioPerfil_DisplayTitleFilterData = new GxObjectCollection( context, "DVB_SDTDropDownOptionsData.Item", "", "wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item", "GeneXus.Programs");
         Ddo_usuario_nome_Filteredtext_set = "";
         Ddo_usuario_nome_Selectedvalue_set = "";
         Ddo_usuario_nome_Sortedstatus = "";
         Ddo_usuario_pessoanom_Filteredtext_set = "";
         Ddo_usuario_pessoanom_Selectedvalue_set = "";
         Ddo_usuario_pessoanom_Sortedstatus = "";
         Ddo_perfil_nome_Filteredtext_set = "";
         Ddo_perfil_nome_Selectedvalue_set = "";
         Ddo_perfil_nome_Sortedstatus = "";
         Ddo_perfil_tipo_Selectedvalue_set = "";
         Ddo_perfil_tipo_Sortedstatus = "";
         Ddo_usuarioperfil_insert_Selectedvalue_set = "";
         Ddo_usuarioperfil_insert_Sortedstatus = "";
         Ddo_usuarioperfil_update_Selectedvalue_set = "";
         Ddo_usuarioperfil_update_Sortedstatus = "";
         Ddo_usuarioperfil_delete_Selectedvalue_set = "";
         Ddo_usuarioperfil_delete_Sortedstatus = "";
         Ddo_usuarioperfil_display_Selectedvalue_set = "";
         Ddo_usuarioperfil_display_Sortedstatus = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV27Update = "";
         AV106Update_GXI = "";
         AV28Delete = "";
         AV107Delete_GXI = "";
         A2Usuario_Nome = "";
         A58Usuario_PessoaNom = "";
         A4Perfil_Nome = "";
         GXCCtl = "";
         GridContainer = new GXWebGrid( context);
         AV101WWUsuarioPerfilDS_14_Tfperfil_tipo_sels = new GxSimpleCollection();
         scmdbuf = "";
         lV89WWUsuarioPerfilDS_2_Perfil_nome1 = "";
         lV90WWUsuarioPerfilDS_3_Usuario_pessoanom1 = "";
         lV93WWUsuarioPerfilDS_6_Perfil_nome2 = "";
         lV94WWUsuarioPerfilDS_7_Usuario_pessoanom2 = "";
         lV95WWUsuarioPerfilDS_8_Tfusuario_nome = "";
         lV97WWUsuarioPerfilDS_10_Tfusuario_pessoanom = "";
         lV99WWUsuarioPerfilDS_12_Tfperfil_nome = "";
         AV88WWUsuarioPerfilDS_1_Dynamicfiltersselector1 = "";
         AV89WWUsuarioPerfilDS_2_Perfil_nome1 = "";
         AV90WWUsuarioPerfilDS_3_Usuario_pessoanom1 = "";
         AV92WWUsuarioPerfilDS_5_Dynamicfiltersselector2 = "";
         AV93WWUsuarioPerfilDS_6_Perfil_nome2 = "";
         AV94WWUsuarioPerfilDS_7_Usuario_pessoanom2 = "";
         AV96WWUsuarioPerfilDS_9_Tfusuario_nome_sel = "";
         AV95WWUsuarioPerfilDS_8_Tfusuario_nome = "";
         AV98WWUsuarioPerfilDS_11_Tfusuario_pessoanom_sel = "";
         AV97WWUsuarioPerfilDS_10_Tfusuario_pessoanom = "";
         AV100WWUsuarioPerfilDS_13_Tfperfil_nome_sel = "";
         AV99WWUsuarioPerfilDS_12_Tfperfil_nome = "";
         H008Y2_A543UsuarioPerfil_Display = new bool[] {false} ;
         H008Y2_n543UsuarioPerfil_Display = new bool[] {false} ;
         H008Y2_A546UsuarioPerfil_Delete = new bool[] {false} ;
         H008Y2_n546UsuarioPerfil_Delete = new bool[] {false} ;
         H008Y2_A659UsuarioPerfil_Update = new bool[] {false} ;
         H008Y2_n659UsuarioPerfil_Update = new bool[] {false} ;
         H008Y2_A544UsuarioPerfil_Insert = new bool[] {false} ;
         H008Y2_n544UsuarioPerfil_Insert = new bool[] {false} ;
         H008Y2_A275Perfil_Tipo = new short[1] ;
         H008Y2_A4Perfil_Nome = new String[] {""} ;
         H008Y2_A58Usuario_PessoaNom = new String[] {""} ;
         H008Y2_n58Usuario_PessoaNom = new bool[] {false} ;
         H008Y2_A2Usuario_Nome = new String[] {""} ;
         H008Y2_n2Usuario_Nome = new bool[] {false} ;
         H008Y2_A57Usuario_PessoaCod = new int[1] ;
         H008Y2_A3Perfil_Codigo = new int[1] ;
         H008Y2_A1Usuario_Codigo = new int[1] ;
         H008Y3_AGRID_nRecordCount = new long[1] ;
         imgAdddynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters2_Jsonclick = "";
         imgCleanfilters_Jsonclick = "";
         GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 = new wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons(context);
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV67TFPerfil_Tipo_SelsJson = "";
         GridRow = new GXWebRow();
         AV49Session = context.GetSession();
         AV11GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV12GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV7HTTPRequest = new GxHttpRequest( context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblUsuarioperfiltitle_Jsonclick = "";
         lblOrderedtext_Jsonclick = "";
         lblJsdynamicfilters_Jsonclick = "";
         lblDynamicfiltersprefix1_Jsonclick = "";
         lblDynamicfiltersmiddle1_Jsonclick = "";
         lblDynamicfiltersprefix2_Jsonclick = "";
         lblDynamicfiltersmiddle2_Jsonclick = "";
         imgInsert_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wwusuarioperfil__default(),
            new Object[][] {
                new Object[] {
               H008Y2_A543UsuarioPerfil_Display, H008Y2_n543UsuarioPerfil_Display, H008Y2_A546UsuarioPerfil_Delete, H008Y2_n546UsuarioPerfil_Delete, H008Y2_A659UsuarioPerfil_Update, H008Y2_n659UsuarioPerfil_Update, H008Y2_A544UsuarioPerfil_Insert, H008Y2_n544UsuarioPerfil_Insert, H008Y2_A275Perfil_Tipo, H008Y2_A4Perfil_Nome,
               H008Y2_A58Usuario_PessoaNom, H008Y2_n58Usuario_PessoaNom, H008Y2_A2Usuario_Nome, H008Y2_n2Usuario_Nome, H008Y2_A57Usuario_PessoaCod, H008Y2_A3Perfil_Codigo, H008Y2_A1Usuario_Codigo
               }
               , new Object[] {
               H008Y3_AGRID_nRecordCount
               }
            }
         );
         AV108Pgmname = "WWUsuarioPerfil";
         /* GeneXus formulas. */
         AV108Pgmname = "WWUsuarioPerfil";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_63 ;
      private short nGXsfl_63_idx=1 ;
      private short AV13OrderedBy ;
      private short AV71TFUsuarioPerfil_Insert_Sel ;
      private short AV74TFUsuarioPerfil_Update_Sel ;
      private short AV77TFUsuarioPerfil_Delete_Sel ;
      private short AV80TFUsuarioPerfil_Display_Sel ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short A275Perfil_Tipo ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_63_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short AV102WWUsuarioPerfilDS_15_Tfusuarioperfil_insert_sel ;
      private short AV103WWUsuarioPerfilDS_16_Tfusuarioperfil_update_sel ;
      private short AV104WWUsuarioPerfilDS_17_Tfusuarioperfil_delete_sel ;
      private short AV105WWUsuarioPerfilDS_18_Tfusuarioperfil_display_sel ;
      private short edtUsuario_Nome_Titleformat ;
      private short edtUsuario_PessoaNom_Titleformat ;
      private short edtPerfil_Nome_Titleformat ;
      private short cmbPerfil_Tipo_Titleformat ;
      private short chkUsuarioPerfil_Insert_Titleformat ;
      private short chkUsuarioPerfil_Update_Titleformat ;
      private short chkUsuarioPerfil_Delete_Titleformat ;
      private short chkUsuarioPerfil_Display_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int subGrid_Rows ;
      private int A1Usuario_Codigo ;
      private int A3Perfil_Codigo ;
      private int Gridpaginationbar_Pagestoshow ;
      private int Ddo_usuario_nome_Datalistupdateminimumcharacters ;
      private int Ddo_usuario_pessoanom_Datalistupdateminimumcharacters ;
      private int Ddo_perfil_nome_Datalistupdateminimumcharacters ;
      private int edtavTfusuario_nome_Visible ;
      private int edtavTfusuario_nome_sel_Visible ;
      private int edtavTfusuario_pessoanom_Visible ;
      private int edtavTfusuario_pessoanom_sel_Visible ;
      private int edtavTfperfil_nome_Visible ;
      private int edtavTfperfil_nome_sel_Visible ;
      private int edtavTfusuarioperfil_insert_sel_Visible ;
      private int edtavTfusuarioperfil_update_sel_Visible ;
      private int edtavTfusuarioperfil_delete_sel_Visible ;
      private int edtavTfusuarioperfil_display_sel_Visible ;
      private int edtavDdo_usuario_nometitlecontrolidtoreplace_Visible ;
      private int edtavDdo_usuario_pessoanomtitlecontrolidtoreplace_Visible ;
      private int edtavDdo_perfil_nometitlecontrolidtoreplace_Visible ;
      private int edtavDdo_perfil_tipotitlecontrolidtoreplace_Visible ;
      private int edtavDdo_usuarioperfil_inserttitlecontrolidtoreplace_Visible ;
      private int edtavDdo_usuarioperfil_updatetitlecontrolidtoreplace_Visible ;
      private int edtavDdo_usuarioperfil_deletetitlecontrolidtoreplace_Visible ;
      private int edtavDdo_usuarioperfil_displaytitlecontrolidtoreplace_Visible ;
      private int A57Usuario_PessoaCod ;
      private int subGrid_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int AV101WWUsuarioPerfilDS_14_Tfperfil_tipo_sels_Count ;
      private int edtavOrdereddsc_Visible ;
      private int edtavUpdate_Visible ;
      private int edtavDelete_Visible ;
      private int imgInsert_Visible ;
      private int AV83PageToGo ;
      private int imgAdddynamicfilters1_Visible ;
      private int imgRemovedynamicfilters1_Visible ;
      private int edtavPerfil_nome1_Visible ;
      private int edtavUsuario_pessoanom1_Visible ;
      private int edtavPerfil_nome2_Visible ;
      private int edtavUsuario_pessoanom2_Visible ;
      private int AV109GXV1 ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private long GRID_nFirstRecordOnPage ;
      private long AV84GridCurrentPage ;
      private long AV85GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private String Gridpaginationbar_Selectedpage ;
      private String Ddo_usuario_nome_Activeeventkey ;
      private String Ddo_usuario_nome_Filteredtext_get ;
      private String Ddo_usuario_nome_Selectedvalue_get ;
      private String Ddo_usuario_pessoanom_Activeeventkey ;
      private String Ddo_usuario_pessoanom_Filteredtext_get ;
      private String Ddo_usuario_pessoanom_Selectedvalue_get ;
      private String Ddo_perfil_nome_Activeeventkey ;
      private String Ddo_perfil_nome_Filteredtext_get ;
      private String Ddo_perfil_nome_Selectedvalue_get ;
      private String Ddo_perfil_tipo_Activeeventkey ;
      private String Ddo_perfil_tipo_Selectedvalue_get ;
      private String Ddo_usuarioperfil_insert_Activeeventkey ;
      private String Ddo_usuarioperfil_insert_Selectedvalue_get ;
      private String Ddo_usuarioperfil_update_Activeeventkey ;
      private String Ddo_usuarioperfil_update_Selectedvalue_get ;
      private String Ddo_usuarioperfil_delete_Activeeventkey ;
      private String Ddo_usuarioperfil_delete_Selectedvalue_get ;
      private String Ddo_usuarioperfil_display_Activeeventkey ;
      private String Ddo_usuarioperfil_display_Selectedvalue_get ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_63_idx="0001" ;
      private String AV18Perfil_Nome1 ;
      private String AV19Usuario_PessoaNom1 ;
      private String AV23Perfil_Nome2 ;
      private String AV24Usuario_PessoaNom2 ;
      private String AV55TFUsuario_Nome ;
      private String AV56TFUsuario_Nome_Sel ;
      private String AV59TFUsuario_PessoaNom ;
      private String AV60TFUsuario_PessoaNom_Sel ;
      private String AV63TFPerfil_Nome ;
      private String AV64TFPerfil_Nome_Sel ;
      private String AV108Pgmname ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String Ddo_usuario_nome_Caption ;
      private String Ddo_usuario_nome_Tooltip ;
      private String Ddo_usuario_nome_Cls ;
      private String Ddo_usuario_nome_Filteredtext_set ;
      private String Ddo_usuario_nome_Selectedvalue_set ;
      private String Ddo_usuario_nome_Dropdownoptionstype ;
      private String Ddo_usuario_nome_Titlecontrolidtoreplace ;
      private String Ddo_usuario_nome_Sortedstatus ;
      private String Ddo_usuario_nome_Filtertype ;
      private String Ddo_usuario_nome_Datalisttype ;
      private String Ddo_usuario_nome_Datalistproc ;
      private String Ddo_usuario_nome_Sortasc ;
      private String Ddo_usuario_nome_Sortdsc ;
      private String Ddo_usuario_nome_Loadingdata ;
      private String Ddo_usuario_nome_Cleanfilter ;
      private String Ddo_usuario_nome_Noresultsfound ;
      private String Ddo_usuario_nome_Searchbuttontext ;
      private String Ddo_usuario_pessoanom_Caption ;
      private String Ddo_usuario_pessoanom_Tooltip ;
      private String Ddo_usuario_pessoanom_Cls ;
      private String Ddo_usuario_pessoanom_Filteredtext_set ;
      private String Ddo_usuario_pessoanom_Selectedvalue_set ;
      private String Ddo_usuario_pessoanom_Dropdownoptionstype ;
      private String Ddo_usuario_pessoanom_Titlecontrolidtoreplace ;
      private String Ddo_usuario_pessoanom_Sortedstatus ;
      private String Ddo_usuario_pessoanom_Filtertype ;
      private String Ddo_usuario_pessoanom_Datalisttype ;
      private String Ddo_usuario_pessoanom_Datalistproc ;
      private String Ddo_usuario_pessoanom_Sortasc ;
      private String Ddo_usuario_pessoanom_Sortdsc ;
      private String Ddo_usuario_pessoanom_Loadingdata ;
      private String Ddo_usuario_pessoanom_Cleanfilter ;
      private String Ddo_usuario_pessoanom_Noresultsfound ;
      private String Ddo_usuario_pessoanom_Searchbuttontext ;
      private String Ddo_perfil_nome_Caption ;
      private String Ddo_perfil_nome_Tooltip ;
      private String Ddo_perfil_nome_Cls ;
      private String Ddo_perfil_nome_Filteredtext_set ;
      private String Ddo_perfil_nome_Selectedvalue_set ;
      private String Ddo_perfil_nome_Dropdownoptionstype ;
      private String Ddo_perfil_nome_Titlecontrolidtoreplace ;
      private String Ddo_perfil_nome_Sortedstatus ;
      private String Ddo_perfil_nome_Filtertype ;
      private String Ddo_perfil_nome_Datalisttype ;
      private String Ddo_perfil_nome_Datalistproc ;
      private String Ddo_perfil_nome_Sortasc ;
      private String Ddo_perfil_nome_Sortdsc ;
      private String Ddo_perfil_nome_Loadingdata ;
      private String Ddo_perfil_nome_Cleanfilter ;
      private String Ddo_perfil_nome_Noresultsfound ;
      private String Ddo_perfil_nome_Searchbuttontext ;
      private String Ddo_perfil_tipo_Caption ;
      private String Ddo_perfil_tipo_Tooltip ;
      private String Ddo_perfil_tipo_Cls ;
      private String Ddo_perfil_tipo_Selectedvalue_set ;
      private String Ddo_perfil_tipo_Dropdownoptionstype ;
      private String Ddo_perfil_tipo_Titlecontrolidtoreplace ;
      private String Ddo_perfil_tipo_Sortedstatus ;
      private String Ddo_perfil_tipo_Datalisttype ;
      private String Ddo_perfil_tipo_Datalistfixedvalues ;
      private String Ddo_perfil_tipo_Sortasc ;
      private String Ddo_perfil_tipo_Sortdsc ;
      private String Ddo_perfil_tipo_Cleanfilter ;
      private String Ddo_perfil_tipo_Searchbuttontext ;
      private String Ddo_usuarioperfil_insert_Caption ;
      private String Ddo_usuarioperfil_insert_Tooltip ;
      private String Ddo_usuarioperfil_insert_Cls ;
      private String Ddo_usuarioperfil_insert_Selectedvalue_set ;
      private String Ddo_usuarioperfil_insert_Dropdownoptionstype ;
      private String Ddo_usuarioperfil_insert_Titlecontrolidtoreplace ;
      private String Ddo_usuarioperfil_insert_Sortedstatus ;
      private String Ddo_usuarioperfil_insert_Datalisttype ;
      private String Ddo_usuarioperfil_insert_Datalistfixedvalues ;
      private String Ddo_usuarioperfil_insert_Sortasc ;
      private String Ddo_usuarioperfil_insert_Sortdsc ;
      private String Ddo_usuarioperfil_insert_Cleanfilter ;
      private String Ddo_usuarioperfil_insert_Searchbuttontext ;
      private String Ddo_usuarioperfil_update_Caption ;
      private String Ddo_usuarioperfil_update_Tooltip ;
      private String Ddo_usuarioperfil_update_Cls ;
      private String Ddo_usuarioperfil_update_Selectedvalue_set ;
      private String Ddo_usuarioperfil_update_Dropdownoptionstype ;
      private String Ddo_usuarioperfil_update_Titlecontrolidtoreplace ;
      private String Ddo_usuarioperfil_update_Sortedstatus ;
      private String Ddo_usuarioperfil_update_Datalisttype ;
      private String Ddo_usuarioperfil_update_Datalistfixedvalues ;
      private String Ddo_usuarioperfil_update_Sortasc ;
      private String Ddo_usuarioperfil_update_Sortdsc ;
      private String Ddo_usuarioperfil_update_Cleanfilter ;
      private String Ddo_usuarioperfil_update_Searchbuttontext ;
      private String Ddo_usuarioperfil_delete_Caption ;
      private String Ddo_usuarioperfil_delete_Tooltip ;
      private String Ddo_usuarioperfil_delete_Cls ;
      private String Ddo_usuarioperfil_delete_Selectedvalue_set ;
      private String Ddo_usuarioperfil_delete_Dropdownoptionstype ;
      private String Ddo_usuarioperfil_delete_Titlecontrolidtoreplace ;
      private String Ddo_usuarioperfil_delete_Sortedstatus ;
      private String Ddo_usuarioperfil_delete_Datalisttype ;
      private String Ddo_usuarioperfil_delete_Datalistfixedvalues ;
      private String Ddo_usuarioperfil_delete_Sortasc ;
      private String Ddo_usuarioperfil_delete_Sortdsc ;
      private String Ddo_usuarioperfil_delete_Cleanfilter ;
      private String Ddo_usuarioperfil_delete_Searchbuttontext ;
      private String Ddo_usuarioperfil_display_Caption ;
      private String Ddo_usuarioperfil_display_Tooltip ;
      private String Ddo_usuarioperfil_display_Cls ;
      private String Ddo_usuarioperfil_display_Selectedvalue_set ;
      private String Ddo_usuarioperfil_display_Dropdownoptionstype ;
      private String Ddo_usuarioperfil_display_Titlecontrolidtoreplace ;
      private String Ddo_usuarioperfil_display_Sortedstatus ;
      private String Ddo_usuarioperfil_display_Datalisttype ;
      private String Ddo_usuarioperfil_display_Datalistfixedvalues ;
      private String Ddo_usuarioperfil_display_Sortasc ;
      private String Ddo_usuarioperfil_display_Sortdsc ;
      private String Ddo_usuarioperfil_display_Cleanfilter ;
      private String Ddo_usuarioperfil_display_Searchbuttontext ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String chkavDynamicfiltersenabled2_Internalname ;
      private String edtavTfusuario_nome_Internalname ;
      private String edtavTfusuario_nome_Jsonclick ;
      private String edtavTfusuario_nome_sel_Internalname ;
      private String edtavTfusuario_nome_sel_Jsonclick ;
      private String edtavTfusuario_pessoanom_Internalname ;
      private String edtavTfusuario_pessoanom_Jsonclick ;
      private String edtavTfusuario_pessoanom_sel_Internalname ;
      private String edtavTfusuario_pessoanom_sel_Jsonclick ;
      private String edtavTfperfil_nome_Internalname ;
      private String edtavTfperfil_nome_Jsonclick ;
      private String edtavTfperfil_nome_sel_Internalname ;
      private String edtavTfperfil_nome_sel_Jsonclick ;
      private String edtavTfusuarioperfil_insert_sel_Internalname ;
      private String edtavTfusuarioperfil_insert_sel_Jsonclick ;
      private String edtavTfusuarioperfil_update_sel_Internalname ;
      private String edtavTfusuarioperfil_update_sel_Jsonclick ;
      private String edtavTfusuarioperfil_delete_sel_Internalname ;
      private String edtavTfusuarioperfil_delete_sel_Jsonclick ;
      private String edtavTfusuarioperfil_display_sel_Internalname ;
      private String edtavTfusuarioperfil_display_sel_Jsonclick ;
      private String edtavDdo_usuario_nometitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_usuario_pessoanomtitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_perfil_nometitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_perfil_tipotitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_usuarioperfil_inserttitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_usuarioperfil_updatetitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_usuarioperfil_deletetitlecontrolidtoreplace_Internalname ;
      private String edtavDdo_usuarioperfil_displaytitlecontrolidtoreplace_Internalname ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavUpdate_Internalname ;
      private String edtavDelete_Internalname ;
      private String edtUsuario_Codigo_Internalname ;
      private String edtPerfil_Codigo_Internalname ;
      private String edtUsuario_PessoaCod_Internalname ;
      private String A2Usuario_Nome ;
      private String edtUsuario_Nome_Internalname ;
      private String A58Usuario_PessoaNom ;
      private String edtUsuario_PessoaNom_Internalname ;
      private String A4Perfil_Nome ;
      private String edtPerfil_Nome_Internalname ;
      private String cmbPerfil_Tipo_Internalname ;
      private String chkUsuarioPerfil_Insert_Internalname ;
      private String chkUsuarioPerfil_Update_Internalname ;
      private String chkUsuarioPerfil_Delete_Internalname ;
      private String chkUsuarioPerfil_Display_Internalname ;
      private String GXCCtl ;
      private String cmbavOrderedby_Internalname ;
      private String scmdbuf ;
      private String lV89WWUsuarioPerfilDS_2_Perfil_nome1 ;
      private String lV90WWUsuarioPerfilDS_3_Usuario_pessoanom1 ;
      private String lV93WWUsuarioPerfilDS_6_Perfil_nome2 ;
      private String lV94WWUsuarioPerfilDS_7_Usuario_pessoanom2 ;
      private String lV95WWUsuarioPerfilDS_8_Tfusuario_nome ;
      private String lV97WWUsuarioPerfilDS_10_Tfusuario_pessoanom ;
      private String lV99WWUsuarioPerfilDS_12_Tfperfil_nome ;
      private String AV89WWUsuarioPerfilDS_2_Perfil_nome1 ;
      private String AV90WWUsuarioPerfilDS_3_Usuario_pessoanom1 ;
      private String AV93WWUsuarioPerfilDS_6_Perfil_nome2 ;
      private String AV94WWUsuarioPerfilDS_7_Usuario_pessoanom2 ;
      private String AV96WWUsuarioPerfilDS_9_Tfusuario_nome_sel ;
      private String AV95WWUsuarioPerfilDS_8_Tfusuario_nome ;
      private String AV98WWUsuarioPerfilDS_11_Tfusuario_pessoanom_sel ;
      private String AV97WWUsuarioPerfilDS_10_Tfusuario_pessoanom ;
      private String AV100WWUsuarioPerfilDS_13_Tfperfil_nome_sel ;
      private String AV99WWUsuarioPerfilDS_12_Tfperfil_nome ;
      private String edtavOrdereddsc_Internalname ;
      private String cmbavDynamicfiltersselector1_Internalname ;
      private String edtavPerfil_nome1_Internalname ;
      private String edtavUsuario_pessoanom1_Internalname ;
      private String cmbavDynamicfiltersselector2_Internalname ;
      private String edtavPerfil_nome2_Internalname ;
      private String edtavUsuario_pessoanom2_Internalname ;
      private String lblJsdynamicfilters_Caption ;
      private String lblJsdynamicfilters_Internalname ;
      private String imgAdddynamicfilters1_Jsonclick ;
      private String imgAdddynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters1_Jsonclick ;
      private String imgRemovedynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters2_Jsonclick ;
      private String imgRemovedynamicfilters2_Internalname ;
      private String subGrid_Internalname ;
      private String Ddo_usuario_nome_Internalname ;
      private String Ddo_usuario_pessoanom_Internalname ;
      private String Ddo_perfil_nome_Internalname ;
      private String Ddo_perfil_tipo_Internalname ;
      private String Ddo_usuarioperfil_insert_Internalname ;
      private String Ddo_usuarioperfil_update_Internalname ;
      private String Ddo_usuarioperfil_delete_Internalname ;
      private String Ddo_usuarioperfil_display_Internalname ;
      private String imgCleanfilters_Jsonclick ;
      private String imgCleanfilters_Internalname ;
      private String edtUsuario_Nome_Title ;
      private String edtUsuario_PessoaNom_Title ;
      private String edtPerfil_Nome_Title ;
      private String imgInsert_Internalname ;
      private String edtavUpdate_Tooltiptext ;
      private String edtavUpdate_Link ;
      private String edtavDelete_Tooltiptext ;
      private String edtavDelete_Link ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTablegridheader_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTableheader_Internalname ;
      private String lblUsuarioperfiltitle_Internalname ;
      private String lblUsuarioperfiltitle_Jsonclick ;
      private String lblOrderedtext_Internalname ;
      private String lblOrderedtext_Jsonclick ;
      private String cmbavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Jsonclick ;
      private String tblTablefilters_Internalname ;
      private String lblJsdynamicfilters_Jsonclick ;
      private String tblTabledynamicfilters_Internalname ;
      private String lblDynamicfiltersprefix1_Internalname ;
      private String lblDynamicfiltersprefix1_Jsonclick ;
      private String cmbavDynamicfiltersselector1_Jsonclick ;
      private String lblDynamicfiltersmiddle1_Internalname ;
      private String lblDynamicfiltersmiddle1_Jsonclick ;
      private String edtavPerfil_nome1_Jsonclick ;
      private String edtavUsuario_pessoanom1_Jsonclick ;
      private String lblDynamicfiltersprefix2_Internalname ;
      private String lblDynamicfiltersprefix2_Jsonclick ;
      private String cmbavDynamicfiltersselector2_Jsonclick ;
      private String lblDynamicfiltersmiddle2_Internalname ;
      private String lblDynamicfiltersmiddle2_Jsonclick ;
      private String edtavPerfil_nome2_Jsonclick ;
      private String edtavUsuario_pessoanom2_Jsonclick ;
      private String tblTableactions_Internalname ;
      private String imgInsert_Jsonclick ;
      private String sGXsfl_63_fel_idx="0001" ;
      private String ROClassString ;
      private String edtUsuario_Codigo_Jsonclick ;
      private String edtPerfil_Codigo_Jsonclick ;
      private String edtUsuario_PessoaCod_Jsonclick ;
      private String edtUsuario_Nome_Jsonclick ;
      private String edtUsuario_PessoaNom_Jsonclick ;
      private String edtPerfil_Nome_Jsonclick ;
      private String cmbPerfil_Tipo_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private bool entryPointCalled ;
      private bool AV14OrderedDsc ;
      private bool AV20DynamicFiltersEnabled2 ;
      private bool AV26DynamicFiltersIgnoreFirst ;
      private bool AV25DynamicFiltersRemoving ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool Ddo_usuario_nome_Includesortasc ;
      private bool Ddo_usuario_nome_Includesortdsc ;
      private bool Ddo_usuario_nome_Includefilter ;
      private bool Ddo_usuario_nome_Filterisrange ;
      private bool Ddo_usuario_nome_Includedatalist ;
      private bool Ddo_usuario_pessoanom_Includesortasc ;
      private bool Ddo_usuario_pessoanom_Includesortdsc ;
      private bool Ddo_usuario_pessoanom_Includefilter ;
      private bool Ddo_usuario_pessoanom_Filterisrange ;
      private bool Ddo_usuario_pessoanom_Includedatalist ;
      private bool Ddo_perfil_nome_Includesortasc ;
      private bool Ddo_perfil_nome_Includesortdsc ;
      private bool Ddo_perfil_nome_Includefilter ;
      private bool Ddo_perfil_nome_Filterisrange ;
      private bool Ddo_perfil_nome_Includedatalist ;
      private bool Ddo_perfil_tipo_Includesortasc ;
      private bool Ddo_perfil_tipo_Includesortdsc ;
      private bool Ddo_perfil_tipo_Includefilter ;
      private bool Ddo_perfil_tipo_Includedatalist ;
      private bool Ddo_perfil_tipo_Allowmultipleselection ;
      private bool Ddo_usuarioperfil_insert_Includesortasc ;
      private bool Ddo_usuarioperfil_insert_Includesortdsc ;
      private bool Ddo_usuarioperfil_insert_Includefilter ;
      private bool Ddo_usuarioperfil_insert_Includedatalist ;
      private bool Ddo_usuarioperfil_update_Includesortasc ;
      private bool Ddo_usuarioperfil_update_Includesortdsc ;
      private bool Ddo_usuarioperfil_update_Includefilter ;
      private bool Ddo_usuarioperfil_update_Includedatalist ;
      private bool Ddo_usuarioperfil_delete_Includesortasc ;
      private bool Ddo_usuarioperfil_delete_Includesortdsc ;
      private bool Ddo_usuarioperfil_delete_Includefilter ;
      private bool Ddo_usuarioperfil_delete_Includedatalist ;
      private bool Ddo_usuarioperfil_display_Includesortasc ;
      private bool Ddo_usuarioperfil_display_Includesortdsc ;
      private bool Ddo_usuarioperfil_display_Includefilter ;
      private bool Ddo_usuarioperfil_display_Includedatalist ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n2Usuario_Nome ;
      private bool n58Usuario_PessoaNom ;
      private bool A544UsuarioPerfil_Insert ;
      private bool n544UsuarioPerfil_Insert ;
      private bool A659UsuarioPerfil_Update ;
      private bool n659UsuarioPerfil_Update ;
      private bool A546UsuarioPerfil_Delete ;
      private bool n546UsuarioPerfil_Delete ;
      private bool A543UsuarioPerfil_Display ;
      private bool n543UsuarioPerfil_Display ;
      private bool AV91WWUsuarioPerfilDS_4_Dynamicfiltersenabled2 ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV27Update_IsBlob ;
      private bool AV28Delete_IsBlob ;
      private String AV67TFPerfil_Tipo_SelsJson ;
      private String AV16DynamicFiltersSelector1 ;
      private String AV21DynamicFiltersSelector2 ;
      private String AV57ddo_Usuario_NomeTitleControlIdToReplace ;
      private String AV61ddo_Usuario_PessoaNomTitleControlIdToReplace ;
      private String AV65ddo_Perfil_NomeTitleControlIdToReplace ;
      private String AV69ddo_Perfil_TipoTitleControlIdToReplace ;
      private String AV72ddo_UsuarioPerfil_InsertTitleControlIdToReplace ;
      private String AV75ddo_UsuarioPerfil_UpdateTitleControlIdToReplace ;
      private String AV78ddo_UsuarioPerfil_DeleteTitleControlIdToReplace ;
      private String AV81ddo_UsuarioPerfil_DisplayTitleControlIdToReplace ;
      private String AV106Update_GXI ;
      private String AV107Delete_GXI ;
      private String AV88WWUsuarioPerfilDS_1_Dynamicfiltersselector1 ;
      private String AV92WWUsuarioPerfilDS_5_Dynamicfiltersselector2 ;
      private String AV27Update ;
      private String AV28Delete ;
      private IGxSession AV49Session ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbavOrderedby ;
      private GXCombobox cmbavDynamicfiltersselector1 ;
      private GXCombobox cmbavDynamicfiltersselector2 ;
      private GXCombobox cmbPerfil_Tipo ;
      private GXCheckbox chkUsuarioPerfil_Insert ;
      private GXCheckbox chkUsuarioPerfil_Update ;
      private GXCheckbox chkUsuarioPerfil_Delete ;
      private GXCheckbox chkUsuarioPerfil_Display ;
      private GXCheckbox chkavDynamicfiltersenabled2 ;
      private IDataStoreProvider pr_default ;
      private bool[] H008Y2_A543UsuarioPerfil_Display ;
      private bool[] H008Y2_n543UsuarioPerfil_Display ;
      private bool[] H008Y2_A546UsuarioPerfil_Delete ;
      private bool[] H008Y2_n546UsuarioPerfil_Delete ;
      private bool[] H008Y2_A659UsuarioPerfil_Update ;
      private bool[] H008Y2_n659UsuarioPerfil_Update ;
      private bool[] H008Y2_A544UsuarioPerfil_Insert ;
      private bool[] H008Y2_n544UsuarioPerfil_Insert ;
      private short[] H008Y2_A275Perfil_Tipo ;
      private String[] H008Y2_A4Perfil_Nome ;
      private String[] H008Y2_A58Usuario_PessoaNom ;
      private bool[] H008Y2_n58Usuario_PessoaNom ;
      private String[] H008Y2_A2Usuario_Nome ;
      private bool[] H008Y2_n2Usuario_Nome ;
      private int[] H008Y2_A57Usuario_PessoaCod ;
      private int[] H008Y2_A3Perfil_Codigo ;
      private int[] H008Y2_A1Usuario_Codigo ;
      private long[] H008Y3_AGRID_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV7HTTPRequest ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV68TFPerfil_Tipo_Sels ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV101WWUsuarioPerfilDS_14_Tfperfil_tipo_sels ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV54Usuario_NomeTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV58Usuario_PessoaNomTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV62Perfil_NomeTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV66Perfil_TipoTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV70UsuarioPerfil_InsertTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV73UsuarioPerfil_UpdateTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV76UsuarioPerfil_DeleteTitleFilterData ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtDVB_SDTDropDownOptionsData_Item ))]
      private IGxCollection AV79UsuarioPerfil_DisplayTitleFilterData ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPGridState AV10GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV11GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV12GridStateDynamicFilter ;
      private wwpbaseobjects.SdtWWPTransactionContext AV8TrnContext ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons AV82DDO_TitleSettingsIcons ;
      private wwpbaseobjects.SdtDVB_SDTDropDownOptionsTitleSettingsIcons GXt_SdtDVB_SDTDropDownOptionsTitleSettingsIcons1 ;
   }

   public class wwusuarioperfil__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H008Y2( IGxContext context ,
                                             short A275Perfil_Tipo ,
                                             IGxCollection AV101WWUsuarioPerfilDS_14_Tfperfil_tipo_sels ,
                                             String AV88WWUsuarioPerfilDS_1_Dynamicfiltersselector1 ,
                                             String AV89WWUsuarioPerfilDS_2_Perfil_nome1 ,
                                             String AV90WWUsuarioPerfilDS_3_Usuario_pessoanom1 ,
                                             bool AV91WWUsuarioPerfilDS_4_Dynamicfiltersenabled2 ,
                                             String AV92WWUsuarioPerfilDS_5_Dynamicfiltersselector2 ,
                                             String AV93WWUsuarioPerfilDS_6_Perfil_nome2 ,
                                             String AV94WWUsuarioPerfilDS_7_Usuario_pessoanom2 ,
                                             String AV96WWUsuarioPerfilDS_9_Tfusuario_nome_sel ,
                                             String AV95WWUsuarioPerfilDS_8_Tfusuario_nome ,
                                             String AV98WWUsuarioPerfilDS_11_Tfusuario_pessoanom_sel ,
                                             String AV97WWUsuarioPerfilDS_10_Tfusuario_pessoanom ,
                                             String AV100WWUsuarioPerfilDS_13_Tfperfil_nome_sel ,
                                             String AV99WWUsuarioPerfilDS_12_Tfperfil_nome ,
                                             int AV101WWUsuarioPerfilDS_14_Tfperfil_tipo_sels_Count ,
                                             short AV102WWUsuarioPerfilDS_15_Tfusuarioperfil_insert_sel ,
                                             short AV103WWUsuarioPerfilDS_16_Tfusuarioperfil_update_sel ,
                                             short AV104WWUsuarioPerfilDS_17_Tfusuarioperfil_delete_sel ,
                                             short AV105WWUsuarioPerfilDS_18_Tfusuarioperfil_display_sel ,
                                             String A4Perfil_Nome ,
                                             String A58Usuario_PessoaNom ,
                                             String A2Usuario_Nome ,
                                             bool A544UsuarioPerfil_Insert ,
                                             bool A659UsuarioPerfil_Update ,
                                             bool A546UsuarioPerfil_Delete ,
                                             bool A543UsuarioPerfil_Display ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [15] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " T1.[UsuarioPerfil_Display], T1.[UsuarioPerfil_Delete], T1.[UsuarioPerfil_Update], T1.[UsuarioPerfil_Insert], T2.[Perfil_Tipo], T2.[Perfil_Nome], T4.[Pessoa_Nome] AS Usuario_PessoaNom, T3.[Usuario_Nome], T3.[Usuario_PessoaCod] AS Usuario_PessoaCod, T1.[Perfil_Codigo], T1.[Usuario_Codigo]";
         sFromString = " FROM ((([UsuarioPerfil] T1 WITH (NOLOCK) INNER JOIN [Perfil] T2 WITH (NOLOCK) ON T2.[Perfil_Codigo] = T1.[Perfil_Codigo]) INNER JOIN [Usuario] T3 WITH (NOLOCK) ON T3.[Usuario_Codigo] = T1.[Usuario_Codigo]) INNER JOIN [Pessoa] T4 WITH (NOLOCK) ON T4.[Pessoa_Codigo] = T3.[Usuario_PessoaCod])";
         sOrderString = "";
         if ( ( StringUtil.StrCmp(AV88WWUsuarioPerfilDS_1_Dynamicfiltersselector1, "PERFIL_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV89WWUsuarioPerfilDS_2_Perfil_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Perfil_Nome] like '%' + @lV89WWUsuarioPerfilDS_2_Perfil_nome1 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Perfil_Nome] like '%' + @lV89WWUsuarioPerfilDS_2_Perfil_nome1 + '%')";
            }
         }
         else
         {
            GXv_int2[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV88WWUsuarioPerfilDS_1_Dynamicfiltersselector1, "USUARIO_PESSOANOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV90WWUsuarioPerfilDS_3_Usuario_pessoanom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like '%' + @lV90WWUsuarioPerfilDS_3_Usuario_pessoanom1 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Nome] like '%' + @lV90WWUsuarioPerfilDS_3_Usuario_pessoanom1 + '%')";
            }
         }
         else
         {
            GXv_int2[1] = 1;
         }
         if ( AV91WWUsuarioPerfilDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV92WWUsuarioPerfilDS_5_Dynamicfiltersselector2, "PERFIL_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV93WWUsuarioPerfilDS_6_Perfil_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Perfil_Nome] like '%' + @lV93WWUsuarioPerfilDS_6_Perfil_nome2 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Perfil_Nome] like '%' + @lV93WWUsuarioPerfilDS_6_Perfil_nome2 + '%')";
            }
         }
         else
         {
            GXv_int2[2] = 1;
         }
         if ( AV91WWUsuarioPerfilDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV92WWUsuarioPerfilDS_5_Dynamicfiltersselector2, "USUARIO_PESSOANOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV94WWUsuarioPerfilDS_7_Usuario_pessoanom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like '%' + @lV94WWUsuarioPerfilDS_7_Usuario_pessoanom2 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Nome] like '%' + @lV94WWUsuarioPerfilDS_7_Usuario_pessoanom2 + '%')";
            }
         }
         else
         {
            GXv_int2[3] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV96WWUsuarioPerfilDS_9_Tfusuario_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV95WWUsuarioPerfilDS_8_Tfusuario_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Usuario_Nome] like @lV95WWUsuarioPerfilDS_8_Tfusuario_nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Usuario_Nome] like @lV95WWUsuarioPerfilDS_8_Tfusuario_nome)";
            }
         }
         else
         {
            GXv_int2[4] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV96WWUsuarioPerfilDS_9_Tfusuario_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Usuario_Nome] = @AV96WWUsuarioPerfilDS_9_Tfusuario_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Usuario_Nome] = @AV96WWUsuarioPerfilDS_9_Tfusuario_nome_sel)";
            }
         }
         else
         {
            GXv_int2[5] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV98WWUsuarioPerfilDS_11_Tfusuario_pessoanom_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV97WWUsuarioPerfilDS_10_Tfusuario_pessoanom)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Nome] like @lV97WWUsuarioPerfilDS_10_Tfusuario_pessoanom)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Nome] like @lV97WWUsuarioPerfilDS_10_Tfusuario_pessoanom)";
            }
         }
         else
         {
            GXv_int2[6] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV98WWUsuarioPerfilDS_11_Tfusuario_pessoanom_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Pessoa_Nome] = @AV98WWUsuarioPerfilDS_11_Tfusuario_pessoanom_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Pessoa_Nome] = @AV98WWUsuarioPerfilDS_11_Tfusuario_pessoanom_sel)";
            }
         }
         else
         {
            GXv_int2[7] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV100WWUsuarioPerfilDS_13_Tfperfil_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV99WWUsuarioPerfilDS_12_Tfperfil_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Perfil_Nome] like @lV99WWUsuarioPerfilDS_12_Tfperfil_nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Perfil_Nome] like @lV99WWUsuarioPerfilDS_12_Tfperfil_nome)";
            }
         }
         else
         {
            GXv_int2[8] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV100WWUsuarioPerfilDS_13_Tfperfil_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Perfil_Nome] = @AV100WWUsuarioPerfilDS_13_Tfperfil_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Perfil_Nome] = @AV100WWUsuarioPerfilDS_13_Tfperfil_nome_sel)";
            }
         }
         else
         {
            GXv_int2[9] = 1;
         }
         if ( AV101WWUsuarioPerfilDS_14_Tfperfil_tipo_sels_Count > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV101WWUsuarioPerfilDS_14_Tfperfil_tipo_sels, "T2.[Perfil_Tipo] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV101WWUsuarioPerfilDS_14_Tfperfil_tipo_sels, "T2.[Perfil_Tipo] IN (", ")") + ")";
            }
         }
         if ( AV102WWUsuarioPerfilDS_15_Tfusuarioperfil_insert_sel == 1 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[UsuarioPerfil_Insert] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[UsuarioPerfil_Insert] = 1)";
            }
         }
         if ( AV102WWUsuarioPerfilDS_15_Tfusuarioperfil_insert_sel == 2 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[UsuarioPerfil_Insert] = 0)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[UsuarioPerfil_Insert] = 0)";
            }
         }
         if ( AV103WWUsuarioPerfilDS_16_Tfusuarioperfil_update_sel == 1 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[UsuarioPerfil_Update] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[UsuarioPerfil_Update] = 1)";
            }
         }
         if ( AV103WWUsuarioPerfilDS_16_Tfusuarioperfil_update_sel == 2 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[UsuarioPerfil_Update] = 0)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[UsuarioPerfil_Update] = 0)";
            }
         }
         if ( AV104WWUsuarioPerfilDS_17_Tfusuarioperfil_delete_sel == 1 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[UsuarioPerfil_Delete] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[UsuarioPerfil_Delete] = 1)";
            }
         }
         if ( AV104WWUsuarioPerfilDS_17_Tfusuarioperfil_delete_sel == 2 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[UsuarioPerfil_Delete] = 0)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[UsuarioPerfil_Delete] = 0)";
            }
         }
         if ( AV105WWUsuarioPerfilDS_18_Tfusuarioperfil_display_sel == 1 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[UsuarioPerfil_Display] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[UsuarioPerfil_Display] = 1)";
            }
         }
         if ( AV105WWUsuarioPerfilDS_18_Tfusuarioperfil_display_sel == 2 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[UsuarioPerfil_Display] = 0)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[UsuarioPerfil_Display] = 0)";
            }
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            sWhereString = " WHERE" + sWhereString;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T2.[Perfil_Nome]";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T2.[Perfil_Nome] DESC";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T3.[Usuario_Nome]";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T3.[Usuario_Nome] DESC";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T4.[Pessoa_Nome]";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T4.[Pessoa_Nome] DESC";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T2.[Perfil_Tipo]";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T2.[Perfil_Tipo] DESC";
         }
         else if ( ( AV13OrderedBy == 5 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[UsuarioPerfil_Insert]";
         }
         else if ( ( AV13OrderedBy == 5 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[UsuarioPerfil_Insert] DESC";
         }
         else if ( ( AV13OrderedBy == 6 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[UsuarioPerfil_Update]";
         }
         else if ( ( AV13OrderedBy == 6 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[UsuarioPerfil_Update] DESC";
         }
         else if ( ( AV13OrderedBy == 7 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[UsuarioPerfil_Delete]";
         }
         else if ( ( AV13OrderedBy == 7 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[UsuarioPerfil_Delete] DESC";
         }
         else if ( ( AV13OrderedBy == 8 ) && ! AV14OrderedDsc )
         {
            sOrderString = sOrderString + " ORDER BY T1.[UsuarioPerfil_Display]";
         }
         else if ( ( AV13OrderedBy == 8 ) && ( AV14OrderedDsc ) )
         {
            sOrderString = sOrderString + " ORDER BY T1.[UsuarioPerfil_Display] DESC";
         }
         else if ( true )
         {
            sOrderString = sOrderString + " ORDER BY T1.[Usuario_Codigo], T1.[Perfil_Codigo]";
         }
         scmdbuf = "SELECT * FROM (SELECT " + sSelectString + ", ROW_NUMBER() OVER (" + sOrderString + " ) AS GX_ROW_NUMBER" + sFromString + sWhereString + "" + ") AS GX_CTE WHERE GX_ROW_NUMBER" + " BETWEEN " + "@GXPagingFrom2" + " AND " + "@GXPagingTo2" + " OR " + "@GXPagingTo2" + " < " + "@GXPagingFrom2" + " AND GX_ROW_NUMBER >= " + "@GXPagingFrom2";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      protected Object[] conditional_H008Y3( IGxContext context ,
                                             short A275Perfil_Tipo ,
                                             IGxCollection AV101WWUsuarioPerfilDS_14_Tfperfil_tipo_sels ,
                                             String AV88WWUsuarioPerfilDS_1_Dynamicfiltersselector1 ,
                                             String AV89WWUsuarioPerfilDS_2_Perfil_nome1 ,
                                             String AV90WWUsuarioPerfilDS_3_Usuario_pessoanom1 ,
                                             bool AV91WWUsuarioPerfilDS_4_Dynamicfiltersenabled2 ,
                                             String AV92WWUsuarioPerfilDS_5_Dynamicfiltersselector2 ,
                                             String AV93WWUsuarioPerfilDS_6_Perfil_nome2 ,
                                             String AV94WWUsuarioPerfilDS_7_Usuario_pessoanom2 ,
                                             String AV96WWUsuarioPerfilDS_9_Tfusuario_nome_sel ,
                                             String AV95WWUsuarioPerfilDS_8_Tfusuario_nome ,
                                             String AV98WWUsuarioPerfilDS_11_Tfusuario_pessoanom_sel ,
                                             String AV97WWUsuarioPerfilDS_10_Tfusuario_pessoanom ,
                                             String AV100WWUsuarioPerfilDS_13_Tfperfil_nome_sel ,
                                             String AV99WWUsuarioPerfilDS_12_Tfperfil_nome ,
                                             int AV101WWUsuarioPerfilDS_14_Tfperfil_tipo_sels_Count ,
                                             short AV102WWUsuarioPerfilDS_15_Tfusuarioperfil_insert_sel ,
                                             short AV103WWUsuarioPerfilDS_16_Tfusuarioperfil_update_sel ,
                                             short AV104WWUsuarioPerfilDS_17_Tfusuarioperfil_delete_sel ,
                                             short AV105WWUsuarioPerfilDS_18_Tfusuarioperfil_display_sel ,
                                             String A4Perfil_Nome ,
                                             String A58Usuario_PessoaNom ,
                                             String A2Usuario_Nome ,
                                             bool A544UsuarioPerfil_Insert ,
                                             bool A659UsuarioPerfil_Update ,
                                             bool A546UsuarioPerfil_Delete ,
                                             bool A543UsuarioPerfil_Display ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int4 ;
         GXv_int4 = new short [10] ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM ((([UsuarioPerfil] T1 WITH (NOLOCK) INNER JOIN [Perfil] T4 WITH (NOLOCK) ON T4.[Perfil_Codigo] = T1.[Perfil_Codigo]) INNER JOIN [Usuario] T2 WITH (NOLOCK) ON T2.[Usuario_Codigo] = T1.[Usuario_Codigo]) INNER JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Usuario_PessoaCod])";
         if ( ( StringUtil.StrCmp(AV88WWUsuarioPerfilDS_1_Dynamicfiltersselector1, "PERFIL_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV89WWUsuarioPerfilDS_2_Perfil_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Perfil_Nome] like '%' + @lV89WWUsuarioPerfilDS_2_Perfil_nome1 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Perfil_Nome] like '%' + @lV89WWUsuarioPerfilDS_2_Perfil_nome1 + '%')";
            }
         }
         else
         {
            GXv_int4[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV88WWUsuarioPerfilDS_1_Dynamicfiltersselector1, "USUARIO_PESSOANOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV90WWUsuarioPerfilDS_3_Usuario_pessoanom1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like '%' + @lV90WWUsuarioPerfilDS_3_Usuario_pessoanom1 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like '%' + @lV90WWUsuarioPerfilDS_3_Usuario_pessoanom1 + '%')";
            }
         }
         else
         {
            GXv_int4[1] = 1;
         }
         if ( AV91WWUsuarioPerfilDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV92WWUsuarioPerfilDS_5_Dynamicfiltersselector2, "PERFIL_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV93WWUsuarioPerfilDS_6_Perfil_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Perfil_Nome] like '%' + @lV93WWUsuarioPerfilDS_6_Perfil_nome2 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Perfil_Nome] like '%' + @lV93WWUsuarioPerfilDS_6_Perfil_nome2 + '%')";
            }
         }
         else
         {
            GXv_int4[2] = 1;
         }
         if ( AV91WWUsuarioPerfilDS_4_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV92WWUsuarioPerfilDS_5_Dynamicfiltersselector2, "USUARIO_PESSOANOM") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV94WWUsuarioPerfilDS_7_Usuario_pessoanom2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like '%' + @lV94WWUsuarioPerfilDS_7_Usuario_pessoanom2 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like '%' + @lV94WWUsuarioPerfilDS_7_Usuario_pessoanom2 + '%')";
            }
         }
         else
         {
            GXv_int4[3] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV96WWUsuarioPerfilDS_9_Tfusuario_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV95WWUsuarioPerfilDS_8_Tfusuario_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Usuario_Nome] like @lV95WWUsuarioPerfilDS_8_Tfusuario_nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Usuario_Nome] like @lV95WWUsuarioPerfilDS_8_Tfusuario_nome)";
            }
         }
         else
         {
            GXv_int4[4] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV96WWUsuarioPerfilDS_9_Tfusuario_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T2.[Usuario_Nome] = @AV96WWUsuarioPerfilDS_9_Tfusuario_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T2.[Usuario_Nome] = @AV96WWUsuarioPerfilDS_9_Tfusuario_nome_sel)";
            }
         }
         else
         {
            GXv_int4[5] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV98WWUsuarioPerfilDS_11_Tfusuario_pessoanom_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV97WWUsuarioPerfilDS_10_Tfusuario_pessoanom)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] like @lV97WWUsuarioPerfilDS_10_Tfusuario_pessoanom)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] like @lV97WWUsuarioPerfilDS_10_Tfusuario_pessoanom)";
            }
         }
         else
         {
            GXv_int4[6] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV98WWUsuarioPerfilDS_11_Tfusuario_pessoanom_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T3.[Pessoa_Nome] = @AV98WWUsuarioPerfilDS_11_Tfusuario_pessoanom_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T3.[Pessoa_Nome] = @AV98WWUsuarioPerfilDS_11_Tfusuario_pessoanom_sel)";
            }
         }
         else
         {
            GXv_int4[7] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV100WWUsuarioPerfilDS_13_Tfperfil_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV99WWUsuarioPerfilDS_12_Tfperfil_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Perfil_Nome] like @lV99WWUsuarioPerfilDS_12_Tfperfil_nome)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Perfil_Nome] like @lV99WWUsuarioPerfilDS_12_Tfperfil_nome)";
            }
         }
         else
         {
            GXv_int4[8] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV100WWUsuarioPerfilDS_13_Tfperfil_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T4.[Perfil_Nome] = @AV100WWUsuarioPerfilDS_13_Tfperfil_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " (T4.[Perfil_Nome] = @AV100WWUsuarioPerfilDS_13_Tfperfil_nome_sel)";
            }
         }
         else
         {
            GXv_int4[9] = 1;
         }
         if ( AV101WWUsuarioPerfilDS_14_Tfperfil_tipo_sels_Count > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV101WWUsuarioPerfilDS_14_Tfperfil_tipo_sels, "T4.[Perfil_Tipo] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV101WWUsuarioPerfilDS_14_Tfperfil_tipo_sels, "T4.[Perfil_Tipo] IN (", ")") + ")";
            }
         }
         if ( AV102WWUsuarioPerfilDS_15_Tfusuarioperfil_insert_sel == 1 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[UsuarioPerfil_Insert] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[UsuarioPerfil_Insert] = 1)";
            }
         }
         if ( AV102WWUsuarioPerfilDS_15_Tfusuarioperfil_insert_sel == 2 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[UsuarioPerfil_Insert] = 0)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[UsuarioPerfil_Insert] = 0)";
            }
         }
         if ( AV103WWUsuarioPerfilDS_16_Tfusuarioperfil_update_sel == 1 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[UsuarioPerfil_Update] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[UsuarioPerfil_Update] = 1)";
            }
         }
         if ( AV103WWUsuarioPerfilDS_16_Tfusuarioperfil_update_sel == 2 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[UsuarioPerfil_Update] = 0)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[UsuarioPerfil_Update] = 0)";
            }
         }
         if ( AV104WWUsuarioPerfilDS_17_Tfusuarioperfil_delete_sel == 1 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[UsuarioPerfil_Delete] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[UsuarioPerfil_Delete] = 1)";
            }
         }
         if ( AV104WWUsuarioPerfilDS_17_Tfusuarioperfil_delete_sel == 2 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[UsuarioPerfil_Delete] = 0)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[UsuarioPerfil_Delete] = 0)";
            }
         }
         if ( AV105WWUsuarioPerfilDS_18_Tfusuarioperfil_display_sel == 1 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[UsuarioPerfil_Display] = 1)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[UsuarioPerfil_Display] = 1)";
            }
         }
         if ( AV105WWUsuarioPerfilDS_18_Tfusuarioperfil_display_sel == 2 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (T1.[UsuarioPerfil_Display] = 0)";
            }
            else
            {
               sWhereString = sWhereString + " (T1.[UsuarioPerfil_Display] = 0)";
            }
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         if ( ( AV13OrderedBy == 1 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 1 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 5 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 5 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 6 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 6 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 7 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 7 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 8 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( ( AV13OrderedBy == 8 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + "";
         }
         else if ( true )
         {
            scmdbuf = scmdbuf + "";
         }
         GXv_Object5[0] = scmdbuf;
         GXv_Object5[1] = GXv_int4;
         return GXv_Object5 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H008Y2(context, (short)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (int)dynConstraints[15] , (short)dynConstraints[16] , (short)dynConstraints[17] , (short)dynConstraints[18] , (short)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (bool)dynConstraints[23] , (bool)dynConstraints[24] , (bool)dynConstraints[25] , (bool)dynConstraints[26] , (short)dynConstraints[27] , (bool)dynConstraints[28] );
               case 1 :
                     return conditional_H008Y3(context, (short)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (bool)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (int)dynConstraints[15] , (short)dynConstraints[16] , (short)dynConstraints[17] , (short)dynConstraints[18] , (short)dynConstraints[19] , (String)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (bool)dynConstraints[23] , (bool)dynConstraints[24] , (bool)dynConstraints[25] , (bool)dynConstraints[26] , (short)dynConstraints[27] , (bool)dynConstraints[28] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH008Y2 ;
          prmH008Y2 = new Object[] {
          new Object[] {"@lV89WWUsuarioPerfilDS_2_Perfil_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV90WWUsuarioPerfilDS_3_Usuario_pessoanom1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV93WWUsuarioPerfilDS_6_Perfil_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV94WWUsuarioPerfilDS_7_Usuario_pessoanom2",SqlDbType.Char,100,0} ,
          new Object[] {"@lV95WWUsuarioPerfilDS_8_Tfusuario_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV96WWUsuarioPerfilDS_9_Tfusuario_nome_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV97WWUsuarioPerfilDS_10_Tfusuario_pessoanom",SqlDbType.Char,100,0} ,
          new Object[] {"@AV98WWUsuarioPerfilDS_11_Tfusuario_pessoanom_sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV99WWUsuarioPerfilDS_12_Tfperfil_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV100WWUsuarioPerfilDS_13_Tfperfil_nome_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH008Y3 ;
          prmH008Y3 = new Object[] {
          new Object[] {"@lV89WWUsuarioPerfilDS_2_Perfil_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV90WWUsuarioPerfilDS_3_Usuario_pessoanom1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV93WWUsuarioPerfilDS_6_Perfil_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV94WWUsuarioPerfilDS_7_Usuario_pessoanom2",SqlDbType.Char,100,0} ,
          new Object[] {"@lV95WWUsuarioPerfilDS_8_Tfusuario_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV96WWUsuarioPerfilDS_9_Tfusuario_nome_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV97WWUsuarioPerfilDS_10_Tfusuario_pessoanom",SqlDbType.Char,100,0} ,
          new Object[] {"@AV98WWUsuarioPerfilDS_11_Tfusuario_pessoanom_sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV99WWUsuarioPerfilDS_12_Tfperfil_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV100WWUsuarioPerfilDS_13_Tfperfil_nome_sel",SqlDbType.Char,50,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H008Y2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH008Y2,11,0,true,false )
             ,new CursorDef("H008Y3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH008Y3,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((bool[]) buf[0])[0] = rslt.getBool(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((bool[]) buf[2])[0] = rslt.getBool(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((bool[]) buf[4])[0] = rslt.getBool(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((bool[]) buf[6])[0] = rslt.getBool(4) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(4);
                ((short[]) buf[8])[0] = rslt.getShort(5) ;
                ((String[]) buf[9])[0] = rslt.getString(6, 50) ;
                ((String[]) buf[10])[0] = rslt.getString(7, 100) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((String[]) buf[12])[0] = rslt.getString(8, 50) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                ((int[]) buf[14])[0] = rslt.getInt(9) ;
                ((int[]) buf[15])[0] = rslt.getInt(10) ;
                ((int[]) buf[16])[0] = rslt.getInt(11) ;
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[25]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[26]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[27]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[28]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[29]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[10]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[11]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[14]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                return;
       }
    }

 }

}
