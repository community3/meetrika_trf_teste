/*
               File: Prc_OsApagar
        Description: Prc_Os Apagar
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/2/2020 16:13:7.24
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_osapagar : GXProcedure
   {
      public prc_osapagar( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_osapagar( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_ContagemResultado_Codigo ,
                           bool aP1_IsApagar ,
                           ref String aP2_TabelasExcluidas )
      {
         this.AV8ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         this.AV12IsApagar = aP1_IsApagar;
         this.AV10TabelasExcluidas = aP2_TabelasExcluidas;
         initialize();
         executePrivate();
         aP2_TabelasExcluidas=this.AV10TabelasExcluidas;
      }

      public String executeUdp( int aP0_ContagemResultado_Codigo ,
                                bool aP1_IsApagar )
      {
         this.AV8ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         this.AV12IsApagar = aP1_IsApagar;
         this.AV10TabelasExcluidas = aP2_TabelasExcluidas;
         initialize();
         executePrivate();
         aP2_TabelasExcluidas=this.AV10TabelasExcluidas;
         return AV10TabelasExcluidas ;
      }

      public void executeSubmit( int aP0_ContagemResultado_Codigo ,
                                 bool aP1_IsApagar ,
                                 ref String aP2_TabelasExcluidas )
      {
         prc_osapagar objprc_osapagar;
         objprc_osapagar = new prc_osapagar();
         objprc_osapagar.AV8ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         objprc_osapagar.AV12IsApagar = aP1_IsApagar;
         objprc_osapagar.AV10TabelasExcluidas = aP2_TabelasExcluidas;
         objprc_osapagar.context.SetSubmitInitialConfig(context);
         objprc_osapagar.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_osapagar);
         aP2_TabelasExcluidas=this.AV10TabelasExcluidas;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_osapagar)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         context.Gx_ope = "Fetch";
         context.Gx_etb = "ContagemResultado";
         /* Using cursor P00C82 */
         pr_default.execute(0, new Object[] {AV8ContagemResultado_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A456ContagemResultado_Codigo = P00C82_A456ContagemResultado_Codigo[0];
            A484ContagemResultado_StatusDmn = P00C82_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = P00C82_n484ContagemResultado_StatusDmn[0];
            if ( AV12IsApagar )
            {
               if ( ! AV13isVinculada )
               {
                  context.Gx_ope = "Delete";
                  context.Gx_etb = "ContagemResultado";
                  /* Using cursor P00C83 */
                  pr_default.execute(1, new Object[] {A456ContagemResultado_Codigo});
                  pr_default.close(1);
                  dsDefault.SmartCacheProvider.SetUpdated("ContagemResultado") ;
               }
               else
               {
                  A484ContagemResultado_StatusDmn = "X";
                  n484ContagemResultado_StatusDmn = false;
               }
               AV10TabelasExcluidas = "";
            }
            context.Gx_ope = "Update";
            context.Gx_etb = "ContagemResultado";
            /* Using cursor P00C84 */
            pr_default.execute(2, new Object[] {n484ContagemResultado_StatusDmn, A484ContagemResultado_StatusDmn, A456ContagemResultado_Codigo});
            pr_default.close(2);
            dsDefault.SmartCacheProvider.SetUpdated("ContagemResultado") ;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(0);
         context.CommitDataStores( "Prc_OsApagar");
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'DBERRORS' Routine */
         AV10TabelasExcluidas = context.Gx_etb;
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00C82_A456ContagemResultado_Codigo = new int[1] ;
         P00C82_A484ContagemResultado_StatusDmn = new String[] {""} ;
         P00C82_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         A484ContagemResultado_StatusDmn = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_osapagar__default(),
            new Object[][] {
                new Object[] {
               P00C82_A456ContagemResultado_Codigo, P00C82_A484ContagemResultado_StatusDmn, P00C82_n484ContagemResultado_StatusDmn
               }
               , new Object[] {
               }
               , new Object[] {
               }
            }
         );
         pr_default.setErrorHandler(new GxErrorHandler( new ErrorHandler(S111), context.ErrorHandlerInfo));
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV8ContagemResultado_Codigo ;
      private int A456ContagemResultado_Codigo ;
      private String scmdbuf ;
      private String A484ContagemResultado_StatusDmn ;
      private bool AV12IsApagar ;
      private bool n484ContagemResultado_StatusDmn ;
      private bool AV13isVinculada ;
      private String AV10TabelasExcluidas ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private String aP2_TabelasExcluidas ;
      private IDataStoreProvider pr_default ;
      private int[] P00C82_A456ContagemResultado_Codigo ;
      private String[] P00C82_A484ContagemResultado_StatusDmn ;
      private bool[] P00C82_n484ContagemResultado_StatusDmn ;
      private GxErrorHandler ErrorHandler ;
   }

   public class prc_osapagar__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new UpdateCursor(def[1])
         ,new UpdateCursor(def[2])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00C82 ;
          prmP00C82 = new Object[] {
          new Object[] {"@AV8ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00C83 ;
          prmP00C83 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00C84 ;
          prmP00C84 = new Object[] {
          new Object[] {"@ContagemResultado_StatusDmn",SqlDbType.Char,1,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00C82", "SELECT [ContagemResultado_Codigo], [ContagemResultado_StatusDmn] FROM [ContagemResultado] WITH (UPDLOCK) WHERE [ContagemResultado_Codigo] = @AV8ContagemResultado_Codigo ORDER BY [ContagemResultado_Codigo] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00C82,1,0,true,true )
             ,new CursorDef("P00C83", "DELETE FROM [ContagemResultado]  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00C83)
             ,new CursorDef("P00C84", "UPDATE [ContagemResultado] SET [ContagemResultado_StatusDmn]=@ContagemResultado_StatusDmn  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00C84)
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 1) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                return;
       }
    }

 }

}
