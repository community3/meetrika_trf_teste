/*
               File: REL_SemPlanilha
        Description: Sem Planilha
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/2/2020 16:14:21.33
       Program type: Main program
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.Printer;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class arel_semplanilha : GXWebProcedure, System.Web.SessionState.IRequiresSessionState
   {
      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize();
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            if ( ! entryPointCalled )
            {
               AV15User_Nome = gxfirstwebparm;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
               {
                  AV18Contratada_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  AV32Area = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  AV31Colaborador = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  AV30Servico = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  AV27Ano = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  AV29Mes = (long)(NumberUtil.Val( GetNextPar( ), "."));
               }
            }
         }
         if ( GxWebError == 0 )
         {
            executePrivate();
         }
         cleanup();
      }

      public arel_semplanilha( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public arel_semplanilha( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_User_Nome ,
                           int aP1_Contratada_Codigo ,
                           int aP2_Area ,
                           int aP3_Colaborador ,
                           int aP4_Servico ,
                           short aP5_Ano ,
                           long aP6_Mes )
      {
         this.AV15User_Nome = aP0_User_Nome;
         this.AV18Contratada_Codigo = aP1_Contratada_Codigo;
         this.AV32Area = aP2_Area;
         this.AV31Colaborador = aP3_Colaborador;
         this.AV30Servico = aP4_Servico;
         this.AV27Ano = aP5_Ano;
         this.AV29Mes = aP6_Mes;
         initialize();
         executePrivate();
      }

      public void executeSubmit( String aP0_User_Nome ,
                                 int aP1_Contratada_Codigo ,
                                 int aP2_Area ,
                                 int aP3_Colaborador ,
                                 int aP4_Servico ,
                                 short aP5_Ano ,
                                 long aP6_Mes )
      {
         arel_semplanilha objarel_semplanilha;
         objarel_semplanilha = new arel_semplanilha();
         objarel_semplanilha.AV15User_Nome = aP0_User_Nome;
         objarel_semplanilha.AV18Contratada_Codigo = aP1_Contratada_Codigo;
         objarel_semplanilha.AV32Area = aP2_Area;
         objarel_semplanilha.AV31Colaborador = aP3_Colaborador;
         objarel_semplanilha.AV30Servico = aP4_Servico;
         objarel_semplanilha.AV27Ano = aP5_Ano;
         objarel_semplanilha.AV29Mes = aP6_Mes;
         objarel_semplanilha.context.SetSubmitInitialConfig(context);
         objarel_semplanilha.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objarel_semplanilha);
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((arel_semplanilha)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         M_top = 0;
         M_bot = 4;
         P_lines = (int)(66-M_bot);
         getPrinter().GxClearAttris() ;
         add_metrics( ) ;
         lineHeight = 15;
         PrtOffset = 0;
         gxXPage = 100;
         gxYPage = 100;
         getPrinter().GxSetDocName("") ;
         try
         {
            Gx_out = "FIL" ;
            if (!initPrinter (Gx_out, gxXPage, gxYPage, "GXPRN.INI", "", "", 2, 1, 256, 16834, 11909, 0, 1, 1, 0, 1, 1) )
            {
               cleanup();
               return;
            }
            getPrinter().setModal(false) ;
            P_lines = (int)(gxYPage-(lineHeight*4));
            Gx_line = (int)(P_lines+1);
            getPrinter().setPageLines(P_lines);
            getPrinter().setLineHeight(lineHeight);
            getPrinter().setM_top(M_top);
            getPrinter().setM_bot(M_bot);
            AV13Hoje = DateTimeUtil.ServerNow( context, "DEFAULT");
            AV36SDT_Codigos.FromXml(AV12WebSession.Get("Codigos"), "SDT_CodigosCollection");
            AV40GXV1 = 1;
            while ( AV40GXV1 <= AV36SDT_Codigos.Count )
            {
               AV37Codigo = ((SdtSDT_Codigos)AV36SDT_Codigos.Item(AV40GXV1));
               AV11Codigos.Add(AV37Codigo.gxTpr_Codigo, 0);
               AV40GXV1 = (int)(AV40GXV1+1);
            }
            AV16Qtde = (short)(AV11Codigos.Count);
            /* Execute user subroutine: 'FILTROS' */
            S111 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
            /* Using cursor P00VV2 */
            pr_default.execute(0, new Object[] {AV18Contratada_Codigo});
            while ( (pr_default.getStatus(0) != 101) )
            {
               A39Contratada_Codigo = P00VV2_A39Contratada_Codigo[0];
               A40001Contratada_Logo_GXI = P00VV2_A40001Contratada_Logo_GXI[0];
               n40001Contratada_Logo_GXI = P00VV2_n40001Contratada_Logo_GXI[0];
               A1664Contratada_Logo = P00VV2_A1664Contratada_Logo[0];
               n1664Contratada_Logo = P00VV2_n1664Contratada_Logo[0];
               AV10Contratada_Logo = A1664Contratada_Logo;
               A40000Contratada_Logo_GXI = A40001Contratada_Logo_GXI;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(0);
            pr_default.dynParam(1, new Object[]{ new Object[]{
                                                 A456ContagemResultado_Codigo ,
                                                 AV11Codigos ,
                                                 A590ContagemResultadoEvidencia_TipoArq },
                                                 new int[] {
                                                 TypeConstants.INT, TypeConstants.STRING, TypeConstants.BOOLEAN
                                                 }
            });
            /* Using cursor P00VV3 */
            pr_default.execute(1);
            while ( (pr_default.getStatus(1) != 101) )
            {
               A590ContagemResultadoEvidencia_TipoArq = P00VV3_A590ContagemResultadoEvidencia_TipoArq[0];
               n590ContagemResultadoEvidencia_TipoArq = P00VV3_n590ContagemResultadoEvidencia_TipoArq[0];
               A456ContagemResultado_Codigo = P00VV3_A456ContagemResultado_Codigo[0];
               A586ContagemResultadoEvidencia_Codigo = P00VV3_A586ContagemResultadoEvidencia_Codigo[0];
               AV11Codigos.RemoveItem(AV11Codigos.IndexOf(A456ContagemResultado_Codigo));
               pr_default.readNext(1);
            }
            pr_default.close(1);
            pr_default.dynParam(2, new Object[]{ new Object[]{
                                                 A1109AnexoDe_Id ,
                                                 AV11Codigos ,
                                                 A1108Anexo_TipoArq ,
                                                 A1110AnexoDe_Tabela },
                                                 new int[] {
                                                 TypeConstants.INT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT
                                                 }
            });
            /* Using cursor P00VV4 */
            pr_default.execute(2);
            while ( (pr_default.getStatus(2) != 101) )
            {
               A1106Anexo_Codigo = P00VV4_A1106Anexo_Codigo[0];
               A1108Anexo_TipoArq = P00VV4_A1108Anexo_TipoArq[0];
               n1108Anexo_TipoArq = P00VV4_n1108Anexo_TipoArq[0];
               A1110AnexoDe_Tabela = P00VV4_A1110AnexoDe_Tabela[0];
               A1109AnexoDe_Id = P00VV4_A1109AnexoDe_Id[0];
               A1108Anexo_TipoArq = P00VV4_A1108Anexo_TipoArq[0];
               n1108Anexo_TipoArq = P00VV4_n1108Anexo_TipoArq[0];
               AV11Codigos.RemoveItem(AV11Codigos.IndexOf(A1109AnexoDe_Id));
               pr_default.readNext(2);
            }
            pr_default.close(2);
            pr_default.dynParam(3, new Object[]{ new Object[]{
                                                 A456ContagemResultado_Codigo ,
                                                 AV11Codigos },
                                                 new int[] {
                                                 TypeConstants.INT
                                                 }
            });
            /* Using cursor P00VV5 */
            pr_default.execute(3);
            while ( (pr_default.getStatus(3) != 101) )
            {
               A490ContagemResultado_ContratadaCod = P00VV5_A490ContagemResultado_ContratadaCod[0];
               n490ContagemResultado_ContratadaCod = P00VV5_n490ContagemResultado_ContratadaCod[0];
               A456ContagemResultado_Codigo = P00VV5_A456ContagemResultado_Codigo[0];
               A52Contratada_AreaTrabalhoCod = P00VV5_A52Contratada_AreaTrabalhoCod[0];
               n52Contratada_AreaTrabalhoCod = P00VV5_n52Contratada_AreaTrabalhoCod[0];
               A52Contratada_AreaTrabalhoCod = P00VV5_A52Contratada_AreaTrabalhoCod[0];
               n52Contratada_AreaTrabalhoCod = P00VV5_n52Contratada_AreaTrabalhoCod[0];
               AV23Area_Codigos.Add(A52Contratada_AreaTrabalhoCod, 0);
               pr_default.readNext(3);
            }
            pr_default.close(3);
            pr_default.dynParam(4, new Object[]{ new Object[]{
                                                 A5AreaTrabalho_Codigo ,
                                                 AV23Area_Codigos },
                                                 new int[] {
                                                 TypeConstants.INT
                                                 }
            });
            /* Using cursor P00VV6 */
            pr_default.execute(4);
            while ( (pr_default.getStatus(4) != 101) )
            {
               A5AreaTrabalho_Codigo = P00VV6_A5AreaTrabalho_Codigo[0];
               A6AreaTrabalho_Descricao = P00VV6_A6AreaTrabalho_Descricao[0];
               AV9AreaTrabalho_Descricao = A6AreaTrabalho_Descricao;
               pr_default.dynParam(5, new Object[]{ new Object[]{
                                                    A456ContagemResultado_Codigo ,
                                                    AV11Codigos ,
                                                    A52Contratada_AreaTrabalhoCod ,
                                                    A5AreaTrabalho_Codigo },
                                                    new int[] {
                                                    TypeConstants.INT, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT
                                                    }
               });
               /* Using cursor P00VV7 */
               pr_default.execute(5, new Object[] {A5AreaTrabalho_Codigo});
               while ( (pr_default.getStatus(5) != 101) )
               {
                  A490ContagemResultado_ContratadaCod = P00VV7_A490ContagemResultado_ContratadaCod[0];
                  n490ContagemResultado_ContratadaCod = P00VV7_n490ContagemResultado_ContratadaCod[0];
                  A456ContagemResultado_Codigo = P00VV7_A456ContagemResultado_Codigo[0];
                  A52Contratada_AreaTrabalhoCod = P00VV7_A52Contratada_AreaTrabalhoCod[0];
                  n52Contratada_AreaTrabalhoCod = P00VV7_n52Contratada_AreaTrabalhoCod[0];
                  A493ContagemResultado_DemandaFM = P00VV7_A493ContagemResultado_DemandaFM[0];
                  n493ContagemResultado_DemandaFM = P00VV7_n493ContagemResultado_DemandaFM[0];
                  A457ContagemResultado_Demanda = P00VV7_A457ContagemResultado_Demanda[0];
                  n457ContagemResultado_Demanda = P00VV7_n457ContagemResultado_Demanda[0];
                  A52Contratada_AreaTrabalhoCod = P00VV7_A52Contratada_AreaTrabalhoCod[0];
                  n52Contratada_AreaTrabalhoCod = P00VV7_n52Contratada_AreaTrabalhoCod[0];
                  A501ContagemResultado_OsFsOsFm = StringUtil.Trim( A457ContagemResultado_Demanda) + (String.IsNullOrEmpty(StringUtil.RTrim( A493ContagemResultado_DemandaFM)) ? "" : "|"+StringUtil.Trim( A493ContagemResultado_DemandaFM));
                  AV8ContagemResultado_Demanda = A501ContagemResultado_OsFsOsFm;
                  HVV0( false, 15) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV9AreaTrabalho_Descricao, "@!")), 42, Gx_line+0, 303, Gx_line+15, 0+256, 0, 0, 0) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV8ContagemResultado_Demanda, "@!")), 183, Gx_line+0, 340, Gx_line+15, 0+256, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+15);
                  pr_default.readNext(5);
               }
               pr_default.close(5);
               pr_default.readNext(4);
            }
            pr_default.close(4);
            AV17QtdeSem = (short)(AV11Codigos.Count);
            if ( AV17QtdeSem > 0 )
            {
               /* Noskip command */
               Gx_line = Gx_OldLine;
            }
            HVV0( false, 15) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("Total", 400, Gx_line+0, 426, Gx_line+14, 0+256, 0, 0, 0) ;
            getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV17QtdeSem), "ZZZ9")), 442, Gx_line+0, 468, Gx_line+15, 2+256, 0, 0, 0) ;
            getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV16Qtde), "ZZZ9")), 500, Gx_line+0, 526, Gx_line+15, 0+256, 0, 0, 0) ;
            getPrinter().GxDrawText("de", 475, Gx_line+0, 489, Gx_line+14, 0+256, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+15);
            /* Print footer for last page */
            ToSkip = (int)(P_lines+1);
            HVV0( true, 0) ;
         }
         catch ( GeneXus.Printer.ProcessInterruptedException e )
         {
         }
         finally
         {
            /* Close printer file */
            try
            {
               getPrinter().GxEndPage() ;
               getPrinter().GxEndDocument() ;
            }
            catch ( GeneXus.Printer.ProcessInterruptedException e )
            {
            }
            endPrinter();
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
         {
            context.Redirect( context.wjLoc );
            context.wjLoc = "";
         }
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'FILTROS' Routine */
         if ( (0==AV32Area) )
         {
            AV24Area_Nome = "Todas";
         }
         else
         {
            /* Using cursor P00VV8 */
            pr_default.execute(6, new Object[] {AV32Area});
            while ( (pr_default.getStatus(6) != 101) )
            {
               A5AreaTrabalho_Codigo = P00VV8_A5AreaTrabalho_Codigo[0];
               A6AreaTrabalho_Descricao = P00VV8_A6AreaTrabalho_Descricao[0];
               AV24Area_Nome = A6AreaTrabalho_Descricao;
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(6);
         }
         if ( (0==AV31Colaborador) )
         {
            AV25Colaborador_Nome = "Todos";
         }
         else
         {
            /* Using cursor P00VV9 */
            pr_default.execute(7, new Object[] {AV31Colaborador});
            while ( (pr_default.getStatus(7) != 101) )
            {
               A57Usuario_PessoaCod = P00VV9_A57Usuario_PessoaCod[0];
               A1Usuario_Codigo = P00VV9_A1Usuario_Codigo[0];
               A58Usuario_PessoaNom = P00VV9_A58Usuario_PessoaNom[0];
               n58Usuario_PessoaNom = P00VV9_n58Usuario_PessoaNom[0];
               A58Usuario_PessoaNom = P00VV9_A58Usuario_PessoaNom[0];
               n58Usuario_PessoaNom = P00VV9_n58Usuario_PessoaNom[0];
               AV25Colaborador_Nome = A58Usuario_PessoaNom;
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(7);
         }
         if ( (0==AV30Servico) )
         {
            AV33Servico_Sigla = "Todos";
         }
         else
         {
            /* Using cursor P00VV10 */
            pr_default.execute(8, new Object[] {AV30Servico});
            while ( (pr_default.getStatus(8) != 101) )
            {
               A155Servico_Codigo = P00VV10_A155Servico_Codigo[0];
               A605Servico_Sigla = P00VV10_A605Servico_Sigla[0];
               AV33Servico_Sigla = A605Servico_Sigla;
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(8);
         }
         if ( (0==AV29Mes) )
         {
            AV26Mes_Nome = "Todos";
         }
         else
         {
            AV34Date = context.localUtil.CToD( "01/01/01", 2);
            AV35m = (short)(AV29Mes-1);
            AV34Date = DateTimeUtil.AddMth( AV34Date, AV35m);
            AV26Mes_Nome = DateTimeUtil.CMonth( AV34Date, "por");
         }
      }

      protected void HVV0( bool bFoot ,
                           int Inc )
      {
         /* Skip the required number of lines */
         while ( ( ToSkip > 0 ) || ( Gx_line + Inc > P_lines ) )
         {
            if ( Gx_line + Inc >= P_lines )
            {
               if ( Gx_page > 0 )
               {
                  /* Print footers */
                  Gx_line = P_lines;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(Gx_page), "ZZZZZ9")), 708, Gx_line+0, 747, Gx_line+15, 1+256, 0, 0, 0) ;
                  getPrinter().GxDrawText("{{Pages}}", 767, Gx_line+0, 816, Gx_line+14, 1+256, 0, 0, 0) ;
                  getPrinter().GxDrawText("de", 750, Gx_line+0, 764, Gx_line+14, 0+256, 0, 0, 0) ;
                  getPrinter().GxDrawText("P�gina", 675, Gx_line+0, 710, Gx_line+14, 0+256, 0, 0, 0) ;
                  getPrinter().GxDrawText(context.localUtil.Format( AV13Hoje, "99/99/99 99:99"), 375, Gx_line+0, 455, Gx_line+15, 1+256, 0, 0, 0) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV15User_Nome, "@!")), 0, Gx_line+0, 261, Gx_line+15, 0+256, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+17);
                  getPrinter().GxEndPage() ;
                  if ( bFoot )
                  {
                     return  ;
                  }
               }
               ToSkip = 0;
               Gx_line = 0;
               Gx_page = (int)(Gx_page+1);
               /* Skip Margin Top Lines */
               Gx_line = (int)(Gx_line+(M_top*lineHeight));
               /* Print headers */
               getPrinter().GxStartPage() ;
               getPrinter().GxDrawLine(25, Gx_line+133, 808, Gx_line+133, 1, 0, 0, 0, 0) ;
               getPrinter().GxAttris("Calibri", 16, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("OS sem Planilha", 300, Gx_line+17, 525, Gx_line+45, 1, 0, 0, 0) ;
               getPrinter().GxDrawBitMap(AV10Contratada_Logo, 0, Gx_line+0, 120, Gx_line+100) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText("�rea", 42, Gx_line+117, 66, Gx_line+131, 0+256, 0, 0, 0) ;
               getPrinter().GxDrawText("N� OS Ref.| N� OS", 183, Gx_line+117, 276, Gx_line+131, 0+256, 0, 0, 0) ;
               getPrinter().GxDrawText("Filtros -", 150, Gx_line+67, 185, Gx_line+81, 0+256, 0, 0, 0) ;
               getPrinter().GxDrawText("�rea:", 208, Gx_line+67, 235, Gx_line+81, 0+256, 0, 0, 0) ;
               getPrinter().GxDrawText("Colaborador:", 208, Gx_line+83, 272, Gx_line+97, 0+256, 0, 0, 0) ;
               getPrinter().GxDrawText("Servi�o:", 550, Gx_line+67, 592, Gx_line+81, 0+256, 0, 0, 0) ;
               getPrinter().GxDrawText("Ano:", 550, Gx_line+83, 574, Gx_line+97, 0+256, 0, 0, 0) ;
               getPrinter().GxDrawText("M�s:", 550, Gx_line+100, 575, Gx_line+114, 0+256, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV24Area_Nome, "@!")), 242, Gx_line+67, 503, Gx_line+82, 0+256, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV25Colaborador_Nome, "@!")), 283, Gx_line+83, 544, Gx_line+98, 0+256, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV27Ano), "ZZZ9")), 592, Gx_line+83, 618, Gx_line+98, 2+256, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV26Mes_Nome, "@!")), 592, Gx_line+100, 817, Gx_line+115, 0, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV33Servico_Sigla, "@!")), 592, Gx_line+67, 671, Gx_line+82, 0+256, 0, 0, 0) ;
               Gx_OldLine = Gx_line;
               Gx_line = (int)(Gx_line+137);
               if (true) break;
            }
            else
            {
               PrtOffset = 0;
               Gx_line = (int)(Gx_line+1);
            }
            ToSkip = (int)(ToSkip-1);
         }
         getPrinter().setPage(Gx_page);
      }

      protected void add_metrics( )
      {
         add_metrics0( ) ;
         add_metrics1( ) ;
      }

      protected void add_metrics0( )
      {
         getPrinter().setMetrics("Microsoft Sans Serif", false, false, 58, 14, 72, 171,  new int[] {48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 18, 20, 23, 36, 36, 57, 43, 12, 21, 21, 25, 37, 18, 21, 18, 18, 36, 36, 36, 36, 36, 36, 36, 36, 36, 36, 18, 18, 37, 37, 37, 36, 65, 43, 43, 46, 46, 43, 39, 50, 46, 18, 32, 43, 36, 53, 46, 50, 43, 50, 46, 43, 40, 46, 43, 64, 41, 42, 39, 18, 18, 18, 27, 36, 21, 36, 36, 32, 36, 36, 18, 36, 36, 14, 15, 33, 14, 55, 36, 36, 36, 36, 21, 32, 18, 36, 33, 47, 31, 31, 31, 21, 17, 21, 37, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 18, 20, 36, 36, 36, 36, 17, 36, 21, 47, 24, 36, 37, 21, 47, 35, 26, 35, 21, 21, 21, 37, 34, 21, 21, 21, 23, 36, 53, 53, 53, 39, 43, 43, 43, 43, 43, 43, 64, 46, 43, 43, 43, 43, 18, 18, 18, 18, 46, 46, 50, 50, 50, 50, 50, 37, 50, 46, 46, 46, 46, 43, 43, 39, 36, 36, 36, 36, 36, 36, 57, 32, 36, 36, 36, 36, 18, 18, 18, 18, 36, 36, 36, 36, 36, 36, 36, 35, 39, 36, 36, 36, 36, 32, 36, 32}) ;
      }

      protected void add_metrics1( )
      {
         getPrinter().setMetrics("Calibri", true, false, 57, 15, 72, 163,  new int[] {47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 17, 19, 29, 34, 34, 55, 45, 15, 21, 21, 24, 36, 17, 21, 17, 17, 34, 34, 34, 34, 34, 34, 34, 34, 34, 34, 21, 21, 36, 36, 36, 38, 60, 43, 45, 45, 45, 41, 38, 48, 45, 17, 34, 45, 38, 53, 45, 48, 41, 48, 45, 41, 38, 45, 41, 57, 41, 41, 38, 21, 17, 21, 36, 34, 21, 34, 38, 34, 38, 34, 21, 38, 38, 17, 17, 34, 17, 55, 38, 38, 38, 38, 24, 34, 21, 38, 33, 49, 34, 34, 31, 24, 17, 24, 36, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 17, 21, 34, 34, 34, 34, 17, 34, 21, 46, 23, 34, 36, 21, 46, 34, 25, 34, 21, 21, 21, 36, 34, 21, 20, 21, 23, 34, 52, 52, 52, 38, 45, 45, 45, 45, 45, 45, 62, 45, 41, 41, 41, 41, 17, 17, 17, 17, 45, 45, 48, 48, 48, 48, 48, 36, 48, 45, 45, 45, 45, 41, 41, 38, 34, 34, 34, 34, 34, 34, 55, 34, 34, 34, 34, 34, 17, 17, 17, 17, 38, 38, 38, 38, 38, 38, 38, 34, 38, 38, 38, 38, 38, 34, 38, 34}) ;
      }

      public override int getOutputType( )
      {
         return GxReportUtils.OUTPUT_PDF ;
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if (IsMain)	waitPrinterEnd();
         base.cleanup();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         GXKey = "";
         gxfirstwebparm = "";
         AV13Hoje = (DateTime)(DateTime.MinValue);
         AV36SDT_Codigos = new GxObjectCollection( context, "SDT_Codigos", "GxEv3Up14_MeetrikaVs3", "SdtSDT_Codigos", "GeneXus.Programs");
         AV12WebSession = context.GetSession();
         AV37Codigo = new SdtSDT_Codigos(context);
         AV11Codigos = new GxSimpleCollection();
         scmdbuf = "";
         P00VV2_A39Contratada_Codigo = new int[1] ;
         P00VV2_A40001Contratada_Logo_GXI = new String[] {""} ;
         P00VV2_n40001Contratada_Logo_GXI = new bool[] {false} ;
         P00VV2_A1664Contratada_Logo = new String[] {""} ;
         P00VV2_n1664Contratada_Logo = new bool[] {false} ;
         A40001Contratada_Logo_GXI = "";
         A1664Contratada_Logo = "";
         AV10Contratada_Logo = "";
         A40000Contratada_Logo_GXI = "";
         A590ContagemResultadoEvidencia_TipoArq = "";
         P00VV3_A590ContagemResultadoEvidencia_TipoArq = new String[] {""} ;
         P00VV3_n590ContagemResultadoEvidencia_TipoArq = new bool[] {false} ;
         P00VV3_A456ContagemResultado_Codigo = new int[1] ;
         P00VV3_A586ContagemResultadoEvidencia_Codigo = new int[1] ;
         A1108Anexo_TipoArq = "";
         P00VV4_A1106Anexo_Codigo = new int[1] ;
         P00VV4_A1108Anexo_TipoArq = new String[] {""} ;
         P00VV4_n1108Anexo_TipoArq = new bool[] {false} ;
         P00VV4_A1110AnexoDe_Tabela = new int[1] ;
         P00VV4_A1109AnexoDe_Id = new int[1] ;
         P00VV5_A490ContagemResultado_ContratadaCod = new int[1] ;
         P00VV5_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         P00VV5_A456ContagemResultado_Codigo = new int[1] ;
         P00VV5_A52Contratada_AreaTrabalhoCod = new int[1] ;
         P00VV5_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         AV23Area_Codigos = new GxSimpleCollection();
         P00VV6_A5AreaTrabalho_Codigo = new int[1] ;
         P00VV6_A6AreaTrabalho_Descricao = new String[] {""} ;
         A6AreaTrabalho_Descricao = "";
         AV9AreaTrabalho_Descricao = "";
         P00VV7_A490ContagemResultado_ContratadaCod = new int[1] ;
         P00VV7_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         P00VV7_A456ContagemResultado_Codigo = new int[1] ;
         P00VV7_A52Contratada_AreaTrabalhoCod = new int[1] ;
         P00VV7_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         P00VV7_A493ContagemResultado_DemandaFM = new String[] {""} ;
         P00VV7_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         P00VV7_A457ContagemResultado_Demanda = new String[] {""} ;
         P00VV7_n457ContagemResultado_Demanda = new bool[] {false} ;
         A493ContagemResultado_DemandaFM = "";
         A457ContagemResultado_Demanda = "";
         A501ContagemResultado_OsFsOsFm = "";
         AV8ContagemResultado_Demanda = "";
         AV24Area_Nome = "";
         P00VV8_A5AreaTrabalho_Codigo = new int[1] ;
         P00VV8_A6AreaTrabalho_Descricao = new String[] {""} ;
         AV25Colaborador_Nome = "";
         P00VV9_A57Usuario_PessoaCod = new int[1] ;
         P00VV9_A1Usuario_Codigo = new int[1] ;
         P00VV9_A58Usuario_PessoaNom = new String[] {""} ;
         P00VV9_n58Usuario_PessoaNom = new bool[] {false} ;
         A58Usuario_PessoaNom = "";
         AV33Servico_Sigla = "";
         P00VV10_A155Servico_Codigo = new int[1] ;
         P00VV10_A605Servico_Sigla = new String[] {""} ;
         A605Servico_Sigla = "";
         AV26Mes_Nome = "";
         AV34Date = DateTime.MinValue;
         AV10Contratada_Logo = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.arel_semplanilha__default(),
            new Object[][] {
                new Object[] {
               P00VV2_A39Contratada_Codigo, P00VV2_A40001Contratada_Logo_GXI, P00VV2_n40001Contratada_Logo_GXI, P00VV2_A1664Contratada_Logo, P00VV2_n1664Contratada_Logo
               }
               , new Object[] {
               P00VV3_A590ContagemResultadoEvidencia_TipoArq, P00VV3_n590ContagemResultadoEvidencia_TipoArq, P00VV3_A456ContagemResultado_Codigo, P00VV3_A586ContagemResultadoEvidencia_Codigo
               }
               , new Object[] {
               P00VV4_A1106Anexo_Codigo, P00VV4_A1108Anexo_TipoArq, P00VV4_n1108Anexo_TipoArq, P00VV4_A1110AnexoDe_Tabela, P00VV4_A1109AnexoDe_Id
               }
               , new Object[] {
               P00VV5_A490ContagemResultado_ContratadaCod, P00VV5_n490ContagemResultado_ContratadaCod, P00VV5_A456ContagemResultado_Codigo, P00VV5_A52Contratada_AreaTrabalhoCod, P00VV5_n52Contratada_AreaTrabalhoCod
               }
               , new Object[] {
               P00VV6_A5AreaTrabalho_Codigo, P00VV6_A6AreaTrabalho_Descricao
               }
               , new Object[] {
               P00VV7_A490ContagemResultado_ContratadaCod, P00VV7_n490ContagemResultado_ContratadaCod, P00VV7_A456ContagemResultado_Codigo, P00VV7_A52Contratada_AreaTrabalhoCod, P00VV7_n52Contratada_AreaTrabalhoCod, P00VV7_A493ContagemResultado_DemandaFM, P00VV7_n493ContagemResultado_DemandaFM, P00VV7_A457ContagemResultado_Demanda, P00VV7_n457ContagemResultado_Demanda
               }
               , new Object[] {
               P00VV8_A5AreaTrabalho_Codigo, P00VV8_A6AreaTrabalho_Descricao
               }
               , new Object[] {
               P00VV9_A57Usuario_PessoaCod, P00VV9_A1Usuario_Codigo, P00VV9_A58Usuario_PessoaNom, P00VV9_n58Usuario_PessoaNom
               }
               , new Object[] {
               P00VV10_A155Servico_Codigo, P00VV10_A605Servico_Sigla
               }
            }
         );
         /* GeneXus formulas. */
         Gx_line = 0;
         context.Gx_err = 0;
      }

      private short gxcookieaux ;
      private short nGotPars ;
      private short AV27Ano ;
      private short GxWebError ;
      private short AV16Qtde ;
      private short AV17QtdeSem ;
      private short AV35m ;
      private int AV18Contratada_Codigo ;
      private int AV32Area ;
      private int AV31Colaborador ;
      private int AV30Servico ;
      private int M_top ;
      private int M_bot ;
      private int Line ;
      private int ToSkip ;
      private int PrtOffset ;
      private int AV40GXV1 ;
      private int A39Contratada_Codigo ;
      private int A456ContagemResultado_Codigo ;
      private int A586ContagemResultadoEvidencia_Codigo ;
      private int A1109AnexoDe_Id ;
      private int A1110AnexoDe_Tabela ;
      private int A1106Anexo_Codigo ;
      private int A490ContagemResultado_ContratadaCod ;
      private int A52Contratada_AreaTrabalhoCod ;
      private int A5AreaTrabalho_Codigo ;
      private int Gx_OldLine ;
      private int A57Usuario_PessoaCod ;
      private int A1Usuario_Codigo ;
      private int A155Servico_Codigo ;
      private long AV29Mes ;
      private String GXKey ;
      private String gxfirstwebparm ;
      private String AV15User_Nome ;
      private String scmdbuf ;
      private String A590ContagemResultadoEvidencia_TipoArq ;
      private String A1108Anexo_TipoArq ;
      private String AV24Area_Nome ;
      private String AV25Colaborador_Nome ;
      private String A58Usuario_PessoaNom ;
      private String AV33Servico_Sigla ;
      private String A605Servico_Sigla ;
      private String AV26Mes_Nome ;
      private DateTime AV13Hoje ;
      private DateTime AV34Date ;
      private bool entryPointCalled ;
      private bool returnInSub ;
      private bool n40001Contratada_Logo_GXI ;
      private bool n1664Contratada_Logo ;
      private bool n590ContagemResultadoEvidencia_TipoArq ;
      private bool n1108Anexo_TipoArq ;
      private bool n490ContagemResultado_ContratadaCod ;
      private bool n52Contratada_AreaTrabalhoCod ;
      private bool n493ContagemResultado_DemandaFM ;
      private bool n457ContagemResultado_Demanda ;
      private bool n58Usuario_PessoaNom ;
      private String A40001Contratada_Logo_GXI ;
      private String A40000Contratada_Logo_GXI ;
      private String A6AreaTrabalho_Descricao ;
      private String AV9AreaTrabalho_Descricao ;
      private String A493ContagemResultado_DemandaFM ;
      private String A457ContagemResultado_Demanda ;
      private String A501ContagemResultado_OsFsOsFm ;
      private String AV8ContagemResultado_Demanda ;
      private String A1664Contratada_Logo ;
      private String AV10Contratada_Logo ;
      private String Contratada_logo ;
      private IGxSession AV12WebSession ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00VV2_A39Contratada_Codigo ;
      private String[] P00VV2_A40001Contratada_Logo_GXI ;
      private bool[] P00VV2_n40001Contratada_Logo_GXI ;
      private String[] P00VV2_A1664Contratada_Logo ;
      private bool[] P00VV2_n1664Contratada_Logo ;
      private String[] P00VV3_A590ContagemResultadoEvidencia_TipoArq ;
      private bool[] P00VV3_n590ContagemResultadoEvidencia_TipoArq ;
      private int[] P00VV3_A456ContagemResultado_Codigo ;
      private int[] P00VV3_A586ContagemResultadoEvidencia_Codigo ;
      private int[] P00VV4_A1106Anexo_Codigo ;
      private String[] P00VV4_A1108Anexo_TipoArq ;
      private bool[] P00VV4_n1108Anexo_TipoArq ;
      private int[] P00VV4_A1110AnexoDe_Tabela ;
      private int[] P00VV4_A1109AnexoDe_Id ;
      private int[] P00VV5_A490ContagemResultado_ContratadaCod ;
      private bool[] P00VV5_n490ContagemResultado_ContratadaCod ;
      private int[] P00VV5_A456ContagemResultado_Codigo ;
      private int[] P00VV5_A52Contratada_AreaTrabalhoCod ;
      private bool[] P00VV5_n52Contratada_AreaTrabalhoCod ;
      private int[] P00VV6_A5AreaTrabalho_Codigo ;
      private String[] P00VV6_A6AreaTrabalho_Descricao ;
      private int[] P00VV7_A490ContagemResultado_ContratadaCod ;
      private bool[] P00VV7_n490ContagemResultado_ContratadaCod ;
      private int[] P00VV7_A456ContagemResultado_Codigo ;
      private int[] P00VV7_A52Contratada_AreaTrabalhoCod ;
      private bool[] P00VV7_n52Contratada_AreaTrabalhoCod ;
      private String[] P00VV7_A493ContagemResultado_DemandaFM ;
      private bool[] P00VV7_n493ContagemResultado_DemandaFM ;
      private String[] P00VV7_A457ContagemResultado_Demanda ;
      private bool[] P00VV7_n457ContagemResultado_Demanda ;
      private int[] P00VV8_A5AreaTrabalho_Codigo ;
      private String[] P00VV8_A6AreaTrabalho_Descricao ;
      private int[] P00VV9_A57Usuario_PessoaCod ;
      private int[] P00VV9_A1Usuario_Codigo ;
      private String[] P00VV9_A58Usuario_PessoaNom ;
      private bool[] P00VV9_n58Usuario_PessoaNom ;
      private int[] P00VV10_A155Servico_Codigo ;
      private String[] P00VV10_A605Servico_Sigla ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV11Codigos ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV23Area_Codigos ;
      [ObjectCollection(ItemType=typeof( SdtSDT_Codigos ))]
      private IGxCollection AV36SDT_Codigos ;
      private SdtSDT_Codigos AV37Codigo ;
   }

   public class arel_semplanilha__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00VV3( IGxContext context ,
                                             int A456ContagemResultado_Codigo ,
                                             IGxCollection AV11Codigos ,
                                             String A590ContagemResultadoEvidencia_TipoArq )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         Object[] GXv_Object1 ;
         GXv_Object1 = new Object [2] ;
         scmdbuf = "SELECT [ContagemResultadoEvidencia_TipoArq], [ContagemResultado_Codigo], [ContagemResultadoEvidencia_Codigo] FROM [ContagemResultadoEvidencia] WITH (NOLOCK)";
         scmdbuf = scmdbuf + " WHERE (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV11Codigos, "[ContagemResultado_Codigo] IN (", ")") + ")";
         scmdbuf = scmdbuf + " and (LOWER(RTRIM(LTRIM([ContagemResultadoEvidencia_TipoArq]))) = 'xlsx')";
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY [ContagemResultadoEvidencia_Codigo]";
         GXv_Object1[0] = scmdbuf;
         return GXv_Object1 ;
      }

      protected Object[] conditional_P00VV4( IGxContext context ,
                                             int A1109AnexoDe_Id ,
                                             IGxCollection AV11Codigos ,
                                             String A1108Anexo_TipoArq ,
                                             int A1110AnexoDe_Tabela )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         scmdbuf = "SELECT T1.[Anexo_Codigo], T2.[Anexo_TipoArq], T1.[AnexoDe_Tabela], T1.[AnexoDe_Id] FROM ([AnexosDe] T1 WITH (NOLOCK) INNER JOIN [Anexos] T2 WITH (NOLOCK) ON T2.[Anexo_Codigo] = T1.[Anexo_Codigo])";
         scmdbuf = scmdbuf + " WHERE (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV11Codigos, "T1.[AnexoDe_Id] IN (", ")") + ")";
         scmdbuf = scmdbuf + " and (LOWER(RTRIM(LTRIM(T2.[Anexo_TipoArq]))) = 'xlsx')";
         scmdbuf = scmdbuf + " and (T1.[AnexoDe_Tabela] = 1)";
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[Anexo_Codigo], T1.[AnexoDe_Id], T1.[AnexoDe_Tabela]";
         GXv_Object3[0] = scmdbuf;
         return GXv_Object3 ;
      }

      protected Object[] conditional_P00VV5( IGxContext context ,
                                             int A456ContagemResultado_Codigo ,
                                             IGxCollection AV11Codigos )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         Object[] GXv_Object5 ;
         GXv_Object5 = new Object [2] ;
         scmdbuf = "SELECT DISTINCT NULL AS [ContagemResultado_ContratadaCod], NULL AS [ContagemResultado_Codigo], [Contratada_AreaTrabalhoCod] FROM ( SELECT T1.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, T1.[ContagemResultado_Codigo], T2.[Contratada_AreaTrabalhoCod] FROM ([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [Contratada] T2 WITH (NOLOCK) ON T2.[Contratada_Codigo] = T1.[ContagemResultado_ContratadaCod])";
         scmdbuf = scmdbuf + " WHERE (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV11Codigos, "T1.[ContagemResultado_Codigo] IN (", ")") + ")";
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + "";
         scmdbuf = scmdbuf + ") DistinctT";
         GXv_Object5[0] = scmdbuf;
         return GXv_Object5 ;
      }

      protected Object[] conditional_P00VV6( IGxContext context ,
                                             int A5AreaTrabalho_Codigo ,
                                             IGxCollection AV23Area_Codigos )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         Object[] GXv_Object7 ;
         GXv_Object7 = new Object [2] ;
         scmdbuf = "SELECT [AreaTrabalho_Codigo], [AreaTrabalho_Descricao] FROM [AreaTrabalho] WITH (NOLOCK)";
         scmdbuf = scmdbuf + " WHERE (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV23Area_Codigos, "[AreaTrabalho_Codigo] IN (", ")") + ")";
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY [AreaTrabalho_Descricao]";
         GXv_Object7[0] = scmdbuf;
         return GXv_Object7 ;
      }

      protected Object[] conditional_P00VV7( IGxContext context ,
                                             int A456ContagemResultado_Codigo ,
                                             IGxCollection AV11Codigos ,
                                             int A52Contratada_AreaTrabalhoCod ,
                                             int A5AreaTrabalho_Codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int9 ;
         GXv_int9 = new short [1] ;
         Object[] GXv_Object10 ;
         GXv_Object10 = new Object [2] ;
         scmdbuf = "SELECT T1.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, T1.[ContagemResultado_Codigo], T2.[Contratada_AreaTrabalhoCod], T1.[ContagemResultado_DemandaFM], T1.[ContagemResultado_Demanda] FROM ([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [Contratada] T2 WITH (NOLOCK) ON T2.[Contratada_Codigo] = T1.[ContagemResultado_ContratadaCod])";
         scmdbuf = scmdbuf + " WHERE (T2.[Contratada_AreaTrabalhoCod] = @AreaTrabalho_Codigo)";
         scmdbuf = scmdbuf + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV11Codigos, "T1.[ContagemResultado_Codigo] IN (", ")") + ")";
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[ContagemResultado_Codigo]";
         GXv_Object10[0] = scmdbuf;
         GXv_Object10[1] = GXv_int9;
         return GXv_Object10 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 1 :
                     return conditional_P00VV3(context, (int)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] );
               case 2 :
                     return conditional_P00VV4(context, (int)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (int)dynConstraints[3] );
               case 3 :
                     return conditional_P00VV5(context, (int)dynConstraints[0] , (IGxCollection)dynConstraints[1] );
               case 4 :
                     return conditional_P00VV6(context, (int)dynConstraints[0] , (IGxCollection)dynConstraints[1] );
               case 5 :
                     return conditional_P00VV7(context, (int)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (int)dynConstraints[2] , (int)dynConstraints[3] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00VV2 ;
          prmP00VV2 = new Object[] {
          new Object[] {"@AV18Contratada_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00VV8 ;
          prmP00VV8 = new Object[] {
          new Object[] {"@AV32Area",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00VV9 ;
          prmP00VV9 = new Object[] {
          new Object[] {"@AV31Colaborador",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00VV10 ;
          prmP00VV10 = new Object[] {
          new Object[] {"@AV30Servico",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00VV3 ;
          prmP00VV3 = new Object[] {
          } ;
          Object[] prmP00VV4 ;
          prmP00VV4 = new Object[] {
          } ;
          Object[] prmP00VV5 ;
          prmP00VV5 = new Object[] {
          } ;
          Object[] prmP00VV6 ;
          prmP00VV6 = new Object[] {
          } ;
          Object[] prmP00VV7 ;
          prmP00VV7 = new Object[] {
          new Object[] {"@AreaTrabalho_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00VV2", "SELECT [Contratada_Codigo], [Contratada_Logo_GXI], [Contratada_Logo] FROM [Contratada] WITH (NOLOCK) WHERE [Contratada_Codigo] = @AV18Contratada_Codigo ORDER BY [Contratada_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00VV2,1,0,false,true )
             ,new CursorDef("P00VV3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00VV3,100,0,false,false )
             ,new CursorDef("P00VV4", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00VV4,100,0,false,false )
             ,new CursorDef("P00VV5", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00VV5,100,0,false,false )
             ,new CursorDef("P00VV6", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00VV6,100,0,true,false )
             ,new CursorDef("P00VV7", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00VV7,100,0,false,false )
             ,new CursorDef("P00VV8", "SELECT TOP 1 [AreaTrabalho_Codigo], [AreaTrabalho_Descricao] FROM [AreaTrabalho] WITH (NOLOCK) WHERE [AreaTrabalho_Codigo] = @AV32Area ORDER BY [AreaTrabalho_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00VV8,1,0,false,true )
             ,new CursorDef("P00VV9", "SELECT TOP 1 T1.[Usuario_PessoaCod] AS Usuario_PessoaCod, T1.[Usuario_Codigo], T2.[Pessoa_Nome] AS Usuario_PessoaNom FROM ([Usuario] T1 WITH (NOLOCK) INNER JOIN [Pessoa] T2 WITH (NOLOCK) ON T2.[Pessoa_Codigo] = T1.[Usuario_PessoaCod]) WHERE T1.[Usuario_Codigo] = @AV31Colaborador ORDER BY T1.[Usuario_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00VV9,1,0,false,true )
             ,new CursorDef("P00VV10", "SELECT TOP 1 [Servico_Codigo], [Servico_Sigla] FROM [Servico] WITH (NOLOCK) WHERE [Servico_Codigo] = @AV30Servico ORDER BY [Servico_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00VV10,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getMultimediaUri(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getMultimediaFile(3, rslt.getVarchar(2)) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                return;
             case 1 :
                ((String[]) buf[0])[0] = rslt.getString(1, 10) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 10) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getVarchar(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((String[]) buf[7])[0] = rslt.getVarchar(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 100) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 15) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 5 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[1]);
                }
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
