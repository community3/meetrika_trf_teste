/*
               File: SolicitacaoServico
        Description: Solicitacao Servico
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/2/2020 16:18:2.72
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class solicitacaoservico : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"CONTAGEMRESULTADO_NAOCNFDMNCOD") == 0 )
         {
            ajax_req_read_hidden_sdt(GetNextPar( ), AV7WWPContext);
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            GXDLACONTAGEMRESULTADO_NAOCNFDMNCOD4169( AV7WWPContext) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"CONTAGEMRESULTADO_CONTRATADAORIGEMCOD") == 0 )
         {
            ajax_req_read_hidden_sdt(GetNextPar( ), AV7WWPContext);
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            GXDLACONTAGEMRESULTADO_CONTRATADAORIGEMCOD4169( AV7WWPContext) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"CONTAGEMRESULTADO_CONTADORFSCOD") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            GXDLACONTAGEMRESULTADO_CONTADORFSCOD4169( ) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"MODULO_CODIGO") == 0 )
         {
            A489ContagemResultado_SistemaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n489ContagemResultado_SistemaCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A489ContagemResultado_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A489ContagemResultado_SistemaCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            GXDLAMODULO_CODIGO4169( A489ContagemResultado_SistemaCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"CONTAGEMRESULTADO_CONTRATADACOD") == 0 )
         {
            ajax_req_read_hidden_sdt(GetNextPar( ), AV7WWPContext);
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            GXDLACONTAGEMRESULTADO_CONTRATADACOD4169( AV7WWPContext) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_32") == 0 )
         {
            A1636ContagemResultado_ServicoSS = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n1636ContagemResultado_ServicoSS = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1636ContagemResultado_ServicoSS", StringUtil.LTrim( StringUtil.Str( (decimal)(A1636ContagemResultado_ServicoSS), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_32( A1636ContagemResultado_ServicoSS) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_34") == 0 )
         {
            A1553ContagemResultado_CntSrvCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n1553ContagemResultado_CntSrvCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1553ContagemResultado_CntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1553ContagemResultado_CntSrvCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_34( A1553ContagemResultado_CntSrvCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_35") == 0 )
         {
            A602ContagemResultado_OSVinculada = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n602ContagemResultado_OSVinculada = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A602ContagemResultado_OSVinculada", StringUtil.LTrim( StringUtil.Str( (decimal)(A602ContagemResultado_OSVinculada), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_35( A602ContagemResultado_OSVinculada) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_27") == 0 )
         {
            A468ContagemResultado_NaoCnfDmnCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n468ContagemResultado_NaoCnfDmnCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A468ContagemResultado_NaoCnfDmnCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A468ContagemResultado_NaoCnfDmnCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_27( A468ContagemResultado_NaoCnfDmnCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_31") == 0 )
         {
            A597ContagemResultado_LoteAceiteCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n597ContagemResultado_LoteAceiteCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A597ContagemResultado_LoteAceiteCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A597ContagemResultado_LoteAceiteCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_31( A597ContagemResultado_LoteAceiteCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_29") == 0 )
         {
            A805ContagemResultado_ContratadaOrigemCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n805ContagemResultado_ContratadaOrigemCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A805ContagemResultado_ContratadaOrigemCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A805ContagemResultado_ContratadaOrigemCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_29( A805ContagemResultado_ContratadaOrigemCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_25") == 0 )
         {
            A454ContagemResultado_ContadorFSCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n454ContagemResultado_ContadorFSCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A454ContagemResultado_ContadorFSCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A454ContagemResultado_ContadorFSCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_25( A454ContagemResultado_ContadorFSCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_26") == 0 )
         {
            A890ContagemResultado_Responsavel = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n890ContagemResultado_Responsavel = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A890ContagemResultado_Responsavel", StringUtil.LTrim( StringUtil.Str( (decimal)(A890ContagemResultado_Responsavel), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_26( A890ContagemResultado_Responsavel) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_33") == 0 )
         {
            A1043ContagemResultado_LiqLogCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n1043ContagemResultado_LiqLogCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1043ContagemResultado_LiqLogCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1043ContagemResultado_LiqLogCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_33( A1043ContagemResultado_LiqLogCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_24") == 0 )
         {
            A146Modulo_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n146Modulo_Codigo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A146Modulo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A146Modulo_Codigo), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_24( A146Modulo_Codigo) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_28") == 0 )
         {
            A490ContagemResultado_ContratadaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n490ContagemResultado_ContratadaCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A490ContagemResultado_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A490ContagemResultado_ContratadaCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_28( A490ContagemResultado_ContratadaCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_30") == 0 )
         {
            A489ContagemResultado_SistemaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            n489ContagemResultado_SistemaCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A489ContagemResultado_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A489ContagemResultado_SistemaCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_30( A489ContagemResultado_SistemaCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         dynContagemResultado_ServicoSS.Name = "CONTAGEMRESULTADO_SERVICOSS";
         dynContagemResultado_ServicoSS.WebTags = "";
         dynContagemResultado_ServicoSS.removeAllItems();
         /* Using cursor T004116 */
         pr_default.execute(14);
         while ( (pr_default.getStatus(14) != 101) )
         {
            dynContagemResultado_ServicoSS.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(T004116_A155Servico_Codigo[0]), 6, 0)), T004116_A608Servico_Nome[0], 0);
            pr_default.readNext(14);
         }
         pr_default.close(14);
         if ( dynContagemResultado_ServicoSS.ItemCount > 0 )
         {
            A1636ContagemResultado_ServicoSS = (int)(NumberUtil.Val( dynContagemResultado_ServicoSS.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1636ContagemResultado_ServicoSS), 6, 0))), "."));
            n1636ContagemResultado_ServicoSS = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1636ContagemResultado_ServicoSS", StringUtil.LTrim( StringUtil.Str( (decimal)(A1636ContagemResultado_ServicoSS), 6, 0)));
         }
         cmbContagemResultado_Evento.Name = "CONTAGEMRESULTADO_EVENTO";
         cmbContagemResultado_Evento.WebTags = "";
         cmbContagemResultado_Evento.addItem("1", "Servi�o Prim�rio", 0);
         cmbContagemResultado_Evento.addItem("2", "Servico Secund�rio", 0);
         cmbContagemResultado_Evento.addItem("3", "Tarefa", 0);
         cmbContagemResultado_Evento.addItem("4", "Reuni�o", 0);
         if ( cmbContagemResultado_Evento.ItemCount > 0 )
         {
            if ( (0==A1515ContagemResultado_Evento) )
            {
               A1515ContagemResultado_Evento = 1;
               n1515ContagemResultado_Evento = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1515ContagemResultado_Evento", StringUtil.LTrim( StringUtil.Str( (decimal)(A1515ContagemResultado_Evento), 2, 0)));
            }
            A1515ContagemResultado_Evento = (short)(NumberUtil.Val( cmbContagemResultado_Evento.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1515ContagemResultado_Evento), 2, 0))), "."));
            n1515ContagemResultado_Evento = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1515ContagemResultado_Evento", StringUtil.LTrim( StringUtil.Str( (decimal)(A1515ContagemResultado_Evento), 2, 0)));
         }
         cmbContagemResultado_TipoRegistro.Name = "CONTAGEMRESULTADO_TIPOREGISTRO";
         cmbContagemResultado_TipoRegistro.WebTags = "";
         cmbContagemResultado_TipoRegistro.addItem("1", "Ordem de Servi�o", 0);
         cmbContagemResultado_TipoRegistro.addItem("2", "Solicita��o de Servi�o", 0);
         cmbContagemResultado_TipoRegistro.addItem("3", "Reuni�o", 0);
         if ( cmbContagemResultado_TipoRegistro.ItemCount > 0 )
         {
            if ( (0==A1583ContagemResultado_TipoRegistro) )
            {
               A1583ContagemResultado_TipoRegistro = 2;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1583ContagemResultado_TipoRegistro", StringUtil.LTrim( StringUtil.Str( (decimal)(A1583ContagemResultado_TipoRegistro), 4, 0)));
            }
            A1583ContagemResultado_TipoRegistro = (short)(NumberUtil.Val( cmbContagemResultado_TipoRegistro.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1583ContagemResultado_TipoRegistro), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1583ContagemResultado_TipoRegistro", StringUtil.LTrim( StringUtil.Str( (decimal)(A1583ContagemResultado_TipoRegistro), 4, 0)));
         }
         cmbContagemResultado_StatusDmn.Name = "CONTAGEMRESULTADO_STATUSDMN";
         cmbContagemResultado_StatusDmn.WebTags = "";
         cmbContagemResultado_StatusDmn.addItem("B", "Stand by", 0);
         cmbContagemResultado_StatusDmn.addItem("S", "Solicitada", 0);
         cmbContagemResultado_StatusDmn.addItem("E", "Em An�lise", 0);
         cmbContagemResultado_StatusDmn.addItem("A", "Em execu��o", 0);
         cmbContagemResultado_StatusDmn.addItem("R", "Resolvida", 0);
         cmbContagemResultado_StatusDmn.addItem("C", "Conferida", 0);
         cmbContagemResultado_StatusDmn.addItem("D", "Retornada", 0);
         cmbContagemResultado_StatusDmn.addItem("H", "Homologada", 0);
         cmbContagemResultado_StatusDmn.addItem("O", "Aceite", 0);
         cmbContagemResultado_StatusDmn.addItem("P", "A Pagar", 0);
         cmbContagemResultado_StatusDmn.addItem("L", "Liquidada", 0);
         cmbContagemResultado_StatusDmn.addItem("X", "Cancelada", 0);
         cmbContagemResultado_StatusDmn.addItem("N", "N�o Faturada", 0);
         cmbContagemResultado_StatusDmn.addItem("J", "Planejamento", 0);
         cmbContagemResultado_StatusDmn.addItem("I", "An�lise Planejamento", 0);
         cmbContagemResultado_StatusDmn.addItem("T", "Validacao T�cnica", 0);
         cmbContagemResultado_StatusDmn.addItem("Q", "Validacao Qualidade", 0);
         cmbContagemResultado_StatusDmn.addItem("G", "Em Homologa��o", 0);
         cmbContagemResultado_StatusDmn.addItem("M", "Valida��o Mensura��o", 0);
         cmbContagemResultado_StatusDmn.addItem("U", "Rascunho", 0);
         if ( cmbContagemResultado_StatusDmn.ItemCount > 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( A484ContagemResultado_StatusDmn)) )
            {
               A484ContagemResultado_StatusDmn = "S";
               n484ContagemResultado_StatusDmn = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A484ContagemResultado_StatusDmn", A484ContagemResultado_StatusDmn);
            }
            A484ContagemResultado_StatusDmn = cmbContagemResultado_StatusDmn.getValidValue(A484ContagemResultado_StatusDmn);
            n484ContagemResultado_StatusDmn = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A484ContagemResultado_StatusDmn", A484ContagemResultado_StatusDmn);
         }
         dynContagemResultado_NaoCnfDmnCod.Name = "CONTAGEMRESULTADO_NAOCNFDMNCOD";
         dynContagemResultado_NaoCnfDmnCod.WebTags = "";
         dynContagemResultado_LoteAceiteCod.Name = "CONTAGEMRESULTADO_LOTEACEITECOD";
         dynContagemResultado_LoteAceiteCod.WebTags = "";
         dynContagemResultado_LoteAceiteCod.removeAllItems();
         /* Using cursor T004117 */
         pr_default.execute(15);
         while ( (pr_default.getStatus(15) != 101) )
         {
            dynContagemResultado_LoteAceiteCod.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(T004117_A596Lote_Codigo[0]), 6, 0)), T004117_A563Lote_Nome[0], 0);
            pr_default.readNext(15);
         }
         pr_default.close(15);
         if ( dynContagemResultado_LoteAceiteCod.ItemCount > 0 )
         {
            A597ContagemResultado_LoteAceiteCod = (int)(NumberUtil.Val( dynContagemResultado_LoteAceiteCod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A597ContagemResultado_LoteAceiteCod), 6, 0))), "."));
            n597ContagemResultado_LoteAceiteCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A597ContagemResultado_LoteAceiteCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A597ContagemResultado_LoteAceiteCod), 6, 0)));
         }
         dynContagemResultado_ContratadaOrigemCod.Name = "CONTAGEMRESULTADO_CONTRATADAORIGEMCOD";
         dynContagemResultado_ContratadaOrigemCod.WebTags = "";
         dynContagemResultado_ContadorFSCod.Name = "CONTAGEMRESULTADO_CONTADORFSCOD";
         dynContagemResultado_ContadorFSCod.WebTags = "";
         dynModulo_Codigo.Name = "MODULO_CODIGO";
         dynModulo_Codigo.WebTags = "";
         dynContagemResultado_ContratadaCod.Name = "CONTAGEMRESULTADO_CONTRATADACOD";
         dynContagemResultado_ContratadaCod.WebTags = "";
         chkContagemResultado_OSManual.Name = "CONTAGEMRESULTADO_OSMANUAL";
         chkContagemResultado_OSManual.WebTags = "";
         chkContagemResultado_OSManual.Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkContagemResultado_OSManual_Internalname, "TitleCaption", chkContagemResultado_OSManual.Caption);
         chkContagemResultado_OSManual.CheckedValue = "false";
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Solicitacao Servico", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = edtContagemResultado_Codigo_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public solicitacaoservico( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public solicitacaoservico( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         dynContagemResultado_ServicoSS = new GXCombobox();
         cmbContagemResultado_Evento = new GXCombobox();
         cmbContagemResultado_TipoRegistro = new GXCombobox();
         cmbContagemResultado_StatusDmn = new GXCombobox();
         dynContagemResultado_NaoCnfDmnCod = new GXCombobox();
         dynContagemResultado_LoteAceiteCod = new GXCombobox();
         dynContagemResultado_ContratadaOrigemCod = new GXCombobox();
         dynContagemResultado_ContadorFSCod = new GXCombobox();
         dynModulo_Codigo = new GXCombobox();
         dynContagemResultado_ContratadaCod = new GXCombobox();
         chkContagemResultado_OSManual = new GXCheckbox();
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
         if ( dynContagemResultado_ServicoSS.ItemCount > 0 )
         {
            A1636ContagemResultado_ServicoSS = (int)(NumberUtil.Val( dynContagemResultado_ServicoSS.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1636ContagemResultado_ServicoSS), 6, 0))), "."));
            n1636ContagemResultado_ServicoSS = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1636ContagemResultado_ServicoSS", StringUtil.LTrim( StringUtil.Str( (decimal)(A1636ContagemResultado_ServicoSS), 6, 0)));
         }
         if ( cmbContagemResultado_Evento.ItemCount > 0 )
         {
            A1515ContagemResultado_Evento = (short)(NumberUtil.Val( cmbContagemResultado_Evento.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1515ContagemResultado_Evento), 2, 0))), "."));
            n1515ContagemResultado_Evento = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1515ContagemResultado_Evento", StringUtil.LTrim( StringUtil.Str( (decimal)(A1515ContagemResultado_Evento), 2, 0)));
         }
         if ( cmbContagemResultado_TipoRegistro.ItemCount > 0 )
         {
            A1583ContagemResultado_TipoRegistro = (short)(NumberUtil.Val( cmbContagemResultado_TipoRegistro.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1583ContagemResultado_TipoRegistro), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1583ContagemResultado_TipoRegistro", StringUtil.LTrim( StringUtil.Str( (decimal)(A1583ContagemResultado_TipoRegistro), 4, 0)));
         }
         if ( cmbContagemResultado_StatusDmn.ItemCount > 0 )
         {
            A484ContagemResultado_StatusDmn = cmbContagemResultado_StatusDmn.getValidValue(A484ContagemResultado_StatusDmn);
            n484ContagemResultado_StatusDmn = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A484ContagemResultado_StatusDmn", A484ContagemResultado_StatusDmn);
         }
         if ( dynContagemResultado_NaoCnfDmnCod.ItemCount > 0 )
         {
            A468ContagemResultado_NaoCnfDmnCod = (int)(NumberUtil.Val( dynContagemResultado_NaoCnfDmnCod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A468ContagemResultado_NaoCnfDmnCod), 6, 0))), "."));
            n468ContagemResultado_NaoCnfDmnCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A468ContagemResultado_NaoCnfDmnCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A468ContagemResultado_NaoCnfDmnCod), 6, 0)));
         }
         if ( dynContagemResultado_LoteAceiteCod.ItemCount > 0 )
         {
            A597ContagemResultado_LoteAceiteCod = (int)(NumberUtil.Val( dynContagemResultado_LoteAceiteCod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A597ContagemResultado_LoteAceiteCod), 6, 0))), "."));
            n597ContagemResultado_LoteAceiteCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A597ContagemResultado_LoteAceiteCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A597ContagemResultado_LoteAceiteCod), 6, 0)));
         }
         if ( dynContagemResultado_ContratadaOrigemCod.ItemCount > 0 )
         {
            A805ContagemResultado_ContratadaOrigemCod = (int)(NumberUtil.Val( dynContagemResultado_ContratadaOrigemCod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A805ContagemResultado_ContratadaOrigemCod), 6, 0))), "."));
            n805ContagemResultado_ContratadaOrigemCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A805ContagemResultado_ContratadaOrigemCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A805ContagemResultado_ContratadaOrigemCod), 6, 0)));
         }
         if ( dynContagemResultado_ContadorFSCod.ItemCount > 0 )
         {
            A454ContagemResultado_ContadorFSCod = (int)(NumberUtil.Val( dynContagemResultado_ContadorFSCod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A454ContagemResultado_ContadorFSCod), 6, 0))), "."));
            n454ContagemResultado_ContadorFSCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A454ContagemResultado_ContadorFSCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A454ContagemResultado_ContadorFSCod), 6, 0)));
         }
         if ( dynModulo_Codigo.ItemCount > 0 )
         {
            A146Modulo_Codigo = (int)(NumberUtil.Val( dynModulo_Codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A146Modulo_Codigo), 6, 0))), "."));
            n146Modulo_Codigo = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A146Modulo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A146Modulo_Codigo), 6, 0)));
         }
         if ( dynContagemResultado_ContratadaCod.ItemCount > 0 )
         {
            A490ContagemResultado_ContratadaCod = (int)(NumberUtil.Val( dynContagemResultado_ContratadaCod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A490ContagemResultado_ContratadaCod), 6, 0))), "."));
            n490ContagemResultado_ContratadaCod = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A490ContagemResultado_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A490ContagemResultado_ContratadaCod), 6, 0)));
         }
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            wb_table1_2_4169( true) ;
         }
         return  ;
      }

      protected void wb_table1_2_4169e( bool wbgen )
      {
         if ( wbgen )
         {
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void wb_table1_2_4169( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblSolicitacaoservicotitle_Internalname, "Solicitacao Servico", "", "", lblSolicitacaoservicotitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_SolicitacaoServico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_8_4169( true) ;
         }
         return  ;
      }

      protected void wb_table2_8_4169e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_179_4169( true) ;
         }
         return  ;
      }

      protected void wb_table3_179_4169e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_4169e( true) ;
         }
         else
         {
            wb_table1_2_4169e( false) ;
         }
      }

      protected void wb_table3_179_4169( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 182,'',false,'',0)\"";
            ClassString = "btn btn-success";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_enter_Internalname, "", "Confirmar", bttBtn_trn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_trn_enter_Visible, bttBtn_trn_enter_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_SolicitacaoServico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 184,'',false,'',0)\"";
            ClassString = "btn btn-default";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_cancel_Internalname, "", "Fechar", bttBtn_trn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, bttBtn_trn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_SolicitacaoServico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 186,'',false,'',0)\"";
            ClassString = "btn btn-default";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_delete_Internalname, "", "Eliminar", bttBtn_trn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_trn_delete_Visible, bttBtn_trn_delete_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_SolicitacaoServico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_179_4169e( true) ;
         }
         else
         {
            wb_table3_179_4169e( false) ;
         }
      }

      protected void wb_table2_8_4169( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "TableContent", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"DVPANEL_TABLEATTRIBUTESContainer"+"Body"+"\" style=\"display:none;\">") ;
            wb_table4_16_4169( true) ;
         }
         return  ;
      }

      protected void wb_table4_16_4169e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_4169e( true) ;
         }
         else
         {
            wb_table2_8_4169e( false) ;
         }
      }

      protected void wb_table4_16_4169( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableattributes_Internalname, tblTableattributes_Internalname, "", "TableData", 0, "", "", 1, 1, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_codigo_Internalname, "Resultado_Codigo", "", "", lblTextblockcontagemresultado_codigo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_SolicitacaoServico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContagemResultado_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A456ContagemResultado_Codigo), 6, 0, ",", "")), ((edtContagemResultado_Codigo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A456ContagemResultado_Codigo), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A456ContagemResultado_Codigo), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,21);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultado_Codigo_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContagemResultado_Codigo_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_SolicitacaoServico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_owner_Internalname, "Criador", "", "", lblTextblockcontagemresultado_owner_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_SolicitacaoServico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 26,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContagemResultado_Owner_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A508ContagemResultado_Owner), 6, 0, ",", "")), ((edtContagemResultado_Owner_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A508ContagemResultado_Owner), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A508ContagemResultado_Owner), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,26);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultado_Owner_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContagemResultado_Owner_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_SolicitacaoServico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_descricao_Internalname, "Titulo", "", "", lblTextblockcontagemresultado_descricao_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_SolicitacaoServico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 31,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContagemResultado_Descricao_Internalname, A494ContagemResultado_Descricao, StringUtil.RTrim( context.localUtil.Format( A494ContagemResultado_Descricao, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,31);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultado_Descricao_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContagemResultado_Descricao_Enabled, 0, "text", "", 100, "%", 1, "row", 500, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_SolicitacaoServico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_observacao_Internalname, "Observa��o", "", "", lblTextblockcontagemresultado_observacao_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_SolicitacaoServico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"CONTAGEMRESULTADO_OBSERVACAOContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_dataentrega_Internalname, "para Entrega", "", "", lblTextblockcontagemresultado_dataentrega_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_SolicitacaoServico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 41,'',false,'',0)\"";
            context.WriteHtmlText( "<div id=\""+edtContagemResultado_DataEntrega_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtContagemResultado_DataEntrega_Internalname, context.localUtil.Format(A472ContagemResultado_DataEntrega, "99/99/99"), context.localUtil.Format( A472ContagemResultado_DataEntrega, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,41);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultado_DataEntrega_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, edtContagemResultado_DataEntrega_Enabled, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_SolicitacaoServico.htm");
            GxWebStd.gx_bitmap( context, edtContagemResultado_DataEntrega_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(edtContagemResultado_DataEntrega_Enabled==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_SolicitacaoServico.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_horaentrega_Internalname, "de Entrega", "", "", lblTextblockcontagemresultado_horaentrega_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_SolicitacaoServico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 46,'',false,'',0)\"";
            context.WriteHtmlText( "<div id=\""+edtContagemResultado_HoraEntrega_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtContagemResultado_HoraEntrega_Internalname, context.localUtil.TToC( A912ContagemResultado_HoraEntrega, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( A912ContagemResultado_HoraEntrega, "99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 0,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,46);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultado_HoraEntrega_Jsonclick, 0, "BootstrapAttributeDateTime", "", "", "", 1, edtContagemResultado_HoraEntrega_Enabled, 0, "text", "", 5, "chr", 1, "row", 5, 0, 0, 0, 1, -1, 0, true, "Time", "right", false, "HLP_SolicitacaoServico.htm");
            GxWebStd.gx_bitmap( context, edtContagemResultado_HoraEntrega_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(edtContagemResultado_HoraEntrega_Enabled==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_SolicitacaoServico.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_servicoss_Internalname, "solicitado", "", "", lblTextblockcontagemresultado_servicoss_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_SolicitacaoServico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 51,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynContagemResultado_ServicoSS, dynContagemResultado_ServicoSS_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A1636ContagemResultado_ServicoSS), 6, 0)), 1, dynContagemResultado_ServicoSS_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, dynContagemResultado_ServicoSS.Enabled, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,51);\"", "", true, "HLP_SolicitacaoServico.htm");
            dynContagemResultado_ServicoSS.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A1636ContagemResultado_ServicoSS), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynContagemResultado_ServicoSS_Internalname, "Values", (String)(dynContagemResultado_ServicoSS.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_ss_Internalname, "SS", "", "", lblTextblockcontagemresultado_ss_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_SolicitacaoServico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 56,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContagemResultado_SS_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1452ContagemResultado_SS), 8, 0, ",", "")), ((edtContagemResultado_SS_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1452ContagemResultado_SS), "ZZZZZZZ9")) : context.localUtil.Format( (decimal)(A1452ContagemResultado_SS), "ZZZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,56);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultado_SS_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContagemResultado_SS_Enabled, 0, "text", "", 80, "px", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_SolicitacaoServico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_demandafm_Internalname, "OS", "", "", lblTextblockcontagemresultado_demandafm_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_SolicitacaoServico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 61,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContagemResultado_DemandaFM_Internalname, A493ContagemResultado_DemandaFM, StringUtil.RTrim( context.localUtil.Format( A493ContagemResultado_DemandaFM, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,61);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultado_DemandaFM_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContagemResultado_DemandaFM_Enabled, 0, "text", "", 80, "px", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_SolicitacaoServico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_uoowner_Internalname, "Resultado_UOOwner", "", "", lblTextblockcontagemresultado_uoowner_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_SolicitacaoServico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 66,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContagemResultado_UOOwner_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1584ContagemResultado_UOOwner), 6, 0, ",", "")), ((edtContagemResultado_UOOwner_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1584ContagemResultado_UOOwner), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A1584ContagemResultado_UOOwner), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,66);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultado_UOOwner_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContagemResultado_UOOwner_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_SolicitacaoServico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_evento_Internalname, "Evento", "", "", lblTextblockcontagemresultado_evento_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_SolicitacaoServico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 71,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbContagemResultado_Evento, cmbContagemResultado_Evento_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A1515ContagemResultado_Evento), 2, 0)), 1, cmbContagemResultado_Evento_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, cmbContagemResultado_Evento.Enabled, 1, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,71);\"", "", true, "HLP_SolicitacaoServico.htm");
            cmbContagemResultado_Evento.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A1515ContagemResultado_Evento), 2, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContagemResultado_Evento_Internalname, "Values", (String)(cmbContagemResultado_Evento.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_tiporegistro_Internalname, "Resultado_Tipo Registro", "", "", lblTextblockcontagemresultado_tiporegistro_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_SolicitacaoServico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 76,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbContagemResultado_TipoRegistro, cmbContagemResultado_TipoRegistro_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A1583ContagemResultado_TipoRegistro), 4, 0)), 1, cmbContagemResultado_TipoRegistro_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, cmbContagemResultado_TipoRegistro.Enabled, 1, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,76);\"", "", true, "HLP_SolicitacaoServico.htm");
            cmbContagemResultado_TipoRegistro.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A1583ContagemResultado_TipoRegistro), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContagemResultado_TipoRegistro_Internalname, "Values", (String)(cmbContagemResultado_TipoRegistro.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_referencia_Internalname, "Resultado_Referencia", "", "", lblTextblockcontagemresultado_referencia_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_SolicitacaoServico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 81,'',false,'',0)\"";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtContagemResultado_Referencia_Internalname, A1585ContagemResultado_Referencia, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,81);\"", 0, 1, edtContagemResultado_Referencia_Enabled, 0, 80, "chr", 3, "row", StyleString, ClassString, "", "500", 1, "", "", -1, true, "", "HLP_SolicitacaoServico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_restricoes_Internalname, "Resultado_Restricoes", "", "", lblTextblockcontagemresultado_restricoes_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_SolicitacaoServico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 86,'',false,'',0)\"";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtContagemResultado_Restricoes_Internalname, A1586ContagemResultado_Restricoes, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,86);\"", 0, 1, edtContagemResultado_Restricoes_Enabled, 0, 80, "chr", 3, "row", StyleString, ClassString, "", "250", 1, "", "", -1, true, "", "HLP_SolicitacaoServico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_prioridadeprevista_Internalname, "Resultado_Prioridade Prevista", "", "", lblTextblockcontagemresultado_prioridadeprevista_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_SolicitacaoServico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 91,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContagemResultado_PrioridadePrevista_Internalname, A1587ContagemResultado_PrioridadePrevista, StringUtil.RTrim( context.localUtil.Format( A1587ContagemResultado_PrioridadePrevista, "")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,91);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultado_PrioridadePrevista_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContagemResultado_PrioridadePrevista_Enabled, 0, "text", "", 100, "%", 1, "row", 250, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_SolicitacaoServico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_statusdmn_Internalname, "Status", "", "", lblTextblockcontagemresultado_statusdmn_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_SolicitacaoServico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 96,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbContagemResultado_StatusDmn, cmbContagemResultado_StatusDmn_Internalname, StringUtil.RTrim( A484ContagemResultado_StatusDmn), 1, cmbContagemResultado_StatusDmn_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", 1, cmbContagemResultado_StatusDmn.Enabled, 1, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,96);\"", "", true, "HLP_SolicitacaoServico.htm");
            cmbContagemResultado_StatusDmn.CurrentValue = StringUtil.RTrim( A484ContagemResultado_StatusDmn);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContagemResultado_StatusDmn_Internalname, "Values", (String)(cmbContagemResultado_StatusDmn.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_datadmn_Internalname, "da OS", "", "", lblTextblockcontagemresultado_datadmn_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_SolicitacaoServico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 101,'',false,'',0)\"";
            context.WriteHtmlText( "<div id=\""+edtContagemResultado_DataDmn_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtContagemResultado_DataDmn_Internalname, context.localUtil.Format(A471ContagemResultado_DataDmn, "99/99/99"), context.localUtil.Format( A471ContagemResultado_DataDmn, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'por',false,0);"+";gx.evt.onblur(this,101);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultado_DataDmn_Jsonclick, 0, "BootstrapAttributeDate", "", "", "", 1, edtContagemResultado_DataDmn_Enabled, 0, "text", "", 70, "px", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_SolicitacaoServico.htm");
            GxWebStd.gx_bitmap( context, edtContagemResultado_DataDmn_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(edtContagemResultado_DataDmn_Enabled==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_SolicitacaoServico.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_cntsrvcod_Internalname, "no Contrato", "", "", lblTextblockcontagemresultado_cntsrvcod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_SolicitacaoServico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 106,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContagemResultado_CntSrvCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1553ContagemResultado_CntSrvCod), 6, 0, ",", "")), ((edtContagemResultado_CntSrvCod_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1553ContagemResultado_CntSrvCod), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A1553ContagemResultado_CntSrvCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,106);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultado_CntSrvCod_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContagemResultado_CntSrvCod_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_SolicitacaoServico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_osvinculada_Internalname, "vinculada", "", "", lblTextblockcontagemresultado_osvinculada_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_SolicitacaoServico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 111,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContagemResultado_OSVinculada_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A602ContagemResultado_OSVinculada), 6, 0, ",", "")), ((edtContagemResultado_OSVinculada_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A602ContagemResultado_OSVinculada), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A602ContagemResultado_OSVinculada), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,111);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultado_OSVinculada_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContagemResultado_OSVinculada_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_SolicitacaoServico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_naocnfdmncod_Internalname, "Conformidade", "", "", lblTextblockcontagemresultado_naocnfdmncod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_SolicitacaoServico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 116,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynContagemResultado_NaoCnfDmnCod, dynContagemResultado_NaoCnfDmnCod_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A468ContagemResultado_NaoCnfDmnCod), 6, 0)), 1, dynContagemResultado_NaoCnfDmnCod_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, dynContagemResultado_NaoCnfDmnCod.Enabled, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,116);\"", "", true, "HLP_SolicitacaoServico.htm");
            dynContagemResultado_NaoCnfDmnCod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A468ContagemResultado_NaoCnfDmnCod), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynContagemResultado_NaoCnfDmnCod_Internalname, "Values", (String)(dynContagemResultado_NaoCnfDmnCod.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_loteaceitecod_Internalname, "Lote", "", "", lblTextblockcontagemresultado_loteaceitecod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_SolicitacaoServico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 121,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynContagemResultado_LoteAceiteCod, dynContagemResultado_LoteAceiteCod_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A597ContagemResultado_LoteAceiteCod), 6, 0)), 1, dynContagemResultado_LoteAceiteCod_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, dynContagemResultado_LoteAceiteCod.Enabled, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,121);\"", "", true, "HLP_SolicitacaoServico.htm");
            dynContagemResultado_LoteAceiteCod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A597ContagemResultado_LoteAceiteCod), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynContagemResultado_LoteAceiteCod_Internalname, "Values", (String)(dynContagemResultado_LoteAceiteCod.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_contratadaorigemcod_Internalname, "Origem", "", "", lblTextblockcontagemresultado_contratadaorigemcod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_SolicitacaoServico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 126,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynContagemResultado_ContratadaOrigemCod, dynContagemResultado_ContratadaOrigemCod_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A805ContagemResultado_ContratadaOrigemCod), 6, 0)), 1, dynContagemResultado_ContratadaOrigemCod_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, dynContagemResultado_ContratadaOrigemCod.Enabled, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,126);\"", "", true, "HLP_SolicitacaoServico.htm");
            dynContagemResultado_ContratadaOrigemCod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A805ContagemResultado_ContratadaOrigemCod), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynContagemResultado_ContratadaOrigemCod_Internalname, "Values", (String)(dynContagemResultado_ContratadaOrigemCod.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_contadorfscod_Internalname, "Respons�vel", "", "", lblTextblockcontagemresultado_contadorfscod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_SolicitacaoServico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 131,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynContagemResultado_ContadorFSCod, dynContagemResultado_ContadorFSCod_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A454ContagemResultado_ContadorFSCod), 6, 0)), 1, dynContagemResultado_ContadorFSCod_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, dynContagemResultado_ContadorFSCod.Enabled, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,131);\"", "", true, "HLP_SolicitacaoServico.htm");
            dynContagemResultado_ContadorFSCod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A454ContagemResultado_ContadorFSCod), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynContagemResultado_ContadorFSCod_Internalname, "Values", (String)(dynContagemResultado_ContadorFSCod.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_responsavel_Internalname, "Respons�vel", "", "", lblTextblockcontagemresultado_responsavel_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_SolicitacaoServico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 136,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContagemResultado_Responsavel_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A890ContagemResultado_Responsavel), 6, 0, ",", "")), ((edtContagemResultado_Responsavel_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A890ContagemResultado_Responsavel), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A890ContagemResultado_Responsavel), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,136);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultado_Responsavel_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContagemResultado_Responsavel_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_SolicitacaoServico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_liqlogcod_Internalname, "Log Cod", "", "", lblTextblockcontagemresultado_liqlogcod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_SolicitacaoServico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 141,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContagemResultado_LiqLogCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1043ContagemResultado_LiqLogCod), 6, 0, ",", "")), ((edtContagemResultado_LiqLogCod_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1043ContagemResultado_LiqLogCod), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A1043ContagemResultado_LiqLogCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,141);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultado_LiqLogCod_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContagemResultado_LiqLogCod_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_SolicitacaoServico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockmodulo_codigo_Internalname, "M�dulo", "", "", lblTextblockmodulo_codigo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_SolicitacaoServico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 146,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynModulo_Codigo, dynModulo_Codigo_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A146Modulo_Codigo), 6, 0)), 1, dynModulo_Codigo_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, dynModulo_Codigo.Enabled, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,146);\"", "", true, "HLP_SolicitacaoServico.htm");
            dynModulo_Codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A146Modulo_Codigo), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynModulo_Codigo_Internalname, "Values", (String)(dynModulo_Codigo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_contratadacod_Internalname, "Contratada", "", "", lblTextblockcontagemresultado_contratadacod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_SolicitacaoServico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 151,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynContagemResultado_ContratadaCod, dynContagemResultado_ContratadaCod_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A490ContagemResultado_ContratadaCod), 6, 0)), 1, dynContagemResultado_ContratadaCod_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, dynContagemResultado_ContratadaCod.Enabled, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,151);\"", "", true, "HLP_SolicitacaoServico.htm");
            dynContagemResultado_ContratadaCod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A490ContagemResultado_ContratadaCod), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynContagemResultado_ContratadaCod_Internalname, "Values", (String)(dynContagemResultado_ContratadaCod.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_sistemacod_Internalname, "Sistema", "", "", lblTextblockcontagemresultado_sistemacod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_SolicitacaoServico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 156,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContagemResultado_SistemaCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A489ContagemResultado_SistemaCod), 6, 0, ",", "")), ((edtContagemResultado_SistemaCod_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A489ContagemResultado_SistemaCod), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A489ContagemResultado_SistemaCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,156);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultado_SistemaCod_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContagemResultado_SistemaCod_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_SolicitacaoServico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_fncusrcod_Internalname, "de Usu�rio", "", "", lblTextblockcontagemresultado_fncusrcod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_SolicitacaoServico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 161,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContagemResultado_FncUsrCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1044ContagemResultado_FncUsrCod), 6, 0, ",", "")), ((edtContagemResultado_FncUsrCod_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1044ContagemResultado_FncUsrCod), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A1044ContagemResultado_FncUsrCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,161);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultado_FncUsrCod_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContagemResultado_FncUsrCod_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_SolicitacaoServico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_prazoinicialdias_Internalname, "inicial", "", "", lblTextblockcontagemresultado_prazoinicialdias_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_SolicitacaoServico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 166,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContagemResultado_PrazoInicialDias_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1227ContagemResultado_PrazoInicialDias), 4, 0, ",", "")), ((edtContagemResultado_PrazoInicialDias_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1227ContagemResultado_PrazoInicialDias), "ZZZ9")) : context.localUtil.Format( (decimal)(A1227ContagemResultado_PrazoInicialDias), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,166);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultado_PrazoInicialDias_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContagemResultado_PrazoInicialDias_Enabled, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_SolicitacaoServico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_datacadastro_Internalname, "de Cadastro", "", "", lblTextblockcontagemresultado_datacadastro_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_SolicitacaoServico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 171,'',false,'',0)\"";
            context.WriteHtmlText( "<div id=\""+edtContagemResultado_DataCadastro_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtContagemResultado_DataCadastro_Internalname, context.localUtil.TToC( A1350ContagemResultado_DataCadastro, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( A1350ContagemResultado_DataCadastro, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,171);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultado_DataCadastro_Jsonclick, 0, "BootstrapAttributeDateTime", "", "", "", 1, edtContagemResultado_DataCadastro_Enabled, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_SolicitacaoServico.htm");
            GxWebStd.gx_bitmap( context, edtContagemResultado_DataCadastro_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(edtContagemResultado_DataCadastro_Enabled==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_SolicitacaoServico.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_osmanual_Internalname, "manual", "", "", lblTextblockcontagemresultado_osmanual_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_SolicitacaoServico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 176,'',false,'',0)\"";
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkContagemResultado_OSManual_Internalname, StringUtil.BoolToStr( A1173ContagemResultado_OSManual), "", "", 1, chkContagemResultado_OSManual.Enabled, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(176, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,176);\"");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_16_4169e( true) ;
         }
         else
         {
            wb_table4_16_4169e( false) ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11412 */
         E11412 ();
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               if ( ( ( context.localUtil.CToN( cgiGet( edtContagemResultado_Codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContagemResultado_Codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTAGEMRESULTADO_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtContagemResultado_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A456ContagemResultado_Codigo = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A456ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A456ContagemResultado_Codigo), 6, 0)));
               }
               else
               {
                  A456ContagemResultado_Codigo = (int)(context.localUtil.CToN( cgiGet( edtContagemResultado_Codigo_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A456ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A456ContagemResultado_Codigo), 6, 0)));
               }
               if ( ( ( context.localUtil.CToN( cgiGet( edtContagemResultado_Owner_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContagemResultado_Owner_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTAGEMRESULTADO_OWNER");
                  AnyError = 1;
                  GX_FocusControl = edtContagemResultado_Owner_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A508ContagemResultado_Owner = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A508ContagemResultado_Owner", StringUtil.LTrim( StringUtil.Str( (decimal)(A508ContagemResultado_Owner), 6, 0)));
               }
               else
               {
                  A508ContagemResultado_Owner = (int)(context.localUtil.CToN( cgiGet( edtContagemResultado_Owner_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A508ContagemResultado_Owner", StringUtil.LTrim( StringUtil.Str( (decimal)(A508ContagemResultado_Owner), 6, 0)));
               }
               A494ContagemResultado_Descricao = cgiGet( edtContagemResultado_Descricao_Internalname);
               n494ContagemResultado_Descricao = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A494ContagemResultado_Descricao", A494ContagemResultado_Descricao);
               n494ContagemResultado_Descricao = (String.IsNullOrEmpty(StringUtil.RTrim( A494ContagemResultado_Descricao)) ? true : false);
               if ( context.localUtil.VCDate( cgiGet( edtContagemResultado_DataEntrega_Internalname), 2) == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Data para Entrega"}), 1, "CONTAGEMRESULTADO_DATAENTREGA");
                  AnyError = 1;
                  GX_FocusControl = edtContagemResultado_DataEntrega_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A472ContagemResultado_DataEntrega = DateTime.MinValue;
                  n472ContagemResultado_DataEntrega = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A472ContagemResultado_DataEntrega", context.localUtil.Format(A472ContagemResultado_DataEntrega, "99/99/99"));
               }
               else
               {
                  A472ContagemResultado_DataEntrega = context.localUtil.CToD( cgiGet( edtContagemResultado_DataEntrega_Internalname), 2);
                  n472ContagemResultado_DataEntrega = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A472ContagemResultado_DataEntrega", context.localUtil.Format(A472ContagemResultado_DataEntrega, "99/99/99"));
               }
               n472ContagemResultado_DataEntrega = ((DateTime.MinValue==A472ContagemResultado_DataEntrega) ? true : false);
               if ( context.localUtil.VCDate( cgiGet( edtContagemResultado_HoraEntrega_Internalname), 2) == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badtime", new   object[]  {"Hora de Entrega"}), 1, "CONTAGEMRESULTADO_HORAENTREGA");
                  AnyError = 1;
                  GX_FocusControl = edtContagemResultado_HoraEntrega_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A912ContagemResultado_HoraEntrega = (DateTime)(DateTime.MinValue);
                  n912ContagemResultado_HoraEntrega = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A912ContagemResultado_HoraEntrega", context.localUtil.TToC( A912ContagemResultado_HoraEntrega, 0, 5, 0, 3, "/", ":", " "));
               }
               else
               {
                  A912ContagemResultado_HoraEntrega = DateTimeUtil.ResetDate(context.localUtil.CToT( cgiGet( edtContagemResultado_HoraEntrega_Internalname)));
                  n912ContagemResultado_HoraEntrega = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A912ContagemResultado_HoraEntrega", context.localUtil.TToC( A912ContagemResultado_HoraEntrega, 0, 5, 0, 3, "/", ":", " "));
               }
               n912ContagemResultado_HoraEntrega = ((DateTime.MinValue==A912ContagemResultado_HoraEntrega) ? true : false);
               dynContagemResultado_ServicoSS.CurrentValue = cgiGet( dynContagemResultado_ServicoSS_Internalname);
               A1636ContagemResultado_ServicoSS = (int)(NumberUtil.Val( cgiGet( dynContagemResultado_ServicoSS_Internalname), "."));
               n1636ContagemResultado_ServicoSS = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1636ContagemResultado_ServicoSS", StringUtil.LTrim( StringUtil.Str( (decimal)(A1636ContagemResultado_ServicoSS), 6, 0)));
               n1636ContagemResultado_ServicoSS = ((0==A1636ContagemResultado_ServicoSS) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtContagemResultado_SS_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContagemResultado_SS_Internalname), ",", ".") > Convert.ToDecimal( 99999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTAGEMRESULTADO_SS");
                  AnyError = 1;
                  GX_FocusControl = edtContagemResultado_SS_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1452ContagemResultado_SS = 0;
                  n1452ContagemResultado_SS = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1452ContagemResultado_SS", StringUtil.LTrim( StringUtil.Str( (decimal)(A1452ContagemResultado_SS), 8, 0)));
               }
               else
               {
                  A1452ContagemResultado_SS = (int)(context.localUtil.CToN( cgiGet( edtContagemResultado_SS_Internalname), ",", "."));
                  n1452ContagemResultado_SS = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1452ContagemResultado_SS", StringUtil.LTrim( StringUtil.Str( (decimal)(A1452ContagemResultado_SS), 8, 0)));
               }
               n1452ContagemResultado_SS = ((0==A1452ContagemResultado_SS) ? true : false);
               A493ContagemResultado_DemandaFM = cgiGet( edtContagemResultado_DemandaFM_Internalname);
               n493ContagemResultado_DemandaFM = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A493ContagemResultado_DemandaFM", A493ContagemResultado_DemandaFM);
               n493ContagemResultado_DemandaFM = (String.IsNullOrEmpty(StringUtil.RTrim( A493ContagemResultado_DemandaFM)) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtContagemResultado_UOOwner_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContagemResultado_UOOwner_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTAGEMRESULTADO_UOOWNER");
                  AnyError = 1;
                  GX_FocusControl = edtContagemResultado_UOOwner_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1584ContagemResultado_UOOwner = 0;
                  n1584ContagemResultado_UOOwner = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1584ContagemResultado_UOOwner", StringUtil.LTrim( StringUtil.Str( (decimal)(A1584ContagemResultado_UOOwner), 6, 0)));
               }
               else
               {
                  A1584ContagemResultado_UOOwner = (int)(context.localUtil.CToN( cgiGet( edtContagemResultado_UOOwner_Internalname), ",", "."));
                  n1584ContagemResultado_UOOwner = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1584ContagemResultado_UOOwner", StringUtil.LTrim( StringUtil.Str( (decimal)(A1584ContagemResultado_UOOwner), 6, 0)));
               }
               n1584ContagemResultado_UOOwner = ((0==A1584ContagemResultado_UOOwner) ? true : false);
               cmbContagemResultado_Evento.CurrentValue = cgiGet( cmbContagemResultado_Evento_Internalname);
               A1515ContagemResultado_Evento = (short)(NumberUtil.Val( cgiGet( cmbContagemResultado_Evento_Internalname), "."));
               n1515ContagemResultado_Evento = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1515ContagemResultado_Evento", StringUtil.LTrim( StringUtil.Str( (decimal)(A1515ContagemResultado_Evento), 2, 0)));
               n1515ContagemResultado_Evento = ((0==A1515ContagemResultado_Evento) ? true : false);
               cmbContagemResultado_TipoRegistro.CurrentValue = cgiGet( cmbContagemResultado_TipoRegistro_Internalname);
               A1583ContagemResultado_TipoRegistro = (short)(NumberUtil.Val( cgiGet( cmbContagemResultado_TipoRegistro_Internalname), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1583ContagemResultado_TipoRegistro", StringUtil.LTrim( StringUtil.Str( (decimal)(A1583ContagemResultado_TipoRegistro), 4, 0)));
               A1585ContagemResultado_Referencia = cgiGet( edtContagemResultado_Referencia_Internalname);
               n1585ContagemResultado_Referencia = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1585ContagemResultado_Referencia", A1585ContagemResultado_Referencia);
               n1585ContagemResultado_Referencia = (String.IsNullOrEmpty(StringUtil.RTrim( A1585ContagemResultado_Referencia)) ? true : false);
               A1586ContagemResultado_Restricoes = cgiGet( edtContagemResultado_Restricoes_Internalname);
               n1586ContagemResultado_Restricoes = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1586ContagemResultado_Restricoes", A1586ContagemResultado_Restricoes);
               n1586ContagemResultado_Restricoes = (String.IsNullOrEmpty(StringUtil.RTrim( A1586ContagemResultado_Restricoes)) ? true : false);
               A1587ContagemResultado_PrioridadePrevista = cgiGet( edtContagemResultado_PrioridadePrevista_Internalname);
               n1587ContagemResultado_PrioridadePrevista = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1587ContagemResultado_PrioridadePrevista", A1587ContagemResultado_PrioridadePrevista);
               n1587ContagemResultado_PrioridadePrevista = (String.IsNullOrEmpty(StringUtil.RTrim( A1587ContagemResultado_PrioridadePrevista)) ? true : false);
               cmbContagemResultado_StatusDmn.CurrentValue = cgiGet( cmbContagemResultado_StatusDmn_Internalname);
               A484ContagemResultado_StatusDmn = cgiGet( cmbContagemResultado_StatusDmn_Internalname);
               n484ContagemResultado_StatusDmn = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A484ContagemResultado_StatusDmn", A484ContagemResultado_StatusDmn);
               n484ContagemResultado_StatusDmn = (String.IsNullOrEmpty(StringUtil.RTrim( A484ContagemResultado_StatusDmn)) ? true : false);
               if ( context.localUtil.VCDate( cgiGet( edtContagemResultado_DataDmn_Internalname), 2) == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Data da OS"}), 1, "CONTAGEMRESULTADO_DATADMN");
                  AnyError = 1;
                  GX_FocusControl = edtContagemResultado_DataDmn_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A471ContagemResultado_DataDmn = DateTime.MinValue;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A471ContagemResultado_DataDmn", context.localUtil.Format(A471ContagemResultado_DataDmn, "99/99/99"));
               }
               else
               {
                  A471ContagemResultado_DataDmn = context.localUtil.CToD( cgiGet( edtContagemResultado_DataDmn_Internalname), 2);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A471ContagemResultado_DataDmn", context.localUtil.Format(A471ContagemResultado_DataDmn, "99/99/99"));
               }
               if ( ( ( context.localUtil.CToN( cgiGet( edtContagemResultado_CntSrvCod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContagemResultado_CntSrvCod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTAGEMRESULTADO_CNTSRVCOD");
                  AnyError = 1;
                  GX_FocusControl = edtContagemResultado_CntSrvCod_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1553ContagemResultado_CntSrvCod = 0;
                  n1553ContagemResultado_CntSrvCod = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1553ContagemResultado_CntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1553ContagemResultado_CntSrvCod), 6, 0)));
               }
               else
               {
                  A1553ContagemResultado_CntSrvCod = (int)(context.localUtil.CToN( cgiGet( edtContagemResultado_CntSrvCod_Internalname), ",", "."));
                  n1553ContagemResultado_CntSrvCod = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1553ContagemResultado_CntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1553ContagemResultado_CntSrvCod), 6, 0)));
               }
               n1553ContagemResultado_CntSrvCod = ((0==A1553ContagemResultado_CntSrvCod) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtContagemResultado_OSVinculada_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContagemResultado_OSVinculada_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTAGEMRESULTADO_OSVINCULADA");
                  AnyError = 1;
                  GX_FocusControl = edtContagemResultado_OSVinculada_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A602ContagemResultado_OSVinculada = 0;
                  n602ContagemResultado_OSVinculada = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A602ContagemResultado_OSVinculada", StringUtil.LTrim( StringUtil.Str( (decimal)(A602ContagemResultado_OSVinculada), 6, 0)));
               }
               else
               {
                  A602ContagemResultado_OSVinculada = (int)(context.localUtil.CToN( cgiGet( edtContagemResultado_OSVinculada_Internalname), ",", "."));
                  n602ContagemResultado_OSVinculada = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A602ContagemResultado_OSVinculada", StringUtil.LTrim( StringUtil.Str( (decimal)(A602ContagemResultado_OSVinculada), 6, 0)));
               }
               n602ContagemResultado_OSVinculada = ((0==A602ContagemResultado_OSVinculada) ? true : false);
               dynContagemResultado_NaoCnfDmnCod.CurrentValue = cgiGet( dynContagemResultado_NaoCnfDmnCod_Internalname);
               A468ContagemResultado_NaoCnfDmnCod = (int)(NumberUtil.Val( cgiGet( dynContagemResultado_NaoCnfDmnCod_Internalname), "."));
               n468ContagemResultado_NaoCnfDmnCod = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A468ContagemResultado_NaoCnfDmnCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A468ContagemResultado_NaoCnfDmnCod), 6, 0)));
               n468ContagemResultado_NaoCnfDmnCod = ((0==A468ContagemResultado_NaoCnfDmnCod) ? true : false);
               dynContagemResultado_LoteAceiteCod.CurrentValue = cgiGet( dynContagemResultado_LoteAceiteCod_Internalname);
               A597ContagemResultado_LoteAceiteCod = (int)(NumberUtil.Val( cgiGet( dynContagemResultado_LoteAceiteCod_Internalname), "."));
               n597ContagemResultado_LoteAceiteCod = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A597ContagemResultado_LoteAceiteCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A597ContagemResultado_LoteAceiteCod), 6, 0)));
               n597ContagemResultado_LoteAceiteCod = ((0==A597ContagemResultado_LoteAceiteCod) ? true : false);
               dynContagemResultado_ContratadaOrigemCod.CurrentValue = cgiGet( dynContagemResultado_ContratadaOrigemCod_Internalname);
               A805ContagemResultado_ContratadaOrigemCod = (int)(NumberUtil.Val( cgiGet( dynContagemResultado_ContratadaOrigemCod_Internalname), "."));
               n805ContagemResultado_ContratadaOrigemCod = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A805ContagemResultado_ContratadaOrigemCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A805ContagemResultado_ContratadaOrigemCod), 6, 0)));
               n805ContagemResultado_ContratadaOrigemCod = ((0==A805ContagemResultado_ContratadaOrigemCod) ? true : false);
               dynContagemResultado_ContadorFSCod.CurrentValue = cgiGet( dynContagemResultado_ContadorFSCod_Internalname);
               A454ContagemResultado_ContadorFSCod = (int)(NumberUtil.Val( cgiGet( dynContagemResultado_ContadorFSCod_Internalname), "."));
               n454ContagemResultado_ContadorFSCod = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A454ContagemResultado_ContadorFSCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A454ContagemResultado_ContadorFSCod), 6, 0)));
               n454ContagemResultado_ContadorFSCod = ((0==A454ContagemResultado_ContadorFSCod) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtContagemResultado_Responsavel_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContagemResultado_Responsavel_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTAGEMRESULTADO_RESPONSAVEL");
                  AnyError = 1;
                  GX_FocusControl = edtContagemResultado_Responsavel_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A890ContagemResultado_Responsavel = 0;
                  n890ContagemResultado_Responsavel = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A890ContagemResultado_Responsavel", StringUtil.LTrim( StringUtil.Str( (decimal)(A890ContagemResultado_Responsavel), 6, 0)));
               }
               else
               {
                  A890ContagemResultado_Responsavel = (int)(context.localUtil.CToN( cgiGet( edtContagemResultado_Responsavel_Internalname), ",", "."));
                  n890ContagemResultado_Responsavel = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A890ContagemResultado_Responsavel", StringUtil.LTrim( StringUtil.Str( (decimal)(A890ContagemResultado_Responsavel), 6, 0)));
               }
               n890ContagemResultado_Responsavel = ((0==A890ContagemResultado_Responsavel) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtContagemResultado_LiqLogCod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContagemResultado_LiqLogCod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTAGEMRESULTADO_LIQLOGCOD");
                  AnyError = 1;
                  GX_FocusControl = edtContagemResultado_LiqLogCod_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1043ContagemResultado_LiqLogCod = 0;
                  n1043ContagemResultado_LiqLogCod = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1043ContagemResultado_LiqLogCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1043ContagemResultado_LiqLogCod), 6, 0)));
               }
               else
               {
                  A1043ContagemResultado_LiqLogCod = (int)(context.localUtil.CToN( cgiGet( edtContagemResultado_LiqLogCod_Internalname), ",", "."));
                  n1043ContagemResultado_LiqLogCod = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1043ContagemResultado_LiqLogCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1043ContagemResultado_LiqLogCod), 6, 0)));
               }
               n1043ContagemResultado_LiqLogCod = ((0==A1043ContagemResultado_LiqLogCod) ? true : false);
               dynModulo_Codigo.CurrentValue = cgiGet( dynModulo_Codigo_Internalname);
               A146Modulo_Codigo = (int)(NumberUtil.Val( cgiGet( dynModulo_Codigo_Internalname), "."));
               n146Modulo_Codigo = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A146Modulo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A146Modulo_Codigo), 6, 0)));
               n146Modulo_Codigo = ((0==A146Modulo_Codigo) ? true : false);
               dynContagemResultado_ContratadaCod.CurrentValue = cgiGet( dynContagemResultado_ContratadaCod_Internalname);
               A490ContagemResultado_ContratadaCod = (int)(NumberUtil.Val( cgiGet( dynContagemResultado_ContratadaCod_Internalname), "."));
               n490ContagemResultado_ContratadaCod = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A490ContagemResultado_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A490ContagemResultado_ContratadaCod), 6, 0)));
               n490ContagemResultado_ContratadaCod = ((0==A490ContagemResultado_ContratadaCod) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtContagemResultado_SistemaCod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContagemResultado_SistemaCod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTAGEMRESULTADO_SISTEMACOD");
                  AnyError = 1;
                  GX_FocusControl = edtContagemResultado_SistemaCod_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A489ContagemResultado_SistemaCod = 0;
                  n489ContagemResultado_SistemaCod = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A489ContagemResultado_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A489ContagemResultado_SistemaCod), 6, 0)));
               }
               else
               {
                  A489ContagemResultado_SistemaCod = (int)(context.localUtil.CToN( cgiGet( edtContagemResultado_SistemaCod_Internalname), ",", "."));
                  n489ContagemResultado_SistemaCod = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A489ContagemResultado_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A489ContagemResultado_SistemaCod), 6, 0)));
               }
               n489ContagemResultado_SistemaCod = ((0==A489ContagemResultado_SistemaCod) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtContagemResultado_FncUsrCod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContagemResultado_FncUsrCod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTAGEMRESULTADO_FNCUSRCOD");
                  AnyError = 1;
                  GX_FocusControl = edtContagemResultado_FncUsrCod_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1044ContagemResultado_FncUsrCod = 0;
                  n1044ContagemResultado_FncUsrCod = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1044ContagemResultado_FncUsrCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1044ContagemResultado_FncUsrCod), 6, 0)));
               }
               else
               {
                  A1044ContagemResultado_FncUsrCod = (int)(context.localUtil.CToN( cgiGet( edtContagemResultado_FncUsrCod_Internalname), ",", "."));
                  n1044ContagemResultado_FncUsrCod = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1044ContagemResultado_FncUsrCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1044ContagemResultado_FncUsrCod), 6, 0)));
               }
               n1044ContagemResultado_FncUsrCod = ((0==A1044ContagemResultado_FncUsrCod) ? true : false);
               if ( ( ( context.localUtil.CToN( cgiGet( edtContagemResultado_PrazoInicialDias_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContagemResultado_PrazoInicialDias_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTAGEMRESULTADO_PRAZOINICIALDIAS");
                  AnyError = 1;
                  GX_FocusControl = edtContagemResultado_PrazoInicialDias_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1227ContagemResultado_PrazoInicialDias = 0;
                  n1227ContagemResultado_PrazoInicialDias = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1227ContagemResultado_PrazoInicialDias", StringUtil.LTrim( StringUtil.Str( (decimal)(A1227ContagemResultado_PrazoInicialDias), 4, 0)));
               }
               else
               {
                  A1227ContagemResultado_PrazoInicialDias = (short)(context.localUtil.CToN( cgiGet( edtContagemResultado_PrazoInicialDias_Internalname), ",", "."));
                  n1227ContagemResultado_PrazoInicialDias = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1227ContagemResultado_PrazoInicialDias", StringUtil.LTrim( StringUtil.Str( (decimal)(A1227ContagemResultado_PrazoInicialDias), 4, 0)));
               }
               n1227ContagemResultado_PrazoInicialDias = ((0==A1227ContagemResultado_PrazoInicialDias) ? true : false);
               if ( context.localUtil.VCDateTime( cgiGet( edtContagemResultado_DataCadastro_Internalname), 2, 0) == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"Data de Cadastro"}), 1, "CONTAGEMRESULTADO_DATACADASTRO");
                  AnyError = 1;
                  GX_FocusControl = edtContagemResultado_DataCadastro_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1350ContagemResultado_DataCadastro = (DateTime)(DateTime.MinValue);
                  n1350ContagemResultado_DataCadastro = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1350ContagemResultado_DataCadastro", context.localUtil.TToC( A1350ContagemResultado_DataCadastro, 8, 5, 0, 3, "/", ":", " "));
               }
               else
               {
                  A1350ContagemResultado_DataCadastro = context.localUtil.CToT( cgiGet( edtContagemResultado_DataCadastro_Internalname));
                  n1350ContagemResultado_DataCadastro = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1350ContagemResultado_DataCadastro", context.localUtil.TToC( A1350ContagemResultado_DataCadastro, 8, 5, 0, 3, "/", ":", " "));
               }
               n1350ContagemResultado_DataCadastro = ((DateTime.MinValue==A1350ContagemResultado_DataCadastro) ? true : false);
               A1173ContagemResultado_OSManual = StringUtil.StrToBool( cgiGet( chkContagemResultado_OSManual_Internalname));
               n1173ContagemResultado_OSManual = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1173ContagemResultado_OSManual", A1173ContagemResultado_OSManual);
               n1173ContagemResultado_OSManual = ((false==A1173ContagemResultado_OSManual) ? true : false);
               /* Read saved values. */
               Z456ContagemResultado_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z456ContagemResultado_Codigo"), ",", "."));
               Z508ContagemResultado_Owner = (int)(context.localUtil.CToN( cgiGet( "Z508ContagemResultado_Owner"), ",", "."));
               Z494ContagemResultado_Descricao = cgiGet( "Z494ContagemResultado_Descricao");
               n494ContagemResultado_Descricao = (String.IsNullOrEmpty(StringUtil.RTrim( A494ContagemResultado_Descricao)) ? true : false);
               Z472ContagemResultado_DataEntrega = context.localUtil.CToD( cgiGet( "Z472ContagemResultado_DataEntrega"), 0);
               n472ContagemResultado_DataEntrega = ((DateTime.MinValue==A472ContagemResultado_DataEntrega) ? true : false);
               Z912ContagemResultado_HoraEntrega = DateTimeUtil.ResetDate(context.localUtil.CToT( cgiGet( "Z912ContagemResultado_HoraEntrega"), 0));
               n912ContagemResultado_HoraEntrega = ((DateTime.MinValue==A912ContagemResultado_HoraEntrega) ? true : false);
               Z1351ContagemResultado_DataPrevista = context.localUtil.CToT( cgiGet( "Z1351ContagemResultado_DataPrevista"), 0);
               n1351ContagemResultado_DataPrevista = ((DateTime.MinValue==A1351ContagemResultado_DataPrevista) ? true : false);
               Z1452ContagemResultado_SS = (int)(context.localUtil.CToN( cgiGet( "Z1452ContagemResultado_SS"), ",", "."));
               n1452ContagemResultado_SS = ((0==A1452ContagemResultado_SS) ? true : false);
               Z493ContagemResultado_DemandaFM = cgiGet( "Z493ContagemResultado_DemandaFM");
               n493ContagemResultado_DemandaFM = (String.IsNullOrEmpty(StringUtil.RTrim( A493ContagemResultado_DemandaFM)) ? true : false);
               Z1584ContagemResultado_UOOwner = (int)(context.localUtil.CToN( cgiGet( "Z1584ContagemResultado_UOOwner"), ",", "."));
               n1584ContagemResultado_UOOwner = ((0==A1584ContagemResultado_UOOwner) ? true : false);
               Z1515ContagemResultado_Evento = (short)(context.localUtil.CToN( cgiGet( "Z1515ContagemResultado_Evento"), ",", "."));
               n1515ContagemResultado_Evento = ((0==A1515ContagemResultado_Evento) ? true : false);
               Z1583ContagemResultado_TipoRegistro = (short)(context.localUtil.CToN( cgiGet( "Z1583ContagemResultado_TipoRegistro"), ",", "."));
               Z1585ContagemResultado_Referencia = cgiGet( "Z1585ContagemResultado_Referencia");
               n1585ContagemResultado_Referencia = (String.IsNullOrEmpty(StringUtil.RTrim( A1585ContagemResultado_Referencia)) ? true : false);
               Z1586ContagemResultado_Restricoes = cgiGet( "Z1586ContagemResultado_Restricoes");
               n1586ContagemResultado_Restricoes = (String.IsNullOrEmpty(StringUtil.RTrim( A1586ContagemResultado_Restricoes)) ? true : false);
               Z1587ContagemResultado_PrioridadePrevista = cgiGet( "Z1587ContagemResultado_PrioridadePrevista");
               n1587ContagemResultado_PrioridadePrevista = (String.IsNullOrEmpty(StringUtil.RTrim( A1587ContagemResultado_PrioridadePrevista)) ? true : false);
               Z484ContagemResultado_StatusDmn = cgiGet( "Z484ContagemResultado_StatusDmn");
               n484ContagemResultado_StatusDmn = (String.IsNullOrEmpty(StringUtil.RTrim( A484ContagemResultado_StatusDmn)) ? true : false);
               Z471ContagemResultado_DataDmn = context.localUtil.CToD( cgiGet( "Z471ContagemResultado_DataDmn"), 0);
               Z1044ContagemResultado_FncUsrCod = (int)(context.localUtil.CToN( cgiGet( "Z1044ContagemResultado_FncUsrCod"), ",", "."));
               n1044ContagemResultado_FncUsrCod = ((0==A1044ContagemResultado_FncUsrCod) ? true : false);
               Z1227ContagemResultado_PrazoInicialDias = (short)(context.localUtil.CToN( cgiGet( "Z1227ContagemResultado_PrazoInicialDias"), ",", "."));
               n1227ContagemResultado_PrazoInicialDias = ((0==A1227ContagemResultado_PrazoInicialDias) ? true : false);
               Z1350ContagemResultado_DataCadastro = context.localUtil.CToT( cgiGet( "Z1350ContagemResultado_DataCadastro"), 0);
               n1350ContagemResultado_DataCadastro = ((DateTime.MinValue==A1350ContagemResultado_DataCadastro) ? true : false);
               Z1173ContagemResultado_OSManual = StringUtil.StrToBool( cgiGet( "Z1173ContagemResultado_OSManual"));
               n1173ContagemResultado_OSManual = ((false==A1173ContagemResultado_OSManual) ? true : false);
               Z598ContagemResultado_Baseline = StringUtil.StrToBool( cgiGet( "Z598ContagemResultado_Baseline"));
               n598ContagemResultado_Baseline = ((false==A598ContagemResultado_Baseline) ? true : false);
               Z485ContagemResultado_EhValidacao = StringUtil.StrToBool( cgiGet( "Z485ContagemResultado_EhValidacao"));
               n485ContagemResultado_EhValidacao = ((false==A485ContagemResultado_EhValidacao) ? true : false);
               Z146Modulo_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z146Modulo_Codigo"), ",", "."));
               n146Modulo_Codigo = ((0==A146Modulo_Codigo) ? true : false);
               Z454ContagemResultado_ContadorFSCod = (int)(context.localUtil.CToN( cgiGet( "Z454ContagemResultado_ContadorFSCod"), ",", "."));
               n454ContagemResultado_ContadorFSCod = ((0==A454ContagemResultado_ContadorFSCod) ? true : false);
               Z890ContagemResultado_Responsavel = (int)(context.localUtil.CToN( cgiGet( "Z890ContagemResultado_Responsavel"), ",", "."));
               n890ContagemResultado_Responsavel = ((0==A890ContagemResultado_Responsavel) ? true : false);
               Z468ContagemResultado_NaoCnfDmnCod = (int)(context.localUtil.CToN( cgiGet( "Z468ContagemResultado_NaoCnfDmnCod"), ",", "."));
               n468ContagemResultado_NaoCnfDmnCod = ((0==A468ContagemResultado_NaoCnfDmnCod) ? true : false);
               Z490ContagemResultado_ContratadaCod = (int)(context.localUtil.CToN( cgiGet( "Z490ContagemResultado_ContratadaCod"), ",", "."));
               n490ContagemResultado_ContratadaCod = ((0==A490ContagemResultado_ContratadaCod) ? true : false);
               Z805ContagemResultado_ContratadaOrigemCod = (int)(context.localUtil.CToN( cgiGet( "Z805ContagemResultado_ContratadaOrigemCod"), ",", "."));
               n805ContagemResultado_ContratadaOrigemCod = ((0==A805ContagemResultado_ContratadaOrigemCod) ? true : false);
               Z489ContagemResultado_SistemaCod = (int)(context.localUtil.CToN( cgiGet( "Z489ContagemResultado_SistemaCod"), ",", "."));
               n489ContagemResultado_SistemaCod = ((0==A489ContagemResultado_SistemaCod) ? true : false);
               Z597ContagemResultado_LoteAceiteCod = (int)(context.localUtil.CToN( cgiGet( "Z597ContagemResultado_LoteAceiteCod"), ",", "."));
               n597ContagemResultado_LoteAceiteCod = ((0==A597ContagemResultado_LoteAceiteCod) ? true : false);
               Z1636ContagemResultado_ServicoSS = (int)(context.localUtil.CToN( cgiGet( "Z1636ContagemResultado_ServicoSS"), ",", "."));
               n1636ContagemResultado_ServicoSS = ((0==A1636ContagemResultado_ServicoSS) ? true : false);
               Z1043ContagemResultado_LiqLogCod = (int)(context.localUtil.CToN( cgiGet( "Z1043ContagemResultado_LiqLogCod"), ",", "."));
               n1043ContagemResultado_LiqLogCod = ((0==A1043ContagemResultado_LiqLogCod) ? true : false);
               Z1553ContagemResultado_CntSrvCod = (int)(context.localUtil.CToN( cgiGet( "Z1553ContagemResultado_CntSrvCod"), ",", "."));
               n1553ContagemResultado_CntSrvCod = ((0==A1553ContagemResultado_CntSrvCod) ? true : false);
               Z602ContagemResultado_OSVinculada = (int)(context.localUtil.CToN( cgiGet( "Z602ContagemResultado_OSVinculada"), ",", "."));
               n602ContagemResultado_OSVinculada = ((0==A602ContagemResultado_OSVinculada) ? true : false);
               A1351ContagemResultado_DataPrevista = context.localUtil.CToT( cgiGet( "Z1351ContagemResultado_DataPrevista"), 0);
               n1351ContagemResultado_DataPrevista = false;
               n1351ContagemResultado_DataPrevista = ((DateTime.MinValue==A1351ContagemResultado_DataPrevista) ? true : false);
               A598ContagemResultado_Baseline = StringUtil.StrToBool( cgiGet( "Z598ContagemResultado_Baseline"));
               n598ContagemResultado_Baseline = false;
               n598ContagemResultado_Baseline = ((false==A598ContagemResultado_Baseline) ? true : false);
               A485ContagemResultado_EhValidacao = StringUtil.StrToBool( cgiGet( "Z485ContagemResultado_EhValidacao"));
               n485ContagemResultado_EhValidacao = false;
               n485ContagemResultado_EhValidacao = ((false==A485ContagemResultado_EhValidacao) ? true : false);
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
               Gx_mode = cgiGet( "Mode");
               Gx_BScreen = (short)(context.localUtil.CToN( cgiGet( "vGXBSCREEN"), ",", "."));
               A598ContagemResultado_Baseline = StringUtil.StrToBool( cgiGet( "CONTAGEMRESULTADO_BASELINE"));
               n598ContagemResultado_Baseline = ((false==A598ContagemResultado_Baseline) ? true : false);
               A485ContagemResultado_EhValidacao = StringUtil.StrToBool( cgiGet( "CONTAGEMRESULTADO_EHVALIDACAO"));
               n485ContagemResultado_EhValidacao = ((false==A485ContagemResultado_EhValidacao) ? true : false);
               Gx_date = context.localUtil.CToD( cgiGet( "vTODAY"), 0);
               ajax_req_read_hidden_sdt(cgiGet( "vWWPCONTEXT"), AV7WWPContext);
               A514ContagemResultado_Observacao = cgiGet( "CONTAGEMRESULTADO_OBSERVACAO");
               n514ContagemResultado_Observacao = false;
               n514ContagemResultado_Observacao = (String.IsNullOrEmpty(StringUtil.RTrim( A514ContagemResultado_Observacao)) ? true : false);
               A1351ContagemResultado_DataPrevista = context.localUtil.CToT( cgiGet( "CONTAGEMRESULTADO_DATAPREVISTA"), 0);
               n1351ContagemResultado_DataPrevista = ((DateTime.MinValue==A1351ContagemResultado_DataPrevista) ? true : false);
               AV26Pgmname = cgiGet( "vPGMNAME");
               Gx_mode = cgiGet( "vMODE");
               Contagemresultado_observacao_Width = cgiGet( "CONTAGEMRESULTADO_OBSERVACAO_Width");
               Contagemresultado_observacao_Height = cgiGet( "CONTAGEMRESULTADO_OBSERVACAO_Height");
               Contagemresultado_observacao_Skin = cgiGet( "CONTAGEMRESULTADO_OBSERVACAO_Skin");
               Contagemresultado_observacao_Toolbar = cgiGet( "CONTAGEMRESULTADO_OBSERVACAO_Toolbar");
               Contagemresultado_observacao_Color = (int)(context.localUtil.CToN( cgiGet( "CONTAGEMRESULTADO_OBSERVACAO_Color"), ",", "."));
               Contagemresultado_observacao_Enabled = StringUtil.StrToBool( cgiGet( "CONTAGEMRESULTADO_OBSERVACAO_Enabled"));
               Contagemresultado_observacao_Class = cgiGet( "CONTAGEMRESULTADO_OBSERVACAO_Class");
               Contagemresultado_observacao_Customtoolbar = cgiGet( "CONTAGEMRESULTADO_OBSERVACAO_Customtoolbar");
               Contagemresultado_observacao_Customconfiguration = cgiGet( "CONTAGEMRESULTADO_OBSERVACAO_Customconfiguration");
               Contagemresultado_observacao_Toolbarcancollapse = StringUtil.StrToBool( cgiGet( "CONTAGEMRESULTADO_OBSERVACAO_Toolbarcancollapse"));
               Contagemresultado_observacao_Toolbarexpanded = StringUtil.StrToBool( cgiGet( "CONTAGEMRESULTADO_OBSERVACAO_Toolbarexpanded"));
               Contagemresultado_observacao_Buttonpressedid = cgiGet( "CONTAGEMRESULTADO_OBSERVACAO_Buttonpressedid");
               Contagemresultado_observacao_Captionvalue = cgiGet( "CONTAGEMRESULTADO_OBSERVACAO_Captionvalue");
               Contagemresultado_observacao_Captionclass = cgiGet( "CONTAGEMRESULTADO_OBSERVACAO_Captionclass");
               Contagemresultado_observacao_Captionposition = cgiGet( "CONTAGEMRESULTADO_OBSERVACAO_Captionposition");
               Contagemresultado_observacao_Coltitle = cgiGet( "CONTAGEMRESULTADO_OBSERVACAO_Coltitle");
               Contagemresultado_observacao_Coltitlefont = cgiGet( "CONTAGEMRESULTADO_OBSERVACAO_Coltitlefont");
               Contagemresultado_observacao_Coltitlecolor = (int)(context.localUtil.CToN( cgiGet( "CONTAGEMRESULTADO_OBSERVACAO_Coltitlecolor"), ",", "."));
               Contagemresultado_observacao_Usercontroliscolumn = StringUtil.StrToBool( cgiGet( "CONTAGEMRESULTADO_OBSERVACAO_Usercontroliscolumn"));
               Contagemresultado_observacao_Visible = StringUtil.StrToBool( cgiGet( "CONTAGEMRESULTADO_OBSERVACAO_Visible"));
               Dvpanel_tableattributes_Width = cgiGet( "DVPANEL_TABLEATTRIBUTES_Width");
               Dvpanel_tableattributes_Height = cgiGet( "DVPANEL_TABLEATTRIBUTES_Height");
               Dvpanel_tableattributes_Cls = cgiGet( "DVPANEL_TABLEATTRIBUTES_Cls");
               Dvpanel_tableattributes_Title = cgiGet( "DVPANEL_TABLEATTRIBUTES_Title");
               Dvpanel_tableattributes_Collapsible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsible"));
               Dvpanel_tableattributes_Collapsed = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Collapsed"));
               Dvpanel_tableattributes_Enabled = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Enabled"));
               Dvpanel_tableattributes_Class = cgiGet( "DVPANEL_TABLEATTRIBUTES_Class");
               Dvpanel_tableattributes_Autowidth = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autowidth"));
               Dvpanel_tableattributes_Autoheight = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoheight"));
               Dvpanel_tableattributes_Showheader = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showheader"));
               Dvpanel_tableattributes_Showcollapseicon = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Showcollapseicon"));
               Dvpanel_tableattributes_Iconposition = cgiGet( "DVPANEL_TABLEATTRIBUTES_Iconposition");
               Dvpanel_tableattributes_Autoscroll = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Autoscroll"));
               Dvpanel_tableattributes_Visible = StringUtil.StrToBool( cgiGet( "DVPANEL_TABLEATTRIBUTES_Visible"));
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               forbiddenHiddens = "hsh" + "SolicitacaoServico";
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( A1351ContagemResultado_DataPrevista, "99/99/99 99:99");
               forbiddenHiddens = forbiddenHiddens + StringUtil.BoolToStr( A598ContagemResultado_Baseline);
               forbiddenHiddens = forbiddenHiddens + StringUtil.BoolToStr( A485ContagemResultado_EhValidacao);
               hsh = cgiGet( "hsh");
               if ( ( ! ( ( A456ContagemResultado_Codigo != Z456ContagemResultado_Codigo ) ) || ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ) && ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
               {
                  GXUtil.WriteLog("solicitacaoservico:[SecurityCheckFailed value for]"+"ContagemResultado_DataPrevista:"+context.localUtil.Format( A1351ContagemResultado_DataPrevista, "99/99/99 99:99"));
                  GXUtil.WriteLog("solicitacaoservico:[SecurityCheckFailed value for]"+"ContagemResultado_Baseline:"+StringUtil.BoolToStr( A598ContagemResultado_Baseline));
                  GXUtil.WriteLog("solicitacaoservico:[SecurityCheckFailed value for]"+"ContagemResultado_EhValidacao:"+StringUtil.BoolToStr( A485ContagemResultado_EhValidacao));
                  GxWebError = 1;
                  context.HttpContext.Response.StatusDescription = 403.ToString();
                  context.HttpContext.Response.StatusCode = 403;
                  context.WriteHtmlText( "<title>403 Forbidden</title>") ;
                  context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
                  context.WriteHtmlText( "<p /><hr />") ;
                  GXUtil.WriteLog("send_http_error_code " + 403.ToString());
                  AnyError = 1;
                  return  ;
               }
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  A456ContagemResultado_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A456ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A456ContagemResultado_Codigo), 6, 0)));
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  disable_std_buttons_dsp( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  Gx_mode = "INS";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  standaloneModal( ) ;
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E11412 */
                           E11412 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "AFTER TRN") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E12412 */
                           E12412 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_enter( ) ;
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DELETE") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_delete( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                        {
                           context.wbHandled = 1;
                           AfterKeyLoadScreen( ) ;
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            /* Execute user event: E12412 */
            E12412 ();
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll4169( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            bttBtn_trn_delete_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Enabled), 5, 0)));
         }
      }

      protected void disable_std_buttons_dsp( )
      {
         bttBtn_trn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
         bttBtn_trn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            bttBtn_trn_enter_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Visible), 5, 0)));
         }
         DisableAttributes4169( ) ;
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void ResetCaption410( )
      {
      }

      protected void E11412( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV7WWPContext) ;
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV7WWPContext) ;
         AV8TrnContext.FromXml(AV9WebSession.Get("TrnContext"), "");
         if ( ( StringUtil.StrCmp(AV8TrnContext.gxTpr_Transactionname, AV26Pgmname) == 0 ) && ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) )
         {
            AV27GXV1 = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV27GXV1), 8, 0)));
            while ( AV27GXV1 <= AV8TrnContext.gxTpr_Attributes.Count )
            {
               AV22TrnContextAtt = ((wwpbaseobjects.SdtWWPTransactionContext_Attribute)AV8TrnContext.gxTpr_Attributes.Item(AV27GXV1));
               if ( StringUtil.StrCmp(AV22TrnContextAtt.gxTpr_Attributename, "ContagemResultado_ServicoSS") == 0 )
               {
                  AV23Insert_ContagemResultado_ServicoSS = (int)(NumberUtil.Val( AV22TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23Insert_ContagemResultado_ServicoSS", StringUtil.LTrim( StringUtil.Str( (decimal)(AV23Insert_ContagemResultado_ServicoSS), 6, 0)));
               }
               else if ( StringUtil.StrCmp(AV22TrnContextAtt.gxTpr_Attributename, "ContagemResultado_CntSrvCod") == 0 )
               {
                  AV11Insert_ContagemResultado_CntSrvCod = (int)(NumberUtil.Val( AV22TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11Insert_ContagemResultado_CntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11Insert_ContagemResultado_CntSrvCod), 6, 0)));
               }
               else if ( StringUtil.StrCmp(AV22TrnContextAtt.gxTpr_Attributename, "ContagemResultado_OSVinculada") == 0 )
               {
                  AV12Insert_ContagemResultado_OSVinculada = (int)(NumberUtil.Val( AV22TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12Insert_ContagemResultado_OSVinculada", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12Insert_ContagemResultado_OSVinculada), 6, 0)));
               }
               else if ( StringUtil.StrCmp(AV22TrnContextAtt.gxTpr_Attributename, "ContagemResultado_NaoCnfDmnCod") == 0 )
               {
                  AV13Insert_ContagemResultado_NaoCnfDmnCod = (int)(NumberUtil.Val( AV22TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13Insert_ContagemResultado_NaoCnfDmnCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13Insert_ContagemResultado_NaoCnfDmnCod), 6, 0)));
               }
               else if ( StringUtil.StrCmp(AV22TrnContextAtt.gxTpr_Attributename, "ContagemResultado_LoteAceiteCod") == 0 )
               {
                  AV14Insert_ContagemResultado_LoteAceiteCod = (int)(NumberUtil.Val( AV22TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14Insert_ContagemResultado_LoteAceiteCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14Insert_ContagemResultado_LoteAceiteCod), 6, 0)));
               }
               else if ( StringUtil.StrCmp(AV22TrnContextAtt.gxTpr_Attributename, "ContagemResultado_ContratadaOrigemCod") == 0 )
               {
                  AV15Insert_ContagemResultado_ContratadaOrigemCod = (int)(NumberUtil.Val( AV22TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15Insert_ContagemResultado_ContratadaOrigemCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15Insert_ContagemResultado_ContratadaOrigemCod), 6, 0)));
               }
               else if ( StringUtil.StrCmp(AV22TrnContextAtt.gxTpr_Attributename, "ContagemResultado_ContadorFSCod") == 0 )
               {
                  AV16Insert_ContagemResultado_ContadorFSCod = (int)(NumberUtil.Val( AV22TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16Insert_ContagemResultado_ContadorFSCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16Insert_ContagemResultado_ContadorFSCod), 6, 0)));
               }
               else if ( StringUtil.StrCmp(AV22TrnContextAtt.gxTpr_Attributename, "ContagemResultado_Responsavel") == 0 )
               {
                  AV17Insert_ContagemResultado_Responsavel = (int)(NumberUtil.Val( AV22TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17Insert_ContagemResultado_Responsavel", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17Insert_ContagemResultado_Responsavel), 6, 0)));
               }
               else if ( StringUtil.StrCmp(AV22TrnContextAtt.gxTpr_Attributename, "ContagemResultado_LiqLogCod") == 0 )
               {
                  AV18Insert_ContagemResultado_LiqLogCod = (int)(NumberUtil.Val( AV22TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18Insert_ContagemResultado_LiqLogCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18Insert_ContagemResultado_LiqLogCod), 6, 0)));
               }
               else if ( StringUtil.StrCmp(AV22TrnContextAtt.gxTpr_Attributename, "Modulo_Codigo") == 0 )
               {
                  AV19Insert_Modulo_Codigo = (int)(NumberUtil.Val( AV22TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19Insert_Modulo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV19Insert_Modulo_Codigo), 6, 0)));
               }
               else if ( StringUtil.StrCmp(AV22TrnContextAtt.gxTpr_Attributename, "ContagemResultado_ContratadaCod") == 0 )
               {
                  AV20Insert_ContagemResultado_ContratadaCod = (int)(NumberUtil.Val( AV22TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20Insert_ContagemResultado_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20Insert_ContagemResultado_ContratadaCod), 6, 0)));
               }
               else if ( StringUtil.StrCmp(AV22TrnContextAtt.gxTpr_Attributename, "ContagemResultado_SistemaCod") == 0 )
               {
                  AV21Insert_ContagemResultado_SistemaCod = (int)(NumberUtil.Val( AV22TrnContextAtt.gxTpr_Attributevalue, "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Insert_ContagemResultado_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21Insert_ContagemResultado_SistemaCod), 6, 0)));
               }
               AV27GXV1 = (int)(AV27GXV1+1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27GXV1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV27GXV1), 8, 0)));
            }
         }
      }

      protected void E12412( )
      {
         /* After Trn Routine */
         context.setWebReturnParms(new Object[] {});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void ZM4169( short GX_JID )
      {
         if ( ( GX_JID == 23 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z508ContagemResultado_Owner = T00413_A508ContagemResultado_Owner[0];
               Z494ContagemResultado_Descricao = T00413_A494ContagemResultado_Descricao[0];
               Z472ContagemResultado_DataEntrega = T00413_A472ContagemResultado_DataEntrega[0];
               Z912ContagemResultado_HoraEntrega = T00413_A912ContagemResultado_HoraEntrega[0];
               Z1351ContagemResultado_DataPrevista = T00413_A1351ContagemResultado_DataPrevista[0];
               Z1452ContagemResultado_SS = T00413_A1452ContagemResultado_SS[0];
               Z493ContagemResultado_DemandaFM = T00413_A493ContagemResultado_DemandaFM[0];
               Z1584ContagemResultado_UOOwner = T00413_A1584ContagemResultado_UOOwner[0];
               Z1515ContagemResultado_Evento = T00413_A1515ContagemResultado_Evento[0];
               Z1583ContagemResultado_TipoRegistro = T00413_A1583ContagemResultado_TipoRegistro[0];
               Z1585ContagemResultado_Referencia = T00413_A1585ContagemResultado_Referencia[0];
               Z1586ContagemResultado_Restricoes = T00413_A1586ContagemResultado_Restricoes[0];
               Z1587ContagemResultado_PrioridadePrevista = T00413_A1587ContagemResultado_PrioridadePrevista[0];
               Z484ContagemResultado_StatusDmn = T00413_A484ContagemResultado_StatusDmn[0];
               Z471ContagemResultado_DataDmn = T00413_A471ContagemResultado_DataDmn[0];
               Z1044ContagemResultado_FncUsrCod = T00413_A1044ContagemResultado_FncUsrCod[0];
               Z1227ContagemResultado_PrazoInicialDias = T00413_A1227ContagemResultado_PrazoInicialDias[0];
               Z1350ContagemResultado_DataCadastro = T00413_A1350ContagemResultado_DataCadastro[0];
               Z1173ContagemResultado_OSManual = T00413_A1173ContagemResultado_OSManual[0];
               Z598ContagemResultado_Baseline = T00413_A598ContagemResultado_Baseline[0];
               Z485ContagemResultado_EhValidacao = T00413_A485ContagemResultado_EhValidacao[0];
               Z146Modulo_Codigo = T00413_A146Modulo_Codigo[0];
               Z454ContagemResultado_ContadorFSCod = T00413_A454ContagemResultado_ContadorFSCod[0];
               Z890ContagemResultado_Responsavel = T00413_A890ContagemResultado_Responsavel[0];
               Z468ContagemResultado_NaoCnfDmnCod = T00413_A468ContagemResultado_NaoCnfDmnCod[0];
               Z490ContagemResultado_ContratadaCod = T00413_A490ContagemResultado_ContratadaCod[0];
               Z805ContagemResultado_ContratadaOrigemCod = T00413_A805ContagemResultado_ContratadaOrigemCod[0];
               Z489ContagemResultado_SistemaCod = T00413_A489ContagemResultado_SistemaCod[0];
               Z597ContagemResultado_LoteAceiteCod = T00413_A597ContagemResultado_LoteAceiteCod[0];
               Z1636ContagemResultado_ServicoSS = T00413_A1636ContagemResultado_ServicoSS[0];
               Z1043ContagemResultado_LiqLogCod = T00413_A1043ContagemResultado_LiqLogCod[0];
               Z1553ContagemResultado_CntSrvCod = T00413_A1553ContagemResultado_CntSrvCod[0];
               Z602ContagemResultado_OSVinculada = T00413_A602ContagemResultado_OSVinculada[0];
            }
            else
            {
               Z508ContagemResultado_Owner = A508ContagemResultado_Owner;
               Z494ContagemResultado_Descricao = A494ContagemResultado_Descricao;
               Z472ContagemResultado_DataEntrega = A472ContagemResultado_DataEntrega;
               Z912ContagemResultado_HoraEntrega = A912ContagemResultado_HoraEntrega;
               Z1351ContagemResultado_DataPrevista = A1351ContagemResultado_DataPrevista;
               Z1452ContagemResultado_SS = A1452ContagemResultado_SS;
               Z493ContagemResultado_DemandaFM = A493ContagemResultado_DemandaFM;
               Z1584ContagemResultado_UOOwner = A1584ContagemResultado_UOOwner;
               Z1515ContagemResultado_Evento = A1515ContagemResultado_Evento;
               Z1583ContagemResultado_TipoRegistro = A1583ContagemResultado_TipoRegistro;
               Z1585ContagemResultado_Referencia = A1585ContagemResultado_Referencia;
               Z1586ContagemResultado_Restricoes = A1586ContagemResultado_Restricoes;
               Z1587ContagemResultado_PrioridadePrevista = A1587ContagemResultado_PrioridadePrevista;
               Z484ContagemResultado_StatusDmn = A484ContagemResultado_StatusDmn;
               Z471ContagemResultado_DataDmn = A471ContagemResultado_DataDmn;
               Z1044ContagemResultado_FncUsrCod = A1044ContagemResultado_FncUsrCod;
               Z1227ContagemResultado_PrazoInicialDias = A1227ContagemResultado_PrazoInicialDias;
               Z1350ContagemResultado_DataCadastro = A1350ContagemResultado_DataCadastro;
               Z1173ContagemResultado_OSManual = A1173ContagemResultado_OSManual;
               Z598ContagemResultado_Baseline = A598ContagemResultado_Baseline;
               Z485ContagemResultado_EhValidacao = A485ContagemResultado_EhValidacao;
               Z146Modulo_Codigo = A146Modulo_Codigo;
               Z454ContagemResultado_ContadorFSCod = A454ContagemResultado_ContadorFSCod;
               Z890ContagemResultado_Responsavel = A890ContagemResultado_Responsavel;
               Z468ContagemResultado_NaoCnfDmnCod = A468ContagemResultado_NaoCnfDmnCod;
               Z490ContagemResultado_ContratadaCod = A490ContagemResultado_ContratadaCod;
               Z805ContagemResultado_ContratadaOrigemCod = A805ContagemResultado_ContratadaOrigemCod;
               Z489ContagemResultado_SistemaCod = A489ContagemResultado_SistemaCod;
               Z597ContagemResultado_LoteAceiteCod = A597ContagemResultado_LoteAceiteCod;
               Z1636ContagemResultado_ServicoSS = A1636ContagemResultado_ServicoSS;
               Z1043ContagemResultado_LiqLogCod = A1043ContagemResultado_LiqLogCod;
               Z1553ContagemResultado_CntSrvCod = A1553ContagemResultado_CntSrvCod;
               Z602ContagemResultado_OSVinculada = A602ContagemResultado_OSVinculada;
            }
         }
         if ( GX_JID == -23 )
         {
            Z456ContagemResultado_Codigo = A456ContagemResultado_Codigo;
            Z508ContagemResultado_Owner = A508ContagemResultado_Owner;
            Z494ContagemResultado_Descricao = A494ContagemResultado_Descricao;
            Z514ContagemResultado_Observacao = A514ContagemResultado_Observacao;
            Z472ContagemResultado_DataEntrega = A472ContagemResultado_DataEntrega;
            Z912ContagemResultado_HoraEntrega = A912ContagemResultado_HoraEntrega;
            Z1351ContagemResultado_DataPrevista = A1351ContagemResultado_DataPrevista;
            Z1452ContagemResultado_SS = A1452ContagemResultado_SS;
            Z493ContagemResultado_DemandaFM = A493ContagemResultado_DemandaFM;
            Z1584ContagemResultado_UOOwner = A1584ContagemResultado_UOOwner;
            Z1515ContagemResultado_Evento = A1515ContagemResultado_Evento;
            Z1583ContagemResultado_TipoRegistro = A1583ContagemResultado_TipoRegistro;
            Z1585ContagemResultado_Referencia = A1585ContagemResultado_Referencia;
            Z1586ContagemResultado_Restricoes = A1586ContagemResultado_Restricoes;
            Z1587ContagemResultado_PrioridadePrevista = A1587ContagemResultado_PrioridadePrevista;
            Z484ContagemResultado_StatusDmn = A484ContagemResultado_StatusDmn;
            Z471ContagemResultado_DataDmn = A471ContagemResultado_DataDmn;
            Z1044ContagemResultado_FncUsrCod = A1044ContagemResultado_FncUsrCod;
            Z1227ContagemResultado_PrazoInicialDias = A1227ContagemResultado_PrazoInicialDias;
            Z1350ContagemResultado_DataCadastro = A1350ContagemResultado_DataCadastro;
            Z1173ContagemResultado_OSManual = A1173ContagemResultado_OSManual;
            Z598ContagemResultado_Baseline = A598ContagemResultado_Baseline;
            Z485ContagemResultado_EhValidacao = A485ContagemResultado_EhValidacao;
            Z146Modulo_Codigo = A146Modulo_Codigo;
            Z454ContagemResultado_ContadorFSCod = A454ContagemResultado_ContadorFSCod;
            Z890ContagemResultado_Responsavel = A890ContagemResultado_Responsavel;
            Z468ContagemResultado_NaoCnfDmnCod = A468ContagemResultado_NaoCnfDmnCod;
            Z490ContagemResultado_ContratadaCod = A490ContagemResultado_ContratadaCod;
            Z805ContagemResultado_ContratadaOrigemCod = A805ContagemResultado_ContratadaOrigemCod;
            Z489ContagemResultado_SistemaCod = A489ContagemResultado_SistemaCod;
            Z597ContagemResultado_LoteAceiteCod = A597ContagemResultado_LoteAceiteCod;
            Z1636ContagemResultado_ServicoSS = A1636ContagemResultado_ServicoSS;
            Z1043ContagemResultado_LiqLogCod = A1043ContagemResultado_LiqLogCod;
            Z1553ContagemResultado_CntSrvCod = A1553ContagemResultado_CntSrvCod;
            Z602ContagemResultado_OSVinculada = A602ContagemResultado_OSVinculada;
         }
      }

      protected void standaloneNotModal( )
      {
         GXACONTAGEMRESULTADO_CONTADORFSCOD_html4169( ) ;
         AV26Pgmname = "SolicitacaoServico";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26Pgmname", AV26Pgmname);
         Gx_date = DateTimeUtil.Today( context);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_date", context.localUtil.Format(Gx_date, "99/99/99"));
         Gx_BScreen = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_BScreen", StringUtil.Str( (decimal)(Gx_BScreen), 1, 0));
         GXACONTAGEMRESULTADO_NAOCNFDMNCOD_html4169( AV7WWPContext) ;
         GXACONTAGEMRESULTADO_CONTRATADAORIGEMCOD_html4169( AV7WWPContext) ;
         GXACONTAGEMRESULTADO_CONTRATADACOD_html4169( AV7WWPContext) ;
      }

      protected void standaloneModal( )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (false==A598ContagemResultado_Baseline) && ( Gx_BScreen == 0 ) )
         {
            A598ContagemResultado_Baseline = false;
            n598ContagemResultado_Baseline = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A598ContagemResultado_Baseline", A598ContagemResultado_Baseline);
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (false==A485ContagemResultado_EhValidacao) && ( Gx_BScreen == 0 ) )
         {
            A485ContagemResultado_EhValidacao = false;
            n485ContagemResultado_EhValidacao = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A485ContagemResultado_EhValidacao", A485ContagemResultado_EhValidacao);
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (DateTime.MinValue==A1350ContagemResultado_DataCadastro) && ( Gx_BScreen == 0 ) )
         {
            A1350ContagemResultado_DataCadastro = DateTimeUtil.ServerNow( context, "DEFAULT");
            n1350ContagemResultado_DataCadastro = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1350ContagemResultado_DataCadastro", context.localUtil.TToC( A1350ContagemResultado_DataCadastro, 8, 5, 0, 3, "/", ":", " "));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (0==A1515ContagemResultado_Evento) && ( Gx_BScreen == 0 ) )
         {
            A1515ContagemResultado_Evento = 1;
            n1515ContagemResultado_Evento = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1515ContagemResultado_Evento", StringUtil.LTrim( StringUtil.Str( (decimal)(A1515ContagemResultado_Evento), 2, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (0==A1452ContagemResultado_SS) && ( Gx_BScreen == 0 ) )
         {
            A1452ContagemResultado_SS = 0;
            n1452ContagemResultado_SS = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1452ContagemResultado_SS", StringUtil.LTrim( StringUtil.Str( (decimal)(A1452ContagemResultado_SS), 8, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (0==A1583ContagemResultado_TipoRegistro) && ( Gx_BScreen == 0 ) )
         {
            A1583ContagemResultado_TipoRegistro = 2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1583ContagemResultado_TipoRegistro", StringUtil.LTrim( StringUtil.Str( (decimal)(A1583ContagemResultado_TipoRegistro), 4, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && String.IsNullOrEmpty(StringUtil.RTrim( A484ContagemResultado_StatusDmn)) && ( Gx_BScreen == 0 ) )
         {
            A484ContagemResultado_StatusDmn = "S";
            n484ContagemResultado_StatusDmn = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A484ContagemResultado_StatusDmn", A484ContagemResultado_StatusDmn);
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (0==A508ContagemResultado_Owner) && ( Gx_BScreen == 0 ) )
         {
            A508ContagemResultado_Owner = AV7WWPContext.gxTpr_Userid;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A508ContagemResultado_Owner", StringUtil.LTrim( StringUtil.Str( (decimal)(A508ContagemResultado_Owner), 6, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (DateTime.MinValue==A471ContagemResultado_DataDmn) && ( Gx_BScreen == 0 ) )
         {
            A471ContagemResultado_DataDmn = Gx_date;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A471ContagemResultado_DataDmn", context.localUtil.Format(A471ContagemResultado_DataDmn, "99/99/99"));
         }
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            bttBtn_trn_delete_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Enabled), 5, 0)));
         }
         else
         {
            bttBtn_trn_delete_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Enabled), 5, 0)));
         }
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            bttBtn_trn_enter_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         else
         {
            bttBtn_trn_enter_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ( Gx_BScreen == 0 ) )
         {
         }
      }

      protected void Load4169( )
      {
         /* Using cursor T004118 */
         pr_default.execute(16, new Object[] {A456ContagemResultado_Codigo});
         if ( (pr_default.getStatus(16) != 101) )
         {
            RcdFound69 = 1;
            A508ContagemResultado_Owner = T004118_A508ContagemResultado_Owner[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A508ContagemResultado_Owner", StringUtil.LTrim( StringUtil.Str( (decimal)(A508ContagemResultado_Owner), 6, 0)));
            A494ContagemResultado_Descricao = T004118_A494ContagemResultado_Descricao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A494ContagemResultado_Descricao", A494ContagemResultado_Descricao);
            n494ContagemResultado_Descricao = T004118_n494ContagemResultado_Descricao[0];
            A514ContagemResultado_Observacao = T004118_A514ContagemResultado_Observacao[0];
            n514ContagemResultado_Observacao = T004118_n514ContagemResultado_Observacao[0];
            A472ContagemResultado_DataEntrega = T004118_A472ContagemResultado_DataEntrega[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A472ContagemResultado_DataEntrega", context.localUtil.Format(A472ContagemResultado_DataEntrega, "99/99/99"));
            n472ContagemResultado_DataEntrega = T004118_n472ContagemResultado_DataEntrega[0];
            A912ContagemResultado_HoraEntrega = T004118_A912ContagemResultado_HoraEntrega[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A912ContagemResultado_HoraEntrega", context.localUtil.TToC( A912ContagemResultado_HoraEntrega, 0, 5, 0, 3, "/", ":", " "));
            n912ContagemResultado_HoraEntrega = T004118_n912ContagemResultado_HoraEntrega[0];
            A1351ContagemResultado_DataPrevista = T004118_A1351ContagemResultado_DataPrevista[0];
            n1351ContagemResultado_DataPrevista = T004118_n1351ContagemResultado_DataPrevista[0];
            A1452ContagemResultado_SS = T004118_A1452ContagemResultado_SS[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1452ContagemResultado_SS", StringUtil.LTrim( StringUtil.Str( (decimal)(A1452ContagemResultado_SS), 8, 0)));
            n1452ContagemResultado_SS = T004118_n1452ContagemResultado_SS[0];
            A493ContagemResultado_DemandaFM = T004118_A493ContagemResultado_DemandaFM[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A493ContagemResultado_DemandaFM", A493ContagemResultado_DemandaFM);
            n493ContagemResultado_DemandaFM = T004118_n493ContagemResultado_DemandaFM[0];
            A1584ContagemResultado_UOOwner = T004118_A1584ContagemResultado_UOOwner[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1584ContagemResultado_UOOwner", StringUtil.LTrim( StringUtil.Str( (decimal)(A1584ContagemResultado_UOOwner), 6, 0)));
            n1584ContagemResultado_UOOwner = T004118_n1584ContagemResultado_UOOwner[0];
            A1515ContagemResultado_Evento = T004118_A1515ContagemResultado_Evento[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1515ContagemResultado_Evento", StringUtil.LTrim( StringUtil.Str( (decimal)(A1515ContagemResultado_Evento), 2, 0)));
            n1515ContagemResultado_Evento = T004118_n1515ContagemResultado_Evento[0];
            A1583ContagemResultado_TipoRegistro = T004118_A1583ContagemResultado_TipoRegistro[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1583ContagemResultado_TipoRegistro", StringUtil.LTrim( StringUtil.Str( (decimal)(A1583ContagemResultado_TipoRegistro), 4, 0)));
            A1585ContagemResultado_Referencia = T004118_A1585ContagemResultado_Referencia[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1585ContagemResultado_Referencia", A1585ContagemResultado_Referencia);
            n1585ContagemResultado_Referencia = T004118_n1585ContagemResultado_Referencia[0];
            A1586ContagemResultado_Restricoes = T004118_A1586ContagemResultado_Restricoes[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1586ContagemResultado_Restricoes", A1586ContagemResultado_Restricoes);
            n1586ContagemResultado_Restricoes = T004118_n1586ContagemResultado_Restricoes[0];
            A1587ContagemResultado_PrioridadePrevista = T004118_A1587ContagemResultado_PrioridadePrevista[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1587ContagemResultado_PrioridadePrevista", A1587ContagemResultado_PrioridadePrevista);
            n1587ContagemResultado_PrioridadePrevista = T004118_n1587ContagemResultado_PrioridadePrevista[0];
            A484ContagemResultado_StatusDmn = T004118_A484ContagemResultado_StatusDmn[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A484ContagemResultado_StatusDmn", A484ContagemResultado_StatusDmn);
            n484ContagemResultado_StatusDmn = T004118_n484ContagemResultado_StatusDmn[0];
            A471ContagemResultado_DataDmn = T004118_A471ContagemResultado_DataDmn[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A471ContagemResultado_DataDmn", context.localUtil.Format(A471ContagemResultado_DataDmn, "99/99/99"));
            A1044ContagemResultado_FncUsrCod = T004118_A1044ContagemResultado_FncUsrCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1044ContagemResultado_FncUsrCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1044ContagemResultado_FncUsrCod), 6, 0)));
            n1044ContagemResultado_FncUsrCod = T004118_n1044ContagemResultado_FncUsrCod[0];
            A1227ContagemResultado_PrazoInicialDias = T004118_A1227ContagemResultado_PrazoInicialDias[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1227ContagemResultado_PrazoInicialDias", StringUtil.LTrim( StringUtil.Str( (decimal)(A1227ContagemResultado_PrazoInicialDias), 4, 0)));
            n1227ContagemResultado_PrazoInicialDias = T004118_n1227ContagemResultado_PrazoInicialDias[0];
            A1350ContagemResultado_DataCadastro = T004118_A1350ContagemResultado_DataCadastro[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1350ContagemResultado_DataCadastro", context.localUtil.TToC( A1350ContagemResultado_DataCadastro, 8, 5, 0, 3, "/", ":", " "));
            n1350ContagemResultado_DataCadastro = T004118_n1350ContagemResultado_DataCadastro[0];
            A1173ContagemResultado_OSManual = T004118_A1173ContagemResultado_OSManual[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1173ContagemResultado_OSManual", A1173ContagemResultado_OSManual);
            n1173ContagemResultado_OSManual = T004118_n1173ContagemResultado_OSManual[0];
            A598ContagemResultado_Baseline = T004118_A598ContagemResultado_Baseline[0];
            n598ContagemResultado_Baseline = T004118_n598ContagemResultado_Baseline[0];
            A485ContagemResultado_EhValidacao = T004118_A485ContagemResultado_EhValidacao[0];
            n485ContagemResultado_EhValidacao = T004118_n485ContagemResultado_EhValidacao[0];
            A146Modulo_Codigo = T004118_A146Modulo_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A146Modulo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A146Modulo_Codigo), 6, 0)));
            n146Modulo_Codigo = T004118_n146Modulo_Codigo[0];
            A454ContagemResultado_ContadorFSCod = T004118_A454ContagemResultado_ContadorFSCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A454ContagemResultado_ContadorFSCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A454ContagemResultado_ContadorFSCod), 6, 0)));
            n454ContagemResultado_ContadorFSCod = T004118_n454ContagemResultado_ContadorFSCod[0];
            A890ContagemResultado_Responsavel = T004118_A890ContagemResultado_Responsavel[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A890ContagemResultado_Responsavel", StringUtil.LTrim( StringUtil.Str( (decimal)(A890ContagemResultado_Responsavel), 6, 0)));
            n890ContagemResultado_Responsavel = T004118_n890ContagemResultado_Responsavel[0];
            A468ContagemResultado_NaoCnfDmnCod = T004118_A468ContagemResultado_NaoCnfDmnCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A468ContagemResultado_NaoCnfDmnCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A468ContagemResultado_NaoCnfDmnCod), 6, 0)));
            n468ContagemResultado_NaoCnfDmnCod = T004118_n468ContagemResultado_NaoCnfDmnCod[0];
            A490ContagemResultado_ContratadaCod = T004118_A490ContagemResultado_ContratadaCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A490ContagemResultado_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A490ContagemResultado_ContratadaCod), 6, 0)));
            n490ContagemResultado_ContratadaCod = T004118_n490ContagemResultado_ContratadaCod[0];
            A805ContagemResultado_ContratadaOrigemCod = T004118_A805ContagemResultado_ContratadaOrigemCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A805ContagemResultado_ContratadaOrigemCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A805ContagemResultado_ContratadaOrigemCod), 6, 0)));
            n805ContagemResultado_ContratadaOrigemCod = T004118_n805ContagemResultado_ContratadaOrigemCod[0];
            A489ContagemResultado_SistemaCod = T004118_A489ContagemResultado_SistemaCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A489ContagemResultado_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A489ContagemResultado_SistemaCod), 6, 0)));
            n489ContagemResultado_SistemaCod = T004118_n489ContagemResultado_SistemaCod[0];
            A597ContagemResultado_LoteAceiteCod = T004118_A597ContagemResultado_LoteAceiteCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A597ContagemResultado_LoteAceiteCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A597ContagemResultado_LoteAceiteCod), 6, 0)));
            n597ContagemResultado_LoteAceiteCod = T004118_n597ContagemResultado_LoteAceiteCod[0];
            A1636ContagemResultado_ServicoSS = T004118_A1636ContagemResultado_ServicoSS[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1636ContagemResultado_ServicoSS", StringUtil.LTrim( StringUtil.Str( (decimal)(A1636ContagemResultado_ServicoSS), 6, 0)));
            n1636ContagemResultado_ServicoSS = T004118_n1636ContagemResultado_ServicoSS[0];
            A1043ContagemResultado_LiqLogCod = T004118_A1043ContagemResultado_LiqLogCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1043ContagemResultado_LiqLogCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1043ContagemResultado_LiqLogCod), 6, 0)));
            n1043ContagemResultado_LiqLogCod = T004118_n1043ContagemResultado_LiqLogCod[0];
            A1553ContagemResultado_CntSrvCod = T004118_A1553ContagemResultado_CntSrvCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1553ContagemResultado_CntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1553ContagemResultado_CntSrvCod), 6, 0)));
            n1553ContagemResultado_CntSrvCod = T004118_n1553ContagemResultado_CntSrvCod[0];
            A602ContagemResultado_OSVinculada = T004118_A602ContagemResultado_OSVinculada[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A602ContagemResultado_OSVinculada", StringUtil.LTrim( StringUtil.Str( (decimal)(A602ContagemResultado_OSVinculada), 6, 0)));
            n602ContagemResultado_OSVinculada = T004118_n602ContagemResultado_OSVinculada[0];
            ZM4169( -23) ;
         }
         pr_default.close(16);
         OnLoadActions4169( ) ;
      }

      protected void OnLoadActions4169( )
      {
         GXAMODULO_CODIGO_html4169( A489ContagemResultado_SistemaCod) ;
      }

      protected void CheckExtendedTable4169( )
      {
         Gx_BScreen = 1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_BScreen", StringUtil.Str( (decimal)(Gx_BScreen), 1, 0));
         standaloneModal( ) ;
         if ( ! ( (DateTime.MinValue==A472ContagemResultado_DataEntrega) || ( A472ContagemResultado_DataEntrega >= context.localUtil.YMDToD( 1753, 1, 1) ) ) )
         {
            GX_msglist.addItem("Campo Data para Entrega fora do intervalo", "OutOfRange", 1, "CONTAGEMRESULTADO_DATAENTREGA");
            AnyError = 1;
            GX_FocusControl = edtContagemResultado_DataEntrega_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         /* Using cursor T004112 */
         pr_default.execute(10, new Object[] {n1636ContagemResultado_ServicoSS, A1636ContagemResultado_ServicoSS});
         if ( (pr_default.getStatus(10) == 101) )
         {
            if ( ! ( (0==A1636ContagemResultado_ServicoSS) ) )
            {
               GX_msglist.addItem("N�o existe 'Contagem Resultado_Servico'.", "ForeignKeyNotFound", 1, "CONTAGEMRESULTADO_SERVICOSS");
               AnyError = 1;
               GX_FocusControl = dynContagemResultado_ServicoSS_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         pr_default.close(10);
         if ( ! ( ( A1515ContagemResultado_Evento == 1 ) || ( A1515ContagemResultado_Evento == 2 ) || ( A1515ContagemResultado_Evento == 3 ) || ( A1515ContagemResultado_Evento == 4 ) || (0==A1515ContagemResultado_Evento) ) )
         {
            GX_msglist.addItem("Campo Evento fora do intervalo", "OutOfRange", 1, "CONTAGEMRESULTADO_EVENTO");
            AnyError = 1;
            GX_FocusControl = cmbContagemResultado_Evento_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( ! ( ( A1583ContagemResultado_TipoRegistro == 1 ) || ( A1583ContagemResultado_TipoRegistro == 2 ) || ( A1583ContagemResultado_TipoRegistro == 3 ) ) )
         {
            GX_msglist.addItem("Campo Contagem Resultado_Tipo Registro fora do intervalo", "OutOfRange", 1, "CONTAGEMRESULTADO_TIPOREGISTRO");
            AnyError = 1;
            GX_FocusControl = cmbContagemResultado_TipoRegistro_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( ! ( ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "B") == 0 ) || ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "S") == 0 ) || ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "E") == 0 ) || ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "A") == 0 ) || ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "R") == 0 ) || ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "C") == 0 ) || ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "D") == 0 ) || ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "H") == 0 ) || ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "O") == 0 ) || ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "P") == 0 ) || ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "L") == 0 ) || ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "X") == 0 ) || ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "N") == 0 ) || ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "J") == 0 ) || ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "I") == 0 ) || ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "T") == 0 ) || ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "Q") == 0 ) || ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "G") == 0 ) || ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "M") == 0 ) || ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "U") == 0 ) || String.IsNullOrEmpty(StringUtil.RTrim( A484ContagemResultado_StatusDmn)) ) )
         {
            GX_msglist.addItem("Campo Status fora do intervalo", "OutOfRange", 1, "CONTAGEMRESULTADO_STATUSDMN");
            AnyError = 1;
            GX_FocusControl = cmbContagemResultado_StatusDmn_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( ! ( (DateTime.MinValue==A471ContagemResultado_DataDmn) || ( A471ContagemResultado_DataDmn >= context.localUtil.YMDToD( 1753, 1, 1) ) ) )
         {
            GX_msglist.addItem("Campo Data da OS fora do intervalo", "OutOfRange", 1, "CONTAGEMRESULTADO_DATADMN");
            AnyError = 1;
            GX_FocusControl = edtContagemResultado_DataDmn_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         /* Using cursor T004114 */
         pr_default.execute(12, new Object[] {n1553ContagemResultado_CntSrvCod, A1553ContagemResultado_CntSrvCod});
         if ( (pr_default.getStatus(12) == 101) )
         {
            if ( ! ( (0==A1553ContagemResultado_CntSrvCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Contagem Resultado_Contrato Servicos'.", "ForeignKeyNotFound", 1, "CONTAGEMRESULTADO_CNTSRVCOD");
               AnyError = 1;
               GX_FocusControl = edtContagemResultado_CntSrvCod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         pr_default.close(12);
         /* Using cursor T004115 */
         pr_default.execute(13, new Object[] {n602ContagemResultado_OSVinculada, A602ContagemResultado_OSVinculada});
         if ( (pr_default.getStatus(13) == 101) )
         {
            if ( ! ( (0==A602ContagemResultado_OSVinculada) ) )
            {
               GX_msglist.addItem("N�o existe 'OS Vinculada'.", "ForeignKeyNotFound", 1, "CONTAGEMRESULTADO_OSVINCULADA");
               AnyError = 1;
               GX_FocusControl = edtContagemResultado_OSVinculada_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         pr_default.close(13);
         /* Using cursor T00417 */
         pr_default.execute(5, new Object[] {n468ContagemResultado_NaoCnfDmnCod, A468ContagemResultado_NaoCnfDmnCod});
         if ( (pr_default.getStatus(5) == 101) )
         {
            if ( ! ( (0==A468ContagemResultado_NaoCnfDmnCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Contagem Resultado_Nao Conformidade da Demanda'.", "ForeignKeyNotFound", 1, "CONTAGEMRESULTADO_NAOCNFDMNCOD");
               AnyError = 1;
               GX_FocusControl = dynContagemResultado_NaoCnfDmnCod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         pr_default.close(5);
         /* Using cursor T004111 */
         pr_default.execute(9, new Object[] {n597ContagemResultado_LoteAceiteCod, A597ContagemResultado_LoteAceiteCod});
         if ( (pr_default.getStatus(9) == 101) )
         {
            if ( ! ( (0==A597ContagemResultado_LoteAceiteCod) ) )
            {
               GX_msglist.addItem("N�o existe 'ST_Contagem Resultado_Lote'.", "ForeignKeyNotFound", 1, "CONTAGEMRESULTADO_LOTEACEITECOD");
               AnyError = 1;
               GX_FocusControl = dynContagemResultado_LoteAceiteCod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         pr_default.close(9);
         /* Using cursor T00419 */
         pr_default.execute(7, new Object[] {n805ContagemResultado_ContratadaOrigemCod, A805ContagemResultado_ContratadaOrigemCod});
         if ( (pr_default.getStatus(7) == 101) )
         {
            if ( ! ( (0==A805ContagemResultado_ContratadaOrigemCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Contagem Resutlado_Contratada Origem'.", "ForeignKeyNotFound", 1, "CONTAGEMRESULTADO_CONTRATADAORIGEMCOD");
               AnyError = 1;
               GX_FocusControl = dynContagemResultado_ContratadaOrigemCod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         pr_default.close(7);
         /* Using cursor T00415 */
         pr_default.execute(3, new Object[] {n454ContagemResultado_ContadorFSCod, A454ContagemResultado_ContadorFSCod});
         if ( (pr_default.getStatus(3) == 101) )
         {
            if ( ! ( (0==A454ContagemResultado_ContadorFSCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Contagem Resultado_Usuario Contador Fab. de Soft'.", "ForeignKeyNotFound", 1, "CONTAGEMRESULTADO_CONTADORFSCOD");
               AnyError = 1;
               GX_FocusControl = dynContagemResultado_ContadorFSCod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         pr_default.close(3);
         /* Using cursor T00416 */
         pr_default.execute(4, new Object[] {n890ContagemResultado_Responsavel, A890ContagemResultado_Responsavel});
         if ( (pr_default.getStatus(4) == 101) )
         {
            if ( ! ( (0==A890ContagemResultado_Responsavel) ) )
            {
               GX_msglist.addItem("N�o existe 'Contagem Resultado_Usuario Responsavel'.", "ForeignKeyNotFound", 1, "CONTAGEMRESULTADO_RESPONSAVEL");
               AnyError = 1;
               GX_FocusControl = edtContagemResultado_Responsavel_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         pr_default.close(4);
         /* Using cursor T004113 */
         pr_default.execute(11, new Object[] {n1043ContagemResultado_LiqLogCod, A1043ContagemResultado_LiqLogCod});
         if ( (pr_default.getStatus(11) == 101) )
         {
            if ( ! ( (0==A1043ContagemResultado_LiqLogCod) ) )
            {
               GX_msglist.addItem("N�o existe 'ST_Contagem Resultado_Contagem Resultado Liq Log'.", "ForeignKeyNotFound", 1, "CONTAGEMRESULTADO_LIQLOGCOD");
               AnyError = 1;
               GX_FocusControl = edtContagemResultado_LiqLogCod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         pr_default.close(11);
         /* Using cursor T00414 */
         pr_default.execute(2, new Object[] {n146Modulo_Codigo, A146Modulo_Codigo});
         if ( (pr_default.getStatus(2) == 101) )
         {
            if ( ! ( (0==A146Modulo_Codigo) ) )
            {
               GX_msglist.addItem("N�o existe 'Modulo'.", "ForeignKeyNotFound", 1, "MODULO_CODIGO");
               AnyError = 1;
               GX_FocusControl = dynModulo_Codigo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         pr_default.close(2);
         /* Using cursor T00418 */
         pr_default.execute(6, new Object[] {n490ContagemResultado_ContratadaCod, A490ContagemResultado_ContratadaCod});
         if ( (pr_default.getStatus(6) == 101) )
         {
            if ( ! ( (0==A490ContagemResultado_ContratadaCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Contagem Resultado_Contratada'.", "ForeignKeyNotFound", 1, "CONTAGEMRESULTADO_CONTRATADACOD");
               AnyError = 1;
               GX_FocusControl = dynContagemResultado_ContratadaCod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         pr_default.close(6);
         /* Using cursor T004110 */
         pr_default.execute(8, new Object[] {n489ContagemResultado_SistemaCod, A489ContagemResultado_SistemaCod});
         if ( (pr_default.getStatus(8) == 101) )
         {
            if ( ! ( (0==A489ContagemResultado_SistemaCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Contagem Resultado_Sistema'.", "ForeignKeyNotFound", 1, "CONTAGEMRESULTADO_SISTEMACOD");
               AnyError = 1;
               GX_FocusControl = edtContagemResultado_SistemaCod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         pr_default.close(8);
         GXAMODULO_CODIGO_html4169( A489ContagemResultado_SistemaCod) ;
         if ( ! ( (DateTime.MinValue==A1350ContagemResultado_DataCadastro) || ( A1350ContagemResultado_DataCadastro >= context.localUtil.YMDHMSToT( 1753, 1, 1, 0, 0, 0) ) ) )
         {
            GX_msglist.addItem("Campo Data de Cadastro fora do intervalo", "OutOfRange", 1, "CONTAGEMRESULTADO_DATACADASTRO");
            AnyError = 1;
            GX_FocusControl = edtContagemResultado_DataCadastro_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
      }

      protected void CloseExtendedTableCursors4169( )
      {
         pr_default.close(10);
         pr_default.close(12);
         pr_default.close(13);
         pr_default.close(5);
         pr_default.close(9);
         pr_default.close(7);
         pr_default.close(3);
         pr_default.close(4);
         pr_default.close(11);
         pr_default.close(2);
         pr_default.close(6);
         pr_default.close(8);
      }

      protected void enableDisable( )
      {
      }

      protected void gxLoad_32( int A1636ContagemResultado_ServicoSS )
      {
         /* Using cursor T004119 */
         pr_default.execute(17, new Object[] {n1636ContagemResultado_ServicoSS, A1636ContagemResultado_ServicoSS});
         if ( (pr_default.getStatus(17) == 101) )
         {
            if ( ! ( (0==A1636ContagemResultado_ServicoSS) ) )
            {
               GX_msglist.addItem("N�o existe 'Contagem Resultado_Servico'.", "ForeignKeyNotFound", 1, "CONTAGEMRESULTADO_SERVICOSS");
               AnyError = 1;
               GX_FocusControl = dynContagemResultado_ServicoSS_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(17) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(17);
      }

      protected void gxLoad_34( int A1553ContagemResultado_CntSrvCod )
      {
         /* Using cursor T004120 */
         pr_default.execute(18, new Object[] {n1553ContagemResultado_CntSrvCod, A1553ContagemResultado_CntSrvCod});
         if ( (pr_default.getStatus(18) == 101) )
         {
            if ( ! ( (0==A1553ContagemResultado_CntSrvCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Contagem Resultado_Contrato Servicos'.", "ForeignKeyNotFound", 1, "CONTAGEMRESULTADO_CNTSRVCOD");
               AnyError = 1;
               GX_FocusControl = edtContagemResultado_CntSrvCod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(18) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(18);
      }

      protected void gxLoad_35( int A602ContagemResultado_OSVinculada )
      {
         /* Using cursor T004121 */
         pr_default.execute(19, new Object[] {n602ContagemResultado_OSVinculada, A602ContagemResultado_OSVinculada});
         if ( (pr_default.getStatus(19) == 101) )
         {
            if ( ! ( (0==A602ContagemResultado_OSVinculada) ) )
            {
               GX_msglist.addItem("N�o existe 'OS Vinculada'.", "ForeignKeyNotFound", 1, "CONTAGEMRESULTADO_OSVINCULADA");
               AnyError = 1;
               GX_FocusControl = edtContagemResultado_OSVinculada_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(19) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(19);
      }

      protected void gxLoad_27( int A468ContagemResultado_NaoCnfDmnCod )
      {
         /* Using cursor T004122 */
         pr_default.execute(20, new Object[] {n468ContagemResultado_NaoCnfDmnCod, A468ContagemResultado_NaoCnfDmnCod});
         if ( (pr_default.getStatus(20) == 101) )
         {
            if ( ! ( (0==A468ContagemResultado_NaoCnfDmnCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Contagem Resultado_Nao Conformidade da Demanda'.", "ForeignKeyNotFound", 1, "CONTAGEMRESULTADO_NAOCNFDMNCOD");
               AnyError = 1;
               GX_FocusControl = dynContagemResultado_NaoCnfDmnCod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(20) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(20);
      }

      protected void gxLoad_31( int A597ContagemResultado_LoteAceiteCod )
      {
         /* Using cursor T004123 */
         pr_default.execute(21, new Object[] {n597ContagemResultado_LoteAceiteCod, A597ContagemResultado_LoteAceiteCod});
         if ( (pr_default.getStatus(21) == 101) )
         {
            if ( ! ( (0==A597ContagemResultado_LoteAceiteCod) ) )
            {
               GX_msglist.addItem("N�o existe 'ST_Contagem Resultado_Lote'.", "ForeignKeyNotFound", 1, "CONTAGEMRESULTADO_LOTEACEITECOD");
               AnyError = 1;
               GX_FocusControl = dynContagemResultado_LoteAceiteCod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(21) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(21);
      }

      protected void gxLoad_29( int A805ContagemResultado_ContratadaOrigemCod )
      {
         /* Using cursor T004124 */
         pr_default.execute(22, new Object[] {n805ContagemResultado_ContratadaOrigemCod, A805ContagemResultado_ContratadaOrigemCod});
         if ( (pr_default.getStatus(22) == 101) )
         {
            if ( ! ( (0==A805ContagemResultado_ContratadaOrigemCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Contagem Resutlado_Contratada Origem'.", "ForeignKeyNotFound", 1, "CONTAGEMRESULTADO_CONTRATADAORIGEMCOD");
               AnyError = 1;
               GX_FocusControl = dynContagemResultado_ContratadaOrigemCod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(22) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(22);
      }

      protected void gxLoad_25( int A454ContagemResultado_ContadorFSCod )
      {
         /* Using cursor T004125 */
         pr_default.execute(23, new Object[] {n454ContagemResultado_ContadorFSCod, A454ContagemResultado_ContadorFSCod});
         if ( (pr_default.getStatus(23) == 101) )
         {
            if ( ! ( (0==A454ContagemResultado_ContadorFSCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Contagem Resultado_Usuario Contador Fab. de Soft'.", "ForeignKeyNotFound", 1, "CONTAGEMRESULTADO_CONTADORFSCOD");
               AnyError = 1;
               GX_FocusControl = dynContagemResultado_ContadorFSCod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(23) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(23);
      }

      protected void gxLoad_26( int A890ContagemResultado_Responsavel )
      {
         /* Using cursor T004126 */
         pr_default.execute(24, new Object[] {n890ContagemResultado_Responsavel, A890ContagemResultado_Responsavel});
         if ( (pr_default.getStatus(24) == 101) )
         {
            if ( ! ( (0==A890ContagemResultado_Responsavel) ) )
            {
               GX_msglist.addItem("N�o existe 'Contagem Resultado_Usuario Responsavel'.", "ForeignKeyNotFound", 1, "CONTAGEMRESULTADO_RESPONSAVEL");
               AnyError = 1;
               GX_FocusControl = edtContagemResultado_Responsavel_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(24) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(24);
      }

      protected void gxLoad_33( int A1043ContagemResultado_LiqLogCod )
      {
         /* Using cursor T004127 */
         pr_default.execute(25, new Object[] {n1043ContagemResultado_LiqLogCod, A1043ContagemResultado_LiqLogCod});
         if ( (pr_default.getStatus(25) == 101) )
         {
            if ( ! ( (0==A1043ContagemResultado_LiqLogCod) ) )
            {
               GX_msglist.addItem("N�o existe 'ST_Contagem Resultado_Contagem Resultado Liq Log'.", "ForeignKeyNotFound", 1, "CONTAGEMRESULTADO_LIQLOGCOD");
               AnyError = 1;
               GX_FocusControl = edtContagemResultado_LiqLogCod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(25) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(25);
      }

      protected void gxLoad_24( int A146Modulo_Codigo )
      {
         /* Using cursor T004128 */
         pr_default.execute(26, new Object[] {n146Modulo_Codigo, A146Modulo_Codigo});
         if ( (pr_default.getStatus(26) == 101) )
         {
            if ( ! ( (0==A146Modulo_Codigo) ) )
            {
               GX_msglist.addItem("N�o existe 'Modulo'.", "ForeignKeyNotFound", 1, "MODULO_CODIGO");
               AnyError = 1;
               GX_FocusControl = dynModulo_Codigo_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(26) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(26);
      }

      protected void gxLoad_28( int A490ContagemResultado_ContratadaCod )
      {
         /* Using cursor T004129 */
         pr_default.execute(27, new Object[] {n490ContagemResultado_ContratadaCod, A490ContagemResultado_ContratadaCod});
         if ( (pr_default.getStatus(27) == 101) )
         {
            if ( ! ( (0==A490ContagemResultado_ContratadaCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Contagem Resultado_Contratada'.", "ForeignKeyNotFound", 1, "CONTAGEMRESULTADO_CONTRATADACOD");
               AnyError = 1;
               GX_FocusControl = dynContagemResultado_ContratadaCod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(27) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(27);
      }

      protected void gxLoad_30( int A489ContagemResultado_SistemaCod )
      {
         /* Using cursor T004130 */
         pr_default.execute(28, new Object[] {n489ContagemResultado_SistemaCod, A489ContagemResultado_SistemaCod});
         if ( (pr_default.getStatus(28) == 101) )
         {
            if ( ! ( (0==A489ContagemResultado_SistemaCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Contagem Resultado_Sistema'.", "ForeignKeyNotFound", 1, "CONTAGEMRESULTADO_SISTEMACOD");
               AnyError = 1;
               GX_FocusControl = edtContagemResultado_SistemaCod_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(28) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(28);
      }

      protected void GetKey4169( )
      {
         /* Using cursor T004131 */
         pr_default.execute(29, new Object[] {A456ContagemResultado_Codigo});
         if ( (pr_default.getStatus(29) != 101) )
         {
            RcdFound69 = 1;
         }
         else
         {
            RcdFound69 = 0;
         }
         pr_default.close(29);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T00413 */
         pr_default.execute(1, new Object[] {A456ContagemResultado_Codigo});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM4169( 23) ;
            RcdFound69 = 1;
            A456ContagemResultado_Codigo = T00413_A456ContagemResultado_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A456ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A456ContagemResultado_Codigo), 6, 0)));
            A508ContagemResultado_Owner = T00413_A508ContagemResultado_Owner[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A508ContagemResultado_Owner", StringUtil.LTrim( StringUtil.Str( (decimal)(A508ContagemResultado_Owner), 6, 0)));
            A494ContagemResultado_Descricao = T00413_A494ContagemResultado_Descricao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A494ContagemResultado_Descricao", A494ContagemResultado_Descricao);
            n494ContagemResultado_Descricao = T00413_n494ContagemResultado_Descricao[0];
            A514ContagemResultado_Observacao = T00413_A514ContagemResultado_Observacao[0];
            n514ContagemResultado_Observacao = T00413_n514ContagemResultado_Observacao[0];
            A472ContagemResultado_DataEntrega = T00413_A472ContagemResultado_DataEntrega[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A472ContagemResultado_DataEntrega", context.localUtil.Format(A472ContagemResultado_DataEntrega, "99/99/99"));
            n472ContagemResultado_DataEntrega = T00413_n472ContagemResultado_DataEntrega[0];
            A912ContagemResultado_HoraEntrega = T00413_A912ContagemResultado_HoraEntrega[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A912ContagemResultado_HoraEntrega", context.localUtil.TToC( A912ContagemResultado_HoraEntrega, 0, 5, 0, 3, "/", ":", " "));
            n912ContagemResultado_HoraEntrega = T00413_n912ContagemResultado_HoraEntrega[0];
            A1351ContagemResultado_DataPrevista = T00413_A1351ContagemResultado_DataPrevista[0];
            n1351ContagemResultado_DataPrevista = T00413_n1351ContagemResultado_DataPrevista[0];
            A1452ContagemResultado_SS = T00413_A1452ContagemResultado_SS[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1452ContagemResultado_SS", StringUtil.LTrim( StringUtil.Str( (decimal)(A1452ContagemResultado_SS), 8, 0)));
            n1452ContagemResultado_SS = T00413_n1452ContagemResultado_SS[0];
            A493ContagemResultado_DemandaFM = T00413_A493ContagemResultado_DemandaFM[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A493ContagemResultado_DemandaFM", A493ContagemResultado_DemandaFM);
            n493ContagemResultado_DemandaFM = T00413_n493ContagemResultado_DemandaFM[0];
            A1584ContagemResultado_UOOwner = T00413_A1584ContagemResultado_UOOwner[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1584ContagemResultado_UOOwner", StringUtil.LTrim( StringUtil.Str( (decimal)(A1584ContagemResultado_UOOwner), 6, 0)));
            n1584ContagemResultado_UOOwner = T00413_n1584ContagemResultado_UOOwner[0];
            A1515ContagemResultado_Evento = T00413_A1515ContagemResultado_Evento[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1515ContagemResultado_Evento", StringUtil.LTrim( StringUtil.Str( (decimal)(A1515ContagemResultado_Evento), 2, 0)));
            n1515ContagemResultado_Evento = T00413_n1515ContagemResultado_Evento[0];
            A1583ContagemResultado_TipoRegistro = T00413_A1583ContagemResultado_TipoRegistro[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1583ContagemResultado_TipoRegistro", StringUtil.LTrim( StringUtil.Str( (decimal)(A1583ContagemResultado_TipoRegistro), 4, 0)));
            A1585ContagemResultado_Referencia = T00413_A1585ContagemResultado_Referencia[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1585ContagemResultado_Referencia", A1585ContagemResultado_Referencia);
            n1585ContagemResultado_Referencia = T00413_n1585ContagemResultado_Referencia[0];
            A1586ContagemResultado_Restricoes = T00413_A1586ContagemResultado_Restricoes[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1586ContagemResultado_Restricoes", A1586ContagemResultado_Restricoes);
            n1586ContagemResultado_Restricoes = T00413_n1586ContagemResultado_Restricoes[0];
            A1587ContagemResultado_PrioridadePrevista = T00413_A1587ContagemResultado_PrioridadePrevista[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1587ContagemResultado_PrioridadePrevista", A1587ContagemResultado_PrioridadePrevista);
            n1587ContagemResultado_PrioridadePrevista = T00413_n1587ContagemResultado_PrioridadePrevista[0];
            A484ContagemResultado_StatusDmn = T00413_A484ContagemResultado_StatusDmn[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A484ContagemResultado_StatusDmn", A484ContagemResultado_StatusDmn);
            n484ContagemResultado_StatusDmn = T00413_n484ContagemResultado_StatusDmn[0];
            A471ContagemResultado_DataDmn = T00413_A471ContagemResultado_DataDmn[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A471ContagemResultado_DataDmn", context.localUtil.Format(A471ContagemResultado_DataDmn, "99/99/99"));
            A1044ContagemResultado_FncUsrCod = T00413_A1044ContagemResultado_FncUsrCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1044ContagemResultado_FncUsrCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1044ContagemResultado_FncUsrCod), 6, 0)));
            n1044ContagemResultado_FncUsrCod = T00413_n1044ContagemResultado_FncUsrCod[0];
            A1227ContagemResultado_PrazoInicialDias = T00413_A1227ContagemResultado_PrazoInicialDias[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1227ContagemResultado_PrazoInicialDias", StringUtil.LTrim( StringUtil.Str( (decimal)(A1227ContagemResultado_PrazoInicialDias), 4, 0)));
            n1227ContagemResultado_PrazoInicialDias = T00413_n1227ContagemResultado_PrazoInicialDias[0];
            A1350ContagemResultado_DataCadastro = T00413_A1350ContagemResultado_DataCadastro[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1350ContagemResultado_DataCadastro", context.localUtil.TToC( A1350ContagemResultado_DataCadastro, 8, 5, 0, 3, "/", ":", " "));
            n1350ContagemResultado_DataCadastro = T00413_n1350ContagemResultado_DataCadastro[0];
            A1173ContagemResultado_OSManual = T00413_A1173ContagemResultado_OSManual[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1173ContagemResultado_OSManual", A1173ContagemResultado_OSManual);
            n1173ContagemResultado_OSManual = T00413_n1173ContagemResultado_OSManual[0];
            A598ContagemResultado_Baseline = T00413_A598ContagemResultado_Baseline[0];
            n598ContagemResultado_Baseline = T00413_n598ContagemResultado_Baseline[0];
            A485ContagemResultado_EhValidacao = T00413_A485ContagemResultado_EhValidacao[0];
            n485ContagemResultado_EhValidacao = T00413_n485ContagemResultado_EhValidacao[0];
            A146Modulo_Codigo = T00413_A146Modulo_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A146Modulo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A146Modulo_Codigo), 6, 0)));
            n146Modulo_Codigo = T00413_n146Modulo_Codigo[0];
            A454ContagemResultado_ContadorFSCod = T00413_A454ContagemResultado_ContadorFSCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A454ContagemResultado_ContadorFSCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A454ContagemResultado_ContadorFSCod), 6, 0)));
            n454ContagemResultado_ContadorFSCod = T00413_n454ContagemResultado_ContadorFSCod[0];
            A890ContagemResultado_Responsavel = T00413_A890ContagemResultado_Responsavel[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A890ContagemResultado_Responsavel", StringUtil.LTrim( StringUtil.Str( (decimal)(A890ContagemResultado_Responsavel), 6, 0)));
            n890ContagemResultado_Responsavel = T00413_n890ContagemResultado_Responsavel[0];
            A468ContagemResultado_NaoCnfDmnCod = T00413_A468ContagemResultado_NaoCnfDmnCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A468ContagemResultado_NaoCnfDmnCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A468ContagemResultado_NaoCnfDmnCod), 6, 0)));
            n468ContagemResultado_NaoCnfDmnCod = T00413_n468ContagemResultado_NaoCnfDmnCod[0];
            A490ContagemResultado_ContratadaCod = T00413_A490ContagemResultado_ContratadaCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A490ContagemResultado_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A490ContagemResultado_ContratadaCod), 6, 0)));
            n490ContagemResultado_ContratadaCod = T00413_n490ContagemResultado_ContratadaCod[0];
            A805ContagemResultado_ContratadaOrigemCod = T00413_A805ContagemResultado_ContratadaOrigemCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A805ContagemResultado_ContratadaOrigemCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A805ContagemResultado_ContratadaOrigemCod), 6, 0)));
            n805ContagemResultado_ContratadaOrigemCod = T00413_n805ContagemResultado_ContratadaOrigemCod[0];
            A489ContagemResultado_SistemaCod = T00413_A489ContagemResultado_SistemaCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A489ContagemResultado_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A489ContagemResultado_SistemaCod), 6, 0)));
            n489ContagemResultado_SistemaCod = T00413_n489ContagemResultado_SistemaCod[0];
            A597ContagemResultado_LoteAceiteCod = T00413_A597ContagemResultado_LoteAceiteCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A597ContagemResultado_LoteAceiteCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A597ContagemResultado_LoteAceiteCod), 6, 0)));
            n597ContagemResultado_LoteAceiteCod = T00413_n597ContagemResultado_LoteAceiteCod[0];
            A1636ContagemResultado_ServicoSS = T00413_A1636ContagemResultado_ServicoSS[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1636ContagemResultado_ServicoSS", StringUtil.LTrim( StringUtil.Str( (decimal)(A1636ContagemResultado_ServicoSS), 6, 0)));
            n1636ContagemResultado_ServicoSS = T00413_n1636ContagemResultado_ServicoSS[0];
            A1043ContagemResultado_LiqLogCod = T00413_A1043ContagemResultado_LiqLogCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1043ContagemResultado_LiqLogCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1043ContagemResultado_LiqLogCod), 6, 0)));
            n1043ContagemResultado_LiqLogCod = T00413_n1043ContagemResultado_LiqLogCod[0];
            A1553ContagemResultado_CntSrvCod = T00413_A1553ContagemResultado_CntSrvCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1553ContagemResultado_CntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1553ContagemResultado_CntSrvCod), 6, 0)));
            n1553ContagemResultado_CntSrvCod = T00413_n1553ContagemResultado_CntSrvCod[0];
            A602ContagemResultado_OSVinculada = T00413_A602ContagemResultado_OSVinculada[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A602ContagemResultado_OSVinculada", StringUtil.LTrim( StringUtil.Str( (decimal)(A602ContagemResultado_OSVinculada), 6, 0)));
            n602ContagemResultado_OSVinculada = T00413_n602ContagemResultado_OSVinculada[0];
            Z456ContagemResultado_Codigo = A456ContagemResultado_Codigo;
            sMode69 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            standaloneModal( ) ;
            Load4169( ) ;
            if ( AnyError == 1 )
            {
               RcdFound69 = 0;
               InitializeNonKey4169( ) ;
            }
            Gx_mode = sMode69;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            RcdFound69 = 0;
            InitializeNonKey4169( ) ;
            sMode69 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            standaloneModal( ) ;
            Gx_mode = sMode69;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey4169( ) ;
         if ( RcdFound69 == 0 )
         {
            Gx_mode = "INS";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound69 = 0;
         /* Using cursor T004132 */
         pr_default.execute(30, new Object[] {A456ContagemResultado_Codigo});
         if ( (pr_default.getStatus(30) != 101) )
         {
            while ( (pr_default.getStatus(30) != 101) && ( ( T004132_A456ContagemResultado_Codigo[0] < A456ContagemResultado_Codigo ) ) )
            {
               pr_default.readNext(30);
            }
            if ( (pr_default.getStatus(30) != 101) && ( ( T004132_A456ContagemResultado_Codigo[0] > A456ContagemResultado_Codigo ) ) )
            {
               A456ContagemResultado_Codigo = T004132_A456ContagemResultado_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A456ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A456ContagemResultado_Codigo), 6, 0)));
               RcdFound69 = 1;
            }
         }
         pr_default.close(30);
      }

      protected void move_previous( )
      {
         RcdFound69 = 0;
         /* Using cursor T004133 */
         pr_default.execute(31, new Object[] {A456ContagemResultado_Codigo});
         if ( (pr_default.getStatus(31) != 101) )
         {
            while ( (pr_default.getStatus(31) != 101) && ( ( T004133_A456ContagemResultado_Codigo[0] > A456ContagemResultado_Codigo ) ) )
            {
               pr_default.readNext(31);
            }
            if ( (pr_default.getStatus(31) != 101) && ( ( T004133_A456ContagemResultado_Codigo[0] < A456ContagemResultado_Codigo ) ) )
            {
               A456ContagemResultado_Codigo = T004133_A456ContagemResultado_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A456ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A456ContagemResultado_Codigo), 6, 0)));
               RcdFound69 = 1;
            }
         }
         pr_default.close(31);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey4169( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = edtContagemResultado_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert4169( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound69 == 1 )
            {
               if ( A456ContagemResultado_Codigo != Z456ContagemResultado_Codigo )
               {
                  A456ContagemResultado_Codigo = Z456ContagemResultado_Codigo;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A456ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A456ContagemResultado_Codigo), 6, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "CONTAGEMRESULTADO_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtContagemResultado_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = edtContagemResultado_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  Gx_mode = "UPD";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  /* Update record */
                  Update4169( ) ;
                  GX_FocusControl = edtContagemResultado_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( A456ContagemResultado_Codigo != Z456ContagemResultado_Codigo )
               {
                  Gx_mode = "INS";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  /* Insert record */
                  GX_FocusControl = edtContagemResultado_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert4169( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "CONTAGEMRESULTADO_CODIGO");
                     AnyError = 1;
                     GX_FocusControl = edtContagemResultado_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     Gx_mode = "INS";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     /* Insert record */
                     GX_FocusControl = edtContagemResultado_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert4169( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
      }

      protected void btn_delete( )
      {
         if ( A456ContagemResultado_Codigo != Z456ContagemResultado_Codigo )
         {
            A456ContagemResultado_Codigo = Z456ContagemResultado_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A456ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A456ContagemResultado_Codigo), 6, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "CONTAGEMRESULTADO_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtContagemResultado_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = edtContagemResultado_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            getByPrimaryKey( ) ;
         }
         CloseOpenCursors();
      }

      protected void btn_get( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         getEqualNoModal( ) ;
         if ( RcdFound69 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "CONTAGEMRESULTADO_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtContagemResultado_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         GX_FocusControl = edtContagemResultado_Owner_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_first( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         ScanStart4169( ) ;
         if ( RcdFound69 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtContagemResultado_Owner_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         ScanEnd4169( ) ;
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_previous( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         move_previous( ) ;
         if ( RcdFound69 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtContagemResultado_Owner_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_next( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         move_next( ) ;
         if ( RcdFound69 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtContagemResultado_Owner_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_last( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         ScanStart4169( ) ;
         if ( RcdFound69 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            while ( RcdFound69 != 0 )
            {
               ScanNext4169( ) ;
            }
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtContagemResultado_Owner_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         ScanEnd4169( ) ;
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_select( )
      {
         getEqualNoModal( ) ;
      }

      protected void CheckOptimisticConcurrency4169( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T00412 */
            pr_default.execute(0, new Object[] {A456ContagemResultado_Codigo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ContagemResultado"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            Gx_longc = false;
            if ( (pr_default.getStatus(0) == 101) || ( Z508ContagemResultado_Owner != T00412_A508ContagemResultado_Owner[0] ) || ( StringUtil.StrCmp(Z494ContagemResultado_Descricao, T00412_A494ContagemResultado_Descricao[0]) != 0 ) || ( Z472ContagemResultado_DataEntrega != T00412_A472ContagemResultado_DataEntrega[0] ) || ( Z912ContagemResultado_HoraEntrega != T00412_A912ContagemResultado_HoraEntrega[0] ) || ( Z1351ContagemResultado_DataPrevista != T00412_A1351ContagemResultado_DataPrevista[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z1452ContagemResultado_SS != T00412_A1452ContagemResultado_SS[0] ) || ( StringUtil.StrCmp(Z493ContagemResultado_DemandaFM, T00412_A493ContagemResultado_DemandaFM[0]) != 0 ) || ( Z1584ContagemResultado_UOOwner != T00412_A1584ContagemResultado_UOOwner[0] ) || ( Z1515ContagemResultado_Evento != T00412_A1515ContagemResultado_Evento[0] ) || ( Z1583ContagemResultado_TipoRegistro != T00412_A1583ContagemResultado_TipoRegistro[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( StringUtil.StrCmp(Z1585ContagemResultado_Referencia, T00412_A1585ContagemResultado_Referencia[0]) != 0 ) || ( StringUtil.StrCmp(Z1586ContagemResultado_Restricoes, T00412_A1586ContagemResultado_Restricoes[0]) != 0 ) || ( StringUtil.StrCmp(Z1587ContagemResultado_PrioridadePrevista, T00412_A1587ContagemResultado_PrioridadePrevista[0]) != 0 ) || ( StringUtil.StrCmp(Z484ContagemResultado_StatusDmn, T00412_A484ContagemResultado_StatusDmn[0]) != 0 ) || ( Z471ContagemResultado_DataDmn != T00412_A471ContagemResultado_DataDmn[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z1044ContagemResultado_FncUsrCod != T00412_A1044ContagemResultado_FncUsrCod[0] ) || ( Z1227ContagemResultado_PrazoInicialDias != T00412_A1227ContagemResultado_PrazoInicialDias[0] ) || ( Z1350ContagemResultado_DataCadastro != T00412_A1350ContagemResultado_DataCadastro[0] ) || ( Z1173ContagemResultado_OSManual != T00412_A1173ContagemResultado_OSManual[0] ) || ( Z598ContagemResultado_Baseline != T00412_A598ContagemResultado_Baseline[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z485ContagemResultado_EhValidacao != T00412_A485ContagemResultado_EhValidacao[0] ) || ( Z146Modulo_Codigo != T00412_A146Modulo_Codigo[0] ) || ( Z454ContagemResultado_ContadorFSCod != T00412_A454ContagemResultado_ContadorFSCod[0] ) || ( Z890ContagemResultado_Responsavel != T00412_A890ContagemResultado_Responsavel[0] ) || ( Z468ContagemResultado_NaoCnfDmnCod != T00412_A468ContagemResultado_NaoCnfDmnCod[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z490ContagemResultado_ContratadaCod != T00412_A490ContagemResultado_ContratadaCod[0] ) || ( Z805ContagemResultado_ContratadaOrigemCod != T00412_A805ContagemResultado_ContratadaOrigemCod[0] ) || ( Z489ContagemResultado_SistemaCod != T00412_A489ContagemResultado_SistemaCod[0] ) || ( Z597ContagemResultado_LoteAceiteCod != T00412_A597ContagemResultado_LoteAceiteCod[0] ) || ( Z1636ContagemResultado_ServicoSS != T00412_A1636ContagemResultado_ServicoSS[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z1043ContagemResultado_LiqLogCod != T00412_A1043ContagemResultado_LiqLogCod[0] ) || ( Z1553ContagemResultado_CntSrvCod != T00412_A1553ContagemResultado_CntSrvCod[0] ) || ( Z602ContagemResultado_OSVinculada != T00412_A602ContagemResultado_OSVinculada[0] ) )
            {
               if ( Z508ContagemResultado_Owner != T00412_A508ContagemResultado_Owner[0] )
               {
                  GXUtil.WriteLog("solicitacaoservico:[seudo value changed for attri]"+"ContagemResultado_Owner");
                  GXUtil.WriteLogRaw("Old: ",Z508ContagemResultado_Owner);
                  GXUtil.WriteLogRaw("Current: ",T00412_A508ContagemResultado_Owner[0]);
               }
               if ( StringUtil.StrCmp(Z494ContagemResultado_Descricao, T00412_A494ContagemResultado_Descricao[0]) != 0 )
               {
                  GXUtil.WriteLog("solicitacaoservico:[seudo value changed for attri]"+"ContagemResultado_Descricao");
                  GXUtil.WriteLogRaw("Old: ",Z494ContagemResultado_Descricao);
                  GXUtil.WriteLogRaw("Current: ",T00412_A494ContagemResultado_Descricao[0]);
               }
               if ( Z472ContagemResultado_DataEntrega != T00412_A472ContagemResultado_DataEntrega[0] )
               {
                  GXUtil.WriteLog("solicitacaoservico:[seudo value changed for attri]"+"ContagemResultado_DataEntrega");
                  GXUtil.WriteLogRaw("Old: ",Z472ContagemResultado_DataEntrega);
                  GXUtil.WriteLogRaw("Current: ",T00412_A472ContagemResultado_DataEntrega[0]);
               }
               if ( Z912ContagemResultado_HoraEntrega != T00412_A912ContagemResultado_HoraEntrega[0] )
               {
                  GXUtil.WriteLog("solicitacaoservico:[seudo value changed for attri]"+"ContagemResultado_HoraEntrega");
                  GXUtil.WriteLogRaw("Old: ",Z912ContagemResultado_HoraEntrega);
                  GXUtil.WriteLogRaw("Current: ",T00412_A912ContagemResultado_HoraEntrega[0]);
               }
               if ( Z1351ContagemResultado_DataPrevista != T00412_A1351ContagemResultado_DataPrevista[0] )
               {
                  GXUtil.WriteLog("solicitacaoservico:[seudo value changed for attri]"+"ContagemResultado_DataPrevista");
                  GXUtil.WriteLogRaw("Old: ",Z1351ContagemResultado_DataPrevista);
                  GXUtil.WriteLogRaw("Current: ",T00412_A1351ContagemResultado_DataPrevista[0]);
               }
               if ( Z1452ContagemResultado_SS != T00412_A1452ContagemResultado_SS[0] )
               {
                  GXUtil.WriteLog("solicitacaoservico:[seudo value changed for attri]"+"ContagemResultado_SS");
                  GXUtil.WriteLogRaw("Old: ",Z1452ContagemResultado_SS);
                  GXUtil.WriteLogRaw("Current: ",T00412_A1452ContagemResultado_SS[0]);
               }
               if ( StringUtil.StrCmp(Z493ContagemResultado_DemandaFM, T00412_A493ContagemResultado_DemandaFM[0]) != 0 )
               {
                  GXUtil.WriteLog("solicitacaoservico:[seudo value changed for attri]"+"ContagemResultado_DemandaFM");
                  GXUtil.WriteLogRaw("Old: ",Z493ContagemResultado_DemandaFM);
                  GXUtil.WriteLogRaw("Current: ",T00412_A493ContagemResultado_DemandaFM[0]);
               }
               if ( Z1584ContagemResultado_UOOwner != T00412_A1584ContagemResultado_UOOwner[0] )
               {
                  GXUtil.WriteLog("solicitacaoservico:[seudo value changed for attri]"+"ContagemResultado_UOOwner");
                  GXUtil.WriteLogRaw("Old: ",Z1584ContagemResultado_UOOwner);
                  GXUtil.WriteLogRaw("Current: ",T00412_A1584ContagemResultado_UOOwner[0]);
               }
               if ( Z1515ContagemResultado_Evento != T00412_A1515ContagemResultado_Evento[0] )
               {
                  GXUtil.WriteLog("solicitacaoservico:[seudo value changed for attri]"+"ContagemResultado_Evento");
                  GXUtil.WriteLogRaw("Old: ",Z1515ContagemResultado_Evento);
                  GXUtil.WriteLogRaw("Current: ",T00412_A1515ContagemResultado_Evento[0]);
               }
               if ( Z1583ContagemResultado_TipoRegistro != T00412_A1583ContagemResultado_TipoRegistro[0] )
               {
                  GXUtil.WriteLog("solicitacaoservico:[seudo value changed for attri]"+"ContagemResultado_TipoRegistro");
                  GXUtil.WriteLogRaw("Old: ",Z1583ContagemResultado_TipoRegistro);
                  GXUtil.WriteLogRaw("Current: ",T00412_A1583ContagemResultado_TipoRegistro[0]);
               }
               if ( StringUtil.StrCmp(Z1585ContagemResultado_Referencia, T00412_A1585ContagemResultado_Referencia[0]) != 0 )
               {
                  GXUtil.WriteLog("solicitacaoservico:[seudo value changed for attri]"+"ContagemResultado_Referencia");
                  GXUtil.WriteLogRaw("Old: ",Z1585ContagemResultado_Referencia);
                  GXUtil.WriteLogRaw("Current: ",T00412_A1585ContagemResultado_Referencia[0]);
               }
               if ( StringUtil.StrCmp(Z1586ContagemResultado_Restricoes, T00412_A1586ContagemResultado_Restricoes[0]) != 0 )
               {
                  GXUtil.WriteLog("solicitacaoservico:[seudo value changed for attri]"+"ContagemResultado_Restricoes");
                  GXUtil.WriteLogRaw("Old: ",Z1586ContagemResultado_Restricoes);
                  GXUtil.WriteLogRaw("Current: ",T00412_A1586ContagemResultado_Restricoes[0]);
               }
               if ( StringUtil.StrCmp(Z1587ContagemResultado_PrioridadePrevista, T00412_A1587ContagemResultado_PrioridadePrevista[0]) != 0 )
               {
                  GXUtil.WriteLog("solicitacaoservico:[seudo value changed for attri]"+"ContagemResultado_PrioridadePrevista");
                  GXUtil.WriteLogRaw("Old: ",Z1587ContagemResultado_PrioridadePrevista);
                  GXUtil.WriteLogRaw("Current: ",T00412_A1587ContagemResultado_PrioridadePrevista[0]);
               }
               if ( StringUtil.StrCmp(Z484ContagemResultado_StatusDmn, T00412_A484ContagemResultado_StatusDmn[0]) != 0 )
               {
                  GXUtil.WriteLog("solicitacaoservico:[seudo value changed for attri]"+"ContagemResultado_StatusDmn");
                  GXUtil.WriteLogRaw("Old: ",Z484ContagemResultado_StatusDmn);
                  GXUtil.WriteLogRaw("Current: ",T00412_A484ContagemResultado_StatusDmn[0]);
               }
               if ( Z471ContagemResultado_DataDmn != T00412_A471ContagemResultado_DataDmn[0] )
               {
                  GXUtil.WriteLog("solicitacaoservico:[seudo value changed for attri]"+"ContagemResultado_DataDmn");
                  GXUtil.WriteLogRaw("Old: ",Z471ContagemResultado_DataDmn);
                  GXUtil.WriteLogRaw("Current: ",T00412_A471ContagemResultado_DataDmn[0]);
               }
               if ( Z1044ContagemResultado_FncUsrCod != T00412_A1044ContagemResultado_FncUsrCod[0] )
               {
                  GXUtil.WriteLog("solicitacaoservico:[seudo value changed for attri]"+"ContagemResultado_FncUsrCod");
                  GXUtil.WriteLogRaw("Old: ",Z1044ContagemResultado_FncUsrCod);
                  GXUtil.WriteLogRaw("Current: ",T00412_A1044ContagemResultado_FncUsrCod[0]);
               }
               if ( Z1227ContagemResultado_PrazoInicialDias != T00412_A1227ContagemResultado_PrazoInicialDias[0] )
               {
                  GXUtil.WriteLog("solicitacaoservico:[seudo value changed for attri]"+"ContagemResultado_PrazoInicialDias");
                  GXUtil.WriteLogRaw("Old: ",Z1227ContagemResultado_PrazoInicialDias);
                  GXUtil.WriteLogRaw("Current: ",T00412_A1227ContagemResultado_PrazoInicialDias[0]);
               }
               if ( Z1350ContagemResultado_DataCadastro != T00412_A1350ContagemResultado_DataCadastro[0] )
               {
                  GXUtil.WriteLog("solicitacaoservico:[seudo value changed for attri]"+"ContagemResultado_DataCadastro");
                  GXUtil.WriteLogRaw("Old: ",Z1350ContagemResultado_DataCadastro);
                  GXUtil.WriteLogRaw("Current: ",T00412_A1350ContagemResultado_DataCadastro[0]);
               }
               if ( Z1173ContagemResultado_OSManual != T00412_A1173ContagemResultado_OSManual[0] )
               {
                  GXUtil.WriteLog("solicitacaoservico:[seudo value changed for attri]"+"ContagemResultado_OSManual");
                  GXUtil.WriteLogRaw("Old: ",Z1173ContagemResultado_OSManual);
                  GXUtil.WriteLogRaw("Current: ",T00412_A1173ContagemResultado_OSManual[0]);
               }
               if ( Z598ContagemResultado_Baseline != T00412_A598ContagemResultado_Baseline[0] )
               {
                  GXUtil.WriteLog("solicitacaoservico:[seudo value changed for attri]"+"ContagemResultado_Baseline");
                  GXUtil.WriteLogRaw("Old: ",Z598ContagemResultado_Baseline);
                  GXUtil.WriteLogRaw("Current: ",T00412_A598ContagemResultado_Baseline[0]);
               }
               if ( Z485ContagemResultado_EhValidacao != T00412_A485ContagemResultado_EhValidacao[0] )
               {
                  GXUtil.WriteLog("solicitacaoservico:[seudo value changed for attri]"+"ContagemResultado_EhValidacao");
                  GXUtil.WriteLogRaw("Old: ",Z485ContagemResultado_EhValidacao);
                  GXUtil.WriteLogRaw("Current: ",T00412_A485ContagemResultado_EhValidacao[0]);
               }
               if ( Z146Modulo_Codigo != T00412_A146Modulo_Codigo[0] )
               {
                  GXUtil.WriteLog("solicitacaoservico:[seudo value changed for attri]"+"Modulo_Codigo");
                  GXUtil.WriteLogRaw("Old: ",Z146Modulo_Codigo);
                  GXUtil.WriteLogRaw("Current: ",T00412_A146Modulo_Codigo[0]);
               }
               if ( Z454ContagemResultado_ContadorFSCod != T00412_A454ContagemResultado_ContadorFSCod[0] )
               {
                  GXUtil.WriteLog("solicitacaoservico:[seudo value changed for attri]"+"ContagemResultado_ContadorFSCod");
                  GXUtil.WriteLogRaw("Old: ",Z454ContagemResultado_ContadorFSCod);
                  GXUtil.WriteLogRaw("Current: ",T00412_A454ContagemResultado_ContadorFSCod[0]);
               }
               if ( Z890ContagemResultado_Responsavel != T00412_A890ContagemResultado_Responsavel[0] )
               {
                  GXUtil.WriteLog("solicitacaoservico:[seudo value changed for attri]"+"ContagemResultado_Responsavel");
                  GXUtil.WriteLogRaw("Old: ",Z890ContagemResultado_Responsavel);
                  GXUtil.WriteLogRaw("Current: ",T00412_A890ContagemResultado_Responsavel[0]);
               }
               if ( Z468ContagemResultado_NaoCnfDmnCod != T00412_A468ContagemResultado_NaoCnfDmnCod[0] )
               {
                  GXUtil.WriteLog("solicitacaoservico:[seudo value changed for attri]"+"ContagemResultado_NaoCnfDmnCod");
                  GXUtil.WriteLogRaw("Old: ",Z468ContagemResultado_NaoCnfDmnCod);
                  GXUtil.WriteLogRaw("Current: ",T00412_A468ContagemResultado_NaoCnfDmnCod[0]);
               }
               if ( Z490ContagemResultado_ContratadaCod != T00412_A490ContagemResultado_ContratadaCod[0] )
               {
                  GXUtil.WriteLog("solicitacaoservico:[seudo value changed for attri]"+"ContagemResultado_ContratadaCod");
                  GXUtil.WriteLogRaw("Old: ",Z490ContagemResultado_ContratadaCod);
                  GXUtil.WriteLogRaw("Current: ",T00412_A490ContagemResultado_ContratadaCod[0]);
               }
               if ( Z805ContagemResultado_ContratadaOrigemCod != T00412_A805ContagemResultado_ContratadaOrigemCod[0] )
               {
                  GXUtil.WriteLog("solicitacaoservico:[seudo value changed for attri]"+"ContagemResultado_ContratadaOrigemCod");
                  GXUtil.WriteLogRaw("Old: ",Z805ContagemResultado_ContratadaOrigemCod);
                  GXUtil.WriteLogRaw("Current: ",T00412_A805ContagemResultado_ContratadaOrigemCod[0]);
               }
               if ( Z489ContagemResultado_SistemaCod != T00412_A489ContagemResultado_SistemaCod[0] )
               {
                  GXUtil.WriteLog("solicitacaoservico:[seudo value changed for attri]"+"ContagemResultado_SistemaCod");
                  GXUtil.WriteLogRaw("Old: ",Z489ContagemResultado_SistemaCod);
                  GXUtil.WriteLogRaw("Current: ",T00412_A489ContagemResultado_SistemaCod[0]);
               }
               if ( Z597ContagemResultado_LoteAceiteCod != T00412_A597ContagemResultado_LoteAceiteCod[0] )
               {
                  GXUtil.WriteLog("solicitacaoservico:[seudo value changed for attri]"+"ContagemResultado_LoteAceiteCod");
                  GXUtil.WriteLogRaw("Old: ",Z597ContagemResultado_LoteAceiteCod);
                  GXUtil.WriteLogRaw("Current: ",T00412_A597ContagemResultado_LoteAceiteCod[0]);
               }
               if ( Z1636ContagemResultado_ServicoSS != T00412_A1636ContagemResultado_ServicoSS[0] )
               {
                  GXUtil.WriteLog("solicitacaoservico:[seudo value changed for attri]"+"ContagemResultado_ServicoSS");
                  GXUtil.WriteLogRaw("Old: ",Z1636ContagemResultado_ServicoSS);
                  GXUtil.WriteLogRaw("Current: ",T00412_A1636ContagemResultado_ServicoSS[0]);
               }
               if ( Z1043ContagemResultado_LiqLogCod != T00412_A1043ContagemResultado_LiqLogCod[0] )
               {
                  GXUtil.WriteLog("solicitacaoservico:[seudo value changed for attri]"+"ContagemResultado_LiqLogCod");
                  GXUtil.WriteLogRaw("Old: ",Z1043ContagemResultado_LiqLogCod);
                  GXUtil.WriteLogRaw("Current: ",T00412_A1043ContagemResultado_LiqLogCod[0]);
               }
               if ( Z1553ContagemResultado_CntSrvCod != T00412_A1553ContagemResultado_CntSrvCod[0] )
               {
                  GXUtil.WriteLog("solicitacaoservico:[seudo value changed for attri]"+"ContagemResultado_CntSrvCod");
                  GXUtil.WriteLogRaw("Old: ",Z1553ContagemResultado_CntSrvCod);
                  GXUtil.WriteLogRaw("Current: ",T00412_A1553ContagemResultado_CntSrvCod[0]);
               }
               if ( Z602ContagemResultado_OSVinculada != T00412_A602ContagemResultado_OSVinculada[0] )
               {
                  GXUtil.WriteLog("solicitacaoservico:[seudo value changed for attri]"+"ContagemResultado_OSVinculada");
                  GXUtil.WriteLogRaw("Old: ",Z602ContagemResultado_OSVinculada);
                  GXUtil.WriteLogRaw("Current: ",T00412_A602ContagemResultado_OSVinculada[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"ContagemResultado"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert4169( )
      {
         BeforeValidate4169( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable4169( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM4169( 0) ;
            CheckOptimisticConcurrency4169( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm4169( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert4169( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T004134 */
                     pr_default.execute(32, new Object[] {A508ContagemResultado_Owner, n494ContagemResultado_Descricao, A494ContagemResultado_Descricao, n514ContagemResultado_Observacao, A514ContagemResultado_Observacao, n472ContagemResultado_DataEntrega, A472ContagemResultado_DataEntrega, n912ContagemResultado_HoraEntrega, A912ContagemResultado_HoraEntrega, n1351ContagemResultado_DataPrevista, A1351ContagemResultado_DataPrevista, n1452ContagemResultado_SS, A1452ContagemResultado_SS, n493ContagemResultado_DemandaFM, A493ContagemResultado_DemandaFM, n1584ContagemResultado_UOOwner, A1584ContagemResultado_UOOwner, n1515ContagemResultado_Evento, A1515ContagemResultado_Evento, A1583ContagemResultado_TipoRegistro, n1585ContagemResultado_Referencia, A1585ContagemResultado_Referencia, n1586ContagemResultado_Restricoes, A1586ContagemResultado_Restricoes, n1587ContagemResultado_PrioridadePrevista, A1587ContagemResultado_PrioridadePrevista, n484ContagemResultado_StatusDmn, A484ContagemResultado_StatusDmn, A471ContagemResultado_DataDmn, n1044ContagemResultado_FncUsrCod, A1044ContagemResultado_FncUsrCod, n1227ContagemResultado_PrazoInicialDias, A1227ContagemResultado_PrazoInicialDias, n1350ContagemResultado_DataCadastro, A1350ContagemResultado_DataCadastro, n1173ContagemResultado_OSManual, A1173ContagemResultado_OSManual, n598ContagemResultado_Baseline, A598ContagemResultado_Baseline, n485ContagemResultado_EhValidacao, A485ContagemResultado_EhValidacao, n146Modulo_Codigo, A146Modulo_Codigo, n454ContagemResultado_ContadorFSCod, A454ContagemResultado_ContadorFSCod, n890ContagemResultado_Responsavel, A890ContagemResultado_Responsavel, n468ContagemResultado_NaoCnfDmnCod, A468ContagemResultado_NaoCnfDmnCod, n490ContagemResultado_ContratadaCod, A490ContagemResultado_ContratadaCod, n805ContagemResultado_ContratadaOrigemCod, A805ContagemResultado_ContratadaOrigemCod, n489ContagemResultado_SistemaCod, A489ContagemResultado_SistemaCod, n597ContagemResultado_LoteAceiteCod, A597ContagemResultado_LoteAceiteCod, n1636ContagemResultado_ServicoSS, A1636ContagemResultado_ServicoSS, n1043ContagemResultado_LiqLogCod, A1043ContagemResultado_LiqLogCod, n1553ContagemResultado_CntSrvCod, A1553ContagemResultado_CntSrvCod, n602ContagemResultado_OSVinculada, A602ContagemResultado_OSVinculada});
                     A456ContagemResultado_Codigo = T004134_A456ContagemResultado_Codigo[0];
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A456ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A456ContagemResultado_Codigo), 6, 0)));
                     pr_default.close(32);
                     dsDefault.SmartCacheProvider.SetUpdated("ContagemResultado") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                           ResetCaption410( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load4169( ) ;
            }
            EndLevel4169( ) ;
         }
         CloseExtendedTableCursors4169( ) ;
      }

      protected void Update4169( )
      {
         BeforeValidate4169( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable4169( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency4169( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm4169( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate4169( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T004135 */
                     pr_default.execute(33, new Object[] {A508ContagemResultado_Owner, n494ContagemResultado_Descricao, A494ContagemResultado_Descricao, n514ContagemResultado_Observacao, A514ContagemResultado_Observacao, n472ContagemResultado_DataEntrega, A472ContagemResultado_DataEntrega, n912ContagemResultado_HoraEntrega, A912ContagemResultado_HoraEntrega, n1351ContagemResultado_DataPrevista, A1351ContagemResultado_DataPrevista, n1452ContagemResultado_SS, A1452ContagemResultado_SS, n493ContagemResultado_DemandaFM, A493ContagemResultado_DemandaFM, n1584ContagemResultado_UOOwner, A1584ContagemResultado_UOOwner, n1515ContagemResultado_Evento, A1515ContagemResultado_Evento, A1583ContagemResultado_TipoRegistro, n1585ContagemResultado_Referencia, A1585ContagemResultado_Referencia, n1586ContagemResultado_Restricoes, A1586ContagemResultado_Restricoes, n1587ContagemResultado_PrioridadePrevista, A1587ContagemResultado_PrioridadePrevista, n484ContagemResultado_StatusDmn, A484ContagemResultado_StatusDmn, A471ContagemResultado_DataDmn, n1044ContagemResultado_FncUsrCod, A1044ContagemResultado_FncUsrCod, n1227ContagemResultado_PrazoInicialDias, A1227ContagemResultado_PrazoInicialDias, n1350ContagemResultado_DataCadastro, A1350ContagemResultado_DataCadastro, n1173ContagemResultado_OSManual, A1173ContagemResultado_OSManual, n598ContagemResultado_Baseline, A598ContagemResultado_Baseline, n485ContagemResultado_EhValidacao, A485ContagemResultado_EhValidacao, n146Modulo_Codigo, A146Modulo_Codigo, n454ContagemResultado_ContadorFSCod, A454ContagemResultado_ContadorFSCod, n890ContagemResultado_Responsavel, A890ContagemResultado_Responsavel, n468ContagemResultado_NaoCnfDmnCod, A468ContagemResultado_NaoCnfDmnCod, n490ContagemResultado_ContratadaCod, A490ContagemResultado_ContratadaCod, n805ContagemResultado_ContratadaOrigemCod, A805ContagemResultado_ContratadaOrigemCod, n489ContagemResultado_SistemaCod, A489ContagemResultado_SistemaCod, n597ContagemResultado_LoteAceiteCod, A597ContagemResultado_LoteAceiteCod, n1636ContagemResultado_ServicoSS, A1636ContagemResultado_ServicoSS, n1043ContagemResultado_LiqLogCod, A1043ContagemResultado_LiqLogCod, n1553ContagemResultado_CntSrvCod, A1553ContagemResultado_CntSrvCod, n602ContagemResultado_OSVinculada, A602ContagemResultado_OSVinculada, A456ContagemResultado_Codigo});
                     pr_default.close(33);
                     dsDefault.SmartCacheProvider.SetUpdated("ContagemResultado") ;
                     if ( (pr_default.getStatus(33) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ContagemResultado"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate4169( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           getByPrimaryKey( ) ;
                           GX_msglist.addItem(context.GetMessage( "GXM_sucupdated", ""), 0, "", true);
                           ResetCaption410( ) ;
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel4169( ) ;
         }
         CloseExtendedTableCursors4169( ) ;
      }

      protected void DeferredUpdate4169( )
      {
      }

      protected void delete( )
      {
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         BeforeValidate4169( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency4169( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls4169( ) ;
            AfterConfirm4169( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete4169( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T004136 */
                  pr_default.execute(34, new Object[] {A456ContagemResultado_Codigo});
                  pr_default.close(34);
                  dsDefault.SmartCacheProvider.SetUpdated("ContagemResultado") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        move_next( ) ;
                        if ( RcdFound69 == 0 )
                        {
                           InitAll4169( ) ;
                           Gx_mode = "INS";
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                        }
                        else
                        {
                           getByPrimaryKey( ) ;
                           Gx_mode = "UPD";
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                        }
                        GX_msglist.addItem(context.GetMessage( "GXM_sucdeleted", ""), 0, "", true);
                        ResetCaption410( ) ;
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode69 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         EndLevel4169( ) ;
         Gx_mode = sMode69;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
      }

      protected void OnDeleteControls4169( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            GXAMODULO_CODIGO_html4169( A489ContagemResultado_SistemaCod) ;
         }
         if ( AnyError == 0 )
         {
            /* Using cursor T004137 */
            pr_default.execute(35, new Object[] {A456ContagemResultado_Codigo});
            if ( (pr_default.getStatus(35) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Resultado das Contagens"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(35);
            /* Using cursor T004138 */
            pr_default.execute(36, new Object[] {A456ContagemResultado_Codigo});
            if ( (pr_default.getStatus(36) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Contagem Resultado Nao Cnf"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(36);
            /* Using cursor T004139 */
            pr_default.execute(37, new Object[] {A456ContagemResultado_Codigo});
            if ( (pr_default.getStatus(37) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Contagem Resultado Requisito"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(37);
            /* Using cursor T004140 */
            pr_default.execute(38, new Object[] {A456ContagemResultado_Codigo});
            if ( (pr_default.getStatus(38) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Contagem Resultado QA"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(38);
            /* Using cursor T004141 */
            pr_default.execute(39, new Object[] {A456ContagemResultado_Codigo});
            if ( (pr_default.getStatus(39) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Contagem Resultado Chck Lst"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(39);
            /* Using cursor T004142 */
            pr_default.execute(40, new Object[] {A456ContagemResultado_Codigo});
            if ( (pr_default.getStatus(40) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Contagem Resultado Chck Lst Log"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(40);
            /* Using cursor T004143 */
            pr_default.execute(41, new Object[] {A456ContagemResultado_Codigo});
            if ( (pr_default.getStatus(41) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Artefatos das OS"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(41);
            /* Using cursor T004144 */
            pr_default.execute(42, new Object[] {A456ContagemResultado_Codigo});
            if ( (pr_default.getStatus(42) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Proposta"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(42);
            /* Using cursor T004145 */
            pr_default.execute(43, new Object[] {A456ContagemResultado_Codigo});
            if ( (pr_default.getStatus(43) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Contagem Resultado Execucao"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(43);
            /* Using cursor T004146 */
            pr_default.execute(44, new Object[] {A456ContagemResultado_Codigo});
            if ( (pr_default.getStatus(44) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"OS"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(44);
            /* Using cursor T004147 */
            pr_default.execute(45, new Object[] {A456ContagemResultado_Codigo});
            if ( (pr_default.getStatus(45) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Nota da OS"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(45);
            /* Using cursor T004148 */
            pr_default.execute(46, new Object[] {A456ContagemResultado_Codigo});
            if ( (pr_default.getStatus(46) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Contagem Resultado Indicadores"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(46);
            /* Using cursor T004149 */
            pr_default.execute(47, new Object[] {A456ContagemResultado_Codigo});
            if ( (pr_default.getStatus(47) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Contagem Resultado Incidentes"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(47);
            /* Using cursor T004150 */
            pr_default.execute(48, new Object[] {A456ContagemResultado_Codigo});
            if ( (pr_default.getStatus(48) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Agenda Atendimento"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(48);
            /* Using cursor T004151 */
            pr_default.execute(49, new Object[] {A456ContagemResultado_Codigo});
            if ( (pr_default.getStatus(49) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Log de a��o dos Respons�veis"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(49);
            /* Using cursor T004152 */
            pr_default.execute(50, new Object[] {A456ContagemResultado_Codigo});
            if ( (pr_default.getStatus(50) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Requisitos de Neg�cio da Solicita��o de Servi�o"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(50);
            /* Using cursor T004153 */
            pr_default.execute(51, new Object[] {A456ContagemResultado_Codigo});
            if ( (pr_default.getStatus(51) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Contagem Resultado Evidencias"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(51);
            /* Using cursor T004154 */
            pr_default.execute(52, new Object[] {A456ContagemResultado_Codigo});
            if ( (pr_default.getStatus(52) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Demanda"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(52);
            /* Using cursor T004155 */
            pr_default.execute(53, new Object[] {A456ContagemResultado_Codigo});
            if ( (pr_default.getStatus(53) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Erros nas confer�ncias de contagens"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(53);
            /* Using cursor T004156 */
            pr_default.execute(54, new Object[] {A456ContagemResultado_Codigo});
            if ( (pr_default.getStatus(54) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Contagem Resultado Contagens"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(54);
         }
      }

      protected void EndLevel4169( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete4169( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(1);
            context.CommitDataStores( "SolicitacaoServico");
            if ( AnyError == 0 )
            {
               ConfirmValues410( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(1);
            context.RollbackDataStores( "SolicitacaoServico");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart4169( )
      {
         /* Scan By routine */
         /* Using cursor T004157 */
         pr_default.execute(55);
         RcdFound69 = 0;
         if ( (pr_default.getStatus(55) != 101) )
         {
            RcdFound69 = 1;
            A456ContagemResultado_Codigo = T004157_A456ContagemResultado_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A456ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A456ContagemResultado_Codigo), 6, 0)));
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext4169( )
      {
         /* Scan next routine */
         pr_default.readNext(55);
         RcdFound69 = 0;
         if ( (pr_default.getStatus(55) != 101) )
         {
            RcdFound69 = 1;
            A456ContagemResultado_Codigo = T004157_A456ContagemResultado_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A456ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A456ContagemResultado_Codigo), 6, 0)));
         }
      }

      protected void ScanEnd4169( )
      {
         pr_default.close(55);
      }

      protected void AfterConfirm4169( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert4169( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate4169( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete4169( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete4169( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate4169( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes4169( )
      {
         edtContagemResultado_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultado_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultado_Codigo_Enabled), 5, 0)));
         edtContagemResultado_Owner_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultado_Owner_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultado_Owner_Enabled), 5, 0)));
         edtContagemResultado_Descricao_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultado_Descricao_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultado_Descricao_Enabled), 5, 0)));
         edtContagemResultado_DataEntrega_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultado_DataEntrega_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultado_DataEntrega_Enabled), 5, 0)));
         edtContagemResultado_HoraEntrega_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultado_HoraEntrega_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultado_HoraEntrega_Enabled), 5, 0)));
         dynContagemResultado_ServicoSS.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynContagemResultado_ServicoSS_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynContagemResultado_ServicoSS.Enabled), 5, 0)));
         edtContagemResultado_SS_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultado_SS_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultado_SS_Enabled), 5, 0)));
         edtContagemResultado_DemandaFM_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultado_DemandaFM_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultado_DemandaFM_Enabled), 5, 0)));
         edtContagemResultado_UOOwner_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultado_UOOwner_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultado_UOOwner_Enabled), 5, 0)));
         cmbContagemResultado_Evento.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContagemResultado_Evento_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbContagemResultado_Evento.Enabled), 5, 0)));
         cmbContagemResultado_TipoRegistro.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContagemResultado_TipoRegistro_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbContagemResultado_TipoRegistro.Enabled), 5, 0)));
         edtContagemResultado_Referencia_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultado_Referencia_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultado_Referencia_Enabled), 5, 0)));
         edtContagemResultado_Restricoes_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultado_Restricoes_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultado_Restricoes_Enabled), 5, 0)));
         edtContagemResultado_PrioridadePrevista_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultado_PrioridadePrevista_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultado_PrioridadePrevista_Enabled), 5, 0)));
         cmbContagemResultado_StatusDmn.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContagemResultado_StatusDmn_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbContagemResultado_StatusDmn.Enabled), 5, 0)));
         edtContagemResultado_DataDmn_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultado_DataDmn_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultado_DataDmn_Enabled), 5, 0)));
         edtContagemResultado_CntSrvCod_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultado_CntSrvCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultado_CntSrvCod_Enabled), 5, 0)));
         edtContagemResultado_OSVinculada_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultado_OSVinculada_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultado_OSVinculada_Enabled), 5, 0)));
         dynContagemResultado_NaoCnfDmnCod.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynContagemResultado_NaoCnfDmnCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynContagemResultado_NaoCnfDmnCod.Enabled), 5, 0)));
         dynContagemResultado_LoteAceiteCod.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynContagemResultado_LoteAceiteCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynContagemResultado_LoteAceiteCod.Enabled), 5, 0)));
         dynContagemResultado_ContratadaOrigemCod.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynContagemResultado_ContratadaOrigemCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynContagemResultado_ContratadaOrigemCod.Enabled), 5, 0)));
         dynContagemResultado_ContadorFSCod.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynContagemResultado_ContadorFSCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynContagemResultado_ContadorFSCod.Enabled), 5, 0)));
         edtContagemResultado_Responsavel_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultado_Responsavel_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultado_Responsavel_Enabled), 5, 0)));
         edtContagemResultado_LiqLogCod_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultado_LiqLogCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultado_LiqLogCod_Enabled), 5, 0)));
         dynModulo_Codigo.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynModulo_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynModulo_Codigo.Enabled), 5, 0)));
         dynContagemResultado_ContratadaCod.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynContagemResultado_ContratadaCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynContagemResultado_ContratadaCod.Enabled), 5, 0)));
         edtContagemResultado_SistemaCod_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultado_SistemaCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultado_SistemaCod_Enabled), 5, 0)));
         edtContagemResultado_FncUsrCod_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultado_FncUsrCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultado_FncUsrCod_Enabled), 5, 0)));
         edtContagemResultado_PrazoInicialDias_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultado_PrazoInicialDias_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultado_PrazoInicialDias_Enabled), 5, 0)));
         edtContagemResultado_DataCadastro_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultado_DataCadastro_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultado_DataCadastro_Enabled), 5, 0)));
         chkContagemResultado_OSManual.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkContagemResultado_OSManual_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(chkContagemResultado_OSManual.Enabled), 5, 0)));
         Contagemresultado_observacao_Enabled = Convert.ToBoolean( 0);
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, Contagemresultado_observacao_Internalname, "Enabled", StringUtil.BoolToStr( Contagemresultado_observacao_Enabled));
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues410( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?2020621618925");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.AddJavascriptSource("CKEditor/ckeditor/ckeditor.js", "");
         context.AddJavascriptSource("CKEditor/CKEditorRender.js", "");
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("solicitacaoservico.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "Z456ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z456ContagemResultado_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z508ContagemResultado_Owner", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z508ContagemResultado_Owner), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z494ContagemResultado_Descricao", Z494ContagemResultado_Descricao);
         GxWebStd.gx_hidden_field( context, "Z472ContagemResultado_DataEntrega", context.localUtil.DToC( Z472ContagemResultado_DataEntrega, 0, "/"));
         GxWebStd.gx_hidden_field( context, "Z912ContagemResultado_HoraEntrega", context.localUtil.TToC( Z912ContagemResultado_HoraEntrega, 10, 8, 0, 0, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "Z1351ContagemResultado_DataPrevista", context.localUtil.TToC( Z1351ContagemResultado_DataPrevista, 10, 8, 0, 0, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "Z1452ContagemResultado_SS", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1452ContagemResultado_SS), 8, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z493ContagemResultado_DemandaFM", Z493ContagemResultado_DemandaFM);
         GxWebStd.gx_hidden_field( context, "Z1584ContagemResultado_UOOwner", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1584ContagemResultado_UOOwner), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1515ContagemResultado_Evento", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1515ContagemResultado_Evento), 2, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1583ContagemResultado_TipoRegistro", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1583ContagemResultado_TipoRegistro), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1585ContagemResultado_Referencia", Z1585ContagemResultado_Referencia);
         GxWebStd.gx_hidden_field( context, "Z1586ContagemResultado_Restricoes", Z1586ContagemResultado_Restricoes);
         GxWebStd.gx_hidden_field( context, "Z1587ContagemResultado_PrioridadePrevista", Z1587ContagemResultado_PrioridadePrevista);
         GxWebStd.gx_hidden_field( context, "Z484ContagemResultado_StatusDmn", StringUtil.RTrim( Z484ContagemResultado_StatusDmn));
         GxWebStd.gx_hidden_field( context, "Z471ContagemResultado_DataDmn", context.localUtil.DToC( Z471ContagemResultado_DataDmn, 0, "/"));
         GxWebStd.gx_hidden_field( context, "Z1044ContagemResultado_FncUsrCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1044ContagemResultado_FncUsrCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1227ContagemResultado_PrazoInicialDias", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1227ContagemResultado_PrazoInicialDias), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1350ContagemResultado_DataCadastro", context.localUtil.TToC( Z1350ContagemResultado_DataCadastro, 10, 8, 0, 0, "/", ":", " "));
         GxWebStd.gx_boolean_hidden_field( context, "Z1173ContagemResultado_OSManual", Z1173ContagemResultado_OSManual);
         GxWebStd.gx_boolean_hidden_field( context, "Z598ContagemResultado_Baseline", Z598ContagemResultado_Baseline);
         GxWebStd.gx_boolean_hidden_field( context, "Z485ContagemResultado_EhValidacao", Z485ContagemResultado_EhValidacao);
         GxWebStd.gx_hidden_field( context, "Z146Modulo_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z146Modulo_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z454ContagemResultado_ContadorFSCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z454ContagemResultado_ContadorFSCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z890ContagemResultado_Responsavel", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z890ContagemResultado_Responsavel), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z468ContagemResultado_NaoCnfDmnCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z468ContagemResultado_NaoCnfDmnCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z490ContagemResultado_ContratadaCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z490ContagemResultado_ContratadaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z805ContagemResultado_ContratadaOrigemCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z805ContagemResultado_ContratadaOrigemCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z489ContagemResultado_SistemaCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z489ContagemResultado_SistemaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z597ContagemResultado_LoteAceiteCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z597ContagemResultado_LoteAceiteCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1636ContagemResultado_ServicoSS", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1636ContagemResultado_ServicoSS), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1043ContagemResultado_LiqLogCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1043ContagemResultado_LiqLogCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1553ContagemResultado_CntSrvCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1553ContagemResultado_CntSrvCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z602ContagemResultado_OSVinculada", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z602ContagemResultado_OSVinculada), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "vGXBSCREEN", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gx_BScreen), 1, 0, ",", "")));
         GxWebStd.gx_boolean_hidden_field( context, "CONTAGEMRESULTADO_BASELINE", A598ContagemResultado_Baseline);
         GxWebStd.gx_boolean_hidden_field( context, "CONTAGEMRESULTADO_EHVALIDACAO", A485ContagemResultado_EhValidacao);
         GxWebStd.gx_hidden_field( context, "vTODAY", context.localUtil.DToC( Gx_date, 0, "/"));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vWWPCONTEXT", AV7WWPContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vWWPCONTEXT", AV7WWPContext);
         }
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_OBSERVACAO", A514ContagemResultado_Observacao);
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_DATAPREVISTA", context.localUtil.TToC( A1351ContagemResultado_DataPrevista, 10, 8, 0, 0, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV26Pgmname));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "CONTAGEMRESULTADO_OBSERVACAO_Enabled", StringUtil.BoolToStr( Contagemresultado_observacao_Enabled));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Width", StringUtil.RTrim( Dvpanel_tableattributes_Width));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Cls", StringUtil.RTrim( Dvpanel_tableattributes_Cls));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Title", StringUtil.RTrim( Dvpanel_tableattributes_Title));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsible", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsible));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Collapsed", StringUtil.BoolToStr( Dvpanel_tableattributes_Collapsed));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Enabled", StringUtil.BoolToStr( Dvpanel_tableattributes_Enabled));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autowidth", StringUtil.BoolToStr( Dvpanel_tableattributes_Autowidth));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoheight", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoheight));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Showcollapseicon", StringUtil.BoolToStr( Dvpanel_tableattributes_Showcollapseicon));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Iconposition", StringUtil.RTrim( Dvpanel_tableattributes_Iconposition));
         GxWebStd.gx_hidden_field( context, "DVPANEL_TABLEATTRIBUTES_Autoscroll", StringUtil.BoolToStr( Dvpanel_tableattributes_Autoscroll));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = "hsh" + "SolicitacaoServico";
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( A1351ContagemResultado_DataPrevista, "99/99/99 99:99");
         forbiddenHiddens = forbiddenHiddens + StringUtil.BoolToStr( A598ContagemResultado_Baseline);
         forbiddenHiddens = forbiddenHiddens + StringUtil.BoolToStr( A485ContagemResultado_EhValidacao);
         GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("solicitacaoservico:[SendSecurityCheck value for]"+"ContagemResultado_DataPrevista:"+context.localUtil.Format( A1351ContagemResultado_DataPrevista, "99/99/99 99:99"));
         GXUtil.WriteLog("solicitacaoservico:[SendSecurityCheck value for]"+"ContagemResultado_Baseline:"+StringUtil.BoolToStr( A598ContagemResultado_Baseline));
         GXUtil.WriteLog("solicitacaoservico:[SendSecurityCheck value for]"+"ContagemResultado_EhValidacao:"+StringUtil.BoolToStr( A485ContagemResultado_EhValidacao));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("solicitacaoservico.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "SolicitacaoServico" ;
      }

      public override String GetPgmdesc( )
      {
         return "Solicitacao Servico" ;
      }

      protected void InitializeNonKey4169( )
      {
         A494ContagemResultado_Descricao = "";
         n494ContagemResultado_Descricao = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A494ContagemResultado_Descricao", A494ContagemResultado_Descricao);
         n494ContagemResultado_Descricao = (String.IsNullOrEmpty(StringUtil.RTrim( A494ContagemResultado_Descricao)) ? true : false);
         A514ContagemResultado_Observacao = "";
         n514ContagemResultado_Observacao = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A514ContagemResultado_Observacao", A514ContagemResultado_Observacao);
         A472ContagemResultado_DataEntrega = DateTime.MinValue;
         n472ContagemResultado_DataEntrega = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A472ContagemResultado_DataEntrega", context.localUtil.Format(A472ContagemResultado_DataEntrega, "99/99/99"));
         n472ContagemResultado_DataEntrega = ((DateTime.MinValue==A472ContagemResultado_DataEntrega) ? true : false);
         A912ContagemResultado_HoraEntrega = (DateTime)(DateTime.MinValue);
         n912ContagemResultado_HoraEntrega = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A912ContagemResultado_HoraEntrega", context.localUtil.TToC( A912ContagemResultado_HoraEntrega, 0, 5, 0, 3, "/", ":", " "));
         n912ContagemResultado_HoraEntrega = ((DateTime.MinValue==A912ContagemResultado_HoraEntrega) ? true : false);
         A1351ContagemResultado_DataPrevista = (DateTime)(DateTime.MinValue);
         n1351ContagemResultado_DataPrevista = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1351ContagemResultado_DataPrevista", context.localUtil.TToC( A1351ContagemResultado_DataPrevista, 8, 5, 0, 3, "/", ":", " "));
         A1636ContagemResultado_ServicoSS = 0;
         n1636ContagemResultado_ServicoSS = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1636ContagemResultado_ServicoSS", StringUtil.LTrim( StringUtil.Str( (decimal)(A1636ContagemResultado_ServicoSS), 6, 0)));
         n1636ContagemResultado_ServicoSS = ((0==A1636ContagemResultado_ServicoSS) ? true : false);
         A493ContagemResultado_DemandaFM = "";
         n493ContagemResultado_DemandaFM = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A493ContagemResultado_DemandaFM", A493ContagemResultado_DemandaFM);
         n493ContagemResultado_DemandaFM = (String.IsNullOrEmpty(StringUtil.RTrim( A493ContagemResultado_DemandaFM)) ? true : false);
         A1584ContagemResultado_UOOwner = 0;
         n1584ContagemResultado_UOOwner = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1584ContagemResultado_UOOwner", StringUtil.LTrim( StringUtil.Str( (decimal)(A1584ContagemResultado_UOOwner), 6, 0)));
         n1584ContagemResultado_UOOwner = ((0==A1584ContagemResultado_UOOwner) ? true : false);
         A1585ContagemResultado_Referencia = "";
         n1585ContagemResultado_Referencia = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1585ContagemResultado_Referencia", A1585ContagemResultado_Referencia);
         n1585ContagemResultado_Referencia = (String.IsNullOrEmpty(StringUtil.RTrim( A1585ContagemResultado_Referencia)) ? true : false);
         A1586ContagemResultado_Restricoes = "";
         n1586ContagemResultado_Restricoes = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1586ContagemResultado_Restricoes", A1586ContagemResultado_Restricoes);
         n1586ContagemResultado_Restricoes = (String.IsNullOrEmpty(StringUtil.RTrim( A1586ContagemResultado_Restricoes)) ? true : false);
         A1587ContagemResultado_PrioridadePrevista = "";
         n1587ContagemResultado_PrioridadePrevista = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1587ContagemResultado_PrioridadePrevista", A1587ContagemResultado_PrioridadePrevista);
         n1587ContagemResultado_PrioridadePrevista = (String.IsNullOrEmpty(StringUtil.RTrim( A1587ContagemResultado_PrioridadePrevista)) ? true : false);
         A1553ContagemResultado_CntSrvCod = 0;
         n1553ContagemResultado_CntSrvCod = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1553ContagemResultado_CntSrvCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1553ContagemResultado_CntSrvCod), 6, 0)));
         n1553ContagemResultado_CntSrvCod = ((0==A1553ContagemResultado_CntSrvCod) ? true : false);
         A602ContagemResultado_OSVinculada = 0;
         n602ContagemResultado_OSVinculada = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A602ContagemResultado_OSVinculada", StringUtil.LTrim( StringUtil.Str( (decimal)(A602ContagemResultado_OSVinculada), 6, 0)));
         n602ContagemResultado_OSVinculada = ((0==A602ContagemResultado_OSVinculada) ? true : false);
         A468ContagemResultado_NaoCnfDmnCod = 0;
         n468ContagemResultado_NaoCnfDmnCod = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A468ContagemResultado_NaoCnfDmnCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A468ContagemResultado_NaoCnfDmnCod), 6, 0)));
         n468ContagemResultado_NaoCnfDmnCod = ((0==A468ContagemResultado_NaoCnfDmnCod) ? true : false);
         A597ContagemResultado_LoteAceiteCod = 0;
         n597ContagemResultado_LoteAceiteCod = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A597ContagemResultado_LoteAceiteCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A597ContagemResultado_LoteAceiteCod), 6, 0)));
         n597ContagemResultado_LoteAceiteCod = ((0==A597ContagemResultado_LoteAceiteCod) ? true : false);
         A805ContagemResultado_ContratadaOrigemCod = 0;
         n805ContagemResultado_ContratadaOrigemCod = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A805ContagemResultado_ContratadaOrigemCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A805ContagemResultado_ContratadaOrigemCod), 6, 0)));
         n805ContagemResultado_ContratadaOrigemCod = ((0==A805ContagemResultado_ContratadaOrigemCod) ? true : false);
         A454ContagemResultado_ContadorFSCod = 0;
         n454ContagemResultado_ContadorFSCod = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A454ContagemResultado_ContadorFSCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A454ContagemResultado_ContadorFSCod), 6, 0)));
         n454ContagemResultado_ContadorFSCod = ((0==A454ContagemResultado_ContadorFSCod) ? true : false);
         A890ContagemResultado_Responsavel = 0;
         n890ContagemResultado_Responsavel = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A890ContagemResultado_Responsavel", StringUtil.LTrim( StringUtil.Str( (decimal)(A890ContagemResultado_Responsavel), 6, 0)));
         n890ContagemResultado_Responsavel = ((0==A890ContagemResultado_Responsavel) ? true : false);
         A1043ContagemResultado_LiqLogCod = 0;
         n1043ContagemResultado_LiqLogCod = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1043ContagemResultado_LiqLogCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1043ContagemResultado_LiqLogCod), 6, 0)));
         n1043ContagemResultado_LiqLogCod = ((0==A1043ContagemResultado_LiqLogCod) ? true : false);
         A146Modulo_Codigo = 0;
         n146Modulo_Codigo = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A146Modulo_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A146Modulo_Codigo), 6, 0)));
         n146Modulo_Codigo = ((0==A146Modulo_Codigo) ? true : false);
         A490ContagemResultado_ContratadaCod = 0;
         n490ContagemResultado_ContratadaCod = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A490ContagemResultado_ContratadaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A490ContagemResultado_ContratadaCod), 6, 0)));
         n490ContagemResultado_ContratadaCod = ((0==A490ContagemResultado_ContratadaCod) ? true : false);
         A489ContagemResultado_SistemaCod = 0;
         n489ContagemResultado_SistemaCod = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A489ContagemResultado_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A489ContagemResultado_SistemaCod), 6, 0)));
         n489ContagemResultado_SistemaCod = ((0==A489ContagemResultado_SistemaCod) ? true : false);
         A1044ContagemResultado_FncUsrCod = 0;
         n1044ContagemResultado_FncUsrCod = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1044ContagemResultado_FncUsrCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1044ContagemResultado_FncUsrCod), 6, 0)));
         n1044ContagemResultado_FncUsrCod = ((0==A1044ContagemResultado_FncUsrCod) ? true : false);
         A1227ContagemResultado_PrazoInicialDias = 0;
         n1227ContagemResultado_PrazoInicialDias = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1227ContagemResultado_PrazoInicialDias", StringUtil.LTrim( StringUtil.Str( (decimal)(A1227ContagemResultado_PrazoInicialDias), 4, 0)));
         n1227ContagemResultado_PrazoInicialDias = ((0==A1227ContagemResultado_PrazoInicialDias) ? true : false);
         A1173ContagemResultado_OSManual = false;
         n1173ContagemResultado_OSManual = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1173ContagemResultado_OSManual", A1173ContagemResultado_OSManual);
         n1173ContagemResultado_OSManual = ((false==A1173ContagemResultado_OSManual) ? true : false);
         A508ContagemResultado_Owner = AV7WWPContext.gxTpr_Userid;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A508ContagemResultado_Owner", StringUtil.LTrim( StringUtil.Str( (decimal)(A508ContagemResultado_Owner), 6, 0)));
         A1452ContagemResultado_SS = 0;
         n1452ContagemResultado_SS = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1452ContagemResultado_SS", StringUtil.LTrim( StringUtil.Str( (decimal)(A1452ContagemResultado_SS), 8, 0)));
         A1515ContagemResultado_Evento = 1;
         n1515ContagemResultado_Evento = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1515ContagemResultado_Evento", StringUtil.LTrim( StringUtil.Str( (decimal)(A1515ContagemResultado_Evento), 2, 0)));
         A1583ContagemResultado_TipoRegistro = 2;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1583ContagemResultado_TipoRegistro", StringUtil.LTrim( StringUtil.Str( (decimal)(A1583ContagemResultado_TipoRegistro), 4, 0)));
         A484ContagemResultado_StatusDmn = "S";
         n484ContagemResultado_StatusDmn = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A484ContagemResultado_StatusDmn", A484ContagemResultado_StatusDmn);
         A471ContagemResultado_DataDmn = Gx_date;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A471ContagemResultado_DataDmn", context.localUtil.Format(A471ContagemResultado_DataDmn, "99/99/99"));
         A1350ContagemResultado_DataCadastro = DateTimeUtil.ServerNow( context, "DEFAULT");
         n1350ContagemResultado_DataCadastro = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1350ContagemResultado_DataCadastro", context.localUtil.TToC( A1350ContagemResultado_DataCadastro, 8, 5, 0, 3, "/", ":", " "));
         A598ContagemResultado_Baseline = false;
         n598ContagemResultado_Baseline = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A598ContagemResultado_Baseline", A598ContagemResultado_Baseline);
         A485ContagemResultado_EhValidacao = false;
         n485ContagemResultado_EhValidacao = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A485ContagemResultado_EhValidacao", A485ContagemResultado_EhValidacao);
         Z508ContagemResultado_Owner = 0;
         Z494ContagemResultado_Descricao = "";
         Z472ContagemResultado_DataEntrega = DateTime.MinValue;
         Z912ContagemResultado_HoraEntrega = (DateTime)(DateTime.MinValue);
         Z1351ContagemResultado_DataPrevista = (DateTime)(DateTime.MinValue);
         Z1452ContagemResultado_SS = 0;
         Z493ContagemResultado_DemandaFM = "";
         Z1584ContagemResultado_UOOwner = 0;
         Z1515ContagemResultado_Evento = 0;
         Z1583ContagemResultado_TipoRegistro = 0;
         Z1585ContagemResultado_Referencia = "";
         Z1586ContagemResultado_Restricoes = "";
         Z1587ContagemResultado_PrioridadePrevista = "";
         Z484ContagemResultado_StatusDmn = "";
         Z471ContagemResultado_DataDmn = DateTime.MinValue;
         Z1044ContagemResultado_FncUsrCod = 0;
         Z1227ContagemResultado_PrazoInicialDias = 0;
         Z1350ContagemResultado_DataCadastro = (DateTime)(DateTime.MinValue);
         Z1173ContagemResultado_OSManual = false;
         Z598ContagemResultado_Baseline = false;
         Z485ContagemResultado_EhValidacao = false;
         Z146Modulo_Codigo = 0;
         Z454ContagemResultado_ContadorFSCod = 0;
         Z890ContagemResultado_Responsavel = 0;
         Z468ContagemResultado_NaoCnfDmnCod = 0;
         Z490ContagemResultado_ContratadaCod = 0;
         Z805ContagemResultado_ContratadaOrigemCod = 0;
         Z489ContagemResultado_SistemaCod = 0;
         Z597ContagemResultado_LoteAceiteCod = 0;
         Z1636ContagemResultado_ServicoSS = 0;
         Z1043ContagemResultado_LiqLogCod = 0;
         Z1553ContagemResultado_CntSrvCod = 0;
         Z602ContagemResultado_OSVinculada = 0;
      }

      protected void InitAll4169( )
      {
         A456ContagemResultado_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A456ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A456ContagemResultado_Codigo), 6, 0)));
         InitializeNonKey4169( ) ;
      }

      protected void StandaloneModalInsert( )
      {
         A598ContagemResultado_Baseline = i598ContagemResultado_Baseline;
         n598ContagemResultado_Baseline = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A598ContagemResultado_Baseline", A598ContagemResultado_Baseline);
         A485ContagemResultado_EhValidacao = i485ContagemResultado_EhValidacao;
         n485ContagemResultado_EhValidacao = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A485ContagemResultado_EhValidacao", A485ContagemResultado_EhValidacao);
         A1350ContagemResultado_DataCadastro = i1350ContagemResultado_DataCadastro;
         n1350ContagemResultado_DataCadastro = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1350ContagemResultado_DataCadastro", context.localUtil.TToC( A1350ContagemResultado_DataCadastro, 8, 5, 0, 3, "/", ":", " "));
         A1515ContagemResultado_Evento = i1515ContagemResultado_Evento;
         n1515ContagemResultado_Evento = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1515ContagemResultado_Evento", StringUtil.LTrim( StringUtil.Str( (decimal)(A1515ContagemResultado_Evento), 2, 0)));
         A1452ContagemResultado_SS = i1452ContagemResultado_SS;
         n1452ContagemResultado_SS = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1452ContagemResultado_SS", StringUtil.LTrim( StringUtil.Str( (decimal)(A1452ContagemResultado_SS), 8, 0)));
         A1583ContagemResultado_TipoRegistro = i1583ContagemResultado_TipoRegistro;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1583ContagemResultado_TipoRegistro", StringUtil.LTrim( StringUtil.Str( (decimal)(A1583ContagemResultado_TipoRegistro), 4, 0)));
         A484ContagemResultado_StatusDmn = i484ContagemResultado_StatusDmn;
         n484ContagemResultado_StatusDmn = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A484ContagemResultado_StatusDmn", A484ContagemResultado_StatusDmn);
         A508ContagemResultado_Owner = i508ContagemResultado_Owner;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A508ContagemResultado_Owner", StringUtil.LTrim( StringUtil.Str( (decimal)(A508ContagemResultado_Owner), 6, 0)));
         A471ContagemResultado_DataDmn = i471ContagemResultado_DataDmn;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A471ContagemResultado_DataDmn", context.localUtil.Format(A471ContagemResultado_DataDmn, "99/99/99"));
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/Bootstrap/Shared/bootstrap.css", "?2319140");
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?2020621618992");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxdec.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("solicitacaoservico.js", "?2020621618993");
         context.AddJavascriptSource("Shared/jquery/jquery1.9.1.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/bootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Shared/DVelopBootstrap.js", "");
         context.AddJavascriptSource("DVelop/Bootstrap/Panel/BootstrapPanelRender.js", "");
         context.AddJavascriptSource("CKEditor/ckeditor/ckeditor.js", "");
         context.AddJavascriptSource("CKEditor/CKEditorRender.js", "");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblSolicitacaoservicotitle_Internalname = "SOLICITACAOSERVICOTITLE";
         lblTextblockcontagemresultado_codigo_Internalname = "TEXTBLOCKCONTAGEMRESULTADO_CODIGO";
         edtContagemResultado_Codigo_Internalname = "CONTAGEMRESULTADO_CODIGO";
         lblTextblockcontagemresultado_owner_Internalname = "TEXTBLOCKCONTAGEMRESULTADO_OWNER";
         edtContagemResultado_Owner_Internalname = "CONTAGEMRESULTADO_OWNER";
         lblTextblockcontagemresultado_descricao_Internalname = "TEXTBLOCKCONTAGEMRESULTADO_DESCRICAO";
         edtContagemResultado_Descricao_Internalname = "CONTAGEMRESULTADO_DESCRICAO";
         lblTextblockcontagemresultado_observacao_Internalname = "TEXTBLOCKCONTAGEMRESULTADO_OBSERVACAO";
         Contagemresultado_observacao_Internalname = "CONTAGEMRESULTADO_OBSERVACAO";
         lblTextblockcontagemresultado_dataentrega_Internalname = "TEXTBLOCKCONTAGEMRESULTADO_DATAENTREGA";
         edtContagemResultado_DataEntrega_Internalname = "CONTAGEMRESULTADO_DATAENTREGA";
         lblTextblockcontagemresultado_horaentrega_Internalname = "TEXTBLOCKCONTAGEMRESULTADO_HORAENTREGA";
         edtContagemResultado_HoraEntrega_Internalname = "CONTAGEMRESULTADO_HORAENTREGA";
         lblTextblockcontagemresultado_servicoss_Internalname = "TEXTBLOCKCONTAGEMRESULTADO_SERVICOSS";
         dynContagemResultado_ServicoSS_Internalname = "CONTAGEMRESULTADO_SERVICOSS";
         lblTextblockcontagemresultado_ss_Internalname = "TEXTBLOCKCONTAGEMRESULTADO_SS";
         edtContagemResultado_SS_Internalname = "CONTAGEMRESULTADO_SS";
         lblTextblockcontagemresultado_demandafm_Internalname = "TEXTBLOCKCONTAGEMRESULTADO_DEMANDAFM";
         edtContagemResultado_DemandaFM_Internalname = "CONTAGEMRESULTADO_DEMANDAFM";
         lblTextblockcontagemresultado_uoowner_Internalname = "TEXTBLOCKCONTAGEMRESULTADO_UOOWNER";
         edtContagemResultado_UOOwner_Internalname = "CONTAGEMRESULTADO_UOOWNER";
         lblTextblockcontagemresultado_evento_Internalname = "TEXTBLOCKCONTAGEMRESULTADO_EVENTO";
         cmbContagemResultado_Evento_Internalname = "CONTAGEMRESULTADO_EVENTO";
         lblTextblockcontagemresultado_tiporegistro_Internalname = "TEXTBLOCKCONTAGEMRESULTADO_TIPOREGISTRO";
         cmbContagemResultado_TipoRegistro_Internalname = "CONTAGEMRESULTADO_TIPOREGISTRO";
         lblTextblockcontagemresultado_referencia_Internalname = "TEXTBLOCKCONTAGEMRESULTADO_REFERENCIA";
         edtContagemResultado_Referencia_Internalname = "CONTAGEMRESULTADO_REFERENCIA";
         lblTextblockcontagemresultado_restricoes_Internalname = "TEXTBLOCKCONTAGEMRESULTADO_RESTRICOES";
         edtContagemResultado_Restricoes_Internalname = "CONTAGEMRESULTADO_RESTRICOES";
         lblTextblockcontagemresultado_prioridadeprevista_Internalname = "TEXTBLOCKCONTAGEMRESULTADO_PRIORIDADEPREVISTA";
         edtContagemResultado_PrioridadePrevista_Internalname = "CONTAGEMRESULTADO_PRIORIDADEPREVISTA";
         lblTextblockcontagemresultado_statusdmn_Internalname = "TEXTBLOCKCONTAGEMRESULTADO_STATUSDMN";
         cmbContagemResultado_StatusDmn_Internalname = "CONTAGEMRESULTADO_STATUSDMN";
         lblTextblockcontagemresultado_datadmn_Internalname = "TEXTBLOCKCONTAGEMRESULTADO_DATADMN";
         edtContagemResultado_DataDmn_Internalname = "CONTAGEMRESULTADO_DATADMN";
         lblTextblockcontagemresultado_cntsrvcod_Internalname = "TEXTBLOCKCONTAGEMRESULTADO_CNTSRVCOD";
         edtContagemResultado_CntSrvCod_Internalname = "CONTAGEMRESULTADO_CNTSRVCOD";
         lblTextblockcontagemresultado_osvinculada_Internalname = "TEXTBLOCKCONTAGEMRESULTADO_OSVINCULADA";
         edtContagemResultado_OSVinculada_Internalname = "CONTAGEMRESULTADO_OSVINCULADA";
         lblTextblockcontagemresultado_naocnfdmncod_Internalname = "TEXTBLOCKCONTAGEMRESULTADO_NAOCNFDMNCOD";
         dynContagemResultado_NaoCnfDmnCod_Internalname = "CONTAGEMRESULTADO_NAOCNFDMNCOD";
         lblTextblockcontagemresultado_loteaceitecod_Internalname = "TEXTBLOCKCONTAGEMRESULTADO_LOTEACEITECOD";
         dynContagemResultado_LoteAceiteCod_Internalname = "CONTAGEMRESULTADO_LOTEACEITECOD";
         lblTextblockcontagemresultado_contratadaorigemcod_Internalname = "TEXTBLOCKCONTAGEMRESULTADO_CONTRATADAORIGEMCOD";
         dynContagemResultado_ContratadaOrigemCod_Internalname = "CONTAGEMRESULTADO_CONTRATADAORIGEMCOD";
         lblTextblockcontagemresultado_contadorfscod_Internalname = "TEXTBLOCKCONTAGEMRESULTADO_CONTADORFSCOD";
         dynContagemResultado_ContadorFSCod_Internalname = "CONTAGEMRESULTADO_CONTADORFSCOD";
         lblTextblockcontagemresultado_responsavel_Internalname = "TEXTBLOCKCONTAGEMRESULTADO_RESPONSAVEL";
         edtContagemResultado_Responsavel_Internalname = "CONTAGEMRESULTADO_RESPONSAVEL";
         lblTextblockcontagemresultado_liqlogcod_Internalname = "TEXTBLOCKCONTAGEMRESULTADO_LIQLOGCOD";
         edtContagemResultado_LiqLogCod_Internalname = "CONTAGEMRESULTADO_LIQLOGCOD";
         lblTextblockmodulo_codigo_Internalname = "TEXTBLOCKMODULO_CODIGO";
         dynModulo_Codigo_Internalname = "MODULO_CODIGO";
         lblTextblockcontagemresultado_contratadacod_Internalname = "TEXTBLOCKCONTAGEMRESULTADO_CONTRATADACOD";
         dynContagemResultado_ContratadaCod_Internalname = "CONTAGEMRESULTADO_CONTRATADACOD";
         lblTextblockcontagemresultado_sistemacod_Internalname = "TEXTBLOCKCONTAGEMRESULTADO_SISTEMACOD";
         edtContagemResultado_SistemaCod_Internalname = "CONTAGEMRESULTADO_SISTEMACOD";
         lblTextblockcontagemresultado_fncusrcod_Internalname = "TEXTBLOCKCONTAGEMRESULTADO_FNCUSRCOD";
         edtContagemResultado_FncUsrCod_Internalname = "CONTAGEMRESULTADO_FNCUSRCOD";
         lblTextblockcontagemresultado_prazoinicialdias_Internalname = "TEXTBLOCKCONTAGEMRESULTADO_PRAZOINICIALDIAS";
         edtContagemResultado_PrazoInicialDias_Internalname = "CONTAGEMRESULTADO_PRAZOINICIALDIAS";
         lblTextblockcontagemresultado_datacadastro_Internalname = "TEXTBLOCKCONTAGEMRESULTADO_DATACADASTRO";
         edtContagemResultado_DataCadastro_Internalname = "CONTAGEMRESULTADO_DATACADASTRO";
         lblTextblockcontagemresultado_osmanual_Internalname = "TEXTBLOCKCONTAGEMRESULTADO_OSMANUAL";
         chkContagemResultado_OSManual_Internalname = "CONTAGEMRESULTADO_OSMANUAL";
         tblTableattributes_Internalname = "TABLEATTRIBUTES";
         Dvpanel_tableattributes_Internalname = "DVPANEL_TABLEATTRIBUTES";
         tblTablecontent_Internalname = "TABLECONTENT";
         bttBtn_trn_enter_Internalname = "BTN_TRN_ENTER";
         bttBtn_trn_cancel_Internalname = "BTN_TRN_CANCEL";
         bttBtn_trn_delete_Internalname = "BTN_TRN_DELETE";
         tblTableactions_Internalname = "TABLEACTIONS";
         tblTablemain_Internalname = "TABLEMAIN";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Dvpanel_tableattributes_Autoscroll = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Iconposition = "left";
         Dvpanel_tableattributes_Showcollapseicon = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Autoheight = Convert.ToBoolean( -1);
         Dvpanel_tableattributes_Autowidth = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsed = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Collapsible = Convert.ToBoolean( 0);
         Dvpanel_tableattributes_Title = "Informa��es Gerais";
         Dvpanel_tableattributes_Cls = "GXUI-DVelop-Panel";
         Dvpanel_tableattributes_Width = "100%";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Solicitacao Servico";
         chkContagemResultado_OSManual.Enabled = 1;
         edtContagemResultado_DataCadastro_Jsonclick = "";
         edtContagemResultado_DataCadastro_Enabled = 1;
         edtContagemResultado_PrazoInicialDias_Jsonclick = "";
         edtContagemResultado_PrazoInicialDias_Enabled = 1;
         edtContagemResultado_FncUsrCod_Jsonclick = "";
         edtContagemResultado_FncUsrCod_Enabled = 1;
         edtContagemResultado_SistemaCod_Jsonclick = "";
         edtContagemResultado_SistemaCod_Enabled = 1;
         dynContagemResultado_ContratadaCod_Jsonclick = "";
         dynContagemResultado_ContratadaCod.Enabled = 1;
         dynModulo_Codigo_Jsonclick = "";
         dynModulo_Codigo.Enabled = 1;
         edtContagemResultado_LiqLogCod_Jsonclick = "";
         edtContagemResultado_LiqLogCod_Enabled = 1;
         edtContagemResultado_Responsavel_Jsonclick = "";
         edtContagemResultado_Responsavel_Enabled = 1;
         dynContagemResultado_ContadorFSCod_Jsonclick = "";
         dynContagemResultado_ContadorFSCod.Enabled = 1;
         dynContagemResultado_ContratadaOrigemCod_Jsonclick = "";
         dynContagemResultado_ContratadaOrigemCod.Enabled = 1;
         dynContagemResultado_LoteAceiteCod_Jsonclick = "";
         dynContagemResultado_LoteAceiteCod.Enabled = 1;
         dynContagemResultado_NaoCnfDmnCod_Jsonclick = "";
         dynContagemResultado_NaoCnfDmnCod.Enabled = 1;
         edtContagemResultado_OSVinculada_Jsonclick = "";
         edtContagemResultado_OSVinculada_Enabled = 1;
         edtContagemResultado_CntSrvCod_Jsonclick = "";
         edtContagemResultado_CntSrvCod_Enabled = 1;
         edtContagemResultado_DataDmn_Jsonclick = "";
         edtContagemResultado_DataDmn_Enabled = 1;
         cmbContagemResultado_StatusDmn_Jsonclick = "";
         cmbContagemResultado_StatusDmn.Enabled = 1;
         edtContagemResultado_PrioridadePrevista_Jsonclick = "";
         edtContagemResultado_PrioridadePrevista_Enabled = 1;
         edtContagemResultado_Restricoes_Enabled = 1;
         edtContagemResultado_Referencia_Enabled = 1;
         cmbContagemResultado_TipoRegistro_Jsonclick = "";
         cmbContagemResultado_TipoRegistro.Enabled = 1;
         cmbContagemResultado_Evento_Jsonclick = "";
         cmbContagemResultado_Evento.Enabled = 1;
         edtContagemResultado_UOOwner_Jsonclick = "";
         edtContagemResultado_UOOwner_Enabled = 1;
         edtContagemResultado_DemandaFM_Jsonclick = "";
         edtContagemResultado_DemandaFM_Enabled = 1;
         edtContagemResultado_SS_Jsonclick = "";
         edtContagemResultado_SS_Enabled = 1;
         dynContagemResultado_ServicoSS_Jsonclick = "";
         dynContagemResultado_ServicoSS.Enabled = 1;
         edtContagemResultado_HoraEntrega_Jsonclick = "";
         edtContagemResultado_HoraEntrega_Enabled = 1;
         edtContagemResultado_DataEntrega_Jsonclick = "";
         edtContagemResultado_DataEntrega_Enabled = 1;
         Contagemresultado_observacao_Enabled = Convert.ToBoolean( 1);
         edtContagemResultado_Descricao_Jsonclick = "";
         edtContagemResultado_Descricao_Enabled = 1;
         edtContagemResultado_Owner_Jsonclick = "";
         edtContagemResultado_Owner_Enabled = 1;
         edtContagemResultado_Codigo_Jsonclick = "";
         edtContagemResultado_Codigo_Enabled = 1;
         bttBtn_trn_delete_Enabled = 1;
         bttBtn_trn_delete_Visible = 1;
         bttBtn_trn_cancel_Visible = 1;
         bttBtn_trn_enter_Enabled = 1;
         bttBtn_trn_enter_Visible = 1;
         chkContagemResultado_OSManual.Caption = "";
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         GXAMODULO_CODIGO_html4169( A489ContagemResultado_SistemaCod) ;
         /* End function dynload_actions */
      }

      protected void GXDLACONTAGEMRESULTADO_LOTEACEITECOD411( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLACONTAGEMRESULTADO_LOTEACEITECOD_data411( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXACONTAGEMRESULTADO_LOTEACEITECOD_html411( )
      {
         int gxdynajaxvalue ;
         GXDLACONTAGEMRESULTADO_LOTEACEITECOD_data411( ) ;
         gxdynajaxindex = 1;
         dynContagemResultado_LoteAceiteCod.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynContagemResultado_LoteAceiteCod.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
      }

      protected void GXDLACONTAGEMRESULTADO_LOTEACEITECOD_data411( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         /* Using cursor T004158 */
         pr_default.execute(56);
         while ( (pr_default.getStatus(56) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(T004158_A596Lote_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( T004158_A563Lote_Nome[0]));
            pr_default.readNext(56);
         }
         pr_default.close(56);
      }

      protected void GXDLACONTAGEMRESULTADO_SERVICOSS411( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLACONTAGEMRESULTADO_SERVICOSS_data411( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXACONTAGEMRESULTADO_SERVICOSS_html411( )
      {
         int gxdynajaxvalue ;
         GXDLACONTAGEMRESULTADO_SERVICOSS_data411( ) ;
         gxdynajaxindex = 1;
         dynContagemResultado_ServicoSS.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynContagemResultado_ServicoSS.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
      }

      protected void GXDLACONTAGEMRESULTADO_SERVICOSS_data411( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         /* Using cursor T004159 */
         pr_default.execute(57);
         while ( (pr_default.getStatus(57) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(T004159_A155Servico_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( T004159_A608Servico_Nome[0]));
            pr_default.readNext(57);
         }
         pr_default.close(57);
      }

      protected void GXDLACONTAGEMRESULTADO_NAOCNFDMNCOD4169( wwpbaseobjects.SdtWWPContext AV7WWPContext )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLACONTAGEMRESULTADO_NAOCNFDMNCOD_data4169( AV7WWPContext) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXACONTAGEMRESULTADO_NAOCNFDMNCOD_html4169( wwpbaseobjects.SdtWWPContext AV7WWPContext )
      {
         int gxdynajaxvalue ;
         GXDLACONTAGEMRESULTADO_NAOCNFDMNCOD_data4169( AV7WWPContext) ;
         gxdynajaxindex = 1;
         dynContagemResultado_NaoCnfDmnCod.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynContagemResultado_NaoCnfDmnCod.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
      }

      protected void GXDLACONTAGEMRESULTADO_NAOCNFDMNCOD_data4169( wwpbaseobjects.SdtWWPContext AV7WWPContext )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("Nenhuma");
         /* Using cursor T004160 */
         pr_default.execute(58, new Object[] {AV7WWPContext.gxTpr_Areatrabalho_codigo});
         while ( (pr_default.getStatus(58) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(T004160_A426NaoConformidade_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( T004160_A427NaoConformidade_Nome[0]));
            pr_default.readNext(58);
         }
         pr_default.close(58);
      }

      protected void GXDLACONTAGEMRESULTADO_CONTRATADAORIGEMCOD4169( wwpbaseobjects.SdtWWPContext AV7WWPContext )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLACONTAGEMRESULTADO_CONTRATADAORIGEMCOD_data4169( AV7WWPContext) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXACONTAGEMRESULTADO_CONTRATADAORIGEMCOD_html4169( wwpbaseobjects.SdtWWPContext AV7WWPContext )
      {
         int gxdynajaxvalue ;
         GXDLACONTAGEMRESULTADO_CONTRATADAORIGEMCOD_data4169( AV7WWPContext) ;
         gxdynajaxindex = 1;
         dynContagemResultado_ContratadaOrigemCod.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynContagemResultado_ContratadaOrigemCod.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
      }

      protected void GXDLACONTAGEMRESULTADO_CONTRATADAORIGEMCOD_data4169( wwpbaseobjects.SdtWWPContext AV7WWPContext )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("Nenhuma");
         /* Using cursor T004161 */
         pr_default.execute(59, new Object[] {AV7WWPContext.gxTpr_Areatrabalho_codigo});
         while ( (pr_default.getStatus(59) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(T004161_A39Contratada_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( T004161_A41Contratada_PessoaNom[0]));
            pr_default.readNext(59);
         }
         pr_default.close(59);
      }

      protected void GXDLACONTAGEMRESULTADO_CONTADORFSCOD4169( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLACONTAGEMRESULTADO_CONTADORFSCOD_data4169( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXACONTAGEMRESULTADO_CONTADORFSCOD_html4169( )
      {
         int gxdynajaxvalue ;
         GXDLACONTAGEMRESULTADO_CONTADORFSCOD_data4169( ) ;
         gxdynajaxindex = 1;
         dynContagemResultado_ContadorFSCod.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynContagemResultado_ContadorFSCod.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
      }

      protected void GXDLACONTAGEMRESULTADO_CONTADORFSCOD_data4169( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("(Nenhum)");
         /* Using cursor T004162 */
         pr_default.execute(60);
         while ( (pr_default.getStatus(60) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(T004162_A454ContagemResultado_ContadorFSCod[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( T004162_A455ContagemResultado_ContadorFSNom[0]));
            pr_default.readNext(60);
         }
         pr_default.close(60);
      }

      protected void GXDLAMODULO_CODIGO4169( int A489ContagemResultado_SistemaCod )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLAMODULO_CODIGO_data4169( A489ContagemResultado_SistemaCod) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXAMODULO_CODIGO_html4169( int A489ContagemResultado_SistemaCod )
      {
         int gxdynajaxvalue ;
         GXDLAMODULO_CODIGO_data4169( A489ContagemResultado_SistemaCod) ;
         gxdynajaxindex = 1;
         dynModulo_Codigo.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynModulo_Codigo.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
      }

      protected void GXDLAMODULO_CODIGO_data4169( int A489ContagemResultado_SistemaCod )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("(Nenhum)");
         /* Using cursor T004163 */
         pr_default.execute(61, new Object[] {n489ContagemResultado_SistemaCod, A489ContagemResultado_SistemaCod});
         while ( (pr_default.getStatus(61) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(T004163_A146Modulo_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( T004163_A143Modulo_Nome[0]));
            pr_default.readNext(61);
         }
         pr_default.close(61);
      }

      protected void GXDLACONTAGEMRESULTADO_CONTRATADACOD4169( wwpbaseobjects.SdtWWPContext AV7WWPContext )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLACONTAGEMRESULTADO_CONTRATADACOD_data4169( AV7WWPContext) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXACONTAGEMRESULTADO_CONTRATADACOD_html4169( wwpbaseobjects.SdtWWPContext AV7WWPContext )
      {
         int gxdynajaxvalue ;
         GXDLACONTAGEMRESULTADO_CONTRATADACOD_data4169( AV7WWPContext) ;
         gxdynajaxindex = 1;
         dynContagemResultado_ContratadaCod.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynContagemResultado_ContratadaCod.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
      }

      protected void GXDLACONTAGEMRESULTADO_CONTRATADACOD_data4169( wwpbaseobjects.SdtWWPContext AV7WWPContext )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("(Nenhuma)");
         /* Using cursor T004164 */
         pr_default.execute(62, new Object[] {AV7WWPContext.gxTpr_Areatrabalho_codigo});
         while ( (pr_default.getStatus(62) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(T004164_A39Contratada_Codigo[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( T004164_A41Contratada_PessoaNom[0]));
            pr_default.readNext(62);
         }
         pr_default.close(62);
      }

      protected void AfterKeyLoadScreen( )
      {
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         getEqualNoModal( ) ;
         GX_FocusControl = edtContagemResultado_Owner_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         standaloneNotModal( ) ;
         standaloneModal( ) ;
         /* End function AfterKeyLoadScreen */
      }

      public void Valid_Contagemresultado_codigo( wwpbaseobjects.SdtWWPContext GX_Parm1 ,
                                                  int GX_Parm2 ,
                                                  int GX_Parm3 ,
                                                  String GX_Parm4 ,
                                                  String GX_Parm5 ,
                                                  DateTime GX_Parm6 ,
                                                  DateTime GX_Parm7 ,
                                                  DateTime GX_Parm8 ,
                                                  int GX_Parm9 ,
                                                  String GX_Parm10 ,
                                                  int GX_Parm11 ,
                                                  GXCombobox cmbGX_Parm12 ,
                                                  GXCombobox cmbGX_Parm13 ,
                                                  String GX_Parm14 ,
                                                  String GX_Parm15 ,
                                                  String GX_Parm16 ,
                                                  GXCombobox cmbGX_Parm17 ,
                                                  DateTime GX_Parm18 ,
                                                  int GX_Parm19 ,
                                                  short GX_Parm20 ,
                                                  DateTime GX_Parm21 ,
                                                  bool GX_Parm22 ,
                                                  bool GX_Parm23 ,
                                                  bool GX_Parm24 ,
                                                  GXCombobox dynGX_Parm25 ,
                                                  GXCombobox dynGX_Parm26 ,
                                                  int GX_Parm27 ,
                                                  GXCombobox dynGX_Parm28 ,
                                                  GXCombobox dynGX_Parm29 ,
                                                  GXCombobox dynGX_Parm30 ,
                                                  int GX_Parm31 ,
                                                  GXCombobox dynGX_Parm32 ,
                                                  GXCombobox dynGX_Parm33 ,
                                                  int GX_Parm34 ,
                                                  int GX_Parm35 ,
                                                  int GX_Parm36 )
      {
         AV7WWPContext = GX_Parm1;
         A456ContagemResultado_Codigo = GX_Parm2;
         A508ContagemResultado_Owner = GX_Parm3;
         A494ContagemResultado_Descricao = GX_Parm4;
         n494ContagemResultado_Descricao = false;
         A514ContagemResultado_Observacao = GX_Parm5;
         n514ContagemResultado_Observacao = false;
         A472ContagemResultado_DataEntrega = GX_Parm6;
         n472ContagemResultado_DataEntrega = false;
         A912ContagemResultado_HoraEntrega = GX_Parm7;
         n912ContagemResultado_HoraEntrega = false;
         A1351ContagemResultado_DataPrevista = GX_Parm8;
         n1351ContagemResultado_DataPrevista = false;
         A1452ContagemResultado_SS = GX_Parm9;
         n1452ContagemResultado_SS = false;
         A493ContagemResultado_DemandaFM = GX_Parm10;
         n493ContagemResultado_DemandaFM = false;
         A1584ContagemResultado_UOOwner = GX_Parm11;
         n1584ContagemResultado_UOOwner = false;
         cmbContagemResultado_Evento = cmbGX_Parm12;
         A1515ContagemResultado_Evento = (short)(NumberUtil.Val( cmbContagemResultado_Evento.CurrentValue, "."));
         n1515ContagemResultado_Evento = false;
         cmbContagemResultado_Evento.CurrentValue = StringUtil.LTrim( StringUtil.Str( (decimal)(A1515ContagemResultado_Evento), 2, 0));
         cmbContagemResultado_TipoRegistro = cmbGX_Parm13;
         A1583ContagemResultado_TipoRegistro = (short)(NumberUtil.Val( cmbContagemResultado_TipoRegistro.CurrentValue, "."));
         cmbContagemResultado_TipoRegistro.CurrentValue = StringUtil.LTrim( StringUtil.Str( (decimal)(A1583ContagemResultado_TipoRegistro), 4, 0));
         A1585ContagemResultado_Referencia = GX_Parm14;
         n1585ContagemResultado_Referencia = false;
         A1586ContagemResultado_Restricoes = GX_Parm15;
         n1586ContagemResultado_Restricoes = false;
         A1587ContagemResultado_PrioridadePrevista = GX_Parm16;
         n1587ContagemResultado_PrioridadePrevista = false;
         cmbContagemResultado_StatusDmn = cmbGX_Parm17;
         A484ContagemResultado_StatusDmn = cmbContagemResultado_StatusDmn.CurrentValue;
         n484ContagemResultado_StatusDmn = false;
         cmbContagemResultado_StatusDmn.CurrentValue = A484ContagemResultado_StatusDmn;
         A471ContagemResultado_DataDmn = GX_Parm18;
         A1044ContagemResultado_FncUsrCod = GX_Parm19;
         n1044ContagemResultado_FncUsrCod = false;
         A1227ContagemResultado_PrazoInicialDias = GX_Parm20;
         n1227ContagemResultado_PrazoInicialDias = false;
         A1350ContagemResultado_DataCadastro = GX_Parm21;
         n1350ContagemResultado_DataCadastro = false;
         A1173ContagemResultado_OSManual = GX_Parm22;
         n1173ContagemResultado_OSManual = false;
         A598ContagemResultado_Baseline = GX_Parm23;
         n598ContagemResultado_Baseline = false;
         A485ContagemResultado_EhValidacao = GX_Parm24;
         n485ContagemResultado_EhValidacao = false;
         dynModulo_Codigo = dynGX_Parm25;
         A146Modulo_Codigo = (int)(NumberUtil.Val( dynModulo_Codigo.CurrentValue, "."));
         n146Modulo_Codigo = false;
         dynContagemResultado_ContadorFSCod = dynGX_Parm26;
         A454ContagemResultado_ContadorFSCod = (int)(NumberUtil.Val( dynContagemResultado_ContadorFSCod.CurrentValue, "."));
         n454ContagemResultado_ContadorFSCod = false;
         A890ContagemResultado_Responsavel = GX_Parm27;
         n890ContagemResultado_Responsavel = false;
         dynContagemResultado_NaoCnfDmnCod = dynGX_Parm28;
         A468ContagemResultado_NaoCnfDmnCod = (int)(NumberUtil.Val( dynContagemResultado_NaoCnfDmnCod.CurrentValue, "."));
         n468ContagemResultado_NaoCnfDmnCod = false;
         dynContagemResultado_ContratadaCod = dynGX_Parm29;
         A490ContagemResultado_ContratadaCod = (int)(NumberUtil.Val( dynContagemResultado_ContratadaCod.CurrentValue, "."));
         n490ContagemResultado_ContratadaCod = false;
         dynContagemResultado_ContratadaOrigemCod = dynGX_Parm30;
         A805ContagemResultado_ContratadaOrigemCod = (int)(NumberUtil.Val( dynContagemResultado_ContratadaOrigemCod.CurrentValue, "."));
         n805ContagemResultado_ContratadaOrigemCod = false;
         A489ContagemResultado_SistemaCod = GX_Parm31;
         n489ContagemResultado_SistemaCod = false;
         dynContagemResultado_LoteAceiteCod = dynGX_Parm32;
         A597ContagemResultado_LoteAceiteCod = (int)(NumberUtil.Val( dynContagemResultado_LoteAceiteCod.CurrentValue, "."));
         n597ContagemResultado_LoteAceiteCod = false;
         dynContagemResultado_ServicoSS = dynGX_Parm33;
         A1636ContagemResultado_ServicoSS = (int)(NumberUtil.Val( dynContagemResultado_ServicoSS.CurrentValue, "."));
         n1636ContagemResultado_ServicoSS = false;
         A1043ContagemResultado_LiqLogCod = GX_Parm34;
         n1043ContagemResultado_LiqLogCod = false;
         A1553ContagemResultado_CntSrvCod = GX_Parm35;
         n1553ContagemResultado_CntSrvCod = false;
         A602ContagemResultado_OSVinculada = GX_Parm36;
         n602ContagemResultado_OSVinculada = false;
         context.wbHandled = 1;
         AfterKeyLoadScreen( ) ;
         Draw( ) ;
         SendCloseFormHiddens( ) ;
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
         }
         dynContagemResultado_ServicoSS.removeAllItems();
         /* Using cursor T004165 */
         pr_default.execute(63);
         while ( (pr_default.getStatus(63) != 101) )
         {
            dynContagemResultado_ServicoSS.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(T004165_A155Servico_Codigo[0]), 6, 0)), T004165_A608Servico_Nome[0], 0);
            pr_default.readNext(63);
         }
         pr_default.close(63);
         dynContagemResultado_ServicoSS.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A1636ContagemResultado_ServicoSS), 6, 0));
         dynContagemResultado_NaoCnfDmnCod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A468ContagemResultado_NaoCnfDmnCod), 6, 0));
         dynContagemResultado_LoteAceiteCod.removeAllItems();
         /* Using cursor T004166 */
         pr_default.execute(64);
         while ( (pr_default.getStatus(64) != 101) )
         {
            dynContagemResultado_LoteAceiteCod.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(T004166_A596Lote_Codigo[0]), 6, 0)), T004166_A563Lote_Nome[0], 0);
            pr_default.readNext(64);
         }
         pr_default.close(64);
         dynContagemResultado_LoteAceiteCod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A597ContagemResultado_LoteAceiteCod), 6, 0));
         dynContagemResultado_ContratadaOrigemCod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A805ContagemResultado_ContratadaOrigemCod), 6, 0));
         GXACONTAGEMRESULTADO_CONTADORFSCOD_html4169( ) ;
         dynContagemResultado_ContadorFSCod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A454ContagemResultado_ContadorFSCod), 6, 0));
         dynContagemResultado_ContratadaCod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A490ContagemResultado_ContratadaCod), 6, 0));
         dynModulo_Codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A146Modulo_Codigo), 6, 0));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A508ContagemResultado_Owner), 6, 0, ".", "")));
         isValidOutput.Add(A494ContagemResultado_Descricao);
         isValidOutput.Add(A514ContagemResultado_Observacao);
         isValidOutput.Add(context.localUtil.Format(A472ContagemResultado_DataEntrega, "99/99/99"));
         isValidOutput.Add(context.localUtil.TToC( A912ContagemResultado_HoraEntrega, 10, 8, 0, 3, "/", ":", " "));
         isValidOutput.Add(context.localUtil.TToC( A1351ContagemResultado_DataPrevista, 10, 8, 0, 3, "/", ":", " "));
         if ( dynContagemResultado_ServicoSS.ItemCount > 0 )
         {
            A1636ContagemResultado_ServicoSS = (int)(NumberUtil.Val( dynContagemResultado_ServicoSS.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1636ContagemResultado_ServicoSS), 6, 0))), "."));
            n1636ContagemResultado_ServicoSS = false;
         }
         dynContagemResultado_ServicoSS.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A1636ContagemResultado_ServicoSS), 6, 0));
         isValidOutput.Add(dynContagemResultado_ServicoSS);
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A1452ContagemResultado_SS), 8, 0, ".", "")));
         isValidOutput.Add(A493ContagemResultado_DemandaFM);
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A1584ContagemResultado_UOOwner), 6, 0, ".", "")));
         cmbContagemResultado_Evento.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A1515ContagemResultado_Evento), 2, 0));
         isValidOutput.Add(cmbContagemResultado_Evento);
         cmbContagemResultado_TipoRegistro.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A1583ContagemResultado_TipoRegistro), 4, 0));
         isValidOutput.Add(cmbContagemResultado_TipoRegistro);
         isValidOutput.Add(A1585ContagemResultado_Referencia);
         isValidOutput.Add(A1586ContagemResultado_Restricoes);
         isValidOutput.Add(A1587ContagemResultado_PrioridadePrevista);
         cmbContagemResultado_StatusDmn.CurrentValue = A484ContagemResultado_StatusDmn;
         isValidOutput.Add(cmbContagemResultado_StatusDmn);
         isValidOutput.Add(context.localUtil.Format(A471ContagemResultado_DataDmn, "99/99/99"));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A1553ContagemResultado_CntSrvCod), 6, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A602ContagemResultado_OSVinculada), 6, 0, ".", "")));
         if ( dynContagemResultado_NaoCnfDmnCod.ItemCount > 0 )
         {
            A468ContagemResultado_NaoCnfDmnCod = (int)(NumberUtil.Val( dynContagemResultado_NaoCnfDmnCod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A468ContagemResultado_NaoCnfDmnCod), 6, 0))), "."));
            n468ContagemResultado_NaoCnfDmnCod = false;
         }
         dynContagemResultado_NaoCnfDmnCod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A468ContagemResultado_NaoCnfDmnCod), 6, 0));
         isValidOutput.Add(dynContagemResultado_NaoCnfDmnCod);
         if ( dynContagemResultado_LoteAceiteCod.ItemCount > 0 )
         {
            A597ContagemResultado_LoteAceiteCod = (int)(NumberUtil.Val( dynContagemResultado_LoteAceiteCod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A597ContagemResultado_LoteAceiteCod), 6, 0))), "."));
            n597ContagemResultado_LoteAceiteCod = false;
         }
         dynContagemResultado_LoteAceiteCod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A597ContagemResultado_LoteAceiteCod), 6, 0));
         isValidOutput.Add(dynContagemResultado_LoteAceiteCod);
         if ( dynContagemResultado_ContratadaOrigemCod.ItemCount > 0 )
         {
            A805ContagemResultado_ContratadaOrigemCod = (int)(NumberUtil.Val( dynContagemResultado_ContratadaOrigemCod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A805ContagemResultado_ContratadaOrigemCod), 6, 0))), "."));
            n805ContagemResultado_ContratadaOrigemCod = false;
         }
         dynContagemResultado_ContratadaOrigemCod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A805ContagemResultado_ContratadaOrigemCod), 6, 0));
         isValidOutput.Add(dynContagemResultado_ContratadaOrigemCod);
         if ( dynContagemResultado_ContadorFSCod.ItemCount > 0 )
         {
            A454ContagemResultado_ContadorFSCod = (int)(NumberUtil.Val( dynContagemResultado_ContadorFSCod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A454ContagemResultado_ContadorFSCod), 6, 0))), "."));
            n454ContagemResultado_ContadorFSCod = false;
         }
         dynContagemResultado_ContadorFSCod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A454ContagemResultado_ContadorFSCod), 6, 0));
         isValidOutput.Add(dynContagemResultado_ContadorFSCod);
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A890ContagemResultado_Responsavel), 6, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A1043ContagemResultado_LiqLogCod), 6, 0, ".", "")));
         if ( dynContagemResultado_ContratadaCod.ItemCount > 0 )
         {
            A490ContagemResultado_ContratadaCod = (int)(NumberUtil.Val( dynContagemResultado_ContratadaCod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A490ContagemResultado_ContratadaCod), 6, 0))), "."));
            n490ContagemResultado_ContratadaCod = false;
         }
         dynContagemResultado_ContratadaCod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A490ContagemResultado_ContratadaCod), 6, 0));
         isValidOutput.Add(dynContagemResultado_ContratadaCod);
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A489ContagemResultado_SistemaCod), 6, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A1044ContagemResultado_FncUsrCod), 6, 0, ".", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A1227ContagemResultado_PrazoInicialDias), 4, 0, ".", "")));
         isValidOutput.Add(context.localUtil.TToC( A1350ContagemResultado_DataCadastro, 10, 8, 0, 3, "/", ":", " "));
         isValidOutput.Add(A1173ContagemResultado_OSManual);
         isValidOutput.Add(A598ContagemResultado_Baseline);
         isValidOutput.Add(A485ContagemResultado_EhValidacao);
         if ( dynModulo_Codigo.ItemCount > 0 )
         {
            A146Modulo_Codigo = (int)(NumberUtil.Val( dynModulo_Codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A146Modulo_Codigo), 6, 0))), "."));
            n146Modulo_Codigo = false;
         }
         dynModulo_Codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A146Modulo_Codigo), 6, 0));
         isValidOutput.Add(dynModulo_Codigo);
         isValidOutput.Add(StringUtil.RTrim( Gx_mode));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z456ContagemResultado_Codigo), 6, 0, ",", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z508ContagemResultado_Owner), 6, 0, ",", "")));
         isValidOutput.Add(Z494ContagemResultado_Descricao);
         isValidOutput.Add(Z514ContagemResultado_Observacao);
         isValidOutput.Add(context.localUtil.DToC( Z472ContagemResultado_DataEntrega, 0, "/"));
         isValidOutput.Add(context.localUtil.TToC( Z912ContagemResultado_HoraEntrega, 10, 8, 0, 0, "/", ":", " "));
         isValidOutput.Add(context.localUtil.TToC( Z1351ContagemResultado_DataPrevista, 10, 8, 0, 0, "/", ":", " "));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1636ContagemResultado_ServicoSS), 6, 0, ",", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1452ContagemResultado_SS), 8, 0, ",", "")));
         isValidOutput.Add(Z493ContagemResultado_DemandaFM);
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1584ContagemResultado_UOOwner), 6, 0, ",", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1515ContagemResultado_Evento), 2, 0, ",", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1583ContagemResultado_TipoRegistro), 4, 0, ",", "")));
         isValidOutput.Add(Z1585ContagemResultado_Referencia);
         isValidOutput.Add(Z1586ContagemResultado_Restricoes);
         isValidOutput.Add(Z1587ContagemResultado_PrioridadePrevista);
         isValidOutput.Add(StringUtil.RTrim( Z484ContagemResultado_StatusDmn));
         isValidOutput.Add(context.localUtil.DToC( Z471ContagemResultado_DataDmn, 0, "/"));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1553ContagemResultado_CntSrvCod), 6, 0, ",", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z602ContagemResultado_OSVinculada), 6, 0, ",", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z468ContagemResultado_NaoCnfDmnCod), 6, 0, ",", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z597ContagemResultado_LoteAceiteCod), 6, 0, ",", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z805ContagemResultado_ContratadaOrigemCod), 6, 0, ",", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z454ContagemResultado_ContadorFSCod), 6, 0, ",", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z890ContagemResultado_Responsavel), 6, 0, ",", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1043ContagemResultado_LiqLogCod), 6, 0, ",", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z490ContagemResultado_ContratadaCod), 6, 0, ",", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z489ContagemResultado_SistemaCod), 6, 0, ",", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1044ContagemResultado_FncUsrCod), 6, 0, ",", "")));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1227ContagemResultado_PrazoInicialDias), 4, 0, ",", "")));
         isValidOutput.Add(context.localUtil.TToC( Z1350ContagemResultado_DataCadastro, 10, 8, 0, 0, "/", ":", " "));
         isValidOutput.Add(Z1173ContagemResultado_OSManual);
         isValidOutput.Add(Z598ContagemResultado_Baseline);
         isValidOutput.Add(Z485ContagemResultado_EhValidacao);
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z146Modulo_Codigo), 6, 0, ",", "")));
         isValidOutput.Add(bttBtn_trn_delete_Enabled);
         isValidOutput.Add(bttBtn_trn_enter_Enabled);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Contagemresultado_servicoss( GXCombobox dynGX_Parm1 )
      {
         dynContagemResultado_ServicoSS = dynGX_Parm1;
         A1636ContagemResultado_ServicoSS = (int)(NumberUtil.Val( dynContagemResultado_ServicoSS.CurrentValue, "."));
         n1636ContagemResultado_ServicoSS = false;
         /* Using cursor T004167 */
         pr_default.execute(65, new Object[] {n1636ContagemResultado_ServicoSS, A1636ContagemResultado_ServicoSS});
         if ( (pr_default.getStatus(65) == 101) )
         {
            if ( ! ( (0==A1636ContagemResultado_ServicoSS) ) )
            {
               GX_msglist.addItem("N�o existe 'Contagem Resultado_Servico'.", "ForeignKeyNotFound", 1, "CONTAGEMRESULTADO_SERVICOSS");
               AnyError = 1;
               GX_FocusControl = dynContagemResultado_ServicoSS_Internalname;
            }
         }
         pr_default.close(65);
         dynload_actions( ) ;
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Contagemresultado_cntsrvcod( int GX_Parm1 )
      {
         A1553ContagemResultado_CntSrvCod = GX_Parm1;
         n1553ContagemResultado_CntSrvCod = false;
         /* Using cursor T004168 */
         pr_default.execute(66, new Object[] {n1553ContagemResultado_CntSrvCod, A1553ContagemResultado_CntSrvCod});
         if ( (pr_default.getStatus(66) == 101) )
         {
            if ( ! ( (0==A1553ContagemResultado_CntSrvCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Contagem Resultado_Contrato Servicos'.", "ForeignKeyNotFound", 1, "CONTAGEMRESULTADO_CNTSRVCOD");
               AnyError = 1;
               GX_FocusControl = edtContagemResultado_CntSrvCod_Internalname;
            }
         }
         pr_default.close(66);
         dynload_actions( ) ;
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Contagemresultado_osvinculada( int GX_Parm1 )
      {
         A602ContagemResultado_OSVinculada = GX_Parm1;
         n602ContagemResultado_OSVinculada = false;
         /* Using cursor T004169 */
         pr_default.execute(67, new Object[] {n602ContagemResultado_OSVinculada, A602ContagemResultado_OSVinculada});
         if ( (pr_default.getStatus(67) == 101) )
         {
            if ( ! ( (0==A602ContagemResultado_OSVinculada) ) )
            {
               GX_msglist.addItem("N�o existe 'OS Vinculada'.", "ForeignKeyNotFound", 1, "CONTAGEMRESULTADO_OSVINCULADA");
               AnyError = 1;
               GX_FocusControl = edtContagemResultado_OSVinculada_Internalname;
            }
         }
         pr_default.close(67);
         dynload_actions( ) ;
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Contagemresultado_naocnfdmncod( GXCombobox dynGX_Parm1 )
      {
         dynContagemResultado_NaoCnfDmnCod = dynGX_Parm1;
         A468ContagemResultado_NaoCnfDmnCod = (int)(NumberUtil.Val( dynContagemResultado_NaoCnfDmnCod.CurrentValue, "."));
         n468ContagemResultado_NaoCnfDmnCod = false;
         /* Using cursor T004170 */
         pr_default.execute(68, new Object[] {n468ContagemResultado_NaoCnfDmnCod, A468ContagemResultado_NaoCnfDmnCod});
         if ( (pr_default.getStatus(68) == 101) )
         {
            if ( ! ( (0==A468ContagemResultado_NaoCnfDmnCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Contagem Resultado_Nao Conformidade da Demanda'.", "ForeignKeyNotFound", 1, "CONTAGEMRESULTADO_NAOCNFDMNCOD");
               AnyError = 1;
               GX_FocusControl = dynContagemResultado_NaoCnfDmnCod_Internalname;
            }
         }
         pr_default.close(68);
         dynload_actions( ) ;
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Contagemresultado_loteaceitecod( GXCombobox dynGX_Parm1 )
      {
         dynContagemResultado_LoteAceiteCod = dynGX_Parm1;
         A597ContagemResultado_LoteAceiteCod = (int)(NumberUtil.Val( dynContagemResultado_LoteAceiteCod.CurrentValue, "."));
         n597ContagemResultado_LoteAceiteCod = false;
         /* Using cursor T004171 */
         pr_default.execute(69, new Object[] {n597ContagemResultado_LoteAceiteCod, A597ContagemResultado_LoteAceiteCod});
         if ( (pr_default.getStatus(69) == 101) )
         {
            if ( ! ( (0==A597ContagemResultado_LoteAceiteCod) ) )
            {
               GX_msglist.addItem("N�o existe 'ST_Contagem Resultado_Lote'.", "ForeignKeyNotFound", 1, "CONTAGEMRESULTADO_LOTEACEITECOD");
               AnyError = 1;
               GX_FocusControl = dynContagemResultado_LoteAceiteCod_Internalname;
            }
         }
         pr_default.close(69);
         dynload_actions( ) ;
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Contagemresultado_contratadaorigemcod( GXCombobox dynGX_Parm1 )
      {
         dynContagemResultado_ContratadaOrigemCod = dynGX_Parm1;
         A805ContagemResultado_ContratadaOrigemCod = (int)(NumberUtil.Val( dynContagemResultado_ContratadaOrigemCod.CurrentValue, "."));
         n805ContagemResultado_ContratadaOrigemCod = false;
         /* Using cursor T004172 */
         pr_default.execute(70, new Object[] {n805ContagemResultado_ContratadaOrigemCod, A805ContagemResultado_ContratadaOrigemCod});
         if ( (pr_default.getStatus(70) == 101) )
         {
            if ( ! ( (0==A805ContagemResultado_ContratadaOrigemCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Contagem Resutlado_Contratada Origem'.", "ForeignKeyNotFound", 1, "CONTAGEMRESULTADO_CONTRATADAORIGEMCOD");
               AnyError = 1;
               GX_FocusControl = dynContagemResultado_ContratadaOrigemCod_Internalname;
            }
         }
         pr_default.close(70);
         dynload_actions( ) ;
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Contagemresultado_contadorfscod( GXCombobox dynGX_Parm1 )
      {
         dynContagemResultado_ContadorFSCod = dynGX_Parm1;
         A454ContagemResultado_ContadorFSCod = (int)(NumberUtil.Val( dynContagemResultado_ContadorFSCod.CurrentValue, "."));
         n454ContagemResultado_ContadorFSCod = false;
         /* Using cursor T004173 */
         pr_default.execute(71, new Object[] {n454ContagemResultado_ContadorFSCod, A454ContagemResultado_ContadorFSCod});
         if ( (pr_default.getStatus(71) == 101) )
         {
            if ( ! ( (0==A454ContagemResultado_ContadorFSCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Contagem Resultado_Usuario Contador Fab. de Soft'.", "ForeignKeyNotFound", 1, "CONTAGEMRESULTADO_CONTADORFSCOD");
               AnyError = 1;
               GX_FocusControl = dynContagemResultado_ContadorFSCod_Internalname;
            }
         }
         pr_default.close(71);
         dynload_actions( ) ;
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Contagemresultado_responsavel( int GX_Parm1 )
      {
         A890ContagemResultado_Responsavel = GX_Parm1;
         n890ContagemResultado_Responsavel = false;
         /* Using cursor T004174 */
         pr_default.execute(72, new Object[] {n890ContagemResultado_Responsavel, A890ContagemResultado_Responsavel});
         if ( (pr_default.getStatus(72) == 101) )
         {
            if ( ! ( (0==A890ContagemResultado_Responsavel) ) )
            {
               GX_msglist.addItem("N�o existe 'Contagem Resultado_Usuario Responsavel'.", "ForeignKeyNotFound", 1, "CONTAGEMRESULTADO_RESPONSAVEL");
               AnyError = 1;
               GX_FocusControl = edtContagemResultado_Responsavel_Internalname;
            }
         }
         pr_default.close(72);
         dynload_actions( ) ;
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Contagemresultado_liqlogcod( int GX_Parm1 )
      {
         A1043ContagemResultado_LiqLogCod = GX_Parm1;
         n1043ContagemResultado_LiqLogCod = false;
         /* Using cursor T004175 */
         pr_default.execute(73, new Object[] {n1043ContagemResultado_LiqLogCod, A1043ContagemResultado_LiqLogCod});
         if ( (pr_default.getStatus(73) == 101) )
         {
            if ( ! ( (0==A1043ContagemResultado_LiqLogCod) ) )
            {
               GX_msglist.addItem("N�o existe 'ST_Contagem Resultado_Contagem Resultado Liq Log'.", "ForeignKeyNotFound", 1, "CONTAGEMRESULTADO_LIQLOGCOD");
               AnyError = 1;
               GX_FocusControl = edtContagemResultado_LiqLogCod_Internalname;
            }
         }
         pr_default.close(73);
         dynload_actions( ) ;
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Modulo_codigo( GXCombobox dynGX_Parm1 )
      {
         dynModulo_Codigo = dynGX_Parm1;
         A146Modulo_Codigo = (int)(NumberUtil.Val( dynModulo_Codigo.CurrentValue, "."));
         n146Modulo_Codigo = false;
         /* Using cursor T004176 */
         pr_default.execute(74, new Object[] {n146Modulo_Codigo, A146Modulo_Codigo});
         if ( (pr_default.getStatus(74) == 101) )
         {
            if ( ! ( (0==A146Modulo_Codigo) ) )
            {
               GX_msglist.addItem("N�o existe 'Modulo'.", "ForeignKeyNotFound", 1, "MODULO_CODIGO");
               AnyError = 1;
               GX_FocusControl = dynModulo_Codigo_Internalname;
            }
         }
         pr_default.close(74);
         dynload_actions( ) ;
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Contagemresultado_contratadacod( GXCombobox dynGX_Parm1 )
      {
         dynContagemResultado_ContratadaCod = dynGX_Parm1;
         A490ContagemResultado_ContratadaCod = (int)(NumberUtil.Val( dynContagemResultado_ContratadaCod.CurrentValue, "."));
         n490ContagemResultado_ContratadaCod = false;
         /* Using cursor T004177 */
         pr_default.execute(75, new Object[] {n490ContagemResultado_ContratadaCod, A490ContagemResultado_ContratadaCod});
         if ( (pr_default.getStatus(75) == 101) )
         {
            if ( ! ( (0==A490ContagemResultado_ContratadaCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Contagem Resultado_Contratada'.", "ForeignKeyNotFound", 1, "CONTAGEMRESULTADO_CONTRATADACOD");
               AnyError = 1;
               GX_FocusControl = dynContagemResultado_ContratadaCod_Internalname;
            }
         }
         pr_default.close(75);
         dynload_actions( ) ;
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Contagemresultado_sistemacod( int GX_Parm1 ,
                                                      GXCombobox dynGX_Parm2 )
      {
         A489ContagemResultado_SistemaCod = GX_Parm1;
         n489ContagemResultado_SistemaCod = false;
         dynModulo_Codigo = dynGX_Parm2;
         A146Modulo_Codigo = (int)(NumberUtil.Val( dynModulo_Codigo.CurrentValue, "."));
         n146Modulo_Codigo = false;
         /* Using cursor T004178 */
         pr_default.execute(76, new Object[] {n489ContagemResultado_SistemaCod, A489ContagemResultado_SistemaCod});
         if ( (pr_default.getStatus(76) == 101) )
         {
            if ( ! ( (0==A489ContagemResultado_SistemaCod) ) )
            {
               GX_msglist.addItem("N�o existe 'Contagem Resultado_Sistema'.", "ForeignKeyNotFound", 1, "CONTAGEMRESULTADO_SISTEMACOD");
               AnyError = 1;
               GX_FocusControl = edtContagemResultado_SistemaCod_Internalname;
            }
         }
         pr_default.close(76);
         GXAMODULO_CODIGO_html4169( A489ContagemResultado_SistemaCod) ;
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
         }
         dynModulo_Codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A146Modulo_Codigo), 6, 0));
         if ( dynModulo_Codigo.ItemCount > 0 )
         {
            A146Modulo_Codigo = (int)(NumberUtil.Val( dynModulo_Codigo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A146Modulo_Codigo), 6, 0))), "."));
            n146Modulo_Codigo = false;
         }
         dynModulo_Codigo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A146Modulo_Codigo), 6, 0));
         isValidOutput.Add(dynModulo_Codigo);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("AFTER TRN","{handler:'E12412',iparms:[],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(74);
         pr_default.close(71);
         pr_default.close(72);
         pr_default.close(68);
         pr_default.close(75);
         pr_default.close(70);
         pr_default.close(76);
         pr_default.close(69);
         pr_default.close(65);
         pr_default.close(73);
         pr_default.close(66);
         pr_default.close(67);
      }

      public override void initialize( )
      {
         sPrefix = "";
         Z494ContagemResultado_Descricao = "";
         Z472ContagemResultado_DataEntrega = DateTime.MinValue;
         Z912ContagemResultado_HoraEntrega = (DateTime)(DateTime.MinValue);
         Z1351ContagemResultado_DataPrevista = (DateTime)(DateTime.MinValue);
         Z493ContagemResultado_DemandaFM = "";
         Z1585ContagemResultado_Referencia = "";
         Z1586ContagemResultado_Restricoes = "";
         Z1587ContagemResultado_PrioridadePrevista = "";
         Z484ContagemResultado_StatusDmn = "";
         Z471ContagemResultado_DataDmn = DateTime.MinValue;
         Z1350ContagemResultado_DataCadastro = (DateTime)(DateTime.MinValue);
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV7WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         GXKey = "";
         T004116_A155Servico_Codigo = new int[1] ;
         T004116_A608Servico_Nome = new String[] {""} ;
         A484ContagemResultado_StatusDmn = "";
         T004117_A596Lote_Codigo = new int[1] ;
         T004117_A563Lote_Nome = new String[] {""} ;
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         sStyleString = "";
         lblSolicitacaoservicotitle_Jsonclick = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         bttBtn_trn_enter_Jsonclick = "";
         bttBtn_trn_cancel_Jsonclick = "";
         bttBtn_trn_delete_Jsonclick = "";
         lblTextblockcontagemresultado_codigo_Jsonclick = "";
         lblTextblockcontagemresultado_owner_Jsonclick = "";
         lblTextblockcontagemresultado_descricao_Jsonclick = "";
         A494ContagemResultado_Descricao = "";
         lblTextblockcontagemresultado_observacao_Jsonclick = "";
         lblTextblockcontagemresultado_dataentrega_Jsonclick = "";
         A472ContagemResultado_DataEntrega = DateTime.MinValue;
         lblTextblockcontagemresultado_horaentrega_Jsonclick = "";
         A912ContagemResultado_HoraEntrega = (DateTime)(DateTime.MinValue);
         lblTextblockcontagemresultado_servicoss_Jsonclick = "";
         lblTextblockcontagemresultado_ss_Jsonclick = "";
         lblTextblockcontagemresultado_demandafm_Jsonclick = "";
         A493ContagemResultado_DemandaFM = "";
         lblTextblockcontagemresultado_uoowner_Jsonclick = "";
         lblTextblockcontagemresultado_evento_Jsonclick = "";
         lblTextblockcontagemresultado_tiporegistro_Jsonclick = "";
         lblTextblockcontagemresultado_referencia_Jsonclick = "";
         A1585ContagemResultado_Referencia = "";
         lblTextblockcontagemresultado_restricoes_Jsonclick = "";
         A1586ContagemResultado_Restricoes = "";
         lblTextblockcontagemresultado_prioridadeprevista_Jsonclick = "";
         A1587ContagemResultado_PrioridadePrevista = "";
         lblTextblockcontagemresultado_statusdmn_Jsonclick = "";
         lblTextblockcontagemresultado_datadmn_Jsonclick = "";
         A471ContagemResultado_DataDmn = DateTime.MinValue;
         lblTextblockcontagemresultado_cntsrvcod_Jsonclick = "";
         lblTextblockcontagemresultado_osvinculada_Jsonclick = "";
         lblTextblockcontagemresultado_naocnfdmncod_Jsonclick = "";
         lblTextblockcontagemresultado_loteaceitecod_Jsonclick = "";
         lblTextblockcontagemresultado_contratadaorigemcod_Jsonclick = "";
         lblTextblockcontagemresultado_contadorfscod_Jsonclick = "";
         lblTextblockcontagemresultado_responsavel_Jsonclick = "";
         lblTextblockcontagemresultado_liqlogcod_Jsonclick = "";
         lblTextblockmodulo_codigo_Jsonclick = "";
         lblTextblockcontagemresultado_contratadacod_Jsonclick = "";
         lblTextblockcontagemresultado_sistemacod_Jsonclick = "";
         lblTextblockcontagemresultado_fncusrcod_Jsonclick = "";
         lblTextblockcontagemresultado_prazoinicialdias_Jsonclick = "";
         lblTextblockcontagemresultado_datacadastro_Jsonclick = "";
         A1350ContagemResultado_DataCadastro = (DateTime)(DateTime.MinValue);
         lblTextblockcontagemresultado_osmanual_Jsonclick = "";
         A1351ContagemResultado_DataPrevista = (DateTime)(DateTime.MinValue);
         Gx_mode = "";
         Gx_date = DateTime.MinValue;
         A514ContagemResultado_Observacao = "";
         AV26Pgmname = "";
         Contagemresultado_observacao_Width = "";
         Contagemresultado_observacao_Height = "";
         Contagemresultado_observacao_Skin = "";
         Contagemresultado_observacao_Toolbar = "";
         Contagemresultado_observacao_Class = "";
         Contagemresultado_observacao_Customtoolbar = "";
         Contagemresultado_observacao_Customconfiguration = "";
         Contagemresultado_observacao_Buttonpressedid = "";
         Contagemresultado_observacao_Captionvalue = "";
         Contagemresultado_observacao_Captionclass = "";
         Contagemresultado_observacao_Captionposition = "";
         Contagemresultado_observacao_Coltitle = "";
         Contagemresultado_observacao_Coltitlefont = "";
         Dvpanel_tableattributes_Height = "";
         Dvpanel_tableattributes_Class = "";
         forbiddenHiddens = "";
         hsh = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV8TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV9WebSession = context.GetSession();
         AV22TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         Z514ContagemResultado_Observacao = "";
         T004118_A456ContagemResultado_Codigo = new int[1] ;
         T004118_A508ContagemResultado_Owner = new int[1] ;
         T004118_A494ContagemResultado_Descricao = new String[] {""} ;
         T004118_n494ContagemResultado_Descricao = new bool[] {false} ;
         T004118_A514ContagemResultado_Observacao = new String[] {""} ;
         T004118_n514ContagemResultado_Observacao = new bool[] {false} ;
         T004118_A472ContagemResultado_DataEntrega = new DateTime[] {DateTime.MinValue} ;
         T004118_n472ContagemResultado_DataEntrega = new bool[] {false} ;
         T004118_A912ContagemResultado_HoraEntrega = new DateTime[] {DateTime.MinValue} ;
         T004118_n912ContagemResultado_HoraEntrega = new bool[] {false} ;
         T004118_A1351ContagemResultado_DataPrevista = new DateTime[] {DateTime.MinValue} ;
         T004118_n1351ContagemResultado_DataPrevista = new bool[] {false} ;
         T004118_A1452ContagemResultado_SS = new int[1] ;
         T004118_n1452ContagemResultado_SS = new bool[] {false} ;
         T004118_A493ContagemResultado_DemandaFM = new String[] {""} ;
         T004118_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         T004118_A1584ContagemResultado_UOOwner = new int[1] ;
         T004118_n1584ContagemResultado_UOOwner = new bool[] {false} ;
         T004118_A1515ContagemResultado_Evento = new short[1] ;
         T004118_n1515ContagemResultado_Evento = new bool[] {false} ;
         T004118_A1583ContagemResultado_TipoRegistro = new short[1] ;
         T004118_A1585ContagemResultado_Referencia = new String[] {""} ;
         T004118_n1585ContagemResultado_Referencia = new bool[] {false} ;
         T004118_A1586ContagemResultado_Restricoes = new String[] {""} ;
         T004118_n1586ContagemResultado_Restricoes = new bool[] {false} ;
         T004118_A1587ContagemResultado_PrioridadePrevista = new String[] {""} ;
         T004118_n1587ContagemResultado_PrioridadePrevista = new bool[] {false} ;
         T004118_A484ContagemResultado_StatusDmn = new String[] {""} ;
         T004118_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         T004118_A471ContagemResultado_DataDmn = new DateTime[] {DateTime.MinValue} ;
         T004118_A1044ContagemResultado_FncUsrCod = new int[1] ;
         T004118_n1044ContagemResultado_FncUsrCod = new bool[] {false} ;
         T004118_A1227ContagemResultado_PrazoInicialDias = new short[1] ;
         T004118_n1227ContagemResultado_PrazoInicialDias = new bool[] {false} ;
         T004118_A1350ContagemResultado_DataCadastro = new DateTime[] {DateTime.MinValue} ;
         T004118_n1350ContagemResultado_DataCadastro = new bool[] {false} ;
         T004118_A1173ContagemResultado_OSManual = new bool[] {false} ;
         T004118_n1173ContagemResultado_OSManual = new bool[] {false} ;
         T004118_A598ContagemResultado_Baseline = new bool[] {false} ;
         T004118_n598ContagemResultado_Baseline = new bool[] {false} ;
         T004118_A485ContagemResultado_EhValidacao = new bool[] {false} ;
         T004118_n485ContagemResultado_EhValidacao = new bool[] {false} ;
         T004118_A146Modulo_Codigo = new int[1] ;
         T004118_n146Modulo_Codigo = new bool[] {false} ;
         T004118_A454ContagemResultado_ContadorFSCod = new int[1] ;
         T004118_n454ContagemResultado_ContadorFSCod = new bool[] {false} ;
         T004118_A890ContagemResultado_Responsavel = new int[1] ;
         T004118_n890ContagemResultado_Responsavel = new bool[] {false} ;
         T004118_A468ContagemResultado_NaoCnfDmnCod = new int[1] ;
         T004118_n468ContagemResultado_NaoCnfDmnCod = new bool[] {false} ;
         T004118_A490ContagemResultado_ContratadaCod = new int[1] ;
         T004118_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         T004118_A805ContagemResultado_ContratadaOrigemCod = new int[1] ;
         T004118_n805ContagemResultado_ContratadaOrigemCod = new bool[] {false} ;
         T004118_A489ContagemResultado_SistemaCod = new int[1] ;
         T004118_n489ContagemResultado_SistemaCod = new bool[] {false} ;
         T004118_A597ContagemResultado_LoteAceiteCod = new int[1] ;
         T004118_n597ContagemResultado_LoteAceiteCod = new bool[] {false} ;
         T004118_A1636ContagemResultado_ServicoSS = new int[1] ;
         T004118_n1636ContagemResultado_ServicoSS = new bool[] {false} ;
         T004118_A1043ContagemResultado_LiqLogCod = new int[1] ;
         T004118_n1043ContagemResultado_LiqLogCod = new bool[] {false} ;
         T004118_A1553ContagemResultado_CntSrvCod = new int[1] ;
         T004118_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         T004118_A602ContagemResultado_OSVinculada = new int[1] ;
         T004118_n602ContagemResultado_OSVinculada = new bool[] {false} ;
         T004112_A1636ContagemResultado_ServicoSS = new int[1] ;
         T004112_n1636ContagemResultado_ServicoSS = new bool[] {false} ;
         T004114_A1553ContagemResultado_CntSrvCod = new int[1] ;
         T004114_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         T004115_A602ContagemResultado_OSVinculada = new int[1] ;
         T004115_n602ContagemResultado_OSVinculada = new bool[] {false} ;
         T00417_A468ContagemResultado_NaoCnfDmnCod = new int[1] ;
         T00417_n468ContagemResultado_NaoCnfDmnCod = new bool[] {false} ;
         T004111_A597ContagemResultado_LoteAceiteCod = new int[1] ;
         T004111_n597ContagemResultado_LoteAceiteCod = new bool[] {false} ;
         T00419_A805ContagemResultado_ContratadaOrigemCod = new int[1] ;
         T00419_n805ContagemResultado_ContratadaOrigemCod = new bool[] {false} ;
         T00415_A454ContagemResultado_ContadorFSCod = new int[1] ;
         T00415_n454ContagemResultado_ContadorFSCod = new bool[] {false} ;
         T00416_A890ContagemResultado_Responsavel = new int[1] ;
         T00416_n890ContagemResultado_Responsavel = new bool[] {false} ;
         T004113_A1043ContagemResultado_LiqLogCod = new int[1] ;
         T004113_n1043ContagemResultado_LiqLogCod = new bool[] {false} ;
         T00414_A146Modulo_Codigo = new int[1] ;
         T00414_n146Modulo_Codigo = new bool[] {false} ;
         T00418_A490ContagemResultado_ContratadaCod = new int[1] ;
         T00418_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         T004110_A489ContagemResultado_SistemaCod = new int[1] ;
         T004110_n489ContagemResultado_SistemaCod = new bool[] {false} ;
         T004119_A1636ContagemResultado_ServicoSS = new int[1] ;
         T004119_n1636ContagemResultado_ServicoSS = new bool[] {false} ;
         T004120_A1553ContagemResultado_CntSrvCod = new int[1] ;
         T004120_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         T004121_A602ContagemResultado_OSVinculada = new int[1] ;
         T004121_n602ContagemResultado_OSVinculada = new bool[] {false} ;
         T004122_A468ContagemResultado_NaoCnfDmnCod = new int[1] ;
         T004122_n468ContagemResultado_NaoCnfDmnCod = new bool[] {false} ;
         T004123_A597ContagemResultado_LoteAceiteCod = new int[1] ;
         T004123_n597ContagemResultado_LoteAceiteCod = new bool[] {false} ;
         T004124_A805ContagemResultado_ContratadaOrigemCod = new int[1] ;
         T004124_n805ContagemResultado_ContratadaOrigemCod = new bool[] {false} ;
         T004125_A454ContagemResultado_ContadorFSCod = new int[1] ;
         T004125_n454ContagemResultado_ContadorFSCod = new bool[] {false} ;
         T004126_A890ContagemResultado_Responsavel = new int[1] ;
         T004126_n890ContagemResultado_Responsavel = new bool[] {false} ;
         T004127_A1043ContagemResultado_LiqLogCod = new int[1] ;
         T004127_n1043ContagemResultado_LiqLogCod = new bool[] {false} ;
         T004128_A146Modulo_Codigo = new int[1] ;
         T004128_n146Modulo_Codigo = new bool[] {false} ;
         T004129_A490ContagemResultado_ContratadaCod = new int[1] ;
         T004129_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         T004130_A489ContagemResultado_SistemaCod = new int[1] ;
         T004130_n489ContagemResultado_SistemaCod = new bool[] {false} ;
         T004131_A456ContagemResultado_Codigo = new int[1] ;
         T00413_A456ContagemResultado_Codigo = new int[1] ;
         T00413_A508ContagemResultado_Owner = new int[1] ;
         T00413_A494ContagemResultado_Descricao = new String[] {""} ;
         T00413_n494ContagemResultado_Descricao = new bool[] {false} ;
         T00413_A514ContagemResultado_Observacao = new String[] {""} ;
         T00413_n514ContagemResultado_Observacao = new bool[] {false} ;
         T00413_A472ContagemResultado_DataEntrega = new DateTime[] {DateTime.MinValue} ;
         T00413_n472ContagemResultado_DataEntrega = new bool[] {false} ;
         T00413_A912ContagemResultado_HoraEntrega = new DateTime[] {DateTime.MinValue} ;
         T00413_n912ContagemResultado_HoraEntrega = new bool[] {false} ;
         T00413_A1351ContagemResultado_DataPrevista = new DateTime[] {DateTime.MinValue} ;
         T00413_n1351ContagemResultado_DataPrevista = new bool[] {false} ;
         T00413_A1452ContagemResultado_SS = new int[1] ;
         T00413_n1452ContagemResultado_SS = new bool[] {false} ;
         T00413_A493ContagemResultado_DemandaFM = new String[] {""} ;
         T00413_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         T00413_A1584ContagemResultado_UOOwner = new int[1] ;
         T00413_n1584ContagemResultado_UOOwner = new bool[] {false} ;
         T00413_A1515ContagemResultado_Evento = new short[1] ;
         T00413_n1515ContagemResultado_Evento = new bool[] {false} ;
         T00413_A1583ContagemResultado_TipoRegistro = new short[1] ;
         T00413_A1585ContagemResultado_Referencia = new String[] {""} ;
         T00413_n1585ContagemResultado_Referencia = new bool[] {false} ;
         T00413_A1586ContagemResultado_Restricoes = new String[] {""} ;
         T00413_n1586ContagemResultado_Restricoes = new bool[] {false} ;
         T00413_A1587ContagemResultado_PrioridadePrevista = new String[] {""} ;
         T00413_n1587ContagemResultado_PrioridadePrevista = new bool[] {false} ;
         T00413_A484ContagemResultado_StatusDmn = new String[] {""} ;
         T00413_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         T00413_A471ContagemResultado_DataDmn = new DateTime[] {DateTime.MinValue} ;
         T00413_A1044ContagemResultado_FncUsrCod = new int[1] ;
         T00413_n1044ContagemResultado_FncUsrCod = new bool[] {false} ;
         T00413_A1227ContagemResultado_PrazoInicialDias = new short[1] ;
         T00413_n1227ContagemResultado_PrazoInicialDias = new bool[] {false} ;
         T00413_A1350ContagemResultado_DataCadastro = new DateTime[] {DateTime.MinValue} ;
         T00413_n1350ContagemResultado_DataCadastro = new bool[] {false} ;
         T00413_A1173ContagemResultado_OSManual = new bool[] {false} ;
         T00413_n1173ContagemResultado_OSManual = new bool[] {false} ;
         T00413_A598ContagemResultado_Baseline = new bool[] {false} ;
         T00413_n598ContagemResultado_Baseline = new bool[] {false} ;
         T00413_A485ContagemResultado_EhValidacao = new bool[] {false} ;
         T00413_n485ContagemResultado_EhValidacao = new bool[] {false} ;
         T00413_A146Modulo_Codigo = new int[1] ;
         T00413_n146Modulo_Codigo = new bool[] {false} ;
         T00413_A454ContagemResultado_ContadorFSCod = new int[1] ;
         T00413_n454ContagemResultado_ContadorFSCod = new bool[] {false} ;
         T00413_A890ContagemResultado_Responsavel = new int[1] ;
         T00413_n890ContagemResultado_Responsavel = new bool[] {false} ;
         T00413_A468ContagemResultado_NaoCnfDmnCod = new int[1] ;
         T00413_n468ContagemResultado_NaoCnfDmnCod = new bool[] {false} ;
         T00413_A490ContagemResultado_ContratadaCod = new int[1] ;
         T00413_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         T00413_A805ContagemResultado_ContratadaOrigemCod = new int[1] ;
         T00413_n805ContagemResultado_ContratadaOrigemCod = new bool[] {false} ;
         T00413_A489ContagemResultado_SistemaCod = new int[1] ;
         T00413_n489ContagemResultado_SistemaCod = new bool[] {false} ;
         T00413_A597ContagemResultado_LoteAceiteCod = new int[1] ;
         T00413_n597ContagemResultado_LoteAceiteCod = new bool[] {false} ;
         T00413_A1636ContagemResultado_ServicoSS = new int[1] ;
         T00413_n1636ContagemResultado_ServicoSS = new bool[] {false} ;
         T00413_A1043ContagemResultado_LiqLogCod = new int[1] ;
         T00413_n1043ContagemResultado_LiqLogCod = new bool[] {false} ;
         T00413_A1553ContagemResultado_CntSrvCod = new int[1] ;
         T00413_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         T00413_A602ContagemResultado_OSVinculada = new int[1] ;
         T00413_n602ContagemResultado_OSVinculada = new bool[] {false} ;
         sMode69 = "";
         T004132_A456ContagemResultado_Codigo = new int[1] ;
         T004133_A456ContagemResultado_Codigo = new int[1] ;
         T00412_A456ContagemResultado_Codigo = new int[1] ;
         T00412_A508ContagemResultado_Owner = new int[1] ;
         T00412_A494ContagemResultado_Descricao = new String[] {""} ;
         T00412_n494ContagemResultado_Descricao = new bool[] {false} ;
         T00412_A514ContagemResultado_Observacao = new String[] {""} ;
         T00412_n514ContagemResultado_Observacao = new bool[] {false} ;
         T00412_A472ContagemResultado_DataEntrega = new DateTime[] {DateTime.MinValue} ;
         T00412_n472ContagemResultado_DataEntrega = new bool[] {false} ;
         T00412_A912ContagemResultado_HoraEntrega = new DateTime[] {DateTime.MinValue} ;
         T00412_n912ContagemResultado_HoraEntrega = new bool[] {false} ;
         T00412_A1351ContagemResultado_DataPrevista = new DateTime[] {DateTime.MinValue} ;
         T00412_n1351ContagemResultado_DataPrevista = new bool[] {false} ;
         T00412_A1452ContagemResultado_SS = new int[1] ;
         T00412_n1452ContagemResultado_SS = new bool[] {false} ;
         T00412_A493ContagemResultado_DemandaFM = new String[] {""} ;
         T00412_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         T00412_A1584ContagemResultado_UOOwner = new int[1] ;
         T00412_n1584ContagemResultado_UOOwner = new bool[] {false} ;
         T00412_A1515ContagemResultado_Evento = new short[1] ;
         T00412_n1515ContagemResultado_Evento = new bool[] {false} ;
         T00412_A1583ContagemResultado_TipoRegistro = new short[1] ;
         T00412_A1585ContagemResultado_Referencia = new String[] {""} ;
         T00412_n1585ContagemResultado_Referencia = new bool[] {false} ;
         T00412_A1586ContagemResultado_Restricoes = new String[] {""} ;
         T00412_n1586ContagemResultado_Restricoes = new bool[] {false} ;
         T00412_A1587ContagemResultado_PrioridadePrevista = new String[] {""} ;
         T00412_n1587ContagemResultado_PrioridadePrevista = new bool[] {false} ;
         T00412_A484ContagemResultado_StatusDmn = new String[] {""} ;
         T00412_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         T00412_A471ContagemResultado_DataDmn = new DateTime[] {DateTime.MinValue} ;
         T00412_A1044ContagemResultado_FncUsrCod = new int[1] ;
         T00412_n1044ContagemResultado_FncUsrCod = new bool[] {false} ;
         T00412_A1227ContagemResultado_PrazoInicialDias = new short[1] ;
         T00412_n1227ContagemResultado_PrazoInicialDias = new bool[] {false} ;
         T00412_A1350ContagemResultado_DataCadastro = new DateTime[] {DateTime.MinValue} ;
         T00412_n1350ContagemResultado_DataCadastro = new bool[] {false} ;
         T00412_A1173ContagemResultado_OSManual = new bool[] {false} ;
         T00412_n1173ContagemResultado_OSManual = new bool[] {false} ;
         T00412_A598ContagemResultado_Baseline = new bool[] {false} ;
         T00412_n598ContagemResultado_Baseline = new bool[] {false} ;
         T00412_A485ContagemResultado_EhValidacao = new bool[] {false} ;
         T00412_n485ContagemResultado_EhValidacao = new bool[] {false} ;
         T00412_A146Modulo_Codigo = new int[1] ;
         T00412_n146Modulo_Codigo = new bool[] {false} ;
         T00412_A454ContagemResultado_ContadorFSCod = new int[1] ;
         T00412_n454ContagemResultado_ContadorFSCod = new bool[] {false} ;
         T00412_A890ContagemResultado_Responsavel = new int[1] ;
         T00412_n890ContagemResultado_Responsavel = new bool[] {false} ;
         T00412_A468ContagemResultado_NaoCnfDmnCod = new int[1] ;
         T00412_n468ContagemResultado_NaoCnfDmnCod = new bool[] {false} ;
         T00412_A490ContagemResultado_ContratadaCod = new int[1] ;
         T00412_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         T00412_A805ContagemResultado_ContratadaOrigemCod = new int[1] ;
         T00412_n805ContagemResultado_ContratadaOrigemCod = new bool[] {false} ;
         T00412_A489ContagemResultado_SistemaCod = new int[1] ;
         T00412_n489ContagemResultado_SistemaCod = new bool[] {false} ;
         T00412_A597ContagemResultado_LoteAceiteCod = new int[1] ;
         T00412_n597ContagemResultado_LoteAceiteCod = new bool[] {false} ;
         T00412_A1636ContagemResultado_ServicoSS = new int[1] ;
         T00412_n1636ContagemResultado_ServicoSS = new bool[] {false} ;
         T00412_A1043ContagemResultado_LiqLogCod = new int[1] ;
         T00412_n1043ContagemResultado_LiqLogCod = new bool[] {false} ;
         T00412_A1553ContagemResultado_CntSrvCod = new int[1] ;
         T00412_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         T00412_A602ContagemResultado_OSVinculada = new int[1] ;
         T00412_n602ContagemResultado_OSVinculada = new bool[] {false} ;
         T004134_A456ContagemResultado_Codigo = new int[1] ;
         T004137_A602ContagemResultado_OSVinculada = new int[1] ;
         T004137_n602ContagemResultado_OSVinculada = new bool[] {false} ;
         T004138_A2024ContagemResultadoNaoCnf_Codigo = new int[1] ;
         T004139_A2005ContagemResultadoRequisito_Codigo = new int[1] ;
         T004140_A1984ContagemResultadoQA_Codigo = new int[1] ;
         T004141_A761ContagemResultadoChckLst_Codigo = new short[1] ;
         T004142_A820ContagemResultadoChckLstLog_Codigo = new int[1] ;
         T004143_A1769ContagemResultadoArtefato_Codigo = new int[1] ;
         T004144_A1685Proposta_Codigo = new int[1] ;
         T004145_A1405ContagemResultadoExecucao_Codigo = new int[1] ;
         T004146_A1033ContagemResultadoLiqLog_Codigo = new int[1] ;
         T004146_A1370ContagemResultadoLiqLogOS_Codigo = new int[1] ;
         T004147_A1331ContagemResultadoNota_Codigo = new int[1] ;
         T004148_A1314ContagemResultadoIndicadores_DemandaCod = new int[1] ;
         T004148_A1315ContagemResultadoIndicadores_IndicadorCod = new int[1] ;
         T004149_A1241Incidentes_Codigo = new int[1] ;
         T004150_A1183AgendaAtendimento_CntSrcCod = new int[1] ;
         T004150_A1184AgendaAtendimento_Data = new DateTime[] {DateTime.MinValue} ;
         T004150_A1209AgendaAtendimento_CodDmn = new int[1] ;
         T004151_A1797LogResponsavel_Codigo = new long[1] ;
         T004152_A1895SolicServicoReqNeg_Codigo = new long[1] ;
         T004153_A586ContagemResultadoEvidencia_Codigo = new int[1] ;
         T004154_A1412ContagemResultadoNotificacao_Codigo = new long[1] ;
         T004154_A456ContagemResultado_Codigo = new int[1] ;
         T004155_A456ContagemResultado_Codigo = new int[1] ;
         T004155_A579ContagemResultadoErro_Tipo = new String[] {""} ;
         T004156_A456ContagemResultado_Codigo = new int[1] ;
         T004156_A473ContagemResultado_DataCnt = new DateTime[] {DateTime.MinValue} ;
         T004156_A511ContagemResultado_HoraCnt = new String[] {""} ;
         T004157_A456ContagemResultado_Codigo = new int[1] ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         i1350ContagemResultado_DataCadastro = (DateTime)(DateTime.MinValue);
         i484ContagemResultado_StatusDmn = "";
         i471ContagemResultado_DataDmn = DateTime.MinValue;
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         T004158_A596Lote_Codigo = new int[1] ;
         T004158_A563Lote_Nome = new String[] {""} ;
         T004159_A155Servico_Codigo = new int[1] ;
         T004159_A608Servico_Nome = new String[] {""} ;
         T004160_A426NaoConformidade_Codigo = new int[1] ;
         T004160_A427NaoConformidade_Nome = new String[] {""} ;
         T004160_A429NaoConformidade_Tipo = new short[1] ;
         T004160_A428NaoConformidade_AreaTrabalhoCod = new int[1] ;
         T004161_A40Contratada_PessoaCod = new int[1] ;
         T004161_A39Contratada_Codigo = new int[1] ;
         T004161_A41Contratada_PessoaNom = new String[] {""} ;
         T004161_n41Contratada_PessoaNom = new bool[] {false} ;
         T004161_A52Contratada_AreaTrabalhoCod = new int[1] ;
         T004162_A480ContagemResultado_CrFSPessoaCod = new int[1] ;
         T004162_n480ContagemResultado_CrFSPessoaCod = new bool[] {false} ;
         T004162_A454ContagemResultado_ContadorFSCod = new int[1] ;
         T004162_n454ContagemResultado_ContadorFSCod = new bool[] {false} ;
         T004162_A455ContagemResultado_ContadorFSNom = new String[] {""} ;
         T004162_n455ContagemResultado_ContadorFSNom = new bool[] {false} ;
         T004163_A146Modulo_Codigo = new int[1] ;
         T004163_n146Modulo_Codigo = new bool[] {false} ;
         T004163_A143Modulo_Nome = new String[] {""} ;
         T004163_A127Sistema_Codigo = new int[1] ;
         T004164_A40Contratada_PessoaCod = new int[1] ;
         T004164_A39Contratada_Codigo = new int[1] ;
         T004164_A41Contratada_PessoaNom = new String[] {""} ;
         T004164_n41Contratada_PessoaNom = new bool[] {false} ;
         T004164_A52Contratada_AreaTrabalhoCod = new int[1] ;
         T004165_A155Servico_Codigo = new int[1] ;
         T004165_A608Servico_Nome = new String[] {""} ;
         T004166_A596Lote_Codigo = new int[1] ;
         T004166_A563Lote_Nome = new String[] {""} ;
         isValidOutput = new GxUnknownObjectCollection();
         T004167_A1636ContagemResultado_ServicoSS = new int[1] ;
         T004167_n1636ContagemResultado_ServicoSS = new bool[] {false} ;
         T004168_A1553ContagemResultado_CntSrvCod = new int[1] ;
         T004168_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         T004169_A602ContagemResultado_OSVinculada = new int[1] ;
         T004169_n602ContagemResultado_OSVinculada = new bool[] {false} ;
         T004170_A468ContagemResultado_NaoCnfDmnCod = new int[1] ;
         T004170_n468ContagemResultado_NaoCnfDmnCod = new bool[] {false} ;
         T004171_A597ContagemResultado_LoteAceiteCod = new int[1] ;
         T004171_n597ContagemResultado_LoteAceiteCod = new bool[] {false} ;
         T004172_A805ContagemResultado_ContratadaOrigemCod = new int[1] ;
         T004172_n805ContagemResultado_ContratadaOrigemCod = new bool[] {false} ;
         T004173_A454ContagemResultado_ContadorFSCod = new int[1] ;
         T004173_n454ContagemResultado_ContadorFSCod = new bool[] {false} ;
         T004174_A890ContagemResultado_Responsavel = new int[1] ;
         T004174_n890ContagemResultado_Responsavel = new bool[] {false} ;
         T004175_A1043ContagemResultado_LiqLogCod = new int[1] ;
         T004175_n1043ContagemResultado_LiqLogCod = new bool[] {false} ;
         T004176_A146Modulo_Codigo = new int[1] ;
         T004176_n146Modulo_Codigo = new bool[] {false} ;
         T004177_A490ContagemResultado_ContratadaCod = new int[1] ;
         T004177_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         T004178_A489ContagemResultado_SistemaCod = new int[1] ;
         T004178_n489ContagemResultado_SistemaCod = new bool[] {false} ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.solicitacaoservico__default(),
            new Object[][] {
                new Object[] {
               T00412_A456ContagemResultado_Codigo, T00412_A508ContagemResultado_Owner, T00412_A494ContagemResultado_Descricao, T00412_n494ContagemResultado_Descricao, T00412_A514ContagemResultado_Observacao, T00412_n514ContagemResultado_Observacao, T00412_A472ContagemResultado_DataEntrega, T00412_n472ContagemResultado_DataEntrega, T00412_A912ContagemResultado_HoraEntrega, T00412_n912ContagemResultado_HoraEntrega,
               T00412_A1351ContagemResultado_DataPrevista, T00412_n1351ContagemResultado_DataPrevista, T00412_A1452ContagemResultado_SS, T00412_n1452ContagemResultado_SS, T00412_A493ContagemResultado_DemandaFM, T00412_n493ContagemResultado_DemandaFM, T00412_A1584ContagemResultado_UOOwner, T00412_n1584ContagemResultado_UOOwner, T00412_A1515ContagemResultado_Evento, T00412_n1515ContagemResultado_Evento,
               T00412_A1583ContagemResultado_TipoRegistro, T00412_A1585ContagemResultado_Referencia, T00412_n1585ContagemResultado_Referencia, T00412_A1586ContagemResultado_Restricoes, T00412_n1586ContagemResultado_Restricoes, T00412_A1587ContagemResultado_PrioridadePrevista, T00412_n1587ContagemResultado_PrioridadePrevista, T00412_A484ContagemResultado_StatusDmn, T00412_n484ContagemResultado_StatusDmn, T00412_A471ContagemResultado_DataDmn,
               T00412_A1044ContagemResultado_FncUsrCod, T00412_n1044ContagemResultado_FncUsrCod, T00412_A1227ContagemResultado_PrazoInicialDias, T00412_n1227ContagemResultado_PrazoInicialDias, T00412_A1350ContagemResultado_DataCadastro, T00412_n1350ContagemResultado_DataCadastro, T00412_A1173ContagemResultado_OSManual, T00412_n1173ContagemResultado_OSManual, T00412_A598ContagemResultado_Baseline, T00412_n598ContagemResultado_Baseline,
               T00412_A485ContagemResultado_EhValidacao, T00412_n485ContagemResultado_EhValidacao, T00412_A146Modulo_Codigo, T00412_n146Modulo_Codigo, T00412_A454ContagemResultado_ContadorFSCod, T00412_n454ContagemResultado_ContadorFSCod, T00412_A890ContagemResultado_Responsavel, T00412_n890ContagemResultado_Responsavel, T00412_A468ContagemResultado_NaoCnfDmnCod, T00412_n468ContagemResultado_NaoCnfDmnCod,
               T00412_A490ContagemResultado_ContratadaCod, T00412_n490ContagemResultado_ContratadaCod, T00412_A805ContagemResultado_ContratadaOrigemCod, T00412_n805ContagemResultado_ContratadaOrigemCod, T00412_A489ContagemResultado_SistemaCod, T00412_n489ContagemResultado_SistemaCod, T00412_A597ContagemResultado_LoteAceiteCod, T00412_n597ContagemResultado_LoteAceiteCod, T00412_A1636ContagemResultado_ServicoSS, T00412_n1636ContagemResultado_ServicoSS,
               T00412_A1043ContagemResultado_LiqLogCod, T00412_n1043ContagemResultado_LiqLogCod, T00412_A1553ContagemResultado_CntSrvCod, T00412_n1553ContagemResultado_CntSrvCod, T00412_A602ContagemResultado_OSVinculada, T00412_n602ContagemResultado_OSVinculada
               }
               , new Object[] {
               T00413_A456ContagemResultado_Codigo, T00413_A508ContagemResultado_Owner, T00413_A494ContagemResultado_Descricao, T00413_n494ContagemResultado_Descricao, T00413_A514ContagemResultado_Observacao, T00413_n514ContagemResultado_Observacao, T00413_A472ContagemResultado_DataEntrega, T00413_n472ContagemResultado_DataEntrega, T00413_A912ContagemResultado_HoraEntrega, T00413_n912ContagemResultado_HoraEntrega,
               T00413_A1351ContagemResultado_DataPrevista, T00413_n1351ContagemResultado_DataPrevista, T00413_A1452ContagemResultado_SS, T00413_n1452ContagemResultado_SS, T00413_A493ContagemResultado_DemandaFM, T00413_n493ContagemResultado_DemandaFM, T00413_A1584ContagemResultado_UOOwner, T00413_n1584ContagemResultado_UOOwner, T00413_A1515ContagemResultado_Evento, T00413_n1515ContagemResultado_Evento,
               T00413_A1583ContagemResultado_TipoRegistro, T00413_A1585ContagemResultado_Referencia, T00413_n1585ContagemResultado_Referencia, T00413_A1586ContagemResultado_Restricoes, T00413_n1586ContagemResultado_Restricoes, T00413_A1587ContagemResultado_PrioridadePrevista, T00413_n1587ContagemResultado_PrioridadePrevista, T00413_A484ContagemResultado_StatusDmn, T00413_n484ContagemResultado_StatusDmn, T00413_A471ContagemResultado_DataDmn,
               T00413_A1044ContagemResultado_FncUsrCod, T00413_n1044ContagemResultado_FncUsrCod, T00413_A1227ContagemResultado_PrazoInicialDias, T00413_n1227ContagemResultado_PrazoInicialDias, T00413_A1350ContagemResultado_DataCadastro, T00413_n1350ContagemResultado_DataCadastro, T00413_A1173ContagemResultado_OSManual, T00413_n1173ContagemResultado_OSManual, T00413_A598ContagemResultado_Baseline, T00413_n598ContagemResultado_Baseline,
               T00413_A485ContagemResultado_EhValidacao, T00413_n485ContagemResultado_EhValidacao, T00413_A146Modulo_Codigo, T00413_n146Modulo_Codigo, T00413_A454ContagemResultado_ContadorFSCod, T00413_n454ContagemResultado_ContadorFSCod, T00413_A890ContagemResultado_Responsavel, T00413_n890ContagemResultado_Responsavel, T00413_A468ContagemResultado_NaoCnfDmnCod, T00413_n468ContagemResultado_NaoCnfDmnCod,
               T00413_A490ContagemResultado_ContratadaCod, T00413_n490ContagemResultado_ContratadaCod, T00413_A805ContagemResultado_ContratadaOrigemCod, T00413_n805ContagemResultado_ContratadaOrigemCod, T00413_A489ContagemResultado_SistemaCod, T00413_n489ContagemResultado_SistemaCod, T00413_A597ContagemResultado_LoteAceiteCod, T00413_n597ContagemResultado_LoteAceiteCod, T00413_A1636ContagemResultado_ServicoSS, T00413_n1636ContagemResultado_ServicoSS,
               T00413_A1043ContagemResultado_LiqLogCod, T00413_n1043ContagemResultado_LiqLogCod, T00413_A1553ContagemResultado_CntSrvCod, T00413_n1553ContagemResultado_CntSrvCod, T00413_A602ContagemResultado_OSVinculada, T00413_n602ContagemResultado_OSVinculada
               }
               , new Object[] {
               T00414_A146Modulo_Codigo
               }
               , new Object[] {
               T00415_A454ContagemResultado_ContadorFSCod
               }
               , new Object[] {
               T00416_A890ContagemResultado_Responsavel
               }
               , new Object[] {
               T00417_A468ContagemResultado_NaoCnfDmnCod
               }
               , new Object[] {
               T00418_A490ContagemResultado_ContratadaCod
               }
               , new Object[] {
               T00419_A805ContagemResultado_ContratadaOrigemCod
               }
               , new Object[] {
               T004110_A489ContagemResultado_SistemaCod
               }
               , new Object[] {
               T004111_A597ContagemResultado_LoteAceiteCod
               }
               , new Object[] {
               T004112_A1636ContagemResultado_ServicoSS
               }
               , new Object[] {
               T004113_A1043ContagemResultado_LiqLogCod
               }
               , new Object[] {
               T004114_A1553ContagemResultado_CntSrvCod
               }
               , new Object[] {
               T004115_A602ContagemResultado_OSVinculada
               }
               , new Object[] {
               T004116_A155Servico_Codigo, T004116_A608Servico_Nome
               }
               , new Object[] {
               T004117_A596Lote_Codigo, T004117_A563Lote_Nome
               }
               , new Object[] {
               T004118_A456ContagemResultado_Codigo, T004118_A508ContagemResultado_Owner, T004118_A494ContagemResultado_Descricao, T004118_n494ContagemResultado_Descricao, T004118_A514ContagemResultado_Observacao, T004118_n514ContagemResultado_Observacao, T004118_A472ContagemResultado_DataEntrega, T004118_n472ContagemResultado_DataEntrega, T004118_A912ContagemResultado_HoraEntrega, T004118_n912ContagemResultado_HoraEntrega,
               T004118_A1351ContagemResultado_DataPrevista, T004118_n1351ContagemResultado_DataPrevista, T004118_A1452ContagemResultado_SS, T004118_n1452ContagemResultado_SS, T004118_A493ContagemResultado_DemandaFM, T004118_n493ContagemResultado_DemandaFM, T004118_A1584ContagemResultado_UOOwner, T004118_n1584ContagemResultado_UOOwner, T004118_A1515ContagemResultado_Evento, T004118_n1515ContagemResultado_Evento,
               T004118_A1583ContagemResultado_TipoRegistro, T004118_A1585ContagemResultado_Referencia, T004118_n1585ContagemResultado_Referencia, T004118_A1586ContagemResultado_Restricoes, T004118_n1586ContagemResultado_Restricoes, T004118_A1587ContagemResultado_PrioridadePrevista, T004118_n1587ContagemResultado_PrioridadePrevista, T004118_A484ContagemResultado_StatusDmn, T004118_n484ContagemResultado_StatusDmn, T004118_A471ContagemResultado_DataDmn,
               T004118_A1044ContagemResultado_FncUsrCod, T004118_n1044ContagemResultado_FncUsrCod, T004118_A1227ContagemResultado_PrazoInicialDias, T004118_n1227ContagemResultado_PrazoInicialDias, T004118_A1350ContagemResultado_DataCadastro, T004118_n1350ContagemResultado_DataCadastro, T004118_A1173ContagemResultado_OSManual, T004118_n1173ContagemResultado_OSManual, T004118_A598ContagemResultado_Baseline, T004118_n598ContagemResultado_Baseline,
               T004118_A485ContagemResultado_EhValidacao, T004118_n485ContagemResultado_EhValidacao, T004118_A146Modulo_Codigo, T004118_n146Modulo_Codigo, T004118_A454ContagemResultado_ContadorFSCod, T004118_n454ContagemResultado_ContadorFSCod, T004118_A890ContagemResultado_Responsavel, T004118_n890ContagemResultado_Responsavel, T004118_A468ContagemResultado_NaoCnfDmnCod, T004118_n468ContagemResultado_NaoCnfDmnCod,
               T004118_A490ContagemResultado_ContratadaCod, T004118_n490ContagemResultado_ContratadaCod, T004118_A805ContagemResultado_ContratadaOrigemCod, T004118_n805ContagemResultado_ContratadaOrigemCod, T004118_A489ContagemResultado_SistemaCod, T004118_n489ContagemResultado_SistemaCod, T004118_A597ContagemResultado_LoteAceiteCod, T004118_n597ContagemResultado_LoteAceiteCod, T004118_A1636ContagemResultado_ServicoSS, T004118_n1636ContagemResultado_ServicoSS,
               T004118_A1043ContagemResultado_LiqLogCod, T004118_n1043ContagemResultado_LiqLogCod, T004118_A1553ContagemResultado_CntSrvCod, T004118_n1553ContagemResultado_CntSrvCod, T004118_A602ContagemResultado_OSVinculada, T004118_n602ContagemResultado_OSVinculada
               }
               , new Object[] {
               T004119_A1636ContagemResultado_ServicoSS
               }
               , new Object[] {
               T004120_A1553ContagemResultado_CntSrvCod
               }
               , new Object[] {
               T004121_A602ContagemResultado_OSVinculada
               }
               , new Object[] {
               T004122_A468ContagemResultado_NaoCnfDmnCod
               }
               , new Object[] {
               T004123_A597ContagemResultado_LoteAceiteCod
               }
               , new Object[] {
               T004124_A805ContagemResultado_ContratadaOrigemCod
               }
               , new Object[] {
               T004125_A454ContagemResultado_ContadorFSCod
               }
               , new Object[] {
               T004126_A890ContagemResultado_Responsavel
               }
               , new Object[] {
               T004127_A1043ContagemResultado_LiqLogCod
               }
               , new Object[] {
               T004128_A146Modulo_Codigo
               }
               , new Object[] {
               T004129_A490ContagemResultado_ContratadaCod
               }
               , new Object[] {
               T004130_A489ContagemResultado_SistemaCod
               }
               , new Object[] {
               T004131_A456ContagemResultado_Codigo
               }
               , new Object[] {
               T004132_A456ContagemResultado_Codigo
               }
               , new Object[] {
               T004133_A456ContagemResultado_Codigo
               }
               , new Object[] {
               T004134_A456ContagemResultado_Codigo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T004137_A602ContagemResultado_OSVinculada
               }
               , new Object[] {
               T004138_A2024ContagemResultadoNaoCnf_Codigo
               }
               , new Object[] {
               T004139_A2005ContagemResultadoRequisito_Codigo
               }
               , new Object[] {
               T004140_A1984ContagemResultadoQA_Codigo
               }
               , new Object[] {
               T004141_A761ContagemResultadoChckLst_Codigo
               }
               , new Object[] {
               T004142_A820ContagemResultadoChckLstLog_Codigo
               }
               , new Object[] {
               T004143_A1769ContagemResultadoArtefato_Codigo
               }
               , new Object[] {
               T004144_A1685Proposta_Codigo
               }
               , new Object[] {
               T004145_A1405ContagemResultadoExecucao_Codigo
               }
               , new Object[] {
               T004146_A1033ContagemResultadoLiqLog_Codigo, T004146_A1370ContagemResultadoLiqLogOS_Codigo
               }
               , new Object[] {
               T004147_A1331ContagemResultadoNota_Codigo
               }
               , new Object[] {
               T004148_A1314ContagemResultadoIndicadores_DemandaCod, T004148_A1315ContagemResultadoIndicadores_IndicadorCod
               }
               , new Object[] {
               T004149_A1241Incidentes_Codigo
               }
               , new Object[] {
               T004150_A1183AgendaAtendimento_CntSrcCod, T004150_A1184AgendaAtendimento_Data, T004150_A1209AgendaAtendimento_CodDmn
               }
               , new Object[] {
               T004151_A1797LogResponsavel_Codigo
               }
               , new Object[] {
               T004152_A1895SolicServicoReqNeg_Codigo
               }
               , new Object[] {
               T004153_A586ContagemResultadoEvidencia_Codigo
               }
               , new Object[] {
               T004154_A1412ContagemResultadoNotificacao_Codigo, T004154_A456ContagemResultado_Codigo
               }
               , new Object[] {
               T004155_A456ContagemResultado_Codigo, T004155_A579ContagemResultadoErro_Tipo
               }
               , new Object[] {
               T004156_A456ContagemResultado_Codigo, T004156_A473ContagemResultado_DataCnt, T004156_A511ContagemResultado_HoraCnt
               }
               , new Object[] {
               T004157_A456ContagemResultado_Codigo
               }
               , new Object[] {
               T004158_A596Lote_Codigo, T004158_A563Lote_Nome
               }
               , new Object[] {
               T004159_A155Servico_Codigo, T004159_A608Servico_Nome
               }
               , new Object[] {
               T004160_A426NaoConformidade_Codigo, T004160_A427NaoConformidade_Nome, T004160_A429NaoConformidade_Tipo, T004160_A428NaoConformidade_AreaTrabalhoCod
               }
               , new Object[] {
               T004161_A40Contratada_PessoaCod, T004161_A39Contratada_Codigo, T004161_A41Contratada_PessoaNom, T004161_n41Contratada_PessoaNom, T004161_A52Contratada_AreaTrabalhoCod
               }
               , new Object[] {
               T004162_A480ContagemResultado_CrFSPessoaCod, T004162_n480ContagemResultado_CrFSPessoaCod, T004162_A454ContagemResultado_ContadorFSCod, T004162_A455ContagemResultado_ContadorFSNom, T004162_n455ContagemResultado_ContadorFSNom
               }
               , new Object[] {
               T004163_A146Modulo_Codigo, T004163_A143Modulo_Nome, T004163_A127Sistema_Codigo
               }
               , new Object[] {
               T004164_A40Contratada_PessoaCod, T004164_A39Contratada_Codigo, T004164_A41Contratada_PessoaNom, T004164_n41Contratada_PessoaNom, T004164_A52Contratada_AreaTrabalhoCod
               }
               , new Object[] {
               T004165_A155Servico_Codigo, T004165_A608Servico_Nome
               }
               , new Object[] {
               T004166_A596Lote_Codigo, T004166_A563Lote_Nome
               }
               , new Object[] {
               T004167_A1636ContagemResultado_ServicoSS
               }
               , new Object[] {
               T004168_A1553ContagemResultado_CntSrvCod
               }
               , new Object[] {
               T004169_A602ContagemResultado_OSVinculada
               }
               , new Object[] {
               T004170_A468ContagemResultado_NaoCnfDmnCod
               }
               , new Object[] {
               T004171_A597ContagemResultado_LoteAceiteCod
               }
               , new Object[] {
               T004172_A805ContagemResultado_ContratadaOrigemCod
               }
               , new Object[] {
               T004173_A454ContagemResultado_ContadorFSCod
               }
               , new Object[] {
               T004174_A890ContagemResultado_Responsavel
               }
               , new Object[] {
               T004175_A1043ContagemResultado_LiqLogCod
               }
               , new Object[] {
               T004176_A146Modulo_Codigo
               }
               , new Object[] {
               T004177_A490ContagemResultado_ContratadaCod
               }
               , new Object[] {
               T004178_A489ContagemResultado_SistemaCod
               }
            }
         );
         Z598ContagemResultado_Baseline = false;
         n598ContagemResultado_Baseline = false;
         A598ContagemResultado_Baseline = false;
         n598ContagemResultado_Baseline = false;
         i598ContagemResultado_Baseline = false;
         n598ContagemResultado_Baseline = false;
         Z485ContagemResultado_EhValidacao = false;
         n485ContagemResultado_EhValidacao = false;
         A485ContagemResultado_EhValidacao = false;
         n485ContagemResultado_EhValidacao = false;
         i485ContagemResultado_EhValidacao = false;
         n485ContagemResultado_EhValidacao = false;
         Z1350ContagemResultado_DataCadastro = DateTimeUtil.ServerNow( context, "DEFAULT");
         n1350ContagemResultado_DataCadastro = false;
         A1350ContagemResultado_DataCadastro = DateTimeUtil.ServerNow( context, "DEFAULT");
         n1350ContagemResultado_DataCadastro = false;
         i1350ContagemResultado_DataCadastro = DateTimeUtil.ServerNow( context, "DEFAULT");
         n1350ContagemResultado_DataCadastro = false;
         Z1515ContagemResultado_Evento = 1;
         n1515ContagemResultado_Evento = false;
         A1515ContagemResultado_Evento = 1;
         n1515ContagemResultado_Evento = false;
         i1515ContagemResultado_Evento = 1;
         n1515ContagemResultado_Evento = false;
         Z1452ContagemResultado_SS = 0;
         n1452ContagemResultado_SS = false;
         A1452ContagemResultado_SS = 0;
         n1452ContagemResultado_SS = false;
         i1452ContagemResultado_SS = 0;
         n1452ContagemResultado_SS = false;
         AV26Pgmname = "SolicitacaoServico";
         Z471ContagemResultado_DataDmn = DateTime.MinValue;
         A471ContagemResultado_DataDmn = DateTime.MinValue;
         i471ContagemResultado_DataDmn = DateTime.MinValue;
         Gx_date = DateTimeUtil.Today( context);
         Z508ContagemResultado_Owner = AV7WWPContext.gxTpr_Userid;
         A508ContagemResultado_Owner = AV7WWPContext.gxTpr_Userid;
         i508ContagemResultado_Owner = AV7WWPContext.gxTpr_Userid;
         Z484ContagemResultado_StatusDmn = "S";
         n484ContagemResultado_StatusDmn = false;
         A484ContagemResultado_StatusDmn = "S";
         n484ContagemResultado_StatusDmn = false;
         i484ContagemResultado_StatusDmn = "S";
         n484ContagemResultado_StatusDmn = false;
         Z1583ContagemResultado_TipoRegistro = 2;
         A1583ContagemResultado_TipoRegistro = 2;
         i1583ContagemResultado_TipoRegistro = 2;
      }

      private short Z1515ContagemResultado_Evento ;
      private short Z1583ContagemResultado_TipoRegistro ;
      private short Z1227ContagemResultado_PrazoInicialDias ;
      private short GxWebError ;
      private short gxcookieaux ;
      private short A1515ContagemResultado_Evento ;
      private short A1583ContagemResultado_TipoRegistro ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short A1227ContagemResultado_PrazoInicialDias ;
      private short Gx_BScreen ;
      private short GX_JID ;
      private short RcdFound69 ;
      private short gxajaxcallmode ;
      private short i1515ContagemResultado_Evento ;
      private short i1583ContagemResultado_TipoRegistro ;
      private short wbTemp ;
      private int Z456ContagemResultado_Codigo ;
      private int Z508ContagemResultado_Owner ;
      private int Z1452ContagemResultado_SS ;
      private int Z1584ContagemResultado_UOOwner ;
      private int Z1044ContagemResultado_FncUsrCod ;
      private int Z146Modulo_Codigo ;
      private int Z454ContagemResultado_ContadorFSCod ;
      private int Z890ContagemResultado_Responsavel ;
      private int Z468ContagemResultado_NaoCnfDmnCod ;
      private int Z490ContagemResultado_ContratadaCod ;
      private int Z805ContagemResultado_ContratadaOrigemCod ;
      private int Z489ContagemResultado_SistemaCod ;
      private int Z597ContagemResultado_LoteAceiteCod ;
      private int Z1636ContagemResultado_ServicoSS ;
      private int Z1043ContagemResultado_LiqLogCod ;
      private int Z1553ContagemResultado_CntSrvCod ;
      private int Z602ContagemResultado_OSVinculada ;
      private int A489ContagemResultado_SistemaCod ;
      private int A1636ContagemResultado_ServicoSS ;
      private int A1553ContagemResultado_CntSrvCod ;
      private int A602ContagemResultado_OSVinculada ;
      private int A468ContagemResultado_NaoCnfDmnCod ;
      private int A597ContagemResultado_LoteAceiteCod ;
      private int A805ContagemResultado_ContratadaOrigemCod ;
      private int A454ContagemResultado_ContadorFSCod ;
      private int A890ContagemResultado_Responsavel ;
      private int A1043ContagemResultado_LiqLogCod ;
      private int A146Modulo_Codigo ;
      private int A490ContagemResultado_ContratadaCod ;
      private int trnEnded ;
      private int bttBtn_trn_enter_Visible ;
      private int bttBtn_trn_enter_Enabled ;
      private int bttBtn_trn_cancel_Visible ;
      private int bttBtn_trn_delete_Visible ;
      private int bttBtn_trn_delete_Enabled ;
      private int A456ContagemResultado_Codigo ;
      private int edtContagemResultado_Codigo_Enabled ;
      private int A508ContagemResultado_Owner ;
      private int edtContagemResultado_Owner_Enabled ;
      private int edtContagemResultado_Descricao_Enabled ;
      private int edtContagemResultado_DataEntrega_Enabled ;
      private int edtContagemResultado_HoraEntrega_Enabled ;
      private int A1452ContagemResultado_SS ;
      private int edtContagemResultado_SS_Enabled ;
      private int edtContagemResultado_DemandaFM_Enabled ;
      private int A1584ContagemResultado_UOOwner ;
      private int edtContagemResultado_UOOwner_Enabled ;
      private int edtContagemResultado_Referencia_Enabled ;
      private int edtContagemResultado_Restricoes_Enabled ;
      private int edtContagemResultado_PrioridadePrevista_Enabled ;
      private int edtContagemResultado_DataDmn_Enabled ;
      private int edtContagemResultado_CntSrvCod_Enabled ;
      private int edtContagemResultado_OSVinculada_Enabled ;
      private int edtContagemResultado_Responsavel_Enabled ;
      private int edtContagemResultado_LiqLogCod_Enabled ;
      private int edtContagemResultado_SistemaCod_Enabled ;
      private int A1044ContagemResultado_FncUsrCod ;
      private int edtContagemResultado_FncUsrCod_Enabled ;
      private int edtContagemResultado_PrazoInicialDias_Enabled ;
      private int edtContagemResultado_DataCadastro_Enabled ;
      private int Contagemresultado_observacao_Color ;
      private int Contagemresultado_observacao_Coltitlecolor ;
      private int AV27GXV1 ;
      private int AV23Insert_ContagemResultado_ServicoSS ;
      private int AV11Insert_ContagemResultado_CntSrvCod ;
      private int AV12Insert_ContagemResultado_OSVinculada ;
      private int AV13Insert_ContagemResultado_NaoCnfDmnCod ;
      private int AV14Insert_ContagemResultado_LoteAceiteCod ;
      private int AV15Insert_ContagemResultado_ContratadaOrigemCod ;
      private int AV16Insert_ContagemResultado_ContadorFSCod ;
      private int AV17Insert_ContagemResultado_Responsavel ;
      private int AV18Insert_ContagemResultado_LiqLogCod ;
      private int AV19Insert_Modulo_Codigo ;
      private int AV20Insert_ContagemResultado_ContratadaCod ;
      private int AV21Insert_ContagemResultado_SistemaCod ;
      private int i1452ContagemResultado_SS ;
      private int i508ContagemResultado_Owner ;
      private int idxLst ;
      private int gxdynajaxindex ;
      private String sPrefix ;
      private String Z484ContagemResultado_StatusDmn ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String GXKey ;
      private String A484ContagemResultado_StatusDmn ;
      private String chkContagemResultado_OSManual_Internalname ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String edtContagemResultado_Codigo_Internalname ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String lblSolicitacaoservicotitle_Internalname ;
      private String lblSolicitacaoservicotitle_Jsonclick ;
      private String tblTableactions_Internalname ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String bttBtn_trn_enter_Internalname ;
      private String bttBtn_trn_enter_Jsonclick ;
      private String bttBtn_trn_cancel_Internalname ;
      private String bttBtn_trn_cancel_Jsonclick ;
      private String bttBtn_trn_delete_Internalname ;
      private String bttBtn_trn_delete_Jsonclick ;
      private String tblTablecontent_Internalname ;
      private String tblTableattributes_Internalname ;
      private String lblTextblockcontagemresultado_codigo_Internalname ;
      private String lblTextblockcontagemresultado_codigo_Jsonclick ;
      private String edtContagemResultado_Codigo_Jsonclick ;
      private String lblTextblockcontagemresultado_owner_Internalname ;
      private String lblTextblockcontagemresultado_owner_Jsonclick ;
      private String edtContagemResultado_Owner_Internalname ;
      private String edtContagemResultado_Owner_Jsonclick ;
      private String lblTextblockcontagemresultado_descricao_Internalname ;
      private String lblTextblockcontagemresultado_descricao_Jsonclick ;
      private String edtContagemResultado_Descricao_Internalname ;
      private String edtContagemResultado_Descricao_Jsonclick ;
      private String lblTextblockcontagemresultado_observacao_Internalname ;
      private String lblTextblockcontagemresultado_observacao_Jsonclick ;
      private String lblTextblockcontagemresultado_dataentrega_Internalname ;
      private String lblTextblockcontagemresultado_dataentrega_Jsonclick ;
      private String edtContagemResultado_DataEntrega_Internalname ;
      private String edtContagemResultado_DataEntrega_Jsonclick ;
      private String lblTextblockcontagemresultado_horaentrega_Internalname ;
      private String lblTextblockcontagemresultado_horaentrega_Jsonclick ;
      private String edtContagemResultado_HoraEntrega_Internalname ;
      private String edtContagemResultado_HoraEntrega_Jsonclick ;
      private String lblTextblockcontagemresultado_servicoss_Internalname ;
      private String lblTextblockcontagemresultado_servicoss_Jsonclick ;
      private String dynContagemResultado_ServicoSS_Internalname ;
      private String dynContagemResultado_ServicoSS_Jsonclick ;
      private String lblTextblockcontagemresultado_ss_Internalname ;
      private String lblTextblockcontagemresultado_ss_Jsonclick ;
      private String edtContagemResultado_SS_Internalname ;
      private String edtContagemResultado_SS_Jsonclick ;
      private String lblTextblockcontagemresultado_demandafm_Internalname ;
      private String lblTextblockcontagemresultado_demandafm_Jsonclick ;
      private String edtContagemResultado_DemandaFM_Internalname ;
      private String edtContagemResultado_DemandaFM_Jsonclick ;
      private String lblTextblockcontagemresultado_uoowner_Internalname ;
      private String lblTextblockcontagemresultado_uoowner_Jsonclick ;
      private String edtContagemResultado_UOOwner_Internalname ;
      private String edtContagemResultado_UOOwner_Jsonclick ;
      private String lblTextblockcontagemresultado_evento_Internalname ;
      private String lblTextblockcontagemresultado_evento_Jsonclick ;
      private String cmbContagemResultado_Evento_Internalname ;
      private String cmbContagemResultado_Evento_Jsonclick ;
      private String lblTextblockcontagemresultado_tiporegistro_Internalname ;
      private String lblTextblockcontagemresultado_tiporegistro_Jsonclick ;
      private String cmbContagemResultado_TipoRegistro_Internalname ;
      private String cmbContagemResultado_TipoRegistro_Jsonclick ;
      private String lblTextblockcontagemresultado_referencia_Internalname ;
      private String lblTextblockcontagemresultado_referencia_Jsonclick ;
      private String edtContagemResultado_Referencia_Internalname ;
      private String lblTextblockcontagemresultado_restricoes_Internalname ;
      private String lblTextblockcontagemresultado_restricoes_Jsonclick ;
      private String edtContagemResultado_Restricoes_Internalname ;
      private String lblTextblockcontagemresultado_prioridadeprevista_Internalname ;
      private String lblTextblockcontagemresultado_prioridadeprevista_Jsonclick ;
      private String edtContagemResultado_PrioridadePrevista_Internalname ;
      private String edtContagemResultado_PrioridadePrevista_Jsonclick ;
      private String lblTextblockcontagemresultado_statusdmn_Internalname ;
      private String lblTextblockcontagemresultado_statusdmn_Jsonclick ;
      private String cmbContagemResultado_StatusDmn_Internalname ;
      private String cmbContagemResultado_StatusDmn_Jsonclick ;
      private String lblTextblockcontagemresultado_datadmn_Internalname ;
      private String lblTextblockcontagemresultado_datadmn_Jsonclick ;
      private String edtContagemResultado_DataDmn_Internalname ;
      private String edtContagemResultado_DataDmn_Jsonclick ;
      private String lblTextblockcontagemresultado_cntsrvcod_Internalname ;
      private String lblTextblockcontagemresultado_cntsrvcod_Jsonclick ;
      private String edtContagemResultado_CntSrvCod_Internalname ;
      private String edtContagemResultado_CntSrvCod_Jsonclick ;
      private String lblTextblockcontagemresultado_osvinculada_Internalname ;
      private String lblTextblockcontagemresultado_osvinculada_Jsonclick ;
      private String edtContagemResultado_OSVinculada_Internalname ;
      private String edtContagemResultado_OSVinculada_Jsonclick ;
      private String lblTextblockcontagemresultado_naocnfdmncod_Internalname ;
      private String lblTextblockcontagemresultado_naocnfdmncod_Jsonclick ;
      private String dynContagemResultado_NaoCnfDmnCod_Internalname ;
      private String dynContagemResultado_NaoCnfDmnCod_Jsonclick ;
      private String lblTextblockcontagemresultado_loteaceitecod_Internalname ;
      private String lblTextblockcontagemresultado_loteaceitecod_Jsonclick ;
      private String dynContagemResultado_LoteAceiteCod_Internalname ;
      private String dynContagemResultado_LoteAceiteCod_Jsonclick ;
      private String lblTextblockcontagemresultado_contratadaorigemcod_Internalname ;
      private String lblTextblockcontagemresultado_contratadaorigemcod_Jsonclick ;
      private String dynContagemResultado_ContratadaOrigemCod_Internalname ;
      private String dynContagemResultado_ContratadaOrigemCod_Jsonclick ;
      private String lblTextblockcontagemresultado_contadorfscod_Internalname ;
      private String lblTextblockcontagemresultado_contadorfscod_Jsonclick ;
      private String dynContagemResultado_ContadorFSCod_Internalname ;
      private String dynContagemResultado_ContadorFSCod_Jsonclick ;
      private String lblTextblockcontagemresultado_responsavel_Internalname ;
      private String lblTextblockcontagemresultado_responsavel_Jsonclick ;
      private String edtContagemResultado_Responsavel_Internalname ;
      private String edtContagemResultado_Responsavel_Jsonclick ;
      private String lblTextblockcontagemresultado_liqlogcod_Internalname ;
      private String lblTextblockcontagemresultado_liqlogcod_Jsonclick ;
      private String edtContagemResultado_LiqLogCod_Internalname ;
      private String edtContagemResultado_LiqLogCod_Jsonclick ;
      private String lblTextblockmodulo_codigo_Internalname ;
      private String lblTextblockmodulo_codigo_Jsonclick ;
      private String dynModulo_Codigo_Internalname ;
      private String dynModulo_Codigo_Jsonclick ;
      private String lblTextblockcontagemresultado_contratadacod_Internalname ;
      private String lblTextblockcontagemresultado_contratadacod_Jsonclick ;
      private String dynContagemResultado_ContratadaCod_Internalname ;
      private String dynContagemResultado_ContratadaCod_Jsonclick ;
      private String lblTextblockcontagemresultado_sistemacod_Internalname ;
      private String lblTextblockcontagemresultado_sistemacod_Jsonclick ;
      private String edtContagemResultado_SistemaCod_Internalname ;
      private String edtContagemResultado_SistemaCod_Jsonclick ;
      private String lblTextblockcontagemresultado_fncusrcod_Internalname ;
      private String lblTextblockcontagemresultado_fncusrcod_Jsonclick ;
      private String edtContagemResultado_FncUsrCod_Internalname ;
      private String edtContagemResultado_FncUsrCod_Jsonclick ;
      private String lblTextblockcontagemresultado_prazoinicialdias_Internalname ;
      private String lblTextblockcontagemresultado_prazoinicialdias_Jsonclick ;
      private String edtContagemResultado_PrazoInicialDias_Internalname ;
      private String edtContagemResultado_PrazoInicialDias_Jsonclick ;
      private String lblTextblockcontagemresultado_datacadastro_Internalname ;
      private String lblTextblockcontagemresultado_datacadastro_Jsonclick ;
      private String edtContagemResultado_DataCadastro_Internalname ;
      private String edtContagemResultado_DataCadastro_Jsonclick ;
      private String lblTextblockcontagemresultado_osmanual_Internalname ;
      private String lblTextblockcontagemresultado_osmanual_Jsonclick ;
      private String Gx_mode ;
      private String AV26Pgmname ;
      private String Contagemresultado_observacao_Width ;
      private String Contagemresultado_observacao_Height ;
      private String Contagemresultado_observacao_Skin ;
      private String Contagemresultado_observacao_Toolbar ;
      private String Contagemresultado_observacao_Class ;
      private String Contagemresultado_observacao_Customtoolbar ;
      private String Contagemresultado_observacao_Customconfiguration ;
      private String Contagemresultado_observacao_Buttonpressedid ;
      private String Contagemresultado_observacao_Captionvalue ;
      private String Contagemresultado_observacao_Captionclass ;
      private String Contagemresultado_observacao_Captionposition ;
      private String Contagemresultado_observacao_Coltitle ;
      private String Contagemresultado_observacao_Coltitlefont ;
      private String Dvpanel_tableattributes_Width ;
      private String Dvpanel_tableattributes_Height ;
      private String Dvpanel_tableattributes_Cls ;
      private String Dvpanel_tableattributes_Title ;
      private String Dvpanel_tableattributes_Class ;
      private String Dvpanel_tableattributes_Iconposition ;
      private String forbiddenHiddens ;
      private String hsh ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String sMode69 ;
      private String Contagemresultado_observacao_Internalname ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String i484ContagemResultado_StatusDmn ;
      private String Dvpanel_tableattributes_Internalname ;
      private String gxwrpcisep ;
      private DateTime Z912ContagemResultado_HoraEntrega ;
      private DateTime Z1351ContagemResultado_DataPrevista ;
      private DateTime Z1350ContagemResultado_DataCadastro ;
      private DateTime A912ContagemResultado_HoraEntrega ;
      private DateTime A1350ContagemResultado_DataCadastro ;
      private DateTime A1351ContagemResultado_DataPrevista ;
      private DateTime i1350ContagemResultado_DataCadastro ;
      private DateTime Z472ContagemResultado_DataEntrega ;
      private DateTime Z471ContagemResultado_DataDmn ;
      private DateTime A472ContagemResultado_DataEntrega ;
      private DateTime A471ContagemResultado_DataDmn ;
      private DateTime Gx_date ;
      private DateTime i471ContagemResultado_DataDmn ;
      private bool Z1173ContagemResultado_OSManual ;
      private bool Z598ContagemResultado_Baseline ;
      private bool Z485ContagemResultado_EhValidacao ;
      private bool entryPointCalled ;
      private bool n489ContagemResultado_SistemaCod ;
      private bool n1636ContagemResultado_ServicoSS ;
      private bool n1553ContagemResultado_CntSrvCod ;
      private bool n602ContagemResultado_OSVinculada ;
      private bool n468ContagemResultado_NaoCnfDmnCod ;
      private bool n597ContagemResultado_LoteAceiteCod ;
      private bool n805ContagemResultado_ContratadaOrigemCod ;
      private bool n454ContagemResultado_ContadorFSCod ;
      private bool n890ContagemResultado_Responsavel ;
      private bool n1043ContagemResultado_LiqLogCod ;
      private bool n146Modulo_Codigo ;
      private bool n490ContagemResultado_ContratadaCod ;
      private bool toggleJsOutput ;
      private bool n1515ContagemResultado_Evento ;
      private bool n484ContagemResultado_StatusDmn ;
      private bool wbErr ;
      private bool A1173ContagemResultado_OSManual ;
      private bool n494ContagemResultado_Descricao ;
      private bool n472ContagemResultado_DataEntrega ;
      private bool n912ContagemResultado_HoraEntrega ;
      private bool n1452ContagemResultado_SS ;
      private bool n493ContagemResultado_DemandaFM ;
      private bool n1584ContagemResultado_UOOwner ;
      private bool n1585ContagemResultado_Referencia ;
      private bool n1586ContagemResultado_Restricoes ;
      private bool n1587ContagemResultado_PrioridadePrevista ;
      private bool n1044ContagemResultado_FncUsrCod ;
      private bool n1227ContagemResultado_PrazoInicialDias ;
      private bool n1350ContagemResultado_DataCadastro ;
      private bool n1173ContagemResultado_OSManual ;
      private bool n1351ContagemResultado_DataPrevista ;
      private bool n598ContagemResultado_Baseline ;
      private bool A598ContagemResultado_Baseline ;
      private bool n485ContagemResultado_EhValidacao ;
      private bool A485ContagemResultado_EhValidacao ;
      private bool n514ContagemResultado_Observacao ;
      private bool Contagemresultado_observacao_Enabled ;
      private bool Contagemresultado_observacao_Toolbarcancollapse ;
      private bool Contagemresultado_observacao_Toolbarexpanded ;
      private bool Contagemresultado_observacao_Usercontroliscolumn ;
      private bool Contagemresultado_observacao_Visible ;
      private bool Dvpanel_tableattributes_Collapsible ;
      private bool Dvpanel_tableattributes_Collapsed ;
      private bool Dvpanel_tableattributes_Enabled ;
      private bool Dvpanel_tableattributes_Autowidth ;
      private bool Dvpanel_tableattributes_Autoheight ;
      private bool Dvpanel_tableattributes_Showheader ;
      private bool Dvpanel_tableattributes_Showcollapseicon ;
      private bool Dvpanel_tableattributes_Autoscroll ;
      private bool Dvpanel_tableattributes_Visible ;
      private bool returnInSub ;
      private bool Gx_longc ;
      private bool i598ContagemResultado_Baseline ;
      private bool i485ContagemResultado_EhValidacao ;
      private String A514ContagemResultado_Observacao ;
      private String Z514ContagemResultado_Observacao ;
      private String Z494ContagemResultado_Descricao ;
      private String Z493ContagemResultado_DemandaFM ;
      private String Z1585ContagemResultado_Referencia ;
      private String Z1586ContagemResultado_Restricoes ;
      private String Z1587ContagemResultado_PrioridadePrevista ;
      private String A494ContagemResultado_Descricao ;
      private String A493ContagemResultado_DemandaFM ;
      private String A1585ContagemResultado_Referencia ;
      private String A1586ContagemResultado_Restricoes ;
      private String A1587ContagemResultado_PrioridadePrevista ;
      private IGxSession AV9WebSession ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private GxUnknownObjectCollection isValidOutput ;
      private IDataStoreProvider pr_default ;
      private int[] T004116_A155Servico_Codigo ;
      private String[] T004116_A608Servico_Nome ;
      private int[] T004117_A596Lote_Codigo ;
      private String[] T004117_A563Lote_Nome ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox dynContagemResultado_ServicoSS ;
      private GXCombobox cmbContagemResultado_Evento ;
      private GXCombobox cmbContagemResultado_TipoRegistro ;
      private GXCombobox cmbContagemResultado_StatusDmn ;
      private GXCombobox dynContagemResultado_NaoCnfDmnCod ;
      private GXCombobox dynContagemResultado_LoteAceiteCod ;
      private GXCombobox dynContagemResultado_ContratadaOrigemCod ;
      private GXCombobox dynContagemResultado_ContadorFSCod ;
      private GXCombobox dynModulo_Codigo ;
      private GXCombobox dynContagemResultado_ContratadaCod ;
      private GXCheckbox chkContagemResultado_OSManual ;
      private int[] T004118_A456ContagemResultado_Codigo ;
      private int[] T004118_A508ContagemResultado_Owner ;
      private String[] T004118_A494ContagemResultado_Descricao ;
      private bool[] T004118_n494ContagemResultado_Descricao ;
      private String[] T004118_A514ContagemResultado_Observacao ;
      private bool[] T004118_n514ContagemResultado_Observacao ;
      private DateTime[] T004118_A472ContagemResultado_DataEntrega ;
      private bool[] T004118_n472ContagemResultado_DataEntrega ;
      private DateTime[] T004118_A912ContagemResultado_HoraEntrega ;
      private bool[] T004118_n912ContagemResultado_HoraEntrega ;
      private DateTime[] T004118_A1351ContagemResultado_DataPrevista ;
      private bool[] T004118_n1351ContagemResultado_DataPrevista ;
      private int[] T004118_A1452ContagemResultado_SS ;
      private bool[] T004118_n1452ContagemResultado_SS ;
      private String[] T004118_A493ContagemResultado_DemandaFM ;
      private bool[] T004118_n493ContagemResultado_DemandaFM ;
      private int[] T004118_A1584ContagemResultado_UOOwner ;
      private bool[] T004118_n1584ContagemResultado_UOOwner ;
      private short[] T004118_A1515ContagemResultado_Evento ;
      private bool[] T004118_n1515ContagemResultado_Evento ;
      private short[] T004118_A1583ContagemResultado_TipoRegistro ;
      private String[] T004118_A1585ContagemResultado_Referencia ;
      private bool[] T004118_n1585ContagemResultado_Referencia ;
      private String[] T004118_A1586ContagemResultado_Restricoes ;
      private bool[] T004118_n1586ContagemResultado_Restricoes ;
      private String[] T004118_A1587ContagemResultado_PrioridadePrevista ;
      private bool[] T004118_n1587ContagemResultado_PrioridadePrevista ;
      private String[] T004118_A484ContagemResultado_StatusDmn ;
      private bool[] T004118_n484ContagemResultado_StatusDmn ;
      private DateTime[] T004118_A471ContagemResultado_DataDmn ;
      private int[] T004118_A1044ContagemResultado_FncUsrCod ;
      private bool[] T004118_n1044ContagemResultado_FncUsrCod ;
      private short[] T004118_A1227ContagemResultado_PrazoInicialDias ;
      private bool[] T004118_n1227ContagemResultado_PrazoInicialDias ;
      private DateTime[] T004118_A1350ContagemResultado_DataCadastro ;
      private bool[] T004118_n1350ContagemResultado_DataCadastro ;
      private bool[] T004118_A1173ContagemResultado_OSManual ;
      private bool[] T004118_n1173ContagemResultado_OSManual ;
      private bool[] T004118_A598ContagemResultado_Baseline ;
      private bool[] T004118_n598ContagemResultado_Baseline ;
      private bool[] T004118_A485ContagemResultado_EhValidacao ;
      private bool[] T004118_n485ContagemResultado_EhValidacao ;
      private int[] T004118_A146Modulo_Codigo ;
      private bool[] T004118_n146Modulo_Codigo ;
      private int[] T004118_A454ContagemResultado_ContadorFSCod ;
      private bool[] T004118_n454ContagemResultado_ContadorFSCod ;
      private int[] T004118_A890ContagemResultado_Responsavel ;
      private bool[] T004118_n890ContagemResultado_Responsavel ;
      private int[] T004118_A468ContagemResultado_NaoCnfDmnCod ;
      private bool[] T004118_n468ContagemResultado_NaoCnfDmnCod ;
      private int[] T004118_A490ContagemResultado_ContratadaCod ;
      private bool[] T004118_n490ContagemResultado_ContratadaCod ;
      private int[] T004118_A805ContagemResultado_ContratadaOrigemCod ;
      private bool[] T004118_n805ContagemResultado_ContratadaOrigemCod ;
      private int[] T004118_A489ContagemResultado_SistemaCod ;
      private bool[] T004118_n489ContagemResultado_SistemaCod ;
      private int[] T004118_A597ContagemResultado_LoteAceiteCod ;
      private bool[] T004118_n597ContagemResultado_LoteAceiteCod ;
      private int[] T004118_A1636ContagemResultado_ServicoSS ;
      private bool[] T004118_n1636ContagemResultado_ServicoSS ;
      private int[] T004118_A1043ContagemResultado_LiqLogCod ;
      private bool[] T004118_n1043ContagemResultado_LiqLogCod ;
      private int[] T004118_A1553ContagemResultado_CntSrvCod ;
      private bool[] T004118_n1553ContagemResultado_CntSrvCod ;
      private int[] T004118_A602ContagemResultado_OSVinculada ;
      private bool[] T004118_n602ContagemResultado_OSVinculada ;
      private int[] T004112_A1636ContagemResultado_ServicoSS ;
      private bool[] T004112_n1636ContagemResultado_ServicoSS ;
      private int[] T004114_A1553ContagemResultado_CntSrvCod ;
      private bool[] T004114_n1553ContagemResultado_CntSrvCod ;
      private int[] T004115_A602ContagemResultado_OSVinculada ;
      private bool[] T004115_n602ContagemResultado_OSVinculada ;
      private int[] T00417_A468ContagemResultado_NaoCnfDmnCod ;
      private bool[] T00417_n468ContagemResultado_NaoCnfDmnCod ;
      private int[] T004111_A597ContagemResultado_LoteAceiteCod ;
      private bool[] T004111_n597ContagemResultado_LoteAceiteCod ;
      private int[] T00419_A805ContagemResultado_ContratadaOrigemCod ;
      private bool[] T00419_n805ContagemResultado_ContratadaOrigemCod ;
      private int[] T00415_A454ContagemResultado_ContadorFSCod ;
      private bool[] T00415_n454ContagemResultado_ContadorFSCod ;
      private int[] T00416_A890ContagemResultado_Responsavel ;
      private bool[] T00416_n890ContagemResultado_Responsavel ;
      private int[] T004113_A1043ContagemResultado_LiqLogCod ;
      private bool[] T004113_n1043ContagemResultado_LiqLogCod ;
      private int[] T00414_A146Modulo_Codigo ;
      private bool[] T00414_n146Modulo_Codigo ;
      private int[] T00418_A490ContagemResultado_ContratadaCod ;
      private bool[] T00418_n490ContagemResultado_ContratadaCod ;
      private int[] T004110_A489ContagemResultado_SistemaCod ;
      private bool[] T004110_n489ContagemResultado_SistemaCod ;
      private int[] T004119_A1636ContagemResultado_ServicoSS ;
      private bool[] T004119_n1636ContagemResultado_ServicoSS ;
      private int[] T004120_A1553ContagemResultado_CntSrvCod ;
      private bool[] T004120_n1553ContagemResultado_CntSrvCod ;
      private int[] T004121_A602ContagemResultado_OSVinculada ;
      private bool[] T004121_n602ContagemResultado_OSVinculada ;
      private int[] T004122_A468ContagemResultado_NaoCnfDmnCod ;
      private bool[] T004122_n468ContagemResultado_NaoCnfDmnCod ;
      private int[] T004123_A597ContagemResultado_LoteAceiteCod ;
      private bool[] T004123_n597ContagemResultado_LoteAceiteCod ;
      private int[] T004124_A805ContagemResultado_ContratadaOrigemCod ;
      private bool[] T004124_n805ContagemResultado_ContratadaOrigemCod ;
      private int[] T004125_A454ContagemResultado_ContadorFSCod ;
      private bool[] T004125_n454ContagemResultado_ContadorFSCod ;
      private int[] T004126_A890ContagemResultado_Responsavel ;
      private bool[] T004126_n890ContagemResultado_Responsavel ;
      private int[] T004127_A1043ContagemResultado_LiqLogCod ;
      private bool[] T004127_n1043ContagemResultado_LiqLogCod ;
      private int[] T004128_A146Modulo_Codigo ;
      private bool[] T004128_n146Modulo_Codigo ;
      private int[] T004129_A490ContagemResultado_ContratadaCod ;
      private bool[] T004129_n490ContagemResultado_ContratadaCod ;
      private int[] T004130_A489ContagemResultado_SistemaCod ;
      private bool[] T004130_n489ContagemResultado_SistemaCod ;
      private int[] T004131_A456ContagemResultado_Codigo ;
      private int[] T00413_A456ContagemResultado_Codigo ;
      private int[] T00413_A508ContagemResultado_Owner ;
      private String[] T00413_A494ContagemResultado_Descricao ;
      private bool[] T00413_n494ContagemResultado_Descricao ;
      private String[] T00413_A514ContagemResultado_Observacao ;
      private bool[] T00413_n514ContagemResultado_Observacao ;
      private DateTime[] T00413_A472ContagemResultado_DataEntrega ;
      private bool[] T00413_n472ContagemResultado_DataEntrega ;
      private DateTime[] T00413_A912ContagemResultado_HoraEntrega ;
      private bool[] T00413_n912ContagemResultado_HoraEntrega ;
      private DateTime[] T00413_A1351ContagemResultado_DataPrevista ;
      private bool[] T00413_n1351ContagemResultado_DataPrevista ;
      private int[] T00413_A1452ContagemResultado_SS ;
      private bool[] T00413_n1452ContagemResultado_SS ;
      private String[] T00413_A493ContagemResultado_DemandaFM ;
      private bool[] T00413_n493ContagemResultado_DemandaFM ;
      private int[] T00413_A1584ContagemResultado_UOOwner ;
      private bool[] T00413_n1584ContagemResultado_UOOwner ;
      private short[] T00413_A1515ContagemResultado_Evento ;
      private bool[] T00413_n1515ContagemResultado_Evento ;
      private short[] T00413_A1583ContagemResultado_TipoRegistro ;
      private String[] T00413_A1585ContagemResultado_Referencia ;
      private bool[] T00413_n1585ContagemResultado_Referencia ;
      private String[] T00413_A1586ContagemResultado_Restricoes ;
      private bool[] T00413_n1586ContagemResultado_Restricoes ;
      private String[] T00413_A1587ContagemResultado_PrioridadePrevista ;
      private bool[] T00413_n1587ContagemResultado_PrioridadePrevista ;
      private String[] T00413_A484ContagemResultado_StatusDmn ;
      private bool[] T00413_n484ContagemResultado_StatusDmn ;
      private DateTime[] T00413_A471ContagemResultado_DataDmn ;
      private int[] T00413_A1044ContagemResultado_FncUsrCod ;
      private bool[] T00413_n1044ContagemResultado_FncUsrCod ;
      private short[] T00413_A1227ContagemResultado_PrazoInicialDias ;
      private bool[] T00413_n1227ContagemResultado_PrazoInicialDias ;
      private DateTime[] T00413_A1350ContagemResultado_DataCadastro ;
      private bool[] T00413_n1350ContagemResultado_DataCadastro ;
      private bool[] T00413_A1173ContagemResultado_OSManual ;
      private bool[] T00413_n1173ContagemResultado_OSManual ;
      private bool[] T00413_A598ContagemResultado_Baseline ;
      private bool[] T00413_n598ContagemResultado_Baseline ;
      private bool[] T00413_A485ContagemResultado_EhValidacao ;
      private bool[] T00413_n485ContagemResultado_EhValidacao ;
      private int[] T00413_A146Modulo_Codigo ;
      private bool[] T00413_n146Modulo_Codigo ;
      private int[] T00413_A454ContagemResultado_ContadorFSCod ;
      private bool[] T00413_n454ContagemResultado_ContadorFSCod ;
      private int[] T00413_A890ContagemResultado_Responsavel ;
      private bool[] T00413_n890ContagemResultado_Responsavel ;
      private int[] T00413_A468ContagemResultado_NaoCnfDmnCod ;
      private bool[] T00413_n468ContagemResultado_NaoCnfDmnCod ;
      private int[] T00413_A490ContagemResultado_ContratadaCod ;
      private bool[] T00413_n490ContagemResultado_ContratadaCod ;
      private int[] T00413_A805ContagemResultado_ContratadaOrigemCod ;
      private bool[] T00413_n805ContagemResultado_ContratadaOrigemCod ;
      private int[] T00413_A489ContagemResultado_SistemaCod ;
      private bool[] T00413_n489ContagemResultado_SistemaCod ;
      private int[] T00413_A597ContagemResultado_LoteAceiteCod ;
      private bool[] T00413_n597ContagemResultado_LoteAceiteCod ;
      private int[] T00413_A1636ContagemResultado_ServicoSS ;
      private bool[] T00413_n1636ContagemResultado_ServicoSS ;
      private int[] T00413_A1043ContagemResultado_LiqLogCod ;
      private bool[] T00413_n1043ContagemResultado_LiqLogCod ;
      private int[] T00413_A1553ContagemResultado_CntSrvCod ;
      private bool[] T00413_n1553ContagemResultado_CntSrvCod ;
      private int[] T00413_A602ContagemResultado_OSVinculada ;
      private bool[] T00413_n602ContagemResultado_OSVinculada ;
      private int[] T004132_A456ContagemResultado_Codigo ;
      private int[] T004133_A456ContagemResultado_Codigo ;
      private int[] T00412_A456ContagemResultado_Codigo ;
      private int[] T00412_A508ContagemResultado_Owner ;
      private String[] T00412_A494ContagemResultado_Descricao ;
      private bool[] T00412_n494ContagemResultado_Descricao ;
      private String[] T00412_A514ContagemResultado_Observacao ;
      private bool[] T00412_n514ContagemResultado_Observacao ;
      private DateTime[] T00412_A472ContagemResultado_DataEntrega ;
      private bool[] T00412_n472ContagemResultado_DataEntrega ;
      private DateTime[] T00412_A912ContagemResultado_HoraEntrega ;
      private bool[] T00412_n912ContagemResultado_HoraEntrega ;
      private DateTime[] T00412_A1351ContagemResultado_DataPrevista ;
      private bool[] T00412_n1351ContagemResultado_DataPrevista ;
      private int[] T00412_A1452ContagemResultado_SS ;
      private bool[] T00412_n1452ContagemResultado_SS ;
      private String[] T00412_A493ContagemResultado_DemandaFM ;
      private bool[] T00412_n493ContagemResultado_DemandaFM ;
      private int[] T00412_A1584ContagemResultado_UOOwner ;
      private bool[] T00412_n1584ContagemResultado_UOOwner ;
      private short[] T00412_A1515ContagemResultado_Evento ;
      private bool[] T00412_n1515ContagemResultado_Evento ;
      private short[] T00412_A1583ContagemResultado_TipoRegistro ;
      private String[] T00412_A1585ContagemResultado_Referencia ;
      private bool[] T00412_n1585ContagemResultado_Referencia ;
      private String[] T00412_A1586ContagemResultado_Restricoes ;
      private bool[] T00412_n1586ContagemResultado_Restricoes ;
      private String[] T00412_A1587ContagemResultado_PrioridadePrevista ;
      private bool[] T00412_n1587ContagemResultado_PrioridadePrevista ;
      private String[] T00412_A484ContagemResultado_StatusDmn ;
      private bool[] T00412_n484ContagemResultado_StatusDmn ;
      private DateTime[] T00412_A471ContagemResultado_DataDmn ;
      private int[] T00412_A1044ContagemResultado_FncUsrCod ;
      private bool[] T00412_n1044ContagemResultado_FncUsrCod ;
      private short[] T00412_A1227ContagemResultado_PrazoInicialDias ;
      private bool[] T00412_n1227ContagemResultado_PrazoInicialDias ;
      private DateTime[] T00412_A1350ContagemResultado_DataCadastro ;
      private bool[] T00412_n1350ContagemResultado_DataCadastro ;
      private bool[] T00412_A1173ContagemResultado_OSManual ;
      private bool[] T00412_n1173ContagemResultado_OSManual ;
      private bool[] T00412_A598ContagemResultado_Baseline ;
      private bool[] T00412_n598ContagemResultado_Baseline ;
      private bool[] T00412_A485ContagemResultado_EhValidacao ;
      private bool[] T00412_n485ContagemResultado_EhValidacao ;
      private int[] T00412_A146Modulo_Codigo ;
      private bool[] T00412_n146Modulo_Codigo ;
      private int[] T00412_A454ContagemResultado_ContadorFSCod ;
      private bool[] T00412_n454ContagemResultado_ContadorFSCod ;
      private int[] T00412_A890ContagemResultado_Responsavel ;
      private bool[] T00412_n890ContagemResultado_Responsavel ;
      private int[] T00412_A468ContagemResultado_NaoCnfDmnCod ;
      private bool[] T00412_n468ContagemResultado_NaoCnfDmnCod ;
      private int[] T00412_A490ContagemResultado_ContratadaCod ;
      private bool[] T00412_n490ContagemResultado_ContratadaCod ;
      private int[] T00412_A805ContagemResultado_ContratadaOrigemCod ;
      private bool[] T00412_n805ContagemResultado_ContratadaOrigemCod ;
      private int[] T00412_A489ContagemResultado_SistemaCod ;
      private bool[] T00412_n489ContagemResultado_SistemaCod ;
      private int[] T00412_A597ContagemResultado_LoteAceiteCod ;
      private bool[] T00412_n597ContagemResultado_LoteAceiteCod ;
      private int[] T00412_A1636ContagemResultado_ServicoSS ;
      private bool[] T00412_n1636ContagemResultado_ServicoSS ;
      private int[] T00412_A1043ContagemResultado_LiqLogCod ;
      private bool[] T00412_n1043ContagemResultado_LiqLogCod ;
      private int[] T00412_A1553ContagemResultado_CntSrvCod ;
      private bool[] T00412_n1553ContagemResultado_CntSrvCod ;
      private int[] T00412_A602ContagemResultado_OSVinculada ;
      private bool[] T00412_n602ContagemResultado_OSVinculada ;
      private int[] T004134_A456ContagemResultado_Codigo ;
      private int[] T004137_A602ContagemResultado_OSVinculada ;
      private bool[] T004137_n602ContagemResultado_OSVinculada ;
      private int[] T004138_A2024ContagemResultadoNaoCnf_Codigo ;
      private int[] T004139_A2005ContagemResultadoRequisito_Codigo ;
      private int[] T004140_A1984ContagemResultadoQA_Codigo ;
      private short[] T004141_A761ContagemResultadoChckLst_Codigo ;
      private int[] T004142_A820ContagemResultadoChckLstLog_Codigo ;
      private int[] T004143_A1769ContagemResultadoArtefato_Codigo ;
      private int[] T004144_A1685Proposta_Codigo ;
      private int[] T004145_A1405ContagemResultadoExecucao_Codigo ;
      private int[] T004146_A1033ContagemResultadoLiqLog_Codigo ;
      private int[] T004146_A1370ContagemResultadoLiqLogOS_Codigo ;
      private int[] T004147_A1331ContagemResultadoNota_Codigo ;
      private int[] T004148_A1314ContagemResultadoIndicadores_DemandaCod ;
      private int[] T004148_A1315ContagemResultadoIndicadores_IndicadorCod ;
      private int[] T004149_A1241Incidentes_Codigo ;
      private int[] T004150_A1183AgendaAtendimento_CntSrcCod ;
      private DateTime[] T004150_A1184AgendaAtendimento_Data ;
      private int[] T004150_A1209AgendaAtendimento_CodDmn ;
      private long[] T004151_A1797LogResponsavel_Codigo ;
      private long[] T004152_A1895SolicServicoReqNeg_Codigo ;
      private int[] T004153_A586ContagemResultadoEvidencia_Codigo ;
      private long[] T004154_A1412ContagemResultadoNotificacao_Codigo ;
      private int[] T004154_A456ContagemResultado_Codigo ;
      private int[] T004155_A456ContagemResultado_Codigo ;
      private String[] T004155_A579ContagemResultadoErro_Tipo ;
      private int[] T004156_A456ContagemResultado_Codigo ;
      private DateTime[] T004156_A473ContagemResultado_DataCnt ;
      private String[] T004156_A511ContagemResultado_HoraCnt ;
      private int[] T004157_A456ContagemResultado_Codigo ;
      private int[] T004158_A596Lote_Codigo ;
      private String[] T004158_A563Lote_Nome ;
      private int[] T004159_A155Servico_Codigo ;
      private String[] T004159_A608Servico_Nome ;
      private int[] T004160_A426NaoConformidade_Codigo ;
      private String[] T004160_A427NaoConformidade_Nome ;
      private short[] T004160_A429NaoConformidade_Tipo ;
      private int[] T004160_A428NaoConformidade_AreaTrabalhoCod ;
      private int[] T004161_A40Contratada_PessoaCod ;
      private int[] T004161_A39Contratada_Codigo ;
      private String[] T004161_A41Contratada_PessoaNom ;
      private bool[] T004161_n41Contratada_PessoaNom ;
      private int[] T004161_A52Contratada_AreaTrabalhoCod ;
      private int[] T004162_A480ContagemResultado_CrFSPessoaCod ;
      private bool[] T004162_n480ContagemResultado_CrFSPessoaCod ;
      private int[] T004162_A454ContagemResultado_ContadorFSCod ;
      private bool[] T004162_n454ContagemResultado_ContadorFSCod ;
      private String[] T004162_A455ContagemResultado_ContadorFSNom ;
      private bool[] T004162_n455ContagemResultado_ContadorFSNom ;
      private int[] T004163_A146Modulo_Codigo ;
      private bool[] T004163_n146Modulo_Codigo ;
      private String[] T004163_A143Modulo_Nome ;
      private int[] T004163_A127Sistema_Codigo ;
      private int[] T004164_A40Contratada_PessoaCod ;
      private int[] T004164_A39Contratada_Codigo ;
      private String[] T004164_A41Contratada_PessoaNom ;
      private bool[] T004164_n41Contratada_PessoaNom ;
      private int[] T004164_A52Contratada_AreaTrabalhoCod ;
      private int[] T004165_A155Servico_Codigo ;
      private String[] T004165_A608Servico_Nome ;
      private int[] T004166_A596Lote_Codigo ;
      private String[] T004166_A563Lote_Nome ;
      private int[] T004167_A1636ContagemResultado_ServicoSS ;
      private bool[] T004167_n1636ContagemResultado_ServicoSS ;
      private int[] T004168_A1553ContagemResultado_CntSrvCod ;
      private bool[] T004168_n1553ContagemResultado_CntSrvCod ;
      private int[] T004169_A602ContagemResultado_OSVinculada ;
      private bool[] T004169_n602ContagemResultado_OSVinculada ;
      private int[] T004170_A468ContagemResultado_NaoCnfDmnCod ;
      private bool[] T004170_n468ContagemResultado_NaoCnfDmnCod ;
      private int[] T004171_A597ContagemResultado_LoteAceiteCod ;
      private bool[] T004171_n597ContagemResultado_LoteAceiteCod ;
      private int[] T004172_A805ContagemResultado_ContratadaOrigemCod ;
      private bool[] T004172_n805ContagemResultado_ContratadaOrigemCod ;
      private int[] T004173_A454ContagemResultado_ContadorFSCod ;
      private bool[] T004173_n454ContagemResultado_ContadorFSCod ;
      private int[] T004174_A890ContagemResultado_Responsavel ;
      private bool[] T004174_n890ContagemResultado_Responsavel ;
      private int[] T004175_A1043ContagemResultado_LiqLogCod ;
      private bool[] T004175_n1043ContagemResultado_LiqLogCod ;
      private int[] T004176_A146Modulo_Codigo ;
      private bool[] T004176_n146Modulo_Codigo ;
      private int[] T004177_A490ContagemResultado_ContratadaCod ;
      private bool[] T004177_n490ContagemResultado_ContratadaCod ;
      private int[] T004178_A489ContagemResultado_SistemaCod ;
      private bool[] T004178_n489ContagemResultado_SistemaCod ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV7WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV8TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV22TrnContextAtt ;
   }

   public class solicitacaoservico__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new ForEachCursor(def[12])
         ,new ForEachCursor(def[13])
         ,new ForEachCursor(def[14])
         ,new ForEachCursor(def[15])
         ,new ForEachCursor(def[16])
         ,new ForEachCursor(def[17])
         ,new ForEachCursor(def[18])
         ,new ForEachCursor(def[19])
         ,new ForEachCursor(def[20])
         ,new ForEachCursor(def[21])
         ,new ForEachCursor(def[22])
         ,new ForEachCursor(def[23])
         ,new ForEachCursor(def[24])
         ,new ForEachCursor(def[25])
         ,new ForEachCursor(def[26])
         ,new ForEachCursor(def[27])
         ,new ForEachCursor(def[28])
         ,new ForEachCursor(def[29])
         ,new ForEachCursor(def[30])
         ,new ForEachCursor(def[31])
         ,new ForEachCursor(def[32])
         ,new UpdateCursor(def[33])
         ,new UpdateCursor(def[34])
         ,new ForEachCursor(def[35])
         ,new ForEachCursor(def[36])
         ,new ForEachCursor(def[37])
         ,new ForEachCursor(def[38])
         ,new ForEachCursor(def[39])
         ,new ForEachCursor(def[40])
         ,new ForEachCursor(def[41])
         ,new ForEachCursor(def[42])
         ,new ForEachCursor(def[43])
         ,new ForEachCursor(def[44])
         ,new ForEachCursor(def[45])
         ,new ForEachCursor(def[46])
         ,new ForEachCursor(def[47])
         ,new ForEachCursor(def[48])
         ,new ForEachCursor(def[49])
         ,new ForEachCursor(def[50])
         ,new ForEachCursor(def[51])
         ,new ForEachCursor(def[52])
         ,new ForEachCursor(def[53])
         ,new ForEachCursor(def[54])
         ,new ForEachCursor(def[55])
         ,new ForEachCursor(def[56])
         ,new ForEachCursor(def[57])
         ,new ForEachCursor(def[58])
         ,new ForEachCursor(def[59])
         ,new ForEachCursor(def[60])
         ,new ForEachCursor(def[61])
         ,new ForEachCursor(def[62])
         ,new ForEachCursor(def[63])
         ,new ForEachCursor(def[64])
         ,new ForEachCursor(def[65])
         ,new ForEachCursor(def[66])
         ,new ForEachCursor(def[67])
         ,new ForEachCursor(def[68])
         ,new ForEachCursor(def[69])
         ,new ForEachCursor(def[70])
         ,new ForEachCursor(def[71])
         ,new ForEachCursor(def[72])
         ,new ForEachCursor(def[73])
         ,new ForEachCursor(def[74])
         ,new ForEachCursor(def[75])
         ,new ForEachCursor(def[76])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT004116 ;
          prmT004116 = new Object[] {
          } ;
          Object[] prmT004117 ;
          prmT004117 = new Object[] {
          } ;
          Object[] prmT004118 ;
          prmT004118 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004112 ;
          prmT004112 = new Object[] {
          new Object[] {"@ContagemResultado_ServicoSS",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004114 ;
          prmT004114 = new Object[] {
          new Object[] {"@ContagemResultado_CntSrvCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004115 ;
          prmT004115 = new Object[] {
          new Object[] {"@ContagemResultado_OSVinculada",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00417 ;
          prmT00417 = new Object[] {
          new Object[] {"@ContagemResultado_NaoCnfDmnCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004111 ;
          prmT004111 = new Object[] {
          new Object[] {"@ContagemResultado_LoteAceiteCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00419 ;
          prmT00419 = new Object[] {
          new Object[] {"@ContagemResultado_ContratadaOrigemCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00415 ;
          prmT00415 = new Object[] {
          new Object[] {"@ContagemResultado_ContadorFSCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00416 ;
          prmT00416 = new Object[] {
          new Object[] {"@ContagemResultado_Responsavel",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004113 ;
          prmT004113 = new Object[] {
          new Object[] {"@ContagemResultado_LiqLogCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00414 ;
          prmT00414 = new Object[] {
          new Object[] {"@Modulo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00418 ;
          prmT00418 = new Object[] {
          new Object[] {"@ContagemResultado_ContratadaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004110 ;
          prmT004110 = new Object[] {
          new Object[] {"@ContagemResultado_SistemaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004119 ;
          prmT004119 = new Object[] {
          new Object[] {"@ContagemResultado_ServicoSS",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004120 ;
          prmT004120 = new Object[] {
          new Object[] {"@ContagemResultado_CntSrvCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004121 ;
          prmT004121 = new Object[] {
          new Object[] {"@ContagemResultado_OSVinculada",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004122 ;
          prmT004122 = new Object[] {
          new Object[] {"@ContagemResultado_NaoCnfDmnCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004123 ;
          prmT004123 = new Object[] {
          new Object[] {"@ContagemResultado_LoteAceiteCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004124 ;
          prmT004124 = new Object[] {
          new Object[] {"@ContagemResultado_ContratadaOrigemCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004125 ;
          prmT004125 = new Object[] {
          new Object[] {"@ContagemResultado_ContadorFSCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004126 ;
          prmT004126 = new Object[] {
          new Object[] {"@ContagemResultado_Responsavel",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004127 ;
          prmT004127 = new Object[] {
          new Object[] {"@ContagemResultado_LiqLogCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004128 ;
          prmT004128 = new Object[] {
          new Object[] {"@Modulo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004129 ;
          prmT004129 = new Object[] {
          new Object[] {"@ContagemResultado_ContratadaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004130 ;
          prmT004130 = new Object[] {
          new Object[] {"@ContagemResultado_SistemaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004131 ;
          prmT004131 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00413 ;
          prmT00413 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004132 ;
          prmT004132 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004133 ;
          prmT004133 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT00412 ;
          prmT00412 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004134 ;
          prmT004134 = new Object[] {
          new Object[] {"@ContagemResultado_Owner",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_Descricao",SqlDbType.VarChar,500,0} ,
          new Object[] {"@ContagemResultado_Observacao",SqlDbType.VarChar,20000,0} ,
          new Object[] {"@ContagemResultado_DataEntrega",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_HoraEntrega",SqlDbType.DateTime,0,5} ,
          new Object[] {"@ContagemResultado_DataPrevista",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultado_SS",SqlDbType.Int,8,0} ,
          new Object[] {"@ContagemResultado_DemandaFM",SqlDbType.VarChar,50,0} ,
          new Object[] {"@ContagemResultado_UOOwner",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_Evento",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@ContagemResultado_TipoRegistro",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContagemResultado_Referencia",SqlDbType.VarChar,500,0} ,
          new Object[] {"@ContagemResultado_Restricoes",SqlDbType.VarChar,250,0} ,
          new Object[] {"@ContagemResultado_PrioridadePrevista",SqlDbType.VarChar,250,0} ,
          new Object[] {"@ContagemResultado_StatusDmn",SqlDbType.Char,1,0} ,
          new Object[] {"@ContagemResultado_DataDmn",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_FncUsrCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_PrazoInicialDias",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContagemResultado_DataCadastro",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultado_OSManual",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContagemResultado_Baseline",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContagemResultado_EhValidacao",SqlDbType.Bit,4,0} ,
          new Object[] {"@Modulo_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_ContadorFSCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_Responsavel",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_NaoCnfDmnCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_ContratadaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_ContratadaOrigemCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_SistemaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_LoteAceiteCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_ServicoSS",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_LiqLogCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_CntSrvCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_OSVinculada",SqlDbType.Int,6,0}
          } ;
          String cmdBufferT004134 ;
          cmdBufferT004134=" INSERT INTO [ContagemResultado]([ContagemResultado_Owner], [ContagemResultado_Descricao], [ContagemResultado_Observacao], [ContagemResultado_DataEntrega], [ContagemResultado_HoraEntrega], [ContagemResultado_DataPrevista], [ContagemResultado_SS], [ContagemResultado_DemandaFM], [ContagemResultado_UOOwner], [ContagemResultado_Evento], [ContagemResultado_TipoRegistro], [ContagemResultado_Referencia], [ContagemResultado_Restricoes], [ContagemResultado_PrioridadePrevista], [ContagemResultado_StatusDmn], [ContagemResultado_DataDmn], [ContagemResultado_FncUsrCod], [ContagemResultado_PrazoInicialDias], [ContagemResultado_DataCadastro], [ContagemResultado_OSManual], [ContagemResultado_Baseline], [ContagemResultado_EhValidacao], [Modulo_Codigo], [ContagemResultado_ContadorFSCod], [ContagemResultado_Responsavel], [ContagemResultado_NaoCnfDmnCod], [ContagemResultado_ContratadaCod], [ContagemResultado_ContratadaOrigemCod], [ContagemResultado_SistemaCod], [ContagemResultado_LoteAceiteCod], [ContagemResultado_ServicoSS], [ContagemResultado_LiqLogCod], [ContagemResultado_CntSrvCod], [ContagemResultado_OSVinculada], [ContagemResultado_Demanda], [ContagemResultado_Link], [ContagemResultado_ValorPF], [ContagemResultado_Evidencia], [ContagemResultado_PFBFSImp], [ContagemResultado_PFLFSImp], [ContagemResultado_Agrupador], [ContagemResultado_GlsData], [ContagemResultado_GlsDescricao], [ContagemResultado_GlsValor], [ContagemResultado_GlsUser], [ContagemResultado_PBFinal], [ContagemResultado_PLFinal], [ContagemResultado_Custo], [ContagemResultado_PrazoMaisDias], [ContagemResultado_DataHomologacao], [ContagemResultado_DataExecucao], [ContagemResultado_RdmnIssueId], [ContagemResultado_RdmnProjectId], [ContagemResultado_RdmnUpdated], [ContagemResultado_CntSrvPrrCod], [ContagemResultado_CntSrvPrrPrz], "
          + " [ContagemResultado_CntSrvPrrCst], [ContagemResultado_TemDpnHmlg], [ContagemResultado_TmpEstExc], [ContagemResultado_TmpEstCrr], [ContagemResultado_InicioExc], [ContagemResultado_FimExc], [ContagemResultado_InicioCrr], [ContagemResultado_FimCrr], [ContagemResultado_TmpEstAnl], [ContagemResultado_InicioAnl], [ContagemResultado_FimAnl], [ContagemResultado_ProjetoCod], [ContagemResultado_VlrAceite], [ContagemResultado_Combinada], [ContagemResultado_Entrega], [ContagemResultado_DataInicio], [ContagemResultado_SemCusto], [ContagemResultado_VlrCnc], [ContagemResultado_PFCnc], [ContagemResultado_DataPrvPgm], [ContagemResultado_DataEntregaReal], [ContagemResultado_QuantidadeSolicitada]) VALUES(@ContagemResultado_Owner, @ContagemResultado_Descricao, @ContagemResultado_Observacao, @ContagemResultado_DataEntrega, @ContagemResultado_HoraEntrega, @ContagemResultado_DataPrevista, @ContagemResultado_SS, @ContagemResultado_DemandaFM, @ContagemResultado_UOOwner, @ContagemResultado_Evento, @ContagemResultado_TipoRegistro, @ContagemResultado_Referencia, @ContagemResultado_Restricoes, @ContagemResultado_PrioridadePrevista, @ContagemResultado_StatusDmn, @ContagemResultado_DataDmn, @ContagemResultado_FncUsrCod, @ContagemResultado_PrazoInicialDias, @ContagemResultado_DataCadastro, @ContagemResultado_OSManual, @ContagemResultado_Baseline, @ContagemResultado_EhValidacao, @Modulo_Codigo, @ContagemResultado_ContadorFSCod, @ContagemResultado_Responsavel, @ContagemResultado_NaoCnfDmnCod, @ContagemResultado_ContratadaCod, @ContagemResultado_ContratadaOrigemCod, @ContagemResultado_SistemaCod, @ContagemResultado_LoteAceiteCod, @ContagemResultado_ServicoSS, @ContagemResultado_LiqLogCod, @ContagemResultado_CntSrvCod, @ContagemResultado_OSVinculada, '', '', convert(int, 0), '', convert(int, 0), convert(int,"
          + " 0), '', convert( DATETIME, '17530101', 112 ), '', convert(int, 0), convert(int, 0), convert(int, 0), convert(int, 0), convert(int, 0), convert(int, 0), convert( DATETIME, '17530101', 112 ), convert( DATETIME, '17530101', 112 ), convert(int, 0), convert(int, 0), convert( DATETIME, '17530101', 112 ), convert(int, 0), convert(int, 0), convert(int, 0), convert(bit, 0), convert(int, 0), convert(int, 0), convert( DATETIME, '17530101', 112 ), convert( DATETIME, '17530101', 112 ), convert( DATETIME, '17530101', 112 ), convert( DATETIME, '17530101', 112 ), convert(int, 0), convert( DATETIME, '17530101', 112 ), convert( DATETIME, '17530101', 112 ), convert(int, 0), convert(int, 0), convert(bit, 0), convert(int, 0), convert( DATETIME, '17530101', 112 ), convert(bit, 0), convert(int, 0), convert(int, 0), convert( DATETIME, '17530101', 112 ), convert( DATETIME, '17530101', 112 ), convert(int, 0)); SELECT SCOPE_IDENTITY()" ;
          Object[] prmT004135 ;
          prmT004135 = new Object[] {
          new Object[] {"@ContagemResultado_Owner",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_Descricao",SqlDbType.VarChar,500,0} ,
          new Object[] {"@ContagemResultado_Observacao",SqlDbType.VarChar,20000,0} ,
          new Object[] {"@ContagemResultado_DataEntrega",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_HoraEntrega",SqlDbType.DateTime,0,5} ,
          new Object[] {"@ContagemResultado_DataPrevista",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultado_SS",SqlDbType.Int,8,0} ,
          new Object[] {"@ContagemResultado_DemandaFM",SqlDbType.VarChar,50,0} ,
          new Object[] {"@ContagemResultado_UOOwner",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_Evento",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@ContagemResultado_TipoRegistro",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContagemResultado_Referencia",SqlDbType.VarChar,500,0} ,
          new Object[] {"@ContagemResultado_Restricoes",SqlDbType.VarChar,250,0} ,
          new Object[] {"@ContagemResultado_PrioridadePrevista",SqlDbType.VarChar,250,0} ,
          new Object[] {"@ContagemResultado_StatusDmn",SqlDbType.Char,1,0} ,
          new Object[] {"@ContagemResultado_DataDmn",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_FncUsrCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_PrazoInicialDias",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@ContagemResultado_DataCadastro",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultado_OSManual",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContagemResultado_Baseline",SqlDbType.Bit,4,0} ,
          new Object[] {"@ContagemResultado_EhValidacao",SqlDbType.Bit,4,0} ,
          new Object[] {"@Modulo_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_ContadorFSCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_Responsavel",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_NaoCnfDmnCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_ContratadaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_ContratadaOrigemCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_SistemaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_LoteAceiteCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_ServicoSS",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_LiqLogCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_CntSrvCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_OSVinculada",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          String cmdBufferT004135 ;
          cmdBufferT004135=" UPDATE [ContagemResultado] SET [ContagemResultado_Owner]=@ContagemResultado_Owner, [ContagemResultado_Descricao]=@ContagemResultado_Descricao, [ContagemResultado_Observacao]=@ContagemResultado_Observacao, [ContagemResultado_DataEntrega]=@ContagemResultado_DataEntrega, [ContagemResultado_HoraEntrega]=@ContagemResultado_HoraEntrega, [ContagemResultado_DataPrevista]=@ContagemResultado_DataPrevista, [ContagemResultado_SS]=@ContagemResultado_SS, [ContagemResultado_DemandaFM]=@ContagemResultado_DemandaFM, [ContagemResultado_UOOwner]=@ContagemResultado_UOOwner, [ContagemResultado_Evento]=@ContagemResultado_Evento, [ContagemResultado_TipoRegistro]=@ContagemResultado_TipoRegistro, [ContagemResultado_Referencia]=@ContagemResultado_Referencia, [ContagemResultado_Restricoes]=@ContagemResultado_Restricoes, [ContagemResultado_PrioridadePrevista]=@ContagemResultado_PrioridadePrevista, [ContagemResultado_StatusDmn]=@ContagemResultado_StatusDmn, [ContagemResultado_DataDmn]=@ContagemResultado_DataDmn, [ContagemResultado_FncUsrCod]=@ContagemResultado_FncUsrCod, [ContagemResultado_PrazoInicialDias]=@ContagemResultado_PrazoInicialDias, [ContagemResultado_DataCadastro]=@ContagemResultado_DataCadastro, [ContagemResultado_OSManual]=@ContagemResultado_OSManual, [ContagemResultado_Baseline]=@ContagemResultado_Baseline, [ContagemResultado_EhValidacao]=@ContagemResultado_EhValidacao, [Modulo_Codigo]=@Modulo_Codigo, [ContagemResultado_ContadorFSCod]=@ContagemResultado_ContadorFSCod, [ContagemResultado_Responsavel]=@ContagemResultado_Responsavel, [ContagemResultado_NaoCnfDmnCod]=@ContagemResultado_NaoCnfDmnCod, [ContagemResultado_ContratadaCod]=@ContagemResultado_ContratadaCod, [ContagemResultado_ContratadaOrigemCod]=@ContagemResultado_ContratadaOrigemCod, [ContagemResultado_SistemaCod]=@ContagemResultado_SistemaCod, "
          + " [ContagemResultado_LoteAceiteCod]=@ContagemResultado_LoteAceiteCod, [ContagemResultado_ServicoSS]=@ContagemResultado_ServicoSS, [ContagemResultado_LiqLogCod]=@ContagemResultado_LiqLogCod, [ContagemResultado_CntSrvCod]=@ContagemResultado_CntSrvCod, [ContagemResultado_OSVinculada]=@ContagemResultado_OSVinculada  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo" ;
          Object[] prmT004136 ;
          prmT004136 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004137 ;
          prmT004137 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004138 ;
          prmT004138 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004139 ;
          prmT004139 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004140 ;
          prmT004140 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004141 ;
          prmT004141 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004142 ;
          prmT004142 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004143 ;
          prmT004143 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004144 ;
          prmT004144 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004145 ;
          prmT004145 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004146 ;
          prmT004146 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004147 ;
          prmT004147 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004148 ;
          prmT004148 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004149 ;
          prmT004149 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004150 ;
          prmT004150 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004151 ;
          prmT004151 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004152 ;
          prmT004152 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004153 ;
          prmT004153 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004154 ;
          prmT004154 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004155 ;
          prmT004155 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004156 ;
          prmT004156 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004157 ;
          prmT004157 = new Object[] {
          } ;
          Object[] prmT004158 ;
          prmT004158 = new Object[] {
          } ;
          Object[] prmT004159 ;
          prmT004159 = new Object[] {
          } ;
          Object[] prmT004160 ;
          prmT004160 = new Object[] {
          new Object[] {"@AV7WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004161 ;
          prmT004161 = new Object[] {
          new Object[] {"@AV7WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004162 ;
          prmT004162 = new Object[] {
          } ;
          Object[] prmT004163 ;
          prmT004163 = new Object[] {
          new Object[] {"@ContagemResultado_SistemaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004164 ;
          prmT004164 = new Object[] {
          new Object[] {"@AV7WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004165 ;
          prmT004165 = new Object[] {
          } ;
          Object[] prmT004166 ;
          prmT004166 = new Object[] {
          } ;
          Object[] prmT004167 ;
          prmT004167 = new Object[] {
          new Object[] {"@ContagemResultado_ServicoSS",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004168 ;
          prmT004168 = new Object[] {
          new Object[] {"@ContagemResultado_CntSrvCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004169 ;
          prmT004169 = new Object[] {
          new Object[] {"@ContagemResultado_OSVinculada",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004170 ;
          prmT004170 = new Object[] {
          new Object[] {"@ContagemResultado_NaoCnfDmnCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004171 ;
          prmT004171 = new Object[] {
          new Object[] {"@ContagemResultado_LoteAceiteCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004172 ;
          prmT004172 = new Object[] {
          new Object[] {"@ContagemResultado_ContratadaOrigemCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004173 ;
          prmT004173 = new Object[] {
          new Object[] {"@ContagemResultado_ContadorFSCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004174 ;
          prmT004174 = new Object[] {
          new Object[] {"@ContagemResultado_Responsavel",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004175 ;
          prmT004175 = new Object[] {
          new Object[] {"@ContagemResultado_LiqLogCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004176 ;
          prmT004176 = new Object[] {
          new Object[] {"@Modulo_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004177 ;
          prmT004177 = new Object[] {
          new Object[] {"@ContagemResultado_ContratadaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT004178 ;
          prmT004178 = new Object[] {
          new Object[] {"@ContagemResultado_SistemaCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("T00412", "SELECT [ContagemResultado_Codigo], [ContagemResultado_Owner], [ContagemResultado_Descricao], [ContagemResultado_Observacao], [ContagemResultado_DataEntrega], [ContagemResultado_HoraEntrega], [ContagemResultado_DataPrevista], [ContagemResultado_SS], [ContagemResultado_DemandaFM], [ContagemResultado_UOOwner], [ContagemResultado_Evento], [ContagemResultado_TipoRegistro], [ContagemResultado_Referencia], [ContagemResultado_Restricoes], [ContagemResultado_PrioridadePrevista], [ContagemResultado_StatusDmn], [ContagemResultado_DataDmn], [ContagemResultado_FncUsrCod], [ContagemResultado_PrazoInicialDias], [ContagemResultado_DataCadastro], [ContagemResultado_OSManual], [ContagemResultado_Baseline], [ContagemResultado_EhValidacao], [Modulo_Codigo], [ContagemResultado_ContadorFSCod] AS ContagemResultado_ContadorFSCod, [ContagemResultado_Responsavel] AS ContagemResultado_Responsavel, [ContagemResultado_NaoCnfDmnCod] AS ContagemResultado_NaoCnfDmnCod, [ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, [ContagemResultado_ContratadaOrigemCod] AS ContagemResultado_ContratadaOr, [ContagemResultado_SistemaCod] AS ContagemResultado_SistemaCod, [ContagemResultado_LoteAceiteCod] AS ContagemResultado_LoteAceiteCod, [ContagemResultado_ServicoSS] AS ContagemResultado_ServicoSS, [ContagemResultado_LiqLogCod] AS ContagemResultado_LiqLogCod, [ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, [ContagemResultado_OSVinculada] AS ContagemResultado_OSVinculada FROM [ContagemResultado] WITH (UPDLOCK) WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT00412,1,0,true,false )
             ,new CursorDef("T00413", "SELECT [ContagemResultado_Codigo], [ContagemResultado_Owner], [ContagemResultado_Descricao], [ContagemResultado_Observacao], [ContagemResultado_DataEntrega], [ContagemResultado_HoraEntrega], [ContagemResultado_DataPrevista], [ContagemResultado_SS], [ContagemResultado_DemandaFM], [ContagemResultado_UOOwner], [ContagemResultado_Evento], [ContagemResultado_TipoRegistro], [ContagemResultado_Referencia], [ContagemResultado_Restricoes], [ContagemResultado_PrioridadePrevista], [ContagemResultado_StatusDmn], [ContagemResultado_DataDmn], [ContagemResultado_FncUsrCod], [ContagemResultado_PrazoInicialDias], [ContagemResultado_DataCadastro], [ContagemResultado_OSManual], [ContagemResultado_Baseline], [ContagemResultado_EhValidacao], [Modulo_Codigo], [ContagemResultado_ContadorFSCod] AS ContagemResultado_ContadorFSCod, [ContagemResultado_Responsavel] AS ContagemResultado_Responsavel, [ContagemResultado_NaoCnfDmnCod] AS ContagemResultado_NaoCnfDmnCod, [ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, [ContagemResultado_ContratadaOrigemCod] AS ContagemResultado_ContratadaOr, [ContagemResultado_SistemaCod] AS ContagemResultado_SistemaCod, [ContagemResultado_LoteAceiteCod] AS ContagemResultado_LoteAceiteCod, [ContagemResultado_ServicoSS] AS ContagemResultado_ServicoSS, [ContagemResultado_LiqLogCod] AS ContagemResultado_LiqLogCod, [ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, [ContagemResultado_OSVinculada] AS ContagemResultado_OSVinculada FROM [ContagemResultado] WITH (NOLOCK) WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT00413,1,0,true,false )
             ,new CursorDef("T00414", "SELECT [Modulo_Codigo] FROM [Modulo] WITH (NOLOCK) WHERE [Modulo_Codigo] = @Modulo_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT00414,1,0,true,false )
             ,new CursorDef("T00415", "SELECT [Usuario_Codigo] AS ContagemResultado_ContadorFSCod FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @ContagemResultado_ContadorFSCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT00415,1,0,true,false )
             ,new CursorDef("T00416", "SELECT [Usuario_Codigo] AS ContagemResultado_Responsavel FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @ContagemResultado_Responsavel ",true, GxErrorMask.GX_NOMASK, false, this,prmT00416,1,0,true,false )
             ,new CursorDef("T00417", "SELECT [NaoConformidade_Codigo] AS ContagemResultado_NaoCnfDmnCod FROM [NaoConformidade] WITH (NOLOCK) WHERE [NaoConformidade_Codigo] = @ContagemResultado_NaoCnfDmnCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT00417,1,0,true,false )
             ,new CursorDef("T00418", "SELECT [Contratada_Codigo] AS ContagemResultado_ContratadaCod FROM [Contratada] WITH (NOLOCK) WHERE [Contratada_Codigo] = @ContagemResultado_ContratadaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT00418,1,0,true,false )
             ,new CursorDef("T00419", "SELECT [Contratada_Codigo] AS ContagemResultado_ContratadaOr FROM [Contratada] WITH (NOLOCK) WHERE [Contratada_Codigo] = @ContagemResultado_ContratadaOrigemCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT00419,1,0,true,false )
             ,new CursorDef("T004110", "SELECT [Sistema_Codigo] AS ContagemResultado_SistemaCod FROM [Sistema] WITH (NOLOCK) WHERE [Sistema_Codigo] = @ContagemResultado_SistemaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT004110,1,0,true,false )
             ,new CursorDef("T004111", "SELECT [Lote_Codigo] AS ContagemResultado_LoteAceiteCod FROM [Lote] WITH (NOLOCK) WHERE [Lote_Codigo] = @ContagemResultado_LoteAceiteCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT004111,1,0,true,false )
             ,new CursorDef("T004112", "SELECT [Servico_Codigo] AS ContagemResultado_ServicoSS FROM [Servico] WITH (NOLOCK) WHERE [Servico_Codigo] = @ContagemResultado_ServicoSS ",true, GxErrorMask.GX_NOMASK, false, this,prmT004112,1,0,true,false )
             ,new CursorDef("T004113", "SELECT [ContagemResultadoLiqLog_Codigo] AS ContagemResultado_LiqLogCod FROM [ContagemResultadoLiqLog] WITH (NOLOCK) WHERE [ContagemResultadoLiqLog_Codigo] = @ContagemResultado_LiqLogCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT004113,1,0,true,false )
             ,new CursorDef("T004114", "SELECT [ContratoServicos_Codigo] AS ContagemResultado_CntSrvCod FROM [ContratoServicos] WITH (NOLOCK) WHERE [ContratoServicos_Codigo] = @ContagemResultado_CntSrvCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT004114,1,0,true,false )
             ,new CursorDef("T004115", "SELECT [ContagemResultado_Codigo] AS ContagemResultado_OSVinculada FROM [ContagemResultado] WITH (NOLOCK) WHERE [ContagemResultado_Codigo] = @ContagemResultado_OSVinculada ",true, GxErrorMask.GX_NOMASK, false, this,prmT004115,1,0,true,false )
             ,new CursorDef("T004116", "SELECT [Servico_Codigo], [Servico_Nome] FROM [Servico] WITH (NOLOCK) ORDER BY [Servico_Nome] ",true, GxErrorMask.GX_NOMASK, false, this,prmT004116,0,0,true,false )
             ,new CursorDef("T004117", "SELECT [Lote_Codigo], [Lote_Nome] FROM [Lote] WITH (NOLOCK) ORDER BY [Lote_Nome] ",true, GxErrorMask.GX_NOMASK, false, this,prmT004117,0,0,true,false )
             ,new CursorDef("T004118", "SELECT TM1.[ContagemResultado_Codigo], TM1.[ContagemResultado_Owner], TM1.[ContagemResultado_Descricao], TM1.[ContagemResultado_Observacao], TM1.[ContagemResultado_DataEntrega], TM1.[ContagemResultado_HoraEntrega], TM1.[ContagemResultado_DataPrevista], TM1.[ContagemResultado_SS], TM1.[ContagemResultado_DemandaFM], TM1.[ContagemResultado_UOOwner], TM1.[ContagemResultado_Evento], TM1.[ContagemResultado_TipoRegistro], TM1.[ContagemResultado_Referencia], TM1.[ContagemResultado_Restricoes], TM1.[ContagemResultado_PrioridadePrevista], TM1.[ContagemResultado_StatusDmn], TM1.[ContagemResultado_DataDmn], TM1.[ContagemResultado_FncUsrCod], TM1.[ContagemResultado_PrazoInicialDias], TM1.[ContagemResultado_DataCadastro], TM1.[ContagemResultado_OSManual], TM1.[ContagemResultado_Baseline], TM1.[ContagemResultado_EhValidacao], TM1.[Modulo_Codigo], TM1.[ContagemResultado_ContadorFSCod] AS ContagemResultado_ContadorFSCod, TM1.[ContagemResultado_Responsavel] AS ContagemResultado_Responsavel, TM1.[ContagemResultado_NaoCnfDmnCod] AS ContagemResultado_NaoCnfDmnCod, TM1.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, TM1.[ContagemResultado_ContratadaOrigemCod] AS ContagemResultado_ContratadaOr, TM1.[ContagemResultado_SistemaCod] AS ContagemResultado_SistemaCod, TM1.[ContagemResultado_LoteAceiteCod] AS ContagemResultado_LoteAceiteCod, TM1.[ContagemResultado_ServicoSS] AS ContagemResultado_ServicoSS, TM1.[ContagemResultado_LiqLogCod] AS ContagemResultado_LiqLogCod, TM1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, TM1.[ContagemResultado_OSVinculada] AS ContagemResultado_OSVinculada FROM [ContagemResultado] TM1 WITH (NOLOCK) WHERE TM1.[ContagemResultado_Codigo] = @ContagemResultado_Codigo ORDER BY TM1.[ContagemResultado_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT004118,100,0,true,false )
             ,new CursorDef("T004119", "SELECT [Servico_Codigo] AS ContagemResultado_ServicoSS FROM [Servico] WITH (NOLOCK) WHERE [Servico_Codigo] = @ContagemResultado_ServicoSS ",true, GxErrorMask.GX_NOMASK, false, this,prmT004119,1,0,true,false )
             ,new CursorDef("T004120", "SELECT [ContratoServicos_Codigo] AS ContagemResultado_CntSrvCod FROM [ContratoServicos] WITH (NOLOCK) WHERE [ContratoServicos_Codigo] = @ContagemResultado_CntSrvCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT004120,1,0,true,false )
             ,new CursorDef("T004121", "SELECT [ContagemResultado_Codigo] AS ContagemResultado_OSVinculada FROM [ContagemResultado] WITH (NOLOCK) WHERE [ContagemResultado_Codigo] = @ContagemResultado_OSVinculada ",true, GxErrorMask.GX_NOMASK, false, this,prmT004121,1,0,true,false )
             ,new CursorDef("T004122", "SELECT [NaoConformidade_Codigo] AS ContagemResultado_NaoCnfDmnCod FROM [NaoConformidade] WITH (NOLOCK) WHERE [NaoConformidade_Codigo] = @ContagemResultado_NaoCnfDmnCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT004122,1,0,true,false )
             ,new CursorDef("T004123", "SELECT [Lote_Codigo] AS ContagemResultado_LoteAceiteCod FROM [Lote] WITH (NOLOCK) WHERE [Lote_Codigo] = @ContagemResultado_LoteAceiteCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT004123,1,0,true,false )
             ,new CursorDef("T004124", "SELECT [Contratada_Codigo] AS ContagemResultado_ContratadaOr FROM [Contratada] WITH (NOLOCK) WHERE [Contratada_Codigo] = @ContagemResultado_ContratadaOrigemCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT004124,1,0,true,false )
             ,new CursorDef("T004125", "SELECT [Usuario_Codigo] AS ContagemResultado_ContadorFSCod FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @ContagemResultado_ContadorFSCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT004125,1,0,true,false )
             ,new CursorDef("T004126", "SELECT [Usuario_Codigo] AS ContagemResultado_Responsavel FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @ContagemResultado_Responsavel ",true, GxErrorMask.GX_NOMASK, false, this,prmT004126,1,0,true,false )
             ,new CursorDef("T004127", "SELECT [ContagemResultadoLiqLog_Codigo] AS ContagemResultado_LiqLogCod FROM [ContagemResultadoLiqLog] WITH (NOLOCK) WHERE [ContagemResultadoLiqLog_Codigo] = @ContagemResultado_LiqLogCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT004127,1,0,true,false )
             ,new CursorDef("T004128", "SELECT [Modulo_Codigo] FROM [Modulo] WITH (NOLOCK) WHERE [Modulo_Codigo] = @Modulo_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT004128,1,0,true,false )
             ,new CursorDef("T004129", "SELECT [Contratada_Codigo] AS ContagemResultado_ContratadaCod FROM [Contratada] WITH (NOLOCK) WHERE [Contratada_Codigo] = @ContagemResultado_ContratadaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT004129,1,0,true,false )
             ,new CursorDef("T004130", "SELECT [Sistema_Codigo] AS ContagemResultado_SistemaCod FROM [Sistema] WITH (NOLOCK) WHERE [Sistema_Codigo] = @ContagemResultado_SistemaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT004130,1,0,true,false )
             ,new CursorDef("T004131", "SELECT [ContagemResultado_Codigo] FROM [ContagemResultado] WITH (NOLOCK) WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT004131,1,0,true,false )
             ,new CursorDef("T004132", "SELECT TOP 1 [ContagemResultado_Codigo] FROM [ContagemResultado] WITH (NOLOCK) WHERE ( [ContagemResultado_Codigo] > @ContagemResultado_Codigo) ORDER BY [ContagemResultado_Codigo]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT004132,1,0,true,true )
             ,new CursorDef("T004133", "SELECT TOP 1 [ContagemResultado_Codigo] FROM [ContagemResultado] WITH (NOLOCK) WHERE ( [ContagemResultado_Codigo] < @ContagemResultado_Codigo) ORDER BY [ContagemResultado_Codigo] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT004133,1,0,true,true )
             ,new CursorDef("T004134", cmdBufferT004134, GxErrorMask.GX_NOMASK,prmT004134)
             ,new CursorDef("T004135", cmdBufferT004135, GxErrorMask.GX_NOMASK,prmT004135)
             ,new CursorDef("T004136", "DELETE FROM [ContagemResultado]  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo", GxErrorMask.GX_NOMASK,prmT004136)
             ,new CursorDef("T004137", "SELECT TOP 1 [ContagemResultado_Codigo] AS ContagemResultado_OSVinculada FROM [ContagemResultado] WITH (NOLOCK) WHERE [ContagemResultado_OSVinculada] = @ContagemResultado_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT004137,1,0,true,true )
             ,new CursorDef("T004138", "SELECT TOP 1 [ContagemResultadoNaoCnf_Codigo] FROM [ContagemResultadoNaoCnf] WITH (NOLOCK) WHERE [ContagemResultadoNaoCnf_OSCod] = @ContagemResultado_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT004138,1,0,true,true )
             ,new CursorDef("T004139", "SELECT TOP 1 [ContagemResultadoRequisito_Codigo] FROM [ContagemResultadoRequisito] WITH (NOLOCK) WHERE [ContagemResultadoRequisito_OSCod] = @ContagemResultado_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT004139,1,0,true,true )
             ,new CursorDef("T004140", "SELECT TOP 1 [ContagemResultadoQA_Codigo] FROM [ContagemResultadoQA] WITH (NOLOCK) WHERE [ContagemResultadoQA_OSCod] = @ContagemResultado_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT004140,1,0,true,true )
             ,new CursorDef("T004141", "SELECT TOP 1 [ContagemResultadoChckLst_Codigo] FROM [ContagemResultadoChckLst] WITH (NOLOCK) WHERE [ContagemResultadoChckLst_OSCod] = @ContagemResultado_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT004141,1,0,true,true )
             ,new CursorDef("T004142", "SELECT TOP 1 [ContagemResultadoChckLstLog_Codigo] FROM [ContagemResultadoChckLstLog] WITH (NOLOCK) WHERE [ContagemResultadoChckLstLog_OSCodigo] = @ContagemResultado_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT004142,1,0,true,true )
             ,new CursorDef("T004143", "SELECT TOP 1 [ContagemResultadoArtefato_Codigo] FROM [ContagemResultadoArtefato] WITH (NOLOCK) WHERE [ContagemResultadoArtefato_OSCod] = @ContagemResultado_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT004143,1,0,true,true )
             ,new CursorDef("T004144", "SELECT TOP 1 [Proposta_Codigo] FROM dbo.[Proposta] WITH (NOLOCK) WHERE [Proposta_OSCodigo] = @ContagemResultado_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT004144,1,0,true,true )
             ,new CursorDef("T004145", "SELECT TOP 1 [ContagemResultadoExecucao_Codigo] FROM [ContagemResultadoExecucao] WITH (NOLOCK) WHERE [ContagemResultadoExecucao_OSCod] = @ContagemResultado_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT004145,1,0,true,true )
             ,new CursorDef("T004146", "SELECT TOP 1 [ContagemResultadoLiqLog_Codigo], [ContagemResultadoLiqLogOS_Codigo] FROM [ContagemResultadoLiqLogOS] WITH (NOLOCK) WHERE [ContagemResultadoLiqLogOS_OSCod] = @ContagemResultado_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT004146,1,0,true,true )
             ,new CursorDef("T004147", "SELECT TOP 1 [ContagemResultadoNota_Codigo] FROM [ContagemResultadoNotas] WITH (NOLOCK) WHERE [ContagemResultadoNota_DemandaCod] = @ContagemResultado_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT004147,1,0,true,true )
             ,new CursorDef("T004148", "SELECT TOP 1 [ContagemResultadoIndicadores_DemandaCod], [ContagemResultadoIndicadores_IndicadorCod] FROM [ContagemResultadoIndicadores] WITH (NOLOCK) WHERE [ContagemResultadoIndicadores_DemandaCod] = @ContagemResultado_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT004148,1,0,true,true )
             ,new CursorDef("T004149", "SELECT TOP 1 [Incidentes_Codigo] FROM [ContagemResultadoIncidentes] WITH (NOLOCK) WHERE [Incidentes_DemandaCod] = @ContagemResultado_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT004149,1,0,true,true )
             ,new CursorDef("T004150", "SELECT TOP 1 [AgendaAtendimento_CntSrcCod], [AgendaAtendimento_Data], [AgendaAtendimento_CodDmn] FROM [AgendaAtendimento] WITH (NOLOCK) WHERE [AgendaAtendimento_CodDmn] = @ContagemResultado_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT004150,1,0,true,true )
             ,new CursorDef("T004151", "SELECT TOP 1 [LogResponsavel_Codigo] FROM [LogResponsavel] WITH (NOLOCK) WHERE [LogResponsavel_DemandaCod] = @ContagemResultado_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT004151,1,0,true,true )
             ,new CursorDef("T004152", "SELECT TOP 1 [SolicServicoReqNeg_Codigo] FROM [SolicitacaoServicoReqNegocio] WITH (NOLOCK) WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT004152,1,0,true,true )
             ,new CursorDef("T004153", "SELECT TOP 1 [ContagemResultadoEvidencia_Codigo] FROM [ContagemResultadoEvidencia] WITH (NOLOCK) WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT004153,1,0,true,true )
             ,new CursorDef("T004154", "SELECT TOP 1 [ContagemResultadoNotificacao_Codigo], [ContagemResultado_Codigo] FROM [ContagemResultadoNotificacaoDemanda] WITH (NOLOCK) WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT004154,1,0,true,true )
             ,new CursorDef("T004155", "SELECT TOP 1 [ContagemResultado_Codigo], [ContagemResultadoErro_Tipo] FROM [ContagemResultadoErro] WITH (NOLOCK) WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT004155,1,0,true,true )
             ,new CursorDef("T004156", "SELECT TOP 1 [ContagemResultado_Codigo], [ContagemResultado_DataCnt], [ContagemResultado_HoraCnt] FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT004156,1,0,true,true )
             ,new CursorDef("T004157", "SELECT [ContagemResultado_Codigo] FROM [ContagemResultado] WITH (NOLOCK) ORDER BY [ContagemResultado_Codigo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT004157,100,0,true,false )
             ,new CursorDef("T004158", "SELECT [Lote_Codigo], [Lote_Nome] FROM [Lote] WITH (NOLOCK) ORDER BY [Lote_Nome] ",true, GxErrorMask.GX_NOMASK, false, this,prmT004158,0,0,true,false )
             ,new CursorDef("T004159", "SELECT [Servico_Codigo], [Servico_Nome] FROM [Servico] WITH (NOLOCK) ORDER BY [Servico_Nome] ",true, GxErrorMask.GX_NOMASK, false, this,prmT004159,0,0,true,false )
             ,new CursorDef("T004160", "SELECT [NaoConformidade_Codigo], [NaoConformidade_Nome], [NaoConformidade_Tipo], [NaoConformidade_AreaTrabalhoCod] FROM [NaoConformidade] WITH (NOLOCK) WHERE ([NaoConformidade_Tipo] = 1) AND ([NaoConformidade_AreaTrabalhoCod] = @AV7WWPCo_1Areatrabalho_codigo) ORDER BY [NaoConformidade_Nome] ",true, GxErrorMask.GX_NOMASK, false, this,prmT004160,0,0,true,false )
             ,new CursorDef("T004161", "SELECT T2.[Pessoa_Codigo] AS Contratada_PessoaCod, T1.[Contratada_Codigo], T2.[Pessoa_Nome] AS Contratada_PessoaNom, T1.[Contratada_AreaTrabalhoCod] FROM ([Contratada] T1 WITH (NOLOCK) INNER JOIN [Pessoa] T2 WITH (NOLOCK) ON T2.[Pessoa_Codigo] = T1.[Contratada_PessoaCod]) WHERE T1.[Contratada_AreaTrabalhoCod] = @AV7WWPCo_1Areatrabalho_codigo ORDER BY T2.[Pessoa_Nome] ",true, GxErrorMask.GX_NOMASK, false, this,prmT004161,0,0,true,false )
             ,new CursorDef("T004162", "SELECT T2.[Pessoa_Codigo] AS ContagemResultado_CrFSPessoaCod, T1.[Usuario_Codigo] AS ContagemResultado_ContadorFSCod, T2.[Pessoa_Nome] AS ContagemResultado_ContadorFSNo FROM ([Usuario] T1 WITH (NOLOCK) LEFT JOIN [Pessoa] T2 WITH (NOLOCK) ON T2.[Pessoa_Codigo] = T1.[Usuario_PessoaCod]) ORDER BY T2.[Pessoa_Nome] ",true, GxErrorMask.GX_NOMASK, false, this,prmT004162,0,0,true,false )
             ,new CursorDef("T004163", "SELECT [Modulo_Codigo], [Modulo_Nome], [Sistema_Codigo] FROM [Modulo] WITH (NOLOCK) WHERE [Sistema_Codigo] = @ContagemResultado_SistemaCod ORDER BY [Modulo_Nome] ",true, GxErrorMask.GX_NOMASK, false, this,prmT004163,0,0,true,false )
             ,new CursorDef("T004164", "SELECT T2.[Pessoa_Codigo] AS Contratada_PessoaCod, T1.[Contratada_Codigo], T2.[Pessoa_Nome] AS Contratada_PessoaNom, T1.[Contratada_AreaTrabalhoCod] FROM ([Contratada] T1 WITH (NOLOCK) INNER JOIN [Pessoa] T2 WITH (NOLOCK) ON T2.[Pessoa_Codigo] = T1.[Contratada_PessoaCod]) WHERE T1.[Contratada_AreaTrabalhoCod] = @AV7WWPCo_1Areatrabalho_codigo ORDER BY T2.[Pessoa_Nome] ",true, GxErrorMask.GX_NOMASK, false, this,prmT004164,0,0,true,false )
             ,new CursorDef("T004165", "SELECT [Servico_Codigo], [Servico_Nome] FROM [Servico] WITH (NOLOCK) ORDER BY [Servico_Nome] ",true, GxErrorMask.GX_NOMASK, false, this,prmT004165,0,0,true,false )
             ,new CursorDef("T004166", "SELECT [Lote_Codigo], [Lote_Nome] FROM [Lote] WITH (NOLOCK) ORDER BY [Lote_Nome] ",true, GxErrorMask.GX_NOMASK, false, this,prmT004166,0,0,true,false )
             ,new CursorDef("T004167", "SELECT [Servico_Codigo] AS ContagemResultado_ServicoSS FROM [Servico] WITH (NOLOCK) WHERE [Servico_Codigo] = @ContagemResultado_ServicoSS ",true, GxErrorMask.GX_NOMASK, false, this,prmT004167,1,0,true,false )
             ,new CursorDef("T004168", "SELECT [ContratoServicos_Codigo] AS ContagemResultado_CntSrvCod FROM [ContratoServicos] WITH (NOLOCK) WHERE [ContratoServicos_Codigo] = @ContagemResultado_CntSrvCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT004168,1,0,true,false )
             ,new CursorDef("T004169", "SELECT [ContagemResultado_Codigo] AS ContagemResultado_OSVinculada FROM [ContagemResultado] WITH (NOLOCK) WHERE [ContagemResultado_Codigo] = @ContagemResultado_OSVinculada ",true, GxErrorMask.GX_NOMASK, false, this,prmT004169,1,0,true,false )
             ,new CursorDef("T004170", "SELECT [NaoConformidade_Codigo] AS ContagemResultado_NaoCnfDmnCod FROM [NaoConformidade] WITH (NOLOCK) WHERE [NaoConformidade_Codigo] = @ContagemResultado_NaoCnfDmnCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT004170,1,0,true,false )
             ,new CursorDef("T004171", "SELECT [Lote_Codigo] AS ContagemResultado_LoteAceiteCod FROM [Lote] WITH (NOLOCK) WHERE [Lote_Codigo] = @ContagemResultado_LoteAceiteCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT004171,1,0,true,false )
             ,new CursorDef("T004172", "SELECT [Contratada_Codigo] AS ContagemResultado_ContratadaOr FROM [Contratada] WITH (NOLOCK) WHERE [Contratada_Codigo] = @ContagemResultado_ContratadaOrigemCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT004172,1,0,true,false )
             ,new CursorDef("T004173", "SELECT [Usuario_Codigo] AS ContagemResultado_ContadorFSCod FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @ContagemResultado_ContadorFSCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT004173,1,0,true,false )
             ,new CursorDef("T004174", "SELECT [Usuario_Codigo] AS ContagemResultado_Responsavel FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @ContagemResultado_Responsavel ",true, GxErrorMask.GX_NOMASK, false, this,prmT004174,1,0,true,false )
             ,new CursorDef("T004175", "SELECT [ContagemResultadoLiqLog_Codigo] AS ContagemResultado_LiqLogCod FROM [ContagemResultadoLiqLog] WITH (NOLOCK) WHERE [ContagemResultadoLiqLog_Codigo] = @ContagemResultado_LiqLogCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT004175,1,0,true,false )
             ,new CursorDef("T004176", "SELECT [Modulo_Codigo] FROM [Modulo] WITH (NOLOCK) WHERE [Modulo_Codigo] = @Modulo_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT004176,1,0,true,false )
             ,new CursorDef("T004177", "SELECT [Contratada_Codigo] AS ContagemResultado_ContratadaCod FROM [Contratada] WITH (NOLOCK) WHERE [Contratada_Codigo] = @ContagemResultado_ContratadaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT004177,1,0,true,false )
             ,new CursorDef("T004178", "SELECT [Sistema_Codigo] AS ContagemResultado_SistemaCod FROM [Sistema] WITH (NOLOCK) WHERE [Sistema_Codigo] = @ContagemResultado_SistemaCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT004178,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((String[]) buf[4])[0] = rslt.getLongVarchar(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((DateTime[]) buf[6])[0] = rslt.getGXDate(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((DateTime[]) buf[8])[0] = rslt.getGXDateTime(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((DateTime[]) buf[10])[0] = rslt.getGXDateTime(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((int[]) buf[12])[0] = rslt.getInt(8) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                ((String[]) buf[14])[0] = rslt.getVarchar(9) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(9);
                ((int[]) buf[16])[0] = rslt.getInt(10) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(10);
                ((short[]) buf[18])[0] = rslt.getShort(11) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(11);
                ((short[]) buf[20])[0] = rslt.getShort(12) ;
                ((String[]) buf[21])[0] = rslt.getVarchar(13) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(13);
                ((String[]) buf[23])[0] = rslt.getVarchar(14) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(14);
                ((String[]) buf[25])[0] = rslt.getVarchar(15) ;
                ((bool[]) buf[26])[0] = rslt.wasNull(15);
                ((String[]) buf[27])[0] = rslt.getString(16, 1) ;
                ((bool[]) buf[28])[0] = rslt.wasNull(16);
                ((DateTime[]) buf[29])[0] = rslt.getGXDate(17) ;
                ((int[]) buf[30])[0] = rslt.getInt(18) ;
                ((bool[]) buf[31])[0] = rslt.wasNull(18);
                ((short[]) buf[32])[0] = rslt.getShort(19) ;
                ((bool[]) buf[33])[0] = rslt.wasNull(19);
                ((DateTime[]) buf[34])[0] = rslt.getGXDateTime(20) ;
                ((bool[]) buf[35])[0] = rslt.wasNull(20);
                ((bool[]) buf[36])[0] = rslt.getBool(21) ;
                ((bool[]) buf[37])[0] = rslt.wasNull(21);
                ((bool[]) buf[38])[0] = rslt.getBool(22) ;
                ((bool[]) buf[39])[0] = rslt.wasNull(22);
                ((bool[]) buf[40])[0] = rslt.getBool(23) ;
                ((bool[]) buf[41])[0] = rslt.wasNull(23);
                ((int[]) buf[42])[0] = rslt.getInt(24) ;
                ((bool[]) buf[43])[0] = rslt.wasNull(24);
                ((int[]) buf[44])[0] = rslt.getInt(25) ;
                ((bool[]) buf[45])[0] = rslt.wasNull(25);
                ((int[]) buf[46])[0] = rslt.getInt(26) ;
                ((bool[]) buf[47])[0] = rslt.wasNull(26);
                ((int[]) buf[48])[0] = rslt.getInt(27) ;
                ((bool[]) buf[49])[0] = rslt.wasNull(27);
                ((int[]) buf[50])[0] = rslt.getInt(28) ;
                ((bool[]) buf[51])[0] = rslt.wasNull(28);
                ((int[]) buf[52])[0] = rslt.getInt(29) ;
                ((bool[]) buf[53])[0] = rslt.wasNull(29);
                ((int[]) buf[54])[0] = rslt.getInt(30) ;
                ((bool[]) buf[55])[0] = rslt.wasNull(30);
                ((int[]) buf[56])[0] = rslt.getInt(31) ;
                ((bool[]) buf[57])[0] = rslt.wasNull(31);
                ((int[]) buf[58])[0] = rslt.getInt(32) ;
                ((bool[]) buf[59])[0] = rslt.wasNull(32);
                ((int[]) buf[60])[0] = rslt.getInt(33) ;
                ((bool[]) buf[61])[0] = rslt.wasNull(33);
                ((int[]) buf[62])[0] = rslt.getInt(34) ;
                ((bool[]) buf[63])[0] = rslt.wasNull(34);
                ((int[]) buf[64])[0] = rslt.getInt(35) ;
                ((bool[]) buf[65])[0] = rslt.wasNull(35);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((String[]) buf[4])[0] = rslt.getLongVarchar(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((DateTime[]) buf[6])[0] = rslt.getGXDate(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((DateTime[]) buf[8])[0] = rslt.getGXDateTime(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((DateTime[]) buf[10])[0] = rslt.getGXDateTime(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((int[]) buf[12])[0] = rslt.getInt(8) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                ((String[]) buf[14])[0] = rslt.getVarchar(9) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(9);
                ((int[]) buf[16])[0] = rslt.getInt(10) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(10);
                ((short[]) buf[18])[0] = rslt.getShort(11) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(11);
                ((short[]) buf[20])[0] = rslt.getShort(12) ;
                ((String[]) buf[21])[0] = rslt.getVarchar(13) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(13);
                ((String[]) buf[23])[0] = rslt.getVarchar(14) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(14);
                ((String[]) buf[25])[0] = rslt.getVarchar(15) ;
                ((bool[]) buf[26])[0] = rslt.wasNull(15);
                ((String[]) buf[27])[0] = rslt.getString(16, 1) ;
                ((bool[]) buf[28])[0] = rslt.wasNull(16);
                ((DateTime[]) buf[29])[0] = rslt.getGXDate(17) ;
                ((int[]) buf[30])[0] = rslt.getInt(18) ;
                ((bool[]) buf[31])[0] = rslt.wasNull(18);
                ((short[]) buf[32])[0] = rslt.getShort(19) ;
                ((bool[]) buf[33])[0] = rslt.wasNull(19);
                ((DateTime[]) buf[34])[0] = rslt.getGXDateTime(20) ;
                ((bool[]) buf[35])[0] = rslt.wasNull(20);
                ((bool[]) buf[36])[0] = rslt.getBool(21) ;
                ((bool[]) buf[37])[0] = rslt.wasNull(21);
                ((bool[]) buf[38])[0] = rslt.getBool(22) ;
                ((bool[]) buf[39])[0] = rslt.wasNull(22);
                ((bool[]) buf[40])[0] = rslt.getBool(23) ;
                ((bool[]) buf[41])[0] = rslt.wasNull(23);
                ((int[]) buf[42])[0] = rslt.getInt(24) ;
                ((bool[]) buf[43])[0] = rslt.wasNull(24);
                ((int[]) buf[44])[0] = rslt.getInt(25) ;
                ((bool[]) buf[45])[0] = rslt.wasNull(25);
                ((int[]) buf[46])[0] = rslt.getInt(26) ;
                ((bool[]) buf[47])[0] = rslt.wasNull(26);
                ((int[]) buf[48])[0] = rslt.getInt(27) ;
                ((bool[]) buf[49])[0] = rslt.wasNull(27);
                ((int[]) buf[50])[0] = rslt.getInt(28) ;
                ((bool[]) buf[51])[0] = rslt.wasNull(28);
                ((int[]) buf[52])[0] = rslt.getInt(29) ;
                ((bool[]) buf[53])[0] = rslt.wasNull(29);
                ((int[]) buf[54])[0] = rslt.getInt(30) ;
                ((bool[]) buf[55])[0] = rslt.wasNull(30);
                ((int[]) buf[56])[0] = rslt.getInt(31) ;
                ((bool[]) buf[57])[0] = rslt.wasNull(31);
                ((int[]) buf[58])[0] = rslt.getInt(32) ;
                ((bool[]) buf[59])[0] = rslt.wasNull(32);
                ((int[]) buf[60])[0] = rslt.getInt(33) ;
                ((bool[]) buf[61])[0] = rslt.wasNull(33);
                ((int[]) buf[62])[0] = rslt.getInt(34) ;
                ((bool[]) buf[63])[0] = rslt.wasNull(34);
                ((int[]) buf[64])[0] = rslt.getInt(35) ;
                ((bool[]) buf[65])[0] = rslt.wasNull(35);
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 10 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 11 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 12 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 13 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 14 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                return;
             case 15 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                return;
             case 16 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((String[]) buf[4])[0] = rslt.getLongVarchar(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((DateTime[]) buf[6])[0] = rslt.getGXDate(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((DateTime[]) buf[8])[0] = rslt.getGXDateTime(6) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((DateTime[]) buf[10])[0] = rslt.getGXDateTime(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((int[]) buf[12])[0] = rslt.getInt(8) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(8);
                ((String[]) buf[14])[0] = rslt.getVarchar(9) ;
                ((bool[]) buf[15])[0] = rslt.wasNull(9);
                ((int[]) buf[16])[0] = rslt.getInt(10) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(10);
                ((short[]) buf[18])[0] = rslt.getShort(11) ;
                ((bool[]) buf[19])[0] = rslt.wasNull(11);
                ((short[]) buf[20])[0] = rslt.getShort(12) ;
                ((String[]) buf[21])[0] = rslt.getVarchar(13) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(13);
                ((String[]) buf[23])[0] = rslt.getVarchar(14) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(14);
                ((String[]) buf[25])[0] = rslt.getVarchar(15) ;
                ((bool[]) buf[26])[0] = rslt.wasNull(15);
                ((String[]) buf[27])[0] = rslt.getString(16, 1) ;
                ((bool[]) buf[28])[0] = rslt.wasNull(16);
                ((DateTime[]) buf[29])[0] = rslt.getGXDate(17) ;
                ((int[]) buf[30])[0] = rslt.getInt(18) ;
                ((bool[]) buf[31])[0] = rslt.wasNull(18);
                ((short[]) buf[32])[0] = rslt.getShort(19) ;
                ((bool[]) buf[33])[0] = rslt.wasNull(19);
                ((DateTime[]) buf[34])[0] = rslt.getGXDateTime(20) ;
                ((bool[]) buf[35])[0] = rslt.wasNull(20);
                ((bool[]) buf[36])[0] = rslt.getBool(21) ;
                ((bool[]) buf[37])[0] = rslt.wasNull(21);
                ((bool[]) buf[38])[0] = rslt.getBool(22) ;
                ((bool[]) buf[39])[0] = rslt.wasNull(22);
                ((bool[]) buf[40])[0] = rslt.getBool(23) ;
                ((bool[]) buf[41])[0] = rslt.wasNull(23);
                ((int[]) buf[42])[0] = rslt.getInt(24) ;
                ((bool[]) buf[43])[0] = rslt.wasNull(24);
                ((int[]) buf[44])[0] = rslt.getInt(25) ;
                ((bool[]) buf[45])[0] = rslt.wasNull(25);
                ((int[]) buf[46])[0] = rslt.getInt(26) ;
                ((bool[]) buf[47])[0] = rslt.wasNull(26);
                ((int[]) buf[48])[0] = rslt.getInt(27) ;
                ((bool[]) buf[49])[0] = rslt.wasNull(27);
                ((int[]) buf[50])[0] = rslt.getInt(28) ;
                ((bool[]) buf[51])[0] = rslt.wasNull(28);
                ((int[]) buf[52])[0] = rslt.getInt(29) ;
                ((bool[]) buf[53])[0] = rslt.wasNull(29);
                ((int[]) buf[54])[0] = rslt.getInt(30) ;
                ((bool[]) buf[55])[0] = rslt.wasNull(30);
                ((int[]) buf[56])[0] = rslt.getInt(31) ;
                ((bool[]) buf[57])[0] = rslt.wasNull(31);
                ((int[]) buf[58])[0] = rslt.getInt(32) ;
                ((bool[]) buf[59])[0] = rslt.wasNull(32);
                ((int[]) buf[60])[0] = rslt.getInt(33) ;
                ((bool[]) buf[61])[0] = rslt.wasNull(33);
                ((int[]) buf[62])[0] = rslt.getInt(34) ;
                ((bool[]) buf[63])[0] = rslt.wasNull(34);
                ((int[]) buf[64])[0] = rslt.getInt(35) ;
                ((bool[]) buf[65])[0] = rslt.wasNull(35);
                return;
             case 17 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 18 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 19 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 20 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 21 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 22 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 23 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 24 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 25 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 26 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 27 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 28 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 29 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
       getresults30( cursor, rslt, buf) ;
    }

    public void getresults30( int cursor ,
                              IFieldGetter rslt ,
                              Object[] buf )
    {
       switch ( cursor )
       {
             case 30 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 31 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 32 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 35 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 36 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 37 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 38 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 39 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                return;
             case 40 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 41 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 42 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 43 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 44 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 45 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 46 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 47 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 48 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDate(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
             case 49 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
             case 50 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
             case 51 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 52 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 53 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 2) ;
                return;
             case 54 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDate(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 5) ;
                return;
             case 55 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 56 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                return;
             case 57 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                return;
             case 58 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((short[]) buf[2])[0] = rslt.getShort(3) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                return;
             case 59 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 100) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                return;
       }
       getresults60( cursor, rslt, buf) ;
    }

    public void getresults60( int cursor ,
                              IFieldGetter rslt ,
                              Object[] buf )
    {
       switch ( cursor )
       {
             case 60 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((String[]) buf[3])[0] = rslt.getString(3, 100) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                return;
             case 61 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
             case 62 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 100) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                return;
             case 63 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                return;
             case 64 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                return;
             case 65 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 66 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 67 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 68 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 69 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 70 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 71 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 72 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 73 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 74 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 75 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 76 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 3 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 4 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 5 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 6 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 7 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 8 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 9 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 10 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 11 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 12 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 13 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 16 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 17 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 18 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 19 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 20 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 21 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 22 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 23 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 24 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 25 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 26 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 27 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 28 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 29 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
       setparameters30( cursor, stmt, parms) ;
    }

    public void setparameters30( int cursor ,
                                 IFieldSetter stmt ,
                                 Object[] parms )
    {
       switch ( cursor )
       {
             case 30 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 31 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 32 :
                stmt.SetParameter(1, (int)parms[0]);
                if ( (bool)parms[1] )
                {
                   stmt.setNull( 2 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(2, (String)parms[2]);
                }
                if ( (bool)parms[3] )
                {
                   stmt.setNull( 3 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(3, (String)parms[4]);
                }
                if ( (bool)parms[5] )
                {
                   stmt.setNull( 4 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameter(4, (DateTime)parms[6]);
                }
                if ( (bool)parms[7] )
                {
                   stmt.setNull( 5 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(5, (DateTime)parms[8]);
                }
                if ( (bool)parms[9] )
                {
                   stmt.setNull( 6 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(6, (DateTime)parms[10]);
                }
                if ( (bool)parms[11] )
                {
                   stmt.setNull( 7 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(7, (int)parms[12]);
                }
                if ( (bool)parms[13] )
                {
                   stmt.setNull( 8 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(8, (String)parms[14]);
                }
                if ( (bool)parms[15] )
                {
                   stmt.setNull( 9 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(9, (int)parms[16]);
                }
                if ( (bool)parms[17] )
                {
                   stmt.setNull( 10 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(10, (short)parms[18]);
                }
                stmt.SetParameter(11, (short)parms[19]);
                if ( (bool)parms[20] )
                {
                   stmt.setNull( 12 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(12, (String)parms[21]);
                }
                if ( (bool)parms[22] )
                {
                   stmt.setNull( 13 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(13, (String)parms[23]);
                }
                if ( (bool)parms[24] )
                {
                   stmt.setNull( 14 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(14, (String)parms[25]);
                }
                if ( (bool)parms[26] )
                {
                   stmt.setNull( 15 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(15, (String)parms[27]);
                }
                stmt.SetParameter(16, (DateTime)parms[28]);
                if ( (bool)parms[29] )
                {
                   stmt.setNull( 17 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(17, (int)parms[30]);
                }
                if ( (bool)parms[31] )
                {
                   stmt.setNull( 18 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(18, (short)parms[32]);
                }
                if ( (bool)parms[33] )
                {
                   stmt.setNull( 19 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(19, (DateTime)parms[34]);
                }
                if ( (bool)parms[35] )
                {
                   stmt.setNull( 20 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(20, (bool)parms[36]);
                }
                if ( (bool)parms[37] )
                {
                   stmt.setNull( 21 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(21, (bool)parms[38]);
                }
                if ( (bool)parms[39] )
                {
                   stmt.setNull( 22 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(22, (bool)parms[40]);
                }
                if ( (bool)parms[41] )
                {
                   stmt.setNull( 23 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(23, (int)parms[42]);
                }
                if ( (bool)parms[43] )
                {
                   stmt.setNull( 24 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(24, (int)parms[44]);
                }
                if ( (bool)parms[45] )
                {
                   stmt.setNull( 25 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(25, (int)parms[46]);
                }
                if ( (bool)parms[47] )
                {
                   stmt.setNull( 26 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(26, (int)parms[48]);
                }
                if ( (bool)parms[49] )
                {
                   stmt.setNull( 27 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(27, (int)parms[50]);
                }
                if ( (bool)parms[51] )
                {
                   stmt.setNull( 28 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(28, (int)parms[52]);
                }
                if ( (bool)parms[53] )
                {
                   stmt.setNull( 29 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(29, (int)parms[54]);
                }
                if ( (bool)parms[55] )
                {
                   stmt.setNull( 30 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(30, (int)parms[56]);
                }
                if ( (bool)parms[57] )
                {
                   stmt.setNull( 31 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(31, (int)parms[58]);
                }
                if ( (bool)parms[59] )
                {
                   stmt.setNull( 32 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(32, (int)parms[60]);
                }
                if ( (bool)parms[61] )
                {
                   stmt.setNull( 33 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(33, (int)parms[62]);
                }
                if ( (bool)parms[63] )
                {
                   stmt.setNull( 34 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(34, (int)parms[64]);
                }
                return;
             case 33 :
                stmt.SetParameter(1, (int)parms[0]);
                if ( (bool)parms[1] )
                {
                   stmt.setNull( 2 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(2, (String)parms[2]);
                }
                if ( (bool)parms[3] )
                {
                   stmt.setNull( 3 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(3, (String)parms[4]);
                }
                if ( (bool)parms[5] )
                {
                   stmt.setNull( 4 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameter(4, (DateTime)parms[6]);
                }
                if ( (bool)parms[7] )
                {
                   stmt.setNull( 5 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(5, (DateTime)parms[8]);
                }
                if ( (bool)parms[9] )
                {
                   stmt.setNull( 6 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(6, (DateTime)parms[10]);
                }
                if ( (bool)parms[11] )
                {
                   stmt.setNull( 7 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(7, (int)parms[12]);
                }
                if ( (bool)parms[13] )
                {
                   stmt.setNull( 8 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(8, (String)parms[14]);
                }
                if ( (bool)parms[15] )
                {
                   stmt.setNull( 9 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(9, (int)parms[16]);
                }
                if ( (bool)parms[17] )
                {
                   stmt.setNull( 10 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(10, (short)parms[18]);
                }
                stmt.SetParameter(11, (short)parms[19]);
                if ( (bool)parms[20] )
                {
                   stmt.setNull( 12 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(12, (String)parms[21]);
                }
                if ( (bool)parms[22] )
                {
                   stmt.setNull( 13 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(13, (String)parms[23]);
                }
                if ( (bool)parms[24] )
                {
                   stmt.setNull( 14 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(14, (String)parms[25]);
                }
                if ( (bool)parms[26] )
                {
                   stmt.setNull( 15 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(15, (String)parms[27]);
                }
                stmt.SetParameter(16, (DateTime)parms[28]);
                if ( (bool)parms[29] )
                {
                   stmt.setNull( 17 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(17, (int)parms[30]);
                }
                if ( (bool)parms[31] )
                {
                   stmt.setNull( 18 , SqlDbType.SmallInt );
                }
                else
                {
                   stmt.SetParameter(18, (short)parms[32]);
                }
                if ( (bool)parms[33] )
                {
                   stmt.setNull( 19 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(19, (DateTime)parms[34]);
                }
                if ( (bool)parms[35] )
                {
                   stmt.setNull( 20 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(20, (bool)parms[36]);
                }
                if ( (bool)parms[37] )
                {
                   stmt.setNull( 21 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(21, (bool)parms[38]);
                }
                if ( (bool)parms[39] )
                {
                   stmt.setNull( 22 , SqlDbType.Bit );
                }
                else
                {
                   stmt.SetParameter(22, (bool)parms[40]);
                }
                if ( (bool)parms[41] )
                {
                   stmt.setNull( 23 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(23, (int)parms[42]);
                }
                if ( (bool)parms[43] )
                {
                   stmt.setNull( 24 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(24, (int)parms[44]);
                }
                if ( (bool)parms[45] )
                {
                   stmt.setNull( 25 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(25, (int)parms[46]);
                }
                if ( (bool)parms[47] )
                {
                   stmt.setNull( 26 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(26, (int)parms[48]);
                }
                if ( (bool)parms[49] )
                {
                   stmt.setNull( 27 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(27, (int)parms[50]);
                }
                if ( (bool)parms[51] )
                {
                   stmt.setNull( 28 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(28, (int)parms[52]);
                }
                if ( (bool)parms[53] )
                {
                   stmt.setNull( 29 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(29, (int)parms[54]);
                }
                if ( (bool)parms[55] )
                {
                   stmt.setNull( 30 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(30, (int)parms[56]);
                }
                if ( (bool)parms[57] )
                {
                   stmt.setNull( 31 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(31, (int)parms[58]);
                }
                if ( (bool)parms[59] )
                {
                   stmt.setNull( 32 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(32, (int)parms[60]);
                }
                if ( (bool)parms[61] )
                {
                   stmt.setNull( 33 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(33, (int)parms[62]);
                }
                if ( (bool)parms[63] )
                {
                   stmt.setNull( 34 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(34, (int)parms[64]);
                }
                stmt.SetParameter(35, (int)parms[65]);
                return;
             case 34 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 35 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 36 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 37 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 38 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 39 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 40 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 41 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 42 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 43 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 44 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 45 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 46 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 47 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 48 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 49 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 50 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 51 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 52 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 53 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 54 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 58 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 59 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 61 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 62 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 65 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 66 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 67 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 68 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 69 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 70 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 71 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 72 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 73 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 74 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 75 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 76 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
       }
    }

 }

}
