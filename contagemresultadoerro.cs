/*
               File: ContagemResultadoErro
        Description: Erros nas confer�ncias de contagens
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/2/2020 16:17:14.24
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class contagemresultadoerro : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_7") == 0 )
         {
            A456ContagemResultado_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A456ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A456ContagemResultado_Codigo), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_7( A456ContagemResultado_Codigo) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_8") == 0 )
         {
            A583ContagemResultadoErro_ContadorFMCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A583ContagemResultadoErro_ContadorFMCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A583ContagemResultadoErro_ContadorFMCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_8( A583ContagemResultadoErro_ContadorFMCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         cmbContagemResultadoErro_Tipo.Name = "CONTAGEMRESULTADOERRO_TIPO";
         cmbContagemResultadoErro_Tipo.WebTags = "";
         cmbContagemResultadoErro_Tipo.addItem("DE", "Descri��o", 0);
         cmbContagemResultadoErro_Tipo.addItem("DA", "Data", 0);
         cmbContagemResultadoErro_Tipo.addItem("SC", "Coordena��o", 0);
         cmbContagemResultadoErro_Tipo.addItem("SI", "Sistema", 0);
         cmbContagemResultadoErro_Tipo.addItem("PF", "Contagem", 0);
         cmbContagemResultadoErro_Tipo.addItem("CO", "Contratada", 0);
         if ( cmbContagemResultadoErro_Tipo.ItemCount > 0 )
         {
            A579ContagemResultadoErro_Tipo = cmbContagemResultadoErro_Tipo.getValidValue(A579ContagemResultadoErro_Tipo);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A579ContagemResultadoErro_Tipo", A579ContagemResultadoErro_Tipo);
         }
         cmbContagemResultadoErro_Status.Name = "CONTAGEMRESULTADOERRO_STATUS";
         cmbContagemResultadoErro_Status.WebTags = "";
         cmbContagemResultadoErro_Status.addItem("C", "Corrigido", 0);
         cmbContagemResultadoErro_Status.addItem("R", "Ratificado", 0);
         cmbContagemResultadoErro_Status.addItem("P", "Pr�prio", 0);
         cmbContagemResultadoErro_Status.addItem("E", "Externo", 0);
         if ( cmbContagemResultadoErro_Status.ItemCount > 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( A581ContagemResultadoErro_Status)) )
            {
               A581ContagemResultadoErro_Status = "P";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A581ContagemResultadoErro_Status", A581ContagemResultadoErro_Status);
            }
            A581ContagemResultadoErro_Status = cmbContagemResultadoErro_Status.getValidValue(A581ContagemResultadoErro_Status);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A581ContagemResultadoErro_Status", A581ContagemResultadoErro_Status);
         }
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Erros nas confer�ncias de contagens", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = edtContagemResultado_Codigo_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public contagemresultadoerro( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public contagemresultadoerro( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbContagemResultadoErro_Tipo = new GXCombobox();
         cmbContagemResultadoErro_Status = new GXCombobox();
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbContagemResultadoErro_Tipo.ItemCount > 0 )
         {
            A579ContagemResultadoErro_Tipo = cmbContagemResultadoErro_Tipo.getValidValue(A579ContagemResultadoErro_Tipo);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A579ContagemResultadoErro_Tipo", A579ContagemResultadoErro_Tipo);
         }
         if ( cmbContagemResultadoErro_Status.ItemCount > 0 )
         {
            A581ContagemResultadoErro_Status = cmbContagemResultadoErro_Status.getValidValue(A581ContagemResultadoErro_Status);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A581ContagemResultadoErro_Status", A581ContagemResultadoErro_Status);
         }
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            wb_table1_2_1Y75( true) ;
         }
         return  ;
      }

      protected void wb_table1_2_1Y75e( bool wbgen )
      {
         if ( wbgen )
         {
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void wb_table1_2_1Y75( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableBorder100x100", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_5_1Y75( true) ;
         }
         return  ;
      }

      protected void wb_table2_5_1Y75e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Control Group */
            GxWebStd.gx_group_start( context, grpGroupdata_Internalname, "Erros nas confer�ncias de contagens", 1, 0, "px", 0, "px", "Group", " "+"style=\"-moz-border-radius: 3pt;;\""+" ", "HLP_ContagemResultadoErro.htm");
            wb_table3_28_1Y75( true) ;
         }
         return  ;
      }

      protected void wb_table3_28_1Y75e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</fieldset>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_1Y75e( true) ;
         }
         else
         {
            wb_table1_2_1Y75e( false) ;
         }
      }

      protected void wb_table3_28_1Y75( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable1_Internalname, tblTable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_34_1Y75( true) ;
         }
         return  ;
      }

      protected void wb_table4_34_1Y75e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 62,'',false,'',0)\"";
            ClassString = "BtnEnter";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_enter_Internalname, "", "Confirmar", bttBtn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_enter_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContagemResultadoErro.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 63,'',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_cancel_Internalname, "", "Fechar", bttBtn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, bttBtn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContagemResultadoErro.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 64,'',false,'',0)\"";
            ClassString = "BtnDelete";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_delete_Internalname, "", "Eliminar", bttBtn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_delete_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContagemResultadoErro.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_28_1Y75e( true) ;
         }
         else
         {
            wb_table3_28_1Y75e( false) ;
         }
      }

      protected void wb_table4_34_1Y75( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable2_Internalname, tblTable2_Internalname, "", "Container", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultado_codigo_Internalname, "Contagem Resultado_Codigo", "", "", lblTextblockcontagemresultado_codigo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ContagemResultadoErro.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 39,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContagemResultado_Codigo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A456ContagemResultado_Codigo), 6, 0, ",", "")), ((edtContagemResultado_Codigo_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A456ContagemResultado_Codigo), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A456ContagemResultado_Codigo), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,39);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultado_Codigo_Jsonclick, 0, "Attribute", "", "", "", 1, edtContagemResultado_Codigo_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContagemResultadoErro.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultadoerro_tipo_Internalname, "Resultado Erro_Tipo", "", "", lblTextblockcontagemresultadoerro_tipo_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ContagemResultadoErro.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 44,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbContagemResultadoErro_Tipo, cmbContagemResultadoErro_Tipo_Internalname, StringUtil.RTrim( A579ContagemResultadoErro_Tipo), 1, cmbContagemResultadoErro_Tipo_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", 1, cmbContagemResultadoErro_Tipo.Enabled, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,44);\"", "", true, "HLP_ContagemResultadoErro.htm");
            cmbContagemResultadoErro_Tipo.CurrentValue = StringUtil.RTrim( A579ContagemResultadoErro_Tipo);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContagemResultadoErro_Tipo_Internalname, "Values", (String)(cmbContagemResultadoErro_Tipo.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultadoerro_data_Internalname, "da confer�ncia", "", "", lblTextblockcontagemresultadoerro_data_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ContagemResultadoErro.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 49,'',false,'',0)\"";
            context.WriteHtmlText( "<div id=\""+edtContagemResultadoErro_Data_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtContagemResultadoErro_Data_Internalname, context.localUtil.TToC( A580ContagemResultadoErro_Data, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( A580ContagemResultadoErro_Data, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'por',false,0);"+";gx.evt.onblur(this,49);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultadoErro_Data_Jsonclick, 0, "Attribute", "", "", "", 1, edtContagemResultadoErro_Data_Enabled, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ContagemResultadoErro.htm");
            GxWebStd.gx_bitmap( context, edtContagemResultadoErro_Data_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(edtContagemResultadoErro_Data_Enabled==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", 1, false, false, "HLP_ContagemResultadoErro.htm");
            context.WriteHtmlTextNl( "</div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultadoerro_status_Internalname, "do Erro", "", "", lblTextblockcontagemresultadoerro_status_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ContagemResultadoErro.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 54,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbContagemResultadoErro_Status, cmbContagemResultadoErro_Status_Internalname, StringUtil.RTrim( A581ContagemResultadoErro_Status), 1, cmbContagemResultadoErro_Status_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", 1, cmbContagemResultadoErro_Status.Enabled, 1, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,54);\"", "", true, "HLP_ContagemResultadoErro.htm");
            cmbContagemResultadoErro_Status.CurrentValue = StringUtil.RTrim( A581ContagemResultadoErro_Status);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContagemResultadoErro_Status_Internalname, "Values", (String)(cmbContagemResultadoErro_Status.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top;width:40px;WHITE-SPACE: nowrap;;")+"\" class='td5'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontagemresultadoerro_contadorfmcod_Internalname, "FM", "", "", lblTextblockcontagemresultadoerro_contadorfmcod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_ContagemResultadoErro.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "WHITE-SPACE: nowrap;")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 59,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContagemResultadoErro_ContadorFMCod_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A583ContagemResultadoErro_ContadorFMCod), 6, 0, ",", "")), ((edtContagemResultadoErro_ContadorFMCod_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A583ContagemResultadoErro_ContadorFMCod), "ZZZZZ9")) : context.localUtil.Format( (decimal)(A583ContagemResultadoErro_ContadorFMCod), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,59);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContagemResultadoErro_ContadorFMCod_Jsonclick, 0, "Attribute", "", "", "", 1, edtContagemResultadoErro_ContadorFMCod_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "Codigo", "right", false, "HLP_ContagemResultadoErro.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_34_1Y75e( true) ;
         }
         else
         {
            wb_table4_34_1Y75e( false) ;
         }
      }

      protected void wb_table2_5_1Y75( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabletoolbar_Internalname, tblTabletoolbar_Internalname, "", "ViewTable", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divSectiontoolbar_Internalname, 1, 0, "px", 0, "px", "ToolbarMain", "left", "top", "", "WHITE-SPACE: nowrap;", "div");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 9,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_first_Internalname, context.GetImagePath( "b9e06284-17ac-4c88-8937-5dbd84ad5d80", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_first_Visible, 1, "", "Primeiro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_first_Jsonclick, "'"+""+"'"+",false,"+"'"+"EFIRST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoErro.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 10,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_first_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_first_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_first_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EFIRST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoErro.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 11,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_previous_Internalname, context.GetImagePath( "7d212604-db7b-4785-9c0d-5faffe71aa33", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_previous_Visible, 1, "", "Anterior", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_previous_Jsonclick, "'"+""+"'"+",false,"+"'"+"EPREVIOUS."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoErro.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 12,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_previous_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_previous_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_previous_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EPREVIOUS."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoErro.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 13,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_next_Internalname, context.GetImagePath( "1ae947cf-1354-41a9-8d59-d91daebf554f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_next_Visible, 1, "", "Pr�ximo", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_next_Jsonclick, "'"+""+"'"+",false,"+"'"+"ENEXT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoErro.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 14,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_next_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_next_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_next_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ENEXT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoErro.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 15,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_last_Internalname, context.GetImagePath( "29211874-e613-48e5-9011-8017d984217e", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_last_Visible, 1, "", "�ltimo", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_last_Jsonclick, "'"+""+"'"+",false,"+"'"+"ELAST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoErro.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 16,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_last_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_last_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_last_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ELAST."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoErro.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 17,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_select_Internalname, context.GetImagePath( "1ca03f75-9947-4d2c-90a4-e8ab9c5cedea", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_select_Visible, 1, "", "Selecionar", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_select_Jsonclick, "'"+""+"'"+",false,"+"'"+"ESELECT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoErro.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_select_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_select_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_select_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ESELECT."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoErro.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 19,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_enter2_Internalname, context.GetImagePath( "2061cf2c-bd33-4433-a13e-34af954142e9", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_enter2_Visible, imgBtn_enter2_Enabled, "", "Confirmar", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_enter2_Jsonclick, "'"+""+"'"+",false,"+"'"+"EENTER."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoErro.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 20,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_enter2_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_enter2_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_enter2_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EENTER."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoErro.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_cancel2_Internalname, context.GetImagePath( "0e94ced8-bc34-47ff-9a53-bc683736a686", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_cancel2_Visible, 1, "", "Fechar", 0, 0, 0, "px", 0, "px", 0, 0, 1, imgBtn_cancel2_Jsonclick, "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoErro.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 22,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_cancel2_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_cancel2_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 1, imgBtn_cancel2_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoErro.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'',false,'',0)\"";
            ClassString = "ImageHandCenter";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_delete2_Internalname, context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_delete2_Visible, imgBtn_delete2_Enabled, "", "Eliminar", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_delete2_Jsonclick, "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoErro.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 24,'',false,'',0)\"";
            ClassString = "ImageTop";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgBtn_delete2_separator_Internalname, context.GetImagePath( "ea7811ba-4fa2-42da-8591-e6cb10669f1f", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgBtn_delete2_separator_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgBtn_delete2_separator_Jsonclick, "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_ContagemResultadoErro.htm");
            GxWebStd.gx_div_end( context, "left", "top");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_1Y75e( true) ;
         }
         else
         {
            wb_table2_5_1Y75e( false) ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               if ( ( ( context.localUtil.CToN( cgiGet( edtContagemResultado_Codigo_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContagemResultado_Codigo_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTAGEMRESULTADO_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtContagemResultado_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A456ContagemResultado_Codigo = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A456ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A456ContagemResultado_Codigo), 6, 0)));
               }
               else
               {
                  A456ContagemResultado_Codigo = (int)(context.localUtil.CToN( cgiGet( edtContagemResultado_Codigo_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A456ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A456ContagemResultado_Codigo), 6, 0)));
               }
               cmbContagemResultadoErro_Tipo.CurrentValue = cgiGet( cmbContagemResultadoErro_Tipo_Internalname);
               A579ContagemResultadoErro_Tipo = cgiGet( cmbContagemResultadoErro_Tipo_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A579ContagemResultadoErro_Tipo", A579ContagemResultadoErro_Tipo);
               if ( context.localUtil.VCDateTime( cgiGet( edtContagemResultadoErro_Data_Internalname), 2, 0) == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"Data da confer�ncia"}), 1, "CONTAGEMRESULTADOERRO_DATA");
                  AnyError = 1;
                  GX_FocusControl = edtContagemResultadoErro_Data_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A580ContagemResultadoErro_Data = (DateTime)(DateTime.MinValue);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A580ContagemResultadoErro_Data", context.localUtil.TToC( A580ContagemResultadoErro_Data, 8, 5, 0, 3, "/", ":", " "));
               }
               else
               {
                  A580ContagemResultadoErro_Data = context.localUtil.CToT( cgiGet( edtContagemResultadoErro_Data_Internalname));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A580ContagemResultadoErro_Data", context.localUtil.TToC( A580ContagemResultadoErro_Data, 8, 5, 0, 3, "/", ":", " "));
               }
               cmbContagemResultadoErro_Status.CurrentValue = cgiGet( cmbContagemResultadoErro_Status_Internalname);
               A581ContagemResultadoErro_Status = cgiGet( cmbContagemResultadoErro_Status_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A581ContagemResultadoErro_Status", A581ContagemResultadoErro_Status);
               if ( ( ( context.localUtil.CToN( cgiGet( edtContagemResultadoErro_ContadorFMCod_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContagemResultadoErro_ContadorFMCod_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTAGEMRESULTADOERRO_CONTADORFMCOD");
                  AnyError = 1;
                  GX_FocusControl = edtContagemResultadoErro_ContadorFMCod_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A583ContagemResultadoErro_ContadorFMCod = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A583ContagemResultadoErro_ContadorFMCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A583ContagemResultadoErro_ContadorFMCod), 6, 0)));
               }
               else
               {
                  A583ContagemResultadoErro_ContadorFMCod = (int)(context.localUtil.CToN( cgiGet( edtContagemResultadoErro_ContadorFMCod_Internalname), ",", "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A583ContagemResultadoErro_ContadorFMCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A583ContagemResultadoErro_ContadorFMCod), 6, 0)));
               }
               /* Read saved values. */
               Z456ContagemResultado_Codigo = (int)(context.localUtil.CToN( cgiGet( "Z456ContagemResultado_Codigo"), ",", "."));
               Z579ContagemResultadoErro_Tipo = cgiGet( "Z579ContagemResultadoErro_Tipo");
               Z580ContagemResultadoErro_Data = context.localUtil.CToT( cgiGet( "Z580ContagemResultadoErro_Data"), 0);
               Z581ContagemResultadoErro_Status = cgiGet( "Z581ContagemResultadoErro_Status");
               Z583ContagemResultadoErro_ContadorFMCod = (int)(context.localUtil.CToN( cgiGet( "Z583ContagemResultadoErro_ContadorFMCod"), ",", "."));
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
               Gx_mode = cgiGet( "Mode");
               Gx_BScreen = (short)(context.localUtil.CToN( cgiGet( "vGXBSCREEN"), ",", "."));
               Gx_mode = cgiGet( "vMODE");
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  A456ContagemResultado_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A456ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A456ContagemResultado_Codigo), 6, 0)));
                  A579ContagemResultadoErro_Tipo = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A579ContagemResultadoErro_Tipo", A579ContagemResultadoErro_Tipo);
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  disable_std_buttons_dsp( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  Gx_mode = "INS";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  standaloneModal( ) ;
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_enter( ) ;
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                        else if ( StringUtil.StrCmp(sEvt, "FIRST") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_first( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "PREVIOUS") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_previous( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "NEXT") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_next( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LAST") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_last( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "SELECT") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_select( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DELETE") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_delete( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                        {
                           context.wbHandled = 1;
                           AfterKeyLoadScreen( ) ;
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll1Y75( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            imgBtn_delete2_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Enabled), 5, 0)));
         }
      }

      protected void disable_std_buttons_dsp( )
      {
         imgBtn_delete2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Visible), 5, 0)));
         imgBtn_delete2_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_separator_Visible), 5, 0)));
         bttBtn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Visible), 5, 0)));
         imgBtn_first_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_first_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_first_Visible), 5, 0)));
         imgBtn_first_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_first_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_first_separator_Visible), 5, 0)));
         imgBtn_previous_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_previous_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_previous_Visible), 5, 0)));
         imgBtn_previous_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_previous_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_previous_separator_Visible), 5, 0)));
         imgBtn_next_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_next_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_next_Visible), 5, 0)));
         imgBtn_next_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_next_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_next_separator_Visible), 5, 0)));
         imgBtn_last_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_last_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_last_Visible), 5, 0)));
         imgBtn_last_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_last_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_last_separator_Visible), 5, 0)));
         imgBtn_select_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_select_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_select_Visible), 5, 0)));
         imgBtn_select_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_select_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_select_separator_Visible), 5, 0)));
         imgBtn_delete2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Visible), 5, 0)));
         imgBtn_delete2_separator_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_separator_Visible), 5, 0)));
         bttBtn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Visible), 5, 0)));
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            imgBtn_enter2_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_Visible), 5, 0)));
            imgBtn_enter2_separator_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_separator_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_separator_Visible), 5, 0)));
            bttBtn_enter_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_enter_Visible), 5, 0)));
         }
         DisableAttributes1Y75( ) ;
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void ResetCaption1Y0( )
      {
      }

      protected void ZM1Y75( short GX_JID )
      {
         if ( ( GX_JID == 6 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z580ContagemResultadoErro_Data = T001Y3_A580ContagemResultadoErro_Data[0];
               Z581ContagemResultadoErro_Status = T001Y3_A581ContagemResultadoErro_Status[0];
               Z583ContagemResultadoErro_ContadorFMCod = T001Y3_A583ContagemResultadoErro_ContadorFMCod[0];
            }
            else
            {
               Z580ContagemResultadoErro_Data = A580ContagemResultadoErro_Data;
               Z581ContagemResultadoErro_Status = A581ContagemResultadoErro_Status;
               Z583ContagemResultadoErro_ContadorFMCod = A583ContagemResultadoErro_ContadorFMCod;
            }
         }
         if ( GX_JID == -6 )
         {
            Z579ContagemResultadoErro_Tipo = A579ContagemResultadoErro_Tipo;
            Z580ContagemResultadoErro_Data = A580ContagemResultadoErro_Data;
            Z581ContagemResultadoErro_Status = A581ContagemResultadoErro_Status;
            Z456ContagemResultado_Codigo = A456ContagemResultado_Codigo;
            Z583ContagemResultadoErro_ContadorFMCod = A583ContagemResultadoErro_ContadorFMCod;
         }
      }

      protected void standaloneNotModal( )
      {
         Gx_BScreen = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_BScreen", StringUtil.Str( (decimal)(Gx_BScreen), 1, 0));
      }

      protected void standaloneModal( )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && String.IsNullOrEmpty(StringUtil.RTrim( A581ContagemResultadoErro_Status)) && ( Gx_BScreen == 0 ) )
         {
            A581ContagemResultadoErro_Status = "P";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A581ContagemResultadoErro_Status", A581ContagemResultadoErro_Status);
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (DateTime.MinValue==A580ContagemResultadoErro_Data) && ( Gx_BScreen == 0 ) )
         {
            A580ContagemResultadoErro_Data = DateTimeUtil.ServerNow( context, "DEFAULT");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A580ContagemResultadoErro_Data", context.localUtil.TToC( A580ContagemResultadoErro_Data, 8, 5, 0, 3, "/", ":", " "));
         }
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            imgBtn_delete2_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Enabled), 5, 0)));
         }
         else
         {
            imgBtn_delete2_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_delete2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_delete2_Enabled), 5, 0)));
         }
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            imgBtn_enter2_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_Enabled), 5, 0)));
         }
         else
         {
            imgBtn_enter2_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgBtn_enter2_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgBtn_enter2_Enabled), 5, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ( Gx_BScreen == 0 ) )
         {
         }
      }

      protected void Load1Y75( )
      {
         /* Using cursor T001Y6 */
         pr_default.execute(4, new Object[] {A456ContagemResultado_Codigo, A579ContagemResultadoErro_Tipo});
         if ( (pr_default.getStatus(4) != 101) )
         {
            RcdFound75 = 1;
            A580ContagemResultadoErro_Data = T001Y6_A580ContagemResultadoErro_Data[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A580ContagemResultadoErro_Data", context.localUtil.TToC( A580ContagemResultadoErro_Data, 8, 5, 0, 3, "/", ":", " "));
            A581ContagemResultadoErro_Status = T001Y6_A581ContagemResultadoErro_Status[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A581ContagemResultadoErro_Status", A581ContagemResultadoErro_Status);
            A583ContagemResultadoErro_ContadorFMCod = T001Y6_A583ContagemResultadoErro_ContadorFMCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A583ContagemResultadoErro_ContadorFMCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A583ContagemResultadoErro_ContadorFMCod), 6, 0)));
            ZM1Y75( -6) ;
         }
         pr_default.close(4);
         OnLoadActions1Y75( ) ;
      }

      protected void OnLoadActions1Y75( )
      {
      }

      protected void CheckExtendedTable1Y75( )
      {
         Gx_BScreen = 1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_BScreen", StringUtil.Str( (decimal)(Gx_BScreen), 1, 0));
         standaloneModal( ) ;
         /* Using cursor T001Y4 */
         pr_default.execute(2, new Object[] {A456ContagemResultado_Codigo});
         if ( (pr_default.getStatus(2) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Resultado das Contagens'.", "ForeignKeyNotFound", 1, "CONTAGEMRESULTADO_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtContagemResultado_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         pr_default.close(2);
         if ( ! ( ( StringUtil.StrCmp(A579ContagemResultadoErro_Tipo, "DE") == 0 ) || ( StringUtil.StrCmp(A579ContagemResultadoErro_Tipo, "DA") == 0 ) || ( StringUtil.StrCmp(A579ContagemResultadoErro_Tipo, "SC") == 0 ) || ( StringUtil.StrCmp(A579ContagemResultadoErro_Tipo, "SI") == 0 ) || ( StringUtil.StrCmp(A579ContagemResultadoErro_Tipo, "PF") == 0 ) || ( StringUtil.StrCmp(A579ContagemResultadoErro_Tipo, "CO") == 0 ) ) )
         {
            GX_msglist.addItem("Campo Contagem Resultado Erro_Tipo fora do intervalo", "OutOfRange", 1, "CONTAGEMRESULTADOERRO_TIPO");
            AnyError = 1;
            GX_FocusControl = cmbContagemResultadoErro_Tipo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( ! ( (DateTime.MinValue==A580ContagemResultadoErro_Data) || ( A580ContagemResultadoErro_Data >= context.localUtil.YMDHMSToT( 1753, 1, 1, 0, 0, 0) ) ) )
         {
            GX_msglist.addItem("Campo Data da confer�ncia fora do intervalo", "OutOfRange", 1, "CONTAGEMRESULTADOERRO_DATA");
            AnyError = 1;
            GX_FocusControl = edtContagemResultadoErro_Data_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( ! ( ( StringUtil.StrCmp(A581ContagemResultadoErro_Status, "C") == 0 ) || ( StringUtil.StrCmp(A581ContagemResultadoErro_Status, "R") == 0 ) || ( StringUtil.StrCmp(A581ContagemResultadoErro_Status, "P") == 0 ) || ( StringUtil.StrCmp(A581ContagemResultadoErro_Status, "E") == 0 ) ) )
         {
            GX_msglist.addItem("Campo Status do Erro fora do intervalo", "OutOfRange", 1, "CONTAGEMRESULTADOERRO_STATUS");
            AnyError = 1;
            GX_FocusControl = cmbContagemResultadoErro_Status_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         /* Using cursor T001Y5 */
         pr_default.execute(3, new Object[] {A583ContagemResultadoErro_ContadorFMCod});
         if ( (pr_default.getStatus(3) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contagem Resultado Erro_ContadorFM'.", "ForeignKeyNotFound", 1, "CONTAGEMRESULTADOERRO_CONTADORFMCOD");
            AnyError = 1;
            GX_FocusControl = edtContagemResultadoErro_ContadorFMCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         pr_default.close(3);
      }

      protected void CloseExtendedTableCursors1Y75( )
      {
         pr_default.close(2);
         pr_default.close(3);
      }

      protected void enableDisable( )
      {
      }

      protected void gxLoad_7( int A456ContagemResultado_Codigo )
      {
         /* Using cursor T001Y7 */
         pr_default.execute(5, new Object[] {A456ContagemResultado_Codigo});
         if ( (pr_default.getStatus(5) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Resultado das Contagens'.", "ForeignKeyNotFound", 1, "CONTAGEMRESULTADO_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtContagemResultado_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(5) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(5);
      }

      protected void gxLoad_8( int A583ContagemResultadoErro_ContadorFMCod )
      {
         /* Using cursor T001Y8 */
         pr_default.execute(6, new Object[] {A583ContagemResultadoErro_ContadorFMCod});
         if ( (pr_default.getStatus(6) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contagem Resultado Erro_ContadorFM'.", "ForeignKeyNotFound", 1, "CONTAGEMRESULTADOERRO_CONTADORFMCOD");
            AnyError = 1;
            GX_FocusControl = edtContagemResultadoErro_ContadorFMCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(6) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(6);
      }

      protected void GetKey1Y75( )
      {
         /* Using cursor T001Y9 */
         pr_default.execute(7, new Object[] {A456ContagemResultado_Codigo, A579ContagemResultadoErro_Tipo});
         if ( (pr_default.getStatus(7) != 101) )
         {
            RcdFound75 = 1;
         }
         else
         {
            RcdFound75 = 0;
         }
         pr_default.close(7);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T001Y3 */
         pr_default.execute(1, new Object[] {A456ContagemResultado_Codigo, A579ContagemResultadoErro_Tipo});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM1Y75( 6) ;
            RcdFound75 = 1;
            A579ContagemResultadoErro_Tipo = T001Y3_A579ContagemResultadoErro_Tipo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A579ContagemResultadoErro_Tipo", A579ContagemResultadoErro_Tipo);
            A580ContagemResultadoErro_Data = T001Y3_A580ContagemResultadoErro_Data[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A580ContagemResultadoErro_Data", context.localUtil.TToC( A580ContagemResultadoErro_Data, 8, 5, 0, 3, "/", ":", " "));
            A581ContagemResultadoErro_Status = T001Y3_A581ContagemResultadoErro_Status[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A581ContagemResultadoErro_Status", A581ContagemResultadoErro_Status);
            A456ContagemResultado_Codigo = T001Y3_A456ContagemResultado_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A456ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A456ContagemResultado_Codigo), 6, 0)));
            A583ContagemResultadoErro_ContadorFMCod = T001Y3_A583ContagemResultadoErro_ContadorFMCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A583ContagemResultadoErro_ContadorFMCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A583ContagemResultadoErro_ContadorFMCod), 6, 0)));
            Z456ContagemResultado_Codigo = A456ContagemResultado_Codigo;
            Z579ContagemResultadoErro_Tipo = A579ContagemResultadoErro_Tipo;
            sMode75 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            standaloneModal( ) ;
            Load1Y75( ) ;
            if ( AnyError == 1 )
            {
               RcdFound75 = 0;
               InitializeNonKey1Y75( ) ;
            }
            Gx_mode = sMode75;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            RcdFound75 = 0;
            InitializeNonKey1Y75( ) ;
            sMode75 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            standaloneModal( ) ;
            Gx_mode = sMode75;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey1Y75( ) ;
         if ( RcdFound75 == 0 )
         {
            Gx_mode = "INS";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound75 = 0;
         /* Using cursor T001Y10 */
         pr_default.execute(8, new Object[] {A456ContagemResultado_Codigo, A579ContagemResultadoErro_Tipo});
         if ( (pr_default.getStatus(8) != 101) )
         {
            while ( (pr_default.getStatus(8) != 101) && ( ( T001Y10_A456ContagemResultado_Codigo[0] < A456ContagemResultado_Codigo ) || ( T001Y10_A456ContagemResultado_Codigo[0] == A456ContagemResultado_Codigo ) && ( StringUtil.StrCmp(T001Y10_A579ContagemResultadoErro_Tipo[0], A579ContagemResultadoErro_Tipo) < 0 ) ) )
            {
               pr_default.readNext(8);
            }
            if ( (pr_default.getStatus(8) != 101) && ( ( T001Y10_A456ContagemResultado_Codigo[0] > A456ContagemResultado_Codigo ) || ( T001Y10_A456ContagemResultado_Codigo[0] == A456ContagemResultado_Codigo ) && ( StringUtil.StrCmp(T001Y10_A579ContagemResultadoErro_Tipo[0], A579ContagemResultadoErro_Tipo) > 0 ) ) )
            {
               A456ContagemResultado_Codigo = T001Y10_A456ContagemResultado_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A456ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A456ContagemResultado_Codigo), 6, 0)));
               A579ContagemResultadoErro_Tipo = T001Y10_A579ContagemResultadoErro_Tipo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A579ContagemResultadoErro_Tipo", A579ContagemResultadoErro_Tipo);
               RcdFound75 = 1;
            }
         }
         pr_default.close(8);
      }

      protected void move_previous( )
      {
         RcdFound75 = 0;
         /* Using cursor T001Y11 */
         pr_default.execute(9, new Object[] {A456ContagemResultado_Codigo, A579ContagemResultadoErro_Tipo});
         if ( (pr_default.getStatus(9) != 101) )
         {
            while ( (pr_default.getStatus(9) != 101) && ( ( T001Y11_A456ContagemResultado_Codigo[0] > A456ContagemResultado_Codigo ) || ( T001Y11_A456ContagemResultado_Codigo[0] == A456ContagemResultado_Codigo ) && ( StringUtil.StrCmp(T001Y11_A579ContagemResultadoErro_Tipo[0], A579ContagemResultadoErro_Tipo) > 0 ) ) )
            {
               pr_default.readNext(9);
            }
            if ( (pr_default.getStatus(9) != 101) && ( ( T001Y11_A456ContagemResultado_Codigo[0] < A456ContagemResultado_Codigo ) || ( T001Y11_A456ContagemResultado_Codigo[0] == A456ContagemResultado_Codigo ) && ( StringUtil.StrCmp(T001Y11_A579ContagemResultadoErro_Tipo[0], A579ContagemResultadoErro_Tipo) < 0 ) ) )
            {
               A456ContagemResultado_Codigo = T001Y11_A456ContagemResultado_Codigo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A456ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A456ContagemResultado_Codigo), 6, 0)));
               A579ContagemResultadoErro_Tipo = T001Y11_A579ContagemResultadoErro_Tipo[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A579ContagemResultadoErro_Tipo", A579ContagemResultadoErro_Tipo);
               RcdFound75 = 1;
            }
         }
         pr_default.close(9);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey1Y75( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = edtContagemResultado_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert1Y75( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound75 == 1 )
            {
               if ( ( A456ContagemResultado_Codigo != Z456ContagemResultado_Codigo ) || ( StringUtil.StrCmp(A579ContagemResultadoErro_Tipo, Z579ContagemResultadoErro_Tipo) != 0 ) )
               {
                  A456ContagemResultado_Codigo = Z456ContagemResultado_Codigo;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A456ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A456ContagemResultado_Codigo), 6, 0)));
                  A579ContagemResultadoErro_Tipo = Z579ContagemResultadoErro_Tipo;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A579ContagemResultadoErro_Tipo", A579ContagemResultadoErro_Tipo);
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "CONTAGEMRESULTADO_CODIGO");
                  AnyError = 1;
                  GX_FocusControl = edtContagemResultado_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = edtContagemResultado_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  Gx_mode = "UPD";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  /* Update record */
                  Update1Y75( ) ;
                  GX_FocusControl = edtContagemResultado_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( ( A456ContagemResultado_Codigo != Z456ContagemResultado_Codigo ) || ( StringUtil.StrCmp(A579ContagemResultadoErro_Tipo, Z579ContagemResultadoErro_Tipo) != 0 ) )
               {
                  Gx_mode = "INS";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  /* Insert record */
                  GX_FocusControl = edtContagemResultado_Codigo_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert1Y75( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "CONTAGEMRESULTADO_CODIGO");
                     AnyError = 1;
                     GX_FocusControl = edtContagemResultado_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     Gx_mode = "INS";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     /* Insert record */
                     GX_FocusControl = edtContagemResultado_Codigo_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert1Y75( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
      }

      protected void btn_delete( )
      {
         if ( ( A456ContagemResultado_Codigo != Z456ContagemResultado_Codigo ) || ( StringUtil.StrCmp(A579ContagemResultadoErro_Tipo, Z579ContagemResultadoErro_Tipo) != 0 ) )
         {
            A456ContagemResultado_Codigo = Z456ContagemResultado_Codigo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A456ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A456ContagemResultado_Codigo), 6, 0)));
            A579ContagemResultadoErro_Tipo = Z579ContagemResultadoErro_Tipo;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A579ContagemResultadoErro_Tipo", A579ContagemResultadoErro_Tipo);
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "CONTAGEMRESULTADO_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtContagemResultado_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = edtContagemResultado_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            getByPrimaryKey( ) ;
         }
         CloseOpenCursors();
      }

      protected void btn_get( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         getEqualNoModal( ) ;
         if ( RcdFound75 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "CONTAGEMRESULTADO_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtContagemResultado_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         GX_FocusControl = edtContagemResultadoErro_Data_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_first( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         ScanStart1Y75( ) ;
         if ( RcdFound75 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtContagemResultadoErro_Data_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         ScanEnd1Y75( ) ;
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_previous( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         move_previous( ) ;
         if ( RcdFound75 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtContagemResultadoErro_Data_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_next( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         move_next( ) ;
         if ( RcdFound75 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtContagemResultadoErro_Data_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_last( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         ScanStart1Y75( ) ;
         if ( RcdFound75 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            while ( RcdFound75 != 0 )
            {
               ScanNext1Y75( ) ;
            }
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtContagemResultadoErro_Data_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         ScanEnd1Y75( ) ;
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_select( )
      {
         getEqualNoModal( ) ;
      }

      protected void CheckOptimisticConcurrency1Y75( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T001Y2 */
            pr_default.execute(0, new Object[] {A456ContagemResultado_Codigo, A579ContagemResultadoErro_Tipo});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ContagemResultadoErro"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) || ( Z580ContagemResultadoErro_Data != T001Y2_A580ContagemResultadoErro_Data[0] ) || ( StringUtil.StrCmp(Z581ContagemResultadoErro_Status, T001Y2_A581ContagemResultadoErro_Status[0]) != 0 ) || ( Z583ContagemResultadoErro_ContadorFMCod != T001Y2_A583ContagemResultadoErro_ContadorFMCod[0] ) )
            {
               if ( Z580ContagemResultadoErro_Data != T001Y2_A580ContagemResultadoErro_Data[0] )
               {
                  GXUtil.WriteLog("contagemresultadoerro:[seudo value changed for attri]"+"ContagemResultadoErro_Data");
                  GXUtil.WriteLogRaw("Old: ",Z580ContagemResultadoErro_Data);
                  GXUtil.WriteLogRaw("Current: ",T001Y2_A580ContagemResultadoErro_Data[0]);
               }
               if ( StringUtil.StrCmp(Z581ContagemResultadoErro_Status, T001Y2_A581ContagemResultadoErro_Status[0]) != 0 )
               {
                  GXUtil.WriteLog("contagemresultadoerro:[seudo value changed for attri]"+"ContagemResultadoErro_Status");
                  GXUtil.WriteLogRaw("Old: ",Z581ContagemResultadoErro_Status);
                  GXUtil.WriteLogRaw("Current: ",T001Y2_A581ContagemResultadoErro_Status[0]);
               }
               if ( Z583ContagemResultadoErro_ContadorFMCod != T001Y2_A583ContagemResultadoErro_ContadorFMCod[0] )
               {
                  GXUtil.WriteLog("contagemresultadoerro:[seudo value changed for attri]"+"ContagemResultadoErro_ContadorFMCod");
                  GXUtil.WriteLogRaw("Old: ",Z583ContagemResultadoErro_ContadorFMCod);
                  GXUtil.WriteLogRaw("Current: ",T001Y2_A583ContagemResultadoErro_ContadorFMCod[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"ContagemResultadoErro"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert1Y75( )
      {
         BeforeValidate1Y75( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable1Y75( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM1Y75( 0) ;
            CheckOptimisticConcurrency1Y75( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm1Y75( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert1Y75( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T001Y12 */
                     pr_default.execute(10, new Object[] {A579ContagemResultadoErro_Tipo, A580ContagemResultadoErro_Data, A581ContagemResultadoErro_Status, A456ContagemResultado_Codigo, A583ContagemResultadoErro_ContadorFMCod});
                     pr_default.close(10);
                     dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoErro") ;
                     if ( (pr_default.getStatus(10) == 1) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                           ResetCaption1Y0( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load1Y75( ) ;
            }
            EndLevel1Y75( ) ;
         }
         CloseExtendedTableCursors1Y75( ) ;
      }

      protected void Update1Y75( )
      {
         BeforeValidate1Y75( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable1Y75( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency1Y75( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm1Y75( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate1Y75( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T001Y13 */
                     pr_default.execute(11, new Object[] {A580ContagemResultadoErro_Data, A581ContagemResultadoErro_Status, A583ContagemResultadoErro_ContadorFMCod, A456ContagemResultado_Codigo, A579ContagemResultadoErro_Tipo});
                     pr_default.close(11);
                     dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoErro") ;
                     if ( (pr_default.getStatus(11) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ContagemResultadoErro"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate1Y75( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           getByPrimaryKey( ) ;
                           GX_msglist.addItem(context.GetMessage( "GXM_sucupdated", ""), 0, "", true);
                           ResetCaption1Y0( ) ;
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel1Y75( ) ;
         }
         CloseExtendedTableCursors1Y75( ) ;
      }

      protected void DeferredUpdate1Y75( )
      {
      }

      protected void delete( )
      {
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         BeforeValidate1Y75( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency1Y75( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls1Y75( ) ;
            AfterConfirm1Y75( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete1Y75( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T001Y14 */
                  pr_default.execute(12, new Object[] {A456ContagemResultado_Codigo, A579ContagemResultadoErro_Tipo});
                  pr_default.close(12);
                  dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoErro") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        move_next( ) ;
                        if ( RcdFound75 == 0 )
                        {
                           InitAll1Y75( ) ;
                           Gx_mode = "INS";
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                        }
                        else
                        {
                           getByPrimaryKey( ) ;
                           Gx_mode = "UPD";
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                        }
                        GX_msglist.addItem(context.GetMessage( "GXM_sucdeleted", ""), 0, "", true);
                        ResetCaption1Y0( ) ;
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode75 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         EndLevel1Y75( ) ;
         Gx_mode = sMode75;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
      }

      protected void OnDeleteControls1Y75( )
      {
         standaloneModal( ) ;
         /* No delete mode formulas found. */
      }

      protected void EndLevel1Y75( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete1Y75( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(1);
            context.CommitDataStores( "ContagemResultadoErro");
            if ( AnyError == 0 )
            {
               ConfirmValues1Y0( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(1);
            context.RollbackDataStores( "ContagemResultadoErro");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart1Y75( )
      {
         /* Using cursor T001Y15 */
         pr_default.execute(13);
         RcdFound75 = 0;
         if ( (pr_default.getStatus(13) != 101) )
         {
            RcdFound75 = 1;
            A456ContagemResultado_Codigo = T001Y15_A456ContagemResultado_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A456ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A456ContagemResultado_Codigo), 6, 0)));
            A579ContagemResultadoErro_Tipo = T001Y15_A579ContagemResultadoErro_Tipo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A579ContagemResultadoErro_Tipo", A579ContagemResultadoErro_Tipo);
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext1Y75( )
      {
         /* Scan next routine */
         pr_default.readNext(13);
         RcdFound75 = 0;
         if ( (pr_default.getStatus(13) != 101) )
         {
            RcdFound75 = 1;
            A456ContagemResultado_Codigo = T001Y15_A456ContagemResultado_Codigo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A456ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A456ContagemResultado_Codigo), 6, 0)));
            A579ContagemResultadoErro_Tipo = T001Y15_A579ContagemResultadoErro_Tipo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A579ContagemResultadoErro_Tipo", A579ContagemResultadoErro_Tipo);
         }
      }

      protected void ScanEnd1Y75( )
      {
         pr_default.close(13);
      }

      protected void AfterConfirm1Y75( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert1Y75( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate1Y75( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete1Y75( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete1Y75( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate1Y75( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes1Y75( )
      {
         edtContagemResultado_Codigo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultado_Codigo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultado_Codigo_Enabled), 5, 0)));
         cmbContagemResultadoErro_Tipo.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContagemResultadoErro_Tipo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbContagemResultadoErro_Tipo.Enabled), 5, 0)));
         edtContagemResultadoErro_Data_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoErro_Data_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultadoErro_Data_Enabled), 5, 0)));
         cmbContagemResultadoErro_Status.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbContagemResultadoErro_Status_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbContagemResultadoErro_Status.Enabled), 5, 0)));
         edtContagemResultadoErro_ContadorFMCod_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContagemResultadoErro_ContadorFMCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContagemResultadoErro_ContadorFMCod_Enabled), 5, 0)));
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues1Y0( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20206216171534");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("calendar-pt.js", "?"+context.GetBuildNumber( 114418));
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("contagemresultadoerro.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "Z456ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z456ContagemResultado_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z579ContagemResultadoErro_Tipo", StringUtil.RTrim( Z579ContagemResultadoErro_Tipo));
         GxWebStd.gx_hidden_field( context, "Z580ContagemResultadoErro_Data", context.localUtil.TToC( Z580ContagemResultadoErro_Data, 10, 8, 0, 0, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "Z581ContagemResultadoErro_Status", StringUtil.RTrim( Z581ContagemResultadoErro_Status));
         GxWebStd.gx_hidden_field( context, "Z583ContagemResultadoErro_ContadorFMCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z583ContagemResultadoErro_ContadorFMCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "vGXBSCREEN", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gx_BScreen), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("contagemresultadoerro.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "ContagemResultadoErro" ;
      }

      public override String GetPgmdesc( )
      {
         return "Erros nas confer�ncias de contagens" ;
      }

      protected void InitializeNonKey1Y75( )
      {
         A583ContagemResultadoErro_ContadorFMCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A583ContagemResultadoErro_ContadorFMCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A583ContagemResultadoErro_ContadorFMCod), 6, 0)));
         A580ContagemResultadoErro_Data = DateTimeUtil.ServerNow( context, "DEFAULT");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A580ContagemResultadoErro_Data", context.localUtil.TToC( A580ContagemResultadoErro_Data, 8, 5, 0, 3, "/", ":", " "));
         A581ContagemResultadoErro_Status = "P";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A581ContagemResultadoErro_Status", A581ContagemResultadoErro_Status);
         Z580ContagemResultadoErro_Data = (DateTime)(DateTime.MinValue);
         Z581ContagemResultadoErro_Status = "";
         Z583ContagemResultadoErro_ContadorFMCod = 0;
      }

      protected void InitAll1Y75( )
      {
         A456ContagemResultado_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A456ContagemResultado_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(A456ContagemResultado_Codigo), 6, 0)));
         A579ContagemResultadoErro_Tipo = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A579ContagemResultadoErro_Tipo", A579ContagemResultadoErro_Tipo);
         InitializeNonKey1Y75( ) ;
      }

      protected void StandaloneModalInsert( )
      {
         A581ContagemResultadoErro_Status = i581ContagemResultadoErro_Status;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A581ContagemResultadoErro_Status", A581ContagemResultadoErro_Status);
         A580ContagemResultadoErro_Data = i580ContagemResultadoErro_Data;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A580ContagemResultadoErro_Data", context.localUtil.TToC( A580ContagemResultadoErro_Data, 8, 5, 0, 3, "/", ":", " "));
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("calendar-system.css", "?4412140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20206216171539");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("contagemresultadoerro.js", "?20206216171539");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         imgBtn_first_Internalname = "BTN_FIRST";
         imgBtn_first_separator_Internalname = "BTN_FIRST_SEPARATOR";
         imgBtn_previous_Internalname = "BTN_PREVIOUS";
         imgBtn_previous_separator_Internalname = "BTN_PREVIOUS_SEPARATOR";
         imgBtn_next_Internalname = "BTN_NEXT";
         imgBtn_next_separator_Internalname = "BTN_NEXT_SEPARATOR";
         imgBtn_last_Internalname = "BTN_LAST";
         imgBtn_last_separator_Internalname = "BTN_LAST_SEPARATOR";
         imgBtn_select_Internalname = "BTN_SELECT";
         imgBtn_select_separator_Internalname = "BTN_SELECT_SEPARATOR";
         imgBtn_enter2_Internalname = "BTN_ENTER2";
         imgBtn_enter2_separator_Internalname = "BTN_ENTER2_SEPARATOR";
         imgBtn_cancel2_Internalname = "BTN_CANCEL2";
         imgBtn_cancel2_separator_Internalname = "BTN_CANCEL2_SEPARATOR";
         imgBtn_delete2_Internalname = "BTN_DELETE2";
         imgBtn_delete2_separator_Internalname = "BTN_DELETE2_SEPARATOR";
         divSectiontoolbar_Internalname = "SECTIONTOOLBAR";
         tblTabletoolbar_Internalname = "TABLETOOLBAR";
         lblTextblockcontagemresultado_codigo_Internalname = "TEXTBLOCKCONTAGEMRESULTADO_CODIGO";
         edtContagemResultado_Codigo_Internalname = "CONTAGEMRESULTADO_CODIGO";
         lblTextblockcontagemresultadoerro_tipo_Internalname = "TEXTBLOCKCONTAGEMRESULTADOERRO_TIPO";
         cmbContagemResultadoErro_Tipo_Internalname = "CONTAGEMRESULTADOERRO_TIPO";
         lblTextblockcontagemresultadoerro_data_Internalname = "TEXTBLOCKCONTAGEMRESULTADOERRO_DATA";
         edtContagemResultadoErro_Data_Internalname = "CONTAGEMRESULTADOERRO_DATA";
         lblTextblockcontagemresultadoerro_status_Internalname = "TEXTBLOCKCONTAGEMRESULTADOERRO_STATUS";
         cmbContagemResultadoErro_Status_Internalname = "CONTAGEMRESULTADOERRO_STATUS";
         lblTextblockcontagemresultadoerro_contadorfmcod_Internalname = "TEXTBLOCKCONTAGEMRESULTADOERRO_CONTADORFMCOD";
         edtContagemResultadoErro_ContadorFMCod_Internalname = "CONTAGEMRESULTADOERRO_CONTADORFMCOD";
         tblTable2_Internalname = "TABLE2";
         bttBtn_enter_Internalname = "BTN_ENTER";
         bttBtn_cancel_Internalname = "BTN_CANCEL";
         bttBtn_delete_Internalname = "BTN_DELETE";
         tblTable1_Internalname = "TABLE1";
         grpGroupdata_Internalname = "GROUPDATA";
         tblTablemain_Internalname = "TABLEMAIN";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Erros nas confer�ncias de contagens";
         imgBtn_delete2_separator_Visible = 1;
         imgBtn_delete2_Enabled = 1;
         imgBtn_delete2_Visible = 1;
         imgBtn_cancel2_separator_Visible = 1;
         imgBtn_cancel2_Visible = 1;
         imgBtn_enter2_separator_Visible = 1;
         imgBtn_enter2_Enabled = 1;
         imgBtn_enter2_Visible = 1;
         imgBtn_select_separator_Visible = 1;
         imgBtn_select_Visible = 1;
         imgBtn_last_separator_Visible = 1;
         imgBtn_last_Visible = 1;
         imgBtn_next_separator_Visible = 1;
         imgBtn_next_Visible = 1;
         imgBtn_previous_separator_Visible = 1;
         imgBtn_previous_Visible = 1;
         imgBtn_first_separator_Visible = 1;
         imgBtn_first_Visible = 1;
         edtContagemResultadoErro_ContadorFMCod_Jsonclick = "";
         edtContagemResultadoErro_ContadorFMCod_Enabled = 1;
         cmbContagemResultadoErro_Status_Jsonclick = "";
         cmbContagemResultadoErro_Status.Enabled = 1;
         edtContagemResultadoErro_Data_Jsonclick = "";
         edtContagemResultadoErro_Data_Enabled = 1;
         cmbContagemResultadoErro_Tipo_Jsonclick = "";
         cmbContagemResultadoErro_Tipo.Enabled = 1;
         edtContagemResultado_Codigo_Jsonclick = "";
         edtContagemResultado_Codigo_Enabled = 1;
         bttBtn_delete_Visible = 1;
         bttBtn_cancel_Visible = 1;
         bttBtn_enter_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void AfterKeyLoadScreen( )
      {
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         getEqualNoModal( ) ;
         /* Using cursor T001Y16 */
         pr_default.execute(14, new Object[] {A456ContagemResultado_Codigo});
         if ( (pr_default.getStatus(14) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Resultado das Contagens'.", "ForeignKeyNotFound", 1, "CONTAGEMRESULTADO_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtContagemResultado_Codigo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         pr_default.close(14);
         GX_FocusControl = edtContagemResultadoErro_Data_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         standaloneNotModal( ) ;
         standaloneModal( ) ;
         /* End function AfterKeyLoadScreen */
      }

      public void Valid_Contagemresultado_codigo( int GX_Parm1 )
      {
         A456ContagemResultado_Codigo = GX_Parm1;
         /* Using cursor T001Y16 */
         pr_default.execute(14, new Object[] {A456ContagemResultado_Codigo});
         if ( (pr_default.getStatus(14) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Resultado das Contagens'.", "ForeignKeyNotFound", 1, "CONTAGEMRESULTADO_CODIGO");
            AnyError = 1;
            GX_FocusControl = edtContagemResultado_Codigo_Internalname;
         }
         pr_default.close(14);
         dynload_actions( ) ;
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Contagemresultadoerro_tipo( int GX_Parm1 ,
                                                    GXCombobox cmbGX_Parm2 ,
                                                    DateTime GX_Parm3 ,
                                                    GXCombobox cmbGX_Parm4 ,
                                                    int GX_Parm5 )
      {
         A456ContagemResultado_Codigo = GX_Parm1;
         cmbContagemResultadoErro_Tipo = cmbGX_Parm2;
         A579ContagemResultadoErro_Tipo = cmbContagemResultadoErro_Tipo.CurrentValue;
         A580ContagemResultadoErro_Data = GX_Parm3;
         cmbContagemResultadoErro_Status = cmbGX_Parm4;
         A581ContagemResultadoErro_Status = cmbContagemResultadoErro_Status.CurrentValue;
         cmbContagemResultadoErro_Status.CurrentValue = A581ContagemResultadoErro_Status;
         A583ContagemResultadoErro_ContadorFMCod = GX_Parm5;
         context.wbHandled = 1;
         AfterKeyLoadScreen( ) ;
         Draw( ) ;
         SendCloseFormHiddens( ) ;
         if ( ! ( ( StringUtil.StrCmp(A579ContagemResultadoErro_Tipo, "DE") == 0 ) || ( StringUtil.StrCmp(A579ContagemResultadoErro_Tipo, "DA") == 0 ) || ( StringUtil.StrCmp(A579ContagemResultadoErro_Tipo, "SC") == 0 ) || ( StringUtil.StrCmp(A579ContagemResultadoErro_Tipo, "SI") == 0 ) || ( StringUtil.StrCmp(A579ContagemResultadoErro_Tipo, "PF") == 0 ) || ( StringUtil.StrCmp(A579ContagemResultadoErro_Tipo, "CO") == 0 ) ) )
         {
            GX_msglist.addItem("Campo Contagem Resultado Erro_Tipo fora do intervalo", "OutOfRange", 1, "CONTAGEMRESULTADOERRO_TIPO");
            AnyError = 1;
            GX_FocusControl = cmbContagemResultadoErro_Tipo_Internalname;
         }
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
         }
         isValidOutput.Add(context.localUtil.TToC( A580ContagemResultadoErro_Data, 10, 8, 0, 3, "/", ":", " "));
         cmbContagemResultadoErro_Status.CurrentValue = A581ContagemResultadoErro_Status;
         isValidOutput.Add(cmbContagemResultadoErro_Status);
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A583ContagemResultadoErro_ContadorFMCod), 6, 0, ".", "")));
         isValidOutput.Add(StringUtil.RTrim( Gx_mode));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z456ContagemResultado_Codigo), 6, 0, ",", "")));
         isValidOutput.Add(StringUtil.RTrim( Z579ContagemResultadoErro_Tipo));
         isValidOutput.Add(context.localUtil.TToC( Z580ContagemResultadoErro_Data, 10, 8, 0, 0, "/", ":", " "));
         isValidOutput.Add(StringUtil.RTrim( Z581ContagemResultadoErro_Status));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z583ContagemResultadoErro_ContadorFMCod), 6, 0, ",", "")));
         isValidOutput.Add(imgBtn_delete2_Enabled);
         isValidOutput.Add(imgBtn_enter2_Enabled);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Contagemresultadoerro_contadorfmcod( int GX_Parm1 )
      {
         A583ContagemResultadoErro_ContadorFMCod = GX_Parm1;
         /* Using cursor T001Y17 */
         pr_default.execute(15, new Object[] {A583ContagemResultadoErro_ContadorFMCod});
         if ( (pr_default.getStatus(15) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contagem Resultado Erro_ContadorFM'.", "ForeignKeyNotFound", 1, "CONTAGEMRESULTADOERRO_CONTADORFMCOD");
            AnyError = 1;
            GX_FocusControl = edtContagemResultadoErro_ContadorFMCod_Internalname;
         }
         pr_default.close(15);
         dynload_actions( ) ;
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(14);
         pr_default.close(15);
      }

      public override void initialize( )
      {
         sPrefix = "";
         Z579ContagemResultadoErro_Tipo = "";
         Z580ContagemResultadoErro_Data = (DateTime)(DateTime.MinValue);
         Z581ContagemResultadoErro_Status = "";
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         A579ContagemResultadoErro_Tipo = "";
         A581ContagemResultadoErro_Status = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         sStyleString = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         bttBtn_enter_Jsonclick = "";
         bttBtn_cancel_Jsonclick = "";
         bttBtn_delete_Jsonclick = "";
         lblTextblockcontagemresultado_codigo_Jsonclick = "";
         lblTextblockcontagemresultadoerro_tipo_Jsonclick = "";
         lblTextblockcontagemresultadoerro_data_Jsonclick = "";
         A580ContagemResultadoErro_Data = (DateTime)(DateTime.MinValue);
         lblTextblockcontagemresultadoerro_status_Jsonclick = "";
         lblTextblockcontagemresultadoerro_contadorfmcod_Jsonclick = "";
         imgBtn_first_Jsonclick = "";
         imgBtn_first_separator_Jsonclick = "";
         imgBtn_previous_Jsonclick = "";
         imgBtn_previous_separator_Jsonclick = "";
         imgBtn_next_Jsonclick = "";
         imgBtn_next_separator_Jsonclick = "";
         imgBtn_last_Jsonclick = "";
         imgBtn_last_separator_Jsonclick = "";
         imgBtn_select_Jsonclick = "";
         imgBtn_select_separator_Jsonclick = "";
         imgBtn_enter2_Jsonclick = "";
         imgBtn_enter2_separator_Jsonclick = "";
         imgBtn_cancel2_Jsonclick = "";
         imgBtn_cancel2_separator_Jsonclick = "";
         imgBtn_delete2_Jsonclick = "";
         imgBtn_delete2_separator_Jsonclick = "";
         Gx_mode = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         T001Y6_A579ContagemResultadoErro_Tipo = new String[] {""} ;
         T001Y6_A580ContagemResultadoErro_Data = new DateTime[] {DateTime.MinValue} ;
         T001Y6_A581ContagemResultadoErro_Status = new String[] {""} ;
         T001Y6_A456ContagemResultado_Codigo = new int[1] ;
         T001Y6_A583ContagemResultadoErro_ContadorFMCod = new int[1] ;
         T001Y4_A456ContagemResultado_Codigo = new int[1] ;
         T001Y5_A583ContagemResultadoErro_ContadorFMCod = new int[1] ;
         T001Y7_A456ContagemResultado_Codigo = new int[1] ;
         T001Y8_A583ContagemResultadoErro_ContadorFMCod = new int[1] ;
         T001Y9_A456ContagemResultado_Codigo = new int[1] ;
         T001Y9_A579ContagemResultadoErro_Tipo = new String[] {""} ;
         T001Y3_A579ContagemResultadoErro_Tipo = new String[] {""} ;
         T001Y3_A580ContagemResultadoErro_Data = new DateTime[] {DateTime.MinValue} ;
         T001Y3_A581ContagemResultadoErro_Status = new String[] {""} ;
         T001Y3_A456ContagemResultado_Codigo = new int[1] ;
         T001Y3_A583ContagemResultadoErro_ContadorFMCod = new int[1] ;
         sMode75 = "";
         T001Y10_A456ContagemResultado_Codigo = new int[1] ;
         T001Y10_A579ContagemResultadoErro_Tipo = new String[] {""} ;
         T001Y11_A456ContagemResultado_Codigo = new int[1] ;
         T001Y11_A579ContagemResultadoErro_Tipo = new String[] {""} ;
         T001Y2_A579ContagemResultadoErro_Tipo = new String[] {""} ;
         T001Y2_A580ContagemResultadoErro_Data = new DateTime[] {DateTime.MinValue} ;
         T001Y2_A581ContagemResultadoErro_Status = new String[] {""} ;
         T001Y2_A456ContagemResultado_Codigo = new int[1] ;
         T001Y2_A583ContagemResultadoErro_ContadorFMCod = new int[1] ;
         T001Y15_A456ContagemResultado_Codigo = new int[1] ;
         T001Y15_A579ContagemResultadoErro_Tipo = new String[] {""} ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         i581ContagemResultadoErro_Status = "";
         i580ContagemResultadoErro_Data = (DateTime)(DateTime.MinValue);
         T001Y16_A456ContagemResultado_Codigo = new int[1] ;
         isValidOutput = new GxUnknownObjectCollection();
         T001Y17_A583ContagemResultadoErro_ContadorFMCod = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.contagemresultadoerro__default(),
            new Object[][] {
                new Object[] {
               T001Y2_A579ContagemResultadoErro_Tipo, T001Y2_A580ContagemResultadoErro_Data, T001Y2_A581ContagemResultadoErro_Status, T001Y2_A456ContagemResultado_Codigo, T001Y2_A583ContagemResultadoErro_ContadorFMCod
               }
               , new Object[] {
               T001Y3_A579ContagemResultadoErro_Tipo, T001Y3_A580ContagemResultadoErro_Data, T001Y3_A581ContagemResultadoErro_Status, T001Y3_A456ContagemResultado_Codigo, T001Y3_A583ContagemResultadoErro_ContadorFMCod
               }
               , new Object[] {
               T001Y4_A456ContagemResultado_Codigo
               }
               , new Object[] {
               T001Y5_A583ContagemResultadoErro_ContadorFMCod
               }
               , new Object[] {
               T001Y6_A579ContagemResultadoErro_Tipo, T001Y6_A580ContagemResultadoErro_Data, T001Y6_A581ContagemResultadoErro_Status, T001Y6_A456ContagemResultado_Codigo, T001Y6_A583ContagemResultadoErro_ContadorFMCod
               }
               , new Object[] {
               T001Y7_A456ContagemResultado_Codigo
               }
               , new Object[] {
               T001Y8_A583ContagemResultadoErro_ContadorFMCod
               }
               , new Object[] {
               T001Y9_A456ContagemResultado_Codigo, T001Y9_A579ContagemResultadoErro_Tipo
               }
               , new Object[] {
               T001Y10_A456ContagemResultado_Codigo, T001Y10_A579ContagemResultadoErro_Tipo
               }
               , new Object[] {
               T001Y11_A456ContagemResultado_Codigo, T001Y11_A579ContagemResultadoErro_Tipo
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T001Y15_A456ContagemResultado_Codigo, T001Y15_A579ContagemResultadoErro_Tipo
               }
               , new Object[] {
               T001Y16_A456ContagemResultado_Codigo
               }
               , new Object[] {
               T001Y17_A583ContagemResultadoErro_ContadorFMCod
               }
            }
         );
         Z581ContagemResultadoErro_Status = "P";
         A581ContagemResultadoErro_Status = "P";
         i581ContagemResultadoErro_Status = "P";
         Z580ContagemResultadoErro_Data = DateTimeUtil.ServerNow( context, "DEFAULT");
         A580ContagemResultadoErro_Data = DateTimeUtil.ServerNow( context, "DEFAULT");
         i580ContagemResultadoErro_Data = DateTimeUtil.ServerNow( context, "DEFAULT");
      }

      private short GxWebError ;
      private short gxcookieaux ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short Gx_BScreen ;
      private short GX_JID ;
      private short RcdFound75 ;
      private short gxajaxcallmode ;
      private short wbTemp ;
      private int Z456ContagemResultado_Codigo ;
      private int Z583ContagemResultadoErro_ContadorFMCod ;
      private int A456ContagemResultado_Codigo ;
      private int A583ContagemResultadoErro_ContadorFMCod ;
      private int trnEnded ;
      private int bttBtn_enter_Visible ;
      private int bttBtn_cancel_Visible ;
      private int bttBtn_delete_Visible ;
      private int edtContagemResultado_Codigo_Enabled ;
      private int edtContagemResultadoErro_Data_Enabled ;
      private int edtContagemResultadoErro_ContadorFMCod_Enabled ;
      private int imgBtn_first_Visible ;
      private int imgBtn_first_separator_Visible ;
      private int imgBtn_previous_Visible ;
      private int imgBtn_previous_separator_Visible ;
      private int imgBtn_next_Visible ;
      private int imgBtn_next_separator_Visible ;
      private int imgBtn_last_Visible ;
      private int imgBtn_last_separator_Visible ;
      private int imgBtn_select_Visible ;
      private int imgBtn_select_separator_Visible ;
      private int imgBtn_enter2_Visible ;
      private int imgBtn_enter2_Enabled ;
      private int imgBtn_enter2_separator_Visible ;
      private int imgBtn_cancel2_Visible ;
      private int imgBtn_cancel2_separator_Visible ;
      private int imgBtn_delete2_Visible ;
      private int imgBtn_delete2_Enabled ;
      private int imgBtn_delete2_separator_Visible ;
      private int idxLst ;
      private String sPrefix ;
      private String Z579ContagemResultadoErro_Tipo ;
      private String Z581ContagemResultadoErro_Status ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String GXKey ;
      private String A579ContagemResultadoErro_Tipo ;
      private String A581ContagemResultadoErro_Status ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String edtContagemResultado_Codigo_Internalname ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String grpGroupdata_Internalname ;
      private String tblTable1_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String TempTags ;
      private String bttBtn_enter_Internalname ;
      private String bttBtn_enter_Jsonclick ;
      private String bttBtn_cancel_Internalname ;
      private String bttBtn_cancel_Jsonclick ;
      private String bttBtn_delete_Internalname ;
      private String bttBtn_delete_Jsonclick ;
      private String tblTable2_Internalname ;
      private String lblTextblockcontagemresultado_codigo_Internalname ;
      private String lblTextblockcontagemresultado_codigo_Jsonclick ;
      private String edtContagemResultado_Codigo_Jsonclick ;
      private String lblTextblockcontagemresultadoerro_tipo_Internalname ;
      private String lblTextblockcontagemresultadoerro_tipo_Jsonclick ;
      private String cmbContagemResultadoErro_Tipo_Internalname ;
      private String cmbContagemResultadoErro_Tipo_Jsonclick ;
      private String lblTextblockcontagemresultadoerro_data_Internalname ;
      private String lblTextblockcontagemresultadoerro_data_Jsonclick ;
      private String edtContagemResultadoErro_Data_Internalname ;
      private String edtContagemResultadoErro_Data_Jsonclick ;
      private String lblTextblockcontagemresultadoerro_status_Internalname ;
      private String lblTextblockcontagemresultadoerro_status_Jsonclick ;
      private String cmbContagemResultadoErro_Status_Internalname ;
      private String cmbContagemResultadoErro_Status_Jsonclick ;
      private String lblTextblockcontagemresultadoerro_contadorfmcod_Internalname ;
      private String lblTextblockcontagemresultadoerro_contadorfmcod_Jsonclick ;
      private String edtContagemResultadoErro_ContadorFMCod_Internalname ;
      private String edtContagemResultadoErro_ContadorFMCod_Jsonclick ;
      private String tblTabletoolbar_Internalname ;
      private String divSectiontoolbar_Internalname ;
      private String imgBtn_first_Internalname ;
      private String imgBtn_first_Jsonclick ;
      private String imgBtn_first_separator_Internalname ;
      private String imgBtn_first_separator_Jsonclick ;
      private String imgBtn_previous_Internalname ;
      private String imgBtn_previous_Jsonclick ;
      private String imgBtn_previous_separator_Internalname ;
      private String imgBtn_previous_separator_Jsonclick ;
      private String imgBtn_next_Internalname ;
      private String imgBtn_next_Jsonclick ;
      private String imgBtn_next_separator_Internalname ;
      private String imgBtn_next_separator_Jsonclick ;
      private String imgBtn_last_Internalname ;
      private String imgBtn_last_Jsonclick ;
      private String imgBtn_last_separator_Internalname ;
      private String imgBtn_last_separator_Jsonclick ;
      private String imgBtn_select_Internalname ;
      private String imgBtn_select_Jsonclick ;
      private String imgBtn_select_separator_Internalname ;
      private String imgBtn_select_separator_Jsonclick ;
      private String imgBtn_enter2_Internalname ;
      private String imgBtn_enter2_Jsonclick ;
      private String imgBtn_enter2_separator_Internalname ;
      private String imgBtn_enter2_separator_Jsonclick ;
      private String imgBtn_cancel2_Internalname ;
      private String imgBtn_cancel2_Jsonclick ;
      private String imgBtn_cancel2_separator_Internalname ;
      private String imgBtn_cancel2_separator_Jsonclick ;
      private String imgBtn_delete2_Internalname ;
      private String imgBtn_delete2_Jsonclick ;
      private String imgBtn_delete2_separator_Internalname ;
      private String imgBtn_delete2_separator_Jsonclick ;
      private String Gx_mode ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String sMode75 ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String i581ContagemResultadoErro_Status ;
      private DateTime Z580ContagemResultadoErro_Data ;
      private DateTime A580ContagemResultadoErro_Data ;
      private DateTime i580ContagemResultadoErro_Data ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbErr ;
      private GxUnknownObjectCollection isValidOutput ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbContagemResultadoErro_Tipo ;
      private GXCombobox cmbContagemResultadoErro_Status ;
      private IDataStoreProvider pr_default ;
      private String[] T001Y6_A579ContagemResultadoErro_Tipo ;
      private DateTime[] T001Y6_A580ContagemResultadoErro_Data ;
      private String[] T001Y6_A581ContagemResultadoErro_Status ;
      private int[] T001Y6_A456ContagemResultado_Codigo ;
      private int[] T001Y6_A583ContagemResultadoErro_ContadorFMCod ;
      private int[] T001Y4_A456ContagemResultado_Codigo ;
      private int[] T001Y5_A583ContagemResultadoErro_ContadorFMCod ;
      private int[] T001Y7_A456ContagemResultado_Codigo ;
      private int[] T001Y8_A583ContagemResultadoErro_ContadorFMCod ;
      private int[] T001Y9_A456ContagemResultado_Codigo ;
      private String[] T001Y9_A579ContagemResultadoErro_Tipo ;
      private String[] T001Y3_A579ContagemResultadoErro_Tipo ;
      private DateTime[] T001Y3_A580ContagemResultadoErro_Data ;
      private String[] T001Y3_A581ContagemResultadoErro_Status ;
      private int[] T001Y3_A456ContagemResultado_Codigo ;
      private int[] T001Y3_A583ContagemResultadoErro_ContadorFMCod ;
      private int[] T001Y10_A456ContagemResultado_Codigo ;
      private String[] T001Y10_A579ContagemResultadoErro_Tipo ;
      private int[] T001Y11_A456ContagemResultado_Codigo ;
      private String[] T001Y11_A579ContagemResultadoErro_Tipo ;
      private String[] T001Y2_A579ContagemResultadoErro_Tipo ;
      private DateTime[] T001Y2_A580ContagemResultadoErro_Data ;
      private String[] T001Y2_A581ContagemResultadoErro_Status ;
      private int[] T001Y2_A456ContagemResultado_Codigo ;
      private int[] T001Y2_A583ContagemResultadoErro_ContadorFMCod ;
      private int[] T001Y15_A456ContagemResultado_Codigo ;
      private String[] T001Y15_A579ContagemResultadoErro_Tipo ;
      private int[] T001Y16_A456ContagemResultado_Codigo ;
      private int[] T001Y17_A583ContagemResultadoErro_ContadorFMCod ;
      private GXWebForm Form ;
   }

   public class contagemresultadoerro__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new UpdateCursor(def[10])
         ,new UpdateCursor(def[11])
         ,new UpdateCursor(def[12])
         ,new ForEachCursor(def[13])
         ,new ForEachCursor(def[14])
         ,new ForEachCursor(def[15])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT001Y6 ;
          prmT001Y6 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultadoErro_Tipo",SqlDbType.Char,2,0}
          } ;
          Object[] prmT001Y4 ;
          prmT001Y4 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001Y5 ;
          prmT001Y5 = new Object[] {
          new Object[] {"@ContagemResultadoErro_ContadorFMCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001Y7 ;
          prmT001Y7 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001Y8 ;
          prmT001Y8 = new Object[] {
          new Object[] {"@ContagemResultadoErro_ContadorFMCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001Y9 ;
          prmT001Y9 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultadoErro_Tipo",SqlDbType.Char,2,0}
          } ;
          Object[] prmT001Y3 ;
          prmT001Y3 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultadoErro_Tipo",SqlDbType.Char,2,0}
          } ;
          Object[] prmT001Y10 ;
          prmT001Y10 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultadoErro_Tipo",SqlDbType.Char,2,0}
          } ;
          Object[] prmT001Y11 ;
          prmT001Y11 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultadoErro_Tipo",SqlDbType.Char,2,0}
          } ;
          Object[] prmT001Y2 ;
          prmT001Y2 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultadoErro_Tipo",SqlDbType.Char,2,0}
          } ;
          Object[] prmT001Y12 ;
          prmT001Y12 = new Object[] {
          new Object[] {"@ContagemResultadoErro_Tipo",SqlDbType.Char,2,0} ,
          new Object[] {"@ContagemResultadoErro_Data",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultadoErro_Status",SqlDbType.Char,1,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultadoErro_ContadorFMCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001Y13 ;
          prmT001Y13 = new Object[] {
          new Object[] {"@ContagemResultadoErro_Data",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultadoErro_Status",SqlDbType.Char,1,0} ,
          new Object[] {"@ContagemResultadoErro_ContadorFMCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultadoErro_Tipo",SqlDbType.Char,2,0}
          } ;
          Object[] prmT001Y14 ;
          prmT001Y14 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultadoErro_Tipo",SqlDbType.Char,2,0}
          } ;
          Object[] prmT001Y15 ;
          prmT001Y15 = new Object[] {
          } ;
          Object[] prmT001Y16 ;
          prmT001Y16 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmT001Y17 ;
          prmT001Y17 = new Object[] {
          new Object[] {"@ContagemResultadoErro_ContadorFMCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("T001Y2", "SELECT [ContagemResultadoErro_Tipo], [ContagemResultadoErro_Data], [ContagemResultadoErro_Status], [ContagemResultado_Codigo], [ContagemResultadoErro_ContadorFMCod] AS ContagemResultadoErro_ContadorFMCod FROM [ContagemResultadoErro] WITH (UPDLOCK) WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo AND [ContagemResultadoErro_Tipo] = @ContagemResultadoErro_Tipo ",true, GxErrorMask.GX_NOMASK, false, this,prmT001Y2,1,0,true,false )
             ,new CursorDef("T001Y3", "SELECT [ContagemResultadoErro_Tipo], [ContagemResultadoErro_Data], [ContagemResultadoErro_Status], [ContagemResultado_Codigo], [ContagemResultadoErro_ContadorFMCod] AS ContagemResultadoErro_ContadorFMCod FROM [ContagemResultadoErro] WITH (NOLOCK) WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo AND [ContagemResultadoErro_Tipo] = @ContagemResultadoErro_Tipo ",true, GxErrorMask.GX_NOMASK, false, this,prmT001Y3,1,0,true,false )
             ,new CursorDef("T001Y4", "SELECT [ContagemResultado_Codigo] FROM [ContagemResultado] WITH (NOLOCK) WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT001Y4,1,0,true,false )
             ,new CursorDef("T001Y5", "SELECT [Usuario_Codigo] AS ContagemResultadoErro_ContadorFMCod FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @ContagemResultadoErro_ContadorFMCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT001Y5,1,0,true,false )
             ,new CursorDef("T001Y6", "SELECT TM1.[ContagemResultadoErro_Tipo], TM1.[ContagemResultadoErro_Data], TM1.[ContagemResultadoErro_Status], TM1.[ContagemResultado_Codigo], TM1.[ContagemResultadoErro_ContadorFMCod] AS ContagemResultadoErro_ContadorFMCod FROM [ContagemResultadoErro] TM1 WITH (NOLOCK) WHERE TM1.[ContagemResultado_Codigo] = @ContagemResultado_Codigo and TM1.[ContagemResultadoErro_Tipo] = @ContagemResultadoErro_Tipo ORDER BY TM1.[ContagemResultado_Codigo], TM1.[ContagemResultadoErro_Tipo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT001Y6,100,0,true,false )
             ,new CursorDef("T001Y7", "SELECT [ContagemResultado_Codigo] FROM [ContagemResultado] WITH (NOLOCK) WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT001Y7,1,0,true,false )
             ,new CursorDef("T001Y8", "SELECT [Usuario_Codigo] AS ContagemResultadoErro_ContadorFMCod FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @ContagemResultadoErro_ContadorFMCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT001Y8,1,0,true,false )
             ,new CursorDef("T001Y9", "SELECT [ContagemResultado_Codigo], [ContagemResultadoErro_Tipo] FROM [ContagemResultadoErro] WITH (NOLOCK) WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo AND [ContagemResultadoErro_Tipo] = @ContagemResultadoErro_Tipo  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT001Y9,1,0,true,false )
             ,new CursorDef("T001Y10", "SELECT TOP 1 [ContagemResultado_Codigo], [ContagemResultadoErro_Tipo] FROM [ContagemResultadoErro] WITH (NOLOCK) WHERE ( [ContagemResultado_Codigo] > @ContagemResultado_Codigo or [ContagemResultado_Codigo] = @ContagemResultado_Codigo and [ContagemResultadoErro_Tipo] > @ContagemResultadoErro_Tipo) ORDER BY [ContagemResultado_Codigo], [ContagemResultadoErro_Tipo]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT001Y10,1,0,true,true )
             ,new CursorDef("T001Y11", "SELECT TOP 1 [ContagemResultado_Codigo], [ContagemResultadoErro_Tipo] FROM [ContagemResultadoErro] WITH (NOLOCK) WHERE ( [ContagemResultado_Codigo] < @ContagemResultado_Codigo or [ContagemResultado_Codigo] = @ContagemResultado_Codigo and [ContagemResultadoErro_Tipo] < @ContagemResultadoErro_Tipo) ORDER BY [ContagemResultado_Codigo] DESC, [ContagemResultadoErro_Tipo] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT001Y11,1,0,true,true )
             ,new CursorDef("T001Y12", "INSERT INTO [ContagemResultadoErro]([ContagemResultadoErro_Tipo], [ContagemResultadoErro_Data], [ContagemResultadoErro_Status], [ContagemResultado_Codigo], [ContagemResultadoErro_ContadorFMCod]) VALUES(@ContagemResultadoErro_Tipo, @ContagemResultadoErro_Data, @ContagemResultadoErro_Status, @ContagemResultado_Codigo, @ContagemResultadoErro_ContadorFMCod)", GxErrorMask.GX_NOMASK,prmT001Y12)
             ,new CursorDef("T001Y13", "UPDATE [ContagemResultadoErro] SET [ContagemResultadoErro_Data]=@ContagemResultadoErro_Data, [ContagemResultadoErro_Status]=@ContagemResultadoErro_Status, [ContagemResultadoErro_ContadorFMCod]=@ContagemResultadoErro_ContadorFMCod  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo AND [ContagemResultadoErro_Tipo] = @ContagemResultadoErro_Tipo", GxErrorMask.GX_NOMASK,prmT001Y13)
             ,new CursorDef("T001Y14", "DELETE FROM [ContagemResultadoErro]  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo AND [ContagemResultadoErro_Tipo] = @ContagemResultadoErro_Tipo", GxErrorMask.GX_NOMASK,prmT001Y14)
             ,new CursorDef("T001Y15", "SELECT [ContagemResultado_Codigo], [ContagemResultadoErro_Tipo] FROM [ContagemResultadoErro] WITH (NOLOCK) ORDER BY [ContagemResultado_Codigo], [ContagemResultadoErro_Tipo]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT001Y15,100,0,true,false )
             ,new CursorDef("T001Y16", "SELECT [ContagemResultado_Codigo] FROM [ContagemResultado] WITH (NOLOCK) WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo ",true, GxErrorMask.GX_NOMASK, false, this,prmT001Y16,1,0,true,false )
             ,new CursorDef("T001Y17", "SELECT [Usuario_Codigo] AS ContagemResultadoErro_ContadorFMCod FROM [Usuario] WITH (NOLOCK) WHERE [Usuario_Codigo] = @ContagemResultadoErro_ContadorFMCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT001Y17,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getString(1, 2) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDateTime(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 1) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                ((int[]) buf[4])[0] = rslt.getInt(5) ;
                return;
             case 1 :
                ((String[]) buf[0])[0] = rslt.getString(1, 2) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDateTime(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 1) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                ((int[]) buf[4])[0] = rslt.getInt(5) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 4 :
                ((String[]) buf[0])[0] = rslt.getString(1, 2) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDateTime(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 1) ;
                ((int[]) buf[3])[0] = rslt.getInt(4) ;
                ((int[]) buf[4])[0] = rslt.getInt(5) ;
                return;
             case 5 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 2) ;
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 2) ;
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 2) ;
                return;
             case 13 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 2) ;
                return;
             case 14 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 15 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                return;
             case 10 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameterDatetime(2, (DateTime)parms[1]);
                stmt.SetParameter(3, (String)parms[2]);
                stmt.SetParameter(4, (int)parms[3]);
                stmt.SetParameter(5, (int)parms[4]);
                return;
             case 11 :
                stmt.SetParameterDatetime(1, (DateTime)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                stmt.SetParameter(4, (int)parms[3]);
                stmt.SetParameter(5, (String)parms[4]);
                return;
             case 12 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                return;
             case 14 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 15 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
