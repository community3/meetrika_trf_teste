/*
               File: SistemaFuncaopAPFWC
        Description: Sistema Funcaop APFWC
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/18/2020 12:48:34.55
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class sistemafuncaopapfwc : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public sistemafuncaopapfwc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
      }

      public sistemafuncaopapfwc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_FuncaoAPF_SistemaCod )
      {
         this.AV21FuncaoAPF_SistemaCod = aP0_FuncaoAPF_SistemaCod;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
         cmbavOrderedby = new GXCombobox();
         cmbavDynamicfiltersselector1 = new GXCombobox();
         cmbavDynamicfiltersoperator1 = new GXCombobox();
         cmbavFuncaoapf_tipo1 = new GXCombobox();
         cmbavDynamicfiltersselector2 = new GXCombobox();
         cmbavDynamicfiltersoperator2 = new GXCombobox();
         cmbavFuncaoapf_tipo2 = new GXCombobox();
         cmbFuncaoAPF_Tipo = new GXCombobox();
         cmbFuncaoAPF_Complexidade = new GXCombobox();
         chkavComdc = new GXCheckbox();
         cmbFuncaoAPF_Ativo = new GXCombobox();
         chkavDynamicfiltersenabled2 = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  AV21FuncaoAPF_SistemaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21FuncaoAPF_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21FuncaoAPF_SistemaCod), 6, 0)));
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(int)AV21FuncaoAPF_SistemaCod});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
               {
                  nRC_GXsfl_72 = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  nGXsfl_72_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  sGXsfl_72_idx = GetNextPar( );
                  sPrefix = GetNextPar( );
                  edtFuncaoAPF_SolucaoTecnica_Visible = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtFuncaoAPF_SolucaoTecnica_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtFuncaoAPF_SolucaoTecnica_Visible), 5, 0)));
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxnrGrid_newrow( ) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
               {
                  subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  AV13OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
                  AV14OrderedDsc = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
                  AV25DynamicFiltersSelector1 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25DynamicFiltersSelector1", AV25DynamicFiltersSelector1);
                  AV26DynamicFiltersOperator1 = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator1), 4, 0)));
                  AV27FuncaoAPF_Nome1 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27FuncaoAPF_Nome1", AV27FuncaoAPF_Nome1);
                  AV28FuncaoAPF_Tipo1 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV28FuncaoAPF_Tipo1", AV28FuncaoAPF_Tipo1);
                  AV38FuncaoAPF_PF1 = NumberUtil.Val( GetNextPar( ), ".");
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV38FuncaoAPF_PF1", StringUtil.LTrim( StringUtil.Str( AV38FuncaoAPF_PF1, 14, 5)));
                  AV30DynamicFiltersSelector2 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30DynamicFiltersSelector2", AV30DynamicFiltersSelector2);
                  AV31DynamicFiltersOperator2 = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV31DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV31DynamicFiltersOperator2), 4, 0)));
                  AV32FuncaoAPF_Nome2 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV32FuncaoAPF_Nome2", AV32FuncaoAPF_Nome2);
                  AV33FuncaoAPF_Tipo2 = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV33FuncaoAPF_Tipo2", AV33FuncaoAPF_Tipo2);
                  AV39FuncaoAPF_PF2 = NumberUtil.Val( GetNextPar( ), ".");
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV39FuncaoAPF_PF2", StringUtil.LTrim( StringUtil.Str( AV39FuncaoAPF_PF2, 14, 5)));
                  AV29DynamicFiltersEnabled2 = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29DynamicFiltersEnabled2", AV29DynamicFiltersEnabled2);
                  AV21FuncaoAPF_SistemaCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21FuncaoAPF_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21FuncaoAPF_SistemaCod), 6, 0)));
                  edtFuncaoAPF_SolucaoTecnica_Visible = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtFuncaoAPF_SolucaoTecnica_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtFuncaoAPF_SolucaoTecnica_Visible), 5, 0)));
                  ajax_req_read_hidden_sdt(GetNextPar( ), AV6WWPContext);
                  AV90Pgmname = GetNextPar( );
                  ajax_req_read_hidden_sdt(GetNextPar( ), AV11GridState);
                  AV35DynamicFiltersIgnoreFirst = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV35DynamicFiltersIgnoreFirst", AV35DynamicFiltersIgnoreFirst);
                  AV34DynamicFiltersRemoving = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV34DynamicFiltersRemoving", AV34DynamicFiltersRemoving);
                  A165FuncaoAPF_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  A432FuncaoAPF_Link = GetNextPar( );
                  n432FuncaoAPF_Link = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A432FuncaoAPF_Link", A432FuncaoAPF_Link);
                  A386FuncaoAPF_PF = NumberUtil.Val( GetNextPar( ), ".");
                  A415FuncaoAPFAtributos_FuncaoDadosTip = GetNextPar( );
                  n415FuncaoAPFAtributos_FuncaoDadosTip = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A415FuncaoAPFAtributos_FuncaoDadosTip", A415FuncaoAPFAtributos_FuncaoDadosTip);
                  sPrefix = GetNextPar( );
                  init_default_properties( ) ;
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV25DynamicFiltersSelector1, AV26DynamicFiltersOperator1, AV27FuncaoAPF_Nome1, AV28FuncaoAPF_Tipo1, AV38FuncaoAPF_PF1, AV30DynamicFiltersSelector2, AV31DynamicFiltersOperator2, AV32FuncaoAPF_Nome2, AV33FuncaoAPF_Tipo2, AV39FuncaoAPF_PF2, AV29DynamicFiltersEnabled2, AV21FuncaoAPF_SistemaCod, AV6WWPContext, AV90Pgmname, AV11GridState, AV35DynamicFiltersIgnoreFirst, AV34DynamicFiltersRemoving, A165FuncaoAPF_Codigo, A432FuncaoAPF_Link, A386FuncaoAPF_PF, A415FuncaoAPFAtributos_FuncaoDadosTip, sPrefix) ;
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
                  return  ;
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PA9A2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV90Pgmname = "SistemaFuncaopAPFWC";
               context.Gx_err = 0;
               chkavComdc.Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkavComdc_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavComdc.Enabled), 5, 0)));
               WS9A2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "Sistema Funcaop APFWC") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?202051812483494");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
            context.WriteHtmlText( "<body") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("sistemafuncaopapfwc.aspx") + "?" + UrlEncode("" +AV21FuncaoAPF_SistemaCod)+"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         }
         else
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vORDEREDDSC", StringUtil.BoolToStr( AV14OrderedDsc));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vDYNAMICFILTERSSELECTOR1", AV25DynamicFiltersSelector1);
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vDYNAMICFILTERSOPERATOR1", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV26DynamicFiltersOperator1), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vFUNCAOAPF_NOME1", AV27FuncaoAPF_Nome1);
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vFUNCAOAPF_TIPO1", StringUtil.RTrim( AV28FuncaoAPF_Tipo1));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vFUNCAOAPF_PF1", StringUtil.LTrim( StringUtil.NToC( AV38FuncaoAPF_PF1, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vDYNAMICFILTERSSELECTOR2", AV30DynamicFiltersSelector2);
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vDYNAMICFILTERSOPERATOR2", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV31DynamicFiltersOperator2), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vFUNCAOAPF_NOME2", AV32FuncaoAPF_Nome2);
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vFUNCAOAPF_TIPO2", StringUtil.RTrim( AV33FuncaoAPF_Tipo2));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vFUNCAOAPF_PF2", StringUtil.LTrim( StringUtil.NToC( AV39FuncaoAPF_PF2, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GXH_vDYNAMICFILTERSENABLED2", StringUtil.BoolToStr( AV29DynamicFiltersEnabled2));
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, sPrefix+"nRC_GXsfl_72", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_72), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vGRIDCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV77GridCurrentPage), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vGRIDPAGECOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV78GridPageCount), 10, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOAV21FuncaoAPF_SistemaCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(wcpOAV21FuncaoAPF_SistemaCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vFUNCAOAPF_SISTEMACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV21FuncaoAPF_SistemaCod), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vWWPCONTEXT", AV6WWPContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vWWPCONTEXT", AV6WWPContext);
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"vPGMNAME", StringUtil.RTrim( AV90Pgmname));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vGRIDSTATE", AV11GridState);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vGRIDSTATE", AV11GridState);
         }
         GxWebStd.gx_boolean_hidden_field( context, sPrefix+"vDYNAMICFILTERSIGNOREFIRST", AV35DynamicFiltersIgnoreFirst);
         GxWebStd.gx_boolean_hidden_field( context, sPrefix+"vDYNAMICFILTERSREMOVING", AV34DynamicFiltersRemoving);
         GxWebStd.gx_hidden_field( context, sPrefix+"FUNCAOAPF_LINK", A432FuncaoAPF_Link);
         GxWebStd.gx_hidden_field( context, sPrefix+"FUNCAOAPFATRIBUTOS_FUNCAODADOSTIP", StringUtil.RTrim( A415FuncaoAPFAtributos_FuncaoDadosTip));
         GxWebStd.gx_hidden_field( context, sPrefix+"vSISTEMA_SIGLA", StringUtil.RTrim( AV40Sistema_Sigla));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"FUNCAOAPF_MELHORACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A744FuncaoAPF_MelhoraCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Class", StringUtil.RTrim( Gridpaginationbar_Class));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_First", StringUtil.RTrim( Gridpaginationbar_First));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Previous", StringUtil.RTrim( Gridpaginationbar_Previous));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Next", StringUtil.RTrim( Gridpaginationbar_Next));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Last", StringUtil.RTrim( Gridpaginationbar_Last));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Caption", StringUtil.RTrim( Gridpaginationbar_Caption));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Showfirst", StringUtil.BoolToStr( Gridpaginationbar_Showfirst));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Showprevious", StringUtil.BoolToStr( Gridpaginationbar_Showprevious));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Shownext", StringUtil.BoolToStr( Gridpaginationbar_Shownext));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Showlast", StringUtil.BoolToStr( Gridpaginationbar_Showlast));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Pagestoshow", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gridpaginationbar_Pagestoshow), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Pagingbuttonsposition", StringUtil.RTrim( Gridpaginationbar_Pagingbuttonsposition));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Pagingcaptionposition", StringUtil.RTrim( Gridpaginationbar_Pagingcaptionposition));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Emptygridclass", StringUtil.RTrim( Gridpaginationbar_Emptygridclass));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Emptygridcaption", StringUtil.RTrim( Gridpaginationbar_Emptygridcaption));
         GxWebStd.gx_hidden_field( context, sPrefix+"FUNCAOAPF_SOLUCAOTECNICA_Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtFuncaoAPF_SolucaoTecnica_Visible), 5, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"GRIDPAGINATIONBAR_Selectedpage", StringUtil.RTrim( Gridpaginationbar_Selectedpage));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseForm9A2( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && context.isAjaxRequest( ) )
         {
            context.AddJavascriptSource("sistemafuncaopapfwc.js", "?202051812483541");
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            context.WriteHtmlTextNl( "</form>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "SistemaFuncaopAPFWC" ;
      }

      public override String GetPgmdesc( )
      {
         return "Sistema Funcaop APFWC" ;
      }

      protected void WB9A0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "sistemafuncaopapfwc.aspx");
               context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
               context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
            }
            wb_table1_2_9A2( true) ;
         }
         else
         {
            wb_table1_2_9A2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_9A2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"WORKWITHPLUSUTILITIES1Container"+"\"></div>") ;
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 93,'" + sPrefix + "',false,'" + sGXsfl_72_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavDynamicfiltersenabled2_Internalname, StringUtil.BoolToStr( AV29DynamicFiltersEnabled2), "", "", chkavDynamicfiltersenabled2.Visible, 1, "true", "", StyleString, ClassString, "", TempTags+" onclick=\"gx.fn.checkboxClick(93, this, 'true', 'false');gx.evt.onchange(this);\" "+" onblur=\""+""+";gx.evt.onblur(this,93);\"");
         }
         wbLoad = true;
      }

      protected void START9A2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
               Form.Meta.addItem("description", "Sistema Funcaop APFWC", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUP9A0( ) ;
            }
         }
      }

      protected void WS9A2( )
      {
         START9A2( ) ;
         EVT9A2( ) ;
      }

      protected void EVT9A2( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP9A0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRIDPAGINATIONBAR.CHANGEPAGE") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP9A0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E119A2 */
                                    E119A2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VORDEREDBY.CLICK") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP9A0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E129A2 */
                                    E129A2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS1'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP9A0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E139A2 */
                                    E139A2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REMOVEDYNAMICFILTERS2'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP9A0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E149A2 */
                                    E149A2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOCLEANFILTERS'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP9A0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E159A2 */
                                    E159A2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOINSERT'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP9A0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E169A2 */
                                    E169A2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOMANTER'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP9A0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E179A2 */
                                    E179A2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ADDDYNAMICFILTERS1'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP9A0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E189A2 */
                                    E189A2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR1.CLICK") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP9A0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E199A2 */
                                    E199A2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VDYNAMICFILTERSSELECTOR2.CLICK") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP9A0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: E209A2 */
                                    E209A2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP9A0( ) ;
                              }
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    GX_FocusControl = chkavComdc_Internalname;
                                    context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                 }
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP9A0( ) ;
                              }
                              nGXsfl_72_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_72_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_72_idx), 4, 0)), 4, "0");
                              SubsflControlProps_722( ) ;
                              AV17Update = cgiGet( edtavUpdate_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavUpdate_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV17Update)) ? AV85Update_GXI : context.convertURL( context.PathToRelativeUrl( AV17Update))));
                              AV18Delete = cgiGet( edtavDelete_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDelete_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV18Delete)) ? AV86Delete_GXI : context.convertURL( context.PathToRelativeUrl( AV18Delete))));
                              AV19Display = cgiGet( edtavDisplay_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDisplay_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV19Display)) ? AV87Display_GXI : context.convertURL( context.PathToRelativeUrl( AV19Display))));
                              A165FuncaoAPF_Codigo = (int)(context.localUtil.CToN( cgiGet( edtFuncaoAPF_Codigo_Internalname), ",", "."));
                              A360FuncaoAPF_SistemaCod = (int)(context.localUtil.CToN( cgiGet( edtFuncaoAPF_SistemaCod_Internalname), ",", "."));
                              n360FuncaoAPF_SistemaCod = false;
                              A166FuncaoAPF_Nome = cgiGet( edtFuncaoAPF_Nome_Internalname);
                              cmbFuncaoAPF_Tipo.Name = cmbFuncaoAPF_Tipo_Internalname;
                              cmbFuncaoAPF_Tipo.CurrentValue = cgiGet( cmbFuncaoAPF_Tipo_Internalname);
                              A184FuncaoAPF_Tipo = cgiGet( cmbFuncaoAPF_Tipo_Internalname);
                              cmbFuncaoAPF_Complexidade.Name = cmbFuncaoAPF_Complexidade_Internalname;
                              cmbFuncaoAPF_Complexidade.CurrentValue = cgiGet( cmbFuncaoAPF_Complexidade_Internalname);
                              A185FuncaoAPF_Complexidade = cgiGet( cmbFuncaoAPF_Complexidade_Internalname);
                              A388FuncaoAPF_TD = (short)(context.localUtil.CToN( cgiGet( edtFuncaoAPF_TD_Internalname), ",", "."));
                              A387FuncaoAPF_AR = (short)(context.localUtil.CToN( cgiGet( edtFuncaoAPF_AR_Internalname), ",", "."));
                              A386FuncaoAPF_PF = context.localUtil.CToN( cgiGet( edtFuncaoAPF_PF_Internalname), ",", ".");
                              AV36ComDC = StringUtil.StrToBool( cgiGet( chkavComdc_Internalname));
                              context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, chkavComdc_Internalname, AV36ComDC);
                              AV37FuncaoAPF_Link = cgiGet( edtavFuncaoapf_link_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavFuncaoapf_link_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV37FuncaoAPF_Link)) ? AV88Funcaoapf_link_GXI : context.convertURL( context.PathToRelativeUrl( AV37FuncaoAPF_Link))));
                              cmbFuncaoAPF_Ativo.Name = cmbFuncaoAPF_Ativo_Internalname;
                              cmbFuncaoAPF_Ativo.CurrentValue = cgiGet( cmbFuncaoAPF_Ativo_Internalname);
                              A183FuncaoAPF_Ativo = cgiGet( cmbFuncaoAPF_Ativo_Internalname);
                              A757FuncaoAPF_SolucaoTecnica = cgiGet( edtFuncaoAPF_SolucaoTecnica_Internalname);
                              AV23ImagemFuncoesUsuario = cgiGet( edtavImagemfuncoesusuario_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavImagemfuncoesusuario_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV23ImagemFuncoesUsuario)) ? AV84Imagemfuncoesusuario_GXI : context.convertURL( context.PathToRelativeUrl( AV23ImagemFuncoesUsuario))));
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = chkavComdc_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E219A2 */
                                          E219A2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = chkavComdc_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E229A2 */
                                          E229A2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = chkavComdc_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: E239A2 */
                                          E239A2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          if ( ! wbErr )
                                          {
                                             Rfr0gs = false;
                                             /* Set Refresh If Orderedby Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Ordereddsc Changed */
                                             if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vORDEREDDSC")) != AV14OrderedDsc )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Dynamicfiltersselector1 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vDYNAMICFILTERSSELECTOR1"), AV25DynamicFiltersSelector1) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Dynamicfiltersoperator1 Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vDYNAMICFILTERSOPERATOR1"), ",", ".") != Convert.ToDecimal( AV26DynamicFiltersOperator1 )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Funcaoapf_nome1 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vFUNCAOAPF_NOME1"), AV27FuncaoAPF_Nome1) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Funcaoapf_tipo1 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vFUNCAOAPF_TIPO1"), AV28FuncaoAPF_Tipo1) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Funcaoapf_pf1 Changed */
                                             if ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vFUNCAOAPF_PF1"), ",", ".") != AV38FuncaoAPF_PF1 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Dynamicfiltersselector2 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vDYNAMICFILTERSSELECTOR2"), AV30DynamicFiltersSelector2) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Dynamicfiltersoperator2 Changed */
                                             if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vDYNAMICFILTERSOPERATOR2"), ",", ".") != Convert.ToDecimal( AV31DynamicFiltersOperator2 )) )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Funcaoapf_nome2 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vFUNCAOAPF_NOME2"), AV32FuncaoAPF_Nome2) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Funcaoapf_tipo2 Changed */
                                             if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vFUNCAOAPF_TIPO2"), AV33FuncaoAPF_Tipo2) != 0 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Funcaoapf_pf2 Changed */
                                             if ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vFUNCAOAPF_PF2"), ",", ".") != AV39FuncaoAPF_PF2 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             /* Set Refresh If Dynamicfiltersenabled2 Changed */
                                             if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vDYNAMICFILTERSENABLED2")) != AV29DynamicFiltersEnabled2 )
                                             {
                                                Rfr0gs = true;
                                             }
                                             if ( ! Rfr0gs )
                                             {
                                             }
                                             dynload_actions( ) ;
                                          }
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "CANCEL") == 0 )
                                 {
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = chkavComdc_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                                    {
                                       STRUP9A0( ) ;
                                    }
                                    if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = chkavComdc_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WE9A2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseForm9A2( ) ;
            }
         }
      }

      protected void PA9A2( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            cmbavOrderedby.Name = "vORDEREDBY";
            cmbavOrderedby.WebTags = "";
            if ( cmbavOrderedby.ItemCount > 0 )
            {
               AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            }
            cmbavDynamicfiltersselector1.Name = "vDYNAMICFILTERSSELECTOR1";
            cmbavDynamicfiltersselector1.WebTags = "";
            cmbavDynamicfiltersselector1.addItem("FUNCAOAPF_NOME", "Nome", 0);
            cmbavDynamicfiltersselector1.addItem("FUNCAOAPF_TIPO", "Tipo da Fun��o", 0);
            cmbavDynamicfiltersselector1.addItem("FUNCAOAPF_PF", "Pontos de Fun��o", 0);
            if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
            {
               AV25DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV25DynamicFiltersSelector1);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25DynamicFiltersSelector1", AV25DynamicFiltersSelector1);
            }
            cmbavDynamicfiltersoperator1.Name = "vDYNAMICFILTERSOPERATOR1";
            cmbavDynamicfiltersoperator1.WebTags = "";
            cmbavDynamicfiltersoperator1.addItem("0", "<", 0);
            cmbavDynamicfiltersoperator1.addItem("1", "=", 0);
            cmbavDynamicfiltersoperator1.addItem("2", ">", 0);
            if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
            {
               AV26DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator1), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator1), 4, 0)));
            }
            cmbavFuncaoapf_tipo1.Name = "vFUNCAOAPF_TIPO1";
            cmbavFuncaoapf_tipo1.WebTags = "";
            cmbavFuncaoapf_tipo1.addItem("", "Todos", 0);
            cmbavFuncaoapf_tipo1.addItem("EE", "EE", 0);
            cmbavFuncaoapf_tipo1.addItem("SE", "SE", 0);
            cmbavFuncaoapf_tipo1.addItem("CE", "CE", 0);
            cmbavFuncaoapf_tipo1.addItem("NM", "NM", 0);
            if ( cmbavFuncaoapf_tipo1.ItemCount > 0 )
            {
               AV28FuncaoAPF_Tipo1 = cmbavFuncaoapf_tipo1.getValidValue(AV28FuncaoAPF_Tipo1);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV28FuncaoAPF_Tipo1", AV28FuncaoAPF_Tipo1);
            }
            cmbavDynamicfiltersselector2.Name = "vDYNAMICFILTERSSELECTOR2";
            cmbavDynamicfiltersselector2.WebTags = "";
            cmbavDynamicfiltersselector2.addItem("FUNCAOAPF_NOME", "Nome", 0);
            cmbavDynamicfiltersselector2.addItem("FUNCAOAPF_TIPO", "Tipo da Fun��o", 0);
            cmbavDynamicfiltersselector2.addItem("FUNCAOAPF_PF", "Pontos de Fun��o", 0);
            if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
            {
               AV30DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV30DynamicFiltersSelector2);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30DynamicFiltersSelector2", AV30DynamicFiltersSelector2);
            }
            cmbavDynamicfiltersoperator2.Name = "vDYNAMICFILTERSOPERATOR2";
            cmbavDynamicfiltersoperator2.WebTags = "";
            cmbavDynamicfiltersoperator2.addItem("0", "<", 0);
            cmbavDynamicfiltersoperator2.addItem("1", "=", 0);
            cmbavDynamicfiltersoperator2.addItem("2", ">", 0);
            if ( cmbavDynamicfiltersoperator2.ItemCount > 0 )
            {
               AV31DynamicFiltersOperator2 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV31DynamicFiltersOperator2), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV31DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV31DynamicFiltersOperator2), 4, 0)));
            }
            cmbavFuncaoapf_tipo2.Name = "vFUNCAOAPF_TIPO2";
            cmbavFuncaoapf_tipo2.WebTags = "";
            cmbavFuncaoapf_tipo2.addItem("", "Todos", 0);
            cmbavFuncaoapf_tipo2.addItem("EE", "EE", 0);
            cmbavFuncaoapf_tipo2.addItem("SE", "SE", 0);
            cmbavFuncaoapf_tipo2.addItem("CE", "CE", 0);
            cmbavFuncaoapf_tipo2.addItem("NM", "NM", 0);
            if ( cmbavFuncaoapf_tipo2.ItemCount > 0 )
            {
               AV33FuncaoAPF_Tipo2 = cmbavFuncaoapf_tipo2.getValidValue(AV33FuncaoAPF_Tipo2);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV33FuncaoAPF_Tipo2", AV33FuncaoAPF_Tipo2);
            }
            GXCCtl = "FUNCAOAPF_TIPO_" + sGXsfl_72_idx;
            cmbFuncaoAPF_Tipo.Name = GXCCtl;
            cmbFuncaoAPF_Tipo.WebTags = "";
            cmbFuncaoAPF_Tipo.addItem("", "(Nenhum)", 0);
            cmbFuncaoAPF_Tipo.addItem("EE", "EE", 0);
            cmbFuncaoAPF_Tipo.addItem("SE", "SE", 0);
            cmbFuncaoAPF_Tipo.addItem("CE", "CE", 0);
            cmbFuncaoAPF_Tipo.addItem("NM", "NM", 0);
            if ( cmbFuncaoAPF_Tipo.ItemCount > 0 )
            {
               A184FuncaoAPF_Tipo = cmbFuncaoAPF_Tipo.getValidValue(A184FuncaoAPF_Tipo);
            }
            GXCCtl = "FUNCAOAPF_COMPLEXIDADE_" + sGXsfl_72_idx;
            cmbFuncaoAPF_Complexidade.Name = GXCCtl;
            cmbFuncaoAPF_Complexidade.WebTags = "";
            cmbFuncaoAPF_Complexidade.addItem("E", "", 0);
            cmbFuncaoAPF_Complexidade.addItem("B", "Baixa", 0);
            cmbFuncaoAPF_Complexidade.addItem("M", "M�dia", 0);
            cmbFuncaoAPF_Complexidade.addItem("A", "Alta", 0);
            if ( cmbFuncaoAPF_Complexidade.ItemCount > 0 )
            {
               A185FuncaoAPF_Complexidade = cmbFuncaoAPF_Complexidade.getValidValue(A185FuncaoAPF_Complexidade);
            }
            GXCCtl = "vCOMDC_" + sGXsfl_72_idx;
            chkavComdc.Name = GXCCtl;
            chkavComdc.WebTags = "";
            chkavComdc.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkavComdc_Internalname, "TitleCaption", chkavComdc.Caption);
            chkavComdc.CheckedValue = "false";
            GXCCtl = "FUNCAOAPF_ATIVO_" + sGXsfl_72_idx;
            cmbFuncaoAPF_Ativo.Name = GXCCtl;
            cmbFuncaoAPF_Ativo.WebTags = "";
            cmbFuncaoAPF_Ativo.addItem("A", "Ativa", 0);
            cmbFuncaoAPF_Ativo.addItem("E", "Exclu�da", 0);
            cmbFuncaoAPF_Ativo.addItem("R", "Rejeitada", 0);
            if ( cmbFuncaoAPF_Ativo.ItemCount > 0 )
            {
               A183FuncaoAPF_Ativo = cmbFuncaoAPF_Ativo.getValidValue(A183FuncaoAPF_Ativo);
            }
            chkavDynamicfiltersenabled2.Name = "vDYNAMICFILTERSENABLED2";
            chkavDynamicfiltersenabled2.WebTags = "";
            chkavDynamicfiltersenabled2.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkavDynamicfiltersenabled2_Internalname, "TitleCaption", chkavDynamicfiltersenabled2.Caption);
            chkavDynamicfiltersenabled2.CheckedValue = "false";
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavOrderedby_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_722( ) ;
         while ( nGXsfl_72_idx <= nRC_GXsfl_72 )
         {
            sendrow_722( ) ;
            nGXsfl_72_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_72_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_72_idx+1));
            sGXsfl_72_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_72_idx), 4, 0)), 4, "0");
            SubsflControlProps_722( ) ;
         }
         context.GX_webresponse.AddString(GridContainer.ToJavascriptSource());
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       short AV13OrderedBy ,
                                       bool AV14OrderedDsc ,
                                       String AV25DynamicFiltersSelector1 ,
                                       short AV26DynamicFiltersOperator1 ,
                                       String AV27FuncaoAPF_Nome1 ,
                                       String AV28FuncaoAPF_Tipo1 ,
                                       decimal AV38FuncaoAPF_PF1 ,
                                       String AV30DynamicFiltersSelector2 ,
                                       short AV31DynamicFiltersOperator2 ,
                                       String AV32FuncaoAPF_Nome2 ,
                                       String AV33FuncaoAPF_Tipo2 ,
                                       decimal AV39FuncaoAPF_PF2 ,
                                       bool AV29DynamicFiltersEnabled2 ,
                                       int AV21FuncaoAPF_SistemaCod ,
                                       wwpbaseobjects.SdtWWPContext AV6WWPContext ,
                                       String AV90Pgmname ,
                                       wwpbaseobjects.SdtWWPGridState AV11GridState ,
                                       bool AV35DynamicFiltersIgnoreFirst ,
                                       bool AV34DynamicFiltersRemoving ,
                                       int A165FuncaoAPF_Codigo ,
                                       String A432FuncaoAPF_Link ,
                                       decimal A386FuncaoAPF_PF ,
                                       String A415FuncaoAPFAtributos_FuncaoDadosTip ,
                                       String sPrefix )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RF9A2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_FUNCAOAPF_CODIGO", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A165FuncaoAPF_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"FUNCAOAPF_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A165FuncaoAPF_Codigo), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_FUNCAOAPF_SISTEMACOD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A360FuncaoAPF_SistemaCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"FUNCAOAPF_SISTEMACOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A360FuncaoAPF_SistemaCod), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_FUNCAOAPF_NOME", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A166FuncaoAPF_Nome, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"FUNCAOAPF_NOME", A166FuncaoAPF_Nome);
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_FUNCAOAPF_TIPO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A184FuncaoAPF_Tipo, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"FUNCAOAPF_TIPO", StringUtil.RTrim( A184FuncaoAPF_Tipo));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_FUNCAOAPF_COMPLEXIDADE", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A185FuncaoAPF_Complexidade, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"FUNCAOAPF_COMPLEXIDADE", StringUtil.RTrim( A185FuncaoAPF_Complexidade));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_FUNCAOAPF_TD", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A388FuncaoAPF_TD), "ZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"FUNCAOAPF_TD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A388FuncaoAPF_TD), 4, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_FUNCAOAPF_AR", GetSecureSignedToken( sPrefix, context.localUtil.Format( (decimal)(A387FuncaoAPF_AR), "ZZZ9")));
         GxWebStd.gx_hidden_field( context, sPrefix+"FUNCAOAPF_AR", StringUtil.LTrim( StringUtil.NToC( (decimal)(A387FuncaoAPF_AR), 4, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_FUNCAOAPF_PF", GetSecureSignedToken( sPrefix, context.localUtil.Format( A386FuncaoAPF_PF, "ZZ,ZZZ,ZZ9.999")));
         GxWebStd.gx_hidden_field( context, sPrefix+"FUNCAOAPF_PF", StringUtil.LTrim( StringUtil.NToC( A386FuncaoAPF_PF, 14, 5, ".", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_FUNCAOAPF_ATIVO", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( A183FuncaoAPF_Ativo, ""))));
         GxWebStd.gx_hidden_field( context, sPrefix+"FUNCAOAPF_ATIVO", StringUtil.RTrim( A183FuncaoAPF_Ativo));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_FUNCAOAPF_SOLUCAOTECNICA", GetSecureSignedToken( sPrefix, A757FuncaoAPF_SolucaoTecnica));
         GxWebStd.gx_hidden_field( context, sPrefix+"FUNCAOAPF_SOLUCAOTECNICA", A757FuncaoAPF_SolucaoTecnica);
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbavOrderedby.ItemCount > 0 )
         {
            AV13OrderedBy = (short)(NumberUtil.Val( cmbavOrderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         }
         if ( cmbavDynamicfiltersselector1.ItemCount > 0 )
         {
            AV25DynamicFiltersSelector1 = cmbavDynamicfiltersselector1.getValidValue(AV25DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25DynamicFiltersSelector1", AV25DynamicFiltersSelector1);
         }
         if ( cmbavDynamicfiltersoperator1.ItemCount > 0 )
         {
            AV26DynamicFiltersOperator1 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator1.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator1), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator1), 4, 0)));
         }
         if ( cmbavFuncaoapf_tipo1.ItemCount > 0 )
         {
            AV28FuncaoAPF_Tipo1 = cmbavFuncaoapf_tipo1.getValidValue(AV28FuncaoAPF_Tipo1);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV28FuncaoAPF_Tipo1", AV28FuncaoAPF_Tipo1);
         }
         if ( cmbavDynamicfiltersselector2.ItemCount > 0 )
         {
            AV30DynamicFiltersSelector2 = cmbavDynamicfiltersselector2.getValidValue(AV30DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30DynamicFiltersSelector2", AV30DynamicFiltersSelector2);
         }
         if ( cmbavDynamicfiltersoperator2.ItemCount > 0 )
         {
            AV31DynamicFiltersOperator2 = (short)(NumberUtil.Val( cmbavDynamicfiltersoperator2.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV31DynamicFiltersOperator2), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV31DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV31DynamicFiltersOperator2), 4, 0)));
         }
         if ( cmbavFuncaoapf_tipo2.ItemCount > 0 )
         {
            AV33FuncaoAPF_Tipo2 = cmbavFuncaoapf_tipo2.getValidValue(AV33FuncaoAPF_Tipo2);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV33FuncaoAPF_Tipo2", AV33FuncaoAPF_Tipo2);
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RF9A2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV90Pgmname = "SistemaFuncaopAPFWC";
         context.Gx_err = 0;
         chkavComdc.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkavComdc_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavComdc.Enabled), 5, 0)));
      }

      protected void RF9A2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 72;
         /* Execute user event: E229A2 */
         E229A2 ();
         nGXsfl_72_idx = 1;
         sGXsfl_72_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_72_idx), 4, 0)), 4, "0");
         SubsflControlProps_722( ) ;
         nGXsfl_72_Refreshing = 1;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", sPrefix);
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_722( ) ;
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 AV25DynamicFiltersSelector1 ,
                                                 AV27FuncaoAPF_Nome1 ,
                                                 AV28FuncaoAPF_Tipo1 ,
                                                 AV29DynamicFiltersEnabled2 ,
                                                 AV30DynamicFiltersSelector2 ,
                                                 AV32FuncaoAPF_Nome2 ,
                                                 AV33FuncaoAPF_Tipo2 ,
                                                 A166FuncaoAPF_Nome ,
                                                 A184FuncaoAPF_Tipo ,
                                                 AV13OrderedBy ,
                                                 AV14OrderedDsc ,
                                                 A360FuncaoAPF_SistemaCod ,
                                                 AV21FuncaoAPF_SistemaCod ,
                                                 AV26DynamicFiltersOperator1 ,
                                                 AV38FuncaoAPF_PF1 ,
                                                 A386FuncaoAPF_PF ,
                                                 AV31DynamicFiltersOperator2 ,
                                                 AV39FuncaoAPF_PF2 ,
                                                 A183FuncaoAPF_Ativo },
                                                 new int[] {
                                                 TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT,
                                                 TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.SHORT, TypeConstants.DECIMAL, TypeConstants.DECIMAL, TypeConstants.SHORT, TypeConstants.DECIMAL, TypeConstants.STRING
                                                 }
            });
            lV27FuncaoAPF_Nome1 = StringUtil.Concat( StringUtil.RTrim( AV27FuncaoAPF_Nome1), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27FuncaoAPF_Nome1", AV27FuncaoAPF_Nome1);
            lV32FuncaoAPF_Nome2 = StringUtil.Concat( StringUtil.RTrim( AV32FuncaoAPF_Nome2), "%", "");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV32FuncaoAPF_Nome2", AV32FuncaoAPF_Nome2);
            /* Using cursor H009A2 */
            pr_default.execute(0, new Object[] {AV21FuncaoAPF_SistemaCod, lV27FuncaoAPF_Nome1, AV28FuncaoAPF_Tipo1, lV32FuncaoAPF_Nome2, AV33FuncaoAPF_Tipo2});
            nGXsfl_72_idx = 1;
            GRID_nEOF = 0;
            GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < GRID_nFirstRecordOnPage + subGrid_Recordsperpage( ) ) ) ) )
            {
               A432FuncaoAPF_Link = H009A2_A432FuncaoAPF_Link[0];
               n432FuncaoAPF_Link = H009A2_n432FuncaoAPF_Link[0];
               A183FuncaoAPF_Ativo = H009A2_A183FuncaoAPF_Ativo[0];
               A184FuncaoAPF_Tipo = H009A2_A184FuncaoAPF_Tipo[0];
               A166FuncaoAPF_Nome = H009A2_A166FuncaoAPF_Nome[0];
               A360FuncaoAPF_SistemaCod = H009A2_A360FuncaoAPF_SistemaCod[0];
               n360FuncaoAPF_SistemaCod = H009A2_n360FuncaoAPF_SistemaCod[0];
               A165FuncaoAPF_Codigo = H009A2_A165FuncaoAPF_Codigo[0];
               A744FuncaoAPF_MelhoraCod = H009A2_A744FuncaoAPF_MelhoraCod[0];
               n744FuncaoAPF_MelhoraCod = H009A2_n744FuncaoAPF_MelhoraCod[0];
               GXt_char1 = A757FuncaoAPF_SolucaoTecnica;
               GXt_int2 = 0;
               new prc_solucaotecnica(context ).execute( ref  A744FuncaoAPF_MelhoraCod, ref  GXt_int2,  "T", out  GXt_char1) ;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A744FuncaoAPF_MelhoraCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A744FuncaoAPF_MelhoraCod), 6, 0)));
               A757FuncaoAPF_SolucaoTecnica = GXt_char1;
               GXt_char1 = A185FuncaoAPF_Complexidade;
               new prc_fapfcomplexidade(context ).execute( ref  A165FuncaoAPF_Codigo, ref  GXt_char1) ;
               A185FuncaoAPF_Complexidade = GXt_char1;
               GXt_int3 = A388FuncaoAPF_TD;
               new prc_funcaoapf_td(context ).execute(  A165FuncaoAPF_Codigo, out  GXt_int3) ;
               A388FuncaoAPF_TD = GXt_int3;
               GXt_int3 = A387FuncaoAPF_AR;
               new prc_funcaoapf_ar(context ).execute( ref  A165FuncaoAPF_Codigo, ref  GXt_int3) ;
               A387FuncaoAPF_AR = GXt_int3;
               GXt_decimal4 = A386FuncaoAPF_PF;
               new prc_fapfpf(context ).execute( ref  A165FuncaoAPF_Codigo, ref  GXt_decimal4) ;
               A386FuncaoAPF_PF = GXt_decimal4;
               if ( ! ( ( StringUtil.StrCmp(AV25DynamicFiltersSelector1, "FUNCAOAPF_PF") == 0 ) && ( AV26DynamicFiltersOperator1 == 0 ) && ( ! (Convert.ToDecimal(0)==AV38FuncaoAPF_PF1) ) ) || ( ( A386FuncaoAPF_PF < AV38FuncaoAPF_PF1 ) ) )
               {
                  if ( ! ( ( StringUtil.StrCmp(AV25DynamicFiltersSelector1, "FUNCAOAPF_PF") == 0 ) && ( AV26DynamicFiltersOperator1 == 1 ) && ( ! (Convert.ToDecimal(0)==AV38FuncaoAPF_PF1) ) ) || ( ( A386FuncaoAPF_PF == AV38FuncaoAPF_PF1 ) ) )
                  {
                     if ( ! ( ( StringUtil.StrCmp(AV25DynamicFiltersSelector1, "FUNCAOAPF_PF") == 0 ) && ( AV26DynamicFiltersOperator1 == 2 ) && ( ! (Convert.ToDecimal(0)==AV38FuncaoAPF_PF1) ) ) || ( ( A386FuncaoAPF_PF > AV38FuncaoAPF_PF1 ) ) )
                     {
                        if ( ! ( AV29DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV30DynamicFiltersSelector2, "FUNCAOAPF_PF") == 0 ) && ( AV31DynamicFiltersOperator2 == 0 ) && ( ! (Convert.ToDecimal(0)==AV39FuncaoAPF_PF2) ) ) || ( ( A386FuncaoAPF_PF < AV39FuncaoAPF_PF2 ) ) )
                        {
                           if ( ! ( AV29DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV30DynamicFiltersSelector2, "FUNCAOAPF_PF") == 0 ) && ( AV31DynamicFiltersOperator2 == 1 ) && ( ! (Convert.ToDecimal(0)==AV39FuncaoAPF_PF2) ) ) || ( ( A386FuncaoAPF_PF == AV39FuncaoAPF_PF2 ) ) )
                           {
                              if ( ! ( AV29DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV30DynamicFiltersSelector2, "FUNCAOAPF_PF") == 0 ) && ( AV31DynamicFiltersOperator2 == 2 ) && ( ! (Convert.ToDecimal(0)==AV39FuncaoAPF_PF2) ) ) || ( ( A386FuncaoAPF_PF > AV39FuncaoAPF_PF2 ) ) )
                              {
                                 /* Execute user event: E239A2 */
                                 E239A2 ();
                              }
                           }
                        }
                     }
                  }
               }
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 72;
            WB9A0( ) ;
         }
         nGXsfl_72_Refreshing = 0;
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         return (int)(-1) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(-1) ;
      }

      protected short subgrid_firstpage( )
      {
         GRID_nFirstRecordOnPage = 0;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV25DynamicFiltersSelector1, AV26DynamicFiltersOperator1, AV27FuncaoAPF_Nome1, AV28FuncaoAPF_Tipo1, AV38FuncaoAPF_PF1, AV30DynamicFiltersSelector2, AV31DynamicFiltersOperator2, AV32FuncaoAPF_Nome2, AV33FuncaoAPF_Tipo2, AV39FuncaoAPF_PF2, AV29DynamicFiltersEnabled2, AV21FuncaoAPF_SistemaCod, AV6WWPContext, AV90Pgmname, AV11GridState, AV35DynamicFiltersIgnoreFirst, AV34DynamicFiltersRemoving, A165FuncaoAPF_Codigo, A432FuncaoAPF_Link, A386FuncaoAPF_PF, A415FuncaoAPFAtributos_FuncaoDadosTip, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         if ( GRID_nEOF == 0 )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV25DynamicFiltersSelector1, AV26DynamicFiltersOperator1, AV27FuncaoAPF_Nome1, AV28FuncaoAPF_Tipo1, AV38FuncaoAPF_PF1, AV30DynamicFiltersSelector2, AV31DynamicFiltersOperator2, AV32FuncaoAPF_Nome2, AV33FuncaoAPF_Tipo2, AV39FuncaoAPF_PF2, AV29DynamicFiltersEnabled2, AV21FuncaoAPF_SistemaCod, AV6WWPContext, AV90Pgmname, AV11GridState, AV35DynamicFiltersIgnoreFirst, AV34DynamicFiltersRemoving, A165FuncaoAPF_Codigo, A432FuncaoAPF_Link, A386FuncaoAPF_PF, A415FuncaoAPFAtributos_FuncaoDadosTip, sPrefix) ;
         }
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV25DynamicFiltersSelector1, AV26DynamicFiltersOperator1, AV27FuncaoAPF_Nome1, AV28FuncaoAPF_Tipo1, AV38FuncaoAPF_PF1, AV30DynamicFiltersSelector2, AV31DynamicFiltersOperator2, AV32FuncaoAPF_Nome2, AV33FuncaoAPF_Tipo2, AV39FuncaoAPF_PF2, AV29DynamicFiltersEnabled2, AV21FuncaoAPF_SistemaCod, AV6WWPContext, AV90Pgmname, AV11GridState, AV35DynamicFiltersIgnoreFirst, AV34DynamicFiltersRemoving, A165FuncaoAPF_Codigo, A432FuncaoAPF_Link, A386FuncaoAPF_PF, A415FuncaoAPFAtributos_FuncaoDadosTip, sPrefix) ;
         }
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         subGrid_Islastpage = 1;
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV25DynamicFiltersSelector1, AV26DynamicFiltersOperator1, AV27FuncaoAPF_Nome1, AV28FuncaoAPF_Tipo1, AV38FuncaoAPF_PF1, AV30DynamicFiltersSelector2, AV31DynamicFiltersOperator2, AV32FuncaoAPF_Nome2, AV33FuncaoAPF_Tipo2, AV39FuncaoAPF_PF2, AV29DynamicFiltersEnabled2, AV21FuncaoAPF_SistemaCod, AV6WWPContext, AV90Pgmname, AV11GridState, AV35DynamicFiltersIgnoreFirst, AV34DynamicFiltersRemoving, A165FuncaoAPF_Codigo, A432FuncaoAPF_Link, A386FuncaoAPF_PF, A415FuncaoAPFAtributos_FuncaoDadosTip, sPrefix) ;
         }
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV25DynamicFiltersSelector1, AV26DynamicFiltersOperator1, AV27FuncaoAPF_Nome1, AV28FuncaoAPF_Tipo1, AV38FuncaoAPF_PF1, AV30DynamicFiltersSelector2, AV31DynamicFiltersOperator2, AV32FuncaoAPF_Nome2, AV33FuncaoAPF_Tipo2, AV39FuncaoAPF_PF2, AV29DynamicFiltersEnabled2, AV21FuncaoAPF_SistemaCod, AV6WWPContext, AV90Pgmname, AV11GridState, AV35DynamicFiltersIgnoreFirst, AV34DynamicFiltersRemoving, A165FuncaoAPF_Codigo, A432FuncaoAPF_Link, A386FuncaoAPF_PF, A415FuncaoAPFAtributos_FuncaoDadosTip, sPrefix) ;
         }
         return (int)(0) ;
      }

      protected void STRUP9A0( )
      {
         /* Before Start, stand alone formulas. */
         AV90Pgmname = "SistemaFuncaopAPFWC";
         context.Gx_err = 0;
         chkavComdc.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkavComdc_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavComdc.Enabled), 5, 0)));
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E219A2 */
         E219A2 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            cmbavOrderedby.Name = cmbavOrderedby_Internalname;
            cmbavOrderedby.CurrentValue = cgiGet( cmbavOrderedby_Internalname);
            AV13OrderedBy = (short)(NumberUtil.Val( cgiGet( cmbavOrderedby_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
            AV14OrderedDsc = StringUtil.StrToBool( cgiGet( edtavOrdereddsc_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
            cmbavDynamicfiltersselector1.Name = cmbavDynamicfiltersselector1_Internalname;
            cmbavDynamicfiltersselector1.CurrentValue = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            AV25DynamicFiltersSelector1 = cgiGet( cmbavDynamicfiltersselector1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25DynamicFiltersSelector1", AV25DynamicFiltersSelector1);
            cmbavDynamicfiltersoperator1.Name = cmbavDynamicfiltersoperator1_Internalname;
            cmbavDynamicfiltersoperator1.CurrentValue = cgiGet( cmbavDynamicfiltersoperator1_Internalname);
            AV26DynamicFiltersOperator1 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator1_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator1), 4, 0)));
            AV27FuncaoAPF_Nome1 = cgiGet( edtavFuncaoapf_nome1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27FuncaoAPF_Nome1", AV27FuncaoAPF_Nome1);
            cmbavFuncaoapf_tipo1.Name = cmbavFuncaoapf_tipo1_Internalname;
            cmbavFuncaoapf_tipo1.CurrentValue = cgiGet( cmbavFuncaoapf_tipo1_Internalname);
            AV28FuncaoAPF_Tipo1 = cgiGet( cmbavFuncaoapf_tipo1_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV28FuncaoAPF_Tipo1", AV28FuncaoAPF_Tipo1);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavFuncaoapf_pf1_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavFuncaoapf_pf1_Internalname), ",", ".") > 99999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vFUNCAOAPF_PF1");
               GX_FocusControl = edtavFuncaoapf_pf1_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV38FuncaoAPF_PF1 = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV38FuncaoAPF_PF1", StringUtil.LTrim( StringUtil.Str( AV38FuncaoAPF_PF1, 14, 5)));
            }
            else
            {
               AV38FuncaoAPF_PF1 = context.localUtil.CToN( cgiGet( edtavFuncaoapf_pf1_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV38FuncaoAPF_PF1", StringUtil.LTrim( StringUtil.Str( AV38FuncaoAPF_PF1, 14, 5)));
            }
            cmbavDynamicfiltersselector2.Name = cmbavDynamicfiltersselector2_Internalname;
            cmbavDynamicfiltersselector2.CurrentValue = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            AV30DynamicFiltersSelector2 = cgiGet( cmbavDynamicfiltersselector2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30DynamicFiltersSelector2", AV30DynamicFiltersSelector2);
            cmbavDynamicfiltersoperator2.Name = cmbavDynamicfiltersoperator2_Internalname;
            cmbavDynamicfiltersoperator2.CurrentValue = cgiGet( cmbavDynamicfiltersoperator2_Internalname);
            AV31DynamicFiltersOperator2 = (short)(NumberUtil.Val( cgiGet( cmbavDynamicfiltersoperator2_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV31DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV31DynamicFiltersOperator2), 4, 0)));
            AV32FuncaoAPF_Nome2 = cgiGet( edtavFuncaoapf_nome2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV32FuncaoAPF_Nome2", AV32FuncaoAPF_Nome2);
            cmbavFuncaoapf_tipo2.Name = cmbavFuncaoapf_tipo2_Internalname;
            cmbavFuncaoapf_tipo2.CurrentValue = cgiGet( cmbavFuncaoapf_tipo2_Internalname);
            AV33FuncaoAPF_Tipo2 = cgiGet( cmbavFuncaoapf_tipo2_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV33FuncaoAPF_Tipo2", AV33FuncaoAPF_Tipo2);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavFuncaoapf_pf2_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavFuncaoapf_pf2_Internalname), ",", ".") > 99999999.99999m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vFUNCAOAPF_PF2");
               GX_FocusControl = edtavFuncaoapf_pf2_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV39FuncaoAPF_PF2 = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV39FuncaoAPF_PF2", StringUtil.LTrim( StringUtil.Str( AV39FuncaoAPF_PF2, 14, 5)));
            }
            else
            {
               AV39FuncaoAPF_PF2 = context.localUtil.CToN( cgiGet( edtavFuncaoapf_pf2_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV39FuncaoAPF_PF2", StringUtil.LTrim( StringUtil.Str( AV39FuncaoAPF_PF2, 14, 5)));
            }
            AV29DynamicFiltersEnabled2 = StringUtil.StrToBool( cgiGet( chkavDynamicfiltersenabled2_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29DynamicFiltersEnabled2", AV29DynamicFiltersEnabled2);
            /* Read saved values. */
            nRC_GXsfl_72 = (short)(context.localUtil.CToN( cgiGet( sPrefix+"nRC_GXsfl_72"), ",", "."));
            AV77GridCurrentPage = (long)(context.localUtil.CToN( cgiGet( sPrefix+"vGRIDCURRENTPAGE"), ",", "."));
            AV78GridPageCount = (long)(context.localUtil.CToN( cgiGet( sPrefix+"vGRIDPAGECOUNT"), ",", "."));
            wcpOAV21FuncaoAPF_SistemaCod = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV21FuncaoAPF_SistemaCod"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( sPrefix+"GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            Gridpaginationbar_Class = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Class");
            Gridpaginationbar_First = cgiGet( sPrefix+"GRIDPAGINATIONBAR_First");
            Gridpaginationbar_Previous = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Previous");
            Gridpaginationbar_Next = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Next");
            Gridpaginationbar_Last = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Last");
            Gridpaginationbar_Caption = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Caption");
            Gridpaginationbar_Showfirst = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Showfirst"));
            Gridpaginationbar_Showprevious = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Showprevious"));
            Gridpaginationbar_Shownext = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Shownext"));
            Gridpaginationbar_Showlast = StringUtil.StrToBool( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Showlast"));
            Gridpaginationbar_Pagestoshow = (int)(context.localUtil.CToN( cgiGet( sPrefix+"GRIDPAGINATIONBAR_Pagestoshow"), ",", "."));
            Gridpaginationbar_Pagingbuttonsposition = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Pagingbuttonsposition");
            Gridpaginationbar_Pagingcaptionposition = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Pagingcaptionposition");
            Gridpaginationbar_Emptygridclass = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Emptygridclass");
            Gridpaginationbar_Emptygridcaption = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Emptygridcaption");
            Gridpaginationbar_Selectedpage = cgiGet( sPrefix+"GRIDPAGINATIONBAR_Selectedpage");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vORDEREDBY"), ",", ".") != Convert.ToDecimal( AV13OrderedBy )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vORDEREDDSC")) != AV14OrderedDsc )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vDYNAMICFILTERSSELECTOR1"), AV25DynamicFiltersSelector1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vDYNAMICFILTERSOPERATOR1"), ",", ".") != Convert.ToDecimal( AV26DynamicFiltersOperator1 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vFUNCAOAPF_NOME1"), AV27FuncaoAPF_Nome1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vFUNCAOAPF_TIPO1"), AV28FuncaoAPF_Tipo1) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vFUNCAOAPF_PF1"), ",", ".") != AV38FuncaoAPF_PF1 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vDYNAMICFILTERSSELECTOR2"), AV30DynamicFiltersSelector2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vDYNAMICFILTERSOPERATOR2"), ",", ".") != Convert.ToDecimal( AV31DynamicFiltersOperator2 )) )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vFUNCAOAPF_NOME2"), AV32FuncaoAPF_Nome2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( sPrefix+"GXH_vFUNCAOAPF_TIPO2"), AV33FuncaoAPF_Tipo2) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToN( cgiGet( sPrefix+"GXH_vFUNCAOAPF_PF2"), ",", ".") != AV39FuncaoAPF_PF2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToBool( cgiGet( sPrefix+"GXH_vDYNAMICFILTERSENABLED2")) != AV29DynamicFiltersEnabled2 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E219A2 */
         E219A2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E219A2( )
      {
         /* Start Routine */
         subGrid_Rows = 50;
         GxWebStd.gx_hidden_field( context, sPrefix+"GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         lblJsdynamicfilters_Caption = "";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         chkavDynamicfiltersenabled2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, chkavDynamicfiltersenabled2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavDynamicfiltersenabled2.Visible), 5, 0)));
         AV28FuncaoAPF_Tipo1 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV28FuncaoAPF_Tipo1", AV28FuncaoAPF_Tipo1);
         AV25DynamicFiltersSelector1 = "FUNCAOAPF_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25DynamicFiltersSelector1", AV25DynamicFiltersSelector1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV33FuncaoAPF_Tipo2 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV33FuncaoAPF_Tipo2", AV33FuncaoAPF_Tipo2);
         AV30DynamicFiltersSelector2 = "FUNCAOAPF_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30DynamicFiltersSelector2", AV30DynamicFiltersSelector2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         imgAdddynamicfilters1_Jsonclick = "WWPDynFilterShow(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgAdddynamicfilters1_Internalname, "Jsonclick", imgAdddynamicfilters1_Jsonclick);
         imgRemovedynamicfilters1_Jsonclick = "WWPDynFilterHideLast(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters1_Internalname, "Jsonclick", imgRemovedynamicfilters1_Jsonclick);
         imgRemovedynamicfilters2_Jsonclick = "WWPDynFilterHideLast(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters2_Internalname, "Jsonclick", imgRemovedynamicfilters2_Jsonclick);
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbavOrderedby.removeAllItems();
         cmbavOrderedby.addItem("1", "Cronologicamente", 0);
         cmbavOrderedby.addItem("2", "Nome", 0);
         cmbavOrderedby.addItem("3", "Tipo", 0);
         cmbavOrderedby.addItem("4", "Status da Fun��o", 0);
         if ( AV13OrderedBy < 1 )
         {
            AV13OrderedBy = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         }
         edtavOrdereddsc_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavOrdereddsc_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavOrdereddsc_Visible), 5, 0)));
         imgCleanfilters_Jsonclick = "WWPDynFilterHideAll(2)";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgCleanfilters_Internalname, "Jsonclick", imgCleanfilters_Jsonclick);
         /* Using cursor H009A3 */
         pr_default.execute(1, new Object[] {AV21FuncaoAPF_SistemaCod});
         while ( (pr_default.getStatus(1) != 101) )
         {
            A127Sistema_Codigo = H009A3_A127Sistema_Codigo[0];
            A699Sistema_Tipo = H009A3_A699Sistema_Tipo[0];
            n699Sistema_Tipo = H009A3_n699Sistema_Tipo[0];
            A129Sistema_Sigla = H009A3_A129Sistema_Sigla[0];
            edtFuncaoAPF_SolucaoTecnica_Visible = (((StringUtil.StrCmp(A699Sistema_Tipo, "M")==0)) ? 1 : 0);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtFuncaoAPF_SolucaoTecnica_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtFuncaoAPF_SolucaoTecnica_Visible), 5, 0)));
            AV40Sistema_Sigla = A129Sistema_Sigla;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV40Sistema_Sigla", AV40Sistema_Sigla);
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(1);
      }

      protected void E229A2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         /* Execute user subroutine: 'CHECKSECURITYFORACTIONS' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtFuncaoAPF_Nome_Titleformat = 2;
         edtFuncaoAPF_Nome_Title = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 2);' >%5</span>", ((AV13OrderedBy==2) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Nome", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtFuncaoAPF_Nome_Internalname, "Title", edtFuncaoAPF_Nome_Title);
         cmbFuncaoAPF_Tipo_Titleformat = 2;
         cmbFuncaoAPF_Tipo.Title.Text = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 3);' >%5</span>", ((AV13OrderedBy==3) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Tipo", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbFuncaoAPF_Tipo_Internalname, "Title", cmbFuncaoAPF_Tipo.Title.Text);
         cmbFuncaoAPF_Ativo_Titleformat = 2;
         cmbFuncaoAPF_Ativo.Title.Text = StringUtil.Format( "<span class='%1' onclick='WWPSortColumn(%2%3%2, %2%4%2, 4);' >%5</span>", ((AV13OrderedBy==4) ? (AV14OrderedDsc ? "GridTitleSortedDsc" : "GridTitleSortedAsc") : "GridTitleSortable"), "\"", edtavOrdereddsc_Internalname, cmbavOrderedby_Internalname, "Status da Fun��o", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbFuncaoAPF_Ativo_Internalname, "Title", cmbFuncaoAPF_Ativo.Title.Text);
         AV77GridCurrentPage = subGrid_Currentpage( );
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV77GridCurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV77GridCurrentPage), 10, 0)));
         AV78GridPageCount = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV78GridPageCount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV78GridPageCount), 10, 0)));
         imgInsert_Visible = (AV6WWPContext.gxTpr_Insert ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgInsert_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgInsert_Visible), 5, 0)));
         edtavUpdate_Visible = (AV6WWPContext.gxTpr_Update ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavUpdate_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavUpdate_Visible), 5, 0)));
         edtavDelete_Visible = (AV6WWPContext.gxTpr_Delete ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDelete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDelete_Visible), 5, 0)));
         edtavDisplay_Visible = (AV6WWPContext.gxTpr_Delete ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavDisplay_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDisplay_Visible), 5, 0)));
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV6WWPContext", AV6WWPContext);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV11GridState", AV11GridState);
      }

      protected void E119A2( )
      {
         /* Gridpaginationbar_Changepage Routine */
         if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Previous") == 0 )
         {
            subgrid_previouspage( ) ;
         }
         else if ( StringUtil.StrCmp(Gridpaginationbar_Selectedpage, "Next") == 0 )
         {
            subgrid_nextpage( ) ;
         }
         else
         {
            AV76PageToGo = (int)(NumberUtil.Val( Gridpaginationbar_Selectedpage, "."));
            subgrid_gotopage( AV76PageToGo) ;
         }
      }

      private void E239A2( )
      {
         /* Grid_Load Routine */
         AV23ImagemFuncoesUsuario = context.GetImagePath( "6591e2a3-49b6-43b7-b8e3-a292564a32a4", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavImagemfuncoesusuario_Internalname, AV23ImagemFuncoesUsuario);
         AV84Imagemfuncoesusuario_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "6591e2a3-49b6-43b7-b8e3-a292564a32a4", "", context.GetTheme( )));
         edtavImagemfuncoesusuario_Tooltiptext = "Associar esta Fun��o de Transa��o com Fun��es de Usu�rio";
         edtavUpdate_Tooltiptext = "Modifica";
         if ( ( AV6WWPContext.gxTpr_Insert && AV6WWPContext.gxTpr_Update && AV6WWPContext.gxTpr_Delete ) || AV6WWPContext.gxTpr_Userehadministradorgam )
         {
            edtavUpdate_Link = formatLink("funcaoapf.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode("" +A165FuncaoAPF_Codigo) + "," + UrlEncode("" +AV21FuncaoAPF_SistemaCod);
            AV17Update = context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavUpdate_Internalname, AV17Update);
            AV85Update_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( )));
            edtavUpdate_Enabled = 1;
         }
         else
         {
            edtavUpdate_Link = "";
            AV17Update = context.GetImagePath( "0211baea-a1e2-4bb5-849c-f3afb69b09ee", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavUpdate_Internalname, AV17Update);
            AV85Update_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "0211baea-a1e2-4bb5-849c-f3afb69b09ee", "", context.GetTheme( )));
            edtavUpdate_Enabled = 0;
         }
         edtavDelete_Tooltiptext = "Eliminar";
         if ( ( AV6WWPContext.gxTpr_Insert && AV6WWPContext.gxTpr_Update && AV6WWPContext.gxTpr_Delete ) || AV6WWPContext.gxTpr_Userehadministradorgam )
         {
            edtavDelete_Link = formatLink("funcaoapf.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode("" +A165FuncaoAPF_Codigo) + "," + UrlEncode("" +AV21FuncaoAPF_SistemaCod);
            AV18Delete = context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavDelete_Internalname, AV18Delete);
            AV86Delete_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )));
            edtavDelete_Enabled = 1;
         }
         else
         {
            edtavDelete_Link = "";
            AV18Delete = context.GetImagePath( "138fdfa8-ae9b-4664-8007-e04d47468418", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavDelete_Internalname, AV18Delete);
            AV86Delete_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "138fdfa8-ae9b-4664-8007-e04d47468418", "", context.GetTheme( )));
            edtavDelete_Enabled = 0;
         }
         AV19Display = context.GetImagePath( "f11923b6-6acd-4a79-bfc0-0cfc6f3bced5", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavDisplay_Internalname, AV19Display);
         AV87Display_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "f11923b6-6acd-4a79-bfc0-0cfc6f3bced5", "", context.GetTheme( )));
         edtavDisplay_Tooltiptext = "Mostrar";
         edtavDisplay_Link = formatLink("viewfuncaoapf.aspx") + "?" + UrlEncode("" +A165FuncaoAPF_Codigo) + "," + UrlEncode("" +AV21FuncaoAPF_SistemaCod) + "," + UrlEncode(StringUtil.RTrim(""));
         AV17Update = context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavUpdate_Internalname, AV17Update);
         AV85Update_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7c63c2b9-483e-4035-b512-febf9186a274", "", context.GetTheme( )));
         AV18Delete = context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavDelete_Internalname, AV18Delete);
         AV86Delete_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "7695fe89-52c9-4b7e-871e-0e11548f823e", "", context.GetTheme( )));
         AV19Display = context.GetImagePath( "f11923b6-6acd-4a79-bfc0-0cfc6f3bced5", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavDisplay_Internalname, AV19Display);
         AV87Display_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "f11923b6-6acd-4a79-bfc0-0cfc6f3bced5", "", context.GetTheme( )));
         if ( String.IsNullOrEmpty(StringUtil.RTrim( A432FuncaoAPF_Link)) )
         {
            AV37FuncaoAPF_Link = "";
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavFuncaoapf_link_Internalname, AV37FuncaoAPF_Link);
            AV88Funcaoapf_link_GXI = "";
            edtavFuncaoapf_link_Tooltiptext = "";
         }
         else
         {
            if ( StringUtil.StringSearch( StringUtil.Lower( A432FuncaoAPF_Link), "www", 1) > 0 )
            {
               if ( StringUtil.StringSearch( A432FuncaoAPF_Link, "http", 1) == 0 )
               {
                  edtavFuncaoapf_link_Link = "http://"+formatLink(A432FuncaoAPF_Link) ;
               }
               else
               {
                  edtavFuncaoapf_link_Link = formatLink(A432FuncaoAPF_Link) ;
               }
               AV37FuncaoAPF_Link = context.GetImagePath( "0f126d8c-68c3-4aea-a745-d86cfc93d63c", "", context.GetTheme( ));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavFuncaoapf_link_Internalname, AV37FuncaoAPF_Link);
               AV88Funcaoapf_link_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "0f126d8c-68c3-4aea-a745-d86cfc93d63c", "", context.GetTheme( )));
               edtavFuncaoapf_link_Linktarget = "_blank";
               edtavFuncaoapf_link_Tooltiptext = A432FuncaoAPF_Link;
            }
            else
            {
               AV37FuncaoAPF_Link = context.GetImagePath( "8a14b909-b0d5-4e04-8371-c6046e78d84f", "", context.GetTheme( ));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavFuncaoapf_link_Internalname, AV37FuncaoAPF_Link);
               AV88Funcaoapf_link_GXI = GeneXus.Utils.GXDbFile.PathToUrl( context.GetImagePath( "8a14b909-b0d5-4e04-8371-c6046e78d84f", "", context.GetTheme( )));
               edtavFuncaoapf_link_Link = "";
            }
            edtavFuncaoapf_link_Tooltiptext = A432FuncaoAPF_Link;
         }
         AV36ComDC = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, chkavComdc_Internalname, AV36ComDC);
         if ( (Convert.ToDecimal(0)==A386FuncaoAPF_PF) )
         {
            /* Using cursor H009A4 */
            pr_default.execute(2, new Object[] {A165FuncaoAPF_Codigo});
            while ( (pr_default.getStatus(2) != 101) )
            {
               A378FuncaoAPFAtributos_FuncaoDadosCod = H009A4_A378FuncaoAPFAtributos_FuncaoDadosCod[0];
               n378FuncaoAPFAtributos_FuncaoDadosCod = H009A4_n378FuncaoAPFAtributos_FuncaoDadosCod[0];
               A415FuncaoAPFAtributos_FuncaoDadosTip = H009A4_A415FuncaoAPFAtributos_FuncaoDadosTip[0];
               n415FuncaoAPFAtributos_FuncaoDadosTip = H009A4_n415FuncaoAPFAtributos_FuncaoDadosTip[0];
               A415FuncaoAPFAtributos_FuncaoDadosTip = H009A4_A415FuncaoAPFAtributos_FuncaoDadosTip[0];
               n415FuncaoAPFAtributos_FuncaoDadosTip = H009A4_n415FuncaoAPFAtributos_FuncaoDadosTip[0];
               AV36ComDC = true;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, chkavComdc_Internalname, AV36ComDC);
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               pr_default.readNext(2);
            }
            pr_default.close(2);
         }
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 72;
         }
         if ( ( subGrid_Islastpage == 1 ) || ( subGrid_Rows == 0 ) || ( ( GRID_nCurrentRecord >= GRID_nFirstRecordOnPage ) && ( GRID_nCurrentRecord < GRID_nFirstRecordOnPage + subGrid_Recordsperpage( ) ) ) )
         {
            sendrow_722( ) ;
            GRID_nEOF = 1;
            GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            if ( ( subGrid_Islastpage == 1 ) && ( ((int)((GRID_nCurrentRecord) % (subGrid_Recordsperpage( )))) == 0 ) )
            {
               GRID_nFirstRecordOnPage = GRID_nCurrentRecord;
            }
         }
         if ( GRID_nCurrentRecord >= GRID_nFirstRecordOnPage + subGrid_Recordsperpage( ) )
         {
            GRID_nEOF = 0;
            GxWebStd.gx_hidden_field( context, sPrefix+"GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
         }
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ( nGXsfl_72_Refreshing == 0 ) )
         {
            context.DoAjaxLoad(72, GridRow);
         }
      }

      protected void E129A2( )
      {
         /* Orderedby_Click Routine */
         context.DoAjaxRefreshCmp(sPrefix);
      }

      protected void E189A2( )
      {
         /* 'AddDynamicFilters1' Routine */
         AV29DynamicFiltersEnabled2 = true;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29DynamicFiltersEnabled2", AV29DynamicFiltersEnabled2);
         imgAdddynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
      }

      protected void E139A2( )
      {
         /* 'RemoveDynamicFilters1' Routine */
         AV34DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV34DynamicFiltersRemoving", AV34DynamicFiltersRemoving);
         AV35DynamicFiltersIgnoreFirst = true;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV35DynamicFiltersIgnoreFirst", AV35DynamicFiltersIgnoreFirst);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV34DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV34DynamicFiltersRemoving", AV34DynamicFiltersRemoving);
         AV35DynamicFiltersIgnoreFirst = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV35DynamicFiltersIgnoreFirst", AV35DynamicFiltersIgnoreFirst);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV25DynamicFiltersSelector1, AV26DynamicFiltersOperator1, AV27FuncaoAPF_Nome1, AV28FuncaoAPF_Tipo1, AV38FuncaoAPF_PF1, AV30DynamicFiltersSelector2, AV31DynamicFiltersOperator2, AV32FuncaoAPF_Nome2, AV33FuncaoAPF_Tipo2, AV39FuncaoAPF_PF2, AV29DynamicFiltersEnabled2, AV21FuncaoAPF_SistemaCod, AV6WWPContext, AV90Pgmname, AV11GridState, AV35DynamicFiltersIgnoreFirst, AV34DynamicFiltersRemoving, A165FuncaoAPF_Codigo, A432FuncaoAPF_Link, A386FuncaoAPF_PF, A415FuncaoAPFAtributos_FuncaoDadosTip, sPrefix) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV11GridState", AV11GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV30DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV25DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavFuncaoapf_tipo1.CurrentValue = StringUtil.RTrim( AV28FuncaoAPF_Tipo1);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavFuncaoapf_tipo1_Internalname, "Values", cmbavFuncaoapf_tipo1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         cmbavFuncaoapf_tipo2.CurrentValue = StringUtil.RTrim( AV33FuncaoAPF_Tipo2);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavFuncaoapf_tipo2_Internalname, "Values", cmbavFuncaoapf_tipo2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV31DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
      }

      protected void E199A2( )
      {
         /* Dynamicfiltersselector1_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E149A2( )
      {
         /* 'RemoveDynamicFilters2' Routine */
         AV34DynamicFiltersRemoving = true;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV34DynamicFiltersRemoving", AV34DynamicFiltersRemoving);
         AV29DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29DynamicFiltersEnabled2", AV29DynamicFiltersEnabled2);
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV34DynamicFiltersRemoving = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV34DynamicFiltersRemoving", AV34DynamicFiltersRemoving);
         gxgrGrid_refresh( subGrid_Rows, AV13OrderedBy, AV14OrderedDsc, AV25DynamicFiltersSelector1, AV26DynamicFiltersOperator1, AV27FuncaoAPF_Nome1, AV28FuncaoAPF_Tipo1, AV38FuncaoAPF_PF1, AV30DynamicFiltersSelector2, AV31DynamicFiltersOperator2, AV32FuncaoAPF_Nome2, AV33FuncaoAPF_Tipo2, AV39FuncaoAPF_PF2, AV29DynamicFiltersEnabled2, AV21FuncaoAPF_SistemaCod, AV6WWPContext, AV90Pgmname, AV11GridState, AV35DynamicFiltersIgnoreFirst, AV34DynamicFiltersRemoving, A165FuncaoAPF_Codigo, A432FuncaoAPF_Link, A386FuncaoAPF_PF, A415FuncaoAPFAtributos_FuncaoDadosTip, sPrefix) ;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV11GridState", AV11GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV30DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV25DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         cmbavFuncaoapf_tipo1.CurrentValue = StringUtil.RTrim( AV28FuncaoAPF_Tipo1);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavFuncaoapf_tipo1_Internalname, "Values", cmbavFuncaoapf_tipo1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         cmbavFuncaoapf_tipo2.CurrentValue = StringUtil.RTrim( AV33FuncaoAPF_Tipo2);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavFuncaoapf_tipo2_Internalname, "Values", cmbavFuncaoapf_tipo2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV31DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
      }

      protected void E209A2( )
      {
         /* Dynamicfiltersselector2_Click Routine */
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E159A2( )
      {
         /* 'DoCleanFilters' Routine */
         /* Execute user subroutine: 'CLEANFILTERS' */
         S202 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subgrid_firstpage( ) ;
         context.DoAjaxRefreshCmp(sPrefix);
         cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV25DynamicFiltersSelector1);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector1_Internalname, "Values", cmbavDynamicfiltersselector1.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV11GridState", AV11GridState);
         cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV30DynamicFiltersSelector2);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector2_Internalname, "Values", cmbavDynamicfiltersselector2.ToJavascriptSource());
         cmbavFuncaoapf_tipo1.CurrentValue = StringUtil.RTrim( AV28FuncaoAPF_Tipo1);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavFuncaoapf_tipo1_Internalname, "Values", cmbavFuncaoapf_tipo1.ToJavascriptSource());
         cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator1), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator1_Internalname, "Values", cmbavDynamicfiltersoperator1.ToJavascriptSource());
         cmbavFuncaoapf_tipo2.CurrentValue = StringUtil.RTrim( AV33FuncaoAPF_Tipo2);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavFuncaoapf_tipo2_Internalname, "Values", cmbavFuncaoapf_tipo2.ToJavascriptSource());
         cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV31DynamicFiltersOperator2), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator2_Internalname, "Values", cmbavDynamicfiltersoperator2.ToJavascriptSource());
      }

      protected void E169A2( )
      {
         /* 'DoInsert' Routine */
         context.wjLoc = formatLink("funcaoapf.aspx") + "?" + UrlEncode(StringUtil.RTrim("INS")) + "," + UrlEncode("" +0) + "," + UrlEncode("" +AV21FuncaoAPF_SistemaCod);
         context.wjLocDisableFrm = 1;
      }

      protected void E179A2( )
      {
         /* 'DoManter' Routine */
         context.wjLoc = formatLink("wp_funcoestrn.aspx") + "?" + UrlEncode("" +AV21FuncaoAPF_SistemaCod) + "," + UrlEncode(StringUtil.RTrim(AV40Sistema_Sigla));
         context.wjLocDisableFrm = 1;
      }

      protected void S152( )
      {
         /* 'CHECKSECURITYFORACTIONS' Routine */
         if ( ! ( ( AV6WWPContext.gxTpr_Insert && AV6WWPContext.gxTpr_Update && AV6WWPContext.gxTpr_Delete ) || AV6WWPContext.gxTpr_Userehadministradorgam ) )
         {
            imgInsert_Link = "";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgInsert_Internalname, "Link", imgInsert_Link);
            imgInsert_Bitmap = context.GetImagePath( "13d28f37-c579-4dd9-a404-ba51ab71d1e3", "", context.GetTheme( ));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgInsert_Internalname, "Bitmap", context.convertURL( context.PathToRelativeUrl( imgInsert_Bitmap)));
            imgInsert_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgInsert_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgInsert_Enabled), 5, 0)));
         }
         if ( ! ( ( AV6WWPContext.gxTpr_Insert && AV6WWPContext.gxTpr_Update && AV6WWPContext.gxTpr_Delete ) || AV6WWPContext.gxTpr_Userehadministradorgam ) )
         {
            bttBtnmanter_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, bttBtnmanter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnmanter_Enabled), 5, 0)));
         }
      }

      protected void S112( )
      {
         /* 'ENABLEDYNAMICFILTERS1' Routine */
         edtavFuncaoapf_nome1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavFuncaoapf_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncaoapf_nome1_Visible), 5, 0)));
         cmbavFuncaoapf_tipo1.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavFuncaoapf_tipo1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavFuncaoapf_tipo1.Visible), 5, 0)));
         edtavFuncaoapf_pf1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavFuncaoapf_pf1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncaoapf_pf1_Visible), 5, 0)));
         cmbavDynamicfiltersoperator1.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV25DynamicFiltersSelector1, "FUNCAOAPF_NOME") == 0 )
         {
            edtavFuncaoapf_nome1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavFuncaoapf_nome1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncaoapf_nome1_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV25DynamicFiltersSelector1, "FUNCAOAPF_TIPO") == 0 )
         {
            cmbavFuncaoapf_tipo1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavFuncaoapf_tipo1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavFuncaoapf_tipo1.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV25DynamicFiltersSelector1, "FUNCAOAPF_PF") == 0 )
         {
            edtavFuncaoapf_pf1_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavFuncaoapf_pf1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncaoapf_pf1_Visible), 5, 0)));
            cmbavDynamicfiltersoperator1.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         }
      }

      protected void S122( )
      {
         /* 'ENABLEDYNAMICFILTERS2' Routine */
         edtavFuncaoapf_nome2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavFuncaoapf_nome2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncaoapf_nome2_Visible), 5, 0)));
         cmbavFuncaoapf_tipo2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavFuncaoapf_tipo2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavFuncaoapf_tipo2.Visible), 5, 0)));
         edtavFuncaoapf_pf2_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavFuncaoapf_pf2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncaoapf_pf2_Visible), 5, 0)));
         cmbavDynamicfiltersoperator2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         if ( StringUtil.StrCmp(AV30DynamicFiltersSelector2, "FUNCAOAPF_NOME") == 0 )
         {
            edtavFuncaoapf_nome2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavFuncaoapf_nome2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncaoapf_nome2_Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV30DynamicFiltersSelector2, "FUNCAOAPF_TIPO") == 0 )
         {
            cmbavFuncaoapf_tipo2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavFuncaoapf_tipo2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavFuncaoapf_tipo2.Visible), 5, 0)));
         }
         else if ( StringUtil.StrCmp(AV30DynamicFiltersSelector2, "FUNCAOAPF_PF") == 0 )
         {
            edtavFuncaoapf_pf2_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavFuncaoapf_pf2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavFuncaoapf_pf2_Visible), 5, 0)));
            cmbavDynamicfiltersoperator2.Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         }
      }

      protected void S182( )
      {
         /* 'RESETDYNFILTERS' Routine */
         AV29DynamicFiltersEnabled2 = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29DynamicFiltersEnabled2", AV29DynamicFiltersEnabled2);
         AV30DynamicFiltersSelector2 = "FUNCAOAPF_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30DynamicFiltersSelector2", AV30DynamicFiltersSelector2);
         AV32FuncaoAPF_Nome2 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV32FuncaoAPF_Nome2", AV32FuncaoAPF_Nome2);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S202( )
      {
         /* 'CLEANFILTERS' Routine */
         AV25DynamicFiltersSelector1 = "FUNCAOAPF_NOME";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25DynamicFiltersSelector1", AV25DynamicFiltersSelector1);
         AV27FuncaoAPF_Nome1 = "";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27FuncaoAPF_Nome1", AV27FuncaoAPF_Nome1);
         /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'RESETDYNFILTERS' */
         S182 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV11GridState.gxTpr_Dynamicfilters.Clear();
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         context.DoAjaxRefreshCmp(sPrefix);
      }

      protected void S142( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV20Session.Get(AV90Pgmname+"GridState"), "") == 0 )
         {
            AV11GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  AV90Pgmname+"GridState"), "");
         }
         else
         {
            AV11GridState.FromXml(AV20Session.Get(AV90Pgmname+"GridState"), "");
         }
         AV13OrderedBy = AV11GridState.gxTpr_Orderedby;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)));
         AV14OrderedDsc = AV11GridState.gxTpr_Ordereddsc;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14OrderedDsc", AV14OrderedDsc);
         /* Execute user subroutine: 'LOADDYNFILTERSSTATE' */
         S192 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S192( )
      {
         /* 'LOADDYNFILTERSSTATE' Routine */
         imgAdddynamicfilters1_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
         imgRemovedynamicfilters1_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
         cmbavDynamicfiltersoperator1.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator1.Visible), 5, 0)));
         cmbavDynamicfiltersoperator2.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator2_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavDynamicfiltersoperator2.Visible), 5, 0)));
         if ( AV11GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV24GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV11GridState.gxTpr_Dynamicfilters.Item(1));
            AV25DynamicFiltersSelector1 = AV24GridStateDynamicFilter.gxTpr_Selected;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25DynamicFiltersSelector1", AV25DynamicFiltersSelector1);
            if ( StringUtil.StrCmp(AV25DynamicFiltersSelector1, "FUNCAOAPF_NOME") == 0 )
            {
               AV27FuncaoAPF_Nome1 = AV24GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV27FuncaoAPF_Nome1", AV27FuncaoAPF_Nome1);
            }
            else if ( StringUtil.StrCmp(AV25DynamicFiltersSelector1, "FUNCAOAPF_TIPO") == 0 )
            {
               AV28FuncaoAPF_Tipo1 = AV24GridStateDynamicFilter.gxTpr_Value;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV28FuncaoAPF_Tipo1", AV28FuncaoAPF_Tipo1);
            }
            else if ( StringUtil.StrCmp(AV25DynamicFiltersSelector1, "FUNCAOAPF_PF") == 0 )
            {
               AV26DynamicFiltersOperator1 = AV24GridStateDynamicFilter.gxTpr_Operator;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26DynamicFiltersOperator1", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator1), 4, 0)));
               AV38FuncaoAPF_PF1 = NumberUtil.Val( AV24GridStateDynamicFilter.gxTpr_Value, ".");
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV38FuncaoAPF_PF1", StringUtil.LTrim( StringUtil.Str( AV38FuncaoAPF_PF1, 14, 5)));
            }
            /* Execute user subroutine: 'ENABLEDYNAMICFILTERS1' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            if ( AV11GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               lblJsdynamicfilters_Caption = "<script type=\"text/javascript\">";
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"WWPDynFilterShow(2);";
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
               imgAdddynamicfilters1_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgAdddynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAdddynamicfilters1_Visible), 5, 0)));
               imgRemovedynamicfilters1_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgRemovedynamicfilters1_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgRemovedynamicfilters1_Visible), 5, 0)));
               AV29DynamicFiltersEnabled2 = true;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV29DynamicFiltersEnabled2", AV29DynamicFiltersEnabled2);
               AV24GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV11GridState.gxTpr_Dynamicfilters.Item(2));
               AV30DynamicFiltersSelector2 = AV24GridStateDynamicFilter.gxTpr_Selected;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV30DynamicFiltersSelector2", AV30DynamicFiltersSelector2);
               if ( StringUtil.StrCmp(AV30DynamicFiltersSelector2, "FUNCAOAPF_NOME") == 0 )
               {
                  AV32FuncaoAPF_Nome2 = AV24GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV32FuncaoAPF_Nome2", AV32FuncaoAPF_Nome2);
               }
               else if ( StringUtil.StrCmp(AV30DynamicFiltersSelector2, "FUNCAOAPF_TIPO") == 0 )
               {
                  AV33FuncaoAPF_Tipo2 = AV24GridStateDynamicFilter.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV33FuncaoAPF_Tipo2", AV33FuncaoAPF_Tipo2);
               }
               else if ( StringUtil.StrCmp(AV30DynamicFiltersSelector2, "FUNCAOAPF_PF") == 0 )
               {
                  AV31DynamicFiltersOperator2 = AV24GridStateDynamicFilter.gxTpr_Operator;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV31DynamicFiltersOperator2", StringUtil.LTrim( StringUtil.Str( (decimal)(AV31DynamicFiltersOperator2), 4, 0)));
                  AV39FuncaoAPF_PF2 = NumberUtil.Val( AV24GridStateDynamicFilter.gxTpr_Value, ".");
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV39FuncaoAPF_PF2", StringUtil.LTrim( StringUtil.Str( AV39FuncaoAPF_PF2, 14, 5)));
               }
               /* Execute user subroutine: 'ENABLEDYNAMICFILTERS2' */
               S122 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
               lblJsdynamicfilters_Caption = lblJsdynamicfilters_Caption+"</script>";
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
            }
         }
         if ( AV34DynamicFiltersRemoving )
         {
            lblJsdynamicfilters_Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblJsdynamicfilters_Internalname, "Caption", lblJsdynamicfilters_Caption);
         }
      }

      protected void S162( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV11GridState.FromXml(AV20Session.Get(AV90Pgmname+"GridState"), "");
         AV11GridState.gxTpr_Orderedby = AV13OrderedBy;
         AV11GridState.gxTpr_Ordereddsc = AV14OrderedDsc;
         /* Execute user subroutine: 'SAVEDYNFILTERSSTATE' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         new wwpbaseobjects.savegridstate(context ).execute(  AV90Pgmname+"GridState",  AV11GridState.ToXml(false, true, "WWPGridState", "GxEv3Up14_MeetrikaVs3")) ;
      }

      protected void S172( )
      {
         /* 'SAVEDYNFILTERSSTATE' Routine */
         AV11GridState.gxTpr_Dynamicfilters.Clear();
         if ( ! AV35DynamicFiltersIgnoreFirst )
         {
            AV24GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV24GridStateDynamicFilter.gxTpr_Selected = AV25DynamicFiltersSelector1;
            if ( ( StringUtil.StrCmp(AV25DynamicFiltersSelector1, "FUNCAOAPF_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV27FuncaoAPF_Nome1)) )
            {
               AV24GridStateDynamicFilter.gxTpr_Value = AV27FuncaoAPF_Nome1;
            }
            else if ( ( StringUtil.StrCmp(AV25DynamicFiltersSelector1, "FUNCAOAPF_TIPO") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV28FuncaoAPF_Tipo1)) )
            {
               AV24GridStateDynamicFilter.gxTpr_Value = AV28FuncaoAPF_Tipo1;
            }
            else if ( ( StringUtil.StrCmp(AV25DynamicFiltersSelector1, "FUNCAOAPF_PF") == 0 ) && ! (Convert.ToDecimal(0)==AV38FuncaoAPF_PF1) )
            {
               AV24GridStateDynamicFilter.gxTpr_Value = StringUtil.Str( AV38FuncaoAPF_PF1, 14, 5);
               AV24GridStateDynamicFilter.gxTpr_Operator = AV26DynamicFiltersOperator1;
            }
            if ( AV34DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV24GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV11GridState.gxTpr_Dynamicfilters.Add(AV24GridStateDynamicFilter, 0);
            }
         }
         if ( AV29DynamicFiltersEnabled2 )
         {
            AV24GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
            AV24GridStateDynamicFilter.gxTpr_Selected = AV30DynamicFiltersSelector2;
            if ( ( StringUtil.StrCmp(AV30DynamicFiltersSelector2, "FUNCAOAPF_NOME") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV32FuncaoAPF_Nome2)) )
            {
               AV24GridStateDynamicFilter.gxTpr_Value = AV32FuncaoAPF_Nome2;
            }
            else if ( ( StringUtil.StrCmp(AV30DynamicFiltersSelector2, "FUNCAOAPF_TIPO") == 0 ) && ! String.IsNullOrEmpty(StringUtil.RTrim( AV33FuncaoAPF_Tipo2)) )
            {
               AV24GridStateDynamicFilter.gxTpr_Value = AV33FuncaoAPF_Tipo2;
            }
            else if ( ( StringUtil.StrCmp(AV30DynamicFiltersSelector2, "FUNCAOAPF_PF") == 0 ) && ! (Convert.ToDecimal(0)==AV39FuncaoAPF_PF2) )
            {
               AV24GridStateDynamicFilter.gxTpr_Value = StringUtil.Str( AV39FuncaoAPF_PF2, 14, 5);
               AV24GridStateDynamicFilter.gxTpr_Operator = AV31DynamicFiltersOperator2;
            }
            if ( AV34DynamicFiltersRemoving || ! String.IsNullOrEmpty(StringUtil.RTrim( AV24GridStateDynamicFilter.gxTpr_Value)) )
            {
               AV11GridState.gxTpr_Dynamicfilters.Add(AV24GridStateDynamicFilter, 0);
            }
         }
      }

      protected void S132( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV9TrnContext.gxTpr_Callerobject = AV90Pgmname;
         AV9TrnContext.gxTpr_Callerondelete = true;
         AV9TrnContext.gxTpr_Callerurl = AV8HTTPRequest.ScriptName+"?"+AV8HTTPRequest.QueryString;
         AV9TrnContext.gxTpr_Transactionname = "FuncaoAPF";
         AV10TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         AV10TrnContextAtt.gxTpr_Attributename = "FuncaoAPF_SistemaCod";
         AV10TrnContextAtt.gxTpr_Attributevalue = StringUtil.Str( (decimal)(AV21FuncaoAPF_SistemaCod), 6, 0);
         AV9TrnContext.gxTpr_Attributes.Add(AV10TrnContextAtt, 0);
         AV20Session.Set("TrnContext", AV9TrnContext.ToXml(false, true, "WWPTransactionContext", "GxEv3Up14_MeetrikaVs3"));
      }

      protected void wb_table1_2_9A2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablegridheader_Internalname, tblTablegridheader_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, sPrefix, "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "vertical-align:top")+"\" class='TableSearchCell'>") ;
            wb_table2_8_9A2( true) ;
         }
         else
         {
            wb_table2_8_9A2( false) ;
         }
         return  ;
      }

      protected void wb_table2_8_9A2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_69_9A2( true) ;
         }
         else
         {
            wb_table3_69_9A2( false) ;
         }
         return  ;
      }

      protected void wb_table3_69_9A2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_9A2e( true) ;
         }
         else
         {
            wb_table1_2_9A2e( false) ;
         }
      }

      protected void wb_table3_69_9A2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblGridtablewithpaginationbar_Internalname, tblGridtablewithpaginationbar_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"DivS\" data-gxgridid=\"72\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "WorkWithBorder WorkWith", 0, "", "", 0, 0, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+((edtavUpdate_Visible==0) ? "display:none;" : "")+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+((edtavDelete_Visible==0) ? "display:none;" : "")+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(36), 4, 0))+"px"+" class=\""+subGrid_Linesclass+"\" "+" style=\""+((edtavDisplay_Visible==0) ? "display:none;" : "")+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Fun��o de Transa��o") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "C�digo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( edtFuncaoAPF_Nome_Titleformat == 0 )
               {
                  context.SendWebValue( edtFuncaoAPF_Nome_Title) ;
               }
               else
               {
                  context.WriteHtmlText( edtFuncaoAPF_Nome_Title) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( cmbFuncaoAPF_Tipo_Titleformat == 0 )
               {
                  context.SendWebValue( cmbFuncaoAPF_Tipo.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( cmbFuncaoAPF_Tipo.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Complexidade") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "DER") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "ALR") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "PF") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Com DC") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Link") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               if ( cmbFuncaoAPF_Ativo_Titleformat == 0 )
               {
                  context.SendWebValue( cmbFuncaoAPF_Ativo.Title.Text) ;
               }
               else
               {
                  context.WriteHtmlText( cmbFuncaoAPF_Ativo.Title.Text) ;
               }
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+((edtFuncaoAPF_SolucaoTecnica_Visible==0) ? "display:none;" : "")+""+"\" "+">") ;
               context.SendWebValue( "Solu��o T�cnica") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" nowrap=\"nowrap\" "+" class=\""+subGrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Fun. de Usu�rio") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "WorkWithBorder WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", sPrefix);
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV17Update));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavUpdate_Enabled), 5, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavUpdate_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavUpdate_Tooltiptext));
               GridColumn.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavUpdate_Visible), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV18Delete));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavDelete_Enabled), 5, 0, ".", "")));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavDelete_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDelete_Tooltiptext));
               GridColumn.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavDelete_Visible), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV19Display));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavDisplay_Link));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavDisplay_Tooltiptext));
               GridColumn.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavDisplay_Visible), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A165FuncaoAPF_Codigo), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A360FuncaoAPF_SistemaCod), 6, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A166FuncaoAPF_Nome);
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( edtFuncaoAPF_Nome_Title));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtFuncaoAPF_Nome_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A184FuncaoAPF_Tipo));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( cmbFuncaoAPF_Tipo.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbFuncaoAPF_Tipo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A185FuncaoAPF_Complexidade));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A388FuncaoAPF_TD), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A387FuncaoAPF_AR), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( A386FuncaoAPF_PF, 14, 5, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.BoolToStr( AV36ComDC));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(chkavComdc.Enabled), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV37FuncaoAPF_Link));
               GridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavFuncaoapf_link_Link));
               GridColumn.AddObjectProperty("Linktarget", StringUtil.RTrim( edtavFuncaoapf_link_Linktarget));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavFuncaoapf_link_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A183FuncaoAPF_Ativo));
               GridColumn.AddObjectProperty("Title", StringUtil.RTrim( cmbFuncaoAPF_Ativo.Title.Text));
               GridColumn.AddObjectProperty("Titleformat", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbFuncaoAPF_Ativo_Titleformat), 4, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A757FuncaoAPF_SolucaoTecnica);
               GridColumn.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtFuncaoAPF_SolucaoTecnica_Visible), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.convertURL( AV23ImagemFuncoesUsuario));
               GridColumn.AddObjectProperty("Tooltiptext", StringUtil.RTrim( edtavImagemfuncoesusuario_Tooltiptext));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 72 )
         {
            wbEnd = 0;
            nRC_GXsfl_72 = (short)(nGXsfl_72_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+sPrefix+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid(sPrefix+"_"+"Grid", GridContainer);
               if ( ! isAjaxCallMode( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+sPrefix+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"GRIDPAGINATIONBARContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_69_9A2e( true) ;
         }
         else
         {
            wb_table3_69_9A2e( false) ;
         }
      }

      protected void wb_table2_8_9A2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablesearch_Internalname, tblTablesearch_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_11_9A2( true) ;
         }
         else
         {
            wb_table4_11_9A2( false) ;
         }
         return  ;
      }

      protected void wb_table4_11_9A2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='OrderedByDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblOrderedtext_Internalname, "Ordenado por", "", "", lblOrderedtext_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "OrderedByDescription", 0, "", 1, 1, 0, "HLP_SistemaFuncaopAPFWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 20,'" + sPrefix + "',false,'" + sGXsfl_72_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavOrderedby, cmbavOrderedby_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0)), 1, cmbavOrderedby_Jsonclick, 5, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"EVORDEREDBY.CLICK."+"'", "int", "", 1, 1, 0, 0, 0, "em", 0, "", "", "Attribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,20);\"", "", true, "HLP_SistemaFuncaopAPFWC.htm");
            cmbavOrderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13OrderedBy), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavOrderedby_Internalname, "Values", (String)(cmbavOrderedby.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'" + sPrefix + "',false,'" + sGXsfl_72_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavOrdereddsc_Internalname, StringUtil.BoolToStr( AV14OrderedDsc), StringUtil.BoolToStr( AV14OrderedDsc), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,21);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavOrdereddsc_Jsonclick, 0, "Attribute", "", "", "", edtavOrdereddsc_Visible, 1, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, 0, 0, true, "", "right", false, "HLP_SistemaFuncaopAPFWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableFiltersCell'>") ;
            wb_table5_23_9A2( true) ;
         }
         else
         {
            wb_table5_23_9A2( false) ;
         }
         return  ;
      }

      protected void wb_table5_23_9A2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_8_9A2e( true) ;
         }
         else
         {
            wb_table2_8_9A2e( false) ;
         }
      }

      protected void wb_table5_23_9A2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefilters_Internalname, tblTablefilters_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 26,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgCleanfilters_Internalname, context.GetImagePath( "effedd9a-25cf-4497-952f-5a5ac032b8be", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Limpar filtros", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgCleanfilters_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOCLEANFILTERS\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_SistemaFuncaopAPFWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table6_28_9A2( true) ;
         }
         else
         {
            wb_table6_28_9A2( false) ;
         }
         return  ;
      }

      protected void wb_table6_28_9A2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblJsdynamicfilters_Internalname, lblJsdynamicfilters_Caption, "", "", lblJsdynamicfilters_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", 0, "", 1, 1, 1, "HLP_SistemaFuncaopAPFWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_23_9A2e( true) ;
         }
         else
         {
            wb_table5_23_9A2e( false) ;
         }
      }

      protected void wb_table6_28_9A2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTabledynamicfilters_Internalname, tblTabledynamicfilters_Internalname, "", "TableDynamicFilters", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix1_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix1_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_SistemaFuncaopAPFWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 33,'" + sPrefix + "',false,'" + sGXsfl_72_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector1, cmbavDynamicfiltersselector1_Internalname, StringUtil.RTrim( AV25DynamicFiltersSelector1), 1, cmbavDynamicfiltersselector1_Jsonclick, 5, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"EVDYNAMICFILTERSSELECTOR1.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,33);\"", "", true, "HLP_SistemaFuncaopAPFWC.htm");
            cmbavDynamicfiltersselector1.CurrentValue = StringUtil.RTrim( AV25DynamicFiltersSelector1);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector1_Internalname, "Values", (String)(cmbavDynamicfiltersselector1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle1_Internalname, "valor", "", "", lblDynamicfiltersmiddle1_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_SistemaFuncaopAPFWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table7_37_9A2( true) ;
         }
         else
         {
            wb_table7_37_9A2( false) ;
         }
         return  ;
      }

      protected void wb_table7_37_9A2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 46,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgAdddynamicfilters1_Internalname, context.GetImagePath( "27283ea5-332f-423b-b880-64b762622df3", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgAdddynamicfilters1_Visible, 1, "", "Adicionar filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgAdddynamicfilters1_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'ADDDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_SistemaFuncaopAPFWC.htm");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 47,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters1_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgRemovedynamicfilters1_Visible, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters1_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'REMOVEDYNAMICFILTERS1\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_SistemaFuncaopAPFWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr id=\"Dynamicfiltersrow2\"  class='Invisible'>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersprefix2_Internalname, "Filtrar por", "", "", lblDynamicfiltersprefix2_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_SistemaFuncaopAPFWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 52,'" + sPrefix + "',false,'" + sGXsfl_72_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersselector2, cmbavDynamicfiltersselector2_Internalname, StringUtil.RTrim( AV30DynamicFiltersSelector2), 1, cmbavDynamicfiltersselector2_Jsonclick, 5, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"EVDYNAMICFILTERSSELECTOR2.CLICK."+"'", "svchar", "", 1, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,52);\"", "", true, "HLP_SistemaFuncaopAPFWC.htm");
            cmbavDynamicfiltersselector2.CurrentValue = StringUtil.RTrim( AV30DynamicFiltersSelector2);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersselector2_Internalname, "Values", (String)(cmbavDynamicfiltersselector2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDynamicfiltersmiddle2_Internalname, "valor", "", "", lblDynamicfiltersmiddle2_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "DataFilterDescription", 0, "", 1, 1, 0, "HLP_SistemaFuncaopAPFWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table8_56_9A2( true) ;
         }
         else
         {
            wb_table8_56_9A2( false) ;
         }
         return  ;
      }

      protected void wb_table8_56_9A2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 65,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgRemovedynamicfilters2_Internalname, context.GetImagePath( "11a6ef14-1a5a-4077-91a2-f41ed9a3a662", "", context.GetTheme( )), "", "", "", context.GetTheme( ), 1, 1, "", "Remover filtro", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgRemovedynamicfilters2_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'REMOVEDYNAMICFILTERS2\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_SistemaFuncaopAPFWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_28_9A2e( true) ;
         }
         else
         {
            wb_table6_28_9A2e( false) ;
         }
      }

      protected void wb_table8_56_9A2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters2_Internalname, tblTablemergeddynamicfilters2_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 59,'" + sPrefix + "',false,'" + sGXsfl_72_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator2, cmbavDynamicfiltersoperator2_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV31DynamicFiltersOperator2), 4, 0)), 1, cmbavDynamicfiltersoperator2_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator2.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,59);\"", "", true, "HLP_SistemaFuncaopAPFWC.htm");
            cmbavDynamicfiltersoperator2.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV31DynamicFiltersOperator2), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator2_Internalname, "Values", (String)(cmbavDynamicfiltersoperator2.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 61,'" + sPrefix + "',false,'" + sGXsfl_72_idx + "',0)\"";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavFuncaoapf_nome2_Internalname, AV32FuncaoAPF_Nome2, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,61);\"", 0, edtavFuncaoapf_nome2_Visible, 1, 0, 80, "chr", 3, "row", StyleString, ClassString, "", "200", -1, "", "", -1, true, "", "HLP_SistemaFuncaopAPFWC.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 62,'" + sPrefix + "',false,'" + sGXsfl_72_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavFuncaoapf_tipo2, cmbavFuncaoapf_tipo2_Internalname, StringUtil.RTrim( AV33FuncaoAPF_Tipo2), 1, cmbavFuncaoapf_tipo2_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "char", "", cmbavFuncaoapf_tipo2.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,62);\"", "", true, "HLP_SistemaFuncaopAPFWC.htm");
            cmbavFuncaoapf_tipo2.CurrentValue = StringUtil.RTrim( AV33FuncaoAPF_Tipo2);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavFuncaoapf_tipo2_Internalname, "Values", (String)(cmbavFuncaoapf_tipo2.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 63,'" + sPrefix + "',false,'" + sGXsfl_72_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavFuncaoapf_pf2_Internalname, StringUtil.LTrim( StringUtil.NToC( AV39FuncaoAPF_PF2, 14, 5, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV39FuncaoAPF_PF2, "ZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,63);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavFuncaoapf_pf2_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavFuncaoapf_pf2_Visible, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_SistemaFuncaopAPFWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table8_56_9A2e( true) ;
         }
         else
         {
            wb_table8_56_9A2e( false) ;
         }
      }

      protected void wb_table7_37_9A2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergeddynamicfilters1_Internalname, tblTablemergeddynamicfilters1_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 40,'" + sPrefix + "',false,'" + sGXsfl_72_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDynamicfiltersoperator1, cmbavDynamicfiltersoperator1_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator1), 4, 0)), 1, cmbavDynamicfiltersoperator1_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "int", "", cmbavDynamicfiltersoperator1.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,40);\"", "", true, "HLP_SistemaFuncaopAPFWC.htm");
            cmbavDynamicfiltersoperator1.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV26DynamicFiltersOperator1), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavDynamicfiltersoperator1_Internalname, "Values", (String)(cmbavDynamicfiltersoperator1.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 42,'" + sPrefix + "',false,'" + sGXsfl_72_idx + "',0)\"";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            ClassString = "BootstrapAttribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavFuncaoapf_nome1_Internalname, AV27FuncaoAPF_Nome1, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,42);\"", 0, edtavFuncaoapf_nome1_Visible, 1, 0, 80, "chr", 3, "row", StyleString, ClassString, "", "200", -1, "", "", -1, true, "", "HLP_SistemaFuncaopAPFWC.htm");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 43,'" + sPrefix + "',false,'" + sGXsfl_72_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavFuncaoapf_tipo1, cmbavFuncaoapf_tipo1_Internalname, StringUtil.RTrim( AV28FuncaoAPF_Tipo1), 1, cmbavFuncaoapf_tipo1_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "char", "", cmbavFuncaoapf_tipo1.Visible, 1, 0, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,43);\"", "", true, "HLP_SistemaFuncaopAPFWC.htm");
            cmbavFuncaoapf_tipo1.CurrentValue = StringUtil.RTrim( AV28FuncaoAPF_Tipo1);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbavFuncaoapf_tipo1_Internalname, "Values", (String)(cmbavFuncaoapf_tipo1.ToJavascriptSource()));
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 44,'" + sPrefix + "',false,'" + sGXsfl_72_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavFuncaoapf_pf1_Internalname, StringUtil.LTrim( StringUtil.NToC( AV38FuncaoAPF_PF1, 14, 5, ",", "")), StringUtil.LTrim( context.localUtil.Format( AV38FuncaoAPF_PF1, "ZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,44);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavFuncaoapf_pf1_Jsonclick, 0, "BootstrapAttribute", "", "", "", edtavFuncaoapf_pf1_Visible, 1, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_SistemaFuncaopAPFWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_37_9A2e( true) ;
         }
         else
         {
            wb_table7_37_9A2e( false) ;
         }
      }

      protected void wb_table4_11_9A2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "Table", 0, "", "", 3, 3, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 14,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgInsert_Internalname, imgInsert_Bitmap, "", "", "", context.GetTheme( ), imgInsert_Visible, imgInsert_Enabled, "", "Inserir", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgInsert_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOINSERT\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_SistemaFuncaopAPFWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 16,'" + sPrefix + "',false,'',0)\"";
            ClassString = "btn-default";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnmanter_Internalname, "gx.evt.setGridEvt("+StringUtil.Str( (decimal)(72), 2, 0)+","+"null"+");", "Manter", bttBtnmanter_Jsonclick, 5, "Manter Fun��es de Transa��o", "", StyleString, ClassString, 1, bttBtnmanter_Enabled, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOMANTER\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_SistemaFuncaopAPFWC.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_11_9A2e( true) ;
         }
         else
         {
            wb_table4_11_9A2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV21FuncaoAPF_SistemaCod = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21FuncaoAPF_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21FuncaoAPF_SistemaCod), 6, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA9A2( ) ;
         WS9A2( ) ;
         WE9A2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlAV21FuncaoAPF_SistemaCod = (String)((String)getParm(obj,0));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PA9A2( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "sistemafuncaopapfwc");
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PA9A2( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            AV21FuncaoAPF_SistemaCod = Convert.ToInt32(getParm(obj,2));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21FuncaoAPF_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21FuncaoAPF_SistemaCod), 6, 0)));
         }
         wcpOAV21FuncaoAPF_SistemaCod = (int)(context.localUtil.CToN( cgiGet( sPrefix+"wcpOAV21FuncaoAPF_SistemaCod"), ",", "."));
         if ( ! GetJustCreated( ) && ( ( AV21FuncaoAPF_SistemaCod != wcpOAV21FuncaoAPF_SistemaCod ) ) )
         {
            setjustcreated();
         }
         wcpOAV21FuncaoAPF_SistemaCod = AV21FuncaoAPF_SistemaCod;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlAV21FuncaoAPF_SistemaCod = cgiGet( sPrefix+"AV21FuncaoAPF_SistemaCod_CTRL");
         if ( StringUtil.Len( sCtrlAV21FuncaoAPF_SistemaCod) > 0 )
         {
            AV21FuncaoAPF_SistemaCod = (int)(context.localUtil.CToN( cgiGet( sCtrlAV21FuncaoAPF_SistemaCod), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21FuncaoAPF_SistemaCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV21FuncaoAPF_SistemaCod), 6, 0)));
         }
         else
         {
            AV21FuncaoAPF_SistemaCod = (int)(context.localUtil.CToN( cgiGet( sPrefix+"AV21FuncaoAPF_SistemaCod_PARM"), ",", "."));
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PA9A2( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WS9A2( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WS9A2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"AV21FuncaoAPF_SistemaCod_PARM", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV21FuncaoAPF_SistemaCod), 6, 0, ",", "")));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlAV21FuncaoAPF_SistemaCod)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"AV21FuncaoAPF_SistemaCod_CTRL", StringUtil.RTrim( sCtrlAV21FuncaoAPF_SistemaCod));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WE9A2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("DVelop/DVPaginationBar/DVPaginationBar.css", "?2319140");
         AddStyleSheetFile("DVelop/Bootstrap/Shared/fontawesome/font-awesome.min.css", "?2319140");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?202051812483948");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("sistemafuncaopapfwc.js", "?202051812483948");
         context.AddJavascriptSource("DVelop/DVPaginationBar/DVPaginationBarRender.js", "");
         context.AddJavascriptSource("DVelop/WorkWithPlusUtilities/WorkWithPlusUtilitiesRender.js", "");
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_722( )
      {
         edtavUpdate_Internalname = sPrefix+"vUPDATE_"+sGXsfl_72_idx;
         edtavDelete_Internalname = sPrefix+"vDELETE_"+sGXsfl_72_idx;
         edtavDisplay_Internalname = sPrefix+"vDISPLAY_"+sGXsfl_72_idx;
         edtFuncaoAPF_Codigo_Internalname = sPrefix+"FUNCAOAPF_CODIGO_"+sGXsfl_72_idx;
         edtFuncaoAPF_SistemaCod_Internalname = sPrefix+"FUNCAOAPF_SISTEMACOD_"+sGXsfl_72_idx;
         edtFuncaoAPF_Nome_Internalname = sPrefix+"FUNCAOAPF_NOME_"+sGXsfl_72_idx;
         cmbFuncaoAPF_Tipo_Internalname = sPrefix+"FUNCAOAPF_TIPO_"+sGXsfl_72_idx;
         cmbFuncaoAPF_Complexidade_Internalname = sPrefix+"FUNCAOAPF_COMPLEXIDADE_"+sGXsfl_72_idx;
         edtFuncaoAPF_TD_Internalname = sPrefix+"FUNCAOAPF_TD_"+sGXsfl_72_idx;
         edtFuncaoAPF_AR_Internalname = sPrefix+"FUNCAOAPF_AR_"+sGXsfl_72_idx;
         edtFuncaoAPF_PF_Internalname = sPrefix+"FUNCAOAPF_PF_"+sGXsfl_72_idx;
         chkavComdc_Internalname = sPrefix+"vCOMDC_"+sGXsfl_72_idx;
         edtavFuncaoapf_link_Internalname = sPrefix+"vFUNCAOAPF_LINK_"+sGXsfl_72_idx;
         cmbFuncaoAPF_Ativo_Internalname = sPrefix+"FUNCAOAPF_ATIVO_"+sGXsfl_72_idx;
         edtFuncaoAPF_SolucaoTecnica_Internalname = sPrefix+"FUNCAOAPF_SOLUCAOTECNICA_"+sGXsfl_72_idx;
         edtavImagemfuncoesusuario_Internalname = sPrefix+"vIMAGEMFUNCOESUSUARIO_"+sGXsfl_72_idx;
      }

      protected void SubsflControlProps_fel_722( )
      {
         edtavUpdate_Internalname = sPrefix+"vUPDATE_"+sGXsfl_72_fel_idx;
         edtavDelete_Internalname = sPrefix+"vDELETE_"+sGXsfl_72_fel_idx;
         edtavDisplay_Internalname = sPrefix+"vDISPLAY_"+sGXsfl_72_fel_idx;
         edtFuncaoAPF_Codigo_Internalname = sPrefix+"FUNCAOAPF_CODIGO_"+sGXsfl_72_fel_idx;
         edtFuncaoAPF_SistemaCod_Internalname = sPrefix+"FUNCAOAPF_SISTEMACOD_"+sGXsfl_72_fel_idx;
         edtFuncaoAPF_Nome_Internalname = sPrefix+"FUNCAOAPF_NOME_"+sGXsfl_72_fel_idx;
         cmbFuncaoAPF_Tipo_Internalname = sPrefix+"FUNCAOAPF_TIPO_"+sGXsfl_72_fel_idx;
         cmbFuncaoAPF_Complexidade_Internalname = sPrefix+"FUNCAOAPF_COMPLEXIDADE_"+sGXsfl_72_fel_idx;
         edtFuncaoAPF_TD_Internalname = sPrefix+"FUNCAOAPF_TD_"+sGXsfl_72_fel_idx;
         edtFuncaoAPF_AR_Internalname = sPrefix+"FUNCAOAPF_AR_"+sGXsfl_72_fel_idx;
         edtFuncaoAPF_PF_Internalname = sPrefix+"FUNCAOAPF_PF_"+sGXsfl_72_fel_idx;
         chkavComdc_Internalname = sPrefix+"vCOMDC_"+sGXsfl_72_fel_idx;
         edtavFuncaoapf_link_Internalname = sPrefix+"vFUNCAOAPF_LINK_"+sGXsfl_72_fel_idx;
         cmbFuncaoAPF_Ativo_Internalname = sPrefix+"FUNCAOAPF_ATIVO_"+sGXsfl_72_fel_idx;
         edtFuncaoAPF_SolucaoTecnica_Internalname = sPrefix+"FUNCAOAPF_SOLUCAOTECNICA_"+sGXsfl_72_fel_idx;
         edtavImagemfuncoesusuario_Internalname = sPrefix+"vIMAGEMFUNCOESUSUARIO_"+sGXsfl_72_fel_idx;
      }

      protected void sendrow_722( )
      {
         SubsflControlProps_722( ) ;
         WB9A0( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_72_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0xFFFFFF);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_72_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0xFFFFFF);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+subGrid_Linesclass+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_72_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+((edtavUpdate_Visible==0) ? "display:none;" : "")+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV17Update_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV17Update))&&String.IsNullOrEmpty(StringUtil.RTrim( AV85Update_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV17Update)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavUpdate_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV17Update)) ? AV85Update_GXI : context.PathToRelativeUrl( AV17Update)),(String)edtavUpdate_Link,(String)"",(String)"",context.GetTheme( ),(int)edtavUpdate_Visible,(int)edtavUpdate_Enabled,(String)"",(String)edtavUpdate_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV17Update_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+((edtavDelete_Visible==0) ? "display:none;" : "")+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV18Delete_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV18Delete))&&String.IsNullOrEmpty(StringUtil.RTrim( AV86Delete_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV18Delete)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDelete_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV18Delete)) ? AV86Delete_GXI : context.PathToRelativeUrl( AV18Delete)),(String)edtavDelete_Link,(String)"",(String)"",context.GetTheme( ),(int)edtavDelete_Visible,(int)edtavDelete_Enabled,(String)"",(String)edtavDelete_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV18Delete_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+((edtavDisplay_Visible==0) ? "display:none;" : "")+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "Image";
            StyleString = "";
            AV19Display_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV19Display))&&String.IsNullOrEmpty(StringUtil.RTrim( AV87Display_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV19Display)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavDisplay_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV19Display)) ? AV87Display_GXI : context.PathToRelativeUrl( AV19Display)),(String)edtavDisplay_Link,(String)"",(String)"",context.GetTheme( ),(int)edtavDisplay_Visible,(short)1,(String)"",(String)edtavDisplay_Tooltiptext,(short)0,(short)1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV19Display_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtFuncaoAPF_Codigo_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A165FuncaoAPF_Codigo), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A165FuncaoAPF_Codigo), "ZZZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtFuncaoAPF_Codigo_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)72,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtFuncaoAPF_SistemaCod_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A360FuncaoAPF_SistemaCod), 6, 0, ",", "")),context.localUtil.Format( (decimal)(A360FuncaoAPF_SistemaCod), "ZZZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtFuncaoAPF_SistemaCod_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)6,(short)0,(short)0,(short)72,(short)1,(short)-1,(short)0,(bool)true,(String)"Codigo",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtFuncaoAPF_Nome_Internalname,(String)A166FuncaoAPF_Nome,(String)"",(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtFuncaoAPF_Nome_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)200,(short)0,(short)0,(short)72,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            if ( ( nGXsfl_72_idx == 1 ) && isAjaxCallMode( ) )
            {
               GXCCtl = "FUNCAOAPF_TIPO_" + sGXsfl_72_idx;
               cmbFuncaoAPF_Tipo.Name = GXCCtl;
               cmbFuncaoAPF_Tipo.WebTags = "";
               cmbFuncaoAPF_Tipo.addItem("", "(Nenhum)", 0);
               cmbFuncaoAPF_Tipo.addItem("EE", "EE", 0);
               cmbFuncaoAPF_Tipo.addItem("SE", "SE", 0);
               cmbFuncaoAPF_Tipo.addItem("CE", "CE", 0);
               cmbFuncaoAPF_Tipo.addItem("NM", "NM", 0);
               if ( cmbFuncaoAPF_Tipo.ItemCount > 0 )
               {
                  A184FuncaoAPF_Tipo = cmbFuncaoAPF_Tipo.getValidValue(A184FuncaoAPF_Tipo);
               }
            }
            /* ComboBox */
            GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbFuncaoAPF_Tipo,(String)cmbFuncaoAPF_Tipo_Internalname,StringUtil.RTrim( A184FuncaoAPF_Tipo),(short)1,(String)cmbFuncaoAPF_Tipo_Jsonclick,(short)0,(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"char",(String)"",(short)-1,(short)0,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"BootstrapAttribute",(String)"",(String)"",(String)"",(bool)true});
            cmbFuncaoAPF_Tipo.CurrentValue = StringUtil.RTrim( A184FuncaoAPF_Tipo);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbFuncaoAPF_Tipo_Internalname, "Values", (String)(cmbFuncaoAPF_Tipo.ToJavascriptSource()));
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            GXCCtl = "FUNCAOAPF_COMPLEXIDADE_" + sGXsfl_72_idx;
            cmbFuncaoAPF_Complexidade.Name = GXCCtl;
            cmbFuncaoAPF_Complexidade.WebTags = "";
            cmbFuncaoAPF_Complexidade.addItem("E", "", 0);
            cmbFuncaoAPF_Complexidade.addItem("B", "Baixa", 0);
            cmbFuncaoAPF_Complexidade.addItem("M", "M�dia", 0);
            cmbFuncaoAPF_Complexidade.addItem("A", "Alta", 0);
            if ( cmbFuncaoAPF_Complexidade.ItemCount > 0 )
            {
               A185FuncaoAPF_Complexidade = cmbFuncaoAPF_Complexidade.getValidValue(A185FuncaoAPF_Complexidade);
            }
            /* ComboBox */
            GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbFuncaoAPF_Complexidade,(String)cmbFuncaoAPF_Complexidade_Internalname,StringUtil.RTrim( A185FuncaoAPF_Complexidade),(short)1,(String)cmbFuncaoAPF_Complexidade_Jsonclick,(short)0,(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"char",(String)"",(short)-1,(short)0,(short)1,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"BootstrapAttribute",(String)"",(String)"",(String)"",(bool)true});
            cmbFuncaoAPF_Complexidade.CurrentValue = StringUtil.RTrim( A185FuncaoAPF_Complexidade);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbFuncaoAPF_Complexidade_Internalname, "Values", (String)(cmbFuncaoAPF_Complexidade.ToJavascriptSource()));
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtFuncaoAPF_TD_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A388FuncaoAPF_TD), 4, 0, ",", "")),context.localUtil.Format( (decimal)(A388FuncaoAPF_TD), "ZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtFuncaoAPF_TD_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)4,(short)0,(short)0,(short)72,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtFuncaoAPF_AR_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A387FuncaoAPF_AR), 4, 0, ",", "")),context.localUtil.Format( (decimal)(A387FuncaoAPF_AR), "ZZZ9"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtFuncaoAPF_AR_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)4,(short)0,(short)0,(short)72,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtFuncaoAPF_PF_Internalname,StringUtil.LTrim( StringUtil.NToC( A386FuncaoAPF_PF, 14, 5, ",", "")),context.localUtil.Format( A386FuncaoAPF_PF, "ZZ,ZZZ,ZZ9.999"),(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtFuncaoAPF_PF_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)14,(short)0,(short)0,(short)72,(short)1,(short)-1,(short)0,(bool)true,(String)"PontosDeFuncao",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Check box */
            TempTags = " " + ((chkavComdc.Enabled!=0)&&(chkavComdc.Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 84,'"+sPrefix+"',false,'"+sGXsfl_72_idx+"',72)\"" : " ");
            ClassString = "BootstrapAttributeCheckBox";
            StyleString = "";
            GridRow.AddColumnProperties("checkbox", 1, isAjaxCallMode( ), new Object[] {(String)chkavComdc_Internalname,StringUtil.BoolToStr( AV36ComDC),(String)"",(String)"",(short)-1,chkavComdc.Enabled,(String)"true",(String)"",(String)StyleString,(String)ClassString,(String)"",TempTags+((chkavComdc.Enabled!=0)&&(chkavComdc.Visible!=0) ? " onclick=\"gx.fn.checkboxClick(84, this, 'true', 'false');gx.evt.onchange(this);\" " : " ")+((chkavComdc.Enabled!=0)&&(chkavComdc.Visible!=0) ? " onblur=\""+""+";gx.evt.onblur(this,84);\"" : " ")});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            ClassString = "BootstrapAttribute";
            StyleString = "";
            AV37FuncaoAPF_Link_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV37FuncaoAPF_Link))&&String.IsNullOrEmpty(StringUtil.RTrim( AV88Funcaoapf_link_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV37FuncaoAPF_Link)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavFuncaoapf_link_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV37FuncaoAPF_Link)) ? AV88Funcaoapf_link_GXI : context.PathToRelativeUrl( AV37FuncaoAPF_Link)),(String)edtavFuncaoapf_link_Link,(String)edtavFuncaoapf_link_Linktarget,(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavFuncaoapf_link_Tooltiptext,(short)1,(short)-1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV37FuncaoAPF_Link_IsBlob,(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
            }
            if ( ( nGXsfl_72_idx == 1 ) && isAjaxCallMode( ) )
            {
               GXCCtl = "FUNCAOAPF_ATIVO_" + sGXsfl_72_idx;
               cmbFuncaoAPF_Ativo.Name = GXCCtl;
               cmbFuncaoAPF_Ativo.WebTags = "";
               cmbFuncaoAPF_Ativo.addItem("A", "Ativa", 0);
               cmbFuncaoAPF_Ativo.addItem("E", "Exclu�da", 0);
               cmbFuncaoAPF_Ativo.addItem("R", "Rejeitada", 0);
               if ( cmbFuncaoAPF_Ativo.ItemCount > 0 )
               {
                  A183FuncaoAPF_Ativo = cmbFuncaoAPF_Ativo.getValidValue(A183FuncaoAPF_Ativo);
               }
            }
            /* ComboBox */
            GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbFuncaoAPF_Ativo,(String)cmbFuncaoAPF_Ativo_Internalname,StringUtil.RTrim( A183FuncaoAPF_Ativo),(short)1,(String)cmbFuncaoAPF_Ativo_Jsonclick,(short)0,(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"char",(String)"",(short)-1,(short)0,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"BootstrapAttribute",(String)"",(String)"",(String)"",(bool)true});
            cmbFuncaoAPF_Ativo.CurrentValue = StringUtil.RTrim( A183FuncaoAPF_Ativo);
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbFuncaoAPF_Ativo_Internalname, "Values", (String)(cmbFuncaoAPF_Ativo.ToJavascriptSource()));
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+((edtFuncaoAPF_SolucaoTecnica_Visible==0) ? "display:none;" : "")+"\">") ;
            }
            /* Single line edit */
            ROClassString = "BootstrapAttribute";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtFuncaoAPF_SolucaoTecnica_Internalname,(String)A757FuncaoAPF_SolucaoTecnica,(String)A757FuncaoAPF_SolucaoTecnica,(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtFuncaoAPF_SolucaoTecnica_Jsonclick,(short)0,(String)"BootstrapAttribute",(String)"",(String)ROClassString,(String)"",(int)edtFuncaoAPF_SolucaoTecnica_Visible,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)500,(short)0,(short)0,(short)72,(short)1,(short)0,(short)-1,(bool)true,(String)"DescricaoLonga",(String)"left",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Active Bitmap Variable */
            TempTags = " " + ((edtavImagemfuncoesusuario_Enabled!=0)&&(edtavImagemfuncoesusuario_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 88,'"+sPrefix+"',false,'',72)\"" : " ");
            ClassString = "BootstrapAttribute";
            StyleString = "";
            AV23ImagemFuncoesUsuario_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV23ImagemFuncoesUsuario))&&String.IsNullOrEmpty(StringUtil.RTrim( AV84Imagemfuncoesusuario_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV23ImagemFuncoesUsuario)));
            GridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavImagemfuncoesusuario_Internalname,(String.IsNullOrEmpty(StringUtil.RTrim( AV23ImagemFuncoesUsuario)) ? AV84Imagemfuncoesusuario_GXI : context.PathToRelativeUrl( AV23ImagemFuncoesUsuario)),(String)"",(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)edtavImagemfuncoesusuario_Tooltiptext,(short)1,(short)-1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)7,(String)edtavImagemfuncoesusuario_Jsonclick,(String)"'"+sPrefix+"'"+",false,"+"'"+"e249a2_client"+"'",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)""+TempTags,(String)"",(String)"",(short)1,(bool)AV23ImagemFuncoesUsuario_IsBlob,(bool)false});
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_FUNCAOAPF_CODIGO"+"_"+sGXsfl_72_idx, GetSecureSignedToken( sPrefix+sGXsfl_72_idx, context.localUtil.Format( (decimal)(A165FuncaoAPF_Codigo), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_FUNCAOAPF_SISTEMACOD"+"_"+sGXsfl_72_idx, GetSecureSignedToken( sPrefix+sGXsfl_72_idx, context.localUtil.Format( (decimal)(A360FuncaoAPF_SistemaCod), "ZZZZZ9")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_FUNCAOAPF_NOME"+"_"+sGXsfl_72_idx, GetSecureSignedToken( sPrefix+sGXsfl_72_idx, StringUtil.RTrim( context.localUtil.Format( A166FuncaoAPF_Nome, ""))));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_FUNCAOAPF_TIPO"+"_"+sGXsfl_72_idx, GetSecureSignedToken( sPrefix+sGXsfl_72_idx, StringUtil.RTrim( context.localUtil.Format( A184FuncaoAPF_Tipo, ""))));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_FUNCAOAPF_COMPLEXIDADE"+"_"+sGXsfl_72_idx, GetSecureSignedToken( sPrefix+sGXsfl_72_idx, StringUtil.RTrim( context.localUtil.Format( A185FuncaoAPF_Complexidade, ""))));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_FUNCAOAPF_TD"+"_"+sGXsfl_72_idx, GetSecureSignedToken( sPrefix+sGXsfl_72_idx, context.localUtil.Format( (decimal)(A388FuncaoAPF_TD), "ZZZ9")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_FUNCAOAPF_AR"+"_"+sGXsfl_72_idx, GetSecureSignedToken( sPrefix+sGXsfl_72_idx, context.localUtil.Format( (decimal)(A387FuncaoAPF_AR), "ZZZ9")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_FUNCAOAPF_PF"+"_"+sGXsfl_72_idx, GetSecureSignedToken( sPrefix+sGXsfl_72_idx, context.localUtil.Format( A386FuncaoAPF_PF, "ZZ,ZZZ,ZZ9.999")));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_FUNCAOAPF_ATIVO"+"_"+sGXsfl_72_idx, GetSecureSignedToken( sPrefix+sGXsfl_72_idx, StringUtil.RTrim( context.localUtil.Format( A183FuncaoAPF_Ativo, ""))));
            GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_FUNCAOAPF_SOLUCAOTECNICA"+"_"+sGXsfl_72_idx, GetSecureSignedToken( sPrefix+sGXsfl_72_idx, A757FuncaoAPF_SolucaoTecnica));
            GridContainer.AddRow(GridRow);
            nGXsfl_72_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_72_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_72_idx+1));
            sGXsfl_72_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_72_idx), 4, 0)), 4, "0");
            SubsflControlProps_722( ) ;
         }
         /* End function sendrow_722 */
      }

      protected void init_default_properties( )
      {
         imgInsert_Internalname = sPrefix+"INSERT";
         bttBtnmanter_Internalname = sPrefix+"BTNMANTER";
         tblTableactions_Internalname = sPrefix+"TABLEACTIONS";
         lblOrderedtext_Internalname = sPrefix+"ORDEREDTEXT";
         cmbavOrderedby_Internalname = sPrefix+"vORDEREDBY";
         edtavOrdereddsc_Internalname = sPrefix+"vORDEREDDSC";
         imgCleanfilters_Internalname = sPrefix+"CLEANFILTERS";
         lblDynamicfiltersprefix1_Internalname = sPrefix+"DYNAMICFILTERSPREFIX1";
         cmbavDynamicfiltersselector1_Internalname = sPrefix+"vDYNAMICFILTERSSELECTOR1";
         lblDynamicfiltersmiddle1_Internalname = sPrefix+"DYNAMICFILTERSMIDDLE1";
         cmbavDynamicfiltersoperator1_Internalname = sPrefix+"vDYNAMICFILTERSOPERATOR1";
         edtavFuncaoapf_nome1_Internalname = sPrefix+"vFUNCAOAPF_NOME1";
         cmbavFuncaoapf_tipo1_Internalname = sPrefix+"vFUNCAOAPF_TIPO1";
         edtavFuncaoapf_pf1_Internalname = sPrefix+"vFUNCAOAPF_PF1";
         tblTablemergeddynamicfilters1_Internalname = sPrefix+"TABLEMERGEDDYNAMICFILTERS1";
         imgAdddynamicfilters1_Internalname = sPrefix+"ADDDYNAMICFILTERS1";
         imgRemovedynamicfilters1_Internalname = sPrefix+"REMOVEDYNAMICFILTERS1";
         lblDynamicfiltersprefix2_Internalname = sPrefix+"DYNAMICFILTERSPREFIX2";
         cmbavDynamicfiltersselector2_Internalname = sPrefix+"vDYNAMICFILTERSSELECTOR2";
         lblDynamicfiltersmiddle2_Internalname = sPrefix+"DYNAMICFILTERSMIDDLE2";
         cmbavDynamicfiltersoperator2_Internalname = sPrefix+"vDYNAMICFILTERSOPERATOR2";
         edtavFuncaoapf_nome2_Internalname = sPrefix+"vFUNCAOAPF_NOME2";
         cmbavFuncaoapf_tipo2_Internalname = sPrefix+"vFUNCAOAPF_TIPO2";
         edtavFuncaoapf_pf2_Internalname = sPrefix+"vFUNCAOAPF_PF2";
         tblTablemergeddynamicfilters2_Internalname = sPrefix+"TABLEMERGEDDYNAMICFILTERS2";
         imgRemovedynamicfilters2_Internalname = sPrefix+"REMOVEDYNAMICFILTERS2";
         tblTabledynamicfilters_Internalname = sPrefix+"TABLEDYNAMICFILTERS";
         lblJsdynamicfilters_Internalname = sPrefix+"JSDYNAMICFILTERS";
         tblTablefilters_Internalname = sPrefix+"TABLEFILTERS";
         tblTablesearch_Internalname = sPrefix+"TABLESEARCH";
         edtavUpdate_Internalname = sPrefix+"vUPDATE";
         edtavDelete_Internalname = sPrefix+"vDELETE";
         edtavDisplay_Internalname = sPrefix+"vDISPLAY";
         edtFuncaoAPF_Codigo_Internalname = sPrefix+"FUNCAOAPF_CODIGO";
         edtFuncaoAPF_SistemaCod_Internalname = sPrefix+"FUNCAOAPF_SISTEMACOD";
         edtFuncaoAPF_Nome_Internalname = sPrefix+"FUNCAOAPF_NOME";
         cmbFuncaoAPF_Tipo_Internalname = sPrefix+"FUNCAOAPF_TIPO";
         cmbFuncaoAPF_Complexidade_Internalname = sPrefix+"FUNCAOAPF_COMPLEXIDADE";
         edtFuncaoAPF_TD_Internalname = sPrefix+"FUNCAOAPF_TD";
         edtFuncaoAPF_AR_Internalname = sPrefix+"FUNCAOAPF_AR";
         edtFuncaoAPF_PF_Internalname = sPrefix+"FUNCAOAPF_PF";
         chkavComdc_Internalname = sPrefix+"vCOMDC";
         edtavFuncaoapf_link_Internalname = sPrefix+"vFUNCAOAPF_LINK";
         cmbFuncaoAPF_Ativo_Internalname = sPrefix+"FUNCAOAPF_ATIVO";
         edtFuncaoAPF_SolucaoTecnica_Internalname = sPrefix+"FUNCAOAPF_SOLUCAOTECNICA";
         edtavImagemfuncoesusuario_Internalname = sPrefix+"vIMAGEMFUNCOESUSUARIO";
         Gridpaginationbar_Internalname = sPrefix+"GRIDPAGINATIONBAR";
         tblGridtablewithpaginationbar_Internalname = sPrefix+"GRIDTABLEWITHPAGINATIONBAR";
         tblTablegridheader_Internalname = sPrefix+"TABLEGRIDHEADER";
         Workwithplusutilities1_Internalname = sPrefix+"WORKWITHPLUSUTILITIES1";
         chkavDynamicfiltersenabled2_Internalname = sPrefix+"vDYNAMICFILTERSENABLED2";
         Form.Internalname = sPrefix+"FORM";
         subGrid_Internalname = sPrefix+"GRID";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         edtavImagemfuncoesusuario_Jsonclick = "";
         edtavImagemfuncoesusuario_Visible = -1;
         edtavImagemfuncoesusuario_Enabled = 1;
         edtFuncaoAPF_SolucaoTecnica_Jsonclick = "";
         cmbFuncaoAPF_Ativo_Jsonclick = "";
         chkavComdc.Visible = -1;
         edtFuncaoAPF_PF_Jsonclick = "";
         edtFuncaoAPF_AR_Jsonclick = "";
         edtFuncaoAPF_TD_Jsonclick = "";
         cmbFuncaoAPF_Complexidade_Jsonclick = "";
         cmbFuncaoAPF_Tipo_Jsonclick = "";
         edtFuncaoAPF_Nome_Jsonclick = "";
         edtFuncaoAPF_SistemaCod_Jsonclick = "";
         edtFuncaoAPF_Codigo_Jsonclick = "";
         bttBtnmanter_Enabled = 1;
         imgInsert_Enabled = 1;
         imgInsert_Visible = 1;
         imgInsert_Bitmap = (String)(context.GetImagePath( "5649fbb8-8ce0-4810-a5ce-bd649ea83c3a", "", context.GetTheme( )));
         edtavFuncaoapf_pf1_Jsonclick = "";
         cmbavFuncaoapf_tipo1_Jsonclick = "";
         cmbavDynamicfiltersoperator1_Jsonclick = "";
         edtavFuncaoapf_pf2_Jsonclick = "";
         cmbavFuncaoapf_tipo2_Jsonclick = "";
         cmbavDynamicfiltersoperator2_Jsonclick = "";
         cmbavDynamicfiltersselector2_Jsonclick = "";
         imgRemovedynamicfilters1_Visible = 1;
         imgAdddynamicfilters1_Visible = 1;
         cmbavDynamicfiltersselector1_Jsonclick = "";
         edtavOrdereddsc_Jsonclick = "";
         cmbavOrderedby_Jsonclick = "";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         edtavImagemfuncoesusuario_Tooltiptext = "";
         edtavFuncaoapf_link_Tooltiptext = "";
         edtavFuncaoapf_link_Linktarget = "";
         edtavFuncaoapf_link_Link = "";
         chkavComdc.Enabled = 1;
         edtavDisplay_Tooltiptext = "Mostrar";
         edtavDisplay_Link = "";
         edtavDelete_Tooltiptext = "Eliminar";
         edtavDelete_Link = "";
         edtavDelete_Enabled = 1;
         edtavUpdate_Tooltiptext = "Modifica";
         edtavUpdate_Link = "";
         edtavUpdate_Enabled = 1;
         cmbFuncaoAPF_Ativo_Titleformat = 0;
         cmbFuncaoAPF_Tipo_Titleformat = 0;
         edtFuncaoAPF_Nome_Titleformat = 0;
         subGrid_Class = "WorkWithBorder WorkWith";
         cmbavDynamicfiltersoperator2.Visible = 1;
         edtavFuncaoapf_pf2_Visible = 1;
         cmbavFuncaoapf_tipo2.Visible = 1;
         edtavFuncaoapf_nome2_Visible = 1;
         cmbavDynamicfiltersoperator1.Visible = 1;
         edtavFuncaoapf_pf1_Visible = 1;
         cmbavFuncaoapf_tipo1.Visible = 1;
         edtavFuncaoapf_nome1_Visible = 1;
         edtavDisplay_Visible = -1;
         edtavDelete_Visible = -1;
         edtavUpdate_Visible = -1;
         cmbFuncaoAPF_Ativo.Title.Text = "Status da Fun��o";
         cmbFuncaoAPF_Tipo.Title.Text = "Tipo";
         edtFuncaoAPF_Nome_Title = "Nome";
         edtavOrdereddsc_Visible = 1;
         lblJsdynamicfilters_Caption = "JSDynamicFilters";
         subGrid_Sortable = 0;
         subGrid_Backcolorstyle = 3;
         chkavDynamicfiltersenabled2.Caption = "";
         chkavComdc.Caption = "";
         chkavDynamicfiltersenabled2.Visible = 1;
         Gridpaginationbar_Emptygridcaption = "N�o encontraram-se registros";
         Gridpaginationbar_Emptygridclass = "PaginationBarEmptyGrid";
         Gridpaginationbar_Pagingcaptionposition = "Left";
         Gridpaginationbar_Pagingbuttonsposition = "Right";
         Gridpaginationbar_Pagestoshow = 5;
         Gridpaginationbar_Showlast = Convert.ToBoolean( -1);
         Gridpaginationbar_Shownext = Convert.ToBoolean( -1);
         Gridpaginationbar_Showprevious = Convert.ToBoolean( -1);
         Gridpaginationbar_Showfirst = Convert.ToBoolean( -1);
         Gridpaginationbar_Caption = "P�gina <CURRENT_PAGE> de <TOTAL_PAGES>";
         Gridpaginationbar_Last = "�|";
         Gridpaginationbar_Next = "�";
         Gridpaginationbar_Previous = "�";
         Gridpaginationbar_First = "|�";
         Gridpaginationbar_Class = "PaginationBar";
         subGrid_Rows = 0;
         edtFuncaoAPF_SolucaoTecnica_Visible = -1;
         context.GX_msglist.DisplayMode = 1;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV21FuncaoAPF_SistemaCod',fld:'vFUNCAOAPF_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'edtFuncaoAPF_SolucaoTecnica_Visible',ctrl:'FUNCAOAPF_SOLUCAOTECNICA',prop:'Visible'},{av:'A165FuncaoAPF_Codigo',fld:'FUNCAOAPF_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A432FuncaoAPF_Link',fld:'FUNCAOAPF_LINK',pic:'',nv:''},{av:'A386FuncaoAPF_PF',fld:'FUNCAOAPF_PF',pic:'ZZ,ZZZ,ZZ9.999',hsh:true,nv:0.0},{av:'A415FuncaoAPFAtributos_FuncaoDadosTip',fld:'FUNCAOAPFATRIBUTOS_FUNCAODADOSTIP',pic:'',nv:''},{av:'sPrefix',nv:''},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV90Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV35DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV25DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV27FuncaoAPF_Nome1',fld:'vFUNCAOAPF_NOME1',pic:'',nv:''},{av:'AV28FuncaoAPF_Tipo1',fld:'vFUNCAOAPF_TIPO1',pic:'',nv:''},{av:'AV38FuncaoAPF_PF1',fld:'vFUNCAOAPF_PF1',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV26DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV34DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV29DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV30DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV32FuncaoAPF_Nome2',fld:'vFUNCAOAPF_NOME2',pic:'',nv:''},{av:'AV33FuncaoAPF_Tipo2',fld:'vFUNCAOAPF_TIPO2',pic:'',nv:''},{av:'AV39FuncaoAPF_PF2',fld:'vFUNCAOAPF_PF2',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV31DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0}],oparms:[{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'edtFuncaoAPF_Nome_Titleformat',ctrl:'FUNCAOAPF_NOME',prop:'Titleformat'},{av:'edtFuncaoAPF_Nome_Title',ctrl:'FUNCAOAPF_NOME',prop:'Title'},{av:'cmbFuncaoAPF_Tipo'},{av:'cmbFuncaoAPF_Ativo'},{av:'AV77GridCurrentPage',fld:'vGRIDCURRENTPAGE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV78GridPageCount',fld:'vGRIDPAGECOUNT',pic:'ZZZZZZZZZ9',nv:0},{av:'imgInsert_Visible',ctrl:'INSERT',prop:'Visible'},{av:'edtavUpdate_Visible',ctrl:'vUPDATE',prop:'Visible'},{av:'edtavDelete_Visible',ctrl:'vDELETE',prop:'Visible'},{av:'edtavDisplay_Visible',ctrl:'vDISPLAY',prop:'Visible'},{av:'imgInsert_Link',ctrl:'INSERT',prop:'Link'},{av:'imgInsert_Enabled',ctrl:'INSERT',prop:'Enabled'},{ctrl:'BTNMANTER',prop:'Enabled'},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null}]}");
         setEventMetadata("GRIDPAGINATIONBAR.CHANGEPAGE","{handler:'E119A2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV25DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV26DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV27FuncaoAPF_Nome1',fld:'vFUNCAOAPF_NOME1',pic:'',nv:''},{av:'AV28FuncaoAPF_Tipo1',fld:'vFUNCAOAPF_TIPO1',pic:'',nv:''},{av:'AV38FuncaoAPF_PF1',fld:'vFUNCAOAPF_PF1',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV30DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV31DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV32FuncaoAPF_Nome2',fld:'vFUNCAOAPF_NOME2',pic:'',nv:''},{av:'AV33FuncaoAPF_Tipo2',fld:'vFUNCAOAPF_TIPO2',pic:'',nv:''},{av:'AV39FuncaoAPF_PF2',fld:'vFUNCAOAPF_PF2',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV29DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV21FuncaoAPF_SistemaCod',fld:'vFUNCAOAPF_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'edtFuncaoAPF_SolucaoTecnica_Visible',ctrl:'FUNCAOAPF_SOLUCAOTECNICA',prop:'Visible'},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV90Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV35DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV34DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A165FuncaoAPF_Codigo',fld:'FUNCAOAPF_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A432FuncaoAPF_Link',fld:'FUNCAOAPF_LINK',pic:'',nv:''},{av:'A386FuncaoAPF_PF',fld:'FUNCAOAPF_PF',pic:'ZZ,ZZZ,ZZ9.999',hsh:true,nv:0.0},{av:'A415FuncaoAPFAtributos_FuncaoDadosTip',fld:'FUNCAOAPFATRIBUTOS_FUNCAODADOSTIP',pic:'',nv:''},{av:'sPrefix',nv:''},{av:'Gridpaginationbar_Selectedpage',ctrl:'GRIDPAGINATIONBAR',prop:'SelectedPage'}],oparms:[]}");
         setEventMetadata("GRID.LOAD","{handler:'E239A2',iparms:[{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'A165FuncaoAPF_Codigo',fld:'FUNCAOAPF_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV21FuncaoAPF_SistemaCod',fld:'vFUNCAOAPF_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'A432FuncaoAPF_Link',fld:'FUNCAOAPF_LINK',pic:'',nv:''},{av:'A386FuncaoAPF_PF',fld:'FUNCAOAPF_PF',pic:'ZZ,ZZZ,ZZ9.999',hsh:true,nv:0.0},{av:'A415FuncaoAPFAtributos_FuncaoDadosTip',fld:'FUNCAOAPFATRIBUTOS_FUNCAODADOSTIP',pic:'',nv:''}],oparms:[{av:'AV23ImagemFuncoesUsuario',fld:'vIMAGEMFUNCOESUSUARIO',pic:'',nv:''},{av:'edtavImagemfuncoesusuario_Tooltiptext',ctrl:'vIMAGEMFUNCOESUSUARIO',prop:'Tooltiptext'},{av:'edtavUpdate_Tooltiptext',ctrl:'vUPDATE',prop:'Tooltiptext'},{av:'edtavUpdate_Link',ctrl:'vUPDATE',prop:'Link'},{av:'AV17Update',fld:'vUPDATE',pic:'',nv:''},{av:'edtavUpdate_Enabled',ctrl:'vUPDATE',prop:'Enabled'},{av:'edtavDelete_Tooltiptext',ctrl:'vDELETE',prop:'Tooltiptext'},{av:'edtavDelete_Link',ctrl:'vDELETE',prop:'Link'},{av:'AV18Delete',fld:'vDELETE',pic:'',nv:''},{av:'edtavDelete_Enabled',ctrl:'vDELETE',prop:'Enabled'},{av:'AV19Display',fld:'vDISPLAY',pic:'',nv:''},{av:'edtavDisplay_Tooltiptext',ctrl:'vDISPLAY',prop:'Tooltiptext'},{av:'edtavDisplay_Link',ctrl:'vDISPLAY',prop:'Link'},{av:'edtavFuncaoapf_link_Linktarget',ctrl:'vFUNCAOAPF_LINK',prop:'Linktarget'},{av:'edtavFuncaoapf_link_Tooltiptext',ctrl:'vFUNCAOAPF_LINK',prop:'Tooltiptext'},{av:'edtavFuncaoapf_link_Link',ctrl:'vFUNCAOAPF_LINK',prop:'Link'},{av:'AV37FuncaoAPF_Link',fld:'vFUNCAOAPF_LINK',pic:'',nv:''},{av:'AV36ComDC',fld:'vCOMDC',pic:'',nv:false}]}");
         setEventMetadata("VORDEREDBY.CLICK","{handler:'E129A2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV25DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV26DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV27FuncaoAPF_Nome1',fld:'vFUNCAOAPF_NOME1',pic:'',nv:''},{av:'AV28FuncaoAPF_Tipo1',fld:'vFUNCAOAPF_TIPO1',pic:'',nv:''},{av:'AV38FuncaoAPF_PF1',fld:'vFUNCAOAPF_PF1',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV30DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV31DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV32FuncaoAPF_Nome2',fld:'vFUNCAOAPF_NOME2',pic:'',nv:''},{av:'AV33FuncaoAPF_Tipo2',fld:'vFUNCAOAPF_TIPO2',pic:'',nv:''},{av:'AV39FuncaoAPF_PF2',fld:'vFUNCAOAPF_PF2',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV29DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV21FuncaoAPF_SistemaCod',fld:'vFUNCAOAPF_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'edtFuncaoAPF_SolucaoTecnica_Visible',ctrl:'FUNCAOAPF_SOLUCAOTECNICA',prop:'Visible'},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV90Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV35DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV34DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A165FuncaoAPF_Codigo',fld:'FUNCAOAPF_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A432FuncaoAPF_Link',fld:'FUNCAOAPF_LINK',pic:'',nv:''},{av:'A386FuncaoAPF_PF',fld:'FUNCAOAPF_PF',pic:'ZZ,ZZZ,ZZ9.999',hsh:true,nv:0.0},{av:'A415FuncaoAPFAtributos_FuncaoDadosTip',fld:'FUNCAOAPFATRIBUTOS_FUNCAODADOSTIP',pic:'',nv:''},{av:'sPrefix',nv:''}],oparms:[]}");
         setEventMetadata("'ADDDYNAMICFILTERS1'","{handler:'E189A2',iparms:[],oparms:[{av:'AV29DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS1'","{handler:'E139A2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV25DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV26DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV27FuncaoAPF_Nome1',fld:'vFUNCAOAPF_NOME1',pic:'',nv:''},{av:'AV28FuncaoAPF_Tipo1',fld:'vFUNCAOAPF_TIPO1',pic:'',nv:''},{av:'AV38FuncaoAPF_PF1',fld:'vFUNCAOAPF_PF1',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV30DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV31DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV32FuncaoAPF_Nome2',fld:'vFUNCAOAPF_NOME2',pic:'',nv:''},{av:'AV33FuncaoAPF_Tipo2',fld:'vFUNCAOAPF_TIPO2',pic:'',nv:''},{av:'AV39FuncaoAPF_PF2',fld:'vFUNCAOAPF_PF2',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV29DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV21FuncaoAPF_SistemaCod',fld:'vFUNCAOAPF_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'edtFuncaoAPF_SolucaoTecnica_Visible',ctrl:'FUNCAOAPF_SOLUCAOTECNICA',prop:'Visible'},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV90Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV35DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV34DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A165FuncaoAPF_Codigo',fld:'FUNCAOAPF_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A432FuncaoAPF_Link',fld:'FUNCAOAPF_LINK',pic:'',nv:''},{av:'A386FuncaoAPF_PF',fld:'FUNCAOAPF_PF',pic:'ZZ,ZZZ,ZZ9.999',hsh:true,nv:0.0},{av:'A415FuncaoAPFAtributos_FuncaoDadosTip',fld:'FUNCAOAPFATRIBUTOS_FUNCAODADOSTIP',pic:'',nv:''},{av:'sPrefix',nv:''}],oparms:[{av:'AV34DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV35DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV29DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV30DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV32FuncaoAPF_Nome2',fld:'vFUNCAOAPF_NOME2',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'},{av:'cmbavDynamicfiltersoperator2'},{av:'AV25DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV27FuncaoAPF_Nome1',fld:'vFUNCAOAPF_NOME1',pic:'',nv:''},{av:'AV28FuncaoAPF_Tipo1',fld:'vFUNCAOAPF_TIPO1',pic:'',nv:''},{av:'AV26DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV38FuncaoAPF_PF1',fld:'vFUNCAOAPF_PF1',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV33FuncaoAPF_Tipo2',fld:'vFUNCAOAPF_TIPO2',pic:'',nv:''},{av:'AV31DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV39FuncaoAPF_PF2',fld:'vFUNCAOAPF_PF2',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'edtavFuncaoapf_nome2_Visible',ctrl:'vFUNCAOAPF_NOME2',prop:'Visible'},{av:'cmbavFuncaoapf_tipo2'},{av:'edtavFuncaoapf_pf2_Visible',ctrl:'vFUNCAOAPF_PF2',prop:'Visible'},{av:'edtavFuncaoapf_nome1_Visible',ctrl:'vFUNCAOAPF_NOME1',prop:'Visible'},{av:'cmbavFuncaoapf_tipo1'},{av:'edtavFuncaoapf_pf1_Visible',ctrl:'vFUNCAOAPF_PF1',prop:'Visible'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR1.CLICK","{handler:'E199A2',iparms:[{av:'AV25DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''}],oparms:[{av:'edtavFuncaoapf_nome1_Visible',ctrl:'vFUNCAOAPF_NOME1',prop:'Visible'},{av:'cmbavFuncaoapf_tipo1'},{av:'edtavFuncaoapf_pf1_Visible',ctrl:'vFUNCAOAPF_PF1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'}]}");
         setEventMetadata("'REMOVEDYNAMICFILTERS2'","{handler:'E149A2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV25DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV26DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV27FuncaoAPF_Nome1',fld:'vFUNCAOAPF_NOME1',pic:'',nv:''},{av:'AV28FuncaoAPF_Tipo1',fld:'vFUNCAOAPF_TIPO1',pic:'',nv:''},{av:'AV38FuncaoAPF_PF1',fld:'vFUNCAOAPF_PF1',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV30DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV31DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV32FuncaoAPF_Nome2',fld:'vFUNCAOAPF_NOME2',pic:'',nv:''},{av:'AV33FuncaoAPF_Tipo2',fld:'vFUNCAOAPF_TIPO2',pic:'',nv:''},{av:'AV39FuncaoAPF_PF2',fld:'vFUNCAOAPF_PF2',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV29DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV21FuncaoAPF_SistemaCod',fld:'vFUNCAOAPF_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'edtFuncaoAPF_SolucaoTecnica_Visible',ctrl:'FUNCAOAPF_SOLUCAOTECNICA',prop:'Visible'},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV90Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV35DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV34DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A165FuncaoAPF_Codigo',fld:'FUNCAOAPF_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A432FuncaoAPF_Link',fld:'FUNCAOAPF_LINK',pic:'',nv:''},{av:'A386FuncaoAPF_PF',fld:'FUNCAOAPF_PF',pic:'ZZ,ZZZ,ZZ9.999',hsh:true,nv:0.0},{av:'A415FuncaoAPFAtributos_FuncaoDadosTip',fld:'FUNCAOAPFATRIBUTOS_FUNCAODADOSTIP',pic:'',nv:''},{av:'sPrefix',nv:''}],oparms:[{av:'AV34DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'AV29DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV30DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV32FuncaoAPF_Nome2',fld:'vFUNCAOAPF_NOME2',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'},{av:'cmbavDynamicfiltersoperator2'},{av:'AV25DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV27FuncaoAPF_Nome1',fld:'vFUNCAOAPF_NOME1',pic:'',nv:''},{av:'AV28FuncaoAPF_Tipo1',fld:'vFUNCAOAPF_TIPO1',pic:'',nv:''},{av:'AV26DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV38FuncaoAPF_PF1',fld:'vFUNCAOAPF_PF1',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV33FuncaoAPF_Tipo2',fld:'vFUNCAOAPF_TIPO2',pic:'',nv:''},{av:'AV31DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV39FuncaoAPF_PF2',fld:'vFUNCAOAPF_PF2',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'edtavFuncaoapf_nome2_Visible',ctrl:'vFUNCAOAPF_NOME2',prop:'Visible'},{av:'cmbavFuncaoapf_tipo2'},{av:'edtavFuncaoapf_pf2_Visible',ctrl:'vFUNCAOAPF_PF2',prop:'Visible'},{av:'edtavFuncaoapf_nome1_Visible',ctrl:'vFUNCAOAPF_NOME1',prop:'Visible'},{av:'cmbavFuncaoapf_tipo1'},{av:'edtavFuncaoapf_pf1_Visible',ctrl:'vFUNCAOAPF_PF1',prop:'Visible'}]}");
         setEventMetadata("VDYNAMICFILTERSSELECTOR2.CLICK","{handler:'E209A2',iparms:[{av:'AV30DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''}],oparms:[{av:'edtavFuncaoapf_nome2_Visible',ctrl:'vFUNCAOAPF_NOME2',prop:'Visible'},{av:'cmbavFuncaoapf_tipo2'},{av:'edtavFuncaoapf_pf2_Visible',ctrl:'vFUNCAOAPF_PF2',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'}]}");
         setEventMetadata("'DOCLEANFILTERS'","{handler:'E159A2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV13OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV14OrderedDsc',fld:'vORDEREDDSC',pic:'',nv:false},{av:'AV25DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV26DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV27FuncaoAPF_Nome1',fld:'vFUNCAOAPF_NOME1',pic:'',nv:''},{av:'AV28FuncaoAPF_Tipo1',fld:'vFUNCAOAPF_TIPO1',pic:'',nv:''},{av:'AV38FuncaoAPF_PF1',fld:'vFUNCAOAPF_PF1',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV30DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV31DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV32FuncaoAPF_Nome2',fld:'vFUNCAOAPF_NOME2',pic:'',nv:''},{av:'AV33FuncaoAPF_Tipo2',fld:'vFUNCAOAPF_TIPO2',pic:'',nv:''},{av:'AV39FuncaoAPF_PF2',fld:'vFUNCAOAPF_PF2',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'AV29DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV21FuncaoAPF_SistemaCod',fld:'vFUNCAOAPF_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'edtFuncaoAPF_SolucaoTecnica_Visible',ctrl:'FUNCAOAPF_SOLUCAOTECNICA',prop:'Visible'},{av:'AV6WWPContext',fld:'vWWPCONTEXT',pic:'',nv:null},{av:'AV90Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'AV35DynamicFiltersIgnoreFirst',fld:'vDYNAMICFILTERSIGNOREFIRST',pic:'',nv:false},{av:'AV34DynamicFiltersRemoving',fld:'vDYNAMICFILTERSREMOVING',pic:'',nv:false},{av:'A165FuncaoAPF_Codigo',fld:'FUNCAOAPF_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A432FuncaoAPF_Link',fld:'FUNCAOAPF_LINK',pic:'',nv:''},{av:'A386FuncaoAPF_PF',fld:'FUNCAOAPF_PF',pic:'ZZ,ZZZ,ZZ9.999',hsh:true,nv:0.0},{av:'A415FuncaoAPFAtributos_FuncaoDadosTip',fld:'FUNCAOAPFATRIBUTOS_FUNCAODADOSTIP',pic:'',nv:''},{av:'sPrefix',nv:''}],oparms:[{av:'AV25DynamicFiltersSelector1',fld:'vDYNAMICFILTERSSELECTOR1',pic:'',nv:''},{av:'AV27FuncaoAPF_Nome1',fld:'vFUNCAOAPF_NOME1',pic:'',nv:''},{av:'AV11GridState',fld:'vGRIDSTATE',pic:'',nv:null},{av:'edtavFuncaoapf_nome1_Visible',ctrl:'vFUNCAOAPF_NOME1',prop:'Visible'},{av:'cmbavFuncaoapf_tipo1'},{av:'edtavFuncaoapf_pf1_Visible',ctrl:'vFUNCAOAPF_PF1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator1'},{av:'AV29DynamicFiltersEnabled2',fld:'vDYNAMICFILTERSENABLED2',pic:'',nv:false},{av:'AV30DynamicFiltersSelector2',fld:'vDYNAMICFILTERSSELECTOR2',pic:'',nv:''},{av:'AV32FuncaoAPF_Nome2',fld:'vFUNCAOAPF_NOME2',pic:'',nv:''},{av:'imgAdddynamicfilters1_Visible',ctrl:'ADDDYNAMICFILTERS1',prop:'Visible'},{av:'imgRemovedynamicfilters1_Visible',ctrl:'REMOVEDYNAMICFILTERS1',prop:'Visible'},{av:'cmbavDynamicfiltersoperator2'},{av:'AV28FuncaoAPF_Tipo1',fld:'vFUNCAOAPF_TIPO1',pic:'',nv:''},{av:'AV26DynamicFiltersOperator1',fld:'vDYNAMICFILTERSOPERATOR1',pic:'ZZZ9',nv:0},{av:'AV38FuncaoAPF_PF1',fld:'vFUNCAOAPF_PF1',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'lblJsdynamicfilters_Caption',ctrl:'JSDYNAMICFILTERS',prop:'Caption'},{av:'AV33FuncaoAPF_Tipo2',fld:'vFUNCAOAPF_TIPO2',pic:'',nv:''},{av:'AV31DynamicFiltersOperator2',fld:'vDYNAMICFILTERSOPERATOR2',pic:'ZZZ9',nv:0},{av:'AV39FuncaoAPF_PF2',fld:'vFUNCAOAPF_PF2',pic:'ZZ,ZZZ,ZZ9.999',nv:0.0},{av:'edtavFuncaoapf_nome2_Visible',ctrl:'vFUNCAOAPF_NOME2',prop:'Visible'},{av:'cmbavFuncaoapf_tipo2'},{av:'edtavFuncaoapf_pf2_Visible',ctrl:'vFUNCAOAPF_PF2',prop:'Visible'}]}");
         setEventMetadata("'DOINSERT'","{handler:'E169A2',iparms:[{av:'AV21FuncaoAPF_SistemaCod',fld:'vFUNCAOAPF_SISTEMACOD',pic:'ZZZZZ9',nv:0}],oparms:[{av:'AV21FuncaoAPF_SistemaCod',fld:'vFUNCAOAPF_SISTEMACOD',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("'DOMANTER'","{handler:'E179A2',iparms:[{av:'AV21FuncaoAPF_SistemaCod',fld:'vFUNCAOAPF_SISTEMACOD',pic:'ZZZZZ9',nv:0},{av:'AV40Sistema_Sigla',fld:'vSISTEMA_SIGLA',pic:'@!',nv:''}],oparms:[{av:'AV40Sistema_Sigla',fld:'vSISTEMA_SIGLA',pic:'@!',nv:''},{av:'AV21FuncaoAPF_SistemaCod',fld:'vFUNCAOAPF_SISTEMACOD',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("VIMAGEMFUNCOESUSUARIO.CLICK","{handler:'E249A2',iparms:[{av:'A165FuncaoAPF_Codigo',fld:'FUNCAOAPF_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A360FuncaoAPF_SistemaCod',fld:'FUNCAOAPF_SISTEMACOD',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Gridpaginationbar_Selectedpage = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         AV25DynamicFiltersSelector1 = "";
         AV27FuncaoAPF_Nome1 = "";
         AV28FuncaoAPF_Tipo1 = "";
         AV30DynamicFiltersSelector2 = "";
         AV32FuncaoAPF_Nome2 = "";
         AV33FuncaoAPF_Tipo2 = "";
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV90Pgmname = "";
         AV11GridState = new wwpbaseobjects.SdtWWPGridState(context);
         A432FuncaoAPF_Link = "";
         A415FuncaoAPFAtributos_FuncaoDadosTip = "";
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV40Sistema_Sigla = "";
         GX_FocusControl = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV17Update = "";
         AV85Update_GXI = "";
         AV18Delete = "";
         AV86Delete_GXI = "";
         AV19Display = "";
         AV87Display_GXI = "";
         A166FuncaoAPF_Nome = "";
         A184FuncaoAPF_Tipo = "";
         A185FuncaoAPF_Complexidade = "";
         AV37FuncaoAPF_Link = "";
         AV88Funcaoapf_link_GXI = "";
         A183FuncaoAPF_Ativo = "";
         A757FuncaoAPF_SolucaoTecnica = "";
         AV23ImagemFuncoesUsuario = "";
         AV84Imagemfuncoesusuario_GXI = "";
         GXCCtl = "";
         GridContainer = new GXWebGrid( context);
         scmdbuf = "";
         lV27FuncaoAPF_Nome1 = "";
         lV32FuncaoAPF_Nome2 = "";
         H009A2_A432FuncaoAPF_Link = new String[] {""} ;
         H009A2_n432FuncaoAPF_Link = new bool[] {false} ;
         H009A2_A183FuncaoAPF_Ativo = new String[] {""} ;
         H009A2_A184FuncaoAPF_Tipo = new String[] {""} ;
         H009A2_A166FuncaoAPF_Nome = new String[] {""} ;
         H009A2_A360FuncaoAPF_SistemaCod = new int[1] ;
         H009A2_n360FuncaoAPF_SistemaCod = new bool[] {false} ;
         H009A2_A165FuncaoAPF_Codigo = new int[1] ;
         H009A2_A744FuncaoAPF_MelhoraCod = new int[1] ;
         H009A2_n744FuncaoAPF_MelhoraCod = new bool[] {false} ;
         GXt_char1 = "";
         imgAdddynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters1_Jsonclick = "";
         imgRemovedynamicfilters2_Jsonclick = "";
         imgCleanfilters_Jsonclick = "";
         H009A3_A127Sistema_Codigo = new int[1] ;
         H009A3_A699Sistema_Tipo = new String[] {""} ;
         H009A3_n699Sistema_Tipo = new bool[] {false} ;
         H009A3_A129Sistema_Sigla = new String[] {""} ;
         A699Sistema_Tipo = "";
         A129Sistema_Sigla = "";
         H009A4_A364FuncaoAPFAtributos_AtributosCod = new int[1] ;
         H009A4_A378FuncaoAPFAtributos_FuncaoDadosCod = new int[1] ;
         H009A4_n378FuncaoAPFAtributos_FuncaoDadosCod = new bool[] {false} ;
         H009A4_A165FuncaoAPF_Codigo = new int[1] ;
         H009A4_A415FuncaoAPFAtributos_FuncaoDadosTip = new String[] {""} ;
         H009A4_n415FuncaoAPFAtributos_FuncaoDadosTip = new bool[] {false} ;
         GridRow = new GXWebRow();
         imgInsert_Link = "";
         AV20Session = context.GetSession();
         AV24GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV8HTTPRequest = new GxHttpRequest( context);
         AV10TrnContextAtt = new wwpbaseobjects.SdtWWPTransactionContext_Attribute(context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblOrderedtext_Jsonclick = "";
         lblJsdynamicfilters_Jsonclick = "";
         lblDynamicfiltersprefix1_Jsonclick = "";
         lblDynamicfiltersmiddle1_Jsonclick = "";
         lblDynamicfiltersprefix2_Jsonclick = "";
         lblDynamicfiltersmiddle2_Jsonclick = "";
         imgInsert_Jsonclick = "";
         bttBtnmanter_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlAV21FuncaoAPF_SistemaCod = "";
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.sistemafuncaopapfwc__default(),
            new Object[][] {
                new Object[] {
               H009A2_A432FuncaoAPF_Link, H009A2_n432FuncaoAPF_Link, H009A2_A183FuncaoAPF_Ativo, H009A2_A184FuncaoAPF_Tipo, H009A2_A166FuncaoAPF_Nome, H009A2_A360FuncaoAPF_SistemaCod, H009A2_n360FuncaoAPF_SistemaCod, H009A2_A165FuncaoAPF_Codigo, H009A2_A744FuncaoAPF_MelhoraCod, H009A2_n744FuncaoAPF_MelhoraCod
               }
               , new Object[] {
               H009A3_A127Sistema_Codigo, H009A3_A699Sistema_Tipo, H009A3_n699Sistema_Tipo, H009A3_A129Sistema_Sigla
               }
               , new Object[] {
               H009A4_A364FuncaoAPFAtributos_AtributosCod, H009A4_A378FuncaoAPFAtributos_FuncaoDadosCod, H009A4_n378FuncaoAPFAtributos_FuncaoDadosCod, H009A4_A165FuncaoAPF_Codigo, H009A4_A415FuncaoAPFAtributos_FuncaoDadosTip, H009A4_n415FuncaoAPFAtributos_FuncaoDadosTip
               }
            }
         );
         AV90Pgmname = "SistemaFuncaopAPFWC";
         /* GeneXus formulas. */
         AV90Pgmname = "SistemaFuncaopAPFWC";
         context.Gx_err = 0;
         chkavComdc.Enabled = 0;
      }

      private short nRcdExists_4 ;
      private short nIsMod_4 ;
      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short nRC_GXsfl_72 ;
      private short nGXsfl_72_idx=1 ;
      private short AV13OrderedBy ;
      private short AV26DynamicFiltersOperator1 ;
      private short AV31DynamicFiltersOperator2 ;
      private short initialized ;
      private short GRID_nEOF ;
      private short wbEnd ;
      private short wbStart ;
      private short nDraw ;
      private short nDoneStart ;
      private short A388FuncaoAPF_TD ;
      private short A387FuncaoAPF_AR ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_72_Refreshing=0 ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short GXt_int3 ;
      private short edtFuncaoAPF_Nome_Titleformat ;
      private short cmbFuncaoAPF_Tipo_Titleformat ;
      private short cmbFuncaoAPF_Ativo_Titleformat ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int AV21FuncaoAPF_SistemaCod ;
      private int wcpOAV21FuncaoAPF_SistemaCod ;
      private int edtFuncaoAPF_SolucaoTecnica_Visible ;
      private int subGrid_Rows ;
      private int A165FuncaoAPF_Codigo ;
      private int A744FuncaoAPF_MelhoraCod ;
      private int Gridpaginationbar_Pagestoshow ;
      private int A360FuncaoAPF_SistemaCod ;
      private int subGrid_Islastpage ;
      private int GXt_int2 ;
      private int edtavOrdereddsc_Visible ;
      private int A127Sistema_Codigo ;
      private int imgInsert_Visible ;
      private int edtavUpdate_Visible ;
      private int edtavDelete_Visible ;
      private int edtavDisplay_Visible ;
      private int AV76PageToGo ;
      private int edtavUpdate_Enabled ;
      private int edtavDelete_Enabled ;
      private int A378FuncaoAPFAtributos_FuncaoDadosCod ;
      private int imgAdddynamicfilters1_Visible ;
      private int imgRemovedynamicfilters1_Visible ;
      private int imgInsert_Enabled ;
      private int bttBtnmanter_Enabled ;
      private int edtavFuncaoapf_nome1_Visible ;
      private int edtavFuncaoapf_pf1_Visible ;
      private int edtavFuncaoapf_nome2_Visible ;
      private int edtavFuncaoapf_pf2_Visible ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private int edtavImagemfuncoesusuario_Enabled ;
      private int edtavImagemfuncoesusuario_Visible ;
      private long GRID_nFirstRecordOnPage ;
      private long AV77GridCurrentPage ;
      private long AV78GridPageCount ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private decimal AV38FuncaoAPF_PF1 ;
      private decimal AV39FuncaoAPF_PF2 ;
      private decimal A386FuncaoAPF_PF ;
      private decimal GXt_decimal4 ;
      private String Gridpaginationbar_Selectedpage ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String sGXsfl_72_idx="0001" ;
      private String edtFuncaoAPF_SolucaoTecnica_Internalname ;
      private String AV28FuncaoAPF_Tipo1 ;
      private String AV33FuncaoAPF_Tipo2 ;
      private String AV90Pgmname ;
      private String A415FuncaoAPFAtributos_FuncaoDadosTip ;
      private String GXKey ;
      private String chkavComdc_Internalname ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String AV40Sistema_Sigla ;
      private String Gridpaginationbar_Class ;
      private String Gridpaginationbar_First ;
      private String Gridpaginationbar_Previous ;
      private String Gridpaginationbar_Next ;
      private String Gridpaginationbar_Last ;
      private String Gridpaginationbar_Caption ;
      private String Gridpaginationbar_Pagingbuttonsposition ;
      private String Gridpaginationbar_Pagingcaptionposition ;
      private String Gridpaginationbar_Emptygridclass ;
      private String Gridpaginationbar_Emptygridcaption ;
      private String GX_FocusControl ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String chkavDynamicfiltersenabled2_Internalname ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavUpdate_Internalname ;
      private String edtavDelete_Internalname ;
      private String edtavDisplay_Internalname ;
      private String edtFuncaoAPF_Codigo_Internalname ;
      private String edtFuncaoAPF_SistemaCod_Internalname ;
      private String edtFuncaoAPF_Nome_Internalname ;
      private String cmbFuncaoAPF_Tipo_Internalname ;
      private String A184FuncaoAPF_Tipo ;
      private String cmbFuncaoAPF_Complexidade_Internalname ;
      private String A185FuncaoAPF_Complexidade ;
      private String edtFuncaoAPF_TD_Internalname ;
      private String edtFuncaoAPF_AR_Internalname ;
      private String edtFuncaoAPF_PF_Internalname ;
      private String edtavFuncaoapf_link_Internalname ;
      private String cmbFuncaoAPF_Ativo_Internalname ;
      private String A183FuncaoAPF_Ativo ;
      private String edtavImagemfuncoesusuario_Internalname ;
      private String GXCCtl ;
      private String cmbavOrderedby_Internalname ;
      private String scmdbuf ;
      private String GXt_char1 ;
      private String edtavOrdereddsc_Internalname ;
      private String cmbavDynamicfiltersselector1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Internalname ;
      private String edtavFuncaoapf_nome1_Internalname ;
      private String cmbavFuncaoapf_tipo1_Internalname ;
      private String edtavFuncaoapf_pf1_Internalname ;
      private String cmbavDynamicfiltersselector2_Internalname ;
      private String cmbavDynamicfiltersoperator2_Internalname ;
      private String edtavFuncaoapf_nome2_Internalname ;
      private String cmbavFuncaoapf_tipo2_Internalname ;
      private String edtavFuncaoapf_pf2_Internalname ;
      private String lblJsdynamicfilters_Caption ;
      private String lblJsdynamicfilters_Internalname ;
      private String imgAdddynamicfilters1_Jsonclick ;
      private String imgAdddynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters1_Jsonclick ;
      private String imgRemovedynamicfilters1_Internalname ;
      private String imgRemovedynamicfilters2_Jsonclick ;
      private String imgRemovedynamicfilters2_Internalname ;
      private String imgCleanfilters_Jsonclick ;
      private String imgCleanfilters_Internalname ;
      private String A699Sistema_Tipo ;
      private String A129Sistema_Sigla ;
      private String edtFuncaoAPF_Nome_Title ;
      private String imgInsert_Internalname ;
      private String edtavImagemfuncoesusuario_Tooltiptext ;
      private String edtavUpdate_Tooltiptext ;
      private String edtavUpdate_Link ;
      private String edtavDelete_Tooltiptext ;
      private String edtavDelete_Link ;
      private String edtavDisplay_Tooltiptext ;
      private String edtavDisplay_Link ;
      private String edtavFuncaoapf_link_Tooltiptext ;
      private String edtavFuncaoapf_link_Link ;
      private String edtavFuncaoapf_link_Linktarget ;
      private String imgInsert_Link ;
      private String bttBtnmanter_Internalname ;
      private String sStyleString ;
      private String tblTablegridheader_Internalname ;
      private String tblGridtablewithpaginationbar_Internalname ;
      private String subGrid_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTablesearch_Internalname ;
      private String lblOrderedtext_Internalname ;
      private String lblOrderedtext_Jsonclick ;
      private String cmbavOrderedby_Jsonclick ;
      private String edtavOrdereddsc_Jsonclick ;
      private String tblTablefilters_Internalname ;
      private String lblJsdynamicfilters_Jsonclick ;
      private String tblTabledynamicfilters_Internalname ;
      private String lblDynamicfiltersprefix1_Internalname ;
      private String lblDynamicfiltersprefix1_Jsonclick ;
      private String cmbavDynamicfiltersselector1_Jsonclick ;
      private String lblDynamicfiltersmiddle1_Internalname ;
      private String lblDynamicfiltersmiddle1_Jsonclick ;
      private String lblDynamicfiltersprefix2_Internalname ;
      private String lblDynamicfiltersprefix2_Jsonclick ;
      private String cmbavDynamicfiltersselector2_Jsonclick ;
      private String lblDynamicfiltersmiddle2_Internalname ;
      private String lblDynamicfiltersmiddle2_Jsonclick ;
      private String tblTablemergeddynamicfilters2_Internalname ;
      private String cmbavDynamicfiltersoperator2_Jsonclick ;
      private String cmbavFuncaoapf_tipo2_Jsonclick ;
      private String edtavFuncaoapf_pf2_Jsonclick ;
      private String tblTablemergeddynamicfilters1_Internalname ;
      private String cmbavDynamicfiltersoperator1_Jsonclick ;
      private String cmbavFuncaoapf_tipo1_Jsonclick ;
      private String edtavFuncaoapf_pf1_Jsonclick ;
      private String tblTableactions_Internalname ;
      private String imgInsert_Jsonclick ;
      private String bttBtnmanter_Jsonclick ;
      private String sCtrlAV21FuncaoAPF_SistemaCod ;
      private String sGXsfl_72_fel_idx="0001" ;
      private String ROClassString ;
      private String edtFuncaoAPF_Codigo_Jsonclick ;
      private String edtFuncaoAPF_SistemaCod_Jsonclick ;
      private String edtFuncaoAPF_Nome_Jsonclick ;
      private String cmbFuncaoAPF_Tipo_Jsonclick ;
      private String cmbFuncaoAPF_Complexidade_Jsonclick ;
      private String edtFuncaoAPF_TD_Jsonclick ;
      private String edtFuncaoAPF_AR_Jsonclick ;
      private String edtFuncaoAPF_PF_Jsonclick ;
      private String cmbFuncaoAPF_Ativo_Jsonclick ;
      private String edtFuncaoAPF_SolucaoTecnica_Jsonclick ;
      private String edtavImagemfuncoesusuario_Jsonclick ;
      private String Gridpaginationbar_Internalname ;
      private String Workwithplusutilities1_Internalname ;
      private bool entryPointCalled ;
      private bool AV14OrderedDsc ;
      private bool AV29DynamicFiltersEnabled2 ;
      private bool AV35DynamicFiltersIgnoreFirst ;
      private bool AV34DynamicFiltersRemoving ;
      private bool n432FuncaoAPF_Link ;
      private bool n415FuncaoAPFAtributos_FuncaoDadosTip ;
      private bool toggleJsOutput ;
      private bool Gridpaginationbar_Showfirst ;
      private bool Gridpaginationbar_Showprevious ;
      private bool Gridpaginationbar_Shownext ;
      private bool Gridpaginationbar_Showlast ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n360FuncaoAPF_SistemaCod ;
      private bool AV36ComDC ;
      private bool n744FuncaoAPF_MelhoraCod ;
      private bool returnInSub ;
      private bool n699Sistema_Tipo ;
      private bool gx_refresh_fired ;
      private bool n378FuncaoAPFAtributos_FuncaoDadosCod ;
      private bool AV17Update_IsBlob ;
      private bool AV18Delete_IsBlob ;
      private bool AV19Display_IsBlob ;
      private bool AV37FuncaoAPF_Link_IsBlob ;
      private bool AV23ImagemFuncoesUsuario_IsBlob ;
      private String A757FuncaoAPF_SolucaoTecnica ;
      private String AV25DynamicFiltersSelector1 ;
      private String AV27FuncaoAPF_Nome1 ;
      private String AV30DynamicFiltersSelector2 ;
      private String AV32FuncaoAPF_Nome2 ;
      private String A432FuncaoAPF_Link ;
      private String AV85Update_GXI ;
      private String AV86Delete_GXI ;
      private String AV87Display_GXI ;
      private String A166FuncaoAPF_Nome ;
      private String AV88Funcaoapf_link_GXI ;
      private String AV84Imagemfuncoesusuario_GXI ;
      private String lV27FuncaoAPF_Nome1 ;
      private String lV32FuncaoAPF_Nome2 ;
      private String AV17Update ;
      private String AV18Delete ;
      private String AV19Display ;
      private String AV37FuncaoAPF_Link ;
      private String AV23ImagemFuncoesUsuario ;
      private String imgInsert_Bitmap ;
      private IGxSession AV20Session ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbavOrderedby ;
      private GXCombobox cmbavDynamicfiltersselector1 ;
      private GXCombobox cmbavDynamicfiltersoperator1 ;
      private GXCombobox cmbavFuncaoapf_tipo1 ;
      private GXCombobox cmbavDynamicfiltersselector2 ;
      private GXCombobox cmbavDynamicfiltersoperator2 ;
      private GXCombobox cmbavFuncaoapf_tipo2 ;
      private GXCombobox cmbFuncaoAPF_Tipo ;
      private GXCombobox cmbFuncaoAPF_Complexidade ;
      private GXCheckbox chkavComdc ;
      private GXCombobox cmbFuncaoAPF_Ativo ;
      private GXCheckbox chkavDynamicfiltersenabled2 ;
      private IDataStoreProvider pr_default ;
      private String[] H009A2_A432FuncaoAPF_Link ;
      private bool[] H009A2_n432FuncaoAPF_Link ;
      private String[] H009A2_A183FuncaoAPF_Ativo ;
      private String[] H009A2_A184FuncaoAPF_Tipo ;
      private String[] H009A2_A166FuncaoAPF_Nome ;
      private int[] H009A2_A360FuncaoAPF_SistemaCod ;
      private bool[] H009A2_n360FuncaoAPF_SistemaCod ;
      private int[] H009A2_A165FuncaoAPF_Codigo ;
      private int[] H009A2_A744FuncaoAPF_MelhoraCod ;
      private bool[] H009A2_n744FuncaoAPF_MelhoraCod ;
      private int[] H009A3_A127Sistema_Codigo ;
      private String[] H009A3_A699Sistema_Tipo ;
      private bool[] H009A3_n699Sistema_Tipo ;
      private String[] H009A3_A129Sistema_Sigla ;
      private int[] H009A4_A364FuncaoAPFAtributos_AtributosCod ;
      private int[] H009A4_A378FuncaoAPFAtributos_FuncaoDadosCod ;
      private bool[] H009A4_n378FuncaoAPFAtributos_FuncaoDadosCod ;
      private int[] H009A4_A165FuncaoAPF_Codigo ;
      private String[] H009A4_A415FuncaoAPFAtributos_FuncaoDadosTip ;
      private bool[] H009A4_n415FuncaoAPFAtributos_FuncaoDadosTip ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV8HTTPRequest ;
      private wwpbaseobjects.SdtWWPGridState AV11GridState ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV24GridStateDynamicFilter ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
      private wwpbaseobjects.SdtWWPTransactionContext_Attribute AV10TrnContextAtt ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
   }

   public class sistemafuncaopapfwc__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H009A2( IGxContext context ,
                                             String AV25DynamicFiltersSelector1 ,
                                             String AV27FuncaoAPF_Nome1 ,
                                             String AV28FuncaoAPF_Tipo1 ,
                                             bool AV29DynamicFiltersEnabled2 ,
                                             String AV30DynamicFiltersSelector2 ,
                                             String AV32FuncaoAPF_Nome2 ,
                                             String AV33FuncaoAPF_Tipo2 ,
                                             String A166FuncaoAPF_Nome ,
                                             String A184FuncaoAPF_Tipo ,
                                             short AV13OrderedBy ,
                                             bool AV14OrderedDsc ,
                                             int A360FuncaoAPF_SistemaCod ,
                                             int AV21FuncaoAPF_SistemaCod ,
                                             short AV26DynamicFiltersOperator1 ,
                                             decimal AV38FuncaoAPF_PF1 ,
                                             decimal A386FuncaoAPF_PF ,
                                             short AV31DynamicFiltersOperator2 ,
                                             decimal AV39FuncaoAPF_PF2 ,
                                             String A183FuncaoAPF_Ativo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int5 ;
         GXv_int5 = new short [5] ;
         Object[] GXv_Object6 ;
         GXv_Object6 = new Object [2] ;
         scmdbuf = "SELECT [FuncaoAPF_Link], [FuncaoAPF_Ativo], [FuncaoAPF_Tipo], [FuncaoAPF_Nome], [FuncaoAPF_SistemaCod], [FuncaoAPF_Codigo], [FuncaoAPF_MelhoraCod] FROM [FuncoesAPF] WITH (NOLOCK)";
         scmdbuf = scmdbuf + " WHERE ([FuncaoAPF_SistemaCod] = @AV21FuncaoAPF_SistemaCod)";
         scmdbuf = scmdbuf + " and ([FuncaoAPF_Ativo] = 'A')";
         if ( ( StringUtil.StrCmp(AV25DynamicFiltersSelector1, "FUNCAOAPF_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV27FuncaoAPF_Nome1)) ) )
         {
            sWhereString = sWhereString + " and ([FuncaoAPF_Nome] like @lV27FuncaoAPF_Nome1 + '%')";
         }
         else
         {
            GXv_int5[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV25DynamicFiltersSelector1, "FUNCAOAPF_TIPO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV28FuncaoAPF_Tipo1)) ) )
         {
            sWhereString = sWhereString + " and ([FuncaoAPF_Tipo] = @AV28FuncaoAPF_Tipo1)";
         }
         else
         {
            GXv_int5[2] = 1;
         }
         if ( AV29DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV30DynamicFiltersSelector2, "FUNCAOAPF_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV32FuncaoAPF_Nome2)) ) )
         {
            sWhereString = sWhereString + " and ([FuncaoAPF_Nome] like @lV32FuncaoAPF_Nome2 + '%')";
         }
         else
         {
            GXv_int5[3] = 1;
         }
         if ( AV29DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV30DynamicFiltersSelector2, "FUNCAOAPF_TIPO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV33FuncaoAPF_Tipo2)) ) )
         {
            sWhereString = sWhereString + " and ([FuncaoAPF_Tipo] = @AV33FuncaoAPF_Tipo2)";
         }
         else
         {
            GXv_int5[4] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         if ( AV13OrderedBy == 1 )
         {
            scmdbuf = scmdbuf + " ORDER BY [FuncaoAPF_Codigo]";
         }
         else if ( ( AV13OrderedBy == 2 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + " ORDER BY [FuncaoAPF_SistemaCod], [FuncaoAPF_Nome]";
         }
         else if ( ( AV13OrderedBy == 2 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + " ORDER BY [FuncaoAPF_SistemaCod] DESC, [FuncaoAPF_Nome] DESC";
         }
         else if ( ( AV13OrderedBy == 3 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + " ORDER BY [FuncaoAPF_SistemaCod], [FuncaoAPF_Tipo]";
         }
         else if ( ( AV13OrderedBy == 3 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + " ORDER BY [FuncaoAPF_SistemaCod] DESC, [FuncaoAPF_Tipo] DESC";
         }
         else if ( ( AV13OrderedBy == 4 ) && ! AV14OrderedDsc )
         {
            scmdbuf = scmdbuf + " ORDER BY [FuncaoAPF_SistemaCod], [FuncaoAPF_Ativo]";
         }
         else if ( ( AV13OrderedBy == 4 ) && ( AV14OrderedDsc ) )
         {
            scmdbuf = scmdbuf + " ORDER BY [FuncaoAPF_SistemaCod] DESC, [FuncaoAPF_Ativo] DESC";
         }
         GXv_Object6[0] = scmdbuf;
         GXv_Object6[1] = GXv_int5;
         return GXv_Object6 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H009A2(context, (String)dynConstraints[0] , (String)dynConstraints[1] , (String)dynConstraints[2] , (bool)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (String)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (short)dynConstraints[9] , (bool)dynConstraints[10] , (int)dynConstraints[11] , (int)dynConstraints[12] , (short)dynConstraints[13] , (decimal)dynConstraints[14] , (decimal)dynConstraints[15] , (short)dynConstraints[16] , (decimal)dynConstraints[17] , (String)dynConstraints[18] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH009A3 ;
          prmH009A3 = new Object[] {
          new Object[] {"@AV21FuncaoAPF_SistemaCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmH009A4 ;
          prmH009A4 = new Object[] {
          new Object[] {"@FuncaoAPF_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH009A2 ;
          prmH009A2 = new Object[] {
          new Object[] {"@AV21FuncaoAPF_SistemaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@lV27FuncaoAPF_Nome1",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV28FuncaoAPF_Tipo1",SqlDbType.Char,3,0} ,
          new Object[] {"@lV32FuncaoAPF_Nome2",SqlDbType.VarChar,200,0} ,
          new Object[] {"@AV33FuncaoAPF_Tipo2",SqlDbType.Char,3,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H009A2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH009A2,11,0,true,false )
             ,new CursorDef("H009A3", "SELECT TOP 1 [Sistema_Codigo], [Sistema_Tipo], [Sistema_Sigla] FROM [Sistema] WITH (NOLOCK) WHERE [Sistema_Codigo] = @AV21FuncaoAPF_SistemaCod ORDER BY [Sistema_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH009A3,1,0,false,true )
             ,new CursorDef("H009A4", "SELECT TOP 1 T1.[FuncaoAPFAtributos_AtributosCod], T1.[FuncaoAPFAtributos_FuncaoDadosCod] AS FuncaoAPFAtributos_FuncaoDadosCod, T1.[FuncaoAPF_Codigo], T2.[FuncaoDados_Tipo] AS FuncaoAPFAtributos_FuncaoDadosTip FROM ([FuncoesAPFAtributos] T1 WITH (NOLOCK) LEFT JOIN [FuncaoDados] T2 WITH (NOLOCK) ON T2.[FuncaoDados_Codigo] = T1.[FuncaoAPFAtributos_FuncaoDadosCod]) WHERE (T1.[FuncaoAPF_Codigo] = @FuncaoAPF_Codigo) AND (T2.[FuncaoDados_Tipo] = 'DC') ORDER BY T1.[FuncaoAPF_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH009A4,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 1) ;
                ((String[]) buf[3])[0] = rslt.getString(3, 3) ;
                ((String[]) buf[4])[0] = rslt.getVarchar(4) ;
                ((int[]) buf[5])[0] = rslt.getInt(5) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((int[]) buf[7])[0] = rslt.getInt(6) ;
                ((int[]) buf[8])[0] = rslt.getInt(7) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(7);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 1) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getString(3, 25) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((String[]) buf[4])[0] = rslt.getString(4, 3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[5]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[6]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[7]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[8]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[9]);
                }
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
