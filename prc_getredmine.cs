/*
               File: PRC_GetRedmine
        Description: Get Redmine
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:51:58.65
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_getredmine : GXProcedure
   {
      public prc_getredmine( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_getredmine( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_Contratante_Codigo ,
                           out String aP1_Redmine_User ,
                           out short aP2_Redmine_Secure ,
                           out String aP3_Redmine_Host ,
                           out String aP4_Redmine_Url ,
                           out String aP5_Redmine_Key ,
                           out String aP6_Redmine_Versao ,
                           out String aP7_Redmine_CampoSistema ,
                           out String aP8_Redmine_CampoServico )
      {
         this.AV11Contratante_Codigo = aP0_Contratante_Codigo;
         this.AV14Redmine_User = "" ;
         this.AV13Redmine_Secure = 0 ;
         this.AV10Redmine_Host = "" ;
         this.AV9Redmine_Url = "" ;
         this.AV8Redmine_Key = "" ;
         this.AV12Redmine_Versao = "" ;
         this.AV15Redmine_CampoSistema = "" ;
         this.AV16Redmine_CampoServico = "" ;
         initialize();
         executePrivate();
         aP0_Contratante_Codigo=this.AV11Contratante_Codigo;
         aP1_Redmine_User=this.AV14Redmine_User;
         aP2_Redmine_Secure=this.AV13Redmine_Secure;
         aP3_Redmine_Host=this.AV10Redmine_Host;
         aP4_Redmine_Url=this.AV9Redmine_Url;
         aP5_Redmine_Key=this.AV8Redmine_Key;
         aP6_Redmine_Versao=this.AV12Redmine_Versao;
         aP7_Redmine_CampoSistema=this.AV15Redmine_CampoSistema;
         aP8_Redmine_CampoServico=this.AV16Redmine_CampoServico;
      }

      public String executeUdp( ref int aP0_Contratante_Codigo ,
                                out String aP1_Redmine_User ,
                                out short aP2_Redmine_Secure ,
                                out String aP3_Redmine_Host ,
                                out String aP4_Redmine_Url ,
                                out String aP5_Redmine_Key ,
                                out String aP6_Redmine_Versao ,
                                out String aP7_Redmine_CampoSistema )
      {
         this.AV11Contratante_Codigo = aP0_Contratante_Codigo;
         this.AV14Redmine_User = "" ;
         this.AV13Redmine_Secure = 0 ;
         this.AV10Redmine_Host = "" ;
         this.AV9Redmine_Url = "" ;
         this.AV8Redmine_Key = "" ;
         this.AV12Redmine_Versao = "" ;
         this.AV15Redmine_CampoSistema = "" ;
         this.AV16Redmine_CampoServico = "" ;
         initialize();
         executePrivate();
         aP0_Contratante_Codigo=this.AV11Contratante_Codigo;
         aP1_Redmine_User=this.AV14Redmine_User;
         aP2_Redmine_Secure=this.AV13Redmine_Secure;
         aP3_Redmine_Host=this.AV10Redmine_Host;
         aP4_Redmine_Url=this.AV9Redmine_Url;
         aP5_Redmine_Key=this.AV8Redmine_Key;
         aP6_Redmine_Versao=this.AV12Redmine_Versao;
         aP7_Redmine_CampoSistema=this.AV15Redmine_CampoSistema;
         aP8_Redmine_CampoServico=this.AV16Redmine_CampoServico;
         return AV16Redmine_CampoServico ;
      }

      public void executeSubmit( ref int aP0_Contratante_Codigo ,
                                 out String aP1_Redmine_User ,
                                 out short aP2_Redmine_Secure ,
                                 out String aP3_Redmine_Host ,
                                 out String aP4_Redmine_Url ,
                                 out String aP5_Redmine_Key ,
                                 out String aP6_Redmine_Versao ,
                                 out String aP7_Redmine_CampoSistema ,
                                 out String aP8_Redmine_CampoServico )
      {
         prc_getredmine objprc_getredmine;
         objprc_getredmine = new prc_getredmine();
         objprc_getredmine.AV11Contratante_Codigo = aP0_Contratante_Codigo;
         objprc_getredmine.AV14Redmine_User = "" ;
         objprc_getredmine.AV13Redmine_Secure = 0 ;
         objprc_getredmine.AV10Redmine_Host = "" ;
         objprc_getredmine.AV9Redmine_Url = "" ;
         objprc_getredmine.AV8Redmine_Key = "" ;
         objprc_getredmine.AV12Redmine_Versao = "" ;
         objprc_getredmine.AV15Redmine_CampoSistema = "" ;
         objprc_getredmine.AV16Redmine_CampoServico = "" ;
         objprc_getredmine.context.SetSubmitInitialConfig(context);
         objprc_getredmine.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_getredmine);
         aP0_Contratante_Codigo=this.AV11Contratante_Codigo;
         aP1_Redmine_User=this.AV14Redmine_User;
         aP2_Redmine_Secure=this.AV13Redmine_Secure;
         aP3_Redmine_Host=this.AV10Redmine_Host;
         aP4_Redmine_Url=this.AV9Redmine_Url;
         aP5_Redmine_Key=this.AV8Redmine_Key;
         aP6_Redmine_Versao=this.AV12Redmine_Versao;
         aP7_Redmine_CampoSistema=this.AV15Redmine_CampoSistema;
         aP8_Redmine_CampoServico=this.AV16Redmine_CampoServico;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_getredmine)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P009F2 */
         pr_default.execute(0, new Object[] {AV11Contratante_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A1384Redmine_ContratanteCod = P009F2_A1384Redmine_ContratanteCod[0];
            A1905Redmine_User = P009F2_A1905Redmine_User[0];
            n1905Redmine_User = P009F2_n1905Redmine_User[0];
            A1904Redmine_Secure = P009F2_A1904Redmine_Secure[0];
            A1381Redmine_Host = P009F2_A1381Redmine_Host[0];
            A1382Redmine_Url = P009F2_A1382Redmine_Url[0];
            A1383Redmine_Key = P009F2_A1383Redmine_Key[0];
            A1391Redmine_Versao = P009F2_A1391Redmine_Versao[0];
            n1391Redmine_Versao = P009F2_n1391Redmine_Versao[0];
            A1906Redmine_CampoSistema = P009F2_A1906Redmine_CampoSistema[0];
            n1906Redmine_CampoSistema = P009F2_n1906Redmine_CampoSistema[0];
            A1907Redmine_CampoServico = P009F2_A1907Redmine_CampoServico[0];
            n1907Redmine_CampoServico = P009F2_n1907Redmine_CampoServico[0];
            A1380Redmine_Codigo = P009F2_A1380Redmine_Codigo[0];
            OV13Redmine_Secure = AV13Redmine_Secure;
            AV14Redmine_User = A1905Redmine_User;
            AV13Redmine_Secure = A1904Redmine_Secure;
            AV10Redmine_Host = A1381Redmine_Host;
            AV9Redmine_Url = A1382Redmine_Url;
            AV8Redmine_Key = A1383Redmine_Key;
            AV12Redmine_Versao = A1391Redmine_Versao;
            AV15Redmine_CampoSistema = A1906Redmine_CampoSistema;
            AV16Redmine_CampoServico = A1907Redmine_CampoServico;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            pr_default.readNext(0);
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P009F2_A1384Redmine_ContratanteCod = new int[1] ;
         P009F2_A1905Redmine_User = new String[] {""} ;
         P009F2_n1905Redmine_User = new bool[] {false} ;
         P009F2_A1904Redmine_Secure = new short[1] ;
         P009F2_A1381Redmine_Host = new String[] {""} ;
         P009F2_A1382Redmine_Url = new String[] {""} ;
         P009F2_A1383Redmine_Key = new String[] {""} ;
         P009F2_A1391Redmine_Versao = new String[] {""} ;
         P009F2_n1391Redmine_Versao = new bool[] {false} ;
         P009F2_A1906Redmine_CampoSistema = new String[] {""} ;
         P009F2_n1906Redmine_CampoSistema = new bool[] {false} ;
         P009F2_A1907Redmine_CampoServico = new String[] {""} ;
         P009F2_n1907Redmine_CampoServico = new bool[] {false} ;
         P009F2_A1380Redmine_Codigo = new int[1] ;
         A1905Redmine_User = "";
         A1381Redmine_Host = "";
         A1382Redmine_Url = "";
         A1383Redmine_Key = "";
         A1391Redmine_Versao = "";
         A1906Redmine_CampoSistema = "";
         A1907Redmine_CampoServico = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_getredmine__default(),
            new Object[][] {
                new Object[] {
               P009F2_A1384Redmine_ContratanteCod, P009F2_A1905Redmine_User, P009F2_n1905Redmine_User, P009F2_A1904Redmine_Secure, P009F2_A1381Redmine_Host, P009F2_A1382Redmine_Url, P009F2_A1383Redmine_Key, P009F2_A1391Redmine_Versao, P009F2_n1391Redmine_Versao, P009F2_A1906Redmine_CampoSistema,
               P009F2_n1906Redmine_CampoSistema, P009F2_A1907Redmine_CampoServico, P009F2_n1907Redmine_CampoServico, P009F2_A1380Redmine_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short A1904Redmine_Secure ;
      private short OV13Redmine_Secure ;
      private short AV13Redmine_Secure ;
      private int AV11Contratante_Codigo ;
      private int A1384Redmine_ContratanteCod ;
      private int A1380Redmine_Codigo ;
      private String AV16Redmine_CampoServico ;
      private String scmdbuf ;
      private String A1905Redmine_User ;
      private String A1381Redmine_Host ;
      private String A1382Redmine_Url ;
      private String A1383Redmine_Key ;
      private String A1391Redmine_Versao ;
      private String A1906Redmine_CampoSistema ;
      private String A1907Redmine_CampoServico ;
      private String AV14Redmine_User ;
      private String AV10Redmine_Host ;
      private String AV9Redmine_Url ;
      private String AV8Redmine_Key ;
      private String AV12Redmine_Versao ;
      private String AV15Redmine_CampoSistema ;
      private bool n1905Redmine_User ;
      private bool n1391Redmine_Versao ;
      private bool n1906Redmine_CampoSistema ;
      private bool n1907Redmine_CampoServico ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_Contratante_Codigo ;
      private IDataStoreProvider pr_default ;
      private int[] P009F2_A1384Redmine_ContratanteCod ;
      private String[] P009F2_A1905Redmine_User ;
      private bool[] P009F2_n1905Redmine_User ;
      private short[] P009F2_A1904Redmine_Secure ;
      private String[] P009F2_A1381Redmine_Host ;
      private String[] P009F2_A1382Redmine_Url ;
      private String[] P009F2_A1383Redmine_Key ;
      private String[] P009F2_A1391Redmine_Versao ;
      private bool[] P009F2_n1391Redmine_Versao ;
      private String[] P009F2_A1906Redmine_CampoSistema ;
      private bool[] P009F2_n1906Redmine_CampoSistema ;
      private String[] P009F2_A1907Redmine_CampoServico ;
      private bool[] P009F2_n1907Redmine_CampoServico ;
      private int[] P009F2_A1380Redmine_Codigo ;
      private String aP1_Redmine_User ;
      private short aP2_Redmine_Secure ;
      private String aP3_Redmine_Host ;
      private String aP4_Redmine_Url ;
      private String aP5_Redmine_Key ;
      private String aP6_Redmine_Versao ;
      private String aP7_Redmine_CampoSistema ;
      private String aP8_Redmine_CampoServico ;
   }

   public class prc_getredmine__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP009F2 ;
          prmP009F2 = new Object[] {
          new Object[] {"@AV11Contratante_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P009F2", "SELECT TOP 1 [Redmine_ContratanteCod], [Redmine_User], [Redmine_Secure], [Redmine_Host], [Redmine_Url], [Redmine_Key], [Redmine_Versao], [Redmine_CampoSistema], [Redmine_CampoServico], [Redmine_Codigo] FROM [Redmine] WITH (NOLOCK) WHERE [Redmine_ContratanteCod] = @AV11Contratante_Codigo ORDER BY [Redmine_ContratanteCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP009F2,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((short[]) buf[3])[0] = rslt.getShort(3) ;
                ((String[]) buf[4])[0] = rslt.getString(4, 100) ;
                ((String[]) buf[5])[0] = rslt.getString(5, 100) ;
                ((String[]) buf[6])[0] = rslt.getString(6, 50) ;
                ((String[]) buf[7])[0] = rslt.getString(7, 20) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(7);
                ((String[]) buf[9])[0] = rslt.getString(8, 50) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(8);
                ((String[]) buf[11])[0] = rslt.getString(9, 50) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(9);
                ((int[]) buf[13])[0] = rslt.getInt(10) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
