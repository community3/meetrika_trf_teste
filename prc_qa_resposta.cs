/*
               File: PRC_QA_Resposta
        Description: QA Com Resposta
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:55:50.17
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_qa_resposta : GXProcedure
   {
      public prc_qa_resposta( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_qa_resposta( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_ContagemResultadoQA_RespostaDe ,
                           out int aP1_Resposta )
      {
         this.A1996ContagemResultadoQA_RespostaDe = aP0_ContagemResultadoQA_RespostaDe;
         this.AV8Resposta = 0 ;
         initialize();
         executePrivate();
         aP0_ContagemResultadoQA_RespostaDe=this.A1996ContagemResultadoQA_RespostaDe;
         aP1_Resposta=this.AV8Resposta;
      }

      public int executeUdp( ref int aP0_ContagemResultadoQA_RespostaDe )
      {
         this.A1996ContagemResultadoQA_RespostaDe = aP0_ContagemResultadoQA_RespostaDe;
         this.AV8Resposta = 0 ;
         initialize();
         executePrivate();
         aP0_ContagemResultadoQA_RespostaDe=this.A1996ContagemResultadoQA_RespostaDe;
         aP1_Resposta=this.AV8Resposta;
         return AV8Resposta ;
      }

      public void executeSubmit( ref int aP0_ContagemResultadoQA_RespostaDe ,
                                 out int aP1_Resposta )
      {
         prc_qa_resposta objprc_qa_resposta;
         objprc_qa_resposta = new prc_qa_resposta();
         objprc_qa_resposta.A1996ContagemResultadoQA_RespostaDe = aP0_ContagemResultadoQA_RespostaDe;
         objprc_qa_resposta.AV8Resposta = 0 ;
         objprc_qa_resposta.context.SetSubmitInitialConfig(context);
         objprc_qa_resposta.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_qa_resposta);
         aP0_ContagemResultadoQA_RespostaDe=this.A1996ContagemResultadoQA_RespostaDe;
         aP1_Resposta=this.AV8Resposta;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_qa_resposta)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P00VJ2 */
         pr_default.execute(0, new Object[] {n1996ContagemResultadoQA_RespostaDe, A1996ContagemResultadoQA_RespostaDe});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A1984ContagemResultadoQA_Codigo = P00VJ2_A1984ContagemResultadoQA_Codigo[0];
            AV8Resposta = A1984ContagemResultadoQA_Codigo;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            pr_default.readNext(0);
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00VJ2_A1996ContagemResultadoQA_RespostaDe = new int[1] ;
         P00VJ2_n1996ContagemResultadoQA_RespostaDe = new bool[] {false} ;
         P00VJ2_A1984ContagemResultadoQA_Codigo = new int[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_qa_resposta__default(),
            new Object[][] {
                new Object[] {
               P00VJ2_A1996ContagemResultadoQA_RespostaDe, P00VJ2_n1996ContagemResultadoQA_RespostaDe, P00VJ2_A1984ContagemResultadoQA_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int A1996ContagemResultadoQA_RespostaDe ;
      private int AV8Resposta ;
      private int A1984ContagemResultadoQA_Codigo ;
      private String scmdbuf ;
      private bool n1996ContagemResultadoQA_RespostaDe ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_ContagemResultadoQA_RespostaDe ;
      private IDataStoreProvider pr_default ;
      private int[] P00VJ2_A1996ContagemResultadoQA_RespostaDe ;
      private bool[] P00VJ2_n1996ContagemResultadoQA_RespostaDe ;
      private int[] P00VJ2_A1984ContagemResultadoQA_Codigo ;
      private int aP1_Resposta ;
   }

   public class prc_qa_resposta__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00VJ2 ;
          prmP00VJ2 = new Object[] {
          new Object[] {"@ContagemResultadoQA_RespostaDe",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00VJ2", "SELECT TOP 1 [ContagemResultadoQA_RespostaDe], [ContagemResultadoQA_Codigo] FROM [ContagemResultadoQA] WITH (NOLOCK) WHERE [ContagemResultadoQA_RespostaDe] = @ContagemResultadoQA_RespostaDe ORDER BY [ContagemResultadoQA_RespostaDe] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00VJ2,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
       }
    }

 }

}
