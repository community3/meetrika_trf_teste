/*
               File: PRC_INSPrioridadesPadrao
        Description: Inserir Prioridades Padrao
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 23:7:13.94
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_insprioridadespadrao : GXProcedure
   {
      public prc_insprioridadespadrao( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_insprioridadespadrao( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_ContratoServico_Codigo ,
                           int aP1_Servico_Codigo )
      {
         this.AV8ContratoServico_Codigo = aP0_ContratoServico_Codigo;
         this.AV9Servico_Codigo = aP1_Servico_Codigo;
         initialize();
         executePrivate();
      }

      public void executeSubmit( int aP0_ContratoServico_Codigo ,
                                 int aP1_Servico_Codigo )
      {
         prc_insprioridadespadrao objprc_insprioridadespadrao;
         objprc_insprioridadespadrao = new prc_insprioridadespadrao();
         objprc_insprioridadespadrao.AV8ContratoServico_Codigo = aP0_ContratoServico_Codigo;
         objprc_insprioridadespadrao.AV9Servico_Codigo = aP1_Servico_Codigo;
         objprc_insprioridadespadrao.context.SetSubmitInitialConfig(context);
         objprc_insprioridadespadrao.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_insprioridadespadrao);
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_insprioridadespadrao)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P00AD2 */
         pr_default.execute(0, new Object[] {AV9Servico_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A1439ServicoPrioridade_SrvCod = P00AD2_A1439ServicoPrioridade_SrvCod[0];
            A1441ServicoPrioridade_Nome = P00AD2_A1441ServicoPrioridade_Nome[0];
            A1442ServicoPrioridade_Finalidade = P00AD2_A1442ServicoPrioridade_Finalidade[0];
            n1442ServicoPrioridade_Finalidade = P00AD2_n1442ServicoPrioridade_Finalidade[0];
            A1440ServicoPrioridade_Codigo = P00AD2_A1440ServicoPrioridade_Codigo[0];
            AV12ContratoServicosPrioridade = new SdtContratoServicosPrioridade(context);
            AV12ContratoServicosPrioridade.gxTpr_Contratoservicosprioridade_cntsrvcod = AV8ContratoServico_Codigo;
            AV12ContratoServicosPrioridade.gxTpr_Contratoservicosprioridade_nome = A1441ServicoPrioridade_Nome;
            AV12ContratoServicosPrioridade.gxTpr_Contratoservicosprioridade_finalidade = A1442ServicoPrioridade_Finalidade;
            AV12ContratoServicosPrioridade.Save();
            pr_default.readNext(0);
         }
         pr_default.close(0);
         context.CommitDataStores( "PRC_INSPrioridadesPadrao");
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00AD2_A1439ServicoPrioridade_SrvCod = new int[1] ;
         P00AD2_A1441ServicoPrioridade_Nome = new String[] {""} ;
         P00AD2_A1442ServicoPrioridade_Finalidade = new String[] {""} ;
         P00AD2_n1442ServicoPrioridade_Finalidade = new bool[] {false} ;
         P00AD2_A1440ServicoPrioridade_Codigo = new int[1] ;
         A1441ServicoPrioridade_Nome = "";
         A1442ServicoPrioridade_Finalidade = "";
         AV12ContratoServicosPrioridade = new SdtContratoServicosPrioridade(context);
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_insprioridadespadrao__default(),
            new Object[][] {
                new Object[] {
               P00AD2_A1439ServicoPrioridade_SrvCod, P00AD2_A1441ServicoPrioridade_Nome, P00AD2_A1442ServicoPrioridade_Finalidade, P00AD2_n1442ServicoPrioridade_Finalidade, P00AD2_A1440ServicoPrioridade_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV8ContratoServico_Codigo ;
      private int AV9Servico_Codigo ;
      private int A1439ServicoPrioridade_SrvCod ;
      private int A1440ServicoPrioridade_Codigo ;
      private String scmdbuf ;
      private String A1441ServicoPrioridade_Nome ;
      private bool n1442ServicoPrioridade_Finalidade ;
      private String A1442ServicoPrioridade_Finalidade ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00AD2_A1439ServicoPrioridade_SrvCod ;
      private String[] P00AD2_A1441ServicoPrioridade_Nome ;
      private String[] P00AD2_A1442ServicoPrioridade_Finalidade ;
      private bool[] P00AD2_n1442ServicoPrioridade_Finalidade ;
      private int[] P00AD2_A1440ServicoPrioridade_Codigo ;
      private SdtContratoServicosPrioridade AV12ContratoServicosPrioridade ;
   }

   public class prc_insprioridadespadrao__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00AD2 ;
          prmP00AD2 = new Object[] {
          new Object[] {"@AV9Servico_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00AD2", "SELECT [ServicoPrioridade_SrvCod], [ServicoPrioridade_Nome], [ServicoPrioridade_Finalidade], [ServicoPrioridade_Codigo] FROM [ServicoPrioridade] WITH (NOLOCK) WHERE [ServicoPrioridade_SrvCod] = @AV9Servico_Codigo ORDER BY [ServicoPrioridade_SrvCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00AD2,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((String[]) buf[2])[0] = rslt.getLongVarchar(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
