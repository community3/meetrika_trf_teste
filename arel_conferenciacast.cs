/*
               File: REL_ConferenciaCAST
        Description: Conferencia CAST
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/2/2020 16:12:10.96
       Program type: Main program
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.Printer;
using GeneXus.XML;
using GeneXus.Office;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class arel_conferenciacast : GXWebProcedure, System.Web.SessionState.IRequiresSessionState
   {
      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize();
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            if ( ! entryPointCalled )
            {
               AV10Arquivo = gxfirstwebparm;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
               {
                  AV90Aba = GetNextPar( );
                  AV39Contratada_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  AV89PraLinCAST = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  AV83ColDmnCASTn = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  AV84ColDataCASTn = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  AV85ColPFBFSCASTn = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  AV86ColPFLFSCASTn = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  AV87ColPFBFMCASTn = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  AV88ColPFLFMCASTn = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  AV92FileName = GetNextPar( );
               }
            }
         }
         if ( GxWebError == 0 )
         {
            executePrivate();
         }
         cleanup();
      }

      public arel_conferenciacast( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public arel_conferenciacast( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_Arquivo ,
                           String aP1_Aba ,
                           int aP2_Contratada_Codigo ,
                           short aP3_PraLinCAST ,
                           short aP4_ColDmnCASTn ,
                           short aP5_ColDataCASTn ,
                           short aP6_ColPFBFSCASTn ,
                           short aP7_ColPFLFSCASTn ,
                           short aP8_ColPFBFMCASTn ,
                           short aP9_ColPFLFMCASTn ,
                           ref String aP10_FileName )
      {
         this.AV10Arquivo = aP0_Arquivo;
         this.AV90Aba = aP1_Aba;
         this.AV39Contratada_Codigo = aP2_Contratada_Codigo;
         this.AV89PraLinCAST = aP3_PraLinCAST;
         this.AV83ColDmnCASTn = aP4_ColDmnCASTn;
         this.AV84ColDataCASTn = aP5_ColDataCASTn;
         this.AV85ColPFBFSCASTn = aP6_ColPFBFSCASTn;
         this.AV86ColPFLFSCASTn = aP7_ColPFLFSCASTn;
         this.AV87ColPFBFMCASTn = aP8_ColPFBFMCASTn;
         this.AV88ColPFLFMCASTn = aP9_ColPFLFMCASTn;
         this.AV92FileName = aP10_FileName;
         initialize();
         executePrivate();
         aP10_FileName=this.AV92FileName;
      }

      public String executeUdp( String aP0_Arquivo ,
                                String aP1_Aba ,
                                int aP2_Contratada_Codigo ,
                                short aP3_PraLinCAST ,
                                short aP4_ColDmnCASTn ,
                                short aP5_ColDataCASTn ,
                                short aP6_ColPFBFSCASTn ,
                                short aP7_ColPFLFSCASTn ,
                                short aP8_ColPFBFMCASTn ,
                                short aP9_ColPFLFMCASTn )
      {
         this.AV10Arquivo = aP0_Arquivo;
         this.AV90Aba = aP1_Aba;
         this.AV39Contratada_Codigo = aP2_Contratada_Codigo;
         this.AV89PraLinCAST = aP3_PraLinCAST;
         this.AV83ColDmnCASTn = aP4_ColDmnCASTn;
         this.AV84ColDataCASTn = aP5_ColDataCASTn;
         this.AV85ColPFBFSCASTn = aP6_ColPFBFSCASTn;
         this.AV86ColPFLFSCASTn = aP7_ColPFLFSCASTn;
         this.AV87ColPFBFMCASTn = aP8_ColPFBFMCASTn;
         this.AV88ColPFLFMCASTn = aP9_ColPFLFMCASTn;
         this.AV92FileName = aP10_FileName;
         initialize();
         executePrivate();
         aP10_FileName=this.AV92FileName;
         return AV92FileName ;
      }

      public void executeSubmit( String aP0_Arquivo ,
                                 String aP1_Aba ,
                                 int aP2_Contratada_Codigo ,
                                 short aP3_PraLinCAST ,
                                 short aP4_ColDmnCASTn ,
                                 short aP5_ColDataCASTn ,
                                 short aP6_ColPFBFSCASTn ,
                                 short aP7_ColPFLFSCASTn ,
                                 short aP8_ColPFBFMCASTn ,
                                 short aP9_ColPFLFMCASTn ,
                                 ref String aP10_FileName )
      {
         arel_conferenciacast objarel_conferenciacast;
         objarel_conferenciacast = new arel_conferenciacast();
         objarel_conferenciacast.AV10Arquivo = aP0_Arquivo;
         objarel_conferenciacast.AV90Aba = aP1_Aba;
         objarel_conferenciacast.AV39Contratada_Codigo = aP2_Contratada_Codigo;
         objarel_conferenciacast.AV89PraLinCAST = aP3_PraLinCAST;
         objarel_conferenciacast.AV83ColDmnCASTn = aP4_ColDmnCASTn;
         objarel_conferenciacast.AV84ColDataCASTn = aP5_ColDataCASTn;
         objarel_conferenciacast.AV85ColPFBFSCASTn = aP6_ColPFBFSCASTn;
         objarel_conferenciacast.AV86ColPFLFSCASTn = aP7_ColPFLFSCASTn;
         objarel_conferenciacast.AV87ColPFBFMCASTn = aP8_ColPFBFMCASTn;
         objarel_conferenciacast.AV88ColPFLFMCASTn = aP9_ColPFLFMCASTn;
         objarel_conferenciacast.AV92FileName = aP10_FileName;
         objarel_conferenciacast.context.SetSubmitInitialConfig(context);
         objarel_conferenciacast.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objarel_conferenciacast);
         aP10_FileName=this.AV92FileName;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((arel_conferenciacast)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         M_top = 0;
         M_bot = 6;
         P_lines = (int)(66-M_bot);
         getPrinter().GxClearAttris() ;
         add_metrics( ) ;
         lineHeight = 15;
         PrtOffset = 0;
         gxXPage = 100;
         gxYPage = 100;
         getPrinter().GxSetDocName("") ;
         try
         {
            Gx_out = "FIL" ;
            if (!initPrinter (Gx_out, gxXPage, gxYPage, "GXPRN.INI", "", "", 2, 1, 256, 16834, 11909, 0, 1, 1, 0, 1, 1) )
            {
               cleanup();
               return;
            }
            getPrinter().setModal(false) ;
            P_lines = (int)(gxYPage-(lineHeight*6));
            Gx_line = (int)(P_lines+1);
            getPrinter().setPageLines(P_lines);
            getPrinter().setLineHeight(lineHeight);
            getPrinter().setM_top(M_top);
            getPrinter().setM_bot(M_bot);
            new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV79WWPContext) ;
            /* Execute user subroutine: 'OPENFILE' */
            S111 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
            AV57NomeArq = "Arquivo: " + AV92FileName;
            /* Using cursor P004I2 */
            pr_default.execute(0, new Object[] {AV39Contratada_Codigo});
            while ( (pr_default.getStatus(0) != 101) )
            {
               A40Contratada_PessoaCod = P004I2_A40Contratada_PessoaCod[0];
               A39Contratada_Codigo = P004I2_A39Contratada_Codigo[0];
               A41Contratada_PessoaNom = P004I2_A41Contratada_PessoaNom[0];
               n41Contratada_PessoaNom = P004I2_n41Contratada_PessoaNom[0];
               A41Contratada_PessoaNom = P004I2_A41Contratada_PessoaNom[0];
               n41Contratada_PessoaNom = P004I2_n41Contratada_PessoaNom[0];
               AV81SubTitulo = A41Contratada_PessoaNom;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(0);
            AV55Ln = AV89PraLinCAST;
            while ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV49ExcelDocument.get_Cells(AV55Ln, AV83ColDmnCASTn, 1, 1).Text)) )
            {
               AV43Date = DateTimeUtil.ResetTime(AV49ExcelDocument.get_Cells(AV55Ln, AV84ColDataCASTn, 1, 1).Date);
               if ( (DateTime.MinValue==AV42DataIni) && (DateTime.MinValue==AV41DataFim) )
               {
                  AV42DataIni = AV43Date;
                  AV41DataFim = AV43Date;
               }
               else if ( AV43Date < AV42DataIni )
               {
                  AV42DataIni = AV43Date;
               }
               else if ( AV43Date > AV41DataFim )
               {
                  AV41DataFim = AV43Date;
               }
               AV55Ln = (short)(AV55Ln+1);
            }
            AV55Ln = 2;
            AV78Titulo = "Resultado da Confer�ncia com planilha";
            AV63Periodo = "Per�odo: " + context.localUtil.DToC( AV42DataIni, 2, "/") + " - " + context.localUtil.DToC( AV41DataFim, 2, "/");
            while ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV49ExcelDocument.get_Cells(AV55Ln, AV83ColDmnCASTn, 1, 1).Text)) )
            {
               AV21ContagemResultado_Demanda = AV49ExcelDocument.get_Cells(AV55Ln, AV83ColDmnCASTn, 1, 1).Text;
               AV43Date = DateTimeUtil.ResetTime(AV49ExcelDocument.get_Cells(AV55Ln, AV84ColDataCASTn, 1, 1).Date);
               AV64PFBFM = (decimal)(AV49ExcelDocument.get_Cells(AV55Ln, AV87ColPFBFMCASTn, 1, 1).Number);
               AV68PFLFM = (decimal)(AV49ExcelDocument.get_Cells(AV55Ln, AV88ColPFLFMCASTn, 1, 1).Number);
               AV65PFBFS = (decimal)(AV49ExcelDocument.get_Cells(AV55Ln, AV85ColPFBFSCASTn, 1, 1).Number);
               AV69PFLFS = (decimal)(AV49ExcelDocument.get_Cells(AV55Ln, AV86ColPFLFSCASTn, 1, 1).Number);
               AV51Flag = false;
               AV99GXLvl50 = 0;
               /* Using cursor P004I3 */
               pr_default.execute(1, new Object[] {AV39Contratada_Codigo, AV21ContagemResultado_Demanda});
               while ( (pr_default.getStatus(1) != 101) )
               {
                  A489ContagemResultado_SistemaCod = P004I3_A489ContagemResultado_SistemaCod[0];
                  n489ContagemResultado_SistemaCod = P004I3_n489ContagemResultado_SistemaCod[0];
                  A805ContagemResultado_ContratadaOrigemCod = P004I3_A805ContagemResultado_ContratadaOrigemCod[0];
                  n805ContagemResultado_ContratadaOrigemCod = P004I3_n805ContagemResultado_ContratadaOrigemCod[0];
                  A457ContagemResultado_Demanda = P004I3_A457ContagemResultado_Demanda[0];
                  n457ContagemResultado_Demanda = P004I3_n457ContagemResultado_Demanda[0];
                  A456ContagemResultado_Codigo = P004I3_A456ContagemResultado_Codigo[0];
                  A484ContagemResultado_StatusDmn = P004I3_A484ContagemResultado_StatusDmn[0];
                  n484ContagemResultado_StatusDmn = P004I3_n484ContagemResultado_StatusDmn[0];
                  A602ContagemResultado_OSVinculada = P004I3_A602ContagemResultado_OSVinculada[0];
                  n602ContagemResultado_OSVinculada = P004I3_n602ContagemResultado_OSVinculada[0];
                  A494ContagemResultado_Descricao = P004I3_A494ContagemResultado_Descricao[0];
                  n494ContagemResultado_Descricao = P004I3_n494ContagemResultado_Descricao[0];
                  A515ContagemResultado_SistemaCoord = P004I3_A515ContagemResultado_SistemaCoord[0];
                  n515ContagemResultado_SistemaCoord = P004I3_n515ContagemResultado_SistemaCoord[0];
                  A515ContagemResultado_SistemaCoord = P004I3_A515ContagemResultado_SistemaCoord[0];
                  n515ContagemResultado_SistemaCoord = P004I3_n515ContagemResultado_SistemaCoord[0];
                  OV24ContagemResultado_HoraCnt = AV24ContagemResultado_HoraCnt;
                  OV34ContagemResultado_Ultima = AV34ContagemResultado_Ultima;
                  OV35ContagemResultadoContagens_Esforco = AV35ContagemResultadoContagens_Esforco;
                  AV99GXLvl50 = 1;
                  AV18Contagemresultado_Codigo = A456ContagemResultado_Codigo;
                  AV21ContagemResultado_Demanda = A457ContagemResultado_Demanda;
                  if ( ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "A") == 0 ) || ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "R") == 0 ) || ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "D") == 0 ) || ( StringUtil.StrCmp(A484ContagemResultado_StatusDmn, "C") == 0 ) )
                  {
                     AV59Ok = true;
                     if ( ! P004I3_n602ContagemResultado_OSVinculada[0] )
                     {
                        AV91ContagemResultado_OSVinculada = A602ContagemResultado_OSVinculada;
                        /* Execute user subroutine: 'STATUSOSVNC' */
                        S141 ();
                        if ( returnInSub )
                        {
                           pr_default.close(1);
                           this.cleanup();
                           if (true) return;
                        }
                     }
                     AV38ContagemResultadoErro_Tipo = "DE";
                     if ( String.IsNullOrEmpty(StringUtil.RTrim( A494ContagemResultado_Descricao)) )
                     {
                        AV54linha = "Demanda: " + AV21ContagemResultado_Demanda + "  -  Sem Descri��o";
                        /* Execute user subroutine: 'REGISTRAERRO' */
                        S121 ();
                        if ( returnInSub )
                        {
                           pr_default.close(1);
                           this.cleanup();
                           if (true) return;
                        }
                        if ( String.IsNullOrEmpty(StringUtil.RTrim( A515ContagemResultado_SistemaCoord)) )
                        {
                           AV54linha = AV54linha + " e sem Coordena��o";
                           AV38ContagemResultadoErro_Tipo = "SC";
                           /* Execute user subroutine: 'REGISTRAERRO' */
                           S121 ();
                           if ( returnInSub )
                           {
                              pr_default.close(1);
                              this.cleanup();
                              if (true) return;
                           }
                        }
                        H4I0( false, 21) ;
                        getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                        getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV54linha, "")), 25, Gx_line+0, 807, Gx_line+15, 0+256, 0, 0, 0) ;
                        Gx_OldLine = Gx_line;
                        Gx_line = (int)(Gx_line+21);
                     }
                     else
                     {
                        /* Execute user subroutine: 'ATUALIZAERRO' */
                        S131 ();
                        if ( returnInSub )
                        {
                           pr_default.close(1);
                           this.cleanup();
                           if (true) return;
                        }
                        AV38ContagemResultadoErro_Tipo = "SC";
                        if ( String.IsNullOrEmpty(StringUtil.RTrim( A515ContagemResultado_SistemaCoord)) )
                        {
                           AV54linha = "Demanda: " + AV21ContagemResultado_Demanda + "  -  Sem Coordena��o";
                           H4I0( false, 21) ;
                           getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                           getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV54linha, "")), 25, Gx_line+0, 807, Gx_line+15, 0+256, 0, 0, 0) ;
                           Gx_OldLine = Gx_line;
                           Gx_line = (int)(Gx_line+21);
                           /* Execute user subroutine: 'REGISTRAERRO' */
                           S121 ();
                           if ( returnInSub )
                           {
                              pr_default.close(1);
                              this.cleanup();
                              if (true) return;
                           }
                        }
                        else
                        {
                           /* Execute user subroutine: 'ATUALIZAERRO' */
                           S131 ();
                           if ( returnInSub )
                           {
                              pr_default.close(1);
                              this.cleanup();
                              if (true) return;
                           }
                        }
                     }
                     /* Using cursor P004I4 */
                     pr_default.execute(2, new Object[] {A456ContagemResultado_Codigo});
                     while ( (pr_default.getStatus(2) != 101) )
                     {
                        A517ContagemResultado_Ultima = P004I4_A517ContagemResultado_Ultima[0];
                        A473ContagemResultado_DataCnt = P004I4_A473ContagemResultado_DataCnt[0];
                        A511ContagemResultado_HoraCnt = P004I4_A511ContagemResultado_HoraCnt[0];
                        A463ContagemResultado_ParecerTcn = P004I4_A463ContagemResultado_ParecerTcn[0];
                        n463ContagemResultado_ParecerTcn = P004I4_n463ContagemResultado_ParecerTcn[0];
                        A460ContagemResultado_PFBFM = P004I4_A460ContagemResultado_PFBFM[0];
                        n460ContagemResultado_PFBFM = P004I4_n460ContagemResultado_PFBFM[0];
                        A458ContagemResultado_PFBFS = P004I4_A458ContagemResultado_PFBFS[0];
                        n458ContagemResultado_PFBFS = P004I4_n458ContagemResultado_PFBFS[0];
                        A461ContagemResultado_PFLFM = P004I4_A461ContagemResultado_PFLFM[0];
                        n461ContagemResultado_PFLFM = P004I4_n461ContagemResultado_PFLFM[0];
                        A459ContagemResultado_PFLFS = P004I4_A459ContagemResultado_PFLFS[0];
                        n459ContagemResultado_PFLFS = P004I4_n459ContagemResultado_PFLFS[0];
                        A470ContagemResultado_ContadorFMCod = P004I4_A470ContagemResultado_ContadorFMCod[0];
                        A462ContagemResultado_Divergencia = P004I4_A462ContagemResultado_Divergencia[0];
                        A469ContagemResultado_NaoCnfCntCod = P004I4_A469ContagemResultado_NaoCnfCntCod[0];
                        n469ContagemResultado_NaoCnfCntCod = P004I4_n469ContagemResultado_NaoCnfCntCod[0];
                        A483ContagemResultado_StatusCnt = P004I4_A483ContagemResultado_StatusCnt[0];
                        A482ContagemResultadoContagens_Esforco = P004I4_A482ContagemResultadoContagens_Esforco[0];
                        AV51Flag = (bool)(((A473ContagemResultado_DataCnt!=AV43Date)));
                        AV38ContagemResultadoErro_Tipo = "DA";
                        if ( AV51Flag )
                        {
                           AV54linha = "Demanda: " + AV21ContagemResultado_Demanda + "  -  Sistema: " + context.localUtil.DToC( A473ContagemResultado_DataCnt, 2, "/") + ", Planilha: " + context.localUtil.DToC( AV43Date, 2, "/");
                           H4I0( false, 21) ;
                           getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                           getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV54linha, "")), 25, Gx_line+0, 807, Gx_line+15, 0+256, 0, 0, 0) ;
                           Gx_OldLine = Gx_line;
                           Gx_line = (int)(Gx_line+21);
                           /* Execute user subroutine: 'REGISTRAERRO' */
                           S121 ();
                           if ( returnInSub )
                           {
                              pr_default.close(2);
                              this.cleanup();
                              if (true) return;
                           }
                           AV18Contagemresultado_Codigo = A456ContagemResultado_Codigo;
                           AV20ContagemResultado_DataCnt = A473ContagemResultado_DataCnt;
                           AV24ContagemResultado_HoraCnt = A511ContagemResultado_HoraCnt;
                           AV26ContagemResultado_ParecerTcn = A463ContagemResultado_ParecerTcn;
                           AV27ContagemResultado_PFBFM = A460ContagemResultado_PFBFM;
                           AV28ContagemResultado_PFBFS = A458ContagemResultado_PFBFS;
                           AV30ContagemResultado_PFLFM = A461ContagemResultado_PFLFM;
                           AV31ContagemResultado_PFLFS = A459ContagemResultado_PFLFS;
                           AV19ContagemResultado_ContadorFMCod = A470ContagemResultado_ContadorFMCod;
                           AV23ContagemResultado_Divergencia = A462ContagemResultado_Divergencia;
                           AV25ContagemResultado_NaoCnfCntCod = A469ContagemResultado_NaoCnfCntCod;
                           AV32ContagemResultado_StatusCnt = A483ContagemResultado_StatusCnt;
                           AV34ContagemResultado_Ultima = A517ContagemResultado_Ultima;
                           AV35ContagemResultadoContagens_Esforco = A482ContagemResultadoContagens_Esforco;
                        }
                        else
                        {
                           /* Execute user subroutine: 'ATUALIZAERRO' */
                           S131 ();
                           if ( returnInSub )
                           {
                              pr_default.close(2);
                              this.cleanup();
                              if (true) return;
                           }
                        }
                        AV38ContagemResultadoErro_Tipo = "PF";
                        if ( ( A458ContagemResultado_PFBFS != AV65PFBFS ) || ( A459ContagemResultado_PFLFS != AV69PFLFS ) || ( A460ContagemResultado_PFBFM != AV64PFBFM ) || ( A461ContagemResultado_PFLFM != AV68PFLFM ) )
                        {
                           AV59Ok = false;
                           AV54linha = "Diferen�a em PF Dmn: " + AV21ContagemResultado_Demanda + "  -  Sistema: (" + StringUtil.Trim( StringUtil.Str( A458ContagemResultado_PFBFS, 14, 5)) + "," + StringUtil.Trim( StringUtil.Str( A459ContagemResultado_PFLFS, 14, 5)) + " " + StringUtil.Trim( StringUtil.Str( A460ContagemResultado_PFBFM, 14, 5)) + "," + StringUtil.Trim( StringUtil.Str( A461ContagemResultado_PFLFM, 14, 5)) + "), Planilha: (" + StringUtil.Trim( StringUtil.Str( AV65PFBFS, 14, 5)) + "," + StringUtil.Trim( StringUtil.Str( AV69PFLFS, 14, 5)) + " " + StringUtil.Trim( StringUtil.Str( AV64PFBFM, 14, 5)) + "," + StringUtil.Trim( StringUtil.Str( AV68PFLFM, 14, 5)) + ")";
                           H4I0( false, 21) ;
                           getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                           getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV54linha, "")), 25, Gx_line+0, 807, Gx_line+15, 0+256, 0, 0, 0) ;
                           Gx_OldLine = Gx_line;
                           Gx_line = (int)(Gx_line+21);
                           /* Execute user subroutine: 'REGISTRAERRO' */
                           S121 ();
                           if ( returnInSub )
                           {
                              pr_default.close(2);
                              this.cleanup();
                              if (true) return;
                           }
                           A483ContagemResultado_StatusCnt = 7;
                        }
                        else
                        {
                           /* Execute user subroutine: 'ATUALIZAERRO' */
                           S131 ();
                           if ( returnInSub )
                           {
                              pr_default.close(2);
                              this.cleanup();
                              if (true) return;
                           }
                           AV17Conferida = (short)(AV17Conferida+1);
                           A483ContagemResultado_StatusCnt = 5;
                        }
                        /* Exit For each command. Update data (if necessary), close cursors & exit. */
                        /* Using cursor P004I5 */
                        pr_default.execute(3, new Object[] {A483ContagemResultado_StatusCnt, A456ContagemResultado_Codigo, A473ContagemResultado_DataCnt, A511ContagemResultado_HoraCnt});
                        pr_default.close(3);
                        dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoContagens") ;
                        if (true) break;
                        /* Using cursor P004I6 */
                        pr_default.execute(4, new Object[] {A483ContagemResultado_StatusCnt, A456ContagemResultado_Codigo, A473ContagemResultado_DataCnt, A511ContagemResultado_HoraCnt});
                        pr_default.close(4);
                        dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoContagens") ;
                        pr_default.readNext(2);
                     }
                     pr_default.close(2);
                     if ( AV59Ok )
                     {
                        A484ContagemResultado_StatusDmn = "C";
                        n484ContagemResultado_StatusDmn = false;
                     }
                     else
                     {
                        A484ContagemResultado_StatusDmn = "A";
                        n484ContagemResultado_StatusDmn = false;
                     }
                  }
                  else
                  {
                     AV51Flag = false;
                     AV54linha = "Demanda: " + AV21ContagemResultado_Demanda + "  -  N�o analisada! Status:" + gxdomainstatusdemanda.getDescription(context,A484ContagemResultado_StatusDmn);
                     H4I0( false, 21) ;
                     getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                     getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV54linha, "")), 25, Gx_line+0, 807, Gx_line+15, 0+256, 0, 0, 0) ;
                     Gx_OldLine = Gx_line;
                     Gx_line = (int)(Gx_line+21);
                  }
                  /* Using cursor P004I7 */
                  pr_default.execute(5, new Object[] {n484ContagemResultado_StatusDmn, A484ContagemResultado_StatusDmn, A456ContagemResultado_Codigo});
                  pr_default.close(5);
                  dsDefault.SmartCacheProvider.SetUpdated("ContagemResultado") ;
                  pr_default.readNext(1);
               }
               pr_default.close(1);
               if ( AV99GXLvl50 == 0 )
               {
                  AV54linha = "Falta cadastrar demanda: " + StringUtil.Trim( AV21ContagemResultado_Demanda) + " ou contratada origem incorreta";
                  AV72Semcadastrar = (short)(AV72Semcadastrar+1);
                  H4I0( false, 21) ;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV54linha, "")), 25, Gx_line+0, 807, Gx_line+15, 0+256, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+21);
               }
               AV55Ln = (short)(AV55Ln+1);
            }
            AV49ExcelDocument.Close();
            AV54linha = "";
            H4I0( false, 21) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV54linha, "")), 25, Gx_line+0, 807, Gx_line+15, 0+256, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+21);
            H4I0( false, 21) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV54linha, "")), 25, Gx_line+0, 807, Gx_line+15, 0+256, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+21);
            AV55Ln = (short)(AV55Ln-AV89PraLinCAST);
            AV54linha = StringUtil.Space( 10) + "Linhas processadas: " + StringUtil.Trim( StringUtil.Str( (decimal)(AV55Ln), 4, 0));
            AV54linha = AV54linha + ", atualizadas: " + StringUtil.Trim( StringUtil.Str( (decimal)(AV17Conferida), 4, 0));
            AV54linha = AV54linha + ", sem cadastrar: " + StringUtil.Trim( StringUtil.Str( (decimal)(AV72Semcadastrar), 4, 0));
            AV55Ln = (short)(AV55Ln-AV17Conferida);
            AV54linha = AV54linha + ". A revisar: " + StringUtil.Trim( StringUtil.Str( (decimal)(AV55Ln), 4, 0));
            H4I0( false, 21) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV54linha, "")), 25, Gx_line+0, 807, Gx_line+15, 0+256, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+21);
            /* Print footer for last page */
            ToSkip = (int)(P_lines+1);
            H4I0( true, 0) ;
         }
         catch ( GeneXus.Printer.ProcessInterruptedException e )
         {
         }
         finally
         {
            /* Close printer file */
            try
            {
               getPrinter().GxEndPage() ;
               getPrinter().GxEndDocument() ;
            }
            catch ( GeneXus.Printer.ProcessInterruptedException e )
            {
            }
            endPrinter();
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
         {
            context.Redirect( context.wjLoc );
            context.wjLoc = "";
         }
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'OPENFILE' Routine */
         AV49ExcelDocument.Open(AV10Arquivo);
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV90Aba)) )
         {
            AV49ExcelDocument.SelectSheet(AV90Aba);
         }
      }

      protected void S121( )
      {
         /* 'REGISTRAERRO' Routine */
         AV101GXLvl195 = 0;
         /* Using cursor P004I8 */
         pr_default.execute(6, new Object[] {AV18Contagemresultado_Codigo, AV38ContagemResultadoErro_Tipo});
         while ( (pr_default.getStatus(6) != 101) )
         {
            A579ContagemResultadoErro_Tipo = P004I8_A579ContagemResultadoErro_Tipo[0];
            A456ContagemResultado_Codigo = P004I8_A456ContagemResultado_Codigo[0];
            A580ContagemResultadoErro_Data = P004I8_A580ContagemResultadoErro_Data[0];
            A581ContagemResultadoErro_Status = P004I8_A581ContagemResultadoErro_Status[0];
            AV101GXLvl195 = 1;
            A580ContagemResultadoErro_Data = DateTimeUtil.ServerNow( context, "DEFAULT");
            A581ContagemResultadoErro_Status = "P";
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            /* Using cursor P004I9 */
            pr_default.execute(7, new Object[] {A580ContagemResultadoErro_Data, A581ContagemResultadoErro_Status, A456ContagemResultado_Codigo, A579ContagemResultadoErro_Tipo});
            pr_default.close(7);
            dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoErro") ;
            if (true) break;
            /* Using cursor P004I10 */
            pr_default.execute(8, new Object[] {A580ContagemResultadoErro_Data, A581ContagemResultadoErro_Status, A456ContagemResultado_Codigo, A579ContagemResultadoErro_Tipo});
            pr_default.close(8);
            dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoErro") ;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(6);
         if ( AV101GXLvl195 == 0 )
         {
            /* Using cursor P004I12 */
            pr_default.execute(9, new Object[] {AV18Contagemresultado_Codigo});
            while ( (pr_default.getStatus(9) != 101) )
            {
               A456ContagemResultado_Codigo = P004I12_A456ContagemResultado_Codigo[0];
               A508ContagemResultado_Owner = P004I12_A508ContagemResultado_Owner[0];
               A584ContagemResultado_ContadorFM = P004I12_A584ContagemResultado_ContadorFM[0];
               n584ContagemResultado_ContadorFM = P004I12_n584ContagemResultado_ContadorFM[0];
               A584ContagemResultado_ContadorFM = P004I12_A584ContagemResultado_ContadorFM[0];
               n584ContagemResultado_ContadorFM = P004I12_n584ContagemResultado_ContadorFM[0];
               if ( (0==A584ContagemResultado_ContadorFM) )
               {
                  AV36ContagemResultadoErro_ContadorFMCod = A508ContagemResultado_Owner;
               }
               else
               {
                  AV36ContagemResultadoErro_ContadorFMCod = A584ContagemResultado_ContadorFM;
               }
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(9);
            /*
               INSERT RECORD ON TABLE ContagemResultadoErro

            */
            A456ContagemResultado_Codigo = AV18Contagemresultado_Codigo;
            A579ContagemResultadoErro_Tipo = AV38ContagemResultadoErro_Tipo;
            A583ContagemResultadoErro_ContadorFMCod = AV36ContagemResultadoErro_ContadorFMCod;
            A580ContagemResultadoErro_Data = DateTimeUtil.ServerNow( context, "DEFAULT");
            A581ContagemResultadoErro_Status = "P";
            /* Using cursor P004I13 */
            pr_default.execute(10, new Object[] {A456ContagemResultado_Codigo, A579ContagemResultadoErro_Tipo, A580ContagemResultadoErro_Data, A581ContagemResultadoErro_Status, A583ContagemResultadoErro_ContadorFMCod});
            pr_default.close(10);
            dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoErro") ;
            if ( (pr_default.getStatus(10) == 1) )
            {
               context.Gx_err = 1;
               Gx_emsg = (String)(context.GetMessage( "GXM_noupdate", ""));
            }
            else
            {
               context.Gx_err = 0;
               Gx_emsg = "";
            }
            /* End Insert */
         }
      }

      protected void S131( )
      {
         /* 'ATUALIZAERRO' Routine */
         /* Using cursor P004I14 */
         pr_default.execute(11, new Object[] {AV18Contagemresultado_Codigo, AV38ContagemResultadoErro_Tipo});
         while ( (pr_default.getStatus(11) != 101) )
         {
            A579ContagemResultadoErro_Tipo = P004I14_A579ContagemResultadoErro_Tipo[0];
            A456ContagemResultado_Codigo = P004I14_A456ContagemResultado_Codigo[0];
            A581ContagemResultadoErro_Status = P004I14_A581ContagemResultadoErro_Status[0];
            if ( StringUtil.StrCmp(A581ContagemResultadoErro_Status, "P") == 0 )
            {
               A581ContagemResultadoErro_Status = "C";
            }
            else if ( StringUtil.StrCmp(A581ContagemResultadoErro_Status, "E") == 0 )
            {
               A581ContagemResultadoErro_Status = "R";
            }
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            /* Using cursor P004I15 */
            pr_default.execute(12, new Object[] {A581ContagemResultadoErro_Status, A456ContagemResultado_Codigo, A579ContagemResultadoErro_Tipo});
            pr_default.close(12);
            dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoErro") ;
            if (true) break;
            /* Using cursor P004I16 */
            pr_default.execute(13, new Object[] {A581ContagemResultadoErro_Status, A456ContagemResultado_Codigo, A579ContagemResultadoErro_Tipo});
            pr_default.close(13);
            dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoErro") ;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(11);
      }

      protected void S141( )
      {
         /* 'STATUSOSVNC' Routine */
         /* Using cursor P004I17 */
         pr_default.execute(14, new Object[] {AV91ContagemResultado_OSVinculada});
         while ( (pr_default.getStatus(14) != 101) )
         {
            A1553ContagemResultado_CntSrvCod = P004I17_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = P004I17_n1553ContagemResultado_CntSrvCod[0];
            A601ContagemResultado_Servico = P004I17_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = P004I17_n601ContagemResultado_Servico[0];
            A484ContagemResultado_StatusDmn = P004I17_A484ContagemResultado_StatusDmn[0];
            n484ContagemResultado_StatusDmn = P004I17_n484ContagemResultado_StatusDmn[0];
            A456ContagemResultado_Codigo = P004I17_A456ContagemResultado_Codigo[0];
            A801ContagemResultado_ServicoSigla = P004I17_A801ContagemResultado_ServicoSigla[0];
            n801ContagemResultado_ServicoSigla = P004I17_n801ContagemResultado_ServicoSigla[0];
            A601ContagemResultado_Servico = P004I17_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = P004I17_n601ContagemResultado_Servico[0];
            A801ContagemResultado_ServicoSigla = P004I17_A801ContagemResultado_ServicoSigla[0];
            n801ContagemResultado_ServicoSigla = P004I17_n801ContagemResultado_ServicoSigla[0];
            AV59Ok = false;
            AV54linha = "Demanda: " + AV21ContagemResultado_Demanda + " com " + StringUtil.Trim( A801ContagemResultado_ServicoSigla) + " em Aberto";
            H4I0( false, 21) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV54linha, "")), 25, Gx_line+0, 807, Gx_line+15, 0+256, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+21);
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(14);
      }

      protected void H4I0( bool bFoot ,
                           int Inc )
      {
         /* Skip the required number of lines */
         while ( ( ToSkip > 0 ) || ( Gx_line + Inc > P_lines ) )
         {
            if ( Gx_line + Inc >= P_lines )
            {
               if ( Gx_page > 0 )
               {
                  /* Print footers */
                  Gx_line = P_lines;
                  getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
                  getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(Gx_page), "ZZZZZ9")), 708, Gx_line+0, 747, Gx_line+15, 1+256, 0, 0, 0) ;
                  getPrinter().GxDrawText("P�gina", 667, Gx_line+0, 702, Gx_line+14, 0+256, 0, 0, 0) ;
                  getPrinter().GxDrawText("de", 750, Gx_line+0, 764, Gx_line+14, 0+256, 0, 0, 0) ;
                  getPrinter().GxDrawText("{{Pages}}", 767, Gx_line+0, 816, Gx_line+14, 1+256, 0, 0, 0) ;
                  Gx_OldLine = Gx_line;
                  Gx_line = (int)(Gx_line+15);
                  getPrinter().GxEndPage() ;
                  if ( bFoot )
                  {
                     return  ;
                  }
               }
               ToSkip = 0;
               Gx_line = 0;
               Gx_page = (int)(Gx_page+1);
               /* Skip Margin Top Lines */
               Gx_line = (int)(Gx_line+(M_top*lineHeight));
               /* Print headers */
               getPrinter().GxStartPage() ;
               getPrinter().GxDrawLine(0, Gx_line+117, 825, Gx_line+117, 1, 0, 0, 0, 0) ;
               getPrinter().GxDrawBitMap(context.GetImagePath( "7ad3f77f-79a3-4554-aaa4-e84910e0d464", "", context.GetTheme( )), 0, Gx_line+0, 120, Gx_line+100) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText(context.localUtil.Format( Gx_date, "99/99/99"), 700, Gx_line+5, 749, Gx_line+20, 2+256, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( Gx_time, "")), 758, Gx_line+5, 824, Gx_line+20, 0, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV57NomeArq, "")), 140, Gx_line+50, 662, Gx_line+65, 1+256, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV81SubTitulo, "")), 140, Gx_line+67, 662, Gx_line+82, 1+256, 0, 0, 0) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 9, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV63Periodo, "")), 325, Gx_line+29, 482, Gx_line+46, 1+256, 0, 0, 0) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 12, true, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( AV78Titulo, "")), 150, Gx_line+0, 651, Gx_line+22, 1+256, 0, 0, 0) ;
               Gx_OldLine = Gx_line;
               Gx_line = (int)(Gx_line+133);
               if (true) break;
            }
            else
            {
               PrtOffset = 0;
               Gx_line = (int)(Gx_line+1);
            }
            ToSkip = (int)(ToSkip-1);
         }
         getPrinter().setPage(Gx_page);
      }

      protected void add_metrics( )
      {
         add_metrics0( ) ;
         add_metrics1( ) ;
      }

      protected void add_metrics0( )
      {
         getPrinter().setMetrics("Microsoft Sans Serif", false, false, 58, 14, 72, 171,  new int[] {48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 18, 20, 23, 36, 36, 57, 43, 12, 21, 21, 25, 37, 18, 21, 18, 18, 36, 36, 36, 36, 36, 36, 36, 36, 36, 36, 18, 18, 37, 37, 37, 36, 65, 43, 43, 46, 46, 43, 39, 50, 46, 18, 32, 43, 36, 53, 46, 50, 43, 50, 46, 43, 40, 46, 43, 64, 41, 42, 39, 18, 18, 18, 27, 36, 21, 36, 36, 32, 36, 36, 18, 36, 36, 14, 15, 33, 14, 55, 36, 36, 36, 36, 21, 32, 18, 36, 33, 47, 31, 31, 31, 21, 17, 21, 37, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 18, 20, 36, 36, 36, 36, 17, 36, 21, 47, 24, 36, 37, 21, 47, 35, 26, 35, 21, 21, 21, 37, 34, 21, 21, 21, 23, 36, 53, 53, 53, 39, 43, 43, 43, 43, 43, 43, 64, 46, 43, 43, 43, 43, 18, 18, 18, 18, 46, 46, 50, 50, 50, 50, 50, 37, 50, 46, 46, 46, 46, 43, 43, 39, 36, 36, 36, 36, 36, 36, 57, 32, 36, 36, 36, 36, 18, 18, 18, 18, 36, 36, 36, 36, 36, 36, 36, 35, 39, 36, 36, 36, 36, 32, 36, 32}) ;
      }

      protected void add_metrics1( )
      {
         getPrinter().setMetrics("Microsoft Sans Serif", true, false, 57, 15, 72, 163,  new int[] {47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 17, 19, 29, 34, 34, 55, 45, 15, 21, 21, 24, 36, 17, 21, 17, 17, 34, 34, 34, 34, 34, 34, 34, 34, 34, 34, 21, 21, 36, 36, 36, 38, 60, 43, 45, 45, 45, 41, 38, 48, 45, 17, 34, 45, 38, 53, 45, 48, 41, 48, 45, 41, 38, 45, 41, 57, 41, 41, 38, 21, 17, 21, 36, 34, 21, 34, 38, 34, 38, 34, 21, 38, 38, 17, 17, 34, 17, 55, 38, 38, 38, 38, 24, 34, 21, 38, 33, 49, 34, 34, 31, 24, 17, 24, 36, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 47, 17, 21, 34, 34, 34, 34, 17, 34, 21, 46, 23, 34, 36, 21, 46, 34, 25, 34, 21, 21, 21, 36, 34, 21, 20, 21, 23, 34, 52, 52, 52, 38, 45, 45, 45, 45, 45, 45, 62, 45, 41, 41, 41, 41, 17, 17, 17, 17, 45, 45, 48, 48, 48, 48, 48, 36, 48, 45, 45, 45, 45, 41, 41, 38, 34, 34, 34, 34, 34, 34, 55, 34, 34, 34, 34, 34, 17, 17, 17, 17, 38, 38, 38, 38, 38, 38, 38, 34, 38, 38, 38, 38, 38, 34, 38, 34}) ;
      }

      public override int getOutputType( )
      {
         return GxReportUtils.OUTPUT_PDF ;
      }

      public override void cleanup( )
      {
         context.CommitDataStores( "REL_ConferenciaCAST");
         CloseOpenCursors();
         if (IsMain)	waitPrinterEnd();
         base.cleanup();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         GXKey = "";
         gxfirstwebparm = "";
         AV79WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV57NomeArq = "";
         scmdbuf = "";
         P004I2_A40Contratada_PessoaCod = new int[1] ;
         P004I2_A39Contratada_Codigo = new int[1] ;
         P004I2_A41Contratada_PessoaNom = new String[] {""} ;
         P004I2_n41Contratada_PessoaNom = new bool[] {false} ;
         A41Contratada_PessoaNom = "";
         AV81SubTitulo = "";
         AV49ExcelDocument = new ExcelDocumentI();
         AV43Date = DateTime.MinValue;
         AV42DataIni = DateTime.MinValue;
         AV41DataFim = DateTime.MinValue;
         AV78Titulo = "";
         AV63Periodo = "";
         AV21ContagemResultado_Demanda = "";
         P004I3_A489ContagemResultado_SistemaCod = new int[1] ;
         P004I3_n489ContagemResultado_SistemaCod = new bool[] {false} ;
         P004I3_A805ContagemResultado_ContratadaOrigemCod = new int[1] ;
         P004I3_n805ContagemResultado_ContratadaOrigemCod = new bool[] {false} ;
         P004I3_A457ContagemResultado_Demanda = new String[] {""} ;
         P004I3_n457ContagemResultado_Demanda = new bool[] {false} ;
         P004I3_A456ContagemResultado_Codigo = new int[1] ;
         P004I3_A484ContagemResultado_StatusDmn = new String[] {""} ;
         P004I3_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         P004I3_A602ContagemResultado_OSVinculada = new int[1] ;
         P004I3_n602ContagemResultado_OSVinculada = new bool[] {false} ;
         P004I3_A494ContagemResultado_Descricao = new String[] {""} ;
         P004I3_n494ContagemResultado_Descricao = new bool[] {false} ;
         P004I3_A515ContagemResultado_SistemaCoord = new String[] {""} ;
         P004I3_n515ContagemResultado_SistemaCoord = new bool[] {false} ;
         A457ContagemResultado_Demanda = "";
         A484ContagemResultado_StatusDmn = "";
         A494ContagemResultado_Descricao = "";
         A515ContagemResultado_SistemaCoord = "";
         OV24ContagemResultado_HoraCnt = "";
         AV24ContagemResultado_HoraCnt = context.localUtil.Time( );
         AV34ContagemResultado_Ultima = true;
         AV35ContagemResultadoContagens_Esforco = 0;
         AV38ContagemResultadoErro_Tipo = "";
         AV54linha = "";
         P004I4_A456ContagemResultado_Codigo = new int[1] ;
         P004I4_A517ContagemResultado_Ultima = new bool[] {false} ;
         P004I4_A473ContagemResultado_DataCnt = new DateTime[] {DateTime.MinValue} ;
         P004I4_A511ContagemResultado_HoraCnt = new String[] {""} ;
         P004I4_A463ContagemResultado_ParecerTcn = new String[] {""} ;
         P004I4_n463ContagemResultado_ParecerTcn = new bool[] {false} ;
         P004I4_A460ContagemResultado_PFBFM = new decimal[1] ;
         P004I4_n460ContagemResultado_PFBFM = new bool[] {false} ;
         P004I4_A458ContagemResultado_PFBFS = new decimal[1] ;
         P004I4_n458ContagemResultado_PFBFS = new bool[] {false} ;
         P004I4_A461ContagemResultado_PFLFM = new decimal[1] ;
         P004I4_n461ContagemResultado_PFLFM = new bool[] {false} ;
         P004I4_A459ContagemResultado_PFLFS = new decimal[1] ;
         P004I4_n459ContagemResultado_PFLFS = new bool[] {false} ;
         P004I4_A470ContagemResultado_ContadorFMCod = new int[1] ;
         P004I4_A462ContagemResultado_Divergencia = new decimal[1] ;
         P004I4_A469ContagemResultado_NaoCnfCntCod = new int[1] ;
         P004I4_n469ContagemResultado_NaoCnfCntCod = new bool[] {false} ;
         P004I4_A483ContagemResultado_StatusCnt = new short[1] ;
         P004I4_A482ContagemResultadoContagens_Esforco = new short[1] ;
         A473ContagemResultado_DataCnt = DateTime.MinValue;
         A511ContagemResultado_HoraCnt = "";
         A463ContagemResultado_ParecerTcn = "";
         AV20ContagemResultado_DataCnt = DateTime.MinValue;
         AV26ContagemResultado_ParecerTcn = "";
         P004I8_A579ContagemResultadoErro_Tipo = new String[] {""} ;
         P004I8_A456ContagemResultado_Codigo = new int[1] ;
         P004I8_A580ContagemResultadoErro_Data = new DateTime[] {DateTime.MinValue} ;
         P004I8_A581ContagemResultadoErro_Status = new String[] {""} ;
         A579ContagemResultadoErro_Tipo = "";
         A580ContagemResultadoErro_Data = (DateTime)(DateTime.MinValue);
         A581ContagemResultadoErro_Status = "";
         P004I12_A456ContagemResultado_Codigo = new int[1] ;
         P004I12_A508ContagemResultado_Owner = new int[1] ;
         P004I12_A584ContagemResultado_ContadorFM = new int[1] ;
         P004I12_n584ContagemResultado_ContadorFM = new bool[] {false} ;
         Gx_emsg = "";
         P004I14_A579ContagemResultadoErro_Tipo = new String[] {""} ;
         P004I14_A456ContagemResultado_Codigo = new int[1] ;
         P004I14_A581ContagemResultadoErro_Status = new String[] {""} ;
         P004I17_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P004I17_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P004I17_A601ContagemResultado_Servico = new int[1] ;
         P004I17_n601ContagemResultado_Servico = new bool[] {false} ;
         P004I17_A484ContagemResultado_StatusDmn = new String[] {""} ;
         P004I17_n484ContagemResultado_StatusDmn = new bool[] {false} ;
         P004I17_A456ContagemResultado_Codigo = new int[1] ;
         P004I17_A801ContagemResultado_ServicoSigla = new String[] {""} ;
         P004I17_n801ContagemResultado_ServicoSigla = new bool[] {false} ;
         A801ContagemResultado_ServicoSigla = "";
         Gx_date = DateTime.MinValue;
         Gx_time = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.arel_conferenciacast__default(),
            new Object[][] {
                new Object[] {
               P004I2_A40Contratada_PessoaCod, P004I2_A39Contratada_Codigo, P004I2_A41Contratada_PessoaNom, P004I2_n41Contratada_PessoaNom
               }
               , new Object[] {
               P004I3_A489ContagemResultado_SistemaCod, P004I3_n489ContagemResultado_SistemaCod, P004I3_A805ContagemResultado_ContratadaOrigemCod, P004I3_n805ContagemResultado_ContratadaOrigemCod, P004I3_A457ContagemResultado_Demanda, P004I3_n457ContagemResultado_Demanda, P004I3_A456ContagemResultado_Codigo, P004I3_A484ContagemResultado_StatusDmn, P004I3_n484ContagemResultado_StatusDmn, P004I3_A602ContagemResultado_OSVinculada,
               P004I3_n602ContagemResultado_OSVinculada, P004I3_A494ContagemResultado_Descricao, P004I3_n494ContagemResultado_Descricao, P004I3_A515ContagemResultado_SistemaCoord, P004I3_n515ContagemResultado_SistemaCoord
               }
               , new Object[] {
               P004I4_A456ContagemResultado_Codigo, P004I4_A517ContagemResultado_Ultima, P004I4_A473ContagemResultado_DataCnt, P004I4_A511ContagemResultado_HoraCnt, P004I4_A463ContagemResultado_ParecerTcn, P004I4_n463ContagemResultado_ParecerTcn, P004I4_A460ContagemResultado_PFBFM, P004I4_n460ContagemResultado_PFBFM, P004I4_A458ContagemResultado_PFBFS, P004I4_n458ContagemResultado_PFBFS,
               P004I4_A461ContagemResultado_PFLFM, P004I4_n461ContagemResultado_PFLFM, P004I4_A459ContagemResultado_PFLFS, P004I4_n459ContagemResultado_PFLFS, P004I4_A470ContagemResultado_ContadorFMCod, P004I4_A462ContagemResultado_Divergencia, P004I4_A469ContagemResultado_NaoCnfCntCod, P004I4_n469ContagemResultado_NaoCnfCntCod, P004I4_A483ContagemResultado_StatusCnt, P004I4_A482ContagemResultadoContagens_Esforco
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               P004I8_A579ContagemResultadoErro_Tipo, P004I8_A456ContagemResultado_Codigo, P004I8_A580ContagemResultadoErro_Data, P004I8_A581ContagemResultadoErro_Status
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               P004I12_A456ContagemResultado_Codigo, P004I12_A508ContagemResultado_Owner, P004I12_A584ContagemResultado_ContadorFM, P004I12_n584ContagemResultado_ContadorFM
               }
               , new Object[] {
               }
               , new Object[] {
               P004I14_A579ContagemResultadoErro_Tipo, P004I14_A456ContagemResultado_Codigo, P004I14_A581ContagemResultadoErro_Status
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               P004I17_A1553ContagemResultado_CntSrvCod, P004I17_n1553ContagemResultado_CntSrvCod, P004I17_A601ContagemResultado_Servico, P004I17_n601ContagemResultado_Servico, P004I17_A484ContagemResultado_StatusDmn, P004I17_n484ContagemResultado_StatusDmn, P004I17_A456ContagemResultado_Codigo, P004I17_A801ContagemResultado_ServicoSigla, P004I17_n801ContagemResultado_ServicoSigla
               }
            }
         );
         Gx_time = context.localUtil.Time( );
         Gx_date = DateTimeUtil.Today( context);
         /* GeneXus formulas. */
         Gx_line = 0;
         Gx_time = context.localUtil.Time( );
         Gx_date = DateTimeUtil.Today( context);
         context.Gx_err = 0;
      }

      private short gxcookieaux ;
      private short nGotPars ;
      private short AV89PraLinCAST ;
      private short AV83ColDmnCASTn ;
      private short AV84ColDataCASTn ;
      private short AV85ColPFBFSCASTn ;
      private short AV86ColPFLFSCASTn ;
      private short AV87ColPFBFMCASTn ;
      private short AV88ColPFLFMCASTn ;
      private short GxWebError ;
      private short AV55Ln ;
      private short AV99GXLvl50 ;
      private short OV35ContagemResultadoContagens_Esforco ;
      private short AV35ContagemResultadoContagens_Esforco ;
      private short A483ContagemResultado_StatusCnt ;
      private short A482ContagemResultadoContagens_Esforco ;
      private short AV32ContagemResultado_StatusCnt ;
      private short AV17Conferida ;
      private short AV72Semcadastrar ;
      private short AV101GXLvl195 ;
      private int AV39Contratada_Codigo ;
      private int M_top ;
      private int M_bot ;
      private int Line ;
      private int ToSkip ;
      private int PrtOffset ;
      private int A40Contratada_PessoaCod ;
      private int A39Contratada_Codigo ;
      private int A489ContagemResultado_SistemaCod ;
      private int A805ContagemResultado_ContratadaOrigemCod ;
      private int A456ContagemResultado_Codigo ;
      private int A602ContagemResultado_OSVinculada ;
      private int AV18Contagemresultado_Codigo ;
      private int AV91ContagemResultado_OSVinculada ;
      private int Gx_OldLine ;
      private int A470ContagemResultado_ContadorFMCod ;
      private int A469ContagemResultado_NaoCnfCntCod ;
      private int AV19ContagemResultado_ContadorFMCod ;
      private int AV25ContagemResultado_NaoCnfCntCod ;
      private int A508ContagemResultado_Owner ;
      private int A584ContagemResultado_ContadorFM ;
      private int AV36ContagemResultadoErro_ContadorFMCod ;
      private int GX_INS75 ;
      private int A583ContagemResultadoErro_ContadorFMCod ;
      private int A1553ContagemResultado_CntSrvCod ;
      private int A601ContagemResultado_Servico ;
      private decimal AV64PFBFM ;
      private decimal AV68PFLFM ;
      private decimal AV65PFBFS ;
      private decimal AV69PFLFS ;
      private decimal A460ContagemResultado_PFBFM ;
      private decimal A458ContagemResultado_PFBFS ;
      private decimal A461ContagemResultado_PFLFM ;
      private decimal A459ContagemResultado_PFLFS ;
      private decimal A462ContagemResultado_Divergencia ;
      private decimal AV27ContagemResultado_PFBFM ;
      private decimal AV28ContagemResultado_PFBFS ;
      private decimal AV30ContagemResultado_PFLFM ;
      private decimal AV31ContagemResultado_PFLFS ;
      private decimal AV23ContagemResultado_Divergencia ;
      private String GXKey ;
      private String gxfirstwebparm ;
      private String AV10Arquivo ;
      private String AV90Aba ;
      private String AV92FileName ;
      private String AV57NomeArq ;
      private String scmdbuf ;
      private String A41Contratada_PessoaNom ;
      private String AV81SubTitulo ;
      private String AV78Titulo ;
      private String AV63Periodo ;
      private String A484ContagemResultado_StatusDmn ;
      private String OV24ContagemResultado_HoraCnt ;
      private String AV24ContagemResultado_HoraCnt ;
      private String AV38ContagemResultadoErro_Tipo ;
      private String AV54linha ;
      private String A511ContagemResultado_HoraCnt ;
      private String A579ContagemResultadoErro_Tipo ;
      private String A581ContagemResultadoErro_Status ;
      private String Gx_emsg ;
      private String A801ContagemResultado_ServicoSigla ;
      private String Gx_time ;
      private DateTime A580ContagemResultadoErro_Data ;
      private DateTime AV43Date ;
      private DateTime AV42DataIni ;
      private DateTime AV41DataFim ;
      private DateTime A473ContagemResultado_DataCnt ;
      private DateTime AV20ContagemResultado_DataCnt ;
      private DateTime Gx_date ;
      private bool entryPointCalled ;
      private bool returnInSub ;
      private bool n41Contratada_PessoaNom ;
      private bool AV51Flag ;
      private bool n489ContagemResultado_SistemaCod ;
      private bool n805ContagemResultado_ContratadaOrigemCod ;
      private bool n457ContagemResultado_Demanda ;
      private bool n484ContagemResultado_StatusDmn ;
      private bool n602ContagemResultado_OSVinculada ;
      private bool n494ContagemResultado_Descricao ;
      private bool n515ContagemResultado_SistemaCoord ;
      private bool OV34ContagemResultado_Ultima ;
      private bool AV34ContagemResultado_Ultima ;
      private bool AV59Ok ;
      private bool A517ContagemResultado_Ultima ;
      private bool n463ContagemResultado_ParecerTcn ;
      private bool n460ContagemResultado_PFBFM ;
      private bool n458ContagemResultado_PFBFS ;
      private bool n461ContagemResultado_PFLFM ;
      private bool n459ContagemResultado_PFLFS ;
      private bool n469ContagemResultado_NaoCnfCntCod ;
      private bool n584ContagemResultado_ContadorFM ;
      private bool n1553ContagemResultado_CntSrvCod ;
      private bool n601ContagemResultado_Servico ;
      private bool n801ContagemResultado_ServicoSigla ;
      private String A463ContagemResultado_ParecerTcn ;
      private String AV26ContagemResultado_ParecerTcn ;
      private String AV21ContagemResultado_Demanda ;
      private String A457ContagemResultado_Demanda ;
      private String A494ContagemResultado_Descricao ;
      private String A515ContagemResultado_SistemaCoord ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private String aP10_FileName ;
      private IDataStoreProvider pr_default ;
      private int[] P004I2_A40Contratada_PessoaCod ;
      private int[] P004I2_A39Contratada_Codigo ;
      private String[] P004I2_A41Contratada_PessoaNom ;
      private bool[] P004I2_n41Contratada_PessoaNom ;
      private int[] P004I3_A489ContagemResultado_SistemaCod ;
      private bool[] P004I3_n489ContagemResultado_SistemaCod ;
      private int[] P004I3_A805ContagemResultado_ContratadaOrigemCod ;
      private bool[] P004I3_n805ContagemResultado_ContratadaOrigemCod ;
      private String[] P004I3_A457ContagemResultado_Demanda ;
      private bool[] P004I3_n457ContagemResultado_Demanda ;
      private int[] P004I3_A456ContagemResultado_Codigo ;
      private String[] P004I3_A484ContagemResultado_StatusDmn ;
      private bool[] P004I3_n484ContagemResultado_StatusDmn ;
      private int[] P004I3_A602ContagemResultado_OSVinculada ;
      private bool[] P004I3_n602ContagemResultado_OSVinculada ;
      private String[] P004I3_A494ContagemResultado_Descricao ;
      private bool[] P004I3_n494ContagemResultado_Descricao ;
      private String[] P004I3_A515ContagemResultado_SistemaCoord ;
      private bool[] P004I3_n515ContagemResultado_SistemaCoord ;
      private int[] P004I4_A456ContagemResultado_Codigo ;
      private bool[] P004I4_A517ContagemResultado_Ultima ;
      private DateTime[] P004I4_A473ContagemResultado_DataCnt ;
      private String[] P004I4_A511ContagemResultado_HoraCnt ;
      private String[] P004I4_A463ContagemResultado_ParecerTcn ;
      private bool[] P004I4_n463ContagemResultado_ParecerTcn ;
      private decimal[] P004I4_A460ContagemResultado_PFBFM ;
      private bool[] P004I4_n460ContagemResultado_PFBFM ;
      private decimal[] P004I4_A458ContagemResultado_PFBFS ;
      private bool[] P004I4_n458ContagemResultado_PFBFS ;
      private decimal[] P004I4_A461ContagemResultado_PFLFM ;
      private bool[] P004I4_n461ContagemResultado_PFLFM ;
      private decimal[] P004I4_A459ContagemResultado_PFLFS ;
      private bool[] P004I4_n459ContagemResultado_PFLFS ;
      private int[] P004I4_A470ContagemResultado_ContadorFMCod ;
      private decimal[] P004I4_A462ContagemResultado_Divergencia ;
      private int[] P004I4_A469ContagemResultado_NaoCnfCntCod ;
      private bool[] P004I4_n469ContagemResultado_NaoCnfCntCod ;
      private short[] P004I4_A483ContagemResultado_StatusCnt ;
      private short[] P004I4_A482ContagemResultadoContagens_Esforco ;
      private String[] P004I8_A579ContagemResultadoErro_Tipo ;
      private int[] P004I8_A456ContagemResultado_Codigo ;
      private DateTime[] P004I8_A580ContagemResultadoErro_Data ;
      private String[] P004I8_A581ContagemResultadoErro_Status ;
      private int[] P004I12_A456ContagemResultado_Codigo ;
      private int[] P004I12_A508ContagemResultado_Owner ;
      private int[] P004I12_A584ContagemResultado_ContadorFM ;
      private bool[] P004I12_n584ContagemResultado_ContadorFM ;
      private String[] P004I14_A579ContagemResultadoErro_Tipo ;
      private int[] P004I14_A456ContagemResultado_Codigo ;
      private String[] P004I14_A581ContagemResultadoErro_Status ;
      private int[] P004I17_A1553ContagemResultado_CntSrvCod ;
      private bool[] P004I17_n1553ContagemResultado_CntSrvCod ;
      private int[] P004I17_A601ContagemResultado_Servico ;
      private bool[] P004I17_n601ContagemResultado_Servico ;
      private String[] P004I17_A484ContagemResultado_StatusDmn ;
      private bool[] P004I17_n484ContagemResultado_StatusDmn ;
      private int[] P004I17_A456ContagemResultado_Codigo ;
      private String[] P004I17_A801ContagemResultado_ServicoSigla ;
      private bool[] P004I17_n801ContagemResultado_ServicoSigla ;
      private ExcelDocumentI AV49ExcelDocument ;
      private wwpbaseobjects.SdtWWPContext AV79WWPContext ;
   }

   public class arel_conferenciacast__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new UpdateCursor(def[3])
         ,new UpdateCursor(def[4])
         ,new UpdateCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new UpdateCursor(def[7])
         ,new UpdateCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new UpdateCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new UpdateCursor(def[12])
         ,new UpdateCursor(def[13])
         ,new ForEachCursor(def[14])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP004I2 ;
          prmP004I2 = new Object[] {
          new Object[] {"@AV39Contratada_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP004I3 ;
          prmP004I3 = new Object[] {
          new Object[] {"@AV39Contratada_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV21ContagemResultado_Demanda",SqlDbType.VarChar,30,0}
          } ;
          Object[] prmP004I4 ;
          prmP004I4 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP004I5 ;
          prmP004I5 = new Object[] {
          new Object[] {"@ContagemResultado_StatusCnt",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_DataCnt",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_HoraCnt",SqlDbType.Char,5,0}
          } ;
          Object[] prmP004I6 ;
          prmP004I6 = new Object[] {
          new Object[] {"@ContagemResultado_StatusCnt",SqlDbType.SmallInt,2,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_DataCnt",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_HoraCnt",SqlDbType.Char,5,0}
          } ;
          Object[] prmP004I7 ;
          prmP004I7 = new Object[] {
          new Object[] {"@ContagemResultado_StatusDmn",SqlDbType.Char,1,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP004I8 ;
          prmP004I8 = new Object[] {
          new Object[] {"@AV18Contagemresultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV38ContagemResultadoErro_Tipo",SqlDbType.Char,2,0}
          } ;
          Object[] prmP004I9 ;
          prmP004I9 = new Object[] {
          new Object[] {"@ContagemResultadoErro_Data",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultadoErro_Status",SqlDbType.Char,1,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultadoErro_Tipo",SqlDbType.Char,2,0}
          } ;
          Object[] prmP004I10 ;
          prmP004I10 = new Object[] {
          new Object[] {"@ContagemResultadoErro_Data",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultadoErro_Status",SqlDbType.Char,1,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultadoErro_Tipo",SqlDbType.Char,2,0}
          } ;
          Object[] prmP004I12 ;
          prmP004I12 = new Object[] {
          new Object[] {"@AV18Contagemresultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP004I13 ;
          prmP004I13 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultadoErro_Tipo",SqlDbType.Char,2,0} ,
          new Object[] {"@ContagemResultadoErro_Data",SqlDbType.DateTime,8,5} ,
          new Object[] {"@ContagemResultadoErro_Status",SqlDbType.Char,1,0} ,
          new Object[] {"@ContagemResultadoErro_ContadorFMCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmP004I14 ;
          prmP004I14 = new Object[] {
          new Object[] {"@AV18Contagemresultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV38ContagemResultadoErro_Tipo",SqlDbType.Char,2,0}
          } ;
          Object[] prmP004I15 ;
          prmP004I15 = new Object[] {
          new Object[] {"@ContagemResultadoErro_Status",SqlDbType.Char,1,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultadoErro_Tipo",SqlDbType.Char,2,0}
          } ;
          Object[] prmP004I16 ;
          prmP004I16 = new Object[] {
          new Object[] {"@ContagemResultadoErro_Status",SqlDbType.Char,1,0} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultadoErro_Tipo",SqlDbType.Char,2,0}
          } ;
          Object[] prmP004I17 ;
          prmP004I17 = new Object[] {
          new Object[] {"@AV91ContagemResultado_OSVinculada",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P004I2", "SELECT T1.[Contratada_PessoaCod] AS Contratada_PessoaCod, T1.[Contratada_Codigo], T2.[Pessoa_Nome] AS Contratada_PessoaNom FROM ([Contratada] T1 WITH (NOLOCK) INNER JOIN [Pessoa] T2 WITH (NOLOCK) ON T2.[Pessoa_Codigo] = T1.[Contratada_PessoaCod]) WHERE T1.[Contratada_Codigo] = @AV39Contratada_Codigo ORDER BY T1.[Contratada_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP004I2,1,0,false,true )
             ,new CursorDef("P004I3", "SELECT T1.[ContagemResultado_SistemaCod] AS ContagemResultado_SistemaCod, T1.[ContagemResultado_ContratadaOrigemCod], T1.[ContagemResultado_Demanda], T1.[ContagemResultado_Codigo], T1.[ContagemResultado_StatusDmn], T1.[ContagemResultado_OSVinculada], T1.[ContagemResultado_Descricao], T2.[Sistema_Coordenacao] AS ContagemResultado_SistemaCoord FROM ([ContagemResultado] T1 WITH (UPDLOCK) LEFT JOIN [Sistema] T2 WITH (NOLOCK) ON T2.[Sistema_Codigo] = T1.[ContagemResultado_SistemaCod]) WHERE T1.[ContagemResultado_ContratadaOrigemCod] = @AV39Contratada_Codigo and T1.[ContagemResultado_Demanda] = @AV21ContagemResultado_Demanda ORDER BY T1.[ContagemResultado_ContratadaOrigemCod], T1.[ContagemResultado_Demanda] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP004I3,1,0,true,false )
             ,new CursorDef("P004I4", "SELECT TOP 1 [ContagemResultado_Codigo], [ContagemResultado_Ultima], [ContagemResultado_DataCnt], [ContagemResultado_HoraCnt], [ContagemResultado_ParecerTcn], [ContagemResultado_PFBFM], [ContagemResultado_PFBFS], [ContagemResultado_PFLFM], [ContagemResultado_PFLFS], [ContagemResultado_ContadorFMCod], [ContagemResultado_Divergencia], [ContagemResultado_NaoCnfCntCod], [ContagemResultado_StatusCnt], [ContagemResultadoContagens_Esforco] FROM [ContagemResultadoContagens] WITH (UPDLOCK) WHERE ([ContagemResultado_Codigo] = @ContagemResultado_Codigo) AND ([ContagemResultado_Ultima] = 1) ORDER BY [ContagemResultado_Codigo] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP004I4,1,0,true,true )
             ,new CursorDef("P004I5", "UPDATE [ContagemResultadoContagens] SET [ContagemResultado_StatusCnt]=@ContagemResultado_StatusCnt  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo AND [ContagemResultado_DataCnt] = @ContagemResultado_DataCnt AND [ContagemResultado_HoraCnt] = @ContagemResultado_HoraCnt", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP004I5)
             ,new CursorDef("P004I6", "UPDATE [ContagemResultadoContagens] SET [ContagemResultado_StatusCnt]=@ContagemResultado_StatusCnt  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo AND [ContagemResultado_DataCnt] = @ContagemResultado_DataCnt AND [ContagemResultado_HoraCnt] = @ContagemResultado_HoraCnt", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP004I6)
             ,new CursorDef("P004I7", "UPDATE [ContagemResultado] SET [ContagemResultado_StatusDmn]=@ContagemResultado_StatusDmn  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP004I7)
             ,new CursorDef("P004I8", "SELECT TOP 1 [ContagemResultadoErro_Tipo], [ContagemResultado_Codigo], [ContagemResultadoErro_Data], [ContagemResultadoErro_Status] FROM [ContagemResultadoErro] WITH (UPDLOCK) WHERE [ContagemResultado_Codigo] = @AV18Contagemresultado_Codigo and [ContagemResultadoErro_Tipo] = @AV38ContagemResultadoErro_Tipo ORDER BY [ContagemResultado_Codigo], [ContagemResultadoErro_Tipo] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP004I8,1,0,true,true )
             ,new CursorDef("P004I9", "UPDATE [ContagemResultadoErro] SET [ContagemResultadoErro_Data]=@ContagemResultadoErro_Data, [ContagemResultadoErro_Status]=@ContagemResultadoErro_Status  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo AND [ContagemResultadoErro_Tipo] = @ContagemResultadoErro_Tipo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP004I9)
             ,new CursorDef("P004I10", "UPDATE [ContagemResultadoErro] SET [ContagemResultadoErro_Data]=@ContagemResultadoErro_Data, [ContagemResultadoErro_Status]=@ContagemResultadoErro_Status  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo AND [ContagemResultadoErro_Tipo] = @ContagemResultadoErro_Tipo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP004I10)
             ,new CursorDef("P004I12", "SELECT TOP 1 T1.[ContagemResultado_Codigo], T1.[ContagemResultado_Owner], COALESCE( T2.[ContagemResultado_ContadorFM], 0) AS ContagemResultado_ContadorFM FROM ([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN (SELECT MIN([ContagemResultado_ContadorFMCod]) AS ContagemResultado_ContadorFM, [ContagemResultado_Codigo] FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T2 ON T2.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) WHERE T1.[ContagemResultado_Codigo] = @AV18Contagemresultado_Codigo ORDER BY T1.[ContagemResultado_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP004I12,1,0,false,true )
             ,new CursorDef("P004I13", "INSERT INTO [ContagemResultadoErro]([ContagemResultado_Codigo], [ContagemResultadoErro_Tipo], [ContagemResultadoErro_Data], [ContagemResultadoErro_Status], [ContagemResultadoErro_ContadorFMCod]) VALUES(@ContagemResultado_Codigo, @ContagemResultadoErro_Tipo, @ContagemResultadoErro_Data, @ContagemResultadoErro_Status, @ContagemResultadoErro_ContadorFMCod)", GxErrorMask.GX_NOMASK,prmP004I13)
             ,new CursorDef("P004I14", "SELECT TOP 1 [ContagemResultadoErro_Tipo], [ContagemResultado_Codigo], [ContagemResultadoErro_Status] FROM [ContagemResultadoErro] WITH (UPDLOCK) WHERE [ContagemResultado_Codigo] = @AV18Contagemresultado_Codigo and [ContagemResultadoErro_Tipo] = @AV38ContagemResultadoErro_Tipo ORDER BY [ContagemResultado_Codigo], [ContagemResultadoErro_Tipo] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP004I14,1,0,true,true )
             ,new CursorDef("P004I15", "UPDATE [ContagemResultadoErro] SET [ContagemResultadoErro_Status]=@ContagemResultadoErro_Status  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo AND [ContagemResultadoErro_Tipo] = @ContagemResultadoErro_Tipo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP004I15)
             ,new CursorDef("P004I16", "UPDATE [ContagemResultadoErro] SET [ContagemResultadoErro_Status]=@ContagemResultadoErro_Status  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo AND [ContagemResultadoErro_Tipo] = @ContagemResultadoErro_Tipo", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP004I16)
             ,new CursorDef("P004I17", "SELECT T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T2.[Servico_Codigo] AS ContagemResultado_Servico, T1.[ContagemResultado_StatusDmn], T1.[ContagemResultado_Codigo], T3.[Servico_Sigla] AS ContagemResultado_ServicoSigla FROM (([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod]) LEFT JOIN [Servico] T3 WITH (NOLOCK) ON T3.[Servico_Codigo] = T2.[Servico_Codigo]) WHERE (T1.[ContagemResultado_Codigo] = @AV91ContagemResultado_OSVinculada) AND (T1.[ContagemResultado_StatusDmn] = 'A') ORDER BY T1.[ContagemResultado_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP004I17,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 100) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                ((String[]) buf[7])[0] = rslt.getString(5, 1) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((int[]) buf[9])[0] = rslt.getInt(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((String[]) buf[11])[0] = rslt.getVarchar(7) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((String[]) buf[13])[0] = rslt.getVarchar(8) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(8);
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.getBool(2) ;
                ((DateTime[]) buf[2])[0] = rslt.getGXDate(3) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 5) ;
                ((String[]) buf[4])[0] = rslt.getLongVarchar(5) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(5);
                ((decimal[]) buf[6])[0] = rslt.getDecimal(6) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(6);
                ((decimal[]) buf[8])[0] = rslt.getDecimal(7) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(7);
                ((decimal[]) buf[10])[0] = rslt.getDecimal(8) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(8);
                ((decimal[]) buf[12])[0] = rslt.getDecimal(9) ;
                ((bool[]) buf[13])[0] = rslt.wasNull(9);
                ((int[]) buf[14])[0] = rslt.getInt(10) ;
                ((decimal[]) buf[15])[0] = rslt.getDecimal(11) ;
                ((int[]) buf[16])[0] = rslt.getInt(12) ;
                ((bool[]) buf[17])[0] = rslt.wasNull(12);
                ((short[]) buf[18])[0] = rslt.getShort(13) ;
                ((short[]) buf[19])[0] = rslt.getShort(14) ;
                return;
             case 6 :
                ((String[]) buf[0])[0] = rslt.getString(1, 2) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((DateTime[]) buf[2])[0] = rslt.getGXDateTime(3) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 1) ;
                return;
             case 9 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                return;
             case 11 :
                ((String[]) buf[0])[0] = rslt.getString(1, 2) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 1) ;
                return;
             case 14 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((String[]) buf[4])[0] = rslt.getString(3, 1) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                ((String[]) buf[7])[0] = rslt.getString(5, 15) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (short)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (DateTime)parms[2]);
                stmt.SetParameter(4, (String)parms[3]);
                return;
             case 4 :
                stmt.SetParameter(1, (short)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (DateTime)parms[2]);
                stmt.SetParameter(4, (String)parms[3]);
                return;
             case 5 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(1, (String)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                return;
             case 7 :
                stmt.SetParameterDatetime(1, (DateTime)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                stmt.SetParameter(4, (String)parms[3]);
                return;
             case 8 :
                stmt.SetParameterDatetime(1, (DateTime)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                stmt.SetParameter(4, (String)parms[3]);
                return;
             case 9 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 10 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                stmt.SetParameterDatetime(3, (DateTime)parms[2]);
                stmt.SetParameter(4, (String)parms[3]);
                stmt.SetParameter(5, (int)parms[4]);
                return;
             case 11 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                return;
             case 12 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (String)parms[2]);
                return;
             case 13 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (String)parms[2]);
                return;
             case 14 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
