/*
               File: type_SdtLoteArquivoAnexo
        Description: Arquivos Anexos
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/18/2020 12:51:41.59
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "LoteArquivoAnexo" )]
   [XmlType(TypeName =  "LoteArquivoAnexo" , Namespace = "GxEv3Up14_MeetrikaVs3" )]
   [Serializable]
   public class SdtLoteArquivoAnexo : GxSilentTrnSdt, System.Web.SessionState.IRequiresSessionState
   {
      public SdtLoteArquivoAnexo( )
      {
         /* Constructor for serialization */
         gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_data = (DateTime)(DateTime.MinValue);
         gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_arquivo = "";
         gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_nomearq = "";
         gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_tipoarq = "";
         gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_descricao = "";
         gxTv_SdtLoteArquivoAnexo_Tipodocumento_nome = "";
         gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_verificador = "";
         gxTv_SdtLoteArquivoAnexo_Mode = "";
         gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_data_Z = (DateTime)(DateTime.MinValue);
         gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_nomearq_Z = "";
         gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_tipoarq_Z = "";
         gxTv_SdtLoteArquivoAnexo_Tipodocumento_nome_Z = "";
         gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_verificador_Z = "";
      }

      public SdtLoteArquivoAnexo( IGxContext context )
      {
         this.context = context;
         constructorCallingAssembly = Assembly.GetCallingAssembly();
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public void Load( int AV841LoteArquivoAnexo_LoteCod ,
                        DateTime AV836LoteArquivoAnexo_Data )
      {
         IGxSilentTrn obj ;
         obj = getTransaction();
         obj.LoadKey(new Object[] {(int)AV841LoteArquivoAnexo_LoteCod,(DateTime)AV836LoteArquivoAnexo_Data});
         return  ;
      }

      public override Object[][] GetBCKey( )
      {
         return (Object[][])(new Object[][]{new Object[]{"LoteArquivoAnexo_LoteCod", typeof(int)}, new Object[]{"LoteArquivoAnexo_Data", typeof(DateTime)}}) ;
      }

      public override IGxCollection GetMessages( )
      {
         short item = 1 ;
         IGxCollection msgs = new GxObjectCollection( context, "Messages.Message", "Genexus", "SdtMessages_Message", "GeneXus.Programs") ;
         SdtMessages_Message m1 ;
         IGxSilentTrn trn = getTransaction() ;
         msglist msgList = trn.GetMessages() ;
         while ( item <= msgList.ItemCount )
         {
            m1 = new SdtMessages_Message(context);
            m1.gxTpr_Id = msgList.getItemValue(item);
            m1.gxTpr_Description = msgList.getItemText(item);
            m1.gxTpr_Type = msgList.getItemType(item);
            msgs.Add(m1, 0);
            item = (short)(item+1);
         }
         return msgs ;
      }

      public override GXProperties GetMetadata( )
      {
         GXProperties metadata = new GXProperties() ;
         metadata.Set("Name", "LoteArquivoAnexo");
         metadata.Set("BT", "LoteArquivoAnexo");
         metadata.Set("PK", "[ \"LoteArquivoAnexo_LoteCod\",\"LoteArquivoAnexo_Data\" ]");
         metadata.Set("PKAssigned", "[ \"LoteArquivoAnexo_Data\",\"LoteArquivoAnexo_LoteCod\" ]");
         metadata.Set("FKList", "[ { \"FK\":[ \"Lote_Codigo\" ],\"FKMap\":[ \"LoteArquivoAnexo_LoteCod-Lote_Codigo\" ] },{ \"FK\":[ \"TipoDocumento_Codigo\" ],\"FKMap\":[  ] } ]");
         metadata.Set("AllowInsert", "True");
         metadata.Set("AllowUpdate", "True");
         metadata.Set("AllowDelete", "True");
         return metadata ;
      }

      public override String ToXml( bool includeHeader ,
                                    bool includeState ,
                                    String name ,
                                    String sNameSpace )
      {
         String s ;
         XmlSerializer xmls ;
         if ( ! includeState )
         {
            XmlAttributeOverrides ov = new XmlAttributeOverrides();
            XmlAttributes attrs = new XmlAttributes();
            attrs.XmlIgnore = true;
            ov.Add(this.GetType(),  "gxTpr_Mode" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Initialized" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Lotearquivoanexo_lotecod_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Lotearquivoanexo_data_Z_Nullable" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Lotearquivoanexo_nomearq_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Lotearquivoanexo_tipoarq_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Tipodocumento_codigo_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Tipodocumento_nome_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Lotearquivoanexo_verificador_Z" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Lotearquivoanexo_arquivo_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Lotearquivoanexo_nomearq_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Lotearquivoanexo_tipoarq_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Lotearquivoanexo_descricao_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Tipodocumento_codigo_N" , attrs);
            ov.Add(this.GetType(),  "gxTpr_Lotearquivoanexo_verificador_N" , attrs);
            xmls = new XmlSerializer(this.GetType(), ov, new Type[0], null, sNameSpace);
         }
         else
         {
            xmls = new XmlSerializer(this.GetType(), sNameSpace);
         }
         System.IO.MemoryStream stream = new System.IO.MemoryStream();
         System.Xml.XmlWriter xmlw = System.Xml.XmlWriter.Create(stream, new System.Xml.XmlWriterSettings() { OmitXmlDeclaration = !includeHeader});
         XmlSerializerNamespaces xmlns = new XmlSerializerNamespaces();
         xmlns.Add(String.Empty, sNameSpace);
         xmls.Serialize( xmlw, this, xmlns);
         stream.Seek(0L, System.IO.SeekOrigin.Begin);
         System.IO.StreamReader sr = new System.IO.StreamReader(stream);
         s = sr.ReadToEnd();
         stream.Close();
         return s ;
      }

      public override void FromXml( String s ,
                                    String name )
      {
         SdtLoteArquivoAnexo deserialized ;
         if (s.Trim().Length == 0)
            return;
         XmlSerializer xmls = new XmlSerializer(this.GetType(),  "GxEv3Up14_MeetrikaVs3" );
         System.IO.StringReader sr = new System.IO.StringReader(s);
         deserialized = (SdtLoteArquivoAnexo)(xmls.Deserialize( sr)) ;
         sr.Close();
         SdtLoteArquivoAnexo obj ;
         obj = this;
         obj.gxTpr_Lotearquivoanexo_lotecod = deserialized.gxTpr_Lotearquivoanexo_lotecod;
         obj.gxTpr_Lotearquivoanexo_data = deserialized.gxTpr_Lotearquivoanexo_data;
         obj.gxTpr_Lotearquivoanexo_arquivo = deserialized.gxTpr_Lotearquivoanexo_arquivo;
         obj.gxTpr_Lotearquivoanexo_nomearq = deserialized.gxTpr_Lotearquivoanexo_nomearq;
         obj.gxTpr_Lotearquivoanexo_tipoarq = deserialized.gxTpr_Lotearquivoanexo_tipoarq;
         obj.gxTpr_Lotearquivoanexo_descricao = deserialized.gxTpr_Lotearquivoanexo_descricao;
         obj.gxTpr_Tipodocumento_codigo = deserialized.gxTpr_Tipodocumento_codigo;
         obj.gxTpr_Tipodocumento_nome = deserialized.gxTpr_Tipodocumento_nome;
         obj.gxTpr_Lotearquivoanexo_verificador = deserialized.gxTpr_Lotearquivoanexo_verificador;
         obj.gxTpr_Mode = deserialized.gxTpr_Mode;
         obj.gxTpr_Initialized = deserialized.gxTpr_Initialized;
         obj.gxTpr_Lotearquivoanexo_lotecod_Z = deserialized.gxTpr_Lotearquivoanexo_lotecod_Z;
         obj.gxTpr_Lotearquivoanexo_data_Z = deserialized.gxTpr_Lotearquivoanexo_data_Z;
         obj.gxTpr_Lotearquivoanexo_nomearq_Z = deserialized.gxTpr_Lotearquivoanexo_nomearq_Z;
         obj.gxTpr_Lotearquivoanexo_tipoarq_Z = deserialized.gxTpr_Lotearquivoanexo_tipoarq_Z;
         obj.gxTpr_Tipodocumento_codigo_Z = deserialized.gxTpr_Tipodocumento_codigo_Z;
         obj.gxTpr_Tipodocumento_nome_Z = deserialized.gxTpr_Tipodocumento_nome_Z;
         obj.gxTpr_Lotearquivoanexo_verificador_Z = deserialized.gxTpr_Lotearquivoanexo_verificador_Z;
         obj.gxTpr_Lotearquivoanexo_arquivo_N = deserialized.gxTpr_Lotearquivoanexo_arquivo_N;
         obj.gxTpr_Lotearquivoanexo_nomearq_N = deserialized.gxTpr_Lotearquivoanexo_nomearq_N;
         obj.gxTpr_Lotearquivoanexo_tipoarq_N = deserialized.gxTpr_Lotearquivoanexo_tipoarq_N;
         obj.gxTpr_Lotearquivoanexo_descricao_N = deserialized.gxTpr_Lotearquivoanexo_descricao_N;
         obj.gxTpr_Tipodocumento_codigo_N = deserialized.gxTpr_Tipodocumento_codigo_N;
         obj.gxTpr_Lotearquivoanexo_verificador_N = deserialized.gxTpr_Lotearquivoanexo_verificador_N;
         return  ;
      }

      public override short readxml( GXXMLReader oReader ,
                                     String sName )
      {
         short GXSoapError = 1 ;
         sTagName = oReader.Name;
         if ( oReader.IsSimple == 0 )
         {
            GXSoapError = oReader.Read();
            nOutParmCount = 0;
            while ( ( ( StringUtil.StrCmp(oReader.Name, sTagName) != 0 ) || ( oReader.NodeType == 1 ) ) && ( GXSoapError > 0 ) )
            {
               readOk = 0;
               if ( StringUtil.StrCmp2( oReader.LocalName, "LoteArquivoAnexo_LoteCod") )
               {
                  gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_lotecod = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "LoteArquivoAnexo_Data") )
               {
                  if ( ( StringUtil.StrCmp(oReader.Value, "0000-00-00T00:00:00") == 0 ) || ( oReader.ExistsAttribute("xsi:nil") == 1 ) )
                  {
                     gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_data = (DateTime)(DateTime.MinValue);
                  }
                  else
                  {
                     gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_data = context.localUtil.YMDHMSToT( (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 1, 4), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 6, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 9, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 12, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 15, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 18, 2), ".")));
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "LoteArquivoAnexo_Arquivo") )
               {
                  gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_arquivo=context.FileFromBase64( oReader.Value) ;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "LoteArquivoAnexo_NomeArq") )
               {
                  gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_nomearq = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "LoteArquivoAnexo_TipoArq") )
               {
                  gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_tipoarq = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "LoteArquivoAnexo_Descricao") )
               {
                  gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_descricao = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "TipoDocumento_Codigo") )
               {
                  gxTv_SdtLoteArquivoAnexo_Tipodocumento_codigo = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "TipoDocumento_Nome") )
               {
                  gxTv_SdtLoteArquivoAnexo_Tipodocumento_nome = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "LoteArquivoAnexo_Verificador") )
               {
                  gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_verificador = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Mode") )
               {
                  gxTv_SdtLoteArquivoAnexo_Mode = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "Initialized") )
               {
                  gxTv_SdtLoteArquivoAnexo_Initialized = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "LoteArquivoAnexo_LoteCod_Z") )
               {
                  gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_lotecod_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "LoteArquivoAnexo_Data_Z") )
               {
                  if ( ( StringUtil.StrCmp(oReader.Value, "0000-00-00T00:00:00") == 0 ) || ( oReader.ExistsAttribute("xsi:nil") == 1 ) )
                  {
                     gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_data_Z = (DateTime)(DateTime.MinValue);
                  }
                  else
                  {
                     gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_data_Z = context.localUtil.YMDHMSToT( (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 1, 4), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 6, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 9, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 12, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 15, 2), ".")), (short)(NumberUtil.Val( StringUtil.Substring( oReader.Value, 18, 2), ".")));
                  }
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "LoteArquivoAnexo_NomeArq_Z") )
               {
                  gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_nomearq_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "LoteArquivoAnexo_TipoArq_Z") )
               {
                  gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_tipoarq_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "TipoDocumento_Codigo_Z") )
               {
                  gxTv_SdtLoteArquivoAnexo_Tipodocumento_codigo_Z = (int)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "TipoDocumento_Nome_Z") )
               {
                  gxTv_SdtLoteArquivoAnexo_Tipodocumento_nome_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "LoteArquivoAnexo_Verificador_Z") )
               {
                  gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_verificador_Z = oReader.Value;
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "LoteArquivoAnexo_Arquivo_N") )
               {
                  gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_arquivo_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "LoteArquivoAnexo_NomeArq_N") )
               {
                  gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_nomearq_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "LoteArquivoAnexo_TipoArq_N") )
               {
                  gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_tipoarq_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "LoteArquivoAnexo_Descricao_N") )
               {
                  gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_descricao_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "TipoDocumento_Codigo_N") )
               {
                  gxTv_SdtLoteArquivoAnexo_Tipodocumento_codigo_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               if ( StringUtil.StrCmp2( oReader.LocalName, "LoteArquivoAnexo_Verificador_N") )
               {
                  gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_verificador_N = (short)(NumberUtil.Val( oReader.Value, "."));
                  if ( GXSoapError > 0 )
                  {
                     readOk = 1;
                  }
                  GXSoapError = oReader.Read();
               }
               nOutParmCount = (short)(nOutParmCount+1);
               if ( readOk == 0 )
               {
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Error reading " + sTagName + StringUtil.NewLine( );
                  context.sSOAPErrMsg = context.sSOAPErrMsg + "Message: " + oReader.ReadRawXML();
                  GXSoapError = (short)(nOutParmCount*-1);
               }
            }
         }
         return GXSoapError ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace )
      {
         writexml(oWriter, sName, sNameSpace, true);
         return  ;
      }

      public override void writexml( GXXMLWriter oWriter ,
                                     String sName ,
                                     String sNameSpace ,
                                     bool sIncludeState )
      {
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sName)) )
         {
            sName = "LoteArquivoAnexo";
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( sNameSpace)) )
         {
            sNameSpace = "GxEv3Up14_MeetrikaVs3";
         }
         oWriter.WriteStartElement(sName);
         if ( StringUtil.StrCmp(StringUtil.Left( sNameSpace, 10), "[*:nosend]") != 0 )
         {
            oWriter.WriteAttribute("xmlns", sNameSpace);
         }
         else
         {
            sNameSpace = StringUtil.Right( sNameSpace, (short)(StringUtil.Len( sNameSpace)-10));
         }
         oWriter.WriteElement("LoteArquivoAnexo_LoteCod", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_lotecod), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         if ( (DateTime.MinValue==gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_data) )
         {
            oWriter.WriteStartElement("LoteArquivoAnexo_Data");
            oWriter.WriteAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
            oWriter.WriteAttribute("xsi:nil", "true");
            oWriter.WriteEndElement();
         }
         else
         {
            sDateCnv = "";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_data)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_data)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_data)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "T";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Hour( gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_data)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + ":";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Minute( gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_data)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + ":";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Second( gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_data)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            oWriter.WriteElement("LoteArquivoAnexo_Data", sDateCnv);
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
         }
         oWriter.WriteElement("LoteArquivoAnexo_Arquivo", context.FileToBase64( gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_arquivo));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("LoteArquivoAnexo_NomeArq", StringUtil.RTrim( gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_nomearq));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("LoteArquivoAnexo_TipoArq", StringUtil.RTrim( gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_tipoarq));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("LoteArquivoAnexo_Descricao", StringUtil.RTrim( gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_descricao));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("TipoDocumento_Codigo", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtLoteArquivoAnexo_Tipodocumento_codigo), 6, 0)));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("TipoDocumento_Nome", StringUtil.RTrim( gxTv_SdtLoteArquivoAnexo_Tipodocumento_nome));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         oWriter.WriteElement("LoteArquivoAnexo_Verificador", StringUtil.RTrim( gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_verificador));
         if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
         {
            oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
         }
         if ( sIncludeState )
         {
            oWriter.WriteElement("Mode", StringUtil.RTrim( gxTv_SdtLoteArquivoAnexo_Mode));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("Initialized", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtLoteArquivoAnexo_Initialized), 4, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("LoteArquivoAnexo_LoteCod_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_lotecod_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            if ( (DateTime.MinValue==gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_data_Z) )
            {
               oWriter.WriteStartElement("LoteArquivoAnexo_Data_Z");
               oWriter.WriteAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
               oWriter.WriteAttribute("xsi:nil", "true");
               oWriter.WriteEndElement();
            }
            else
            {
               sDateCnv = "";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_data_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + "-";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_data_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + "-";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_data_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + "T";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Hour( gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_data_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + ":";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Minute( gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_data_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               sDateCnv = sDateCnv + ":";
               sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Second( gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_data_Z)), 10, 0));
               sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
               oWriter.WriteElement("LoteArquivoAnexo_Data_Z", sDateCnv);
               if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
               {
                  oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
               }
            }
            oWriter.WriteElement("LoteArquivoAnexo_NomeArq_Z", StringUtil.RTrim( gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_nomearq_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("LoteArquivoAnexo_TipoArq_Z", StringUtil.RTrim( gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_tipoarq_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("TipoDocumento_Codigo_Z", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtLoteArquivoAnexo_Tipodocumento_codigo_Z), 6, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("TipoDocumento_Nome_Z", StringUtil.RTrim( gxTv_SdtLoteArquivoAnexo_Tipodocumento_nome_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("LoteArquivoAnexo_Verificador_Z", StringUtil.RTrim( gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_verificador_Z));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("LoteArquivoAnexo_Arquivo_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_arquivo_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("LoteArquivoAnexo_NomeArq_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_nomearq_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("LoteArquivoAnexo_TipoArq_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_tipoarq_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("LoteArquivoAnexo_Descricao_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_descricao_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("TipoDocumento_Codigo_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtLoteArquivoAnexo_Tipodocumento_codigo_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
            oWriter.WriteElement("LoteArquivoAnexo_Verificador_N", StringUtil.Trim( StringUtil.Str( (decimal)(gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_verificador_N), 1, 0)));
            if ( StringUtil.StrCmp(sNameSpace, "GxEv3Up14_MeetrikaVs3") != 0 )
            {
               oWriter.WriteAttribute("xmlns", "GxEv3Up14_MeetrikaVs3");
            }
         }
         oWriter.WriteEndElement();
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("LoteArquivoAnexo_LoteCod", gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_lotecod, false);
         datetime_STZ = gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_data;
         sDateCnv = "";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "T";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Hour( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + ":";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Minute( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + ":";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Second( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         AddObjectProperty("LoteArquivoAnexo_Data", sDateCnv, false);
         AddObjectProperty("LoteArquivoAnexo_Arquivo", gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_arquivo, false);
         AddObjectProperty("LoteArquivoAnexo_NomeArq", gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_nomearq, false);
         AddObjectProperty("LoteArquivoAnexo_TipoArq", gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_tipoarq, false);
         AddObjectProperty("LoteArquivoAnexo_Descricao", gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_descricao, false);
         AddObjectProperty("TipoDocumento_Codigo", gxTv_SdtLoteArquivoAnexo_Tipodocumento_codigo, false);
         AddObjectProperty("TipoDocumento_Nome", gxTv_SdtLoteArquivoAnexo_Tipodocumento_nome, false);
         AddObjectProperty("LoteArquivoAnexo_Verificador", gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_verificador, false);
         if ( includeState )
         {
            AddObjectProperty("Mode", gxTv_SdtLoteArquivoAnexo_Mode, false);
            AddObjectProperty("Initialized", gxTv_SdtLoteArquivoAnexo_Initialized, false);
            AddObjectProperty("LoteArquivoAnexo_LoteCod_Z", gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_lotecod_Z, false);
            datetime_STZ = gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_data_Z;
            sDateCnv = "";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( datetime_STZ)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( datetime_STZ)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( datetime_STZ)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "T";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Hour( datetime_STZ)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + ":";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Minute( datetime_STZ)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + ":";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Second( datetime_STZ)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            AddObjectProperty("LoteArquivoAnexo_Data_Z", sDateCnv, false);
            AddObjectProperty("LoteArquivoAnexo_NomeArq_Z", gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_nomearq_Z, false);
            AddObjectProperty("LoteArquivoAnexo_TipoArq_Z", gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_tipoarq_Z, false);
            AddObjectProperty("TipoDocumento_Codigo_Z", gxTv_SdtLoteArquivoAnexo_Tipodocumento_codigo_Z, false);
            AddObjectProperty("TipoDocumento_Nome_Z", gxTv_SdtLoteArquivoAnexo_Tipodocumento_nome_Z, false);
            AddObjectProperty("LoteArquivoAnexo_Verificador_Z", gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_verificador_Z, false);
            AddObjectProperty("LoteArquivoAnexo_Arquivo_N", gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_arquivo_N, false);
            AddObjectProperty("LoteArquivoAnexo_NomeArq_N", gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_nomearq_N, false);
            AddObjectProperty("LoteArquivoAnexo_TipoArq_N", gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_tipoarq_N, false);
            AddObjectProperty("LoteArquivoAnexo_Descricao_N", gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_descricao_N, false);
            AddObjectProperty("TipoDocumento_Codigo_N", gxTv_SdtLoteArquivoAnexo_Tipodocumento_codigo_N, false);
            AddObjectProperty("LoteArquivoAnexo_Verificador_N", gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_verificador_N, false);
         }
         return  ;
      }

      [  SoapElement( ElementName = "LoteArquivoAnexo_LoteCod" )]
      [  XmlElement( ElementName = "LoteArquivoAnexo_LoteCod"   )]
      public int gxTpr_Lotearquivoanexo_lotecod
      {
         get {
            return gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_lotecod ;
         }

         set {
            if ( gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_lotecod != value )
            {
               gxTv_SdtLoteArquivoAnexo_Mode = "INS";
               this.gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_lotecod_Z_SetNull( );
               this.gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_data_Z_SetNull( );
               this.gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_nomearq_Z_SetNull( );
               this.gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_tipoarq_Z_SetNull( );
               this.gxTv_SdtLoteArquivoAnexo_Tipodocumento_codigo_Z_SetNull( );
               this.gxTv_SdtLoteArquivoAnexo_Tipodocumento_nome_Z_SetNull( );
               this.gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_verificador_Z_SetNull( );
            }
            gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_lotecod = (int)(value);
         }

      }

      [  SoapElement( ElementName = "LoteArquivoAnexo_Data" )]
      [  XmlElement( ElementName = "LoteArquivoAnexo_Data"  , IsNullable=true )]
      public string gxTpr_Lotearquivoanexo_data_Nullable
      {
         get {
            if ( gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_data == DateTime.MinValue)
               return null;
            return new GxDatetimeString(gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_data).value ;
         }

         set {
            if (value == null || value == GxDatetimeString.NullValue )
               gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_data = DateTime.MinValue;
            else
               gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_data = DateTime.Parse( value);
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public DateTime gxTpr_Lotearquivoanexo_data
      {
         get {
            return gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_data ;
         }

         set {
            if ( gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_data != value )
            {
               gxTv_SdtLoteArquivoAnexo_Mode = "INS";
               this.gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_lotecod_Z_SetNull( );
               this.gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_data_Z_SetNull( );
               this.gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_nomearq_Z_SetNull( );
               this.gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_tipoarq_Z_SetNull( );
               this.gxTv_SdtLoteArquivoAnexo_Tipodocumento_codigo_Z_SetNull( );
               this.gxTv_SdtLoteArquivoAnexo_Tipodocumento_nome_Z_SetNull( );
               this.gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_verificador_Z_SetNull( );
            }
            gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_data = (DateTime)(value);
         }

      }

      [  SoapElement( ElementName = "LoteArquivoAnexo_Arquivo" )]
      [  XmlElement( ElementName = "LoteArquivoAnexo_Arquivo"   )]
      [GxUpload()]
      public byte[] gxTpr_Lotearquivoanexo_arquivo_Blob
      {
         get {
            IGxContext context = this.context == null ? new GxContext() : this.context;
            return context.FileToByteArray( gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_arquivo) ;
         }

         set {
            gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_arquivo_N = 0;
            IGxContext context = this.context == null ? new GxContext() : this.context;
            gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_arquivo=context.FileFromByteArray( value) ;
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      [GxUpload()]
      public String gxTpr_Lotearquivoanexo_arquivo
      {
         get {
            return gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_arquivo ;
         }

         set {
            gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_arquivo_N = 0;
            gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_arquivo = value;
         }

      }

      public void gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_arquivo_SetBlob( String blob ,
                                                                             String fileName ,
                                                                             String fileType )
      {
         gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_arquivo = blob;
         gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_nomearq = fileName;
         gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_tipoarq = fileType;
         return  ;
      }

      public void gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_arquivo_SetNull( )
      {
         gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_arquivo_N = 1;
         gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_arquivo = "";
         return  ;
      }

      public bool gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_arquivo_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "LoteArquivoAnexo_NomeArq" )]
      [  XmlElement( ElementName = "LoteArquivoAnexo_NomeArq"   )]
      public String gxTpr_Lotearquivoanexo_nomearq
      {
         get {
            return gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_nomearq ;
         }

         set {
            gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_nomearq_N = 0;
            gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_nomearq = (String)(value);
         }

      }

      public void gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_nomearq_SetNull( )
      {
         gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_nomearq_N = 1;
         gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_nomearq = "";
         return  ;
      }

      public bool gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_nomearq_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "LoteArquivoAnexo_TipoArq" )]
      [  XmlElement( ElementName = "LoteArquivoAnexo_TipoArq"   )]
      public String gxTpr_Lotearquivoanexo_tipoarq
      {
         get {
            return gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_tipoarq ;
         }

         set {
            gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_tipoarq_N = 0;
            gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_tipoarq = (String)(value);
         }

      }

      public void gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_tipoarq_SetNull( )
      {
         gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_tipoarq_N = 1;
         gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_tipoarq = "";
         return  ;
      }

      public bool gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_tipoarq_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "LoteArquivoAnexo_Descricao" )]
      [  XmlElement( ElementName = "LoteArquivoAnexo_Descricao"   )]
      public String gxTpr_Lotearquivoanexo_descricao
      {
         get {
            return gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_descricao ;
         }

         set {
            gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_descricao_N = 0;
            gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_descricao = (String)(value);
         }

      }

      public void gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_descricao_SetNull( )
      {
         gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_descricao_N = 1;
         gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_descricao = "";
         return  ;
      }

      public bool gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_descricao_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "TipoDocumento_Codigo" )]
      [  XmlElement( ElementName = "TipoDocumento_Codigo"   )]
      public int gxTpr_Tipodocumento_codigo
      {
         get {
            return gxTv_SdtLoteArquivoAnexo_Tipodocumento_codigo ;
         }

         set {
            gxTv_SdtLoteArquivoAnexo_Tipodocumento_codigo_N = 0;
            gxTv_SdtLoteArquivoAnexo_Tipodocumento_codigo = (int)(value);
         }

      }

      public void gxTv_SdtLoteArquivoAnexo_Tipodocumento_codigo_SetNull( )
      {
         gxTv_SdtLoteArquivoAnexo_Tipodocumento_codigo_N = 1;
         gxTv_SdtLoteArquivoAnexo_Tipodocumento_codigo = 0;
         return  ;
      }

      public bool gxTv_SdtLoteArquivoAnexo_Tipodocumento_codigo_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "TipoDocumento_Nome" )]
      [  XmlElement( ElementName = "TipoDocumento_Nome"   )]
      public String gxTpr_Tipodocumento_nome
      {
         get {
            return gxTv_SdtLoteArquivoAnexo_Tipodocumento_nome ;
         }

         set {
            gxTv_SdtLoteArquivoAnexo_Tipodocumento_nome = (String)(value);
         }

      }

      [  SoapElement( ElementName = "LoteArquivoAnexo_Verificador" )]
      [  XmlElement( ElementName = "LoteArquivoAnexo_Verificador"   )]
      public String gxTpr_Lotearquivoanexo_verificador
      {
         get {
            return gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_verificador ;
         }

         set {
            gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_verificador_N = 0;
            gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_verificador = (String)(value);
         }

      }

      public void gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_verificador_SetNull( )
      {
         gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_verificador_N = 1;
         gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_verificador = "";
         return  ;
      }

      public bool gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_verificador_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Mode" )]
      [  XmlElement( ElementName = "Mode"   )]
      public String gxTpr_Mode
      {
         get {
            return gxTv_SdtLoteArquivoAnexo_Mode ;
         }

         set {
            gxTv_SdtLoteArquivoAnexo_Mode = (String)(value);
         }

      }

      public void gxTv_SdtLoteArquivoAnexo_Mode_SetNull( )
      {
         gxTv_SdtLoteArquivoAnexo_Mode = "";
         return  ;
      }

      public bool gxTv_SdtLoteArquivoAnexo_Mode_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Initialized" )]
      [  XmlElement( ElementName = "Initialized"   )]
      public short gxTpr_Initialized
      {
         get {
            return gxTv_SdtLoteArquivoAnexo_Initialized ;
         }

         set {
            gxTv_SdtLoteArquivoAnexo_Initialized = (short)(value);
         }

      }

      public void gxTv_SdtLoteArquivoAnexo_Initialized_SetNull( )
      {
         gxTv_SdtLoteArquivoAnexo_Initialized = 0;
         return  ;
      }

      public bool gxTv_SdtLoteArquivoAnexo_Initialized_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "LoteArquivoAnexo_LoteCod_Z" )]
      [  XmlElement( ElementName = "LoteArquivoAnexo_LoteCod_Z"   )]
      public int gxTpr_Lotearquivoanexo_lotecod_Z
      {
         get {
            return gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_lotecod_Z ;
         }

         set {
            gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_lotecod_Z = (int)(value);
         }

      }

      public void gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_lotecod_Z_SetNull( )
      {
         gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_lotecod_Z = 0;
         return  ;
      }

      public bool gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_lotecod_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "LoteArquivoAnexo_Data_Z" )]
      [  XmlElement( ElementName = "LoteArquivoAnexo_Data_Z"  , IsNullable=true )]
      public string gxTpr_Lotearquivoanexo_data_Z_Nullable
      {
         get {
            if ( gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_data_Z == DateTime.MinValue)
               return null;
            return new GxDatetimeString(gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_data_Z).value ;
         }

         set {
            if (value == null || value == GxDatetimeString.NullValue )
               gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_data_Z = DateTime.MinValue;
            else
               gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_data_Z = DateTime.Parse( value);
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public DateTime gxTpr_Lotearquivoanexo_data_Z
      {
         get {
            return gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_data_Z ;
         }

         set {
            gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_data_Z = (DateTime)(value);
         }

      }

      public void gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_data_Z_SetNull( )
      {
         gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_data_Z = (DateTime)(DateTime.MinValue);
         return  ;
      }

      public bool gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_data_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "LoteArquivoAnexo_NomeArq_Z" )]
      [  XmlElement( ElementName = "LoteArquivoAnexo_NomeArq_Z"   )]
      public String gxTpr_Lotearquivoanexo_nomearq_Z
      {
         get {
            return gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_nomearq_Z ;
         }

         set {
            gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_nomearq_Z = (String)(value);
         }

      }

      public void gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_nomearq_Z_SetNull( )
      {
         gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_nomearq_Z = "";
         return  ;
      }

      public bool gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_nomearq_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "LoteArquivoAnexo_TipoArq_Z" )]
      [  XmlElement( ElementName = "LoteArquivoAnexo_TipoArq_Z"   )]
      public String gxTpr_Lotearquivoanexo_tipoarq_Z
      {
         get {
            return gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_tipoarq_Z ;
         }

         set {
            gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_tipoarq_Z = (String)(value);
         }

      }

      public void gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_tipoarq_Z_SetNull( )
      {
         gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_tipoarq_Z = "";
         return  ;
      }

      public bool gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_tipoarq_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "TipoDocumento_Codigo_Z" )]
      [  XmlElement( ElementName = "TipoDocumento_Codigo_Z"   )]
      public int gxTpr_Tipodocumento_codigo_Z
      {
         get {
            return gxTv_SdtLoteArquivoAnexo_Tipodocumento_codigo_Z ;
         }

         set {
            gxTv_SdtLoteArquivoAnexo_Tipodocumento_codigo_Z = (int)(value);
         }

      }

      public void gxTv_SdtLoteArquivoAnexo_Tipodocumento_codigo_Z_SetNull( )
      {
         gxTv_SdtLoteArquivoAnexo_Tipodocumento_codigo_Z = 0;
         return  ;
      }

      public bool gxTv_SdtLoteArquivoAnexo_Tipodocumento_codigo_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "TipoDocumento_Nome_Z" )]
      [  XmlElement( ElementName = "TipoDocumento_Nome_Z"   )]
      public String gxTpr_Tipodocumento_nome_Z
      {
         get {
            return gxTv_SdtLoteArquivoAnexo_Tipodocumento_nome_Z ;
         }

         set {
            gxTv_SdtLoteArquivoAnexo_Tipodocumento_nome_Z = (String)(value);
         }

      }

      public void gxTv_SdtLoteArquivoAnexo_Tipodocumento_nome_Z_SetNull( )
      {
         gxTv_SdtLoteArquivoAnexo_Tipodocumento_nome_Z = "";
         return  ;
      }

      public bool gxTv_SdtLoteArquivoAnexo_Tipodocumento_nome_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "LoteArquivoAnexo_Verificador_Z" )]
      [  XmlElement( ElementName = "LoteArquivoAnexo_Verificador_Z"   )]
      public String gxTpr_Lotearquivoanexo_verificador_Z
      {
         get {
            return gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_verificador_Z ;
         }

         set {
            gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_verificador_Z = (String)(value);
         }

      }

      public void gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_verificador_Z_SetNull( )
      {
         gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_verificador_Z = "";
         return  ;
      }

      public bool gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_verificador_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "LoteArquivoAnexo_Arquivo_N" )]
      [  XmlElement( ElementName = "LoteArquivoAnexo_Arquivo_N"   )]
      public short gxTpr_Lotearquivoanexo_arquivo_N
      {
         get {
            return gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_arquivo_N ;
         }

         set {
            gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_arquivo_N = (short)(value);
         }

      }

      public void gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_arquivo_N_SetNull( )
      {
         gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_arquivo_N = 0;
         return  ;
      }

      public bool gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_arquivo_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "LoteArquivoAnexo_NomeArq_N" )]
      [  XmlElement( ElementName = "LoteArquivoAnexo_NomeArq_N"   )]
      public short gxTpr_Lotearquivoanexo_nomearq_N
      {
         get {
            return gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_nomearq_N ;
         }

         set {
            gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_nomearq_N = (short)(value);
         }

      }

      public void gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_nomearq_N_SetNull( )
      {
         gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_nomearq_N = 0;
         return  ;
      }

      public bool gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_nomearq_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "LoteArquivoAnexo_TipoArq_N" )]
      [  XmlElement( ElementName = "LoteArquivoAnexo_TipoArq_N"   )]
      public short gxTpr_Lotearquivoanexo_tipoarq_N
      {
         get {
            return gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_tipoarq_N ;
         }

         set {
            gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_tipoarq_N = (short)(value);
         }

      }

      public void gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_tipoarq_N_SetNull( )
      {
         gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_tipoarq_N = 0;
         return  ;
      }

      public bool gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_tipoarq_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "LoteArquivoAnexo_Descricao_N" )]
      [  XmlElement( ElementName = "LoteArquivoAnexo_Descricao_N"   )]
      public short gxTpr_Lotearquivoanexo_descricao_N
      {
         get {
            return gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_descricao_N ;
         }

         set {
            gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_descricao_N = (short)(value);
         }

      }

      public void gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_descricao_N_SetNull( )
      {
         gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_descricao_N = 0;
         return  ;
      }

      public bool gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_descricao_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "TipoDocumento_Codigo_N" )]
      [  XmlElement( ElementName = "TipoDocumento_Codigo_N"   )]
      public short gxTpr_Tipodocumento_codigo_N
      {
         get {
            return gxTv_SdtLoteArquivoAnexo_Tipodocumento_codigo_N ;
         }

         set {
            gxTv_SdtLoteArquivoAnexo_Tipodocumento_codigo_N = (short)(value);
         }

      }

      public void gxTv_SdtLoteArquivoAnexo_Tipodocumento_codigo_N_SetNull( )
      {
         gxTv_SdtLoteArquivoAnexo_Tipodocumento_codigo_N = 0;
         return  ;
      }

      public bool gxTv_SdtLoteArquivoAnexo_Tipodocumento_codigo_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "LoteArquivoAnexo_Verificador_N" )]
      [  XmlElement( ElementName = "LoteArquivoAnexo_Verificador_N"   )]
      public short gxTpr_Lotearquivoanexo_verificador_N
      {
         get {
            return gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_verificador_N ;
         }

         set {
            gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_verificador_N = (short)(value);
         }

      }

      public void gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_verificador_N_SetNull( )
      {
         gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_verificador_N = 0;
         return  ;
      }

      public bool gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_verificador_N_IsNull( )
      {
         return false ;
      }

      public void initialize( )
      {
         gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_data = DateTimeUtil.ServerNow( (IGxContext)(context), "DEFAULT");
         gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_arquivo = "";
         gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_nomearq = "";
         gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_tipoarq = "";
         gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_descricao = "";
         gxTv_SdtLoteArquivoAnexo_Tipodocumento_nome = "";
         gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_verificador = "";
         gxTv_SdtLoteArquivoAnexo_Mode = "";
         gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_data_Z = (DateTime)(DateTime.MinValue);
         gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_nomearq_Z = "";
         gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_tipoarq_Z = "";
         gxTv_SdtLoteArquivoAnexo_Tipodocumento_nome_Z = "";
         gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_verificador_Z = "";
         sTagName = "";
         sDateCnv = "";
         sNumToPad = "";
         datetime_STZ = (DateTime)(DateTime.MinValue);
         IGxSilentTrn obj ;
         obj = (IGxSilentTrn)ClassLoader.FindInstance( "lotearquivoanexo", "GeneXus.Programs.lotearquivoanexo_bc", new Object[] {context}, constructorCallingAssembly);
         obj.initialize();
         obj.SetSDT(this, 1);
         setTransaction( obj) ;
         obj.SetMode("INS");
         return  ;
      }

      private short gxTv_SdtLoteArquivoAnexo_Initialized ;
      private short gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_arquivo_N ;
      private short gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_nomearq_N ;
      private short gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_tipoarq_N ;
      private short gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_descricao_N ;
      private short gxTv_SdtLoteArquivoAnexo_Tipodocumento_codigo_N ;
      private short gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_verificador_N ;
      private short readOk ;
      private short nOutParmCount ;
      private int gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_lotecod ;
      private int gxTv_SdtLoteArquivoAnexo_Tipodocumento_codigo ;
      private int gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_lotecod_Z ;
      private int gxTv_SdtLoteArquivoAnexo_Tipodocumento_codigo_Z ;
      private String gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_nomearq ;
      private String gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_tipoarq ;
      private String gxTv_SdtLoteArquivoAnexo_Tipodocumento_nome ;
      private String gxTv_SdtLoteArquivoAnexo_Mode ;
      private String gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_nomearq_Z ;
      private String gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_tipoarq_Z ;
      private String gxTv_SdtLoteArquivoAnexo_Tipodocumento_nome_Z ;
      private String sTagName ;
      private String sDateCnv ;
      private String sNumToPad ;
      private DateTime gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_data ;
      private DateTime gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_data_Z ;
      private DateTime datetime_STZ ;
      private String gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_descricao ;
      private String gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_verificador ;
      private String gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_verificador_Z ;
      private String gxTv_SdtLoteArquivoAnexo_Lotearquivoanexo_arquivo ;
      private Assembly constructorCallingAssembly ;
   }

   [DataContract(Name = @"LoteArquivoAnexo", Namespace = "GxEv3Up14_MeetrikaVs3")]
   public class SdtLoteArquivoAnexo_RESTInterface : GxGenericCollectionItem<SdtLoteArquivoAnexo>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtLoteArquivoAnexo_RESTInterface( ) : base()
      {
      }

      public SdtLoteArquivoAnexo_RESTInterface( SdtLoteArquivoAnexo psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "LoteArquivoAnexo_LoteCod" , Order = 0 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Lotearquivoanexo_lotecod
      {
         get {
            return sdt.gxTpr_Lotearquivoanexo_lotecod ;
         }

         set {
            sdt.gxTpr_Lotearquivoanexo_lotecod = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "LoteArquivoAnexo_Data" , Order = 1 )]
      [GxSeudo()]
      public String gxTpr_Lotearquivoanexo_data
      {
         get {
            return DateTimeUtil.TToC2( sdt.gxTpr_Lotearquivoanexo_data) ;
         }

         set {
            sdt.gxTpr_Lotearquivoanexo_data = DateTimeUtil.CToT2( (String)(value));
         }

      }

      [DataMember( Name = "LoteArquivoAnexo_Arquivo" , Order = 2 )]
      [GxUpload()]
      public String gxTpr_Lotearquivoanexo_arquivo
      {
         get {
            return PathUtil.RelativePath( sdt.gxTpr_Lotearquivoanexo_arquivo) ;
         }

         set {
            sdt.gxTpr_Lotearquivoanexo_arquivo = value;
         }

      }

      [DataMember( Name = "LoteArquivoAnexo_NomeArq" , Order = 3 )]
      public String gxTpr_Lotearquivoanexo_nomearq
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Lotearquivoanexo_nomearq) ;
         }

         set {
            sdt.gxTpr_Lotearquivoanexo_nomearq = (String)(value);
         }

      }

      [DataMember( Name = "LoteArquivoAnexo_TipoArq" , Order = 4 )]
      public String gxTpr_Lotearquivoanexo_tipoarq
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Lotearquivoanexo_tipoarq) ;
         }

         set {
            sdt.gxTpr_Lotearquivoanexo_tipoarq = (String)(value);
         }

      }

      [DataMember( Name = "LoteArquivoAnexo_Descricao" , Order = 5 )]
      public String gxTpr_Lotearquivoanexo_descricao
      {
         get {
            return sdt.gxTpr_Lotearquivoanexo_descricao ;
         }

         set {
            sdt.gxTpr_Lotearquivoanexo_descricao = (String)(value);
         }

      }

      [DataMember( Name = "TipoDocumento_Codigo" , Order = 6 )]
      [GxSeudo()]
      public Nullable<int> gxTpr_Tipodocumento_codigo
      {
         get {
            return sdt.gxTpr_Tipodocumento_codigo ;
         }

         set {
            sdt.gxTpr_Tipodocumento_codigo = (int)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "TipoDocumento_Nome" , Order = 7 )]
      [GxSeudo()]
      public String gxTpr_Tipodocumento_nome
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Tipodocumento_nome) ;
         }

         set {
            sdt.gxTpr_Tipodocumento_nome = (String)(value);
         }

      }

      [DataMember( Name = "LoteArquivoAnexo_Verificador" , Order = 8 )]
      [GxSeudo()]
      public String gxTpr_Lotearquivoanexo_verificador
      {
         get {
            return sdt.gxTpr_Lotearquivoanexo_verificador ;
         }

         set {
            sdt.gxTpr_Lotearquivoanexo_verificador = (String)(value);
         }

      }

      public SdtLoteArquivoAnexo sdt
      {
         get {
            return (SdtLoteArquivoAnexo)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtLoteArquivoAnexo() ;
         }
      }

      [DataMember( Name = "gx_md5_hash", Order = 24 )]
      public string Hash
      {
         get {
            if ( StringUtil.StrCmp(md5Hash, null) == 0 )
            {
               md5Hash = (String)(getHash());
            }
            return md5Hash ;
         }

         set {
            md5Hash = value ;
         }

      }

      private String md5Hash ;
   }

}
