/*
               File: ContratoUnidades
        Description: Unidade Contratada
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/6/2020 23:55:58.16
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class contratounidades : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxJX_Action7") == 0 )
         {
            A1207ContratoUnidades_ContratoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1207ContratoUnidades_ContratoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1207ContratoUnidades_ContratoCod), 6, 0)));
            A1204ContratoUnidades_UndMedCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1204ContratoUnidades_UndMedCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1204ContratoUnidades_UndMedCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            XC_7_3C143( A1207ContratoUnidades_ContratoCod, A1204ContratoUnidades_UndMedCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxCallCrl"+"_"+"CONTRATOUNIDADES_UNDMEDCOD") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            GXDLACONTRATOUNIDADES_UNDMEDCOD3C143( ) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_10") == 0 )
         {
            A1204ContratoUnidades_UndMedCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1204ContratoUnidades_UndMedCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1204ContratoUnidades_UndMedCod), 6, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_10( A1204ContratoUnidades_UndMedCod) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
         {
            Gx_mode = gxfirstwebparm;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
            {
               AV7ContratoUnidades_ContratoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7ContratoUnidades_ContratoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV7ContratoUnidades_ContratoCod), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTRATOUNIDADES_CONTRATOCOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7ContratoUnidades_ContratoCod), "ZZZZZ9")));
               AV13ContratoUnidades_UndMedCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13ContratoUnidades_UndMedCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13ContratoUnidades_UndMedCod), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTRATOUNIDADES_UNDMEDCOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV13ContratoUnidades_UndMedCod), "ZZZZZ9")));
            }
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         dynContratoUnidades_UndMedCod.Name = "CONTRATOUNIDADES_UNDMEDCOD";
         dynContratoUnidades_UndMedCod.WebTags = "";
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Unidade Contratada", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = dynContratoUnidades_UndMedCod_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public contratounidades( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public contratounidades( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_Gx_mode ,
                           int aP1_ContratoUnidades_ContratoCod ,
                           int aP2_ContratoUnidades_UndMedCod )
      {
         this.Gx_mode = aP0_Gx_mode;
         this.AV7ContratoUnidades_ContratoCod = aP1_ContratoUnidades_ContratoCod;
         this.AV13ContratoUnidades_UndMedCod = aP2_ContratoUnidades_UndMedCod;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         dynContratoUnidades_UndMedCod = new GXCombobox();
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("workwithplusbootstrapmasterpage", "GeneXus.Programs.workwithplusbootstrapmasterpage", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
         if ( dynContratoUnidades_UndMedCod.ItemCount > 0 )
         {
            A1204ContratoUnidades_UndMedCod = (int)(NumberUtil.Val( dynContratoUnidades_UndMedCod.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A1204ContratoUnidades_UndMedCod), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1204ContratoUnidades_UndMedCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1204ContratoUnidades_UndMedCod), 6, 0)));
         }
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            wb_table1_2_3C143( true) ;
         }
         return  ;
      }

      protected void wb_table1_2_3C143e( bool wbgen )
      {
         if ( wbgen )
         {
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void wb_table1_2_3C143( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_5_3C143( true) ;
         }
         return  ;
      }

      protected void wb_table2_5_3C143e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_29_3C143( true) ;
         }
         return  ;
      }

      protected void wb_table3_29_3C143e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_3C143e( true) ;
         }
         else
         {
            wb_table1_2_3C143e( false) ;
         }
      }

      protected void wb_table3_29_3C143( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 32,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_enter_Internalname, "", "Confirmar", bttBtn_trn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_trn_enter_Visible, bttBtn_trn_enter_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContratoUnidades.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 34,'',false,'',0)\"";
            ClassString = "SpecialButtons";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_cancel_Internalname, "", "Fechar", bttBtn_trn_cancel_Jsonclick, 1, "Fechar", "", StyleString, ClassString, bttBtn_trn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContratoUnidades.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='TableActionsCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 36,'',false,'',0)\"";
            ClassString = "btn btn-danger";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_trn_delete_Internalname, "", "Eliminar", bttBtn_trn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_trn_delete_Visible, bttBtn_trn_delete_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_ContratoUnidades.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_29_3C143e( true) ;
         }
         else
         {
            wb_table3_29_3C143e( false) ;
         }
      }

      protected void wb_table2_5_3C143( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table4_11_3C143( true) ;
         }
         return  ;
      }

      protected void wb_table4_11_3C143e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_3C143e( true) ;
         }
         else
         {
            wb_table2_5_3C143e( false) ;
         }
      }

      protected void wb_table4_11_3C143( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtable1_Internalname, tblUnnamedtable1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratounidades_undmedcod_Internalname, "Nome", "", "", lblTextblockcontratounidades_undmedcod_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoUnidades.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 16,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, dynContratoUnidades_UndMedCod, dynContratoUnidades_UndMedCod_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A1204ContratoUnidades_UndMedCod), 6, 0)), 1, dynContratoUnidades_UndMedCod_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, dynContratoUnidades_UndMedCod.Enabled, 1, 0, 0, "em", 0, "", "", "BootstrapAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,16);\"", "", true, "HLP_ContratoUnidades.htm");
            dynContratoUnidades_UndMedCod.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A1204ContratoUnidades_UndMedCod), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynContratoUnidades_UndMedCod_Internalname, "Values", (String)(dynContratoUnidades_UndMedCod.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='DataDescriptionCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockcontratounidades_produtividade_Internalname, "Produtividade diaria", "", "", lblTextblockcontratounidades_produtividade_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "DataDescription", 0, "", 1, 1, 0, "HLP_ContratoUnidades.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            wb_table5_21_3C143( true) ;
         }
         return  ;
      }

      protected void wb_table5_21_3C143e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_11_3C143e( true) ;
         }
         else
         {
            wb_table4_11_3C143e( false) ;
         }
      }

      protected void wb_table5_21_3C143( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedcontratounidades_produtividade_Internalname, tblTablemergedcontratounidades_produtividade_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 24,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtContratoUnidades_Produtividade_Internalname, StringUtil.LTrim( StringUtil.NToC( A1208ContratoUnidades_Produtividade, 14, 5, ",", "")), ((edtContratoUnidades_Produtividade_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( A1208ContratoUnidades_Produtividade, "ZZ,ZZZ,ZZ9.999")) : context.localUtil.Format( A1208ContratoUnidades_Produtividade, "ZZ,ZZZ,ZZ9.999")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','5');"+";gx.evt.onblur(this,24);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoUnidades_Produtividade_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContratoUnidades_Produtividade_Enabled, 0, "text", "", 80, "px", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "PontosDeFuncao", "right", false, "HLP_ContratoUnidades.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='DataContentCell'>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtContratoUnidades_UndMedSigla_Internalname, StringUtil.RTrim( A1206ContratoUnidades_UndMedSigla), StringUtil.RTrim( context.localUtil.Format( A1206ContratoUnidades_UndMedSigla, "@!")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtContratoUnidades_UndMedSigla_Jsonclick, 0, "BootstrapAttribute", "", "", "", 1, edtContratoUnidades_UndMedSigla_Enabled, 0, "text", "", 15, "chr", 1, "row", 15, 0, 0, 0, 1, -1, -1, true, "Sigla", "left", true, "HLP_ContratoUnidades.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_21_3C143e( true) ;
         }
         else
         {
            wb_table5_21_3C143e( false) ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E113C2 */
         E113C2 ();
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               dynContratoUnidades_UndMedCod.CurrentValue = cgiGet( dynContratoUnidades_UndMedCod_Internalname);
               A1204ContratoUnidades_UndMedCod = (int)(NumberUtil.Val( cgiGet( dynContratoUnidades_UndMedCod_Internalname), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1204ContratoUnidades_UndMedCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1204ContratoUnidades_UndMedCod), 6, 0)));
               if ( ( ( context.localUtil.CToN( cgiGet( edtContratoUnidades_Produtividade_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtContratoUnidades_Produtividade_Internalname), ",", ".") > 99999999.99999m ) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CONTRATOUNIDADES_PRODUTIVIDADE");
                  AnyError = 1;
                  GX_FocusControl = edtContratoUnidades_Produtividade_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A1208ContratoUnidades_Produtividade = 0;
                  n1208ContratoUnidades_Produtividade = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1208ContratoUnidades_Produtividade", StringUtil.LTrim( StringUtil.Str( A1208ContratoUnidades_Produtividade, 14, 5)));
               }
               else
               {
                  A1208ContratoUnidades_Produtividade = context.localUtil.CToN( cgiGet( edtContratoUnidades_Produtividade_Internalname), ",", ".");
                  n1208ContratoUnidades_Produtividade = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1208ContratoUnidades_Produtividade", StringUtil.LTrim( StringUtil.Str( A1208ContratoUnidades_Produtividade, 14, 5)));
               }
               n1208ContratoUnidades_Produtividade = ((Convert.ToDecimal(0)==A1208ContratoUnidades_Produtividade) ? true : false);
               A1206ContratoUnidades_UndMedSigla = StringUtil.Upper( cgiGet( edtContratoUnidades_UndMedSigla_Internalname));
               n1206ContratoUnidades_UndMedSigla = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1206ContratoUnidades_UndMedSigla", A1206ContratoUnidades_UndMedSigla);
               /* Read saved values. */
               Z1207ContratoUnidades_ContratoCod = (int)(context.localUtil.CToN( cgiGet( "Z1207ContratoUnidades_ContratoCod"), ",", "."));
               Z1204ContratoUnidades_UndMedCod = (int)(context.localUtil.CToN( cgiGet( "Z1204ContratoUnidades_UndMedCod"), ",", "."));
               Z1208ContratoUnidades_Produtividade = context.localUtil.CToN( cgiGet( "Z1208ContratoUnidades_Produtividade"), ",", ".");
               n1208ContratoUnidades_Produtividade = ((Convert.ToDecimal(0)==A1208ContratoUnidades_Produtividade) ? true : false);
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
               Gx_mode = cgiGet( "Mode");
               AV7ContratoUnidades_ContratoCod = (int)(context.localUtil.CToN( cgiGet( "vCONTRATOUNIDADES_CONTRATOCOD"), ",", "."));
               A1207ContratoUnidades_ContratoCod = (int)(context.localUtil.CToN( cgiGet( "CONTRATOUNIDADES_CONTRATOCOD"), ",", "."));
               AV13ContratoUnidades_UndMedCod = (int)(context.localUtil.CToN( cgiGet( "vCONTRATOUNIDADES_UNDMEDCOD"), ",", "."));
               AV14SaldoContrato_Codigo = (int)(context.localUtil.CToN( cgiGet( "vSALDOCONTRATO_CODIGO"), ",", "."));
               A1205ContratoUnidades_UndMedNom = cgiGet( "CONTRATOUNIDADES_UNDMEDNOM");
               n1205ContratoUnidades_UndMedNom = false;
               Gx_mode = cgiGet( "vMODE");
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               forbiddenHiddens = "hsh" + "ContratoUnidades";
               forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
               forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A1207ContratoUnidades_ContratoCod), "ZZZZZ9");
               hsh = cgiGet( "hsh");
               if ( ( ! ( ( A1204ContratoUnidades_UndMedCod != Z1204ContratoUnidades_UndMedCod ) ) || ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ) && ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
               {
                  GXUtil.WriteLog("contratounidades:[SecurityCheckFailed value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
                  GXUtil.WriteLog("contratounidades:[SecurityCheckFailed value for]"+"ContratoUnidades_ContratoCod:"+context.localUtil.Format( (decimal)(A1207ContratoUnidades_ContratoCod), "ZZZZZ9"));
                  GxWebError = 1;
                  context.HttpContext.Response.StatusDescription = 403.ToString();
                  context.HttpContext.Response.StatusCode = 403;
                  context.WriteHtmlText( "<title>403 Forbidden</title>") ;
                  context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
                  context.WriteHtmlText( "<p /><hr />") ;
                  GXUtil.WriteLog("send_http_error_code " + 403.ToString());
                  AnyError = 1;
                  return  ;
               }
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  A1207ContratoUnidades_ContratoCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1207ContratoUnidades_ContratoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1207ContratoUnidades_ContratoCod), 6, 0)));
                  A1204ContratoUnidades_UndMedCod = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1204ContratoUnidades_UndMedCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1204ContratoUnidades_UndMedCod), 6, 0)));
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  disable_std_buttons( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
                  {
                     sMode143 = Gx_mode;
                     Gx_mode = "UPD";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                     Gx_mode = sMode143;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
                  }
                  standaloneModal( ) ;
                  if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
                  {
                     getByPrimaryKey( ) ;
                     if ( RcdFound143 == 1 )
                     {
                        if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
                        {
                           /* Confirm record */
                           CONFIRM_3C0( ) ;
                           if ( AnyError == 0 )
                           {
                              GX_FocusControl = bttBtn_trn_enter_Internalname;
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noinsert", ""), 1, "CONTRATOUNIDADES_UNDMEDCOD");
                        AnyError = 1;
                        GX_FocusControl = dynContratoUnidades_UndMedCod_Internalname;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E113C2 */
                           E113C2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "AFTER TRN") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E123C2 */
                           E123C2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           if ( StringUtil.StrCmp(Gx_mode, "DSP") != 0 )
                           {
                              btn_enter( ) ;
                           }
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            /* Execute user event: E123C2 */
            E123C2 ();
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll3C143( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         bttBtn_trn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
         if ( ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            bttBtn_trn_delete_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Visible), 5, 0)));
            if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
            {
               bttBtn_trn_enter_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Visible), 5, 0)));
            }
            DisableAttributes3C143( ) ;
         }
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void CONFIRM_3C0( )
      {
         BeforeValidate3C143( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls3C143( ) ;
            }
            else
            {
               CheckExtendedTable3C143( ) ;
               CloseExtendedTableCursors3C143( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         }
      }

      protected void ResetCaption3C0( )
      {
      }

      protected void E113C2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV8WWPContext) ;
         AV9TrnContext.FromXml(AV10WebSession.Get("TrnContext"), "");
      }

      protected void E123C2( )
      {
         /* After Trn Routine */
         if ( ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) && ! AV9TrnContext.gxTpr_Callerondelete )
         {
            context.wjLoc = formatLink("wwcontratounidades.aspx") ;
            context.wjLocDisableFrm = 1;
         }
         context.setWebReturnParms(new Object[] {});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void ZM3C143( short GX_JID )
      {
         if ( ( GX_JID == 8 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z1208ContratoUnidades_Produtividade = T003C3_A1208ContratoUnidades_Produtividade[0];
            }
            else
            {
               Z1208ContratoUnidades_Produtividade = A1208ContratoUnidades_Produtividade;
            }
         }
         if ( GX_JID == -8 )
         {
            Z1208ContratoUnidades_Produtividade = A1208ContratoUnidades_Produtividade;
            Z1207ContratoUnidades_ContratoCod = A1207ContratoUnidades_ContratoCod;
            Z1204ContratoUnidades_UndMedCod = A1204ContratoUnidades_UndMedCod;
            Z1205ContratoUnidades_UndMedNom = A1205ContratoUnidades_UndMedNom;
            Z1206ContratoUnidades_UndMedSigla = A1206ContratoUnidades_UndMedSigla;
         }
      }

      protected void standaloneNotModal( )
      {
         GXACONTRATOUNIDADES_UNDMEDCOD_html3C143( ) ;
         bttBtn_trn_delete_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_delete_Enabled), 5, 0)));
         if ( ! (0==AV7ContratoUnidades_ContratoCod) )
         {
            A1207ContratoUnidades_ContratoCod = AV7ContratoUnidades_ContratoCod;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1207ContratoUnidades_ContratoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1207ContratoUnidades_ContratoCod), 6, 0)));
         }
         /* Using cursor T003C4 */
         pr_default.execute(2, new Object[] {A1207ContratoUnidades_ContratoCod});
         if ( (pr_default.getStatus(2) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contrato Unidades_Contrato'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         pr_default.close(2);
         if ( ! (0==AV13ContratoUnidades_UndMedCod) )
         {
            A1204ContratoUnidades_UndMedCod = AV13ContratoUnidades_UndMedCod;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1204ContratoUnidades_UndMedCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1204ContratoUnidades_UndMedCod), 6, 0)));
         }
         if ( ! (0==AV13ContratoUnidades_UndMedCod) )
         {
            dynContratoUnidades_UndMedCod.Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynContratoUnidades_UndMedCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynContratoUnidades_UndMedCod.Enabled), 5, 0)));
         }
         else
         {
            dynContratoUnidades_UndMedCod.Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynContratoUnidades_UndMedCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynContratoUnidades_UndMedCod.Enabled), 5, 0)));
         }
         if ( ! (0==AV13ContratoUnidades_UndMedCod) )
         {
            dynContratoUnidades_UndMedCod.Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynContratoUnidades_UndMedCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynContratoUnidades_UndMedCod.Enabled), 5, 0)));
         }
      }

      protected void standaloneModal( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            bttBtn_trn_enter_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         else
         {
            bttBtn_trn_enter_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_trn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_trn_enter_Enabled), 5, 0)));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ( Gx_BScreen == 0 ) )
         {
            /* Using cursor T003C5 */
            pr_default.execute(3, new Object[] {A1204ContratoUnidades_UndMedCod});
            A1205ContratoUnidades_UndMedNom = T003C5_A1205ContratoUnidades_UndMedNom[0];
            n1205ContratoUnidades_UndMedNom = T003C5_n1205ContratoUnidades_UndMedNom[0];
            A1206ContratoUnidades_UndMedSigla = T003C5_A1206ContratoUnidades_UndMedSigla[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1206ContratoUnidades_UndMedSigla", A1206ContratoUnidades_UndMedSigla);
            n1206ContratoUnidades_UndMedSigla = T003C5_n1206ContratoUnidades_UndMedSigla[0];
            pr_default.close(3);
         }
      }

      protected void Load3C143( )
      {
         /* Using cursor T003C6 */
         pr_default.execute(4, new Object[] {A1207ContratoUnidades_ContratoCod, A1204ContratoUnidades_UndMedCod});
         if ( (pr_default.getStatus(4) != 101) )
         {
            RcdFound143 = 1;
            A1205ContratoUnidades_UndMedNom = T003C6_A1205ContratoUnidades_UndMedNom[0];
            n1205ContratoUnidades_UndMedNom = T003C6_n1205ContratoUnidades_UndMedNom[0];
            A1206ContratoUnidades_UndMedSigla = T003C6_A1206ContratoUnidades_UndMedSigla[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1206ContratoUnidades_UndMedSigla", A1206ContratoUnidades_UndMedSigla);
            n1206ContratoUnidades_UndMedSigla = T003C6_n1206ContratoUnidades_UndMedSigla[0];
            A1208ContratoUnidades_Produtividade = T003C6_A1208ContratoUnidades_Produtividade[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1208ContratoUnidades_Produtividade", StringUtil.LTrim( StringUtil.Str( A1208ContratoUnidades_Produtividade, 14, 5)));
            n1208ContratoUnidades_Produtividade = T003C6_n1208ContratoUnidades_Produtividade[0];
            ZM3C143( -8) ;
         }
         pr_default.close(4);
         OnLoadActions3C143( ) ;
      }

      protected void OnLoadActions3C143( )
      {
      }

      protected void CheckExtendedTable3C143( )
      {
         Gx_BScreen = 1;
         standaloneModal( ) ;
         /* Using cursor T003C5 */
         pr_default.execute(3, new Object[] {A1204ContratoUnidades_UndMedCod});
         if ( (pr_default.getStatus(3) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contrato Unidades_Unidades Medicao'.", "ForeignKeyNotFound", 1, "CONTRATOUNIDADES_UNDMEDCOD");
            AnyError = 1;
            GX_FocusControl = dynContratoUnidades_UndMedCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A1205ContratoUnidades_UndMedNom = T003C5_A1205ContratoUnidades_UndMedNom[0];
         n1205ContratoUnidades_UndMedNom = T003C5_n1205ContratoUnidades_UndMedNom[0];
         A1206ContratoUnidades_UndMedSigla = T003C5_A1206ContratoUnidades_UndMedSigla[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1206ContratoUnidades_UndMedSigla", A1206ContratoUnidades_UndMedSigla);
         n1206ContratoUnidades_UndMedSigla = T003C5_n1206ContratoUnidades_UndMedSigla[0];
         pr_default.close(3);
      }

      protected void CloseExtendedTableCursors3C143( )
      {
         pr_default.close(3);
      }

      protected void enableDisable( )
      {
      }

      protected void gxLoad_10( int A1204ContratoUnidades_UndMedCod )
      {
         /* Using cursor T003C7 */
         pr_default.execute(5, new Object[] {A1204ContratoUnidades_UndMedCod});
         if ( (pr_default.getStatus(5) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contrato Unidades_Unidades Medicao'.", "ForeignKeyNotFound", 1, "CONTRATOUNIDADES_UNDMEDCOD");
            AnyError = 1;
            GX_FocusControl = dynContratoUnidades_UndMedCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A1205ContratoUnidades_UndMedNom = T003C7_A1205ContratoUnidades_UndMedNom[0];
         n1205ContratoUnidades_UndMedNom = T003C7_n1205ContratoUnidades_UndMedNom[0];
         A1206ContratoUnidades_UndMedSigla = T003C7_A1206ContratoUnidades_UndMedSigla[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1206ContratoUnidades_UndMedSigla", A1206ContratoUnidades_UndMedSigla);
         n1206ContratoUnidades_UndMedSigla = T003C7_n1206ContratoUnidades_UndMedSigla[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A1205ContratoUnidades_UndMedNom))+"\""+","+"\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A1206ContratoUnidades_UndMedSigla))+"\"");
         context.GX_webresponse.AddString(")");
         if ( (pr_default.getStatus(5) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(5);
      }

      protected void GetKey3C143( )
      {
         /* Using cursor T003C8 */
         pr_default.execute(6, new Object[] {A1207ContratoUnidades_ContratoCod, A1204ContratoUnidades_UndMedCod});
         if ( (pr_default.getStatus(6) != 101) )
         {
            RcdFound143 = 1;
         }
         else
         {
            RcdFound143 = 0;
         }
         pr_default.close(6);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T003C3 */
         pr_default.execute(1, new Object[] {A1207ContratoUnidades_ContratoCod, A1204ContratoUnidades_UndMedCod});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM3C143( 8) ;
            RcdFound143 = 1;
            A1208ContratoUnidades_Produtividade = T003C3_A1208ContratoUnidades_Produtividade[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1208ContratoUnidades_Produtividade", StringUtil.LTrim( StringUtil.Str( A1208ContratoUnidades_Produtividade, 14, 5)));
            n1208ContratoUnidades_Produtividade = T003C3_n1208ContratoUnidades_Produtividade[0];
            A1207ContratoUnidades_ContratoCod = T003C3_A1207ContratoUnidades_ContratoCod[0];
            A1204ContratoUnidades_UndMedCod = T003C3_A1204ContratoUnidades_UndMedCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1204ContratoUnidades_UndMedCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1204ContratoUnidades_UndMedCod), 6, 0)));
            Z1207ContratoUnidades_ContratoCod = A1207ContratoUnidades_ContratoCod;
            Z1204ContratoUnidades_UndMedCod = A1204ContratoUnidades_UndMedCod;
            sMode143 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            Load3C143( ) ;
            if ( AnyError == 1 )
            {
               RcdFound143 = 0;
               InitializeNonKey3C143( ) ;
            }
            Gx_mode = sMode143;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         else
         {
            RcdFound143 = 0;
            InitializeNonKey3C143( ) ;
            sMode143 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
            standaloneModal( ) ;
            Gx_mode = sMode143;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey3C143( ) ;
         if ( RcdFound143 == 0 )
         {
         }
         else
         {
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound143 = 0;
         /* Using cursor T003C9 */
         pr_default.execute(7, new Object[] {A1207ContratoUnidades_ContratoCod, A1204ContratoUnidades_UndMedCod});
         if ( (pr_default.getStatus(7) != 101) )
         {
            while ( (pr_default.getStatus(7) != 101) && ( ( T003C9_A1207ContratoUnidades_ContratoCod[0] < A1207ContratoUnidades_ContratoCod ) || ( T003C9_A1207ContratoUnidades_ContratoCod[0] == A1207ContratoUnidades_ContratoCod ) && ( T003C9_A1204ContratoUnidades_UndMedCod[0] < A1204ContratoUnidades_UndMedCod ) ) )
            {
               pr_default.readNext(7);
            }
            if ( (pr_default.getStatus(7) != 101) && ( ( T003C9_A1207ContratoUnidades_ContratoCod[0] > A1207ContratoUnidades_ContratoCod ) || ( T003C9_A1207ContratoUnidades_ContratoCod[0] == A1207ContratoUnidades_ContratoCod ) && ( T003C9_A1204ContratoUnidades_UndMedCod[0] > A1204ContratoUnidades_UndMedCod ) ) )
            {
               A1207ContratoUnidades_ContratoCod = T003C9_A1207ContratoUnidades_ContratoCod[0];
               A1204ContratoUnidades_UndMedCod = T003C9_A1204ContratoUnidades_UndMedCod[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1204ContratoUnidades_UndMedCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1204ContratoUnidades_UndMedCod), 6, 0)));
               RcdFound143 = 1;
            }
         }
         pr_default.close(7);
      }

      protected void move_previous( )
      {
         RcdFound143 = 0;
         /* Using cursor T003C10 */
         pr_default.execute(8, new Object[] {A1207ContratoUnidades_ContratoCod, A1204ContratoUnidades_UndMedCod});
         if ( (pr_default.getStatus(8) != 101) )
         {
            while ( (pr_default.getStatus(8) != 101) && ( ( T003C10_A1207ContratoUnidades_ContratoCod[0] > A1207ContratoUnidades_ContratoCod ) || ( T003C10_A1207ContratoUnidades_ContratoCod[0] == A1207ContratoUnidades_ContratoCod ) && ( T003C10_A1204ContratoUnidades_UndMedCod[0] > A1204ContratoUnidades_UndMedCod ) ) )
            {
               pr_default.readNext(8);
            }
            if ( (pr_default.getStatus(8) != 101) && ( ( T003C10_A1207ContratoUnidades_ContratoCod[0] < A1207ContratoUnidades_ContratoCod ) || ( T003C10_A1207ContratoUnidades_ContratoCod[0] == A1207ContratoUnidades_ContratoCod ) && ( T003C10_A1204ContratoUnidades_UndMedCod[0] < A1204ContratoUnidades_UndMedCod ) ) )
            {
               A1207ContratoUnidades_ContratoCod = T003C10_A1207ContratoUnidades_ContratoCod[0];
               A1204ContratoUnidades_UndMedCod = T003C10_A1204ContratoUnidades_UndMedCod[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1204ContratoUnidades_UndMedCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1204ContratoUnidades_UndMedCod), 6, 0)));
               RcdFound143 = 1;
            }
         }
         pr_default.close(8);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey3C143( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = dynContratoUnidades_UndMedCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert3C143( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound143 == 1 )
            {
               if ( ( A1207ContratoUnidades_ContratoCod != Z1207ContratoUnidades_ContratoCod ) || ( A1204ContratoUnidades_UndMedCod != Z1204ContratoUnidades_UndMedCod ) )
               {
                  A1207ContratoUnidades_ContratoCod = Z1207ContratoUnidades_ContratoCod;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1207ContratoUnidades_ContratoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1207ContratoUnidades_ContratoCod), 6, 0)));
                  A1204ContratoUnidades_UndMedCod = Z1204ContratoUnidades_UndMedCod;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1204ContratoUnidades_UndMedCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1204ContratoUnidades_UndMedCod), 6, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "CONTRATOUNIDADES_UNDMEDCOD");
                  AnyError = 1;
                  GX_FocusControl = dynContratoUnidades_UndMedCod_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = dynContratoUnidades_UndMedCod_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  /* Update record */
                  Update3C143( ) ;
                  GX_FocusControl = dynContratoUnidades_UndMedCod_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( ( A1207ContratoUnidades_ContratoCod != Z1207ContratoUnidades_ContratoCod ) || ( A1204ContratoUnidades_UndMedCod != Z1204ContratoUnidades_UndMedCod ) )
               {
                  /* Insert record */
                  GX_FocusControl = dynContratoUnidades_UndMedCod_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert3C143( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "CONTRATOUNIDADES_UNDMEDCOD");
                     AnyError = 1;
                     GX_FocusControl = dynContratoUnidades_UndMedCod_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     /* Insert record */
                     GX_FocusControl = dynContratoUnidades_UndMedCod_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert3C143( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            if ( AnyError == 0 )
            {
               context.nUserReturn = 1;
            }
         }
      }

      protected void btn_delete( )
      {
         if ( ( A1207ContratoUnidades_ContratoCod != Z1207ContratoUnidades_ContratoCod ) || ( A1204ContratoUnidades_UndMedCod != Z1204ContratoUnidades_UndMedCod ) )
         {
            A1207ContratoUnidades_ContratoCod = Z1207ContratoUnidades_ContratoCod;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1207ContratoUnidades_ContratoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1207ContratoUnidades_ContratoCod), 6, 0)));
            A1204ContratoUnidades_UndMedCod = Z1204ContratoUnidades_UndMedCod;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1204ContratoUnidades_UndMedCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1204ContratoUnidades_UndMedCod), 6, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "CONTRATOUNIDADES_UNDMEDCOD");
            AnyError = 1;
            GX_FocusControl = dynContratoUnidades_UndMedCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = dynContratoUnidades_UndMedCod_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
         }
      }

      protected void CheckOptimisticConcurrency3C143( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T003C2 */
            pr_default.execute(0, new Object[] {A1207ContratoUnidades_ContratoCod, A1204ContratoUnidades_UndMedCod});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ContratoUnidades"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) || ( Z1208ContratoUnidades_Produtividade != T003C2_A1208ContratoUnidades_Produtividade[0] ) )
            {
               if ( Z1208ContratoUnidades_Produtividade != T003C2_A1208ContratoUnidades_Produtividade[0] )
               {
                  GXUtil.WriteLog("contratounidades:[seudo value changed for attri]"+"ContratoUnidades_Produtividade");
                  GXUtil.WriteLogRaw("Old: ",Z1208ContratoUnidades_Produtividade);
                  GXUtil.WriteLogRaw("Current: ",T003C2_A1208ContratoUnidades_Produtividade[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"ContratoUnidades"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert3C143( )
      {
         BeforeValidate3C143( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable3C143( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM3C143( 0) ;
            CheckOptimisticConcurrency3C143( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm3C143( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert3C143( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T003C11 */
                     pr_default.execute(9, new Object[] {n1208ContratoUnidades_Produtividade, A1208ContratoUnidades_Produtividade, A1207ContratoUnidades_ContratoCod, A1204ContratoUnidades_UndMedCod});
                     pr_default.close(9);
                     dsDefault.SmartCacheProvider.SetUpdated("ContratoUnidades") ;
                     if ( (pr_default.getStatus(9) == 1) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        new prc_saldocontratounidademedicao_criar(context ).execute(  A1207ContratoUnidades_ContratoCod,  A1204ContratoUnidades_UndMedCod, out  AV14SaldoContrato_Codigo) ;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1207ContratoUnidades_ContratoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1207ContratoUnidades_ContratoCod), 6, 0)));
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1204ContratoUnidades_UndMedCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1204ContratoUnidades_UndMedCod), 6, 0)));
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14SaldoContrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14SaldoContrato_Codigo), 6, 0)));
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                           ResetCaption3C0( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load3C143( ) ;
            }
            EndLevel3C143( ) ;
         }
         CloseExtendedTableCursors3C143( ) ;
      }

      protected void Update3C143( )
      {
         BeforeValidate3C143( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable3C143( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency3C143( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm3C143( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate3C143( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T003C12 */
                     pr_default.execute(10, new Object[] {n1208ContratoUnidades_Produtividade, A1208ContratoUnidades_Produtividade, A1207ContratoUnidades_ContratoCod, A1204ContratoUnidades_UndMedCod});
                     pr_default.close(10);
                     dsDefault.SmartCacheProvider.SetUpdated("ContratoUnidades") ;
                     if ( (pr_default.getStatus(10) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"ContratoUnidades"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate3C143( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                           {
                              if ( AnyError == 0 )
                              {
                                 context.nUserReturn = 1;
                              }
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel3C143( ) ;
         }
         CloseExtendedTableCursors3C143( ) ;
      }

      protected void DeferredUpdate3C143( )
      {
      }

      protected void delete( )
      {
         BeforeValidate3C143( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency3C143( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls3C143( ) ;
            AfterConfirm3C143( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete3C143( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T003C13 */
                  pr_default.execute(11, new Object[] {A1207ContratoUnidades_ContratoCod, A1204ContratoUnidades_UndMedCod});
                  pr_default.close(11);
                  dsDefault.SmartCacheProvider.SetUpdated("ContratoUnidades") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                        {
                           if ( AnyError == 0 )
                           {
                              context.nUserReturn = 1;
                           }
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode143 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         EndLevel3C143( ) ;
         Gx_mode = sMode143;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
      }

      protected void OnDeleteControls3C143( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            /* Using cursor T003C14 */
            pr_default.execute(12, new Object[] {A1204ContratoUnidades_UndMedCod});
            A1205ContratoUnidades_UndMedNom = T003C14_A1205ContratoUnidades_UndMedNom[0];
            n1205ContratoUnidades_UndMedNom = T003C14_n1205ContratoUnidades_UndMedNom[0];
            A1206ContratoUnidades_UndMedSigla = T003C14_A1206ContratoUnidades_UndMedSigla[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1206ContratoUnidades_UndMedSigla", A1206ContratoUnidades_UndMedSigla);
            n1206ContratoUnidades_UndMedSigla = T003C14_n1206ContratoUnidades_UndMedSigla[0];
            pr_default.close(12);
         }
      }

      protected void EndLevel3C143( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete3C143( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(1);
            pr_default.close(12);
            context.CommitDataStores( "ContratoUnidades");
            if ( AnyError == 0 )
            {
               ConfirmValues3C0( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(1);
            pr_default.close(12);
            context.RollbackDataStores( "ContratoUnidades");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart3C143( )
      {
         /* Scan By routine */
         /* Using cursor T003C15 */
         pr_default.execute(13);
         RcdFound143 = 0;
         if ( (pr_default.getStatus(13) != 101) )
         {
            RcdFound143 = 1;
            A1207ContratoUnidades_ContratoCod = T003C15_A1207ContratoUnidades_ContratoCod[0];
            A1204ContratoUnidades_UndMedCod = T003C15_A1204ContratoUnidades_UndMedCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1204ContratoUnidades_UndMedCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1204ContratoUnidades_UndMedCod), 6, 0)));
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext3C143( )
      {
         /* Scan next routine */
         pr_default.readNext(13);
         RcdFound143 = 0;
         if ( (pr_default.getStatus(13) != 101) )
         {
            RcdFound143 = 1;
            A1207ContratoUnidades_ContratoCod = T003C15_A1207ContratoUnidades_ContratoCod[0];
            A1204ContratoUnidades_UndMedCod = T003C15_A1204ContratoUnidades_UndMedCod[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1204ContratoUnidades_UndMedCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1204ContratoUnidades_UndMedCod), 6, 0)));
         }
      }

      protected void ScanEnd3C143( )
      {
         pr_default.close(13);
      }

      protected void AfterConfirm3C143( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert3C143( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate3C143( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete3C143( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete3C143( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate3C143( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes3C143( )
      {
         dynContratoUnidades_UndMedCod.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, dynContratoUnidades_UndMedCod_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(dynContratoUnidades_UndMedCod.Enabled), 5, 0)));
         edtContratoUnidades_Produtividade_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoUnidades_Produtividade_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoUnidades_Produtividade_Enabled), 5, 0)));
         edtContratoUnidades_UndMedSigla_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtContratoUnidades_UndMedSigla_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtContratoUnidades_UndMedSigla_Enabled), 5, 0)));
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues3C0( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?2020562355596");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("contratounidades.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7ContratoUnidades_ContratoCod) + "," + UrlEncode("" +AV13ContratoUnidades_UndMedCod)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "Z1207ContratoUnidades_ContratoCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1207ContratoUnidades_ContratoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1204ContratoUnidades_UndMedCod", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1204ContratoUnidades_UndMedCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z1208ContratoUnidades_Produtividade", StringUtil.LTrim( StringUtil.NToC( Z1208ContratoUnidades_Produtividade, 14, 5, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vTRNCONTEXT", AV9TrnContext);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vTRNCONTEXT", AV9TrnContext);
         }
         GxWebStd.gx_hidden_field( context, "vCONTRATOUNIDADES_CONTRATOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV7ContratoUnidades_ContratoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATOUNIDADES_CONTRATOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1207ContratoUnidades_ContratoCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vCONTRATOUNIDADES_UNDMEDCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV13ContratoUnidades_UndMedCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vSALDOCONTRATO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV14SaldoContrato_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATOUNIDADES_UNDMEDNOM", StringUtil.RTrim( A1205ContratoUnidades_UndMedNom));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vCONTRATOUNIDADES_CONTRATOCOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV7ContratoUnidades_ContratoCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vCONTRATOUNIDADES_UNDMEDCOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV13ContratoUnidades_UndMedCod), "ZZZZZ9")));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = "hsh" + "ContratoUnidades";
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(A1207ContratoUnidades_ContratoCod), "ZZZZZ9");
         GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("contratounidades:[SendSecurityCheck value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
         GXUtil.WriteLog("contratounidades:[SendSecurityCheck value for]"+"ContratoUnidades_ContratoCod:"+context.localUtil.Format( (decimal)(A1207ContratoUnidades_ContratoCod), "ZZZZZ9"));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("contratounidades.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV7ContratoUnidades_ContratoCod) + "," + UrlEncode("" +AV13ContratoUnidades_UndMedCod) ;
      }

      public override String GetPgmname( )
      {
         return "ContratoUnidades" ;
      }

      public override String GetPgmdesc( )
      {
         return "Unidade Contratada" ;
      }

      protected void InitializeNonKey3C143( )
      {
         AV14SaldoContrato_Codigo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14SaldoContrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14SaldoContrato_Codigo), 6, 0)));
         A1205ContratoUnidades_UndMedNom = "";
         n1205ContratoUnidades_UndMedNom = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1205ContratoUnidades_UndMedNom", A1205ContratoUnidades_UndMedNom);
         A1206ContratoUnidades_UndMedSigla = "";
         n1206ContratoUnidades_UndMedSigla = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1206ContratoUnidades_UndMedSigla", A1206ContratoUnidades_UndMedSigla);
         A1208ContratoUnidades_Produtividade = 0;
         n1208ContratoUnidades_Produtividade = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1208ContratoUnidades_Produtividade", StringUtil.LTrim( StringUtil.Str( A1208ContratoUnidades_Produtividade, 14, 5)));
         n1208ContratoUnidades_Produtividade = ((Convert.ToDecimal(0)==A1208ContratoUnidades_Produtividade) ? true : false);
         Z1208ContratoUnidades_Produtividade = 0;
      }

      protected void InitAll3C143( )
      {
         A1207ContratoUnidades_ContratoCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1207ContratoUnidades_ContratoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1207ContratoUnidades_ContratoCod), 6, 0)));
         A1204ContratoUnidades_UndMedCod = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1204ContratoUnidades_UndMedCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1204ContratoUnidades_UndMedCod), 6, 0)));
         InitializeNonKey3C143( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20205623555914");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("contratounidades.js", "?20205623555915");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblockcontratounidades_undmedcod_Internalname = "TEXTBLOCKCONTRATOUNIDADES_UNDMEDCOD";
         dynContratoUnidades_UndMedCod_Internalname = "CONTRATOUNIDADES_UNDMEDCOD";
         lblTextblockcontratounidades_produtividade_Internalname = "TEXTBLOCKCONTRATOUNIDADES_PRODUTIVIDADE";
         edtContratoUnidades_Produtividade_Internalname = "CONTRATOUNIDADES_PRODUTIVIDADE";
         edtContratoUnidades_UndMedSigla_Internalname = "CONTRATOUNIDADES_UNDMEDSIGLA";
         tblTablemergedcontratounidades_produtividade_Internalname = "TABLEMERGEDCONTRATOUNIDADES_PRODUTIVIDADE";
         tblUnnamedtable1_Internalname = "UNNAMEDTABLE1";
         tblTablecontent_Internalname = "TABLECONTENT";
         bttBtn_trn_enter_Internalname = "BTN_TRN_ENTER";
         bttBtn_trn_cancel_Internalname = "BTN_TRN_CANCEL";
         bttBtn_trn_delete_Internalname = "BTN_TRN_DELETE";
         tblTableactions_Internalname = "TABLEACTIONS";
         tblTablemain_Internalname = "TABLEMAIN";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Unidade Contratada";
         edtContratoUnidades_UndMedSigla_Jsonclick = "";
         edtContratoUnidades_UndMedSigla_Enabled = 0;
         edtContratoUnidades_Produtividade_Jsonclick = "";
         edtContratoUnidades_Produtividade_Enabled = 1;
         dynContratoUnidades_UndMedCod_Jsonclick = "";
         dynContratoUnidades_UndMedCod.Enabled = 1;
         bttBtn_trn_delete_Enabled = 0;
         bttBtn_trn_delete_Visible = 1;
         bttBtn_trn_cancel_Visible = 1;
         bttBtn_trn_enter_Enabled = 1;
         bttBtn_trn_enter_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void GXDLACONTRATOUNIDADES_UNDMEDCOD3C143( )
      {
         if ( ! context.isAjaxRequest( ) )
         {
            context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
         }
         context.GX_webresponse.AddString("(new Array( new Array(");
         GXDLACONTRATOUNIDADES_UNDMEDCOD_data3C143( ) ;
         gxdynajaxindex = 1;
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            context.GX_webresponse.AddString(gxwrpcisep+"{c:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)))+"\",d:\""+GXUtil.EncodeJSConstant( ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)))+"\"}");
            gxdynajaxindex = (int)(gxdynajaxindex+1);
            gxwrpcisep = ",";
         }
         context.GX_webresponse.AddString(")");
         if ( gxdynajaxctrlcodr.Count == 0 )
         {
            context.GX_webresponse.AddString(",101");
         }
         context.GX_webresponse.AddString("))");
      }

      protected void GXACONTRATOUNIDADES_UNDMEDCOD_html3C143( )
      {
         int gxdynajaxvalue ;
         GXDLACONTRATOUNIDADES_UNDMEDCOD_data3C143( ) ;
         gxdynajaxindex = 1;
         dynContratoUnidades_UndMedCod.removeAllItems();
         while ( gxdynajaxindex <= gxdynajaxctrlcodr.Count )
         {
            gxdynajaxvalue = (int)(NumberUtil.Val( ((String)gxdynajaxctrlcodr.Item(gxdynajaxindex)), "."));
            dynContratoUnidades_UndMedCod.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(gxdynajaxvalue), 6, 0)), ((String)gxdynajaxctrldescr.Item(gxdynajaxindex)), 0);
            gxdynajaxindex = (int)(gxdynajaxindex+1);
         }
      }

      protected void GXDLACONTRATOUNIDADES_UNDMEDCOD_data3C143( )
      {
         gxdynajaxctrlcodr.Clear();
         gxdynajaxctrldescr.Clear();
         gxdynajaxctrlcodr.Add(StringUtil.Str( (decimal)(0), 1, 0));
         gxdynajaxctrldescr.Add("(Nenhum)");
         /* Using cursor T003C16 */
         pr_default.execute(14);
         while ( (pr_default.getStatus(14) != 101) )
         {
            gxdynajaxctrlcodr.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(T003C16_A1204ContratoUnidades_UndMedCod[0]), 6, 0, ".", "")));
            gxdynajaxctrldescr.Add(StringUtil.RTrim( T003C16_A1205ContratoUnidades_UndMedNom[0]));
            pr_default.readNext(14);
         }
         pr_default.close(14);
      }

      protected void XC_7_3C143( int A1207ContratoUnidades_ContratoCod ,
                                 int A1204ContratoUnidades_UndMedCod )
      {
         new prc_saldocontratounidademedicao_criar(context ).execute(  A1207ContratoUnidades_ContratoCod,  A1204ContratoUnidades_UndMedCod, out  AV14SaldoContrato_Codigo) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1207ContratoUnidades_ContratoCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1207ContratoUnidades_ContratoCod), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1204ContratoUnidades_UndMedCod", StringUtil.LTrim( StringUtil.Str( (decimal)(A1204ContratoUnidades_UndMedCod), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14SaldoContrato_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14SaldoContrato_Codigo), 6, 0)));
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("new Array( new Array(");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(AV14SaldoContrato_Codigo), 6, 0, ".", "")))+"\"");
         context.GX_webresponse.AddString(")");
         if ( true )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
      }

      public void Valid_Contratounidades_undmedcod( GXCombobox dynGX_Parm1 ,
                                                    String GX_Parm2 ,
                                                    String GX_Parm3 )
      {
         dynContratoUnidades_UndMedCod = dynGX_Parm1;
         A1204ContratoUnidades_UndMedCod = (int)(NumberUtil.Val( dynContratoUnidades_UndMedCod.CurrentValue, "."));
         A1205ContratoUnidades_UndMedNom = GX_Parm2;
         n1205ContratoUnidades_UndMedNom = false;
         A1206ContratoUnidades_UndMedSigla = GX_Parm3;
         n1206ContratoUnidades_UndMedSigla = false;
         /* Using cursor T003C17 */
         pr_default.execute(15, new Object[] {A1204ContratoUnidades_UndMedCod});
         if ( (pr_default.getStatus(15) == 101) )
         {
            GX_msglist.addItem("N�o existe 'Contrato Unidades_Unidades Medicao'.", "ForeignKeyNotFound", 1, "CONTRATOUNIDADES_UNDMEDCOD");
            AnyError = 1;
            GX_FocusControl = dynContratoUnidades_UndMedCod_Internalname;
         }
         A1205ContratoUnidades_UndMedNom = T003C17_A1205ContratoUnidades_UndMedNom[0];
         n1205ContratoUnidades_UndMedNom = T003C17_n1205ContratoUnidades_UndMedNom[0];
         A1206ContratoUnidades_UndMedSigla = T003C17_A1206ContratoUnidades_UndMedSigla[0];
         n1206ContratoUnidades_UndMedSigla = T003C17_n1206ContratoUnidades_UndMedSigla[0];
         pr_default.close(15);
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A1205ContratoUnidades_UndMedNom = "";
            n1205ContratoUnidades_UndMedNom = false;
            A1206ContratoUnidades_UndMedSigla = "";
            n1206ContratoUnidades_UndMedSigla = false;
         }
         isValidOutput.Add(StringUtil.RTrim( A1205ContratoUnidades_UndMedNom));
         isValidOutput.Add(StringUtil.RTrim( A1206ContratoUnidades_UndMedSigla));
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true},{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV7ContratoUnidades_ContratoCod',fld:'vCONTRATOUNIDADES_CONTRATOCOD',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV13ContratoUnidades_UndMedCod',fld:'vCONTRATOUNIDADES_UNDMEDCOD',pic:'ZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("AFTER TRN","{handler:'E123C2',iparms:[{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV9TrnContext',fld:'vTRNCONTEXT',pic:'',nv:null}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(15);
         pr_default.close(12);
      }

      public override void initialize( )
      {
         sPrefix = "";
         wcpOGx_mode = "";
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         sStyleString = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         bttBtn_trn_enter_Jsonclick = "";
         bttBtn_trn_cancel_Jsonclick = "";
         bttBtn_trn_delete_Jsonclick = "";
         lblTextblockcontratounidades_undmedcod_Jsonclick = "";
         lblTextblockcontratounidades_produtividade_Jsonclick = "";
         A1206ContratoUnidades_UndMedSigla = "";
         A1205ContratoUnidades_UndMedNom = "";
         forbiddenHiddens = "";
         hsh = "";
         sMode143 = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV8WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV9TrnContext = new wwpbaseobjects.SdtWWPTransactionContext(context);
         AV10WebSession = context.GetSession();
         Z1205ContratoUnidades_UndMedNom = "";
         Z1206ContratoUnidades_UndMedSigla = "";
         T003C4_A1207ContratoUnidades_ContratoCod = new int[1] ;
         T003C5_A1205ContratoUnidades_UndMedNom = new String[] {""} ;
         T003C5_n1205ContratoUnidades_UndMedNom = new bool[] {false} ;
         T003C5_A1206ContratoUnidades_UndMedSigla = new String[] {""} ;
         T003C5_n1206ContratoUnidades_UndMedSigla = new bool[] {false} ;
         T003C6_A1205ContratoUnidades_UndMedNom = new String[] {""} ;
         T003C6_n1205ContratoUnidades_UndMedNom = new bool[] {false} ;
         T003C6_A1206ContratoUnidades_UndMedSigla = new String[] {""} ;
         T003C6_n1206ContratoUnidades_UndMedSigla = new bool[] {false} ;
         T003C6_A1208ContratoUnidades_Produtividade = new decimal[1] ;
         T003C6_n1208ContratoUnidades_Produtividade = new bool[] {false} ;
         T003C6_A1207ContratoUnidades_ContratoCod = new int[1] ;
         T003C6_A1204ContratoUnidades_UndMedCod = new int[1] ;
         T003C7_A1205ContratoUnidades_UndMedNom = new String[] {""} ;
         T003C7_n1205ContratoUnidades_UndMedNom = new bool[] {false} ;
         T003C7_A1206ContratoUnidades_UndMedSigla = new String[] {""} ;
         T003C7_n1206ContratoUnidades_UndMedSigla = new bool[] {false} ;
         T003C8_A1207ContratoUnidades_ContratoCod = new int[1] ;
         T003C8_A1204ContratoUnidades_UndMedCod = new int[1] ;
         T003C3_A1208ContratoUnidades_Produtividade = new decimal[1] ;
         T003C3_n1208ContratoUnidades_Produtividade = new bool[] {false} ;
         T003C3_A1207ContratoUnidades_ContratoCod = new int[1] ;
         T003C3_A1204ContratoUnidades_UndMedCod = new int[1] ;
         T003C9_A1207ContratoUnidades_ContratoCod = new int[1] ;
         T003C9_A1204ContratoUnidades_UndMedCod = new int[1] ;
         T003C10_A1207ContratoUnidades_ContratoCod = new int[1] ;
         T003C10_A1204ContratoUnidades_UndMedCod = new int[1] ;
         T003C2_A1208ContratoUnidades_Produtividade = new decimal[1] ;
         T003C2_n1208ContratoUnidades_Produtividade = new bool[] {false} ;
         T003C2_A1207ContratoUnidades_ContratoCod = new int[1] ;
         T003C2_A1204ContratoUnidades_UndMedCod = new int[1] ;
         T003C14_A1205ContratoUnidades_UndMedNom = new String[] {""} ;
         T003C14_n1205ContratoUnidades_UndMedNom = new bool[] {false} ;
         T003C14_A1206ContratoUnidades_UndMedSigla = new String[] {""} ;
         T003C14_n1206ContratoUnidades_UndMedSigla = new bool[] {false} ;
         T003C15_A1207ContratoUnidades_ContratoCod = new int[1] ;
         T003C15_A1204ContratoUnidades_UndMedCod = new int[1] ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         gxdynajaxctrlcodr = new GeneXus.Utils.GxStringCollection();
         gxdynajaxctrldescr = new GeneXus.Utils.GxStringCollection();
         gxwrpcisep = "";
         T003C16_A1204ContratoUnidades_UndMedCod = new int[1] ;
         T003C16_A1205ContratoUnidades_UndMedNom = new String[] {""} ;
         T003C16_n1205ContratoUnidades_UndMedNom = new bool[] {false} ;
         T003C16_A1199UnidadeMedicao_Ativo = new bool[] {false} ;
         T003C16_n1199UnidadeMedicao_Ativo = new bool[] {false} ;
         T003C17_A1205ContratoUnidades_UndMedNom = new String[] {""} ;
         T003C17_n1205ContratoUnidades_UndMedNom = new bool[] {false} ;
         T003C17_A1206ContratoUnidades_UndMedSigla = new String[] {""} ;
         T003C17_n1206ContratoUnidades_UndMedSigla = new bool[] {false} ;
         isValidOutput = new GxUnknownObjectCollection();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.contratounidades__default(),
            new Object[][] {
                new Object[] {
               T003C2_A1208ContratoUnidades_Produtividade, T003C2_n1208ContratoUnidades_Produtividade, T003C2_A1207ContratoUnidades_ContratoCod, T003C2_A1204ContratoUnidades_UndMedCod
               }
               , new Object[] {
               T003C3_A1208ContratoUnidades_Produtividade, T003C3_n1208ContratoUnidades_Produtividade, T003C3_A1207ContratoUnidades_ContratoCod, T003C3_A1204ContratoUnidades_UndMedCod
               }
               , new Object[] {
               T003C4_A1207ContratoUnidades_ContratoCod
               }
               , new Object[] {
               T003C5_A1205ContratoUnidades_UndMedNom, T003C5_n1205ContratoUnidades_UndMedNom, T003C5_A1206ContratoUnidades_UndMedSigla, T003C5_n1206ContratoUnidades_UndMedSigla
               }
               , new Object[] {
               T003C6_A1205ContratoUnidades_UndMedNom, T003C6_n1205ContratoUnidades_UndMedNom, T003C6_A1206ContratoUnidades_UndMedSigla, T003C6_n1206ContratoUnidades_UndMedSigla, T003C6_A1208ContratoUnidades_Produtividade, T003C6_n1208ContratoUnidades_Produtividade, T003C6_A1207ContratoUnidades_ContratoCod, T003C6_A1204ContratoUnidades_UndMedCod
               }
               , new Object[] {
               T003C7_A1205ContratoUnidades_UndMedNom, T003C7_n1205ContratoUnidades_UndMedNom, T003C7_A1206ContratoUnidades_UndMedSigla, T003C7_n1206ContratoUnidades_UndMedSigla
               }
               , new Object[] {
               T003C8_A1207ContratoUnidades_ContratoCod, T003C8_A1204ContratoUnidades_UndMedCod
               }
               , new Object[] {
               T003C9_A1207ContratoUnidades_ContratoCod, T003C9_A1204ContratoUnidades_UndMedCod
               }
               , new Object[] {
               T003C10_A1207ContratoUnidades_ContratoCod, T003C10_A1204ContratoUnidades_UndMedCod
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T003C14_A1205ContratoUnidades_UndMedNom, T003C14_n1205ContratoUnidades_UndMedNom, T003C14_A1206ContratoUnidades_UndMedSigla, T003C14_n1206ContratoUnidades_UndMedSigla
               }
               , new Object[] {
               T003C15_A1207ContratoUnidades_ContratoCod, T003C15_A1204ContratoUnidades_UndMedCod
               }
               , new Object[] {
               T003C16_A1204ContratoUnidades_UndMedCod, T003C16_A1205ContratoUnidades_UndMedNom, T003C16_n1205ContratoUnidades_UndMedNom, T003C16_A1199UnidadeMedicao_Ativo, T003C16_n1199UnidadeMedicao_Ativo
               }
               , new Object[] {
               T003C17_A1205ContratoUnidades_UndMedNom, T003C17_n1205ContratoUnidades_UndMedNom, T003C17_A1206ContratoUnidades_UndMedSigla, T003C17_n1206ContratoUnidades_UndMedSigla
               }
            }
         );
      }

      private short GxWebError ;
      private short gxcookieaux ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short RcdFound143 ;
      private short GX_JID ;
      private short Gx_BScreen ;
      private short gxajaxcallmode ;
      private short wbTemp ;
      private int wcpOAV7ContratoUnidades_ContratoCod ;
      private int wcpOAV13ContratoUnidades_UndMedCod ;
      private int Z1207ContratoUnidades_ContratoCod ;
      private int Z1204ContratoUnidades_UndMedCod ;
      private int A1207ContratoUnidades_ContratoCod ;
      private int A1204ContratoUnidades_UndMedCod ;
      private int AV7ContratoUnidades_ContratoCod ;
      private int AV13ContratoUnidades_UndMedCod ;
      private int trnEnded ;
      private int bttBtn_trn_enter_Visible ;
      private int bttBtn_trn_enter_Enabled ;
      private int bttBtn_trn_cancel_Visible ;
      private int bttBtn_trn_delete_Visible ;
      private int bttBtn_trn_delete_Enabled ;
      private int edtContratoUnidades_Produtividade_Enabled ;
      private int edtContratoUnidades_UndMedSigla_Enabled ;
      private int AV14SaldoContrato_Codigo ;
      private int idxLst ;
      private int gxdynajaxindex ;
      private decimal Z1208ContratoUnidades_Produtividade ;
      private decimal A1208ContratoUnidades_Produtividade ;
      private String sPrefix ;
      private String wcpOGx_mode ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String Gx_mode ;
      private String GXKey ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String dynContratoUnidades_UndMedCod_Internalname ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String tblTableactions_Internalname ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String bttBtn_trn_enter_Internalname ;
      private String bttBtn_trn_enter_Jsonclick ;
      private String bttBtn_trn_cancel_Internalname ;
      private String bttBtn_trn_cancel_Jsonclick ;
      private String bttBtn_trn_delete_Internalname ;
      private String bttBtn_trn_delete_Jsonclick ;
      private String tblTablecontent_Internalname ;
      private String tblUnnamedtable1_Internalname ;
      private String lblTextblockcontratounidades_undmedcod_Internalname ;
      private String lblTextblockcontratounidades_undmedcod_Jsonclick ;
      private String dynContratoUnidades_UndMedCod_Jsonclick ;
      private String lblTextblockcontratounidades_produtividade_Internalname ;
      private String lblTextblockcontratounidades_produtividade_Jsonclick ;
      private String tblTablemergedcontratounidades_produtividade_Internalname ;
      private String edtContratoUnidades_Produtividade_Internalname ;
      private String edtContratoUnidades_Produtividade_Jsonclick ;
      private String edtContratoUnidades_UndMedSigla_Internalname ;
      private String A1206ContratoUnidades_UndMedSigla ;
      private String edtContratoUnidades_UndMedSigla_Jsonclick ;
      private String A1205ContratoUnidades_UndMedNom ;
      private String forbiddenHiddens ;
      private String hsh ;
      private String sMode143 ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String Z1205ContratoUnidades_UndMedNom ;
      private String Z1206ContratoUnidades_UndMedSigla ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String gxwrpcisep ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbErr ;
      private bool n1208ContratoUnidades_Produtividade ;
      private bool n1206ContratoUnidades_UndMedSigla ;
      private bool n1205ContratoUnidades_UndMedNom ;
      private bool returnInSub ;
      private IGxSession AV10WebSession ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrlcodr ;
      private GeneXus.Utils.GxStringCollection gxdynajaxctrldescr ;
      private GxUnknownObjectCollection isValidOutput ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox dynContratoUnidades_UndMedCod ;
      private IDataStoreProvider pr_default ;
      private int[] T003C4_A1207ContratoUnidades_ContratoCod ;
      private String[] T003C5_A1205ContratoUnidades_UndMedNom ;
      private bool[] T003C5_n1205ContratoUnidades_UndMedNom ;
      private String[] T003C5_A1206ContratoUnidades_UndMedSigla ;
      private bool[] T003C5_n1206ContratoUnidades_UndMedSigla ;
      private String[] T003C6_A1205ContratoUnidades_UndMedNom ;
      private bool[] T003C6_n1205ContratoUnidades_UndMedNom ;
      private String[] T003C6_A1206ContratoUnidades_UndMedSigla ;
      private bool[] T003C6_n1206ContratoUnidades_UndMedSigla ;
      private decimal[] T003C6_A1208ContratoUnidades_Produtividade ;
      private bool[] T003C6_n1208ContratoUnidades_Produtividade ;
      private int[] T003C6_A1207ContratoUnidades_ContratoCod ;
      private int[] T003C6_A1204ContratoUnidades_UndMedCod ;
      private String[] T003C7_A1205ContratoUnidades_UndMedNom ;
      private bool[] T003C7_n1205ContratoUnidades_UndMedNom ;
      private String[] T003C7_A1206ContratoUnidades_UndMedSigla ;
      private bool[] T003C7_n1206ContratoUnidades_UndMedSigla ;
      private int[] T003C8_A1207ContratoUnidades_ContratoCod ;
      private int[] T003C8_A1204ContratoUnidades_UndMedCod ;
      private decimal[] T003C3_A1208ContratoUnidades_Produtividade ;
      private bool[] T003C3_n1208ContratoUnidades_Produtividade ;
      private int[] T003C3_A1207ContratoUnidades_ContratoCod ;
      private int[] T003C3_A1204ContratoUnidades_UndMedCod ;
      private int[] T003C9_A1207ContratoUnidades_ContratoCod ;
      private int[] T003C9_A1204ContratoUnidades_UndMedCod ;
      private int[] T003C10_A1207ContratoUnidades_ContratoCod ;
      private int[] T003C10_A1204ContratoUnidades_UndMedCod ;
      private decimal[] T003C2_A1208ContratoUnidades_Produtividade ;
      private bool[] T003C2_n1208ContratoUnidades_Produtividade ;
      private int[] T003C2_A1207ContratoUnidades_ContratoCod ;
      private int[] T003C2_A1204ContratoUnidades_UndMedCod ;
      private String[] T003C14_A1205ContratoUnidades_UndMedNom ;
      private bool[] T003C14_n1205ContratoUnidades_UndMedNom ;
      private String[] T003C14_A1206ContratoUnidades_UndMedSigla ;
      private bool[] T003C14_n1206ContratoUnidades_UndMedSigla ;
      private int[] T003C15_A1207ContratoUnidades_ContratoCod ;
      private int[] T003C15_A1204ContratoUnidades_UndMedCod ;
      private int[] T003C16_A1204ContratoUnidades_UndMedCod ;
      private String[] T003C16_A1205ContratoUnidades_UndMedNom ;
      private bool[] T003C16_n1205ContratoUnidades_UndMedNom ;
      private bool[] T003C16_A1199UnidadeMedicao_Ativo ;
      private bool[] T003C16_n1199UnidadeMedicao_Ativo ;
      private String[] T003C17_A1205ContratoUnidades_UndMedNom ;
      private bool[] T003C17_n1205ContratoUnidades_UndMedNom ;
      private String[] T003C17_A1206ContratoUnidades_UndMedSigla ;
      private bool[] T003C17_n1206ContratoUnidades_UndMedSigla ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV8WWPContext ;
      private wwpbaseobjects.SdtWWPTransactionContext AV9TrnContext ;
   }

   public class contratounidades__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new UpdateCursor(def[9])
         ,new UpdateCursor(def[10])
         ,new UpdateCursor(def[11])
         ,new ForEachCursor(def[12])
         ,new ForEachCursor(def[13])
         ,new ForEachCursor(def[14])
         ,new ForEachCursor(def[15])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT003C4 ;
          prmT003C4 = new Object[] {
          new Object[] {"@ContratoUnidades_ContratoCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003C6 ;
          prmT003C6 = new Object[] {
          new Object[] {"@ContratoUnidades_ContratoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoUnidades_UndMedCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003C5 ;
          prmT003C5 = new Object[] {
          new Object[] {"@ContratoUnidades_UndMedCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003C7 ;
          prmT003C7 = new Object[] {
          new Object[] {"@ContratoUnidades_UndMedCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003C8 ;
          prmT003C8 = new Object[] {
          new Object[] {"@ContratoUnidades_ContratoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoUnidades_UndMedCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003C3 ;
          prmT003C3 = new Object[] {
          new Object[] {"@ContratoUnidades_ContratoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoUnidades_UndMedCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003C9 ;
          prmT003C9 = new Object[] {
          new Object[] {"@ContratoUnidades_ContratoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoUnidades_UndMedCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003C10 ;
          prmT003C10 = new Object[] {
          new Object[] {"@ContratoUnidades_ContratoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoUnidades_UndMedCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003C2 ;
          prmT003C2 = new Object[] {
          new Object[] {"@ContratoUnidades_ContratoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoUnidades_UndMedCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003C11 ;
          prmT003C11 = new Object[] {
          new Object[] {"@ContratoUnidades_Produtividade",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContratoUnidades_ContratoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoUnidades_UndMedCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003C12 ;
          prmT003C12 = new Object[] {
          new Object[] {"@ContratoUnidades_Produtividade",SqlDbType.Decimal,14,5} ,
          new Object[] {"@ContratoUnidades_ContratoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoUnidades_UndMedCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003C13 ;
          prmT003C13 = new Object[] {
          new Object[] {"@ContratoUnidades_ContratoCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratoUnidades_UndMedCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003C14 ;
          prmT003C14 = new Object[] {
          new Object[] {"@ContratoUnidades_UndMedCod",SqlDbType.Int,6,0}
          } ;
          Object[] prmT003C15 ;
          prmT003C15 = new Object[] {
          } ;
          Object[] prmT003C16 ;
          prmT003C16 = new Object[] {
          } ;
          Object[] prmT003C17 ;
          prmT003C17 = new Object[] {
          new Object[] {"@ContratoUnidades_UndMedCod",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("T003C2", "SELECT [ContratoUnidades_Produtividade], [ContratoUnidades_ContratoCod] AS ContratoUnidades_ContratoCod, [ContratoUnidades_UndMedCod] AS ContratoUnidades_UndMedCod FROM [ContratoUnidades] WITH (UPDLOCK) WHERE [ContratoUnidades_ContratoCod] = @ContratoUnidades_ContratoCod AND [ContratoUnidades_UndMedCod] = @ContratoUnidades_UndMedCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT003C2,1,0,true,false )
             ,new CursorDef("T003C3", "SELECT [ContratoUnidades_Produtividade], [ContratoUnidades_ContratoCod] AS ContratoUnidades_ContratoCod, [ContratoUnidades_UndMedCod] AS ContratoUnidades_UndMedCod FROM [ContratoUnidades] WITH (NOLOCK) WHERE [ContratoUnidades_ContratoCod] = @ContratoUnidades_ContratoCod AND [ContratoUnidades_UndMedCod] = @ContratoUnidades_UndMedCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT003C3,1,0,true,false )
             ,new CursorDef("T003C4", "SELECT [Contrato_Codigo] AS ContratoUnidades_ContratoCod FROM [Contrato] WITH (NOLOCK) WHERE [Contrato_Codigo] = @ContratoUnidades_ContratoCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT003C4,1,0,true,false )
             ,new CursorDef("T003C5", "SELECT [UnidadeMedicao_Nome] AS ContratoUnidades_UndMedNom, [UnidadeMedicao_Sigla] AS ContratoUnidades_UndMedSigla FROM [UnidadeMedicao] WITH (NOLOCK) WHERE [UnidadeMedicao_Codigo] = @ContratoUnidades_UndMedCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT003C5,1,0,true,false )
             ,new CursorDef("T003C6", "SELECT T2.[UnidadeMedicao_Nome] AS ContratoUnidades_UndMedNom, T2.[UnidadeMedicao_Sigla] AS ContratoUnidades_UndMedSigla, TM1.[ContratoUnidades_Produtividade], TM1.[ContratoUnidades_ContratoCod] AS ContratoUnidades_ContratoCod, TM1.[ContratoUnidades_UndMedCod] AS ContratoUnidades_UndMedCod FROM ([ContratoUnidades] TM1 WITH (NOLOCK) INNER JOIN [UnidadeMedicao] T2 WITH (NOLOCK) ON T2.[UnidadeMedicao_Codigo] = TM1.[ContratoUnidades_UndMedCod]) WHERE TM1.[ContratoUnidades_ContratoCod] = @ContratoUnidades_ContratoCod and TM1.[ContratoUnidades_UndMedCod] = @ContratoUnidades_UndMedCod ORDER BY TM1.[ContratoUnidades_ContratoCod], TM1.[ContratoUnidades_UndMedCod]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT003C6,100,0,true,false )
             ,new CursorDef("T003C7", "SELECT [UnidadeMedicao_Nome] AS ContratoUnidades_UndMedNom, [UnidadeMedicao_Sigla] AS ContratoUnidades_UndMedSigla FROM [UnidadeMedicao] WITH (NOLOCK) WHERE [UnidadeMedicao_Codigo] = @ContratoUnidades_UndMedCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT003C7,1,0,true,false )
             ,new CursorDef("T003C8", "SELECT [ContratoUnidades_ContratoCod] AS ContratoUnidades_ContratoCod, [ContratoUnidades_UndMedCod] AS ContratoUnidades_UndMedCod FROM [ContratoUnidades] WITH (NOLOCK) WHERE [ContratoUnidades_ContratoCod] = @ContratoUnidades_ContratoCod AND [ContratoUnidades_UndMedCod] = @ContratoUnidades_UndMedCod  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT003C8,1,0,true,false )
             ,new CursorDef("T003C9", "SELECT TOP 1 [ContratoUnidades_ContratoCod] AS ContratoUnidades_ContratoCod, [ContratoUnidades_UndMedCod] AS ContratoUnidades_UndMedCod FROM [ContratoUnidades] WITH (NOLOCK) WHERE ( [ContratoUnidades_ContratoCod] > @ContratoUnidades_ContratoCod or [ContratoUnidades_ContratoCod] = @ContratoUnidades_ContratoCod and [ContratoUnidades_UndMedCod] > @ContratoUnidades_UndMedCod) ORDER BY [ContratoUnidades_ContratoCod], [ContratoUnidades_UndMedCod]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT003C9,1,0,true,true )
             ,new CursorDef("T003C10", "SELECT TOP 1 [ContratoUnidades_ContratoCod] AS ContratoUnidades_ContratoCod, [ContratoUnidades_UndMedCod] AS ContratoUnidades_UndMedCod FROM [ContratoUnidades] WITH (NOLOCK) WHERE ( [ContratoUnidades_ContratoCod] < @ContratoUnidades_ContratoCod or [ContratoUnidades_ContratoCod] = @ContratoUnidades_ContratoCod and [ContratoUnidades_UndMedCod] < @ContratoUnidades_UndMedCod) ORDER BY [ContratoUnidades_ContratoCod] DESC, [ContratoUnidades_UndMedCod] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT003C10,1,0,true,true )
             ,new CursorDef("T003C11", "INSERT INTO [ContratoUnidades]([ContratoUnidades_Produtividade], [ContratoUnidades_ContratoCod], [ContratoUnidades_UndMedCod]) VALUES(@ContratoUnidades_Produtividade, @ContratoUnidades_ContratoCod, @ContratoUnidades_UndMedCod)", GxErrorMask.GX_NOMASK,prmT003C11)
             ,new CursorDef("T003C12", "UPDATE [ContratoUnidades] SET [ContratoUnidades_Produtividade]=@ContratoUnidades_Produtividade  WHERE [ContratoUnidades_ContratoCod] = @ContratoUnidades_ContratoCod AND [ContratoUnidades_UndMedCod] = @ContratoUnidades_UndMedCod", GxErrorMask.GX_NOMASK,prmT003C12)
             ,new CursorDef("T003C13", "DELETE FROM [ContratoUnidades]  WHERE [ContratoUnidades_ContratoCod] = @ContratoUnidades_ContratoCod AND [ContratoUnidades_UndMedCod] = @ContratoUnidades_UndMedCod", GxErrorMask.GX_NOMASK,prmT003C13)
             ,new CursorDef("T003C14", "SELECT [UnidadeMedicao_Nome] AS ContratoUnidades_UndMedNom, [UnidadeMedicao_Sigla] AS ContratoUnidades_UndMedSigla FROM [UnidadeMedicao] WITH (NOLOCK) WHERE [UnidadeMedicao_Codigo] = @ContratoUnidades_UndMedCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT003C14,1,0,true,false )
             ,new CursorDef("T003C15", "SELECT [ContratoUnidades_ContratoCod] AS ContratoUnidades_ContratoCod, [ContratoUnidades_UndMedCod] AS ContratoUnidades_UndMedCod FROM [ContratoUnidades] WITH (NOLOCK) ORDER BY [ContratoUnidades_ContratoCod], [ContratoUnidades_UndMedCod]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT003C15,100,0,true,false )
             ,new CursorDef("T003C16", "SELECT [UnidadeMedicao_Codigo] AS ContratoUnidades_UndMedCod, [UnidadeMedicao_Nome] AS ContratoUnidades_UndMedNom, [UnidadeMedicao_Ativo] FROM [UnidadeMedicao] WITH (NOLOCK) WHERE [UnidadeMedicao_Ativo] = 1 ORDER BY [UnidadeMedicao_Nome] ",true, GxErrorMask.GX_NOMASK, false, this,prmT003C16,0,0,true,false )
             ,new CursorDef("T003C17", "SELECT [UnidadeMedicao_Nome] AS ContratoUnidades_UndMedNom, [UnidadeMedicao_Sigla] AS ContratoUnidades_UndMedSigla FROM [UnidadeMedicao] WITH (NOLOCK) WHERE [UnidadeMedicao_Codigo] = @ContratoUnidades_UndMedCod ",true, GxErrorMask.GX_NOMASK, false, this,prmT003C17,1,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((decimal[]) buf[0])[0] = rslt.getDecimal(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                return;
             case 1 :
                ((decimal[]) buf[0])[0] = rslt.getDecimal(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 3 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 15) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 4 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 15) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((decimal[]) buf[4])[0] = rslt.getDecimal(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                ((int[]) buf[7])[0] = rslt.getInt(5) ;
                return;
             case 5 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 15) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 6 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 7 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 8 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 12 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 15) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
             case 13 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 14 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((bool[]) buf[3])[0] = rslt.getBool(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                return;
             case 15 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 15) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 5 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 6 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 7 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 8 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 9 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(1, (decimal)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                stmt.SetParameter(3, (int)parms[3]);
                return;
             case 10 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(1, (decimal)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                stmt.SetParameter(3, (int)parms[3]);
                return;
             case 11 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                return;
             case 12 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 15 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
       }
    }

 }

}
