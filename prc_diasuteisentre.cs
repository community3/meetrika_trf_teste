/*
               File: PRC_DiasUteisEntre
        Description: Dias Uteis Entre
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:51:45.8
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_diasuteisentre : GXProcedure
   {
      public prc_diasuteisentre( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_diasuteisentre( IGxContext context )
      {
         this.context = context;
         IsMain = false;
      }

      public void release( )
      {
      }

      public void execute( DateTime aP0_DataInicio ,
                           DateTime aP1_DataFim ,
                           String aP2_TipoDias ,
                           out short aP3_Dias )
      {
         this.AV8DataInicio = aP0_DataInicio;
         this.AV9DataFim = aP1_DataFim;
         this.AV15TipoDias = aP2_TipoDias;
         this.AV10Dias = 0 ;
         initialize();
         executePrivate();
         aP3_Dias=this.AV10Dias;
      }

      public short executeUdp( DateTime aP0_DataInicio ,
                               DateTime aP1_DataFim ,
                               String aP2_TipoDias )
      {
         this.AV8DataInicio = aP0_DataInicio;
         this.AV9DataFim = aP1_DataFim;
         this.AV15TipoDias = aP2_TipoDias;
         this.AV10Dias = 0 ;
         initialize();
         executePrivate();
         aP3_Dias=this.AV10Dias;
         return AV10Dias ;
      }

      public void executeSubmit( DateTime aP0_DataInicio ,
                                 DateTime aP1_DataFim ,
                                 String aP2_TipoDias ,
                                 out short aP3_Dias )
      {
         prc_diasuteisentre objprc_diasuteisentre;
         objprc_diasuteisentre = new prc_diasuteisentre();
         objprc_diasuteisentre.AV8DataInicio = aP0_DataInicio;
         objprc_diasuteisentre.AV9DataFim = aP1_DataFim;
         objprc_diasuteisentre.AV15TipoDias = aP2_TipoDias;
         objprc_diasuteisentre.AV10Dias = 0 ;
         objprc_diasuteisentre.context.SetSubmitInitialConfig(context);
         objprc_diasuteisentre.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_diasuteisentre);
         aP3_Dias=this.AV10Dias;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_diasuteisentre)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         if ( (DateTime.MinValue==AV8DataInicio) )
         {
            AV10Dias = (short)(DateTimeUtil.DDiff(AV9DataFim,DateTimeUtil.ServerDate( context, "DEFAULT")));
         }
         else
         {
            AV10Dias = (short)(DateTimeUtil.DDiff(AV9DataFim,AV8DataInicio));
         }
         AV12f = AV10Dias;
         if ( AV12f <= 0 )
         {
            AV10Dias = 0;
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(AV15TipoDias, "U") == 0 )
         {
            AV13Date = AV9DataFim;
            AV11i = 1;
            while ( AV11i <= AV12f )
            {
               GXt_boolean1 = AV14Feriado;
               GXt_dtime2 = DateTimeUtil.ResetTime( AV13Date ) ;
               new prc_ehferiado(context ).execute( ref  GXt_dtime2, out  GXt_boolean1) ;
               AV13Date = DateTimeUtil.ResetTime((DateTime)(GXt_dtime2));
               AV14Feriado = GXt_boolean1;
               if ( AV14Feriado || ( DateTimeUtil.Dow( AV13Date) == 1 ) || ( DateTimeUtil.Dow( AV13Date) == 7 ) )
               {
                  AV10Dias = (short)(AV10Dias-1);
               }
               AV13Date = DateTimeUtil.DAdd(AV13Date,-((int)(1)));
               AV11i = (short)(AV11i+1);
            }
         }
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV13Date = DateTime.MinValue;
         GXt_dtime2 = (DateTime)(DateTime.MinValue);
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV10Dias ;
      private short AV12f ;
      private short AV11i ;
      private String AV15TipoDias ;
      private DateTime GXt_dtime2 ;
      private DateTime AV8DataInicio ;
      private DateTime AV9DataFim ;
      private DateTime AV13Date ;
      private bool AV14Feriado ;
      private bool GXt_boolean1 ;
      private short aP3_Dias ;
   }

}
