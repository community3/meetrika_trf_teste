/*
               File: GetPromptContratadaFilterData
        Description: Get Prompt Contratada Filter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:53:10.26
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getpromptcontratadafilterdata : GXProcedure
   {
      public getpromptcontratadafilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getpromptcontratadafilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV17DDOName = aP0_DDOName;
         this.AV15SearchTxt = aP1_SearchTxt;
         this.AV16SearchTxtTo = aP2_SearchTxtTo;
         this.AV21OptionsJson = "" ;
         this.AV24OptionsDescJson = "" ;
         this.AV26OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV21OptionsJson;
         aP4_OptionsDescJson=this.AV24OptionsDescJson;
         aP5_OptionIndexesJson=this.AV26OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV17DDOName = aP0_DDOName;
         this.AV15SearchTxt = aP1_SearchTxt;
         this.AV16SearchTxtTo = aP2_SearchTxtTo;
         this.AV21OptionsJson = "" ;
         this.AV24OptionsDescJson = "" ;
         this.AV26OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV21OptionsJson;
         aP4_OptionsDescJson=this.AV24OptionsDescJson;
         aP5_OptionIndexesJson=this.AV26OptionIndexesJson;
         return AV26OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getpromptcontratadafilterdata objgetpromptcontratadafilterdata;
         objgetpromptcontratadafilterdata = new getpromptcontratadafilterdata();
         objgetpromptcontratadafilterdata.AV17DDOName = aP0_DDOName;
         objgetpromptcontratadafilterdata.AV15SearchTxt = aP1_SearchTxt;
         objgetpromptcontratadafilterdata.AV16SearchTxtTo = aP2_SearchTxtTo;
         objgetpromptcontratadafilterdata.AV21OptionsJson = "" ;
         objgetpromptcontratadafilterdata.AV24OptionsDescJson = "" ;
         objgetpromptcontratadafilterdata.AV26OptionIndexesJson = "" ;
         objgetpromptcontratadafilterdata.context.SetSubmitInitialConfig(context);
         objgetpromptcontratadafilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetpromptcontratadafilterdata);
         aP3_OptionsJson=this.AV21OptionsJson;
         aP4_OptionsDescJson=this.AV24OptionsDescJson;
         aP5_OptionIndexesJson=this.AV26OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getpromptcontratadafilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV20Options = (IGxCollection)(new GxSimpleCollection());
         AV23OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV25OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV17DDOName), "DDO_CONTRATADA_PESSOANOM") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTRATADA_PESSOANOMOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV17DDOName), "DDO_CONTRATADA_PESSOACNPJ") == 0 )
         {
            /* Execute user subroutine: 'LOADCONTRATADA_PESSOACNPJOPTIONS' */
            S131 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV21OptionsJson = AV20Options.ToJSonString(false);
         AV24OptionsDescJson = AV23OptionsDesc.ToJSonString(false);
         AV26OptionIndexesJson = AV25OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV28Session.Get("PromptContratadaGridState"), "") == 0 )
         {
            AV30GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "PromptContratadaGridState"), "");
         }
         else
         {
            AV30GridState.FromXml(AV28Session.Get("PromptContratadaGridState"), "");
         }
         AV45GXV1 = 1;
         while ( AV45GXV1 <= AV30GridState.gxTpr_Filtervalues.Count )
         {
            AV31GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV30GridState.gxTpr_Filtervalues.Item(AV45GXV1));
            if ( StringUtil.StrCmp(AV31GridStateFilterValue.gxTpr_Name, "CONTRATADA_AREATRABALHOCOD") == 0 )
            {
               AV33Contratada_AreaTrabalhoCod = (int)(NumberUtil.Val( AV31GridStateFilterValue.gxTpr_Value, "."));
            }
            else if ( StringUtil.StrCmp(AV31GridStateFilterValue.gxTpr_Name, "TFCONTRATADA_PESSOANOM") == 0 )
            {
               AV10TFContratada_PessoaNom = AV31GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV31GridStateFilterValue.gxTpr_Name, "TFCONTRATADA_PESSOANOM_SEL") == 0 )
            {
               AV11TFContratada_PessoaNom_Sel = AV31GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV31GridStateFilterValue.gxTpr_Name, "TFCONTRATADA_PESSOACNPJ") == 0 )
            {
               AV12TFContratada_PessoaCNPJ = AV31GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV31GridStateFilterValue.gxTpr_Name, "TFCONTRATADA_PESSOACNPJ_SEL") == 0 )
            {
               AV13TFContratada_PessoaCNPJ_Sel = AV31GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV31GridStateFilterValue.gxTpr_Name, "TFCONTRATADA_ATIVO_SEL") == 0 )
            {
               AV14TFContratada_Ativo_Sel = (short)(NumberUtil.Val( AV31GridStateFilterValue.gxTpr_Value, "."));
            }
            AV45GXV1 = (int)(AV45GXV1+1);
         }
         if ( AV30GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV32GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV30GridState.gxTpr_Dynamicfilters.Item(1));
            AV34DynamicFiltersSelector1 = AV32GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV34DynamicFiltersSelector1, "CONTRATADA_PESSOANOM") == 0 )
            {
               AV35DynamicFiltersOperator1 = AV32GridStateDynamicFilter.gxTpr_Operator;
               AV36Contratada_PessoaNom1 = AV32GridStateDynamicFilter.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV34DynamicFiltersSelector1, "CONTRATADA_CODIGO") == 0 )
            {
               AV35DynamicFiltersOperator1 = AV32GridStateDynamicFilter.gxTpr_Operator;
               AV37Contratada_Codigo1 = (int)(NumberUtil.Val( AV32GridStateDynamicFilter.gxTpr_Value, "."));
            }
            if ( AV30GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV38DynamicFiltersEnabled2 = true;
               AV32GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV30GridState.gxTpr_Dynamicfilters.Item(2));
               AV39DynamicFiltersSelector2 = AV32GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV39DynamicFiltersSelector2, "CONTRATADA_PESSOANOM") == 0 )
               {
                  AV40DynamicFiltersOperator2 = AV32GridStateDynamicFilter.gxTpr_Operator;
                  AV41Contratada_PessoaNom2 = AV32GridStateDynamicFilter.gxTpr_Value;
               }
               else if ( StringUtil.StrCmp(AV39DynamicFiltersSelector2, "CONTRATADA_CODIGO") == 0 )
               {
                  AV40DynamicFiltersOperator2 = AV32GridStateDynamicFilter.gxTpr_Operator;
                  AV42Contratada_Codigo2 = (int)(NumberUtil.Val( AV32GridStateDynamicFilter.gxTpr_Value, "."));
               }
            }
         }
      }

      protected void S121( )
      {
         /* 'LOADCONTRATADA_PESSOANOMOPTIONS' Routine */
         AV10TFContratada_PessoaNom = AV15SearchTxt;
         AV11TFContratada_PessoaNom_Sel = "";
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              AV34DynamicFiltersSelector1 ,
                                              AV35DynamicFiltersOperator1 ,
                                              AV36Contratada_PessoaNom1 ,
                                              AV37Contratada_Codigo1 ,
                                              AV38DynamicFiltersEnabled2 ,
                                              AV39DynamicFiltersSelector2 ,
                                              AV40DynamicFiltersOperator2 ,
                                              AV41Contratada_PessoaNom2 ,
                                              AV42Contratada_Codigo2 ,
                                              AV11TFContratada_PessoaNom_Sel ,
                                              AV10TFContratada_PessoaNom ,
                                              AV13TFContratada_PessoaCNPJ_Sel ,
                                              AV12TFContratada_PessoaCNPJ ,
                                              AV14TFContratada_Ativo_Sel ,
                                              A41Contratada_PessoaNom ,
                                              A39Contratada_Codigo ,
                                              A42Contratada_PessoaCNPJ ,
                                              A43Contratada_Ativo ,
                                              A52Contratada_AreaTrabalhoCod ,
                                              AV9WWPContext.gxTpr_Areatrabalho_codigo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN,
                                              TypeConstants.INT, TypeConstants.INT
                                              }
         });
         lV36Contratada_PessoaNom1 = StringUtil.PadR( StringUtil.RTrim( AV36Contratada_PessoaNom1), 100, "%");
         lV36Contratada_PessoaNom1 = StringUtil.PadR( StringUtil.RTrim( AV36Contratada_PessoaNom1), 100, "%");
         lV41Contratada_PessoaNom2 = StringUtil.PadR( StringUtil.RTrim( AV41Contratada_PessoaNom2), 100, "%");
         lV41Contratada_PessoaNom2 = StringUtil.PadR( StringUtil.RTrim( AV41Contratada_PessoaNom2), 100, "%");
         lV10TFContratada_PessoaNom = StringUtil.PadR( StringUtil.RTrim( AV10TFContratada_PessoaNom), 100, "%");
         lV12TFContratada_PessoaCNPJ = StringUtil.Concat( StringUtil.RTrim( AV12TFContratada_PessoaCNPJ), "%", "");
         /* Using cursor P00FB2 */
         pr_default.execute(0, new Object[] {AV9WWPContext.gxTpr_Areatrabalho_codigo, lV36Contratada_PessoaNom1, lV36Contratada_PessoaNom1, AV37Contratada_Codigo1, lV41Contratada_PessoaNom2, lV41Contratada_PessoaNom2, AV42Contratada_Codigo2, lV10TFContratada_PessoaNom, AV11TFContratada_PessoaNom_Sel, lV12TFContratada_PessoaCNPJ, AV13TFContratada_PessoaCNPJ_Sel});
         while ( (pr_default.getStatus(0) != 101) )
         {
            BRKFB2 = false;
            A40Contratada_PessoaCod = P00FB2_A40Contratada_PessoaCod[0];
            A43Contratada_Ativo = P00FB2_A43Contratada_Ativo[0];
            A42Contratada_PessoaCNPJ = P00FB2_A42Contratada_PessoaCNPJ[0];
            n42Contratada_PessoaCNPJ = P00FB2_n42Contratada_PessoaCNPJ[0];
            A39Contratada_Codigo = P00FB2_A39Contratada_Codigo[0];
            A41Contratada_PessoaNom = P00FB2_A41Contratada_PessoaNom[0];
            n41Contratada_PessoaNom = P00FB2_n41Contratada_PessoaNom[0];
            A52Contratada_AreaTrabalhoCod = P00FB2_A52Contratada_AreaTrabalhoCod[0];
            A42Contratada_PessoaCNPJ = P00FB2_A42Contratada_PessoaCNPJ[0];
            n42Contratada_PessoaCNPJ = P00FB2_n42Contratada_PessoaCNPJ[0];
            A41Contratada_PessoaNom = P00FB2_A41Contratada_PessoaNom[0];
            n41Contratada_PessoaNom = P00FB2_n41Contratada_PessoaNom[0];
            AV27count = 0;
            while ( (pr_default.getStatus(0) != 101) && ( P00FB2_A40Contratada_PessoaCod[0] == A40Contratada_PessoaCod ) )
            {
               BRKFB2 = false;
               A39Contratada_Codigo = P00FB2_A39Contratada_Codigo[0];
               AV27count = (long)(AV27count+1);
               BRKFB2 = true;
               pr_default.readNext(0);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A41Contratada_PessoaNom)) )
            {
               AV19Option = A41Contratada_PessoaNom;
               AV18InsertIndex = 1;
               while ( ( AV18InsertIndex <= AV20Options.Count ) && ( StringUtil.StrCmp(((String)AV20Options.Item(AV18InsertIndex)), AV19Option) < 0 ) )
               {
                  AV18InsertIndex = (int)(AV18InsertIndex+1);
               }
               AV20Options.Add(AV19Option, AV18InsertIndex);
               AV25OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV27count), "Z,ZZZ,ZZZ,ZZ9")), AV18InsertIndex);
            }
            if ( AV20Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKFB2 )
            {
               BRKFB2 = true;
               pr_default.readNext(0);
            }
         }
         pr_default.close(0);
      }

      protected void S131( )
      {
         /* 'LOADCONTRATADA_PESSOACNPJOPTIONS' Routine */
         AV12TFContratada_PessoaCNPJ = AV15SearchTxt;
         AV13TFContratada_PessoaCNPJ_Sel = "";
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV34DynamicFiltersSelector1 ,
                                              AV35DynamicFiltersOperator1 ,
                                              AV36Contratada_PessoaNom1 ,
                                              AV37Contratada_Codigo1 ,
                                              AV38DynamicFiltersEnabled2 ,
                                              AV39DynamicFiltersSelector2 ,
                                              AV40DynamicFiltersOperator2 ,
                                              AV41Contratada_PessoaNom2 ,
                                              AV42Contratada_Codigo2 ,
                                              AV11TFContratada_PessoaNom_Sel ,
                                              AV10TFContratada_PessoaNom ,
                                              AV13TFContratada_PessoaCNPJ_Sel ,
                                              AV12TFContratada_PessoaCNPJ ,
                                              AV14TFContratada_Ativo_Sel ,
                                              A41Contratada_PessoaNom ,
                                              A39Contratada_Codigo ,
                                              A42Contratada_PessoaCNPJ ,
                                              A43Contratada_Ativo ,
                                              A52Contratada_AreaTrabalhoCod ,
                                              AV9WWPContext.gxTpr_Areatrabalho_codigo },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.INT, TypeConstants.STRING,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.SHORT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.INT, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN,
                                              TypeConstants.INT, TypeConstants.INT
                                              }
         });
         lV36Contratada_PessoaNom1 = StringUtil.PadR( StringUtil.RTrim( AV36Contratada_PessoaNom1), 100, "%");
         lV36Contratada_PessoaNom1 = StringUtil.PadR( StringUtil.RTrim( AV36Contratada_PessoaNom1), 100, "%");
         lV41Contratada_PessoaNom2 = StringUtil.PadR( StringUtil.RTrim( AV41Contratada_PessoaNom2), 100, "%");
         lV41Contratada_PessoaNom2 = StringUtil.PadR( StringUtil.RTrim( AV41Contratada_PessoaNom2), 100, "%");
         lV10TFContratada_PessoaNom = StringUtil.PadR( StringUtil.RTrim( AV10TFContratada_PessoaNom), 100, "%");
         lV12TFContratada_PessoaCNPJ = StringUtil.Concat( StringUtil.RTrim( AV12TFContratada_PessoaCNPJ), "%", "");
         /* Using cursor P00FB3 */
         pr_default.execute(1, new Object[] {AV9WWPContext.gxTpr_Areatrabalho_codigo, lV36Contratada_PessoaNom1, lV36Contratada_PessoaNom1, AV37Contratada_Codigo1, lV41Contratada_PessoaNom2, lV41Contratada_PessoaNom2, AV42Contratada_Codigo2, lV10TFContratada_PessoaNom, AV11TFContratada_PessoaNom_Sel, lV12TFContratada_PessoaCNPJ, AV13TFContratada_PessoaCNPJ_Sel});
         while ( (pr_default.getStatus(1) != 101) )
         {
            BRKFB4 = false;
            A40Contratada_PessoaCod = P00FB3_A40Contratada_PessoaCod[0];
            A52Contratada_AreaTrabalhoCod = P00FB3_A52Contratada_AreaTrabalhoCod[0];
            A42Contratada_PessoaCNPJ = P00FB3_A42Contratada_PessoaCNPJ[0];
            n42Contratada_PessoaCNPJ = P00FB3_n42Contratada_PessoaCNPJ[0];
            A43Contratada_Ativo = P00FB3_A43Contratada_Ativo[0];
            A39Contratada_Codigo = P00FB3_A39Contratada_Codigo[0];
            A41Contratada_PessoaNom = P00FB3_A41Contratada_PessoaNom[0];
            n41Contratada_PessoaNom = P00FB3_n41Contratada_PessoaNom[0];
            A42Contratada_PessoaCNPJ = P00FB3_A42Contratada_PessoaCNPJ[0];
            n42Contratada_PessoaCNPJ = P00FB3_n42Contratada_PessoaCNPJ[0];
            A41Contratada_PessoaNom = P00FB3_A41Contratada_PessoaNom[0];
            n41Contratada_PessoaNom = P00FB3_n41Contratada_PessoaNom[0];
            AV27count = 0;
            while ( (pr_default.getStatus(1) != 101) && ( StringUtil.StrCmp(P00FB3_A42Contratada_PessoaCNPJ[0], A42Contratada_PessoaCNPJ) == 0 ) )
            {
               BRKFB4 = false;
               A40Contratada_PessoaCod = P00FB3_A40Contratada_PessoaCod[0];
               A39Contratada_Codigo = P00FB3_A39Contratada_Codigo[0];
               AV27count = (long)(AV27count+1);
               BRKFB4 = true;
               pr_default.readNext(1);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A42Contratada_PessoaCNPJ)) )
            {
               AV19Option = A42Contratada_PessoaCNPJ;
               AV20Options.Add(AV19Option, 0);
               AV25OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV27count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV20Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKFB4 )
            {
               BRKFB4 = true;
               pr_default.readNext(1);
            }
         }
         pr_default.close(1);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV20Options = new GxSimpleCollection();
         AV23OptionsDesc = new GxSimpleCollection();
         AV25OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV28Session = context.GetSession();
         AV30GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV31GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV10TFContratada_PessoaNom = "";
         AV11TFContratada_PessoaNom_Sel = "";
         AV12TFContratada_PessoaCNPJ = "";
         AV13TFContratada_PessoaCNPJ_Sel = "";
         AV32GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV34DynamicFiltersSelector1 = "";
         AV36Contratada_PessoaNom1 = "";
         AV39DynamicFiltersSelector2 = "";
         AV41Contratada_PessoaNom2 = "";
         scmdbuf = "";
         lV36Contratada_PessoaNom1 = "";
         lV41Contratada_PessoaNom2 = "";
         lV10TFContratada_PessoaNom = "";
         lV12TFContratada_PessoaCNPJ = "";
         A41Contratada_PessoaNom = "";
         A42Contratada_PessoaCNPJ = "";
         P00FB2_A40Contratada_PessoaCod = new int[1] ;
         P00FB2_A43Contratada_Ativo = new bool[] {false} ;
         P00FB2_A42Contratada_PessoaCNPJ = new String[] {""} ;
         P00FB2_n42Contratada_PessoaCNPJ = new bool[] {false} ;
         P00FB2_A39Contratada_Codigo = new int[1] ;
         P00FB2_A41Contratada_PessoaNom = new String[] {""} ;
         P00FB2_n41Contratada_PessoaNom = new bool[] {false} ;
         P00FB2_A52Contratada_AreaTrabalhoCod = new int[1] ;
         AV19Option = "";
         P00FB3_A40Contratada_PessoaCod = new int[1] ;
         P00FB3_A52Contratada_AreaTrabalhoCod = new int[1] ;
         P00FB3_A42Contratada_PessoaCNPJ = new String[] {""} ;
         P00FB3_n42Contratada_PessoaCNPJ = new bool[] {false} ;
         P00FB3_A43Contratada_Ativo = new bool[] {false} ;
         P00FB3_A39Contratada_Codigo = new int[1] ;
         P00FB3_A41Contratada_PessoaNom = new String[] {""} ;
         P00FB3_n41Contratada_PessoaNom = new bool[] {false} ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getpromptcontratadafilterdata__default(),
            new Object[][] {
                new Object[] {
               P00FB2_A40Contratada_PessoaCod, P00FB2_A43Contratada_Ativo, P00FB2_A42Contratada_PessoaCNPJ, P00FB2_n42Contratada_PessoaCNPJ, P00FB2_A39Contratada_Codigo, P00FB2_A41Contratada_PessoaNom, P00FB2_n41Contratada_PessoaNom, P00FB2_A52Contratada_AreaTrabalhoCod
               }
               , new Object[] {
               P00FB3_A40Contratada_PessoaCod, P00FB3_A52Contratada_AreaTrabalhoCod, P00FB3_A42Contratada_PessoaCNPJ, P00FB3_n42Contratada_PessoaCNPJ, P00FB3_A43Contratada_Ativo, P00FB3_A39Contratada_Codigo, P00FB3_A41Contratada_PessoaNom, P00FB3_n41Contratada_PessoaNom
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV14TFContratada_Ativo_Sel ;
      private short AV35DynamicFiltersOperator1 ;
      private short AV40DynamicFiltersOperator2 ;
      private int AV45GXV1 ;
      private int AV33Contratada_AreaTrabalhoCod ;
      private int AV37Contratada_Codigo1 ;
      private int AV42Contratada_Codigo2 ;
      private int AV9WWPContext_gxTpr_Areatrabalho_codigo ;
      private int A39Contratada_Codigo ;
      private int A52Contratada_AreaTrabalhoCod ;
      private int A40Contratada_PessoaCod ;
      private int AV18InsertIndex ;
      private long AV27count ;
      private String AV10TFContratada_PessoaNom ;
      private String AV11TFContratada_PessoaNom_Sel ;
      private String AV36Contratada_PessoaNom1 ;
      private String AV41Contratada_PessoaNom2 ;
      private String scmdbuf ;
      private String lV36Contratada_PessoaNom1 ;
      private String lV41Contratada_PessoaNom2 ;
      private String lV10TFContratada_PessoaNom ;
      private String A41Contratada_PessoaNom ;
      private bool returnInSub ;
      private bool AV38DynamicFiltersEnabled2 ;
      private bool A43Contratada_Ativo ;
      private bool BRKFB2 ;
      private bool n42Contratada_PessoaCNPJ ;
      private bool n41Contratada_PessoaNom ;
      private bool BRKFB4 ;
      private String AV26OptionIndexesJson ;
      private String AV21OptionsJson ;
      private String AV24OptionsDescJson ;
      private String AV17DDOName ;
      private String AV15SearchTxt ;
      private String AV16SearchTxtTo ;
      private String AV12TFContratada_PessoaCNPJ ;
      private String AV13TFContratada_PessoaCNPJ_Sel ;
      private String AV34DynamicFiltersSelector1 ;
      private String AV39DynamicFiltersSelector2 ;
      private String lV12TFContratada_PessoaCNPJ ;
      private String A42Contratada_PessoaCNPJ ;
      private String AV19Option ;
      private IGxSession AV28Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] P00FB2_A40Contratada_PessoaCod ;
      private bool[] P00FB2_A43Contratada_Ativo ;
      private String[] P00FB2_A42Contratada_PessoaCNPJ ;
      private bool[] P00FB2_n42Contratada_PessoaCNPJ ;
      private int[] P00FB2_A39Contratada_Codigo ;
      private String[] P00FB2_A41Contratada_PessoaNom ;
      private bool[] P00FB2_n41Contratada_PessoaNom ;
      private int[] P00FB2_A52Contratada_AreaTrabalhoCod ;
      private int[] P00FB3_A40Contratada_PessoaCod ;
      private int[] P00FB3_A52Contratada_AreaTrabalhoCod ;
      private String[] P00FB3_A42Contratada_PessoaCNPJ ;
      private bool[] P00FB3_n42Contratada_PessoaCNPJ ;
      private bool[] P00FB3_A43Contratada_Ativo ;
      private int[] P00FB3_A39Contratada_Codigo ;
      private String[] P00FB3_A41Contratada_PessoaNom ;
      private bool[] P00FB3_n41Contratada_PessoaNom ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV20Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV23OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV25OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV30GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV31GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV32GridStateDynamicFilter ;
   }

   public class getpromptcontratadafilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00FB2( IGxContext context ,
                                             String AV34DynamicFiltersSelector1 ,
                                             short AV35DynamicFiltersOperator1 ,
                                             String AV36Contratada_PessoaNom1 ,
                                             int AV37Contratada_Codigo1 ,
                                             bool AV38DynamicFiltersEnabled2 ,
                                             String AV39DynamicFiltersSelector2 ,
                                             short AV40DynamicFiltersOperator2 ,
                                             String AV41Contratada_PessoaNom2 ,
                                             int AV42Contratada_Codigo2 ,
                                             String AV11TFContratada_PessoaNom_Sel ,
                                             String AV10TFContratada_PessoaNom ,
                                             String AV13TFContratada_PessoaCNPJ_Sel ,
                                             String AV12TFContratada_PessoaCNPJ ,
                                             short AV14TFContratada_Ativo_Sel ,
                                             String A41Contratada_PessoaNom ,
                                             int A39Contratada_Codigo ,
                                             String A42Contratada_PessoaCNPJ ,
                                             bool A43Contratada_Ativo ,
                                             int A52Contratada_AreaTrabalhoCod ,
                                             int AV9WWPContext_gxTpr_Areatrabalho_codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [11] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT T1.[Contratada_PessoaCod] AS Contratada_PessoaCod, T1.[Contratada_Ativo], T2.[Pessoa_Docto] AS Contratada_PessoaCNPJ, T1.[Contratada_Codigo], T2.[Pessoa_Nome] AS Contratada_PessoaNom, T1.[Contratada_AreaTrabalhoCod] FROM ([Contratada] T1 WITH (NOLOCK) INNER JOIN [Pessoa] T2 WITH (NOLOCK) ON T2.[Pessoa_Codigo] = T1.[Contratada_PessoaCod])";
         scmdbuf = scmdbuf + " WHERE (T1.[Contratada_AreaTrabalhoCod] = @AV9WWPCo_1Areatrabalho_codigo)";
         if ( ( StringUtil.StrCmp(AV34DynamicFiltersSelector1, "CONTRATADA_PESSOANOM") == 0 ) && ( AV35DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV36Contratada_PessoaNom1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Pessoa_Nome] like @lV36Contratada_PessoaNom1)";
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV34DynamicFiltersSelector1, "CONTRATADA_PESSOANOM") == 0 ) && ( AV35DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV36Contratada_PessoaNom1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Pessoa_Nome] like '%' + @lV36Contratada_PessoaNom1)";
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV34DynamicFiltersSelector1, "CONTRATADA_CODIGO") == 0 ) && ( AV35DynamicFiltersOperator1 == 0 ) && ( ! (0==AV37Contratada_Codigo1) ) )
         {
            sWhereString = sWhereString + " and (T1.[Contratada_Codigo] = @AV37Contratada_Codigo1)";
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( AV38DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV39DynamicFiltersSelector2, "CONTRATADA_PESSOANOM") == 0 ) && ( AV40DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV41Contratada_PessoaNom2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Pessoa_Nome] like @lV41Contratada_PessoaNom2)";
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( AV38DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV39DynamicFiltersSelector2, "CONTRATADA_PESSOANOM") == 0 ) && ( AV40DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV41Contratada_PessoaNom2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Pessoa_Nome] like '%' + @lV41Contratada_PessoaNom2)";
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( AV38DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV39DynamicFiltersSelector2, "CONTRATADA_CODIGO") == 0 ) && ( AV40DynamicFiltersOperator2 == 0 ) && ( ! (0==AV42Contratada_Codigo2) ) )
         {
            sWhereString = sWhereString + " and (T1.[Contratada_Codigo] = @AV42Contratada_Codigo2)";
         }
         else
         {
            GXv_int1[6] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV11TFContratada_PessoaNom_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV10TFContratada_PessoaNom)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Pessoa_Nome] like @lV10TFContratada_PessoaNom)";
         }
         else
         {
            GXv_int1[7] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV11TFContratada_PessoaNom_Sel)) )
         {
            sWhereString = sWhereString + " and (T2.[Pessoa_Nome] = @AV11TFContratada_PessoaNom_Sel)";
         }
         else
         {
            GXv_int1[8] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV13TFContratada_PessoaCNPJ_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV12TFContratada_PessoaCNPJ)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Pessoa_Docto] like @lV12TFContratada_PessoaCNPJ)";
         }
         else
         {
            GXv_int1[9] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV13TFContratada_PessoaCNPJ_Sel)) )
         {
            sWhereString = sWhereString + " and (T2.[Pessoa_Docto] = @AV13TFContratada_PessoaCNPJ_Sel)";
         }
         else
         {
            GXv_int1[10] = 1;
         }
         if ( AV14TFContratada_Ativo_Sel == 1 )
         {
            sWhereString = sWhereString + " and (T1.[Contratada_Ativo] = 1)";
         }
         if ( AV14TFContratada_Ativo_Sel == 2 )
         {
            sWhereString = sWhereString + " and (T1.[Contratada_Ativo] = 0)";
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T1.[Contratada_PessoaCod]";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      protected Object[] conditional_P00FB3( IGxContext context ,
                                             String AV34DynamicFiltersSelector1 ,
                                             short AV35DynamicFiltersOperator1 ,
                                             String AV36Contratada_PessoaNom1 ,
                                             int AV37Contratada_Codigo1 ,
                                             bool AV38DynamicFiltersEnabled2 ,
                                             String AV39DynamicFiltersSelector2 ,
                                             short AV40DynamicFiltersOperator2 ,
                                             String AV41Contratada_PessoaNom2 ,
                                             int AV42Contratada_Codigo2 ,
                                             String AV11TFContratada_PessoaNom_Sel ,
                                             String AV10TFContratada_PessoaNom ,
                                             String AV13TFContratada_PessoaCNPJ_Sel ,
                                             String AV12TFContratada_PessoaCNPJ ,
                                             short AV14TFContratada_Ativo_Sel ,
                                             String A41Contratada_PessoaNom ,
                                             int A39Contratada_Codigo ,
                                             String A42Contratada_PessoaCNPJ ,
                                             bool A43Contratada_Ativo ,
                                             int A52Contratada_AreaTrabalhoCod ,
                                             int AV9WWPContext_gxTpr_Areatrabalho_codigo )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [11] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT T1.[Contratada_PessoaCod] AS Contratada_PessoaCod, T1.[Contratada_AreaTrabalhoCod], T2.[Pessoa_Docto] AS Contratada_PessoaCNPJ, T1.[Contratada_Ativo], T1.[Contratada_Codigo], T2.[Pessoa_Nome] AS Contratada_PessoaNom FROM ([Contratada] T1 WITH (NOLOCK) INNER JOIN [Pessoa] T2 WITH (NOLOCK) ON T2.[Pessoa_Codigo] = T1.[Contratada_PessoaCod])";
         scmdbuf = scmdbuf + " WHERE (T1.[Contratada_AreaTrabalhoCod] = @AV9WWPCo_1Areatrabalho_codigo)";
         if ( ( StringUtil.StrCmp(AV34DynamicFiltersSelector1, "CONTRATADA_PESSOANOM") == 0 ) && ( AV35DynamicFiltersOperator1 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV36Contratada_PessoaNom1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Pessoa_Nome] like @lV36Contratada_PessoaNom1)";
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV34DynamicFiltersSelector1, "CONTRATADA_PESSOANOM") == 0 ) && ( AV35DynamicFiltersOperator1 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV36Contratada_PessoaNom1)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Pessoa_Nome] like '%' + @lV36Contratada_PessoaNom1)";
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( ( StringUtil.StrCmp(AV34DynamicFiltersSelector1, "CONTRATADA_CODIGO") == 0 ) && ( AV35DynamicFiltersOperator1 == 0 ) && ( ! (0==AV37Contratada_Codigo1) ) )
         {
            sWhereString = sWhereString + " and (T1.[Contratada_Codigo] = @AV37Contratada_Codigo1)";
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( AV38DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV39DynamicFiltersSelector2, "CONTRATADA_PESSOANOM") == 0 ) && ( AV40DynamicFiltersOperator2 == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV41Contratada_PessoaNom2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Pessoa_Nome] like @lV41Contratada_PessoaNom2)";
         }
         else
         {
            GXv_int3[4] = 1;
         }
         if ( AV38DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV39DynamicFiltersSelector2, "CONTRATADA_PESSOANOM") == 0 ) && ( AV40DynamicFiltersOperator2 == 1 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV41Contratada_PessoaNom2)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Pessoa_Nome] like '%' + @lV41Contratada_PessoaNom2)";
         }
         else
         {
            GXv_int3[5] = 1;
         }
         if ( AV38DynamicFiltersEnabled2 && ( StringUtil.StrCmp(AV39DynamicFiltersSelector2, "CONTRATADA_CODIGO") == 0 ) && ( AV40DynamicFiltersOperator2 == 0 ) && ( ! (0==AV42Contratada_Codigo2) ) )
         {
            sWhereString = sWhereString + " and (T1.[Contratada_Codigo] = @AV42Contratada_Codigo2)";
         }
         else
         {
            GXv_int3[6] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV11TFContratada_PessoaNom_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV10TFContratada_PessoaNom)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Pessoa_Nome] like @lV10TFContratada_PessoaNom)";
         }
         else
         {
            GXv_int3[7] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV11TFContratada_PessoaNom_Sel)) )
         {
            sWhereString = sWhereString + " and (T2.[Pessoa_Nome] = @AV11TFContratada_PessoaNom_Sel)";
         }
         else
         {
            GXv_int3[8] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV13TFContratada_PessoaCNPJ_Sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV12TFContratada_PessoaCNPJ)) ) )
         {
            sWhereString = sWhereString + " and (T2.[Pessoa_Docto] like @lV12TFContratada_PessoaCNPJ)";
         }
         else
         {
            GXv_int3[9] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV13TFContratada_PessoaCNPJ_Sel)) )
         {
            sWhereString = sWhereString + " and (T2.[Pessoa_Docto] = @AV13TFContratada_PessoaCNPJ_Sel)";
         }
         else
         {
            GXv_int3[10] = 1;
         }
         if ( AV14TFContratada_Ativo_Sel == 1 )
         {
            sWhereString = sWhereString + " and (T1.[Contratada_Ativo] = 1)";
         }
         if ( AV14TFContratada_Ativo_Sel == 2 )
         {
            sWhereString = sWhereString + " and (T1.[Contratada_Ativo] = 0)";
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY T2.[Pessoa_Docto]";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00FB2(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (int)dynConstraints[3] , (bool)dynConstraints[4] , (String)dynConstraints[5] , (short)dynConstraints[6] , (String)dynConstraints[7] , (int)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (short)dynConstraints[13] , (String)dynConstraints[14] , (int)dynConstraints[15] , (String)dynConstraints[16] , (bool)dynConstraints[17] , (int)dynConstraints[18] , (int)dynConstraints[19] );
               case 1 :
                     return conditional_P00FB3(context, (String)dynConstraints[0] , (short)dynConstraints[1] , (String)dynConstraints[2] , (int)dynConstraints[3] , (bool)dynConstraints[4] , (String)dynConstraints[5] , (short)dynConstraints[6] , (String)dynConstraints[7] , (int)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (String)dynConstraints[11] , (String)dynConstraints[12] , (short)dynConstraints[13] , (String)dynConstraints[14] , (int)dynConstraints[15] , (String)dynConstraints[16] , (bool)dynConstraints[17] , (int)dynConstraints[18] , (int)dynConstraints[19] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00FB2 ;
          prmP00FB2 = new Object[] {
          new Object[] {"@AV9WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV36Contratada_PessoaNom1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV36Contratada_PessoaNom1",SqlDbType.Char,100,0} ,
          new Object[] {"@AV37Contratada_Codigo1",SqlDbType.Int,6,0} ,
          new Object[] {"@lV41Contratada_PessoaNom2",SqlDbType.Char,100,0} ,
          new Object[] {"@lV41Contratada_PessoaNom2",SqlDbType.Char,100,0} ,
          new Object[] {"@AV42Contratada_Codigo2",SqlDbType.Int,6,0} ,
          new Object[] {"@lV10TFContratada_PessoaNom",SqlDbType.Char,100,0} ,
          new Object[] {"@AV11TFContratada_PessoaNom_Sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV12TFContratada_PessoaCNPJ",SqlDbType.VarChar,15,0} ,
          new Object[] {"@AV13TFContratada_PessoaCNPJ_Sel",SqlDbType.VarChar,15,0}
          } ;
          Object[] prmP00FB3 ;
          prmP00FB3 = new Object[] {
          new Object[] {"@AV9WWPCo_1Areatrabalho_codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@lV36Contratada_PessoaNom1",SqlDbType.Char,100,0} ,
          new Object[] {"@lV36Contratada_PessoaNom1",SqlDbType.Char,100,0} ,
          new Object[] {"@AV37Contratada_Codigo1",SqlDbType.Int,6,0} ,
          new Object[] {"@lV41Contratada_PessoaNom2",SqlDbType.Char,100,0} ,
          new Object[] {"@lV41Contratada_PessoaNom2",SqlDbType.Char,100,0} ,
          new Object[] {"@AV42Contratada_Codigo2",SqlDbType.Int,6,0} ,
          new Object[] {"@lV10TFContratada_PessoaNom",SqlDbType.Char,100,0} ,
          new Object[] {"@AV11TFContratada_PessoaNom_Sel",SqlDbType.Char,100,0} ,
          new Object[] {"@lV12TFContratada_PessoaCNPJ",SqlDbType.VarChar,15,0} ,
          new Object[] {"@AV13TFContratada_PessoaCNPJ_Sel",SqlDbType.VarChar,15,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00FB2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00FB2,100,0,true,false )
             ,new CursorDef("P00FB3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00FB3,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.getBool(2) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((int[]) buf[4])[0] = rslt.getInt(4) ;
                ((String[]) buf[5])[0] = rslt.getString(5, 100) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((int[]) buf[7])[0] = rslt.getInt(6) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((bool[]) buf[4])[0] = rslt.getBool(4) ;
                ((int[]) buf[5])[0] = rslt.getInt(5) ;
                ((String[]) buf[6])[0] = rslt.getString(6, 100) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(6);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[11]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[14]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[17]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[11]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[13]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[14]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[17]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getpromptcontratadafilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getpromptcontratadafilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getpromptcontratadafilterdata") )
          {
             return  ;
          }
          getpromptcontratadafilterdata worker = new getpromptcontratadafilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
