/*
               File: REL_CompromissosAssumidos
        Description: Stub for REL_CompromissosAssumidos
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:53:4.0
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using System.Web.Services;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class rel_compromissosassumidos : GXProcedure
   {
      public rel_compromissosassumidos( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
      }

      public rel_compromissosassumidos( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_AreaTrabalho_Codigo ,
                           decimal aP1_Linha5 ,
                           bool aP2_TodasAsAreas )
      {
         this.AV2AreaTrabalho_Codigo = aP0_AreaTrabalho_Codigo;
         this.AV3Linha5 = aP1_Linha5;
         this.AV4TodasAsAreas = aP2_TodasAsAreas;
         initialize();
         executePrivate();
         aP0_AreaTrabalho_Codigo=this.AV2AreaTrabalho_Codigo;
      }

      public void executeSubmit( ref int aP0_AreaTrabalho_Codigo ,
                                 decimal aP1_Linha5 ,
                                 bool aP2_TodasAsAreas )
      {
         rel_compromissosassumidos objrel_compromissosassumidos;
         objrel_compromissosassumidos = new rel_compromissosassumidos();
         objrel_compromissosassumidos.AV2AreaTrabalho_Codigo = aP0_AreaTrabalho_Codigo;
         objrel_compromissosassumidos.AV3Linha5 = aP1_Linha5;
         objrel_compromissosassumidos.AV4TodasAsAreas = aP2_TodasAsAreas;
         objrel_compromissosassumidos.context.SetSubmitInitialConfig(context);
         objrel_compromissosassumidos.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objrel_compromissosassumidos);
         aP0_AreaTrabalho_Codigo=this.AV2AreaTrabalho_Codigo;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((rel_compromissosassumidos)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         args = new Object[] {(int)AV2AreaTrabalho_Codigo,(decimal)AV3Linha5,(bool)AV4TodasAsAreas} ;
         ClassLoader.Execute("arel_compromissosassumidos","GeneXus.Programs.arel_compromissosassumidos", new Object[] {context }, "execute", args);
         if ( ( args != null ) && ( args.Length == 3 ) )
         {
            AV2AreaTrabalho_Codigo = (int)(args[0]) ;
         }
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV2AreaTrabalho_Codigo ;
      private decimal AV3Linha5 ;
      private bool AV4TodasAsAreas ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_AreaTrabalho_Codigo ;
      private Object[] args ;
   }

}
