/*
               File: PRC_InsLogResponsavel
        Description: Inserir Log Responsavel
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/2/2020 16:12:19.1
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_inslogresponsavel : GXProcedure
   {
      public prc_inslogresponsavel( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_inslogresponsavel( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_ContagemResultado_Codigo ,
                           int aP1_ContagemResultado_Responsavel ,
                           String aP2_Acao ,
                           String aP3_Objeto ,
                           int aP4_User_Id ,
                           short aP5_Sequencial ,
                           String aP6_LogResponsavel_Status ,
                           String aP7_LogResponsavel_NovoStatus ,
                           String aP8_LogResponsavel_Observacao ,
                           DateTime aP9_LogResponsavel_Prazo ,
                           bool aP10_Notificar )
      {
         this.A456ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         this.AV9ContagemResultado_Responsavel = aP1_ContagemResultado_Responsavel;
         this.AV8Acao = aP2_Acao;
         this.AV10Objeto = aP3_Objeto;
         this.AV11User_Id = aP4_User_Id;
         this.AV13Sequencial = aP5_Sequencial;
         this.AV14LogResponsavel_Status = aP6_LogResponsavel_Status;
         this.AV17LogResponsavel_NovoStatus = aP7_LogResponsavel_NovoStatus;
         this.AV15LogResponsavel_Observacao = aP8_LogResponsavel_Observacao;
         this.AV16LogResponsavel_Prazo = aP9_LogResponsavel_Prazo;
         this.AV22Notificar = aP10_Notificar;
         initialize();
         executePrivate();
         aP0_ContagemResultado_Codigo=this.A456ContagemResultado_Codigo;
      }

      public void executeSubmit( ref int aP0_ContagemResultado_Codigo ,
                                 int aP1_ContagemResultado_Responsavel ,
                                 String aP2_Acao ,
                                 String aP3_Objeto ,
                                 int aP4_User_Id ,
                                 short aP5_Sequencial ,
                                 String aP6_LogResponsavel_Status ,
                                 String aP7_LogResponsavel_NovoStatus ,
                                 String aP8_LogResponsavel_Observacao ,
                                 DateTime aP9_LogResponsavel_Prazo ,
                                 bool aP10_Notificar )
      {
         prc_inslogresponsavel objprc_inslogresponsavel;
         objprc_inslogresponsavel = new prc_inslogresponsavel();
         objprc_inslogresponsavel.A456ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         objprc_inslogresponsavel.AV9ContagemResultado_Responsavel = aP1_ContagemResultado_Responsavel;
         objprc_inslogresponsavel.AV8Acao = aP2_Acao;
         objprc_inslogresponsavel.AV10Objeto = aP3_Objeto;
         objprc_inslogresponsavel.AV11User_Id = aP4_User_Id;
         objprc_inslogresponsavel.AV13Sequencial = aP5_Sequencial;
         objprc_inslogresponsavel.AV14LogResponsavel_Status = aP6_LogResponsavel_Status;
         objprc_inslogresponsavel.AV17LogResponsavel_NovoStatus = aP7_LogResponsavel_NovoStatus;
         objprc_inslogresponsavel.AV15LogResponsavel_Observacao = aP8_LogResponsavel_Observacao;
         objprc_inslogresponsavel.AV16LogResponsavel_Prazo = aP9_LogResponsavel_Prazo;
         objprc_inslogresponsavel.AV22Notificar = aP10_Notificar;
         objprc_inslogresponsavel.context.SetSubmitInitialConfig(context);
         objprc_inslogresponsavel.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_inslogresponsavel);
         aP0_ContagemResultado_Codigo=this.A456ContagemResultado_Codigo;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_inslogresponsavel)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV12LogResponsavel_DataHora = DateTimeUtil.ServerNow( context, "DEFAULT");
         /*
            INSERT RECORD ON TABLE LogResponsavel

         */
         A893LogResponsavel_DataHora = AV12LogResponsavel_DataHora;
         A894LogResponsavel_Acao = AV8Acao;
         A895LogResponsavel_ObjetoTipo = AV10Objeto;
         A892LogResponsavel_DemandaCod = A456ContagemResultado_Codigo;
         n892LogResponsavel_DemandaCod = false;
         A896LogResponsavel_Owner = AV11User_Id;
         A1130LogResponsavel_Status = AV14LogResponsavel_Status;
         n1130LogResponsavel_Status = false;
         A1234LogResponsavel_NovoStatus = AV17LogResponsavel_NovoStatus;
         n1234LogResponsavel_NovoStatus = false;
         A1131LogResponsavel_Observacao = AV15LogResponsavel_Observacao;
         n1131LogResponsavel_Observacao = false;
         A1177LogResponsavel_Prazo = AV16LogResponsavel_Prazo;
         n1177LogResponsavel_Prazo = false;
         if ( (0==AV9ContagemResultado_Responsavel) || ( StringUtil.StrCmp(AV8Acao, "C") == 0 ) )
         {
            A891LogResponsavel_UsuarioCod = 0;
            n891LogResponsavel_UsuarioCod = false;
            n891LogResponsavel_UsuarioCod = true;
         }
         else
         {
            A891LogResponsavel_UsuarioCod = AV9ContagemResultado_Responsavel;
            n891LogResponsavel_UsuarioCod = false;
         }
         /* Using cursor P006F2 */
         pr_default.execute(0, new Object[] {A893LogResponsavel_DataHora, n1177LogResponsavel_Prazo, A1177LogResponsavel_Prazo, A894LogResponsavel_Acao, n891LogResponsavel_UsuarioCod, A891LogResponsavel_UsuarioCod, A895LogResponsavel_ObjetoTipo, n892LogResponsavel_DemandaCod, A892LogResponsavel_DemandaCod, A896LogResponsavel_Owner, n1130LogResponsavel_Status, A1130LogResponsavel_Status, n1234LogResponsavel_NovoStatus, A1234LogResponsavel_NovoStatus, n1131LogResponsavel_Observacao, A1131LogResponsavel_Observacao});
         A1797LogResponsavel_Codigo = P006F2_A1797LogResponsavel_Codigo[0];
         pr_default.close(0);
         dsDefault.SmartCacheProvider.SetUpdated("LogResponsavel") ;
         if ( (pr_default.getStatus(0) == 1) )
         {
            context.Gx_err = 1;
            Gx_emsg = (String)(context.GetMessage( "GXM_noupdate", ""));
         }
         else
         {
            context.Gx_err = 0;
            Gx_emsg = "";
         }
         /* End Insert */
         AV32WebSession.Set("LogRspCodigo", StringUtil.Str( (decimal)(A1797LogResponsavel_Codigo), 10, 0));
         /* Using cursor P006F4 */
         pr_default.execute(1, new Object[] {A456ContagemResultado_Codigo});
         while ( (pr_default.getStatus(1) != 101) )
         {
            A1553ContagemResultado_CntSrvCod = P006F4_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = P006F4_n1553ContagemResultado_CntSrvCod[0];
            A1604ContagemResultado_CntPrpCod = P006F4_A1604ContagemResultado_CntPrpCod[0];
            n1604ContagemResultado_CntPrpCod = P006F4_n1604ContagemResultado_CntPrpCod[0];
            A1605ContagemResultado_CntPrpPesCod = P006F4_A1605ContagemResultado_CntPrpPesCod[0];
            n1605ContagemResultado_CntPrpPesCod = P006F4_n1605ContagemResultado_CntPrpPesCod[0];
            A490ContagemResultado_ContratadaCod = P006F4_A490ContagemResultado_ContratadaCod[0];
            n490ContagemResultado_ContratadaCod = P006F4_n490ContagemResultado_ContratadaCod[0];
            A601ContagemResultado_Servico = P006F4_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = P006F4_n601ContagemResultado_Servico[0];
            A801ContagemResultado_ServicoSigla = P006F4_A801ContagemResultado_ServicoSigla[0];
            n801ContagemResultado_ServicoSigla = P006F4_n801ContagemResultado_ServicoSigla[0];
            A1636ContagemResultado_ServicoSS = P006F4_A1636ContagemResultado_ServicoSS[0];
            n1636ContagemResultado_ServicoSS = P006F4_n1636ContagemResultado_ServicoSS[0];
            A493ContagemResultado_DemandaFM = P006F4_A493ContagemResultado_DemandaFM[0];
            n493ContagemResultado_DemandaFM = P006F4_n493ContagemResultado_DemandaFM[0];
            A457ContagemResultado_Demanda = P006F4_A457ContagemResultado_Demanda[0];
            n457ContagemResultado_Demanda = P006F4_n457ContagemResultado_Demanda[0];
            A602ContagemResultado_OSVinculada = P006F4_A602ContagemResultado_OSVinculada[0];
            n602ContagemResultado_OSVinculada = P006F4_n602ContagemResultado_OSVinculada[0];
            A52Contratada_AreaTrabalhoCod = P006F4_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = P006F4_n52Contratada_AreaTrabalhoCod[0];
            A494ContagemResultado_Descricao = P006F4_A494ContagemResultado_Descricao[0];
            n494ContagemResultado_Descricao = P006F4_n494ContagemResultado_Descricao[0];
            A1603ContagemResultado_CntCod = P006F4_A1603ContagemResultado_CntCod[0];
            n1603ContagemResultado_CntCod = P006F4_n1603ContagemResultado_CntCod[0];
            A803ContagemResultado_ContratadaSigla = P006F4_A803ContagemResultado_ContratadaSigla[0];
            n803ContagemResultado_ContratadaSigla = P006F4_n803ContagemResultado_ContratadaSigla[0];
            A1606ContagemResultado_CntPrpPesNom = P006F4_A1606ContagemResultado_CntPrpPesNom[0];
            n1606ContagemResultado_CntPrpPesNom = P006F4_n1606ContagemResultado_CntPrpPesNom[0];
            A508ContagemResultado_Owner = P006F4_A508ContagemResultado_Owner[0];
            A584ContagemResultado_ContadorFM = P006F4_A584ContagemResultado_ContadorFM[0];
            n584ContagemResultado_ContadorFM = P006F4_n584ContagemResultado_ContadorFM[0];
            A601ContagemResultado_Servico = P006F4_A601ContagemResultado_Servico[0];
            n601ContagemResultado_Servico = P006F4_n601ContagemResultado_Servico[0];
            A1603ContagemResultado_CntCod = P006F4_A1603ContagemResultado_CntCod[0];
            n1603ContagemResultado_CntCod = P006F4_n1603ContagemResultado_CntCod[0];
            A801ContagemResultado_ServicoSigla = P006F4_A801ContagemResultado_ServicoSigla[0];
            n801ContagemResultado_ServicoSigla = P006F4_n801ContagemResultado_ServicoSigla[0];
            A1604ContagemResultado_CntPrpCod = P006F4_A1604ContagemResultado_CntPrpCod[0];
            n1604ContagemResultado_CntPrpCod = P006F4_n1604ContagemResultado_CntPrpCod[0];
            A1605ContagemResultado_CntPrpPesCod = P006F4_A1605ContagemResultado_CntPrpPesCod[0];
            n1605ContagemResultado_CntPrpPesCod = P006F4_n1605ContagemResultado_CntPrpPesCod[0];
            A1606ContagemResultado_CntPrpPesNom = P006F4_A1606ContagemResultado_CntPrpPesNom[0];
            n1606ContagemResultado_CntPrpPesNom = P006F4_n1606ContagemResultado_CntPrpPesNom[0];
            A52Contratada_AreaTrabalhoCod = P006F4_A52Contratada_AreaTrabalhoCod[0];
            n52Contratada_AreaTrabalhoCod = P006F4_n52Contratada_AreaTrabalhoCod[0];
            A803ContagemResultado_ContratadaSigla = P006F4_A803ContagemResultado_ContratadaSigla[0];
            n803ContagemResultado_ContratadaSigla = P006F4_n803ContagemResultado_ContratadaSigla[0];
            A584ContagemResultado_ContadorFM = P006F4_A584ContagemResultado_ContadorFM[0];
            n584ContagemResultado_ContadorFM = P006F4_n584ContagemResultado_ContadorFM[0];
            AV18Contratada = A490ContagemResultado_ContratadaCod;
            AV19Servico = A601ContagemResultado_Servico;
            AV38ServicoSigla = A801ContagemResultado_ServicoSigla;
            AV36Contador = A584ContagemResultado_ContadorFM;
            if ( A1636ContagemResultado_ServicoSS > 0 )
            {
               AV45Subject = "SS ";
            }
            else
            {
               AV45Subject = "OS ";
            }
            if ( String.IsNullOrEmpty(StringUtil.RTrim( A493ContagemResultado_DemandaFM)) )
            {
               AV37Demanda = A457ContagemResultado_Demanda;
            }
            else
            {
               AV37Demanda = A493ContagemResultado_DemandaFM;
            }
            AV44OSVinculada = A602ContagemResultado_OSVinculada;
            AV35AreaTrabalho = A52Contratada_AreaTrabalhoCod;
            AV55Descricao = A494ContagemResultado_Descricao;
            AV49Contrato_Codigo = A1603ContagemResultado_CntCod;
            AV41UserName = StringUtil.Trim( A1606ContagemResultado_CntPrpPesNom) + " da " + A803ContagemResultado_ContratadaSigla;
            AV59ContagemResultado_Owner = A508ContagemResultado_Owner;
            /* Exit For each command. Update data (if necessary), close cursors & exit. */
            if (true) break;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(1);
         AV56AcoesQueNaoMudamStatus = "L D N NO U PR B";
         AV57cAcao = AV8Acao + " ";
         if ( AV22Notificar && ( ( ( StringUtil.StrCmp(AV14LogResponsavel_Status, AV17LogResponsavel_NovoStatus) != 0 ) || ( StringUtil.StringSearch( AV56AcoesQueNaoMudamStatus, AV57cAcao, 1) > 0 ) ) || ( ( StringUtil.StrCmp(AV57cAcao, "EA") == 0 ) && ( StringUtil.StrCmp(AV14LogResponsavel_Status, "B") == 0 ) && ( StringUtil.StrCmp(AV17LogResponsavel_NovoStatus, "E") == 0 ) ) ) )
         {
            new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV48WWPContext) ;
            if ( ! ( ( StringUtil.StrCmp(AV17LogResponsavel_NovoStatus, "S") == 0 ) && (0==AV44OSVinculada) ) || ( ( StringUtil.StrCmp(AV17LogResponsavel_NovoStatus, "B") == 0 ) ) )
            {
               /* Using cursor P006F5 */
               pr_default.execute(2, new Object[] {AV49Contrato_Codigo});
               while ( (pr_default.getStatus(2) != 101) )
               {
                  A1078ContratoGestor_ContratoCod = P006F5_A1078ContratoGestor_ContratoCod[0];
                  A1079ContratoGestor_UsuarioCod = P006F5_A1079ContratoGestor_UsuarioCod[0];
                  GXt_int1 = AV48WWPContext.gxTpr_Areatrabalho_codigo;
                  if ( new prc_usuarionotificanostatus(context).executeUdp( ref  A1079ContratoGestor_UsuarioCod, ref  GXt_int1,  AV17LogResponsavel_NovoStatus) )
                  {
                     AV20Usuarios.Add(A1079ContratoGestor_UsuarioCod, 0);
                  }
                  pr_default.readNext(2);
               }
               pr_default.close(2);
               if ( ! (0==AV9ContagemResultado_Responsavel) )
               {
                  if ( ( AV11User_Id != AV9ContagemResultado_Responsavel ) && ( AV20Usuarios.IndexOf(AV9ContagemResultado_Responsavel) == 0 ) )
                  {
                     pr_default.dynParam(3, new Object[]{ new Object[]{
                                                          A1Usuario_Codigo ,
                                                          AV20Usuarios ,
                                                          AV9ContagemResultado_Responsavel },
                                                          new int[] {
                                                          TypeConstants.INT, TypeConstants.INT
                                                          }
                     });
                     /* Using cursor P006F6 */
                     pr_default.execute(3, new Object[] {AV9ContagemResultado_Responsavel});
                     while ( (pr_default.getStatus(3) != 101) )
                     {
                        A1Usuario_Codigo = P006F6_A1Usuario_Codigo[0];
                        GXt_int1 = AV48WWPContext.gxTpr_Areatrabalho_codigo;
                        if ( new prc_usuarionotificanostatus(context).executeUdp( ref  AV9ContagemResultado_Responsavel, ref  GXt_int1,  AV17LogResponsavel_NovoStatus) )
                        {
                           AV20Usuarios.Add(AV9ContagemResultado_Responsavel, 0);
                        }
                        /* Exit For each command. Update data (if necessary), close cursors & exit. */
                        if (true) break;
                        /* Exiting from a For First loop. */
                        if (true) break;
                     }
                     pr_default.close(3);
                  }
               }
               if ( AV48WWPContext.gxTpr_Userehcontratante )
               {
                  AV41UserName = AV48WWPContext.gxTpr_Username;
               }
               if ( StringUtil.StrCmp(AV17LogResponsavel_NovoStatus, "B") == 0 )
               {
                  if ( (0==AV20Usuarios.IndexOf((int)(AV48WWPContext.gxTpr_Userid))) )
                  {
                     AV20Usuarios.Add(AV48WWPContext.gxTpr_Userid, 0);
                  }
                  if ( (0==AV20Usuarios.IndexOf(AV59ContagemResultado_Owner)) )
                  {
                     AV20Usuarios.Add(AV59ContagemResultado_Owner, 0);
                  }
               }
               if ( ( StringUtil.StrCmp(AV14LogResponsavel_Status, "B") == 0 ) && ( StringUtil.StrCmp(AV17LogResponsavel_NovoStatus, "E") == 0 ) )
               {
                  if ( (0==AV20Usuarios.IndexOf((int)(AV48WWPContext.gxTpr_Userid))) )
                  {
                     AV20Usuarios.Add(AV48WWPContext.gxTpr_Userid, 0);
                  }
                  if ( (0==AV20Usuarios.IndexOf(AV59ContagemResultado_Owner)) )
                  {
                     AV20Usuarios.Add(AV59ContagemResultado_Owner, 0);
                  }
               }
               if ( AV20Usuarios.Count > 0 )
               {
                  AV23EmailText = StringUtil.NewLine( ) + "Prezado(a)," + StringUtil.NewLine( ) + StringUtil.NewLine( );
                  AV23EmailText = AV23EmailText + "A solicita��o de  " + AV38ServicoSigla + " sob n�mero " + AV37Demanda + ": \"" + AV55Descricao + "\"" + StringUtil.NewLine( );
                  AV23EmailText = AV23EmailText + " foi de: " + gxdomainstatusdemanda.getDescription(context,AV14LogResponsavel_Status) + " para " + gxdomainstatusdemanda.getDescription(context,AV17LogResponsavel_NovoStatus) + "." + StringUtil.NewLine( );
                  if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV15LogResponsavel_Observacao)) )
                  {
                     AV23EmailText = AV23EmailText + StringUtil.NewLine( ) + AV15LogResponsavel_Observacao + StringUtil.NewLine( );
                  }
                  AV23EmailText = AV23EmailText + StringUtil.NewLine( ) + AV48WWPContext.gxTpr_Areatrabalho_descricao + ", usu�rio " + AV41UserName + StringUtil.NewLine( );
                  AV23EmailText = AV23EmailText + StringUtil.NewLine( ) + StringUtil.NewLine( ) + "Esta mensagem pode conter informa��o confidencial e/ou privilegiada. Se voc� n�o for o destinat�rio ou a pessoa autorizada a receber esta mensagem, n�o pode usar, copiar ou divulgar as informa��es nela contidas ou tomar qualquer a��o baseada nessas informa��es. Se voc� recebeu esta mensagem por engano, por favor avise imediatamente o remetente, respondendo o e-mail e em seguida apague-o.";
                  AV45Subject = AV45Subject + AV37Demanda + " " + gxdomainstatusdemanda.getDescription(context,AV17LogResponsavel_NovoStatus) + " (No reply)";
                  AV54ContagemResultado_Codigos.Add(A456ContagemResultado_Codigo, 0);
                  AV32WebSession.Set("DemandaCodigo", AV54ContagemResultado_Codigos.ToXml(false, true, "Collection", ""));
                  new prc_enviaremail(context ).execute(  AV35AreaTrabalho,  AV20Usuarios,  AV45Subject,  AV23EmailText,  AV33Attachments, ref  AV34Resultado) ;
                  AV32WebSession.Remove("DemandaCodigo");
               }
            }
         }
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV12LogResponsavel_DataHora = (DateTime)(DateTime.MinValue);
         A893LogResponsavel_DataHora = (DateTime)(DateTime.MinValue);
         A894LogResponsavel_Acao = "";
         A895LogResponsavel_ObjetoTipo = "";
         A1130LogResponsavel_Status = "";
         A1234LogResponsavel_NovoStatus = "";
         A1131LogResponsavel_Observacao = "";
         A1177LogResponsavel_Prazo = (DateTime)(DateTime.MinValue);
         P006F2_A1797LogResponsavel_Codigo = new long[1] ;
         Gx_emsg = "";
         AV32WebSession = context.GetSession();
         scmdbuf = "";
         P006F4_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P006F4_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P006F4_A1604ContagemResultado_CntPrpCod = new int[1] ;
         P006F4_n1604ContagemResultado_CntPrpCod = new bool[] {false} ;
         P006F4_A1605ContagemResultado_CntPrpPesCod = new int[1] ;
         P006F4_n1605ContagemResultado_CntPrpPesCod = new bool[] {false} ;
         P006F4_A456ContagemResultado_Codigo = new int[1] ;
         P006F4_A490ContagemResultado_ContratadaCod = new int[1] ;
         P006F4_n490ContagemResultado_ContratadaCod = new bool[] {false} ;
         P006F4_A601ContagemResultado_Servico = new int[1] ;
         P006F4_n601ContagemResultado_Servico = new bool[] {false} ;
         P006F4_A801ContagemResultado_ServicoSigla = new String[] {""} ;
         P006F4_n801ContagemResultado_ServicoSigla = new bool[] {false} ;
         P006F4_A1636ContagemResultado_ServicoSS = new int[1] ;
         P006F4_n1636ContagemResultado_ServicoSS = new bool[] {false} ;
         P006F4_A493ContagemResultado_DemandaFM = new String[] {""} ;
         P006F4_n493ContagemResultado_DemandaFM = new bool[] {false} ;
         P006F4_A457ContagemResultado_Demanda = new String[] {""} ;
         P006F4_n457ContagemResultado_Demanda = new bool[] {false} ;
         P006F4_A602ContagemResultado_OSVinculada = new int[1] ;
         P006F4_n602ContagemResultado_OSVinculada = new bool[] {false} ;
         P006F4_A52Contratada_AreaTrabalhoCod = new int[1] ;
         P006F4_n52Contratada_AreaTrabalhoCod = new bool[] {false} ;
         P006F4_A494ContagemResultado_Descricao = new String[] {""} ;
         P006F4_n494ContagemResultado_Descricao = new bool[] {false} ;
         P006F4_A1603ContagemResultado_CntCod = new int[1] ;
         P006F4_n1603ContagemResultado_CntCod = new bool[] {false} ;
         P006F4_A803ContagemResultado_ContratadaSigla = new String[] {""} ;
         P006F4_n803ContagemResultado_ContratadaSigla = new bool[] {false} ;
         P006F4_A1606ContagemResultado_CntPrpPesNom = new String[] {""} ;
         P006F4_n1606ContagemResultado_CntPrpPesNom = new bool[] {false} ;
         P006F4_A508ContagemResultado_Owner = new int[1] ;
         P006F4_A584ContagemResultado_ContadorFM = new int[1] ;
         P006F4_n584ContagemResultado_ContadorFM = new bool[] {false} ;
         A801ContagemResultado_ServicoSigla = "";
         A493ContagemResultado_DemandaFM = "";
         A457ContagemResultado_Demanda = "";
         A494ContagemResultado_Descricao = "";
         A803ContagemResultado_ContratadaSigla = "";
         A1606ContagemResultado_CntPrpPesNom = "";
         AV38ServicoSigla = "";
         AV45Subject = "";
         AV37Demanda = "";
         AV55Descricao = "";
         AV41UserName = "";
         AV56AcoesQueNaoMudamStatus = "";
         AV57cAcao = "";
         AV48WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         P006F5_A1078ContratoGestor_ContratoCod = new int[1] ;
         P006F5_A1079ContratoGestor_UsuarioCod = new int[1] ;
         AV20Usuarios = new GxSimpleCollection();
         P006F6_A1Usuario_Codigo = new int[1] ;
         AV23EmailText = "";
         AV54ContagemResultado_Codigos = new GxSimpleCollection();
         AV33Attachments = new GxSimpleCollection();
         AV34Resultado = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_inslogresponsavel__default(),
            new Object[][] {
                new Object[] {
               P006F2_A1797LogResponsavel_Codigo
               }
               , new Object[] {
               P006F4_A1553ContagemResultado_CntSrvCod, P006F4_n1553ContagemResultado_CntSrvCod, P006F4_A1604ContagemResultado_CntPrpCod, P006F4_n1604ContagemResultado_CntPrpCod, P006F4_A1605ContagemResultado_CntPrpPesCod, P006F4_n1605ContagemResultado_CntPrpPesCod, P006F4_A456ContagemResultado_Codigo, P006F4_A490ContagemResultado_ContratadaCod, P006F4_n490ContagemResultado_ContratadaCod, P006F4_A601ContagemResultado_Servico,
               P006F4_n601ContagemResultado_Servico, P006F4_A801ContagemResultado_ServicoSigla, P006F4_n801ContagemResultado_ServicoSigla, P006F4_A1636ContagemResultado_ServicoSS, P006F4_n1636ContagemResultado_ServicoSS, P006F4_A493ContagemResultado_DemandaFM, P006F4_n493ContagemResultado_DemandaFM, P006F4_A457ContagemResultado_Demanda, P006F4_n457ContagemResultado_Demanda, P006F4_A602ContagemResultado_OSVinculada,
               P006F4_n602ContagemResultado_OSVinculada, P006F4_A52Contratada_AreaTrabalhoCod, P006F4_n52Contratada_AreaTrabalhoCod, P006F4_A494ContagemResultado_Descricao, P006F4_n494ContagemResultado_Descricao, P006F4_A1603ContagemResultado_CntCod, P006F4_n1603ContagemResultado_CntCod, P006F4_A803ContagemResultado_ContratadaSigla, P006F4_n803ContagemResultado_ContratadaSigla, P006F4_A1606ContagemResultado_CntPrpPesNom,
               P006F4_n1606ContagemResultado_CntPrpPesNom, P006F4_A508ContagemResultado_Owner, P006F4_A584ContagemResultado_ContadorFM, P006F4_n584ContagemResultado_ContadorFM
               }
               , new Object[] {
               P006F5_A1078ContratoGestor_ContratoCod, P006F5_A1079ContratoGestor_UsuarioCod
               }
               , new Object[] {
               P006F6_A1Usuario_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV13Sequencial ;
      private int A456ContagemResultado_Codigo ;
      private int AV9ContagemResultado_Responsavel ;
      private int AV11User_Id ;
      private int GX_INS198 ;
      private int A892LogResponsavel_DemandaCod ;
      private int A896LogResponsavel_Owner ;
      private int A891LogResponsavel_UsuarioCod ;
      private int A1553ContagemResultado_CntSrvCod ;
      private int A1604ContagemResultado_CntPrpCod ;
      private int A1605ContagemResultado_CntPrpPesCod ;
      private int A490ContagemResultado_ContratadaCod ;
      private int A601ContagemResultado_Servico ;
      private int A1636ContagemResultado_ServicoSS ;
      private int A602ContagemResultado_OSVinculada ;
      private int A52Contratada_AreaTrabalhoCod ;
      private int A1603ContagemResultado_CntCod ;
      private int A508ContagemResultado_Owner ;
      private int A584ContagemResultado_ContadorFM ;
      private int AV18Contratada ;
      private int AV19Servico ;
      private int AV36Contador ;
      private int AV44OSVinculada ;
      private int AV35AreaTrabalho ;
      private int AV49Contrato_Codigo ;
      private int AV59ContagemResultado_Owner ;
      private int A1078ContratoGestor_ContratoCod ;
      private int A1079ContratoGestor_UsuarioCod ;
      private int A1Usuario_Codigo ;
      private int GXt_int1 ;
      private long A1797LogResponsavel_Codigo ;
      private String AV8Acao ;
      private String AV10Objeto ;
      private String AV14LogResponsavel_Status ;
      private String AV17LogResponsavel_NovoStatus ;
      private String A894LogResponsavel_Acao ;
      private String A895LogResponsavel_ObjetoTipo ;
      private String A1130LogResponsavel_Status ;
      private String A1234LogResponsavel_NovoStatus ;
      private String Gx_emsg ;
      private String scmdbuf ;
      private String A801ContagemResultado_ServicoSigla ;
      private String A803ContagemResultado_ContratadaSigla ;
      private String A1606ContagemResultado_CntPrpPesNom ;
      private String AV38ServicoSigla ;
      private String AV45Subject ;
      private String AV41UserName ;
      private String AV56AcoesQueNaoMudamStatus ;
      private String AV57cAcao ;
      private String AV23EmailText ;
      private String AV34Resultado ;
      private DateTime AV16LogResponsavel_Prazo ;
      private DateTime AV12LogResponsavel_DataHora ;
      private DateTime A893LogResponsavel_DataHora ;
      private DateTime A1177LogResponsavel_Prazo ;
      private bool AV22Notificar ;
      private bool n892LogResponsavel_DemandaCod ;
      private bool n1130LogResponsavel_Status ;
      private bool n1234LogResponsavel_NovoStatus ;
      private bool n1131LogResponsavel_Observacao ;
      private bool n1177LogResponsavel_Prazo ;
      private bool n891LogResponsavel_UsuarioCod ;
      private bool n1553ContagemResultado_CntSrvCod ;
      private bool n1604ContagemResultado_CntPrpCod ;
      private bool n1605ContagemResultado_CntPrpPesCod ;
      private bool n490ContagemResultado_ContratadaCod ;
      private bool n601ContagemResultado_Servico ;
      private bool n801ContagemResultado_ServicoSigla ;
      private bool n1636ContagemResultado_ServicoSS ;
      private bool n493ContagemResultado_DemandaFM ;
      private bool n457ContagemResultado_Demanda ;
      private bool n602ContagemResultado_OSVinculada ;
      private bool n52Contratada_AreaTrabalhoCod ;
      private bool n494ContagemResultado_Descricao ;
      private bool n1603ContagemResultado_CntCod ;
      private bool n803ContagemResultado_ContratadaSigla ;
      private bool n1606ContagemResultado_CntPrpPesNom ;
      private bool n584ContagemResultado_ContadorFM ;
      private String AV15LogResponsavel_Observacao ;
      private String A1131LogResponsavel_Observacao ;
      private String A493ContagemResultado_DemandaFM ;
      private String A457ContagemResultado_Demanda ;
      private String A494ContagemResultado_Descricao ;
      private String AV37Demanda ;
      private String AV55Descricao ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_ContagemResultado_Codigo ;
      private IDataStoreProvider pr_default ;
      private long[] P006F2_A1797LogResponsavel_Codigo ;
      private int[] P006F4_A1553ContagemResultado_CntSrvCod ;
      private bool[] P006F4_n1553ContagemResultado_CntSrvCod ;
      private int[] P006F4_A1604ContagemResultado_CntPrpCod ;
      private bool[] P006F4_n1604ContagemResultado_CntPrpCod ;
      private int[] P006F4_A1605ContagemResultado_CntPrpPesCod ;
      private bool[] P006F4_n1605ContagemResultado_CntPrpPesCod ;
      private int[] P006F4_A456ContagemResultado_Codigo ;
      private int[] P006F4_A490ContagemResultado_ContratadaCod ;
      private bool[] P006F4_n490ContagemResultado_ContratadaCod ;
      private int[] P006F4_A601ContagemResultado_Servico ;
      private bool[] P006F4_n601ContagemResultado_Servico ;
      private String[] P006F4_A801ContagemResultado_ServicoSigla ;
      private bool[] P006F4_n801ContagemResultado_ServicoSigla ;
      private int[] P006F4_A1636ContagemResultado_ServicoSS ;
      private bool[] P006F4_n1636ContagemResultado_ServicoSS ;
      private String[] P006F4_A493ContagemResultado_DemandaFM ;
      private bool[] P006F4_n493ContagemResultado_DemandaFM ;
      private String[] P006F4_A457ContagemResultado_Demanda ;
      private bool[] P006F4_n457ContagemResultado_Demanda ;
      private int[] P006F4_A602ContagemResultado_OSVinculada ;
      private bool[] P006F4_n602ContagemResultado_OSVinculada ;
      private int[] P006F4_A52Contratada_AreaTrabalhoCod ;
      private bool[] P006F4_n52Contratada_AreaTrabalhoCod ;
      private String[] P006F4_A494ContagemResultado_Descricao ;
      private bool[] P006F4_n494ContagemResultado_Descricao ;
      private int[] P006F4_A1603ContagemResultado_CntCod ;
      private bool[] P006F4_n1603ContagemResultado_CntCod ;
      private String[] P006F4_A803ContagemResultado_ContratadaSigla ;
      private bool[] P006F4_n803ContagemResultado_ContratadaSigla ;
      private String[] P006F4_A1606ContagemResultado_CntPrpPesNom ;
      private bool[] P006F4_n1606ContagemResultado_CntPrpPesNom ;
      private int[] P006F4_A508ContagemResultado_Owner ;
      private int[] P006F4_A584ContagemResultado_ContadorFM ;
      private bool[] P006F4_n584ContagemResultado_ContadorFM ;
      private int[] P006F5_A1078ContratoGestor_ContratoCod ;
      private int[] P006F5_A1079ContratoGestor_UsuarioCod ;
      private int[] P006F6_A1Usuario_Codigo ;
      private IGxSession AV32WebSession ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV20Usuarios ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV54ContagemResultado_Codigos ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV33Attachments ;
      private wwpbaseobjects.SdtWWPContext AV48WWPContext ;
   }

   public class prc_inslogresponsavel__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P006F6( IGxContext context ,
                                             int A1Usuario_Codigo ,
                                             IGxCollection AV20Usuarios ,
                                             int AV9ContagemResultado_Responsavel )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [1] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         scmdbuf = "SELECT TOP 1 [Usuario_Codigo] FROM [Usuario] WITH (NOLOCK)";
         scmdbuf = scmdbuf + " WHERE ([Usuario_Codigo] = @AV9ContagemResultado_Responsavel)";
         scmdbuf = scmdbuf + " and (Not " + new GxDbmsUtils( new GxSqlServer()).ValueList(AV20Usuarios, "[Usuario_Codigo] IN (", ")") + ")";
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + " ORDER BY [Usuario_Codigo]";
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 3 :
                     return conditional_P006F6(context, (int)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (int)dynConstraints[2] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP006F2 ;
          prmP006F2 = new Object[] {
          new Object[] {"@LogResponsavel_DataHora",SqlDbType.DateTime,8,5} ,
          new Object[] {"@LogResponsavel_Prazo",SqlDbType.DateTime,8,5} ,
          new Object[] {"@LogResponsavel_Acao",SqlDbType.Char,20,0} ,
          new Object[] {"@LogResponsavel_UsuarioCod",SqlDbType.Int,6,0} ,
          new Object[] {"@LogResponsavel_ObjetoTipo",SqlDbType.Char,1,0} ,
          new Object[] {"@LogResponsavel_DemandaCod",SqlDbType.Int,6,0} ,
          new Object[] {"@LogResponsavel_Owner",SqlDbType.Int,6,0} ,
          new Object[] {"@LogResponsavel_Status",SqlDbType.Char,1,0} ,
          new Object[] {"@LogResponsavel_NovoStatus",SqlDbType.Char,1,0} ,
          new Object[] {"@LogResponsavel_Observacao",SqlDbType.VarChar,2097152,0}
          } ;
          Object[] prmP006F4 ;
          prmP006F4 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          String cmdBufferP006F4 ;
          cmdBufferP006F4=" SELECT TOP 1 T1.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T4.[Contrato_PrepostoCod] AS ContagemResultado_CntPrpCod, T5.[Usuario_PessoaCod] AS ContagemResultado_CntPrpPesCod, T1.[ContagemResultado_Codigo], T1.[ContagemResultado_ContratadaCod] AS ContagemResultado_ContratadaCod, T2.[Servico_Codigo] AS ContagemResultado_Servico, T3.[Servico_Sigla] AS ContagemResultado_ServicoSigla, T1.[ContagemResultado_ServicoSS], T1.[ContagemResultado_DemandaFM], T1.[ContagemResultado_Demanda], T1.[ContagemResultado_OSVinculada], T7.[Contratada_AreaTrabalhoCod], T1.[ContagemResultado_Descricao], T2.[Contrato_Codigo] AS ContagemResultado_CntCod, T7.[Contratada_Sigla] AS ContagemResultado_ContratadaSigla, T6.[Pessoa_Nome] AS ContagemResultado_CntPrpPesNom, T1.[ContagemResultado_Owner], COALESCE( T8.[ContagemResultado_ContadorFM], 0) AS ContagemResultado_ContadorFM FROM ((((((([ContagemResultado] T1 WITH (NOLOCK) LEFT JOIN [ContratoServicos] T2 WITH (NOLOCK) ON T2.[ContratoServicos_Codigo] = T1.[ContagemResultado_CntSrvCod]) LEFT JOIN [Servico] T3 WITH (NOLOCK) ON T3.[Servico_Codigo] = T2.[Servico_Codigo]) LEFT JOIN [Contrato] T4 WITH (NOLOCK) ON T4.[Contrato_Codigo] = T2.[Contrato_Codigo]) LEFT JOIN [Usuario] T5 WITH (NOLOCK) ON T5.[Usuario_Codigo] = T4.[Contrato_PrepostoCod]) LEFT JOIN [Pessoa] T6 WITH (NOLOCK) ON T6.[Pessoa_Codigo] = T5.[Usuario_PessoaCod]) LEFT JOIN [Contratada] T7 WITH (NOLOCK) ON T7.[Contratada_Codigo] = T1.[ContagemResultado_ContratadaCod]) LEFT JOIN (SELECT MIN([ContagemResultado_ContadorFMCod]) AS ContagemResultado_ContadorFM, [ContagemResultado_Codigo] FROM [ContagemResultadoContagens] WITH (NOLOCK) WHERE [ContagemResultado_Ultima] = 1 GROUP BY [ContagemResultado_Codigo] ) T8 ON T8.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) WHERE "
          + " T1.[ContagemResultado_Codigo] = @ContagemResultado_Codigo ORDER BY T1.[ContagemResultado_Codigo]" ;
          Object[] prmP006F5 ;
          prmP006F5 = new Object[] {
          new Object[] {"@AV49Contrato_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP006F6 ;
          prmP006F6 = new Object[] {
          new Object[] {"@AV9ContagemResultado_Responsavel",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P006F2", "INSERT INTO [LogResponsavel]([LogResponsavel_DataHora], [LogResponsavel_Prazo], [LogResponsavel_Acao], [LogResponsavel_UsuarioCod], [LogResponsavel_ObjetoTipo], [LogResponsavel_DemandaCod], [LogResponsavel_Owner], [LogResponsavel_Status], [LogResponsavel_NovoStatus], [LogResponsavel_Observacao]) VALUES(@LogResponsavel_DataHora, @LogResponsavel_Prazo, @LogResponsavel_Acao, @LogResponsavel_UsuarioCod, @LogResponsavel_ObjetoTipo, @LogResponsavel_DemandaCod, @LogResponsavel_Owner, @LogResponsavel_Status, @LogResponsavel_NovoStatus, @LogResponsavel_Observacao); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmP006F2)
             ,new CursorDef("P006F4", cmdBufferP006F4,false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP006F4,1,0,false,true )
             ,new CursorDef("P006F5", "SELECT [ContratoGestor_ContratoCod], [ContratoGestor_UsuarioCod] FROM [ContratoGestor] WITH (NOLOCK) WHERE [ContratoGestor_ContratoCod] = @AV49Contrato_Codigo ORDER BY [ContratoGestor_ContratoCod] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP006F5,100,0,true,false )
             ,new CursorDef("P006F6", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP006F6,1,0,true,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((int[]) buf[4])[0] = rslt.getInt(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((int[]) buf[6])[0] = rslt.getInt(4) ;
                ((int[]) buf[7])[0] = rslt.getInt(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((int[]) buf[9])[0] = rslt.getInt(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((String[]) buf[11])[0] = rslt.getString(7, 15) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(7);
                ((int[]) buf[13])[0] = rslt.getInt(8) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(8);
                ((String[]) buf[15])[0] = rslt.getVarchar(9) ;
                ((bool[]) buf[16])[0] = rslt.wasNull(9);
                ((String[]) buf[17])[0] = rslt.getVarchar(10) ;
                ((bool[]) buf[18])[0] = rslt.wasNull(10);
                ((int[]) buf[19])[0] = rslt.getInt(11) ;
                ((bool[]) buf[20])[0] = rslt.wasNull(11);
                ((int[]) buf[21])[0] = rslt.getInt(12) ;
                ((bool[]) buf[22])[0] = rslt.wasNull(12);
                ((String[]) buf[23])[0] = rslt.getVarchar(13) ;
                ((bool[]) buf[24])[0] = rslt.wasNull(13);
                ((int[]) buf[25])[0] = rslt.getInt(14) ;
                ((bool[]) buf[26])[0] = rslt.wasNull(14);
                ((String[]) buf[27])[0] = rslt.getString(15, 15) ;
                ((bool[]) buf[28])[0] = rslt.wasNull(15);
                ((String[]) buf[29])[0] = rslt.getString(16, 100) ;
                ((bool[]) buf[30])[0] = rslt.wasNull(16);
                ((int[]) buf[31])[0] = rslt.getInt(17) ;
                ((int[]) buf[32])[0] = rslt.getInt(18) ;
                ((bool[]) buf[33])[0] = rslt.wasNull(18);
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameterDatetime(1, (DateTime)parms[0]);
                if ( (bool)parms[1] )
                {
                   stmt.setNull( 2 , SqlDbType.DateTime );
                }
                else
                {
                   stmt.SetParameterDatetime(2, (DateTime)parms[2]);
                }
                stmt.SetParameter(3, (String)parms[3]);
                if ( (bool)parms[4] )
                {
                   stmt.setNull( 4 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(4, (int)parms[5]);
                }
                stmt.SetParameter(5, (String)parms[6]);
                if ( (bool)parms[7] )
                {
                   stmt.setNull( 6 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(6, (int)parms[8]);
                }
                stmt.SetParameter(7, (int)parms[9]);
                if ( (bool)parms[10] )
                {
                   stmt.setNull( 8 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(8, (String)parms[11]);
                }
                if ( (bool)parms[12] )
                {
                   stmt.setNull( 9 , SqlDbType.Char );
                }
                else
                {
                   stmt.SetParameter(9, (String)parms[13]);
                }
                if ( (bool)parms[14] )
                {
                   stmt.setNull( 10 , SqlDbType.VarChar );
                }
                else
                {
                   stmt.SetParameter(10, (String)parms[15]);
                }
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[1]);
                }
                return;
       }
    }

 }

}
