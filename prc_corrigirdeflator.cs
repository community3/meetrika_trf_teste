/*
               File: PRC_CorrigirDeflator
        Description: Corrigir Deflator
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 6/2/2020 16:14:5.22
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class prc_corrigirdeflator : GXProcedure
   {
      public prc_corrigirdeflator( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public prc_corrigirdeflator( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref int aP0_ContagemResultado_Codigo )
      {
         this.A456ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         initialize();
         executePrivate();
         aP0_ContagemResultado_Codigo=this.A456ContagemResultado_Codigo;
      }

      public int executeUdp( )
      {
         this.A456ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         initialize();
         executePrivate();
         aP0_ContagemResultado_Codigo=this.A456ContagemResultado_Codigo;
         return A456ContagemResultado_Codigo ;
      }

      public void executeSubmit( ref int aP0_ContagemResultado_Codigo )
      {
         prc_corrigirdeflator objprc_corrigirdeflator;
         objprc_corrigirdeflator = new prc_corrigirdeflator();
         objprc_corrigirdeflator.A456ContagemResultado_Codigo = aP0_ContagemResultado_Codigo;
         objprc_corrigirdeflator.context.SetSubmitInitialConfig(context);
         objprc_corrigirdeflator.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objprc_corrigirdeflator);
         aP0_ContagemResultado_Codigo=this.A456ContagemResultado_Codigo;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((prc_corrigirdeflator)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         /* Using cursor P00V22 */
         pr_default.execute(0, new Object[] {A456ContagemResultado_Codigo});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A1553ContagemResultado_CntSrvCod = P00V22_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = P00V22_n1553ContagemResultado_CntSrvCod[0];
            A517ContagemResultado_Ultima = P00V22_A517ContagemResultado_Ultima[0];
            A1596ContagemResultado_CntSrvPrc = P00V22_A1596ContagemResultado_CntSrvPrc[0];
            n1596ContagemResultado_CntSrvPrc = P00V22_n1596ContagemResultado_CntSrvPrc[0];
            A800ContagemResultado_Deflator = P00V22_A800ContagemResultado_Deflator[0];
            n800ContagemResultado_Deflator = P00V22_n800ContagemResultado_Deflator[0];
            A473ContagemResultado_DataCnt = P00V22_A473ContagemResultado_DataCnt[0];
            A511ContagemResultado_HoraCnt = P00V22_A511ContagemResultado_HoraCnt[0];
            A1553ContagemResultado_CntSrvCod = P00V22_A1553ContagemResultado_CntSrvCod[0];
            n1553ContagemResultado_CntSrvCod = P00V22_n1553ContagemResultado_CntSrvCod[0];
            A1596ContagemResultado_CntSrvPrc = P00V22_A1596ContagemResultado_CntSrvPrc[0];
            n1596ContagemResultado_CntSrvPrc = P00V22_n1596ContagemResultado_CntSrvPrc[0];
            A800ContagemResultado_Deflator = A1596ContagemResultado_CntSrvPrc;
            n800ContagemResultado_Deflator = false;
            /* Using cursor P00V23 */
            pr_default.execute(1, new Object[] {n800ContagemResultado_Deflator, A800ContagemResultado_Deflator, A456ContagemResultado_Codigo, A473ContagemResultado_DataCnt, A511ContagemResultado_HoraCnt});
            pr_default.close(1);
            dsDefault.SmartCacheProvider.SetUpdated("ContagemResultadoContagens") ;
            pr_default.readNext(0);
         }
         pr_default.close(0);
         this.cleanup();
      }

      public override void cleanup( )
      {
         context.CommitDataStores( "PRC_CorrigirDeflator");
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         scmdbuf = "";
         P00V22_A1553ContagemResultado_CntSrvCod = new int[1] ;
         P00V22_n1553ContagemResultado_CntSrvCod = new bool[] {false} ;
         P00V22_A456ContagemResultado_Codigo = new int[1] ;
         P00V22_A517ContagemResultado_Ultima = new bool[] {false} ;
         P00V22_A1596ContagemResultado_CntSrvPrc = new decimal[1] ;
         P00V22_n1596ContagemResultado_CntSrvPrc = new bool[] {false} ;
         P00V22_A800ContagemResultado_Deflator = new decimal[1] ;
         P00V22_n800ContagemResultado_Deflator = new bool[] {false} ;
         P00V22_A473ContagemResultado_DataCnt = new DateTime[] {DateTime.MinValue} ;
         P00V22_A511ContagemResultado_HoraCnt = new String[] {""} ;
         A473ContagemResultado_DataCnt = DateTime.MinValue;
         A511ContagemResultado_HoraCnt = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.prc_corrigirdeflator__default(),
            new Object[][] {
                new Object[] {
               P00V22_A1553ContagemResultado_CntSrvCod, P00V22_n1553ContagemResultado_CntSrvCod, P00V22_A456ContagemResultado_Codigo, P00V22_A517ContagemResultado_Ultima, P00V22_A1596ContagemResultado_CntSrvPrc, P00V22_n1596ContagemResultado_CntSrvPrc, P00V22_A800ContagemResultado_Deflator, P00V22_n800ContagemResultado_Deflator, P00V22_A473ContagemResultado_DataCnt, P00V22_A511ContagemResultado_HoraCnt
               }
               , new Object[] {
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int A456ContagemResultado_Codigo ;
      private int A1553ContagemResultado_CntSrvCod ;
      private decimal A1596ContagemResultado_CntSrvPrc ;
      private decimal A800ContagemResultado_Deflator ;
      private String scmdbuf ;
      private String A511ContagemResultado_HoraCnt ;
      private DateTime A473ContagemResultado_DataCnt ;
      private bool n1553ContagemResultado_CntSrvCod ;
      private bool A517ContagemResultado_Ultima ;
      private bool n1596ContagemResultado_CntSrvPrc ;
      private bool n800ContagemResultado_Deflator ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private int aP0_ContagemResultado_Codigo ;
      private IDataStoreProvider pr_default ;
      private int[] P00V22_A1553ContagemResultado_CntSrvCod ;
      private bool[] P00V22_n1553ContagemResultado_CntSrvCod ;
      private int[] P00V22_A456ContagemResultado_Codigo ;
      private bool[] P00V22_A517ContagemResultado_Ultima ;
      private decimal[] P00V22_A1596ContagemResultado_CntSrvPrc ;
      private bool[] P00V22_n1596ContagemResultado_CntSrvPrc ;
      private decimal[] P00V22_A800ContagemResultado_Deflator ;
      private bool[] P00V22_n800ContagemResultado_Deflator ;
      private DateTime[] P00V22_A473ContagemResultado_DataCnt ;
      private String[] P00V22_A511ContagemResultado_HoraCnt ;
   }

   public class prc_corrigirdeflator__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new UpdateCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00V22 ;
          prmP00V22 = new Object[] {
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmP00V23 ;
          prmP00V23 = new Object[] {
          new Object[] {"@ContagemResultado_Deflator",SqlDbType.Decimal,6,3} ,
          new Object[] {"@ContagemResultado_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@ContagemResultado_DataCnt",SqlDbType.DateTime,8,0} ,
          new Object[] {"@ContagemResultado_HoraCnt",SqlDbType.Char,5,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00V22", "SELECT T2.[ContagemResultado_CntSrvCod] AS ContagemResultado_CntSrvCod, T1.[ContagemResultado_Codigo], T1.[ContagemResultado_Ultima], T3.[Servico_Percentual] AS ContagemResultado_CntSrvPrc, T1.[ContagemResultado_Deflator], T1.[ContagemResultado_DataCnt], T1.[ContagemResultado_HoraCnt] FROM (([ContagemResultadoContagens] T1 WITH (UPDLOCK) INNER JOIN [ContagemResultado] T2 WITH (NOLOCK) ON T2.[ContagemResultado_Codigo] = T1.[ContagemResultado_Codigo]) LEFT JOIN [ContratoServicos] T3 WITH (NOLOCK) ON T3.[ContratoServicos_Codigo] = T2.[ContagemResultado_CntSrvCod]) WHERE (T1.[ContagemResultado_Codigo] = @ContagemResultado_Codigo) AND (T1.[ContagemResultado_Ultima] = 1) ORDER BY T1.[ContagemResultado_Codigo] ",true, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00V22,1,0,true,false )
             ,new CursorDef("P00V23", "UPDATE [ContagemResultadoContagens] SET [ContagemResultado_Deflator]=@ContagemResultado_Deflator  WHERE [ContagemResultado_Codigo] = @ContagemResultado_Codigo AND [ContagemResultado_DataCnt] = @ContagemResultado_DataCnt AND [ContagemResultado_HoraCnt] = @ContagemResultado_HoraCnt", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP00V23)
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((bool[]) buf[3])[0] = rslt.getBool(3) ;
                ((decimal[]) buf[4])[0] = rslt.getDecimal(4) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                ((decimal[]) buf[6])[0] = rslt.getDecimal(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((DateTime[]) buf[8])[0] = rslt.getGXDate(6) ;
                ((String[]) buf[9])[0] = rslt.getString(7, 5) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Decimal );
                }
                else
                {
                   stmt.SetParameter(1, (decimal)parms[1]);
                }
                stmt.SetParameter(2, (int)parms[2]);
                stmt.SetParameter(3, (DateTime)parms[3]);
                stmt.SetParameter(4, (String)parms[4]);
                return;
       }
    }

 }

}
