/*
               File: ViewAmbienteTecnologico
        Description: View Ambiente Tecnologico
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/3/2020 20:20:15.87
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class viewambientetecnologico : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public viewambientetecnologico( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public viewambientetecnologico( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_AmbienteTecnologico_Codigo ,
                           String aP1_TabCode )
      {
         this.AV9AmbienteTecnologico_Codigo = aP0_AmbienteTecnologico_Codigo;
         this.AV7TabCode = aP1_TabCode;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               AV9AmbienteTecnologico_Codigo = (int)(NumberUtil.Val( gxfirstwebparm, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9AmbienteTecnologico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9AmbienteTecnologico_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vAMBIENTETECNOLOGICO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV9AmbienteTecnologico_Codigo), "ZZZZZ9")));
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
               {
                  AV7TabCode = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7TabCode", AV7TabCode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vTABCODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV7TabCode, ""))));
               }
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PA9S2( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            START9S2( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20205320201593");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("viewambientetecnologico.aspx") + "?" + UrlEncode("" +AV9AmbienteTecnologico_Codigo) + "," + UrlEncode(StringUtil.RTrim(AV7TabCode))+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "vAMBIENTETECNOLOGICO_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV9AmbienteTecnologico_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vTABCODE", StringUtil.RTrim( AV7TabCode));
         GxWebStd.gx_hidden_field( context, "gxhash_AMBIENTETECNOLOGICO_DESCRICAO", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A352AmbienteTecnologico_Descricao, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vAMBIENTETECNOLOGICO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV9AmbienteTecnologico_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vTABCODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV7TabCode, ""))));
         GxWebStd.gx_hidden_field( context, "gxhash_vAMBIENTETECNOLOGICO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV9AmbienteTecnologico_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vTABCODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV7TabCode, ""))));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = "hsh" + "ViewAmbienteTecnologico";
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( A352AmbienteTecnologico_Descricao, "@!"));
         GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("viewambientetecnologico:[SendSecurityCheck value for]"+"AmbienteTecnologico_Descricao:"+StringUtil.RTrim( context.localUtil.Format( A352AmbienteTecnologico_Descricao, "@!")));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
         if ( ! ( WebComp_Tabbedview == null ) )
         {
            WebComp_Tabbedview.componentjscripts();
         }
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WE9S2( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVT9S2( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return false ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("viewambientetecnologico.aspx") + "?" + UrlEncode("" +AV9AmbienteTecnologico_Codigo) + "," + UrlEncode(StringUtil.RTrim(AV7TabCode)) ;
      }

      public override String GetPgmname( )
      {
         return "ViewAmbienteTecnologico" ;
      }

      public override String GetPgmdesc( )
      {
         return "View Ambiente Tecnologico" ;
      }

      protected void WB9S0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, "", "", "", "false");
            wb_table1_2_9S2( true) ;
         }
         else
         {
            wb_table1_2_9S2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_9S2e( bool wbgen )
      {
         if ( wbgen )
         {
         }
         wbLoad = true;
      }

      protected void START9S2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "View Ambiente Tecnologico", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUP9S0( ) ;
      }

      protected void WS9S2( )
      {
         START9S2( ) ;
         EVT9S2( ) ;
      }

      protected void EVT9S2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E119S2 */
                              E119S2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E129S2 */
                              E129S2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              context.wbHandled = 1;
                              if ( ! wbErr )
                              {
                                 Rfr0gs = false;
                                 if ( ! Rfr0gs )
                                 {
                                 }
                                 dynload_actions( ) ;
                              }
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                        }
                     }
                     else if ( StringUtil.StrCmp(sEvtType, "W") == 0 )
                     {
                        sEvtType = StringUtil.Left( sEvt, 4);
                        sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-4));
                        nCmpId = (short)(NumberUtil.Val( sEvtType, "."));
                        if ( nCmpId == 16 )
                        {
                           OldTabbedview = cgiGet( "W0016");
                           if ( ( StringUtil.Len( OldTabbedview) == 0 ) || ( StringUtil.StrCmp(OldTabbedview, WebComp_Tabbedview_Component) != 0 ) )
                           {
                              WebComp_Tabbedview = getWebComponent(GetType(), "GeneXus.Programs", OldTabbedview, new Object[] {context} );
                              WebComp_Tabbedview.ComponentInit();
                              WebComp_Tabbedview.Name = "OldTabbedview";
                              WebComp_Tabbedview_Component = OldTabbedview;
                           }
                           if ( StringUtil.Len( WebComp_Tabbedview_Component) != 0 )
                           {
                              WebComp_Tabbedview.componentprocess("W0016", "", sEvt);
                           }
                           WebComp_Tabbedview_Component = OldTabbedview;
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WE9S2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PA9S2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RF9S2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      protected void RF9S2( )
      {
         initialize_formulas( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            if ( 1 != 0 )
            {
               if ( StringUtil.Len( WebComp_Tabbedview_Component) != 0 )
               {
                  WebComp_Tabbedview.componentstart();
               }
            }
         }
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Using cursor H009S2 */
            pr_default.execute(0, new Object[] {AV9AmbienteTecnologico_Codigo});
            while ( (pr_default.getStatus(0) != 101) )
            {
               A351AmbienteTecnologico_Codigo = H009S2_A351AmbienteTecnologico_Codigo[0];
               n351AmbienteTecnologico_Codigo = H009S2_n351AmbienteTecnologico_Codigo[0];
               A352AmbienteTecnologico_Descricao = H009S2_A352AmbienteTecnologico_Descricao[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A352AmbienteTecnologico_Descricao", A352AmbienteTecnologico_Descricao);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_AMBIENTETECNOLOGICO_DESCRICAO", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A352AmbienteTecnologico_Descricao, "@!"))));
               /* Execute user event: E129S2 */
               E129S2 ();
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(0);
            WB9S0( ) ;
         }
      }

      protected void STRUP9S0( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E119S2 */
         E119S2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            A352AmbienteTecnologico_Descricao = StringUtil.Upper( cgiGet( edtAmbienteTecnologico_Descricao_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A352AmbienteTecnologico_Descricao", A352AmbienteTecnologico_Descricao);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_AMBIENTETECNOLOGICO_DESCRICAO", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A352AmbienteTecnologico_Descricao, "@!"))));
            /* Read saved values. */
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            forbiddenHiddens = "hsh" + "ViewAmbienteTecnologico";
            A352AmbienteTecnologico_Descricao = cgiGet( edtAmbienteTecnologico_Descricao_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A352AmbienteTecnologico_Descricao", A352AmbienteTecnologico_Descricao);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_AMBIENTETECNOLOGICO_DESCRICAO", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A352AmbienteTecnologico_Descricao, "@!"))));
            forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( A352AmbienteTecnologico_Descricao, "@!"));
            hsh = cgiGet( "hsh");
            if ( ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
            {
               GXUtil.WriteLog("viewambientetecnologico:[SecurityCheckFailed value for]"+"AmbienteTecnologico_Descricao:"+StringUtil.RTrim( context.localUtil.Format( A352AmbienteTecnologico_Descricao, "@!")));
               GxWebError = 1;
               context.HttpContext.Response.StatusDescription = 403.ToString();
               context.HttpContext.Response.StatusCode = 403;
               context.WriteHtmlText( "<title>403 Forbidden</title>") ;
               context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
               context.WriteHtmlText( "<p /><hr />") ;
               GXUtil.WriteLog("send_http_error_code " + 403.ToString());
               return  ;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E119S2 */
         E119S2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E119S2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         lblWorkwithlink_Link = formatLink("wwambientetecnologico.aspx") ;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblWorkwithlink_Internalname, "Link", lblWorkwithlink_Link);
         AV15GXLvl9 = 0;
         /* Using cursor H009S3 */
         pr_default.execute(1, new Object[] {AV9AmbienteTecnologico_Codigo});
         while ( (pr_default.getStatus(1) != 101) )
         {
            A351AmbienteTecnologico_Codigo = H009S3_A351AmbienteTecnologico_Codigo[0];
            n351AmbienteTecnologico_Codigo = H009S3_n351AmbienteTecnologico_Codigo[0];
            A352AmbienteTecnologico_Descricao = H009S3_A352AmbienteTecnologico_Descricao[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A352AmbienteTecnologico_Descricao", A352AmbienteTecnologico_Descricao);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_AMBIENTETECNOLOGICO_DESCRICAO", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A352AmbienteTecnologico_Descricao, "@!"))));
            AV15GXLvl9 = 1;
            Form.Caption = A352AmbienteTecnologico_Descricao;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
            AV8Exists = true;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(1);
         if ( AV15GXLvl9 == 0 )
         {
            Form.Caption = "Registro n�o encontrado ";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
            AV8Exists = false;
         }
         if ( AV8Exists )
         {
            /* Execute user subroutine: 'LOADTABS' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            /* Object Property */
            if ( StringUtil.StrCmp(StringUtil.Lower( WebComp_Tabbedview_Component), StringUtil.Lower( "WWPBaseObjects.WWPTabbedView")) != 0 )
            {
               WebComp_Tabbedview = getWebComponent(GetType(), "GeneXus.Programs", "wwpbaseobjects.wwptabbedview", new Object[] {context} );
               WebComp_Tabbedview.ComponentInit();
               WebComp_Tabbedview.Name = "WWPBaseObjects.WWPTabbedView";
               WebComp_Tabbedview_Component = "WWPBaseObjects.WWPTabbedView";
            }
            if ( StringUtil.Len( WebComp_Tabbedview_Component) != 0 )
            {
               WebComp_Tabbedview.setjustcreated();
               WebComp_Tabbedview.componentprepare(new Object[] {(String)"W0016",(String)"",(IGxCollection)AV10Tabs,(String)AV7TabCode});
               WebComp_Tabbedview.componentbind(new Object[] {(String)"",(String)""});
            }
         }
      }

      protected void nextLoad( )
      {
      }

      protected void E129S2( )
      {
         /* Load Routine */
      }

      protected void S112( )
      {
         /* 'LOADTABS' Routine */
         AV10Tabs = new GxObjectCollection( context, "WWPTabOptions.TabOptionsItem", "GxEv3Up14_MeetrikaVs3", "wwpbaseobjects.SdtWWPTabOptions_TabOptionsItem", "GeneXus.Programs");
         AV11Tab = new wwpbaseobjects.SdtWWPTabOptions_TabOptionsItem(context);
         AV11Tab.gxTpr_Code = "General";
         AV11Tab.gxTpr_Description = "Dados";
         AV11Tab.gxTpr_Webcomponent = formatLink("ambientetecnologicogeneral.aspx") + "?" + UrlEncode("" +AV9AmbienteTecnologico_Codigo);
         AV11Tab.gxTpr_Link = formatLink("viewambientetecnologico.aspx") + "?" + UrlEncode("" +AV9AmbienteTecnologico_Codigo) + "," + UrlEncode(StringUtil.RTrim(AV11Tab.gxTpr_Code));
         AV11Tab.gxTpr_Includeinpanel = 0;
         AV11Tab.gxTpr_Collapsable = true;
         AV11Tab.gxTpr_Collapsedbydefault = false;
         AV10Tabs.Add(AV11Tab, 0);
         AV11Tab = new wwpbaseobjects.SdtWWPTabOptions_TabOptionsItem(context);
         AV11Tab.gxTpr_Code = "AmbienteTecnologicoTecnologias";
         AV11Tab.gxTpr_Description = "Tecnolog�as";
         AV11Tab.gxTpr_Webcomponent = formatLink("ambientetecnologicoambientetecnologicotecnologiaswc.aspx") + "?" + UrlEncode("" +AV9AmbienteTecnologico_Codigo);
         AV11Tab.gxTpr_Link = formatLink("viewambientetecnologico.aspx") + "?" + UrlEncode("" +AV9AmbienteTecnologico_Codigo) + "," + UrlEncode(StringUtil.RTrim(AV11Tab.gxTpr_Code));
         AV11Tab.gxTpr_Includeinpanel = 1;
         AV11Tab.gxTpr_Collapsable = true;
         AV11Tab.gxTpr_Collapsedbydefault = true;
         AV10Tabs.Add(AV11Tab, 0);
         AV11Tab = new wwpbaseobjects.SdtWWPTabOptions_TabOptionsItem(context);
         AV11Tab.gxTpr_Code = "Sistema";
         AV11Tab.gxTpr_Description = "Sistemas";
         AV11Tab.gxTpr_Webcomponent = formatLink("ambientetecnologicosistemas.aspx") + "?" + UrlEncode("" +AV9AmbienteTecnologico_Codigo);
         AV11Tab.gxTpr_Link = formatLink("viewambientetecnologico.aspx") + "?" + UrlEncode("" +AV9AmbienteTecnologico_Codigo) + "," + UrlEncode(StringUtil.RTrim(AV11Tab.gxTpr_Code));
         AV11Tab.gxTpr_Includeinpanel = 1;
         AV11Tab.gxTpr_Collapsable = true;
         AV11Tab.gxTpr_Collapsedbydefault = false;
         AV10Tabs.Add(AV11Tab, 0);
         /* Using cursor H009S4 */
         pr_default.execute(2, new Object[] {AV9AmbienteTecnologico_Codigo});
         while ( (pr_default.getStatus(2) != 101) )
         {
            A351AmbienteTecnologico_Codigo = H009S4_A351AmbienteTecnologico_Codigo[0];
            n351AmbienteTecnologico_Codigo = H009S4_n351AmbienteTecnologico_Codigo[0];
            AV12Count = 0;
            /* Using cursor H009S5 */
            pr_default.execute(3, new Object[] {n351AmbienteTecnologico_Codigo, A351AmbienteTecnologico_Codigo});
            while ( (pr_default.getStatus(3) != 101) )
            {
               AV12Count = (short)(AV12Count+1);
               ((wwpbaseobjects.SdtWWPTabOptions_TabOptionsItem)AV10Tabs.Item(2)).gxTpr_Description = "Tecnolog�as ("+StringUtil.Trim( StringUtil.Str( (decimal)(AV12Count), 4, 0))+")";
               pr_default.readNext(3);
            }
            pr_default.close(3);
            AV12Count = 0;
            /* Using cursor H009S6 */
            pr_default.execute(4, new Object[] {n351AmbienteTecnologico_Codigo, A351AmbienteTecnologico_Codigo});
            while ( (pr_default.getStatus(4) != 101) )
            {
               AV12Count = (short)(AV12Count+1);
               ((wwpbaseobjects.SdtWWPTabOptions_TabOptionsItem)AV10Tabs.Item(3)).gxTpr_Description = "Sistemas ("+StringUtil.Trim( StringUtil.Str( (decimal)(AV12Count), 4, 0))+")";
               pr_default.readNext(4);
            }
            pr_default.close(4);
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(2);
      }

      protected void wb_table1_2_9S2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableContentNoMargin", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TextBlockTitleCell'>") ;
            wb_table2_5_9S2( true) ;
         }
         else
         {
            wb_table2_5_9S2( false) ;
         }
         return  ;
      }

      protected void wb_table2_5_9S2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td data-align=\"right\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-right;text-align:-moz-right;text-align:-webkit-right")+"\" class='TableTitleCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblWorkwithlink_Internalname, "Voltar", lblWorkwithlink_Link, "", lblWorkwithlink_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockLink", 0, "", 1, 1, 0, "HLP_ViewAmbienteTecnologico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"3\" >") ;
            if ( ! isFullAjaxMode( ) )
            {
               /* WebComponent */
               GxWebStd.gx_hidden_field( context, "W0016"+"", StringUtil.RTrim( WebComp_Tabbedview_Component));
               context.WriteHtmlText( "<div") ;
               GxWebStd.ClassAttribute( context, "gxwebcomponent");
               context.WriteHtmlText( " id=\""+"gxHTMLWrpW0016"+""+"\""+"") ;
               context.WriteHtmlText( ">") ;
               if ( StringUtil.Len( WebComp_Tabbedview_Component) != 0 )
               {
                  if ( StringUtil.StrCmp(StringUtil.Lower( OldTabbedview), StringUtil.Lower( WebComp_Tabbedview_Component)) != 0 )
                  {
                     context.httpAjaxContext.ajax_rspStartCmp("gxHTMLWrpW0016"+"");
                  }
                  WebComp_Tabbedview.componentdraw();
                  if ( StringUtil.StrCmp(StringUtil.Lower( OldTabbedview), StringUtil.Lower( WebComp_Tabbedview_Component)) != 0 )
                  {
                     context.httpAjaxContext.ajax_rspEndCmp();
                  }
               }
               context.WriteHtmlText( "</div>") ;
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_9S2e( true) ;
         }
         else
         {
            wb_table1_2_9S2e( false) ;
         }
      }

      protected void wb_table2_5_9S2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedviewtitle_Internalname, tblTablemergedviewtitle_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblViewtitle_Internalname, "Ambiente Operacional :: ", "", "", lblViewtitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_ViewAmbienteTecnologico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtAmbienteTecnologico_Descricao_Internalname, A352AmbienteTecnologico_Descricao, StringUtil.RTrim( context.localUtil.Format( A352AmbienteTecnologico_Descricao, "@!")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtAmbienteTecnologico_Descricao_Jsonclick, 0, "AttributeTitleWWP", "", "", "", 1, 0, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Descricao", "left", true, "HLP_ViewAmbienteTecnologico.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_9S2e( true) ;
         }
         else
         {
            wb_table2_5_9S2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV9AmbienteTecnologico_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9AmbienteTecnologico_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9AmbienteTecnologico_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vAMBIENTETECNOLOGICO_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV9AmbienteTecnologico_Codigo), "ZZZZZ9")));
         AV7TabCode = (String)getParm(obj,1);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7TabCode", AV7TabCode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vTABCODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV7TabCode, ""))));
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA9S2( ) ;
         WS9S2( ) ;
         WE9S2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?2249787");
         if ( ! ( WebComp_Tabbedview == null ) )
         {
            if ( StringUtil.Len( WebComp_Tabbedview_Component) != 0 )
            {
               WebComp_Tabbedview.componentthemes();
            }
         }
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20205320201623");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("viewambientetecnologico.js", "?20205320201624");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblViewtitle_Internalname = "VIEWTITLE";
         edtAmbienteTecnologico_Descricao_Internalname = "AMBIENTETECNOLOGICO_DESCRICAO";
         tblTablemergedviewtitle_Internalname = "TABLEMERGEDVIEWTITLE";
         lblWorkwithlink_Internalname = "WORKWITHLINK";
         tblTablemain_Internalname = "TABLEMAIN";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtAmbienteTecnologico_Descricao_Jsonclick = "";
         lblWorkwithlink_Link = "";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "View Ambiente Tecnologico";
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         wcpOAV7TabCode = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         A352AmbienteTecnologico_Descricao = "";
         GXKey = "";
         forbiddenHiddens = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         OldTabbedview = "";
         WebComp_Tabbedview_Component = "";
         scmdbuf = "";
         H009S2_A351AmbienteTecnologico_Codigo = new int[1] ;
         H009S2_n351AmbienteTecnologico_Codigo = new bool[] {false} ;
         H009S2_A352AmbienteTecnologico_Descricao = new String[] {""} ;
         hsh = "";
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         H009S3_A351AmbienteTecnologico_Codigo = new int[1] ;
         H009S3_n351AmbienteTecnologico_Codigo = new bool[] {false} ;
         H009S3_A352AmbienteTecnologico_Descricao = new String[] {""} ;
         AV10Tabs = new GxObjectCollection( context, "WWPTabOptions.TabOptionsItem", "GxEv3Up14_MeetrikaVs3", "wwpbaseobjects.SdtWWPTabOptions_TabOptionsItem", "GeneXus.Programs");
         AV11Tab = new wwpbaseobjects.SdtWWPTabOptions_TabOptionsItem(context);
         H009S4_A351AmbienteTecnologico_Codigo = new int[1] ;
         H009S4_n351AmbienteTecnologico_Codigo = new bool[] {false} ;
         H009S5_A131Tecnologia_Codigo = new int[1] ;
         H009S5_A351AmbienteTecnologico_Codigo = new int[1] ;
         H009S5_n351AmbienteTecnologico_Codigo = new bool[] {false} ;
         H009S6_A127Sistema_Codigo = new int[1] ;
         H009S6_A351AmbienteTecnologico_Codigo = new int[1] ;
         H009S6_n351AmbienteTecnologico_Codigo = new bool[] {false} ;
         sStyleString = "";
         lblWorkwithlink_Jsonclick = "";
         lblViewtitle_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.viewambientetecnologico__default(),
            new Object[][] {
                new Object[] {
               H009S2_A351AmbienteTecnologico_Codigo, H009S2_A352AmbienteTecnologico_Descricao
               }
               , new Object[] {
               H009S3_A351AmbienteTecnologico_Codigo, H009S3_A352AmbienteTecnologico_Descricao
               }
               , new Object[] {
               H009S4_A351AmbienteTecnologico_Codigo
               }
               , new Object[] {
               H009S5_A131Tecnologia_Codigo, H009S5_A351AmbienteTecnologico_Codigo
               }
               , new Object[] {
               H009S6_A127Sistema_Codigo, H009S6_A351AmbienteTecnologico_Codigo, H009S6_n351AmbienteTecnologico_Codigo
               }
            }
         );
         WebComp_Tabbedview = new GeneXus.Http.GXNullWebComponent();
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short wbEnd ;
      private short wbStart ;
      private short nCmpId ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short AV15GXLvl9 ;
      private short AV12Count ;
      private short nGXWrapped ;
      private int AV9AmbienteTecnologico_Codigo ;
      private int wcpOAV9AmbienteTecnologico_Codigo ;
      private int A351AmbienteTecnologico_Codigo ;
      private int idxLst ;
      private String AV7TabCode ;
      private String wcpOAV7TabCode ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String GXKey ;
      private String forbiddenHiddens ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String OldTabbedview ;
      private String WebComp_Tabbedview_Component ;
      private String scmdbuf ;
      private String edtAmbienteTecnologico_Descricao_Internalname ;
      private String hsh ;
      private String lblWorkwithlink_Link ;
      private String lblWorkwithlink_Internalname ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String lblWorkwithlink_Jsonclick ;
      private String tblTablemergedviewtitle_Internalname ;
      private String lblViewtitle_Internalname ;
      private String lblViewtitle_Jsonclick ;
      private String edtAmbienteTecnologico_Descricao_Jsonclick ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n351AmbienteTecnologico_Codigo ;
      private bool returnInSub ;
      private bool AV8Exists ;
      private String A352AmbienteTecnologico_Descricao ;
      private GXWebComponent WebComp_Tabbedview ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private int[] H009S2_A351AmbienteTecnologico_Codigo ;
      private bool[] H009S2_n351AmbienteTecnologico_Codigo ;
      private String[] H009S2_A352AmbienteTecnologico_Descricao ;
      private int[] H009S3_A351AmbienteTecnologico_Codigo ;
      private bool[] H009S3_n351AmbienteTecnologico_Codigo ;
      private String[] H009S3_A352AmbienteTecnologico_Descricao ;
      private int[] H009S4_A351AmbienteTecnologico_Codigo ;
      private bool[] H009S4_n351AmbienteTecnologico_Codigo ;
      private int[] H009S5_A131Tecnologia_Codigo ;
      private int[] H009S5_A351AmbienteTecnologico_Codigo ;
      private bool[] H009S5_n351AmbienteTecnologico_Codigo ;
      private int[] H009S6_A127Sistema_Codigo ;
      private int[] H009S6_A351AmbienteTecnologico_Codigo ;
      private bool[] H009S6_n351AmbienteTecnologico_Codigo ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtWWPTabOptions_TabOptionsItem ))]
      private IGxCollection AV10Tabs ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTabOptions_TabOptionsItem AV11Tab ;
   }

   public class viewambientetecnologico__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH009S2 ;
          prmH009S2 = new Object[] {
          new Object[] {"@AV9AmbienteTecnologico_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH009S3 ;
          prmH009S3 = new Object[] {
          new Object[] {"@AV9AmbienteTecnologico_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH009S4 ;
          prmH009S4 = new Object[] {
          new Object[] {"@AV9AmbienteTecnologico_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH009S5 ;
          prmH009S5 = new Object[] {
          new Object[] {"@AmbienteTecnologico_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH009S6 ;
          prmH009S6 = new Object[] {
          new Object[] {"@AmbienteTecnologico_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H009S2", "SELECT [AmbienteTecnologico_Codigo], [AmbienteTecnologico_Descricao] FROM [AmbienteTecnologico] WITH (NOLOCK) WHERE [AmbienteTecnologico_Codigo] = @AV9AmbienteTecnologico_Codigo ORDER BY [AmbienteTecnologico_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH009S2,1,0,true,true )
             ,new CursorDef("H009S3", "SELECT [AmbienteTecnologico_Codigo], [AmbienteTecnologico_Descricao] FROM [AmbienteTecnologico] WITH (NOLOCK) WHERE [AmbienteTecnologico_Codigo] = @AV9AmbienteTecnologico_Codigo ORDER BY [AmbienteTecnologico_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH009S3,1,0,false,true )
             ,new CursorDef("H009S4", "SELECT [AmbienteTecnologico_Codigo] FROM [AmbienteTecnologico] WITH (NOLOCK) WHERE [AmbienteTecnologico_Codigo] = @AV9AmbienteTecnologico_Codigo ORDER BY [AmbienteTecnologico_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH009S4,1,0,true,true )
             ,new CursorDef("H009S5", "SELECT [Tecnologia_Codigo], [AmbienteTecnologico_Codigo] FROM [AmbienteTecnologicoTecnologias] WITH (NOLOCK) WHERE [AmbienteTecnologico_Codigo] = @AmbienteTecnologico_Codigo ORDER BY [AmbienteTecnologico_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH009S5,100,0,false,false )
             ,new CursorDef("H009S6", "SELECT [Sistema_Codigo], [AmbienteTecnologico_Codigo] FROM [Sistema] WITH (NOLOCK) WHERE [AmbienteTecnologico_Codigo] = @AmbienteTecnologico_Codigo ORDER BY [AmbienteTecnologico_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH009S6,100,0,false,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 3 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
             case 4 :
                if ( (bool)parms[0] )
                {
                   stmt.setNull( 1 , SqlDbType.Int );
                }
                else
                {
                   stmt.SetParameter(1, (int)parms[1]);
                }
                return;
       }
    }

 }

}
