/*
               File: ViewLoteArquivoAnexo
        Description: View Lote Arquivo Anexo
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 23:27:59.92
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class viewlotearquivoanexo : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public viewlotearquivoanexo( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public viewlotearquivoanexo( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_LoteArquivoAnexo_LoteCod ,
                           DateTime aP1_LoteArquivoAnexo_Data ,
                           String aP2_TabCode )
      {
         this.AV9LoteArquivoAnexo_LoteCod = aP0_LoteArquivoAnexo_LoteCod;
         this.AV10LoteArquivoAnexo_Data = aP1_LoteArquivoAnexo_Data;
         this.AV7TabCode = aP2_TabCode;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               AV9LoteArquivoAnexo_LoteCod = (int)(NumberUtil.Val( gxfirstwebparm, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9LoteArquivoAnexo_LoteCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9LoteArquivoAnexo_LoteCod), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vLOTEARQUIVOANEXO_LOTECOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV9LoteArquivoAnexo_LoteCod), "ZZZZZ9")));
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
               {
                  AV10LoteArquivoAnexo_Data = context.localUtil.ParseDTimeParm( GetNextPar( ));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10LoteArquivoAnexo_Data", context.localUtil.TToC( AV10LoteArquivoAnexo_Data, 8, 5, 0, 3, "/", ":", " "));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vLOTEARQUIVOANEXO_DATA", GetSecureSignedToken( "", context.localUtil.Format( AV10LoteArquivoAnexo_Data, "99/99/99 99:99")));
                  AV7TabCode = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7TabCode", AV7TabCode);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vTABCODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV7TabCode, ""))));
               }
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("masterpagemeetrika", "GeneXus.Programs.masterpagemeetrika", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PAEV2( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            STARTEV2( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?202042823275995");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("viewlotearquivoanexo.aspx") + "?" + UrlEncode("" +AV9LoteArquivoAnexo_LoteCod) + "," + UrlEncode(DateTimeUtil.FormatDateTimeParm(AV10LoteArquivoAnexo_Data)) + "," + UrlEncode(StringUtil.RTrim(AV7TabCode))+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         GxWebStd.gx_hidden_field( context, "vLOTEARQUIVOANEXO_LOTECOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV9LoteArquivoAnexo_LoteCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vLOTEARQUIVOANEXO_DATA", context.localUtil.TToC( AV10LoteArquivoAnexo_Data, 10, 8, 0, 0, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "vTABCODE", StringUtil.RTrim( AV7TabCode));
         GxWebStd.gx_hidden_field( context, "gxhash_LOTEARQUIVOANEXO_NOMEARQ", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A839LoteArquivoAnexo_NomeArq, ""))));
         GxWebStd.gx_hidden_field( context, "gxhash_vLOTEARQUIVOANEXO_LOTECOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV9LoteArquivoAnexo_LoteCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vLOTEARQUIVOANEXO_DATA", GetSecureSignedToken( "", context.localUtil.Format( AV10LoteArquivoAnexo_Data, "99/99/99 99:99")));
         GxWebStd.gx_hidden_field( context, "gxhash_vTABCODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV7TabCode, ""))));
         GxWebStd.gx_hidden_field( context, "gxhash_vLOTEARQUIVOANEXO_LOTECOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV9LoteArquivoAnexo_LoteCod), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vLOTEARQUIVOANEXO_DATA", GetSecureSignedToken( "", context.localUtil.Format( AV10LoteArquivoAnexo_Data, "99/99/99 99:99")));
         GxWebStd.gx_hidden_field( context, "gxhash_vTABCODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV7TabCode, ""))));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = "hsh" + "ViewLoteArquivoAnexo";
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( A839LoteArquivoAnexo_NomeArq, ""));
         GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("viewlotearquivoanexo:[SendSecurityCheck value for]"+"LoteArquivoAnexo_NomeArq:"+StringUtil.RTrim( context.localUtil.Format( A839LoteArquivoAnexo_NomeArq, "")));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
         if ( ! ( WebComp_Tabbedview == null ) )
         {
            WebComp_Tabbedview.componentjscripts();
         }
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WEEV2( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVTEV2( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return false ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("viewlotearquivoanexo.aspx") + "?" + UrlEncode("" +AV9LoteArquivoAnexo_LoteCod) + "," + UrlEncode(DateTimeUtil.FormatDateTimeParm(AV10LoteArquivoAnexo_Data)) + "," + UrlEncode(StringUtil.RTrim(AV7TabCode)) ;
      }

      public override String GetPgmname( )
      {
         return "ViewLoteArquivoAnexo" ;
      }

      public override String GetPgmdesc( )
      {
         return "View Lote Arquivo Anexo" ;
      }

      protected void WBEV0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, "", "", "", "false");
            wb_table1_2_EV2( true) ;
         }
         else
         {
            wb_table1_2_EV2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_EV2e( bool wbgen )
      {
         if ( wbgen )
         {
         }
         wbLoad = true;
      }

      protected void STARTEV2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "View Lote Arquivo Anexo", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPEV0( ) ;
      }

      protected void WSEV2( )
      {
         STARTEV2( ) ;
         EVTEV2( ) ;
      }

      protected void EVTEV2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E11EV2 */
                              E11EV2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: E12EV2 */
                              E12EV2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              context.wbHandled = 1;
                              if ( ! wbErr )
                              {
                                 Rfr0gs = false;
                                 if ( ! Rfr0gs )
                                 {
                                 }
                                 dynload_actions( ) ;
                              }
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                        }
                     }
                     else if ( StringUtil.StrCmp(sEvtType, "W") == 0 )
                     {
                        sEvtType = StringUtil.Left( sEvt, 4);
                        sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-4));
                        nCmpId = (short)(NumberUtil.Val( sEvtType, "."));
                        if ( nCmpId == 16 )
                        {
                           OldTabbedview = cgiGet( "W0016");
                           if ( ( StringUtil.Len( OldTabbedview) == 0 ) || ( StringUtil.StrCmp(OldTabbedview, WebComp_Tabbedview_Component) != 0 ) )
                           {
                              WebComp_Tabbedview = getWebComponent(GetType(), "GeneXus.Programs", OldTabbedview, new Object[] {context} );
                              WebComp_Tabbedview.ComponentInit();
                              WebComp_Tabbedview.Name = "OldTabbedview";
                              WebComp_Tabbedview_Component = OldTabbedview;
                           }
                           if ( StringUtil.Len( WebComp_Tabbedview_Component) != 0 )
                           {
                              WebComp_Tabbedview.componentprocess("W0016", "", sEvt);
                           }
                           WebComp_Tabbedview_Component = OldTabbedview;
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WEEV2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PAEV2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFEV2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      protected void RFEV2( )
      {
         initialize_formulas( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            if ( 1 != 0 )
            {
               if ( StringUtil.Len( WebComp_Tabbedview_Component) != 0 )
               {
                  WebComp_Tabbedview.componentstart();
               }
            }
         }
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Using cursor H00EV2 */
            pr_default.execute(0, new Object[] {AV9LoteArquivoAnexo_LoteCod, AV10LoteArquivoAnexo_Data});
            while ( (pr_default.getStatus(0) != 101) )
            {
               A836LoteArquivoAnexo_Data = H00EV2_A836LoteArquivoAnexo_Data[0];
               A841LoteArquivoAnexo_LoteCod = H00EV2_A841LoteArquivoAnexo_LoteCod[0];
               A839LoteArquivoAnexo_NomeArq = H00EV2_A839LoteArquivoAnexo_NomeArq[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A839LoteArquivoAnexo_NomeArq", A839LoteArquivoAnexo_NomeArq);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_LOTEARQUIVOANEXO_NOMEARQ", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A839LoteArquivoAnexo_NomeArq, ""))));
               n839LoteArquivoAnexo_NomeArq = H00EV2_n839LoteArquivoAnexo_NomeArq[0];
               /* Execute user event: E12EV2 */
               E12EV2 ();
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(0);
            WBEV0( ) ;
         }
      }

      protected void STRUPEV0( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11EV2 */
         E11EV2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            A839LoteArquivoAnexo_NomeArq = cgiGet( edtLoteArquivoAnexo_NomeArq_Internalname);
            n839LoteArquivoAnexo_NomeArq = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A839LoteArquivoAnexo_NomeArq", A839LoteArquivoAnexo_NomeArq);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_LOTEARQUIVOANEXO_NOMEARQ", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A839LoteArquivoAnexo_NomeArq, ""))));
            /* Read saved values. */
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            forbiddenHiddens = "hsh" + "ViewLoteArquivoAnexo";
            A839LoteArquivoAnexo_NomeArq = cgiGet( edtLoteArquivoAnexo_NomeArq_Internalname);
            n839LoteArquivoAnexo_NomeArq = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A839LoteArquivoAnexo_NomeArq", A839LoteArquivoAnexo_NomeArq);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_LOTEARQUIVOANEXO_NOMEARQ", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A839LoteArquivoAnexo_NomeArq, ""))));
            forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( A839LoteArquivoAnexo_NomeArq, ""));
            hsh = cgiGet( "hsh");
            if ( ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
            {
               GXUtil.WriteLog("viewlotearquivoanexo:[SecurityCheckFailed value for]"+"LoteArquivoAnexo_NomeArq:"+StringUtil.RTrim( context.localUtil.Format( A839LoteArquivoAnexo_NomeArq, "")));
               GxWebError = 1;
               context.HttpContext.Response.StatusDescription = 403.ToString();
               context.HttpContext.Response.StatusCode = 403;
               context.WriteHtmlText( "<title>403 Forbidden</title>") ;
               context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
               context.WriteHtmlText( "<p /><hr />") ;
               GXUtil.WriteLog("send_http_error_code " + 403.ToString());
               return  ;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E11EV2 */
         E11EV2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E11EV2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV6WWPContext) ;
         lblWorkwithlink_Link = formatLink("wwlotearquivoanexo.aspx") ;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblWorkwithlink_Internalname, "Link", lblWorkwithlink_Link);
         AV15GXLvl9 = 0;
         /* Using cursor H00EV3 */
         pr_default.execute(1, new Object[] {AV9LoteArquivoAnexo_LoteCod, AV10LoteArquivoAnexo_Data});
         while ( (pr_default.getStatus(1) != 101) )
         {
            A836LoteArquivoAnexo_Data = H00EV3_A836LoteArquivoAnexo_Data[0];
            A841LoteArquivoAnexo_LoteCod = H00EV3_A841LoteArquivoAnexo_LoteCod[0];
            A839LoteArquivoAnexo_NomeArq = H00EV3_A839LoteArquivoAnexo_NomeArq[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A839LoteArquivoAnexo_NomeArq", A839LoteArquivoAnexo_NomeArq);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_LOTEARQUIVOANEXO_NOMEARQ", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A839LoteArquivoAnexo_NomeArq, ""))));
            n839LoteArquivoAnexo_NomeArq = H00EV3_n839LoteArquivoAnexo_NomeArq[0];
            AV15GXLvl9 = 1;
            Form.Caption = A839LoteArquivoAnexo_NomeArq;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
            AV8Exists = true;
            /* Exiting from a For First loop. */
            if (true) break;
         }
         pr_default.close(1);
         if ( AV15GXLvl9 == 0 )
         {
            Form.Caption = "Registro n�o encontrado ";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption);
            AV8Exists = false;
         }
         if ( AV8Exists )
         {
            /* Execute user subroutine: 'LOADTABS' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            /* Object Property */
            if ( StringUtil.StrCmp(StringUtil.Lower( WebComp_Tabbedview_Component), StringUtil.Lower( "WWPBaseObjects.WWPTabbedView")) != 0 )
            {
               WebComp_Tabbedview = getWebComponent(GetType(), "GeneXus.Programs", "wwpbaseobjects.wwptabbedview", new Object[] {context} );
               WebComp_Tabbedview.ComponentInit();
               WebComp_Tabbedview.Name = "WWPBaseObjects.WWPTabbedView";
               WebComp_Tabbedview_Component = "WWPBaseObjects.WWPTabbedView";
            }
            if ( StringUtil.Len( WebComp_Tabbedview_Component) != 0 )
            {
               WebComp_Tabbedview.setjustcreated();
               WebComp_Tabbedview.componentprepare(new Object[] {(String)"W0016",(String)"",(IGxCollection)AV11Tabs,(String)AV7TabCode});
               WebComp_Tabbedview.componentbind(new Object[] {(String)"",(String)""});
            }
         }
         lblWorkwithlink_Link = formatLink("viewlote.aspx") + "?" + UrlEncode("" +AV9LoteArquivoAnexo_LoteCod) + "," + UrlEncode(StringUtil.RTrim("LoteArquivoAnexo"));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblWorkwithlink_Internalname, "Link", lblWorkwithlink_Link);
      }

      protected void nextLoad( )
      {
      }

      protected void E12EV2( )
      {
         /* Load Routine */
      }

      protected void S112( )
      {
         /* 'LOADTABS' Routine */
         AV11Tabs = new GxObjectCollection( context, "WWPTabOptions.TabOptionsItem", "GxEv3Up14_MeetrikaVs3", "wwpbaseobjects.SdtWWPTabOptions_TabOptionsItem", "GeneXus.Programs");
         AV12Tab = new wwpbaseobjects.SdtWWPTabOptions_TabOptionsItem(context);
         AV12Tab.gxTpr_Code = "General";
         AV12Tab.gxTpr_Description = "Arquivo anexado";
         AV12Tab.gxTpr_Webcomponent = formatLink("lotearquivoanexogeneral.aspx") + "?" + UrlEncode("" +AV9LoteArquivoAnexo_LoteCod) + "," + UrlEncode(DateTimeUtil.FormatDateTimeParm(AV10LoteArquivoAnexo_Data));
         AV12Tab.gxTpr_Link = formatLink("viewlotearquivoanexo.aspx") + "?" + UrlEncode("" +AV9LoteArquivoAnexo_LoteCod) + "," + UrlEncode(DateTimeUtil.FormatDateTimeParm(AV10LoteArquivoAnexo_Data)) + "," + UrlEncode(StringUtil.RTrim(AV12Tab.gxTpr_Code));
         AV12Tab.gxTpr_Includeinpanel = 0;
         AV12Tab.gxTpr_Collapsable = true;
         AV12Tab.gxTpr_Collapsedbydefault = false;
         AV11Tabs.Add(AV12Tab, 0);
      }

      protected void wb_table1_2_EV2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableContentNoMargin", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TextBlockTitleCell'>") ;
            wb_table2_5_EV2( true) ;
         }
         else
         {
            wb_table2_5_EV2( false) ;
         }
         return  ;
      }

      protected void wb_table2_5_EV2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td data-align=\"right\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-right;text-align:-moz-right;text-align:-webkit-right")+"\" class='TableTitleCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblWorkwithlink_Internalname, "Voltar", lblWorkwithlink_Link, "", lblWorkwithlink_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockLink", 0, "", 1, 1, 0, "HLP_ViewLoteArquivoAnexo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"3\" >") ;
            if ( ! isFullAjaxMode( ) )
            {
               /* WebComponent */
               GxWebStd.gx_hidden_field( context, "W0016"+"", StringUtil.RTrim( WebComp_Tabbedview_Component));
               context.WriteHtmlText( "<div") ;
               GxWebStd.ClassAttribute( context, "gxwebcomponent");
               context.WriteHtmlText( " id=\""+"gxHTMLWrpW0016"+""+"\""+"") ;
               context.WriteHtmlText( ">") ;
               if ( StringUtil.Len( WebComp_Tabbedview_Component) != 0 )
               {
                  if ( StringUtil.StrCmp(StringUtil.Lower( OldTabbedview), StringUtil.Lower( WebComp_Tabbedview_Component)) != 0 )
                  {
                     context.httpAjaxContext.ajax_rspStartCmp("gxHTMLWrpW0016"+"");
                  }
                  WebComp_Tabbedview.componentdraw();
                  if ( StringUtil.StrCmp(StringUtil.Lower( OldTabbedview), StringUtil.Lower( WebComp_Tabbedview_Component)) != 0 )
                  {
                     context.httpAjaxContext.ajax_rspEndCmp();
                  }
               }
               context.WriteHtmlText( "</div>") ;
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_EV2e( true) ;
         }
         else
         {
            wb_table1_2_EV2e( false) ;
         }
      }

      protected void wb_table2_5_EV2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemergedviewtitle_Internalname, tblTablemergedviewtitle_Internalname, "", "TableMerged", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblViewtitle_Internalname, "Arquivos Anexos :: ", "", "", lblViewtitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_ViewLoteArquivoAnexo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtLoteArquivoAnexo_NomeArq_Internalname, StringUtil.RTrim( A839LoteArquivoAnexo_NomeArq), StringUtil.RTrim( context.localUtil.Format( A839LoteArquivoAnexo_NomeArq, "")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtLoteArquivoAnexo_NomeArq_Jsonclick, 0, "AttributeTitleWWP", "", "", "", 1, 0, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "NomeArq", "left", true, "HLP_ViewLoteArquivoAnexo.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_5_EV2e( true) ;
         }
         else
         {
            wb_table2_5_EV2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV9LoteArquivoAnexo_LoteCod = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9LoteArquivoAnexo_LoteCod", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9LoteArquivoAnexo_LoteCod), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vLOTEARQUIVOANEXO_LOTECOD", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV9LoteArquivoAnexo_LoteCod), "ZZZZZ9")));
         AV10LoteArquivoAnexo_Data = (DateTime)getParm(obj,1);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10LoteArquivoAnexo_Data", context.localUtil.TToC( AV10LoteArquivoAnexo_Data, 8, 5, 0, 3, "/", ":", " "));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vLOTEARQUIVOANEXO_DATA", GetSecureSignedToken( "", context.localUtil.Format( AV10LoteArquivoAnexo_Data, "99/99/99 99:99")));
         AV7TabCode = (String)getParm(obj,2);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7TabCode", AV7TabCode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vTABCODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV7TabCode, ""))));
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAEV2( ) ;
         WSEV2( ) ;
         WEEV2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?2249787");
         if ( ! ( WebComp_Tabbedview == null ) )
         {
            if ( StringUtil.Len( WebComp_Tabbedview_Component) != 0 )
            {
               WebComp_Tabbedview.componentthemes();
            }
         }
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20204282328013");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("viewlotearquivoanexo.js", "?20204282328013");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblViewtitle_Internalname = "VIEWTITLE";
         edtLoteArquivoAnexo_NomeArq_Internalname = "LOTEARQUIVOANEXO_NOMEARQ";
         tblTablemergedviewtitle_Internalname = "TABLEMERGEDVIEWTITLE";
         lblWorkwithlink_Internalname = "WORKWITHLINK";
         tblTablemain_Internalname = "TABLEMAIN";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtLoteArquivoAnexo_NomeArq_Jsonclick = "";
         lblWorkwithlink_Link = "";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "View Lote Arquivo Anexo";
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         wcpOAV10LoteArquivoAnexo_Data = (DateTime)(DateTime.MinValue);
         wcpOAV7TabCode = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         A839LoteArquivoAnexo_NomeArq = "";
         GXKey = "";
         forbiddenHiddens = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         OldTabbedview = "";
         WebComp_Tabbedview_Component = "";
         scmdbuf = "";
         H00EV2_A836LoteArquivoAnexo_Data = new DateTime[] {DateTime.MinValue} ;
         H00EV2_A841LoteArquivoAnexo_LoteCod = new int[1] ;
         H00EV2_A839LoteArquivoAnexo_NomeArq = new String[] {""} ;
         H00EV2_n839LoteArquivoAnexo_NomeArq = new bool[] {false} ;
         A836LoteArquivoAnexo_Data = (DateTime)(DateTime.MinValue);
         hsh = "";
         AV6WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         H00EV3_A836LoteArquivoAnexo_Data = new DateTime[] {DateTime.MinValue} ;
         H00EV3_A841LoteArquivoAnexo_LoteCod = new int[1] ;
         H00EV3_A839LoteArquivoAnexo_NomeArq = new String[] {""} ;
         H00EV3_n839LoteArquivoAnexo_NomeArq = new bool[] {false} ;
         AV11Tabs = new GxObjectCollection( context, "WWPTabOptions.TabOptionsItem", "GxEv3Up14_MeetrikaVs3", "wwpbaseobjects.SdtWWPTabOptions_TabOptionsItem", "GeneXus.Programs");
         AV12Tab = new wwpbaseobjects.SdtWWPTabOptions_TabOptionsItem(context);
         sStyleString = "";
         lblWorkwithlink_Jsonclick = "";
         lblViewtitle_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.viewlotearquivoanexo__default(),
            new Object[][] {
                new Object[] {
               H00EV2_A836LoteArquivoAnexo_Data, H00EV2_A841LoteArquivoAnexo_LoteCod, H00EV2_A839LoteArquivoAnexo_NomeArq, H00EV2_n839LoteArquivoAnexo_NomeArq
               }
               , new Object[] {
               H00EV3_A836LoteArquivoAnexo_Data, H00EV3_A841LoteArquivoAnexo_LoteCod, H00EV3_A839LoteArquivoAnexo_NomeArq, H00EV3_n839LoteArquivoAnexo_NomeArq
               }
            }
         );
         WebComp_Tabbedview = new GeneXus.Http.GXNullWebComponent();
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short wbEnd ;
      private short wbStart ;
      private short nCmpId ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short AV15GXLvl9 ;
      private short nGXWrapped ;
      private int AV9LoteArquivoAnexo_LoteCod ;
      private int wcpOAV9LoteArquivoAnexo_LoteCod ;
      private int A841LoteArquivoAnexo_LoteCod ;
      private int idxLst ;
      private String AV7TabCode ;
      private String wcpOAV7TabCode ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String A839LoteArquivoAnexo_NomeArq ;
      private String GXKey ;
      private String forbiddenHiddens ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String OldTabbedview ;
      private String WebComp_Tabbedview_Component ;
      private String scmdbuf ;
      private String edtLoteArquivoAnexo_NomeArq_Internalname ;
      private String hsh ;
      private String lblWorkwithlink_Link ;
      private String lblWorkwithlink_Internalname ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String lblWorkwithlink_Jsonclick ;
      private String tblTablemergedviewtitle_Internalname ;
      private String lblViewtitle_Internalname ;
      private String lblViewtitle_Jsonclick ;
      private String edtLoteArquivoAnexo_NomeArq_Jsonclick ;
      private DateTime AV10LoteArquivoAnexo_Data ;
      private DateTime wcpOAV10LoteArquivoAnexo_Data ;
      private DateTime A836LoteArquivoAnexo_Data ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool n839LoteArquivoAnexo_NomeArq ;
      private bool returnInSub ;
      private bool AV8Exists ;
      private GXWebComponent WebComp_Tabbedview ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private DateTime[] H00EV2_A836LoteArquivoAnexo_Data ;
      private int[] H00EV2_A841LoteArquivoAnexo_LoteCod ;
      private String[] H00EV2_A839LoteArquivoAnexo_NomeArq ;
      private bool[] H00EV2_n839LoteArquivoAnexo_NomeArq ;
      private DateTime[] H00EV3_A836LoteArquivoAnexo_Data ;
      private int[] H00EV3_A841LoteArquivoAnexo_LoteCod ;
      private String[] H00EV3_A839LoteArquivoAnexo_NomeArq ;
      private bool[] H00EV3_n839LoteArquivoAnexo_NomeArq ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      [ObjectCollection(ItemType=typeof( wwpbaseobjects.SdtWWPTabOptions_TabOptionsItem ))]
      private IGxCollection AV11Tabs ;
      private GXWebForm Form ;
      private wwpbaseobjects.SdtWWPContext AV6WWPContext ;
      private wwpbaseobjects.SdtWWPTabOptions_TabOptionsItem AV12Tab ;
   }

   public class viewlotearquivoanexo__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00EV2 ;
          prmH00EV2 = new Object[] {
          new Object[] {"@AV9LoteArquivoAnexo_LoteCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV10LoteArquivoAnexo_Data",SqlDbType.DateTime,8,5}
          } ;
          Object[] prmH00EV3 ;
          prmH00EV3 = new Object[] {
          new Object[] {"@AV9LoteArquivoAnexo_LoteCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV10LoteArquivoAnexo_Data",SqlDbType.DateTime,8,5}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00EV2", "SELECT [LoteArquivoAnexo_Data], [LoteArquivoAnexo_LoteCod], [LoteArquivoAnexo_NomeArq] FROM [LoteArquivoAnexo] WITH (NOLOCK) WHERE [LoteArquivoAnexo_LoteCod] = @AV9LoteArquivoAnexo_LoteCod and [LoteArquivoAnexo_Data] = @AV10LoteArquivoAnexo_Data ORDER BY [LoteArquivoAnexo_LoteCod], [LoteArquivoAnexo_Data] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00EV2,1,0,true,true )
             ,new CursorDef("H00EV3", "SELECT [LoteArquivoAnexo_Data], [LoteArquivoAnexo_LoteCod], [LoteArquivoAnexo_NomeArq] FROM [LoteArquivoAnexo] WITH (NOLOCK) WHERE ([LoteArquivoAnexo_LoteCod] = @AV9LoteArquivoAnexo_LoteCod and [LoteArquivoAnexo_Data] = @AV10LoteArquivoAnexo_Data) AND ([LoteArquivoAnexo_LoteCod] = @AV9LoteArquivoAnexo_LoteCod and [LoteArquivoAnexo_Data] = @AV10LoteArquivoAnexo_Data) ORDER BY [LoteArquivoAnexo_LoteCod], [LoteArquivoAnexo_Data] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00EV3,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((DateTime[]) buf[0])[0] = rslt.getGXDateTime(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 50) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                return;
             case 1 :
                ((DateTime[]) buf[0])[0] = rslt.getGXDateTime(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 50) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameterDatetime(2, (DateTime)parms[1]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameterDatetime(2, (DateTime)parms[1]);
                return;
       }
    }

 }

}
