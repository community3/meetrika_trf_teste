/*
               File: GetWWTipodeContaFilterData
        Description: Get WWTipode Conta Filter Data
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 22:54:12.19
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class getwwtipodecontafilterdata : GXProcedure
   {
      public getwwtipodecontafilterdata( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public getwwtipodecontafilterdata( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_DDOName ,
                           String aP1_SearchTxt ,
                           String aP2_SearchTxtTo ,
                           out String aP3_OptionsJson ,
                           out String aP4_OptionsDescJson ,
                           out String aP5_OptionIndexesJson )
      {
         this.AV20DDOName = aP0_DDOName;
         this.AV18SearchTxt = aP1_SearchTxt;
         this.AV19SearchTxtTo = aP2_SearchTxtTo;
         this.AV24OptionsJson = "" ;
         this.AV27OptionsDescJson = "" ;
         this.AV29OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV24OptionsJson;
         aP4_OptionsDescJson=this.AV27OptionsDescJson;
         aP5_OptionIndexesJson=this.AV29OptionIndexesJson;
      }

      public String executeUdp( String aP0_DDOName ,
                                String aP1_SearchTxt ,
                                String aP2_SearchTxtTo ,
                                out String aP3_OptionsJson ,
                                out String aP4_OptionsDescJson )
      {
         this.AV20DDOName = aP0_DDOName;
         this.AV18SearchTxt = aP1_SearchTxt;
         this.AV19SearchTxtTo = aP2_SearchTxtTo;
         this.AV24OptionsJson = "" ;
         this.AV27OptionsDescJson = "" ;
         this.AV29OptionIndexesJson = "" ;
         initialize();
         executePrivate();
         aP3_OptionsJson=this.AV24OptionsJson;
         aP4_OptionsDescJson=this.AV27OptionsDescJson;
         aP5_OptionIndexesJson=this.AV29OptionIndexesJson;
         return AV29OptionIndexesJson ;
      }

      public void executeSubmit( String aP0_DDOName ,
                                 String aP1_SearchTxt ,
                                 String aP2_SearchTxtTo ,
                                 out String aP3_OptionsJson ,
                                 out String aP4_OptionsDescJson ,
                                 out String aP5_OptionIndexesJson )
      {
         getwwtipodecontafilterdata objgetwwtipodecontafilterdata;
         objgetwwtipodecontafilterdata = new getwwtipodecontafilterdata();
         objgetwwtipodecontafilterdata.AV20DDOName = aP0_DDOName;
         objgetwwtipodecontafilterdata.AV18SearchTxt = aP1_SearchTxt;
         objgetwwtipodecontafilterdata.AV19SearchTxtTo = aP2_SearchTxtTo;
         objgetwwtipodecontafilterdata.AV24OptionsJson = "" ;
         objgetwwtipodecontafilterdata.AV27OptionsDescJson = "" ;
         objgetwwtipodecontafilterdata.AV29OptionIndexesJson = "" ;
         objgetwwtipodecontafilterdata.context.SetSubmitInitialConfig(context);
         objgetwwtipodecontafilterdata.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgetwwtipodecontafilterdata);
         aP3_OptionsJson=this.AV24OptionsJson;
         aP4_OptionsDescJson=this.AV27OptionsDescJson;
         aP5_OptionIndexesJson=this.AV29OptionIndexesJson;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((getwwtipodecontafilterdata)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Desenvolvimento 3.0", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV23Options = (IGxCollection)(new GxSimpleCollection());
         AV26OptionsDesc = (IGxCollection)(new GxSimpleCollection());
         AV28OptionIndexes = (IGxCollection)(new GxSimpleCollection());
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV9WWPContext) ;
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         if ( StringUtil.StrCmp(StringUtil.Upper( AV20DDOName), "DDO_TIPODECONTA_CODIGO") == 0 )
         {
            /* Execute user subroutine: 'LOADTIPODECONTA_CODIGOOPTIONS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV20DDOName), "DDO_TIPODECONTA_NOME") == 0 )
         {
            /* Execute user subroutine: 'LOADTIPODECONTA_NOMEOPTIONS' */
            S131 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(StringUtil.Upper( AV20DDOName), "DDO_TIPODECONTA_CONTAPAICOD") == 0 )
         {
            /* Execute user subroutine: 'LOADTIPODECONTA_CONTAPAICODOPTIONS' */
            S141 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         AV24OptionsJson = AV23Options.ToJSonString(false);
         AV27OptionsDescJson = AV26OptionsDesc.ToJSonString(false);
         AV29OptionIndexesJson = AV28OptionIndexes.ToJSonString(false);
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV31Session.Get("WWTipodeContaGridState"), "") == 0 )
         {
            AV33GridState.FromXml(new wwpbaseobjects.loadgridstate(context).executeUdp(  "WWTipodeContaGridState"), "");
         }
         else
         {
            AV33GridState.FromXml(AV31Session.Get("WWTipodeContaGridState"), "");
         }
         AV52GXV1 = 1;
         while ( AV52GXV1 <= AV33GridState.gxTpr_Filtervalues.Count )
         {
            AV34GridStateFilterValue = ((wwpbaseobjects.SdtWWPGridState_FilterValue)AV33GridState.gxTpr_Filtervalues.Item(AV52GXV1));
            if ( StringUtil.StrCmp(AV34GridStateFilterValue.gxTpr_Name, "TFTIPODECONTA_CODIGO") == 0 )
            {
               AV10TFTipodeConta_Codigo = AV34GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV34GridStateFilterValue.gxTpr_Name, "TFTIPODECONTA_CODIGO_SEL") == 0 )
            {
               AV11TFTipodeConta_Codigo_Sel = AV34GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV34GridStateFilterValue.gxTpr_Name, "TFTIPODECONTA_NOME") == 0 )
            {
               AV12TFTipodeConta_Nome = AV34GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV34GridStateFilterValue.gxTpr_Name, "TFTIPODECONTA_NOME_SEL") == 0 )
            {
               AV13TFTipodeConta_Nome_Sel = AV34GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV34GridStateFilterValue.gxTpr_Name, "TFTIPODECONTA_TIPO_SEL") == 0 )
            {
               AV14TFTipodeConta_Tipo_SelsJson = AV34GridStateFilterValue.gxTpr_Value;
               AV15TFTipodeConta_Tipo_Sels.FromJSonString(AV14TFTipodeConta_Tipo_SelsJson);
            }
            else if ( StringUtil.StrCmp(AV34GridStateFilterValue.gxTpr_Name, "TFTIPODECONTA_CONTAPAICOD") == 0 )
            {
               AV16TFTipoDeConta_ContaPaiCod = AV34GridStateFilterValue.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV34GridStateFilterValue.gxTpr_Name, "TFTIPODECONTA_CONTAPAICOD_SEL") == 0 )
            {
               AV17TFTipoDeConta_ContaPaiCod_Sel = AV34GridStateFilterValue.gxTpr_Value;
            }
            AV52GXV1 = (int)(AV52GXV1+1);
         }
         if ( AV33GridState.gxTpr_Dynamicfilters.Count >= 1 )
         {
            AV35GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV33GridState.gxTpr_Dynamicfilters.Item(1));
            AV36DynamicFiltersSelector1 = AV35GridStateDynamicFilter.gxTpr_Selected;
            if ( StringUtil.StrCmp(AV36DynamicFiltersSelector1, "TIPODECONTA_CODIGO") == 0 )
            {
               AV37TipodeConta_Codigo1 = AV35GridStateDynamicFilter.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV36DynamicFiltersSelector1, "TIPODECONTA_NOME") == 0 )
            {
               AV38TipodeConta_Nome1 = AV35GridStateDynamicFilter.gxTpr_Value;
            }
            else if ( StringUtil.StrCmp(AV36DynamicFiltersSelector1, "TIPODECONTA_CONTAPAICOD") == 0 )
            {
               AV39TipoDeConta_ContaPaiCod1 = AV35GridStateDynamicFilter.gxTpr_Value;
            }
            if ( AV33GridState.gxTpr_Dynamicfilters.Count >= 2 )
            {
               AV40DynamicFiltersEnabled2 = true;
               AV35GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV33GridState.gxTpr_Dynamicfilters.Item(2));
               AV41DynamicFiltersSelector2 = AV35GridStateDynamicFilter.gxTpr_Selected;
               if ( StringUtil.StrCmp(AV41DynamicFiltersSelector2, "TIPODECONTA_CODIGO") == 0 )
               {
                  AV42TipodeConta_Codigo2 = AV35GridStateDynamicFilter.gxTpr_Value;
               }
               else if ( StringUtil.StrCmp(AV41DynamicFiltersSelector2, "TIPODECONTA_NOME") == 0 )
               {
                  AV43TipodeConta_Nome2 = AV35GridStateDynamicFilter.gxTpr_Value;
               }
               else if ( StringUtil.StrCmp(AV41DynamicFiltersSelector2, "TIPODECONTA_CONTAPAICOD") == 0 )
               {
                  AV44TipoDeConta_ContaPaiCod2 = AV35GridStateDynamicFilter.gxTpr_Value;
               }
               if ( AV33GridState.gxTpr_Dynamicfilters.Count >= 3 )
               {
                  AV45DynamicFiltersEnabled3 = true;
                  AV35GridStateDynamicFilter = ((wwpbaseobjects.SdtWWPGridState_DynamicFilter)AV33GridState.gxTpr_Dynamicfilters.Item(3));
                  AV46DynamicFiltersSelector3 = AV35GridStateDynamicFilter.gxTpr_Selected;
                  if ( StringUtil.StrCmp(AV46DynamicFiltersSelector3, "TIPODECONTA_CODIGO") == 0 )
                  {
                     AV47TipodeConta_Codigo3 = AV35GridStateDynamicFilter.gxTpr_Value;
                  }
                  else if ( StringUtil.StrCmp(AV46DynamicFiltersSelector3, "TIPODECONTA_NOME") == 0 )
                  {
                     AV48TipodeConta_Nome3 = AV35GridStateDynamicFilter.gxTpr_Value;
                  }
                  else if ( StringUtil.StrCmp(AV46DynamicFiltersSelector3, "TIPODECONTA_CONTAPAICOD") == 0 )
                  {
                     AV49TipoDeConta_ContaPaiCod3 = AV35GridStateDynamicFilter.gxTpr_Value;
                  }
               }
            }
         }
      }

      protected void S121( )
      {
         /* 'LOADTIPODECONTA_CODIGOOPTIONS' Routine */
         AV10TFTipodeConta_Codigo = AV18SearchTxt;
         AV11TFTipodeConta_Codigo_Sel = "";
         AV54WWTipodeContaDS_1_Dynamicfiltersselector1 = AV36DynamicFiltersSelector1;
         AV55WWTipodeContaDS_2_Tipodeconta_codigo1 = AV37TipodeConta_Codigo1;
         AV56WWTipodeContaDS_3_Tipodeconta_nome1 = AV38TipodeConta_Nome1;
         AV57WWTipodeContaDS_4_Tipodeconta_contapaicod1 = AV39TipoDeConta_ContaPaiCod1;
         AV58WWTipodeContaDS_5_Dynamicfiltersenabled2 = AV40DynamicFiltersEnabled2;
         AV59WWTipodeContaDS_6_Dynamicfiltersselector2 = AV41DynamicFiltersSelector2;
         AV60WWTipodeContaDS_7_Tipodeconta_codigo2 = AV42TipodeConta_Codigo2;
         AV61WWTipodeContaDS_8_Tipodeconta_nome2 = AV43TipodeConta_Nome2;
         AV62WWTipodeContaDS_9_Tipodeconta_contapaicod2 = AV44TipoDeConta_ContaPaiCod2;
         AV63WWTipodeContaDS_10_Dynamicfiltersenabled3 = AV45DynamicFiltersEnabled3;
         AV64WWTipodeContaDS_11_Dynamicfiltersselector3 = AV46DynamicFiltersSelector3;
         AV65WWTipodeContaDS_12_Tipodeconta_codigo3 = AV47TipodeConta_Codigo3;
         AV66WWTipodeContaDS_13_Tipodeconta_nome3 = AV48TipodeConta_Nome3;
         AV67WWTipodeContaDS_14_Tipodeconta_contapaicod3 = AV49TipoDeConta_ContaPaiCod3;
         AV68WWTipodeContaDS_15_Tftipodeconta_codigo = AV10TFTipodeConta_Codigo;
         AV69WWTipodeContaDS_16_Tftipodeconta_codigo_sel = AV11TFTipodeConta_Codigo_Sel;
         AV70WWTipodeContaDS_17_Tftipodeconta_nome = AV12TFTipodeConta_Nome;
         AV71WWTipodeContaDS_18_Tftipodeconta_nome_sel = AV13TFTipodeConta_Nome_Sel;
         AV72WWTipodeContaDS_19_Tftipodeconta_tipo_sels = AV15TFTipodeConta_Tipo_Sels;
         AV73WWTipodeContaDS_20_Tftipodeconta_contapaicod = AV16TFTipoDeConta_ContaPaiCod;
         AV74WWTipodeContaDS_21_Tftipodeconta_contapaicod_sel = AV17TFTipoDeConta_ContaPaiCod_Sel;
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              A873TipodeConta_Tipo ,
                                              AV72WWTipodeContaDS_19_Tftipodeconta_tipo_sels ,
                                              AV54WWTipodeContaDS_1_Dynamicfiltersselector1 ,
                                              AV55WWTipodeContaDS_2_Tipodeconta_codigo1 ,
                                              AV56WWTipodeContaDS_3_Tipodeconta_nome1 ,
                                              AV57WWTipodeContaDS_4_Tipodeconta_contapaicod1 ,
                                              AV58WWTipodeContaDS_5_Dynamicfiltersenabled2 ,
                                              AV59WWTipodeContaDS_6_Dynamicfiltersselector2 ,
                                              AV60WWTipodeContaDS_7_Tipodeconta_codigo2 ,
                                              AV61WWTipodeContaDS_8_Tipodeconta_nome2 ,
                                              AV62WWTipodeContaDS_9_Tipodeconta_contapaicod2 ,
                                              AV63WWTipodeContaDS_10_Dynamicfiltersenabled3 ,
                                              AV64WWTipodeContaDS_11_Dynamicfiltersselector3 ,
                                              AV65WWTipodeContaDS_12_Tipodeconta_codigo3 ,
                                              AV66WWTipodeContaDS_13_Tipodeconta_nome3 ,
                                              AV67WWTipodeContaDS_14_Tipodeconta_contapaicod3 ,
                                              AV69WWTipodeContaDS_16_Tftipodeconta_codigo_sel ,
                                              AV68WWTipodeContaDS_15_Tftipodeconta_codigo ,
                                              AV71WWTipodeContaDS_18_Tftipodeconta_nome_sel ,
                                              AV70WWTipodeContaDS_17_Tftipodeconta_nome ,
                                              AV72WWTipodeContaDS_19_Tftipodeconta_tipo_sels.Count ,
                                              AV74WWTipodeContaDS_21_Tftipodeconta_contapaicod_sel ,
                                              AV73WWTipodeContaDS_20_Tftipodeconta_contapaicod ,
                                              A870TipodeConta_Codigo ,
                                              A871TipodeConta_Nome ,
                                              A885TipoDeConta_ContaPaiCod },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN
                                              }
         });
         lV55WWTipodeContaDS_2_Tipodeconta_codigo1 = StringUtil.PadR( StringUtil.RTrim( AV55WWTipodeContaDS_2_Tipodeconta_codigo1), 20, "%");
         lV56WWTipodeContaDS_3_Tipodeconta_nome1 = StringUtil.PadR( StringUtil.RTrim( AV56WWTipodeContaDS_3_Tipodeconta_nome1), 50, "%");
         lV57WWTipodeContaDS_4_Tipodeconta_contapaicod1 = StringUtil.PadR( StringUtil.RTrim( AV57WWTipodeContaDS_4_Tipodeconta_contapaicod1), 20, "%");
         lV60WWTipodeContaDS_7_Tipodeconta_codigo2 = StringUtil.PadR( StringUtil.RTrim( AV60WWTipodeContaDS_7_Tipodeconta_codigo2), 20, "%");
         lV61WWTipodeContaDS_8_Tipodeconta_nome2 = StringUtil.PadR( StringUtil.RTrim( AV61WWTipodeContaDS_8_Tipodeconta_nome2), 50, "%");
         lV62WWTipodeContaDS_9_Tipodeconta_contapaicod2 = StringUtil.PadR( StringUtil.RTrim( AV62WWTipodeContaDS_9_Tipodeconta_contapaicod2), 20, "%");
         lV65WWTipodeContaDS_12_Tipodeconta_codigo3 = StringUtil.PadR( StringUtil.RTrim( AV65WWTipodeContaDS_12_Tipodeconta_codigo3), 20, "%");
         lV66WWTipodeContaDS_13_Tipodeconta_nome3 = StringUtil.PadR( StringUtil.RTrim( AV66WWTipodeContaDS_13_Tipodeconta_nome3), 50, "%");
         lV67WWTipodeContaDS_14_Tipodeconta_contapaicod3 = StringUtil.PadR( StringUtil.RTrim( AV67WWTipodeContaDS_14_Tipodeconta_contapaicod3), 20, "%");
         lV68WWTipodeContaDS_15_Tftipodeconta_codigo = StringUtil.PadR( StringUtil.RTrim( AV68WWTipodeContaDS_15_Tftipodeconta_codigo), 20, "%");
         lV70WWTipodeContaDS_17_Tftipodeconta_nome = StringUtil.PadR( StringUtil.RTrim( AV70WWTipodeContaDS_17_Tftipodeconta_nome), 50, "%");
         lV73WWTipodeContaDS_20_Tftipodeconta_contapaicod = StringUtil.PadR( StringUtil.RTrim( AV73WWTipodeContaDS_20_Tftipodeconta_contapaicod), 20, "%");
         /* Using cursor P00OZ2 */
         pr_default.execute(0, new Object[] {lV55WWTipodeContaDS_2_Tipodeconta_codigo1, lV56WWTipodeContaDS_3_Tipodeconta_nome1, lV57WWTipodeContaDS_4_Tipodeconta_contapaicod1, lV60WWTipodeContaDS_7_Tipodeconta_codigo2, lV61WWTipodeContaDS_8_Tipodeconta_nome2, lV62WWTipodeContaDS_9_Tipodeconta_contapaicod2, lV65WWTipodeContaDS_12_Tipodeconta_codigo3, lV66WWTipodeContaDS_13_Tipodeconta_nome3, lV67WWTipodeContaDS_14_Tipodeconta_contapaicod3, lV68WWTipodeContaDS_15_Tftipodeconta_codigo, AV69WWTipodeContaDS_16_Tftipodeconta_codigo_sel, lV70WWTipodeContaDS_17_Tftipodeconta_nome, AV71WWTipodeContaDS_18_Tftipodeconta_nome_sel, lV73WWTipodeContaDS_20_Tftipodeconta_contapaicod, AV74WWTipodeContaDS_21_Tftipodeconta_contapaicod_sel});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A873TipodeConta_Tipo = P00OZ2_A873TipodeConta_Tipo[0];
            A885TipoDeConta_ContaPaiCod = P00OZ2_A885TipoDeConta_ContaPaiCod[0];
            n885TipoDeConta_ContaPaiCod = P00OZ2_n885TipoDeConta_ContaPaiCod[0];
            A871TipodeConta_Nome = P00OZ2_A871TipodeConta_Nome[0];
            A870TipodeConta_Codigo = P00OZ2_A870TipodeConta_Codigo[0];
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A870TipodeConta_Codigo)) )
            {
               AV22Option = A870TipodeConta_Codigo;
               AV23Options.Add(AV22Option, 0);
            }
            if ( AV23Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            pr_default.readNext(0);
         }
         pr_default.close(0);
      }

      protected void S131( )
      {
         /* 'LOADTIPODECONTA_NOMEOPTIONS' Routine */
         AV12TFTipodeConta_Nome = AV18SearchTxt;
         AV13TFTipodeConta_Nome_Sel = "";
         AV54WWTipodeContaDS_1_Dynamicfiltersselector1 = AV36DynamicFiltersSelector1;
         AV55WWTipodeContaDS_2_Tipodeconta_codigo1 = AV37TipodeConta_Codigo1;
         AV56WWTipodeContaDS_3_Tipodeconta_nome1 = AV38TipodeConta_Nome1;
         AV57WWTipodeContaDS_4_Tipodeconta_contapaicod1 = AV39TipoDeConta_ContaPaiCod1;
         AV58WWTipodeContaDS_5_Dynamicfiltersenabled2 = AV40DynamicFiltersEnabled2;
         AV59WWTipodeContaDS_6_Dynamicfiltersselector2 = AV41DynamicFiltersSelector2;
         AV60WWTipodeContaDS_7_Tipodeconta_codigo2 = AV42TipodeConta_Codigo2;
         AV61WWTipodeContaDS_8_Tipodeconta_nome2 = AV43TipodeConta_Nome2;
         AV62WWTipodeContaDS_9_Tipodeconta_contapaicod2 = AV44TipoDeConta_ContaPaiCod2;
         AV63WWTipodeContaDS_10_Dynamicfiltersenabled3 = AV45DynamicFiltersEnabled3;
         AV64WWTipodeContaDS_11_Dynamicfiltersselector3 = AV46DynamicFiltersSelector3;
         AV65WWTipodeContaDS_12_Tipodeconta_codigo3 = AV47TipodeConta_Codigo3;
         AV66WWTipodeContaDS_13_Tipodeconta_nome3 = AV48TipodeConta_Nome3;
         AV67WWTipodeContaDS_14_Tipodeconta_contapaicod3 = AV49TipoDeConta_ContaPaiCod3;
         AV68WWTipodeContaDS_15_Tftipodeconta_codigo = AV10TFTipodeConta_Codigo;
         AV69WWTipodeContaDS_16_Tftipodeconta_codigo_sel = AV11TFTipodeConta_Codigo_Sel;
         AV70WWTipodeContaDS_17_Tftipodeconta_nome = AV12TFTipodeConta_Nome;
         AV71WWTipodeContaDS_18_Tftipodeconta_nome_sel = AV13TFTipodeConta_Nome_Sel;
         AV72WWTipodeContaDS_19_Tftipodeconta_tipo_sels = AV15TFTipodeConta_Tipo_Sels;
         AV73WWTipodeContaDS_20_Tftipodeconta_contapaicod = AV16TFTipoDeConta_ContaPaiCod;
         AV74WWTipodeContaDS_21_Tftipodeconta_contapaicod_sel = AV17TFTipoDeConta_ContaPaiCod_Sel;
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              A873TipodeConta_Tipo ,
                                              AV72WWTipodeContaDS_19_Tftipodeconta_tipo_sels ,
                                              AV54WWTipodeContaDS_1_Dynamicfiltersselector1 ,
                                              AV55WWTipodeContaDS_2_Tipodeconta_codigo1 ,
                                              AV56WWTipodeContaDS_3_Tipodeconta_nome1 ,
                                              AV57WWTipodeContaDS_4_Tipodeconta_contapaicod1 ,
                                              AV58WWTipodeContaDS_5_Dynamicfiltersenabled2 ,
                                              AV59WWTipodeContaDS_6_Dynamicfiltersselector2 ,
                                              AV60WWTipodeContaDS_7_Tipodeconta_codigo2 ,
                                              AV61WWTipodeContaDS_8_Tipodeconta_nome2 ,
                                              AV62WWTipodeContaDS_9_Tipodeconta_contapaicod2 ,
                                              AV63WWTipodeContaDS_10_Dynamicfiltersenabled3 ,
                                              AV64WWTipodeContaDS_11_Dynamicfiltersselector3 ,
                                              AV65WWTipodeContaDS_12_Tipodeconta_codigo3 ,
                                              AV66WWTipodeContaDS_13_Tipodeconta_nome3 ,
                                              AV67WWTipodeContaDS_14_Tipodeconta_contapaicod3 ,
                                              AV69WWTipodeContaDS_16_Tftipodeconta_codigo_sel ,
                                              AV68WWTipodeContaDS_15_Tftipodeconta_codigo ,
                                              AV71WWTipodeContaDS_18_Tftipodeconta_nome_sel ,
                                              AV70WWTipodeContaDS_17_Tftipodeconta_nome ,
                                              AV72WWTipodeContaDS_19_Tftipodeconta_tipo_sels.Count ,
                                              AV74WWTipodeContaDS_21_Tftipodeconta_contapaicod_sel ,
                                              AV73WWTipodeContaDS_20_Tftipodeconta_contapaicod ,
                                              A870TipodeConta_Codigo ,
                                              A871TipodeConta_Nome ,
                                              A885TipoDeConta_ContaPaiCod },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN
                                              }
         });
         lV55WWTipodeContaDS_2_Tipodeconta_codigo1 = StringUtil.PadR( StringUtil.RTrim( AV55WWTipodeContaDS_2_Tipodeconta_codigo1), 20, "%");
         lV56WWTipodeContaDS_3_Tipodeconta_nome1 = StringUtil.PadR( StringUtil.RTrim( AV56WWTipodeContaDS_3_Tipodeconta_nome1), 50, "%");
         lV57WWTipodeContaDS_4_Tipodeconta_contapaicod1 = StringUtil.PadR( StringUtil.RTrim( AV57WWTipodeContaDS_4_Tipodeconta_contapaicod1), 20, "%");
         lV60WWTipodeContaDS_7_Tipodeconta_codigo2 = StringUtil.PadR( StringUtil.RTrim( AV60WWTipodeContaDS_7_Tipodeconta_codigo2), 20, "%");
         lV61WWTipodeContaDS_8_Tipodeconta_nome2 = StringUtil.PadR( StringUtil.RTrim( AV61WWTipodeContaDS_8_Tipodeconta_nome2), 50, "%");
         lV62WWTipodeContaDS_9_Tipodeconta_contapaicod2 = StringUtil.PadR( StringUtil.RTrim( AV62WWTipodeContaDS_9_Tipodeconta_contapaicod2), 20, "%");
         lV65WWTipodeContaDS_12_Tipodeconta_codigo3 = StringUtil.PadR( StringUtil.RTrim( AV65WWTipodeContaDS_12_Tipodeconta_codigo3), 20, "%");
         lV66WWTipodeContaDS_13_Tipodeconta_nome3 = StringUtil.PadR( StringUtil.RTrim( AV66WWTipodeContaDS_13_Tipodeconta_nome3), 50, "%");
         lV67WWTipodeContaDS_14_Tipodeconta_contapaicod3 = StringUtil.PadR( StringUtil.RTrim( AV67WWTipodeContaDS_14_Tipodeconta_contapaicod3), 20, "%");
         lV68WWTipodeContaDS_15_Tftipodeconta_codigo = StringUtil.PadR( StringUtil.RTrim( AV68WWTipodeContaDS_15_Tftipodeconta_codigo), 20, "%");
         lV70WWTipodeContaDS_17_Tftipodeconta_nome = StringUtil.PadR( StringUtil.RTrim( AV70WWTipodeContaDS_17_Tftipodeconta_nome), 50, "%");
         lV73WWTipodeContaDS_20_Tftipodeconta_contapaicod = StringUtil.PadR( StringUtil.RTrim( AV73WWTipodeContaDS_20_Tftipodeconta_contapaicod), 20, "%");
         /* Using cursor P00OZ3 */
         pr_default.execute(1, new Object[] {lV55WWTipodeContaDS_2_Tipodeconta_codigo1, lV56WWTipodeContaDS_3_Tipodeconta_nome1, lV57WWTipodeContaDS_4_Tipodeconta_contapaicod1, lV60WWTipodeContaDS_7_Tipodeconta_codigo2, lV61WWTipodeContaDS_8_Tipodeconta_nome2, lV62WWTipodeContaDS_9_Tipodeconta_contapaicod2, lV65WWTipodeContaDS_12_Tipodeconta_codigo3, lV66WWTipodeContaDS_13_Tipodeconta_nome3, lV67WWTipodeContaDS_14_Tipodeconta_contapaicod3, lV68WWTipodeContaDS_15_Tftipodeconta_codigo, AV69WWTipodeContaDS_16_Tftipodeconta_codigo_sel, lV70WWTipodeContaDS_17_Tftipodeconta_nome, AV71WWTipodeContaDS_18_Tftipodeconta_nome_sel, lV73WWTipodeContaDS_20_Tftipodeconta_contapaicod, AV74WWTipodeContaDS_21_Tftipodeconta_contapaicod_sel});
         while ( (pr_default.getStatus(1) != 101) )
         {
            BRKOZ3 = false;
            A871TipodeConta_Nome = P00OZ3_A871TipodeConta_Nome[0];
            A873TipodeConta_Tipo = P00OZ3_A873TipodeConta_Tipo[0];
            A885TipoDeConta_ContaPaiCod = P00OZ3_A885TipoDeConta_ContaPaiCod[0];
            n885TipoDeConta_ContaPaiCod = P00OZ3_n885TipoDeConta_ContaPaiCod[0];
            A870TipodeConta_Codigo = P00OZ3_A870TipodeConta_Codigo[0];
            AV30count = 0;
            while ( (pr_default.getStatus(1) != 101) && ( StringUtil.StrCmp(P00OZ3_A871TipodeConta_Nome[0], A871TipodeConta_Nome) == 0 ) )
            {
               BRKOZ3 = false;
               A870TipodeConta_Codigo = P00OZ3_A870TipodeConta_Codigo[0];
               AV30count = (long)(AV30count+1);
               BRKOZ3 = true;
               pr_default.readNext(1);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A871TipodeConta_Nome)) )
            {
               AV22Option = A871TipodeConta_Nome;
               AV23Options.Add(AV22Option, 0);
               AV28OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV30count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV23Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKOZ3 )
            {
               BRKOZ3 = true;
               pr_default.readNext(1);
            }
         }
         pr_default.close(1);
      }

      protected void S141( )
      {
         /* 'LOADTIPODECONTA_CONTAPAICODOPTIONS' Routine */
         AV16TFTipoDeConta_ContaPaiCod = AV18SearchTxt;
         AV17TFTipoDeConta_ContaPaiCod_Sel = "";
         AV54WWTipodeContaDS_1_Dynamicfiltersselector1 = AV36DynamicFiltersSelector1;
         AV55WWTipodeContaDS_2_Tipodeconta_codigo1 = AV37TipodeConta_Codigo1;
         AV56WWTipodeContaDS_3_Tipodeconta_nome1 = AV38TipodeConta_Nome1;
         AV57WWTipodeContaDS_4_Tipodeconta_contapaicod1 = AV39TipoDeConta_ContaPaiCod1;
         AV58WWTipodeContaDS_5_Dynamicfiltersenabled2 = AV40DynamicFiltersEnabled2;
         AV59WWTipodeContaDS_6_Dynamicfiltersselector2 = AV41DynamicFiltersSelector2;
         AV60WWTipodeContaDS_7_Tipodeconta_codigo2 = AV42TipodeConta_Codigo2;
         AV61WWTipodeContaDS_8_Tipodeconta_nome2 = AV43TipodeConta_Nome2;
         AV62WWTipodeContaDS_9_Tipodeconta_contapaicod2 = AV44TipoDeConta_ContaPaiCod2;
         AV63WWTipodeContaDS_10_Dynamicfiltersenabled3 = AV45DynamicFiltersEnabled3;
         AV64WWTipodeContaDS_11_Dynamicfiltersselector3 = AV46DynamicFiltersSelector3;
         AV65WWTipodeContaDS_12_Tipodeconta_codigo3 = AV47TipodeConta_Codigo3;
         AV66WWTipodeContaDS_13_Tipodeconta_nome3 = AV48TipodeConta_Nome3;
         AV67WWTipodeContaDS_14_Tipodeconta_contapaicod3 = AV49TipoDeConta_ContaPaiCod3;
         AV68WWTipodeContaDS_15_Tftipodeconta_codigo = AV10TFTipodeConta_Codigo;
         AV69WWTipodeContaDS_16_Tftipodeconta_codigo_sel = AV11TFTipodeConta_Codigo_Sel;
         AV70WWTipodeContaDS_17_Tftipodeconta_nome = AV12TFTipodeConta_Nome;
         AV71WWTipodeContaDS_18_Tftipodeconta_nome_sel = AV13TFTipodeConta_Nome_Sel;
         AV72WWTipodeContaDS_19_Tftipodeconta_tipo_sels = AV15TFTipodeConta_Tipo_Sels;
         AV73WWTipodeContaDS_20_Tftipodeconta_contapaicod = AV16TFTipoDeConta_ContaPaiCod;
         AV74WWTipodeContaDS_21_Tftipodeconta_contapaicod_sel = AV17TFTipoDeConta_ContaPaiCod_Sel;
         pr_default.dynParam(2, new Object[]{ new Object[]{
                                              A873TipodeConta_Tipo ,
                                              AV72WWTipodeContaDS_19_Tftipodeconta_tipo_sels ,
                                              AV54WWTipodeContaDS_1_Dynamicfiltersselector1 ,
                                              AV55WWTipodeContaDS_2_Tipodeconta_codigo1 ,
                                              AV56WWTipodeContaDS_3_Tipodeconta_nome1 ,
                                              AV57WWTipodeContaDS_4_Tipodeconta_contapaicod1 ,
                                              AV58WWTipodeContaDS_5_Dynamicfiltersenabled2 ,
                                              AV59WWTipodeContaDS_6_Dynamicfiltersselector2 ,
                                              AV60WWTipodeContaDS_7_Tipodeconta_codigo2 ,
                                              AV61WWTipodeContaDS_8_Tipodeconta_nome2 ,
                                              AV62WWTipodeContaDS_9_Tipodeconta_contapaicod2 ,
                                              AV63WWTipodeContaDS_10_Dynamicfiltersenabled3 ,
                                              AV64WWTipodeContaDS_11_Dynamicfiltersselector3 ,
                                              AV65WWTipodeContaDS_12_Tipodeconta_codigo3 ,
                                              AV66WWTipodeContaDS_13_Tipodeconta_nome3 ,
                                              AV67WWTipodeContaDS_14_Tipodeconta_contapaicod3 ,
                                              AV69WWTipodeContaDS_16_Tftipodeconta_codigo_sel ,
                                              AV68WWTipodeContaDS_15_Tftipodeconta_codigo ,
                                              AV71WWTipodeContaDS_18_Tftipodeconta_nome_sel ,
                                              AV70WWTipodeContaDS_17_Tftipodeconta_nome ,
                                              AV72WWTipodeContaDS_19_Tftipodeconta_tipo_sels.Count ,
                                              AV74WWTipodeContaDS_21_Tftipodeconta_contapaicod_sel ,
                                              AV73WWTipodeContaDS_20_Tftipodeconta_contapaicod ,
                                              A870TipodeConta_Codigo ,
                                              A871TipodeConta_Nome ,
                                              A885TipoDeConta_ContaPaiCod },
                                              new int[] {
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING,
                                              TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.INT,
                                              TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.BOOLEAN
                                              }
         });
         lV55WWTipodeContaDS_2_Tipodeconta_codigo1 = StringUtil.PadR( StringUtil.RTrim( AV55WWTipodeContaDS_2_Tipodeconta_codigo1), 20, "%");
         lV56WWTipodeContaDS_3_Tipodeconta_nome1 = StringUtil.PadR( StringUtil.RTrim( AV56WWTipodeContaDS_3_Tipodeconta_nome1), 50, "%");
         lV57WWTipodeContaDS_4_Tipodeconta_contapaicod1 = StringUtil.PadR( StringUtil.RTrim( AV57WWTipodeContaDS_4_Tipodeconta_contapaicod1), 20, "%");
         lV60WWTipodeContaDS_7_Tipodeconta_codigo2 = StringUtil.PadR( StringUtil.RTrim( AV60WWTipodeContaDS_7_Tipodeconta_codigo2), 20, "%");
         lV61WWTipodeContaDS_8_Tipodeconta_nome2 = StringUtil.PadR( StringUtil.RTrim( AV61WWTipodeContaDS_8_Tipodeconta_nome2), 50, "%");
         lV62WWTipodeContaDS_9_Tipodeconta_contapaicod2 = StringUtil.PadR( StringUtil.RTrim( AV62WWTipodeContaDS_9_Tipodeconta_contapaicod2), 20, "%");
         lV65WWTipodeContaDS_12_Tipodeconta_codigo3 = StringUtil.PadR( StringUtil.RTrim( AV65WWTipodeContaDS_12_Tipodeconta_codigo3), 20, "%");
         lV66WWTipodeContaDS_13_Tipodeconta_nome3 = StringUtil.PadR( StringUtil.RTrim( AV66WWTipodeContaDS_13_Tipodeconta_nome3), 50, "%");
         lV67WWTipodeContaDS_14_Tipodeconta_contapaicod3 = StringUtil.PadR( StringUtil.RTrim( AV67WWTipodeContaDS_14_Tipodeconta_contapaicod3), 20, "%");
         lV68WWTipodeContaDS_15_Tftipodeconta_codigo = StringUtil.PadR( StringUtil.RTrim( AV68WWTipodeContaDS_15_Tftipodeconta_codigo), 20, "%");
         lV70WWTipodeContaDS_17_Tftipodeconta_nome = StringUtil.PadR( StringUtil.RTrim( AV70WWTipodeContaDS_17_Tftipodeconta_nome), 50, "%");
         lV73WWTipodeContaDS_20_Tftipodeconta_contapaicod = StringUtil.PadR( StringUtil.RTrim( AV73WWTipodeContaDS_20_Tftipodeconta_contapaicod), 20, "%");
         /* Using cursor P00OZ4 */
         pr_default.execute(2, new Object[] {lV55WWTipodeContaDS_2_Tipodeconta_codigo1, lV56WWTipodeContaDS_3_Tipodeconta_nome1, lV57WWTipodeContaDS_4_Tipodeconta_contapaicod1, lV60WWTipodeContaDS_7_Tipodeconta_codigo2, lV61WWTipodeContaDS_8_Tipodeconta_nome2, lV62WWTipodeContaDS_9_Tipodeconta_contapaicod2, lV65WWTipodeContaDS_12_Tipodeconta_codigo3, lV66WWTipodeContaDS_13_Tipodeconta_nome3, lV67WWTipodeContaDS_14_Tipodeconta_contapaicod3, lV68WWTipodeContaDS_15_Tftipodeconta_codigo, AV69WWTipodeContaDS_16_Tftipodeconta_codigo_sel, lV70WWTipodeContaDS_17_Tftipodeconta_nome, AV71WWTipodeContaDS_18_Tftipodeconta_nome_sel, lV73WWTipodeContaDS_20_Tftipodeconta_contapaicod, AV74WWTipodeContaDS_21_Tftipodeconta_contapaicod_sel});
         while ( (pr_default.getStatus(2) != 101) )
         {
            BRKOZ5 = false;
            A885TipoDeConta_ContaPaiCod = P00OZ4_A885TipoDeConta_ContaPaiCod[0];
            n885TipoDeConta_ContaPaiCod = P00OZ4_n885TipoDeConta_ContaPaiCod[0];
            A873TipodeConta_Tipo = P00OZ4_A873TipodeConta_Tipo[0];
            A871TipodeConta_Nome = P00OZ4_A871TipodeConta_Nome[0];
            A870TipodeConta_Codigo = P00OZ4_A870TipodeConta_Codigo[0];
            AV30count = 0;
            while ( (pr_default.getStatus(2) != 101) && ( StringUtil.StrCmp(P00OZ4_A885TipoDeConta_ContaPaiCod[0], A885TipoDeConta_ContaPaiCod) == 0 ) )
            {
               BRKOZ5 = false;
               A870TipodeConta_Codigo = P00OZ4_A870TipodeConta_Codigo[0];
               AV30count = (long)(AV30count+1);
               BRKOZ5 = true;
               pr_default.readNext(2);
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( A885TipoDeConta_ContaPaiCod)) )
            {
               AV22Option = A885TipoDeConta_ContaPaiCod;
               AV23Options.Add(AV22Option, 0);
               AV28OptionIndexes.Add(StringUtil.Trim( context.localUtil.Format( (decimal)(AV30count), "Z,ZZZ,ZZZ,ZZ9")), 0);
            }
            if ( AV23Options.Count == 50 )
            {
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
            }
            if ( ! BRKOZ5 )
            {
               BRKOZ5 = true;
               pr_default.readNext(2);
            }
         }
         pr_default.close(2);
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV23Options = new GxSimpleCollection();
         AV26OptionsDesc = new GxSimpleCollection();
         AV28OptionIndexes = new GxSimpleCollection();
         AV9WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV31Session = context.GetSession();
         AV33GridState = new wwpbaseobjects.SdtWWPGridState(context);
         AV34GridStateFilterValue = new wwpbaseobjects.SdtWWPGridState_FilterValue(context);
         AV10TFTipodeConta_Codigo = "";
         AV11TFTipodeConta_Codigo_Sel = "";
         AV12TFTipodeConta_Nome = "";
         AV13TFTipodeConta_Nome_Sel = "";
         AV14TFTipodeConta_Tipo_SelsJson = "";
         AV15TFTipodeConta_Tipo_Sels = new GxSimpleCollection();
         AV16TFTipoDeConta_ContaPaiCod = "";
         AV17TFTipoDeConta_ContaPaiCod_Sel = "";
         AV35GridStateDynamicFilter = new wwpbaseobjects.SdtWWPGridState_DynamicFilter(context);
         AV36DynamicFiltersSelector1 = "";
         AV37TipodeConta_Codigo1 = "";
         AV38TipodeConta_Nome1 = "";
         AV39TipoDeConta_ContaPaiCod1 = "";
         AV41DynamicFiltersSelector2 = "";
         AV42TipodeConta_Codigo2 = "";
         AV43TipodeConta_Nome2 = "";
         AV44TipoDeConta_ContaPaiCod2 = "";
         AV46DynamicFiltersSelector3 = "";
         AV47TipodeConta_Codigo3 = "";
         AV48TipodeConta_Nome3 = "";
         AV49TipoDeConta_ContaPaiCod3 = "";
         AV54WWTipodeContaDS_1_Dynamicfiltersselector1 = "";
         AV55WWTipodeContaDS_2_Tipodeconta_codigo1 = "";
         AV56WWTipodeContaDS_3_Tipodeconta_nome1 = "";
         AV57WWTipodeContaDS_4_Tipodeconta_contapaicod1 = "";
         AV59WWTipodeContaDS_6_Dynamicfiltersselector2 = "";
         AV60WWTipodeContaDS_7_Tipodeconta_codigo2 = "";
         AV61WWTipodeContaDS_8_Tipodeconta_nome2 = "";
         AV62WWTipodeContaDS_9_Tipodeconta_contapaicod2 = "";
         AV64WWTipodeContaDS_11_Dynamicfiltersselector3 = "";
         AV65WWTipodeContaDS_12_Tipodeconta_codigo3 = "";
         AV66WWTipodeContaDS_13_Tipodeconta_nome3 = "";
         AV67WWTipodeContaDS_14_Tipodeconta_contapaicod3 = "";
         AV68WWTipodeContaDS_15_Tftipodeconta_codigo = "";
         AV69WWTipodeContaDS_16_Tftipodeconta_codigo_sel = "";
         AV70WWTipodeContaDS_17_Tftipodeconta_nome = "";
         AV71WWTipodeContaDS_18_Tftipodeconta_nome_sel = "";
         AV72WWTipodeContaDS_19_Tftipodeconta_tipo_sels = new GxSimpleCollection();
         AV73WWTipodeContaDS_20_Tftipodeconta_contapaicod = "";
         AV74WWTipodeContaDS_21_Tftipodeconta_contapaicod_sel = "";
         scmdbuf = "";
         lV55WWTipodeContaDS_2_Tipodeconta_codigo1 = "";
         lV56WWTipodeContaDS_3_Tipodeconta_nome1 = "";
         lV57WWTipodeContaDS_4_Tipodeconta_contapaicod1 = "";
         lV60WWTipodeContaDS_7_Tipodeconta_codigo2 = "";
         lV61WWTipodeContaDS_8_Tipodeconta_nome2 = "";
         lV62WWTipodeContaDS_9_Tipodeconta_contapaicod2 = "";
         lV65WWTipodeContaDS_12_Tipodeconta_codigo3 = "";
         lV66WWTipodeContaDS_13_Tipodeconta_nome3 = "";
         lV67WWTipodeContaDS_14_Tipodeconta_contapaicod3 = "";
         lV68WWTipodeContaDS_15_Tftipodeconta_codigo = "";
         lV70WWTipodeContaDS_17_Tftipodeconta_nome = "";
         lV73WWTipodeContaDS_20_Tftipodeconta_contapaicod = "";
         A873TipodeConta_Tipo = "";
         A870TipodeConta_Codigo = "";
         A871TipodeConta_Nome = "";
         A885TipoDeConta_ContaPaiCod = "";
         P00OZ2_A873TipodeConta_Tipo = new String[] {""} ;
         P00OZ2_A885TipoDeConta_ContaPaiCod = new String[] {""} ;
         P00OZ2_n885TipoDeConta_ContaPaiCod = new bool[] {false} ;
         P00OZ2_A871TipodeConta_Nome = new String[] {""} ;
         P00OZ2_A870TipodeConta_Codigo = new String[] {""} ;
         AV22Option = "";
         P00OZ3_A871TipodeConta_Nome = new String[] {""} ;
         P00OZ3_A873TipodeConta_Tipo = new String[] {""} ;
         P00OZ3_A885TipoDeConta_ContaPaiCod = new String[] {""} ;
         P00OZ3_n885TipoDeConta_ContaPaiCod = new bool[] {false} ;
         P00OZ3_A870TipodeConta_Codigo = new String[] {""} ;
         P00OZ4_A885TipoDeConta_ContaPaiCod = new String[] {""} ;
         P00OZ4_n885TipoDeConta_ContaPaiCod = new bool[] {false} ;
         P00OZ4_A873TipodeConta_Tipo = new String[] {""} ;
         P00OZ4_A871TipodeConta_Nome = new String[] {""} ;
         P00OZ4_A870TipodeConta_Codigo = new String[] {""} ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.getwwtipodecontafilterdata__default(),
            new Object[][] {
                new Object[] {
               P00OZ2_A873TipodeConta_Tipo, P00OZ2_A885TipoDeConta_ContaPaiCod, P00OZ2_n885TipoDeConta_ContaPaiCod, P00OZ2_A871TipodeConta_Nome, P00OZ2_A870TipodeConta_Codigo
               }
               , new Object[] {
               P00OZ3_A871TipodeConta_Nome, P00OZ3_A873TipodeConta_Tipo, P00OZ3_A885TipoDeConta_ContaPaiCod, P00OZ3_n885TipoDeConta_ContaPaiCod, P00OZ3_A870TipodeConta_Codigo
               }
               , new Object[] {
               P00OZ4_A885TipoDeConta_ContaPaiCod, P00OZ4_n885TipoDeConta_ContaPaiCod, P00OZ4_A873TipodeConta_Tipo, P00OZ4_A871TipodeConta_Nome, P00OZ4_A870TipodeConta_Codigo
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV52GXV1 ;
      private int AV72WWTipodeContaDS_19_Tftipodeconta_tipo_sels_Count ;
      private long AV30count ;
      private String AV10TFTipodeConta_Codigo ;
      private String AV11TFTipodeConta_Codigo_Sel ;
      private String AV12TFTipodeConta_Nome ;
      private String AV13TFTipodeConta_Nome_Sel ;
      private String AV16TFTipoDeConta_ContaPaiCod ;
      private String AV17TFTipoDeConta_ContaPaiCod_Sel ;
      private String AV37TipodeConta_Codigo1 ;
      private String AV38TipodeConta_Nome1 ;
      private String AV39TipoDeConta_ContaPaiCod1 ;
      private String AV42TipodeConta_Codigo2 ;
      private String AV43TipodeConta_Nome2 ;
      private String AV44TipoDeConta_ContaPaiCod2 ;
      private String AV47TipodeConta_Codigo3 ;
      private String AV48TipodeConta_Nome3 ;
      private String AV49TipoDeConta_ContaPaiCod3 ;
      private String AV55WWTipodeContaDS_2_Tipodeconta_codigo1 ;
      private String AV56WWTipodeContaDS_3_Tipodeconta_nome1 ;
      private String AV57WWTipodeContaDS_4_Tipodeconta_contapaicod1 ;
      private String AV60WWTipodeContaDS_7_Tipodeconta_codigo2 ;
      private String AV61WWTipodeContaDS_8_Tipodeconta_nome2 ;
      private String AV62WWTipodeContaDS_9_Tipodeconta_contapaicod2 ;
      private String AV65WWTipodeContaDS_12_Tipodeconta_codigo3 ;
      private String AV66WWTipodeContaDS_13_Tipodeconta_nome3 ;
      private String AV67WWTipodeContaDS_14_Tipodeconta_contapaicod3 ;
      private String AV68WWTipodeContaDS_15_Tftipodeconta_codigo ;
      private String AV69WWTipodeContaDS_16_Tftipodeconta_codigo_sel ;
      private String AV70WWTipodeContaDS_17_Tftipodeconta_nome ;
      private String AV71WWTipodeContaDS_18_Tftipodeconta_nome_sel ;
      private String AV73WWTipodeContaDS_20_Tftipodeconta_contapaicod ;
      private String AV74WWTipodeContaDS_21_Tftipodeconta_contapaicod_sel ;
      private String scmdbuf ;
      private String lV55WWTipodeContaDS_2_Tipodeconta_codigo1 ;
      private String lV56WWTipodeContaDS_3_Tipodeconta_nome1 ;
      private String lV57WWTipodeContaDS_4_Tipodeconta_contapaicod1 ;
      private String lV60WWTipodeContaDS_7_Tipodeconta_codigo2 ;
      private String lV61WWTipodeContaDS_8_Tipodeconta_nome2 ;
      private String lV62WWTipodeContaDS_9_Tipodeconta_contapaicod2 ;
      private String lV65WWTipodeContaDS_12_Tipodeconta_codigo3 ;
      private String lV66WWTipodeContaDS_13_Tipodeconta_nome3 ;
      private String lV67WWTipodeContaDS_14_Tipodeconta_contapaicod3 ;
      private String lV68WWTipodeContaDS_15_Tftipodeconta_codigo ;
      private String lV70WWTipodeContaDS_17_Tftipodeconta_nome ;
      private String lV73WWTipodeContaDS_20_Tftipodeconta_contapaicod ;
      private String A873TipodeConta_Tipo ;
      private String A870TipodeConta_Codigo ;
      private String A871TipodeConta_Nome ;
      private String A885TipoDeConta_ContaPaiCod ;
      private bool returnInSub ;
      private bool AV40DynamicFiltersEnabled2 ;
      private bool AV45DynamicFiltersEnabled3 ;
      private bool AV58WWTipodeContaDS_5_Dynamicfiltersenabled2 ;
      private bool AV63WWTipodeContaDS_10_Dynamicfiltersenabled3 ;
      private bool n885TipoDeConta_ContaPaiCod ;
      private bool BRKOZ3 ;
      private bool BRKOZ5 ;
      private String AV29OptionIndexesJson ;
      private String AV24OptionsJson ;
      private String AV27OptionsDescJson ;
      private String AV14TFTipodeConta_Tipo_SelsJson ;
      private String AV20DDOName ;
      private String AV18SearchTxt ;
      private String AV19SearchTxtTo ;
      private String AV36DynamicFiltersSelector1 ;
      private String AV41DynamicFiltersSelector2 ;
      private String AV46DynamicFiltersSelector3 ;
      private String AV54WWTipodeContaDS_1_Dynamicfiltersselector1 ;
      private String AV59WWTipodeContaDS_6_Dynamicfiltersselector2 ;
      private String AV64WWTipodeContaDS_11_Dynamicfiltersselector3 ;
      private String AV22Option ;
      private IGxSession AV31Session ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private String[] P00OZ2_A873TipodeConta_Tipo ;
      private String[] P00OZ2_A885TipoDeConta_ContaPaiCod ;
      private bool[] P00OZ2_n885TipoDeConta_ContaPaiCod ;
      private String[] P00OZ2_A871TipodeConta_Nome ;
      private String[] P00OZ2_A870TipodeConta_Codigo ;
      private String[] P00OZ3_A871TipodeConta_Nome ;
      private String[] P00OZ3_A873TipodeConta_Tipo ;
      private String[] P00OZ3_A885TipoDeConta_ContaPaiCod ;
      private bool[] P00OZ3_n885TipoDeConta_ContaPaiCod ;
      private String[] P00OZ3_A870TipodeConta_Codigo ;
      private String[] P00OZ4_A885TipoDeConta_ContaPaiCod ;
      private bool[] P00OZ4_n885TipoDeConta_ContaPaiCod ;
      private String[] P00OZ4_A873TipodeConta_Tipo ;
      private String[] P00OZ4_A871TipodeConta_Nome ;
      private String[] P00OZ4_A870TipodeConta_Codigo ;
      private String aP3_OptionsJson ;
      private String aP4_OptionsDescJson ;
      private String aP5_OptionIndexesJson ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV15TFTipodeConta_Tipo_Sels ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV72WWTipodeContaDS_19_Tftipodeconta_tipo_sels ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV23Options ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV26OptionsDesc ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV28OptionIndexes ;
      private wwpbaseobjects.SdtWWPContext AV9WWPContext ;
      private wwpbaseobjects.SdtWWPGridState AV33GridState ;
      private wwpbaseobjects.SdtWWPGridState_FilterValue AV34GridStateFilterValue ;
      private wwpbaseobjects.SdtWWPGridState_DynamicFilter AV35GridStateDynamicFilter ;
   }

   public class getwwtipodecontafilterdata__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P00OZ2( IGxContext context ,
                                             String A873TipodeConta_Tipo ,
                                             IGxCollection AV72WWTipodeContaDS_19_Tftipodeconta_tipo_sels ,
                                             String AV54WWTipodeContaDS_1_Dynamicfiltersselector1 ,
                                             String AV55WWTipodeContaDS_2_Tipodeconta_codigo1 ,
                                             String AV56WWTipodeContaDS_3_Tipodeconta_nome1 ,
                                             String AV57WWTipodeContaDS_4_Tipodeconta_contapaicod1 ,
                                             bool AV58WWTipodeContaDS_5_Dynamicfiltersenabled2 ,
                                             String AV59WWTipodeContaDS_6_Dynamicfiltersselector2 ,
                                             String AV60WWTipodeContaDS_7_Tipodeconta_codigo2 ,
                                             String AV61WWTipodeContaDS_8_Tipodeconta_nome2 ,
                                             String AV62WWTipodeContaDS_9_Tipodeconta_contapaicod2 ,
                                             bool AV63WWTipodeContaDS_10_Dynamicfiltersenabled3 ,
                                             String AV64WWTipodeContaDS_11_Dynamicfiltersselector3 ,
                                             String AV65WWTipodeContaDS_12_Tipodeconta_codigo3 ,
                                             String AV66WWTipodeContaDS_13_Tipodeconta_nome3 ,
                                             String AV67WWTipodeContaDS_14_Tipodeconta_contapaicod3 ,
                                             String AV69WWTipodeContaDS_16_Tftipodeconta_codigo_sel ,
                                             String AV68WWTipodeContaDS_15_Tftipodeconta_codigo ,
                                             String AV71WWTipodeContaDS_18_Tftipodeconta_nome_sel ,
                                             String AV70WWTipodeContaDS_17_Tftipodeconta_nome ,
                                             int AV72WWTipodeContaDS_19_Tftipodeconta_tipo_sels_Count ,
                                             String AV74WWTipodeContaDS_21_Tftipodeconta_contapaicod_sel ,
                                             String AV73WWTipodeContaDS_20_Tftipodeconta_contapaicod ,
                                             String A870TipodeConta_Codigo ,
                                             String A871TipodeConta_Nome ,
                                             String A885TipoDeConta_ContaPaiCod )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [15] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         scmdbuf = "SELECT DISTINCT NULL AS [TipodeConta_Tipo], NULL AS [TipoDeConta_ContaPaiCod], NULL AS [TipodeConta_Nome], [TipodeConta_Codigo] FROM ( SELECT TOP(100) PERCENT [TipodeConta_Tipo], [TipoDeConta_ContaPaiCod], [TipodeConta_Nome], [TipodeConta_Codigo] FROM [TipodeConta] WITH (NOLOCK)";
         if ( ( StringUtil.StrCmp(AV54WWTipodeContaDS_1_Dynamicfiltersselector1, "TIPODECONTA_CODIGO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV55WWTipodeContaDS_2_Tipodeconta_codigo1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipodeConta_Codigo] like '%' + @lV55WWTipodeContaDS_2_Tipodeconta_codigo1)";
            }
            else
            {
               sWhereString = sWhereString + " ([TipodeConta_Codigo] like '%' + @lV55WWTipodeContaDS_2_Tipodeconta_codigo1)";
            }
         }
         else
         {
            GXv_int1[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV54WWTipodeContaDS_1_Dynamicfiltersselector1, "TIPODECONTA_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV56WWTipodeContaDS_3_Tipodeconta_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipodeConta_Nome] like '%' + @lV56WWTipodeContaDS_3_Tipodeconta_nome1 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " ([TipodeConta_Nome] like '%' + @lV56WWTipodeContaDS_3_Tipodeconta_nome1 + '%')";
            }
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV54WWTipodeContaDS_1_Dynamicfiltersselector1, "TIPODECONTA_CONTAPAICOD") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV57WWTipodeContaDS_4_Tipodeconta_contapaicod1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipoDeConta_ContaPaiCod] like '%' + @lV57WWTipodeContaDS_4_Tipodeconta_contapaicod1)";
            }
            else
            {
               sWhereString = sWhereString + " ([TipoDeConta_ContaPaiCod] like '%' + @lV57WWTipodeContaDS_4_Tipodeconta_contapaicod1)";
            }
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( AV58WWTipodeContaDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV59WWTipodeContaDS_6_Dynamicfiltersselector2, "TIPODECONTA_CODIGO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60WWTipodeContaDS_7_Tipodeconta_codigo2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipodeConta_Codigo] like '%' + @lV60WWTipodeContaDS_7_Tipodeconta_codigo2)";
            }
            else
            {
               sWhereString = sWhereString + " ([TipodeConta_Codigo] like '%' + @lV60WWTipodeContaDS_7_Tipodeconta_codigo2)";
            }
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( AV58WWTipodeContaDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV59WWTipodeContaDS_6_Dynamicfiltersselector2, "TIPODECONTA_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61WWTipodeContaDS_8_Tipodeconta_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipodeConta_Nome] like '%' + @lV61WWTipodeContaDS_8_Tipodeconta_nome2 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " ([TipodeConta_Nome] like '%' + @lV61WWTipodeContaDS_8_Tipodeconta_nome2 + '%')";
            }
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( AV58WWTipodeContaDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV59WWTipodeContaDS_6_Dynamicfiltersselector2, "TIPODECONTA_CONTAPAICOD") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV62WWTipodeContaDS_9_Tipodeconta_contapaicod2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipoDeConta_ContaPaiCod] like '%' + @lV62WWTipodeContaDS_9_Tipodeconta_contapaicod2)";
            }
            else
            {
               sWhereString = sWhereString + " ([TipoDeConta_ContaPaiCod] like '%' + @lV62WWTipodeContaDS_9_Tipodeconta_contapaicod2)";
            }
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( AV63WWTipodeContaDS_10_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV64WWTipodeContaDS_11_Dynamicfiltersselector3, "TIPODECONTA_CODIGO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV65WWTipodeContaDS_12_Tipodeconta_codigo3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipodeConta_Codigo] like '%' + @lV65WWTipodeContaDS_12_Tipodeconta_codigo3)";
            }
            else
            {
               sWhereString = sWhereString + " ([TipodeConta_Codigo] like '%' + @lV65WWTipodeContaDS_12_Tipodeconta_codigo3)";
            }
         }
         else
         {
            GXv_int1[6] = 1;
         }
         if ( AV63WWTipodeContaDS_10_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV64WWTipodeContaDS_11_Dynamicfiltersselector3, "TIPODECONTA_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV66WWTipodeContaDS_13_Tipodeconta_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipodeConta_Nome] like '%' + @lV66WWTipodeContaDS_13_Tipodeconta_nome3 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " ([TipodeConta_Nome] like '%' + @lV66WWTipodeContaDS_13_Tipodeconta_nome3 + '%')";
            }
         }
         else
         {
            GXv_int1[7] = 1;
         }
         if ( AV63WWTipodeContaDS_10_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV64WWTipodeContaDS_11_Dynamicfiltersselector3, "TIPODECONTA_CONTAPAICOD") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV67WWTipodeContaDS_14_Tipodeconta_contapaicod3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipoDeConta_ContaPaiCod] like '%' + @lV67WWTipodeContaDS_14_Tipodeconta_contapaicod3)";
            }
            else
            {
               sWhereString = sWhereString + " ([TipoDeConta_ContaPaiCod] like '%' + @lV67WWTipodeContaDS_14_Tipodeconta_contapaicod3)";
            }
         }
         else
         {
            GXv_int1[8] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV69WWTipodeContaDS_16_Tftipodeconta_codigo_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV68WWTipodeContaDS_15_Tftipodeconta_codigo)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipodeConta_Codigo] like @lV68WWTipodeContaDS_15_Tftipodeconta_codigo)";
            }
            else
            {
               sWhereString = sWhereString + " ([TipodeConta_Codigo] like @lV68WWTipodeContaDS_15_Tftipodeconta_codigo)";
            }
         }
         else
         {
            GXv_int1[9] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV69WWTipodeContaDS_16_Tftipodeconta_codigo_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipodeConta_Codigo] = @AV69WWTipodeContaDS_16_Tftipodeconta_codigo_sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([TipodeConta_Codigo] = @AV69WWTipodeContaDS_16_Tftipodeconta_codigo_sel)";
            }
         }
         else
         {
            GXv_int1[10] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV71WWTipodeContaDS_18_Tftipodeconta_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV70WWTipodeContaDS_17_Tftipodeconta_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipodeConta_Nome] like @lV70WWTipodeContaDS_17_Tftipodeconta_nome)";
            }
            else
            {
               sWhereString = sWhereString + " ([TipodeConta_Nome] like @lV70WWTipodeContaDS_17_Tftipodeconta_nome)";
            }
         }
         else
         {
            GXv_int1[11] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV71WWTipodeContaDS_18_Tftipodeconta_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipodeConta_Nome] = @AV71WWTipodeContaDS_18_Tftipodeconta_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([TipodeConta_Nome] = @AV71WWTipodeContaDS_18_Tftipodeconta_nome_sel)";
            }
         }
         else
         {
            GXv_int1[12] = 1;
         }
         if ( AV72WWTipodeContaDS_19_Tftipodeconta_tipo_sels_Count > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV72WWTipodeContaDS_19_Tftipodeconta_tipo_sels, "[TipodeConta_Tipo] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV72WWTipodeContaDS_19_Tftipodeconta_tipo_sels, "[TipodeConta_Tipo] IN (", ")") + ")";
            }
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV74WWTipodeContaDS_21_Tftipodeconta_contapaicod_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV73WWTipodeContaDS_20_Tftipodeconta_contapaicod)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipoDeConta_ContaPaiCod] like @lV73WWTipodeContaDS_20_Tftipodeconta_contapaicod)";
            }
            else
            {
               sWhereString = sWhereString + " ([TipoDeConta_ContaPaiCod] like @lV73WWTipodeContaDS_20_Tftipodeconta_contapaicod)";
            }
         }
         else
         {
            GXv_int1[13] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV74WWTipodeContaDS_21_Tftipodeconta_contapaicod_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipoDeConta_ContaPaiCod] = @AV74WWTipodeContaDS_21_Tftipodeconta_contapaicod_sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([TipoDeConta_ContaPaiCod] = @AV74WWTipodeContaDS_21_Tftipodeconta_contapaicod_sel)";
            }
         }
         else
         {
            GXv_int1[14] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY [TipodeConta_Codigo]";
         scmdbuf = scmdbuf + ") DistinctT";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      protected Object[] conditional_P00OZ3( IGxContext context ,
                                             String A873TipodeConta_Tipo ,
                                             IGxCollection AV72WWTipodeContaDS_19_Tftipodeconta_tipo_sels ,
                                             String AV54WWTipodeContaDS_1_Dynamicfiltersselector1 ,
                                             String AV55WWTipodeContaDS_2_Tipodeconta_codigo1 ,
                                             String AV56WWTipodeContaDS_3_Tipodeconta_nome1 ,
                                             String AV57WWTipodeContaDS_4_Tipodeconta_contapaicod1 ,
                                             bool AV58WWTipodeContaDS_5_Dynamicfiltersenabled2 ,
                                             String AV59WWTipodeContaDS_6_Dynamicfiltersselector2 ,
                                             String AV60WWTipodeContaDS_7_Tipodeconta_codigo2 ,
                                             String AV61WWTipodeContaDS_8_Tipodeconta_nome2 ,
                                             String AV62WWTipodeContaDS_9_Tipodeconta_contapaicod2 ,
                                             bool AV63WWTipodeContaDS_10_Dynamicfiltersenabled3 ,
                                             String AV64WWTipodeContaDS_11_Dynamicfiltersselector3 ,
                                             String AV65WWTipodeContaDS_12_Tipodeconta_codigo3 ,
                                             String AV66WWTipodeContaDS_13_Tipodeconta_nome3 ,
                                             String AV67WWTipodeContaDS_14_Tipodeconta_contapaicod3 ,
                                             String AV69WWTipodeContaDS_16_Tftipodeconta_codigo_sel ,
                                             String AV68WWTipodeContaDS_15_Tftipodeconta_codigo ,
                                             String AV71WWTipodeContaDS_18_Tftipodeconta_nome_sel ,
                                             String AV70WWTipodeContaDS_17_Tftipodeconta_nome ,
                                             int AV72WWTipodeContaDS_19_Tftipodeconta_tipo_sels_Count ,
                                             String AV74WWTipodeContaDS_21_Tftipodeconta_contapaicod_sel ,
                                             String AV73WWTipodeContaDS_20_Tftipodeconta_contapaicod ,
                                             String A870TipodeConta_Codigo ,
                                             String A871TipodeConta_Nome ,
                                             String A885TipoDeConta_ContaPaiCod )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [15] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT [TipodeConta_Nome], [TipodeConta_Tipo], [TipoDeConta_ContaPaiCod], [TipodeConta_Codigo] FROM [TipodeConta] WITH (NOLOCK)";
         if ( ( StringUtil.StrCmp(AV54WWTipodeContaDS_1_Dynamicfiltersselector1, "TIPODECONTA_CODIGO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV55WWTipodeContaDS_2_Tipodeconta_codigo1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipodeConta_Codigo] like '%' + @lV55WWTipodeContaDS_2_Tipodeconta_codigo1)";
            }
            else
            {
               sWhereString = sWhereString + " ([TipodeConta_Codigo] like '%' + @lV55WWTipodeContaDS_2_Tipodeconta_codigo1)";
            }
         }
         else
         {
            GXv_int3[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV54WWTipodeContaDS_1_Dynamicfiltersselector1, "TIPODECONTA_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV56WWTipodeContaDS_3_Tipodeconta_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipodeConta_Nome] like '%' + @lV56WWTipodeContaDS_3_Tipodeconta_nome1 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " ([TipodeConta_Nome] like '%' + @lV56WWTipodeContaDS_3_Tipodeconta_nome1 + '%')";
            }
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV54WWTipodeContaDS_1_Dynamicfiltersselector1, "TIPODECONTA_CONTAPAICOD") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV57WWTipodeContaDS_4_Tipodeconta_contapaicod1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipoDeConta_ContaPaiCod] like '%' + @lV57WWTipodeContaDS_4_Tipodeconta_contapaicod1)";
            }
            else
            {
               sWhereString = sWhereString + " ([TipoDeConta_ContaPaiCod] like '%' + @lV57WWTipodeContaDS_4_Tipodeconta_contapaicod1)";
            }
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( AV58WWTipodeContaDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV59WWTipodeContaDS_6_Dynamicfiltersselector2, "TIPODECONTA_CODIGO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60WWTipodeContaDS_7_Tipodeconta_codigo2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipodeConta_Codigo] like '%' + @lV60WWTipodeContaDS_7_Tipodeconta_codigo2)";
            }
            else
            {
               sWhereString = sWhereString + " ([TipodeConta_Codigo] like '%' + @lV60WWTipodeContaDS_7_Tipodeconta_codigo2)";
            }
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( AV58WWTipodeContaDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV59WWTipodeContaDS_6_Dynamicfiltersselector2, "TIPODECONTA_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61WWTipodeContaDS_8_Tipodeconta_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipodeConta_Nome] like '%' + @lV61WWTipodeContaDS_8_Tipodeconta_nome2 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " ([TipodeConta_Nome] like '%' + @lV61WWTipodeContaDS_8_Tipodeconta_nome2 + '%')";
            }
         }
         else
         {
            GXv_int3[4] = 1;
         }
         if ( AV58WWTipodeContaDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV59WWTipodeContaDS_6_Dynamicfiltersselector2, "TIPODECONTA_CONTAPAICOD") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV62WWTipodeContaDS_9_Tipodeconta_contapaicod2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipoDeConta_ContaPaiCod] like '%' + @lV62WWTipodeContaDS_9_Tipodeconta_contapaicod2)";
            }
            else
            {
               sWhereString = sWhereString + " ([TipoDeConta_ContaPaiCod] like '%' + @lV62WWTipodeContaDS_9_Tipodeconta_contapaicod2)";
            }
         }
         else
         {
            GXv_int3[5] = 1;
         }
         if ( AV63WWTipodeContaDS_10_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV64WWTipodeContaDS_11_Dynamicfiltersselector3, "TIPODECONTA_CODIGO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV65WWTipodeContaDS_12_Tipodeconta_codigo3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipodeConta_Codigo] like '%' + @lV65WWTipodeContaDS_12_Tipodeconta_codigo3)";
            }
            else
            {
               sWhereString = sWhereString + " ([TipodeConta_Codigo] like '%' + @lV65WWTipodeContaDS_12_Tipodeconta_codigo3)";
            }
         }
         else
         {
            GXv_int3[6] = 1;
         }
         if ( AV63WWTipodeContaDS_10_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV64WWTipodeContaDS_11_Dynamicfiltersselector3, "TIPODECONTA_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV66WWTipodeContaDS_13_Tipodeconta_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipodeConta_Nome] like '%' + @lV66WWTipodeContaDS_13_Tipodeconta_nome3 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " ([TipodeConta_Nome] like '%' + @lV66WWTipodeContaDS_13_Tipodeconta_nome3 + '%')";
            }
         }
         else
         {
            GXv_int3[7] = 1;
         }
         if ( AV63WWTipodeContaDS_10_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV64WWTipodeContaDS_11_Dynamicfiltersselector3, "TIPODECONTA_CONTAPAICOD") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV67WWTipodeContaDS_14_Tipodeconta_contapaicod3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipoDeConta_ContaPaiCod] like '%' + @lV67WWTipodeContaDS_14_Tipodeconta_contapaicod3)";
            }
            else
            {
               sWhereString = sWhereString + " ([TipoDeConta_ContaPaiCod] like '%' + @lV67WWTipodeContaDS_14_Tipodeconta_contapaicod3)";
            }
         }
         else
         {
            GXv_int3[8] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV69WWTipodeContaDS_16_Tftipodeconta_codigo_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV68WWTipodeContaDS_15_Tftipodeconta_codigo)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipodeConta_Codigo] like @lV68WWTipodeContaDS_15_Tftipodeconta_codigo)";
            }
            else
            {
               sWhereString = sWhereString + " ([TipodeConta_Codigo] like @lV68WWTipodeContaDS_15_Tftipodeconta_codigo)";
            }
         }
         else
         {
            GXv_int3[9] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV69WWTipodeContaDS_16_Tftipodeconta_codigo_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipodeConta_Codigo] = @AV69WWTipodeContaDS_16_Tftipodeconta_codigo_sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([TipodeConta_Codigo] = @AV69WWTipodeContaDS_16_Tftipodeconta_codigo_sel)";
            }
         }
         else
         {
            GXv_int3[10] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV71WWTipodeContaDS_18_Tftipodeconta_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV70WWTipodeContaDS_17_Tftipodeconta_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipodeConta_Nome] like @lV70WWTipodeContaDS_17_Tftipodeconta_nome)";
            }
            else
            {
               sWhereString = sWhereString + " ([TipodeConta_Nome] like @lV70WWTipodeContaDS_17_Tftipodeconta_nome)";
            }
         }
         else
         {
            GXv_int3[11] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV71WWTipodeContaDS_18_Tftipodeconta_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipodeConta_Nome] = @AV71WWTipodeContaDS_18_Tftipodeconta_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([TipodeConta_Nome] = @AV71WWTipodeContaDS_18_Tftipodeconta_nome_sel)";
            }
         }
         else
         {
            GXv_int3[12] = 1;
         }
         if ( AV72WWTipodeContaDS_19_Tftipodeconta_tipo_sels_Count > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV72WWTipodeContaDS_19_Tftipodeconta_tipo_sels, "[TipodeConta_Tipo] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV72WWTipodeContaDS_19_Tftipodeconta_tipo_sels, "[TipodeConta_Tipo] IN (", ")") + ")";
            }
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV74WWTipodeContaDS_21_Tftipodeconta_contapaicod_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV73WWTipodeContaDS_20_Tftipodeconta_contapaicod)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipoDeConta_ContaPaiCod] like @lV73WWTipodeContaDS_20_Tftipodeconta_contapaicod)";
            }
            else
            {
               sWhereString = sWhereString + " ([TipoDeConta_ContaPaiCod] like @lV73WWTipodeContaDS_20_Tftipodeconta_contapaicod)";
            }
         }
         else
         {
            GXv_int3[13] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV74WWTipodeContaDS_21_Tftipodeconta_contapaicod_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipoDeConta_ContaPaiCod] = @AV74WWTipodeContaDS_21_Tftipodeconta_contapaicod_sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([TipoDeConta_ContaPaiCod] = @AV74WWTipodeContaDS_21_Tftipodeconta_contapaicod_sel)";
            }
         }
         else
         {
            GXv_int3[14] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY [TipodeConta_Nome]";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      protected Object[] conditional_P00OZ4( IGxContext context ,
                                             String A873TipodeConta_Tipo ,
                                             IGxCollection AV72WWTipodeContaDS_19_Tftipodeconta_tipo_sels ,
                                             String AV54WWTipodeContaDS_1_Dynamicfiltersselector1 ,
                                             String AV55WWTipodeContaDS_2_Tipodeconta_codigo1 ,
                                             String AV56WWTipodeContaDS_3_Tipodeconta_nome1 ,
                                             String AV57WWTipodeContaDS_4_Tipodeconta_contapaicod1 ,
                                             bool AV58WWTipodeContaDS_5_Dynamicfiltersenabled2 ,
                                             String AV59WWTipodeContaDS_6_Dynamicfiltersselector2 ,
                                             String AV60WWTipodeContaDS_7_Tipodeconta_codigo2 ,
                                             String AV61WWTipodeContaDS_8_Tipodeconta_nome2 ,
                                             String AV62WWTipodeContaDS_9_Tipodeconta_contapaicod2 ,
                                             bool AV63WWTipodeContaDS_10_Dynamicfiltersenabled3 ,
                                             String AV64WWTipodeContaDS_11_Dynamicfiltersselector3 ,
                                             String AV65WWTipodeContaDS_12_Tipodeconta_codigo3 ,
                                             String AV66WWTipodeContaDS_13_Tipodeconta_nome3 ,
                                             String AV67WWTipodeContaDS_14_Tipodeconta_contapaicod3 ,
                                             String AV69WWTipodeContaDS_16_Tftipodeconta_codigo_sel ,
                                             String AV68WWTipodeContaDS_15_Tftipodeconta_codigo ,
                                             String AV71WWTipodeContaDS_18_Tftipodeconta_nome_sel ,
                                             String AV70WWTipodeContaDS_17_Tftipodeconta_nome ,
                                             int AV72WWTipodeContaDS_19_Tftipodeconta_tipo_sels_Count ,
                                             String AV74WWTipodeContaDS_21_Tftipodeconta_contapaicod_sel ,
                                             String AV73WWTipodeContaDS_20_Tftipodeconta_contapaicod ,
                                             String A870TipodeConta_Codigo ,
                                             String A871TipodeConta_Nome ,
                                             String A885TipoDeConta_ContaPaiCod )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int5 ;
         GXv_int5 = new short [15] ;
         Object[] GXv_Object6 ;
         GXv_Object6 = new Object [2] ;
         scmdbuf = "SELECT [TipoDeConta_ContaPaiCod], [TipodeConta_Tipo], [TipodeConta_Nome], [TipodeConta_Codigo] FROM [TipodeConta] WITH (NOLOCK)";
         if ( ( StringUtil.StrCmp(AV54WWTipodeContaDS_1_Dynamicfiltersselector1, "TIPODECONTA_CODIGO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV55WWTipodeContaDS_2_Tipodeconta_codigo1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipodeConta_Codigo] like '%' + @lV55WWTipodeContaDS_2_Tipodeconta_codigo1)";
            }
            else
            {
               sWhereString = sWhereString + " ([TipodeConta_Codigo] like '%' + @lV55WWTipodeContaDS_2_Tipodeconta_codigo1)";
            }
         }
         else
         {
            GXv_int5[0] = 1;
         }
         if ( ( StringUtil.StrCmp(AV54WWTipodeContaDS_1_Dynamicfiltersselector1, "TIPODECONTA_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV56WWTipodeContaDS_3_Tipodeconta_nome1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipodeConta_Nome] like '%' + @lV56WWTipodeContaDS_3_Tipodeconta_nome1 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " ([TipodeConta_Nome] like '%' + @lV56WWTipodeContaDS_3_Tipodeconta_nome1 + '%')";
            }
         }
         else
         {
            GXv_int5[1] = 1;
         }
         if ( ( StringUtil.StrCmp(AV54WWTipodeContaDS_1_Dynamicfiltersselector1, "TIPODECONTA_CONTAPAICOD") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV57WWTipodeContaDS_4_Tipodeconta_contapaicod1)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipoDeConta_ContaPaiCod] like '%' + @lV57WWTipodeContaDS_4_Tipodeconta_contapaicod1)";
            }
            else
            {
               sWhereString = sWhereString + " ([TipoDeConta_ContaPaiCod] like '%' + @lV57WWTipodeContaDS_4_Tipodeconta_contapaicod1)";
            }
         }
         else
         {
            GXv_int5[2] = 1;
         }
         if ( AV58WWTipodeContaDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV59WWTipodeContaDS_6_Dynamicfiltersselector2, "TIPODECONTA_CODIGO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV60WWTipodeContaDS_7_Tipodeconta_codigo2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipodeConta_Codigo] like '%' + @lV60WWTipodeContaDS_7_Tipodeconta_codigo2)";
            }
            else
            {
               sWhereString = sWhereString + " ([TipodeConta_Codigo] like '%' + @lV60WWTipodeContaDS_7_Tipodeconta_codigo2)";
            }
         }
         else
         {
            GXv_int5[3] = 1;
         }
         if ( AV58WWTipodeContaDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV59WWTipodeContaDS_6_Dynamicfiltersselector2, "TIPODECONTA_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV61WWTipodeContaDS_8_Tipodeconta_nome2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipodeConta_Nome] like '%' + @lV61WWTipodeContaDS_8_Tipodeconta_nome2 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " ([TipodeConta_Nome] like '%' + @lV61WWTipodeContaDS_8_Tipodeconta_nome2 + '%')";
            }
         }
         else
         {
            GXv_int5[4] = 1;
         }
         if ( AV58WWTipodeContaDS_5_Dynamicfiltersenabled2 && ( StringUtil.StrCmp(AV59WWTipodeContaDS_6_Dynamicfiltersselector2, "TIPODECONTA_CONTAPAICOD") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV62WWTipodeContaDS_9_Tipodeconta_contapaicod2)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipoDeConta_ContaPaiCod] like '%' + @lV62WWTipodeContaDS_9_Tipodeconta_contapaicod2)";
            }
            else
            {
               sWhereString = sWhereString + " ([TipoDeConta_ContaPaiCod] like '%' + @lV62WWTipodeContaDS_9_Tipodeconta_contapaicod2)";
            }
         }
         else
         {
            GXv_int5[5] = 1;
         }
         if ( AV63WWTipodeContaDS_10_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV64WWTipodeContaDS_11_Dynamicfiltersselector3, "TIPODECONTA_CODIGO") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV65WWTipodeContaDS_12_Tipodeconta_codigo3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipodeConta_Codigo] like '%' + @lV65WWTipodeContaDS_12_Tipodeconta_codigo3)";
            }
            else
            {
               sWhereString = sWhereString + " ([TipodeConta_Codigo] like '%' + @lV65WWTipodeContaDS_12_Tipodeconta_codigo3)";
            }
         }
         else
         {
            GXv_int5[6] = 1;
         }
         if ( AV63WWTipodeContaDS_10_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV64WWTipodeContaDS_11_Dynamicfiltersselector3, "TIPODECONTA_NOME") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV66WWTipodeContaDS_13_Tipodeconta_nome3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipodeConta_Nome] like '%' + @lV66WWTipodeContaDS_13_Tipodeconta_nome3 + '%')";
            }
            else
            {
               sWhereString = sWhereString + " ([TipodeConta_Nome] like '%' + @lV66WWTipodeContaDS_13_Tipodeconta_nome3 + '%')";
            }
         }
         else
         {
            GXv_int5[7] = 1;
         }
         if ( AV63WWTipodeContaDS_10_Dynamicfiltersenabled3 && ( StringUtil.StrCmp(AV64WWTipodeContaDS_11_Dynamicfiltersselector3, "TIPODECONTA_CONTAPAICOD") == 0 ) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV67WWTipodeContaDS_14_Tipodeconta_contapaicod3)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipoDeConta_ContaPaiCod] like '%' + @lV67WWTipodeContaDS_14_Tipodeconta_contapaicod3)";
            }
            else
            {
               sWhereString = sWhereString + " ([TipoDeConta_ContaPaiCod] like '%' + @lV67WWTipodeContaDS_14_Tipodeconta_contapaicod3)";
            }
         }
         else
         {
            GXv_int5[8] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV69WWTipodeContaDS_16_Tftipodeconta_codigo_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV68WWTipodeContaDS_15_Tftipodeconta_codigo)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipodeConta_Codigo] like @lV68WWTipodeContaDS_15_Tftipodeconta_codigo)";
            }
            else
            {
               sWhereString = sWhereString + " ([TipodeConta_Codigo] like @lV68WWTipodeContaDS_15_Tftipodeconta_codigo)";
            }
         }
         else
         {
            GXv_int5[9] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV69WWTipodeContaDS_16_Tftipodeconta_codigo_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipodeConta_Codigo] = @AV69WWTipodeContaDS_16_Tftipodeconta_codigo_sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([TipodeConta_Codigo] = @AV69WWTipodeContaDS_16_Tftipodeconta_codigo_sel)";
            }
         }
         else
         {
            GXv_int5[10] = 1;
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV71WWTipodeContaDS_18_Tftipodeconta_nome_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV70WWTipodeContaDS_17_Tftipodeconta_nome)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipodeConta_Nome] like @lV70WWTipodeContaDS_17_Tftipodeconta_nome)";
            }
            else
            {
               sWhereString = sWhereString + " ([TipodeConta_Nome] like @lV70WWTipodeContaDS_17_Tftipodeconta_nome)";
            }
         }
         else
         {
            GXv_int5[11] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV71WWTipodeContaDS_18_Tftipodeconta_nome_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipodeConta_Nome] = @AV71WWTipodeContaDS_18_Tftipodeconta_nome_sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([TipodeConta_Nome] = @AV71WWTipodeContaDS_18_Tftipodeconta_nome_sel)";
            }
         }
         else
         {
            GXv_int5[12] = 1;
         }
         if ( AV72WWTipodeContaDS_19_Tftipodeconta_tipo_sels_Count > 0 )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV72WWTipodeContaDS_19_Tftipodeconta_tipo_sels, "[TipodeConta_Tipo] IN (", ")") + ")";
            }
            else
            {
               sWhereString = sWhereString + " (" + new GxDbmsUtils( new GxSqlServer()).ValueList(AV72WWTipodeContaDS_19_Tftipodeconta_tipo_sels, "[TipodeConta_Tipo] IN (", ")") + ")";
            }
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( AV74WWTipodeContaDS_21_Tftipodeconta_contapaicod_sel)) && ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV73WWTipodeContaDS_20_Tftipodeconta_contapaicod)) ) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipoDeConta_ContaPaiCod] like @lV73WWTipodeContaDS_20_Tftipodeconta_contapaicod)";
            }
            else
            {
               sWhereString = sWhereString + " ([TipoDeConta_ContaPaiCod] like @lV73WWTipodeContaDS_20_Tftipodeconta_contapaicod)";
            }
         }
         else
         {
            GXv_int5[13] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV74WWTipodeContaDS_21_Tftipodeconta_contapaicod_sel)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([TipoDeConta_ContaPaiCod] = @AV74WWTipodeContaDS_21_Tftipodeconta_contapaicod_sel)";
            }
            else
            {
               sWhereString = sWhereString + " ([TipoDeConta_ContaPaiCod] = @AV74WWTipodeContaDS_21_Tftipodeconta_contapaicod_sel)";
            }
         }
         else
         {
            GXv_int5[14] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         scmdbuf = scmdbuf + " ORDER BY [TipoDeConta_ContaPaiCod]";
         GXv_Object6[0] = scmdbuf;
         GXv_Object6[1] = GXv_int5;
         return GXv_Object6 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P00OZ2(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (bool)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (bool)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (int)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] );
               case 1 :
                     return conditional_P00OZ3(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (bool)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (bool)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (int)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] );
               case 2 :
                     return conditional_P00OZ4(context, (String)dynConstraints[0] , (IGxCollection)dynConstraints[1] , (String)dynConstraints[2] , (String)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (bool)dynConstraints[6] , (String)dynConstraints[7] , (String)dynConstraints[8] , (String)dynConstraints[9] , (String)dynConstraints[10] , (bool)dynConstraints[11] , (String)dynConstraints[12] , (String)dynConstraints[13] , (String)dynConstraints[14] , (String)dynConstraints[15] , (String)dynConstraints[16] , (String)dynConstraints[17] , (String)dynConstraints[18] , (String)dynConstraints[19] , (int)dynConstraints[20] , (String)dynConstraints[21] , (String)dynConstraints[22] , (String)dynConstraints[23] , (String)dynConstraints[24] , (String)dynConstraints[25] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP00OZ2 ;
          prmP00OZ2 = new Object[] {
          new Object[] {"@lV55WWTipodeContaDS_2_Tipodeconta_codigo1",SqlDbType.Char,20,0} ,
          new Object[] {"@lV56WWTipodeContaDS_3_Tipodeconta_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV57WWTipodeContaDS_4_Tipodeconta_contapaicod1",SqlDbType.Char,20,0} ,
          new Object[] {"@lV60WWTipodeContaDS_7_Tipodeconta_codigo2",SqlDbType.Char,20,0} ,
          new Object[] {"@lV61WWTipodeContaDS_8_Tipodeconta_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV62WWTipodeContaDS_9_Tipodeconta_contapaicod2",SqlDbType.Char,20,0} ,
          new Object[] {"@lV65WWTipodeContaDS_12_Tipodeconta_codigo3",SqlDbType.Char,20,0} ,
          new Object[] {"@lV66WWTipodeContaDS_13_Tipodeconta_nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV67WWTipodeContaDS_14_Tipodeconta_contapaicod3",SqlDbType.Char,20,0} ,
          new Object[] {"@lV68WWTipodeContaDS_15_Tftipodeconta_codigo",SqlDbType.Char,20,0} ,
          new Object[] {"@AV69WWTipodeContaDS_16_Tftipodeconta_codigo_sel",SqlDbType.Char,20,0} ,
          new Object[] {"@lV70WWTipodeContaDS_17_Tftipodeconta_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV71WWTipodeContaDS_18_Tftipodeconta_nome_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV73WWTipodeContaDS_20_Tftipodeconta_contapaicod",SqlDbType.Char,20,0} ,
          new Object[] {"@AV74WWTipodeContaDS_21_Tftipodeconta_contapaicod_sel",SqlDbType.Char,20,0}
          } ;
          Object[] prmP00OZ3 ;
          prmP00OZ3 = new Object[] {
          new Object[] {"@lV55WWTipodeContaDS_2_Tipodeconta_codigo1",SqlDbType.Char,20,0} ,
          new Object[] {"@lV56WWTipodeContaDS_3_Tipodeconta_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV57WWTipodeContaDS_4_Tipodeconta_contapaicod1",SqlDbType.Char,20,0} ,
          new Object[] {"@lV60WWTipodeContaDS_7_Tipodeconta_codigo2",SqlDbType.Char,20,0} ,
          new Object[] {"@lV61WWTipodeContaDS_8_Tipodeconta_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV62WWTipodeContaDS_9_Tipodeconta_contapaicod2",SqlDbType.Char,20,0} ,
          new Object[] {"@lV65WWTipodeContaDS_12_Tipodeconta_codigo3",SqlDbType.Char,20,0} ,
          new Object[] {"@lV66WWTipodeContaDS_13_Tipodeconta_nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV67WWTipodeContaDS_14_Tipodeconta_contapaicod3",SqlDbType.Char,20,0} ,
          new Object[] {"@lV68WWTipodeContaDS_15_Tftipodeconta_codigo",SqlDbType.Char,20,0} ,
          new Object[] {"@AV69WWTipodeContaDS_16_Tftipodeconta_codigo_sel",SqlDbType.Char,20,0} ,
          new Object[] {"@lV70WWTipodeContaDS_17_Tftipodeconta_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV71WWTipodeContaDS_18_Tftipodeconta_nome_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV73WWTipodeContaDS_20_Tftipodeconta_contapaicod",SqlDbType.Char,20,0} ,
          new Object[] {"@AV74WWTipodeContaDS_21_Tftipodeconta_contapaicod_sel",SqlDbType.Char,20,0}
          } ;
          Object[] prmP00OZ4 ;
          prmP00OZ4 = new Object[] {
          new Object[] {"@lV55WWTipodeContaDS_2_Tipodeconta_codigo1",SqlDbType.Char,20,0} ,
          new Object[] {"@lV56WWTipodeContaDS_3_Tipodeconta_nome1",SqlDbType.Char,50,0} ,
          new Object[] {"@lV57WWTipodeContaDS_4_Tipodeconta_contapaicod1",SqlDbType.Char,20,0} ,
          new Object[] {"@lV60WWTipodeContaDS_7_Tipodeconta_codigo2",SqlDbType.Char,20,0} ,
          new Object[] {"@lV61WWTipodeContaDS_8_Tipodeconta_nome2",SqlDbType.Char,50,0} ,
          new Object[] {"@lV62WWTipodeContaDS_9_Tipodeconta_contapaicod2",SqlDbType.Char,20,0} ,
          new Object[] {"@lV65WWTipodeContaDS_12_Tipodeconta_codigo3",SqlDbType.Char,20,0} ,
          new Object[] {"@lV66WWTipodeContaDS_13_Tipodeconta_nome3",SqlDbType.Char,50,0} ,
          new Object[] {"@lV67WWTipodeContaDS_14_Tipodeconta_contapaicod3",SqlDbType.Char,20,0} ,
          new Object[] {"@lV68WWTipodeContaDS_15_Tftipodeconta_codigo",SqlDbType.Char,20,0} ,
          new Object[] {"@AV69WWTipodeContaDS_16_Tftipodeconta_codigo_sel",SqlDbType.Char,20,0} ,
          new Object[] {"@lV70WWTipodeContaDS_17_Tftipodeconta_nome",SqlDbType.Char,50,0} ,
          new Object[] {"@AV71WWTipodeContaDS_18_Tftipodeconta_nome_sel",SqlDbType.Char,50,0} ,
          new Object[] {"@lV73WWTipodeContaDS_20_Tftipodeconta_contapaicod",SqlDbType.Char,20,0} ,
          new Object[] {"@AV74WWTipodeContaDS_21_Tftipodeconta_contapaicod_sel",SqlDbType.Char,20,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P00OZ2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00OZ2,100,0,false,false )
             ,new CursorDef("P00OZ3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00OZ3,100,0,true,false )
             ,new CursorDef("P00OZ4", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP00OZ4,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getString(1, 1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 20) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getString(3, 50) ;
                ((String[]) buf[4])[0] = rslt.getString(4, 20) ;
                return;
             case 1 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 1) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 20) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(3);
                ((String[]) buf[4])[0] = rslt.getString(4, 20) ;
                return;
             case 2 :
                ((String[]) buf[0])[0] = rslt.getString(1, 20) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getString(2, 1) ;
                ((String[]) buf[3])[0] = rslt.getString(3, 50) ;
                ((String[]) buf[4])[0] = rslt.getString(4, 20) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                return;
             case 2 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[15]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[16]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[17]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[18]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[19]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[20]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[21]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[22]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[23]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[24]);
                }
                if ( (short)parms[10] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[25]);
                }
                if ( (short)parms[11] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[26]);
                }
                if ( (short)parms[12] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[27]);
                }
                if ( (short)parms[13] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[28]);
                }
                if ( (short)parms[14] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[29]);
                }
                return;
       }
    }

 }

 [ServiceContract(Namespace = "GeneXus.Programs.getwwtipodecontafilterdata_services")]
 [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
 [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
 public class getwwtipodecontafilterdata_services : GxRestService
 {
    protected override bool IntegratedSecurityEnabled
    {
       get {
          return true ;
       }

    }

    protected override GAMSecurityLevel IntegratedSecurityLevel
    {
       get {
          return GAMSecurityLevel.SecurityLow ;
       }

    }

    [OperationContract]
    [WebInvoke(Method =  "POST" ,
    	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
    	ResponseFormat = WebMessageFormat.Json,
    	UriTemplate = "/")]
    public void execute( String DDOName ,
                         String SearchTxt ,
                         String SearchTxtTo ,
                         out String OptionsJson ,
                         out String OptionsDescJson ,
                         out String OptionIndexesJson )
    {
       OptionsJson = "" ;
       OptionsDescJson = "" ;
       OptionIndexesJson = "" ;
       try
       {
          permissionPrefix = "";
          if ( ! IsAuthenticated() )
          {
             return  ;
          }
          if ( ! ProcessHeaders("getwwtipodecontafilterdata") )
          {
             return  ;
          }
          getwwtipodecontafilterdata worker = new getwwtipodecontafilterdata(context) ;
          worker.IsMain = RunAsMain ;
          worker.execute(DDOName,SearchTxt,SearchTxtTo,out OptionsJson,out OptionsDescJson,out OptionIndexesJson );
          worker.cleanup( );
       }
       catch ( Exception e )
       {
          WebException(e);
       }
       finally
       {
          Cleanup();
       }
    }

 }

}
