/*
               File: LogErroBC_BC
        Description: Log Erro BC
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 4/28/2020 23:8:54.76
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class logerrobc_bc : GXHttpHandler, IGxSilentTrn
   {
      public logerrobc_bc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public logerrobc_bc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      protected void INITTRN( )
      {
      }

      public void GetInsDefault( )
      {
         ReadRow47186( ) ;
         standaloneNotModal( ) ;
         InitializeNonKey47186( ) ;
         standaloneModal( ) ;
         AddRow47186( ) ;
         Gx_mode = "INS";
         return  ;
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               Z1668LogErroBCID = A1668LogErroBCID;
               SetMode( "UPD") ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      public bool Reindex( )
      {
         return true ;
      }

      protected void CONFIRM_470( )
      {
         BeforeValidate47186( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls47186( ) ;
            }
            else
            {
               CheckExtendedTable47186( ) ;
               if ( AnyError == 0 )
               {
               }
               CloseExtendedTableCursors47186( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
         }
      }

      protected void ZM47186( short GX_JID )
      {
         if ( ( GX_JID == 2 ) || ( GX_JID == 0 ) )
         {
            Z1680LogErroBCModulo = A1680LogErroBCModulo;
            Z1681LogErroBCData = A1681LogErroBCData;
            Z1682LogErroBCNomeBC = A1682LogErroBCNomeBC;
         }
         if ( GX_JID == -2 )
         {
            Z1668LogErroBCID = A1668LogErroBCID;
            Z1680LogErroBCModulo = A1680LogErroBCModulo;
            Z1681LogErroBCData = A1681LogErroBCData;
            Z1682LogErroBCNomeBC = A1682LogErroBCNomeBC;
            Z1683LogErroBCJsonBC = A1683LogErroBCJsonBC;
            Z1684LogErroBCJasonMessage = A1684LogErroBCJasonMessage;
         }
      }

      protected void standaloneNotModal( )
      {
      }

      protected void standaloneModal( )
      {
      }

      protected void Load47186( )
      {
         /* Using cursor BC00474 */
         pr_default.execute(2, new Object[] {A1668LogErroBCID});
         if ( (pr_default.getStatus(2) != 101) )
         {
            RcdFound186 = 1;
            A1680LogErroBCModulo = BC00474_A1680LogErroBCModulo[0];
            A1681LogErroBCData = BC00474_A1681LogErroBCData[0];
            A1682LogErroBCNomeBC = BC00474_A1682LogErroBCNomeBC[0];
            A1683LogErroBCJsonBC = BC00474_A1683LogErroBCJsonBC[0];
            A1684LogErroBCJasonMessage = BC00474_A1684LogErroBCJasonMessage[0];
            ZM47186( -2) ;
         }
         pr_default.close(2);
         OnLoadActions47186( ) ;
      }

      protected void OnLoadActions47186( )
      {
      }

      protected void CheckExtendedTable47186( )
      {
         standaloneModal( ) ;
         if ( ! ( (DateTime.MinValue==A1681LogErroBCData) || ( A1681LogErroBCData >= context.localUtil.YMDToD( 1753, 1, 1) ) ) )
         {
            GX_msglist.addItem("Campo Log Erro BCData fora do intervalo", "OutOfRange", 1, "");
            AnyError = 1;
         }
      }

      protected void CloseExtendedTableCursors47186( )
      {
      }

      protected void enableDisable( )
      {
      }

      protected void GetKey47186( )
      {
         /* Using cursor BC00475 */
         pr_default.execute(3, new Object[] {A1668LogErroBCID});
         if ( (pr_default.getStatus(3) != 101) )
         {
            RcdFound186 = 1;
         }
         else
         {
            RcdFound186 = 0;
         }
         pr_default.close(3);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor BC00473 */
         pr_default.execute(1, new Object[] {A1668LogErroBCID});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM47186( 2) ;
            RcdFound186 = 1;
            A1668LogErroBCID = BC00473_A1668LogErroBCID[0];
            A1680LogErroBCModulo = BC00473_A1680LogErroBCModulo[0];
            A1681LogErroBCData = BC00473_A1681LogErroBCData[0];
            A1682LogErroBCNomeBC = BC00473_A1682LogErroBCNomeBC[0];
            A1683LogErroBCJsonBC = BC00473_A1683LogErroBCJsonBC[0];
            A1684LogErroBCJasonMessage = BC00473_A1684LogErroBCJasonMessage[0];
            Z1668LogErroBCID = A1668LogErroBCID;
            sMode186 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal( ) ;
            Load47186( ) ;
            if ( AnyError == 1 )
            {
               RcdFound186 = 0;
               InitializeNonKey47186( ) ;
            }
            Gx_mode = sMode186;
         }
         else
         {
            RcdFound186 = 0;
            InitializeNonKey47186( ) ;
            sMode186 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal( ) ;
            Gx_mode = sMode186;
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey47186( ) ;
         if ( RcdFound186 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
         }
         getByPrimaryKey( ) ;
      }

      protected void insert_Check( )
      {
         CONFIRM_470( ) ;
         IsConfirmed = 0;
      }

      protected void update_Check( )
      {
         insert_Check( ) ;
      }

      protected void delete_Check( )
      {
         insert_Check( ) ;
      }

      protected void CheckOptimisticConcurrency47186( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor BC00472 */
            pr_default.execute(0, new Object[] {A1668LogErroBCID});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"LogErroBC"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) || ( StringUtil.StrCmp(Z1680LogErroBCModulo, BC00472_A1680LogErroBCModulo[0]) != 0 ) || ( Z1681LogErroBCData != BC00472_A1681LogErroBCData[0] ) || ( StringUtil.StrCmp(Z1682LogErroBCNomeBC, BC00472_A1682LogErroBCNomeBC[0]) != 0 ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"LogErroBC"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert47186( )
      {
         BeforeValidate47186( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable47186( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM47186( 0) ;
            CheckOptimisticConcurrency47186( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm47186( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert47186( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC00476 */
                     pr_default.execute(4, new Object[] {A1668LogErroBCID, A1680LogErroBCModulo, A1681LogErroBCData, A1682LogErroBCNomeBC, A1683LogErroBCJsonBC, A1684LogErroBCJasonMessage});
                     pr_default.close(4);
                     dsDefault.SmartCacheProvider.SetUpdated("LogErroBC") ;
                     if ( (pr_default.getStatus(4) == 1) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), 0, "", true);
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load47186( ) ;
            }
            EndLevel47186( ) ;
         }
         CloseExtendedTableCursors47186( ) ;
      }

      protected void Update47186( )
      {
         BeforeValidate47186( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable47186( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency47186( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm47186( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate47186( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC00477 */
                     pr_default.execute(5, new Object[] {A1680LogErroBCModulo, A1681LogErroBCData, A1682LogErroBCNomeBC, A1683LogErroBCJsonBC, A1684LogErroBCJasonMessage, A1668LogErroBCID});
                     pr_default.close(5);
                     dsDefault.SmartCacheProvider.SetUpdated("LogErroBC") ;
                     if ( (pr_default.getStatus(5) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"LogErroBC"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate47186( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           getByPrimaryKey( ) ;
                           GX_msglist.addItem(context.GetMessage( "GXM_sucupdated", ""), 0, "", true);
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel47186( ) ;
         }
         CloseExtendedTableCursors47186( ) ;
      }

      protected void DeferredUpdate47186( )
      {
      }

      protected void delete( )
      {
         Gx_mode = "DLT";
         BeforeValidate47186( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency47186( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls47186( ) ;
            AfterConfirm47186( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete47186( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor BC00478 */
                  pr_default.execute(6, new Object[] {A1668LogErroBCID});
                  pr_default.close(6);
                  dsDefault.SmartCacheProvider.SetUpdated("LogErroBC") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_sucdeleted", ""), 0, "", true);
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode186 = Gx_mode;
         Gx_mode = "DLT";
         EndLevel47186( ) ;
         Gx_mode = sMode186;
      }

      protected void OnDeleteControls47186( )
      {
         standaloneModal( ) ;
         /* No delete mode formulas found. */
      }

      protected void EndLevel47186( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete47186( ) ;
         }
         if ( AnyError == 0 )
         {
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanKeyStart47186( )
      {
         /* Using cursor BC00479 */
         pr_default.execute(7, new Object[] {A1668LogErroBCID});
         RcdFound186 = 0;
         if ( (pr_default.getStatus(7) != 101) )
         {
            RcdFound186 = 1;
            A1668LogErroBCID = BC00479_A1668LogErroBCID[0];
            A1680LogErroBCModulo = BC00479_A1680LogErroBCModulo[0];
            A1681LogErroBCData = BC00479_A1681LogErroBCData[0];
            A1682LogErroBCNomeBC = BC00479_A1682LogErroBCNomeBC[0];
            A1683LogErroBCJsonBC = BC00479_A1683LogErroBCJsonBC[0];
            A1684LogErroBCJasonMessage = BC00479_A1684LogErroBCJasonMessage[0];
         }
         /* Load Subordinate Levels */
      }

      protected void ScanKeyNext47186( )
      {
         /* Scan next routine */
         pr_default.readNext(7);
         RcdFound186 = 0;
         ScanKeyLoad47186( ) ;
      }

      protected void ScanKeyLoad47186( )
      {
         sMode186 = Gx_mode;
         Gx_mode = "DSP";
         if ( (pr_default.getStatus(7) != 101) )
         {
            RcdFound186 = 1;
            A1668LogErroBCID = BC00479_A1668LogErroBCID[0];
            A1680LogErroBCModulo = BC00479_A1680LogErroBCModulo[0];
            A1681LogErroBCData = BC00479_A1681LogErroBCData[0];
            A1682LogErroBCNomeBC = BC00479_A1682LogErroBCNomeBC[0];
            A1683LogErroBCJsonBC = BC00479_A1683LogErroBCJsonBC[0];
            A1684LogErroBCJasonMessage = BC00479_A1684LogErroBCJasonMessage[0];
         }
         Gx_mode = sMode186;
      }

      protected void ScanKeyEnd47186( )
      {
         pr_default.close(7);
      }

      protected void AfterConfirm47186( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert47186( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate47186( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete47186( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete47186( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate47186( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes47186( )
      {
      }

      protected void AddRow47186( )
      {
         VarsToRow186( bcLogErroBC) ;
      }

      protected void ReadRow47186( )
      {
         RowToVars186( bcLogErroBC, 1) ;
      }

      protected void InitializeNonKey47186( )
      {
         A1680LogErroBCModulo = "";
         A1681LogErroBCData = DateTime.MinValue;
         A1682LogErroBCNomeBC = "";
         A1683LogErroBCJsonBC = "";
         A1684LogErroBCJasonMessage = "";
         Z1680LogErroBCModulo = "";
         Z1681LogErroBCData = DateTime.MinValue;
         Z1682LogErroBCNomeBC = "";
      }

      protected void InitAll47186( )
      {
         A1668LogErroBCID = 0;
         InitializeNonKey47186( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      public void VarsToRow186( SdtLogErroBC obj186 )
      {
         obj186.gxTpr_Mode = Gx_mode;
         obj186.gxTpr_Logerrobcmodulo = A1680LogErroBCModulo;
         obj186.gxTpr_Logerrobcdata = A1681LogErroBCData;
         obj186.gxTpr_Logerrobcnomebc = A1682LogErroBCNomeBC;
         obj186.gxTpr_Logerrobcjsonbc = A1683LogErroBCJsonBC;
         obj186.gxTpr_Logerrobcjasonmessage = A1684LogErroBCJasonMessage;
         obj186.gxTpr_Logerrobcid = A1668LogErroBCID;
         obj186.gxTpr_Logerrobcid_Z = Z1668LogErroBCID;
         obj186.gxTpr_Logerrobcmodulo_Z = Z1680LogErroBCModulo;
         obj186.gxTpr_Logerrobcdata_Z = Z1681LogErroBCData;
         obj186.gxTpr_Logerrobcnomebc_Z = Z1682LogErroBCNomeBC;
         obj186.gxTpr_Mode = Gx_mode;
         return  ;
      }

      public void KeyVarsToRow186( SdtLogErroBC obj186 )
      {
         obj186.gxTpr_Logerrobcid = A1668LogErroBCID;
         return  ;
      }

      public void RowToVars186( SdtLogErroBC obj186 ,
                                int forceLoad )
      {
         Gx_mode = obj186.gxTpr_Mode;
         A1680LogErroBCModulo = obj186.gxTpr_Logerrobcmodulo;
         A1681LogErroBCData = obj186.gxTpr_Logerrobcdata;
         A1682LogErroBCNomeBC = obj186.gxTpr_Logerrobcnomebc;
         A1683LogErroBCJsonBC = obj186.gxTpr_Logerrobcjsonbc;
         A1684LogErroBCJasonMessage = obj186.gxTpr_Logerrobcjasonmessage;
         A1668LogErroBCID = obj186.gxTpr_Logerrobcid;
         Z1668LogErroBCID = obj186.gxTpr_Logerrobcid_Z;
         Z1680LogErroBCModulo = obj186.gxTpr_Logerrobcmodulo_Z;
         Z1681LogErroBCData = obj186.gxTpr_Logerrobcdata_Z;
         Z1682LogErroBCNomeBC = obj186.gxTpr_Logerrobcnomebc_Z;
         Gx_mode = obj186.gxTpr_Mode;
         return  ;
      }

      public void LoadKey( Object[] obj )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         A1668LogErroBCID = (long)getParm(obj,0);
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         InitializeNonKey47186( ) ;
         ScanKeyStart47186( ) ;
         if ( RcdFound186 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
            Z1668LogErroBCID = A1668LogErroBCID;
         }
         ZM47186( -2) ;
         OnLoadActions47186( ) ;
         AddRow47186( ) ;
         ScanKeyEnd47186( ) ;
         if ( RcdFound186 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "");
            AnyError = 1;
         }
         context.GX_msglist = BackMsgLst;
      }

      public void Load( )
      {
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         RowToVars186( bcLogErroBC, 0) ;
         ScanKeyStart47186( ) ;
         if ( RcdFound186 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
            Z1668LogErroBCID = A1668LogErroBCID;
         }
         ZM47186( -2) ;
         OnLoadActions47186( ) ;
         AddRow47186( ) ;
         ScanKeyEnd47186( ) ;
         if ( RcdFound186 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "");
            AnyError = 1;
         }
         context.GX_msglist = BackMsgLst;
      }

      public void Save( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         IsConfirmed = 1;
         RowToVars186( bcLogErroBC, 0) ;
         nKeyPressed = 1;
         GetKey47186( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            Insert47186( ) ;
         }
         else
         {
            if ( RcdFound186 == 1 )
            {
               if ( A1668LogErroBCID != Z1668LogErroBCID )
               {
                  A1668LogErroBCID = Z1668LogErroBCID;
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
               }
               else
               {
                  Gx_mode = "UPD";
                  /* Update record */
                  Update47186( ) ;
               }
            }
            else
            {
               if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else
               {
                  if ( A1668LogErroBCID != Z1668LogErroBCID )
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     else
                     {
                        Gx_mode = "INS";
                        /* Insert record */
                        Insert47186( ) ;
                     }
                  }
                  else
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                        AnyError = 1;
                     }
                     else
                     {
                        Gx_mode = "INS";
                        /* Insert record */
                        Insert47186( ) ;
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         VarsToRow186( bcLogErroBC) ;
         context.GX_msglist = BackMsgLst;
         return  ;
      }

      public void Check( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         RowToVars186( bcLogErroBC, 0) ;
         nKeyPressed = 3;
         IsConfirmed = 0;
         GetKey47186( ) ;
         if ( RcdFound186 == 1 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
               AnyError = 1;
            }
            else if ( A1668LogErroBCID != Z1668LogErroBCID )
            {
               A1668LogErroBCID = Z1668LogErroBCID;
               GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "DuplicatePrimaryKey", 1, "");
               AnyError = 1;
            }
            else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               delete_Check( ) ;
            }
            else
            {
               Gx_mode = "UPD";
               update_Check( ) ;
            }
         }
         else
         {
            if ( A1668LogErroBCID != Z1668LogErroBCID )
            {
               Gx_mode = "INS";
               insert_Check( ) ;
            }
            else
            {
               if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                  AnyError = 1;
               }
               else
               {
                  Gx_mode = "INS";
                  insert_Check( ) ;
               }
            }
         }
         pr_default.close(1);
         context.RollbackDataStores( "LogErroBC_BC");
         VarsToRow186( bcLogErroBC) ;
         context.GX_msglist = BackMsgLst;
         return  ;
      }

      public int Errors( )
      {
         if ( AnyError == 0 )
         {
            return (int)(0) ;
         }
         return (int)(1) ;
      }

      public msglist GetMessages( )
      {
         return LclMsgLst ;
      }

      public String GetMode( )
      {
         Gx_mode = bcLogErroBC.gxTpr_Mode;
         return Gx_mode ;
      }

      public void SetMode( String lMode )
      {
         Gx_mode = lMode;
         bcLogErroBC.gxTpr_Mode = Gx_mode;
         return  ;
      }

      public void SetSDT( GxSilentTrnSdt sdt ,
                          short sdtToBc )
      {
         if ( sdt != bcLogErroBC )
         {
            bcLogErroBC = (SdtLogErroBC)(sdt);
            if ( StringUtil.StrCmp(bcLogErroBC.gxTpr_Mode, "") == 0 )
            {
               bcLogErroBC.gxTpr_Mode = "INS";
            }
            if ( sdtToBc == 1 )
            {
               VarsToRow186( bcLogErroBC) ;
            }
            else
            {
               RowToVars186( bcLogErroBC, 1) ;
            }
         }
         else
         {
            if ( StringUtil.StrCmp(bcLogErroBC.gxTpr_Mode, "") == 0 )
            {
               bcLogErroBC.gxTpr_Mode = "INS";
            }
         }
         return  ;
      }

      public void ReloadFromSDT( )
      {
         RowToVars186( bcLogErroBC, 1) ;
         return  ;
      }

      public SdtLogErroBC LogErroBC_BC
      {
         get {
            return bcLogErroBC ;
         }

      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         createObjects();
         initialize();
      }

      protected override void createObjects( )
      {
      }

      protected void Process( )
      {
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
      }

      public override void initialize( )
      {
         scmdbuf = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Gx_mode = "";
         Z1680LogErroBCModulo = "";
         A1680LogErroBCModulo = "";
         Z1681LogErroBCData = DateTime.MinValue;
         A1681LogErroBCData = DateTime.MinValue;
         Z1682LogErroBCNomeBC = "";
         A1682LogErroBCNomeBC = "";
         Z1683LogErroBCJsonBC = "";
         A1683LogErroBCJsonBC = "";
         Z1684LogErroBCJasonMessage = "";
         A1684LogErroBCJasonMessage = "";
         BC00474_A1668LogErroBCID = new long[1] ;
         BC00474_A1680LogErroBCModulo = new String[] {""} ;
         BC00474_A1681LogErroBCData = new DateTime[] {DateTime.MinValue} ;
         BC00474_A1682LogErroBCNomeBC = new String[] {""} ;
         BC00474_A1683LogErroBCJsonBC = new String[] {""} ;
         BC00474_A1684LogErroBCJasonMessage = new String[] {""} ;
         BC00475_A1668LogErroBCID = new long[1] ;
         BC00473_A1668LogErroBCID = new long[1] ;
         BC00473_A1680LogErroBCModulo = new String[] {""} ;
         BC00473_A1681LogErroBCData = new DateTime[] {DateTime.MinValue} ;
         BC00473_A1682LogErroBCNomeBC = new String[] {""} ;
         BC00473_A1683LogErroBCJsonBC = new String[] {""} ;
         BC00473_A1684LogErroBCJasonMessage = new String[] {""} ;
         sMode186 = "";
         BC00472_A1668LogErroBCID = new long[1] ;
         BC00472_A1680LogErroBCModulo = new String[] {""} ;
         BC00472_A1681LogErroBCData = new DateTime[] {DateTime.MinValue} ;
         BC00472_A1682LogErroBCNomeBC = new String[] {""} ;
         BC00472_A1683LogErroBCJsonBC = new String[] {""} ;
         BC00472_A1684LogErroBCJasonMessage = new String[] {""} ;
         BC00479_A1668LogErroBCID = new long[1] ;
         BC00479_A1680LogErroBCModulo = new String[] {""} ;
         BC00479_A1681LogErroBCData = new DateTime[] {DateTime.MinValue} ;
         BC00479_A1682LogErroBCNomeBC = new String[] {""} ;
         BC00479_A1683LogErroBCJsonBC = new String[] {""} ;
         BC00479_A1684LogErroBCJasonMessage = new String[] {""} ;
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.logerrobc_bc__default(),
            new Object[][] {
                new Object[] {
               BC00472_A1668LogErroBCID, BC00472_A1680LogErroBCModulo, BC00472_A1681LogErroBCData, BC00472_A1682LogErroBCNomeBC, BC00472_A1683LogErroBCJsonBC, BC00472_A1684LogErroBCJasonMessage
               }
               , new Object[] {
               BC00473_A1668LogErroBCID, BC00473_A1680LogErroBCModulo, BC00473_A1681LogErroBCData, BC00473_A1682LogErroBCNomeBC, BC00473_A1683LogErroBCJsonBC, BC00473_A1684LogErroBCJasonMessage
               }
               , new Object[] {
               BC00474_A1668LogErroBCID, BC00474_A1680LogErroBCModulo, BC00474_A1681LogErroBCData, BC00474_A1682LogErroBCNomeBC, BC00474_A1683LogErroBCJsonBC, BC00474_A1684LogErroBCJasonMessage
               }
               , new Object[] {
               BC00475_A1668LogErroBCID
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               BC00479_A1668LogErroBCID, BC00479_A1680LogErroBCModulo, BC00479_A1681LogErroBCData, BC00479_A1682LogErroBCNomeBC, BC00479_A1683LogErroBCJsonBC, BC00479_A1684LogErroBCJasonMessage
               }
            }
         );
         INITTRN();
         /* Execute Start event if defined. */
         standaloneNotModal( ) ;
      }

      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short GX_JID ;
      private short RcdFound186 ;
      private int trnEnded ;
      private long Z1668LogErroBCID ;
      private long A1668LogErroBCID ;
      private String scmdbuf ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String Gx_mode ;
      private String sMode186 ;
      private DateTime Z1681LogErroBCData ;
      private DateTime A1681LogErroBCData ;
      private String Z1683LogErroBCJsonBC ;
      private String A1683LogErroBCJsonBC ;
      private String Z1684LogErroBCJasonMessage ;
      private String A1684LogErroBCJasonMessage ;
      private String Z1680LogErroBCModulo ;
      private String A1680LogErroBCModulo ;
      private String Z1682LogErroBCNomeBC ;
      private String A1682LogErroBCNomeBC ;
      private SdtLogErroBC bcLogErroBC ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private long[] BC00474_A1668LogErroBCID ;
      private String[] BC00474_A1680LogErroBCModulo ;
      private DateTime[] BC00474_A1681LogErroBCData ;
      private String[] BC00474_A1682LogErroBCNomeBC ;
      private String[] BC00474_A1683LogErroBCJsonBC ;
      private String[] BC00474_A1684LogErroBCJasonMessage ;
      private long[] BC00475_A1668LogErroBCID ;
      private long[] BC00473_A1668LogErroBCID ;
      private String[] BC00473_A1680LogErroBCModulo ;
      private DateTime[] BC00473_A1681LogErroBCData ;
      private String[] BC00473_A1682LogErroBCNomeBC ;
      private String[] BC00473_A1683LogErroBCJsonBC ;
      private String[] BC00473_A1684LogErroBCJasonMessage ;
      private long[] BC00472_A1668LogErroBCID ;
      private String[] BC00472_A1680LogErroBCModulo ;
      private DateTime[] BC00472_A1681LogErroBCData ;
      private String[] BC00472_A1682LogErroBCNomeBC ;
      private String[] BC00472_A1683LogErroBCJsonBC ;
      private String[] BC00472_A1684LogErroBCJasonMessage ;
      private long[] BC00479_A1668LogErroBCID ;
      private String[] BC00479_A1680LogErroBCModulo ;
      private DateTime[] BC00479_A1681LogErroBCData ;
      private String[] BC00479_A1682LogErroBCNomeBC ;
      private String[] BC00479_A1683LogErroBCJsonBC ;
      private String[] BC00479_A1684LogErroBCJasonMessage ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
   }

   public class logerrobc_bc__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new UpdateCursor(def[4])
         ,new UpdateCursor(def[5])
         ,new UpdateCursor(def[6])
         ,new ForEachCursor(def[7])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmBC00474 ;
          prmBC00474 = new Object[] {
          new Object[] {"@LogErroBCID",SqlDbType.Decimal,18,0}
          } ;
          Object[] prmBC00475 ;
          prmBC00475 = new Object[] {
          new Object[] {"@LogErroBCID",SqlDbType.Decimal,18,0}
          } ;
          Object[] prmBC00473 ;
          prmBC00473 = new Object[] {
          new Object[] {"@LogErroBCID",SqlDbType.Decimal,18,0}
          } ;
          Object[] prmBC00472 ;
          prmBC00472 = new Object[] {
          new Object[] {"@LogErroBCID",SqlDbType.Decimal,18,0}
          } ;
          Object[] prmBC00476 ;
          prmBC00476 = new Object[] {
          new Object[] {"@LogErroBCID",SqlDbType.Decimal,18,0} ,
          new Object[] {"@LogErroBCModulo",SqlDbType.VarChar,40,0} ,
          new Object[] {"@LogErroBCData",SqlDbType.DateTime,8,0} ,
          new Object[] {"@LogErroBCNomeBC",SqlDbType.VarChar,40,0} ,
          new Object[] {"@LogErroBCJsonBC",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@LogErroBCJasonMessage",SqlDbType.VarChar,2097152,0}
          } ;
          Object[] prmBC00477 ;
          prmBC00477 = new Object[] {
          new Object[] {"@LogErroBCModulo",SqlDbType.VarChar,40,0} ,
          new Object[] {"@LogErroBCData",SqlDbType.DateTime,8,0} ,
          new Object[] {"@LogErroBCNomeBC",SqlDbType.VarChar,40,0} ,
          new Object[] {"@LogErroBCJsonBC",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@LogErroBCJasonMessage",SqlDbType.VarChar,2097152,0} ,
          new Object[] {"@LogErroBCID",SqlDbType.Decimal,18,0}
          } ;
          Object[] prmBC00478 ;
          prmBC00478 = new Object[] {
          new Object[] {"@LogErroBCID",SqlDbType.Decimal,18,0}
          } ;
          Object[] prmBC00479 ;
          prmBC00479 = new Object[] {
          new Object[] {"@LogErroBCID",SqlDbType.Decimal,18,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("BC00472", "SELECT [LogErroBCID], [LogErroBCModulo], [LogErroBCData], [LogErroBCNomeBC], [LogErroBCJsonBC], [LogErroBCJasonMessage] FROM [LogErroBC] WITH (UPDLOCK) WHERE [LogErroBCID] = @LogErroBCID ",true, GxErrorMask.GX_NOMASK, false, this,prmBC00472,1,0,true,false )
             ,new CursorDef("BC00473", "SELECT [LogErroBCID], [LogErroBCModulo], [LogErroBCData], [LogErroBCNomeBC], [LogErroBCJsonBC], [LogErroBCJasonMessage] FROM [LogErroBC] WITH (NOLOCK) WHERE [LogErroBCID] = @LogErroBCID ",true, GxErrorMask.GX_NOMASK, false, this,prmBC00473,1,0,true,false )
             ,new CursorDef("BC00474", "SELECT TM1.[LogErroBCID], TM1.[LogErroBCModulo], TM1.[LogErroBCData], TM1.[LogErroBCNomeBC], TM1.[LogErroBCJsonBC], TM1.[LogErroBCJasonMessage] FROM [LogErroBC] TM1 WITH (NOLOCK) WHERE TM1.[LogErroBCID] = @LogErroBCID ORDER BY TM1.[LogErroBCID]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmBC00474,100,0,true,false )
             ,new CursorDef("BC00475", "SELECT [LogErroBCID] FROM [LogErroBC] WITH (NOLOCK) WHERE [LogErroBCID] = @LogErroBCID  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmBC00475,1,0,true,false )
             ,new CursorDef("BC00476", "INSERT INTO [LogErroBC]([LogErroBCID], [LogErroBCModulo], [LogErroBCData], [LogErroBCNomeBC], [LogErroBCJsonBC], [LogErroBCJasonMessage]) VALUES(@LogErroBCID, @LogErroBCModulo, @LogErroBCData, @LogErroBCNomeBC, @LogErroBCJsonBC, @LogErroBCJasonMessage)", GxErrorMask.GX_NOMASK,prmBC00476)
             ,new CursorDef("BC00477", "UPDATE [LogErroBC] SET [LogErroBCModulo]=@LogErroBCModulo, [LogErroBCData]=@LogErroBCData, [LogErroBCNomeBC]=@LogErroBCNomeBC, [LogErroBCJsonBC]=@LogErroBCJsonBC, [LogErroBCJasonMessage]=@LogErroBCJasonMessage  WHERE [LogErroBCID] = @LogErroBCID", GxErrorMask.GX_NOMASK,prmBC00477)
             ,new CursorDef("BC00478", "DELETE FROM [LogErroBC]  WHERE [LogErroBCID] = @LogErroBCID", GxErrorMask.GX_NOMASK,prmBC00478)
             ,new CursorDef("BC00479", "SELECT TM1.[LogErroBCID], TM1.[LogErroBCModulo], TM1.[LogErroBCData], TM1.[LogErroBCNomeBC], TM1.[LogErroBCJsonBC], TM1.[LogErroBCJasonMessage] FROM [LogErroBC] TM1 WITH (NOLOCK) WHERE TM1.[LogErroBCID] = @LogErroBCID ORDER BY TM1.[LogErroBCID]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmBC00479,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((DateTime[]) buf[2])[0] = rslt.getGXDate(3) ;
                ((String[]) buf[3])[0] = rslt.getVarchar(4) ;
                ((String[]) buf[4])[0] = rslt.getLongVarchar(5) ;
                ((String[]) buf[5])[0] = rslt.getLongVarchar(6) ;
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((DateTime[]) buf[2])[0] = rslt.getGXDate(3) ;
                ((String[]) buf[3])[0] = rslt.getVarchar(4) ;
                ((String[]) buf[4])[0] = rslt.getLongVarchar(5) ;
                ((String[]) buf[5])[0] = rslt.getLongVarchar(6) ;
                return;
             case 2 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((DateTime[]) buf[2])[0] = rslt.getGXDate(3) ;
                ((String[]) buf[3])[0] = rslt.getVarchar(4) ;
                ((String[]) buf[4])[0] = rslt.getLongVarchar(5) ;
                ((String[]) buf[5])[0] = rslt.getLongVarchar(6) ;
                return;
             case 3 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
             case 7 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((DateTime[]) buf[2])[0] = rslt.getGXDate(3) ;
                ((String[]) buf[3])[0] = rslt.getVarchar(4) ;
                ((String[]) buf[4])[0] = rslt.getLongVarchar(5) ;
                ((String[]) buf[5])[0] = rslt.getLongVarchar(6) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (long)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (long)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (long)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (long)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (long)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                stmt.SetParameter(3, (DateTime)parms[2]);
                stmt.SetParameter(4, (String)parms[3]);
                stmt.SetParameter(5, (String)parms[4]);
                stmt.SetParameter(6, (String)parms[5]);
                return;
             case 5 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (DateTime)parms[1]);
                stmt.SetParameter(3, (String)parms[2]);
                stmt.SetParameter(4, (String)parms[3]);
                stmt.SetParameter(5, (String)parms[4]);
                stmt.SetParameter(6, (long)parms[5]);
                return;
             case 6 :
                stmt.SetParameter(1, (long)parms[0]);
                return;
             case 7 :
                stmt.SetParameter(1, (long)parms[0]);
                return;
       }
    }

 }

}
