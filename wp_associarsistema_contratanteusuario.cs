/*
               File: WP_AssociarSistema_ContratanteUsuario
        Description: Associar
             Author: GeneXus C# Generator version 10_3_14-114418
       Generated on: 5/29/2020 9:43:51.41
       Program type: Callable routine
          Main DBMS: sqlserver
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.Data.SqlTypes;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wp_associarsistema_contratanteusuario : GXHttpHandler, System.Web.SessionState.IRequiresSessionState
   {
      public wp_associarsistema_contratanteusuario( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
      }

      public wp_associarsistema_contratanteusuario( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( int aP0_Sistema_Codigo ,
                           int aP1_Contratante_Codigo )
      {
         this.AV25Sistema_Codigo = aP0_Sistema_Codigo;
         this.AV29Contratante_Codigo = aP1_Contratante_Codigo;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         lstavNotassociatedrecords = new GXListbox();
         lstavAssociatedrecords = new GXListbox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               AV25Sistema_Codigo = (int)(NumberUtil.Val( gxfirstwebparm, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV25Sistema_Codigo), 6, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vSISTEMA_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV25Sistema_Codigo), "ZZZZZ9")));
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
               {
                  AV29Contratante_Codigo = (int)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29Contratante_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV29Contratante_Codigo), 6, 0)));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTRATANTE_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV29Contratante_Codigo), "ZZZZZ9")));
               }
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityLow ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            ValidateSpaRequest();
            PAMZ2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               context.Gx_err = 0;
               edtavSistema_sigla_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavSistema_sigla_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavSistema_sigla_Enabled), 5, 0)));
               WSMZ2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  WEMZ2( ) ;
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( "Associar") ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("gxcfg.js", "?20205299435150");
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body") ;
         bodyStyle = "";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" novalidate action=\""+formatLink("wp_associarsistema_contratanteusuario.aspx") + "?" + UrlEncode("" +AV25Sistema_Codigo) + "," + UrlEncode("" +AV29Contratante_Codigo)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vADDEDKEYLIST", AV7AddedKeyList);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vADDEDKEYLIST", AV7AddedKeyList);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vADDEDDSCLIST", AV5AddedDscList);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vADDEDDSCLIST", AV5AddedDscList);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vNOTADDEDKEYLIST", AV22NotAddedKeyList);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vNOTADDEDKEYLIST", AV22NotAddedKeyList);
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vNOTADDEDDSCLIST", AV20NotAddedDscList);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vNOTADDEDDSCLIST", AV20NotAddedDscList);
         }
         GxWebStd.gx_hidden_field( context, "CONTRATANTEUSUARIO_CONTRATANTECOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A63ContratanteUsuario_ContratanteCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vCONTRATANTE_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV29Contratante_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "CONTRATANTEUSUARIO_USUARIOCOD", StringUtil.LTrim( StringUtil.NToC( (decimal)(A60ContratanteUsuario_UsuarioCod), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "SISTEMA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A127Sistema_Codigo), 6, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vSISTEMA_CODIGO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV25Sistema_Codigo), 6, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCONTRATANTEUSUARIOSISTEMA", AV12ContratanteUsuarioSistema);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCONTRATANTEUSUARIOSISTEMA", AV12ContratanteUsuarioSistema);
         }
         GxWebStd.gx_hidden_field( context, "gxhash_vSISTEMA_SIGLA", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV30Sistema_Sigla, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vSISTEMA_SIGLA", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV30Sistema_Sigla, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vSISTEMA_SIGLA", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV30Sistema_Sigla, "@!"))));
         GxWebStd.gx_hidden_field( context, "gxhash_vSISTEMA_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV25Sistema_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vCONTRATANTE_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV29Contratante_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vSISTEMA_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV25Sistema_Codigo), "ZZZZZ9")));
         GxWebStd.gx_hidden_field( context, "gxhash_vCONTRATANTE_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV29Contratante_Codigo), "ZZZZZ9")));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void RenderHtmlCloseFormMZ2( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
         context.WriteHtmlTextNl( "</body>") ;
         context.WriteHtmlTextNl( "</html>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
      }

      public override String GetPgmname( )
      {
         return "WP_AssociarSistema_ContratanteUsuario" ;
      }

      public override String GetPgmdesc( )
      {
         return "Associar" ;
      }

      protected void WBMZ0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            RenderHtmlHeaders( ) ;
            RenderHtmlOpenForm( ) ;
            wb_table1_2_MZ2( true) ;
         }
         else
         {
            wb_table1_2_MZ2( false) ;
         }
         return  ;
      }

      protected void wb_table1_2_MZ2e( bool wbgen )
      {
         if ( wbgen )
         {
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 50,'',false,'',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavAddedkeylistxml_Internalname, AV8AddedKeyListXml, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,50);\"", 0, edtavAddedkeylistxml_Visible, 1, 0, 80, "chr", 10, "row", StyleString, ClassString, "", "2097152", -1, "", "", -1, true, "", "HLP_WP_AssociarSistema_ContratanteUsuario.htm");
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 51,'',false,'',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavNotaddedkeylistxml_Internalname, AV23NotAddedKeyListXml, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,51);\"", 0, edtavNotaddedkeylistxml_Visible, 1, 0, 80, "chr", 10, "row", StyleString, ClassString, "", "2097152", -1, "", "", -1, true, "", "HLP_WP_AssociarSistema_ContratanteUsuario.htm");
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 52,'',false,'',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavAddeddsclistxml_Internalname, AV6AddedDscListXml, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,52);\"", 0, edtavAddeddsclistxml_Visible, 1, 0, 80, "chr", 10, "row", StyleString, ClassString, "", "2097152", -1, "", "", -1, true, "", "HLP_WP_AssociarSistema_ContratanteUsuario.htm");
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 53,'',false,'',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavNotaddeddsclistxml_Internalname, AV21NotAddedDscListXml, "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,53);\"", 0, edtavNotaddeddsclistxml_Visible, 1, 0, 80, "chr", 10, "row", StyleString, ClassString, "", "2097152", -1, "", "", -1, true, "", "HLP_WP_AssociarSistema_ContratanteUsuario.htm");
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbjava_Internalname, lblTbjava_Caption, "", "", lblTbjava_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", lblTbjava_Visible, 1, 1, "HLP_WP_AssociarSistema_ContratanteUsuario.htm");
         }
         wbLoad = true;
      }

      protected void STARTMZ2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 10_3_14-114418", 0) ;
            Form.Meta.addItem("description", "Associar", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUPMZ0( ) ;
      }

      protected void WSMZ2( )
      {
         STARTMZ2( ) ;
         EVTMZ2( ) ;
      }

      protected void EVTMZ2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E11MZ2 */
                           E11MZ2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E12MZ2 */
                           E12MZ2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           if ( ! wbErr )
                           {
                              Rfr0gs = false;
                              if ( ! Rfr0gs )
                              {
                                 /* Execute user event: E13MZ2 */
                                 E13MZ2 ();
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'DISASSOCIATE SELECTED'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E14MZ2 */
                           E14MZ2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'ASSOCIATE SELECTED'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E15MZ2 */
                           E15MZ2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'ASSOCIATE ALL'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E16MZ2 */
                           E16MZ2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'DISASSOCIATE ALL'") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E17MZ2 */
                           E17MZ2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VASSOCIATEDRECORDS.DBLCLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E18MZ2 */
                           E18MZ2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VNOTASSOCIATEDRECORDS.DBLCLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E19MZ2 */
                           E19MZ2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E20MZ2 */
                           E20MZ2 ();
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VASSOCIATEDRECORDS.DBLCLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E18MZ2 */
                           E18MZ2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "VNOTASSOCIATEDRECORDS.DBLCLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: E19MZ2 */
                           E19MZ2 ();
                           dynload_actions( ) ;
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void WEMZ2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseFormMZ2( ) ;
            }
         }
      }

      protected void PAMZ2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            lstavNotassociatedrecords.Name = "vNOTASSOCIATEDRECORDS";
            lstavNotassociatedrecords.WebTags = "";
            lstavNotassociatedrecords.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 6, 0)), "Nenhum", 0);
            if ( lstavNotassociatedrecords.ItemCount > 0 )
            {
               AV24NotAssociatedRecords = (int)(NumberUtil.Val( lstavNotassociatedrecords.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV24NotAssociatedRecords), 6, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24NotAssociatedRecords", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24NotAssociatedRecords), 6, 0)));
            }
            lstavAssociatedrecords.Name = "vASSOCIATEDRECORDS";
            lstavAssociatedrecords.WebTags = "";
            lstavAssociatedrecords.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 6, 0)), "Nenhum", 0);
            if ( lstavAssociatedrecords.ItemCount > 0 )
            {
               AV9AssociatedRecords = (int)(NumberUtil.Val( lstavAssociatedrecords.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV9AssociatedRecords), 6, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9AssociatedRecords", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9AssociatedRecords), 6, 0)));
            }
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = edtavSistema_sigla_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
         if ( lstavNotassociatedrecords.ItemCount > 0 )
         {
            AV24NotAssociatedRecords = (int)(NumberUtil.Val( lstavNotassociatedrecords.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV24NotAssociatedRecords), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24NotAssociatedRecords", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24NotAssociatedRecords), 6, 0)));
         }
         if ( lstavAssociatedrecords.ItemCount > 0 )
         {
            AV9AssociatedRecords = (int)(NumberUtil.Val( lstavAssociatedrecords.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV9AssociatedRecords), 6, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9AssociatedRecords", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9AssociatedRecords), 6, 0)));
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RFMZ2( ) ;
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
         edtavSistema_sigla_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavSistema_sigla_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavSistema_sigla_Enabled), 5, 0)));
      }

      protected void RFMZ2( )
      {
         initialize_formulas( ) ;
         /* Execute user event: E12MZ2 */
         E12MZ2 ();
         fix_multi_value_controls( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.wjLoc)) && ( context.nUserReturn != 1 ) )
         {
            /* Execute user event: E20MZ2 */
            E20MZ2 ();
            WBMZ0( ) ;
         }
      }

      protected void STRUPMZ0( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         edtavSistema_sigla_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavSistema_sigla_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavSistema_sigla_Enabled), 5, 0)));
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: E11MZ2 */
         E11MZ2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            AV30Sistema_Sigla = StringUtil.Upper( cgiGet( edtavSistema_sigla_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30Sistema_Sigla", AV30Sistema_Sigla);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vSISTEMA_SIGLA", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV30Sistema_Sigla, "@!"))));
            lstavNotassociatedrecords.CurrentValue = cgiGet( lstavNotassociatedrecords_Internalname);
            AV24NotAssociatedRecords = (int)(NumberUtil.Val( cgiGet( lstavNotassociatedrecords_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24NotAssociatedRecords", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24NotAssociatedRecords), 6, 0)));
            lstavAssociatedrecords.CurrentValue = cgiGet( lstavAssociatedrecords_Internalname);
            AV9AssociatedRecords = (int)(NumberUtil.Val( cgiGet( lstavAssociatedrecords_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9AssociatedRecords", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9AssociatedRecords), 6, 0)));
            AV8AddedKeyListXml = cgiGet( edtavAddedkeylistxml_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8AddedKeyListXml", AV8AddedKeyListXml);
            AV23NotAddedKeyListXml = cgiGet( edtavNotaddedkeylistxml_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23NotAddedKeyListXml", AV23NotAddedKeyListXml);
            AV6AddedDscListXml = cgiGet( edtavAddeddsclistxml_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6AddedDscListXml", AV6AddedDscListXml);
            AV21NotAddedDscListXml = cgiGet( edtavNotaddeddsclistxml_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21NotAddedDscListXml", AV21NotAddedDscListXml);
            /* Read saved values. */
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: E11MZ2 */
         E11MZ2 ();
         if (returnInSub) return;
      }

      protected void E11MZ2( )
      {
         /* Start Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV27WWPContext) ;
         if ( StringUtil.StrCmp(AV15HTTPRequest.Method, "GET") == 0 )
         {
            AV33GXLvl6 = 0;
            /* Using cursor H00MZ2 */
            pr_default.execute(0, new Object[] {AV25Sistema_Codigo});
            while ( (pr_default.getStatus(0) != 101) )
            {
               A127Sistema_Codigo = H00MZ2_A127Sistema_Codigo[0];
               A129Sistema_Sigla = H00MZ2_A129Sistema_Sigla[0];
               AV33GXLvl6 = 1;
               AV30Sistema_Sigla = A129Sistema_Sigla;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30Sistema_Sigla", AV30Sistema_Sigla);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vSISTEMA_SIGLA", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV30Sistema_Sigla, "@!"))));
               /* Exit For each command. Update data (if necessary), close cursors & exit. */
               if (true) break;
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(0);
            if ( AV33GXLvl6 == 0 )
            {
               GX_msglist.addItem("Registro n�o encontrado.");
            }
            /* Using cursor H00MZ3 */
            pr_default.execute(1, new Object[] {AV29Contratante_Codigo});
            while ( (pr_default.getStatus(1) != 101) )
            {
               A61ContratanteUsuario_UsuarioPessoaCod = H00MZ3_A61ContratanteUsuario_UsuarioPessoaCod[0];
               n61ContratanteUsuario_UsuarioPessoaCod = H00MZ3_n61ContratanteUsuario_UsuarioPessoaCod[0];
               A63ContratanteUsuario_ContratanteCod = H00MZ3_A63ContratanteUsuario_ContratanteCod[0];
               A60ContratanteUsuario_UsuarioCod = H00MZ3_A60ContratanteUsuario_UsuarioCod[0];
               A62ContratanteUsuario_UsuarioPessoaNom = H00MZ3_A62ContratanteUsuario_UsuarioPessoaNom[0];
               n62ContratanteUsuario_UsuarioPessoaNom = H00MZ3_n62ContratanteUsuario_UsuarioPessoaNom[0];
               A61ContratanteUsuario_UsuarioPessoaCod = H00MZ3_A61ContratanteUsuario_UsuarioPessoaCod[0];
               n61ContratanteUsuario_UsuarioPessoaCod = H00MZ3_n61ContratanteUsuario_UsuarioPessoaCod[0];
               A62ContratanteUsuario_UsuarioPessoaNom = H00MZ3_A62ContratanteUsuario_UsuarioPessoaNom[0];
               n62ContratanteUsuario_UsuarioPessoaNom = H00MZ3_n62ContratanteUsuario_UsuarioPessoaNom[0];
               AV14Exist = false;
               /* Using cursor H00MZ4 */
               pr_default.execute(2, new Object[] {A63ContratanteUsuario_ContratanteCod, A60ContratanteUsuario_UsuarioCod, AV25Sistema_Codigo});
               while ( (pr_default.getStatus(2) != 101) )
               {
                  A127Sistema_Codigo = H00MZ4_A127Sistema_Codigo[0];
                  AV14Exist = true;
                  /* Exiting from a For First loop. */
                  if (true) break;
               }
               pr_default.close(2);
               AV13Description = A62ContratanteUsuario_UsuarioPessoaNom;
               if ( AV14Exist )
               {
                  AV7AddedKeyList.Add(A60ContratanteUsuario_UsuarioCod, 0);
                  AV5AddedDscList.Add(AV13Description, 0);
               }
               else
               {
                  AV22NotAddedKeyList.Add(A60ContratanteUsuario_UsuarioCod, 0);
                  AV20NotAddedDscList.Add(AV13Description, 0);
               }
               pr_default.readNext(1);
            }
            pr_default.close(1);
            /* Execute user subroutine: 'SAVELISTS' */
            S112 ();
            if (returnInSub) return;
         }
         edtavAddedkeylistxml_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavAddedkeylistxml_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavAddedkeylistxml_Visible), 5, 0)));
         edtavNotaddedkeylistxml_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavNotaddedkeylistxml_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavNotaddedkeylistxml_Visible), 5, 0)));
         edtavAddeddsclistxml_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavAddeddsclistxml_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavAddeddsclistxml_Visible), 5, 0)));
         edtavNotaddeddsclistxml_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavNotaddeddsclistxml_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavNotaddeddsclistxml_Visible), 5, 0)));
         lblTbjava_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbjava_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblTbjava_Visible), 5, 0)));
         lblTbjava_Caption = "<script type='text/javascript'> document.body.style.overflow = 'hidden'; </script>";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblTbjava_Internalname, "Caption", lblTbjava_Caption);
      }

      protected void E12MZ2( )
      {
         /* Refresh Routine */
         new wwpbaseobjects.loadwwpcontext(context ).execute( out  AV27WWPContext) ;
         imgImageassociateselected_Visible = (AV27WWPContext.gxTpr_Insert ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgImageassociateselected_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgImageassociateselected_Visible), 5, 0)));
         imgImageassociateall_Visible = (AV27WWPContext.gxTpr_Insert ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgImageassociateall_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgImageassociateall_Visible), 5, 0)));
         imgImagedisassociateselected_Visible = (AV27WWPContext.gxTpr_Update ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgImagedisassociateselected_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgImagedisassociateselected_Visible), 5, 0)));
         imgImagedisassociateall_Visible = (AV27WWPContext.gxTpr_Update ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgImagedisassociateall_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgImagedisassociateall_Visible), 5, 0)));
         bttBtn_confirm_Visible = (AV27WWPContext.gxTpr_Insert||AV27WWPContext.gxTpr_Update ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_confirm_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_confirm_Visible), 5, 0)));
         /* Execute user subroutine: 'UPDATEASSOCIATIONVARIABLES' */
         S122 ();
         if (returnInSub) return;
         lstavAssociatedrecords.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV9AssociatedRecords), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lstavAssociatedrecords_Internalname, "Values", lstavAssociatedrecords.ToJavascriptSource());
         lstavNotassociatedrecords.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24NotAssociatedRecords), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lstavNotassociatedrecords_Internalname, "Values", lstavNotassociatedrecords.ToJavascriptSource());
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV5AddedDscList", AV5AddedDscList);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV7AddedKeyList", AV7AddedKeyList);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV22NotAddedKeyList", AV22NotAddedKeyList);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV20NotAddedDscList", AV20NotAddedDscList);
      }

      public void GXEnter( )
      {
         /* Execute user event: E13MZ2 */
         E13MZ2 ();
         if (returnInSub) return;
      }

      protected void E13MZ2( )
      {
         /* Enter Routine */
         /* Execute user subroutine: 'LOADLISTS' */
         S132 ();
         if (returnInSub) return;
         AV16i = 1;
         AV26Success = true;
         AV36GXV1 = 1;
         while ( AV36GXV1 <= AV7AddedKeyList.Count )
         {
            AV28Usuario_Codigo = (int)(AV7AddedKeyList.GetNumeric(AV36GXV1));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV28Usuario_Codigo), 6, 0)));
            if ( AV26Success )
            {
               AV14Exist = false;
               /* Using cursor H00MZ5 */
               pr_default.execute(3, new Object[] {AV29Contratante_Codigo, AV28Usuario_Codigo, AV25Sistema_Codigo});
               while ( (pr_default.getStatus(3) != 101) )
               {
                  A127Sistema_Codigo = H00MZ5_A127Sistema_Codigo[0];
                  A60ContratanteUsuario_UsuarioCod = H00MZ5_A60ContratanteUsuario_UsuarioCod[0];
                  A63ContratanteUsuario_ContratanteCod = H00MZ5_A63ContratanteUsuario_ContratanteCod[0];
                  AV14Exist = true;
                  /* Exiting from a For First loop. */
                  if (true) break;
               }
               pr_default.close(3);
               if ( ! AV14Exist )
               {
                  AV12ContratanteUsuarioSistema = new SdtContratanteUsuarioSistema(context);
                  AV12ContratanteUsuarioSistema.gxTpr_Contratanteusuario_contratantecod = AV29Contratante_Codigo;
                  AV12ContratanteUsuarioSistema.gxTpr_Contratanteusuario_usuariocod = AV28Usuario_Codigo;
                  AV12ContratanteUsuarioSistema.gxTpr_Sistema_codigo = AV25Sistema_Codigo;
                  AV12ContratanteUsuarioSistema.Save();
                  if ( ! AV12ContratanteUsuarioSistema.Success() )
                  {
                     AV26Success = false;
                  }
               }
            }
            AV16i = (int)(AV16i+1);
            AV36GXV1 = (int)(AV36GXV1+1);
         }
         AV16i = 1;
         AV38GXV2 = 1;
         while ( AV38GXV2 <= AV22NotAddedKeyList.Count )
         {
            AV28Usuario_Codigo = (int)(AV22NotAddedKeyList.GetNumeric(AV38GXV2));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV28Usuario_Codigo), 6, 0)));
            if ( AV26Success )
            {
               AV14Exist = false;
               /* Using cursor H00MZ6 */
               pr_default.execute(4, new Object[] {AV29Contratante_Codigo, AV28Usuario_Codigo, AV25Sistema_Codigo});
               while ( (pr_default.getStatus(4) != 101) )
               {
                  A127Sistema_Codigo = H00MZ6_A127Sistema_Codigo[0];
                  A60ContratanteUsuario_UsuarioCod = H00MZ6_A60ContratanteUsuario_UsuarioCod[0];
                  A63ContratanteUsuario_ContratanteCod = H00MZ6_A63ContratanteUsuario_ContratanteCod[0];
                  AV14Exist = true;
                  /* Exiting from a For First loop. */
                  if (true) break;
               }
               pr_default.close(4);
               if ( AV14Exist )
               {
                  AV12ContratanteUsuarioSistema = new SdtContratanteUsuarioSistema(context);
                  AV12ContratanteUsuarioSistema.Load(AV29Contratante_Codigo, AV28Usuario_Codigo, AV25Sistema_Codigo);
                  if ( AV12ContratanteUsuarioSistema.Success() )
                  {
                     AV12ContratanteUsuarioSistema.Delete();
                  }
                  if ( ! AV12ContratanteUsuarioSistema.Success() )
                  {
                     AV26Success = false;
                  }
               }
            }
            AV16i = (int)(AV16i+1);
            AV38GXV2 = (int)(AV38GXV2+1);
         }
         if ( AV26Success )
         {
            context.CommitDataStores( "WP_AssociarSistema_ContratanteUsuario");
            context.setWebReturnParms(new Object[] {});
            context.wjLocDisableFrm = 1;
            context.nUserReturn = 1;
            returnInSub = true;
            if (true) return;
         }
         else
         {
            /* Execute user subroutine: 'SHOW ERROR MESSAGES' */
            S142 ();
            if (returnInSub) return;
         }
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV12ContratanteUsuarioSistema", AV12ContratanteUsuarioSistema);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV5AddedDscList", AV5AddedDscList);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV7AddedKeyList", AV7AddedKeyList);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV22NotAddedKeyList", AV22NotAddedKeyList);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV20NotAddedDscList", AV20NotAddedDscList);
      }

      protected void E14MZ2( )
      {
         /* 'Disassociate Selected' Routine */
         /* Execute user subroutine: 'DISASSOCIATESELECTED' */
         S152 ();
         if (returnInSub) return;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV22NotAddedKeyList", AV22NotAddedKeyList);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV20NotAddedDscList", AV20NotAddedDscList);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV7AddedKeyList", AV7AddedKeyList);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV5AddedDscList", AV5AddedDscList);
         lstavAssociatedrecords.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV9AssociatedRecords), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lstavAssociatedrecords_Internalname, "Values", lstavAssociatedrecords.ToJavascriptSource());
         lstavNotassociatedrecords.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24NotAssociatedRecords), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lstavNotassociatedrecords_Internalname, "Values", lstavNotassociatedrecords.ToJavascriptSource());
      }

      protected void E15MZ2( )
      {
         /* 'Associate selected' Routine */
         /* Execute user subroutine: 'ASSOCIATESELECTED' */
         S162 ();
         if (returnInSub) return;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV7AddedKeyList", AV7AddedKeyList);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV5AddedDscList", AV5AddedDscList);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV22NotAddedKeyList", AV22NotAddedKeyList);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV20NotAddedDscList", AV20NotAddedDscList);
         lstavAssociatedrecords.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV9AssociatedRecords), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lstavAssociatedrecords_Internalname, "Values", lstavAssociatedrecords.ToJavascriptSource());
         lstavNotassociatedrecords.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24NotAssociatedRecords), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lstavNotassociatedrecords_Internalname, "Values", lstavNotassociatedrecords.ToJavascriptSource());
      }

      protected void E16MZ2( )
      {
         /* 'Associate All' Routine */
         /* Execute user subroutine: 'ASSOCIATEALL' */
         S172 ();
         if (returnInSub) return;
         /* Execute user subroutine: 'UPDATEASSOCIATIONVARIABLES' */
         S122 ();
         if (returnInSub) return;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV7AddedKeyList", AV7AddedKeyList);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV5AddedDscList", AV5AddedDscList);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV22NotAddedKeyList", AV22NotAddedKeyList);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV20NotAddedDscList", AV20NotAddedDscList);
         lstavAssociatedrecords.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV9AssociatedRecords), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lstavAssociatedrecords_Internalname, "Values", lstavAssociatedrecords.ToJavascriptSource());
         lstavNotassociatedrecords.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24NotAssociatedRecords), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lstavNotassociatedrecords_Internalname, "Values", lstavNotassociatedrecords.ToJavascriptSource());
      }

      protected void E17MZ2( )
      {
         /* 'Disassociate All' Routine */
         /* Execute user subroutine: 'ASSOCIATEALL' */
         S172 ();
         if (returnInSub) return;
         AV22NotAddedKeyList = (IGxCollection)(AV7AddedKeyList.Clone());
         AV20NotAddedDscList = (IGxCollection)(AV5AddedDscList.Clone());
         AV5AddedDscList.Clear();
         AV7AddedKeyList.Clear();
         /* Execute user subroutine: 'SAVELISTS' */
         S112 ();
         if (returnInSub) return;
         /* Execute user subroutine: 'UPDATEASSOCIATIONVARIABLES' */
         S122 ();
         if (returnInSub) return;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV22NotAddedKeyList", AV22NotAddedKeyList);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV20NotAddedDscList", AV20NotAddedDscList);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV5AddedDscList", AV5AddedDscList);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV7AddedKeyList", AV7AddedKeyList);
         lstavAssociatedrecords.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV9AssociatedRecords), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lstavAssociatedrecords_Internalname, "Values", lstavAssociatedrecords.ToJavascriptSource());
         lstavNotassociatedrecords.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24NotAssociatedRecords), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lstavNotassociatedrecords_Internalname, "Values", lstavNotassociatedrecords.ToJavascriptSource());
      }

      protected void E18MZ2( )
      {
         /* Associatedrecords_Dblclick Routine */
         /* Execute user subroutine: 'DISASSOCIATESELECTED' */
         S152 ();
         if (returnInSub) return;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV22NotAddedKeyList", AV22NotAddedKeyList);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV20NotAddedDscList", AV20NotAddedDscList);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV7AddedKeyList", AV7AddedKeyList);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV5AddedDscList", AV5AddedDscList);
         lstavAssociatedrecords.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV9AssociatedRecords), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lstavAssociatedrecords_Internalname, "Values", lstavAssociatedrecords.ToJavascriptSource());
         lstavNotassociatedrecords.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24NotAssociatedRecords), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lstavNotassociatedrecords_Internalname, "Values", lstavNotassociatedrecords.ToJavascriptSource());
      }

      protected void E19MZ2( )
      {
         /* Notassociatedrecords_Dblclick Routine */
         /* Execute user subroutine: 'ASSOCIATESELECTED' */
         S162 ();
         if (returnInSub) return;
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV7AddedKeyList", AV7AddedKeyList);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV5AddedDscList", AV5AddedDscList);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV22NotAddedKeyList", AV22NotAddedKeyList);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV20NotAddedDscList", AV20NotAddedDscList);
         lstavAssociatedrecords.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV9AssociatedRecords), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lstavAssociatedrecords_Internalname, "Values", lstavAssociatedrecords.ToJavascriptSource());
         lstavNotassociatedrecords.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24NotAssociatedRecords), 6, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lstavNotassociatedrecords_Internalname, "Values", lstavNotassociatedrecords.ToJavascriptSource());
      }

      protected void S122( )
      {
         /* 'UPDATEASSOCIATIONVARIABLES' Routine */
         /* Execute user subroutine: 'LOADLISTS' */
         S132 ();
         if (returnInSub) return;
         lstavAssociatedrecords.removeAllItems();
         lstavNotassociatedrecords.removeAllItems();
         AV16i = 1;
         AV40GXV3 = 1;
         while ( AV40GXV3 <= AV7AddedKeyList.Count )
         {
            AV28Usuario_Codigo = (int)(AV7AddedKeyList.GetNumeric(AV40GXV3));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV28Usuario_Codigo), 6, 0)));
            AV13Description = ((String)AV5AddedDscList.Item(AV16i));
            lstavAssociatedrecords.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(AV28Usuario_Codigo), 6, 0)), StringUtil.Trim( AV13Description), 0);
            AV16i = (int)(AV16i+1);
            AV40GXV3 = (int)(AV40GXV3+1);
         }
         AV16i = 1;
         AV41GXV4 = 1;
         while ( AV41GXV4 <= AV22NotAddedKeyList.Count )
         {
            AV28Usuario_Codigo = (int)(AV22NotAddedKeyList.GetNumeric(AV41GXV4));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV28Usuario_Codigo), 6, 0)));
            AV13Description = ((String)AV20NotAddedDscList.Item(AV16i));
            lstavNotassociatedrecords.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(AV28Usuario_Codigo), 6, 0)), StringUtil.Trim( AV13Description), 0);
            AV16i = (int)(AV16i+1);
            AV41GXV4 = (int)(AV41GXV4+1);
         }
      }

      protected void S142( )
      {
         /* 'SHOW ERROR MESSAGES' Routine */
         AV43GXV6 = 1;
         AV42GXV5 = AV12ContratanteUsuarioSistema.GetMessages();
         while ( AV43GXV6 <= AV42GXV5.Count )
         {
            AV19Message = ((SdtMessages_Message)AV42GXV5.Item(AV43GXV6));
            if ( AV19Message.gxTpr_Type == 1 )
            {
               GX_msglist.addItem(AV19Message.gxTpr_Description);
            }
            AV43GXV6 = (int)(AV43GXV6+1);
         }
      }

      protected void S132( )
      {
         /* 'LOADLISTS' Routine */
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV8AddedKeyListXml)) )
         {
            AV5AddedDscList.FromXml(AV6AddedDscListXml, "Collection");
            AV7AddedKeyList.FromXml(AV8AddedKeyListXml, "Collection");
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV23NotAddedKeyListXml)) )
         {
            AV22NotAddedKeyList.FromXml(AV23NotAddedKeyListXml, "Collection");
            AV20NotAddedDscList.FromXml(AV21NotAddedDscListXml, "Collection");
         }
      }

      protected void S112( )
      {
         /* 'SAVELISTS' Routine */
         if ( AV7AddedKeyList.Count > 0 )
         {
            AV8AddedKeyListXml = AV7AddedKeyList.ToXml(false, true, "Collection", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8AddedKeyListXml", AV8AddedKeyListXml);
            AV6AddedDscListXml = AV5AddedDscList.ToXml(false, true, "Collection", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6AddedDscListXml", AV6AddedDscListXml);
         }
         else
         {
            AV8AddedKeyListXml = "";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8AddedKeyListXml", AV8AddedKeyListXml);
            AV6AddedDscListXml = "";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6AddedDscListXml", AV6AddedDscListXml);
         }
         if ( AV22NotAddedKeyList.Count > 0 )
         {
            AV23NotAddedKeyListXml = AV22NotAddedKeyList.ToXml(false, true, "Collection", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23NotAddedKeyListXml", AV23NotAddedKeyListXml);
            AV21NotAddedDscListXml = AV20NotAddedDscList.ToXml(false, true, "Collection", "");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21NotAddedDscListXml", AV21NotAddedDscListXml);
         }
         else
         {
            AV23NotAddedKeyListXml = "";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23NotAddedKeyListXml", AV23NotAddedKeyListXml);
            AV21NotAddedDscListXml = "";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21NotAddedDscListXml", AV21NotAddedDscListXml);
         }
      }

      protected void S172( )
      {
         /* 'ASSOCIATEALL' Routine */
         /* Execute user subroutine: 'LOADLISTS' */
         S132 ();
         if (returnInSub) return;
         AV16i = 1;
         AV17InsertIndex = 1;
         AV44GXV7 = 1;
         while ( AV44GXV7 <= AV22NotAddedKeyList.Count )
         {
            AV28Usuario_Codigo = (int)(AV22NotAddedKeyList.GetNumeric(AV44GXV7));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV28Usuario_Codigo), 6, 0)));
            AV13Description = ((String)AV20NotAddedDscList.Item(AV16i));
            while ( ( AV17InsertIndex <= AV5AddedDscList.Count ) && ( StringUtil.StrCmp(((String)AV5AddedDscList.Item(AV17InsertIndex)), AV13Description) < 0 ) )
            {
               AV17InsertIndex = (int)(AV17InsertIndex+1);
            }
            AV7AddedKeyList.Add(AV28Usuario_Codigo, AV17InsertIndex);
            AV5AddedDscList.Add(AV13Description, AV17InsertIndex);
            AV16i = (int)(AV16i+1);
            AV44GXV7 = (int)(AV44GXV7+1);
         }
         AV22NotAddedKeyList.Clear();
         AV20NotAddedDscList.Clear();
         /* Execute user subroutine: 'SAVELISTS' */
         S112 ();
         if (returnInSub) return;
      }

      protected void S162( )
      {
         /* 'ASSOCIATESELECTED' Routine */
         /* Execute user subroutine: 'LOADLISTS' */
         S132 ();
         if (returnInSub) return;
         AV16i = 1;
         AV45GXV8 = 1;
         while ( AV45GXV8 <= AV22NotAddedKeyList.Count )
         {
            AV28Usuario_Codigo = (int)(AV22NotAddedKeyList.GetNumeric(AV45GXV8));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV28Usuario_Codigo), 6, 0)));
            if ( AV28Usuario_Codigo == AV24NotAssociatedRecords )
            {
               if (true) break;
            }
            AV16i = (int)(AV16i+1);
            AV45GXV8 = (int)(AV45GXV8+1);
         }
         if ( AV16i <= AV22NotAddedKeyList.Count )
         {
            AV13Description = ((String)AV20NotAddedDscList.Item(AV16i));
            AV17InsertIndex = 1;
            while ( ( AV17InsertIndex <= AV5AddedDscList.Count ) && ( StringUtil.StrCmp(((String)AV5AddedDscList.Item(AV17InsertIndex)), AV13Description) < 0 ) )
            {
               AV17InsertIndex = (int)(AV17InsertIndex+1);
            }
            AV7AddedKeyList.Add(AV24NotAssociatedRecords, AV17InsertIndex);
            AV5AddedDscList.Add(AV13Description, AV17InsertIndex);
            AV22NotAddedKeyList.RemoveItem(AV16i);
            AV20NotAddedDscList.RemoveItem(AV16i);
            /* Execute user subroutine: 'SAVELISTS' */
            S112 ();
            if (returnInSub) return;
         }
         /* Execute user subroutine: 'UPDATEASSOCIATIONVARIABLES' */
         S122 ();
         if (returnInSub) return;
      }

      protected void S152( )
      {
         /* 'DISASSOCIATESELECTED' Routine */
         /* Execute user subroutine: 'LOADLISTS' */
         S132 ();
         if (returnInSub) return;
         AV16i = 1;
         AV46GXV9 = 1;
         while ( AV46GXV9 <= AV7AddedKeyList.Count )
         {
            AV28Usuario_Codigo = (int)(AV7AddedKeyList.GetNumeric(AV46GXV9));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28Usuario_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV28Usuario_Codigo), 6, 0)));
            if ( AV28Usuario_Codigo == AV9AssociatedRecords )
            {
               if (true) break;
            }
            AV16i = (int)(AV16i+1);
            AV46GXV9 = (int)(AV46GXV9+1);
         }
         if ( AV16i <= AV7AddedKeyList.Count )
         {
            AV13Description = ((String)AV5AddedDscList.Item(AV16i));
            AV17InsertIndex = 1;
            while ( ( AV17InsertIndex <= AV20NotAddedDscList.Count ) && ( StringUtil.StrCmp(((String)AV20NotAddedDscList.Item(AV17InsertIndex)), AV13Description) < 0 ) )
            {
               AV17InsertIndex = (int)(AV17InsertIndex+1);
            }
            AV22NotAddedKeyList.Add(AV9AssociatedRecords, AV17InsertIndex);
            AV20NotAddedDscList.Add(AV13Description, AV17InsertIndex);
            AV7AddedKeyList.RemoveItem(AV16i);
            AV5AddedDscList.RemoveItem(AV16i);
            /* Execute user subroutine: 'SAVELISTS' */
            S112 ();
            if (returnInSub) return;
         }
         /* Execute user subroutine: 'UPDATEASSOCIATIONVARIABLES' */
         S122 ();
         if (returnInSub) return;
      }

      protected void nextLoad( )
      {
      }

      protected void E20MZ2( )
      {
         /* Load Routine */
      }

      protected void wb_table1_2_MZ2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablemain_Internalname, tblTablemain_Internalname, "", "TableMain", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TextBlockTitleCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblAssociationtitle_Internalname, "Usu�rios ao Sistema :: ", "", "", lblAssociationtitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlockTitleWWP", 0, "", 1, 1, 0, "HLP_WP_AssociarSistema_ContratanteUsuario.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 9,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavSistema_sigla_Internalname, StringUtil.RTrim( AV30Sistema_Sigla), StringUtil.RTrim( context.localUtil.Format( AV30Sistema_Sigla, "@!")), TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+"this.value=this.value.toUpperCase();"+";gx.evt.onblur(this,9);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavSistema_sigla_Jsonclick, 0, "AttributeTitleWWP", "", "", "", 1, edtavSistema_sigla_Enabled, 0, "text", "", 25, "chr", 1, "row", 25, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WP_AssociarSistema_ContratanteUsuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table2_12_MZ2( true) ;
         }
         else
         {
            wb_table2_12_MZ2( false) ;
         }
         return  ;
      }

      protected void wb_table2_12_MZ2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_44_MZ2( true) ;
         }
         else
         {
            wb_table3_44_MZ2( false) ;
         }
         return  ;
      }

      protected void wb_table3_44_MZ2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_2_MZ2e( true) ;
         }
         else
         {
            wb_table1_2_MZ2e( false) ;
         }
      }

      protected void wb_table3_44_MZ2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTableactions_Internalname, tblTableactions_Internalname, "", "TableActions", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 47,'',false,'',0)\"";
            ClassString = "BtnEnter";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_confirm_Internalname, "", "Confirmar", bttBtn_confirm_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_confirm_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_AssociarSistema_ContratanteUsuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 49,'',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_cancel_Internalname, "", "Cancelar", bttBtn_cancel_Jsonclick, 1, "Cancelar", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_WP_AssociarSistema_ContratanteUsuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_44_MZ2e( true) ;
         }
         else
         {
            wb_table3_44_MZ2e( false) ;
         }
      }

      protected void wb_table2_12_MZ2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablefullcontent_Internalname, tblTablefullcontent_Internalname, "", "TableContent", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='TableAttributesCell'>") ;
            wb_table4_15_MZ2( true) ;
         }
         else
         {
            wb_table4_15_MZ2( false) ;
         }
         return  ;
      }

      protected void wb_table4_15_MZ2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_12_MZ2e( true) ;
         }
         else
         {
            wb_table2_12_MZ2e( false) ;
         }
      }

      protected void wb_table4_15_MZ2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTablecontent_Internalname, tblTablecontent_Internalname, "", "TableAssociation", 0, "", "", 4, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\" class='AssociationTitleCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblNotassociatedrecordstitle_Internalname, "Usu�rios N�o Associados", "", "", lblNotassociatedrecordstitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "AssociationTitle", 0, "", 1, 1, 0, "HLP_WP_AssociarSistema_ContratanteUsuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\" class='AssociationTitleCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblCenterrecordstitle_Internalname, " ", "", "", lblCenterrecordstitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "AssociationTitle", 0, "", 1, 1, 0, "HLP_WP_AssociarSistema_ContratanteUsuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\" class='AssociationTitleCell'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblAssociatedrecordstitle_Internalname, "Usu�rios Associados", "", "", lblAssociatedrecordstitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "AssociationTitle", 0, "", 1, 1, 0, "HLP_WP_AssociarSistema_ContratanteUsuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 25,'',false,'',0)\"";
            /* ListBox */
            GxWebStd.gx_listbox_ctrl1( context, lstavNotassociatedrecords, lstavNotassociatedrecords_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV24NotAssociatedRecords), 6, 0)), 2, lstavNotassociatedrecords_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, 1, 0, 0, 0, "px", 0, "row", "", "AssociationListAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,25);\"", "", true, "HLP_WP_AssociarSistema_ContratanteUsuario.htm");
            lstavNotassociatedrecords.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24NotAssociatedRecords), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lstavNotassociatedrecords_Internalname, "Values", (String)(lstavNotassociatedrecords.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center")+"\">") ;
            wb_table5_27_MZ2( true) ;
         }
         else
         {
            wb_table5_27_MZ2( false) ;
         }
         return  ;
      }

      protected void wb_table5_27_MZ2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 41,'',false,'',0)\"";
            /* ListBox */
            GxWebStd.gx_listbox_ctrl1( context, lstavAssociatedrecords, lstavAssociatedrecords_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV9AssociatedRecords), 6, 0)), 2, lstavAssociatedrecords_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, 1, 0, 0, 0, "px", 0, "row", "", "AssociationListAttribute", "", TempTags+" onchange=\"gx.evt.onchange(this)\" "+" onblur=\""+""+";gx.evt.onblur(this,41);\"", "", true, "HLP_WP_AssociarSistema_ContratanteUsuario.htm");
            lstavAssociatedrecords.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV9AssociatedRecords), 6, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lstavAssociatedrecords_Internalname, "Values", (String)(lstavAssociatedrecords.ToJavascriptSource()));
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_15_MZ2e( true) ;
         }
         else
         {
            wb_table4_15_MZ2e( false) ;
         }
      }

      protected void wb_table5_27_MZ2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblUnnamedtableassociationbuttons_Internalname, tblUnnamedtableassociationbuttons_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 30,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgImageassociateall_Internalname, context.GetImagePath( "6591e2a3-49b6-43b7-b8e3-a292564a32a4", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgImageassociateall_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgImageassociateall_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ASSOCIATE ALL\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WP_AssociarSistema_ContratanteUsuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 33,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgImageassociateselected_Internalname, context.GetImagePath( "56a5f17b-0bc3-48b5-b303-afa6e0585b6d", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgImageassociateselected_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgImageassociateselected_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'ASSOCIATE SELECTED\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WP_AssociarSistema_ContratanteUsuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 36,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgImagedisassociateselected_Internalname, context.GetImagePath( "a3800d0c-bf04-4575-bc01-11fe5d7b3525", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgImagedisassociateselected_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgImagedisassociateselected_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DISASSOCIATE SELECTED\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WP_AssociarSistema_ContratanteUsuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 39,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            GxWebStd.gx_bitmap( context, imgImagedisassociateall_Internalname, context.GetImagePath( "c619e28f-4b32-4ff9-baaf-b3063fe4f782", "", context.GetTheme( )), "", "", "", context.GetTheme( ), imgImagedisassociateall_Visible, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgImagedisassociateall_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DISASSOCIATE ALL\\'."+"'", StyleString, ClassString, "", "", "", ""+TempTags, "", "", 1, false, false, "HLP_WP_AssociarSistema_ContratanteUsuario.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_27_MZ2e( true) ;
         }
         else
         {
            wb_table5_27_MZ2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV25Sistema_Codigo = Convert.ToInt32(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25Sistema_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV25Sistema_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vSISTEMA_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV25Sistema_Codigo), "ZZZZZ9")));
         AV29Contratante_Codigo = Convert.ToInt32(getParm(obj,1));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29Contratante_Codigo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV29Contratante_Codigo), 6, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCONTRATANTE_CODIGO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV29Contratante_Codigo), "ZZZZZ9")));
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("WorkWithPlusBootstrapTheme");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PAMZ2( ) ;
         WSMZ2( ) ;
         WEMZ2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?771914");
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20205299435220");
            idxLst = (int)(idxLst+1);
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.por.js", "?"+context.GetBuildNumber( 114418));
         context.AddJavascriptSource("wp_associarsistema_contratanteusuario.js", "?20205299435220");
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblAssociationtitle_Internalname = "ASSOCIATIONTITLE";
         edtavSistema_sigla_Internalname = "vSISTEMA_SIGLA";
         lblNotassociatedrecordstitle_Internalname = "NOTASSOCIATEDRECORDSTITLE";
         lblCenterrecordstitle_Internalname = "CENTERRECORDSTITLE";
         lblAssociatedrecordstitle_Internalname = "ASSOCIATEDRECORDSTITLE";
         lstavNotassociatedrecords_Internalname = "vNOTASSOCIATEDRECORDS";
         imgImageassociateall_Internalname = "IMAGEASSOCIATEALL";
         imgImageassociateselected_Internalname = "IMAGEASSOCIATESELECTED";
         imgImagedisassociateselected_Internalname = "IMAGEDISASSOCIATESELECTED";
         imgImagedisassociateall_Internalname = "IMAGEDISASSOCIATEALL";
         tblUnnamedtableassociationbuttons_Internalname = "UNNAMEDTABLEASSOCIATIONBUTTONS";
         lstavAssociatedrecords_Internalname = "vASSOCIATEDRECORDS";
         tblTablecontent_Internalname = "TABLECONTENT";
         tblTablefullcontent_Internalname = "TABLEFULLCONTENT";
         bttBtn_confirm_Internalname = "BTN_CONFIRM";
         bttBtn_cancel_Internalname = "BTN_CANCEL";
         tblTableactions_Internalname = "TABLEACTIONS";
         tblTablemain_Internalname = "TABLEMAIN";
         edtavAddedkeylistxml_Internalname = "vADDEDKEYLISTXML";
         edtavNotaddedkeylistxml_Internalname = "vNOTADDEDKEYLISTXML";
         edtavAddeddsclistxml_Internalname = "vADDEDDSCLISTXML";
         edtavNotaddeddsclistxml_Internalname = "vNOTADDEDDSCLISTXML";
         lblTbjava_Internalname = "TBJAVA";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         imgImagedisassociateall_Visible = 1;
         imgImagedisassociateselected_Visible = 1;
         imgImageassociateselected_Visible = 1;
         imgImageassociateall_Visible = 1;
         lstavAssociatedrecords_Jsonclick = "";
         lstavNotassociatedrecords_Jsonclick = "";
         bttBtn_confirm_Visible = 1;
         edtavSistema_sigla_Jsonclick = "";
         edtavSistema_sigla_Enabled = 1;
         lblTbjava_Caption = "tbJava";
         lblTbjava_Visible = 1;
         edtavNotaddeddsclistxml_Visible = 1;
         edtavAddeddsclistxml_Visible = 1;
         edtavNotaddedkeylistxml_Visible = 1;
         edtavAddedkeylistxml_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'AV7AddedKeyList',fld:'vADDEDKEYLIST',pic:'',nv:null},{av:'AV5AddedDscList',fld:'vADDEDDSCLIST',pic:'',nv:null},{av:'AV22NotAddedKeyList',fld:'vNOTADDEDKEYLIST',pic:'',nv:null},{av:'AV20NotAddedDscList',fld:'vNOTADDEDDSCLIST',pic:'',nv:null},{av:'AV8AddedKeyListXml',fld:'vADDEDKEYLISTXML',pic:'',nv:''},{av:'AV6AddedDscListXml',fld:'vADDEDDSCLISTXML',pic:'',nv:''},{av:'AV23NotAddedKeyListXml',fld:'vNOTADDEDKEYLISTXML',pic:'',nv:''},{av:'AV21NotAddedDscListXml',fld:'vNOTADDEDDSCLISTXML',pic:'',nv:''}],oparms:[{av:'imgImageassociateselected_Visible',ctrl:'IMAGEASSOCIATESELECTED',prop:'Visible'},{av:'imgImageassociateall_Visible',ctrl:'IMAGEASSOCIATEALL',prop:'Visible'},{av:'imgImagedisassociateselected_Visible',ctrl:'IMAGEDISASSOCIATESELECTED',prop:'Visible'},{av:'imgImagedisassociateall_Visible',ctrl:'IMAGEDISASSOCIATEALL',prop:'Visible'},{ctrl:'BTN_CONFIRM',prop:'Visible'},{av:'AV9AssociatedRecords',fld:'vASSOCIATEDRECORDS',pic:'ZZZZZ9',nv:0},{av:'AV24NotAssociatedRecords',fld:'vNOTASSOCIATEDRECORDS',pic:'ZZZZZ9',nv:0},{av:'AV28Usuario_Codigo',fld:'vUSUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV5AddedDscList',fld:'vADDEDDSCLIST',pic:'',nv:null},{av:'AV7AddedKeyList',fld:'vADDEDKEYLIST',pic:'',nv:null},{av:'AV22NotAddedKeyList',fld:'vNOTADDEDKEYLIST',pic:'',nv:null},{av:'AV20NotAddedDscList',fld:'vNOTADDEDDSCLIST',pic:'',nv:null}]}");
         setEventMetadata("ENTER","{handler:'E13MZ2',iparms:[{av:'AV7AddedKeyList',fld:'vADDEDKEYLIST',pic:'',nv:null},{av:'A63ContratanteUsuario_ContratanteCod',fld:'CONTRATANTEUSUARIO_CONTRATANTECOD',pic:'ZZZZZ9',nv:0},{av:'AV29Contratante_Codigo',fld:'vCONTRATANTE_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'A60ContratanteUsuario_UsuarioCod',fld:'CONTRATANTEUSUARIO_USUARIOCOD',pic:'ZZZZZ9',nv:0},{av:'A127Sistema_Codigo',fld:'SISTEMA_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV25Sistema_Codigo',fld:'vSISTEMA_CODIGO',pic:'ZZZZZ9',hsh:true,nv:0},{av:'AV22NotAddedKeyList',fld:'vNOTADDEDKEYLIST',pic:'',nv:null},{av:'AV8AddedKeyListXml',fld:'vADDEDKEYLISTXML',pic:'',nv:''},{av:'AV6AddedDscListXml',fld:'vADDEDDSCLISTXML',pic:'',nv:''},{av:'AV23NotAddedKeyListXml',fld:'vNOTADDEDKEYLISTXML',pic:'',nv:''},{av:'AV21NotAddedDscListXml',fld:'vNOTADDEDDSCLISTXML',pic:'',nv:''},{av:'AV12ContratanteUsuarioSistema',fld:'vCONTRATANTEUSUARIOSISTEMA',pic:'',nv:null}],oparms:[{av:'AV28Usuario_Codigo',fld:'vUSUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV12ContratanteUsuarioSistema',fld:'vCONTRATANTEUSUARIOSISTEMA',pic:'',nv:null},{av:'AV5AddedDscList',fld:'vADDEDDSCLIST',pic:'',nv:null},{av:'AV7AddedKeyList',fld:'vADDEDKEYLIST',pic:'',nv:null},{av:'AV22NotAddedKeyList',fld:'vNOTADDEDKEYLIST',pic:'',nv:null},{av:'AV20NotAddedDscList',fld:'vNOTADDEDDSCLIST',pic:'',nv:null}]}");
         setEventMetadata("'DISASSOCIATE SELECTED'","{handler:'E14MZ2',iparms:[{av:'AV7AddedKeyList',fld:'vADDEDKEYLIST',pic:'',nv:null},{av:'AV9AssociatedRecords',fld:'vASSOCIATEDRECORDS',pic:'ZZZZZ9',nv:0},{av:'AV5AddedDscList',fld:'vADDEDDSCLIST',pic:'',nv:null},{av:'AV20NotAddedDscList',fld:'vNOTADDEDDSCLIST',pic:'',nv:null},{av:'AV22NotAddedKeyList',fld:'vNOTADDEDKEYLIST',pic:'',nv:null},{av:'AV8AddedKeyListXml',fld:'vADDEDKEYLISTXML',pic:'',nv:''},{av:'AV6AddedDscListXml',fld:'vADDEDDSCLISTXML',pic:'',nv:''},{av:'AV23NotAddedKeyListXml',fld:'vNOTADDEDKEYLISTXML',pic:'',nv:''},{av:'AV21NotAddedDscListXml',fld:'vNOTADDEDDSCLISTXML',pic:'',nv:''}],oparms:[{av:'AV28Usuario_Codigo',fld:'vUSUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV22NotAddedKeyList',fld:'vNOTADDEDKEYLIST',pic:'',nv:null},{av:'AV20NotAddedDscList',fld:'vNOTADDEDDSCLIST',pic:'',nv:null},{av:'AV7AddedKeyList',fld:'vADDEDKEYLIST',pic:'',nv:null},{av:'AV5AddedDscList',fld:'vADDEDDSCLIST',pic:'',nv:null},{av:'AV8AddedKeyListXml',fld:'vADDEDKEYLISTXML',pic:'',nv:''},{av:'AV6AddedDscListXml',fld:'vADDEDDSCLISTXML',pic:'',nv:''},{av:'AV23NotAddedKeyListXml',fld:'vNOTADDEDKEYLISTXML',pic:'',nv:''},{av:'AV21NotAddedDscListXml',fld:'vNOTADDEDDSCLISTXML',pic:'',nv:''},{av:'AV9AssociatedRecords',fld:'vASSOCIATEDRECORDS',pic:'ZZZZZ9',nv:0},{av:'AV24NotAssociatedRecords',fld:'vNOTASSOCIATEDRECORDS',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("'ASSOCIATE SELECTED'","{handler:'E15MZ2',iparms:[{av:'AV22NotAddedKeyList',fld:'vNOTADDEDKEYLIST',pic:'',nv:null},{av:'AV24NotAssociatedRecords',fld:'vNOTASSOCIATEDRECORDS',pic:'ZZZZZ9',nv:0},{av:'AV20NotAddedDscList',fld:'vNOTADDEDDSCLIST',pic:'',nv:null},{av:'AV5AddedDscList',fld:'vADDEDDSCLIST',pic:'',nv:null},{av:'AV7AddedKeyList',fld:'vADDEDKEYLIST',pic:'',nv:null},{av:'AV8AddedKeyListXml',fld:'vADDEDKEYLISTXML',pic:'',nv:''},{av:'AV6AddedDscListXml',fld:'vADDEDDSCLISTXML',pic:'',nv:''},{av:'AV23NotAddedKeyListXml',fld:'vNOTADDEDKEYLISTXML',pic:'',nv:''},{av:'AV21NotAddedDscListXml',fld:'vNOTADDEDDSCLISTXML',pic:'',nv:''}],oparms:[{av:'AV28Usuario_Codigo',fld:'vUSUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV7AddedKeyList',fld:'vADDEDKEYLIST',pic:'',nv:null},{av:'AV5AddedDscList',fld:'vADDEDDSCLIST',pic:'',nv:null},{av:'AV22NotAddedKeyList',fld:'vNOTADDEDKEYLIST',pic:'',nv:null},{av:'AV20NotAddedDscList',fld:'vNOTADDEDDSCLIST',pic:'',nv:null},{av:'AV8AddedKeyListXml',fld:'vADDEDKEYLISTXML',pic:'',nv:''},{av:'AV6AddedDscListXml',fld:'vADDEDDSCLISTXML',pic:'',nv:''},{av:'AV23NotAddedKeyListXml',fld:'vNOTADDEDKEYLISTXML',pic:'',nv:''},{av:'AV21NotAddedDscListXml',fld:'vNOTADDEDDSCLISTXML',pic:'',nv:''},{av:'AV9AssociatedRecords',fld:'vASSOCIATEDRECORDS',pic:'ZZZZZ9',nv:0},{av:'AV24NotAssociatedRecords',fld:'vNOTASSOCIATEDRECORDS',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("'ASSOCIATE ALL'","{handler:'E16MZ2',iparms:[{av:'AV22NotAddedKeyList',fld:'vNOTADDEDKEYLIST',pic:'',nv:null},{av:'AV20NotAddedDscList',fld:'vNOTADDEDDSCLIST',pic:'',nv:null},{av:'AV5AddedDscList',fld:'vADDEDDSCLIST',pic:'',nv:null},{av:'AV7AddedKeyList',fld:'vADDEDKEYLIST',pic:'',nv:null},{av:'AV8AddedKeyListXml',fld:'vADDEDKEYLISTXML',pic:'',nv:''},{av:'AV6AddedDscListXml',fld:'vADDEDDSCLISTXML',pic:'',nv:''},{av:'AV23NotAddedKeyListXml',fld:'vNOTADDEDKEYLISTXML',pic:'',nv:''},{av:'AV21NotAddedDscListXml',fld:'vNOTADDEDDSCLISTXML',pic:'',nv:''}],oparms:[{av:'AV28Usuario_Codigo',fld:'vUSUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV7AddedKeyList',fld:'vADDEDKEYLIST',pic:'',nv:null},{av:'AV5AddedDscList',fld:'vADDEDDSCLIST',pic:'',nv:null},{av:'AV22NotAddedKeyList',fld:'vNOTADDEDKEYLIST',pic:'',nv:null},{av:'AV20NotAddedDscList',fld:'vNOTADDEDDSCLIST',pic:'',nv:null},{av:'AV9AssociatedRecords',fld:'vASSOCIATEDRECORDS',pic:'ZZZZZ9',nv:0},{av:'AV24NotAssociatedRecords',fld:'vNOTASSOCIATEDRECORDS',pic:'ZZZZZ9',nv:0},{av:'AV8AddedKeyListXml',fld:'vADDEDKEYLISTXML',pic:'',nv:''},{av:'AV6AddedDscListXml',fld:'vADDEDDSCLISTXML',pic:'',nv:''},{av:'AV23NotAddedKeyListXml',fld:'vNOTADDEDKEYLISTXML',pic:'',nv:''},{av:'AV21NotAddedDscListXml',fld:'vNOTADDEDDSCLISTXML',pic:'',nv:''}]}");
         setEventMetadata("'DISASSOCIATE ALL'","{handler:'E17MZ2',iparms:[{av:'AV7AddedKeyList',fld:'vADDEDKEYLIST',pic:'',nv:null},{av:'AV5AddedDscList',fld:'vADDEDDSCLIST',pic:'',nv:null},{av:'AV22NotAddedKeyList',fld:'vNOTADDEDKEYLIST',pic:'',nv:null},{av:'AV20NotAddedDscList',fld:'vNOTADDEDDSCLIST',pic:'',nv:null},{av:'AV8AddedKeyListXml',fld:'vADDEDKEYLISTXML',pic:'',nv:''},{av:'AV6AddedDscListXml',fld:'vADDEDDSCLISTXML',pic:'',nv:''},{av:'AV23NotAddedKeyListXml',fld:'vNOTADDEDKEYLISTXML',pic:'',nv:''},{av:'AV21NotAddedDscListXml',fld:'vNOTADDEDDSCLISTXML',pic:'',nv:''}],oparms:[{av:'AV22NotAddedKeyList',fld:'vNOTADDEDKEYLIST',pic:'',nv:null},{av:'AV20NotAddedDscList',fld:'vNOTADDEDDSCLIST',pic:'',nv:null},{av:'AV5AddedDscList',fld:'vADDEDDSCLIST',pic:'',nv:null},{av:'AV7AddedKeyList',fld:'vADDEDKEYLIST',pic:'',nv:null},{av:'AV28Usuario_Codigo',fld:'vUSUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV8AddedKeyListXml',fld:'vADDEDKEYLISTXML',pic:'',nv:''},{av:'AV6AddedDscListXml',fld:'vADDEDDSCLISTXML',pic:'',nv:''},{av:'AV23NotAddedKeyListXml',fld:'vNOTADDEDKEYLISTXML',pic:'',nv:''},{av:'AV21NotAddedDscListXml',fld:'vNOTADDEDDSCLISTXML',pic:'',nv:''},{av:'AV9AssociatedRecords',fld:'vASSOCIATEDRECORDS',pic:'ZZZZZ9',nv:0},{av:'AV24NotAssociatedRecords',fld:'vNOTASSOCIATEDRECORDS',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("VASSOCIATEDRECORDS.DBLCLICK","{handler:'E18MZ2',iparms:[{av:'AV7AddedKeyList',fld:'vADDEDKEYLIST',pic:'',nv:null},{av:'AV9AssociatedRecords',fld:'vASSOCIATEDRECORDS',pic:'ZZZZZ9',nv:0},{av:'AV5AddedDscList',fld:'vADDEDDSCLIST',pic:'',nv:null},{av:'AV20NotAddedDscList',fld:'vNOTADDEDDSCLIST',pic:'',nv:null},{av:'AV22NotAddedKeyList',fld:'vNOTADDEDKEYLIST',pic:'',nv:null},{av:'AV8AddedKeyListXml',fld:'vADDEDKEYLISTXML',pic:'',nv:''},{av:'AV6AddedDscListXml',fld:'vADDEDDSCLISTXML',pic:'',nv:''},{av:'AV23NotAddedKeyListXml',fld:'vNOTADDEDKEYLISTXML',pic:'',nv:''},{av:'AV21NotAddedDscListXml',fld:'vNOTADDEDDSCLISTXML',pic:'',nv:''}],oparms:[{av:'AV28Usuario_Codigo',fld:'vUSUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV22NotAddedKeyList',fld:'vNOTADDEDKEYLIST',pic:'',nv:null},{av:'AV20NotAddedDscList',fld:'vNOTADDEDDSCLIST',pic:'',nv:null},{av:'AV7AddedKeyList',fld:'vADDEDKEYLIST',pic:'',nv:null},{av:'AV5AddedDscList',fld:'vADDEDDSCLIST',pic:'',nv:null},{av:'AV8AddedKeyListXml',fld:'vADDEDKEYLISTXML',pic:'',nv:''},{av:'AV6AddedDscListXml',fld:'vADDEDDSCLISTXML',pic:'',nv:''},{av:'AV23NotAddedKeyListXml',fld:'vNOTADDEDKEYLISTXML',pic:'',nv:''},{av:'AV21NotAddedDscListXml',fld:'vNOTADDEDDSCLISTXML',pic:'',nv:''},{av:'AV9AssociatedRecords',fld:'vASSOCIATEDRECORDS',pic:'ZZZZZ9',nv:0},{av:'AV24NotAssociatedRecords',fld:'vNOTASSOCIATEDRECORDS',pic:'ZZZZZ9',nv:0}]}");
         setEventMetadata("VNOTASSOCIATEDRECORDS.DBLCLICK","{handler:'E19MZ2',iparms:[{av:'AV22NotAddedKeyList',fld:'vNOTADDEDKEYLIST',pic:'',nv:null},{av:'AV24NotAssociatedRecords',fld:'vNOTASSOCIATEDRECORDS',pic:'ZZZZZ9',nv:0},{av:'AV20NotAddedDscList',fld:'vNOTADDEDDSCLIST',pic:'',nv:null},{av:'AV5AddedDscList',fld:'vADDEDDSCLIST',pic:'',nv:null},{av:'AV7AddedKeyList',fld:'vADDEDKEYLIST',pic:'',nv:null},{av:'AV8AddedKeyListXml',fld:'vADDEDKEYLISTXML',pic:'',nv:''},{av:'AV6AddedDscListXml',fld:'vADDEDDSCLISTXML',pic:'',nv:''},{av:'AV23NotAddedKeyListXml',fld:'vNOTADDEDKEYLISTXML',pic:'',nv:''},{av:'AV21NotAddedDscListXml',fld:'vNOTADDEDDSCLISTXML',pic:'',nv:''}],oparms:[{av:'AV28Usuario_Codigo',fld:'vUSUARIO_CODIGO',pic:'ZZZZZ9',nv:0},{av:'AV7AddedKeyList',fld:'vADDEDKEYLIST',pic:'',nv:null},{av:'AV5AddedDscList',fld:'vADDEDDSCLIST',pic:'',nv:null},{av:'AV22NotAddedKeyList',fld:'vNOTADDEDKEYLIST',pic:'',nv:null},{av:'AV20NotAddedDscList',fld:'vNOTADDEDDSCLIST',pic:'',nv:null},{av:'AV8AddedKeyListXml',fld:'vADDEDKEYLISTXML',pic:'',nv:''},{av:'AV6AddedDscListXml',fld:'vADDEDDSCLISTXML',pic:'',nv:''},{av:'AV23NotAddedKeyListXml',fld:'vNOTADDEDKEYLISTXML',pic:'',nv:''},{av:'AV21NotAddedDscListXml',fld:'vNOTADDEDDSCLISTXML',pic:'',nv:''},{av:'AV9AssociatedRecords',fld:'vASSOCIATEDRECORDS',pic:'ZZZZZ9',nv:0},{av:'AV24NotAssociatedRecords',fld:'vNOTASSOCIATEDRECORDS',pic:'ZZZZZ9',nv:0}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV7AddedKeyList = new GxSimpleCollection();
         AV5AddedDscList = new GxSimpleCollection();
         AV22NotAddedKeyList = new GxSimpleCollection();
         AV20NotAddedDscList = new GxSimpleCollection();
         AV12ContratanteUsuarioSistema = new SdtContratanteUsuarioSistema(context);
         AV30Sistema_Sigla = "";
         GXKey = "";
         GX_FocusControl = "";
         sPrefix = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         AV8AddedKeyListXml = "";
         AV23NotAddedKeyListXml = "";
         AV6AddedDscListXml = "";
         AV21NotAddedDscListXml = "";
         lblTbjava_Jsonclick = "";
         Form = new GXWebForm();
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV27WWPContext = new wwpbaseobjects.SdtWWPContext(context);
         AV15HTTPRequest = new GxHttpRequest( context);
         scmdbuf = "";
         H00MZ2_A127Sistema_Codigo = new int[1] ;
         H00MZ2_A129Sistema_Sigla = new String[] {""} ;
         A129Sistema_Sigla = "";
         H00MZ3_A61ContratanteUsuario_UsuarioPessoaCod = new int[1] ;
         H00MZ3_n61ContratanteUsuario_UsuarioPessoaCod = new bool[] {false} ;
         H00MZ3_A63ContratanteUsuario_ContratanteCod = new int[1] ;
         H00MZ3_A60ContratanteUsuario_UsuarioCod = new int[1] ;
         H00MZ3_A62ContratanteUsuario_UsuarioPessoaNom = new String[] {""} ;
         H00MZ3_n62ContratanteUsuario_UsuarioPessoaNom = new bool[] {false} ;
         A62ContratanteUsuario_UsuarioPessoaNom = "";
         H00MZ4_A63ContratanteUsuario_ContratanteCod = new int[1] ;
         H00MZ4_A60ContratanteUsuario_UsuarioCod = new int[1] ;
         H00MZ4_A127Sistema_Codigo = new int[1] ;
         AV13Description = "";
         H00MZ5_A127Sistema_Codigo = new int[1] ;
         H00MZ5_A60ContratanteUsuario_UsuarioCod = new int[1] ;
         H00MZ5_A63ContratanteUsuario_ContratanteCod = new int[1] ;
         H00MZ6_A127Sistema_Codigo = new int[1] ;
         H00MZ6_A60ContratanteUsuario_UsuarioCod = new int[1] ;
         H00MZ6_A63ContratanteUsuario_ContratanteCod = new int[1] ;
         AV42GXV5 = new GxObjectCollection( context, "Messages.Message", "Genexus", "SdtMessages_Message", "GeneXus.Programs");
         AV19Message = new SdtMessages_Message(context);
         sStyleString = "";
         lblAssociationtitle_Jsonclick = "";
         bttBtn_confirm_Jsonclick = "";
         bttBtn_cancel_Jsonclick = "";
         lblNotassociatedrecordstitle_Jsonclick = "";
         lblCenterrecordstitle_Jsonclick = "";
         lblAssociatedrecordstitle_Jsonclick = "";
         imgImageassociateall_Jsonclick = "";
         imgImageassociateselected_Jsonclick = "";
         imgImagedisassociateselected_Jsonclick = "";
         imgImagedisassociateall_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wp_associarsistema_contratanteusuario__default(),
            new Object[][] {
                new Object[] {
               H00MZ2_A127Sistema_Codigo, H00MZ2_A129Sistema_Sigla
               }
               , new Object[] {
               H00MZ3_A61ContratanteUsuario_UsuarioPessoaCod, H00MZ3_n61ContratanteUsuario_UsuarioPessoaCod, H00MZ3_A63ContratanteUsuario_ContratanteCod, H00MZ3_A60ContratanteUsuario_UsuarioCod, H00MZ3_A62ContratanteUsuario_UsuarioPessoaNom, H00MZ3_n62ContratanteUsuario_UsuarioPessoaNom
               }
               , new Object[] {
               H00MZ4_A63ContratanteUsuario_ContratanteCod, H00MZ4_A60ContratanteUsuario_UsuarioCod, H00MZ4_A127Sistema_Codigo
               }
               , new Object[] {
               H00MZ5_A127Sistema_Codigo, H00MZ5_A60ContratanteUsuario_UsuarioCod, H00MZ5_A63ContratanteUsuario_ContratanteCod
               }
               , new Object[] {
               H00MZ6_A127Sistema_Codigo, H00MZ6_A60ContratanteUsuario_UsuarioCod, H00MZ6_A63ContratanteUsuario_ContratanteCod
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
         edtavSistema_sigla_Enabled = 0;
      }

      private short nRcdExists_7 ;
      private short nIsMod_7 ;
      private short nRcdExists_6 ;
      private short nIsMod_6 ;
      private short nGotPars ;
      private short GxWebError ;
      private short initialized ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short AV33GXLvl6 ;
      private short nGXWrapped ;
      private int AV25Sistema_Codigo ;
      private int AV29Contratante_Codigo ;
      private int wcpOAV25Sistema_Codigo ;
      private int wcpOAV29Contratante_Codigo ;
      private int edtavSistema_sigla_Enabled ;
      private int A63ContratanteUsuario_ContratanteCod ;
      private int A60ContratanteUsuario_UsuarioCod ;
      private int A127Sistema_Codigo ;
      private int edtavAddedkeylistxml_Visible ;
      private int edtavNotaddedkeylistxml_Visible ;
      private int edtavAddeddsclistxml_Visible ;
      private int edtavNotaddeddsclistxml_Visible ;
      private int lblTbjava_Visible ;
      private int AV24NotAssociatedRecords ;
      private int AV9AssociatedRecords ;
      private int A61ContratanteUsuario_UsuarioPessoaCod ;
      private int imgImageassociateselected_Visible ;
      private int imgImageassociateall_Visible ;
      private int imgImagedisassociateselected_Visible ;
      private int imgImagedisassociateall_Visible ;
      private int bttBtn_confirm_Visible ;
      private int AV16i ;
      private int AV36GXV1 ;
      private int AV28Usuario_Codigo ;
      private int AV38GXV2 ;
      private int AV40GXV3 ;
      private int AV41GXV4 ;
      private int AV43GXV6 ;
      private int AV17InsertIndex ;
      private int AV44GXV7 ;
      private int AV45GXV8 ;
      private int AV46GXV9 ;
      private int idxLst ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String edtavSistema_sigla_Internalname ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String AV30Sistema_Sigla ;
      private String GXKey ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String edtavAddedkeylistxml_Internalname ;
      private String edtavNotaddedkeylistxml_Internalname ;
      private String edtavAddeddsclistxml_Internalname ;
      private String edtavNotaddeddsclistxml_Internalname ;
      private String lblTbjava_Internalname ;
      private String lblTbjava_Caption ;
      private String lblTbjava_Jsonclick ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String lstavNotassociatedrecords_Internalname ;
      private String lstavAssociatedrecords_Internalname ;
      private String scmdbuf ;
      private String A129Sistema_Sigla ;
      private String A62ContratanteUsuario_UsuarioPessoaNom ;
      private String imgImageassociateselected_Internalname ;
      private String imgImageassociateall_Internalname ;
      private String imgImagedisassociateselected_Internalname ;
      private String imgImagedisassociateall_Internalname ;
      private String bttBtn_confirm_Internalname ;
      private String sStyleString ;
      private String tblTablemain_Internalname ;
      private String lblAssociationtitle_Internalname ;
      private String lblAssociationtitle_Jsonclick ;
      private String edtavSistema_sigla_Jsonclick ;
      private String tblTableactions_Internalname ;
      private String bttBtn_confirm_Jsonclick ;
      private String bttBtn_cancel_Internalname ;
      private String bttBtn_cancel_Jsonclick ;
      private String tblTablefullcontent_Internalname ;
      private String tblTablecontent_Internalname ;
      private String lblNotassociatedrecordstitle_Internalname ;
      private String lblNotassociatedrecordstitle_Jsonclick ;
      private String lblCenterrecordstitle_Internalname ;
      private String lblCenterrecordstitle_Jsonclick ;
      private String lblAssociatedrecordstitle_Internalname ;
      private String lblAssociatedrecordstitle_Jsonclick ;
      private String lstavNotassociatedrecords_Jsonclick ;
      private String lstavAssociatedrecords_Jsonclick ;
      private String tblUnnamedtableassociationbuttons_Internalname ;
      private String imgImageassociateall_Jsonclick ;
      private String imgImageassociateselected_Jsonclick ;
      private String imgImagedisassociateselected_Jsonclick ;
      private String imgImagedisassociateall_Jsonclick ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool returnInSub ;
      private bool n61ContratanteUsuario_UsuarioPessoaCod ;
      private bool n62ContratanteUsuario_UsuarioPessoaNom ;
      private bool AV14Exist ;
      private bool AV26Success ;
      private String AV8AddedKeyListXml ;
      private String AV23NotAddedKeyListXml ;
      private String AV6AddedDscListXml ;
      private String AV21NotAddedDscListXml ;
      private String AV13Description ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXListbox lstavNotassociatedrecords ;
      private GXListbox lstavAssociatedrecords ;
      private IDataStoreProvider pr_default ;
      private int[] H00MZ2_A127Sistema_Codigo ;
      private String[] H00MZ2_A129Sistema_Sigla ;
      private int[] H00MZ3_A61ContratanteUsuario_UsuarioPessoaCod ;
      private bool[] H00MZ3_n61ContratanteUsuario_UsuarioPessoaCod ;
      private int[] H00MZ3_A63ContratanteUsuario_ContratanteCod ;
      private int[] H00MZ3_A60ContratanteUsuario_UsuarioCod ;
      private String[] H00MZ3_A62ContratanteUsuario_UsuarioPessoaNom ;
      private bool[] H00MZ3_n62ContratanteUsuario_UsuarioPessoaNom ;
      private int[] H00MZ4_A63ContratanteUsuario_ContratanteCod ;
      private int[] H00MZ4_A60ContratanteUsuario_UsuarioCod ;
      private int[] H00MZ4_A127Sistema_Codigo ;
      private int[] H00MZ5_A127Sistema_Codigo ;
      private int[] H00MZ5_A60ContratanteUsuario_UsuarioCod ;
      private int[] H00MZ5_A63ContratanteUsuario_ContratanteCod ;
      private int[] H00MZ6_A127Sistema_Codigo ;
      private int[] H00MZ6_A60ContratanteUsuario_UsuarioCod ;
      private int[] H00MZ6_A63ContratanteUsuario_ContratanteCod ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV15HTTPRequest ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV7AddedKeyList ;
      [ObjectCollection(ItemType=typeof( double ))]
      private IGxCollection AV22NotAddedKeyList ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV5AddedDscList ;
      [ObjectCollection(ItemType=typeof( String ))]
      private IGxCollection AV20NotAddedDscList ;
      [ObjectCollection(ItemType=typeof( SdtMessages_Message ))]
      private IGxCollection AV42GXV5 ;
      private SdtContratanteUsuarioSistema AV12ContratanteUsuarioSistema ;
      private SdtMessages_Message AV19Message ;
      private wwpbaseobjects.SdtWWPContext AV27WWPContext ;
   }

   public class wp_associarsistema_contratanteusuario__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00MZ2 ;
          prmH00MZ2 = new Object[] {
          new Object[] {"@AV25Sistema_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00MZ3 ;
          prmH00MZ3 = new Object[] {
          new Object[] {"@AV29Contratante_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00MZ4 ;
          prmH00MZ4 = new Object[] {
          new Object[] {"@ContratanteUsuario_ContratanteCod",SqlDbType.Int,6,0} ,
          new Object[] {"@ContratanteUsuario_UsuarioCod",SqlDbType.Int,6,0} ,
          new Object[] {"@AV25Sistema_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00MZ5 ;
          prmH00MZ5 = new Object[] {
          new Object[] {"@AV29Contratante_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV28Usuario_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV25Sistema_Codigo",SqlDbType.Int,6,0}
          } ;
          Object[] prmH00MZ6 ;
          prmH00MZ6 = new Object[] {
          new Object[] {"@AV29Contratante_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV28Usuario_Codigo",SqlDbType.Int,6,0} ,
          new Object[] {"@AV25Sistema_Codigo",SqlDbType.Int,6,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H00MZ2", "SELECT TOP 1 [Sistema_Codigo], [Sistema_Sigla] FROM [Sistema] WITH (NOLOCK) WHERE [Sistema_Codigo] = @AV25Sistema_Codigo ORDER BY [Sistema_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00MZ2,1,0,false,true )
             ,new CursorDef("H00MZ3", "SELECT T2.[Usuario_PessoaCod] AS ContratanteUsuario_UsuarioPessoaCod, T1.[ContratanteUsuario_ContratanteCod], T1.[ContratanteUsuario_UsuarioCod] AS ContratanteUsuario_UsuarioCod, T3.[Pessoa_Nome] AS ContratanteUsuario_UsuarioPessoaNom FROM (([ContratanteUsuario] T1 WITH (NOLOCK) INNER JOIN [Usuario] T2 WITH (NOLOCK) ON T2.[Usuario_Codigo] = T1.[ContratanteUsuario_UsuarioCod]) LEFT JOIN [Pessoa] T3 WITH (NOLOCK) ON T3.[Pessoa_Codigo] = T2.[Usuario_PessoaCod]) WHERE T1.[ContratanteUsuario_ContratanteCod] = @AV29Contratante_Codigo ORDER BY T3.[Pessoa_Nome] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00MZ3,100,0,true,false )
             ,new CursorDef("H00MZ4", "SELECT [ContratanteUsuario_ContratanteCod], [ContratanteUsuario_UsuarioCod] AS ContratanteUsuario_UsuarioCod, [Sistema_Codigo] FROM [ContratanteUsuarioSistema] WITH (NOLOCK) WHERE [ContratanteUsuario_ContratanteCod] = @ContratanteUsuario_ContratanteCod and [ContratanteUsuario_UsuarioCod] = @ContratanteUsuario_UsuarioCod and [Sistema_Codigo] = @AV25Sistema_Codigo ORDER BY [ContratanteUsuario_ContratanteCod], [ContratanteUsuario_UsuarioCod], [Sistema_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00MZ4,1,0,false,true )
             ,new CursorDef("H00MZ5", "SELECT [Sistema_Codigo], [ContratanteUsuario_UsuarioCod] AS ContratanteUsuario_UsuarioCod, [ContratanteUsuario_ContratanteCod] FROM [ContratanteUsuarioSistema] WITH (NOLOCK) WHERE [ContratanteUsuario_ContratanteCod] = @AV29Contratante_Codigo and [ContratanteUsuario_UsuarioCod] = @AV28Usuario_Codigo and [Sistema_Codigo] = @AV25Sistema_Codigo ORDER BY [ContratanteUsuario_ContratanteCod], [ContratanteUsuario_UsuarioCod], [Sistema_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00MZ5,1,0,false,true )
             ,new CursorDef("H00MZ6", "SELECT [Sistema_Codigo], [ContratanteUsuario_UsuarioCod] AS ContratanteUsuario_UsuarioCod, [ContratanteUsuario_ContratanteCod] FROM [ContratanteUsuarioSistema] WITH (NOLOCK) WHERE [ContratanteUsuario_ContratanteCod] = @AV29Contratante_Codigo and [ContratanteUsuario_UsuarioCod] = @AV28Usuario_Codigo and [Sistema_Codigo] = @AV25Sistema_Codigo ORDER BY [ContratanteUsuario_ContratanteCod], [ContratanteUsuario_UsuarioCod], [Sistema_Codigo] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00MZ6,1,0,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 25) ;
                return;
             case 1 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((int[]) buf[2])[0] = rslt.getInt(2) ;
                ((int[]) buf[3])[0] = rslt.getInt(3) ;
                ((String[]) buf[4])[0] = rslt.getString(4, 100) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(4);
                return;
             case 2 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
             case 3 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
             case 4 :
                ((int[]) buf[0])[0] = rslt.getInt(1) ;
                ((int[]) buf[1])[0] = rslt.getInt(2) ;
                ((int[]) buf[2])[0] = rslt.getInt(3) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (int)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                return;
             case 3 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                return;
             case 4 :
                stmt.SetParameter(1, (int)parms[0]);
                stmt.SetParameter(2, (int)parms[1]);
                stmt.SetParameter(3, (int)parms[2]);
                return;
       }
    }

 }

}
